/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAChargeFeeDB;

/*
 * <p>ClassName: LAChargeFeeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险中介新建表
 * @CreateDate：2015-09-24
 */
public class LAChargeFeeSchema implements Schema, Cloneable
{
	// @Field
	/** 系列号 */
	private String CommisionSN;
	/** 管理机构 */
	private String ManageCom;
	/** 代理机构 */
	private String AgentCom;
	/** 展业类型 */
	private String BranchType;
	/** 计算年月 */
	private String WageNo;
	/** 渠道 */
	private String BranchType2;
	/** 交易金额 */
	private double TransMoney;
	/** 首续期续费比例 */
	private double FirConChargeRate;
	/** 首续期手续费金额 */
	private double FirConCharge;
	/** 月度奖金率 */
	private double MonChargeRate;
	/** 月度奖金 */
	private double MonCharge;
	/** 年度奖金率 */
	private double YearChargeRate;
	/** 年度奖 */
	private double YearCharge;
	/** 继续率奖金比例 */
	private double ContChargeRate;
	/** 继续率奖金 */
	private double ContCharge;
	/** 销售奖金率 */
	private double SaleBonusRate;
	/** 销售奖金 */
	private double SaleBonus;
	/** 续年度服务津贴率 */
	private double ConServBountyRate;
	/** 续年度服务津贴 */
	private double ConServBounty;
	/** 月度奖加款 */
	private double MonAddFee;
	/** 季度奖加款 */
	private double QuarterlyAddFee;
	/** 年度奖加款 */
	private double YearAddFee;
	/** 继续率奖加款 */
	private double ConAddFee;
	/** 其它加款1 */
	private double OtherAddFee1;
	/** 其它扣款1 */
	private double OtherMinFee1;
	/** 其它加款2 */
	private double OtherAddFee2;
	/** 其它扣款2 */
	private double OtherMinFee2;
	/** 其它加款3 */
	private double OtherAddFee3;
	/** 其它扣款3 */
	private double OtherMinFee3;
	/** 季度奖比率 */
	private double ExtraChargeRate1;
	/** 季度奖 */
	private double ExtraCharge1;
	/** 备用手续费率2 */
	private double ExtraChargeRate2;
	/** 备用手续费2 */
	private double ExtraCharge2;
	/** 备用手续费率3 */
	private double ExtraChargeRate3;
	/** 备用手续费3 */
	private double ExtraCharge3;
	/** 备用手续费率4 */
	private double ExtraChargeRate4;
	/** 备用手续费4 */
	private double ExtraCharge4;
	/** T1 */
	private Date T1;
	/** T2 */
	private Date T2;
	/** T3 */
	private Date T3;
	/** T4 */
	private Date T4;
	/** P1 */
	private String P1;
	/** P2 */
	private String P2;
	/** P3 */
	private String P3;
	/** P4 */
	private String P4;
	/** F1 */
	private double F1;
	/** F2 */
	private double F2;
	/** F3 */
	private double F3;
	/** F4 */
	private double F4;
	/** F5 */
	private double F5;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 55;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAChargeFeeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CommisionSN";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAChargeFeeSchema cloned = (LAChargeFeeSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCommisionSN()
	{
		return CommisionSN;
	}
	public void setCommisionSN(String aCommisionSN)
	{
		CommisionSN = aCommisionSN;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public double getTransMoney()
	{
		return TransMoney;
	}
	public void setTransMoney(double aTransMoney)
	{
		TransMoney = Arith.round(aTransMoney,2);
	}
	public void setTransMoney(String aTransMoney)
	{
		if (aTransMoney != null && !aTransMoney.equals(""))
		{
			Double tDouble = new Double(aTransMoney);
			double d = tDouble.doubleValue();
                TransMoney = Arith.round(d,2);
		}
	}

	public double getFirConChargeRate()
	{
		return FirConChargeRate;
	}
	public void setFirConChargeRate(double aFirConChargeRate)
	{
		FirConChargeRate = Arith.round(aFirConChargeRate,6);
	}
	public void setFirConChargeRate(String aFirConChargeRate)
	{
		if (aFirConChargeRate != null && !aFirConChargeRate.equals(""))
		{
			Double tDouble = new Double(aFirConChargeRate);
			double d = tDouble.doubleValue();
                FirConChargeRate = Arith.round(d,6);
		}
	}

	public double getFirConCharge()
	{
		return FirConCharge;
	}
	public void setFirConCharge(double aFirConCharge)
	{
		FirConCharge = Arith.round(aFirConCharge,2);
	}
	public void setFirConCharge(String aFirConCharge)
	{
		if (aFirConCharge != null && !aFirConCharge.equals(""))
		{
			Double tDouble = new Double(aFirConCharge);
			double d = tDouble.doubleValue();
                FirConCharge = Arith.round(d,2);
		}
	}

	public double getMonChargeRate()
	{
		return MonChargeRate;
	}
	public void setMonChargeRate(double aMonChargeRate)
	{
		MonChargeRate = Arith.round(aMonChargeRate,6);
	}
	public void setMonChargeRate(String aMonChargeRate)
	{
		if (aMonChargeRate != null && !aMonChargeRate.equals(""))
		{
			Double tDouble = new Double(aMonChargeRate);
			double d = tDouble.doubleValue();
                MonChargeRate = Arith.round(d,6);
		}
	}

	public double getMonCharge()
	{
		return MonCharge;
	}
	public void setMonCharge(double aMonCharge)
	{
		MonCharge = Arith.round(aMonCharge,2);
	}
	public void setMonCharge(String aMonCharge)
	{
		if (aMonCharge != null && !aMonCharge.equals(""))
		{
			Double tDouble = new Double(aMonCharge);
			double d = tDouble.doubleValue();
                MonCharge = Arith.round(d,2);
		}
	}

	public double getYearChargeRate()
	{
		return YearChargeRate;
	}
	public void setYearChargeRate(double aYearChargeRate)
	{
		YearChargeRate = Arith.round(aYearChargeRate,6);
	}
	public void setYearChargeRate(String aYearChargeRate)
	{
		if (aYearChargeRate != null && !aYearChargeRate.equals(""))
		{
			Double tDouble = new Double(aYearChargeRate);
			double d = tDouble.doubleValue();
                YearChargeRate = Arith.round(d,6);
		}
	}

	public double getYearCharge()
	{
		return YearCharge;
	}
	public void setYearCharge(double aYearCharge)
	{
		YearCharge = Arith.round(aYearCharge,2);
	}
	public void setYearCharge(String aYearCharge)
	{
		if (aYearCharge != null && !aYearCharge.equals(""))
		{
			Double tDouble = new Double(aYearCharge);
			double d = tDouble.doubleValue();
                YearCharge = Arith.round(d,2);
		}
	}

	public double getContChargeRate()
	{
		return ContChargeRate;
	}
	public void setContChargeRate(double aContChargeRate)
	{
		ContChargeRate = Arith.round(aContChargeRate,6);
	}
	public void setContChargeRate(String aContChargeRate)
	{
		if (aContChargeRate != null && !aContChargeRate.equals(""))
		{
			Double tDouble = new Double(aContChargeRate);
			double d = tDouble.doubleValue();
                ContChargeRate = Arith.round(d,6);
		}
	}

	public double getContCharge()
	{
		return ContCharge;
	}
	public void setContCharge(double aContCharge)
	{
		ContCharge = Arith.round(aContCharge,2);
	}
	public void setContCharge(String aContCharge)
	{
		if (aContCharge != null && !aContCharge.equals(""))
		{
			Double tDouble = new Double(aContCharge);
			double d = tDouble.doubleValue();
                ContCharge = Arith.round(d,2);
		}
	}

	public double getSaleBonusRate()
	{
		return SaleBonusRate;
	}
	public void setSaleBonusRate(double aSaleBonusRate)
	{
		SaleBonusRate = Arith.round(aSaleBonusRate,6);
	}
	public void setSaleBonusRate(String aSaleBonusRate)
	{
		if (aSaleBonusRate != null && !aSaleBonusRate.equals(""))
		{
			Double tDouble = new Double(aSaleBonusRate);
			double d = tDouble.doubleValue();
                SaleBonusRate = Arith.round(d,6);
		}
	}

	public double getSaleBonus()
	{
		return SaleBonus;
	}
	public void setSaleBonus(double aSaleBonus)
	{
		SaleBonus = Arith.round(aSaleBonus,2);
	}
	public void setSaleBonus(String aSaleBonus)
	{
		if (aSaleBonus != null && !aSaleBonus.equals(""))
		{
			Double tDouble = new Double(aSaleBonus);
			double d = tDouble.doubleValue();
                SaleBonus = Arith.round(d,2);
		}
	}

	public double getConServBountyRate()
	{
		return ConServBountyRate;
	}
	public void setConServBountyRate(double aConServBountyRate)
	{
		ConServBountyRate = Arith.round(aConServBountyRate,6);
	}
	public void setConServBountyRate(String aConServBountyRate)
	{
		if (aConServBountyRate != null && !aConServBountyRate.equals(""))
		{
			Double tDouble = new Double(aConServBountyRate);
			double d = tDouble.doubleValue();
                ConServBountyRate = Arith.round(d,6);
		}
	}

	public double getConServBounty()
	{
		return ConServBounty;
	}
	public void setConServBounty(double aConServBounty)
	{
		ConServBounty = Arith.round(aConServBounty,2);
	}
	public void setConServBounty(String aConServBounty)
	{
		if (aConServBounty != null && !aConServBounty.equals(""))
		{
			Double tDouble = new Double(aConServBounty);
			double d = tDouble.doubleValue();
                ConServBounty = Arith.round(d,2);
		}
	}

	public double getMonAddFee()
	{
		return MonAddFee;
	}
	public void setMonAddFee(double aMonAddFee)
	{
		MonAddFee = Arith.round(aMonAddFee,2);
	}
	public void setMonAddFee(String aMonAddFee)
	{
		if (aMonAddFee != null && !aMonAddFee.equals(""))
		{
			Double tDouble = new Double(aMonAddFee);
			double d = tDouble.doubleValue();
                MonAddFee = Arith.round(d,2);
		}
	}

	public double getQuarterlyAddFee()
	{
		return QuarterlyAddFee;
	}
	public void setQuarterlyAddFee(double aQuarterlyAddFee)
	{
		QuarterlyAddFee = Arith.round(aQuarterlyAddFee,2);
	}
	public void setQuarterlyAddFee(String aQuarterlyAddFee)
	{
		if (aQuarterlyAddFee != null && !aQuarterlyAddFee.equals(""))
		{
			Double tDouble = new Double(aQuarterlyAddFee);
			double d = tDouble.doubleValue();
                QuarterlyAddFee = Arith.round(d,2);
		}
	}

	public double getYearAddFee()
	{
		return YearAddFee;
	}
	public void setYearAddFee(double aYearAddFee)
	{
		YearAddFee = Arith.round(aYearAddFee,2);
	}
	public void setYearAddFee(String aYearAddFee)
	{
		if (aYearAddFee != null && !aYearAddFee.equals(""))
		{
			Double tDouble = new Double(aYearAddFee);
			double d = tDouble.doubleValue();
                YearAddFee = Arith.round(d,2);
		}
	}

	public double getConAddFee()
	{
		return ConAddFee;
	}
	public void setConAddFee(double aConAddFee)
	{
		ConAddFee = Arith.round(aConAddFee,2);
	}
	public void setConAddFee(String aConAddFee)
	{
		if (aConAddFee != null && !aConAddFee.equals(""))
		{
			Double tDouble = new Double(aConAddFee);
			double d = tDouble.doubleValue();
                ConAddFee = Arith.round(d,2);
		}
	}

	public double getOtherAddFee1()
	{
		return OtherAddFee1;
	}
	public void setOtherAddFee1(double aOtherAddFee1)
	{
		OtherAddFee1 = Arith.round(aOtherAddFee1,2);
	}
	public void setOtherAddFee1(String aOtherAddFee1)
	{
		if (aOtherAddFee1 != null && !aOtherAddFee1.equals(""))
		{
			Double tDouble = new Double(aOtherAddFee1);
			double d = tDouble.doubleValue();
                OtherAddFee1 = Arith.round(d,2);
		}
	}

	public double getOtherMinFee1()
	{
		return OtherMinFee1;
	}
	public void setOtherMinFee1(double aOtherMinFee1)
	{
		OtherMinFee1 = Arith.round(aOtherMinFee1,2);
	}
	public void setOtherMinFee1(String aOtherMinFee1)
	{
		if (aOtherMinFee1 != null && !aOtherMinFee1.equals(""))
		{
			Double tDouble = new Double(aOtherMinFee1);
			double d = tDouble.doubleValue();
                OtherMinFee1 = Arith.round(d,2);
		}
	}

	public double getOtherAddFee2()
	{
		return OtherAddFee2;
	}
	public void setOtherAddFee2(double aOtherAddFee2)
	{
		OtherAddFee2 = Arith.round(aOtherAddFee2,2);
	}
	public void setOtherAddFee2(String aOtherAddFee2)
	{
		if (aOtherAddFee2 != null && !aOtherAddFee2.equals(""))
		{
			Double tDouble = new Double(aOtherAddFee2);
			double d = tDouble.doubleValue();
                OtherAddFee2 = Arith.round(d,2);
		}
	}

	public double getOtherMinFee2()
	{
		return OtherMinFee2;
	}
	public void setOtherMinFee2(double aOtherMinFee2)
	{
		OtherMinFee2 = Arith.round(aOtherMinFee2,2);
	}
	public void setOtherMinFee2(String aOtherMinFee2)
	{
		if (aOtherMinFee2 != null && !aOtherMinFee2.equals(""))
		{
			Double tDouble = new Double(aOtherMinFee2);
			double d = tDouble.doubleValue();
                OtherMinFee2 = Arith.round(d,2);
		}
	}

	public double getOtherAddFee3()
	{
		return OtherAddFee3;
	}
	public void setOtherAddFee3(double aOtherAddFee3)
	{
		OtherAddFee3 = Arith.round(aOtherAddFee3,2);
	}
	public void setOtherAddFee3(String aOtherAddFee3)
	{
		if (aOtherAddFee3 != null && !aOtherAddFee3.equals(""))
		{
			Double tDouble = new Double(aOtherAddFee3);
			double d = tDouble.doubleValue();
                OtherAddFee3 = Arith.round(d,2);
		}
	}

	public double getOtherMinFee3()
	{
		return OtherMinFee3;
	}
	public void setOtherMinFee3(double aOtherMinFee3)
	{
		OtherMinFee3 = Arith.round(aOtherMinFee3,2);
	}
	public void setOtherMinFee3(String aOtherMinFee3)
	{
		if (aOtherMinFee3 != null && !aOtherMinFee3.equals(""))
		{
			Double tDouble = new Double(aOtherMinFee3);
			double d = tDouble.doubleValue();
                OtherMinFee3 = Arith.round(d,2);
		}
	}

	public double getExtraChargeRate1()
	{
		return ExtraChargeRate1;
	}
	public void setExtraChargeRate1(double aExtraChargeRate1)
	{
		ExtraChargeRate1 = Arith.round(aExtraChargeRate1,6);
	}
	public void setExtraChargeRate1(String aExtraChargeRate1)
	{
		if (aExtraChargeRate1 != null && !aExtraChargeRate1.equals(""))
		{
			Double tDouble = new Double(aExtraChargeRate1);
			double d = tDouble.doubleValue();
                ExtraChargeRate1 = Arith.round(d,6);
		}
	}

	public double getExtraCharge1()
	{
		return ExtraCharge1;
	}
	public void setExtraCharge1(double aExtraCharge1)
	{
		ExtraCharge1 = Arith.round(aExtraCharge1,2);
	}
	public void setExtraCharge1(String aExtraCharge1)
	{
		if (aExtraCharge1 != null && !aExtraCharge1.equals(""))
		{
			Double tDouble = new Double(aExtraCharge1);
			double d = tDouble.doubleValue();
                ExtraCharge1 = Arith.round(d,2);
		}
	}

	public double getExtraChargeRate2()
	{
		return ExtraChargeRate2;
	}
	public void setExtraChargeRate2(double aExtraChargeRate2)
	{
		ExtraChargeRate2 = Arith.round(aExtraChargeRate2,6);
	}
	public void setExtraChargeRate2(String aExtraChargeRate2)
	{
		if (aExtraChargeRate2 != null && !aExtraChargeRate2.equals(""))
		{
			Double tDouble = new Double(aExtraChargeRate2);
			double d = tDouble.doubleValue();
                ExtraChargeRate2 = Arith.round(d,6);
		}
	}

	public double getExtraCharge2()
	{
		return ExtraCharge2;
	}
	public void setExtraCharge2(double aExtraCharge2)
	{
		ExtraCharge2 = Arith.round(aExtraCharge2,2);
	}
	public void setExtraCharge2(String aExtraCharge2)
	{
		if (aExtraCharge2 != null && !aExtraCharge2.equals(""))
		{
			Double tDouble = new Double(aExtraCharge2);
			double d = tDouble.doubleValue();
                ExtraCharge2 = Arith.round(d,2);
		}
	}

	public double getExtraChargeRate3()
	{
		return ExtraChargeRate3;
	}
	public void setExtraChargeRate3(double aExtraChargeRate3)
	{
		ExtraChargeRate3 = Arith.round(aExtraChargeRate3,6);
	}
	public void setExtraChargeRate3(String aExtraChargeRate3)
	{
		if (aExtraChargeRate3 != null && !aExtraChargeRate3.equals(""))
		{
			Double tDouble = new Double(aExtraChargeRate3);
			double d = tDouble.doubleValue();
                ExtraChargeRate3 = Arith.round(d,6);
		}
	}

	public double getExtraCharge3()
	{
		return ExtraCharge3;
	}
	public void setExtraCharge3(double aExtraCharge3)
	{
		ExtraCharge3 = Arith.round(aExtraCharge3,2);
	}
	public void setExtraCharge3(String aExtraCharge3)
	{
		if (aExtraCharge3 != null && !aExtraCharge3.equals(""))
		{
			Double tDouble = new Double(aExtraCharge3);
			double d = tDouble.doubleValue();
                ExtraCharge3 = Arith.round(d,2);
		}
	}

	public double getExtraChargeRate4()
	{
		return ExtraChargeRate4;
	}
	public void setExtraChargeRate4(double aExtraChargeRate4)
	{
		ExtraChargeRate4 = Arith.round(aExtraChargeRate4,6);
	}
	public void setExtraChargeRate4(String aExtraChargeRate4)
	{
		if (aExtraChargeRate4 != null && !aExtraChargeRate4.equals(""))
		{
			Double tDouble = new Double(aExtraChargeRate4);
			double d = tDouble.doubleValue();
                ExtraChargeRate4 = Arith.round(d,6);
		}
	}

	public double getExtraCharge4()
	{
		return ExtraCharge4;
	}
	public void setExtraCharge4(double aExtraCharge4)
	{
		ExtraCharge4 = Arith.round(aExtraCharge4,2);
	}
	public void setExtraCharge4(String aExtraCharge4)
	{
		if (aExtraCharge4 != null && !aExtraCharge4.equals(""))
		{
			Double tDouble = new Double(aExtraCharge4);
			double d = tDouble.doubleValue();
                ExtraCharge4 = Arith.round(d,2);
		}
	}

	public String getT1()
	{
		if( T1 != null )
			return fDate.getString(T1);
		else
			return null;
	}
	public void setT1(Date aT1)
	{
		T1 = aT1;
	}
	public void setT1(String aT1)
	{
		if (aT1 != null && !aT1.equals("") )
		{
			T1 = fDate.getDate( aT1 );
		}
		else
			T1 = null;
	}

	public String getT2()
	{
		if( T2 != null )
			return fDate.getString(T2);
		else
			return null;
	}
	public void setT2(Date aT2)
	{
		T2 = aT2;
	}
	public void setT2(String aT2)
	{
		if (aT2 != null && !aT2.equals("") )
		{
			T2 = fDate.getDate( aT2 );
		}
		else
			T2 = null;
	}

	public String getT3()
	{
		if( T3 != null )
			return fDate.getString(T3);
		else
			return null;
	}
	public void setT3(Date aT3)
	{
		T3 = aT3;
	}
	public void setT3(String aT3)
	{
		if (aT3 != null && !aT3.equals("") )
		{
			T3 = fDate.getDate( aT3 );
		}
		else
			T3 = null;
	}

	public String getT4()
	{
		if( T4 != null )
			return fDate.getString(T4);
		else
			return null;
	}
	public void setT4(Date aT4)
	{
		T4 = aT4;
	}
	public void setT4(String aT4)
	{
		if (aT4 != null && !aT4.equals("") )
		{
			T4 = fDate.getDate( aT4 );
		}
		else
			T4 = null;
	}

	public String getP1()
	{
		return P1;
	}
	public void setP1(String aP1)
	{
		P1 = aP1;
	}
	public String getP2()
	{
		return P2;
	}
	public void setP2(String aP2)
	{
		P2 = aP2;
	}
	public String getP3()
	{
		return P3;
	}
	public void setP3(String aP3)
	{
		P3 = aP3;
	}
	public String getP4()
	{
		return P4;
	}
	public void setP4(String aP4)
	{
		P4 = aP4;
	}
	public double getF1()
	{
		return F1;
	}
	public void setF1(double aF1)
	{
		F1 = Arith.round(aF1,2);
	}
	public void setF1(String aF1)
	{
		if (aF1 != null && !aF1.equals(""))
		{
			Double tDouble = new Double(aF1);
			double d = tDouble.doubleValue();
                F1 = Arith.round(d,2);
		}
	}

	public double getF2()
	{
		return F2;
	}
	public void setF2(double aF2)
	{
		F2 = Arith.round(aF2,2);
	}
	public void setF2(String aF2)
	{
		if (aF2 != null && !aF2.equals(""))
		{
			Double tDouble = new Double(aF2);
			double d = tDouble.doubleValue();
                F2 = Arith.round(d,2);
		}
	}

	public double getF3()
	{
		return F3;
	}
	public void setF3(double aF3)
	{
		F3 = Arith.round(aF3,2);
	}
	public void setF3(String aF3)
	{
		if (aF3 != null && !aF3.equals(""))
		{
			Double tDouble = new Double(aF3);
			double d = tDouble.doubleValue();
                F3 = Arith.round(d,2);
		}
	}

	public double getF4()
	{
		return F4;
	}
	public void setF4(double aF4)
	{
		F4 = Arith.round(aF4,2);
	}
	public void setF4(String aF4)
	{
		if (aF4 != null && !aF4.equals(""))
		{
			Double tDouble = new Double(aF4);
			double d = tDouble.doubleValue();
                F4 = Arith.round(d,2);
		}
	}

	public double getF5()
	{
		return F5;
	}
	public void setF5(double aF5)
	{
		F5 = Arith.round(aF5,2);
	}
	public void setF5(String aF5)
	{
		if (aF5 != null && !aF5.equals(""))
		{
			Double tDouble = new Double(aF5);
			double d = tDouble.doubleValue();
                F5 = Arith.round(d,2);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LAChargeFeeSchema 对象给 Schema 赋值
	* @param: aLAChargeFeeSchema LAChargeFeeSchema
	**/
	public void setSchema(LAChargeFeeSchema aLAChargeFeeSchema)
	{
		this.CommisionSN = aLAChargeFeeSchema.getCommisionSN();
		this.ManageCom = aLAChargeFeeSchema.getManageCom();
		this.AgentCom = aLAChargeFeeSchema.getAgentCom();
		this.BranchType = aLAChargeFeeSchema.getBranchType();
		this.WageNo = aLAChargeFeeSchema.getWageNo();
		this.BranchType2 = aLAChargeFeeSchema.getBranchType2();
		this.TransMoney = aLAChargeFeeSchema.getTransMoney();
		this.FirConChargeRate = aLAChargeFeeSchema.getFirConChargeRate();
		this.FirConCharge = aLAChargeFeeSchema.getFirConCharge();
		this.MonChargeRate = aLAChargeFeeSchema.getMonChargeRate();
		this.MonCharge = aLAChargeFeeSchema.getMonCharge();
		this.YearChargeRate = aLAChargeFeeSchema.getYearChargeRate();
		this.YearCharge = aLAChargeFeeSchema.getYearCharge();
		this.ContChargeRate = aLAChargeFeeSchema.getContChargeRate();
		this.ContCharge = aLAChargeFeeSchema.getContCharge();
		this.SaleBonusRate = aLAChargeFeeSchema.getSaleBonusRate();
		this.SaleBonus = aLAChargeFeeSchema.getSaleBonus();
		this.ConServBountyRate = aLAChargeFeeSchema.getConServBountyRate();
		this.ConServBounty = aLAChargeFeeSchema.getConServBounty();
		this.MonAddFee = aLAChargeFeeSchema.getMonAddFee();
		this.QuarterlyAddFee = aLAChargeFeeSchema.getQuarterlyAddFee();
		this.YearAddFee = aLAChargeFeeSchema.getYearAddFee();
		this.ConAddFee = aLAChargeFeeSchema.getConAddFee();
		this.OtherAddFee1 = aLAChargeFeeSchema.getOtherAddFee1();
		this.OtherMinFee1 = aLAChargeFeeSchema.getOtherMinFee1();
		this.OtherAddFee2 = aLAChargeFeeSchema.getOtherAddFee2();
		this.OtherMinFee2 = aLAChargeFeeSchema.getOtherMinFee2();
		this.OtherAddFee3 = aLAChargeFeeSchema.getOtherAddFee3();
		this.OtherMinFee3 = aLAChargeFeeSchema.getOtherMinFee3();
		this.ExtraChargeRate1 = aLAChargeFeeSchema.getExtraChargeRate1();
		this.ExtraCharge1 = aLAChargeFeeSchema.getExtraCharge1();
		this.ExtraChargeRate2 = aLAChargeFeeSchema.getExtraChargeRate2();
		this.ExtraCharge2 = aLAChargeFeeSchema.getExtraCharge2();
		this.ExtraChargeRate3 = aLAChargeFeeSchema.getExtraChargeRate3();
		this.ExtraCharge3 = aLAChargeFeeSchema.getExtraCharge3();
		this.ExtraChargeRate4 = aLAChargeFeeSchema.getExtraChargeRate4();
		this.ExtraCharge4 = aLAChargeFeeSchema.getExtraCharge4();
		this.T1 = fDate.getDate( aLAChargeFeeSchema.getT1());
		this.T2 = fDate.getDate( aLAChargeFeeSchema.getT2());
		this.T3 = fDate.getDate( aLAChargeFeeSchema.getT3());
		this.T4 = fDate.getDate( aLAChargeFeeSchema.getT4());
		this.P1 = aLAChargeFeeSchema.getP1();
		this.P2 = aLAChargeFeeSchema.getP2();
		this.P3 = aLAChargeFeeSchema.getP3();
		this.P4 = aLAChargeFeeSchema.getP4();
		this.F1 = aLAChargeFeeSchema.getF1();
		this.F2 = aLAChargeFeeSchema.getF2();
		this.F3 = aLAChargeFeeSchema.getF3();
		this.F4 = aLAChargeFeeSchema.getF4();
		this.F5 = aLAChargeFeeSchema.getF5();
		this.Operator = aLAChargeFeeSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAChargeFeeSchema.getMakeDate());
		this.MakeTime = aLAChargeFeeSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAChargeFeeSchema.getModifyDate());
		this.ModifyTime = aLAChargeFeeSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CommisionSN") == null )
				this.CommisionSN = null;
			else
				this.CommisionSN = rs.getString("CommisionSN").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			this.TransMoney = rs.getDouble("TransMoney");
			this.FirConChargeRate = rs.getDouble("FirConChargeRate");
			this.FirConCharge = rs.getDouble("FirConCharge");
			this.MonChargeRate = rs.getDouble("MonChargeRate");
			this.MonCharge = rs.getDouble("MonCharge");
			this.YearChargeRate = rs.getDouble("YearChargeRate");
			this.YearCharge = rs.getDouble("YearCharge");
			this.ContChargeRate = rs.getDouble("ContChargeRate");
			this.ContCharge = rs.getDouble("ContCharge");
			this.SaleBonusRate = rs.getDouble("SaleBonusRate");
			this.SaleBonus = rs.getDouble("SaleBonus");
			this.ConServBountyRate = rs.getDouble("ConServBountyRate");
			this.ConServBounty = rs.getDouble("ConServBounty");
			this.MonAddFee = rs.getDouble("MonAddFee");
			this.QuarterlyAddFee = rs.getDouble("QuarterlyAddFee");
			this.YearAddFee = rs.getDouble("YearAddFee");
			this.ConAddFee = rs.getDouble("ConAddFee");
			this.OtherAddFee1 = rs.getDouble("OtherAddFee1");
			this.OtherMinFee1 = rs.getDouble("OtherMinFee1");
			this.OtherAddFee2 = rs.getDouble("OtherAddFee2");
			this.OtherMinFee2 = rs.getDouble("OtherMinFee2");
			this.OtherAddFee3 = rs.getDouble("OtherAddFee3");
			this.OtherMinFee3 = rs.getDouble("OtherMinFee3");
			this.ExtraChargeRate1 = rs.getDouble("ExtraChargeRate1");
			this.ExtraCharge1 = rs.getDouble("ExtraCharge1");
			this.ExtraChargeRate2 = rs.getDouble("ExtraChargeRate2");
			this.ExtraCharge2 = rs.getDouble("ExtraCharge2");
			this.ExtraChargeRate3 = rs.getDouble("ExtraChargeRate3");
			this.ExtraCharge3 = rs.getDouble("ExtraCharge3");
			this.ExtraChargeRate4 = rs.getDouble("ExtraChargeRate4");
			this.ExtraCharge4 = rs.getDouble("ExtraCharge4");
			this.T1 = rs.getDate("T1");
			this.T2 = rs.getDate("T2");
			this.T3 = rs.getDate("T3");
			this.T4 = rs.getDate("T4");
			if( rs.getString("P1") == null )
				this.P1 = null;
			else
				this.P1 = rs.getString("P1").trim();

			if( rs.getString("P2") == null )
				this.P2 = null;
			else
				this.P2 = rs.getString("P2").trim();

			if( rs.getString("P3") == null )
				this.P3 = null;
			else
				this.P3 = rs.getString("P3").trim();

			if( rs.getString("P4") == null )
				this.P4 = null;
			else
				this.P4 = rs.getString("P4").trim();

			this.F1 = rs.getDouble("F1");
			this.F2 = rs.getDouble("F2");
			this.F3 = rs.getDouble("F3");
			this.F4 = rs.getDouble("F4");
			this.F5 = rs.getDouble("F5");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAChargeFee表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAChargeFeeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAChargeFeeSchema getSchema()
	{
		LAChargeFeeSchema aLAChargeFeeSchema = new LAChargeFeeSchema();
		aLAChargeFeeSchema.setSchema(this);
		return aLAChargeFeeSchema;
	}

	public LAChargeFeeDB getDB()
	{
		LAChargeFeeDB aDBOper = new LAChargeFeeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAChargeFee描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CommisionSN)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FirConChargeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FirConCharge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MonChargeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MonCharge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(YearChargeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(YearCharge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ContChargeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ContCharge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SaleBonusRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SaleBonus));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ConServBountyRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ConServBounty));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MonAddFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(QuarterlyAddFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(YearAddFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ConAddFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherAddFee1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherMinFee1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherAddFee2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherMinFee2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherAddFee3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherMinFee3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExtraChargeRate1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExtraCharge1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExtraChargeRate2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExtraCharge2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExtraChargeRate3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExtraCharge3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExtraChargeRate4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExtraCharge4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( T1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( T2 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( T3 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( T4 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F5));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAChargeFee>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CommisionSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			TransMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			FirConChargeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			FirConCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			MonChargeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			MonCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			YearChargeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			YearCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			ContChargeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			ContCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			SaleBonusRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			SaleBonus = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			ConServBountyRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			ConServBounty = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			MonAddFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			QuarterlyAddFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			YearAddFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			ConAddFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			OtherAddFee1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			OtherMinFee1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			OtherAddFee2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			OtherMinFee2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			OtherAddFee3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			OtherMinFee3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			ExtraChargeRate1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			ExtraCharge1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			ExtraChargeRate2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			ExtraCharge2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).doubleValue();
			ExtraChargeRate3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).doubleValue();
			ExtraCharge3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).doubleValue();
			ExtraChargeRate4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
			ExtraCharge4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).doubleValue();
			T1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,SysConst.PACKAGESPILTER));
			T2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
			T3 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,SysConst.PACKAGESPILTER));
			T4 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
			P1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			P2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			P3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			P4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			F1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,46,SysConst.PACKAGESPILTER))).doubleValue();
			F2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).doubleValue();
			F3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,48,SysConst.PACKAGESPILTER))).doubleValue();
			F4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,49,SysConst.PACKAGESPILTER))).doubleValue();
			F5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,50,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAChargeFeeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CommisionSN"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommisionSN));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("TransMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMoney));
		}
		if (FCode.equals("FirConChargeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirConChargeRate));
		}
		if (FCode.equals("FirConCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirConCharge));
		}
		if (FCode.equals("MonChargeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MonChargeRate));
		}
		if (FCode.equals("MonCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MonCharge));
		}
		if (FCode.equals("YearChargeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YearChargeRate));
		}
		if (FCode.equals("YearCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YearCharge));
		}
		if (FCode.equals("ContChargeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContChargeRate));
		}
		if (FCode.equals("ContCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContCharge));
		}
		if (FCode.equals("SaleBonusRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleBonusRate));
		}
		if (FCode.equals("SaleBonus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleBonus));
		}
		if (FCode.equals("ConServBountyRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConServBountyRate));
		}
		if (FCode.equals("ConServBounty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConServBounty));
		}
		if (FCode.equals("MonAddFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MonAddFee));
		}
		if (FCode.equals("QuarterlyAddFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(QuarterlyAddFee));
		}
		if (FCode.equals("YearAddFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YearAddFee));
		}
		if (FCode.equals("ConAddFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConAddFee));
		}
		if (FCode.equals("OtherAddFee1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherAddFee1));
		}
		if (FCode.equals("OtherMinFee1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherMinFee1));
		}
		if (FCode.equals("OtherAddFee2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherAddFee2));
		}
		if (FCode.equals("OtherMinFee2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherMinFee2));
		}
		if (FCode.equals("OtherAddFee3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherAddFee3));
		}
		if (FCode.equals("OtherMinFee3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherMinFee3));
		}
		if (FCode.equals("ExtraChargeRate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExtraChargeRate1));
		}
		if (FCode.equals("ExtraCharge1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExtraCharge1));
		}
		if (FCode.equals("ExtraChargeRate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExtraChargeRate2));
		}
		if (FCode.equals("ExtraCharge2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExtraCharge2));
		}
		if (FCode.equals("ExtraChargeRate3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExtraChargeRate3));
		}
		if (FCode.equals("ExtraCharge3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExtraCharge3));
		}
		if (FCode.equals("ExtraChargeRate4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExtraChargeRate4));
		}
		if (FCode.equals("ExtraCharge4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExtraCharge4));
		}
		if (FCode.equals("T1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getT1()));
		}
		if (FCode.equals("T2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getT2()));
		}
		if (FCode.equals("T3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getT3()));
		}
		if (FCode.equals("T4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getT4()));
		}
		if (FCode.equals("P1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P1));
		}
		if (FCode.equals("P2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P2));
		}
		if (FCode.equals("P3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P3));
		}
		if (FCode.equals("P4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P4));
		}
		if (FCode.equals("F1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F1));
		}
		if (FCode.equals("F2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F2));
		}
		if (FCode.equals("F3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F3));
		}
		if (FCode.equals("F4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F4));
		}
		if (FCode.equals("F5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F5));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CommisionSN);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 6:
				strFieldValue = String.valueOf(TransMoney);
				break;
			case 7:
				strFieldValue = String.valueOf(FirConChargeRate);
				break;
			case 8:
				strFieldValue = String.valueOf(FirConCharge);
				break;
			case 9:
				strFieldValue = String.valueOf(MonChargeRate);
				break;
			case 10:
				strFieldValue = String.valueOf(MonCharge);
				break;
			case 11:
				strFieldValue = String.valueOf(YearChargeRate);
				break;
			case 12:
				strFieldValue = String.valueOf(YearCharge);
				break;
			case 13:
				strFieldValue = String.valueOf(ContChargeRate);
				break;
			case 14:
				strFieldValue = String.valueOf(ContCharge);
				break;
			case 15:
				strFieldValue = String.valueOf(SaleBonusRate);
				break;
			case 16:
				strFieldValue = String.valueOf(SaleBonus);
				break;
			case 17:
				strFieldValue = String.valueOf(ConServBountyRate);
				break;
			case 18:
				strFieldValue = String.valueOf(ConServBounty);
				break;
			case 19:
				strFieldValue = String.valueOf(MonAddFee);
				break;
			case 20:
				strFieldValue = String.valueOf(QuarterlyAddFee);
				break;
			case 21:
				strFieldValue = String.valueOf(YearAddFee);
				break;
			case 22:
				strFieldValue = String.valueOf(ConAddFee);
				break;
			case 23:
				strFieldValue = String.valueOf(OtherAddFee1);
				break;
			case 24:
				strFieldValue = String.valueOf(OtherMinFee1);
				break;
			case 25:
				strFieldValue = String.valueOf(OtherAddFee2);
				break;
			case 26:
				strFieldValue = String.valueOf(OtherMinFee2);
				break;
			case 27:
				strFieldValue = String.valueOf(OtherAddFee3);
				break;
			case 28:
				strFieldValue = String.valueOf(OtherMinFee3);
				break;
			case 29:
				strFieldValue = String.valueOf(ExtraChargeRate1);
				break;
			case 30:
				strFieldValue = String.valueOf(ExtraCharge1);
				break;
			case 31:
				strFieldValue = String.valueOf(ExtraChargeRate2);
				break;
			case 32:
				strFieldValue = String.valueOf(ExtraCharge2);
				break;
			case 33:
				strFieldValue = String.valueOf(ExtraChargeRate3);
				break;
			case 34:
				strFieldValue = String.valueOf(ExtraCharge3);
				break;
			case 35:
				strFieldValue = String.valueOf(ExtraChargeRate4);
				break;
			case 36:
				strFieldValue = String.valueOf(ExtraCharge4);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getT1()));
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getT2()));
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getT3()));
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getT4()));
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(P1);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(P2);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(P3);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(P4);
				break;
			case 45:
				strFieldValue = String.valueOf(F1);
				break;
			case 46:
				strFieldValue = String.valueOf(F2);
				break;
			case 47:
				strFieldValue = String.valueOf(F3);
				break;
			case 48:
				strFieldValue = String.valueOf(F4);
				break;
			case 49:
				strFieldValue = String.valueOf(F5);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CommisionSN"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CommisionSN = FValue.trim();
			}
			else
				CommisionSN = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("TransMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("FirConChargeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FirConChargeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("FirConCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FirConCharge = d;
			}
		}
		if (FCode.equalsIgnoreCase("MonChargeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MonChargeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("MonCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MonCharge = d;
			}
		}
		if (FCode.equalsIgnoreCase("YearChargeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				YearChargeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("YearCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				YearCharge = d;
			}
		}
		if (FCode.equalsIgnoreCase("ContChargeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ContChargeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ContCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ContCharge = d;
			}
		}
		if (FCode.equalsIgnoreCase("SaleBonusRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SaleBonusRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("SaleBonus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SaleBonus = d;
			}
		}
		if (FCode.equalsIgnoreCase("ConServBountyRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ConServBountyRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ConServBounty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ConServBounty = d;
			}
		}
		if (FCode.equalsIgnoreCase("MonAddFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MonAddFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("QuarterlyAddFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				QuarterlyAddFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("YearAddFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				YearAddFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("ConAddFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ConAddFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherAddFee1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherAddFee1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherMinFee1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherMinFee1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherAddFee2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherAddFee2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherMinFee2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherMinFee2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherAddFee3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherAddFee3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherMinFee3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherMinFee3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExtraChargeRate1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExtraChargeRate1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExtraCharge1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExtraCharge1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExtraChargeRate2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExtraChargeRate2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExtraCharge2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExtraCharge2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExtraChargeRate3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExtraChargeRate3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExtraCharge3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExtraCharge3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExtraChargeRate4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExtraChargeRate4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExtraCharge4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExtraCharge4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("T1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				T1 = fDate.getDate( FValue );
			}
			else
				T1 = null;
		}
		if (FCode.equalsIgnoreCase("T2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				T2 = fDate.getDate( FValue );
			}
			else
				T2 = null;
		}
		if (FCode.equalsIgnoreCase("T3"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				T3 = fDate.getDate( FValue );
			}
			else
				T3 = null;
		}
		if (FCode.equalsIgnoreCase("T4"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				T4 = fDate.getDate( FValue );
			}
			else
				T4 = null;
		}
		if (FCode.equalsIgnoreCase("P1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P1 = FValue.trim();
			}
			else
				P1 = null;
		}
		if (FCode.equalsIgnoreCase("P2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P2 = FValue.trim();
			}
			else
				P2 = null;
		}
		if (FCode.equalsIgnoreCase("P3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P3 = FValue.trim();
			}
			else
				P3 = null;
		}
		if (FCode.equalsIgnoreCase("P4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P4 = FValue.trim();
			}
			else
				P4 = null;
		}
		if (FCode.equalsIgnoreCase("F1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F5 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAChargeFeeSchema other = (LAChargeFeeSchema)otherObject;
		return
			(CommisionSN == null ? other.getCommisionSN() == null : CommisionSN.equals(other.getCommisionSN()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& TransMoney == other.getTransMoney()
			&& FirConChargeRate == other.getFirConChargeRate()
			&& FirConCharge == other.getFirConCharge()
			&& MonChargeRate == other.getMonChargeRate()
			&& MonCharge == other.getMonCharge()
			&& YearChargeRate == other.getYearChargeRate()
			&& YearCharge == other.getYearCharge()
			&& ContChargeRate == other.getContChargeRate()
			&& ContCharge == other.getContCharge()
			&& SaleBonusRate == other.getSaleBonusRate()
			&& SaleBonus == other.getSaleBonus()
			&& ConServBountyRate == other.getConServBountyRate()
			&& ConServBounty == other.getConServBounty()
			&& MonAddFee == other.getMonAddFee()
			&& QuarterlyAddFee == other.getQuarterlyAddFee()
			&& YearAddFee == other.getYearAddFee()
			&& ConAddFee == other.getConAddFee()
			&& OtherAddFee1 == other.getOtherAddFee1()
			&& OtherMinFee1 == other.getOtherMinFee1()
			&& OtherAddFee2 == other.getOtherAddFee2()
			&& OtherMinFee2 == other.getOtherMinFee2()
			&& OtherAddFee3 == other.getOtherAddFee3()
			&& OtherMinFee3 == other.getOtherMinFee3()
			&& ExtraChargeRate1 == other.getExtraChargeRate1()
			&& ExtraCharge1 == other.getExtraCharge1()
			&& ExtraChargeRate2 == other.getExtraChargeRate2()
			&& ExtraCharge2 == other.getExtraCharge2()
			&& ExtraChargeRate3 == other.getExtraChargeRate3()
			&& ExtraCharge3 == other.getExtraCharge3()
			&& ExtraChargeRate4 == other.getExtraChargeRate4()
			&& ExtraCharge4 == other.getExtraCharge4()
			&& (T1 == null ? other.getT1() == null : fDate.getString(T1).equals(other.getT1()))
			&& (T2 == null ? other.getT2() == null : fDate.getString(T2).equals(other.getT2()))
			&& (T3 == null ? other.getT3() == null : fDate.getString(T3).equals(other.getT3()))
			&& (T4 == null ? other.getT4() == null : fDate.getString(T4).equals(other.getT4()))
			&& (P1 == null ? other.getP1() == null : P1.equals(other.getP1()))
			&& (P2 == null ? other.getP2() == null : P2.equals(other.getP2()))
			&& (P3 == null ? other.getP3() == null : P3.equals(other.getP3()))
			&& (P4 == null ? other.getP4() == null : P4.equals(other.getP4()))
			&& F1 == other.getF1()
			&& F2 == other.getF2()
			&& F3 == other.getF3()
			&& F4 == other.getF4()
			&& F5 == other.getF5()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CommisionSN") ) {
			return 0;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 1;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 2;
		}
		if( strFieldName.equals("BranchType") ) {
			return 3;
		}
		if( strFieldName.equals("WageNo") ) {
			return 4;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 5;
		}
		if( strFieldName.equals("TransMoney") ) {
			return 6;
		}
		if( strFieldName.equals("FirConChargeRate") ) {
			return 7;
		}
		if( strFieldName.equals("FirConCharge") ) {
			return 8;
		}
		if( strFieldName.equals("MonChargeRate") ) {
			return 9;
		}
		if( strFieldName.equals("MonCharge") ) {
			return 10;
		}
		if( strFieldName.equals("YearChargeRate") ) {
			return 11;
		}
		if( strFieldName.equals("YearCharge") ) {
			return 12;
		}
		if( strFieldName.equals("ContChargeRate") ) {
			return 13;
		}
		if( strFieldName.equals("ContCharge") ) {
			return 14;
		}
		if( strFieldName.equals("SaleBonusRate") ) {
			return 15;
		}
		if( strFieldName.equals("SaleBonus") ) {
			return 16;
		}
		if( strFieldName.equals("ConServBountyRate") ) {
			return 17;
		}
		if( strFieldName.equals("ConServBounty") ) {
			return 18;
		}
		if( strFieldName.equals("MonAddFee") ) {
			return 19;
		}
		if( strFieldName.equals("QuarterlyAddFee") ) {
			return 20;
		}
		if( strFieldName.equals("YearAddFee") ) {
			return 21;
		}
		if( strFieldName.equals("ConAddFee") ) {
			return 22;
		}
		if( strFieldName.equals("OtherAddFee1") ) {
			return 23;
		}
		if( strFieldName.equals("OtherMinFee1") ) {
			return 24;
		}
		if( strFieldName.equals("OtherAddFee2") ) {
			return 25;
		}
		if( strFieldName.equals("OtherMinFee2") ) {
			return 26;
		}
		if( strFieldName.equals("OtherAddFee3") ) {
			return 27;
		}
		if( strFieldName.equals("OtherMinFee3") ) {
			return 28;
		}
		if( strFieldName.equals("ExtraChargeRate1") ) {
			return 29;
		}
		if( strFieldName.equals("ExtraCharge1") ) {
			return 30;
		}
		if( strFieldName.equals("ExtraChargeRate2") ) {
			return 31;
		}
		if( strFieldName.equals("ExtraCharge2") ) {
			return 32;
		}
		if( strFieldName.equals("ExtraChargeRate3") ) {
			return 33;
		}
		if( strFieldName.equals("ExtraCharge3") ) {
			return 34;
		}
		if( strFieldName.equals("ExtraChargeRate4") ) {
			return 35;
		}
		if( strFieldName.equals("ExtraCharge4") ) {
			return 36;
		}
		if( strFieldName.equals("T1") ) {
			return 37;
		}
		if( strFieldName.equals("T2") ) {
			return 38;
		}
		if( strFieldName.equals("T3") ) {
			return 39;
		}
		if( strFieldName.equals("T4") ) {
			return 40;
		}
		if( strFieldName.equals("P1") ) {
			return 41;
		}
		if( strFieldName.equals("P2") ) {
			return 42;
		}
		if( strFieldName.equals("P3") ) {
			return 43;
		}
		if( strFieldName.equals("P4") ) {
			return 44;
		}
		if( strFieldName.equals("F1") ) {
			return 45;
		}
		if( strFieldName.equals("F2") ) {
			return 46;
		}
		if( strFieldName.equals("F3") ) {
			return 47;
		}
		if( strFieldName.equals("F4") ) {
			return 48;
		}
		if( strFieldName.equals("F5") ) {
			return 49;
		}
		if( strFieldName.equals("Operator") ) {
			return 50;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 51;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 52;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 53;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 54;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CommisionSN";
				break;
			case 1:
				strFieldName = "ManageCom";
				break;
			case 2:
				strFieldName = "AgentCom";
				break;
			case 3:
				strFieldName = "BranchType";
				break;
			case 4:
				strFieldName = "WageNo";
				break;
			case 5:
				strFieldName = "BranchType2";
				break;
			case 6:
				strFieldName = "TransMoney";
				break;
			case 7:
				strFieldName = "FirConChargeRate";
				break;
			case 8:
				strFieldName = "FirConCharge";
				break;
			case 9:
				strFieldName = "MonChargeRate";
				break;
			case 10:
				strFieldName = "MonCharge";
				break;
			case 11:
				strFieldName = "YearChargeRate";
				break;
			case 12:
				strFieldName = "YearCharge";
				break;
			case 13:
				strFieldName = "ContChargeRate";
				break;
			case 14:
				strFieldName = "ContCharge";
				break;
			case 15:
				strFieldName = "SaleBonusRate";
				break;
			case 16:
				strFieldName = "SaleBonus";
				break;
			case 17:
				strFieldName = "ConServBountyRate";
				break;
			case 18:
				strFieldName = "ConServBounty";
				break;
			case 19:
				strFieldName = "MonAddFee";
				break;
			case 20:
				strFieldName = "QuarterlyAddFee";
				break;
			case 21:
				strFieldName = "YearAddFee";
				break;
			case 22:
				strFieldName = "ConAddFee";
				break;
			case 23:
				strFieldName = "OtherAddFee1";
				break;
			case 24:
				strFieldName = "OtherMinFee1";
				break;
			case 25:
				strFieldName = "OtherAddFee2";
				break;
			case 26:
				strFieldName = "OtherMinFee2";
				break;
			case 27:
				strFieldName = "OtherAddFee3";
				break;
			case 28:
				strFieldName = "OtherMinFee3";
				break;
			case 29:
				strFieldName = "ExtraChargeRate1";
				break;
			case 30:
				strFieldName = "ExtraCharge1";
				break;
			case 31:
				strFieldName = "ExtraChargeRate2";
				break;
			case 32:
				strFieldName = "ExtraCharge2";
				break;
			case 33:
				strFieldName = "ExtraChargeRate3";
				break;
			case 34:
				strFieldName = "ExtraCharge3";
				break;
			case 35:
				strFieldName = "ExtraChargeRate4";
				break;
			case 36:
				strFieldName = "ExtraCharge4";
				break;
			case 37:
				strFieldName = "T1";
				break;
			case 38:
				strFieldName = "T2";
				break;
			case 39:
				strFieldName = "T3";
				break;
			case 40:
				strFieldName = "T4";
				break;
			case 41:
				strFieldName = "P1";
				break;
			case 42:
				strFieldName = "P2";
				break;
			case 43:
				strFieldName = "P3";
				break;
			case 44:
				strFieldName = "P4";
				break;
			case 45:
				strFieldName = "F1";
				break;
			case 46:
				strFieldName = "F2";
				break;
			case 47:
				strFieldName = "F3";
				break;
			case 48:
				strFieldName = "F4";
				break;
			case 49:
				strFieldName = "F5";
				break;
			case 50:
				strFieldName = "Operator";
				break;
			case 51:
				strFieldName = "MakeDate";
				break;
			case 52:
				strFieldName = "MakeTime";
				break;
			case 53:
				strFieldName = "ModifyDate";
				break;
			case 54:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CommisionSN") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FirConChargeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FirConCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MonChargeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MonCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("YearChargeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("YearCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ContChargeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ContCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SaleBonusRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SaleBonus") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ConServBountyRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ConServBounty") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MonAddFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("QuarterlyAddFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("YearAddFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ConAddFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherAddFee1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherMinFee1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherAddFee2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherMinFee2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherAddFee3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherMinFee3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExtraChargeRate1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExtraCharge1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExtraChargeRate2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExtraCharge2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExtraChargeRate3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExtraCharge3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExtraChargeRate4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExtraCharge4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("T2") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("T3") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("T4") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("P1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F5") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 33:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 34:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 36:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 37:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 38:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 39:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 46:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 47:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 48:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 49:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
