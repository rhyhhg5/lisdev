/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIAboriginalGenDetailPDB;

/*
 * <p>ClassName: FIAboriginalGenDetailPSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2012-04-05
 */
public class FIAboriginalGenDetailPSchema implements Schema, Cloneable
{
	// @Field
	/** 明细物理表 */
	private String SubPhysicalTable;
	/** 主键信息 */
	private String KeyUnionValue;
	/** 主业务数据流水号 */
	private String MSerialNo;
	/** 子业务数据流水号 */
	private String ASerialNo;
	/** 业务索引类型 */
	private String IndexCode;
	/** 业务索引号码 */
	private String IndexNo;
	/** 其它号码类型 */
	private String OtherNoType;
	/** 其它业务号码 */
	private String OtherNo;
	/** 通知书号码 */
	private String GetNoticeNo;
	/** 暂收号 */
	private String TempFeeNo;
	/** 暂收费类型 */
	private String TempFeeType;
	/** 收付费方式 */
	private String PayMode;
	/** 案件号 */
	private String CaseNo;
	/** 工单号 */
	private String EdorAcceptNo;
	/** 批单号 */
	private String EndorsementNo;
	/** 交费收据号 */
	private String ReceiptNo;
	/** 客户号码 */
	private String CustomerNo;
	/** 供应商号码 */
	private String SupplierNo;
	/** 帐户类型 */
	private String AccType;
	/** 用途或来源 */
	private String DestSource;
	/** 备注号 */
	private String BakNo;
	/** 保单类型 */
	private String PolType;
	/** 团体保单号 */
	private String GrpContNo;
	/** 团体保单险种号码 */
	private String GrpPolNo;
	/** 个人保单号 */
	private String ContNo;
	/** 保单险种号码 */
	private String PolNo;
	/** 险种类别 */
	private String RiskPeriod;
	/** 险种分类 */
	private String RiskType;
	/** 险种编码 */
	private String RiskCode;
	/** 责任编码 */
	private String DutyCode;
	/** 给付责任类型 */
	private String GetDutyKind;
	/** 给付责任编码 */
	private String GetDutyCode;
	/** 交费计划编码 */
	private String PayPlanCode;
	/** 交费间隔 */
	private int PayIntv;
	/** 第几次交费 */
	private int PayCount;
	/** 交费年次 */
	private int PayYears;
	/** 险种生效日期 */
	private Date CValiDate;
	/** 账户余额 */
	private double AccBala;
	/** 费用类型 */
	private String FeeType;
	/** 明细费用类型 */
	private String FeeFinaType;
	/** 费用金额 */
	private double SumActuMoney;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐户名 */
	private String AccName;
	/** 银行帐号 */
	private String BankAccNo;
	/** 销售渠道 */
	private String SaleChnl;
	/** 渠道明细 */
	private String SaleChnlDetail;
	/** 管理机构 */
	private String ManageCom;
	/** 处理机构 */
	private String ExecuteCom;
	/** 成本中心 */
	private String CostCenter;
	/** 中介机构类别 */
	private String ACType;
	/** 代理机构 */
	private String AgentCom;
	/** 代理机构内部分类 */
	private String AgentType;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人组别 */
	private String AgentGroup;
	/** 保险年龄年期标志 */
	private String InsuYearFlag;
	/** 保险年龄年期 */
	private int InsuYear;
	/** 保险年期 */
	private int Years;
	/** 保单年度 */
	private int PolYear;
	/** 核算保险年期-计费首年 */
	private int AccYears;
	/** 市场类型 */
	private String MarketType;
	/** 红利年度 */
	private String BonusYear;
	/** 续保收费标记 */
	private String PayTypeFlag;
	/** 原交至日期 */
	private Date LastPayToDate;
	/** 现交至日期 */
	private Date CurPayToDate;
	/** 展业类型 */
	private String BranchType;
	/** 展业渠道 */
	private String BranchType2;
	/** 奖励 */
	private double Award;
	/** 津贴 */
	private double Allowance;
	/** 手续费比例 */
	private double ChargeRate;
	/** 手续费状态 */
	private String ChargeState;
	/** 交易类别 */
	private String TransType;
	/** 手续费类型 */
	private String ChargeType;
	/** 到帐日期 */
	private Date EnterAccDate;
	/** 确认日期 */
	private Date ConfDate;
	/** 账务日期 */
	private Date AccountDate;
	/** 币别 */
	private String Currency;
	/** 描述 */
	private String Remark;
	/** 操作员 */
	private String Operator;
	/** 提数日期 */
	private Date MakeDate;
	/** 提数时间 */
	private String MakeTime;
	/** 校检标记 */
	private String CheckFlag;
	/** 执行事件号 */
	private String EventNo;
	/** 状态 */
	private String State;

	public static final int FIELDNUM = 83;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIAboriginalGenDetailPSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "SubPhysicalTable";
		pk[1] = "KeyUnionValue";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIAboriginalGenDetailPSchema cloned = (FIAboriginalGenDetailPSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSubPhysicalTable()
	{
		return SubPhysicalTable;
	}
	public void setSubPhysicalTable(String aSubPhysicalTable)
	{
		SubPhysicalTable = aSubPhysicalTable;
	}
	public String getKeyUnionValue()
	{
		return KeyUnionValue;
	}
	public void setKeyUnionValue(String aKeyUnionValue)
	{
		KeyUnionValue = aKeyUnionValue;
	}
	public String getMSerialNo()
	{
		return MSerialNo;
	}
	public void setMSerialNo(String aMSerialNo)
	{
		MSerialNo = aMSerialNo;
	}
	public String getASerialNo()
	{
		return ASerialNo;
	}
	public void setASerialNo(String aASerialNo)
	{
		ASerialNo = aASerialNo;
	}
	public String getIndexCode()
	{
		return IndexCode;
	}
	public void setIndexCode(String aIndexCode)
	{
		IndexCode = aIndexCode;
	}
	public String getIndexNo()
	{
		return IndexNo;
	}
	public void setIndexNo(String aIndexNo)
	{
		IndexNo = aIndexNo;
	}
	public String getOtherNoType()
	{
		return OtherNoType;
	}
	public void setOtherNoType(String aOtherNoType)
	{
		OtherNoType = aOtherNoType;
	}
	public String getOtherNo()
	{
		return OtherNo;
	}
	public void setOtherNo(String aOtherNo)
	{
		OtherNo = aOtherNo;
	}
	public String getGetNoticeNo()
	{
		return GetNoticeNo;
	}
	public void setGetNoticeNo(String aGetNoticeNo)
	{
		GetNoticeNo = aGetNoticeNo;
	}
	public String getTempFeeNo()
	{
		return TempFeeNo;
	}
	public void setTempFeeNo(String aTempFeeNo)
	{
		TempFeeNo = aTempFeeNo;
	}
	public String getTempFeeType()
	{
		return TempFeeType;
	}
	public void setTempFeeType(String aTempFeeType)
	{
		TempFeeType = aTempFeeType;
	}
	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getEdorAcceptNo()
	{
		return EdorAcceptNo;
	}
	public void setEdorAcceptNo(String aEdorAcceptNo)
	{
		EdorAcceptNo = aEdorAcceptNo;
	}
	public String getEndorsementNo()
	{
		return EndorsementNo;
	}
	public void setEndorsementNo(String aEndorsementNo)
	{
		EndorsementNo = aEndorsementNo;
	}
	public String getReceiptNo()
	{
		return ReceiptNo;
	}
	public void setReceiptNo(String aReceiptNo)
	{
		ReceiptNo = aReceiptNo;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getSupplierNo()
	{
		return SupplierNo;
	}
	public void setSupplierNo(String aSupplierNo)
	{
		SupplierNo = aSupplierNo;
	}
	public String getAccType()
	{
		return AccType;
	}
	public void setAccType(String aAccType)
	{
		AccType = aAccType;
	}
	public String getDestSource()
	{
		return DestSource;
	}
	public void setDestSource(String aDestSource)
	{
		DestSource = aDestSource;
	}
	public String getBakNo()
	{
		return BakNo;
	}
	public void setBakNo(String aBakNo)
	{
		BakNo = aBakNo;
	}
	public String getPolType()
	{
		return PolType;
	}
	public void setPolType(String aPolType)
	{
		PolType = aPolType;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpPolNo()
	{
		return GrpPolNo;
	}
	public void setGrpPolNo(String aGrpPolNo)
	{
		GrpPolNo = aGrpPolNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getRiskPeriod()
	{
		return RiskPeriod;
	}
	public void setRiskPeriod(String aRiskPeriod)
	{
		RiskPeriod = aRiskPeriod;
	}
	public String getRiskType()
	{
		return RiskType;
	}
	public void setRiskType(String aRiskType)
	{
		RiskType = aRiskType;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getDutyCode()
	{
		return DutyCode;
	}
	public void setDutyCode(String aDutyCode)
	{
		DutyCode = aDutyCode;
	}
	public String getGetDutyKind()
	{
		return GetDutyKind;
	}
	public void setGetDutyKind(String aGetDutyKind)
	{
		GetDutyKind = aGetDutyKind;
	}
	public String getGetDutyCode()
	{
		return GetDutyCode;
	}
	public void setGetDutyCode(String aGetDutyCode)
	{
		GetDutyCode = aGetDutyCode;
	}
	public String getPayPlanCode()
	{
		return PayPlanCode;
	}
	public void setPayPlanCode(String aPayPlanCode)
	{
		PayPlanCode = aPayPlanCode;
	}
	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public int getPayCount()
	{
		return PayCount;
	}
	public void setPayCount(int aPayCount)
	{
		PayCount = aPayCount;
	}
	public void setPayCount(String aPayCount)
	{
		if (aPayCount != null && !aPayCount.equals(""))
		{
			Integer tInteger = new Integer(aPayCount);
			int i = tInteger.intValue();
			PayCount = i;
		}
	}

	public int getPayYears()
	{
		return PayYears;
	}
	public void setPayYears(int aPayYears)
	{
		PayYears = aPayYears;
	}
	public void setPayYears(String aPayYears)
	{
		if (aPayYears != null && !aPayYears.equals(""))
		{
			Integer tInteger = new Integer(aPayYears);
			int i = tInteger.intValue();
			PayYears = i;
		}
	}

	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public double getAccBala()
	{
		return AccBala;
	}
	public void setAccBala(double aAccBala)
	{
		AccBala = Arith.round(aAccBala,2);
	}
	public void setAccBala(String aAccBala)
	{
		if (aAccBala != null && !aAccBala.equals(""))
		{
			Double tDouble = new Double(aAccBala);
			double d = tDouble.doubleValue();
                AccBala = Arith.round(d,2);
		}
	}

	public String getFeeType()
	{
		return FeeType;
	}
	public void setFeeType(String aFeeType)
	{
		FeeType = aFeeType;
	}
	public String getFeeFinaType()
	{
		return FeeFinaType;
	}
	public void setFeeFinaType(String aFeeFinaType)
	{
		FeeFinaType = aFeeFinaType;
	}
	public double getSumActuMoney()
	{
		return SumActuMoney;
	}
	public void setSumActuMoney(double aSumActuMoney)
	{
		SumActuMoney = Arith.round(aSumActuMoney,2);
	}
	public void setSumActuMoney(String aSumActuMoney)
	{
		if (aSumActuMoney != null && !aSumActuMoney.equals(""))
		{
			Double tDouble = new Double(aSumActuMoney);
			double d = tDouble.doubleValue();
                SumActuMoney = Arith.round(d,2);
		}
	}

	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getSaleChnlDetail()
	{
		return SaleChnlDetail;
	}
	public void setSaleChnlDetail(String aSaleChnlDetail)
	{
		SaleChnlDetail = aSaleChnlDetail;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getExecuteCom()
	{
		return ExecuteCom;
	}
	public void setExecuteCom(String aExecuteCom)
	{
		ExecuteCom = aExecuteCom;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getACType()
	{
		return ACType;
	}
	public void setACType(String aACType)
	{
		ACType = aACType;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentType()
	{
		return AgentType;
	}
	public void setAgentType(String aAgentType)
	{
		AgentType = aAgentType;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getInsuYearFlag()
	{
		return InsuYearFlag;
	}
	public void setInsuYearFlag(String aInsuYearFlag)
	{
		InsuYearFlag = aInsuYearFlag;
	}
	public int getInsuYear()
	{
		return InsuYear;
	}
	public void setInsuYear(int aInsuYear)
	{
		InsuYear = aInsuYear;
	}
	public void setInsuYear(String aInsuYear)
	{
		if (aInsuYear != null && !aInsuYear.equals(""))
		{
			Integer tInteger = new Integer(aInsuYear);
			int i = tInteger.intValue();
			InsuYear = i;
		}
	}

	public int getYears()
	{
		return Years;
	}
	public void setYears(int aYears)
	{
		Years = aYears;
	}
	public void setYears(String aYears)
	{
		if (aYears != null && !aYears.equals(""))
		{
			Integer tInteger = new Integer(aYears);
			int i = tInteger.intValue();
			Years = i;
		}
	}

	public int getPolYear()
	{
		return PolYear;
	}
	public void setPolYear(int aPolYear)
	{
		PolYear = aPolYear;
	}
	public void setPolYear(String aPolYear)
	{
		if (aPolYear != null && !aPolYear.equals(""))
		{
			Integer tInteger = new Integer(aPolYear);
			int i = tInteger.intValue();
			PolYear = i;
		}
	}

	public int getAccYears()
	{
		return AccYears;
	}
	public void setAccYears(int aAccYears)
	{
		AccYears = aAccYears;
	}
	public void setAccYears(String aAccYears)
	{
		if (aAccYears != null && !aAccYears.equals(""))
		{
			Integer tInteger = new Integer(aAccYears);
			int i = tInteger.intValue();
			AccYears = i;
		}
	}

	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getBonusYear()
	{
		return BonusYear;
	}
	public void setBonusYear(String aBonusYear)
	{
		BonusYear = aBonusYear;
	}
	public String getPayTypeFlag()
	{
		return PayTypeFlag;
	}
	public void setPayTypeFlag(String aPayTypeFlag)
	{
		PayTypeFlag = aPayTypeFlag;
	}
	public String getLastPayToDate()
	{
		if( LastPayToDate != null )
			return fDate.getString(LastPayToDate);
		else
			return null;
	}
	public void setLastPayToDate(Date aLastPayToDate)
	{
		LastPayToDate = aLastPayToDate;
	}
	public void setLastPayToDate(String aLastPayToDate)
	{
		if (aLastPayToDate != null && !aLastPayToDate.equals("") )
		{
			LastPayToDate = fDate.getDate( aLastPayToDate );
		}
		else
			LastPayToDate = null;
	}

	public String getCurPayToDate()
	{
		if( CurPayToDate != null )
			return fDate.getString(CurPayToDate);
		else
			return null;
	}
	public void setCurPayToDate(Date aCurPayToDate)
	{
		CurPayToDate = aCurPayToDate;
	}
	public void setCurPayToDate(String aCurPayToDate)
	{
		if (aCurPayToDate != null && !aCurPayToDate.equals("") )
		{
			CurPayToDate = fDate.getDate( aCurPayToDate );
		}
		else
			CurPayToDate = null;
	}

	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public double getAward()
	{
		return Award;
	}
	public void setAward(double aAward)
	{
		Award = Arith.round(aAward,2);
	}
	public void setAward(String aAward)
	{
		if (aAward != null && !aAward.equals(""))
		{
			Double tDouble = new Double(aAward);
			double d = tDouble.doubleValue();
                Award = Arith.round(d,2);
		}
	}

	public double getAllowance()
	{
		return Allowance;
	}
	public void setAllowance(double aAllowance)
	{
		Allowance = Arith.round(aAllowance,2);
	}
	public void setAllowance(String aAllowance)
	{
		if (aAllowance != null && !aAllowance.equals(""))
		{
			Double tDouble = new Double(aAllowance);
			double d = tDouble.doubleValue();
                Allowance = Arith.round(d,2);
		}
	}

	public double getChargeRate()
	{
		return ChargeRate;
	}
	public void setChargeRate(double aChargeRate)
	{
		ChargeRate = Arith.round(aChargeRate,6);
	}
	public void setChargeRate(String aChargeRate)
	{
		if (aChargeRate != null && !aChargeRate.equals(""))
		{
			Double tDouble = new Double(aChargeRate);
			double d = tDouble.doubleValue();
                ChargeRate = Arith.round(d,6);
		}
	}

	public String getChargeState()
	{
		return ChargeState;
	}
	public void setChargeState(String aChargeState)
	{
		ChargeState = aChargeState;
	}
	public String getTransType()
	{
		return TransType;
	}
	public void setTransType(String aTransType)
	{
		TransType = aTransType;
	}
	public String getChargeType()
	{
		return ChargeType;
	}
	public void setChargeType(String aChargeType)
	{
		ChargeType = aChargeType;
	}
	public String getEnterAccDate()
	{
		if( EnterAccDate != null )
			return fDate.getString(EnterAccDate);
		else
			return null;
	}
	public void setEnterAccDate(Date aEnterAccDate)
	{
		EnterAccDate = aEnterAccDate;
	}
	public void setEnterAccDate(String aEnterAccDate)
	{
		if (aEnterAccDate != null && !aEnterAccDate.equals("") )
		{
			EnterAccDate = fDate.getDate( aEnterAccDate );
		}
		else
			EnterAccDate = null;
	}

	public String getConfDate()
	{
		if( ConfDate != null )
			return fDate.getString(ConfDate);
		else
			return null;
	}
	public void setConfDate(Date aConfDate)
	{
		ConfDate = aConfDate;
	}
	public void setConfDate(String aConfDate)
	{
		if (aConfDate != null && !aConfDate.equals("") )
		{
			ConfDate = fDate.getDate( aConfDate );
		}
		else
			ConfDate = null;
	}

	public String getAccountDate()
	{
		if( AccountDate != null )
			return fDate.getString(AccountDate);
		else
			return null;
	}
	public void setAccountDate(Date aAccountDate)
	{
		AccountDate = aAccountDate;
	}
	public void setAccountDate(String aAccountDate)
	{
		if (aAccountDate != null && !aAccountDate.equals("") )
		{
			AccountDate = fDate.getDate( aAccountDate );
		}
		else
			AccountDate = null;
	}

	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getCheckFlag()
	{
		return CheckFlag;
	}
	public void setCheckFlag(String aCheckFlag)
	{
		CheckFlag = aCheckFlag;
	}
	public String getEventNo()
	{
		return EventNo;
	}
	public void setEventNo(String aEventNo)
	{
		EventNo = aEventNo;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}

	/**
	* 使用另外一个 FIAboriginalGenDetailPSchema 对象给 Schema 赋值
	* @param: aFIAboriginalGenDetailPSchema FIAboriginalGenDetailPSchema
	**/
	public void setSchema(FIAboriginalGenDetailPSchema aFIAboriginalGenDetailPSchema)
	{
		this.SubPhysicalTable = aFIAboriginalGenDetailPSchema.getSubPhysicalTable();
		this.KeyUnionValue = aFIAboriginalGenDetailPSchema.getKeyUnionValue();
		this.MSerialNo = aFIAboriginalGenDetailPSchema.getMSerialNo();
		this.ASerialNo = aFIAboriginalGenDetailPSchema.getASerialNo();
		this.IndexCode = aFIAboriginalGenDetailPSchema.getIndexCode();
		this.IndexNo = aFIAboriginalGenDetailPSchema.getIndexNo();
		this.OtherNoType = aFIAboriginalGenDetailPSchema.getOtherNoType();
		this.OtherNo = aFIAboriginalGenDetailPSchema.getOtherNo();
		this.GetNoticeNo = aFIAboriginalGenDetailPSchema.getGetNoticeNo();
		this.TempFeeNo = aFIAboriginalGenDetailPSchema.getTempFeeNo();
		this.TempFeeType = aFIAboriginalGenDetailPSchema.getTempFeeType();
		this.PayMode = aFIAboriginalGenDetailPSchema.getPayMode();
		this.CaseNo = aFIAboriginalGenDetailPSchema.getCaseNo();
		this.EdorAcceptNo = aFIAboriginalGenDetailPSchema.getEdorAcceptNo();
		this.EndorsementNo = aFIAboriginalGenDetailPSchema.getEndorsementNo();
		this.ReceiptNo = aFIAboriginalGenDetailPSchema.getReceiptNo();
		this.CustomerNo = aFIAboriginalGenDetailPSchema.getCustomerNo();
		this.SupplierNo = aFIAboriginalGenDetailPSchema.getSupplierNo();
		this.AccType = aFIAboriginalGenDetailPSchema.getAccType();
		this.DestSource = aFIAboriginalGenDetailPSchema.getDestSource();
		this.BakNo = aFIAboriginalGenDetailPSchema.getBakNo();
		this.PolType = aFIAboriginalGenDetailPSchema.getPolType();
		this.GrpContNo = aFIAboriginalGenDetailPSchema.getGrpContNo();
		this.GrpPolNo = aFIAboriginalGenDetailPSchema.getGrpPolNo();
		this.ContNo = aFIAboriginalGenDetailPSchema.getContNo();
		this.PolNo = aFIAboriginalGenDetailPSchema.getPolNo();
		this.RiskPeriod = aFIAboriginalGenDetailPSchema.getRiskPeriod();
		this.RiskType = aFIAboriginalGenDetailPSchema.getRiskType();
		this.RiskCode = aFIAboriginalGenDetailPSchema.getRiskCode();
		this.DutyCode = aFIAboriginalGenDetailPSchema.getDutyCode();
		this.GetDutyKind = aFIAboriginalGenDetailPSchema.getGetDutyKind();
		this.GetDutyCode = aFIAboriginalGenDetailPSchema.getGetDutyCode();
		this.PayPlanCode = aFIAboriginalGenDetailPSchema.getPayPlanCode();
		this.PayIntv = aFIAboriginalGenDetailPSchema.getPayIntv();
		this.PayCount = aFIAboriginalGenDetailPSchema.getPayCount();
		this.PayYears = aFIAboriginalGenDetailPSchema.getPayYears();
		this.CValiDate = fDate.getDate( aFIAboriginalGenDetailPSchema.getCValiDate());
		this.AccBala = aFIAboriginalGenDetailPSchema.getAccBala();
		this.FeeType = aFIAboriginalGenDetailPSchema.getFeeType();
		this.FeeFinaType = aFIAboriginalGenDetailPSchema.getFeeFinaType();
		this.SumActuMoney = aFIAboriginalGenDetailPSchema.getSumActuMoney();
		this.BankCode = aFIAboriginalGenDetailPSchema.getBankCode();
		this.AccName = aFIAboriginalGenDetailPSchema.getAccName();
		this.BankAccNo = aFIAboriginalGenDetailPSchema.getBankAccNo();
		this.SaleChnl = aFIAboriginalGenDetailPSchema.getSaleChnl();
		this.SaleChnlDetail = aFIAboriginalGenDetailPSchema.getSaleChnlDetail();
		this.ManageCom = aFIAboriginalGenDetailPSchema.getManageCom();
		this.ExecuteCom = aFIAboriginalGenDetailPSchema.getExecuteCom();
		this.CostCenter = aFIAboriginalGenDetailPSchema.getCostCenter();
		this.ACType = aFIAboriginalGenDetailPSchema.getACType();
		this.AgentCom = aFIAboriginalGenDetailPSchema.getAgentCom();
		this.AgentType = aFIAboriginalGenDetailPSchema.getAgentType();
		this.AgentCode = aFIAboriginalGenDetailPSchema.getAgentCode();
		this.AgentGroup = aFIAboriginalGenDetailPSchema.getAgentGroup();
		this.InsuYearFlag = aFIAboriginalGenDetailPSchema.getInsuYearFlag();
		this.InsuYear = aFIAboriginalGenDetailPSchema.getInsuYear();
		this.Years = aFIAboriginalGenDetailPSchema.getYears();
		this.PolYear = aFIAboriginalGenDetailPSchema.getPolYear();
		this.AccYears = aFIAboriginalGenDetailPSchema.getAccYears();
		this.MarketType = aFIAboriginalGenDetailPSchema.getMarketType();
		this.BonusYear = aFIAboriginalGenDetailPSchema.getBonusYear();
		this.PayTypeFlag = aFIAboriginalGenDetailPSchema.getPayTypeFlag();
		this.LastPayToDate = fDate.getDate( aFIAboriginalGenDetailPSchema.getLastPayToDate());
		this.CurPayToDate = fDate.getDate( aFIAboriginalGenDetailPSchema.getCurPayToDate());
		this.BranchType = aFIAboriginalGenDetailPSchema.getBranchType();
		this.BranchType2 = aFIAboriginalGenDetailPSchema.getBranchType2();
		this.Award = aFIAboriginalGenDetailPSchema.getAward();
		this.Allowance = aFIAboriginalGenDetailPSchema.getAllowance();
		this.ChargeRate = aFIAboriginalGenDetailPSchema.getChargeRate();
		this.ChargeState = aFIAboriginalGenDetailPSchema.getChargeState();
		this.TransType = aFIAboriginalGenDetailPSchema.getTransType();
		this.ChargeType = aFIAboriginalGenDetailPSchema.getChargeType();
		this.EnterAccDate = fDate.getDate( aFIAboriginalGenDetailPSchema.getEnterAccDate());
		this.ConfDate = fDate.getDate( aFIAboriginalGenDetailPSchema.getConfDate());
		this.AccountDate = fDate.getDate( aFIAboriginalGenDetailPSchema.getAccountDate());
		this.Currency = aFIAboriginalGenDetailPSchema.getCurrency();
		this.Remark = aFIAboriginalGenDetailPSchema.getRemark();
		this.Operator = aFIAboriginalGenDetailPSchema.getOperator();
		this.MakeDate = fDate.getDate( aFIAboriginalGenDetailPSchema.getMakeDate());
		this.MakeTime = aFIAboriginalGenDetailPSchema.getMakeTime();
		this.CheckFlag = aFIAboriginalGenDetailPSchema.getCheckFlag();
		this.EventNo = aFIAboriginalGenDetailPSchema.getEventNo();
		this.State = aFIAboriginalGenDetailPSchema.getState();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SubPhysicalTable") == null )
				this.SubPhysicalTable = null;
			else
				this.SubPhysicalTable = rs.getString("SubPhysicalTable").trim();

			if( rs.getString("KeyUnionValue") == null )
				this.KeyUnionValue = null;
			else
				this.KeyUnionValue = rs.getString("KeyUnionValue").trim();

			if( rs.getString("MSerialNo") == null )
				this.MSerialNo = null;
			else
				this.MSerialNo = rs.getString("MSerialNo").trim();

			if( rs.getString("ASerialNo") == null )
				this.ASerialNo = null;
			else
				this.ASerialNo = rs.getString("ASerialNo").trim();

			if( rs.getString("IndexCode") == null )
				this.IndexCode = null;
			else
				this.IndexCode = rs.getString("IndexCode").trim();

			if( rs.getString("IndexNo") == null )
				this.IndexNo = null;
			else
				this.IndexNo = rs.getString("IndexNo").trim();

			if( rs.getString("OtherNoType") == null )
				this.OtherNoType = null;
			else
				this.OtherNoType = rs.getString("OtherNoType").trim();

			if( rs.getString("OtherNo") == null )
				this.OtherNo = null;
			else
				this.OtherNo = rs.getString("OtherNo").trim();

			if( rs.getString("GetNoticeNo") == null )
				this.GetNoticeNo = null;
			else
				this.GetNoticeNo = rs.getString("GetNoticeNo").trim();

			if( rs.getString("TempFeeNo") == null )
				this.TempFeeNo = null;
			else
				this.TempFeeNo = rs.getString("TempFeeNo").trim();

			if( rs.getString("TempFeeType") == null )
				this.TempFeeType = null;
			else
				this.TempFeeType = rs.getString("TempFeeType").trim();

			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("EdorAcceptNo") == null )
				this.EdorAcceptNo = null;
			else
				this.EdorAcceptNo = rs.getString("EdorAcceptNo").trim();

			if( rs.getString("EndorsementNo") == null )
				this.EndorsementNo = null;
			else
				this.EndorsementNo = rs.getString("EndorsementNo").trim();

			if( rs.getString("ReceiptNo") == null )
				this.ReceiptNo = null;
			else
				this.ReceiptNo = rs.getString("ReceiptNo").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("SupplierNo") == null )
				this.SupplierNo = null;
			else
				this.SupplierNo = rs.getString("SupplierNo").trim();

			if( rs.getString("AccType") == null )
				this.AccType = null;
			else
				this.AccType = rs.getString("AccType").trim();

			if( rs.getString("DestSource") == null )
				this.DestSource = null;
			else
				this.DestSource = rs.getString("DestSource").trim();

			if( rs.getString("BakNo") == null )
				this.BakNo = null;
			else
				this.BakNo = rs.getString("BakNo").trim();

			if( rs.getString("PolType") == null )
				this.PolType = null;
			else
				this.PolType = rs.getString("PolType").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpPolNo") == null )
				this.GrpPolNo = null;
			else
				this.GrpPolNo = rs.getString("GrpPolNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("RiskPeriod") == null )
				this.RiskPeriod = null;
			else
				this.RiskPeriod = rs.getString("RiskPeriod").trim();

			if( rs.getString("RiskType") == null )
				this.RiskType = null;
			else
				this.RiskType = rs.getString("RiskType").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("DutyCode") == null )
				this.DutyCode = null;
			else
				this.DutyCode = rs.getString("DutyCode").trim();

			if( rs.getString("GetDutyKind") == null )
				this.GetDutyKind = null;
			else
				this.GetDutyKind = rs.getString("GetDutyKind").trim();

			if( rs.getString("GetDutyCode") == null )
				this.GetDutyCode = null;
			else
				this.GetDutyCode = rs.getString("GetDutyCode").trim();

			if( rs.getString("PayPlanCode") == null )
				this.PayPlanCode = null;
			else
				this.PayPlanCode = rs.getString("PayPlanCode").trim();

			this.PayIntv = rs.getInt("PayIntv");
			this.PayCount = rs.getInt("PayCount");
			this.PayYears = rs.getInt("PayYears");
			this.CValiDate = rs.getDate("CValiDate");
			this.AccBala = rs.getDouble("AccBala");
			if( rs.getString("FeeType") == null )
				this.FeeType = null;
			else
				this.FeeType = rs.getString("FeeType").trim();

			if( rs.getString("FeeFinaType") == null )
				this.FeeFinaType = null;
			else
				this.FeeFinaType = rs.getString("FeeFinaType").trim();

			this.SumActuMoney = rs.getDouble("SumActuMoney");
			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("SaleChnlDetail") == null )
				this.SaleChnlDetail = null;
			else
				this.SaleChnlDetail = rs.getString("SaleChnlDetail").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ExecuteCom") == null )
				this.ExecuteCom = null;
			else
				this.ExecuteCom = rs.getString("ExecuteCom").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("ACType") == null )
				this.ACType = null;
			else
				this.ACType = rs.getString("ACType").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentType") == null )
				this.AgentType = null;
			else
				this.AgentType = rs.getString("AgentType").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("InsuYearFlag") == null )
				this.InsuYearFlag = null;
			else
				this.InsuYearFlag = rs.getString("InsuYearFlag").trim();

			this.InsuYear = rs.getInt("InsuYear");
			this.Years = rs.getInt("Years");
			this.PolYear = rs.getInt("PolYear");
			this.AccYears = rs.getInt("AccYears");
			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("BonusYear") == null )
				this.BonusYear = null;
			else
				this.BonusYear = rs.getString("BonusYear").trim();

			if( rs.getString("PayTypeFlag") == null )
				this.PayTypeFlag = null;
			else
				this.PayTypeFlag = rs.getString("PayTypeFlag").trim();

			this.LastPayToDate = rs.getDate("LastPayToDate");
			this.CurPayToDate = rs.getDate("CurPayToDate");
			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			this.Award = rs.getDouble("Award");
			this.Allowance = rs.getDouble("Allowance");
			this.ChargeRate = rs.getDouble("ChargeRate");
			if( rs.getString("ChargeState") == null )
				this.ChargeState = null;
			else
				this.ChargeState = rs.getString("ChargeState").trim();

			if( rs.getString("TransType") == null )
				this.TransType = null;
			else
				this.TransType = rs.getString("TransType").trim();

			if( rs.getString("ChargeType") == null )
				this.ChargeType = null;
			else
				this.ChargeType = rs.getString("ChargeType").trim();

			this.EnterAccDate = rs.getDate("EnterAccDate");
			this.ConfDate = rs.getDate("ConfDate");
			this.AccountDate = rs.getDate("AccountDate");
			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			if( rs.getString("CheckFlag") == null )
				this.CheckFlag = null;
			else
				this.CheckFlag = rs.getString("CheckFlag").trim();

			if( rs.getString("EventNo") == null )
				this.EventNo = null;
			else
				this.EventNo = rs.getString("EventNo").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIAboriginalGenDetailP表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIAboriginalGenDetailPSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIAboriginalGenDetailPSchema getSchema()
	{
		FIAboriginalGenDetailPSchema aFIAboriginalGenDetailPSchema = new FIAboriginalGenDetailPSchema();
		aFIAboriginalGenDetailPSchema.setSchema(this);
		return aFIAboriginalGenDetailPSchema;
	}

	public FIAboriginalGenDetailPDB getDB()
	{
		FIAboriginalGenDetailPDB aDBOper = new FIAboriginalGenDetailPDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIAboriginalGenDetailP描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SubPhysicalTable)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(KeyUnionValue)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ASerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetNoticeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempFeeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorAcceptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EndorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SupplierNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DestSource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BakNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskPeriod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayYears));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AccBala));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeFinaType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumActuMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnlDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExecuteCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ACType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AccYears));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BonusYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayTypeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LastPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CurPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Award));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Allowance));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ChargeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChargeState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChargeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EnterAccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccountDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EventNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIAboriginalGenDetailP>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SubPhysicalTable = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			KeyUnionValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			MSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ASerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IndexNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			TempFeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			EndorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			SupplierNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			DestSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			BakNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			RiskPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).intValue();
			PayCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).intValue();
			PayYears= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).intValue();
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,SysConst.PACKAGESPILTER));
			AccBala = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,38,SysConst.PACKAGESPILTER))).doubleValue();
			FeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			FeeFinaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			SumActuMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).doubleValue();
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			SaleChnlDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			ExecuteCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			ACType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			InsuYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,56,SysConst.PACKAGESPILTER))).intValue();
			Years= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,57,SysConst.PACKAGESPILTER))).intValue();
			PolYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,58,SysConst.PACKAGESPILTER))).intValue();
			AccYears= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,59,SysConst.PACKAGESPILTER))).intValue();
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			BonusYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			PayTypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			LastPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63,SysConst.PACKAGESPILTER));
			CurPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64,SysConst.PACKAGESPILTER));
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
			Award = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,67,SysConst.PACKAGESPILTER))).doubleValue();
			Allowance = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,68,SysConst.PACKAGESPILTER))).doubleValue();
			ChargeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,69,SysConst.PACKAGESPILTER))).doubleValue();
			ChargeState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70, SysConst.PACKAGESPILTER );
			TransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
			ChargeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
			EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73,SysConst.PACKAGESPILTER));
			ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74,SysConst.PACKAGESPILTER));
			AccountDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75,SysConst.PACKAGESPILTER));
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 78, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 79,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 80, SysConst.PACKAGESPILTER );
			CheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 81, SysConst.PACKAGESPILTER );
			EventNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 82, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 83, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIAboriginalGenDetailPSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SubPhysicalTable"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubPhysicalTable));
		}
		if (FCode.equals("KeyUnionValue"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(KeyUnionValue));
		}
		if (FCode.equals("MSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MSerialNo));
		}
		if (FCode.equals("ASerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ASerialNo));
		}
		if (FCode.equals("IndexCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
		}
		if (FCode.equals("IndexNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexNo));
		}
		if (FCode.equals("OtherNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
		}
		if (FCode.equals("OtherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
		}
		if (FCode.equals("GetNoticeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
		}
		if (FCode.equals("TempFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
		}
		if (FCode.equals("TempFeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeType));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("EdorAcceptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
		}
		if (FCode.equals("EndorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndorsementNo));
		}
		if (FCode.equals("ReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("SupplierNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SupplierNo));
		}
		if (FCode.equals("AccType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccType));
		}
		if (FCode.equals("DestSource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DestSource));
		}
		if (FCode.equals("BakNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BakNo));
		}
		if (FCode.equals("PolType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("RiskPeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskPeriod));
		}
		if (FCode.equals("RiskType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("DutyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
		}
		if (FCode.equals("GetDutyKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
		}
		if (FCode.equals("GetDutyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
		}
		if (FCode.equals("PayPlanCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("PayCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
		}
		if (FCode.equals("PayYears"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayYears));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("AccBala"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccBala));
		}
		if (FCode.equals("FeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
		}
		if (FCode.equals("FeeFinaType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeFinaType));
		}
		if (FCode.equals("SumActuMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumActuMoney));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("SaleChnlDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnlDetail));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ExecuteCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("ACType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ACType));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("InsuYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
		}
		if (FCode.equals("InsuYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
		}
		if (FCode.equals("Years"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
		}
		if (FCode.equals("PolYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolYear));
		}
		if (FCode.equals("AccYears"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccYears));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("BonusYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BonusYear));
		}
		if (FCode.equals("PayTypeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayTypeFlag));
		}
		if (FCode.equals("LastPayToDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
		}
		if (FCode.equals("CurPayToDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("Award"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Award));
		}
		if (FCode.equals("Allowance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Allowance));
		}
		if (FCode.equals("ChargeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeRate));
		}
		if (FCode.equals("ChargeState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeState));
		}
		if (FCode.equals("TransType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
		}
		if (FCode.equals("ChargeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeType));
		}
		if (FCode.equals("EnterAccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
		}
		if (FCode.equals("ConfDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
		}
		if (FCode.equals("AccountDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccountDate()));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("CheckFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
		}
		if (FCode.equals("EventNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EventNo));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SubPhysicalTable);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(KeyUnionValue);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(MSerialNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ASerialNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(IndexCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IndexNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(OtherNoType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(OtherNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(TempFeeType);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(EndorsementNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(SupplierNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(AccType);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(DestSource);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(BakNo);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(PolType);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(RiskPeriod);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(RiskType);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(DutyCode);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
				break;
			case 33:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 34:
				strFieldValue = String.valueOf(PayCount);
				break;
			case 35:
				strFieldValue = String.valueOf(PayYears);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 37:
				strFieldValue = String.valueOf(AccBala);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(FeeType);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(FeeFinaType);
				break;
			case 40:
				strFieldValue = String.valueOf(SumActuMoney);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(SaleChnlDetail);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(ExecuteCom);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(ACType);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(AgentType);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
				break;
			case 55:
				strFieldValue = String.valueOf(InsuYear);
				break;
			case 56:
				strFieldValue = String.valueOf(Years);
				break;
			case 57:
				strFieldValue = String.valueOf(PolYear);
				break;
			case 58:
				strFieldValue = String.valueOf(AccYears);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(BonusYear);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(PayTypeFlag);
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 66:
				strFieldValue = String.valueOf(Award);
				break;
			case 67:
				strFieldValue = String.valueOf(Allowance);
				break;
			case 68:
				strFieldValue = String.valueOf(ChargeRate);
				break;
			case 69:
				strFieldValue = StrTool.GBKToUnicode(ChargeState);
				break;
			case 70:
				strFieldValue = StrTool.GBKToUnicode(TransType);
				break;
			case 71:
				strFieldValue = StrTool.GBKToUnicode(ChargeType);
				break;
			case 72:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnterAccDate()));
				break;
			case 73:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getConfDate()));
				break;
			case 74:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccountDate()));
				break;
			case 75:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			case 76:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 77:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 78:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 79:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 80:
				strFieldValue = StrTool.GBKToUnicode(CheckFlag);
				break;
			case 81:
				strFieldValue = StrTool.GBKToUnicode(EventNo);
				break;
			case 82:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SubPhysicalTable"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubPhysicalTable = FValue.trim();
			}
			else
				SubPhysicalTable = null;
		}
		if (FCode.equalsIgnoreCase("KeyUnionValue"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				KeyUnionValue = FValue.trim();
			}
			else
				KeyUnionValue = null;
		}
		if (FCode.equalsIgnoreCase("MSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MSerialNo = FValue.trim();
			}
			else
				MSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("ASerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ASerialNo = FValue.trim();
			}
			else
				ASerialNo = null;
		}
		if (FCode.equalsIgnoreCase("IndexCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCode = FValue.trim();
			}
			else
				IndexCode = null;
		}
		if (FCode.equalsIgnoreCase("IndexNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexNo = FValue.trim();
			}
			else
				IndexNo = null;
		}
		if (FCode.equalsIgnoreCase("OtherNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNoType = FValue.trim();
			}
			else
				OtherNoType = null;
		}
		if (FCode.equalsIgnoreCase("OtherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNo = FValue.trim();
			}
			else
				OtherNo = null;
		}
		if (FCode.equalsIgnoreCase("GetNoticeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetNoticeNo = FValue.trim();
			}
			else
				GetNoticeNo = null;
		}
		if (FCode.equalsIgnoreCase("TempFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempFeeNo = FValue.trim();
			}
			else
				TempFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("TempFeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempFeeType = FValue.trim();
			}
			else
				TempFeeType = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorAcceptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorAcceptNo = FValue.trim();
			}
			else
				EdorAcceptNo = null;
		}
		if (FCode.equalsIgnoreCase("EndorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndorsementNo = FValue.trim();
			}
			else
				EndorsementNo = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNo = FValue.trim();
			}
			else
				ReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("SupplierNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SupplierNo = FValue.trim();
			}
			else
				SupplierNo = null;
		}
		if (FCode.equalsIgnoreCase("AccType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccType = FValue.trim();
			}
			else
				AccType = null;
		}
		if (FCode.equalsIgnoreCase("DestSource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DestSource = FValue.trim();
			}
			else
				DestSource = null;
		}
		if (FCode.equalsIgnoreCase("BakNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BakNo = FValue.trim();
			}
			else
				BakNo = null;
		}
		if (FCode.equalsIgnoreCase("PolType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolType = FValue.trim();
			}
			else
				PolType = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpPolNo = FValue.trim();
			}
			else
				GrpPolNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskPeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskPeriod = FValue.trim();
			}
			else
				RiskPeriod = null;
		}
		if (FCode.equalsIgnoreCase("RiskType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType = FValue.trim();
			}
			else
				RiskType = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("DutyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DutyCode = FValue.trim();
			}
			else
				DutyCode = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyKind = FValue.trim();
			}
			else
				GetDutyKind = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyCode = FValue.trim();
			}
			else
				GetDutyCode = null;
		}
		if (FCode.equalsIgnoreCase("PayPlanCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayPlanCode = FValue.trim();
			}
			else
				PayPlanCode = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayYears"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayYears = i;
			}
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("AccBala"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AccBala = d;
			}
		}
		if (FCode.equalsIgnoreCase("FeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeType = FValue.trim();
			}
			else
				FeeType = null;
		}
		if (FCode.equalsIgnoreCase("FeeFinaType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeFinaType = FValue.trim();
			}
			else
				FeeFinaType = null;
		}
		if (FCode.equalsIgnoreCase("SumActuMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumActuMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnlDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnlDetail = FValue.trim();
			}
			else
				SaleChnlDetail = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ExecuteCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExecuteCom = FValue.trim();
			}
			else
				ExecuteCom = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("ACType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ACType = FValue.trim();
			}
			else
				ACType = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentType = FValue.trim();
			}
			else
				AgentType = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("InsuYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuYearFlag = FValue.trim();
			}
			else
				InsuYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("InsuYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("Years"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Years = i;
			}
		}
		if (FCode.equalsIgnoreCase("PolYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("AccYears"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AccYears = i;
			}
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("BonusYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BonusYear = FValue.trim();
			}
			else
				BonusYear = null;
		}
		if (FCode.equalsIgnoreCase("PayTypeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayTypeFlag = FValue.trim();
			}
			else
				PayTypeFlag = null;
		}
		if (FCode.equalsIgnoreCase("LastPayToDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LastPayToDate = fDate.getDate( FValue );
			}
			else
				LastPayToDate = null;
		}
		if (FCode.equalsIgnoreCase("CurPayToDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CurPayToDate = fDate.getDate( FValue );
			}
			else
				CurPayToDate = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("Award"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Award = d;
			}
		}
		if (FCode.equalsIgnoreCase("Allowance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Allowance = d;
			}
		}
		if (FCode.equalsIgnoreCase("ChargeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ChargeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ChargeState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChargeState = FValue.trim();
			}
			else
				ChargeState = null;
		}
		if (FCode.equalsIgnoreCase("TransType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransType = FValue.trim();
			}
			else
				TransType = null;
		}
		if (FCode.equalsIgnoreCase("ChargeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChargeType = FValue.trim();
			}
			else
				ChargeType = null;
		}
		if (FCode.equalsIgnoreCase("EnterAccDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EnterAccDate = fDate.getDate( FValue );
			}
			else
				EnterAccDate = null;
		}
		if (FCode.equalsIgnoreCase("ConfDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ConfDate = fDate.getDate( FValue );
			}
			else
				ConfDate = null;
		}
		if (FCode.equalsIgnoreCase("AccountDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccountDate = fDate.getDate( FValue );
			}
			else
				AccountDate = null;
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("CheckFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckFlag = FValue.trim();
			}
			else
				CheckFlag = null;
		}
		if (FCode.equalsIgnoreCase("EventNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EventNo = FValue.trim();
			}
			else
				EventNo = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIAboriginalGenDetailPSchema other = (FIAboriginalGenDetailPSchema)otherObject;
		return
			(SubPhysicalTable == null ? other.getSubPhysicalTable() == null : SubPhysicalTable.equals(other.getSubPhysicalTable()))
			&& (KeyUnionValue == null ? other.getKeyUnionValue() == null : KeyUnionValue.equals(other.getKeyUnionValue()))
			&& (MSerialNo == null ? other.getMSerialNo() == null : MSerialNo.equals(other.getMSerialNo()))
			&& (ASerialNo == null ? other.getASerialNo() == null : ASerialNo.equals(other.getASerialNo()))
			&& (IndexCode == null ? other.getIndexCode() == null : IndexCode.equals(other.getIndexCode()))
			&& (IndexNo == null ? other.getIndexNo() == null : IndexNo.equals(other.getIndexNo()))
			&& (OtherNoType == null ? other.getOtherNoType() == null : OtherNoType.equals(other.getOtherNoType()))
			&& (OtherNo == null ? other.getOtherNo() == null : OtherNo.equals(other.getOtherNo()))
			&& (GetNoticeNo == null ? other.getGetNoticeNo() == null : GetNoticeNo.equals(other.getGetNoticeNo()))
			&& (TempFeeNo == null ? other.getTempFeeNo() == null : TempFeeNo.equals(other.getTempFeeNo()))
			&& (TempFeeType == null ? other.getTempFeeType() == null : TempFeeType.equals(other.getTempFeeType()))
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (EdorAcceptNo == null ? other.getEdorAcceptNo() == null : EdorAcceptNo.equals(other.getEdorAcceptNo()))
			&& (EndorsementNo == null ? other.getEndorsementNo() == null : EndorsementNo.equals(other.getEndorsementNo()))
			&& (ReceiptNo == null ? other.getReceiptNo() == null : ReceiptNo.equals(other.getReceiptNo()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (SupplierNo == null ? other.getSupplierNo() == null : SupplierNo.equals(other.getSupplierNo()))
			&& (AccType == null ? other.getAccType() == null : AccType.equals(other.getAccType()))
			&& (DestSource == null ? other.getDestSource() == null : DestSource.equals(other.getDestSource()))
			&& (BakNo == null ? other.getBakNo() == null : BakNo.equals(other.getBakNo()))
			&& (PolType == null ? other.getPolType() == null : PolType.equals(other.getPolType()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (GrpPolNo == null ? other.getGrpPolNo() == null : GrpPolNo.equals(other.getGrpPolNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (RiskPeriod == null ? other.getRiskPeriod() == null : RiskPeriod.equals(other.getRiskPeriod()))
			&& (RiskType == null ? other.getRiskType() == null : RiskType.equals(other.getRiskType()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (DutyCode == null ? other.getDutyCode() == null : DutyCode.equals(other.getDutyCode()))
			&& (GetDutyKind == null ? other.getGetDutyKind() == null : GetDutyKind.equals(other.getGetDutyKind()))
			&& (GetDutyCode == null ? other.getGetDutyCode() == null : GetDutyCode.equals(other.getGetDutyCode()))
			&& (PayPlanCode == null ? other.getPayPlanCode() == null : PayPlanCode.equals(other.getPayPlanCode()))
			&& PayIntv == other.getPayIntv()
			&& PayCount == other.getPayCount()
			&& PayYears == other.getPayYears()
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& AccBala == other.getAccBala()
			&& (FeeType == null ? other.getFeeType() == null : FeeType.equals(other.getFeeType()))
			&& (FeeFinaType == null ? other.getFeeFinaType() == null : FeeFinaType.equals(other.getFeeFinaType()))
			&& SumActuMoney == other.getSumActuMoney()
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (SaleChnlDetail == null ? other.getSaleChnlDetail() == null : SaleChnlDetail.equals(other.getSaleChnlDetail()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (ExecuteCom == null ? other.getExecuteCom() == null : ExecuteCom.equals(other.getExecuteCom()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()))
			&& (ACType == null ? other.getACType() == null : ACType.equals(other.getACType()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentType == null ? other.getAgentType() == null : AgentType.equals(other.getAgentType()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (InsuYearFlag == null ? other.getInsuYearFlag() == null : InsuYearFlag.equals(other.getInsuYearFlag()))
			&& InsuYear == other.getInsuYear()
			&& Years == other.getYears()
			&& PolYear == other.getPolYear()
			&& AccYears == other.getAccYears()
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (BonusYear == null ? other.getBonusYear() == null : BonusYear.equals(other.getBonusYear()))
			&& (PayTypeFlag == null ? other.getPayTypeFlag() == null : PayTypeFlag.equals(other.getPayTypeFlag()))
			&& (LastPayToDate == null ? other.getLastPayToDate() == null : fDate.getString(LastPayToDate).equals(other.getLastPayToDate()))
			&& (CurPayToDate == null ? other.getCurPayToDate() == null : fDate.getString(CurPayToDate).equals(other.getCurPayToDate()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& Award == other.getAward()
			&& Allowance == other.getAllowance()
			&& ChargeRate == other.getChargeRate()
			&& (ChargeState == null ? other.getChargeState() == null : ChargeState.equals(other.getChargeState()))
			&& (TransType == null ? other.getTransType() == null : TransType.equals(other.getTransType()))
			&& (ChargeType == null ? other.getChargeType() == null : ChargeType.equals(other.getChargeType()))
			&& (EnterAccDate == null ? other.getEnterAccDate() == null : fDate.getString(EnterAccDate).equals(other.getEnterAccDate()))
			&& (ConfDate == null ? other.getConfDate() == null : fDate.getString(ConfDate).equals(other.getConfDate()))
			&& (AccountDate == null ? other.getAccountDate() == null : fDate.getString(AccountDate).equals(other.getAccountDate()))
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (CheckFlag == null ? other.getCheckFlag() == null : CheckFlag.equals(other.getCheckFlag()))
			&& (EventNo == null ? other.getEventNo() == null : EventNo.equals(other.getEventNo()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SubPhysicalTable") ) {
			return 0;
		}
		if( strFieldName.equals("KeyUnionValue") ) {
			return 1;
		}
		if( strFieldName.equals("MSerialNo") ) {
			return 2;
		}
		if( strFieldName.equals("ASerialNo") ) {
			return 3;
		}
		if( strFieldName.equals("IndexCode") ) {
			return 4;
		}
		if( strFieldName.equals("IndexNo") ) {
			return 5;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return 6;
		}
		if( strFieldName.equals("OtherNo") ) {
			return 7;
		}
		if( strFieldName.equals("GetNoticeNo") ) {
			return 8;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return 9;
		}
		if( strFieldName.equals("TempFeeType") ) {
			return 10;
		}
		if( strFieldName.equals("PayMode") ) {
			return 11;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 12;
		}
		if( strFieldName.equals("EdorAcceptNo") ) {
			return 13;
		}
		if( strFieldName.equals("EndorsementNo") ) {
			return 14;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return 15;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 16;
		}
		if( strFieldName.equals("SupplierNo") ) {
			return 17;
		}
		if( strFieldName.equals("AccType") ) {
			return 18;
		}
		if( strFieldName.equals("DestSource") ) {
			return 19;
		}
		if( strFieldName.equals("BakNo") ) {
			return 20;
		}
		if( strFieldName.equals("PolType") ) {
			return 21;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 22;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return 23;
		}
		if( strFieldName.equals("ContNo") ) {
			return 24;
		}
		if( strFieldName.equals("PolNo") ) {
			return 25;
		}
		if( strFieldName.equals("RiskPeriod") ) {
			return 26;
		}
		if( strFieldName.equals("RiskType") ) {
			return 27;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 28;
		}
		if( strFieldName.equals("DutyCode") ) {
			return 29;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return 30;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return 31;
		}
		if( strFieldName.equals("PayPlanCode") ) {
			return 32;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 33;
		}
		if( strFieldName.equals("PayCount") ) {
			return 34;
		}
		if( strFieldName.equals("PayYears") ) {
			return 35;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 36;
		}
		if( strFieldName.equals("AccBala") ) {
			return 37;
		}
		if( strFieldName.equals("FeeType") ) {
			return 38;
		}
		if( strFieldName.equals("FeeFinaType") ) {
			return 39;
		}
		if( strFieldName.equals("SumActuMoney") ) {
			return 40;
		}
		if( strFieldName.equals("BankCode") ) {
			return 41;
		}
		if( strFieldName.equals("AccName") ) {
			return 42;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 43;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 44;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return 45;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 46;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return 47;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 48;
		}
		if( strFieldName.equals("ACType") ) {
			return 49;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 50;
		}
		if( strFieldName.equals("AgentType") ) {
			return 51;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 52;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 53;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return 54;
		}
		if( strFieldName.equals("InsuYear") ) {
			return 55;
		}
		if( strFieldName.equals("Years") ) {
			return 56;
		}
		if( strFieldName.equals("PolYear") ) {
			return 57;
		}
		if( strFieldName.equals("AccYears") ) {
			return 58;
		}
		if( strFieldName.equals("MarketType") ) {
			return 59;
		}
		if( strFieldName.equals("BonusYear") ) {
			return 60;
		}
		if( strFieldName.equals("PayTypeFlag") ) {
			return 61;
		}
		if( strFieldName.equals("LastPayToDate") ) {
			return 62;
		}
		if( strFieldName.equals("CurPayToDate") ) {
			return 63;
		}
		if( strFieldName.equals("BranchType") ) {
			return 64;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 65;
		}
		if( strFieldName.equals("Award") ) {
			return 66;
		}
		if( strFieldName.equals("Allowance") ) {
			return 67;
		}
		if( strFieldName.equals("ChargeRate") ) {
			return 68;
		}
		if( strFieldName.equals("ChargeState") ) {
			return 69;
		}
		if( strFieldName.equals("TransType") ) {
			return 70;
		}
		if( strFieldName.equals("ChargeType") ) {
			return 71;
		}
		if( strFieldName.equals("EnterAccDate") ) {
			return 72;
		}
		if( strFieldName.equals("ConfDate") ) {
			return 73;
		}
		if( strFieldName.equals("AccountDate") ) {
			return 74;
		}
		if( strFieldName.equals("Currency") ) {
			return 75;
		}
		if( strFieldName.equals("Remark") ) {
			return 76;
		}
		if( strFieldName.equals("Operator") ) {
			return 77;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 78;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 79;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return 80;
		}
		if( strFieldName.equals("EventNo") ) {
			return 81;
		}
		if( strFieldName.equals("State") ) {
			return 82;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SubPhysicalTable";
				break;
			case 1:
				strFieldName = "KeyUnionValue";
				break;
			case 2:
				strFieldName = "MSerialNo";
				break;
			case 3:
				strFieldName = "ASerialNo";
				break;
			case 4:
				strFieldName = "IndexCode";
				break;
			case 5:
				strFieldName = "IndexNo";
				break;
			case 6:
				strFieldName = "OtherNoType";
				break;
			case 7:
				strFieldName = "OtherNo";
				break;
			case 8:
				strFieldName = "GetNoticeNo";
				break;
			case 9:
				strFieldName = "TempFeeNo";
				break;
			case 10:
				strFieldName = "TempFeeType";
				break;
			case 11:
				strFieldName = "PayMode";
				break;
			case 12:
				strFieldName = "CaseNo";
				break;
			case 13:
				strFieldName = "EdorAcceptNo";
				break;
			case 14:
				strFieldName = "EndorsementNo";
				break;
			case 15:
				strFieldName = "ReceiptNo";
				break;
			case 16:
				strFieldName = "CustomerNo";
				break;
			case 17:
				strFieldName = "SupplierNo";
				break;
			case 18:
				strFieldName = "AccType";
				break;
			case 19:
				strFieldName = "DestSource";
				break;
			case 20:
				strFieldName = "BakNo";
				break;
			case 21:
				strFieldName = "PolType";
				break;
			case 22:
				strFieldName = "GrpContNo";
				break;
			case 23:
				strFieldName = "GrpPolNo";
				break;
			case 24:
				strFieldName = "ContNo";
				break;
			case 25:
				strFieldName = "PolNo";
				break;
			case 26:
				strFieldName = "RiskPeriod";
				break;
			case 27:
				strFieldName = "RiskType";
				break;
			case 28:
				strFieldName = "RiskCode";
				break;
			case 29:
				strFieldName = "DutyCode";
				break;
			case 30:
				strFieldName = "GetDutyKind";
				break;
			case 31:
				strFieldName = "GetDutyCode";
				break;
			case 32:
				strFieldName = "PayPlanCode";
				break;
			case 33:
				strFieldName = "PayIntv";
				break;
			case 34:
				strFieldName = "PayCount";
				break;
			case 35:
				strFieldName = "PayYears";
				break;
			case 36:
				strFieldName = "CValiDate";
				break;
			case 37:
				strFieldName = "AccBala";
				break;
			case 38:
				strFieldName = "FeeType";
				break;
			case 39:
				strFieldName = "FeeFinaType";
				break;
			case 40:
				strFieldName = "SumActuMoney";
				break;
			case 41:
				strFieldName = "BankCode";
				break;
			case 42:
				strFieldName = "AccName";
				break;
			case 43:
				strFieldName = "BankAccNo";
				break;
			case 44:
				strFieldName = "SaleChnl";
				break;
			case 45:
				strFieldName = "SaleChnlDetail";
				break;
			case 46:
				strFieldName = "ManageCom";
				break;
			case 47:
				strFieldName = "ExecuteCom";
				break;
			case 48:
				strFieldName = "CostCenter";
				break;
			case 49:
				strFieldName = "ACType";
				break;
			case 50:
				strFieldName = "AgentCom";
				break;
			case 51:
				strFieldName = "AgentType";
				break;
			case 52:
				strFieldName = "AgentCode";
				break;
			case 53:
				strFieldName = "AgentGroup";
				break;
			case 54:
				strFieldName = "InsuYearFlag";
				break;
			case 55:
				strFieldName = "InsuYear";
				break;
			case 56:
				strFieldName = "Years";
				break;
			case 57:
				strFieldName = "PolYear";
				break;
			case 58:
				strFieldName = "AccYears";
				break;
			case 59:
				strFieldName = "MarketType";
				break;
			case 60:
				strFieldName = "BonusYear";
				break;
			case 61:
				strFieldName = "PayTypeFlag";
				break;
			case 62:
				strFieldName = "LastPayToDate";
				break;
			case 63:
				strFieldName = "CurPayToDate";
				break;
			case 64:
				strFieldName = "BranchType";
				break;
			case 65:
				strFieldName = "BranchType2";
				break;
			case 66:
				strFieldName = "Award";
				break;
			case 67:
				strFieldName = "Allowance";
				break;
			case 68:
				strFieldName = "ChargeRate";
				break;
			case 69:
				strFieldName = "ChargeState";
				break;
			case 70:
				strFieldName = "TransType";
				break;
			case 71:
				strFieldName = "ChargeType";
				break;
			case 72:
				strFieldName = "EnterAccDate";
				break;
			case 73:
				strFieldName = "ConfDate";
				break;
			case 74:
				strFieldName = "AccountDate";
				break;
			case 75:
				strFieldName = "Currency";
				break;
			case 76:
				strFieldName = "Remark";
				break;
			case 77:
				strFieldName = "Operator";
				break;
			case 78:
				strFieldName = "MakeDate";
				break;
			case 79:
				strFieldName = "MakeTime";
				break;
			case 80:
				strFieldName = "CheckFlag";
				break;
			case 81:
				strFieldName = "EventNo";
				break;
			case 82:
				strFieldName = "State";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SubPhysicalTable") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("KeyUnionValue") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ASerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetNoticeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempFeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorAcceptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SupplierNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DestSource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BakNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskPeriod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DutyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayPlanCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayYears") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccBala") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeFinaType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumActuMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ACType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Years") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PolYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AccYears") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BonusYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayTypeFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LastPayToDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CurPayToDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Award") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Allowance") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ChargeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ChargeState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChargeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnterAccDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ConfDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccountDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EventNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_INT;
				break;
			case 34:
				nFieldType = Schema.TYPE_INT;
				break;
			case 35:
				nFieldType = Schema.TYPE_INT;
				break;
			case 36:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 37:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_INT;
				break;
			case 56:
				nFieldType = Schema.TYPE_INT;
				break;
			case 57:
				nFieldType = Schema.TYPE_INT;
				break;
			case 58:
				nFieldType = Schema.TYPE_INT;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 63:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 66:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 67:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 68:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 69:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 70:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 71:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 72:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 73:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 74:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 75:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 76:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 77:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 78:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 79:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 80:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 81:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 82:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
