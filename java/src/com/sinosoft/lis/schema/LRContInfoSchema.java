/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRContInfoDB;

/*
 * <p>ClassName: LRContInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 再保合同信息表
 * @CreateDate：2016-06-07
 */
public class LRContInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 合同编号 */
	private String ReContCode;
	/** 合同名称 */
	private String ReContName;
	/** 再保公司代码 */
	private String ReComCode;
	/** 合同生效日期 */
	private Date RValiDate;
	/** 合同终止日期 */
	private Date RInvaliDate;
	/** 合同录入日期 */
	private Date InputDate;
	/** 再保项目 */
	private String ReinsurItem;
	/** 险种类别 */
	private String DiskKind;
	/** 分保方式 */
	private String CessionMode;
	/** 分保保费方式 */
	private String CessionFeeMode;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 操作人 */
	private String Operator;
	/** 管理机构 */
	private String ManageCom;
	/** 分保规则 */
	private String ReType;
	/** 合同状态 */
	private String ReContState;
	/** 合同溢额细分类型 */
	private String CessionDetailType;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRContInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ReContCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRContInfoSchema cloned = (LRContInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getReContCode()
	{
		return ReContCode;
	}
	public void setReContCode(String aReContCode)
	{
		ReContCode = aReContCode;
	}
	public String getReContName()
	{
		return ReContName;
	}
	public void setReContName(String aReContName)
	{
		ReContName = aReContName;
	}
	public String getReComCode()
	{
		return ReComCode;
	}
	public void setReComCode(String aReComCode)
	{
		ReComCode = aReComCode;
	}
	public String getRValiDate()
	{
		if( RValiDate != null )
			return fDate.getString(RValiDate);
		else
			return null;
	}
	public void setRValiDate(Date aRValiDate)
	{
		RValiDate = aRValiDate;
	}
	public void setRValiDate(String aRValiDate)
	{
		if (aRValiDate != null && !aRValiDate.equals("") )
		{
			RValiDate = fDate.getDate( aRValiDate );
		}
		else
			RValiDate = null;
	}

	public String getRInvaliDate()
	{
		if( RInvaliDate != null )
			return fDate.getString(RInvaliDate);
		else
			return null;
	}
	public void setRInvaliDate(Date aRInvaliDate)
	{
		RInvaliDate = aRInvaliDate;
	}
	public void setRInvaliDate(String aRInvaliDate)
	{
		if (aRInvaliDate != null && !aRInvaliDate.equals("") )
		{
			RInvaliDate = fDate.getDate( aRInvaliDate );
		}
		else
			RInvaliDate = null;
	}

	public String getInputDate()
	{
		if( InputDate != null )
			return fDate.getString(InputDate);
		else
			return null;
	}
	public void setInputDate(Date aInputDate)
	{
		InputDate = aInputDate;
	}
	public void setInputDate(String aInputDate)
	{
		if (aInputDate != null && !aInputDate.equals("") )
		{
			InputDate = fDate.getDate( aInputDate );
		}
		else
			InputDate = null;
	}

	public String getReinsurItem()
	{
		return ReinsurItem;
	}
	public void setReinsurItem(String aReinsurItem)
	{
		ReinsurItem = aReinsurItem;
	}
	public String getDiskKind()
	{
		return DiskKind;
	}
	public void setDiskKind(String aDiskKind)
	{
		DiskKind = aDiskKind;
	}
	public String getCessionMode()
	{
		return CessionMode;
	}
	public void setCessionMode(String aCessionMode)
	{
		CessionMode = aCessionMode;
	}
	public String getCessionFeeMode()
	{
		return CessionFeeMode;
	}
	public void setCessionFeeMode(String aCessionFeeMode)
	{
		CessionFeeMode = aCessionFeeMode;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getReType()
	{
		return ReType;
	}
	public void setReType(String aReType)
	{
		ReType = aReType;
	}
	public String getReContState()
	{
		return ReContState;
	}
	public void setReContState(String aReContState)
	{
		ReContState = aReContState;
	}
	public String getCessionDetailType()
	{
		return CessionDetailType;
	}
	public void setCessionDetailType(String aCessionDetailType)
	{
		CessionDetailType = aCessionDetailType;
	}

	/**
	* 使用另外一个 LRContInfoSchema 对象给 Schema 赋值
	* @param: aLRContInfoSchema LRContInfoSchema
	**/
	public void setSchema(LRContInfoSchema aLRContInfoSchema)
	{
		this.ReContCode = aLRContInfoSchema.getReContCode();
		this.ReContName = aLRContInfoSchema.getReContName();
		this.ReComCode = aLRContInfoSchema.getReComCode();
		this.RValiDate = fDate.getDate( aLRContInfoSchema.getRValiDate());
		this.RInvaliDate = fDate.getDate( aLRContInfoSchema.getRInvaliDate());
		this.InputDate = fDate.getDate( aLRContInfoSchema.getInputDate());
		this.ReinsurItem = aLRContInfoSchema.getReinsurItem();
		this.DiskKind = aLRContInfoSchema.getDiskKind();
		this.CessionMode = aLRContInfoSchema.getCessionMode();
		this.CessionFeeMode = aLRContInfoSchema.getCessionFeeMode();
		this.MakeDate = fDate.getDate( aLRContInfoSchema.getMakeDate());
		this.MakeTime = aLRContInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRContInfoSchema.getModifyDate());
		this.ModifyTime = aLRContInfoSchema.getModifyTime();
		this.Operator = aLRContInfoSchema.getOperator();
		this.ManageCom = aLRContInfoSchema.getManageCom();
		this.ReType = aLRContInfoSchema.getReType();
		this.ReContState = aLRContInfoSchema.getReContState();
		this.CessionDetailType = aLRContInfoSchema.getCessionDetailType();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ReContCode") == null )
				this.ReContCode = null;
			else
				this.ReContCode = rs.getString("ReContCode").trim();

			if( rs.getString("ReContName") == null )
				this.ReContName = null;
			else
				this.ReContName = rs.getString("ReContName").trim();

			if( rs.getString("ReComCode") == null )
				this.ReComCode = null;
			else
				this.ReComCode = rs.getString("ReComCode").trim();

			this.RValiDate = rs.getDate("RValiDate");
			this.RInvaliDate = rs.getDate("RInvaliDate");
			this.InputDate = rs.getDate("InputDate");
			if( rs.getString("ReinsurItem") == null )
				this.ReinsurItem = null;
			else
				this.ReinsurItem = rs.getString("ReinsurItem").trim();

			if( rs.getString("DiskKind") == null )
				this.DiskKind = null;
			else
				this.DiskKind = rs.getString("DiskKind").trim();

			if( rs.getString("CessionMode") == null )
				this.CessionMode = null;
			else
				this.CessionMode = rs.getString("CessionMode").trim();

			if( rs.getString("CessionFeeMode") == null )
				this.CessionFeeMode = null;
			else
				this.CessionFeeMode = rs.getString("CessionFeeMode").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ReType") == null )
				this.ReType = null;
			else
				this.ReType = rs.getString("ReType").trim();

			if( rs.getString("ReContState") == null )
				this.ReContState = null;
			else
				this.ReContState = rs.getString("ReContState").trim();

			if( rs.getString("CessionDetailType") == null )
				this.CessionDetailType = null;
			else
				this.CessionDetailType = rs.getString("CessionDetailType").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRContInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRContInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRContInfoSchema getSchema()
	{
		LRContInfoSchema aLRContInfoSchema = new LRContInfoSchema();
		aLRContInfoSchema.setSchema(this);
		return aLRContInfoSchema;
	}

	public LRContInfoDB getDB()
	{
		LRContInfoDB aDBOper = new LRContInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRContInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ReContCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReContName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RInvaliDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InputDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReinsurItem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiskKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CessionMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CessionFeeMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReContState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CessionDetailType));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRContInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ReContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ReContName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ReComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			RInvaliDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			InputDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			ReinsurItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			DiskKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CessionMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CessionFeeMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ReType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ReContState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			CessionDetailType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRContInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ReContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContCode));
		}
		if (FCode.equals("ReContName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContName));
		}
		if (FCode.equals("ReComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReComCode));
		}
		if (FCode.equals("RValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRValiDate()));
		}
		if (FCode.equals("RInvaliDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRInvaliDate()));
		}
		if (FCode.equals("InputDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
		}
		if (FCode.equals("ReinsurItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReinsurItem));
		}
		if (FCode.equals("DiskKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiskKind));
		}
		if (FCode.equals("CessionMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionMode));
		}
		if (FCode.equals("CessionFeeMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionFeeMode));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ReType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReType));
		}
		if (FCode.equals("ReContState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContState));
		}
		if (FCode.equals("CessionDetailType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionDetailType));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ReContCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ReContName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ReComCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRValiDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRInvaliDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ReinsurItem);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(DiskKind);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CessionMode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CessionFeeMode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ReType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ReContState);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(CessionDetailType);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ReContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContCode = FValue.trim();
			}
			else
				ReContCode = null;
		}
		if (FCode.equalsIgnoreCase("ReContName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContName = FValue.trim();
			}
			else
				ReContName = null;
		}
		if (FCode.equalsIgnoreCase("ReComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReComCode = FValue.trim();
			}
			else
				ReComCode = null;
		}
		if (FCode.equalsIgnoreCase("RValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RValiDate = fDate.getDate( FValue );
			}
			else
				RValiDate = null;
		}
		if (FCode.equalsIgnoreCase("RInvaliDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RInvaliDate = fDate.getDate( FValue );
			}
			else
				RInvaliDate = null;
		}
		if (FCode.equalsIgnoreCase("InputDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InputDate = fDate.getDate( FValue );
			}
			else
				InputDate = null;
		}
		if (FCode.equalsIgnoreCase("ReinsurItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReinsurItem = FValue.trim();
			}
			else
				ReinsurItem = null;
		}
		if (FCode.equalsIgnoreCase("DiskKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiskKind = FValue.trim();
			}
			else
				DiskKind = null;
		}
		if (FCode.equalsIgnoreCase("CessionMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CessionMode = FValue.trim();
			}
			else
				CessionMode = null;
		}
		if (FCode.equalsIgnoreCase("CessionFeeMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CessionFeeMode = FValue.trim();
			}
			else
				CessionFeeMode = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ReType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReType = FValue.trim();
			}
			else
				ReType = null;
		}
		if (FCode.equalsIgnoreCase("ReContState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContState = FValue.trim();
			}
			else
				ReContState = null;
		}
		if (FCode.equalsIgnoreCase("CessionDetailType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CessionDetailType = FValue.trim();
			}
			else
				CessionDetailType = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRContInfoSchema other = (LRContInfoSchema)otherObject;
		return
			(ReContCode == null ? other.getReContCode() == null : ReContCode.equals(other.getReContCode()))
			&& (ReContName == null ? other.getReContName() == null : ReContName.equals(other.getReContName()))
			&& (ReComCode == null ? other.getReComCode() == null : ReComCode.equals(other.getReComCode()))
			&& (RValiDate == null ? other.getRValiDate() == null : fDate.getString(RValiDate).equals(other.getRValiDate()))
			&& (RInvaliDate == null ? other.getRInvaliDate() == null : fDate.getString(RInvaliDate).equals(other.getRInvaliDate()))
			&& (InputDate == null ? other.getInputDate() == null : fDate.getString(InputDate).equals(other.getInputDate()))
			&& (ReinsurItem == null ? other.getReinsurItem() == null : ReinsurItem.equals(other.getReinsurItem()))
			&& (DiskKind == null ? other.getDiskKind() == null : DiskKind.equals(other.getDiskKind()))
			&& (CessionMode == null ? other.getCessionMode() == null : CessionMode.equals(other.getCessionMode()))
			&& (CessionFeeMode == null ? other.getCessionFeeMode() == null : CessionFeeMode.equals(other.getCessionFeeMode()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (ReType == null ? other.getReType() == null : ReType.equals(other.getReType()))
			&& (ReContState == null ? other.getReContState() == null : ReContState.equals(other.getReContState()))
			&& (CessionDetailType == null ? other.getCessionDetailType() == null : CessionDetailType.equals(other.getCessionDetailType()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ReContCode") ) {
			return 0;
		}
		if( strFieldName.equals("ReContName") ) {
			return 1;
		}
		if( strFieldName.equals("ReComCode") ) {
			return 2;
		}
		if( strFieldName.equals("RValiDate") ) {
			return 3;
		}
		if( strFieldName.equals("RInvaliDate") ) {
			return 4;
		}
		if( strFieldName.equals("InputDate") ) {
			return 5;
		}
		if( strFieldName.equals("ReinsurItem") ) {
			return 6;
		}
		if( strFieldName.equals("DiskKind") ) {
			return 7;
		}
		if( strFieldName.equals("CessionMode") ) {
			return 8;
		}
		if( strFieldName.equals("CessionFeeMode") ) {
			return 9;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 13;
		}
		if( strFieldName.equals("Operator") ) {
			return 14;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 15;
		}
		if( strFieldName.equals("ReType") ) {
			return 16;
		}
		if( strFieldName.equals("ReContState") ) {
			return 17;
		}
		if( strFieldName.equals("CessionDetailType") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ReContCode";
				break;
			case 1:
				strFieldName = "ReContName";
				break;
			case 2:
				strFieldName = "ReComCode";
				break;
			case 3:
				strFieldName = "RValiDate";
				break;
			case 4:
				strFieldName = "RInvaliDate";
				break;
			case 5:
				strFieldName = "InputDate";
				break;
			case 6:
				strFieldName = "ReinsurItem";
				break;
			case 7:
				strFieldName = "DiskKind";
				break;
			case 8:
				strFieldName = "CessionMode";
				break;
			case 9:
				strFieldName = "CessionFeeMode";
				break;
			case 10:
				strFieldName = "MakeDate";
				break;
			case 11:
				strFieldName = "MakeTime";
				break;
			case 12:
				strFieldName = "ModifyDate";
				break;
			case 13:
				strFieldName = "ModifyTime";
				break;
			case 14:
				strFieldName = "Operator";
				break;
			case 15:
				strFieldName = "ManageCom";
				break;
			case 16:
				strFieldName = "ReType";
				break;
			case 17:
				strFieldName = "ReContState";
				break;
			case 18:
				strFieldName = "CessionDetailType";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ReContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReContName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RInvaliDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InputDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReinsurItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiskKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessionMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessionFeeMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReContState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessionDetailType") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
