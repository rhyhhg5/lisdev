/*
 * <p>ClassName: LFComFinSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LFComFinDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFComFinSchema implements Schema
{
    // @Field
    /** 财务管理机构编码 */
    private String ComFin;
    /** 机构名称 */
    private String Name;
    /** 成立日期 */
    private Date FoundDate;
    /** 停业标志 */
    private String EndFlag;
    /** 停业日期 */
    private Date EndDate;
    /** 机构属性 */
    private String Attribute;
    /** 机构标志 */
    private String Sign;
    /** 统计标志 */
    private String calFlag;
    /** 机构地址 */
    private String Address;
    /** 机构邮编 */
    private String ZipCode;
    /** 机构电话 */
    private String Phone;
    /** 机构传真 */
    private String Fax;
    /** Email */
    private String EMail;
    /** 网址 */
    private String WebAddress;
    /** 主管人姓名 */
    private String SatrapName;
    /** 总公司正式批示时间 */
    private Date ApproveDate;
    /** 工商批准营业时间 */
    private Date PassDate;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFComFinSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ComFin";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getComFin()
    {
        if (ComFin != null && !ComFin.equals("") && SysConst.CHANGECHARSET == true)
        {
            ComFin = StrTool.unicodeToGBK(ComFin);
        }
        return ComFin;
    }

    public void setComFin(String aComFin)
    {
        ComFin = aComFin;
    }

    public String getName()
    {
        if (Name != null && !Name.equals("") && SysConst.CHANGECHARSET == true)
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getFoundDate()
    {
        if (FoundDate != null)
        {
            return fDate.getString(FoundDate);
        }
        else
        {
            return null;
        }
    }

    public void setFoundDate(Date aFoundDate)
    {
        FoundDate = aFoundDate;
    }

    public void setFoundDate(String aFoundDate)
    {
        if (aFoundDate != null && !aFoundDate.equals(""))
        {
            FoundDate = fDate.getDate(aFoundDate);
        }
        else
        {
            FoundDate = null;
        }
    }

    public String getEndFlag()
    {
        if (EndFlag != null && !EndFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            EndFlag = StrTool.unicodeToGBK(EndFlag);
        }
        return EndFlag;
    }

    public void setEndFlag(String aEndFlag)
    {
        EndFlag = aEndFlag;
    }

    public String getEndDate()
    {
        if (EndDate != null)
        {
            return fDate.getString(EndDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndDate(Date aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            EndDate = fDate.getDate(aEndDate);
        }
        else
        {
            EndDate = null;
        }
    }

    public String getAttribute()
    {
        if (Attribute != null && !Attribute.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            Attribute = StrTool.unicodeToGBK(Attribute);
        }
        return Attribute;
    }

    public void setAttribute(String aAttribute)
    {
        Attribute = aAttribute;
    }

    public String getSign()
    {
        if (Sign != null && !Sign.equals("") && SysConst.CHANGECHARSET == true)
        {
            Sign = StrTool.unicodeToGBK(Sign);
        }
        return Sign;
    }

    public void setSign(String aSign)
    {
        Sign = aSign;
    }

    public String getcalFlag()
    {
        if (calFlag != null && !calFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            calFlag = StrTool.unicodeToGBK(calFlag);
        }
        return calFlag;
    }

    public void setcalFlag(String acalFlag)
    {
        calFlag = acalFlag;
    }

    public String getAddress()
    {
        if (Address != null && !Address.equals("") && SysConst.CHANGECHARSET == true)
        {
            Address = StrTool.unicodeToGBK(Address);
        }
        return Address;
    }

    public void setAddress(String aAddress)
    {
        Address = aAddress;
    }

    public String getZipCode()
    {
        if (ZipCode != null && !ZipCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ZipCode = StrTool.unicodeToGBK(ZipCode);
        }
        return ZipCode;
    }

    public void setZipCode(String aZipCode)
    {
        ZipCode = aZipCode;
    }

    public String getPhone()
    {
        if (Phone != null && !Phone.equals("") && SysConst.CHANGECHARSET == true)
        {
            Phone = StrTool.unicodeToGBK(Phone);
        }
        return Phone;
    }

    public void setPhone(String aPhone)
    {
        Phone = aPhone;
    }

    public String getFax()
    {
        if (Fax != null && !Fax.equals("") && SysConst.CHANGECHARSET == true)
        {
            Fax = StrTool.unicodeToGBK(Fax);
        }
        return Fax;
    }

    public void setFax(String aFax)
    {
        Fax = aFax;
    }

    public String getEMail()
    {
        if (EMail != null && !EMail.equals("") && SysConst.CHANGECHARSET == true)
        {
            EMail = StrTool.unicodeToGBK(EMail);
        }
        return EMail;
    }

    public void setEMail(String aEMail)
    {
        EMail = aEMail;
    }

    public String getWebAddress()
    {
        if (WebAddress != null && !WebAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            WebAddress = StrTool.unicodeToGBK(WebAddress);
        }
        return WebAddress;
    }

    public void setWebAddress(String aWebAddress)
    {
        WebAddress = aWebAddress;
    }

    public String getSatrapName()
    {
        if (SatrapName != null && !SatrapName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SatrapName = StrTool.unicodeToGBK(SatrapName);
        }
        return SatrapName;
    }

    public void setSatrapName(String aSatrapName)
    {
        SatrapName = aSatrapName;
    }

    public String getApproveDate()
    {
        if (ApproveDate != null)
        {
            return fDate.getString(ApproveDate);
        }
        else
        {
            return null;
        }
    }

    public void setApproveDate(Date aApproveDate)
    {
        ApproveDate = aApproveDate;
    }

    public void setApproveDate(String aApproveDate)
    {
        if (aApproveDate != null && !aApproveDate.equals(""))
        {
            ApproveDate = fDate.getDate(aApproveDate);
        }
        else
        {
            ApproveDate = null;
        }
    }

    public String getPassDate()
    {
        if (PassDate != null)
        {
            return fDate.getString(PassDate);
        }
        else
        {
            return null;
        }
    }

    public void setPassDate(Date aPassDate)
    {
        PassDate = aPassDate;
    }

    public void setPassDate(String aPassDate)
    {
        if (aPassDate != null && !aPassDate.equals(""))
        {
            PassDate = fDate.getDate(aPassDate);
        }
        else
        {
            PassDate = null;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LFComFinSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFComFinSchema aLFComFinSchema)
    {
        this.ComFin = aLFComFinSchema.getComFin();
        this.Name = aLFComFinSchema.getName();
        this.FoundDate = fDate.getDate(aLFComFinSchema.getFoundDate());
        this.EndFlag = aLFComFinSchema.getEndFlag();
        this.EndDate = fDate.getDate(aLFComFinSchema.getEndDate());
        this.Attribute = aLFComFinSchema.getAttribute();
        this.Sign = aLFComFinSchema.getSign();
        this.calFlag = aLFComFinSchema.getcalFlag();
        this.Address = aLFComFinSchema.getAddress();
        this.ZipCode = aLFComFinSchema.getZipCode();
        this.Phone = aLFComFinSchema.getPhone();
        this.Fax = aLFComFinSchema.getFax();
        this.EMail = aLFComFinSchema.getEMail();
        this.WebAddress = aLFComFinSchema.getWebAddress();
        this.SatrapName = aLFComFinSchema.getSatrapName();
        this.ApproveDate = fDate.getDate(aLFComFinSchema.getApproveDate());
        this.PassDate = fDate.getDate(aLFComFinSchema.getPassDate());
        this.Operator = aLFComFinSchema.getOperator();
        this.MakeDate = fDate.getDate(aLFComFinSchema.getMakeDate());
        this.MakeTime = aLFComFinSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLFComFinSchema.getModifyDate());
        this.ModifyTime = aLFComFinSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ComFin") == null)
            {
                this.ComFin = null;
            }
            else
            {
                this.ComFin = rs.getString("ComFin").trim();
            }

            if (rs.getString("Name") == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString("Name").trim();
            }

            this.FoundDate = rs.getDate("FoundDate");
            if (rs.getString("EndFlag") == null)
            {
                this.EndFlag = null;
            }
            else
            {
                this.EndFlag = rs.getString("EndFlag").trim();
            }

            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("Attribute") == null)
            {
                this.Attribute = null;
            }
            else
            {
                this.Attribute = rs.getString("Attribute").trim();
            }

            if (rs.getString("Sign") == null)
            {
                this.Sign = null;
            }
            else
            {
                this.Sign = rs.getString("Sign").trim();
            }

            if (rs.getString("calFlag") == null)
            {
                this.calFlag = null;
            }
            else
            {
                this.calFlag = rs.getString("calFlag").trim();
            }

            if (rs.getString("Address") == null)
            {
                this.Address = null;
            }
            else
            {
                this.Address = rs.getString("Address").trim();
            }

            if (rs.getString("ZipCode") == null)
            {
                this.ZipCode = null;
            }
            else
            {
                this.ZipCode = rs.getString("ZipCode").trim();
            }

            if (rs.getString("Phone") == null)
            {
                this.Phone = null;
            }
            else
            {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("Fax") == null)
            {
                this.Fax = null;
            }
            else
            {
                this.Fax = rs.getString("Fax").trim();
            }

            if (rs.getString("EMail") == null)
            {
                this.EMail = null;
            }
            else
            {
                this.EMail = rs.getString("EMail").trim();
            }

            if (rs.getString("WebAddress") == null)
            {
                this.WebAddress = null;
            }
            else
            {
                this.WebAddress = rs.getString("WebAddress").trim();
            }

            if (rs.getString("SatrapName") == null)
            {
                this.SatrapName = null;
            }
            else
            {
                this.SatrapName = rs.getString("SatrapName").trim();
            }

            this.ApproveDate = rs.getDate("ApproveDate");
            this.PassDate = rs.getDate("PassDate");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFComFinSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFComFinSchema getSchema()
    {
        LFComFinSchema aLFComFinSchema = new LFComFinSchema();
        aLFComFinSchema.setSchema(this);
        return aLFComFinSchema;
    }

    public LFComFinDB getDB()
    {
        LFComFinDB aDBOper = new LFComFinDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFComFin描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ComFin)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Name)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            FoundDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EndFlag)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(EndDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Attribute)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Sign)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(calFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Address)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ZipCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Phone)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Fax)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EMail)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(WebAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SatrapName)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ApproveDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(PassDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFComFin>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ComFin = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                  SysConst.PACKAGESPILTER);
            FoundDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            EndFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            Attribute = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            Sign = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            calFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                   SysConst.PACKAGESPILTER);
            Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                 SysConst.PACKAGESPILTER);
            EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                   SysConst.PACKAGESPILTER);
            WebAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            SatrapName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            PassDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFComFinSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ComFin"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComFin));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Name));
        }
        if (FCode.equals("FoundDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getFoundDate()));
        }
        if (FCode.equals("EndFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndFlag));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getEndDate()));
        }
        if (FCode.equals("Attribute"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Attribute));
        }
        if (FCode.equals("Sign"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Sign));
        }
        if (FCode.equals("calFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(calFlag));
        }
        if (FCode.equals("Address"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Address));
        }
        if (FCode.equals("ZipCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ZipCode));
        }
        if (FCode.equals("Phone"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Phone));
        }
        if (FCode.equals("Fax"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Fax));
        }
        if (FCode.equals("EMail"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EMail));
        }
        if (FCode.equals("WebAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WebAddress));
        }
        if (FCode.equals("SatrapName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SatrapName));
        }
        if (FCode.equals("ApproveDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getApproveDate()));
        }
        if (FCode.equals("PassDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getPassDate()));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ComFin);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFoundDate()));
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(EndFlag);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Attribute);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Sign);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(calFlag);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Address);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Fax);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(EMail);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(WebAddress);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(SatrapName);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getApproveDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPassDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ComFin"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComFin = FValue.trim();
            }
            else
            {
                ComFin = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("FoundDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FoundDate = fDate.getDate(FValue);
            }
            else
            {
                FoundDate = null;
            }
        }
        if (FCode.equals("EndFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndFlag = FValue.trim();
            }
            else
            {
                EndFlag = null;
            }
        }
        if (FCode.equals("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDate = fDate.getDate(FValue);
            }
            else
            {
                EndDate = null;
            }
        }
        if (FCode.equals("Attribute"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Attribute = FValue.trim();
            }
            else
            {
                Attribute = null;
            }
        }
        if (FCode.equals("Sign"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Sign = FValue.trim();
            }
            else
            {
                Sign = null;
            }
        }
        if (FCode.equals("calFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                calFlag = FValue.trim();
            }
            else
            {
                calFlag = null;
            }
        }
        if (FCode.equals("Address"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
            {
                Address = null;
            }
        }
        if (FCode.equals("ZipCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
            {
                ZipCode = null;
            }
        }
        if (FCode.equals("Phone"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
            {
                Phone = null;
            }
        }
        if (FCode.equals("Fax"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
            {
                Fax = null;
            }
        }
        if (FCode.equals("EMail"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
            {
                EMail = null;
            }
        }
        if (FCode.equals("WebAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WebAddress = FValue.trim();
            }
            else
            {
                WebAddress = null;
            }
        }
        if (FCode.equals("SatrapName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SatrapName = FValue.trim();
            }
            else
            {
                SatrapName = null;
            }
        }
        if (FCode.equals("ApproveDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveDate = fDate.getDate(FValue);
            }
            else
            {
                ApproveDate = null;
            }
        }
        if (FCode.equals("PassDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PassDate = fDate.getDate(FValue);
            }
            else
            {
                PassDate = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFComFinSchema other = (LFComFinSchema) otherObject;
        return
                ComFin.equals(other.getComFin())
                && Name.equals(other.getName())
                && fDate.getString(FoundDate).equals(other.getFoundDate())
                && EndFlag.equals(other.getEndFlag())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && Attribute.equals(other.getAttribute())
                && Sign.equals(other.getSign())
                && calFlag.equals(other.getcalFlag())
                && Address.equals(other.getAddress())
                && ZipCode.equals(other.getZipCode())
                && Phone.equals(other.getPhone())
                && Fax.equals(other.getFax())
                && EMail.equals(other.getEMail())
                && WebAddress.equals(other.getWebAddress())
                && SatrapName.equals(other.getSatrapName())
                && fDate.getString(ApproveDate).equals(other.getApproveDate())
                && fDate.getString(PassDate).equals(other.getPassDate())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ComFin"))
        {
            return 0;
        }
        if (strFieldName.equals("Name"))
        {
            return 1;
        }
        if (strFieldName.equals("FoundDate"))
        {
            return 2;
        }
        if (strFieldName.equals("EndFlag"))
        {
            return 3;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 4;
        }
        if (strFieldName.equals("Attribute"))
        {
            return 5;
        }
        if (strFieldName.equals("Sign"))
        {
            return 6;
        }
        if (strFieldName.equals("calFlag"))
        {
            return 7;
        }
        if (strFieldName.equals("Address"))
        {
            return 8;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return 9;
        }
        if (strFieldName.equals("Phone"))
        {
            return 10;
        }
        if (strFieldName.equals("Fax"))
        {
            return 11;
        }
        if (strFieldName.equals("EMail"))
        {
            return 12;
        }
        if (strFieldName.equals("WebAddress"))
        {
            return 13;
        }
        if (strFieldName.equals("SatrapName"))
        {
            return 14;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return 15;
        }
        if (strFieldName.equals("PassDate"))
        {
            return 16;
        }
        if (strFieldName.equals("Operator"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ComFin";
                break;
            case 1:
                strFieldName = "Name";
                break;
            case 2:
                strFieldName = "FoundDate";
                break;
            case 3:
                strFieldName = "EndFlag";
                break;
            case 4:
                strFieldName = "EndDate";
                break;
            case 5:
                strFieldName = "Attribute";
                break;
            case 6:
                strFieldName = "Sign";
                break;
            case 7:
                strFieldName = "calFlag";
                break;
            case 8:
                strFieldName = "Address";
                break;
            case 9:
                strFieldName = "ZipCode";
                break;
            case 10:
                strFieldName = "Phone";
                break;
            case 11:
                strFieldName = "Fax";
                break;
            case 12:
                strFieldName = "EMail";
                break;
            case 13:
                strFieldName = "WebAddress";
                break;
            case 14:
                strFieldName = "SatrapName";
                break;
            case 15:
                strFieldName = "ApproveDate";
                break;
            case 16:
                strFieldName = "PassDate";
                break;
            case 17:
                strFieldName = "Operator";
                break;
            case 18:
                strFieldName = "MakeDate";
                break;
            case 19:
                strFieldName = "MakeTime";
                break;
            case 20:
                strFieldName = "ModifyDate";
                break;
            case 21:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ComFin"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FoundDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Attribute"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Sign"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("calFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Address"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Fax"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EMail"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WebAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SatrapName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PassDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
