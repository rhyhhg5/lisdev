/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCASESTATEPUSHDB;

/*
 * <p>ClassName: LLCASESTATEPUSHSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-07-27
 */
public class LLCASESTATEPUSHSchema implements Schema, Cloneable
{
	// @Field
	/** 分案号(个人理赔号) */
	private String CASENO;
	/** 案件状态 */
	private String RGTSTATE;
	/** 受理类型 */
	private String RGTTYPE;
	/** 机构代码 */
	private String MNGCOM;
	/** 入机日期 */
	private Date MAKEDATE;
	/** 入机时间 */
	private String MAKETIME;
	/** 修改日期 */
	private Date MODIFYDATE;
	/** 修改时间 */
	private String MODIFYTIME;
	/** 备注 */
	private String REMARK;

	public static final int FIELDNUM = 9;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCASESTATEPUSHSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "CASENO";
		pk[1] = "RGTSTATE";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLCASESTATEPUSHSchema cloned = (LLCASESTATEPUSHSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCASENO()
	{
		return CASENO;
	}
	public void setCASENO(String aCASENO)
	{
		CASENO = aCASENO;
	}
	public String getRGTSTATE()
	{
		return RGTSTATE;
	}
	public void setRGTSTATE(String aRGTSTATE)
	{
		RGTSTATE = aRGTSTATE;
	}
	public String getRGTTYPE()
	{
		return RGTTYPE;
	}
	public void setRGTTYPE(String aRGTTYPE)
	{
		RGTTYPE = aRGTTYPE;
	}
	public String getMNGCOM()
	{
		return MNGCOM;
	}
	public void setMNGCOM(String aMNGCOM)
	{
		MNGCOM = aMNGCOM;
	}
	public String getMAKEDATE()
	{
		if( MAKEDATE != null )
			return fDate.getString(MAKEDATE);
		else
			return null;
	}
	public void setMAKEDATE(Date aMAKEDATE)
	{
		MAKEDATE = aMAKEDATE;
	}
	public void setMAKEDATE(String aMAKEDATE)
	{
		if (aMAKEDATE != null && !aMAKEDATE.equals("") )
		{
			MAKEDATE = fDate.getDate( aMAKEDATE );
		}
		else
			MAKEDATE = null;
	}

	public String getMAKETIME()
	{
		return MAKETIME;
	}
	public void setMAKETIME(String aMAKETIME)
	{
		MAKETIME = aMAKETIME;
	}
	public String getMODIFYDATE()
	{
		if( MODIFYDATE != null )
			return fDate.getString(MODIFYDATE);
		else
			return null;
	}
	public void setMODIFYDATE(Date aMODIFYDATE)
	{
		MODIFYDATE = aMODIFYDATE;
	}
	public void setMODIFYDATE(String aMODIFYDATE)
	{
		if (aMODIFYDATE != null && !aMODIFYDATE.equals("") )
		{
			MODIFYDATE = fDate.getDate( aMODIFYDATE );
		}
		else
			MODIFYDATE = null;
	}

	public String getMODIFYTIME()
	{
		return MODIFYTIME;
	}
	public void setMODIFYTIME(String aMODIFYTIME)
	{
		MODIFYTIME = aMODIFYTIME;
	}
	public String getREMARK()
	{
		return REMARK;
	}
	public void setREMARK(String aREMARK)
	{
		REMARK = aREMARK;
	}

	/**
	* 使用另外一个 LLCASESTATEPUSHSchema 对象给 Schema 赋值
	* @param: aLLCASESTATEPUSHSchema LLCASESTATEPUSHSchema
	**/
	public void setSchema(LLCASESTATEPUSHSchema aLLCASESTATEPUSHSchema)
	{
		this.CASENO = aLLCASESTATEPUSHSchema.getCASENO();
		this.RGTSTATE = aLLCASESTATEPUSHSchema.getRGTSTATE();
		this.RGTTYPE = aLLCASESTATEPUSHSchema.getRGTTYPE();
		this.MNGCOM = aLLCASESTATEPUSHSchema.getMNGCOM();
		this.MAKEDATE = fDate.getDate( aLLCASESTATEPUSHSchema.getMAKEDATE());
		this.MAKETIME = aLLCASESTATEPUSHSchema.getMAKETIME();
		this.MODIFYDATE = fDate.getDate( aLLCASESTATEPUSHSchema.getMODIFYDATE());
		this.MODIFYTIME = aLLCASESTATEPUSHSchema.getMODIFYTIME();
		this.REMARK = aLLCASESTATEPUSHSchema.getREMARK();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CASENO") == null )
				this.CASENO = null;
			else
				this.CASENO = rs.getString("CASENO").trim();

			if( rs.getString("RGTSTATE") == null )
				this.RGTSTATE = null;
			else
				this.RGTSTATE = rs.getString("RGTSTATE").trim();

			if( rs.getString("RGTTYPE") == null )
				this.RGTTYPE = null;
			else
				this.RGTTYPE = rs.getString("RGTTYPE").trim();

			if( rs.getString("MNGCOM") == null )
				this.MNGCOM = null;
			else
				this.MNGCOM = rs.getString("MNGCOM").trim();

			this.MAKEDATE = rs.getDate("MAKEDATE");
			if( rs.getString("MAKETIME") == null )
				this.MAKETIME = null;
			else
				this.MAKETIME = rs.getString("MAKETIME").trim();

			this.MODIFYDATE = rs.getDate("MODIFYDATE");
			if( rs.getString("MODIFYTIME") == null )
				this.MODIFYTIME = null;
			else
				this.MODIFYTIME = rs.getString("MODIFYTIME").trim();

			if( rs.getString("REMARK") == null )
				this.REMARK = null;
			else
				this.REMARK = rs.getString("REMARK").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCASESTATEPUSH表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCASESTATEPUSHSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCASESTATEPUSHSchema getSchema()
	{
		LLCASESTATEPUSHSchema aLLCASESTATEPUSHSchema = new LLCASESTATEPUSHSchema();
		aLLCASESTATEPUSHSchema.setSchema(this);
		return aLLCASESTATEPUSHSchema;
	}

	public LLCASESTATEPUSHDB getDB()
	{
		LLCASESTATEPUSHDB aDBOper = new LLCASESTATEPUSHDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCASESTATEPUSH描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CASENO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RGTSTATE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RGTTYPE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MNGCOM)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MAKEDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MAKETIME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MODIFYDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MODIFYTIME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(REMARK));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCASESTATEPUSH>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CASENO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RGTSTATE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RGTTYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			MNGCOM = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			MAKEDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			MAKETIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			MODIFYDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			MODIFYTIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			REMARK = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCASESTATEPUSHSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CASENO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CASENO));
		}
		if (FCode.equals("RGTSTATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RGTSTATE));
		}
		if (FCode.equals("RGTTYPE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RGTTYPE));
		}
		if (FCode.equals("MNGCOM"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MNGCOM));
		}
		if (FCode.equals("MAKEDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
		}
		if (FCode.equals("MAKETIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
		}
		if (FCode.equals("MODIFYDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
		}
		if (FCode.equals("MODIFYTIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYTIME));
		}
		if (FCode.equals("REMARK"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(REMARK));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CASENO);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RGTSTATE);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RGTTYPE);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(MNGCOM);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(MAKETIME);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MODIFYTIME);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(REMARK);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CASENO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CASENO = FValue.trim();
			}
			else
				CASENO = null;
		}
		if (FCode.equalsIgnoreCase("RGTSTATE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RGTSTATE = FValue.trim();
			}
			else
				RGTSTATE = null;
		}
		if (FCode.equalsIgnoreCase("RGTTYPE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RGTTYPE = FValue.trim();
			}
			else
				RGTTYPE = null;
		}
		if (FCode.equalsIgnoreCase("MNGCOM"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MNGCOM = FValue.trim();
			}
			else
				MNGCOM = null;
		}
		if (FCode.equalsIgnoreCase("MAKEDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MAKEDATE = fDate.getDate( FValue );
			}
			else
				MAKEDATE = null;
		}
		if (FCode.equalsIgnoreCase("MAKETIME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MAKETIME = FValue.trim();
			}
			else
				MAKETIME = null;
		}
		if (FCode.equalsIgnoreCase("MODIFYDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MODIFYDATE = fDate.getDate( FValue );
			}
			else
				MODIFYDATE = null;
		}
		if (FCode.equalsIgnoreCase("MODIFYTIME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MODIFYTIME = FValue.trim();
			}
			else
				MODIFYTIME = null;
		}
		if (FCode.equalsIgnoreCase("REMARK"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				REMARK = FValue.trim();
			}
			else
				REMARK = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCASESTATEPUSHSchema other = (LLCASESTATEPUSHSchema)otherObject;
		return
			(CASENO == null ? other.getCASENO() == null : CASENO.equals(other.getCASENO()))
			&& (RGTSTATE == null ? other.getRGTSTATE() == null : RGTSTATE.equals(other.getRGTSTATE()))
			&& (RGTTYPE == null ? other.getRGTTYPE() == null : RGTTYPE.equals(other.getRGTTYPE()))
			&& (MNGCOM == null ? other.getMNGCOM() == null : MNGCOM.equals(other.getMNGCOM()))
			&& (MAKEDATE == null ? other.getMAKEDATE() == null : fDate.getString(MAKEDATE).equals(other.getMAKEDATE()))
			&& (MAKETIME == null ? other.getMAKETIME() == null : MAKETIME.equals(other.getMAKETIME()))
			&& (MODIFYDATE == null ? other.getMODIFYDATE() == null : fDate.getString(MODIFYDATE).equals(other.getMODIFYDATE()))
			&& (MODIFYTIME == null ? other.getMODIFYTIME() == null : MODIFYTIME.equals(other.getMODIFYTIME()))
			&& (REMARK == null ? other.getREMARK() == null : REMARK.equals(other.getREMARK()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CASENO") ) {
			return 0;
		}
		if( strFieldName.equals("RGTSTATE") ) {
			return 1;
		}
		if( strFieldName.equals("RGTTYPE") ) {
			return 2;
		}
		if( strFieldName.equals("MNGCOM") ) {
			return 3;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return 4;
		}
		if( strFieldName.equals("MAKETIME") ) {
			return 5;
		}
		if( strFieldName.equals("MODIFYDATE") ) {
			return 6;
		}
		if( strFieldName.equals("MODIFYTIME") ) {
			return 7;
		}
		if( strFieldName.equals("REMARK") ) {
			return 8;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CASENO";
				break;
			case 1:
				strFieldName = "RGTSTATE";
				break;
			case 2:
				strFieldName = "RGTTYPE";
				break;
			case 3:
				strFieldName = "MNGCOM";
				break;
			case 4:
				strFieldName = "MAKEDATE";
				break;
			case 5:
				strFieldName = "MAKETIME";
				break;
			case 6:
				strFieldName = "MODIFYDATE";
				break;
			case 7:
				strFieldName = "MODIFYTIME";
				break;
			case 8:
				strFieldName = "REMARK";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CASENO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RGTSTATE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RGTTYPE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MNGCOM") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MAKETIME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MODIFYDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MODIFYTIME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("REMARK") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
