/*
 * <p>ClassName: LFDBConfigSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LFDBConfigDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFDBConfigSchema implements Schema
{
    // @Field
    /** 数据库服务代码 */
    private String DBServiceCode;
    /** 数据库连接序号 */
    private int DBServiceIndex;
    /** 数据库服务分类 */
    private String DBServiceKind;
    /** 数据库服务名称 */
    private String DBName;
    /** 数据库服务类别 */
    private String DBType;
    /** 数据库机器ip */
    private String IP;
    /** 数据库服务端口 */
    private String Port;
    /** 访问数据库服务用户名 */
    private String UserName;
    /** 访问数据库服务密码 */
    private String Password;
    /** 备注 */
    private String Noti;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFDBConfigSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getDBServiceCode()
    {
        if (DBServiceCode != null && !DBServiceCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DBServiceCode = StrTool.unicodeToGBK(DBServiceCode);
        }
        return DBServiceCode;
    }

    public void setDBServiceCode(String aDBServiceCode)
    {
        DBServiceCode = aDBServiceCode;
    }

    public int getDBServiceIndex()
    {
        return DBServiceIndex;
    }

    public void setDBServiceIndex(int aDBServiceIndex)
    {
        DBServiceIndex = aDBServiceIndex;
    }

    public void setDBServiceIndex(String aDBServiceIndex)
    {
        if (aDBServiceIndex != null && !aDBServiceIndex.equals(""))
        {
            Integer tInteger = new Integer(aDBServiceIndex);
            int i = tInteger.intValue();
            DBServiceIndex = i;
        }
    }

    public String getDBServiceKind()
    {
        if (DBServiceKind != null && !DBServiceKind.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DBServiceKind = StrTool.unicodeToGBK(DBServiceKind);
        }
        return DBServiceKind;
    }

    public void setDBServiceKind(String aDBServiceKind)
    {
        DBServiceKind = aDBServiceKind;
    }

    public String getDBName()
    {
        if (DBName != null && !DBName.equals("") && SysConst.CHANGECHARSET == true)
        {
            DBName = StrTool.unicodeToGBK(DBName);
        }
        return DBName;
    }

    public void setDBName(String aDBName)
    {
        DBName = aDBName;
    }

    public String getDBType()
    {
        if (DBType != null && !DBType.equals("") && SysConst.CHANGECHARSET == true)
        {
            DBType = StrTool.unicodeToGBK(DBType);
        }
        return DBType;
    }

    public void setDBType(String aDBType)
    {
        DBType = aDBType;
    }

    public String getIP()
    {
        if (IP != null && !IP.equals("") && SysConst.CHANGECHARSET == true)
        {
            IP = StrTool.unicodeToGBK(IP);
        }
        return IP;
    }

    public void setIP(String aIP)
    {
        IP = aIP;
    }

    public String getPort()
    {
        if (Port != null && !Port.equals("") && SysConst.CHANGECHARSET == true)
        {
            Port = StrTool.unicodeToGBK(Port);
        }
        return Port;
    }

    public void setPort(String aPort)
    {
        Port = aPort;
    }

    public String getUserName()
    {
        if (UserName != null && !UserName.equals("") && SysConst.CHANGECHARSET == true)
        {
            UserName = StrTool.unicodeToGBK(UserName);
        }
        return UserName;
    }

    public void setUserName(String aUserName)
    {
        UserName = aUserName;
    }

    public String getPassword()
    {
        if (Password != null && !Password.equals("") && SysConst.CHANGECHARSET == true)
        {
            Password = StrTool.unicodeToGBK(Password);
        }
        return Password;
    }

    public void setPassword(String aPassword)
    {
        Password = aPassword;
    }

    public String getNoti()
    {
        if (Noti != null && !Noti.equals("") && SysConst.CHANGECHARSET == true)
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    /**
     * 使用另外一个 LFDBConfigSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFDBConfigSchema aLFDBConfigSchema)
    {
        this.DBServiceCode = aLFDBConfigSchema.getDBServiceCode();
        this.DBServiceIndex = aLFDBConfigSchema.getDBServiceIndex();
        this.DBServiceKind = aLFDBConfigSchema.getDBServiceKind();
        this.DBName = aLFDBConfigSchema.getDBName();
        this.DBType = aLFDBConfigSchema.getDBType();
        this.IP = aLFDBConfigSchema.getIP();
        this.Port = aLFDBConfigSchema.getPort();
        this.UserName = aLFDBConfigSchema.getUserName();
        this.Password = aLFDBConfigSchema.getPassword();
        this.Noti = aLFDBConfigSchema.getNoti();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("DBServiceCode") == null)
            {
                this.DBServiceCode = null;
            }
            else
            {
                this.DBServiceCode = rs.getString("DBServiceCode").trim();
            }

            this.DBServiceIndex = rs.getInt("DBServiceIndex");
            if (rs.getString("DBServiceKind") == null)
            {
                this.DBServiceKind = null;
            }
            else
            {
                this.DBServiceKind = rs.getString("DBServiceKind").trim();
            }

            if (rs.getString("DBName") == null)
            {
                this.DBName = null;
            }
            else
            {
                this.DBName = rs.getString("DBName").trim();
            }

            if (rs.getString("DBType") == null)
            {
                this.DBType = null;
            }
            else
            {
                this.DBType = rs.getString("DBType").trim();
            }

            if (rs.getString("IP") == null)
            {
                this.IP = null;
            }
            else
            {
                this.IP = rs.getString("IP").trim();
            }

            if (rs.getString("Port") == null)
            {
                this.Port = null;
            }
            else
            {
                this.Port = rs.getString("Port").trim();
            }

            if (rs.getString("UserName") == null)
            {
                this.UserName = null;
            }
            else
            {
                this.UserName = rs.getString("UserName").trim();
            }

            if (rs.getString("Password") == null)
            {
                this.Password = null;
            }
            else
            {
                this.Password = rs.getString("Password").trim();
            }

            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFDBConfigSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFDBConfigSchema getSchema()
    {
        LFDBConfigSchema aLFDBConfigSchema = new LFDBConfigSchema();
        aLFDBConfigSchema.setSchema(this);
        return aLFDBConfigSchema;
    }

    public LFDBConfigDB getDB()
    {
        LFDBConfigDB aDBOper = new LFDBConfigDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFDBConfig描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(DBServiceCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(DBServiceIndex) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DBServiceKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DBName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DBType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IP)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Port)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UserName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Password)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Noti));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFDBConfig>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            DBServiceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            DBServiceIndex = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).intValue();
            DBServiceKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            DBName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            DBType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            IP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                SysConst.PACKAGESPILTER);
            Port = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            UserName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                  SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFDBConfigSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("DBServiceCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DBServiceCode));
        }
        if (FCode.equals("DBServiceIndex"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DBServiceIndex));
        }
        if (FCode.equals("DBServiceKind"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DBServiceKind));
        }
        if (FCode.equals("DBName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DBName));
        }
        if (FCode.equals("DBType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DBType));
        }
        if (FCode.equals("IP"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(IP));
        }
        if (FCode.equals("Port"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Port));
        }
        if (FCode.equals("UserName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UserName));
        }
        if (FCode.equals("Password"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Password));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Noti));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(DBServiceCode);
                break;
            case 1:
                strFieldValue = String.valueOf(DBServiceIndex);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(DBServiceKind);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(DBName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(DBType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(IP);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Port);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(UserName);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Password);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("DBServiceCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DBServiceCode = FValue.trim();
            }
            else
            {
                DBServiceCode = null;
            }
        }
        if (FCode.equals("DBServiceIndex"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DBServiceIndex = i;
            }
        }
        if (FCode.equals("DBServiceKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DBServiceKind = FValue.trim();
            }
            else
            {
                DBServiceKind = null;
            }
        }
        if (FCode.equals("DBName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DBName = FValue.trim();
            }
            else
            {
                DBName = null;
            }
        }
        if (FCode.equals("DBType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DBType = FValue.trim();
            }
            else
            {
                DBType = null;
            }
        }
        if (FCode.equals("IP"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IP = FValue.trim();
            }
            else
            {
                IP = null;
            }
        }
        if (FCode.equals("Port"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Port = FValue.trim();
            }
            else
            {
                Port = null;
            }
        }
        if (FCode.equals("UserName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UserName = FValue.trim();
            }
            else
            {
                UserName = null;
            }
        }
        if (FCode.equals("Password"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
            {
                Password = null;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFDBConfigSchema other = (LFDBConfigSchema) otherObject;
        return
                DBServiceCode.equals(other.getDBServiceCode())
                && DBServiceIndex == other.getDBServiceIndex()
                && DBServiceKind.equals(other.getDBServiceKind())
                && DBName.equals(other.getDBName())
                && DBType.equals(other.getDBType())
                && IP.equals(other.getIP())
                && Port.equals(other.getPort())
                && UserName.equals(other.getUserName())
                && Password.equals(other.getPassword())
                && Noti.equals(other.getNoti());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("DBServiceCode"))
        {
            return 0;
        }
        if (strFieldName.equals("DBServiceIndex"))
        {
            return 1;
        }
        if (strFieldName.equals("DBServiceKind"))
        {
            return 2;
        }
        if (strFieldName.equals("DBName"))
        {
            return 3;
        }
        if (strFieldName.equals("DBType"))
        {
            return 4;
        }
        if (strFieldName.equals("IP"))
        {
            return 5;
        }
        if (strFieldName.equals("Port"))
        {
            return 6;
        }
        if (strFieldName.equals("UserName"))
        {
            return 7;
        }
        if (strFieldName.equals("Password"))
        {
            return 8;
        }
        if (strFieldName.equals("Noti"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "DBServiceCode";
                break;
            case 1:
                strFieldName = "DBServiceIndex";
                break;
            case 2:
                strFieldName = "DBServiceKind";
                break;
            case 3:
                strFieldName = "DBName";
                break;
            case 4:
                strFieldName = "DBType";
                break;
            case 5:
                strFieldName = "IP";
                break;
            case 6:
                strFieldName = "Port";
                break;
            case 7:
                strFieldName = "UserName";
                break;
            case 8:
                strFieldName = "Password";
                break;
            case 9:
                strFieldName = "Noti";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("DBServiceCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DBServiceIndex"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DBServiceKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DBName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DBType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IP"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Port"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UserName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Password"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
