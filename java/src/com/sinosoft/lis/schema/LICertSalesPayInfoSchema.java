/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LICertSalesPayInfoDB;

/*
 * <p>ClassName: LICertSalesPayInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 电子商务复杂产品
 * @CreateDate：2011-05-10
 */
public class LICertSalesPayInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 交易号 */
	private String PayTradeSeq;
	/** 管理机构 */
	private String ManageCom;
	/** 销售渠道 */
	private String SaleChnl;
	/** 销售渠道名称 */
	private String SaleChnlName;
	/** 中介机构 */
	private String AgentCom;
	/** 业务员代码 */
	private String AgentCode;
	/** 业务员姓名 */
	private String AgentName;
	/** 保险卡类型 */
	private String CertifyCode;
	/** 保险卡名称 */
	private String CertifyName;
	/** 套餐代码 */
	private String WrapCode;
	/** 套餐名称 */
	private String WrapName;
	/** 销售数量 */
	private String SalesCount;
	/** 销售时间 */
	private String SalesDate;
	/** 缴费方式 */
	private String PayMode;
	/** 缴费支付平台类型 */
	private String PayChnlType;
	/** 缴费支付平台名称 */
	private String PayChnlName;
	/** 缴费总金额 */
	private String PayMoney;
	/** 支付时间 */
	private String PayDate;
	/** 到帐时间 */
	private String PayEntAccDate;
	/** 到帐确认时间 */
	private String PayConfAccDate;
	/** 收费银行编码 */
	private String DescBankCode;
	/** 收费银行名称 */
	private String DescBankName;
	/** 收费帐户编码 */
	private String DescBankAccCode;
	/** 收费帐户名称 */
	private String DescBankAccName;
	/** 说明（备注） */
	private String Remark;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** Relainfolist */
	private String RelaInfoList;
	/** 缴费信息类型 */
	private String PayInfoType;
	/** 缴费状态 */
	private String PayState;

	public static final int FIELDNUM = 34;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LICertSalesPayInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BatchNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LICertSalesPayInfoSchema cloned = (LICertSalesPayInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getPayTradeSeq()
	{
		return PayTradeSeq;
	}
	public void setPayTradeSeq(String aPayTradeSeq)
	{
		PayTradeSeq = aPayTradeSeq;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getSaleChnlName()
	{
		return SaleChnlName;
	}
	public void setSaleChnlName(String aSaleChnlName)
	{
		SaleChnlName = aSaleChnlName;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentName()
	{
		return AgentName;
	}
	public void setAgentName(String aAgentName)
	{
		AgentName = aAgentName;
	}
	public String getCertifyCode()
	{
		return CertifyCode;
	}
	public void setCertifyCode(String aCertifyCode)
	{
		CertifyCode = aCertifyCode;
	}
	public String getCertifyName()
	{
		return CertifyName;
	}
	public void setCertifyName(String aCertifyName)
	{
		CertifyName = aCertifyName;
	}
	public String getWrapCode()
	{
		return WrapCode;
	}
	public void setWrapCode(String aWrapCode)
	{
		WrapCode = aWrapCode;
	}
	public String getWrapName()
	{
		return WrapName;
	}
	public void setWrapName(String aWrapName)
	{
		WrapName = aWrapName;
	}
	public String getSalesCount()
	{
		return SalesCount;
	}
	public void setSalesCount(String aSalesCount)
	{
		SalesCount = aSalesCount;
	}
	public String getSalesDate()
	{
		return SalesDate;
	}
	public void setSalesDate(String aSalesDate)
	{
		SalesDate = aSalesDate;
	}
	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getPayChnlType()
	{
		return PayChnlType;
	}
	public void setPayChnlType(String aPayChnlType)
	{
		PayChnlType = aPayChnlType;
	}
	public String getPayChnlName()
	{
		return PayChnlName;
	}
	public void setPayChnlName(String aPayChnlName)
	{
		PayChnlName = aPayChnlName;
	}
	public String getPayMoney()
	{
		return PayMoney;
	}
	public void setPayMoney(String aPayMoney)
	{
		PayMoney = aPayMoney;
	}
	public String getPayDate()
	{
		return PayDate;
	}
	public void setPayDate(String aPayDate)
	{
		PayDate = aPayDate;
	}
	public String getPayEntAccDate()
	{
		return PayEntAccDate;
	}
	public void setPayEntAccDate(String aPayEntAccDate)
	{
		PayEntAccDate = aPayEntAccDate;
	}
	public String getPayConfAccDate()
	{
		return PayConfAccDate;
	}
	public void setPayConfAccDate(String aPayConfAccDate)
	{
		PayConfAccDate = aPayConfAccDate;
	}
	public String getDescBankCode()
	{
		return DescBankCode;
	}
	public void setDescBankCode(String aDescBankCode)
	{
		DescBankCode = aDescBankCode;
	}
	public String getDescBankName()
	{
		return DescBankName;
	}
	public void setDescBankName(String aDescBankName)
	{
		DescBankName = aDescBankName;
	}
	public String getDescBankAccCode()
	{
		return DescBankAccCode;
	}
	public void setDescBankAccCode(String aDescBankAccCode)
	{
		DescBankAccCode = aDescBankAccCode;
	}
	public String getDescBankAccName()
	{
		return DescBankAccName;
	}
	public void setDescBankAccName(String aDescBankAccName)
	{
		DescBankAccName = aDescBankAccName;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getRelaInfoList()
	{
		return RelaInfoList;
	}
	public void setRelaInfoList(String aRelaInfoList)
	{
		RelaInfoList = aRelaInfoList;
	}
	public String getPayInfoType()
	{
		return PayInfoType;
	}
	public void setPayInfoType(String aPayInfoType)
	{
		PayInfoType = aPayInfoType;
	}
	public String getPayState()
	{
		return PayState;
	}
	public void setPayState(String aPayState)
	{
		PayState = aPayState;
	}

	/**
	* 使用另外一个 LICertSalesPayInfoSchema 对象给 Schema 赋值
	* @param: aLICertSalesPayInfoSchema LICertSalesPayInfoSchema
	**/
	public void setSchema(LICertSalesPayInfoSchema aLICertSalesPayInfoSchema)
	{
		this.BatchNo = aLICertSalesPayInfoSchema.getBatchNo();
		this.PayTradeSeq = aLICertSalesPayInfoSchema.getPayTradeSeq();
		this.ManageCom = aLICertSalesPayInfoSchema.getManageCom();
		this.SaleChnl = aLICertSalesPayInfoSchema.getSaleChnl();
		this.SaleChnlName = aLICertSalesPayInfoSchema.getSaleChnlName();
		this.AgentCom = aLICertSalesPayInfoSchema.getAgentCom();
		this.AgentCode = aLICertSalesPayInfoSchema.getAgentCode();
		this.AgentName = aLICertSalesPayInfoSchema.getAgentName();
		this.CertifyCode = aLICertSalesPayInfoSchema.getCertifyCode();
		this.CertifyName = aLICertSalesPayInfoSchema.getCertifyName();
		this.WrapCode = aLICertSalesPayInfoSchema.getWrapCode();
		this.WrapName = aLICertSalesPayInfoSchema.getWrapName();
		this.SalesCount = aLICertSalesPayInfoSchema.getSalesCount();
		this.SalesDate = aLICertSalesPayInfoSchema.getSalesDate();
		this.PayMode = aLICertSalesPayInfoSchema.getPayMode();
		this.PayChnlType = aLICertSalesPayInfoSchema.getPayChnlType();
		this.PayChnlName = aLICertSalesPayInfoSchema.getPayChnlName();
		this.PayMoney = aLICertSalesPayInfoSchema.getPayMoney();
		this.PayDate = aLICertSalesPayInfoSchema.getPayDate();
		this.PayEntAccDate = aLICertSalesPayInfoSchema.getPayEntAccDate();
		this.PayConfAccDate = aLICertSalesPayInfoSchema.getPayConfAccDate();
		this.DescBankCode = aLICertSalesPayInfoSchema.getDescBankCode();
		this.DescBankName = aLICertSalesPayInfoSchema.getDescBankName();
		this.DescBankAccCode = aLICertSalesPayInfoSchema.getDescBankAccCode();
		this.DescBankAccName = aLICertSalesPayInfoSchema.getDescBankAccName();
		this.Remark = aLICertSalesPayInfoSchema.getRemark();
		this.Operator = aLICertSalesPayInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLICertSalesPayInfoSchema.getMakeDate());
		this.MakeTime = aLICertSalesPayInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLICertSalesPayInfoSchema.getModifyDate());
		this.ModifyTime = aLICertSalesPayInfoSchema.getModifyTime();
		this.RelaInfoList = aLICertSalesPayInfoSchema.getRelaInfoList();
		this.PayInfoType = aLICertSalesPayInfoSchema.getPayInfoType();
		this.PayState = aLICertSalesPayInfoSchema.getPayState();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("PayTradeSeq") == null )
				this.PayTradeSeq = null;
			else
				this.PayTradeSeq = rs.getString("PayTradeSeq").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("SaleChnlName") == null )
				this.SaleChnlName = null;
			else
				this.SaleChnlName = rs.getString("SaleChnlName").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentName") == null )
				this.AgentName = null;
			else
				this.AgentName = rs.getString("AgentName").trim();

			if( rs.getString("CertifyCode") == null )
				this.CertifyCode = null;
			else
				this.CertifyCode = rs.getString("CertifyCode").trim();

			if( rs.getString("CertifyName") == null )
				this.CertifyName = null;
			else
				this.CertifyName = rs.getString("CertifyName").trim();

			if( rs.getString("WrapCode") == null )
				this.WrapCode = null;
			else
				this.WrapCode = rs.getString("WrapCode").trim();

			if( rs.getString("WrapName") == null )
				this.WrapName = null;
			else
				this.WrapName = rs.getString("WrapName").trim();

			if( rs.getString("SalesCount") == null )
				this.SalesCount = null;
			else
				this.SalesCount = rs.getString("SalesCount").trim();

			if( rs.getString("SalesDate") == null )
				this.SalesDate = null;
			else
				this.SalesDate = rs.getString("SalesDate").trim();

			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("PayChnlType") == null )
				this.PayChnlType = null;
			else
				this.PayChnlType = rs.getString("PayChnlType").trim();

			if( rs.getString("PayChnlName") == null )
				this.PayChnlName = null;
			else
				this.PayChnlName = rs.getString("PayChnlName").trim();

			if( rs.getString("PayMoney") == null )
				this.PayMoney = null;
			else
				this.PayMoney = rs.getString("PayMoney").trim();

			if( rs.getString("PayDate") == null )
				this.PayDate = null;
			else
				this.PayDate = rs.getString("PayDate").trim();

			if( rs.getString("PayEntAccDate") == null )
				this.PayEntAccDate = null;
			else
				this.PayEntAccDate = rs.getString("PayEntAccDate").trim();

			if( rs.getString("PayConfAccDate") == null )
				this.PayConfAccDate = null;
			else
				this.PayConfAccDate = rs.getString("PayConfAccDate").trim();

			if( rs.getString("DescBankCode") == null )
				this.DescBankCode = null;
			else
				this.DescBankCode = rs.getString("DescBankCode").trim();

			if( rs.getString("DescBankName") == null )
				this.DescBankName = null;
			else
				this.DescBankName = rs.getString("DescBankName").trim();

			if( rs.getString("DescBankAccCode") == null )
				this.DescBankAccCode = null;
			else
				this.DescBankAccCode = rs.getString("DescBankAccCode").trim();

			if( rs.getString("DescBankAccName") == null )
				this.DescBankAccName = null;
			else
				this.DescBankAccName = rs.getString("DescBankAccName").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("RelaInfoList") == null )
				this.RelaInfoList = null;
			else
				this.RelaInfoList = rs.getString("RelaInfoList").trim();

			if( rs.getString("PayInfoType") == null )
				this.PayInfoType = null;
			else
				this.PayInfoType = rs.getString("PayInfoType").trim();

			if( rs.getString("PayState") == null )
				this.PayState = null;
			else
				this.PayState = rs.getString("PayState").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LICertSalesPayInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LICertSalesPayInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LICertSalesPayInfoSchema getSchema()
	{
		LICertSalesPayInfoSchema aLICertSalesPayInfoSchema = new LICertSalesPayInfoSchema();
		aLICertSalesPayInfoSchema.setSchema(this);
		return aLICertSalesPayInfoSchema;
	}

	public LICertSalesPayInfoDB getDB()
	{
		LICertSalesPayInfoDB aDBOper = new LICertSalesPayInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLICertSalesPayInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayTradeSeq)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnlName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WrapCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WrapName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SalesCount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SalesDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayChnlType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayChnlName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMoney)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayEntAccDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayConfAccDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DescBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DescBankName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DescBankAccCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DescBankAccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelaInfoList)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayInfoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayState));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLICertSalesPayInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PayTradeSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SaleChnlName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CertifyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			WrapCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			WrapName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			SalesCount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			SalesDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			PayChnlType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			PayChnlName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			PayMoney = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			PayDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			PayEntAccDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PayConfAccDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			DescBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			DescBankName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			DescBankAccCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			DescBankAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			RelaInfoList = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			PayInfoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			PayState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LICertSalesPayInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("PayTradeSeq"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayTradeSeq));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("SaleChnlName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnlName));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
		}
		if (FCode.equals("CertifyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
		}
		if (FCode.equals("CertifyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyName));
		}
		if (FCode.equals("WrapCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrapCode));
		}
		if (FCode.equals("WrapName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WrapName));
		}
		if (FCode.equals("SalesCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SalesCount));
		}
		if (FCode.equals("SalesDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SalesDate));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("PayChnlType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayChnlType));
		}
		if (FCode.equals("PayChnlName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayChnlName));
		}
		if (FCode.equals("PayMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
		}
		if (FCode.equals("PayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayDate));
		}
		if (FCode.equals("PayEntAccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayEntAccDate));
		}
		if (FCode.equals("PayConfAccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayConfAccDate));
		}
		if (FCode.equals("DescBankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DescBankCode));
		}
		if (FCode.equals("DescBankName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DescBankName));
		}
		if (FCode.equals("DescBankAccCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DescBankAccCode));
		}
		if (FCode.equals("DescBankAccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DescBankAccName));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("RelaInfoList"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaInfoList));
		}
		if (FCode.equals("PayInfoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayInfoType));
		}
		if (FCode.equals("PayState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayState));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PayTradeSeq);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SaleChnlName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AgentName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CertifyCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CertifyName);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(WrapCode);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(WrapName);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(SalesCount);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(SalesDate);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(PayChnlType);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(PayChnlName);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(PayMoney);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(PayDate);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(PayEntAccDate);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(PayConfAccDate);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(DescBankCode);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(DescBankName);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(DescBankAccCode);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(DescBankAccName);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(RelaInfoList);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(PayInfoType);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(PayState);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("PayTradeSeq"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayTradeSeq = FValue.trim();
			}
			else
				PayTradeSeq = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnlName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnlName = FValue.trim();
			}
			else
				SaleChnlName = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentName = FValue.trim();
			}
			else
				AgentName = null;
		}
		if (FCode.equalsIgnoreCase("CertifyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyCode = FValue.trim();
			}
			else
				CertifyCode = null;
		}
		if (FCode.equalsIgnoreCase("CertifyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyName = FValue.trim();
			}
			else
				CertifyName = null;
		}
		if (FCode.equalsIgnoreCase("WrapCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WrapCode = FValue.trim();
			}
			else
				WrapCode = null;
		}
		if (FCode.equalsIgnoreCase("WrapName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WrapName = FValue.trim();
			}
			else
				WrapName = null;
		}
		if (FCode.equalsIgnoreCase("SalesCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SalesCount = FValue.trim();
			}
			else
				SalesCount = null;
		}
		if (FCode.equalsIgnoreCase("SalesDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SalesDate = FValue.trim();
			}
			else
				SalesDate = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("PayChnlType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayChnlType = FValue.trim();
			}
			else
				PayChnlType = null;
		}
		if (FCode.equalsIgnoreCase("PayChnlName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayChnlName = FValue.trim();
			}
			else
				PayChnlName = null;
		}
		if (FCode.equalsIgnoreCase("PayMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMoney = FValue.trim();
			}
			else
				PayMoney = null;
		}
		if (FCode.equalsIgnoreCase("PayDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayDate = FValue.trim();
			}
			else
				PayDate = null;
		}
		if (FCode.equalsIgnoreCase("PayEntAccDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayEntAccDate = FValue.trim();
			}
			else
				PayEntAccDate = null;
		}
		if (FCode.equalsIgnoreCase("PayConfAccDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayConfAccDate = FValue.trim();
			}
			else
				PayConfAccDate = null;
		}
		if (FCode.equalsIgnoreCase("DescBankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DescBankCode = FValue.trim();
			}
			else
				DescBankCode = null;
		}
		if (FCode.equalsIgnoreCase("DescBankName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DescBankName = FValue.trim();
			}
			else
				DescBankName = null;
		}
		if (FCode.equalsIgnoreCase("DescBankAccCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DescBankAccCode = FValue.trim();
			}
			else
				DescBankAccCode = null;
		}
		if (FCode.equalsIgnoreCase("DescBankAccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DescBankAccName = FValue.trim();
			}
			else
				DescBankAccName = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("RelaInfoList"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelaInfoList = FValue.trim();
			}
			else
				RelaInfoList = null;
		}
		if (FCode.equalsIgnoreCase("PayInfoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayInfoType = FValue.trim();
			}
			else
				PayInfoType = null;
		}
		if (FCode.equalsIgnoreCase("PayState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayState = FValue.trim();
			}
			else
				PayState = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LICertSalesPayInfoSchema other = (LICertSalesPayInfoSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (PayTradeSeq == null ? other.getPayTradeSeq() == null : PayTradeSeq.equals(other.getPayTradeSeq()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (SaleChnlName == null ? other.getSaleChnlName() == null : SaleChnlName.equals(other.getSaleChnlName()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentName == null ? other.getAgentName() == null : AgentName.equals(other.getAgentName()))
			&& (CertifyCode == null ? other.getCertifyCode() == null : CertifyCode.equals(other.getCertifyCode()))
			&& (CertifyName == null ? other.getCertifyName() == null : CertifyName.equals(other.getCertifyName()))
			&& (WrapCode == null ? other.getWrapCode() == null : WrapCode.equals(other.getWrapCode()))
			&& (WrapName == null ? other.getWrapName() == null : WrapName.equals(other.getWrapName()))
			&& (SalesCount == null ? other.getSalesCount() == null : SalesCount.equals(other.getSalesCount()))
			&& (SalesDate == null ? other.getSalesDate() == null : SalesDate.equals(other.getSalesDate()))
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (PayChnlType == null ? other.getPayChnlType() == null : PayChnlType.equals(other.getPayChnlType()))
			&& (PayChnlName == null ? other.getPayChnlName() == null : PayChnlName.equals(other.getPayChnlName()))
			&& (PayMoney == null ? other.getPayMoney() == null : PayMoney.equals(other.getPayMoney()))
			&& (PayDate == null ? other.getPayDate() == null : PayDate.equals(other.getPayDate()))
			&& (PayEntAccDate == null ? other.getPayEntAccDate() == null : PayEntAccDate.equals(other.getPayEntAccDate()))
			&& (PayConfAccDate == null ? other.getPayConfAccDate() == null : PayConfAccDate.equals(other.getPayConfAccDate()))
			&& (DescBankCode == null ? other.getDescBankCode() == null : DescBankCode.equals(other.getDescBankCode()))
			&& (DescBankName == null ? other.getDescBankName() == null : DescBankName.equals(other.getDescBankName()))
			&& (DescBankAccCode == null ? other.getDescBankAccCode() == null : DescBankAccCode.equals(other.getDescBankAccCode()))
			&& (DescBankAccName == null ? other.getDescBankAccName() == null : DescBankAccName.equals(other.getDescBankAccName()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (RelaInfoList == null ? other.getRelaInfoList() == null : RelaInfoList.equals(other.getRelaInfoList()))
			&& (PayInfoType == null ? other.getPayInfoType() == null : PayInfoType.equals(other.getPayInfoType()))
			&& (PayState == null ? other.getPayState() == null : PayState.equals(other.getPayState()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("PayTradeSeq") ) {
			return 1;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 3;
		}
		if( strFieldName.equals("SaleChnlName") ) {
			return 4;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 5;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 6;
		}
		if( strFieldName.equals("AgentName") ) {
			return 7;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return 8;
		}
		if( strFieldName.equals("CertifyName") ) {
			return 9;
		}
		if( strFieldName.equals("WrapCode") ) {
			return 10;
		}
		if( strFieldName.equals("WrapName") ) {
			return 11;
		}
		if( strFieldName.equals("SalesCount") ) {
			return 12;
		}
		if( strFieldName.equals("SalesDate") ) {
			return 13;
		}
		if( strFieldName.equals("PayMode") ) {
			return 14;
		}
		if( strFieldName.equals("PayChnlType") ) {
			return 15;
		}
		if( strFieldName.equals("PayChnlName") ) {
			return 16;
		}
		if( strFieldName.equals("PayMoney") ) {
			return 17;
		}
		if( strFieldName.equals("PayDate") ) {
			return 18;
		}
		if( strFieldName.equals("PayEntAccDate") ) {
			return 19;
		}
		if( strFieldName.equals("PayConfAccDate") ) {
			return 20;
		}
		if( strFieldName.equals("DescBankCode") ) {
			return 21;
		}
		if( strFieldName.equals("DescBankName") ) {
			return 22;
		}
		if( strFieldName.equals("DescBankAccCode") ) {
			return 23;
		}
		if( strFieldName.equals("DescBankAccName") ) {
			return 24;
		}
		if( strFieldName.equals("Remark") ) {
			return 25;
		}
		if( strFieldName.equals("Operator") ) {
			return 26;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 27;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 28;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 29;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 30;
		}
		if( strFieldName.equals("RelaInfoList") ) {
			return 31;
		}
		if( strFieldName.equals("PayInfoType") ) {
			return 32;
		}
		if( strFieldName.equals("PayState") ) {
			return 33;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "PayTradeSeq";
				break;
			case 2:
				strFieldName = "ManageCom";
				break;
			case 3:
				strFieldName = "SaleChnl";
				break;
			case 4:
				strFieldName = "SaleChnlName";
				break;
			case 5:
				strFieldName = "AgentCom";
				break;
			case 6:
				strFieldName = "AgentCode";
				break;
			case 7:
				strFieldName = "AgentName";
				break;
			case 8:
				strFieldName = "CertifyCode";
				break;
			case 9:
				strFieldName = "CertifyName";
				break;
			case 10:
				strFieldName = "WrapCode";
				break;
			case 11:
				strFieldName = "WrapName";
				break;
			case 12:
				strFieldName = "SalesCount";
				break;
			case 13:
				strFieldName = "SalesDate";
				break;
			case 14:
				strFieldName = "PayMode";
				break;
			case 15:
				strFieldName = "PayChnlType";
				break;
			case 16:
				strFieldName = "PayChnlName";
				break;
			case 17:
				strFieldName = "PayMoney";
				break;
			case 18:
				strFieldName = "PayDate";
				break;
			case 19:
				strFieldName = "PayEntAccDate";
				break;
			case 20:
				strFieldName = "PayConfAccDate";
				break;
			case 21:
				strFieldName = "DescBankCode";
				break;
			case 22:
				strFieldName = "DescBankName";
				break;
			case 23:
				strFieldName = "DescBankAccCode";
				break;
			case 24:
				strFieldName = "DescBankAccName";
				break;
			case 25:
				strFieldName = "Remark";
				break;
			case 26:
				strFieldName = "Operator";
				break;
			case 27:
				strFieldName = "MakeDate";
				break;
			case 28:
				strFieldName = "MakeTime";
				break;
			case 29:
				strFieldName = "ModifyDate";
				break;
			case 30:
				strFieldName = "ModifyTime";
				break;
			case 31:
				strFieldName = "RelaInfoList";
				break;
			case 32:
				strFieldName = "PayInfoType";
				break;
			case 33:
				strFieldName = "PayState";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayTradeSeq") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnlName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WrapCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WrapName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SalesCount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SalesDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayChnlType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayChnlName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMoney") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayEntAccDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayConfAccDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DescBankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DescBankName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DescBankAccCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DescBankAccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelaInfoList") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayInfoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayState") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
