/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIDataExtractDefDB;

/*
 * <p>ClassName: FIDataExtractDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIDataExtractDefSchema implements Schema, Cloneable
{
	// @Field
	/** 版本编号 */
	private String VersionNo;
	/** 规则编码 */
	private String RuleDefID;
	/** 规则名称 */
	private String RuleDefName;
	/** 规则类型 */
	private String RuleType;
	/** 业务索引类型 */
	private String IndexCode;
	/** 索引存储字段 */
	private String IndexColumn;
	/** 执行顺序 */
	private int DealOrder;
	/** 规则描述 */
	private String Remark;
	/** 规则状态 */
	private String RuleState;

	public static final int FIELDNUM = 9;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIDataExtractDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "VersionNo";
		pk[1] = "RuleDefID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIDataExtractDefSchema cloned = (FIDataExtractDefSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getRuleDefID()
	{
		return RuleDefID;
	}
	public void setRuleDefID(String aRuleDefID)
	{
		RuleDefID = aRuleDefID;
	}
	public String getRuleDefName()
	{
		return RuleDefName;
	}
	public void setRuleDefName(String aRuleDefName)
	{
		RuleDefName = aRuleDefName;
	}
	public String getRuleType()
	{
		return RuleType;
	}
	public void setRuleType(String aRuleType)
	{
		RuleType = aRuleType;
	}
	public String getIndexCode()
	{
		return IndexCode;
	}
	public void setIndexCode(String aIndexCode)
	{
		IndexCode = aIndexCode;
	}
	public String getIndexColumn()
	{
		return IndexColumn;
	}
	public void setIndexColumn(String aIndexColumn)
	{
		IndexColumn = aIndexColumn;
	}
	public int getDealOrder()
	{
		return DealOrder;
	}
	public void setDealOrder(int aDealOrder)
	{
		DealOrder = aDealOrder;
	}
	public void setDealOrder(String aDealOrder)
	{
		if (aDealOrder != null && !aDealOrder.equals(""))
		{
			Integer tInteger = new Integer(aDealOrder);
			int i = tInteger.intValue();
			DealOrder = i;
		}
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getRuleState()
	{
		return RuleState;
	}
	public void setRuleState(String aRuleState)
	{
		RuleState = aRuleState;
	}

	/**
	* 使用另外一个 FIDataExtractDefSchema 对象给 Schema 赋值
	* @param: aFIDataExtractDefSchema FIDataExtractDefSchema
	**/
	public void setSchema(FIDataExtractDefSchema aFIDataExtractDefSchema)
	{
		this.VersionNo = aFIDataExtractDefSchema.getVersionNo();
		this.RuleDefID = aFIDataExtractDefSchema.getRuleDefID();
		this.RuleDefName = aFIDataExtractDefSchema.getRuleDefName();
		this.RuleType = aFIDataExtractDefSchema.getRuleType();
		this.IndexCode = aFIDataExtractDefSchema.getIndexCode();
		this.IndexColumn = aFIDataExtractDefSchema.getIndexColumn();
		this.DealOrder = aFIDataExtractDefSchema.getDealOrder();
		this.Remark = aFIDataExtractDefSchema.getRemark();
		this.RuleState = aFIDataExtractDefSchema.getRuleState();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("RuleDefID") == null )
				this.RuleDefID = null;
			else
				this.RuleDefID = rs.getString("RuleDefID").trim();

			if( rs.getString("RuleDefName") == null )
				this.RuleDefName = null;
			else
				this.RuleDefName = rs.getString("RuleDefName").trim();

			if( rs.getString("RuleType") == null )
				this.RuleType = null;
			else
				this.RuleType = rs.getString("RuleType").trim();

			if( rs.getString("IndexCode") == null )
				this.IndexCode = null;
			else
				this.IndexCode = rs.getString("IndexCode").trim();

			if( rs.getString("IndexColumn") == null )
				this.IndexColumn = null;
			else
				this.IndexColumn = rs.getString("IndexColumn").trim();

			this.DealOrder = rs.getInt("DealOrder");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("RuleState") == null )
				this.RuleState = null;
			else
				this.RuleState = rs.getString("RuleState").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIDataExtractDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIDataExtractDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIDataExtractDefSchema getSchema()
	{
		FIDataExtractDefSchema aFIDataExtractDefSchema = new FIDataExtractDefSchema();
		aFIDataExtractDefSchema.setSchema(this);
		return aFIDataExtractDefSchema;
	}

	public FIDataExtractDefDB getDB()
	{
		FIDataExtractDefDB aDBOper = new FIDataExtractDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIDataExtractDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleDefID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleDefName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexColumn)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DealOrder));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleState));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIDataExtractDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RuleDefID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RuleDefName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RuleType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IndexColumn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			DealOrder= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).intValue();
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RuleState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIDataExtractDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("RuleDefID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleDefID));
		}
		if (FCode.equals("RuleDefName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleDefName));
		}
		if (FCode.equals("RuleType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleType));
		}
		if (FCode.equals("IndexCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
		}
		if (FCode.equals("IndexColumn"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexColumn));
		}
		if (FCode.equals("DealOrder"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealOrder));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("RuleState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleState));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RuleDefID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RuleDefName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RuleType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(IndexCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IndexColumn);
				break;
			case 6:
				strFieldValue = String.valueOf(DealOrder);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RuleState);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("RuleDefID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleDefID = FValue.trim();
			}
			else
				RuleDefID = null;
		}
		if (FCode.equalsIgnoreCase("RuleDefName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleDefName = FValue.trim();
			}
			else
				RuleDefName = null;
		}
		if (FCode.equalsIgnoreCase("RuleType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleType = FValue.trim();
			}
			else
				RuleType = null;
		}
		if (FCode.equalsIgnoreCase("IndexCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCode = FValue.trim();
			}
			else
				IndexCode = null;
		}
		if (FCode.equalsIgnoreCase("IndexColumn"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexColumn = FValue.trim();
			}
			else
				IndexColumn = null;
		}
		if (FCode.equalsIgnoreCase("DealOrder"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DealOrder = i;
			}
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("RuleState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleState = FValue.trim();
			}
			else
				RuleState = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIDataExtractDefSchema other = (FIDataExtractDefSchema)otherObject;
		return
			(VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (RuleDefID == null ? other.getRuleDefID() == null : RuleDefID.equals(other.getRuleDefID()))
			&& (RuleDefName == null ? other.getRuleDefName() == null : RuleDefName.equals(other.getRuleDefName()))
			&& (RuleType == null ? other.getRuleType() == null : RuleType.equals(other.getRuleType()))
			&& (IndexCode == null ? other.getIndexCode() == null : IndexCode.equals(other.getIndexCode()))
			&& (IndexColumn == null ? other.getIndexColumn() == null : IndexColumn.equals(other.getIndexColumn()))
			&& DealOrder == other.getDealOrder()
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (RuleState == null ? other.getRuleState() == null : RuleState.equals(other.getRuleState()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return 0;
		}
		if( strFieldName.equals("RuleDefID") ) {
			return 1;
		}
		if( strFieldName.equals("RuleDefName") ) {
			return 2;
		}
		if( strFieldName.equals("RuleType") ) {
			return 3;
		}
		if( strFieldName.equals("IndexCode") ) {
			return 4;
		}
		if( strFieldName.equals("IndexColumn") ) {
			return 5;
		}
		if( strFieldName.equals("DealOrder") ) {
			return 6;
		}
		if( strFieldName.equals("Remark") ) {
			return 7;
		}
		if( strFieldName.equals("RuleState") ) {
			return 8;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "VersionNo";
				break;
			case 1:
				strFieldName = "RuleDefID";
				break;
			case 2:
				strFieldName = "RuleDefName";
				break;
			case 3:
				strFieldName = "RuleType";
				break;
			case 4:
				strFieldName = "IndexCode";
				break;
			case 5:
				strFieldName = "IndexColumn";
				break;
			case 6:
				strFieldName = "DealOrder";
				break;
			case 7:
				strFieldName = "Remark";
				break;
			case 8:
				strFieldName = "RuleState";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleDefID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleDefName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexColumn") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealOrder") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleState") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_INT;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
