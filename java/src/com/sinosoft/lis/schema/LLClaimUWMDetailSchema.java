/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLClaimUWMDetailDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LLClaimUWMDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LLClaimUWMDetailSchema implements Schema
{
    // @Field
    /** 核赔次数 */
    private String ClmUWNo;
    /** 赔案号 */
    private String ClmNo;
    /** 立案号 */
    private String RgtNo;
    /** 分案号 */
    private String CaseNo;
    /** 核赔员 */
    private String ClmUWer;
    /** 核赔级别 */
    private String ClmUWGrade;
    /** 核赔结论 */
    private String ClmDecision;
    /** 申请级别 */
    private String AppGrade;
    /** 审核类型 */
    private String CheckType;
    /** 申请审核人员 */
    private String AppClmUWer;
    /** 申请阶段 */
    private String AppPhase;
    /** 申请动作 */
    private String AppActionType;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String MngCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLClaimUWMDetailSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ClmUWNo";
        pk[1] = "ClmNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getClmUWNo()
    {
        if (ClmUWNo != null && !ClmUWNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmUWNo = StrTool.unicodeToGBK(ClmUWNo);
        }
        return ClmUWNo;
    }

    public void setClmUWNo(String aClmUWNo)
    {
        ClmUWNo = aClmUWNo;
    }

    public String getClmNo()
    {
        if (ClmNo != null && !ClmNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmNo = StrTool.unicodeToGBK(ClmNo);
        }
        return ClmNo;
    }

    public void setClmNo(String aClmNo)
    {
        ClmNo = aClmNo;
    }

    public String getRgtNo()
    {
        if (RgtNo != null && !RgtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            RgtNo = StrTool.unicodeToGBK(RgtNo);
        }
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo)
    {
        RgtNo = aRgtNo;
    }

    public String getCaseNo()
    {
        if (CaseNo != null && !CaseNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            CaseNo = StrTool.unicodeToGBK(CaseNo);
        }
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo)
    {
        CaseNo = aCaseNo;
    }

    public String getClmUWer()
    {
        if (ClmUWer != null && !ClmUWer.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmUWer = StrTool.unicodeToGBK(ClmUWer);
        }
        return ClmUWer;
    }

    public void setClmUWer(String aClmUWer)
    {
        ClmUWer = aClmUWer;
    }

    public String getClmUWGrade()
    {
        if (ClmUWGrade != null && !ClmUWGrade.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClmUWGrade = StrTool.unicodeToGBK(ClmUWGrade);
        }
        return ClmUWGrade;
    }

    public void setClmUWGrade(String aClmUWGrade)
    {
        ClmUWGrade = aClmUWGrade;
    }

    public String getClmDecision()
    {
        if (ClmDecision != null && !ClmDecision.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClmDecision = StrTool.unicodeToGBK(ClmDecision);
        }
        return ClmDecision;
    }

    public void setClmDecision(String aClmDecision)
    {
        ClmDecision = aClmDecision;
    }

    public String getAppGrade()
    {
        if (AppGrade != null && !AppGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppGrade = StrTool.unicodeToGBK(AppGrade);
        }
        return AppGrade;
    }

    public void setAppGrade(String aAppGrade)
    {
        AppGrade = aAppGrade;
    }

    public String getCheckType()
    {
        if (CheckType != null && !CheckType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CheckType = StrTool.unicodeToGBK(CheckType);
        }
        return CheckType;
    }

    public void setCheckType(String aCheckType)
    {
        CheckType = aCheckType;
    }

    public String getAppClmUWer()
    {
        if (AppClmUWer != null && !AppClmUWer.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppClmUWer = StrTool.unicodeToGBK(AppClmUWer);
        }
        return AppClmUWer;
    }

    public void setAppClmUWer(String aAppClmUWer)
    {
        AppClmUWer = aAppClmUWer;
    }

    public String getAppPhase()
    {
        if (AppPhase != null && !AppPhase.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppPhase = StrTool.unicodeToGBK(AppPhase);
        }
        return AppPhase;
    }

    public void setAppPhase(String aAppPhase)
    {
        AppPhase = aAppPhase;
    }

    public String getAppActionType()
    {
        if (AppActionType != null && !AppActionType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppActionType = StrTool.unicodeToGBK(AppActionType);
        }
        return AppActionType;
    }

    public void setAppActionType(String aAppActionType)
    {
        AppActionType = aAppActionType;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMngCom()
    {
        if (MngCom != null && !MngCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLClaimUWMDetailSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLClaimUWMDetailSchema aLLClaimUWMDetailSchema)
    {
        this.ClmUWNo = aLLClaimUWMDetailSchema.getClmUWNo();
        this.ClmNo = aLLClaimUWMDetailSchema.getClmNo();
        this.RgtNo = aLLClaimUWMDetailSchema.getRgtNo();
        this.CaseNo = aLLClaimUWMDetailSchema.getCaseNo();
        this.ClmUWer = aLLClaimUWMDetailSchema.getClmUWer();
        this.ClmUWGrade = aLLClaimUWMDetailSchema.getClmUWGrade();
        this.ClmDecision = aLLClaimUWMDetailSchema.getClmDecision();
        this.AppGrade = aLLClaimUWMDetailSchema.getAppGrade();
        this.CheckType = aLLClaimUWMDetailSchema.getCheckType();
        this.AppClmUWer = aLLClaimUWMDetailSchema.getAppClmUWer();
        this.AppPhase = aLLClaimUWMDetailSchema.getAppPhase();
        this.AppActionType = aLLClaimUWMDetailSchema.getAppActionType();
        this.Remark = aLLClaimUWMDetailSchema.getRemark();
        this.Operator = aLLClaimUWMDetailSchema.getOperator();
        this.MngCom = aLLClaimUWMDetailSchema.getMngCom();
        this.MakeDate = fDate.getDate(aLLClaimUWMDetailSchema.getMakeDate());
        this.MakeTime = aLLClaimUWMDetailSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLClaimUWMDetailSchema.getModifyDate());
        this.ModifyTime = aLLClaimUWMDetailSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ClmUWNo") == null)
            {
                this.ClmUWNo = null;
            }
            else
            {
                this.ClmUWNo = rs.getString("ClmUWNo").trim();
            }

            if (rs.getString("ClmNo") == null)
            {
                this.ClmNo = null;
            }
            else
            {
                this.ClmNo = rs.getString("ClmNo").trim();
            }

            if (rs.getString("RgtNo") == null)
            {
                this.RgtNo = null;
            }
            else
            {
                this.RgtNo = rs.getString("RgtNo").trim();
            }

            if (rs.getString("CaseNo") == null)
            {
                this.CaseNo = null;
            }
            else
            {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("ClmUWer") == null)
            {
                this.ClmUWer = null;
            }
            else
            {
                this.ClmUWer = rs.getString("ClmUWer").trim();
            }

            if (rs.getString("ClmUWGrade") == null)
            {
                this.ClmUWGrade = null;
            }
            else
            {
                this.ClmUWGrade = rs.getString("ClmUWGrade").trim();
            }

            if (rs.getString("ClmDecision") == null)
            {
                this.ClmDecision = null;
            }
            else
            {
                this.ClmDecision = rs.getString("ClmDecision").trim();
            }

            if (rs.getString("AppGrade") == null)
            {
                this.AppGrade = null;
            }
            else
            {
                this.AppGrade = rs.getString("AppGrade").trim();
            }

            if (rs.getString("CheckType") == null)
            {
                this.CheckType = null;
            }
            else
            {
                this.CheckType = rs.getString("CheckType").trim();
            }

            if (rs.getString("AppClmUWer") == null)
            {
                this.AppClmUWer = null;
            }
            else
            {
                this.AppClmUWer = rs.getString("AppClmUWer").trim();
            }

            if (rs.getString("AppPhase") == null)
            {
                this.AppPhase = null;
            }
            else
            {
                this.AppPhase = rs.getString("AppPhase").trim();
            }

            if (rs.getString("AppActionType") == null)
            {
                this.AppActionType = null;
            }
            else
            {
                this.AppActionType = rs.getString("AppActionType").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimUWMDetailSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLClaimUWMDetailSchema getSchema()
    {
        LLClaimUWMDetailSchema aLLClaimUWMDetailSchema = new
                LLClaimUWMDetailSchema();
        aLLClaimUWMDetailSchema.setSchema(this);
        return aLLClaimUWMDetailSchema;
    }

    public LLClaimUWMDetailDB getDB()
    {
        LLClaimUWMDetailDB aDBOper = new LLClaimUWMDetailDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimUWMDetail描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ClmUWNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RgtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CaseNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmUWer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmUWGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmDecision)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CheckType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppClmUWer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppPhase)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppActionType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MngCom)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimUWMDetail>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ClmUWNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            ClmUWer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            ClmUWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            ClmDecision = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            AppGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            CheckType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            AppClmUWer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            AppPhase = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            AppActionType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                           SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimUWMDetailSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ClmUWNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmUWNo));
        }
        if (FCode.equals("ClmNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmNo));
        }
        if (FCode.equals("RgtNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RgtNo));
        }
        if (FCode.equals("CaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CaseNo));
        }
        if (FCode.equals("ClmUWer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmUWer));
        }
        if (FCode.equals("ClmUWGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmUWGrade));
        }
        if (FCode.equals("ClmDecision"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmDecision));
        }
        if (FCode.equals("AppGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppGrade));
        }
        if (FCode.equals("CheckType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CheckType));
        }
        if (FCode.equals("AppClmUWer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppClmUWer));
        }
        if (FCode.equals("AppPhase"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppPhase));
        }
        if (FCode.equals("AppActionType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppActionType));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MngCom));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ClmUWNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ClmNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RgtNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ClmUWer);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ClmUWGrade);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ClmDecision);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AppGrade);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(CheckType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AppClmUWer);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AppPhase);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AppActionType);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ClmUWNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmUWNo = FValue.trim();
            }
            else
            {
                ClmUWNo = null;
            }
        }
        if (FCode.equals("ClmNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmNo = FValue.trim();
            }
            else
            {
                ClmNo = null;
            }
        }
        if (FCode.equals("RgtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtNo = FValue.trim();
            }
            else
            {
                RgtNo = null;
            }
        }
        if (FCode.equals("CaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
            {
                CaseNo = null;
            }
        }
        if (FCode.equals("ClmUWer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmUWer = FValue.trim();
            }
            else
            {
                ClmUWer = null;
            }
        }
        if (FCode.equals("ClmUWGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmUWGrade = FValue.trim();
            }
            else
            {
                ClmUWGrade = null;
            }
        }
        if (FCode.equals("ClmDecision"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmDecision = FValue.trim();
            }
            else
            {
                ClmDecision = null;
            }
        }
        if (FCode.equals("AppGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppGrade = FValue.trim();
            }
            else
            {
                AppGrade = null;
            }
        }
        if (FCode.equals("CheckType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CheckType = FValue.trim();
            }
            else
            {
                CheckType = null;
            }
        }
        if (FCode.equals("AppClmUWer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppClmUWer = FValue.trim();
            }
            else
            {
                AppClmUWer = null;
            }
        }
        if (FCode.equals("AppPhase"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppPhase = FValue.trim();
            }
            else
            {
                AppPhase = null;
            }
        }
        if (FCode.equals("AppActionType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppActionType = FValue.trim();
            }
            else
            {
                AppActionType = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLClaimUWMDetailSchema other = (LLClaimUWMDetailSchema) otherObject;
        return
                ClmUWNo.equals(other.getClmUWNo())
                && ClmNo.equals(other.getClmNo())
                && RgtNo.equals(other.getRgtNo())
                && CaseNo.equals(other.getCaseNo())
                && ClmUWer.equals(other.getClmUWer())
                && ClmUWGrade.equals(other.getClmUWGrade())
                && ClmDecision.equals(other.getClmDecision())
                && AppGrade.equals(other.getAppGrade())
                && CheckType.equals(other.getCheckType())
                && AppClmUWer.equals(other.getAppClmUWer())
                && AppPhase.equals(other.getAppPhase())
                && AppActionType.equals(other.getAppActionType())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && MngCom.equals(other.getMngCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ClmUWNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ClmNo"))
        {
            return 1;
        }
        if (strFieldName.equals("RgtNo"))
        {
            return 2;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return 3;
        }
        if (strFieldName.equals("ClmUWer"))
        {
            return 4;
        }
        if (strFieldName.equals("ClmUWGrade"))
        {
            return 5;
        }
        if (strFieldName.equals("ClmDecision"))
        {
            return 6;
        }
        if (strFieldName.equals("AppGrade"))
        {
            return 7;
        }
        if (strFieldName.equals("CheckType"))
        {
            return 8;
        }
        if (strFieldName.equals("AppClmUWer"))
        {
            return 9;
        }
        if (strFieldName.equals("AppPhase"))
        {
            return 10;
        }
        if (strFieldName.equals("AppActionType"))
        {
            return 11;
        }
        if (strFieldName.equals("Remark"))
        {
            return 12;
        }
        if (strFieldName.equals("Operator"))
        {
            return 13;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 14;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 16;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 17;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ClmUWNo";
                break;
            case 1:
                strFieldName = "ClmNo";
                break;
            case 2:
                strFieldName = "RgtNo";
                break;
            case 3:
                strFieldName = "CaseNo";
                break;
            case 4:
                strFieldName = "ClmUWer";
                break;
            case 5:
                strFieldName = "ClmUWGrade";
                break;
            case 6:
                strFieldName = "ClmDecision";
                break;
            case 7:
                strFieldName = "AppGrade";
                break;
            case 8:
                strFieldName = "CheckType";
                break;
            case 9:
                strFieldName = "AppClmUWer";
                break;
            case 10:
                strFieldName = "AppPhase";
                break;
            case 11:
                strFieldName = "AppActionType";
                break;
            case 12:
                strFieldName = "Remark";
                break;
            case 13:
                strFieldName = "Operator";
                break;
            case 14:
                strFieldName = "MngCom";
                break;
            case 15:
                strFieldName = "MakeDate";
                break;
            case 16:
                strFieldName = "MakeTime";
                break;
            case 17:
                strFieldName = "ModifyDate";
                break;
            case 18:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ClmUWNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmUWer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmUWGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmDecision"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CheckType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppClmUWer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppPhase"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppActionType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
