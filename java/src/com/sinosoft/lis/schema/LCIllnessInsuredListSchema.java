/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCIllnessInsuredListDB;

/*
 * <p>ClassName: LCIllnessInsuredListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2013-09-10
 */
public class LCIllnessInsuredListSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SeqNo;
	/** 导入批次号 */
	private String BatchNo;
	/** 团体保单合同号 */
	private String GrpContNo;
	/** 印刷号 */
	private String PrtNo;
	/** 所属保障计划 */
	private String ContPlanCode;
	/** 合同号码 */
	private String ContNo;
	/** 状态 */
	private String State;
	/** 主被保人id编号 */
	private String MainInsuID;
	/** 被保人序号 */
	private String InsuredID;
	/** 被保人客户号 */
	private String InsuredNo;
	/** 姓名 */
	private String Name;
	/** 性别 */
	private String Sex;
	/** 出生日期 */
	private Date Birthday;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 证件有效期起 */
	private Date IDStartDate;
	/** 证件有效期止 */
	private Date IDEndDate;
	/** 其他证件类型 */
	private String OthIDType;
	/** 其他证件号码 */
	private String OthIDNo;
	/** 人员类别 */
	private String PersonType;
	/** 社保卡号 */
	private String SocialSecurityNo;
	/** 联系方式 */
	private String ContactInformation;
	/** 职业类别 */
	private String OccupationType;
	/** 职业代码 */
	private String OccupationCode;
	/** 投保年龄 */
	private int InsuredAppAge;
	/** 在职/退休 */
	private String Retire;
	/** 员工姓名 */
	private String EmployeeName;
	/** 与员工关系 */
	private String Relation;
	/** 理赔金转帐银行 */
	private String BankCode;
	/** 帐号 */
	private String BankAccNo;
	/** 户名 */
	private String AccName;
	/** 英文名称 */
	private String EnglishName;
	/** 电话 */
	private String Phone;
	/** 手机 */
	private String Mobile;
	/** 职级 */
	private String Position;
	/** 计算规则 */
	private String CalRule;
	/** 无名单人数 */
	private int NoNamePeoples;
	/** 分单合同生效日 */
	private Date CValiDate;
	/** 分单合同终止日期 */
	private Date CInValiDate;
	/** 被保人合同状态 */
	private String StateFlag;
	/** 数据质检标志 */
	private String DataChkFlag;
	/** 数据质检结果说明 */
	private String ChkRemark;
	/** 算费标识 */
	private String CalState;
	/** 算费结果说明 */
	private String CalRemark;
	/** 承保状态 */
	private String AppFlag;
	/** 公共账户类型 */
	private String PublicAccType;
	/** 公共账户 */
	private double PublicAcc;
	/** 保费计算方式 */
	private String CalPremType;
	/** 保全号 */
	private String EdorNo;
	/** 保全生效日期 */
	private Date EdorValiDate;
	/** 期交保费 */
	private String Prem;
	/** 未满期保费 */
	private String EdorPrem;
	/** 单位编码 */
	private String GrpNo;
	/** 单位名称 */
	private String GrpName;
	/** 社保分中心代码 */
	private String SocialCenterNo;
	/** 社保中心名称 */
	private String SocialCenterName;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 61;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCIllnessInsuredListSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SeqNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCIllnessInsuredListSchema cloned = (LCIllnessInsuredListSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSeqNo()
	{
		return SeqNo;
	}
	public void setSeqNo(String aSeqNo)
	{
		SeqNo = aSeqNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getContPlanCode()
	{
		return ContPlanCode;
	}
	public void setContPlanCode(String aContPlanCode)
	{
		ContPlanCode = aContPlanCode;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getMainInsuID()
	{
		return MainInsuID;
	}
	public void setMainInsuID(String aMainInsuID)
	{
		MainInsuID = aMainInsuID;
	}
	public String getInsuredID()
	{
		return InsuredID;
	}
	public void setInsuredID(String aInsuredID)
	{
		InsuredID = aInsuredID;
	}
	public String getInsuredNo()
	{
		return InsuredNo;
	}
	public void setInsuredNo(String aInsuredNo)
	{
		InsuredNo = aInsuredNo;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
		Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getIDStartDate()
	{
		if( IDStartDate != null )
			return fDate.getString(IDStartDate);
		else
			return null;
	}
	public void setIDStartDate(Date aIDStartDate)
	{
		IDStartDate = aIDStartDate;
	}
	public void setIDStartDate(String aIDStartDate)
	{
		if (aIDStartDate != null && !aIDStartDate.equals("") )
		{
			IDStartDate = fDate.getDate( aIDStartDate );
		}
		else
			IDStartDate = null;
	}

	public String getIDEndDate()
	{
		if( IDEndDate != null )
			return fDate.getString(IDEndDate);
		else
			return null;
	}
	public void setIDEndDate(Date aIDEndDate)
	{
		IDEndDate = aIDEndDate;
	}
	public void setIDEndDate(String aIDEndDate)
	{
		if (aIDEndDate != null && !aIDEndDate.equals("") )
		{
			IDEndDate = fDate.getDate( aIDEndDate );
		}
		else
			IDEndDate = null;
	}

	public String getOthIDType()
	{
		return OthIDType;
	}
	public void setOthIDType(String aOthIDType)
	{
		OthIDType = aOthIDType;
	}
	public String getOthIDNo()
	{
		return OthIDNo;
	}
	public void setOthIDNo(String aOthIDNo)
	{
		OthIDNo = aOthIDNo;
	}
	public String getPersonType()
	{
		return PersonType;
	}
	public void setPersonType(String aPersonType)
	{
		PersonType = aPersonType;
	}
	public String getSocialSecurityNo()
	{
		return SocialSecurityNo;
	}
	public void setSocialSecurityNo(String aSocialSecurityNo)
	{
		SocialSecurityNo = aSocialSecurityNo;
	}
	public String getContactInformation()
	{
		return ContactInformation;
	}
	public void setContactInformation(String aContactInformation)
	{
		ContactInformation = aContactInformation;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}
	public String getOccupationCode()
	{
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
		OccupationCode = aOccupationCode;
	}
	public int getInsuredAppAge()
	{
		return InsuredAppAge;
	}
	public void setInsuredAppAge(int aInsuredAppAge)
	{
		InsuredAppAge = aInsuredAppAge;
	}
	public void setInsuredAppAge(String aInsuredAppAge)
	{
		if (aInsuredAppAge != null && !aInsuredAppAge.equals(""))
		{
			Integer tInteger = new Integer(aInsuredAppAge);
			int i = tInteger.intValue();
			InsuredAppAge = i;
		}
	}

	public String getRetire()
	{
		return Retire;
	}
	public void setRetire(String aRetire)
	{
		Retire = aRetire;
	}
	public String getEmployeeName()
	{
		return EmployeeName;
	}
	public void setEmployeeName(String aEmployeeName)
	{
		EmployeeName = aEmployeeName;
	}
	public String getRelation()
	{
		return Relation;
	}
	public void setRelation(String aRelation)
	{
		Relation = aRelation;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getEnglishName()
	{
		return EnglishName;
	}
	public void setEnglishName(String aEnglishName)
	{
		EnglishName = aEnglishName;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getPosition()
	{
		return Position;
	}
	public void setPosition(String aPosition)
	{
		Position = aPosition;
	}
	public String getCalRule()
	{
		return CalRule;
	}
	public void setCalRule(String aCalRule)
	{
		CalRule = aCalRule;
	}
	public int getNoNamePeoples()
	{
		return NoNamePeoples;
	}
	public void setNoNamePeoples(int aNoNamePeoples)
	{
		NoNamePeoples = aNoNamePeoples;
	}
	public void setNoNamePeoples(String aNoNamePeoples)
	{
		if (aNoNamePeoples != null && !aNoNamePeoples.equals(""))
		{
			Integer tInteger = new Integer(aNoNamePeoples);
			int i = tInteger.intValue();
			NoNamePeoples = i;
		}
	}

	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public String getCInValiDate()
	{
		if( CInValiDate != null )
			return fDate.getString(CInValiDate);
		else
			return null;
	}
	public void setCInValiDate(Date aCInValiDate)
	{
		CInValiDate = aCInValiDate;
	}
	public void setCInValiDate(String aCInValiDate)
	{
		if (aCInValiDate != null && !aCInValiDate.equals("") )
		{
			CInValiDate = fDate.getDate( aCInValiDate );
		}
		else
			CInValiDate = null;
	}

	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public String getDataChkFlag()
	{
		return DataChkFlag;
	}
	public void setDataChkFlag(String aDataChkFlag)
	{
		DataChkFlag = aDataChkFlag;
	}
	public String getChkRemark()
	{
		return ChkRemark;
	}
	public void setChkRemark(String aChkRemark)
	{
		ChkRemark = aChkRemark;
	}
	public String getCalState()
	{
		return CalState;
	}
	public void setCalState(String aCalState)
	{
		CalState = aCalState;
	}
	public String getCalRemark()
	{
		return CalRemark;
	}
	public void setCalRemark(String aCalRemark)
	{
		CalRemark = aCalRemark;
	}
	public String getAppFlag()
	{
		return AppFlag;
	}
	public void setAppFlag(String aAppFlag)
	{
		AppFlag = aAppFlag;
	}
	public String getPublicAccType()
	{
		return PublicAccType;
	}
	public void setPublicAccType(String aPublicAccType)
	{
		PublicAccType = aPublicAccType;
	}
	public double getPublicAcc()
	{
		return PublicAcc;
	}
	public void setPublicAcc(double aPublicAcc)
	{
		PublicAcc = Arith.round(aPublicAcc,2);
	}
	public void setPublicAcc(String aPublicAcc)
	{
		if (aPublicAcc != null && !aPublicAcc.equals(""))
		{
			Double tDouble = new Double(aPublicAcc);
			double d = tDouble.doubleValue();
                PublicAcc = Arith.round(d,2);
		}
	}

	public String getCalPremType()
	{
		return CalPremType;
	}
	public void setCalPremType(String aCalPremType)
	{
		CalPremType = aCalPremType;
	}
	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getEdorValiDate()
	{
		if( EdorValiDate != null )
			return fDate.getString(EdorValiDate);
		else
			return null;
	}
	public void setEdorValiDate(Date aEdorValiDate)
	{
		EdorValiDate = aEdorValiDate;
	}
	public void setEdorValiDate(String aEdorValiDate)
	{
		if (aEdorValiDate != null && !aEdorValiDate.equals("") )
		{
			EdorValiDate = fDate.getDate( aEdorValiDate );
		}
		else
			EdorValiDate = null;
	}

	public String getPrem()
	{
		return Prem;
	}
	public void setPrem(String aPrem)
	{
		Prem = aPrem;
	}
	public String getEdorPrem()
	{
		return EdorPrem;
	}
	public void setEdorPrem(String aEdorPrem)
	{
		EdorPrem = aEdorPrem;
	}
	public String getGrpNo()
	{
		return GrpNo;
	}
	public void setGrpNo(String aGrpNo)
	{
		GrpNo = aGrpNo;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getSocialCenterNo()
	{
		return SocialCenterNo;
	}
	public void setSocialCenterNo(String aSocialCenterNo)
	{
		SocialCenterNo = aSocialCenterNo;
	}
	public String getSocialCenterName()
	{
		return SocialCenterName;
	}
	public void setSocialCenterName(String aSocialCenterName)
	{
		SocialCenterName = aSocialCenterName;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LCIllnessInsuredListSchema 对象给 Schema 赋值
	* @param: aLCIllnessInsuredListSchema LCIllnessInsuredListSchema
	**/
	public void setSchema(LCIllnessInsuredListSchema aLCIllnessInsuredListSchema)
	{
		this.SeqNo = aLCIllnessInsuredListSchema.getSeqNo();
		this.BatchNo = aLCIllnessInsuredListSchema.getBatchNo();
		this.GrpContNo = aLCIllnessInsuredListSchema.getGrpContNo();
		this.PrtNo = aLCIllnessInsuredListSchema.getPrtNo();
		this.ContPlanCode = aLCIllnessInsuredListSchema.getContPlanCode();
		this.ContNo = aLCIllnessInsuredListSchema.getContNo();
		this.State = aLCIllnessInsuredListSchema.getState();
		this.MainInsuID = aLCIllnessInsuredListSchema.getMainInsuID();
		this.InsuredID = aLCIllnessInsuredListSchema.getInsuredID();
		this.InsuredNo = aLCIllnessInsuredListSchema.getInsuredNo();
		this.Name = aLCIllnessInsuredListSchema.getName();
		this.Sex = aLCIllnessInsuredListSchema.getSex();
		this.Birthday = fDate.getDate( aLCIllnessInsuredListSchema.getBirthday());
		this.IDType = aLCIllnessInsuredListSchema.getIDType();
		this.IDNo = aLCIllnessInsuredListSchema.getIDNo();
		this.IDStartDate = fDate.getDate( aLCIllnessInsuredListSchema.getIDStartDate());
		this.IDEndDate = fDate.getDate( aLCIllnessInsuredListSchema.getIDEndDate());
		this.OthIDType = aLCIllnessInsuredListSchema.getOthIDType();
		this.OthIDNo = aLCIllnessInsuredListSchema.getOthIDNo();
		this.PersonType = aLCIllnessInsuredListSchema.getPersonType();
		this.SocialSecurityNo = aLCIllnessInsuredListSchema.getSocialSecurityNo();
		this.ContactInformation = aLCIllnessInsuredListSchema.getContactInformation();
		this.OccupationType = aLCIllnessInsuredListSchema.getOccupationType();
		this.OccupationCode = aLCIllnessInsuredListSchema.getOccupationCode();
		this.InsuredAppAge = aLCIllnessInsuredListSchema.getInsuredAppAge();
		this.Retire = aLCIllnessInsuredListSchema.getRetire();
		this.EmployeeName = aLCIllnessInsuredListSchema.getEmployeeName();
		this.Relation = aLCIllnessInsuredListSchema.getRelation();
		this.BankCode = aLCIllnessInsuredListSchema.getBankCode();
		this.BankAccNo = aLCIllnessInsuredListSchema.getBankAccNo();
		this.AccName = aLCIllnessInsuredListSchema.getAccName();
		this.EnglishName = aLCIllnessInsuredListSchema.getEnglishName();
		this.Phone = aLCIllnessInsuredListSchema.getPhone();
		this.Mobile = aLCIllnessInsuredListSchema.getMobile();
		this.Position = aLCIllnessInsuredListSchema.getPosition();
		this.CalRule = aLCIllnessInsuredListSchema.getCalRule();
		this.NoNamePeoples = aLCIllnessInsuredListSchema.getNoNamePeoples();
		this.CValiDate = fDate.getDate( aLCIllnessInsuredListSchema.getCValiDate());
		this.CInValiDate = fDate.getDate( aLCIllnessInsuredListSchema.getCInValiDate());
		this.StateFlag = aLCIllnessInsuredListSchema.getStateFlag();
		this.DataChkFlag = aLCIllnessInsuredListSchema.getDataChkFlag();
		this.ChkRemark = aLCIllnessInsuredListSchema.getChkRemark();
		this.CalState = aLCIllnessInsuredListSchema.getCalState();
		this.CalRemark = aLCIllnessInsuredListSchema.getCalRemark();
		this.AppFlag = aLCIllnessInsuredListSchema.getAppFlag();
		this.PublicAccType = aLCIllnessInsuredListSchema.getPublicAccType();
		this.PublicAcc = aLCIllnessInsuredListSchema.getPublicAcc();
		this.CalPremType = aLCIllnessInsuredListSchema.getCalPremType();
		this.EdorNo = aLCIllnessInsuredListSchema.getEdorNo();
		this.EdorValiDate = fDate.getDate( aLCIllnessInsuredListSchema.getEdorValiDate());
		this.Prem = aLCIllnessInsuredListSchema.getPrem();
		this.EdorPrem = aLCIllnessInsuredListSchema.getEdorPrem();
		this.GrpNo = aLCIllnessInsuredListSchema.getGrpNo();
		this.GrpName = aLCIllnessInsuredListSchema.getGrpName();
		this.SocialCenterNo = aLCIllnessInsuredListSchema.getSocialCenterNo();
		this.SocialCenterName = aLCIllnessInsuredListSchema.getSocialCenterName();
		this.Operator = aLCIllnessInsuredListSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCIllnessInsuredListSchema.getMakeDate());
		this.MakeTime = aLCIllnessInsuredListSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCIllnessInsuredListSchema.getModifyDate());
		this.ModifyTime = aLCIllnessInsuredListSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SeqNo") == null )
				this.SeqNo = null;
			else
				this.SeqNo = rs.getString("SeqNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("ContPlanCode") == null )
				this.ContPlanCode = null;
			else
				this.ContPlanCode = rs.getString("ContPlanCode").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("MainInsuID") == null )
				this.MainInsuID = null;
			else
				this.MainInsuID = rs.getString("MainInsuID").trim();

			if( rs.getString("InsuredID") == null )
				this.InsuredID = null;
			else
				this.InsuredID = rs.getString("InsuredID").trim();

			if( rs.getString("InsuredNo") == null )
				this.InsuredNo = null;
			else
				this.InsuredNo = rs.getString("InsuredNo").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			this.Birthday = rs.getDate("Birthday");
			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			this.IDStartDate = rs.getDate("IDStartDate");
			this.IDEndDate = rs.getDate("IDEndDate");
			if( rs.getString("OthIDType") == null )
				this.OthIDType = null;
			else
				this.OthIDType = rs.getString("OthIDType").trim();

			if( rs.getString("OthIDNo") == null )
				this.OthIDNo = null;
			else
				this.OthIDNo = rs.getString("OthIDNo").trim();

			if( rs.getString("PersonType") == null )
				this.PersonType = null;
			else
				this.PersonType = rs.getString("PersonType").trim();

			if( rs.getString("SocialSecurityNo") == null )
				this.SocialSecurityNo = null;
			else
				this.SocialSecurityNo = rs.getString("SocialSecurityNo").trim();

			if( rs.getString("ContactInformation") == null )
				this.ContactInformation = null;
			else
				this.ContactInformation = rs.getString("ContactInformation").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			if( rs.getString("OccupationCode") == null )
				this.OccupationCode = null;
			else
				this.OccupationCode = rs.getString("OccupationCode").trim();

			this.InsuredAppAge = rs.getInt("InsuredAppAge");
			if( rs.getString("Retire") == null )
				this.Retire = null;
			else
				this.Retire = rs.getString("Retire").trim();

			if( rs.getString("EmployeeName") == null )
				this.EmployeeName = null;
			else
				this.EmployeeName = rs.getString("EmployeeName").trim();

			if( rs.getString("Relation") == null )
				this.Relation = null;
			else
				this.Relation = rs.getString("Relation").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("EnglishName") == null )
				this.EnglishName = null;
			else
				this.EnglishName = rs.getString("EnglishName").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("Position") == null )
				this.Position = null;
			else
				this.Position = rs.getString("Position").trim();

			if( rs.getString("CalRule") == null )
				this.CalRule = null;
			else
				this.CalRule = rs.getString("CalRule").trim();

			this.NoNamePeoples = rs.getInt("NoNamePeoples");
			this.CValiDate = rs.getDate("CValiDate");
			this.CInValiDate = rs.getDate("CInValiDate");
			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			if( rs.getString("DataChkFlag") == null )
				this.DataChkFlag = null;
			else
				this.DataChkFlag = rs.getString("DataChkFlag").trim();

			if( rs.getString("ChkRemark") == null )
				this.ChkRemark = null;
			else
				this.ChkRemark = rs.getString("ChkRemark").trim();

			if( rs.getString("CalState") == null )
				this.CalState = null;
			else
				this.CalState = rs.getString("CalState").trim();

			if( rs.getString("CalRemark") == null )
				this.CalRemark = null;
			else
				this.CalRemark = rs.getString("CalRemark").trim();

			if( rs.getString("AppFlag") == null )
				this.AppFlag = null;
			else
				this.AppFlag = rs.getString("AppFlag").trim();

			if( rs.getString("PublicAccType") == null )
				this.PublicAccType = null;
			else
				this.PublicAccType = rs.getString("PublicAccType").trim();

			this.PublicAcc = rs.getDouble("PublicAcc");
			if( rs.getString("CalPremType") == null )
				this.CalPremType = null;
			else
				this.CalPremType = rs.getString("CalPremType").trim();

			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			this.EdorValiDate = rs.getDate("EdorValiDate");
			if( rs.getString("Prem") == null )
				this.Prem = null;
			else
				this.Prem = rs.getString("Prem").trim();

			if( rs.getString("EdorPrem") == null )
				this.EdorPrem = null;
			else
				this.EdorPrem = rs.getString("EdorPrem").trim();

			if( rs.getString("GrpNo") == null )
				this.GrpNo = null;
			else
				this.GrpNo = rs.getString("GrpNo").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("SocialCenterNo") == null )
				this.SocialCenterNo = null;
			else
				this.SocialCenterNo = rs.getString("SocialCenterNo").trim();

			if( rs.getString("SocialCenterName") == null )
				this.SocialCenterName = null;
			else
				this.SocialCenterName = rs.getString("SocialCenterName").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCIllnessInsuredList表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCIllnessInsuredListSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCIllnessInsuredListSchema getSchema()
	{
		LCIllnessInsuredListSchema aLCIllnessInsuredListSchema = new LCIllnessInsuredListSchema();
		aLCIllnessInsuredListSchema.setSchema(this);
		return aLCIllnessInsuredListSchema;
	}

	public LCIllnessInsuredListDB getDB()
	{
		LCIllnessInsuredListDB aDBOper = new LCIllnessInsuredListDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCIllnessInsuredList描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SeqNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainInsuID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PersonType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialSecurityNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContactInformation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuredAppAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Retire)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EmployeeName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Relation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EnglishName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalRule)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(NoNamePeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CInValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataChkFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChkRemark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalRemark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PublicAccType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PublicAcc));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalPremType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EdorValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Prem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorPrem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialCenterNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialCenterName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCIllnessInsuredList>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SeqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MainInsuID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			InsuredID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			IDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			IDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			OthIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			OthIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			PersonType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			SocialSecurityNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ContactInformation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			InsuredAppAge= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).intValue();
			Retire = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			EmployeeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Relation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			EnglishName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			CalRule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			NoNamePeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).intValue();
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,SysConst.PACKAGESPILTER));
			CInValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			DataChkFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			ChkRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			CalState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			CalRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			AppFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			PublicAccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			PublicAcc = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).doubleValue();
			CalPremType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			EdorValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50,SysConst.PACKAGESPILTER));
			Prem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			EdorPrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			SocialCenterNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			SocialCenterName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCIllnessInsuredListSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SeqNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SeqNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("ContPlanCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("MainInsuID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainInsuID));
		}
		if (FCode.equals("InsuredID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredID));
		}
		if (FCode.equals("InsuredNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("IDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
		}
		if (FCode.equals("IDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
		}
		if (FCode.equals("OthIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDType));
		}
		if (FCode.equals("OthIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDNo));
		}
		if (FCode.equals("PersonType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersonType));
		}
		if (FCode.equals("SocialSecurityNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialSecurityNo));
		}
		if (FCode.equals("ContactInformation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContactInformation));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("OccupationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
		}
		if (FCode.equals("InsuredAppAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredAppAge));
		}
		if (FCode.equals("Retire"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Retire));
		}
		if (FCode.equals("EmployeeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EmployeeName));
		}
		if (FCode.equals("Relation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("EnglishName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EnglishName));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("Position"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
		}
		if (FCode.equals("CalRule"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalRule));
		}
		if (FCode.equals("NoNamePeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NoNamePeoples));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("CInValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCInValiDate()));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("DataChkFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataChkFlag));
		}
		if (FCode.equals("ChkRemark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChkRemark));
		}
		if (FCode.equals("CalState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalState));
		}
		if (FCode.equals("CalRemark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalRemark));
		}
		if (FCode.equals("AppFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
		}
		if (FCode.equals("PublicAccType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PublicAccType));
		}
		if (FCode.equals("PublicAcc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PublicAcc));
		}
		if (FCode.equals("CalPremType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalPremType));
		}
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("EdorValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEdorValiDate()));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("EdorPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorPrem));
		}
		if (FCode.equals("GrpNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("SocialCenterNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialCenterNo));
		}
		if (FCode.equals("SocialCenterName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialCenterName));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SeqNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MainInsuID);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(InsuredID);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(InsuredNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(OthIDType);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(OthIDNo);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(PersonType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(SocialSecurityNo);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ContactInformation);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(OccupationCode);
				break;
			case 24:
				strFieldValue = String.valueOf(InsuredAppAge);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Retire);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(EmployeeName);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Relation);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(EnglishName);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(Position);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(CalRule);
				break;
			case 36:
				strFieldValue = String.valueOf(NoNamePeoples);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCInValiDate()));
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(DataChkFlag);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(ChkRemark);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(CalState);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(CalRemark);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(AppFlag);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(PublicAccType);
				break;
			case 46:
				strFieldValue = String.valueOf(PublicAcc);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(CalPremType);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEdorValiDate()));
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(Prem);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(EdorPrem);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(GrpNo);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(SocialCenterNo);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(SocialCenterName);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SeqNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SeqNo = FValue.trim();
			}
			else
				SeqNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("ContPlanCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPlanCode = FValue.trim();
			}
			else
				ContPlanCode = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("MainInsuID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainInsuID = FValue.trim();
			}
			else
				MainInsuID = null;
		}
		if (FCode.equalsIgnoreCase("InsuredID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredID = FValue.trim();
			}
			else
				InsuredID = null;
		}
		if (FCode.equalsIgnoreCase("InsuredNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredNo = FValue.trim();
			}
			else
				InsuredNo = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("IDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDStartDate = fDate.getDate( FValue );
			}
			else
				IDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("IDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDEndDate = fDate.getDate( FValue );
			}
			else
				IDEndDate = null;
		}
		if (FCode.equalsIgnoreCase("OthIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDType = FValue.trim();
			}
			else
				OthIDType = null;
		}
		if (FCode.equalsIgnoreCase("OthIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDNo = FValue.trim();
			}
			else
				OthIDNo = null;
		}
		if (FCode.equalsIgnoreCase("PersonType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PersonType = FValue.trim();
			}
			else
				PersonType = null;
		}
		if (FCode.equalsIgnoreCase("SocialSecurityNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialSecurityNo = FValue.trim();
			}
			else
				SocialSecurityNo = null;
		}
		if (FCode.equalsIgnoreCase("ContactInformation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContactInformation = FValue.trim();
			}
			else
				ContactInformation = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("OccupationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationCode = FValue.trim();
			}
			else
				OccupationCode = null;
		}
		if (FCode.equalsIgnoreCase("InsuredAppAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuredAppAge = i;
			}
		}
		if (FCode.equalsIgnoreCase("Retire"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Retire = FValue.trim();
			}
			else
				Retire = null;
		}
		if (FCode.equalsIgnoreCase("EmployeeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EmployeeName = FValue.trim();
			}
			else
				EmployeeName = null;
		}
		if (FCode.equalsIgnoreCase("Relation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Relation = FValue.trim();
			}
			else
				Relation = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("EnglishName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EnglishName = FValue.trim();
			}
			else
				EnglishName = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("Position"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Position = FValue.trim();
			}
			else
				Position = null;
		}
		if (FCode.equalsIgnoreCase("CalRule"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalRule = FValue.trim();
			}
			else
				CalRule = null;
		}
		if (FCode.equalsIgnoreCase("NoNamePeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				NoNamePeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("CInValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CInValiDate = fDate.getDate( FValue );
			}
			else
				CInValiDate = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("DataChkFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataChkFlag = FValue.trim();
			}
			else
				DataChkFlag = null;
		}
		if (FCode.equalsIgnoreCase("ChkRemark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChkRemark = FValue.trim();
			}
			else
				ChkRemark = null;
		}
		if (FCode.equalsIgnoreCase("CalState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalState = FValue.trim();
			}
			else
				CalState = null;
		}
		if (FCode.equalsIgnoreCase("CalRemark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalRemark = FValue.trim();
			}
			else
				CalRemark = null;
		}
		if (FCode.equalsIgnoreCase("AppFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppFlag = FValue.trim();
			}
			else
				AppFlag = null;
		}
		if (FCode.equalsIgnoreCase("PublicAccType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PublicAccType = FValue.trim();
			}
			else
				PublicAccType = null;
		}
		if (FCode.equalsIgnoreCase("PublicAcc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PublicAcc = d;
			}
		}
		if (FCode.equalsIgnoreCase("CalPremType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalPremType = FValue.trim();
			}
			else
				CalPremType = null;
		}
		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EdorValiDate = fDate.getDate( FValue );
			}
			else
				EdorValiDate = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Prem = FValue.trim();
			}
			else
				Prem = null;
		}
		if (FCode.equalsIgnoreCase("EdorPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorPrem = FValue.trim();
			}
			else
				EdorPrem = null;
		}
		if (FCode.equalsIgnoreCase("GrpNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNo = FValue.trim();
			}
			else
				GrpNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("SocialCenterNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialCenterNo = FValue.trim();
			}
			else
				SocialCenterNo = null;
		}
		if (FCode.equalsIgnoreCase("SocialCenterName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialCenterName = FValue.trim();
			}
			else
				SocialCenterName = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCIllnessInsuredListSchema other = (LCIllnessInsuredListSchema)otherObject;
		return
			(SeqNo == null ? other.getSeqNo() == null : SeqNo.equals(other.getSeqNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (ContPlanCode == null ? other.getContPlanCode() == null : ContPlanCode.equals(other.getContPlanCode()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (MainInsuID == null ? other.getMainInsuID() == null : MainInsuID.equals(other.getMainInsuID()))
			&& (InsuredID == null ? other.getInsuredID() == null : InsuredID.equals(other.getInsuredID()))
			&& (InsuredNo == null ? other.getInsuredNo() == null : InsuredNo.equals(other.getInsuredNo()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (Birthday == null ? other.getBirthday() == null : fDate.getString(Birthday).equals(other.getBirthday()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (IDStartDate == null ? other.getIDStartDate() == null : fDate.getString(IDStartDate).equals(other.getIDStartDate()))
			&& (IDEndDate == null ? other.getIDEndDate() == null : fDate.getString(IDEndDate).equals(other.getIDEndDate()))
			&& (OthIDType == null ? other.getOthIDType() == null : OthIDType.equals(other.getOthIDType()))
			&& (OthIDNo == null ? other.getOthIDNo() == null : OthIDNo.equals(other.getOthIDNo()))
			&& (PersonType == null ? other.getPersonType() == null : PersonType.equals(other.getPersonType()))
			&& (SocialSecurityNo == null ? other.getSocialSecurityNo() == null : SocialSecurityNo.equals(other.getSocialSecurityNo()))
			&& (ContactInformation == null ? other.getContactInformation() == null : ContactInformation.equals(other.getContactInformation()))
			&& (OccupationType == null ? other.getOccupationType() == null : OccupationType.equals(other.getOccupationType()))
			&& (OccupationCode == null ? other.getOccupationCode() == null : OccupationCode.equals(other.getOccupationCode()))
			&& InsuredAppAge == other.getInsuredAppAge()
			&& (Retire == null ? other.getRetire() == null : Retire.equals(other.getRetire()))
			&& (EmployeeName == null ? other.getEmployeeName() == null : EmployeeName.equals(other.getEmployeeName()))
			&& (Relation == null ? other.getRelation() == null : Relation.equals(other.getRelation()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (EnglishName == null ? other.getEnglishName() == null : EnglishName.equals(other.getEnglishName()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (Position == null ? other.getPosition() == null : Position.equals(other.getPosition()))
			&& (CalRule == null ? other.getCalRule() == null : CalRule.equals(other.getCalRule()))
			&& NoNamePeoples == other.getNoNamePeoples()
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& (CInValiDate == null ? other.getCInValiDate() == null : fDate.getString(CInValiDate).equals(other.getCInValiDate()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& (DataChkFlag == null ? other.getDataChkFlag() == null : DataChkFlag.equals(other.getDataChkFlag()))
			&& (ChkRemark == null ? other.getChkRemark() == null : ChkRemark.equals(other.getChkRemark()))
			&& (CalState == null ? other.getCalState() == null : CalState.equals(other.getCalState()))
			&& (CalRemark == null ? other.getCalRemark() == null : CalRemark.equals(other.getCalRemark()))
			&& (AppFlag == null ? other.getAppFlag() == null : AppFlag.equals(other.getAppFlag()))
			&& (PublicAccType == null ? other.getPublicAccType() == null : PublicAccType.equals(other.getPublicAccType()))
			&& PublicAcc == other.getPublicAcc()
			&& (CalPremType == null ? other.getCalPremType() == null : CalPremType.equals(other.getCalPremType()))
			&& (EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (EdorValiDate == null ? other.getEdorValiDate() == null : fDate.getString(EdorValiDate).equals(other.getEdorValiDate()))
			&& (Prem == null ? other.getPrem() == null : Prem.equals(other.getPrem()))
			&& (EdorPrem == null ? other.getEdorPrem() == null : EdorPrem.equals(other.getEdorPrem()))
			&& (GrpNo == null ? other.getGrpNo() == null : GrpNo.equals(other.getGrpNo()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (SocialCenterNo == null ? other.getSocialCenterNo() == null : SocialCenterNo.equals(other.getSocialCenterNo()))
			&& (SocialCenterName == null ? other.getSocialCenterName() == null : SocialCenterName.equals(other.getSocialCenterName()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SeqNo") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 2;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 3;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return 4;
		}
		if( strFieldName.equals("ContNo") ) {
			return 5;
		}
		if( strFieldName.equals("State") ) {
			return 6;
		}
		if( strFieldName.equals("MainInsuID") ) {
			return 7;
		}
		if( strFieldName.equals("InsuredID") ) {
			return 8;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return 9;
		}
		if( strFieldName.equals("Name") ) {
			return 10;
		}
		if( strFieldName.equals("Sex") ) {
			return 11;
		}
		if( strFieldName.equals("Birthday") ) {
			return 12;
		}
		if( strFieldName.equals("IDType") ) {
			return 13;
		}
		if( strFieldName.equals("IDNo") ) {
			return 14;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return 15;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return 16;
		}
		if( strFieldName.equals("OthIDType") ) {
			return 17;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return 18;
		}
		if( strFieldName.equals("PersonType") ) {
			return 19;
		}
		if( strFieldName.equals("SocialSecurityNo") ) {
			return 20;
		}
		if( strFieldName.equals("ContactInformation") ) {
			return 21;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 22;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return 23;
		}
		if( strFieldName.equals("InsuredAppAge") ) {
			return 24;
		}
		if( strFieldName.equals("Retire") ) {
			return 25;
		}
		if( strFieldName.equals("EmployeeName") ) {
			return 26;
		}
		if( strFieldName.equals("Relation") ) {
			return 27;
		}
		if( strFieldName.equals("BankCode") ) {
			return 28;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 29;
		}
		if( strFieldName.equals("AccName") ) {
			return 30;
		}
		if( strFieldName.equals("EnglishName") ) {
			return 31;
		}
		if( strFieldName.equals("Phone") ) {
			return 32;
		}
		if( strFieldName.equals("Mobile") ) {
			return 33;
		}
		if( strFieldName.equals("Position") ) {
			return 34;
		}
		if( strFieldName.equals("CalRule") ) {
			return 35;
		}
		if( strFieldName.equals("NoNamePeoples") ) {
			return 36;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 37;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return 38;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 39;
		}
		if( strFieldName.equals("DataChkFlag") ) {
			return 40;
		}
		if( strFieldName.equals("ChkRemark") ) {
			return 41;
		}
		if( strFieldName.equals("CalState") ) {
			return 42;
		}
		if( strFieldName.equals("CalRemark") ) {
			return 43;
		}
		if( strFieldName.equals("AppFlag") ) {
			return 44;
		}
		if( strFieldName.equals("PublicAccType") ) {
			return 45;
		}
		if( strFieldName.equals("PublicAcc") ) {
			return 46;
		}
		if( strFieldName.equals("CalPremType") ) {
			return 47;
		}
		if( strFieldName.equals("EdorNo") ) {
			return 48;
		}
		if( strFieldName.equals("EdorValiDate") ) {
			return 49;
		}
		if( strFieldName.equals("Prem") ) {
			return 50;
		}
		if( strFieldName.equals("EdorPrem") ) {
			return 51;
		}
		if( strFieldName.equals("GrpNo") ) {
			return 52;
		}
		if( strFieldName.equals("GrpName") ) {
			return 53;
		}
		if( strFieldName.equals("SocialCenterNo") ) {
			return 54;
		}
		if( strFieldName.equals("SocialCenterName") ) {
			return 55;
		}
		if( strFieldName.equals("Operator") ) {
			return 56;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 57;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 58;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 59;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 60;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SeqNo";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "GrpContNo";
				break;
			case 3:
				strFieldName = "PrtNo";
				break;
			case 4:
				strFieldName = "ContPlanCode";
				break;
			case 5:
				strFieldName = "ContNo";
				break;
			case 6:
				strFieldName = "State";
				break;
			case 7:
				strFieldName = "MainInsuID";
				break;
			case 8:
				strFieldName = "InsuredID";
				break;
			case 9:
				strFieldName = "InsuredNo";
				break;
			case 10:
				strFieldName = "Name";
				break;
			case 11:
				strFieldName = "Sex";
				break;
			case 12:
				strFieldName = "Birthday";
				break;
			case 13:
				strFieldName = "IDType";
				break;
			case 14:
				strFieldName = "IDNo";
				break;
			case 15:
				strFieldName = "IDStartDate";
				break;
			case 16:
				strFieldName = "IDEndDate";
				break;
			case 17:
				strFieldName = "OthIDType";
				break;
			case 18:
				strFieldName = "OthIDNo";
				break;
			case 19:
				strFieldName = "PersonType";
				break;
			case 20:
				strFieldName = "SocialSecurityNo";
				break;
			case 21:
				strFieldName = "ContactInformation";
				break;
			case 22:
				strFieldName = "OccupationType";
				break;
			case 23:
				strFieldName = "OccupationCode";
				break;
			case 24:
				strFieldName = "InsuredAppAge";
				break;
			case 25:
				strFieldName = "Retire";
				break;
			case 26:
				strFieldName = "EmployeeName";
				break;
			case 27:
				strFieldName = "Relation";
				break;
			case 28:
				strFieldName = "BankCode";
				break;
			case 29:
				strFieldName = "BankAccNo";
				break;
			case 30:
				strFieldName = "AccName";
				break;
			case 31:
				strFieldName = "EnglishName";
				break;
			case 32:
				strFieldName = "Phone";
				break;
			case 33:
				strFieldName = "Mobile";
				break;
			case 34:
				strFieldName = "Position";
				break;
			case 35:
				strFieldName = "CalRule";
				break;
			case 36:
				strFieldName = "NoNamePeoples";
				break;
			case 37:
				strFieldName = "CValiDate";
				break;
			case 38:
				strFieldName = "CInValiDate";
				break;
			case 39:
				strFieldName = "StateFlag";
				break;
			case 40:
				strFieldName = "DataChkFlag";
				break;
			case 41:
				strFieldName = "ChkRemark";
				break;
			case 42:
				strFieldName = "CalState";
				break;
			case 43:
				strFieldName = "CalRemark";
				break;
			case 44:
				strFieldName = "AppFlag";
				break;
			case 45:
				strFieldName = "PublicAccType";
				break;
			case 46:
				strFieldName = "PublicAcc";
				break;
			case 47:
				strFieldName = "CalPremType";
				break;
			case 48:
				strFieldName = "EdorNo";
				break;
			case 49:
				strFieldName = "EdorValiDate";
				break;
			case 50:
				strFieldName = "Prem";
				break;
			case 51:
				strFieldName = "EdorPrem";
				break;
			case 52:
				strFieldName = "GrpNo";
				break;
			case 53:
				strFieldName = "GrpName";
				break;
			case 54:
				strFieldName = "SocialCenterNo";
				break;
			case 55:
				strFieldName = "SocialCenterName";
				break;
			case 56:
				strFieldName = "Operator";
				break;
			case 57:
				strFieldName = "MakeDate";
				break;
			case 58:
				strFieldName = "MakeTime";
				break;
			case 59:
				strFieldName = "ModifyDate";
				break;
			case 60:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SeqNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainInsuID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OthIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PersonType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialSecurityNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContactInformation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredAppAge") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Retire") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EmployeeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Relation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnglishName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Position") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalRule") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NoNamePeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataChkFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChkRemark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalRemark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PublicAccType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PublicAcc") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CalPremType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorPrem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialCenterNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialCenterName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_INT;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_INT;
				break;
			case 37:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 38:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
