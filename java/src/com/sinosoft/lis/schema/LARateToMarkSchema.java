/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LARateToMarkDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LARateToMarkSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LARateToMarkSchema implements Schema
{
    // @Field
    /** 代理人职级 */
    private String AgentGrade;
    /** 展业类型 */
    private String BranchType;
    /** 达成类型 */
    private String RateType;
    /** 起始业务达成 */
    private double StartRate;
    /** 终止业务达成 */
    private double endRate;
    /** 评分 */
    private double Mark;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LARateToMarkSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "AgentGrade";
        pk[1] = "RateType";
        pk[2] = "StartRate";
        pk[3] = "endRate";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getRateType()
    {
        if (SysConst.CHANGECHARSET && RateType != null && !RateType.equals(""))
        {
            RateType = StrTool.unicodeToGBK(RateType);
        }
        return RateType;
    }

    public void setRateType(String aRateType)
    {
        RateType = aRateType;
    }

    public double getStartRate()
    {
        return StartRate;
    }

    public void setStartRate(double aStartRate)
    {
        StartRate = aStartRate;
    }

    public void setStartRate(String aStartRate)
    {
        if (aStartRate != null && !aStartRate.equals(""))
        {
            Double tDouble = new Double(aStartRate);
            double d = tDouble.doubleValue();
            StartRate = d;
        }
    }

    public double getendRate()
    {
        return endRate;
    }

    public void setendRate(double aendRate)
    {
        endRate = aendRate;
    }

    public void setendRate(String aendRate)
    {
        if (aendRate != null && !aendRate.equals(""))
        {
            Double tDouble = new Double(aendRate);
            double d = tDouble.doubleValue();
            endRate = d;
        }
    }

    public double getMark()
    {
        return Mark;
    }

    public void setMark(double aMark)
    {
        Mark = aMark;
    }

    public void setMark(String aMark)
    {
        if (aMark != null && !aMark.equals(""))
        {
            Double tDouble = new Double(aMark);
            double d = tDouble.doubleValue();
            Mark = d;
        }
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LARateToMarkSchema 对象给 Schema 赋值
     * @param: aLARateToMarkSchema LARateToMarkSchema
     **/
    public void setSchema(LARateToMarkSchema aLARateToMarkSchema)
    {
        this.AgentGrade = aLARateToMarkSchema.getAgentGrade();
        this.BranchType = aLARateToMarkSchema.getBranchType();
        this.RateType = aLARateToMarkSchema.getRateType();
        this.StartRate = aLARateToMarkSchema.getStartRate();
        this.endRate = aLARateToMarkSchema.getendRate();
        this.Mark = aLARateToMarkSchema.getMark();
        this.BranchType2 = aLARateToMarkSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("RateType") == null)
            {
                this.RateType = null;
            }
            else
            {
                this.RateType = rs.getString("RateType").trim();
            }

            this.StartRate = rs.getDouble("StartRate");
            this.endRate = rs.getDouble("endRate");
            this.Mark = rs.getDouble("Mark");
            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LARateToMarkSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LARateToMarkSchema getSchema()
    {
        LARateToMarkSchema aLARateToMarkSchema = new LARateToMarkSchema();
        aLARateToMarkSchema.setSchema(this);
        return aLARateToMarkSchema;
    }

    public LARateToMarkDB getDB()
    {
        LARateToMarkDB aDBOper = new LARateToMarkDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARateToMark描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RateType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StartRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(endRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Mark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARateToMark>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            RateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            StartRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            endRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            Mark = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    6, SysConst.PACKAGESPILTER))).doubleValue();
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LARateToMarkSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("RateType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RateType));
        }
        if (FCode.equals("StartRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartRate));
        }
        if (FCode.equals("endRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(endRate));
        }
        if (FCode.equals("Mark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mark));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RateType);
                break;
            case 3:
                strFieldValue = String.valueOf(StartRate);
                break;
            case 4:
                strFieldValue = String.valueOf(endRate);
                break;
            case 5:
                strFieldValue = String.valueOf(Mark);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("RateType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RateType = FValue.trim();
            }
            else
            {
                RateType = null;
            }
        }
        if (FCode.equals("StartRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StartRate = d;
            }
        }
        if (FCode.equals("endRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                endRate = d;
            }
        }
        if (FCode.equals("Mark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Mark = d;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LARateToMarkSchema other = (LARateToMarkSchema) otherObject;
        return
                AgentGrade.equals(other.getAgentGrade())
                && BranchType.equals(other.getBranchType())
                && RateType.equals(other.getRateType())
                && StartRate == other.getStartRate()
                && endRate == other.getendRate()
                && Mark == other.getMark()
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return 0;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 1;
        }
        if (strFieldName.equals("RateType"))
        {
            return 2;
        }
        if (strFieldName.equals("StartRate"))
        {
            return 3;
        }
        if (strFieldName.equals("endRate"))
        {
            return 4;
        }
        if (strFieldName.equals("Mark"))
        {
            return 5;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentGrade";
                break;
            case 1:
                strFieldName = "BranchType";
                break;
            case 2:
                strFieldName = "RateType";
                break;
            case 3:
                strFieldName = "StartRate";
                break;
            case 4:
                strFieldName = "endRate";
                break;
            case 5:
                strFieldName = "Mark";
                break;
            case 6:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RateType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("endRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Mark"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
