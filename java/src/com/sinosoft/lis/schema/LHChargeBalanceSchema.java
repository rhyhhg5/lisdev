/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHChargeBalanceDB;

/*
 * <p>ClassName: LHChargeBalanceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表修改-合同责任结算-20061206
 * @CreateDate：2006-12-06
 */
public class LHChargeBalanceSchema implements Schema, Cloneable {
    // @Field
    /** 合同编号 */
    private String ContraNo;
    /** 合同责任项目编号 */
    private String ContraItemNo;
    /** 结算方式编码 */
    private String ContBalanceNo;
    /** 费用结算时间 */
    private Date BalanceDate;
    /** 结算提醒时间 */
    private Date BalanceRemindDate;
    /** 结算方式 */
    private String BalanceType;
    /** 银行账号 */
    private String Accounts;
    /** 结算时间单位 */
    private String BalanceUnit;
    /** 结算时间间隔 */
    private String BalanceInt;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 医院代码 */
    private String HospitCode;
    /** 帐户名 */
    private String AccName;
    /** 银行编码 */
    private String BankCode;
    /** 银行名称 */
    private String BankName;
    /** 领取人 */
    private String Drawer;
    /** 领取人身份证号 */
    private String DrawerID;
    /** 票据号 */
    private String ChequeNo;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHChargeBalanceSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ContraNo";
        pk[1] = "ContraItemNo";
        pk[2] = "ContBalanceNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHChargeBalanceSchema cloned = (LHChargeBalanceSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getContraNo() {
        return ContraNo;
    }

    public void setContraNo(String aContraNo) {
        ContraNo = aContraNo;
    }

    public String getContraItemNo() {
        return ContraItemNo;
    }

    public void setContraItemNo(String aContraItemNo) {
        ContraItemNo = aContraItemNo;
    }

    public String getContBalanceNo() {
        return ContBalanceNo;
    }

    public void setContBalanceNo(String aContBalanceNo) {
        ContBalanceNo = aContBalanceNo;
    }

    public String getBalanceDate() {
        if (BalanceDate != null) {
            return fDate.getString(BalanceDate);
        } else {
            return null;
        }
    }

    public void setBalanceDate(Date aBalanceDate) {
        BalanceDate = aBalanceDate;
    }

    public void setBalanceDate(String aBalanceDate) {
        if (aBalanceDate != null && !aBalanceDate.equals("")) {
            BalanceDate = fDate.getDate(aBalanceDate);
        } else {
            BalanceDate = null;
        }
    }

    public String getBalanceRemindDate() {
        if (BalanceRemindDate != null) {
            return fDate.getString(BalanceRemindDate);
        } else {
            return null;
        }
    }

    public void setBalanceRemindDate(Date aBalanceRemindDate) {
        BalanceRemindDate = aBalanceRemindDate;
    }

    public void setBalanceRemindDate(String aBalanceRemindDate) {
        if (aBalanceRemindDate != null && !aBalanceRemindDate.equals("")) {
            BalanceRemindDate = fDate.getDate(aBalanceRemindDate);
        } else {
            BalanceRemindDate = null;
        }
    }

    public String getBalanceType() {
        return BalanceType;
    }

    public void setBalanceType(String aBalanceType) {
        BalanceType = aBalanceType;
    }

    public String getAccounts() {
        return Accounts;
    }

    public void setAccounts(String aAccounts) {
        Accounts = aAccounts;
    }

    public String getBalanceUnit() {
        return BalanceUnit;
    }

    public void setBalanceUnit(String aBalanceUnit) {
        BalanceUnit = aBalanceUnit;
    }

    public String getBalanceInt() {
        return BalanceInt;
    }

    public void setBalanceInt(String aBalanceInt) {
        BalanceInt = aBalanceInt;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getHospitCode() {
        return HospitCode;
    }

    public void setHospitCode(String aHospitCode) {
        HospitCode = aHospitCode;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String aAccName) {
        AccName = aAccName;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String aBankName) {
        BankName = aBankName;
    }

    public String getDrawer() {
        return Drawer;
    }

    public void setDrawer(String aDrawer) {
        Drawer = aDrawer;
    }

    public String getDrawerID() {
        return DrawerID;
    }

    public void setDrawerID(String aDrawerID) {
        DrawerID = aDrawerID;
    }

    public String getChequeNo() {
        return ChequeNo;
    }

    public void setChequeNo(String aChequeNo) {
        ChequeNo = aChequeNo;
    }

    /**
     * 使用另外一个 LHChargeBalanceSchema 对象给 Schema 赋值
     * @param: aLHChargeBalanceSchema LHChargeBalanceSchema
     **/
    public void setSchema(LHChargeBalanceSchema aLHChargeBalanceSchema) {
        this.ContraNo = aLHChargeBalanceSchema.getContraNo();
        this.ContraItemNo = aLHChargeBalanceSchema.getContraItemNo();
        this.ContBalanceNo = aLHChargeBalanceSchema.getContBalanceNo();
        this.BalanceDate = fDate.getDate(aLHChargeBalanceSchema.getBalanceDate());
        this.BalanceRemindDate = fDate.getDate(aLHChargeBalanceSchema.
                                               getBalanceRemindDate());
        this.BalanceType = aLHChargeBalanceSchema.getBalanceType();
        this.Accounts = aLHChargeBalanceSchema.getAccounts();
        this.BalanceUnit = aLHChargeBalanceSchema.getBalanceUnit();
        this.BalanceInt = aLHChargeBalanceSchema.getBalanceInt();
        this.ManageCom = aLHChargeBalanceSchema.getManageCom();
        this.Operator = aLHChargeBalanceSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHChargeBalanceSchema.getMakeDate());
        this.MakeTime = aLHChargeBalanceSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHChargeBalanceSchema.getModifyDate());
        this.ModifyTime = aLHChargeBalanceSchema.getModifyTime();
        this.HospitCode = aLHChargeBalanceSchema.getHospitCode();
        this.AccName = aLHChargeBalanceSchema.getAccName();
        this.BankCode = aLHChargeBalanceSchema.getBankCode();
        this.BankName = aLHChargeBalanceSchema.getBankName();
        this.Drawer = aLHChargeBalanceSchema.getDrawer();
        this.DrawerID = aLHChargeBalanceSchema.getDrawerID();
        this.ChequeNo = aLHChargeBalanceSchema.getChequeNo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ContraNo") == null) {
                this.ContraNo = null;
            } else {
                this.ContraNo = rs.getString("ContraNo").trim();
            }

            if (rs.getString("ContraItemNo") == null) {
                this.ContraItemNo = null;
            } else {
                this.ContraItemNo = rs.getString("ContraItemNo").trim();
            }

            if (rs.getString("ContBalanceNo") == null) {
                this.ContBalanceNo = null;
            } else {
                this.ContBalanceNo = rs.getString("ContBalanceNo").trim();
            }

            this.BalanceDate = rs.getDate("BalanceDate");
            this.BalanceRemindDate = rs.getDate("BalanceRemindDate");
            if (rs.getString("BalanceType") == null) {
                this.BalanceType = null;
            } else {
                this.BalanceType = rs.getString("BalanceType").trim();
            }

            if (rs.getString("Accounts") == null) {
                this.Accounts = null;
            } else {
                this.Accounts = rs.getString("Accounts").trim();
            }

            if (rs.getString("BalanceUnit") == null) {
                this.BalanceUnit = null;
            } else {
                this.BalanceUnit = rs.getString("BalanceUnit").trim();
            }

            if (rs.getString("BalanceInt") == null) {
                this.BalanceInt = null;
            } else {
                this.BalanceInt = rs.getString("BalanceInt").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("HospitCode") == null) {
                this.HospitCode = null;
            } else {
                this.HospitCode = rs.getString("HospitCode").trim();
            }

            if (rs.getString("AccName") == null) {
                this.AccName = null;
            } else {
                this.AccName = rs.getString("AccName").trim();
            }

            if (rs.getString("BankCode") == null) {
                this.BankCode = null;
            } else {
                this.BankCode = rs.getString("BankCode").trim();
            }

            if (rs.getString("BankName") == null) {
                this.BankName = null;
            } else {
                this.BankName = rs.getString("BankName").trim();
            }

            if (rs.getString("Drawer") == null) {
                this.Drawer = null;
            } else {
                this.Drawer = rs.getString("Drawer").trim();
            }

            if (rs.getString("DrawerID") == null) {
                this.DrawerID = null;
            } else {
                this.DrawerID = rs.getString("DrawerID").trim();
            }

            if (rs.getString("ChequeNo") == null) {
                this.ChequeNo = null;
            } else {
                this.ChequeNo = rs.getString("ChequeNo").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHChargeBalance表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHChargeBalanceSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHChargeBalanceSchema getSchema() {
        LHChargeBalanceSchema aLHChargeBalanceSchema = new
                LHChargeBalanceSchema();
        aLHChargeBalanceSchema.setSchema(this);
        return aLHChargeBalanceSchema;
    }

    public LHChargeBalanceDB getDB() {
        LHChargeBalanceDB aDBOper = new LHChargeBalanceDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHChargeBalance描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ContraNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContraItemNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContBalanceNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(BalanceDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(BalanceRemindDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BalanceType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Accounts));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BalanceUnit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BalanceInt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HospitCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Drawer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DrawerID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChequeNo));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHChargeBalance>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ContraNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ContraItemNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ContBalanceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            BalanceDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            BalanceRemindDate = fDate.getDate(StrTool.getStr(StrTool.
                    GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER));
            BalanceType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            Accounts = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            BalanceUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            BalanceInt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            HospitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                     SysConst.PACKAGESPILTER);
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            BankName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            Drawer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                    SysConst.PACKAGESPILTER);
            DrawerID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            ChequeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHChargeBalanceSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ContraNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContraNo));
        }
        if (FCode.equals("ContraItemNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContraItemNo));
        }
        if (FCode.equals("ContBalanceNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContBalanceNo));
        }
        if (FCode.equals("BalanceDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getBalanceDate()));
        }
        if (FCode.equals("BalanceRemindDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getBalanceRemindDate()));
        }
        if (FCode.equals("BalanceType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceType));
        }
        if (FCode.equals("Accounts")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Accounts));
        }
        if (FCode.equals("BalanceUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceUnit));
        }
        if (FCode.equals("BalanceInt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceInt));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("HospitCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitCode));
        }
        if (FCode.equals("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equals("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equals("BankName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankName));
        }
        if (FCode.equals("Drawer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Drawer));
        }
        if (FCode.equals("DrawerID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerID));
        }
        if (FCode.equals("ChequeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChequeNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ContraNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ContraItemNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ContBalanceNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getBalanceDate()));
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getBalanceRemindDate()));
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(BalanceType);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Accounts);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(BalanceUnit);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(BalanceInt);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(HospitCode);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(AccName);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(BankCode);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(BankName);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(Drawer);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(DrawerID);
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(ChequeNo);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ContraNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContraNo = FValue.trim();
            } else {
                ContraNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContraItemNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContraItemNo = FValue.trim();
            } else {
                ContraItemNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContBalanceNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContBalanceNo = FValue.trim();
            } else {
                ContBalanceNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("BalanceDate")) {
            if (FValue != null && !FValue.equals("")) {
                BalanceDate = fDate.getDate(FValue);
            } else {
                BalanceDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("BalanceRemindDate")) {
            if (FValue != null && !FValue.equals("")) {
                BalanceRemindDate = fDate.getDate(FValue);
            } else {
                BalanceRemindDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("BalanceType")) {
            if (FValue != null && !FValue.equals("")) {
                BalanceType = FValue.trim();
            } else {
                BalanceType = null;
            }
        }
        if (FCode.equalsIgnoreCase("Accounts")) {
            if (FValue != null && !FValue.equals("")) {
                Accounts = FValue.trim();
            } else {
                Accounts = null;
            }
        }
        if (FCode.equalsIgnoreCase("BalanceUnit")) {
            if (FValue != null && !FValue.equals("")) {
                BalanceUnit = FValue.trim();
            } else {
                BalanceUnit = null;
            }
        }
        if (FCode.equalsIgnoreCase("BalanceInt")) {
            if (FValue != null && !FValue.equals("")) {
                BalanceInt = FValue.trim();
            } else {
                BalanceInt = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("HospitCode")) {
            if (FValue != null && !FValue.equals("")) {
                HospitCode = FValue.trim();
            } else {
                HospitCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if (FValue != null && !FValue.equals("")) {
                AccName = FValue.trim();
            } else {
                AccName = null;
            }
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if (FValue != null && !FValue.equals("")) {
                BankCode = FValue.trim();
            } else {
                BankCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BankName")) {
            if (FValue != null && !FValue.equals("")) {
                BankName = FValue.trim();
            } else {
                BankName = null;
            }
        }
        if (FCode.equalsIgnoreCase("Drawer")) {
            if (FValue != null && !FValue.equals("")) {
                Drawer = FValue.trim();
            } else {
                Drawer = null;
            }
        }
        if (FCode.equalsIgnoreCase("DrawerID")) {
            if (FValue != null && !FValue.equals("")) {
                DrawerID = FValue.trim();
            } else {
                DrawerID = null;
            }
        }
        if (FCode.equalsIgnoreCase("ChequeNo")) {
            if (FValue != null && !FValue.equals("")) {
                ChequeNo = FValue.trim();
            } else {
                ChequeNo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHChargeBalanceSchema other = (LHChargeBalanceSchema) otherObject;
        return
                ContraNo.equals(other.getContraNo())
                && ContraItemNo.equals(other.getContraItemNo())
                && ContBalanceNo.equals(other.getContBalanceNo())
                && fDate.getString(BalanceDate).equals(other.getBalanceDate())
                &&
                fDate.getString(BalanceRemindDate).equals(other.getBalanceRemindDate())
                && BalanceType.equals(other.getBalanceType())
                && Accounts.equals(other.getAccounts())
                && BalanceUnit.equals(other.getBalanceUnit())
                && BalanceInt.equals(other.getBalanceInt())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && HospitCode.equals(other.getHospitCode())
                && AccName.equals(other.getAccName())
                && BankCode.equals(other.getBankCode())
                && BankName.equals(other.getBankName())
                && Drawer.equals(other.getDrawer())
                && DrawerID.equals(other.getDrawerID())
                && ChequeNo.equals(other.getChequeNo());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ContraNo")) {
            return 0;
        }
        if (strFieldName.equals("ContraItemNo")) {
            return 1;
        }
        if (strFieldName.equals("ContBalanceNo")) {
            return 2;
        }
        if (strFieldName.equals("BalanceDate")) {
            return 3;
        }
        if (strFieldName.equals("BalanceRemindDate")) {
            return 4;
        }
        if (strFieldName.equals("BalanceType")) {
            return 5;
        }
        if (strFieldName.equals("Accounts")) {
            return 6;
        }
        if (strFieldName.equals("BalanceUnit")) {
            return 7;
        }
        if (strFieldName.equals("BalanceInt")) {
            return 8;
        }
        if (strFieldName.equals("ManageCom")) {
            return 9;
        }
        if (strFieldName.equals("Operator")) {
            return 10;
        }
        if (strFieldName.equals("MakeDate")) {
            return 11;
        }
        if (strFieldName.equals("MakeTime")) {
            return 12;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 13;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 14;
        }
        if (strFieldName.equals("HospitCode")) {
            return 15;
        }
        if (strFieldName.equals("AccName")) {
            return 16;
        }
        if (strFieldName.equals("BankCode")) {
            return 17;
        }
        if (strFieldName.equals("BankName")) {
            return 18;
        }
        if (strFieldName.equals("Drawer")) {
            return 19;
        }
        if (strFieldName.equals("DrawerID")) {
            return 20;
        }
        if (strFieldName.equals("ChequeNo")) {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ContraNo";
            break;
        case 1:
            strFieldName = "ContraItemNo";
            break;
        case 2:
            strFieldName = "ContBalanceNo";
            break;
        case 3:
            strFieldName = "BalanceDate";
            break;
        case 4:
            strFieldName = "BalanceRemindDate";
            break;
        case 5:
            strFieldName = "BalanceType";
            break;
        case 6:
            strFieldName = "Accounts";
            break;
        case 7:
            strFieldName = "BalanceUnit";
            break;
        case 8:
            strFieldName = "BalanceInt";
            break;
        case 9:
            strFieldName = "ManageCom";
            break;
        case 10:
            strFieldName = "Operator";
            break;
        case 11:
            strFieldName = "MakeDate";
            break;
        case 12:
            strFieldName = "MakeTime";
            break;
        case 13:
            strFieldName = "ModifyDate";
            break;
        case 14:
            strFieldName = "ModifyTime";
            break;
        case 15:
            strFieldName = "HospitCode";
            break;
        case 16:
            strFieldName = "AccName";
            break;
        case 17:
            strFieldName = "BankCode";
            break;
        case 18:
            strFieldName = "BankName";
            break;
        case 19:
            strFieldName = "Drawer";
            break;
        case 20:
            strFieldName = "DrawerID";
            break;
        case 21:
            strFieldName = "ChequeNo";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ContraNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContraItemNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContBalanceNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BalanceDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BalanceRemindDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BalanceType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Accounts")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BalanceUnit")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BalanceInt")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HospitCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Drawer")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrawerID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChequeNo")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 4:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 21:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
