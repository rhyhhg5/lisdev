/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.ybtsynfeeDB;

/*
 * <p>ClassName: ybtsynfeeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2019-06-26
 */
public class ybtsynfeeSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String serialno;
	/** 保单号 */
	private String contno;
	/** 交退费编号 */
	private String paycode;
	/** 客户号 */
	private String customerno;
	/** 客户姓名 */
	private String customername;
	/** 客户证件类型 */
	private String idtype;
	/** 客户证件号码 */
	private String idno;
	/** 缴费方式 */
	private String paymode;
	/** 卡号 */
	private String accno;
	/** 缴费类型 */
	private String paytype;
	/** 本次缴费金额 */
	private double prem;
	/** 缴费日期 */
	private Date paydate;
	/** 缴费次数 */
	private String count;
	/** 累计缴费金额 */
	private double sumprem;
	/** 续期缴费期数 */
	private String paycount;
	/** 累计续期缴费金额 */
	private double summoney;
	/** 当前保单现价 */
	private double cash;
	/** 数据状态 */
	private String status;
	/** 创建日期 */
	private Date makedate;
	/** 创建时间 */
	private String maketime;
	/** 修改日期 */
	private Date modifydate;
	/** 修改时间 */
	private String modifytime;
	/** 备用字段1 */
	private String bak1;
	/** 备用字段2 */
	private String bak2;
	/** 备用字段3 */
	private String bak3;
	/** 备用字段4 */
	private String bak4;
	/** 备用字段5 */
	private String bak5;

	public static final int FIELDNUM = 27;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public ybtsynfeeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "serialno";
		pk[1] = "contno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		ybtsynfeeSchema cloned = (ybtsynfeeSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getserialno()
	{
		return serialno;
	}
	public void setserialno(String aserialno)
	{
		serialno = aserialno;
	}
	public String getcontno()
	{
		return contno;
	}
	public void setcontno(String acontno)
	{
		contno = acontno;
	}
	public String getpaycode()
	{
		return paycode;
	}
	public void setpaycode(String apaycode)
	{
		paycode = apaycode;
	}
	public String getcustomerno()
	{
		return customerno;
	}
	public void setcustomerno(String acustomerno)
	{
		customerno = acustomerno;
	}
	public String getcustomername()
	{
		return customername;
	}
	public void setcustomername(String acustomername)
	{
		customername = acustomername;
	}
	public String getidtype()
	{
		return idtype;
	}
	public void setidtype(String aidtype)
	{
		idtype = aidtype;
	}
	public String getidno()
	{
		return idno;
	}
	public void setidno(String aidno)
	{
		idno = aidno;
	}
	public String getpaymode()
	{
		return paymode;
	}
	public void setpaymode(String apaymode)
	{
		paymode = apaymode;
	}
	public String getaccno()
	{
		return accno;
	}
	public void setaccno(String aaccno)
	{
		accno = aaccno;
	}
	public String getpaytype()
	{
		return paytype;
	}
	public void setpaytype(String apaytype)
	{
		paytype = apaytype;
	}
	public double getprem()
	{
		return prem;
	}
	public void setprem(double aprem)
	{
		prem = Arith.round(aprem,2);
	}
	public void setprem(String aprem)
	{
		if (aprem != null && !aprem.equals(""))
		{
			Double tDouble = new Double(aprem);
			double d = tDouble.doubleValue();
                prem = Arith.round(d,2);
		}
	}

	public String getpaydate()
	{
		if( paydate != null )
			return fDate.getString(paydate);
		else
			return null;
	}
	public void setpaydate(Date apaydate)
	{
		paydate = apaydate;
	}
	public void setpaydate(String apaydate)
	{
		if (apaydate != null && !apaydate.equals("") )
		{
			paydate = fDate.getDate( apaydate );
		}
		else
			paydate = null;
	}

	public String getcount()
	{
		return count;
	}
	public void setcount(String acount)
	{
		count = acount;
	}
	public double getsumprem()
	{
		return sumprem;
	}
	public void setsumprem(double asumprem)
	{
		sumprem = Arith.round(asumprem,2);
	}
	public void setsumprem(String asumprem)
	{
		if (asumprem != null && !asumprem.equals(""))
		{
			Double tDouble = new Double(asumprem);
			double d = tDouble.doubleValue();
                sumprem = Arith.round(d,2);
		}
	}

	public String getpaycount()
	{
		return paycount;
	}
	public void setpaycount(String apaycount)
	{
		paycount = apaycount;
	}
	public double getsummoney()
	{
		return summoney;
	}
	public void setsummoney(double asummoney)
	{
		summoney = Arith.round(asummoney,2);
	}
	public void setsummoney(String asummoney)
	{
		if (asummoney != null && !asummoney.equals(""))
		{
			Double tDouble = new Double(asummoney);
			double d = tDouble.doubleValue();
                summoney = Arith.round(d,2);
		}
	}

	public double getcash()
	{
		return cash;
	}
	public void setcash(double acash)
	{
		cash = Arith.round(acash,2);
	}
	public void setcash(String acash)
	{
		if (acash != null && !acash.equals(""))
		{
			Double tDouble = new Double(acash);
			double d = tDouble.doubleValue();
                cash = Arith.round(d,2);
		}
	}

	public String getstatus()
	{
		return status;
	}
	public void setstatus(String astatus)
	{
		status = astatus;
	}
	public String getmakedate()
	{
		if( makedate != null )
			return fDate.getString(makedate);
		else
			return null;
	}
	public void setmakedate(Date amakedate)
	{
		makedate = amakedate;
	}
	public void setmakedate(String amakedate)
	{
		if (amakedate != null && !amakedate.equals("") )
		{
			makedate = fDate.getDate( amakedate );
		}
		else
			makedate = null;
	}

	public String getmaketime()
	{
		return maketime;
	}
	public void setmaketime(String amaketime)
	{
		maketime = amaketime;
	}
	public String getmodifydate()
	{
		if( modifydate != null )
			return fDate.getString(modifydate);
		else
			return null;
	}
	public void setmodifydate(Date amodifydate)
	{
		modifydate = amodifydate;
	}
	public void setmodifydate(String amodifydate)
	{
		if (amodifydate != null && !amodifydate.equals("") )
		{
			modifydate = fDate.getDate( amodifydate );
		}
		else
			modifydate = null;
	}

	public String getmodifytime()
	{
		return modifytime;
	}
	public void setmodifytime(String amodifytime)
	{
		modifytime = amodifytime;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}
	public String getbak4()
	{
		return bak4;
	}
	public void setbak4(String abak4)
	{
		bak4 = abak4;
	}
	public String getbak5()
	{
		return bak5;
	}
	public void setbak5(String abak5)
	{
		bak5 = abak5;
	}

	/**
	* 使用另外一个 ybtsynfeeSchema 对象给 Schema 赋值
	* @param: aybtsynfeeSchema ybtsynfeeSchema
	**/
	public void setSchema(ybtsynfeeSchema aybtsynfeeSchema)
	{
		this.serialno = aybtsynfeeSchema.getserialno();
		this.contno = aybtsynfeeSchema.getcontno();
		this.paycode = aybtsynfeeSchema.getpaycode();
		this.customerno = aybtsynfeeSchema.getcustomerno();
		this.customername = aybtsynfeeSchema.getcustomername();
		this.idtype = aybtsynfeeSchema.getidtype();
		this.idno = aybtsynfeeSchema.getidno();
		this.paymode = aybtsynfeeSchema.getpaymode();
		this.accno = aybtsynfeeSchema.getaccno();
		this.paytype = aybtsynfeeSchema.getpaytype();
		this.prem = aybtsynfeeSchema.getprem();
		this.paydate = fDate.getDate( aybtsynfeeSchema.getpaydate());
		this.count = aybtsynfeeSchema.getcount();
		this.sumprem = aybtsynfeeSchema.getsumprem();
		this.paycount = aybtsynfeeSchema.getpaycount();
		this.summoney = aybtsynfeeSchema.getsummoney();
		this.cash = aybtsynfeeSchema.getcash();
		this.status = aybtsynfeeSchema.getstatus();
		this.makedate = fDate.getDate( aybtsynfeeSchema.getmakedate());
		this.maketime = aybtsynfeeSchema.getmaketime();
		this.modifydate = fDate.getDate( aybtsynfeeSchema.getmodifydate());
		this.modifytime = aybtsynfeeSchema.getmodifytime();
		this.bak1 = aybtsynfeeSchema.getbak1();
		this.bak2 = aybtsynfeeSchema.getbak2();
		this.bak3 = aybtsynfeeSchema.getbak3();
		this.bak4 = aybtsynfeeSchema.getbak4();
		this.bak5 = aybtsynfeeSchema.getbak5();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("serialno") == null )
				this.serialno = null;
			else
				this.serialno = rs.getString("serialno").trim();

			if( rs.getString("contno") == null )
				this.contno = null;
			else
				this.contno = rs.getString("contno").trim();

			if( rs.getString("paycode") == null )
				this.paycode = null;
			else
				this.paycode = rs.getString("paycode").trim();

			if( rs.getString("customerno") == null )
				this.customerno = null;
			else
				this.customerno = rs.getString("customerno").trim();

			if( rs.getString("customername") == null )
				this.customername = null;
			else
				this.customername = rs.getString("customername").trim();

			if( rs.getString("idtype") == null )
				this.idtype = null;
			else
				this.idtype = rs.getString("idtype").trim();

			if( rs.getString("idno") == null )
				this.idno = null;
			else
				this.idno = rs.getString("idno").trim();

			if( rs.getString("paymode") == null )
				this.paymode = null;
			else
				this.paymode = rs.getString("paymode").trim();

			if( rs.getString("accno") == null )
				this.accno = null;
			else
				this.accno = rs.getString("accno").trim();

			if( rs.getString("paytype") == null )
				this.paytype = null;
			else
				this.paytype = rs.getString("paytype").trim();

			this.prem = rs.getDouble("prem");
			this.paydate = rs.getDate("paydate");
			if( rs.getString("count") == null )
				this.count = null;
			else
				this.count = rs.getString("count").trim();

			this.sumprem = rs.getDouble("sumprem");
			if( rs.getString("paycount") == null )
				this.paycount = null;
			else
				this.paycount = rs.getString("paycount").trim();

			this.summoney = rs.getDouble("summoney");
			this.cash = rs.getDouble("cash");
			if( rs.getString("status") == null )
				this.status = null;
			else
				this.status = rs.getString("status").trim();

			this.makedate = rs.getDate("makedate");
			if( rs.getString("maketime") == null )
				this.maketime = null;
			else
				this.maketime = rs.getString("maketime").trim();

			this.modifydate = rs.getDate("modifydate");
			if( rs.getString("modifytime") == null )
				this.modifytime = null;
			else
				this.modifytime = rs.getString("modifytime").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

			if( rs.getString("bak4") == null )
				this.bak4 = null;
			else
				this.bak4 = rs.getString("bak4").trim();

			if( rs.getString("bak5") == null )
				this.bak5 = null;
			else
				this.bak5 = rs.getString("bak5").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的ybtsynfee表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ybtsynfeeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public ybtsynfeeSchema getSchema()
	{
		ybtsynfeeSchema aybtsynfeeSchema = new ybtsynfeeSchema();
		aybtsynfeeSchema.setSchema(this);
		return aybtsynfeeSchema;
	}

	public ybtsynfeeDB getDB()
	{
		ybtsynfeeDB aDBOper = new ybtsynfeeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpybtsynfee描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(serialno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(contno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paycode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(customerno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(customername)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(idtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(idno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paymode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(accno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paytype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( paydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(count)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(sumprem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paycount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(summoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(cash));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(maketime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( modifydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifytime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak5));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpybtsynfee>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			serialno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			contno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			paycode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			customerno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			customername = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			idtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			idno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			paymode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			accno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			paytype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			paydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			count = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			sumprem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			paycount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			summoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			cash = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			modifydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			modifytime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			bak5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ybtsynfeeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("serialno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(serialno));
		}
		if (FCode.equals("contno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(contno));
		}
		if (FCode.equals("paycode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paycode));
		}
		if (FCode.equals("customerno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(customerno));
		}
		if (FCode.equals("customername"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(customername));
		}
		if (FCode.equals("idtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(idtype));
		}
		if (FCode.equals("idno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(idno));
		}
		if (FCode.equals("paymode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paymode));
		}
		if (FCode.equals("accno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(accno));
		}
		if (FCode.equals("paytype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paytype));
		}
		if (FCode.equals("prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prem));
		}
		if (FCode.equals("paydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getpaydate()));
		}
		if (FCode.equals("count"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(count));
		}
		if (FCode.equals("sumprem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sumprem));
		}
		if (FCode.equals("paycount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paycount));
		}
		if (FCode.equals("summoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(summoney));
		}
		if (FCode.equals("cash"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cash));
		}
		if (FCode.equals("status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(status));
		}
		if (FCode.equals("makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakedate()));
		}
		if (FCode.equals("maketime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(maketime));
		}
		if (FCode.equals("modifydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmodifydate()));
		}
		if (FCode.equals("modifytime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifytime));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (FCode.equals("bak4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak4));
		}
		if (FCode.equals("bak5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak5));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(serialno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(contno);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(paycode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(customerno);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(customername);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(idtype);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(idno);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(paymode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(accno);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(paytype);
				break;
			case 10:
				strFieldValue = String.valueOf(prem);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getpaydate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(count);
				break;
			case 13:
				strFieldValue = String.valueOf(sumprem);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(paycount);
				break;
			case 15:
				strFieldValue = String.valueOf(summoney);
				break;
			case 16:
				strFieldValue = String.valueOf(cash);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(status);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakedate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(maketime);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmodifydate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(modifytime);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(bak4);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(bak5);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("serialno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				serialno = FValue.trim();
			}
			else
				serialno = null;
		}
		if (FCode.equalsIgnoreCase("contno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				contno = FValue.trim();
			}
			else
				contno = null;
		}
		if (FCode.equalsIgnoreCase("paycode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paycode = FValue.trim();
			}
			else
				paycode = null;
		}
		if (FCode.equalsIgnoreCase("customerno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				customerno = FValue.trim();
			}
			else
				customerno = null;
		}
		if (FCode.equalsIgnoreCase("customername"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				customername = FValue.trim();
			}
			else
				customername = null;
		}
		if (FCode.equalsIgnoreCase("idtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				idtype = FValue.trim();
			}
			else
				idtype = null;
		}
		if (FCode.equalsIgnoreCase("idno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				idno = FValue.trim();
			}
			else
				idno = null;
		}
		if (FCode.equalsIgnoreCase("paymode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paymode = FValue.trim();
			}
			else
				paymode = null;
		}
		if (FCode.equalsIgnoreCase("accno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				accno = FValue.trim();
			}
			else
				accno = null;
		}
		if (FCode.equalsIgnoreCase("paytype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paytype = FValue.trim();
			}
			else
				paytype = null;
		}
		if (FCode.equalsIgnoreCase("prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("paydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				paydate = fDate.getDate( FValue );
			}
			else
				paydate = null;
		}
		if (FCode.equalsIgnoreCase("count"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				count = FValue.trim();
			}
			else
				count = null;
		}
		if (FCode.equalsIgnoreCase("sumprem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				sumprem = d;
			}
		}
		if (FCode.equalsIgnoreCase("paycount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paycount = FValue.trim();
			}
			else
				paycount = null;
		}
		if (FCode.equalsIgnoreCase("summoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				summoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("cash"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				cash = d;
			}
		}
		if (FCode.equalsIgnoreCase("status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				status = FValue.trim();
			}
			else
				status = null;
		}
		if (FCode.equalsIgnoreCase("makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makedate = fDate.getDate( FValue );
			}
			else
				makedate = null;
		}
		if (FCode.equalsIgnoreCase("maketime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				maketime = FValue.trim();
			}
			else
				maketime = null;
		}
		if (FCode.equalsIgnoreCase("modifydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				modifydate = fDate.getDate( FValue );
			}
			else
				modifydate = null;
		}
		if (FCode.equalsIgnoreCase("modifytime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifytime = FValue.trim();
			}
			else
				modifytime = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		if (FCode.equalsIgnoreCase("bak4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak4 = FValue.trim();
			}
			else
				bak4 = null;
		}
		if (FCode.equalsIgnoreCase("bak5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak5 = FValue.trim();
			}
			else
				bak5 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		ybtsynfeeSchema other = (ybtsynfeeSchema)otherObject;
		return
			(serialno == null ? other.getserialno() == null : serialno.equals(other.getserialno()))
			&& (contno == null ? other.getcontno() == null : contno.equals(other.getcontno()))
			&& (paycode == null ? other.getpaycode() == null : paycode.equals(other.getpaycode()))
			&& (customerno == null ? other.getcustomerno() == null : customerno.equals(other.getcustomerno()))
			&& (customername == null ? other.getcustomername() == null : customername.equals(other.getcustomername()))
			&& (idtype == null ? other.getidtype() == null : idtype.equals(other.getidtype()))
			&& (idno == null ? other.getidno() == null : idno.equals(other.getidno()))
			&& (paymode == null ? other.getpaymode() == null : paymode.equals(other.getpaymode()))
			&& (accno == null ? other.getaccno() == null : accno.equals(other.getaccno()))
			&& (paytype == null ? other.getpaytype() == null : paytype.equals(other.getpaytype()))
			&& prem == other.getprem()
			&& (paydate == null ? other.getpaydate() == null : fDate.getString(paydate).equals(other.getpaydate()))
			&& (count == null ? other.getcount() == null : count.equals(other.getcount()))
			&& sumprem == other.getsumprem()
			&& (paycount == null ? other.getpaycount() == null : paycount.equals(other.getpaycount()))
			&& summoney == other.getsummoney()
			&& cash == other.getcash()
			&& (status == null ? other.getstatus() == null : status.equals(other.getstatus()))
			&& (makedate == null ? other.getmakedate() == null : fDate.getString(makedate).equals(other.getmakedate()))
			&& (maketime == null ? other.getmaketime() == null : maketime.equals(other.getmaketime()))
			&& (modifydate == null ? other.getmodifydate() == null : fDate.getString(modifydate).equals(other.getmodifydate()))
			&& (modifytime == null ? other.getmodifytime() == null : modifytime.equals(other.getmodifytime()))
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()))
			&& (bak4 == null ? other.getbak4() == null : bak4.equals(other.getbak4()))
			&& (bak5 == null ? other.getbak5() == null : bak5.equals(other.getbak5()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("serialno") ) {
			return 0;
		}
		if( strFieldName.equals("contno") ) {
			return 1;
		}
		if( strFieldName.equals("paycode") ) {
			return 2;
		}
		if( strFieldName.equals("customerno") ) {
			return 3;
		}
		if( strFieldName.equals("customername") ) {
			return 4;
		}
		if( strFieldName.equals("idtype") ) {
			return 5;
		}
		if( strFieldName.equals("idno") ) {
			return 6;
		}
		if( strFieldName.equals("paymode") ) {
			return 7;
		}
		if( strFieldName.equals("accno") ) {
			return 8;
		}
		if( strFieldName.equals("paytype") ) {
			return 9;
		}
		if( strFieldName.equals("prem") ) {
			return 10;
		}
		if( strFieldName.equals("paydate") ) {
			return 11;
		}
		if( strFieldName.equals("count") ) {
			return 12;
		}
		if( strFieldName.equals("sumprem") ) {
			return 13;
		}
		if( strFieldName.equals("paycount") ) {
			return 14;
		}
		if( strFieldName.equals("summoney") ) {
			return 15;
		}
		if( strFieldName.equals("cash") ) {
			return 16;
		}
		if( strFieldName.equals("status") ) {
			return 17;
		}
		if( strFieldName.equals("makedate") ) {
			return 18;
		}
		if( strFieldName.equals("maketime") ) {
			return 19;
		}
		if( strFieldName.equals("modifydate") ) {
			return 20;
		}
		if( strFieldName.equals("modifytime") ) {
			return 21;
		}
		if( strFieldName.equals("bak1") ) {
			return 22;
		}
		if( strFieldName.equals("bak2") ) {
			return 23;
		}
		if( strFieldName.equals("bak3") ) {
			return 24;
		}
		if( strFieldName.equals("bak4") ) {
			return 25;
		}
		if( strFieldName.equals("bak5") ) {
			return 26;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "serialno";
				break;
			case 1:
				strFieldName = "contno";
				break;
			case 2:
				strFieldName = "paycode";
				break;
			case 3:
				strFieldName = "customerno";
				break;
			case 4:
				strFieldName = "customername";
				break;
			case 5:
				strFieldName = "idtype";
				break;
			case 6:
				strFieldName = "idno";
				break;
			case 7:
				strFieldName = "paymode";
				break;
			case 8:
				strFieldName = "accno";
				break;
			case 9:
				strFieldName = "paytype";
				break;
			case 10:
				strFieldName = "prem";
				break;
			case 11:
				strFieldName = "paydate";
				break;
			case 12:
				strFieldName = "count";
				break;
			case 13:
				strFieldName = "sumprem";
				break;
			case 14:
				strFieldName = "paycount";
				break;
			case 15:
				strFieldName = "summoney";
				break;
			case 16:
				strFieldName = "cash";
				break;
			case 17:
				strFieldName = "status";
				break;
			case 18:
				strFieldName = "makedate";
				break;
			case 19:
				strFieldName = "maketime";
				break;
			case 20:
				strFieldName = "modifydate";
				break;
			case 21:
				strFieldName = "modifytime";
				break;
			case 22:
				strFieldName = "bak1";
				break;
			case 23:
				strFieldName = "bak2";
				break;
			case 24:
				strFieldName = "bak3";
				break;
			case 25:
				strFieldName = "bak4";
				break;
			case 26:
				strFieldName = "bak5";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("serialno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("contno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("paycode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("customerno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("customername") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("idtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("idno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("paymode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("accno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("paytype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("paydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("count") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sumprem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("paycount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("summoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("cash") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("maketime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("modifytime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak5") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
