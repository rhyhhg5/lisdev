/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDRInforDB;

/*
 * <p>ClassName: LDRInforSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: downloadreport
 * @CreateDate：2010-09-03
 */
public class LDRInforSchema implements Schema, Cloneable
{
	// @Field
	/** 编号 */
	private String Number;
	/** 临时报表名 */
	private String TempName;
	/** 用户名 */
	private String UserName;
	/** 报表名称 */
	private String ReportName;
	/** 报表名 */
	private String RptName;
	/** 统计机构 */
	private String ManageCom;
	/** 登录机构 */
	private String MagCom;
	/** 报表状态 */
	private String MakeStatus;
	/** 打印开始日期 */
	private Date MakeDate;
	/** 打印开始时间 */
	private String MakeTime;
	/** 打印结束日期 */
	private Date ModifyDate;
	/** 打印结束时间 */
	private String ModifyTime;
	/** 统计起期 */
	private Date MagStartDate;
	/** 统计止期 */
	private Date MagEndDate;
	/** 备注1 */
	private String Remark1;
	/** 备注2 */
	private String Remark2;
	/** 备注3 */
	private String Remark3;

	public static final int FIELDNUM = 17;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDRInforSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Number";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDRInforSchema cloned = (LDRInforSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getNumber()
	{
		return Number;
	}
	public void setNumber(String aNumber)
	{
		Number = aNumber;
	}
	public String getTempName()
	{
		return TempName;
	}
	public void setTempName(String aTempName)
	{
		TempName = aTempName;
	}
	public String getUserName()
	{
		return UserName;
	}
	public void setUserName(String aUserName)
	{
		UserName = aUserName;
	}
	public String getReportName()
	{
		return ReportName;
	}
	public void setReportName(String aReportName)
	{
		ReportName = aReportName;
	}
	public String getRptName()
	{
		return RptName;
	}
	public void setRptName(String aRptName)
	{
		RptName = aRptName;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getMagCom()
	{
		return MagCom;
	}
	public void setMagCom(String aMagCom)
	{
		MagCom = aMagCom;
	}
	public String getMakeStatus()
	{
		return MakeStatus;
	}
	public void setMakeStatus(String aMakeStatus)
	{
		MakeStatus = aMakeStatus;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getMagStartDate()
	{
		if( MagStartDate != null )
			return fDate.getString(MagStartDate);
		else
			return null;
	}
	public void setMagStartDate(Date aMagStartDate)
	{
		MagStartDate = aMagStartDate;
	}
	public void setMagStartDate(String aMagStartDate)
	{
		if (aMagStartDate != null && !aMagStartDate.equals("") )
		{
			MagStartDate = fDate.getDate( aMagStartDate );
		}
		else
			MagStartDate = null;
	}

	public String getMagEndDate()
	{
		if( MagEndDate != null )
			return fDate.getString(MagEndDate);
		else
			return null;
	}
	public void setMagEndDate(Date aMagEndDate)
	{
		MagEndDate = aMagEndDate;
	}
	public void setMagEndDate(String aMagEndDate)
	{
		if (aMagEndDate != null && !aMagEndDate.equals("") )
		{
			MagEndDate = fDate.getDate( aMagEndDate );
		}
		else
			MagEndDate = null;
	}

	public String getRemark1()
	{
		return Remark1;
	}
	public void setRemark1(String aRemark1)
	{
		Remark1 = aRemark1;
	}
	public String getRemark2()
	{
		return Remark2;
	}
	public void setRemark2(String aRemark2)
	{
		Remark2 = aRemark2;
	}
	public String getRemark3()
	{
		return Remark3;
	}
	public void setRemark3(String aRemark3)
	{
		Remark3 = aRemark3;
	}

	/**
	* 使用另外一个 LDRInforSchema 对象给 Schema 赋值
	* @param: aLDRInforSchema LDRInforSchema
	**/
	public void setSchema(LDRInforSchema aLDRInforSchema)
	{
		this.Number = aLDRInforSchema.getNumber();
		this.TempName = aLDRInforSchema.getTempName();
		this.UserName = aLDRInforSchema.getUserName();
		this.ReportName = aLDRInforSchema.getReportName();
		this.RptName = aLDRInforSchema.getRptName();
		this.ManageCom = aLDRInforSchema.getManageCom();
		this.MagCom = aLDRInforSchema.getMagCom();
		this.MakeStatus = aLDRInforSchema.getMakeStatus();
		this.MakeDate = fDate.getDate( aLDRInforSchema.getMakeDate());
		this.MakeTime = aLDRInforSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLDRInforSchema.getModifyDate());
		this.ModifyTime = aLDRInforSchema.getModifyTime();
		this.MagStartDate = fDate.getDate( aLDRInforSchema.getMagStartDate());
		this.MagEndDate = fDate.getDate( aLDRInforSchema.getMagEndDate());
		this.Remark1 = aLDRInforSchema.getRemark1();
		this.Remark2 = aLDRInforSchema.getRemark2();
		this.Remark3 = aLDRInforSchema.getRemark3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Number") == null )
				this.Number = null;
			else
				this.Number = rs.getString("Number").trim();

			if( rs.getString("TempName") == null )
				this.TempName = null;
			else
				this.TempName = rs.getString("TempName").trim();

			if( rs.getString("UserName") == null )
				this.UserName = null;
			else
				this.UserName = rs.getString("UserName").trim();

			if( rs.getString("ReportName") == null )
				this.ReportName = null;
			else
				this.ReportName = rs.getString("ReportName").trim();

			if( rs.getString("RptName") == null )
				this.RptName = null;
			else
				this.RptName = rs.getString("RptName").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("MagCom") == null )
				this.MagCom = null;
			else
				this.MagCom = rs.getString("MagCom").trim();

			if( rs.getString("MakeStatus") == null )
				this.MakeStatus = null;
			else
				this.MakeStatus = rs.getString("MakeStatus").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.MagStartDate = rs.getDate("MagStartDate");
			this.MagEndDate = rs.getDate("MagEndDate");
			if( rs.getString("Remark1") == null )
				this.Remark1 = null;
			else
				this.Remark1 = rs.getString("Remark1").trim();

			if( rs.getString("Remark2") == null )
				this.Remark2 = null;
			else
				this.Remark2 = rs.getString("Remark2").trim();

			if( rs.getString("Remark3") == null )
				this.Remark3 = null;
			else
				this.Remark3 = rs.getString("Remark3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDRInfor表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDRInforSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDRInforSchema getSchema()
	{
		LDRInforSchema aLDRInforSchema = new LDRInforSchema();
		aLDRInforSchema.setSchema(this);
		return aLDRInforSchema;
	}

	public LDRInforDB getDB()
	{
		LDRInforDB aDBOper = new LDRInforDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRInfor描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Number)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UserName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReportName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RptName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MagCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MagStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MagEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRInfor>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Number = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TempName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			UserName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ReportName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RptName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			MagCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MakeStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MagStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MagEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			Remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Remark3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDRInforSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Number"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Number));
		}
		if (FCode.equals("TempName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempName));
		}
		if (FCode.equals("UserName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UserName));
		}
		if (FCode.equals("ReportName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReportName));
		}
		if (FCode.equals("RptName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RptName));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("MagCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MagCom));
		}
		if (FCode.equals("MakeStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeStatus));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("MagStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMagStartDate()));
		}
		if (FCode.equals("MagEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMagEndDate()));
		}
		if (FCode.equals("Remark1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
		}
		if (FCode.equals("Remark2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
		}
		if (FCode.equals("Remark3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Number);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TempName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(UserName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ReportName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(RptName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(MagCom);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MakeStatus);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMagStartDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMagEndDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Remark1);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Remark2);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Remark3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Number"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Number = FValue.trim();
			}
			else
				Number = null;
		}
		if (FCode.equalsIgnoreCase("TempName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempName = FValue.trim();
			}
			else
				TempName = null;
		}
		if (FCode.equalsIgnoreCase("UserName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UserName = FValue.trim();
			}
			else
				UserName = null;
		}
		if (FCode.equalsIgnoreCase("ReportName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReportName = FValue.trim();
			}
			else
				ReportName = null;
		}
		if (FCode.equalsIgnoreCase("RptName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RptName = FValue.trim();
			}
			else
				RptName = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("MagCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MagCom = FValue.trim();
			}
			else
				MagCom = null;
		}
		if (FCode.equalsIgnoreCase("MakeStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeStatus = FValue.trim();
			}
			else
				MakeStatus = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("MagStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MagStartDate = fDate.getDate( FValue );
			}
			else
				MagStartDate = null;
		}
		if (FCode.equalsIgnoreCase("MagEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MagEndDate = fDate.getDate( FValue );
			}
			else
				MagEndDate = null;
		}
		if (FCode.equalsIgnoreCase("Remark1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark1 = FValue.trim();
			}
			else
				Remark1 = null;
		}
		if (FCode.equalsIgnoreCase("Remark2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark2 = FValue.trim();
			}
			else
				Remark2 = null;
		}
		if (FCode.equalsIgnoreCase("Remark3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark3 = FValue.trim();
			}
			else
				Remark3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDRInforSchema other = (LDRInforSchema)otherObject;
		return
			(Number == null ? other.getNumber() == null : Number.equals(other.getNumber()))
			&& (TempName == null ? other.getTempName() == null : TempName.equals(other.getTempName()))
			&& (UserName == null ? other.getUserName() == null : UserName.equals(other.getUserName()))
			&& (ReportName == null ? other.getReportName() == null : ReportName.equals(other.getReportName()))
			&& (RptName == null ? other.getRptName() == null : RptName.equals(other.getRptName()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (MagCom == null ? other.getMagCom() == null : MagCom.equals(other.getMagCom()))
			&& (MakeStatus == null ? other.getMakeStatus() == null : MakeStatus.equals(other.getMakeStatus()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (MagStartDate == null ? other.getMagStartDate() == null : fDate.getString(MagStartDate).equals(other.getMagStartDate()))
			&& (MagEndDate == null ? other.getMagEndDate() == null : fDate.getString(MagEndDate).equals(other.getMagEndDate()))
			&& (Remark1 == null ? other.getRemark1() == null : Remark1.equals(other.getRemark1()))
			&& (Remark2 == null ? other.getRemark2() == null : Remark2.equals(other.getRemark2()))
			&& (Remark3 == null ? other.getRemark3() == null : Remark3.equals(other.getRemark3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Number") ) {
			return 0;
		}
		if( strFieldName.equals("TempName") ) {
			return 1;
		}
		if( strFieldName.equals("UserName") ) {
			return 2;
		}
		if( strFieldName.equals("ReportName") ) {
			return 3;
		}
		if( strFieldName.equals("RptName") ) {
			return 4;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 5;
		}
		if( strFieldName.equals("MagCom") ) {
			return 6;
		}
		if( strFieldName.equals("MakeStatus") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		if( strFieldName.equals("MagStartDate") ) {
			return 12;
		}
		if( strFieldName.equals("MagEndDate") ) {
			return 13;
		}
		if( strFieldName.equals("Remark1") ) {
			return 14;
		}
		if( strFieldName.equals("Remark2") ) {
			return 15;
		}
		if( strFieldName.equals("Remark3") ) {
			return 16;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Number";
				break;
			case 1:
				strFieldName = "TempName";
				break;
			case 2:
				strFieldName = "UserName";
				break;
			case 3:
				strFieldName = "ReportName";
				break;
			case 4:
				strFieldName = "RptName";
				break;
			case 5:
				strFieldName = "ManageCom";
				break;
			case 6:
				strFieldName = "MagCom";
				break;
			case 7:
				strFieldName = "MakeStatus";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			case 12:
				strFieldName = "MagStartDate";
				break;
			case 13:
				strFieldName = "MagEndDate";
				break;
			case 14:
				strFieldName = "Remark1";
				break;
			case 15:
				strFieldName = "Remark2";
				break;
			case 16:
				strFieldName = "Remark3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Number") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UserName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReportName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RptName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MagCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MagStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MagEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Remark1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
