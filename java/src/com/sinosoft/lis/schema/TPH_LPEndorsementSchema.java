/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.TPH_LPEndorsementDB;

/*
 * <p>ClassName: TPH_LPEndorsementSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 税优平台表机构变更
 * @CreateDate：2015-10-14
 */
public class TPH_LPEndorsementSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String policyNo;
	/** 保单类别 */
	private String policyType;
	/** 保单所属二级机构名称 */
	private String policyOrg;
	/** 保单所属区域 */
	private String policyArea;
	/** 保全类型 */
	private String endorsementType;
	/** 保全批单号 */
	private String endorsementNo;
	/** 保单批单序号 */
	private String polEndSeq;
	/** 批单完成时间 */
	private String finishTime;
	/** 批单状态 */
	private String endorsementStatus;
	/** 保全申请日期 */
	private Date endorsementApplicationDate;
	/** 保全生效日期 */
	private Date endorsementEffectiveDate;
	/** 保单生效时间 */
	private Date effectiveDate;
	/** 保单终止日期 */
	private Date terminationDate;
	/** 保单中止日期 */
	private Date suspendDate;
	/** 保单效力恢复日期 */
	private Date recoverDate;
	/** 保单来源 */
	private String policySource;
	/** 保单状态 */
	private String policyStatus;
	/** 保单终止原因 */
	private String terminationReason;
	/** 缴费频率 */
	private String paymentFrequency;
	/** 退保金 */
	private double surrenderAmount;
	/** 补税金额 */
	private double taxDeduct;
	/** 追加交费金额 */
	private double topUpAmount;
	/** 上报子任务号 */
	private String subTaskNo;
	/** 报送状态 */
	private String sendState;
	/** 平台返回编码 */
	private String returnSequenceNo;
	/** 报送结果信息 */
	private String message;
	/** 管理机构 */
	private String comCode;
	/** 操作人员 */
	private String oper;
	/** 处理日期 */
	private Date makeDate;
	/** 处理时间 */
	private String makeTime;

	public static final int FIELDNUM = 30;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public TPH_LPEndorsementSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "policyNo";
		pk[1] = "endorsementType";
		pk[2] = "endorsementNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		TPH_LPEndorsementSchema cloned = (TPH_LPEndorsementSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getpolicyNo()
	{
		return policyNo;
	}
	public void setpolicyNo(String apolicyNo)
	{
		policyNo = apolicyNo;
	}
	public String getpolicyType()
	{
		return policyType;
	}
	public void setpolicyType(String apolicyType)
	{
		policyType = apolicyType;
	}
	public String getpolicyOrg()
	{
		return policyOrg;
	}
	public void setpolicyOrg(String apolicyOrg)
	{
		policyOrg = apolicyOrg;
	}
	public String getpolicyArea()
	{
		return policyArea;
	}
	public void setpolicyArea(String apolicyArea)
	{
		policyArea = apolicyArea;
	}
	public String getendorsementType()
	{
		return endorsementType;
	}
	public void setendorsementType(String aendorsementType)
	{
		endorsementType = aendorsementType;
	}
	public String getendorsementNo()
	{
		return endorsementNo;
	}
	public void setendorsementNo(String aendorsementNo)
	{
		endorsementNo = aendorsementNo;
	}
	public String getpolEndSeq()
	{
		return polEndSeq;
	}
	public void setpolEndSeq(String apolEndSeq)
	{
		polEndSeq = apolEndSeq;
	}
	public String getfinishTime()
	{
		return finishTime;
	}
	public void setfinishTime(String afinishTime)
	{
		finishTime = afinishTime;
	}
	public String getendorsementStatus()
	{
		return endorsementStatus;
	}
	public void setendorsementStatus(String aendorsementStatus)
	{
		endorsementStatus = aendorsementStatus;
	}
	public String getendorsementApplicationDate()
	{
		if( endorsementApplicationDate != null )
			return fDate.getString(endorsementApplicationDate);
		else
			return null;
	}
	public void setendorsementApplicationDate(Date aendorsementApplicationDate)
	{
		endorsementApplicationDate = aendorsementApplicationDate;
	}
	public void setendorsementApplicationDate(String aendorsementApplicationDate)
	{
		if (aendorsementApplicationDate != null && !aendorsementApplicationDate.equals("") )
		{
			endorsementApplicationDate = fDate.getDate( aendorsementApplicationDate );
		}
		else
			endorsementApplicationDate = null;
	}

	public String getendorsementEffectiveDate()
	{
		if( endorsementEffectiveDate != null )
			return fDate.getString(endorsementEffectiveDate);
		else
			return null;
	}
	public void setendorsementEffectiveDate(Date aendorsementEffectiveDate)
	{
		endorsementEffectiveDate = aendorsementEffectiveDate;
	}
	public void setendorsementEffectiveDate(String aendorsementEffectiveDate)
	{
		if (aendorsementEffectiveDate != null && !aendorsementEffectiveDate.equals("") )
		{
			endorsementEffectiveDate = fDate.getDate( aendorsementEffectiveDate );
		}
		else
			endorsementEffectiveDate = null;
	}

	public String geteffectiveDate()
	{
		if( effectiveDate != null )
			return fDate.getString(effectiveDate);
		else
			return null;
	}
	public void seteffectiveDate(Date aeffectiveDate)
	{
		effectiveDate = aeffectiveDate;
	}
	public void seteffectiveDate(String aeffectiveDate)
	{
		if (aeffectiveDate != null && !aeffectiveDate.equals("") )
		{
			effectiveDate = fDate.getDate( aeffectiveDate );
		}
		else
			effectiveDate = null;
	}

	public String getterminationDate()
	{
		if( terminationDate != null )
			return fDate.getString(terminationDate);
		else
			return null;
	}
	public void setterminationDate(Date aterminationDate)
	{
		terminationDate = aterminationDate;
	}
	public void setterminationDate(String aterminationDate)
	{
		if (aterminationDate != null && !aterminationDate.equals("") )
		{
			terminationDate = fDate.getDate( aterminationDate );
		}
		else
			terminationDate = null;
	}

	public String getsuspendDate()
	{
		if( suspendDate != null )
			return fDate.getString(suspendDate);
		else
			return null;
	}
	public void setsuspendDate(Date asuspendDate)
	{
		suspendDate = asuspendDate;
	}
	public void setsuspendDate(String asuspendDate)
	{
		if (asuspendDate != null && !asuspendDate.equals("") )
		{
			suspendDate = fDate.getDate( asuspendDate );
		}
		else
			suspendDate = null;
	}

	public String getrecoverDate()
	{
		if( recoverDate != null )
			return fDate.getString(recoverDate);
		else
			return null;
	}
	public void setrecoverDate(Date arecoverDate)
	{
		recoverDate = arecoverDate;
	}
	public void setrecoverDate(String arecoverDate)
	{
		if (arecoverDate != null && !arecoverDate.equals("") )
		{
			recoverDate = fDate.getDate( arecoverDate );
		}
		else
			recoverDate = null;
	}

	public String getpolicySource()
	{
		return policySource;
	}
	public void setpolicySource(String apolicySource)
	{
		policySource = apolicySource;
	}
	public String getpolicyStatus()
	{
		return policyStatus;
	}
	public void setpolicyStatus(String apolicyStatus)
	{
		policyStatus = apolicyStatus;
	}
	public String getterminationReason()
	{
		return terminationReason;
	}
	public void setterminationReason(String aterminationReason)
	{
		terminationReason = aterminationReason;
	}
	public String getpaymentFrequency()
	{
		return paymentFrequency;
	}
	public void setpaymentFrequency(String apaymentFrequency)
	{
		paymentFrequency = apaymentFrequency;
	}
	public double getsurrenderAmount()
	{
		return surrenderAmount;
	}
	public void setsurrenderAmount(double asurrenderAmount)
	{
		surrenderAmount = Arith.round(asurrenderAmount,2);
	}
	public void setsurrenderAmount(String asurrenderAmount)
	{
		if (asurrenderAmount != null && !asurrenderAmount.equals(""))
		{
			Double tDouble = new Double(asurrenderAmount);
			double d = tDouble.doubleValue();
                surrenderAmount = Arith.round(d,2);
		}
	}

	public double gettaxDeduct()
	{
		return taxDeduct;
	}
	public void settaxDeduct(double ataxDeduct)
	{
		taxDeduct = Arith.round(ataxDeduct,2);
	}
	public void settaxDeduct(String ataxDeduct)
	{
		if (ataxDeduct != null && !ataxDeduct.equals(""))
		{
			Double tDouble = new Double(ataxDeduct);
			double d = tDouble.doubleValue();
                taxDeduct = Arith.round(d,2);
		}
	}

	public double gettopUpAmount()
	{
		return topUpAmount;
	}
	public void settopUpAmount(double atopUpAmount)
	{
		topUpAmount = Arith.round(atopUpAmount,2);
	}
	public void settopUpAmount(String atopUpAmount)
	{
		if (atopUpAmount != null && !atopUpAmount.equals(""))
		{
			Double tDouble = new Double(atopUpAmount);
			double d = tDouble.doubleValue();
                topUpAmount = Arith.round(d,2);
		}
	}

	public String getsubTaskNo()
	{
		return subTaskNo;
	}
	public void setsubTaskNo(String asubTaskNo)
	{
		subTaskNo = asubTaskNo;
	}
	public String getsendState()
	{
		return sendState;
	}
	public void setsendState(String asendState)
	{
		sendState = asendState;
	}
	public String getreturnSequenceNo()
	{
		return returnSequenceNo;
	}
	public void setreturnSequenceNo(String areturnSequenceNo)
	{
		returnSequenceNo = areturnSequenceNo;
	}
	public String getmessage()
	{
		return message;
	}
	public void setmessage(String amessage)
	{
		message = amessage;
	}
	public String getcomCode()
	{
		return comCode;
	}
	public void setcomCode(String acomCode)
	{
		comCode = acomCode;
	}
	public String getoper()
	{
		return oper;
	}
	public void setoper(String aoper)
	{
		oper = aoper;
	}
	public String getmakeDate()
	{
		if( makeDate != null )
			return fDate.getString(makeDate);
		else
			return null;
	}
	public void setmakeDate(Date amakeDate)
	{
		makeDate = amakeDate;
	}
	public void setmakeDate(String amakeDate)
	{
		if (amakeDate != null && !amakeDate.equals("") )
		{
			makeDate = fDate.getDate( amakeDate );
		}
		else
			makeDate = null;
	}

	public String getmakeTime()
	{
		return makeTime;
	}
	public void setmakeTime(String amakeTime)
	{
		makeTime = amakeTime;
	}

	/**
	* 使用另外一个 TPH_LPEndorsementSchema 对象给 Schema 赋值
	* @param: aTPH_LPEndorsementSchema TPH_LPEndorsementSchema
	**/
	public void setSchema(TPH_LPEndorsementSchema aTPH_LPEndorsementSchema)
	{
		this.policyNo = aTPH_LPEndorsementSchema.getpolicyNo();
		this.policyType = aTPH_LPEndorsementSchema.getpolicyType();
		this.policyOrg = aTPH_LPEndorsementSchema.getpolicyOrg();
		this.policyArea = aTPH_LPEndorsementSchema.getpolicyArea();
		this.endorsementType = aTPH_LPEndorsementSchema.getendorsementType();
		this.endorsementNo = aTPH_LPEndorsementSchema.getendorsementNo();
		this.polEndSeq = aTPH_LPEndorsementSchema.getpolEndSeq();
		this.finishTime = aTPH_LPEndorsementSchema.getfinishTime();
		this.endorsementStatus = aTPH_LPEndorsementSchema.getendorsementStatus();
		this.endorsementApplicationDate = fDate.getDate( aTPH_LPEndorsementSchema.getendorsementApplicationDate());
		this.endorsementEffectiveDate = fDate.getDate( aTPH_LPEndorsementSchema.getendorsementEffectiveDate());
		this.effectiveDate = fDate.getDate( aTPH_LPEndorsementSchema.geteffectiveDate());
		this.terminationDate = fDate.getDate( aTPH_LPEndorsementSchema.getterminationDate());
		this.suspendDate = fDate.getDate( aTPH_LPEndorsementSchema.getsuspendDate());
		this.recoverDate = fDate.getDate( aTPH_LPEndorsementSchema.getrecoverDate());
		this.policySource = aTPH_LPEndorsementSchema.getpolicySource();
		this.policyStatus = aTPH_LPEndorsementSchema.getpolicyStatus();
		this.terminationReason = aTPH_LPEndorsementSchema.getterminationReason();
		this.paymentFrequency = aTPH_LPEndorsementSchema.getpaymentFrequency();
		this.surrenderAmount = aTPH_LPEndorsementSchema.getsurrenderAmount();
		this.taxDeduct = aTPH_LPEndorsementSchema.gettaxDeduct();
		this.topUpAmount = aTPH_LPEndorsementSchema.gettopUpAmount();
		this.subTaskNo = aTPH_LPEndorsementSchema.getsubTaskNo();
		this.sendState = aTPH_LPEndorsementSchema.getsendState();
		this.returnSequenceNo = aTPH_LPEndorsementSchema.getreturnSequenceNo();
		this.message = aTPH_LPEndorsementSchema.getmessage();
		this.comCode = aTPH_LPEndorsementSchema.getcomCode();
		this.oper = aTPH_LPEndorsementSchema.getoper();
		this.makeDate = fDate.getDate( aTPH_LPEndorsementSchema.getmakeDate());
		this.makeTime = aTPH_LPEndorsementSchema.getmakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("policyNo") == null )
				this.policyNo = null;
			else
				this.policyNo = rs.getString("policyNo").trim();

			if( rs.getString("policyType") == null )
				this.policyType = null;
			else
				this.policyType = rs.getString("policyType").trim();

			if( rs.getString("policyOrg") == null )
				this.policyOrg = null;
			else
				this.policyOrg = rs.getString("policyOrg").trim();

			if( rs.getString("policyArea") == null )
				this.policyArea = null;
			else
				this.policyArea = rs.getString("policyArea").trim();

			if( rs.getString("endorsementType") == null )
				this.endorsementType = null;
			else
				this.endorsementType = rs.getString("endorsementType").trim();

			if( rs.getString("endorsementNo") == null )
				this.endorsementNo = null;
			else
				this.endorsementNo = rs.getString("endorsementNo").trim();

			if( rs.getString("polEndSeq") == null )
				this.polEndSeq = null;
			else
				this.polEndSeq = rs.getString("polEndSeq").trim();

			if( rs.getString("finishTime") == null )
				this.finishTime = null;
			else
				this.finishTime = rs.getString("finishTime").trim();

			if( rs.getString("endorsementStatus") == null )
				this.endorsementStatus = null;
			else
				this.endorsementStatus = rs.getString("endorsementStatus").trim();

			this.endorsementApplicationDate = rs.getDate("endorsementApplicationDate");
			this.endorsementEffectiveDate = rs.getDate("endorsementEffectiveDate");
			this.effectiveDate = rs.getDate("effectiveDate");
			this.terminationDate = rs.getDate("terminationDate");
			this.suspendDate = rs.getDate("suspendDate");
			this.recoverDate = rs.getDate("recoverDate");
			if( rs.getString("policySource") == null )
				this.policySource = null;
			else
				this.policySource = rs.getString("policySource").trim();

			if( rs.getString("policyStatus") == null )
				this.policyStatus = null;
			else
				this.policyStatus = rs.getString("policyStatus").trim();

			if( rs.getString("terminationReason") == null )
				this.terminationReason = null;
			else
				this.terminationReason = rs.getString("terminationReason").trim();

			if( rs.getString("paymentFrequency") == null )
				this.paymentFrequency = null;
			else
				this.paymentFrequency = rs.getString("paymentFrequency").trim();

			this.surrenderAmount = rs.getDouble("surrenderAmount");
			this.taxDeduct = rs.getDouble("taxDeduct");
			this.topUpAmount = rs.getDouble("topUpAmount");
			if( rs.getString("subTaskNo") == null )
				this.subTaskNo = null;
			else
				this.subTaskNo = rs.getString("subTaskNo").trim();

			if( rs.getString("sendState") == null )
				this.sendState = null;
			else
				this.sendState = rs.getString("sendState").trim();

			if( rs.getString("returnSequenceNo") == null )
				this.returnSequenceNo = null;
			else
				this.returnSequenceNo = rs.getString("returnSequenceNo").trim();

			if( rs.getString("message") == null )
				this.message = null;
			else
				this.message = rs.getString("message").trim();

			if( rs.getString("comCode") == null )
				this.comCode = null;
			else
				this.comCode = rs.getString("comCode").trim();

			if( rs.getString("oper") == null )
				this.oper = null;
			else
				this.oper = rs.getString("oper").trim();

			this.makeDate = rs.getDate("makeDate");
			if( rs.getString("makeTime") == null )
				this.makeTime = null;
			else
				this.makeTime = rs.getString("makeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的TPH_LPEndorsement表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LPEndorsementSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public TPH_LPEndorsementSchema getSchema()
	{
		TPH_LPEndorsementSchema aTPH_LPEndorsementSchema = new TPH_LPEndorsementSchema();
		aTPH_LPEndorsementSchema.setSchema(this);
		return aTPH_LPEndorsementSchema;
	}

	public TPH_LPEndorsementDB getDB()
	{
		TPH_LPEndorsementDB aDBOper = new TPH_LPEndorsementDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LPEndorsement描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(policyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(policyType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(policyOrg)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(policyArea)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(endorsementType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(endorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(polEndSeq)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(finishTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(endorsementStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( endorsementApplicationDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( endorsementEffectiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( effectiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( terminationDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( suspendDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( recoverDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(policySource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(policyStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(terminationReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paymentFrequency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(surrenderAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(taxDeduct));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(topUpAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(subTaskNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sendState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(returnSequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(message)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(oper)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LPEndorsement>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			policyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			policyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			policyOrg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			policyArea = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			endorsementType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			endorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			polEndSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			finishTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			endorsementStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			endorsementApplicationDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			endorsementEffectiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			effectiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			terminationDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			suspendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			recoverDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			policySource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			policyStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			terminationReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			paymentFrequency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			surrenderAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			taxDeduct = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			topUpAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			subTaskNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			sendState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			returnSequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			message = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			comCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			oper = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			makeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			makeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LPEndorsementSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("policyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
		}
		if (FCode.equals("policyType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyType));
		}
		if (FCode.equals("policyOrg"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyOrg));
		}
		if (FCode.equals("policyArea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyArea));
		}
		if (FCode.equals("endorsementType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(endorsementType));
		}
		if (FCode.equals("endorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(endorsementNo));
		}
		if (FCode.equals("polEndSeq"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(polEndSeq));
		}
		if (FCode.equals("finishTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(finishTime));
		}
		if (FCode.equals("endorsementStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(endorsementStatus));
		}
		if (FCode.equals("endorsementApplicationDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getendorsementApplicationDate()));
		}
		if (FCode.equals("endorsementEffectiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getendorsementEffectiveDate()));
		}
		if (FCode.equals("effectiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.geteffectiveDate()));
		}
		if (FCode.equals("terminationDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getterminationDate()));
		}
		if (FCode.equals("suspendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getsuspendDate()));
		}
		if (FCode.equals("recoverDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getrecoverDate()));
		}
		if (FCode.equals("policySource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policySource));
		}
		if (FCode.equals("policyStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyStatus));
		}
		if (FCode.equals("terminationReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(terminationReason));
		}
		if (FCode.equals("paymentFrequency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paymentFrequency));
		}
		if (FCode.equals("surrenderAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(surrenderAmount));
		}
		if (FCode.equals("taxDeduct"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(taxDeduct));
		}
		if (FCode.equals("topUpAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(topUpAmount));
		}
		if (FCode.equals("subTaskNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(subTaskNo));
		}
		if (FCode.equals("sendState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sendState));
		}
		if (FCode.equals("returnSequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(returnSequenceNo));
		}
		if (FCode.equals("message"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(message));
		}
		if (FCode.equals("comCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comCode));
		}
		if (FCode.equals("oper"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(oper));
		}
		if (FCode.equals("makeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
		}
		if (FCode.equals("makeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(policyNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(policyType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(policyOrg);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(policyArea);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(endorsementType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(endorsementNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(polEndSeq);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(finishTime);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(endorsementStatus);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getendorsementApplicationDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getendorsementEffectiveDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.geteffectiveDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getterminationDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getsuspendDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getrecoverDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(policySource);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(policyStatus);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(terminationReason);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(paymentFrequency);
				break;
			case 19:
				strFieldValue = String.valueOf(surrenderAmount);
				break;
			case 20:
				strFieldValue = String.valueOf(taxDeduct);
				break;
			case 21:
				strFieldValue = String.valueOf(topUpAmount);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(subTaskNo);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(sendState);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(returnSequenceNo);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(message);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(comCode);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(oper);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(makeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("policyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyNo = FValue.trim();
			}
			else
				policyNo = null;
		}
		if (FCode.equalsIgnoreCase("policyType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyType = FValue.trim();
			}
			else
				policyType = null;
		}
		if (FCode.equalsIgnoreCase("policyOrg"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyOrg = FValue.trim();
			}
			else
				policyOrg = null;
		}
		if (FCode.equalsIgnoreCase("policyArea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyArea = FValue.trim();
			}
			else
				policyArea = null;
		}
		if (FCode.equalsIgnoreCase("endorsementType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				endorsementType = FValue.trim();
			}
			else
				endorsementType = null;
		}
		if (FCode.equalsIgnoreCase("endorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				endorsementNo = FValue.trim();
			}
			else
				endorsementNo = null;
		}
		if (FCode.equalsIgnoreCase("polEndSeq"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				polEndSeq = FValue.trim();
			}
			else
				polEndSeq = null;
		}
		if (FCode.equalsIgnoreCase("finishTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				finishTime = FValue.trim();
			}
			else
				finishTime = null;
		}
		if (FCode.equalsIgnoreCase("endorsementStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				endorsementStatus = FValue.trim();
			}
			else
				endorsementStatus = null;
		}
		if (FCode.equalsIgnoreCase("endorsementApplicationDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				endorsementApplicationDate = fDate.getDate( FValue );
			}
			else
				endorsementApplicationDate = null;
		}
		if (FCode.equalsIgnoreCase("endorsementEffectiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				endorsementEffectiveDate = fDate.getDate( FValue );
			}
			else
				endorsementEffectiveDate = null;
		}
		if (FCode.equalsIgnoreCase("effectiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				effectiveDate = fDate.getDate( FValue );
			}
			else
				effectiveDate = null;
		}
		if (FCode.equalsIgnoreCase("terminationDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				terminationDate = fDate.getDate( FValue );
			}
			else
				terminationDate = null;
		}
		if (FCode.equalsIgnoreCase("suspendDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				suspendDate = fDate.getDate( FValue );
			}
			else
				suspendDate = null;
		}
		if (FCode.equalsIgnoreCase("recoverDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				recoverDate = fDate.getDate( FValue );
			}
			else
				recoverDate = null;
		}
		if (FCode.equalsIgnoreCase("policySource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policySource = FValue.trim();
			}
			else
				policySource = null;
		}
		if (FCode.equalsIgnoreCase("policyStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyStatus = FValue.trim();
			}
			else
				policyStatus = null;
		}
		if (FCode.equalsIgnoreCase("terminationReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				terminationReason = FValue.trim();
			}
			else
				terminationReason = null;
		}
		if (FCode.equalsIgnoreCase("paymentFrequency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paymentFrequency = FValue.trim();
			}
			else
				paymentFrequency = null;
		}
		if (FCode.equalsIgnoreCase("surrenderAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				surrenderAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("taxDeduct"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				taxDeduct = d;
			}
		}
		if (FCode.equalsIgnoreCase("topUpAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				topUpAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("subTaskNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				subTaskNo = FValue.trim();
			}
			else
				subTaskNo = null;
		}
		if (FCode.equalsIgnoreCase("sendState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sendState = FValue.trim();
			}
			else
				sendState = null;
		}
		if (FCode.equalsIgnoreCase("returnSequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				returnSequenceNo = FValue.trim();
			}
			else
				returnSequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("message"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				message = FValue.trim();
			}
			else
				message = null;
		}
		if (FCode.equalsIgnoreCase("comCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comCode = FValue.trim();
			}
			else
				comCode = null;
		}
		if (FCode.equalsIgnoreCase("oper"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				oper = FValue.trim();
			}
			else
				oper = null;
		}
		if (FCode.equalsIgnoreCase("makeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makeDate = fDate.getDate( FValue );
			}
			else
				makeDate = null;
		}
		if (FCode.equalsIgnoreCase("makeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makeTime = FValue.trim();
			}
			else
				makeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		TPH_LPEndorsementSchema other = (TPH_LPEndorsementSchema)otherObject;
		return
			(policyNo == null ? other.getpolicyNo() == null : policyNo.equals(other.getpolicyNo()))
			&& (policyType == null ? other.getpolicyType() == null : policyType.equals(other.getpolicyType()))
			&& (policyOrg == null ? other.getpolicyOrg() == null : policyOrg.equals(other.getpolicyOrg()))
			&& (policyArea == null ? other.getpolicyArea() == null : policyArea.equals(other.getpolicyArea()))
			&& (endorsementType == null ? other.getendorsementType() == null : endorsementType.equals(other.getendorsementType()))
			&& (endorsementNo == null ? other.getendorsementNo() == null : endorsementNo.equals(other.getendorsementNo()))
			&& (polEndSeq == null ? other.getpolEndSeq() == null : polEndSeq.equals(other.getpolEndSeq()))
			&& (finishTime == null ? other.getfinishTime() == null : finishTime.equals(other.getfinishTime()))
			&& (endorsementStatus == null ? other.getendorsementStatus() == null : endorsementStatus.equals(other.getendorsementStatus()))
			&& (endorsementApplicationDate == null ? other.getendorsementApplicationDate() == null : fDate.getString(endorsementApplicationDate).equals(other.getendorsementApplicationDate()))
			&& (endorsementEffectiveDate == null ? other.getendorsementEffectiveDate() == null : fDate.getString(endorsementEffectiveDate).equals(other.getendorsementEffectiveDate()))
			&& (effectiveDate == null ? other.geteffectiveDate() == null : fDate.getString(effectiveDate).equals(other.geteffectiveDate()))
			&& (terminationDate == null ? other.getterminationDate() == null : fDate.getString(terminationDate).equals(other.getterminationDate()))
			&& (suspendDate == null ? other.getsuspendDate() == null : fDate.getString(suspendDate).equals(other.getsuspendDate()))
			&& (recoverDate == null ? other.getrecoverDate() == null : fDate.getString(recoverDate).equals(other.getrecoverDate()))
			&& (policySource == null ? other.getpolicySource() == null : policySource.equals(other.getpolicySource()))
			&& (policyStatus == null ? other.getpolicyStatus() == null : policyStatus.equals(other.getpolicyStatus()))
			&& (terminationReason == null ? other.getterminationReason() == null : terminationReason.equals(other.getterminationReason()))
			&& (paymentFrequency == null ? other.getpaymentFrequency() == null : paymentFrequency.equals(other.getpaymentFrequency()))
			&& surrenderAmount == other.getsurrenderAmount()
			&& taxDeduct == other.gettaxDeduct()
			&& topUpAmount == other.gettopUpAmount()
			&& (subTaskNo == null ? other.getsubTaskNo() == null : subTaskNo.equals(other.getsubTaskNo()))
			&& (sendState == null ? other.getsendState() == null : sendState.equals(other.getsendState()))
			&& (returnSequenceNo == null ? other.getreturnSequenceNo() == null : returnSequenceNo.equals(other.getreturnSequenceNo()))
			&& (message == null ? other.getmessage() == null : message.equals(other.getmessage()))
			&& (comCode == null ? other.getcomCode() == null : comCode.equals(other.getcomCode()))
			&& (oper == null ? other.getoper() == null : oper.equals(other.getoper()))
			&& (makeDate == null ? other.getmakeDate() == null : fDate.getString(makeDate).equals(other.getmakeDate()))
			&& (makeTime == null ? other.getmakeTime() == null : makeTime.equals(other.getmakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return 0;
		}
		if( strFieldName.equals("policyType") ) {
			return 1;
		}
		if( strFieldName.equals("policyOrg") ) {
			return 2;
		}
		if( strFieldName.equals("policyArea") ) {
			return 3;
		}
		if( strFieldName.equals("endorsementType") ) {
			return 4;
		}
		if( strFieldName.equals("endorsementNo") ) {
			return 5;
		}
		if( strFieldName.equals("polEndSeq") ) {
			return 6;
		}
		if( strFieldName.equals("finishTime") ) {
			return 7;
		}
		if( strFieldName.equals("endorsementStatus") ) {
			return 8;
		}
		if( strFieldName.equals("endorsementApplicationDate") ) {
			return 9;
		}
		if( strFieldName.equals("endorsementEffectiveDate") ) {
			return 10;
		}
		if( strFieldName.equals("effectiveDate") ) {
			return 11;
		}
		if( strFieldName.equals("terminationDate") ) {
			return 12;
		}
		if( strFieldName.equals("suspendDate") ) {
			return 13;
		}
		if( strFieldName.equals("recoverDate") ) {
			return 14;
		}
		if( strFieldName.equals("policySource") ) {
			return 15;
		}
		if( strFieldName.equals("policyStatus") ) {
			return 16;
		}
		if( strFieldName.equals("terminationReason") ) {
			return 17;
		}
		if( strFieldName.equals("paymentFrequency") ) {
			return 18;
		}
		if( strFieldName.equals("surrenderAmount") ) {
			return 19;
		}
		if( strFieldName.equals("taxDeduct") ) {
			return 20;
		}
		if( strFieldName.equals("topUpAmount") ) {
			return 21;
		}
		if( strFieldName.equals("subTaskNo") ) {
			return 22;
		}
		if( strFieldName.equals("sendState") ) {
			return 23;
		}
		if( strFieldName.equals("returnSequenceNo") ) {
			return 24;
		}
		if( strFieldName.equals("message") ) {
			return 25;
		}
		if( strFieldName.equals("comCode") ) {
			return 26;
		}
		if( strFieldName.equals("oper") ) {
			return 27;
		}
		if( strFieldName.equals("makeDate") ) {
			return 28;
		}
		if( strFieldName.equals("makeTime") ) {
			return 29;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "policyNo";
				break;
			case 1:
				strFieldName = "policyType";
				break;
			case 2:
				strFieldName = "policyOrg";
				break;
			case 3:
				strFieldName = "policyArea";
				break;
			case 4:
				strFieldName = "endorsementType";
				break;
			case 5:
				strFieldName = "endorsementNo";
				break;
			case 6:
				strFieldName = "polEndSeq";
				break;
			case 7:
				strFieldName = "finishTime";
				break;
			case 8:
				strFieldName = "endorsementStatus";
				break;
			case 9:
				strFieldName = "endorsementApplicationDate";
				break;
			case 10:
				strFieldName = "endorsementEffectiveDate";
				break;
			case 11:
				strFieldName = "effectiveDate";
				break;
			case 12:
				strFieldName = "terminationDate";
				break;
			case 13:
				strFieldName = "suspendDate";
				break;
			case 14:
				strFieldName = "recoverDate";
				break;
			case 15:
				strFieldName = "policySource";
				break;
			case 16:
				strFieldName = "policyStatus";
				break;
			case 17:
				strFieldName = "terminationReason";
				break;
			case 18:
				strFieldName = "paymentFrequency";
				break;
			case 19:
				strFieldName = "surrenderAmount";
				break;
			case 20:
				strFieldName = "taxDeduct";
				break;
			case 21:
				strFieldName = "topUpAmount";
				break;
			case 22:
				strFieldName = "subTaskNo";
				break;
			case 23:
				strFieldName = "sendState";
				break;
			case 24:
				strFieldName = "returnSequenceNo";
				break;
			case 25:
				strFieldName = "message";
				break;
			case 26:
				strFieldName = "comCode";
				break;
			case 27:
				strFieldName = "oper";
				break;
			case 28:
				strFieldName = "makeDate";
				break;
			case 29:
				strFieldName = "makeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("policyType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("policyOrg") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("policyArea") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endorsementType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("polEndSeq") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("finishTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endorsementStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endorsementApplicationDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("endorsementEffectiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("effectiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("terminationDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("suspendDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("recoverDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("policySource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("policyStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("terminationReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("paymentFrequency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("surrenderAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("taxDeduct") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("topUpAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("subTaskNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sendState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("returnSequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("message") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("oper") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("makeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
