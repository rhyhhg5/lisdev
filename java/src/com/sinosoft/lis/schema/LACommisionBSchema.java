/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LACommisionBDB;

/*
 * <p>ClassName: LACommisionBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 互动开发表
 * @CreateDate：2016-01-07
 */
public class LACommisionBSchema implements Schema, Cloneable
{
	// @Field
	/** 转储号码 */
	private String EdorNo;
	/** 转储类型 */
	private String EdorType;
	/** 系列号 */
	private String CommisionSN;
	/** 系列原始号 */
	private String CommisionBaseNo;
	/** 佣金计算年月代码 */
	private String WageNo;
	/** 集体合同号码 */
	private String GrpContNo;
	/** 集体保单号码 */
	private String GrpPolNo;
	/** 个人合同号码 */
	private String ContNo;
	/** 保单号码 */
	private String PolNo;
	/** 主险保单号码 */
	private String MainPolNo;
	/** 管理机构 */
	private String ManageCom;
	/** 投保人客户号码 */
	private String AppntNo;
	/** 险种编码 */
	private String RiskCode;
	/** 险种版本 */
	private String RiskVersion;
	/** 责任编码 */
	private String DutyCode;
	/** 交费计划编码 */
	private String PayPlanCode;
	/** 保单生效日期 */
	private Date CValiDate;
	/** 交费间隔 */
	private int PayIntv;
	/** 交费方式 */
	private String PayMode;
	/** 交费收据号 */
	private String ReceiptNo;
	/** 交易交费日期 */
	private Date TPayDate;
	/** 交易到帐日期 */
	private Date TEnterAccDate;
	/** 交易确认日期 */
	private Date TConfDate;
	/** 交易入机日期 */
	private Date TMakeDate;
	/** 生成扎账信息日期 */
	private Date CommDate;
	/** 交易金额 */
	private double TransMoney;
	/** 交易标准金额 */
	private double TransStandMoney;
	/** 原交至日期 */
	private Date LastPayToDate;
	/** 现交至日期 */
	private Date CurPayToDate;
	/** 交易类别 */
	private String TransType;
	/** 佣金计算特征 */
	private String CommDire;
	/** 交易处理状态 */
	private String TransState;
	/** 直接佣金 */
	private double DirectWage;
	/** 附加佣金 */
	private double AppendWage;
	/** 组提佣金额 */
	private double GrpFYC;
	/** 统计件数 */
	private double CalCount;
	/** 统计日期 */
	private Date CalDate;
	/** 标准提佣比例 */
	private double StandFYCRate;
	/** 实际提佣比例 */
	private double FYCRate;
	/** 实际提佣金额 */
	private double FYC;
	/** 部提佣金额 */
	private double DepFYC;
	/** 折算标保 */
	private double StandPrem;
	/** 标准网点手续费 */
	private double CommCharge;
	/** 标准分理处手续费 */
	private double CommCharge1;
	/** 标准支行手续费 */
	private double CommCharge2;
	/** 标准分行手续费 */
	private double CommCharge3;
	/** 标准总行手续费 */
	private double CommCharge4;
	/** 组提奖比例 */
	private double GrpFYCRate;
	/** 部提奖比例 */
	private double DepFYCRate;
	/** 折标比例 */
	private double StandPremRate;
	/** 交易业务属性1 */
	private String F1;
	/** 交易业务属性2 */
	private String F2;
	/** 交易业务属性3 */
	private String F3;
	/** 交易业务属性4 */
	private String F4;
	/** 交易业务属性5 */
	private String F5;
	/** 业务费用1 */
	private double K1;
	/** 业务费用2 */
	private double K2;
	/** 业务费用3 */
	private double K3;
	/** 业务费用4 */
	private double K4;
	/** 业务费用5 */
	private double K5;
	/** 标志位 */
	private String Flag;
	/** 复效日期/还垫日期 */
	private Date CalcDate;
	/** 交费年度 */
	private int PayYear;
	/** 交费年期 */
	private int PayYears;
	/** 保险年期 */
	private int Years;
	/** 第几次交费 */
	private int PayCount;
	/** 签单日期 */
	private Date SignDate;
	/** 保单送达日期 */
	private Date GetPolDate;
	/** 展业类型 */
	private String BranchType;
	/** 代理机构 */
	private String AgentCom;
	/** 柜员姓名 */
	private String BankServer;
	/** 代理机构内部分类 */
	private String AgentType;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人展业机构代码 */
	private String AgentGroup;
	/** 代理人组别 */
	private String BranchCode;
	/** 展业机构序列编码 */
	private String BranchSeries;
	/** 展业机构外部编码 */
	private String BranchAttr;
	/** 保单类型 */
	private String PolType;
	/** 管理费比例 */
	private double P1;
	/** 分红比例 */
	private double P2;
	/** 被保人的投保年龄 */
	private double P3;
	/** Nafyc每月增量 */
	private double P4;
	/** 自动垫交标记 */
	private double P5;
	/** P6 */
	private double P6;
	/** 帐户管理费 */
	private double P7;
	/** P8 */
	private double P8;
	/** P9 */
	private double P9;
	/** P10 */
	private double P10;
	/** 投保人名称 */
	private String P11;
	/** 被保人号码 */
	private String P12;
	/** 被保人姓名 */
	private String P13;
	/** 印刷号 */
	private String P14;
	/** 投保单号 */
	private String P15;
	/** 交单日期 */
	private Date MakePolDate;
	/** 保单回执客户签收日期 */
	private Date CustomGetPolDate;
	/** 主附险标志 */
	private String riskmark;
	/** 扫描日期 */
	private Date ScanDate;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 渠道 */
	private String BranchType2;
	/** 第几次续保 */
	private int ReNewCount;
	/** 市场类型 */
	private String MarketType;
	/** 渠道明细 */
	private String BranchType3;
	/** 批单号码 */
	private String EndorsementNo;

	public static final int FIELDNUM = 107;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LACommisionBSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "EdorNo";
		pk[1] = "EdorType";
		pk[2] = "CommisionSN";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LACommisionBSchema cloned = (LACommisionBSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getEdorType()
	{
		return EdorType;
	}
	public void setEdorType(String aEdorType)
	{
		EdorType = aEdorType;
	}
	public String getCommisionSN()
	{
		return CommisionSN;
	}
	public void setCommisionSN(String aCommisionSN)
	{
		CommisionSN = aCommisionSN;
	}
	public String getCommisionBaseNo()
	{
		return CommisionBaseNo;
	}
	public void setCommisionBaseNo(String aCommisionBaseNo)
	{
		CommisionBaseNo = aCommisionBaseNo;
	}
	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpPolNo()
	{
		return GrpPolNo;
	}
	public void setGrpPolNo(String aGrpPolNo)
	{
		GrpPolNo = aGrpPolNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getMainPolNo()
	{
		return MainPolNo;
	}
	public void setMainPolNo(String aMainPolNo)
	{
		MainPolNo = aMainPolNo;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getAppntNo()
	{
		return AppntNo;
	}
	public void setAppntNo(String aAppntNo)
	{
		AppntNo = aAppntNo;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskVersion()
	{
		return RiskVersion;
	}
	public void setRiskVersion(String aRiskVersion)
	{
		RiskVersion = aRiskVersion;
	}
	public String getDutyCode()
	{
		return DutyCode;
	}
	public void setDutyCode(String aDutyCode)
	{
		DutyCode = aDutyCode;
	}
	public String getPayPlanCode()
	{
		return PayPlanCode;
	}
	public void setPayPlanCode(String aPayPlanCode)
	{
		PayPlanCode = aPayPlanCode;
	}
	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getReceiptNo()
	{
		return ReceiptNo;
	}
	public void setReceiptNo(String aReceiptNo)
	{
		ReceiptNo = aReceiptNo;
	}
	public String getTPayDate()
	{
		if( TPayDate != null )
			return fDate.getString(TPayDate);
		else
			return null;
	}
	public void setTPayDate(Date aTPayDate)
	{
		TPayDate = aTPayDate;
	}
	public void setTPayDate(String aTPayDate)
	{
		if (aTPayDate != null && !aTPayDate.equals("") )
		{
			TPayDate = fDate.getDate( aTPayDate );
		}
		else
			TPayDate = null;
	}

	public String getTEnterAccDate()
	{
		if( TEnterAccDate != null )
			return fDate.getString(TEnterAccDate);
		else
			return null;
	}
	public void setTEnterAccDate(Date aTEnterAccDate)
	{
		TEnterAccDate = aTEnterAccDate;
	}
	public void setTEnterAccDate(String aTEnterAccDate)
	{
		if (aTEnterAccDate != null && !aTEnterAccDate.equals("") )
		{
			TEnterAccDate = fDate.getDate( aTEnterAccDate );
		}
		else
			TEnterAccDate = null;
	}

	public String getTConfDate()
	{
		if( TConfDate != null )
			return fDate.getString(TConfDate);
		else
			return null;
	}
	public void setTConfDate(Date aTConfDate)
	{
		TConfDate = aTConfDate;
	}
	public void setTConfDate(String aTConfDate)
	{
		if (aTConfDate != null && !aTConfDate.equals("") )
		{
			TConfDate = fDate.getDate( aTConfDate );
		}
		else
			TConfDate = null;
	}

	public String getTMakeDate()
	{
		if( TMakeDate != null )
			return fDate.getString(TMakeDate);
		else
			return null;
	}
	public void setTMakeDate(Date aTMakeDate)
	{
		TMakeDate = aTMakeDate;
	}
	public void setTMakeDate(String aTMakeDate)
	{
		if (aTMakeDate != null && !aTMakeDate.equals("") )
		{
			TMakeDate = fDate.getDate( aTMakeDate );
		}
		else
			TMakeDate = null;
	}

	public String getCommDate()
	{
		if( CommDate != null )
			return fDate.getString(CommDate);
		else
			return null;
	}
	public void setCommDate(Date aCommDate)
	{
		CommDate = aCommDate;
	}
	public void setCommDate(String aCommDate)
	{
		if (aCommDate != null && !aCommDate.equals("") )
		{
			CommDate = fDate.getDate( aCommDate );
		}
		else
			CommDate = null;
	}

	public double getTransMoney()
	{
		return TransMoney;
	}
	public void setTransMoney(double aTransMoney)
	{
		TransMoney = Arith.round(aTransMoney,2);
	}
	public void setTransMoney(String aTransMoney)
	{
		if (aTransMoney != null && !aTransMoney.equals(""))
		{
			Double tDouble = new Double(aTransMoney);
			double d = tDouble.doubleValue();
                TransMoney = Arith.round(d,2);
		}
	}

	public double getTransStandMoney()
	{
		return TransStandMoney;
	}
	public void setTransStandMoney(double aTransStandMoney)
	{
		TransStandMoney = Arith.round(aTransStandMoney,2);
	}
	public void setTransStandMoney(String aTransStandMoney)
	{
		if (aTransStandMoney != null && !aTransStandMoney.equals(""))
		{
			Double tDouble = new Double(aTransStandMoney);
			double d = tDouble.doubleValue();
                TransStandMoney = Arith.round(d,2);
		}
	}

	public String getLastPayToDate()
	{
		if( LastPayToDate != null )
			return fDate.getString(LastPayToDate);
		else
			return null;
	}
	public void setLastPayToDate(Date aLastPayToDate)
	{
		LastPayToDate = aLastPayToDate;
	}
	public void setLastPayToDate(String aLastPayToDate)
	{
		if (aLastPayToDate != null && !aLastPayToDate.equals("") )
		{
			LastPayToDate = fDate.getDate( aLastPayToDate );
		}
		else
			LastPayToDate = null;
	}

	public String getCurPayToDate()
	{
		if( CurPayToDate != null )
			return fDate.getString(CurPayToDate);
		else
			return null;
	}
	public void setCurPayToDate(Date aCurPayToDate)
	{
		CurPayToDate = aCurPayToDate;
	}
	public void setCurPayToDate(String aCurPayToDate)
	{
		if (aCurPayToDate != null && !aCurPayToDate.equals("") )
		{
			CurPayToDate = fDate.getDate( aCurPayToDate );
		}
		else
			CurPayToDate = null;
	}

	public String getTransType()
	{
		return TransType;
	}
	public void setTransType(String aTransType)
	{
		TransType = aTransType;
	}
	public String getCommDire()
	{
		return CommDire;
	}
	public void setCommDire(String aCommDire)
	{
		CommDire = aCommDire;
	}
	public String getTransState()
	{
		return TransState;
	}
	public void setTransState(String aTransState)
	{
		TransState = aTransState;
	}
	public double getDirectWage()
	{
		return DirectWage;
	}
	public void setDirectWage(double aDirectWage)
	{
		DirectWage = Arith.round(aDirectWage,2);
	}
	public void setDirectWage(String aDirectWage)
	{
		if (aDirectWage != null && !aDirectWage.equals(""))
		{
			Double tDouble = new Double(aDirectWage);
			double d = tDouble.doubleValue();
                DirectWage = Arith.round(d,2);
		}
	}

	public double getAppendWage()
	{
		return AppendWage;
	}
	public void setAppendWage(double aAppendWage)
	{
		AppendWage = Arith.round(aAppendWage,2);
	}
	public void setAppendWage(String aAppendWage)
	{
		if (aAppendWage != null && !aAppendWage.equals(""))
		{
			Double tDouble = new Double(aAppendWage);
			double d = tDouble.doubleValue();
                AppendWage = Arith.round(d,2);
		}
	}

	public double getGrpFYC()
	{
		return GrpFYC;
	}
	public void setGrpFYC(double aGrpFYC)
	{
		GrpFYC = Arith.round(aGrpFYC,2);
	}
	public void setGrpFYC(String aGrpFYC)
	{
		if (aGrpFYC != null && !aGrpFYC.equals(""))
		{
			Double tDouble = new Double(aGrpFYC);
			double d = tDouble.doubleValue();
                GrpFYC = Arith.round(d,2);
		}
	}

	public double getCalCount()
	{
		return CalCount;
	}
	public void setCalCount(double aCalCount)
	{
		CalCount = Arith.round(aCalCount,2);
	}
	public void setCalCount(String aCalCount)
	{
		if (aCalCount != null && !aCalCount.equals(""))
		{
			Double tDouble = new Double(aCalCount);
			double d = tDouble.doubleValue();
                CalCount = Arith.round(d,2);
		}
	}

	public String getCalDate()
	{
		if( CalDate != null )
			return fDate.getString(CalDate);
		else
			return null;
	}
	public void setCalDate(Date aCalDate)
	{
		CalDate = aCalDate;
	}
	public void setCalDate(String aCalDate)
	{
		if (aCalDate != null && !aCalDate.equals("") )
		{
			CalDate = fDate.getDate( aCalDate );
		}
		else
			CalDate = null;
	}

	public double getStandFYCRate()
	{
		return StandFYCRate;
	}
	public void setStandFYCRate(double aStandFYCRate)
	{
		StandFYCRate = Arith.round(aStandFYCRate,6);
	}
	public void setStandFYCRate(String aStandFYCRate)
	{
		if (aStandFYCRate != null && !aStandFYCRate.equals(""))
		{
			Double tDouble = new Double(aStandFYCRate);
			double d = tDouble.doubleValue();
                StandFYCRate = Arith.round(d,6);
		}
	}

	public double getFYCRate()
	{
		return FYCRate;
	}
	public void setFYCRate(double aFYCRate)
	{
		FYCRate = Arith.round(aFYCRate,6);
	}
	public void setFYCRate(String aFYCRate)
	{
		if (aFYCRate != null && !aFYCRate.equals(""))
		{
			Double tDouble = new Double(aFYCRate);
			double d = tDouble.doubleValue();
                FYCRate = Arith.round(d,6);
		}
	}

	public double getFYC()
	{
		return FYC;
	}
	public void setFYC(double aFYC)
	{
		FYC = Arith.round(aFYC,2);
	}
	public void setFYC(String aFYC)
	{
		if (aFYC != null && !aFYC.equals(""))
		{
			Double tDouble = new Double(aFYC);
			double d = tDouble.doubleValue();
                FYC = Arith.round(d,2);
		}
	}

	public double getDepFYC()
	{
		return DepFYC;
	}
	public void setDepFYC(double aDepFYC)
	{
		DepFYC = Arith.round(aDepFYC,2);
	}
	public void setDepFYC(String aDepFYC)
	{
		if (aDepFYC != null && !aDepFYC.equals(""))
		{
			Double tDouble = new Double(aDepFYC);
			double d = tDouble.doubleValue();
                DepFYC = Arith.round(d,2);
		}
	}

	public double getStandPrem()
	{
		return StandPrem;
	}
	public void setStandPrem(double aStandPrem)
	{
		StandPrem = Arith.round(aStandPrem,2);
	}
	public void setStandPrem(String aStandPrem)
	{
		if (aStandPrem != null && !aStandPrem.equals(""))
		{
			Double tDouble = new Double(aStandPrem);
			double d = tDouble.doubleValue();
                StandPrem = Arith.round(d,2);
		}
	}

	public double getCommCharge()
	{
		return CommCharge;
	}
	public void setCommCharge(double aCommCharge)
	{
		CommCharge = Arith.round(aCommCharge,2);
	}
	public void setCommCharge(String aCommCharge)
	{
		if (aCommCharge != null && !aCommCharge.equals(""))
		{
			Double tDouble = new Double(aCommCharge);
			double d = tDouble.doubleValue();
                CommCharge = Arith.round(d,2);
		}
	}

	public double getCommCharge1()
	{
		return CommCharge1;
	}
	public void setCommCharge1(double aCommCharge1)
	{
		CommCharge1 = Arith.round(aCommCharge1,2);
	}
	public void setCommCharge1(String aCommCharge1)
	{
		if (aCommCharge1 != null && !aCommCharge1.equals(""))
		{
			Double tDouble = new Double(aCommCharge1);
			double d = tDouble.doubleValue();
                CommCharge1 = Arith.round(d,2);
		}
	}

	public double getCommCharge2()
	{
		return CommCharge2;
	}
	public void setCommCharge2(double aCommCharge2)
	{
		CommCharge2 = Arith.round(aCommCharge2,2);
	}
	public void setCommCharge2(String aCommCharge2)
	{
		if (aCommCharge2 != null && !aCommCharge2.equals(""))
		{
			Double tDouble = new Double(aCommCharge2);
			double d = tDouble.doubleValue();
                CommCharge2 = Arith.round(d,2);
		}
	}

	public double getCommCharge3()
	{
		return CommCharge3;
	}
	public void setCommCharge3(double aCommCharge3)
	{
		CommCharge3 = Arith.round(aCommCharge3,2);
	}
	public void setCommCharge3(String aCommCharge3)
	{
		if (aCommCharge3 != null && !aCommCharge3.equals(""))
		{
			Double tDouble = new Double(aCommCharge3);
			double d = tDouble.doubleValue();
                CommCharge3 = Arith.round(d,2);
		}
	}

	public double getCommCharge4()
	{
		return CommCharge4;
	}
	public void setCommCharge4(double aCommCharge4)
	{
		CommCharge4 = Arith.round(aCommCharge4,2);
	}
	public void setCommCharge4(String aCommCharge4)
	{
		if (aCommCharge4 != null && !aCommCharge4.equals(""))
		{
			Double tDouble = new Double(aCommCharge4);
			double d = tDouble.doubleValue();
                CommCharge4 = Arith.round(d,2);
		}
	}

	public double getGrpFYCRate()
	{
		return GrpFYCRate;
	}
	public void setGrpFYCRate(double aGrpFYCRate)
	{
		GrpFYCRate = Arith.round(aGrpFYCRate,6);
	}
	public void setGrpFYCRate(String aGrpFYCRate)
	{
		if (aGrpFYCRate != null && !aGrpFYCRate.equals(""))
		{
			Double tDouble = new Double(aGrpFYCRate);
			double d = tDouble.doubleValue();
                GrpFYCRate = Arith.round(d,6);
		}
	}

	public double getDepFYCRate()
	{
		return DepFYCRate;
	}
	public void setDepFYCRate(double aDepFYCRate)
	{
		DepFYCRate = Arith.round(aDepFYCRate,6);
	}
	public void setDepFYCRate(String aDepFYCRate)
	{
		if (aDepFYCRate != null && !aDepFYCRate.equals(""))
		{
			Double tDouble = new Double(aDepFYCRate);
			double d = tDouble.doubleValue();
                DepFYCRate = Arith.round(d,6);
		}
	}

	public double getStandPremRate()
	{
		return StandPremRate;
	}
	public void setStandPremRate(double aStandPremRate)
	{
		StandPremRate = Arith.round(aStandPremRate,6);
	}
	public void setStandPremRate(String aStandPremRate)
	{
		if (aStandPremRate != null && !aStandPremRate.equals(""))
		{
			Double tDouble = new Double(aStandPremRate);
			double d = tDouble.doubleValue();
                StandPremRate = Arith.round(d,6);
		}
	}

	public String getF1()
	{
		return F1;
	}
	public void setF1(String aF1)
	{
		F1 = aF1;
	}
	public String getF2()
	{
		return F2;
	}
	public void setF2(String aF2)
	{
		F2 = aF2;
	}
	public String getF3()
	{
		return F3;
	}
	public void setF3(String aF3)
	{
		F3 = aF3;
	}
	public String getF4()
	{
		return F4;
	}
	public void setF4(String aF4)
	{
		F4 = aF4;
	}
	public String getF5()
	{
		return F5;
	}
	public void setF5(String aF5)
	{
		F5 = aF5;
	}
	public double getK1()
	{
		return K1;
	}
	public void setK1(double aK1)
	{
		K1 = Arith.round(aK1,2);
	}
	public void setK1(String aK1)
	{
		if (aK1 != null && !aK1.equals(""))
		{
			Double tDouble = new Double(aK1);
			double d = tDouble.doubleValue();
                K1 = Arith.round(d,2);
		}
	}

	public double getK2()
	{
		return K2;
	}
	public void setK2(double aK2)
	{
		K2 = Arith.round(aK2,2);
	}
	public void setK2(String aK2)
	{
		if (aK2 != null && !aK2.equals(""))
		{
			Double tDouble = new Double(aK2);
			double d = tDouble.doubleValue();
                K2 = Arith.round(d,2);
		}
	}

	public double getK3()
	{
		return K3;
	}
	public void setK3(double aK3)
	{
		K3 = Arith.round(aK3,2);
	}
	public void setK3(String aK3)
	{
		if (aK3 != null && !aK3.equals(""))
		{
			Double tDouble = new Double(aK3);
			double d = tDouble.doubleValue();
                K3 = Arith.round(d,2);
		}
	}

	public double getK4()
	{
		return K4;
	}
	public void setK4(double aK4)
	{
		K4 = Arith.round(aK4,2);
	}
	public void setK4(String aK4)
	{
		if (aK4 != null && !aK4.equals(""))
		{
			Double tDouble = new Double(aK4);
			double d = tDouble.doubleValue();
                K4 = Arith.round(d,2);
		}
	}

	public double getK5()
	{
		return K5;
	}
	public void setK5(double aK5)
	{
		K5 = Arith.round(aK5,2);
	}
	public void setK5(String aK5)
	{
		if (aK5 != null && !aK5.equals(""))
		{
			Double tDouble = new Double(aK5);
			double d = tDouble.doubleValue();
                K5 = Arith.round(d,2);
		}
	}

	public String getFlag()
	{
		return Flag;
	}
	public void setFlag(String aFlag)
	{
		Flag = aFlag;
	}
	public String getCalcDate()
	{
		if( CalcDate != null )
			return fDate.getString(CalcDate);
		else
			return null;
	}
	public void setCalcDate(Date aCalcDate)
	{
		CalcDate = aCalcDate;
	}
	public void setCalcDate(String aCalcDate)
	{
		if (aCalcDate != null && !aCalcDate.equals("") )
		{
			CalcDate = fDate.getDate( aCalcDate );
		}
		else
			CalcDate = null;
	}

	public int getPayYear()
	{
		return PayYear;
	}
	public void setPayYear(int aPayYear)
	{
		PayYear = aPayYear;
	}
	public void setPayYear(String aPayYear)
	{
		if (aPayYear != null && !aPayYear.equals(""))
		{
			Integer tInteger = new Integer(aPayYear);
			int i = tInteger.intValue();
			PayYear = i;
		}
	}

	public int getPayYears()
	{
		return PayYears;
	}
	public void setPayYears(int aPayYears)
	{
		PayYears = aPayYears;
	}
	public void setPayYears(String aPayYears)
	{
		if (aPayYears != null && !aPayYears.equals(""))
		{
			Integer tInteger = new Integer(aPayYears);
			int i = tInteger.intValue();
			PayYears = i;
		}
	}

	public int getYears()
	{
		return Years;
	}
	public void setYears(int aYears)
	{
		Years = aYears;
	}
	public void setYears(String aYears)
	{
		if (aYears != null && !aYears.equals(""))
		{
			Integer tInteger = new Integer(aYears);
			int i = tInteger.intValue();
			Years = i;
		}
	}

	public int getPayCount()
	{
		return PayCount;
	}
	public void setPayCount(int aPayCount)
	{
		PayCount = aPayCount;
	}
	public void setPayCount(String aPayCount)
	{
		if (aPayCount != null && !aPayCount.equals(""))
		{
			Integer tInteger = new Integer(aPayCount);
			int i = tInteger.intValue();
			PayCount = i;
		}
	}

	public String getSignDate()
	{
		if( SignDate != null )
			return fDate.getString(SignDate);
		else
			return null;
	}
	public void setSignDate(Date aSignDate)
	{
		SignDate = aSignDate;
	}
	public void setSignDate(String aSignDate)
	{
		if (aSignDate != null && !aSignDate.equals("") )
		{
			SignDate = fDate.getDate( aSignDate );
		}
		else
			SignDate = null;
	}

	public String getGetPolDate()
	{
		if( GetPolDate != null )
			return fDate.getString(GetPolDate);
		else
			return null;
	}
	public void setGetPolDate(Date aGetPolDate)
	{
		GetPolDate = aGetPolDate;
	}
	public void setGetPolDate(String aGetPolDate)
	{
		if (aGetPolDate != null && !aGetPolDate.equals("") )
		{
			GetPolDate = fDate.getDate( aGetPolDate );
		}
		else
			GetPolDate = null;
	}

	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getBankServer()
	{
		return BankServer;
	}
	public void setBankServer(String aBankServer)
	{
		BankServer = aBankServer;
	}
	public String getAgentType()
	{
		return AgentType;
	}
	public void setAgentType(String aAgentType)
	{
		AgentType = aAgentType;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getBranchCode()
	{
		return BranchCode;
	}
	public void setBranchCode(String aBranchCode)
	{
		BranchCode = aBranchCode;
	}
	public String getBranchSeries()
	{
		return BranchSeries;
	}
	public void setBranchSeries(String aBranchSeries)
	{
		BranchSeries = aBranchSeries;
	}
	public String getBranchAttr()
	{
		return BranchAttr;
	}
	public void setBranchAttr(String aBranchAttr)
	{
		BranchAttr = aBranchAttr;
	}
	public String getPolType()
	{
		return PolType;
	}
	public void setPolType(String aPolType)
	{
		PolType = aPolType;
	}
	public double getP1()
	{
		return P1;
	}
	public void setP1(double aP1)
	{
		P1 = Arith.round(aP1,6);
	}
	public void setP1(String aP1)
	{
		if (aP1 != null && !aP1.equals(""))
		{
			Double tDouble = new Double(aP1);
			double d = tDouble.doubleValue();
                P1 = Arith.round(d,6);
		}
	}

	public double getP2()
	{
		return P2;
	}
	public void setP2(double aP2)
	{
		P2 = Arith.round(aP2,6);
	}
	public void setP2(String aP2)
	{
		if (aP2 != null && !aP2.equals(""))
		{
			Double tDouble = new Double(aP2);
			double d = tDouble.doubleValue();
                P2 = Arith.round(d,6);
		}
	}

	public double getP3()
	{
		return P3;
	}
	public void setP3(double aP3)
	{
		P3 = Arith.round(aP3,6);
	}
	public void setP3(String aP3)
	{
		if (aP3 != null && !aP3.equals(""))
		{
			Double tDouble = new Double(aP3);
			double d = tDouble.doubleValue();
                P3 = Arith.round(d,6);
		}
	}

	public double getP4()
	{
		return P4;
	}
	public void setP4(double aP4)
	{
		P4 = Arith.round(aP4,6);
	}
	public void setP4(String aP4)
	{
		if (aP4 != null && !aP4.equals(""))
		{
			Double tDouble = new Double(aP4);
			double d = tDouble.doubleValue();
                P4 = Arith.round(d,6);
		}
	}

	public double getP5()
	{
		return P5;
	}
	public void setP5(double aP5)
	{
		P5 = Arith.round(aP5,2);
	}
	public void setP5(String aP5)
	{
		if (aP5 != null && !aP5.equals(""))
		{
			Double tDouble = new Double(aP5);
			double d = tDouble.doubleValue();
                P5 = Arith.round(d,2);
		}
	}

	public double getP6()
	{
		return P6;
	}
	public void setP6(double aP6)
	{
		P6 = Arith.round(aP6,2);
	}
	public void setP6(String aP6)
	{
		if (aP6 != null && !aP6.equals(""))
		{
			Double tDouble = new Double(aP6);
			double d = tDouble.doubleValue();
                P6 = Arith.round(d,2);
		}
	}

	public double getP7()
	{
		return P7;
	}
	public void setP7(double aP7)
	{
		P7 = Arith.round(aP7,2);
	}
	public void setP7(String aP7)
	{
		if (aP7 != null && !aP7.equals(""))
		{
			Double tDouble = new Double(aP7);
			double d = tDouble.doubleValue();
                P7 = Arith.round(d,2);
		}
	}

	public double getP8()
	{
		return P8;
	}
	public void setP8(double aP8)
	{
		P8 = Arith.round(aP8,2);
	}
	public void setP8(String aP8)
	{
		if (aP8 != null && !aP8.equals(""))
		{
			Double tDouble = new Double(aP8);
			double d = tDouble.doubleValue();
                P8 = Arith.round(d,2);
		}
	}

	public double getP9()
	{
		return P9;
	}
	public void setP9(double aP9)
	{
		P9 = Arith.round(aP9,2);
	}
	public void setP9(String aP9)
	{
		if (aP9 != null && !aP9.equals(""))
		{
			Double tDouble = new Double(aP9);
			double d = tDouble.doubleValue();
                P9 = Arith.round(d,2);
		}
	}

	public double getP10()
	{
		return P10;
	}
	public void setP10(double aP10)
	{
		P10 = Arith.round(aP10,2);
	}
	public void setP10(String aP10)
	{
		if (aP10 != null && !aP10.equals(""))
		{
			Double tDouble = new Double(aP10);
			double d = tDouble.doubleValue();
                P10 = Arith.round(d,2);
		}
	}

	public String getP11()
	{
		return P11;
	}
	public void setP11(String aP11)
	{
		P11 = aP11;
	}
	public String getP12()
	{
		return P12;
	}
	public void setP12(String aP12)
	{
		P12 = aP12;
	}
	public String getP13()
	{
		return P13;
	}
	public void setP13(String aP13)
	{
		P13 = aP13;
	}
	public String getP14()
	{
		return P14;
	}
	public void setP14(String aP14)
	{
		P14 = aP14;
	}
	public String getP15()
	{
		return P15;
	}
	public void setP15(String aP15)
	{
		P15 = aP15;
	}
	public String getMakePolDate()
	{
		if( MakePolDate != null )
			return fDate.getString(MakePolDate);
		else
			return null;
	}
	public void setMakePolDate(Date aMakePolDate)
	{
		MakePolDate = aMakePolDate;
	}
	public void setMakePolDate(String aMakePolDate)
	{
		if (aMakePolDate != null && !aMakePolDate.equals("") )
		{
			MakePolDate = fDate.getDate( aMakePolDate );
		}
		else
			MakePolDate = null;
	}

	public String getCustomGetPolDate()
	{
		if( CustomGetPolDate != null )
			return fDate.getString(CustomGetPolDate);
		else
			return null;
	}
	public void setCustomGetPolDate(Date aCustomGetPolDate)
	{
		CustomGetPolDate = aCustomGetPolDate;
	}
	public void setCustomGetPolDate(String aCustomGetPolDate)
	{
		if (aCustomGetPolDate != null && !aCustomGetPolDate.equals("") )
		{
			CustomGetPolDate = fDate.getDate( aCustomGetPolDate );
		}
		else
			CustomGetPolDate = null;
	}

	public String getriskmark()
	{
		return riskmark;
	}
	public void setriskmark(String ariskmark)
	{
		riskmark = ariskmark;
	}
	public String getScanDate()
	{
		if( ScanDate != null )
			return fDate.getString(ScanDate);
		else
			return null;
	}
	public void setScanDate(Date aScanDate)
	{
		ScanDate = aScanDate;
	}
	public void setScanDate(String aScanDate)
	{
		if (aScanDate != null && !aScanDate.equals("") )
		{
			ScanDate = fDate.getDate( aScanDate );
		}
		else
			ScanDate = null;
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public int getReNewCount()
	{
		return ReNewCount;
	}
	public void setReNewCount(int aReNewCount)
	{
		ReNewCount = aReNewCount;
	}
	public void setReNewCount(String aReNewCount)
	{
		if (aReNewCount != null && !aReNewCount.equals(""))
		{
			Integer tInteger = new Integer(aReNewCount);
			int i = tInteger.intValue();
			ReNewCount = i;
		}
	}

	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getBranchType3()
	{
		return BranchType3;
	}
	public void setBranchType3(String aBranchType3)
	{
		BranchType3 = aBranchType3;
	}
	public String getEndorsementNo()
	{
		return EndorsementNo;
	}
	public void setEndorsementNo(String aEndorsementNo)
	{
		EndorsementNo = aEndorsementNo;
	}

	/**
	* 使用另外一个 LACommisionBSchema 对象给 Schema 赋值
	* @param: aLACommisionBSchema LACommisionBSchema
	**/
	public void setSchema(LACommisionBSchema aLACommisionBSchema)
	{
		this.EdorNo = aLACommisionBSchema.getEdorNo();
		this.EdorType = aLACommisionBSchema.getEdorType();
		this.CommisionSN = aLACommisionBSchema.getCommisionSN();
		this.CommisionBaseNo = aLACommisionBSchema.getCommisionBaseNo();
		this.WageNo = aLACommisionBSchema.getWageNo();
		this.GrpContNo = aLACommisionBSchema.getGrpContNo();
		this.GrpPolNo = aLACommisionBSchema.getGrpPolNo();
		this.ContNo = aLACommisionBSchema.getContNo();
		this.PolNo = aLACommisionBSchema.getPolNo();
		this.MainPolNo = aLACommisionBSchema.getMainPolNo();
		this.ManageCom = aLACommisionBSchema.getManageCom();
		this.AppntNo = aLACommisionBSchema.getAppntNo();
		this.RiskCode = aLACommisionBSchema.getRiskCode();
		this.RiskVersion = aLACommisionBSchema.getRiskVersion();
		this.DutyCode = aLACommisionBSchema.getDutyCode();
		this.PayPlanCode = aLACommisionBSchema.getPayPlanCode();
		this.CValiDate = fDate.getDate( aLACommisionBSchema.getCValiDate());
		this.PayIntv = aLACommisionBSchema.getPayIntv();
		this.PayMode = aLACommisionBSchema.getPayMode();
		this.ReceiptNo = aLACommisionBSchema.getReceiptNo();
		this.TPayDate = fDate.getDate( aLACommisionBSchema.getTPayDate());
		this.TEnterAccDate = fDate.getDate( aLACommisionBSchema.getTEnterAccDate());
		this.TConfDate = fDate.getDate( aLACommisionBSchema.getTConfDate());
		this.TMakeDate = fDate.getDate( aLACommisionBSchema.getTMakeDate());
		this.CommDate = fDate.getDate( aLACommisionBSchema.getCommDate());
		this.TransMoney = aLACommisionBSchema.getTransMoney();
		this.TransStandMoney = aLACommisionBSchema.getTransStandMoney();
		this.LastPayToDate = fDate.getDate( aLACommisionBSchema.getLastPayToDate());
		this.CurPayToDate = fDate.getDate( aLACommisionBSchema.getCurPayToDate());
		this.TransType = aLACommisionBSchema.getTransType();
		this.CommDire = aLACommisionBSchema.getCommDire();
		this.TransState = aLACommisionBSchema.getTransState();
		this.DirectWage = aLACommisionBSchema.getDirectWage();
		this.AppendWage = aLACommisionBSchema.getAppendWage();
		this.GrpFYC = aLACommisionBSchema.getGrpFYC();
		this.CalCount = aLACommisionBSchema.getCalCount();
		this.CalDate = fDate.getDate( aLACommisionBSchema.getCalDate());
		this.StandFYCRate = aLACommisionBSchema.getStandFYCRate();
		this.FYCRate = aLACommisionBSchema.getFYCRate();
		this.FYC = aLACommisionBSchema.getFYC();
		this.DepFYC = aLACommisionBSchema.getDepFYC();
		this.StandPrem = aLACommisionBSchema.getStandPrem();
		this.CommCharge = aLACommisionBSchema.getCommCharge();
		this.CommCharge1 = aLACommisionBSchema.getCommCharge1();
		this.CommCharge2 = aLACommisionBSchema.getCommCharge2();
		this.CommCharge3 = aLACommisionBSchema.getCommCharge3();
		this.CommCharge4 = aLACommisionBSchema.getCommCharge4();
		this.GrpFYCRate = aLACommisionBSchema.getGrpFYCRate();
		this.DepFYCRate = aLACommisionBSchema.getDepFYCRate();
		this.StandPremRate = aLACommisionBSchema.getStandPremRate();
		this.F1 = aLACommisionBSchema.getF1();
		this.F2 = aLACommisionBSchema.getF2();
		this.F3 = aLACommisionBSchema.getF3();
		this.F4 = aLACommisionBSchema.getF4();
		this.F5 = aLACommisionBSchema.getF5();
		this.K1 = aLACommisionBSchema.getK1();
		this.K2 = aLACommisionBSchema.getK2();
		this.K3 = aLACommisionBSchema.getK3();
		this.K4 = aLACommisionBSchema.getK4();
		this.K5 = aLACommisionBSchema.getK5();
		this.Flag = aLACommisionBSchema.getFlag();
		this.CalcDate = fDate.getDate( aLACommisionBSchema.getCalcDate());
		this.PayYear = aLACommisionBSchema.getPayYear();
		this.PayYears = aLACommisionBSchema.getPayYears();
		this.Years = aLACommisionBSchema.getYears();
		this.PayCount = aLACommisionBSchema.getPayCount();
		this.SignDate = fDate.getDate( aLACommisionBSchema.getSignDate());
		this.GetPolDate = fDate.getDate( aLACommisionBSchema.getGetPolDate());
		this.BranchType = aLACommisionBSchema.getBranchType();
		this.AgentCom = aLACommisionBSchema.getAgentCom();
		this.BankServer = aLACommisionBSchema.getBankServer();
		this.AgentType = aLACommisionBSchema.getAgentType();
		this.AgentCode = aLACommisionBSchema.getAgentCode();
		this.AgentGroup = aLACommisionBSchema.getAgentGroup();
		this.BranchCode = aLACommisionBSchema.getBranchCode();
		this.BranchSeries = aLACommisionBSchema.getBranchSeries();
		this.BranchAttr = aLACommisionBSchema.getBranchAttr();
		this.PolType = aLACommisionBSchema.getPolType();
		this.P1 = aLACommisionBSchema.getP1();
		this.P2 = aLACommisionBSchema.getP2();
		this.P3 = aLACommisionBSchema.getP3();
		this.P4 = aLACommisionBSchema.getP4();
		this.P5 = aLACommisionBSchema.getP5();
		this.P6 = aLACommisionBSchema.getP6();
		this.P7 = aLACommisionBSchema.getP7();
		this.P8 = aLACommisionBSchema.getP8();
		this.P9 = aLACommisionBSchema.getP9();
		this.P10 = aLACommisionBSchema.getP10();
		this.P11 = aLACommisionBSchema.getP11();
		this.P12 = aLACommisionBSchema.getP12();
		this.P13 = aLACommisionBSchema.getP13();
		this.P14 = aLACommisionBSchema.getP14();
		this.P15 = aLACommisionBSchema.getP15();
		this.MakePolDate = fDate.getDate( aLACommisionBSchema.getMakePolDate());
		this.CustomGetPolDate = fDate.getDate( aLACommisionBSchema.getCustomGetPolDate());
		this.riskmark = aLACommisionBSchema.getriskmark();
		this.ScanDate = fDate.getDate( aLACommisionBSchema.getScanDate());
		this.Operator = aLACommisionBSchema.getOperator();
		this.MakeDate = fDate.getDate( aLACommisionBSchema.getMakeDate());
		this.MakeTime = aLACommisionBSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLACommisionBSchema.getModifyDate());
		this.ModifyTime = aLACommisionBSchema.getModifyTime();
		this.BranchType2 = aLACommisionBSchema.getBranchType2();
		this.ReNewCount = aLACommisionBSchema.getReNewCount();
		this.MarketType = aLACommisionBSchema.getMarketType();
		this.BranchType3 = aLACommisionBSchema.getBranchType3();
		this.EndorsementNo = aLACommisionBSchema.getEndorsementNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("EdorType") == null )
				this.EdorType = null;
			else
				this.EdorType = rs.getString("EdorType").trim();

			if( rs.getString("CommisionSN") == null )
				this.CommisionSN = null;
			else
				this.CommisionSN = rs.getString("CommisionSN").trim();

			if( rs.getString("CommisionBaseNo") == null )
				this.CommisionBaseNo = null;
			else
				this.CommisionBaseNo = rs.getString("CommisionBaseNo").trim();

			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpPolNo") == null )
				this.GrpPolNo = null;
			else
				this.GrpPolNo = rs.getString("GrpPolNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("MainPolNo") == null )
				this.MainPolNo = null;
			else
				this.MainPolNo = rs.getString("MainPolNo").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("AppntNo") == null )
				this.AppntNo = null;
			else
				this.AppntNo = rs.getString("AppntNo").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskVersion") == null )
				this.RiskVersion = null;
			else
				this.RiskVersion = rs.getString("RiskVersion").trim();

			if( rs.getString("DutyCode") == null )
				this.DutyCode = null;
			else
				this.DutyCode = rs.getString("DutyCode").trim();

			if( rs.getString("PayPlanCode") == null )
				this.PayPlanCode = null;
			else
				this.PayPlanCode = rs.getString("PayPlanCode").trim();

			this.CValiDate = rs.getDate("CValiDate");
			this.PayIntv = rs.getInt("PayIntv");
			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("ReceiptNo") == null )
				this.ReceiptNo = null;
			else
				this.ReceiptNo = rs.getString("ReceiptNo").trim();

			this.TPayDate = rs.getDate("TPayDate");
			this.TEnterAccDate = rs.getDate("TEnterAccDate");
			this.TConfDate = rs.getDate("TConfDate");
			this.TMakeDate = rs.getDate("TMakeDate");
			this.CommDate = rs.getDate("CommDate");
			this.TransMoney = rs.getDouble("TransMoney");
			this.TransStandMoney = rs.getDouble("TransStandMoney");
			this.LastPayToDate = rs.getDate("LastPayToDate");
			this.CurPayToDate = rs.getDate("CurPayToDate");
			if( rs.getString("TransType") == null )
				this.TransType = null;
			else
				this.TransType = rs.getString("TransType").trim();

			if( rs.getString("CommDire") == null )
				this.CommDire = null;
			else
				this.CommDire = rs.getString("CommDire").trim();

			if( rs.getString("TransState") == null )
				this.TransState = null;
			else
				this.TransState = rs.getString("TransState").trim();

			this.DirectWage = rs.getDouble("DirectWage");
			this.AppendWage = rs.getDouble("AppendWage");
			this.GrpFYC = rs.getDouble("GrpFYC");
			this.CalCount = rs.getDouble("CalCount");
			this.CalDate = rs.getDate("CalDate");
			this.StandFYCRate = rs.getDouble("StandFYCRate");
			this.FYCRate = rs.getDouble("FYCRate");
			this.FYC = rs.getDouble("FYC");
			this.DepFYC = rs.getDouble("DepFYC");
			this.StandPrem = rs.getDouble("StandPrem");
			this.CommCharge = rs.getDouble("CommCharge");
			this.CommCharge1 = rs.getDouble("CommCharge1");
			this.CommCharge2 = rs.getDouble("CommCharge2");
			this.CommCharge3 = rs.getDouble("CommCharge3");
			this.CommCharge4 = rs.getDouble("CommCharge4");
			this.GrpFYCRate = rs.getDouble("GrpFYCRate");
			this.DepFYCRate = rs.getDouble("DepFYCRate");
			this.StandPremRate = rs.getDouble("StandPremRate");
			if( rs.getString("F1") == null )
				this.F1 = null;
			else
				this.F1 = rs.getString("F1").trim();

			if( rs.getString("F2") == null )
				this.F2 = null;
			else
				this.F2 = rs.getString("F2").trim();

			if( rs.getString("F3") == null )
				this.F3 = null;
			else
				this.F3 = rs.getString("F3").trim();

			if( rs.getString("F4") == null )
				this.F4 = null;
			else
				this.F4 = rs.getString("F4").trim();

			if( rs.getString("F5") == null )
				this.F5 = null;
			else
				this.F5 = rs.getString("F5").trim();

			this.K1 = rs.getDouble("K1");
			this.K2 = rs.getDouble("K2");
			this.K3 = rs.getDouble("K3");
			this.K4 = rs.getDouble("K4");
			this.K5 = rs.getDouble("K5");
			if( rs.getString("Flag") == null )
				this.Flag = null;
			else
				this.Flag = rs.getString("Flag").trim();

			this.CalcDate = rs.getDate("CalcDate");
			this.PayYear = rs.getInt("PayYear");
			this.PayYears = rs.getInt("PayYears");
			this.Years = rs.getInt("Years");
			this.PayCount = rs.getInt("PayCount");
			this.SignDate = rs.getDate("SignDate");
			this.GetPolDate = rs.getDate("GetPolDate");
			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("BankServer") == null )
				this.BankServer = null;
			else
				this.BankServer = rs.getString("BankServer").trim();

			if( rs.getString("AgentType") == null )
				this.AgentType = null;
			else
				this.AgentType = rs.getString("AgentType").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("BranchCode") == null )
				this.BranchCode = null;
			else
				this.BranchCode = rs.getString("BranchCode").trim();

			if( rs.getString("BranchSeries") == null )
				this.BranchSeries = null;
			else
				this.BranchSeries = rs.getString("BranchSeries").trim();

			if( rs.getString("BranchAttr") == null )
				this.BranchAttr = null;
			else
				this.BranchAttr = rs.getString("BranchAttr").trim();

			if( rs.getString("PolType") == null )
				this.PolType = null;
			else
				this.PolType = rs.getString("PolType").trim();

			this.P1 = rs.getDouble("P1");
			this.P2 = rs.getDouble("P2");
			this.P3 = rs.getDouble("P3");
			this.P4 = rs.getDouble("P4");
			this.P5 = rs.getDouble("P5");
			this.P6 = rs.getDouble("P6");
			this.P7 = rs.getDouble("P7");
			this.P8 = rs.getDouble("P8");
			this.P9 = rs.getDouble("P9");
			this.P10 = rs.getDouble("P10");
			if( rs.getString("P11") == null )
				this.P11 = null;
			else
				this.P11 = rs.getString("P11").trim();

			if( rs.getString("P12") == null )
				this.P12 = null;
			else
				this.P12 = rs.getString("P12").trim();

			if( rs.getString("P13") == null )
				this.P13 = null;
			else
				this.P13 = rs.getString("P13").trim();

			if( rs.getString("P14") == null )
				this.P14 = null;
			else
				this.P14 = rs.getString("P14").trim();

			if( rs.getString("P15") == null )
				this.P15 = null;
			else
				this.P15 = rs.getString("P15").trim();

			this.MakePolDate = rs.getDate("MakePolDate");
			this.CustomGetPolDate = rs.getDate("CustomGetPolDate");
			if( rs.getString("riskmark") == null )
				this.riskmark = null;
			else
				this.riskmark = rs.getString("riskmark").trim();

			this.ScanDate = rs.getDate("ScanDate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			this.ReNewCount = rs.getInt("ReNewCount");
			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("BranchType3") == null )
				this.BranchType3 = null;
			else
				this.BranchType3 = rs.getString("BranchType3").trim();

			if( rs.getString("EndorsementNo") == null )
				this.EndorsementNo = null;
			else
				this.EndorsementNo = rs.getString("EndorsementNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LACommisionB表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LACommisionBSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LACommisionBSchema getSchema()
	{
		LACommisionBSchema aLACommisionBSchema = new LACommisionBSchema();
		aLACommisionBSchema.setSchema(this);
		return aLACommisionBSchema;
	}

	public LACommisionBDB getDB()
	{
		LACommisionBDB aDBOper = new LACommisionBDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACommisionB描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CommisionSN)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CommisionBaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TPayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TEnterAccDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CommDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransStandMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LastPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CurPayToDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CommDire)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DirectWage));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppendWage));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GrpFYC));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CalCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CalDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandFYCRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FYCRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FYC));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DepFYC));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CommCharge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CommCharge1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CommCharge2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CommCharge3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CommCharge4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GrpFYCRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DepFYCRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandPremRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(K1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(K2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(K3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(K4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(K5));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Flag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CalcDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayYears));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankServer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchSeries)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchAttr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P5));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P6));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P7));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P8));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P9));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(P10));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P11)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P12)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P13)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P14)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P15)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakePolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CustomGetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(riskmark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ScanDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReNewCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EndorsementNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACommisionB>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CommisionSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CommisionBaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MainPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).intValue();
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			TPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			TEnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			TConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			TMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			CommDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			TransMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			TransStandMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			LastPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			CurPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			TransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			CommDire = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			TransState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			DirectWage = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).doubleValue();
			AppendWage = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).doubleValue();
			GrpFYC = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).doubleValue();
			CalCount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
			CalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,SysConst.PACKAGESPILTER));
			StandFYCRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,38,SysConst.PACKAGESPILTER))).doubleValue();
			FYCRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,39,SysConst.PACKAGESPILTER))).doubleValue();
			FYC = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,40,SysConst.PACKAGESPILTER))).doubleValue();
			DepFYC = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).doubleValue();
			StandPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,42,SysConst.PACKAGESPILTER))).doubleValue();
			CommCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,43,SysConst.PACKAGESPILTER))).doubleValue();
			CommCharge1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,44,SysConst.PACKAGESPILTER))).doubleValue();
			CommCharge2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,45,SysConst.PACKAGESPILTER))).doubleValue();
			CommCharge3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,46,SysConst.PACKAGESPILTER))).doubleValue();
			CommCharge4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).doubleValue();
			GrpFYCRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,48,SysConst.PACKAGESPILTER))).doubleValue();
			DepFYCRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,49,SysConst.PACKAGESPILTER))).doubleValue();
			StandPremRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,50,SysConst.PACKAGESPILTER))).doubleValue();
			F1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			F2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			F3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			F4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			F5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			K1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,56,SysConst.PACKAGESPILTER))).doubleValue();
			K2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,57,SysConst.PACKAGESPILTER))).doubleValue();
			K3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,58,SysConst.PACKAGESPILTER))).doubleValue();
			K4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,59,SysConst.PACKAGESPILTER))).doubleValue();
			K5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,60,SysConst.PACKAGESPILTER))).doubleValue();
			Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			CalcDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62,SysConst.PACKAGESPILTER));
			PayYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,63,SysConst.PACKAGESPILTER))).intValue();
			PayYears= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,64,SysConst.PACKAGESPILTER))).intValue();
			Years= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,65,SysConst.PACKAGESPILTER))).intValue();
			PayCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,66,SysConst.PACKAGESPILTER))).intValue();
			SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67,SysConst.PACKAGESPILTER));
			GetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68,SysConst.PACKAGESPILTER));
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70, SysConst.PACKAGESPILTER );
			BankServer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
			AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74, SysConst.PACKAGESPILTER );
			BranchCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER );
			BranchSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76, SysConst.PACKAGESPILTER );
			BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER );
			PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 78, SysConst.PACKAGESPILTER );
			P1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,79,SysConst.PACKAGESPILTER))).doubleValue();
			P2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,80,SysConst.PACKAGESPILTER))).doubleValue();
			P3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,81,SysConst.PACKAGESPILTER))).doubleValue();
			P4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,82,SysConst.PACKAGESPILTER))).doubleValue();
			P5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,83,SysConst.PACKAGESPILTER))).doubleValue();
			P6 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,84,SysConst.PACKAGESPILTER))).doubleValue();
			P7 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,85,SysConst.PACKAGESPILTER))).doubleValue();
			P8 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,86,SysConst.PACKAGESPILTER))).doubleValue();
			P9 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,87,SysConst.PACKAGESPILTER))).doubleValue();
			P10 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,88,SysConst.PACKAGESPILTER))).doubleValue();
			P11 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 89, SysConst.PACKAGESPILTER );
			P12 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 90, SysConst.PACKAGESPILTER );
			P13 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 91, SysConst.PACKAGESPILTER );
			P14 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 92, SysConst.PACKAGESPILTER );
			P15 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 93, SysConst.PACKAGESPILTER );
			MakePolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 94,SysConst.PACKAGESPILTER));
			CustomGetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 95,SysConst.PACKAGESPILTER));
			riskmark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 96, SysConst.PACKAGESPILTER );
			ScanDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 97,SysConst.PACKAGESPILTER));
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 98, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 99,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 100, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 101,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 102, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 103, SysConst.PACKAGESPILTER );
			ReNewCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,104,SysConst.PACKAGESPILTER))).intValue();
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 105, SysConst.PACKAGESPILTER );
			BranchType3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 106, SysConst.PACKAGESPILTER );
			EndorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 107, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LACommisionBSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("EdorType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
		}
		if (FCode.equals("CommisionSN"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommisionSN));
		}
		if (FCode.equals("CommisionBaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommisionBaseNo));
		}
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("MainPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolNo));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("AppntNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskVersion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
		}
		if (FCode.equals("DutyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
		}
		if (FCode.equals("PayPlanCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayPlanCode));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("ReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
		}
		if (FCode.equals("TPayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTPayDate()));
		}
		if (FCode.equals("TEnterAccDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTEnterAccDate()));
		}
		if (FCode.equals("TConfDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTConfDate()));
		}
		if (FCode.equals("TMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTMakeDate()));
		}
		if (FCode.equals("CommDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCommDate()));
		}
		if (FCode.equals("TransMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMoney));
		}
		if (FCode.equals("TransStandMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransStandMoney));
		}
		if (FCode.equals("LastPayToDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
		}
		if (FCode.equals("CurPayToDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
		}
		if (FCode.equals("TransType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
		}
		if (FCode.equals("CommDire"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommDire));
		}
		if (FCode.equals("TransState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransState));
		}
		if (FCode.equals("DirectWage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DirectWage));
		}
		if (FCode.equals("AppendWage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppendWage));
		}
		if (FCode.equals("GrpFYC"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpFYC));
		}
		if (FCode.equals("CalCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalCount));
		}
		if (FCode.equals("CalDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCalDate()));
		}
		if (FCode.equals("StandFYCRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandFYCRate));
		}
		if (FCode.equals("FYCRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FYCRate));
		}
		if (FCode.equals("FYC"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FYC));
		}
		if (FCode.equals("DepFYC"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepFYC));
		}
		if (FCode.equals("StandPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandPrem));
		}
		if (FCode.equals("CommCharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommCharge));
		}
		if (FCode.equals("CommCharge1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommCharge1));
		}
		if (FCode.equals("CommCharge2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommCharge2));
		}
		if (FCode.equals("CommCharge3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommCharge3));
		}
		if (FCode.equals("CommCharge4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommCharge4));
		}
		if (FCode.equals("GrpFYCRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpFYCRate));
		}
		if (FCode.equals("DepFYCRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepFYCRate));
		}
		if (FCode.equals("StandPremRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandPremRate));
		}
		if (FCode.equals("F1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F1));
		}
		if (FCode.equals("F2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F2));
		}
		if (FCode.equals("F3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F3));
		}
		if (FCode.equals("F4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F4));
		}
		if (FCode.equals("F5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F5));
		}
		if (FCode.equals("K1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(K1));
		}
		if (FCode.equals("K2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(K2));
		}
		if (FCode.equals("K3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(K3));
		}
		if (FCode.equals("K4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(K4));
		}
		if (FCode.equals("K5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(K5));
		}
		if (FCode.equals("Flag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Flag));
		}
		if (FCode.equals("CalcDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCalcDate()));
		}
		if (FCode.equals("PayYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayYear));
		}
		if (FCode.equals("PayYears"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayYears));
		}
		if (FCode.equals("Years"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
		}
		if (FCode.equals("PayCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
		}
		if (FCode.equals("SignDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
		}
		if (FCode.equals("GetPolDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("BankServer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankServer));
		}
		if (FCode.equals("AgentType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("BranchCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchCode));
		}
		if (FCode.equals("BranchSeries"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchSeries));
		}
		if (FCode.equals("BranchAttr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
		}
		if (FCode.equals("PolType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
		}
		if (FCode.equals("P1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P1));
		}
		if (FCode.equals("P2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P2));
		}
		if (FCode.equals("P3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P3));
		}
		if (FCode.equals("P4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P4));
		}
		if (FCode.equals("P5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P5));
		}
		if (FCode.equals("P6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P6));
		}
		if (FCode.equals("P7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P7));
		}
		if (FCode.equals("P8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P8));
		}
		if (FCode.equals("P9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P9));
		}
		if (FCode.equals("P10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P10));
		}
		if (FCode.equals("P11"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P11));
		}
		if (FCode.equals("P12"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P12));
		}
		if (FCode.equals("P13"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P13));
		}
		if (FCode.equals("P14"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P14));
		}
		if (FCode.equals("P15"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P15));
		}
		if (FCode.equals("MakePolDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakePolDate()));
		}
		if (FCode.equals("CustomGetPolDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
		}
		if (FCode.equals("riskmark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(riskmark));
		}
		if (FCode.equals("ScanDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getScanDate()));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("ReNewCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReNewCount));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("BranchType3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType3));
		}
		if (FCode.equals("EndorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndorsementNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(EdorType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CommisionSN);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CommisionBaseNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MainPolNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(AppntNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(RiskVersion);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(DutyCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 17:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTPayDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTEnterAccDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTConfDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTMakeDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCommDate()));
				break;
			case 25:
				strFieldValue = String.valueOf(TransMoney);
				break;
			case 26:
				strFieldValue = String.valueOf(TransStandMoney);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastPayToDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCurPayToDate()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(TransType);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(CommDire);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(TransState);
				break;
			case 32:
				strFieldValue = String.valueOf(DirectWage);
				break;
			case 33:
				strFieldValue = String.valueOf(AppendWage);
				break;
			case 34:
				strFieldValue = String.valueOf(GrpFYC);
				break;
			case 35:
				strFieldValue = String.valueOf(CalCount);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCalDate()));
				break;
			case 37:
				strFieldValue = String.valueOf(StandFYCRate);
				break;
			case 38:
				strFieldValue = String.valueOf(FYCRate);
				break;
			case 39:
				strFieldValue = String.valueOf(FYC);
				break;
			case 40:
				strFieldValue = String.valueOf(DepFYC);
				break;
			case 41:
				strFieldValue = String.valueOf(StandPrem);
				break;
			case 42:
				strFieldValue = String.valueOf(CommCharge);
				break;
			case 43:
				strFieldValue = String.valueOf(CommCharge1);
				break;
			case 44:
				strFieldValue = String.valueOf(CommCharge2);
				break;
			case 45:
				strFieldValue = String.valueOf(CommCharge3);
				break;
			case 46:
				strFieldValue = String.valueOf(CommCharge4);
				break;
			case 47:
				strFieldValue = String.valueOf(GrpFYCRate);
				break;
			case 48:
				strFieldValue = String.valueOf(DepFYCRate);
				break;
			case 49:
				strFieldValue = String.valueOf(StandPremRate);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(F1);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(F2);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(F3);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(F4);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(F5);
				break;
			case 55:
				strFieldValue = String.valueOf(K1);
				break;
			case 56:
				strFieldValue = String.valueOf(K2);
				break;
			case 57:
				strFieldValue = String.valueOf(K3);
				break;
			case 58:
				strFieldValue = String.valueOf(K4);
				break;
			case 59:
				strFieldValue = String.valueOf(K5);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(Flag);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCalcDate()));
				break;
			case 62:
				strFieldValue = String.valueOf(PayYear);
				break;
			case 63:
				strFieldValue = String.valueOf(PayYears);
				break;
			case 64:
				strFieldValue = String.valueOf(Years);
				break;
			case 65:
				strFieldValue = String.valueOf(PayCount);
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
				break;
			case 68:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 69:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 70:
				strFieldValue = StrTool.GBKToUnicode(BankServer);
				break;
			case 71:
				strFieldValue = StrTool.GBKToUnicode(AgentType);
				break;
			case 72:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 73:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 74:
				strFieldValue = StrTool.GBKToUnicode(BranchCode);
				break;
			case 75:
				strFieldValue = StrTool.GBKToUnicode(BranchSeries);
				break;
			case 76:
				strFieldValue = StrTool.GBKToUnicode(BranchAttr);
				break;
			case 77:
				strFieldValue = StrTool.GBKToUnicode(PolType);
				break;
			case 78:
				strFieldValue = String.valueOf(P1);
				break;
			case 79:
				strFieldValue = String.valueOf(P2);
				break;
			case 80:
				strFieldValue = String.valueOf(P3);
				break;
			case 81:
				strFieldValue = String.valueOf(P4);
				break;
			case 82:
				strFieldValue = String.valueOf(P5);
				break;
			case 83:
				strFieldValue = String.valueOf(P6);
				break;
			case 84:
				strFieldValue = String.valueOf(P7);
				break;
			case 85:
				strFieldValue = String.valueOf(P8);
				break;
			case 86:
				strFieldValue = String.valueOf(P9);
				break;
			case 87:
				strFieldValue = String.valueOf(P10);
				break;
			case 88:
				strFieldValue = StrTool.GBKToUnicode(P11);
				break;
			case 89:
				strFieldValue = StrTool.GBKToUnicode(P12);
				break;
			case 90:
				strFieldValue = StrTool.GBKToUnicode(P13);
				break;
			case 91:
				strFieldValue = StrTool.GBKToUnicode(P14);
				break;
			case 92:
				strFieldValue = StrTool.GBKToUnicode(P15);
				break;
			case 93:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakePolDate()));
				break;
			case 94:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
				break;
			case 95:
				strFieldValue = StrTool.GBKToUnicode(riskmark);
				break;
			case 96:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getScanDate()));
				break;
			case 97:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 98:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 99:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 100:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 101:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 102:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 103:
				strFieldValue = String.valueOf(ReNewCount);
				break;
			case 104:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 105:
				strFieldValue = StrTool.GBKToUnicode(BranchType3);
				break;
			case 106:
				strFieldValue = StrTool.GBKToUnicode(EndorsementNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorType = FValue.trim();
			}
			else
				EdorType = null;
		}
		if (FCode.equalsIgnoreCase("CommisionSN"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CommisionSN = FValue.trim();
			}
			else
				CommisionSN = null;
		}
		if (FCode.equalsIgnoreCase("CommisionBaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CommisionBaseNo = FValue.trim();
			}
			else
				CommisionBaseNo = null;
		}
		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpPolNo = FValue.trim();
			}
			else
				GrpPolNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("MainPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainPolNo = FValue.trim();
			}
			else
				MainPolNo = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AppntNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntNo = FValue.trim();
			}
			else
				AppntNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskVersion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskVersion = FValue.trim();
			}
			else
				RiskVersion = null;
		}
		if (FCode.equalsIgnoreCase("DutyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DutyCode = FValue.trim();
			}
			else
				DutyCode = null;
		}
		if (FCode.equalsIgnoreCase("PayPlanCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayPlanCode = FValue.trim();
			}
			else
				PayPlanCode = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNo = FValue.trim();
			}
			else
				ReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("TPayDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TPayDate = fDate.getDate( FValue );
			}
			else
				TPayDate = null;
		}
		if (FCode.equalsIgnoreCase("TEnterAccDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TEnterAccDate = fDate.getDate( FValue );
			}
			else
				TEnterAccDate = null;
		}
		if (FCode.equalsIgnoreCase("TConfDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TConfDate = fDate.getDate( FValue );
			}
			else
				TConfDate = null;
		}
		if (FCode.equalsIgnoreCase("TMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TMakeDate = fDate.getDate( FValue );
			}
			else
				TMakeDate = null;
		}
		if (FCode.equalsIgnoreCase("CommDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CommDate = fDate.getDate( FValue );
			}
			else
				CommDate = null;
		}
		if (FCode.equalsIgnoreCase("TransMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("TransStandMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransStandMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("LastPayToDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LastPayToDate = fDate.getDate( FValue );
			}
			else
				LastPayToDate = null;
		}
		if (FCode.equalsIgnoreCase("CurPayToDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CurPayToDate = fDate.getDate( FValue );
			}
			else
				CurPayToDate = null;
		}
		if (FCode.equalsIgnoreCase("TransType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransType = FValue.trim();
			}
			else
				TransType = null;
		}
		if (FCode.equalsIgnoreCase("CommDire"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CommDire = FValue.trim();
			}
			else
				CommDire = null;
		}
		if (FCode.equalsIgnoreCase("TransState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransState = FValue.trim();
			}
			else
				TransState = null;
		}
		if (FCode.equalsIgnoreCase("DirectWage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DirectWage = d;
			}
		}
		if (FCode.equalsIgnoreCase("AppendWage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AppendWage = d;
			}
		}
		if (FCode.equalsIgnoreCase("GrpFYC"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GrpFYC = d;
			}
		}
		if (FCode.equalsIgnoreCase("CalCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CalCount = d;
			}
		}
		if (FCode.equalsIgnoreCase("CalDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CalDate = fDate.getDate( FValue );
			}
			else
				CalDate = null;
		}
		if (FCode.equalsIgnoreCase("StandFYCRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandFYCRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("FYCRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FYCRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("FYC"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FYC = d;
			}
		}
		if (FCode.equalsIgnoreCase("DepFYC"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DepFYC = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("CommCharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CommCharge = d;
			}
		}
		if (FCode.equalsIgnoreCase("CommCharge1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CommCharge1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("CommCharge2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CommCharge2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("CommCharge3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CommCharge3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("CommCharge4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CommCharge4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("GrpFYCRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GrpFYCRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("DepFYCRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DepFYCRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandPremRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandPremRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("F1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F1 = FValue.trim();
			}
			else
				F1 = null;
		}
		if (FCode.equalsIgnoreCase("F2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F2 = FValue.trim();
			}
			else
				F2 = null;
		}
		if (FCode.equalsIgnoreCase("F3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F3 = FValue.trim();
			}
			else
				F3 = null;
		}
		if (FCode.equalsIgnoreCase("F4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F4 = FValue.trim();
			}
			else
				F4 = null;
		}
		if (FCode.equalsIgnoreCase("F5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F5 = FValue.trim();
			}
			else
				F5 = null;
		}
		if (FCode.equalsIgnoreCase("K1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				K1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("K2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				K2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("K3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				K3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("K4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				K4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("K5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				K5 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Flag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Flag = FValue.trim();
			}
			else
				Flag = null;
		}
		if (FCode.equalsIgnoreCase("CalcDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CalcDate = fDate.getDate( FValue );
			}
			else
				CalcDate = null;
		}
		if (FCode.equalsIgnoreCase("PayYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayYears"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayYears = i;
			}
		}
		if (FCode.equalsIgnoreCase("Years"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Years = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("SignDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SignDate = fDate.getDate( FValue );
			}
			else
				SignDate = null;
		}
		if (FCode.equalsIgnoreCase("GetPolDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GetPolDate = fDate.getDate( FValue );
			}
			else
				GetPolDate = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("BankServer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankServer = FValue.trim();
			}
			else
				BankServer = null;
		}
		if (FCode.equalsIgnoreCase("AgentType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentType = FValue.trim();
			}
			else
				AgentType = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("BranchCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchCode = FValue.trim();
			}
			else
				BranchCode = null;
		}
		if (FCode.equalsIgnoreCase("BranchSeries"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchSeries = FValue.trim();
			}
			else
				BranchSeries = null;
		}
		if (FCode.equalsIgnoreCase("BranchAttr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchAttr = FValue.trim();
			}
			else
				BranchAttr = null;
		}
		if (FCode.equalsIgnoreCase("PolType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolType = FValue.trim();
			}
			else
				PolType = null;
		}
		if (FCode.equalsIgnoreCase("P1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P5 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P6 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P7 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P8 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P9 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				P10 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P11"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P11 = FValue.trim();
			}
			else
				P11 = null;
		}
		if (FCode.equalsIgnoreCase("P12"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P12 = FValue.trim();
			}
			else
				P12 = null;
		}
		if (FCode.equalsIgnoreCase("P13"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P13 = FValue.trim();
			}
			else
				P13 = null;
		}
		if (FCode.equalsIgnoreCase("P14"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P14 = FValue.trim();
			}
			else
				P14 = null;
		}
		if (FCode.equalsIgnoreCase("P15"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P15 = FValue.trim();
			}
			else
				P15 = null;
		}
		if (FCode.equalsIgnoreCase("MakePolDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakePolDate = fDate.getDate( FValue );
			}
			else
				MakePolDate = null;
		}
		if (FCode.equalsIgnoreCase("CustomGetPolDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CustomGetPolDate = fDate.getDate( FValue );
			}
			else
				CustomGetPolDate = null;
		}
		if (FCode.equalsIgnoreCase("riskmark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				riskmark = FValue.trim();
			}
			else
				riskmark = null;
		}
		if (FCode.equalsIgnoreCase("ScanDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ScanDate = fDate.getDate( FValue );
			}
			else
				ScanDate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("ReNewCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ReNewCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType3 = FValue.trim();
			}
			else
				BranchType3 = null;
		}
		if (FCode.equalsIgnoreCase("EndorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndorsementNo = FValue.trim();
			}
			else
				EndorsementNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LACommisionBSchema other = (LACommisionBSchema)otherObject;
		return
			(EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (EdorType == null ? other.getEdorType() == null : EdorType.equals(other.getEdorType()))
			&& (CommisionSN == null ? other.getCommisionSN() == null : CommisionSN.equals(other.getCommisionSN()))
			&& (CommisionBaseNo == null ? other.getCommisionBaseNo() == null : CommisionBaseNo.equals(other.getCommisionBaseNo()))
			&& (WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (GrpPolNo == null ? other.getGrpPolNo() == null : GrpPolNo.equals(other.getGrpPolNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (MainPolNo == null ? other.getMainPolNo() == null : MainPolNo.equals(other.getMainPolNo()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (AppntNo == null ? other.getAppntNo() == null : AppntNo.equals(other.getAppntNo()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskVersion == null ? other.getRiskVersion() == null : RiskVersion.equals(other.getRiskVersion()))
			&& (DutyCode == null ? other.getDutyCode() == null : DutyCode.equals(other.getDutyCode()))
			&& (PayPlanCode == null ? other.getPayPlanCode() == null : PayPlanCode.equals(other.getPayPlanCode()))
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& PayIntv == other.getPayIntv()
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (ReceiptNo == null ? other.getReceiptNo() == null : ReceiptNo.equals(other.getReceiptNo()))
			&& (TPayDate == null ? other.getTPayDate() == null : fDate.getString(TPayDate).equals(other.getTPayDate()))
			&& (TEnterAccDate == null ? other.getTEnterAccDate() == null : fDate.getString(TEnterAccDate).equals(other.getTEnterAccDate()))
			&& (TConfDate == null ? other.getTConfDate() == null : fDate.getString(TConfDate).equals(other.getTConfDate()))
			&& (TMakeDate == null ? other.getTMakeDate() == null : fDate.getString(TMakeDate).equals(other.getTMakeDate()))
			&& (CommDate == null ? other.getCommDate() == null : fDate.getString(CommDate).equals(other.getCommDate()))
			&& TransMoney == other.getTransMoney()
			&& TransStandMoney == other.getTransStandMoney()
			&& (LastPayToDate == null ? other.getLastPayToDate() == null : fDate.getString(LastPayToDate).equals(other.getLastPayToDate()))
			&& (CurPayToDate == null ? other.getCurPayToDate() == null : fDate.getString(CurPayToDate).equals(other.getCurPayToDate()))
			&& (TransType == null ? other.getTransType() == null : TransType.equals(other.getTransType()))
			&& (CommDire == null ? other.getCommDire() == null : CommDire.equals(other.getCommDire()))
			&& (TransState == null ? other.getTransState() == null : TransState.equals(other.getTransState()))
			&& DirectWage == other.getDirectWage()
			&& AppendWage == other.getAppendWage()
			&& GrpFYC == other.getGrpFYC()
			&& CalCount == other.getCalCount()
			&& (CalDate == null ? other.getCalDate() == null : fDate.getString(CalDate).equals(other.getCalDate()))
			&& StandFYCRate == other.getStandFYCRate()
			&& FYCRate == other.getFYCRate()
			&& FYC == other.getFYC()
			&& DepFYC == other.getDepFYC()
			&& StandPrem == other.getStandPrem()
			&& CommCharge == other.getCommCharge()
			&& CommCharge1 == other.getCommCharge1()
			&& CommCharge2 == other.getCommCharge2()
			&& CommCharge3 == other.getCommCharge3()
			&& CommCharge4 == other.getCommCharge4()
			&& GrpFYCRate == other.getGrpFYCRate()
			&& DepFYCRate == other.getDepFYCRate()
			&& StandPremRate == other.getStandPremRate()
			&& (F1 == null ? other.getF1() == null : F1.equals(other.getF1()))
			&& (F2 == null ? other.getF2() == null : F2.equals(other.getF2()))
			&& (F3 == null ? other.getF3() == null : F3.equals(other.getF3()))
			&& (F4 == null ? other.getF4() == null : F4.equals(other.getF4()))
			&& (F5 == null ? other.getF5() == null : F5.equals(other.getF5()))
			&& K1 == other.getK1()
			&& K2 == other.getK2()
			&& K3 == other.getK3()
			&& K4 == other.getK4()
			&& K5 == other.getK5()
			&& (Flag == null ? other.getFlag() == null : Flag.equals(other.getFlag()))
			&& (CalcDate == null ? other.getCalcDate() == null : fDate.getString(CalcDate).equals(other.getCalcDate()))
			&& PayYear == other.getPayYear()
			&& PayYears == other.getPayYears()
			&& Years == other.getYears()
			&& PayCount == other.getPayCount()
			&& (SignDate == null ? other.getSignDate() == null : fDate.getString(SignDate).equals(other.getSignDate()))
			&& (GetPolDate == null ? other.getGetPolDate() == null : fDate.getString(GetPolDate).equals(other.getGetPolDate()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (BankServer == null ? other.getBankServer() == null : BankServer.equals(other.getBankServer()))
			&& (AgentType == null ? other.getAgentType() == null : AgentType.equals(other.getAgentType()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (BranchCode == null ? other.getBranchCode() == null : BranchCode.equals(other.getBranchCode()))
			&& (BranchSeries == null ? other.getBranchSeries() == null : BranchSeries.equals(other.getBranchSeries()))
			&& (BranchAttr == null ? other.getBranchAttr() == null : BranchAttr.equals(other.getBranchAttr()))
			&& (PolType == null ? other.getPolType() == null : PolType.equals(other.getPolType()))
			&& P1 == other.getP1()
			&& P2 == other.getP2()
			&& P3 == other.getP3()
			&& P4 == other.getP4()
			&& P5 == other.getP5()
			&& P6 == other.getP6()
			&& P7 == other.getP7()
			&& P8 == other.getP8()
			&& P9 == other.getP9()
			&& P10 == other.getP10()
			&& (P11 == null ? other.getP11() == null : P11.equals(other.getP11()))
			&& (P12 == null ? other.getP12() == null : P12.equals(other.getP12()))
			&& (P13 == null ? other.getP13() == null : P13.equals(other.getP13()))
			&& (P14 == null ? other.getP14() == null : P14.equals(other.getP14()))
			&& (P15 == null ? other.getP15() == null : P15.equals(other.getP15()))
			&& (MakePolDate == null ? other.getMakePolDate() == null : fDate.getString(MakePolDate).equals(other.getMakePolDate()))
			&& (CustomGetPolDate == null ? other.getCustomGetPolDate() == null : fDate.getString(CustomGetPolDate).equals(other.getCustomGetPolDate()))
			&& (riskmark == null ? other.getriskmark() == null : riskmark.equals(other.getriskmark()))
			&& (ScanDate == null ? other.getScanDate() == null : fDate.getString(ScanDate).equals(other.getScanDate()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& ReNewCount == other.getReNewCount()
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (BranchType3 == null ? other.getBranchType3() == null : BranchType3.equals(other.getBranchType3()))
			&& (EndorsementNo == null ? other.getEndorsementNo() == null : EndorsementNo.equals(other.getEndorsementNo()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("EdorType") ) {
			return 1;
		}
		if( strFieldName.equals("CommisionSN") ) {
			return 2;
		}
		if( strFieldName.equals("CommisionBaseNo") ) {
			return 3;
		}
		if( strFieldName.equals("WageNo") ) {
			return 4;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 5;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return 6;
		}
		if( strFieldName.equals("ContNo") ) {
			return 7;
		}
		if( strFieldName.equals("PolNo") ) {
			return 8;
		}
		if( strFieldName.equals("MainPolNo") ) {
			return 9;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 10;
		}
		if( strFieldName.equals("AppntNo") ) {
			return 11;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 12;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return 13;
		}
		if( strFieldName.equals("DutyCode") ) {
			return 14;
		}
		if( strFieldName.equals("PayPlanCode") ) {
			return 15;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 16;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 17;
		}
		if( strFieldName.equals("PayMode") ) {
			return 18;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return 19;
		}
		if( strFieldName.equals("TPayDate") ) {
			return 20;
		}
		if( strFieldName.equals("TEnterAccDate") ) {
			return 21;
		}
		if( strFieldName.equals("TConfDate") ) {
			return 22;
		}
		if( strFieldName.equals("TMakeDate") ) {
			return 23;
		}
		if( strFieldName.equals("CommDate") ) {
			return 24;
		}
		if( strFieldName.equals("TransMoney") ) {
			return 25;
		}
		if( strFieldName.equals("TransStandMoney") ) {
			return 26;
		}
		if( strFieldName.equals("LastPayToDate") ) {
			return 27;
		}
		if( strFieldName.equals("CurPayToDate") ) {
			return 28;
		}
		if( strFieldName.equals("TransType") ) {
			return 29;
		}
		if( strFieldName.equals("CommDire") ) {
			return 30;
		}
		if( strFieldName.equals("TransState") ) {
			return 31;
		}
		if( strFieldName.equals("DirectWage") ) {
			return 32;
		}
		if( strFieldName.equals("AppendWage") ) {
			return 33;
		}
		if( strFieldName.equals("GrpFYC") ) {
			return 34;
		}
		if( strFieldName.equals("CalCount") ) {
			return 35;
		}
		if( strFieldName.equals("CalDate") ) {
			return 36;
		}
		if( strFieldName.equals("StandFYCRate") ) {
			return 37;
		}
		if( strFieldName.equals("FYCRate") ) {
			return 38;
		}
		if( strFieldName.equals("FYC") ) {
			return 39;
		}
		if( strFieldName.equals("DepFYC") ) {
			return 40;
		}
		if( strFieldName.equals("StandPrem") ) {
			return 41;
		}
		if( strFieldName.equals("CommCharge") ) {
			return 42;
		}
		if( strFieldName.equals("CommCharge1") ) {
			return 43;
		}
		if( strFieldName.equals("CommCharge2") ) {
			return 44;
		}
		if( strFieldName.equals("CommCharge3") ) {
			return 45;
		}
		if( strFieldName.equals("CommCharge4") ) {
			return 46;
		}
		if( strFieldName.equals("GrpFYCRate") ) {
			return 47;
		}
		if( strFieldName.equals("DepFYCRate") ) {
			return 48;
		}
		if( strFieldName.equals("StandPremRate") ) {
			return 49;
		}
		if( strFieldName.equals("F1") ) {
			return 50;
		}
		if( strFieldName.equals("F2") ) {
			return 51;
		}
		if( strFieldName.equals("F3") ) {
			return 52;
		}
		if( strFieldName.equals("F4") ) {
			return 53;
		}
		if( strFieldName.equals("F5") ) {
			return 54;
		}
		if( strFieldName.equals("K1") ) {
			return 55;
		}
		if( strFieldName.equals("K2") ) {
			return 56;
		}
		if( strFieldName.equals("K3") ) {
			return 57;
		}
		if( strFieldName.equals("K4") ) {
			return 58;
		}
		if( strFieldName.equals("K5") ) {
			return 59;
		}
		if( strFieldName.equals("Flag") ) {
			return 60;
		}
		if( strFieldName.equals("CalcDate") ) {
			return 61;
		}
		if( strFieldName.equals("PayYear") ) {
			return 62;
		}
		if( strFieldName.equals("PayYears") ) {
			return 63;
		}
		if( strFieldName.equals("Years") ) {
			return 64;
		}
		if( strFieldName.equals("PayCount") ) {
			return 65;
		}
		if( strFieldName.equals("SignDate") ) {
			return 66;
		}
		if( strFieldName.equals("GetPolDate") ) {
			return 67;
		}
		if( strFieldName.equals("BranchType") ) {
			return 68;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 69;
		}
		if( strFieldName.equals("BankServer") ) {
			return 70;
		}
		if( strFieldName.equals("AgentType") ) {
			return 71;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 72;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 73;
		}
		if( strFieldName.equals("BranchCode") ) {
			return 74;
		}
		if( strFieldName.equals("BranchSeries") ) {
			return 75;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return 76;
		}
		if( strFieldName.equals("PolType") ) {
			return 77;
		}
		if( strFieldName.equals("P1") ) {
			return 78;
		}
		if( strFieldName.equals("P2") ) {
			return 79;
		}
		if( strFieldName.equals("P3") ) {
			return 80;
		}
		if( strFieldName.equals("P4") ) {
			return 81;
		}
		if( strFieldName.equals("P5") ) {
			return 82;
		}
		if( strFieldName.equals("P6") ) {
			return 83;
		}
		if( strFieldName.equals("P7") ) {
			return 84;
		}
		if( strFieldName.equals("P8") ) {
			return 85;
		}
		if( strFieldName.equals("P9") ) {
			return 86;
		}
		if( strFieldName.equals("P10") ) {
			return 87;
		}
		if( strFieldName.equals("P11") ) {
			return 88;
		}
		if( strFieldName.equals("P12") ) {
			return 89;
		}
		if( strFieldName.equals("P13") ) {
			return 90;
		}
		if( strFieldName.equals("P14") ) {
			return 91;
		}
		if( strFieldName.equals("P15") ) {
			return 92;
		}
		if( strFieldName.equals("MakePolDate") ) {
			return 93;
		}
		if( strFieldName.equals("CustomGetPolDate") ) {
			return 94;
		}
		if( strFieldName.equals("riskmark") ) {
			return 95;
		}
		if( strFieldName.equals("ScanDate") ) {
			return 96;
		}
		if( strFieldName.equals("Operator") ) {
			return 97;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 98;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 99;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 100;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 101;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 102;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return 103;
		}
		if( strFieldName.equals("MarketType") ) {
			return 104;
		}
		if( strFieldName.equals("BranchType3") ) {
			return 105;
		}
		if( strFieldName.equals("EndorsementNo") ) {
			return 106;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "EdorType";
				break;
			case 2:
				strFieldName = "CommisionSN";
				break;
			case 3:
				strFieldName = "CommisionBaseNo";
				break;
			case 4:
				strFieldName = "WageNo";
				break;
			case 5:
				strFieldName = "GrpContNo";
				break;
			case 6:
				strFieldName = "GrpPolNo";
				break;
			case 7:
				strFieldName = "ContNo";
				break;
			case 8:
				strFieldName = "PolNo";
				break;
			case 9:
				strFieldName = "MainPolNo";
				break;
			case 10:
				strFieldName = "ManageCom";
				break;
			case 11:
				strFieldName = "AppntNo";
				break;
			case 12:
				strFieldName = "RiskCode";
				break;
			case 13:
				strFieldName = "RiskVersion";
				break;
			case 14:
				strFieldName = "DutyCode";
				break;
			case 15:
				strFieldName = "PayPlanCode";
				break;
			case 16:
				strFieldName = "CValiDate";
				break;
			case 17:
				strFieldName = "PayIntv";
				break;
			case 18:
				strFieldName = "PayMode";
				break;
			case 19:
				strFieldName = "ReceiptNo";
				break;
			case 20:
				strFieldName = "TPayDate";
				break;
			case 21:
				strFieldName = "TEnterAccDate";
				break;
			case 22:
				strFieldName = "TConfDate";
				break;
			case 23:
				strFieldName = "TMakeDate";
				break;
			case 24:
				strFieldName = "CommDate";
				break;
			case 25:
				strFieldName = "TransMoney";
				break;
			case 26:
				strFieldName = "TransStandMoney";
				break;
			case 27:
				strFieldName = "LastPayToDate";
				break;
			case 28:
				strFieldName = "CurPayToDate";
				break;
			case 29:
				strFieldName = "TransType";
				break;
			case 30:
				strFieldName = "CommDire";
				break;
			case 31:
				strFieldName = "TransState";
				break;
			case 32:
				strFieldName = "DirectWage";
				break;
			case 33:
				strFieldName = "AppendWage";
				break;
			case 34:
				strFieldName = "GrpFYC";
				break;
			case 35:
				strFieldName = "CalCount";
				break;
			case 36:
				strFieldName = "CalDate";
				break;
			case 37:
				strFieldName = "StandFYCRate";
				break;
			case 38:
				strFieldName = "FYCRate";
				break;
			case 39:
				strFieldName = "FYC";
				break;
			case 40:
				strFieldName = "DepFYC";
				break;
			case 41:
				strFieldName = "StandPrem";
				break;
			case 42:
				strFieldName = "CommCharge";
				break;
			case 43:
				strFieldName = "CommCharge1";
				break;
			case 44:
				strFieldName = "CommCharge2";
				break;
			case 45:
				strFieldName = "CommCharge3";
				break;
			case 46:
				strFieldName = "CommCharge4";
				break;
			case 47:
				strFieldName = "GrpFYCRate";
				break;
			case 48:
				strFieldName = "DepFYCRate";
				break;
			case 49:
				strFieldName = "StandPremRate";
				break;
			case 50:
				strFieldName = "F1";
				break;
			case 51:
				strFieldName = "F2";
				break;
			case 52:
				strFieldName = "F3";
				break;
			case 53:
				strFieldName = "F4";
				break;
			case 54:
				strFieldName = "F5";
				break;
			case 55:
				strFieldName = "K1";
				break;
			case 56:
				strFieldName = "K2";
				break;
			case 57:
				strFieldName = "K3";
				break;
			case 58:
				strFieldName = "K4";
				break;
			case 59:
				strFieldName = "K5";
				break;
			case 60:
				strFieldName = "Flag";
				break;
			case 61:
				strFieldName = "CalcDate";
				break;
			case 62:
				strFieldName = "PayYear";
				break;
			case 63:
				strFieldName = "PayYears";
				break;
			case 64:
				strFieldName = "Years";
				break;
			case 65:
				strFieldName = "PayCount";
				break;
			case 66:
				strFieldName = "SignDate";
				break;
			case 67:
				strFieldName = "GetPolDate";
				break;
			case 68:
				strFieldName = "BranchType";
				break;
			case 69:
				strFieldName = "AgentCom";
				break;
			case 70:
				strFieldName = "BankServer";
				break;
			case 71:
				strFieldName = "AgentType";
				break;
			case 72:
				strFieldName = "AgentCode";
				break;
			case 73:
				strFieldName = "AgentGroup";
				break;
			case 74:
				strFieldName = "BranchCode";
				break;
			case 75:
				strFieldName = "BranchSeries";
				break;
			case 76:
				strFieldName = "BranchAttr";
				break;
			case 77:
				strFieldName = "PolType";
				break;
			case 78:
				strFieldName = "P1";
				break;
			case 79:
				strFieldName = "P2";
				break;
			case 80:
				strFieldName = "P3";
				break;
			case 81:
				strFieldName = "P4";
				break;
			case 82:
				strFieldName = "P5";
				break;
			case 83:
				strFieldName = "P6";
				break;
			case 84:
				strFieldName = "P7";
				break;
			case 85:
				strFieldName = "P8";
				break;
			case 86:
				strFieldName = "P9";
				break;
			case 87:
				strFieldName = "P10";
				break;
			case 88:
				strFieldName = "P11";
				break;
			case 89:
				strFieldName = "P12";
				break;
			case 90:
				strFieldName = "P13";
				break;
			case 91:
				strFieldName = "P14";
				break;
			case 92:
				strFieldName = "P15";
				break;
			case 93:
				strFieldName = "MakePolDate";
				break;
			case 94:
				strFieldName = "CustomGetPolDate";
				break;
			case 95:
				strFieldName = "riskmark";
				break;
			case 96:
				strFieldName = "ScanDate";
				break;
			case 97:
				strFieldName = "Operator";
				break;
			case 98:
				strFieldName = "MakeDate";
				break;
			case 99:
				strFieldName = "MakeTime";
				break;
			case 100:
				strFieldName = "ModifyDate";
				break;
			case 101:
				strFieldName = "ModifyTime";
				break;
			case 102:
				strFieldName = "BranchType2";
				break;
			case 103:
				strFieldName = "ReNewCount";
				break;
			case 104:
				strFieldName = "MarketType";
				break;
			case 105:
				strFieldName = "BranchType3";
				break;
			case 106:
				strFieldName = "EndorsementNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CommisionSN") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CommisionBaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DutyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayPlanCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TPayDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TEnterAccDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TConfDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CommDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TransMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TransStandMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("LastPayToDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CurPayToDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TransType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CommDire") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DirectWage") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AppendWage") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GrpFYC") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CalCount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CalDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StandFYCRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FYCRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FYC") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DepFYC") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CommCharge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CommCharge1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CommCharge2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CommCharge3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CommCharge4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GrpFYCRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DepFYCRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandPremRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("K1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("K2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("K3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("K4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("K5") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Flag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalcDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PayYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayYears") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Years") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SignDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GetPolDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankServer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchSeries") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P5") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P6") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P7") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P8") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P9") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P10") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P11") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P12") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P13") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P14") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P15") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakePolDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CustomGetPolDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("riskmark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ScanDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_INT;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 33:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 34:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 36:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 37:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 38:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 39:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 40:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 41:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 42:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 43:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 44:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 45:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 46:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 47:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 48:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 49:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 56:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 57:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 58:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 59:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 62:
				nFieldType = Schema.TYPE_INT;
				break;
			case 63:
				nFieldType = Schema.TYPE_INT;
				break;
			case 64:
				nFieldType = Schema.TYPE_INT;
				break;
			case 65:
				nFieldType = Schema.TYPE_INT;
				break;
			case 66:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 67:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 68:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 69:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 70:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 71:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 72:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 73:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 74:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 75:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 76:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 77:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 78:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 79:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 80:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 81:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 82:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 83:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 84:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 85:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 86:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 87:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 88:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 89:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 90:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 91:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 92:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 93:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 94:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 95:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 96:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 97:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 98:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 99:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 100:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 101:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 102:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 103:
				nFieldType = Schema.TYPE_INT;
				break;
			case 104:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 105:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 106:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
