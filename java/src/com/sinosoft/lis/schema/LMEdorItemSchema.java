/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMEdorItemDB;

/*
 * <p>ClassName: LMEdorItemSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: FP-Picch-SPTO-刘振江-保全项目定义表-2017-11-22
 * @CreateDate：2017-11-22
 */
public class LMEdorItemSchema implements Schema, Cloneable
{
	// @Field
	/** 保全项目编码 */
	private String EdorCode;
	/** 保全项目名称 */
	private String EdorName;
	/** 申请对象类型 */
	private String AppObj;
	/** 页面展示层次标记 */
	private String DisplayFlag;
	/** 是否需要重算 */
	private String CalFlag;
	/** 界面中是否显示项目明细 */
	private String NeedDetail;
	/** 是否需要打印保全清单 */
	private String GrpNeedList;
	/** 保全权限 */
	private String EdorPopedom;
	/** 项目类型 */
	private String EdorTypeFlag;
	/** 是否需要扫描件 */
	private String IsNeedScanning;

	public static final int FIELDNUM = 10;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LMEdorItemSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "EdorCode";
		pk[1] = "AppObj";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LMEdorItemSchema cloned = (LMEdorItemSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorCode()
	{
		return EdorCode;
	}
	public void setEdorCode(String aEdorCode)
	{
		EdorCode = aEdorCode;
	}
	public String getEdorName()
	{
		return EdorName;
	}
	public void setEdorName(String aEdorName)
	{
		EdorName = aEdorName;
	}
	public String getAppObj()
	{
		return AppObj;
	}
	public void setAppObj(String aAppObj)
	{
		AppObj = aAppObj;
	}
	public String getDisplayFlag()
	{
		return DisplayFlag;
	}
	public void setDisplayFlag(String aDisplayFlag)
	{
		DisplayFlag = aDisplayFlag;
	}
	public String getCalFlag()
	{
		return CalFlag;
	}
	public void setCalFlag(String aCalFlag)
	{
		CalFlag = aCalFlag;
	}
	public String getNeedDetail()
	{
		return NeedDetail;
	}
	public void setNeedDetail(String aNeedDetail)
	{
		NeedDetail = aNeedDetail;
	}
	public String getGrpNeedList()
	{
		return GrpNeedList;
	}
	public void setGrpNeedList(String aGrpNeedList)
	{
		GrpNeedList = aGrpNeedList;
	}
	public String getEdorPopedom()
	{
		return EdorPopedom;
	}
	public void setEdorPopedom(String aEdorPopedom)
	{
		EdorPopedom = aEdorPopedom;
	}
	public String getEdorTypeFlag()
	{
		return EdorTypeFlag;
	}
	public void setEdorTypeFlag(String aEdorTypeFlag)
	{
		EdorTypeFlag = aEdorTypeFlag;
	}
	public String getIsNeedScanning()
	{
		return IsNeedScanning;
	}
	public void setIsNeedScanning(String aIsNeedScanning)
	{
		IsNeedScanning = aIsNeedScanning;
	}

	/**
	* 使用另外一个 LMEdorItemSchema 对象给 Schema 赋值
	* @param: aLMEdorItemSchema LMEdorItemSchema
	**/
	public void setSchema(LMEdorItemSchema aLMEdorItemSchema)
	{
		this.EdorCode = aLMEdorItemSchema.getEdorCode();
		this.EdorName = aLMEdorItemSchema.getEdorName();
		this.AppObj = aLMEdorItemSchema.getAppObj();
		this.DisplayFlag = aLMEdorItemSchema.getDisplayFlag();
		this.CalFlag = aLMEdorItemSchema.getCalFlag();
		this.NeedDetail = aLMEdorItemSchema.getNeedDetail();
		this.GrpNeedList = aLMEdorItemSchema.getGrpNeedList();
		this.EdorPopedom = aLMEdorItemSchema.getEdorPopedom();
		this.EdorTypeFlag = aLMEdorItemSchema.getEdorTypeFlag();
		this.IsNeedScanning = aLMEdorItemSchema.getIsNeedScanning();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorCode") == null )
				this.EdorCode = null;
			else
				this.EdorCode = rs.getString("EdorCode").trim();

			if( rs.getString("EdorName") == null )
				this.EdorName = null;
			else
				this.EdorName = rs.getString("EdorName").trim();

			if( rs.getString("AppObj") == null )
				this.AppObj = null;
			else
				this.AppObj = rs.getString("AppObj").trim();

			if( rs.getString("DisplayFlag") == null )
				this.DisplayFlag = null;
			else
				this.DisplayFlag = rs.getString("DisplayFlag").trim();

			if( rs.getString("CalFlag") == null )
				this.CalFlag = null;
			else
				this.CalFlag = rs.getString("CalFlag").trim();

			if( rs.getString("NeedDetail") == null )
				this.NeedDetail = null;
			else
				this.NeedDetail = rs.getString("NeedDetail").trim();

			if( rs.getString("GrpNeedList") == null )
				this.GrpNeedList = null;
			else
				this.GrpNeedList = rs.getString("GrpNeedList").trim();

			if( rs.getString("EdorPopedom") == null )
				this.EdorPopedom = null;
			else
				this.EdorPopedom = rs.getString("EdorPopedom").trim();

			if( rs.getString("EdorTypeFlag") == null )
				this.EdorTypeFlag = null;
			else
				this.EdorTypeFlag = rs.getString("EdorTypeFlag").trim();

			if( rs.getString("IsNeedScanning") == null )
				this.IsNeedScanning = null;
			else
				this.IsNeedScanning = rs.getString("IsNeedScanning").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LMEdorItem表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMEdorItemSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LMEdorItemSchema getSchema()
	{
		LMEdorItemSchema aLMEdorItemSchema = new LMEdorItemSchema();
		aLMEdorItemSchema.setSchema(this);
		return aLMEdorItemSchema;
	}

	public LMEdorItemDB getDB()
	{
		LMEdorItemDB aDBOper = new LMEdorItemDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorItem描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppObj)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DisplayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NeedDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNeedList)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorPopedom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorTypeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IsNeedScanning));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorItem>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			EdorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AppObj = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DisplayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			NeedDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			GrpNeedList = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			EdorPopedom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			EdorTypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			IsNeedScanning = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMEdorItemSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorCode));
		}
		if (FCode.equals("EdorName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorName));
		}
		if (FCode.equals("AppObj"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppObj));
		}
		if (FCode.equals("DisplayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DisplayFlag));
		}
		if (FCode.equals("CalFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalFlag));
		}
		if (FCode.equals("NeedDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NeedDetail));
		}
		if (FCode.equals("GrpNeedList"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNeedList));
		}
		if (FCode.equals("EdorPopedom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorPopedom));
		}
		if (FCode.equals("EdorTypeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorTypeFlag));
		}
		if (FCode.equals("IsNeedScanning"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IsNeedScanning));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(EdorName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AppObj);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(DisplayFlag);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CalFlag);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(NeedDetail);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(GrpNeedList);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(EdorPopedom);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(EdorTypeFlag);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(IsNeedScanning);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorCode = FValue.trim();
			}
			else
				EdorCode = null;
		}
		if (FCode.equalsIgnoreCase("EdorName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorName = FValue.trim();
			}
			else
				EdorName = null;
		}
		if (FCode.equalsIgnoreCase("AppObj"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppObj = FValue.trim();
			}
			else
				AppObj = null;
		}
		if (FCode.equalsIgnoreCase("DisplayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DisplayFlag = FValue.trim();
			}
			else
				DisplayFlag = null;
		}
		if (FCode.equalsIgnoreCase("CalFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalFlag = FValue.trim();
			}
			else
				CalFlag = null;
		}
		if (FCode.equalsIgnoreCase("NeedDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NeedDetail = FValue.trim();
			}
			else
				NeedDetail = null;
		}
		if (FCode.equalsIgnoreCase("GrpNeedList"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNeedList = FValue.trim();
			}
			else
				GrpNeedList = null;
		}
		if (FCode.equalsIgnoreCase("EdorPopedom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorPopedom = FValue.trim();
			}
			else
				EdorPopedom = null;
		}
		if (FCode.equalsIgnoreCase("EdorTypeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorTypeFlag = FValue.trim();
			}
			else
				EdorTypeFlag = null;
		}
		if (FCode.equalsIgnoreCase("IsNeedScanning"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IsNeedScanning = FValue.trim();
			}
			else
				IsNeedScanning = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LMEdorItemSchema other = (LMEdorItemSchema)otherObject;
		return
			(EdorCode == null ? other.getEdorCode() == null : EdorCode.equals(other.getEdorCode()))
			&& (EdorName == null ? other.getEdorName() == null : EdorName.equals(other.getEdorName()))
			&& (AppObj == null ? other.getAppObj() == null : AppObj.equals(other.getAppObj()))
			&& (DisplayFlag == null ? other.getDisplayFlag() == null : DisplayFlag.equals(other.getDisplayFlag()))
			&& (CalFlag == null ? other.getCalFlag() == null : CalFlag.equals(other.getCalFlag()))
			&& (NeedDetail == null ? other.getNeedDetail() == null : NeedDetail.equals(other.getNeedDetail()))
			&& (GrpNeedList == null ? other.getGrpNeedList() == null : GrpNeedList.equals(other.getGrpNeedList()))
			&& (EdorPopedom == null ? other.getEdorPopedom() == null : EdorPopedom.equals(other.getEdorPopedom()))
			&& (EdorTypeFlag == null ? other.getEdorTypeFlag() == null : EdorTypeFlag.equals(other.getEdorTypeFlag()))
			&& (IsNeedScanning == null ? other.getIsNeedScanning() == null : IsNeedScanning.equals(other.getIsNeedScanning()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorCode") ) {
			return 0;
		}
		if( strFieldName.equals("EdorName") ) {
			return 1;
		}
		if( strFieldName.equals("AppObj") ) {
			return 2;
		}
		if( strFieldName.equals("DisplayFlag") ) {
			return 3;
		}
		if( strFieldName.equals("CalFlag") ) {
			return 4;
		}
		if( strFieldName.equals("NeedDetail") ) {
			return 5;
		}
		if( strFieldName.equals("GrpNeedList") ) {
			return 6;
		}
		if( strFieldName.equals("EdorPopedom") ) {
			return 7;
		}
		if( strFieldName.equals("EdorTypeFlag") ) {
			return 8;
		}
		if( strFieldName.equals("IsNeedScanning") ) {
			return 9;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorCode";
				break;
			case 1:
				strFieldName = "EdorName";
				break;
			case 2:
				strFieldName = "AppObj";
				break;
			case 3:
				strFieldName = "DisplayFlag";
				break;
			case 4:
				strFieldName = "CalFlag";
				break;
			case 5:
				strFieldName = "NeedDetail";
				break;
			case 6:
				strFieldName = "GrpNeedList";
				break;
			case 7:
				strFieldName = "EdorPopedom";
				break;
			case 8:
				strFieldName = "EdorTypeFlag";
				break;
			case 9:
				strFieldName = "IsNeedScanning";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppObj") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DisplayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NeedDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNeedList") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorPopedom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorTypeFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IsNeedScanning") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
