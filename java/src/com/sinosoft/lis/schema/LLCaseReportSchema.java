/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLCaseReportDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLCaseReportSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-11-09
 */
public class LLCaseReportSchema implements Schema, Cloneable {
    // @Field
    /** 流水号 */
    private String SerialNo;
    /** 受理方式 */
    private String RgtType;
    /** 报案人姓名 */
    private String RptorName;
    /** 报案人性别 */
    private String RptorSex;
    /** 报案人与被保人关系 */
    private String Relation;
    /** 报案人地址 */
    private String RptorAddress;
    /** 报案人电话 */
    private String RptorPhone;
    /** 报案人手机 */
    private String RptorMobile;
    /** 报案人电邮 */
    private String Email;
    /** 报案人邮政编码 */
    private String PostCode;
    /** 报案人证件类型 */
    private String RptorIDType;
    /** 报案人证件号码 */
    private String RptorIDNo;
    /** 报案日期 */
    private Date RptDate;
    /** 出险人客户号 */
    private String CustomerNo;
    /** 出险人名称 */
    private String CustomerName;
    /** 出险人性别 */
    private String CustomerSex;
    /** 出险人年龄 */
    private int CustomerAge;
    /** 出险人证件类型 */
    private String CustomerIDType;
    /** 出险人证件号码 */
    private String CustomerIDNo;
    /** 出险日期 */
    private Date AccidentDate;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 出险描述 */
    private String AccdentDesc;
    /** 批次号 */
    private String BatchNo;
    /** 批数据的日期 */
    private Date BatchDate;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 27; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLCaseReportSchema() {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LLCaseReportSchema cloned = (LLCaseReportSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    public String getRgtType() {
        return RgtType;
    }

    public void setRgtType(String aRgtType) {
        RgtType = aRgtType;
    }

    public String getRptorName() {
        return RptorName;
    }

    public void setRptorName(String aRptorName) {
        RptorName = aRptorName;
    }

    public String getRptorSex() {
        return RptorSex;
    }

    public void setRptorSex(String aRptorSex) {
        RptorSex = aRptorSex;
    }

    public String getRelation() {
        return Relation;
    }

    public void setRelation(String aRelation) {
        Relation = aRelation;
    }

    public String getRptorAddress() {
        return RptorAddress;
    }

    public void setRptorAddress(String aRptorAddress) {
        RptorAddress = aRptorAddress;
    }

    public String getRptorPhone() {
        return RptorPhone;
    }

    public void setRptorPhone(String aRptorPhone) {
        RptorPhone = aRptorPhone;
    }

    public String getRptorMobile() {
        return RptorMobile;
    }

    public void setRptorMobile(String aRptorMobile) {
        RptorMobile = aRptorMobile;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String aEmail) {
        Email = aEmail;
    }

    public String getPostCode() {
        return PostCode;
    }

    public void setPostCode(String aPostCode) {
        PostCode = aPostCode;
    }

    public String getRptorIDType() {
        return RptorIDType;
    }

    public void setRptorIDType(String aRptorIDType) {
        RptorIDType = aRptorIDType;
    }

    public String getRptorIDNo() {
        return RptorIDNo;
    }

    public void setRptorIDNo(String aRptorIDNo) {
        RptorIDNo = aRptorIDNo;
    }

    public String getRptDate() {
        if (RptDate != null) {
            return fDate.getString(RptDate);
        } else {
            return null;
        }
    }

    public void setRptDate(Date aRptDate) {
        RptDate = aRptDate;
    }

    public void setRptDate(String aRptDate) {
        if (aRptDate != null && !aRptDate.equals("")) {
            RptDate = fDate.getDate(aRptDate);
        } else {
            RptDate = null;
        }
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String aCustomerName) {
        CustomerName = aCustomerName;
    }

    public String getCustomerSex() {
        return CustomerSex;
    }

    public void setCustomerSex(String aCustomerSex) {
        CustomerSex = aCustomerSex;
    }

    public int getCustomerAge() {
        return CustomerAge;
    }

    public void setCustomerAge(int aCustomerAge) {
        CustomerAge = aCustomerAge;
    }

    public void setCustomerAge(String aCustomerAge) {
        if (aCustomerAge != null && !aCustomerAge.equals("")) {
            Integer tInteger = new Integer(aCustomerAge);
            int i = tInteger.intValue();
            CustomerAge = i;
        }
    }

    public String getCustomerIDType() {
        return CustomerIDType;
    }

    public void setCustomerIDType(String aCustomerIDType) {
        CustomerIDType = aCustomerIDType;
    }

    public String getCustomerIDNo() {
        return CustomerIDNo;
    }

    public void setCustomerIDNo(String aCustomerIDNo) {
        CustomerIDNo = aCustomerIDNo;
    }

    public String getAccidentDate() {
        if (AccidentDate != null) {
            return fDate.getString(AccidentDate);
        } else {
            return null;
        }
    }

    public void setAccidentDate(Date aAccidentDate) {
        AccidentDate = aAccidentDate;
    }

    public void setAccidentDate(String aAccidentDate) {
        if (aAccidentDate != null && !aAccidentDate.equals("")) {
            AccidentDate = fDate.getDate(aAccidentDate);
        } else {
            AccidentDate = null;
        }
    }

    public String getMngCom() {
        return MngCom;
    }

    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getAccdentDesc() {
        return AccdentDesc;
    }

    public void setAccdentDesc(String aAccdentDesc) {
        AccdentDesc = aAccdentDesc;
    }

    public String getBatchNo() {
        return BatchNo;
    }

    public void setBatchNo(String aBatchNo) {
        BatchNo = aBatchNo;
    }

    public String getBatchDate() {
        if (BatchDate != null) {
            return fDate.getString(BatchDate);
        } else {
            return null;
        }
    }

    public void setBatchDate(Date aBatchDate) {
        BatchDate = aBatchDate;
    }

    public void setBatchDate(String aBatchDate) {
        if (aBatchDate != null && !aBatchDate.equals("")) {
            BatchDate = fDate.getDate(aBatchDate);
        } else {
            BatchDate = null;
        }
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LLCaseReportSchema 对象给 Schema 赋值
     * @param: aLLCaseReportSchema LLCaseReportSchema
     **/
    public void setSchema(LLCaseReportSchema aLLCaseReportSchema) {
        this.SerialNo = aLLCaseReportSchema.getSerialNo();
        this.RgtType = aLLCaseReportSchema.getRgtType();
        this.RptorName = aLLCaseReportSchema.getRptorName();
        this.RptorSex = aLLCaseReportSchema.getRptorSex();
        this.Relation = aLLCaseReportSchema.getRelation();
        this.RptorAddress = aLLCaseReportSchema.getRptorAddress();
        this.RptorPhone = aLLCaseReportSchema.getRptorPhone();
        this.RptorMobile = aLLCaseReportSchema.getRptorMobile();
        this.Email = aLLCaseReportSchema.getEmail();
        this.PostCode = aLLCaseReportSchema.getPostCode();
        this.RptorIDType = aLLCaseReportSchema.getRptorIDType();
        this.RptorIDNo = aLLCaseReportSchema.getRptorIDNo();
        this.RptDate = fDate.getDate(aLLCaseReportSchema.getRptDate());
        this.CustomerNo = aLLCaseReportSchema.getCustomerNo();
        this.CustomerName = aLLCaseReportSchema.getCustomerName();
        this.CustomerSex = aLLCaseReportSchema.getCustomerSex();
        this.CustomerAge = aLLCaseReportSchema.getCustomerAge();
        this.CustomerIDType = aLLCaseReportSchema.getCustomerIDType();
        this.CustomerIDNo = aLLCaseReportSchema.getCustomerIDNo();
        this.AccidentDate = fDate.getDate(aLLCaseReportSchema.getAccidentDate());
        this.MngCom = aLLCaseReportSchema.getMngCom();
        this.Operator = aLLCaseReportSchema.getOperator();
        this.AccdentDesc = aLLCaseReportSchema.getAccdentDesc();
        this.BatchNo = aLLCaseReportSchema.getBatchNo();
        this.BatchDate = fDate.getDate(aLLCaseReportSchema.getBatchDate());
        this.MakeDate = fDate.getDate(aLLCaseReportSchema.getMakeDate());
        this.MakeTime = aLLCaseReportSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null) {
                this.SerialNo = null;
            } else {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("RgtType") == null) {
                this.RgtType = null;
            } else {
                this.RgtType = rs.getString("RgtType").trim();
            }

            if (rs.getString("RptorName") == null) {
                this.RptorName = null;
            } else {
                this.RptorName = rs.getString("RptorName").trim();
            }

            if (rs.getString("RptorSex") == null) {
                this.RptorSex = null;
            } else {
                this.RptorSex = rs.getString("RptorSex").trim();
            }

            if (rs.getString("Relation") == null) {
                this.Relation = null;
            } else {
                this.Relation = rs.getString("Relation").trim();
            }

            if (rs.getString("RptorAddress") == null) {
                this.RptorAddress = null;
            } else {
                this.RptorAddress = rs.getString("RptorAddress").trim();
            }

            if (rs.getString("RptorPhone") == null) {
                this.RptorPhone = null;
            } else {
                this.RptorPhone = rs.getString("RptorPhone").trim();
            }

            if (rs.getString("RptorMobile") == null) {
                this.RptorMobile = null;
            } else {
                this.RptorMobile = rs.getString("RptorMobile").trim();
            }

            if (rs.getString("Email") == null) {
                this.Email = null;
            } else {
                this.Email = rs.getString("Email").trim();
            }

            if (rs.getString("PostCode") == null) {
                this.PostCode = null;
            } else {
                this.PostCode = rs.getString("PostCode").trim();
            }

            if (rs.getString("RptorIDType") == null) {
                this.RptorIDType = null;
            } else {
                this.RptorIDType = rs.getString("RptorIDType").trim();
            }

            if (rs.getString("RptorIDNo") == null) {
                this.RptorIDNo = null;
            } else {
                this.RptorIDNo = rs.getString("RptorIDNo").trim();
            }

            this.RptDate = rs.getDate("RptDate");
            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("CustomerName") == null) {
                this.CustomerName = null;
            } else {
                this.CustomerName = rs.getString("CustomerName").trim();
            }

            if (rs.getString("CustomerSex") == null) {
                this.CustomerSex = null;
            } else {
                this.CustomerSex = rs.getString("CustomerSex").trim();
            }

            this.CustomerAge = rs.getInt("CustomerAge");
            if (rs.getString("CustomerIDType") == null) {
                this.CustomerIDType = null;
            } else {
                this.CustomerIDType = rs.getString("CustomerIDType").trim();
            }

            if (rs.getString("CustomerIDNo") == null) {
                this.CustomerIDNo = null;
            } else {
                this.CustomerIDNo = rs.getString("CustomerIDNo").trim();
            }

            this.AccidentDate = rs.getDate("AccidentDate");
            if (rs.getString("MngCom") == null) {
                this.MngCom = null;
            } else {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("AccdentDesc") == null) {
                this.AccdentDesc = null;
            } else {
                this.AccdentDesc = rs.getString("AccdentDesc").trim();
            }

            if (rs.getString("BatchNo") == null) {
                this.BatchNo = null;
            } else {
                this.BatchNo = rs.getString("BatchNo").trim();
            }

            this.BatchDate = rs.getDate("BatchDate");
            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LLCaseReport表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseReportSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLCaseReportSchema getSchema() {
        LLCaseReportSchema aLLCaseReportSchema = new LLCaseReportSchema();
        aLLCaseReportSchema.setSchema(this);
        return aLLCaseReportSchema;
    }

    public LLCaseReportDB getDB() {
        LLCaseReportDB aDBOper = new LLCaseReportDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseReport描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RptorName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RptorSex));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Relation));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RptorAddress));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RptorPhone));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RptorMobile));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Email));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PostCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RptorIDType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RptorIDNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(RptDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerSex));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CustomerAge));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerIDType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerIDNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(AccidentDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MngCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccdentDesc));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BatchNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(BatchDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseReport>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RgtType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            RptorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            RptorSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            Relation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            RptorAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            RptorPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            RptorMobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            Email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            PostCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            RptorIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
            RptorIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            RptDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                          SysConst.PACKAGESPILTER);
            CustomerSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
            CustomerAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).intValue();
            CustomerIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            18, SysConst.PACKAGESPILTER);
            CustomerIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                          SysConst.PACKAGESPILTER);
            AccidentDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            AccdentDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                         SysConst.PACKAGESPILTER);
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                     SysConst.PACKAGESPILTER);
            BatchDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 26, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseReportSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("RgtType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtType));
        }
        if (FCode.equals("RptorName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorName));
        }
        if (FCode.equals("RptorSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorSex));
        }
        if (FCode.equals("Relation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
        }
        if (FCode.equals("RptorAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorAddress));
        }
        if (FCode.equals("RptorPhone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorPhone));
        }
        if (FCode.equals("RptorMobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorMobile));
        }
        if (FCode.equals("Email")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
        }
        if (FCode.equals("PostCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostCode));
        }
        if (FCode.equals("RptorIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorIDType));
        }
        if (FCode.equals("RptorIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorIDNo));
        }
        if (FCode.equals("RptDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getRptDate()));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("CustomerName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
        }
        if (FCode.equals("CustomerSex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerSex));
        }
        if (FCode.equals("CustomerAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerAge));
        }
        if (FCode.equals("CustomerIDType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerIDType));
        }
        if (FCode.equals("CustomerIDNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerIDNo));
        }
        if (FCode.equals("AccidentDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getAccidentDate()));
        }
        if (FCode.equals("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("AccdentDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccdentDesc));
        }
        if (FCode.equals("BatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
        }
        if (FCode.equals("BatchDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getBatchDate()));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SerialNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RgtType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(RptorName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(RptorSex);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(Relation);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(RptorAddress);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(RptorPhone);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(RptorMobile);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(Email);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(PostCode);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(RptorIDType);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(RptorIDNo);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getRptDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(CustomerName);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(CustomerSex);
            break;
        case 16:
            strFieldValue = String.valueOf(CustomerAge);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(CustomerIDType);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(CustomerIDNo);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getAccidentDate()));
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(MngCom);
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(AccdentDesc);
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(BatchNo);
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getBatchDate()));
            break;
        case 25:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 26:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNo = FValue.trim();
            } else {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RgtType")) {
            if (FValue != null && !FValue.equals("")) {
                RgtType = FValue.trim();
            } else {
                RgtType = null;
            }
        }
        if (FCode.equalsIgnoreCase("RptorName")) {
            if (FValue != null && !FValue.equals("")) {
                RptorName = FValue.trim();
            } else {
                RptorName = null;
            }
        }
        if (FCode.equalsIgnoreCase("RptorSex")) {
            if (FValue != null && !FValue.equals("")) {
                RptorSex = FValue.trim();
            } else {
                RptorSex = null;
            }
        }
        if (FCode.equalsIgnoreCase("Relation")) {
            if (FValue != null && !FValue.equals("")) {
                Relation = FValue.trim();
            } else {
                Relation = null;
            }
        }
        if (FCode.equalsIgnoreCase("RptorAddress")) {
            if (FValue != null && !FValue.equals("")) {
                RptorAddress = FValue.trim();
            } else {
                RptorAddress = null;
            }
        }
        if (FCode.equalsIgnoreCase("RptorPhone")) {
            if (FValue != null && !FValue.equals("")) {
                RptorPhone = FValue.trim();
            } else {
                RptorPhone = null;
            }
        }
        if (FCode.equalsIgnoreCase("RptorMobile")) {
            if (FValue != null && !FValue.equals("")) {
                RptorMobile = FValue.trim();
            } else {
                RptorMobile = null;
            }
        }
        if (FCode.equalsIgnoreCase("Email")) {
            if (FValue != null && !FValue.equals("")) {
                Email = FValue.trim();
            } else {
                Email = null;
            }
        }
        if (FCode.equalsIgnoreCase("PostCode")) {
            if (FValue != null && !FValue.equals("")) {
                PostCode = FValue.trim();
            } else {
                PostCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RptorIDType")) {
            if (FValue != null && !FValue.equals("")) {
                RptorIDType = FValue.trim();
            } else {
                RptorIDType = null;
            }
        }
        if (FCode.equalsIgnoreCase("RptorIDNo")) {
            if (FValue != null && !FValue.equals("")) {
                RptorIDNo = FValue.trim();
            } else {
                RptorIDNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RptDate")) {
            if (FValue != null && !FValue.equals("")) {
                RptDate = fDate.getDate(FValue);
            } else {
                RptDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerName")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerName = FValue.trim();
            } else {
                CustomerName = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerSex")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerSex = FValue.trim();
            } else {
                CustomerSex = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerAge")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CustomerAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerIDType")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerIDType = FValue.trim();
            } else {
                CustomerIDType = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerIDNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerIDNo = FValue.trim();
            } else {
                CustomerIDNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("AccidentDate")) {
            if (FValue != null && !FValue.equals("")) {
                AccidentDate = fDate.getDate(FValue);
            } else {
                AccidentDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if (FValue != null && !FValue.equals("")) {
                MngCom = FValue.trim();
            } else {
                MngCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("AccdentDesc")) {
            if (FValue != null && !FValue.equals("")) {
                AccdentDesc = FValue.trim();
            } else {
                AccdentDesc = null;
            }
        }
        if (FCode.equalsIgnoreCase("BatchNo")) {
            if (FValue != null && !FValue.equals("")) {
                BatchNo = FValue.trim();
            } else {
                BatchNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("BatchDate")) {
            if (FValue != null && !FValue.equals("")) {
                BatchDate = fDate.getDate(FValue);
            } else {
                BatchDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LLCaseReportSchema other = (LLCaseReportSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && RgtType.equals(other.getRgtType())
                && RptorName.equals(other.getRptorName())
                && RptorSex.equals(other.getRptorSex())
                && Relation.equals(other.getRelation())
                && RptorAddress.equals(other.getRptorAddress())
                && RptorPhone.equals(other.getRptorPhone())
                && RptorMobile.equals(other.getRptorMobile())
                && Email.equals(other.getEmail())
                && PostCode.equals(other.getPostCode())
                && RptorIDType.equals(other.getRptorIDType())
                && RptorIDNo.equals(other.getRptorIDNo())
                && fDate.getString(RptDate).equals(other.getRptDate())
                && CustomerNo.equals(other.getCustomerNo())
                && CustomerName.equals(other.getCustomerName())
                && CustomerSex.equals(other.getCustomerSex())
                && CustomerAge == other.getCustomerAge()
                && CustomerIDType.equals(other.getCustomerIDType())
                && CustomerIDNo.equals(other.getCustomerIDNo())
                && fDate.getString(AccidentDate).equals(other.getAccidentDate())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && AccdentDesc.equals(other.getAccdentDesc())
                && BatchNo.equals(other.getBatchNo())
                && fDate.getString(BatchDate).equals(other.getBatchDate())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return 0;
        }
        if (strFieldName.equals("RgtType")) {
            return 1;
        }
        if (strFieldName.equals("RptorName")) {
            return 2;
        }
        if (strFieldName.equals("RptorSex")) {
            return 3;
        }
        if (strFieldName.equals("Relation")) {
            return 4;
        }
        if (strFieldName.equals("RptorAddress")) {
            return 5;
        }
        if (strFieldName.equals("RptorPhone")) {
            return 6;
        }
        if (strFieldName.equals("RptorMobile")) {
            return 7;
        }
        if (strFieldName.equals("Email")) {
            return 8;
        }
        if (strFieldName.equals("PostCode")) {
            return 9;
        }
        if (strFieldName.equals("RptorIDType")) {
            return 10;
        }
        if (strFieldName.equals("RptorIDNo")) {
            return 11;
        }
        if (strFieldName.equals("RptDate")) {
            return 12;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 13;
        }
        if (strFieldName.equals("CustomerName")) {
            return 14;
        }
        if (strFieldName.equals("CustomerSex")) {
            return 15;
        }
        if (strFieldName.equals("CustomerAge")) {
            return 16;
        }
        if (strFieldName.equals("CustomerIDType")) {
            return 17;
        }
        if (strFieldName.equals("CustomerIDNo")) {
            return 18;
        }
        if (strFieldName.equals("AccidentDate")) {
            return 19;
        }
        if (strFieldName.equals("MngCom")) {
            return 20;
        }
        if (strFieldName.equals("Operator")) {
            return 21;
        }
        if (strFieldName.equals("AccdentDesc")) {
            return 22;
        }
        if (strFieldName.equals("BatchNo")) {
            return 23;
        }
        if (strFieldName.equals("BatchDate")) {
            return 24;
        }
        if (strFieldName.equals("MakeDate")) {
            return 25;
        }
        if (strFieldName.equals("MakeTime")) {
            return 26;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SerialNo";
            break;
        case 1:
            strFieldName = "RgtType";
            break;
        case 2:
            strFieldName = "RptorName";
            break;
        case 3:
            strFieldName = "RptorSex";
            break;
        case 4:
            strFieldName = "Relation";
            break;
        case 5:
            strFieldName = "RptorAddress";
            break;
        case 6:
            strFieldName = "RptorPhone";
            break;
        case 7:
            strFieldName = "RptorMobile";
            break;
        case 8:
            strFieldName = "Email";
            break;
        case 9:
            strFieldName = "PostCode";
            break;
        case 10:
            strFieldName = "RptorIDType";
            break;
        case 11:
            strFieldName = "RptorIDNo";
            break;
        case 12:
            strFieldName = "RptDate";
            break;
        case 13:
            strFieldName = "CustomerNo";
            break;
        case 14:
            strFieldName = "CustomerName";
            break;
        case 15:
            strFieldName = "CustomerSex";
            break;
        case 16:
            strFieldName = "CustomerAge";
            break;
        case 17:
            strFieldName = "CustomerIDType";
            break;
        case 18:
            strFieldName = "CustomerIDNo";
            break;
        case 19:
            strFieldName = "AccidentDate";
            break;
        case 20:
            strFieldName = "MngCom";
            break;
        case 21:
            strFieldName = "Operator";
            break;
        case 22:
            strFieldName = "AccdentDesc";
            break;
        case 23:
            strFieldName = "BatchNo";
            break;
        case 24:
            strFieldName = "BatchDate";
            break;
        case 25:
            strFieldName = "MakeDate";
            break;
        case 26:
            strFieldName = "MakeTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorSex")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Relation")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorAddress")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorPhone")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorMobile")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Email")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PostCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorIDType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorIDNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerSex")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerAge")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("CustomerIDType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerIDNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccidentDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MngCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccdentDesc")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BatchNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BatchDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_INT;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 21:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 22:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 23:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 24:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 25:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 26:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
