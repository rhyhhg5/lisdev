/*
 * <p>ClassName: LFTZXmlSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LFTZXmlDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFTZXmlSchema implements Schema
{
    // @Field
    /** 内部机构编码 */
    private String ComCode;
    /** 内部科目编码 */
    private String ItemCode;
    /** 类型 */
    private String RepType;
    /** 统计年 */
    private int StatYear;
    /** 统计月 */
    private int StatMon;
    /** 上级内部科目编码 */
    private String UpItemCode;
    /** 上级机构编码 */
    private String ParentComCodeISC;
    /** 层级 */
    private int Layer;
    /** 统计值 */
    private double StatValue;
    /** 备注 */
    private String Remark;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFTZXmlSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "ComCode";
        pk[1] = "ItemCode";
        pk[2] = "RepType";
        pk[3] = "StatYear";
        pk[4] = "StatMon";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getComCode()
    {
        if (ComCode != null && !ComCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ComCode = StrTool.unicodeToGBK(ComCode);
        }
        return ComCode;
    }

    public void setComCode(String aComCode)
    {
        ComCode = aComCode;
    }

    public String getItemCode()
    {
        if (ItemCode != null && !ItemCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ItemCode = StrTool.unicodeToGBK(ItemCode);
        }
        return ItemCode;
    }

    public void setItemCode(String aItemCode)
    {
        ItemCode = aItemCode;
    }

    public String getRepType()
    {
        if (RepType != null && !RepType.equals("") && SysConst.CHANGECHARSET == true)
        {
            RepType = StrTool.unicodeToGBK(RepType);
        }
        return RepType;
    }

    public void setRepType(String aRepType)
    {
        RepType = aRepType;
    }

    public int getStatYear()
    {
        return StatYear;
    }

    public void setStatYear(int aStatYear)
    {
        StatYear = aStatYear;
    }

    public void setStatYear(String aStatYear)
    {
        if (aStatYear != null && !aStatYear.equals(""))
        {
            Integer tInteger = new Integer(aStatYear);
            int i = tInteger.intValue();
            StatYear = i;
        }
    }

    public int getStatMon()
    {
        return StatMon;
    }

    public void setStatMon(int aStatMon)
    {
        StatMon = aStatMon;
    }

    public void setStatMon(String aStatMon)
    {
        if (aStatMon != null && !aStatMon.equals(""))
        {
            Integer tInteger = new Integer(aStatMon);
            int i = tInteger.intValue();
            StatMon = i;
        }
    }

    public String getUpItemCode()
    {
        if (UpItemCode != null && !UpItemCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UpItemCode = StrTool.unicodeToGBK(UpItemCode);
        }
        return UpItemCode;
    }

    public void setUpItemCode(String aUpItemCode)
    {
        UpItemCode = aUpItemCode;
    }

    public String getParentComCodeISC()
    {
        if (ParentComCodeISC != null && !ParentComCodeISC.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ParentComCodeISC = StrTool.unicodeToGBK(ParentComCodeISC);
        }
        return ParentComCodeISC;
    }

    public void setParentComCodeISC(String aParentComCodeISC)
    {
        ParentComCodeISC = aParentComCodeISC;
    }

    public int getLayer()
    {
        return Layer;
    }

    public void setLayer(int aLayer)
    {
        Layer = aLayer;
    }

    public void setLayer(String aLayer)
    {
        if (aLayer != null && !aLayer.equals(""))
        {
            Integer tInteger = new Integer(aLayer);
            int i = tInteger.intValue();
            Layer = i;
        }
    }

    public double getStatValue()
    {
        return StatValue;
    }

    public void setStatValue(double aStatValue)
    {
        StatValue = aStatValue;
    }

    public void setStatValue(String aStatValue)
    {
        if (aStatValue != null && !aStatValue.equals(""))
        {
            Double tDouble = new Double(aStatValue);
            double d = tDouble.doubleValue();
            StatValue = d;
        }
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    /**
     * 使用另外一个 LFTZXmlSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFTZXmlSchema aLFTZXmlSchema)
    {
        this.ComCode = aLFTZXmlSchema.getComCode();
        this.ItemCode = aLFTZXmlSchema.getItemCode();
        this.RepType = aLFTZXmlSchema.getRepType();
        this.StatYear = aLFTZXmlSchema.getStatYear();
        this.StatMon = aLFTZXmlSchema.getStatMon();
        this.UpItemCode = aLFTZXmlSchema.getUpItemCode();
        this.ParentComCodeISC = aLFTZXmlSchema.getParentComCodeISC();
        this.Layer = aLFTZXmlSchema.getLayer();
        this.StatValue = aLFTZXmlSchema.getStatValue();
        this.Remark = aLFTZXmlSchema.getRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ComCode") == null)
            {
                this.ComCode = null;
            }
            else
            {
                this.ComCode = rs.getString("ComCode").trim();
            }

            if (rs.getString("ItemCode") == null)
            {
                this.ItemCode = null;
            }
            else
            {
                this.ItemCode = rs.getString("ItemCode").trim();
            }

            if (rs.getString("RepType") == null)
            {
                this.RepType = null;
            }
            else
            {
                this.RepType = rs.getString("RepType").trim();
            }

            this.StatYear = rs.getInt("StatYear");
            this.StatMon = rs.getInt("StatMon");
            if (rs.getString("UpItemCode") == null)
            {
                this.UpItemCode = null;
            }
            else
            {
                this.UpItemCode = rs.getString("UpItemCode").trim();
            }

            if (rs.getString("ParentComCodeISC") == null)
            {
                this.ParentComCodeISC = null;
            }
            else
            {
                this.ParentComCodeISC = rs.getString("ParentComCodeISC").trim();
            }

            this.Layer = rs.getInt("Layer");
            this.StatValue = rs.getDouble("StatValue");
            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFTZXmlSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFTZXmlSchema getSchema()
    {
        LFTZXmlSchema aLFTZXmlSchema = new LFTZXmlSchema();
        aLFTZXmlSchema.setSchema(this);
        return aLFTZXmlSchema;
    }

    public LFTZXmlDB getDB()
    {
        LFTZXmlDB aDBOper = new LFTZXmlDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFTZXml描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ComCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ItemCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RepType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(StatYear) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StatMon) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UpItemCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ParentComCodeISC)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Layer) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StatValue) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFTZXml>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            RepType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            StatYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            StatMon = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            UpItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            ParentComCodeISC = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              7, SysConst.PACKAGESPILTER);
            Layer = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    8, SysConst.PACKAGESPILTER))).intValue();
            StatValue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFTZXmlSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ComCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComCode));
        }
        if (FCode.equals("ItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ItemCode));
        }
        if (FCode.equals("RepType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RepType));
        }
        if (FCode.equals("StatYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StatYear));
        }
        if (FCode.equals("StatMon"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StatMon));
        }
        if (FCode.equals("UpItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UpItemCode));
        }
        if (FCode.equals("ParentComCodeISC"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ParentComCodeISC));
        }
        if (FCode.equals("Layer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Layer));
        }
        if (FCode.equals("StatValue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StatValue));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ItemCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RepType);
                break;
            case 3:
                strFieldValue = String.valueOf(StatYear);
                break;
            case 4:
                strFieldValue = String.valueOf(StatMon);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(UpItemCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ParentComCodeISC);
                break;
            case 7:
                strFieldValue = String.valueOf(Layer);
                break;
            case 8:
                strFieldValue = String.valueOf(StatValue);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ComCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
            {
                ComCode = null;
            }
        }
        if (FCode.equals("ItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemCode = FValue.trim();
            }
            else
            {
                ItemCode = null;
            }
        }
        if (FCode.equals("RepType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RepType = FValue.trim();
            }
            else
            {
                RepType = null;
            }
        }
        if (FCode.equals("StatYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                StatYear = i;
            }
        }
        if (FCode.equals("StatMon"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                StatMon = i;
            }
        }
        if (FCode.equals("UpItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UpItemCode = FValue.trim();
            }
            else
            {
                UpItemCode = null;
            }
        }
        if (FCode.equals("ParentComCodeISC"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ParentComCodeISC = FValue.trim();
            }
            else
            {
                ParentComCodeISC = null;
            }
        }
        if (FCode.equals("Layer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Layer = i;
            }
        }
        if (FCode.equals("StatValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StatValue = d;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFTZXmlSchema other = (LFTZXmlSchema) otherObject;
        return
                ComCode.equals(other.getComCode())
                && ItemCode.equals(other.getItemCode())
                && RepType.equals(other.getRepType())
                && StatYear == other.getStatYear()
                && StatMon == other.getStatMon()
                && UpItemCode.equals(other.getUpItemCode())
                && ParentComCodeISC.equals(other.getParentComCodeISC())
                && Layer == other.getLayer()
                && StatValue == other.getStatValue()
                && Remark.equals(other.getRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ComCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ItemCode"))
        {
            return 1;
        }
        if (strFieldName.equals("RepType"))
        {
            return 2;
        }
        if (strFieldName.equals("StatYear"))
        {
            return 3;
        }
        if (strFieldName.equals("StatMon"))
        {
            return 4;
        }
        if (strFieldName.equals("UpItemCode"))
        {
            return 5;
        }
        if (strFieldName.equals("ParentComCodeISC"))
        {
            return 6;
        }
        if (strFieldName.equals("Layer"))
        {
            return 7;
        }
        if (strFieldName.equals("StatValue"))
        {
            return 8;
        }
        if (strFieldName.equals("Remark"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ComCode";
                break;
            case 1:
                strFieldName = "ItemCode";
                break;
            case 2:
                strFieldName = "RepType";
                break;
            case 3:
                strFieldName = "StatYear";
                break;
            case 4:
                strFieldName = "StatMon";
                break;
            case 5:
                strFieldName = "UpItemCode";
                break;
            case 6:
                strFieldName = "ParentComCodeISC";
                break;
            case 7:
                strFieldName = "Layer";
                break;
            case 8:
                strFieldName = "StatValue";
                break;
            case 9:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ComCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RepType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StatYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("StatMon"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("UpItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParentComCodeISC"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Layer"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("StatValue"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_INT;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
