/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIAboriginalGenAttDB;

/*
 * <p>ClassName: FIAboriginalGenAttSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIAboriginalGenAttSchema implements Schema, Cloneable
{
	// @Field
	/** 业务数据流水号 */
	private String ASerialNo;
	/** 明细物理表 */
	private String SubPhysicalTable;
	/** 业务索引类型 */
	private String IndexCode;
	/** 业务索引号码 */
	private String IndexNo;
	/** 凭证转换标记 */
	private String TrandFlag;
	/** 业务大类 */
	private String BusinessCode;
	/** 业务类别 */
	private String SubBusinessCode;
	/** 业务明细 */
	private String BusDetail;
	/** 费用类型 */
	private String FeeType;
	/** 费用明细 */
	private String FeeDetail;
	/** 费用金额 */
	private double SumActuMoney;
	/** 个团标记 */
	private String ListFlag;
	/** 保单号 */
	private String ContNo;
	/** 暂收类型标记 */
	private String TempFeeFlag;
	/** 邮储标志 */
	private String YCFlag;
	/** 收付费方式 */
	private String PayMode;
	/** 广东卡单标记 */
	private String GDCardFlag;
	/** 续期催收标记 */
	private String XQDealState;
	/** 赠送保费标志 */
	private String ZengSongFlag;
	/** 共保类型 */
	private String GBType;
	/** 首续年标志 */
	private String FirstYearFlag;
	/** 首续期标志 */
	private String FirstTermFlag;
	/** 是否异地 */
	private String DifComFlag;
	/** 对方机构 */
	private String ExecuteCom;
	/** 保险年期 */
	private int Years;
	/** 险种分类 */
	private String RiskType;
	/** 险种分类1 */
	private String RiskType1;
	/** 险种分类2 */
	private String RiskType2;
	/** 险种明细分类 */
	private String SubRiskType;
	/** 长短险标志 */
	private String RiskPeriod;
	/** 主附险标记 */
	private String RiskMainFlag;
	/** 渠道分类 */
	private String SaleChnlType;
	/** 保费收入类型 */
	private String PremiumType;
	/** 交费年期 */
	private int PolYear;
	/** 计费首年 */
	private String FirstYear;
	/** 出纳类型 */
	private String OperationType;
	/** 预算 */
	private String Budget;
	/** 客户编号 */
	private String CustomerID;
	/** 市场类型 */
	private String MarketType;
	/** 现金流量 */
	private String CashFlow;
	/** 记账日期 */
	private Date FIDate;
	/** 币别 */
	private String Currency;

	public static final int FIELDNUM = 42;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIAboriginalGenAttSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ASerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIAboriginalGenAttSchema cloned = (FIAboriginalGenAttSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getASerialNo()
	{
		return ASerialNo;
	}
	public void setASerialNo(String aASerialNo)
	{
		ASerialNo = aASerialNo;
	}
	public String getSubPhysicalTable()
	{
		return SubPhysicalTable;
	}
	public void setSubPhysicalTable(String aSubPhysicalTable)
	{
		SubPhysicalTable = aSubPhysicalTable;
	}
	public String getIndexCode()
	{
		return IndexCode;
	}
	public void setIndexCode(String aIndexCode)
	{
		IndexCode = aIndexCode;
	}
	public String getIndexNo()
	{
		return IndexNo;
	}
	public void setIndexNo(String aIndexNo)
	{
		IndexNo = aIndexNo;
	}
	public String getTrandFlag()
	{
		return TrandFlag;
	}
	public void setTrandFlag(String aTrandFlag)
	{
		TrandFlag = aTrandFlag;
	}
	public String getBusinessCode()
	{
		return BusinessCode;
	}
	public void setBusinessCode(String aBusinessCode)
	{
		BusinessCode = aBusinessCode;
	}
	public String getSubBusinessCode()
	{
		return SubBusinessCode;
	}
	public void setSubBusinessCode(String aSubBusinessCode)
	{
		SubBusinessCode = aSubBusinessCode;
	}
	public String getBusDetail()
	{
		return BusDetail;
	}
	public void setBusDetail(String aBusDetail)
	{
		BusDetail = aBusDetail;
	}
	public String getFeeType()
	{
		return FeeType;
	}
	public void setFeeType(String aFeeType)
	{
		FeeType = aFeeType;
	}
	public String getFeeDetail()
	{
		return FeeDetail;
	}
	public void setFeeDetail(String aFeeDetail)
	{
		FeeDetail = aFeeDetail;
	}
	public double getSumActuMoney()
	{
		return SumActuMoney;
	}
	public void setSumActuMoney(double aSumActuMoney)
	{
		SumActuMoney = Arith.round(aSumActuMoney,2);
	}
	public void setSumActuMoney(String aSumActuMoney)
	{
		if (aSumActuMoney != null && !aSumActuMoney.equals(""))
		{
			Double tDouble = new Double(aSumActuMoney);
			double d = tDouble.doubleValue();
                SumActuMoney = Arith.round(d,2);
		}
	}

	public String getListFlag()
	{
		return ListFlag;
	}
	public void setListFlag(String aListFlag)
	{
		ListFlag = aListFlag;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getTempFeeFlag()
	{
		return TempFeeFlag;
	}
	public void setTempFeeFlag(String aTempFeeFlag)
	{
		TempFeeFlag = aTempFeeFlag;
	}
	public String getYCFlag()
	{
		return YCFlag;
	}
	public void setYCFlag(String aYCFlag)
	{
		YCFlag = aYCFlag;
	}
	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getGDCardFlag()
	{
		return GDCardFlag;
	}
	public void setGDCardFlag(String aGDCardFlag)
	{
		GDCardFlag = aGDCardFlag;
	}
	public String getXQDealState()
	{
		return XQDealState;
	}
	public void setXQDealState(String aXQDealState)
	{
		XQDealState = aXQDealState;
	}
	public String getZengSongFlag()
	{
		return ZengSongFlag;
	}
	public void setZengSongFlag(String aZengSongFlag)
	{
		ZengSongFlag = aZengSongFlag;
	}
	public String getGBType()
	{
		return GBType;
	}
	public void setGBType(String aGBType)
	{
		GBType = aGBType;
	}
	public String getFirstYearFlag()
	{
		return FirstYearFlag;
	}
	public void setFirstYearFlag(String aFirstYearFlag)
	{
		FirstYearFlag = aFirstYearFlag;
	}
	public String getFirstTermFlag()
	{
		return FirstTermFlag;
	}
	public void setFirstTermFlag(String aFirstTermFlag)
	{
		FirstTermFlag = aFirstTermFlag;
	}
	public String getDifComFlag()
	{
		return DifComFlag;
	}
	public void setDifComFlag(String aDifComFlag)
	{
		DifComFlag = aDifComFlag;
	}
	public String getExecuteCom()
	{
		return ExecuteCom;
	}
	public void setExecuteCom(String aExecuteCom)
	{
		ExecuteCom = aExecuteCom;
	}
	public int getYears()
	{
		return Years;
	}
	public void setYears(int aYears)
	{
		Years = aYears;
	}
	public void setYears(String aYears)
	{
		if (aYears != null && !aYears.equals(""))
		{
			Integer tInteger = new Integer(aYears);
			int i = tInteger.intValue();
			Years = i;
		}
	}

	public String getRiskType()
	{
		return RiskType;
	}
	public void setRiskType(String aRiskType)
	{
		RiskType = aRiskType;
	}
	public String getRiskType1()
	{
		return RiskType1;
	}
	public void setRiskType1(String aRiskType1)
	{
		RiskType1 = aRiskType1;
	}
	public String getRiskType2()
	{
		return RiskType2;
	}
	public void setRiskType2(String aRiskType2)
	{
		RiskType2 = aRiskType2;
	}
	public String getSubRiskType()
	{
		return SubRiskType;
	}
	public void setSubRiskType(String aSubRiskType)
	{
		SubRiskType = aSubRiskType;
	}
	public String getRiskPeriod()
	{
		return RiskPeriod;
	}
	public void setRiskPeriod(String aRiskPeriod)
	{
		RiskPeriod = aRiskPeriod;
	}
	public String getRiskMainFlag()
	{
		return RiskMainFlag;
	}
	public void setRiskMainFlag(String aRiskMainFlag)
	{
		RiskMainFlag = aRiskMainFlag;
	}
	public String getSaleChnlType()
	{
		return SaleChnlType;
	}
	public void setSaleChnlType(String aSaleChnlType)
	{
		SaleChnlType = aSaleChnlType;
	}
	public String getPremiumType()
	{
		return PremiumType;
	}
	public void setPremiumType(String aPremiumType)
	{
		PremiumType = aPremiumType;
	}
	public int getPolYear()
	{
		return PolYear;
	}
	public void setPolYear(int aPolYear)
	{
		PolYear = aPolYear;
	}
	public void setPolYear(String aPolYear)
	{
		if (aPolYear != null && !aPolYear.equals(""))
		{
			Integer tInteger = new Integer(aPolYear);
			int i = tInteger.intValue();
			PolYear = i;
		}
	}

	public String getFirstYear()
	{
		return FirstYear;
	}
	public void setFirstYear(String aFirstYear)
	{
		FirstYear = aFirstYear;
	}
	public String getOperationType()
	{
		return OperationType;
	}
	public void setOperationType(String aOperationType)
	{
		OperationType = aOperationType;
	}
	public String getBudget()
	{
		return Budget;
	}
	public void setBudget(String aBudget)
	{
		Budget = aBudget;
	}
	public String getCustomerID()
	{
		return CustomerID;
	}
	public void setCustomerID(String aCustomerID)
	{
		CustomerID = aCustomerID;
	}
	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getCashFlow()
	{
		return CashFlow;
	}
	public void setCashFlow(String aCashFlow)
	{
		CashFlow = aCashFlow;
	}
	public String getFIDate()
	{
		if( FIDate != null )
			return fDate.getString(FIDate);
		else
			return null;
	}
	public void setFIDate(Date aFIDate)
	{
		FIDate = aFIDate;
	}
	public void setFIDate(String aFIDate)
	{
		if (aFIDate != null && !aFIDate.equals("") )
		{
			FIDate = fDate.getDate( aFIDate );
		}
		else
			FIDate = null;
	}

	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}

	/**
	* 使用另外一个 FIAboriginalGenAttSchema 对象给 Schema 赋值
	* @param: aFIAboriginalGenAttSchema FIAboriginalGenAttSchema
	**/
	public void setSchema(FIAboriginalGenAttSchema aFIAboriginalGenAttSchema)
	{
		this.ASerialNo = aFIAboriginalGenAttSchema.getASerialNo();
		this.SubPhysicalTable = aFIAboriginalGenAttSchema.getSubPhysicalTable();
		this.IndexCode = aFIAboriginalGenAttSchema.getIndexCode();
		this.IndexNo = aFIAboriginalGenAttSchema.getIndexNo();
		this.TrandFlag = aFIAboriginalGenAttSchema.getTrandFlag();
		this.BusinessCode = aFIAboriginalGenAttSchema.getBusinessCode();
		this.SubBusinessCode = aFIAboriginalGenAttSchema.getSubBusinessCode();
		this.BusDetail = aFIAboriginalGenAttSchema.getBusDetail();
		this.FeeType = aFIAboriginalGenAttSchema.getFeeType();
		this.FeeDetail = aFIAboriginalGenAttSchema.getFeeDetail();
		this.SumActuMoney = aFIAboriginalGenAttSchema.getSumActuMoney();
		this.ListFlag = aFIAboriginalGenAttSchema.getListFlag();
		this.ContNo = aFIAboriginalGenAttSchema.getContNo();
		this.TempFeeFlag = aFIAboriginalGenAttSchema.getTempFeeFlag();
		this.YCFlag = aFIAboriginalGenAttSchema.getYCFlag();
		this.PayMode = aFIAboriginalGenAttSchema.getPayMode();
		this.GDCardFlag = aFIAboriginalGenAttSchema.getGDCardFlag();
		this.XQDealState = aFIAboriginalGenAttSchema.getXQDealState();
		this.ZengSongFlag = aFIAboriginalGenAttSchema.getZengSongFlag();
		this.GBType = aFIAboriginalGenAttSchema.getGBType();
		this.FirstYearFlag = aFIAboriginalGenAttSchema.getFirstYearFlag();
		this.FirstTermFlag = aFIAboriginalGenAttSchema.getFirstTermFlag();
		this.DifComFlag = aFIAboriginalGenAttSchema.getDifComFlag();
		this.ExecuteCom = aFIAboriginalGenAttSchema.getExecuteCom();
		this.Years = aFIAboriginalGenAttSchema.getYears();
		this.RiskType = aFIAboriginalGenAttSchema.getRiskType();
		this.RiskType1 = aFIAboriginalGenAttSchema.getRiskType1();
		this.RiskType2 = aFIAboriginalGenAttSchema.getRiskType2();
		this.SubRiskType = aFIAboriginalGenAttSchema.getSubRiskType();
		this.RiskPeriod = aFIAboriginalGenAttSchema.getRiskPeriod();
		this.RiskMainFlag = aFIAboriginalGenAttSchema.getRiskMainFlag();
		this.SaleChnlType = aFIAboriginalGenAttSchema.getSaleChnlType();
		this.PremiumType = aFIAboriginalGenAttSchema.getPremiumType();
		this.PolYear = aFIAboriginalGenAttSchema.getPolYear();
		this.FirstYear = aFIAboriginalGenAttSchema.getFirstYear();
		this.OperationType = aFIAboriginalGenAttSchema.getOperationType();
		this.Budget = aFIAboriginalGenAttSchema.getBudget();
		this.CustomerID = aFIAboriginalGenAttSchema.getCustomerID();
		this.MarketType = aFIAboriginalGenAttSchema.getMarketType();
		this.CashFlow = aFIAboriginalGenAttSchema.getCashFlow();
		this.FIDate = fDate.getDate( aFIAboriginalGenAttSchema.getFIDate());
		this.Currency = aFIAboriginalGenAttSchema.getCurrency();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ASerialNo") == null )
				this.ASerialNo = null;
			else
				this.ASerialNo = rs.getString("ASerialNo").trim();

			if( rs.getString("SubPhysicalTable") == null )
				this.SubPhysicalTable = null;
			else
				this.SubPhysicalTable = rs.getString("SubPhysicalTable").trim();

			if( rs.getString("IndexCode") == null )
				this.IndexCode = null;
			else
				this.IndexCode = rs.getString("IndexCode").trim();

			if( rs.getString("IndexNo") == null )
				this.IndexNo = null;
			else
				this.IndexNo = rs.getString("IndexNo").trim();

			if( rs.getString("TrandFlag") == null )
				this.TrandFlag = null;
			else
				this.TrandFlag = rs.getString("TrandFlag").trim();

			if( rs.getString("BusinessCode") == null )
				this.BusinessCode = null;
			else
				this.BusinessCode = rs.getString("BusinessCode").trim();

			if( rs.getString("SubBusinessCode") == null )
				this.SubBusinessCode = null;
			else
				this.SubBusinessCode = rs.getString("SubBusinessCode").trim();

			if( rs.getString("BusDetail") == null )
				this.BusDetail = null;
			else
				this.BusDetail = rs.getString("BusDetail").trim();

			if( rs.getString("FeeType") == null )
				this.FeeType = null;
			else
				this.FeeType = rs.getString("FeeType").trim();

			if( rs.getString("FeeDetail") == null )
				this.FeeDetail = null;
			else
				this.FeeDetail = rs.getString("FeeDetail").trim();

			this.SumActuMoney = rs.getDouble("SumActuMoney");
			if( rs.getString("ListFlag") == null )
				this.ListFlag = null;
			else
				this.ListFlag = rs.getString("ListFlag").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("TempFeeFlag") == null )
				this.TempFeeFlag = null;
			else
				this.TempFeeFlag = rs.getString("TempFeeFlag").trim();

			if( rs.getString("YCFlag") == null )
				this.YCFlag = null;
			else
				this.YCFlag = rs.getString("YCFlag").trim();

			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("GDCardFlag") == null )
				this.GDCardFlag = null;
			else
				this.GDCardFlag = rs.getString("GDCardFlag").trim();

			if( rs.getString("XQDealState") == null )
				this.XQDealState = null;
			else
				this.XQDealState = rs.getString("XQDealState").trim();

			if( rs.getString("ZengSongFlag") == null )
				this.ZengSongFlag = null;
			else
				this.ZengSongFlag = rs.getString("ZengSongFlag").trim();

			if( rs.getString("GBType") == null )
				this.GBType = null;
			else
				this.GBType = rs.getString("GBType").trim();

			if( rs.getString("FirstYearFlag") == null )
				this.FirstYearFlag = null;
			else
				this.FirstYearFlag = rs.getString("FirstYearFlag").trim();

			if( rs.getString("FirstTermFlag") == null )
				this.FirstTermFlag = null;
			else
				this.FirstTermFlag = rs.getString("FirstTermFlag").trim();

			if( rs.getString("DifComFlag") == null )
				this.DifComFlag = null;
			else
				this.DifComFlag = rs.getString("DifComFlag").trim();

			if( rs.getString("ExecuteCom") == null )
				this.ExecuteCom = null;
			else
				this.ExecuteCom = rs.getString("ExecuteCom").trim();

			this.Years = rs.getInt("Years");
			if( rs.getString("RiskType") == null )
				this.RiskType = null;
			else
				this.RiskType = rs.getString("RiskType").trim();

			if( rs.getString("RiskType1") == null )
				this.RiskType1 = null;
			else
				this.RiskType1 = rs.getString("RiskType1").trim();

			if( rs.getString("RiskType2") == null )
				this.RiskType2 = null;
			else
				this.RiskType2 = rs.getString("RiskType2").trim();

			if( rs.getString("SubRiskType") == null )
				this.SubRiskType = null;
			else
				this.SubRiskType = rs.getString("SubRiskType").trim();

			if( rs.getString("RiskPeriod") == null )
				this.RiskPeriod = null;
			else
				this.RiskPeriod = rs.getString("RiskPeriod").trim();

			if( rs.getString("RiskMainFlag") == null )
				this.RiskMainFlag = null;
			else
				this.RiskMainFlag = rs.getString("RiskMainFlag").trim();

			if( rs.getString("SaleChnlType") == null )
				this.SaleChnlType = null;
			else
				this.SaleChnlType = rs.getString("SaleChnlType").trim();

			if( rs.getString("PremiumType") == null )
				this.PremiumType = null;
			else
				this.PremiumType = rs.getString("PremiumType").trim();

			this.PolYear = rs.getInt("PolYear");
			if( rs.getString("FirstYear") == null )
				this.FirstYear = null;
			else
				this.FirstYear = rs.getString("FirstYear").trim();

			if( rs.getString("OperationType") == null )
				this.OperationType = null;
			else
				this.OperationType = rs.getString("OperationType").trim();

			if( rs.getString("Budget") == null )
				this.Budget = null;
			else
				this.Budget = rs.getString("Budget").trim();

			if( rs.getString("CustomerID") == null )
				this.CustomerID = null;
			else
				this.CustomerID = rs.getString("CustomerID").trim();

			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("CashFlow") == null )
				this.CashFlow = null;
			else
				this.CashFlow = rs.getString("CashFlow").trim();

			this.FIDate = rs.getDate("FIDate");
			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIAboriginalGenAtt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIAboriginalGenAttSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIAboriginalGenAttSchema getSchema()
	{
		FIAboriginalGenAttSchema aFIAboriginalGenAttSchema = new FIAboriginalGenAttSchema();
		aFIAboriginalGenAttSchema.setSchema(this);
		return aFIAboriginalGenAttSchema;
	}

	public FIAboriginalGenAttDB getDB()
	{
		FIAboriginalGenAttDB aDBOper = new FIAboriginalGenAttDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIAboriginalGenAtt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ASerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubPhysicalTable)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrandFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubBusinessCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumActuMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ListFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempFeeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(YCFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GDCardFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(XQDealState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZengSongFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GBType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstTermFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DifComFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExecuteCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubRiskType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskPeriod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskMainFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnlType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremiumType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OperationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Budget)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CashFlow)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FIDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIAboriginalGenAtt>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ASerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SubPhysicalTable = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			IndexNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			TrandFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BusinessCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			SubBusinessCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BusDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			FeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			FeeDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			SumActuMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			ListFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			TempFeeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			YCFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			GDCardFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			XQDealState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ZengSongFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			GBType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			FirstYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			FirstTermFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			DifComFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ExecuteCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Years= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).intValue();
			RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			RiskType1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			RiskType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			SubRiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			RiskPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			RiskMainFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			SaleChnlType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			PremiumType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			PolYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).intValue();
			FirstYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			OperationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			Budget = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			CustomerID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			CashFlow = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			FIDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIAboriginalGenAttSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ASerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ASerialNo));
		}
		if (FCode.equals("SubPhysicalTable"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubPhysicalTable));
		}
		if (FCode.equals("IndexCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
		}
		if (FCode.equals("IndexNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexNo));
		}
		if (FCode.equals("TrandFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrandFlag));
		}
		if (FCode.equals("BusinessCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessCode));
		}
		if (FCode.equals("SubBusinessCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubBusinessCode));
		}
		if (FCode.equals("BusDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusDetail));
		}
		if (FCode.equals("FeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
		}
		if (FCode.equals("FeeDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeDetail));
		}
		if (FCode.equals("SumActuMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumActuMoney));
		}
		if (FCode.equals("ListFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ListFlag));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("TempFeeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeFlag));
		}
		if (FCode.equals("YCFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YCFlag));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("GDCardFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GDCardFlag));
		}
		if (FCode.equals("XQDealState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(XQDealState));
		}
		if (FCode.equals("ZengSongFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZengSongFlag));
		}
		if (FCode.equals("GBType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GBType));
		}
		if (FCode.equals("FirstYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstYearFlag));
		}
		if (FCode.equals("FirstTermFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTermFlag));
		}
		if (FCode.equals("DifComFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DifComFlag));
		}
		if (FCode.equals("ExecuteCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
		}
		if (FCode.equals("Years"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
		}
		if (FCode.equals("RiskType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
		}
		if (FCode.equals("RiskType1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType1));
		}
		if (FCode.equals("RiskType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType2));
		}
		if (FCode.equals("SubRiskType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubRiskType));
		}
		if (FCode.equals("RiskPeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskPeriod));
		}
		if (FCode.equals("RiskMainFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskMainFlag));
		}
		if (FCode.equals("SaleChnlType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnlType));
		}
		if (FCode.equals("PremiumType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremiumType));
		}
		if (FCode.equals("PolYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolYear));
		}
		if (FCode.equals("FirstYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstYear));
		}
		if (FCode.equals("OperationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OperationType));
		}
		if (FCode.equals("Budget"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Budget));
		}
		if (FCode.equals("CustomerID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerID));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("CashFlow"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CashFlow));
		}
		if (FCode.equals("FIDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFIDate()));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ASerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SubPhysicalTable);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(IndexCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(IndexNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(TrandFlag);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BusinessCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(SubBusinessCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BusDetail);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(FeeType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(FeeDetail);
				break;
			case 10:
				strFieldValue = String.valueOf(SumActuMoney);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ListFlag);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(TempFeeFlag);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(YCFlag);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(GDCardFlag);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(XQDealState);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ZengSongFlag);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(GBType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(FirstYearFlag);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(FirstTermFlag);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(DifComFlag);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ExecuteCom);
				break;
			case 24:
				strFieldValue = String.valueOf(Years);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(RiskType);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(RiskType1);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(RiskType2);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(SubRiskType);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(RiskPeriod);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(RiskMainFlag);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(SaleChnlType);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(PremiumType);
				break;
			case 33:
				strFieldValue = String.valueOf(PolYear);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(FirstYear);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(OperationType);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(Budget);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(CustomerID);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(CashFlow);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFIDate()));
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ASerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ASerialNo = FValue.trim();
			}
			else
				ASerialNo = null;
		}
		if (FCode.equalsIgnoreCase("SubPhysicalTable"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubPhysicalTable = FValue.trim();
			}
			else
				SubPhysicalTable = null;
		}
		if (FCode.equalsIgnoreCase("IndexCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCode = FValue.trim();
			}
			else
				IndexCode = null;
		}
		if (FCode.equalsIgnoreCase("IndexNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexNo = FValue.trim();
			}
			else
				IndexNo = null;
		}
		if (FCode.equalsIgnoreCase("TrandFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrandFlag = FValue.trim();
			}
			else
				TrandFlag = null;
		}
		if (FCode.equalsIgnoreCase("BusinessCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessCode = FValue.trim();
			}
			else
				BusinessCode = null;
		}
		if (FCode.equalsIgnoreCase("SubBusinessCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubBusinessCode = FValue.trim();
			}
			else
				SubBusinessCode = null;
		}
		if (FCode.equalsIgnoreCase("BusDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusDetail = FValue.trim();
			}
			else
				BusDetail = null;
		}
		if (FCode.equalsIgnoreCase("FeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeType = FValue.trim();
			}
			else
				FeeType = null;
		}
		if (FCode.equalsIgnoreCase("FeeDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeDetail = FValue.trim();
			}
			else
				FeeDetail = null;
		}
		if (FCode.equalsIgnoreCase("SumActuMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumActuMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("ListFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ListFlag = FValue.trim();
			}
			else
				ListFlag = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("TempFeeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempFeeFlag = FValue.trim();
			}
			else
				TempFeeFlag = null;
		}
		if (FCode.equalsIgnoreCase("YCFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				YCFlag = FValue.trim();
			}
			else
				YCFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("GDCardFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GDCardFlag = FValue.trim();
			}
			else
				GDCardFlag = null;
		}
		if (FCode.equalsIgnoreCase("XQDealState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				XQDealState = FValue.trim();
			}
			else
				XQDealState = null;
		}
		if (FCode.equalsIgnoreCase("ZengSongFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZengSongFlag = FValue.trim();
			}
			else
				ZengSongFlag = null;
		}
		if (FCode.equalsIgnoreCase("GBType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GBType = FValue.trim();
			}
			else
				GBType = null;
		}
		if (FCode.equalsIgnoreCase("FirstYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstYearFlag = FValue.trim();
			}
			else
				FirstYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("FirstTermFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTermFlag = FValue.trim();
			}
			else
				FirstTermFlag = null;
		}
		if (FCode.equalsIgnoreCase("DifComFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DifComFlag = FValue.trim();
			}
			else
				DifComFlag = null;
		}
		if (FCode.equalsIgnoreCase("ExecuteCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExecuteCom = FValue.trim();
			}
			else
				ExecuteCom = null;
		}
		if (FCode.equalsIgnoreCase("Years"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Years = i;
			}
		}
		if (FCode.equalsIgnoreCase("RiskType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType = FValue.trim();
			}
			else
				RiskType = null;
		}
		if (FCode.equalsIgnoreCase("RiskType1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType1 = FValue.trim();
			}
			else
				RiskType1 = null;
		}
		if (FCode.equalsIgnoreCase("RiskType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType2 = FValue.trim();
			}
			else
				RiskType2 = null;
		}
		if (FCode.equalsIgnoreCase("SubRiskType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubRiskType = FValue.trim();
			}
			else
				SubRiskType = null;
		}
		if (FCode.equalsIgnoreCase("RiskPeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskPeriod = FValue.trim();
			}
			else
				RiskPeriod = null;
		}
		if (FCode.equalsIgnoreCase("RiskMainFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskMainFlag = FValue.trim();
			}
			else
				RiskMainFlag = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnlType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnlType = FValue.trim();
			}
			else
				SaleChnlType = null;
		}
		if (FCode.equalsIgnoreCase("PremiumType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremiumType = FValue.trim();
			}
			else
				PremiumType = null;
		}
		if (FCode.equalsIgnoreCase("PolYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("FirstYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstYear = FValue.trim();
			}
			else
				FirstYear = null;
		}
		if (FCode.equalsIgnoreCase("OperationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OperationType = FValue.trim();
			}
			else
				OperationType = null;
		}
		if (FCode.equalsIgnoreCase("Budget"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Budget = FValue.trim();
			}
			else
				Budget = null;
		}
		if (FCode.equalsIgnoreCase("CustomerID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerID = FValue.trim();
			}
			else
				CustomerID = null;
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("CashFlow"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CashFlow = FValue.trim();
			}
			else
				CashFlow = null;
		}
		if (FCode.equalsIgnoreCase("FIDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FIDate = fDate.getDate( FValue );
			}
			else
				FIDate = null;
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIAboriginalGenAttSchema other = (FIAboriginalGenAttSchema)otherObject;
		return
			(ASerialNo == null ? other.getASerialNo() == null : ASerialNo.equals(other.getASerialNo()))
			&& (SubPhysicalTable == null ? other.getSubPhysicalTable() == null : SubPhysicalTable.equals(other.getSubPhysicalTable()))
			&& (IndexCode == null ? other.getIndexCode() == null : IndexCode.equals(other.getIndexCode()))
			&& (IndexNo == null ? other.getIndexNo() == null : IndexNo.equals(other.getIndexNo()))
			&& (TrandFlag == null ? other.getTrandFlag() == null : TrandFlag.equals(other.getTrandFlag()))
			&& (BusinessCode == null ? other.getBusinessCode() == null : BusinessCode.equals(other.getBusinessCode()))
			&& (SubBusinessCode == null ? other.getSubBusinessCode() == null : SubBusinessCode.equals(other.getSubBusinessCode()))
			&& (BusDetail == null ? other.getBusDetail() == null : BusDetail.equals(other.getBusDetail()))
			&& (FeeType == null ? other.getFeeType() == null : FeeType.equals(other.getFeeType()))
			&& (FeeDetail == null ? other.getFeeDetail() == null : FeeDetail.equals(other.getFeeDetail()))
			&& SumActuMoney == other.getSumActuMoney()
			&& (ListFlag == null ? other.getListFlag() == null : ListFlag.equals(other.getListFlag()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (TempFeeFlag == null ? other.getTempFeeFlag() == null : TempFeeFlag.equals(other.getTempFeeFlag()))
			&& (YCFlag == null ? other.getYCFlag() == null : YCFlag.equals(other.getYCFlag()))
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (GDCardFlag == null ? other.getGDCardFlag() == null : GDCardFlag.equals(other.getGDCardFlag()))
			&& (XQDealState == null ? other.getXQDealState() == null : XQDealState.equals(other.getXQDealState()))
			&& (ZengSongFlag == null ? other.getZengSongFlag() == null : ZengSongFlag.equals(other.getZengSongFlag()))
			&& (GBType == null ? other.getGBType() == null : GBType.equals(other.getGBType()))
			&& (FirstYearFlag == null ? other.getFirstYearFlag() == null : FirstYearFlag.equals(other.getFirstYearFlag()))
			&& (FirstTermFlag == null ? other.getFirstTermFlag() == null : FirstTermFlag.equals(other.getFirstTermFlag()))
			&& (DifComFlag == null ? other.getDifComFlag() == null : DifComFlag.equals(other.getDifComFlag()))
			&& (ExecuteCom == null ? other.getExecuteCom() == null : ExecuteCom.equals(other.getExecuteCom()))
			&& Years == other.getYears()
			&& (RiskType == null ? other.getRiskType() == null : RiskType.equals(other.getRiskType()))
			&& (RiskType1 == null ? other.getRiskType1() == null : RiskType1.equals(other.getRiskType1()))
			&& (RiskType2 == null ? other.getRiskType2() == null : RiskType2.equals(other.getRiskType2()))
			&& (SubRiskType == null ? other.getSubRiskType() == null : SubRiskType.equals(other.getSubRiskType()))
			&& (RiskPeriod == null ? other.getRiskPeriod() == null : RiskPeriod.equals(other.getRiskPeriod()))
			&& (RiskMainFlag == null ? other.getRiskMainFlag() == null : RiskMainFlag.equals(other.getRiskMainFlag()))
			&& (SaleChnlType == null ? other.getSaleChnlType() == null : SaleChnlType.equals(other.getSaleChnlType()))
			&& (PremiumType == null ? other.getPremiumType() == null : PremiumType.equals(other.getPremiumType()))
			&& PolYear == other.getPolYear()
			&& (FirstYear == null ? other.getFirstYear() == null : FirstYear.equals(other.getFirstYear()))
			&& (OperationType == null ? other.getOperationType() == null : OperationType.equals(other.getOperationType()))
			&& (Budget == null ? other.getBudget() == null : Budget.equals(other.getBudget()))
			&& (CustomerID == null ? other.getCustomerID() == null : CustomerID.equals(other.getCustomerID()))
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (CashFlow == null ? other.getCashFlow() == null : CashFlow.equals(other.getCashFlow()))
			&& (FIDate == null ? other.getFIDate() == null : fDate.getString(FIDate).equals(other.getFIDate()))
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ASerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("SubPhysicalTable") ) {
			return 1;
		}
		if( strFieldName.equals("IndexCode") ) {
			return 2;
		}
		if( strFieldName.equals("IndexNo") ) {
			return 3;
		}
		if( strFieldName.equals("TrandFlag") ) {
			return 4;
		}
		if( strFieldName.equals("BusinessCode") ) {
			return 5;
		}
		if( strFieldName.equals("SubBusinessCode") ) {
			return 6;
		}
		if( strFieldName.equals("BusDetail") ) {
			return 7;
		}
		if( strFieldName.equals("FeeType") ) {
			return 8;
		}
		if( strFieldName.equals("FeeDetail") ) {
			return 9;
		}
		if( strFieldName.equals("SumActuMoney") ) {
			return 10;
		}
		if( strFieldName.equals("ListFlag") ) {
			return 11;
		}
		if( strFieldName.equals("ContNo") ) {
			return 12;
		}
		if( strFieldName.equals("TempFeeFlag") ) {
			return 13;
		}
		if( strFieldName.equals("YCFlag") ) {
			return 14;
		}
		if( strFieldName.equals("PayMode") ) {
			return 15;
		}
		if( strFieldName.equals("GDCardFlag") ) {
			return 16;
		}
		if( strFieldName.equals("XQDealState") ) {
			return 17;
		}
		if( strFieldName.equals("ZengSongFlag") ) {
			return 18;
		}
		if( strFieldName.equals("GBType") ) {
			return 19;
		}
		if( strFieldName.equals("FirstYearFlag") ) {
			return 20;
		}
		if( strFieldName.equals("FirstTermFlag") ) {
			return 21;
		}
		if( strFieldName.equals("DifComFlag") ) {
			return 22;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return 23;
		}
		if( strFieldName.equals("Years") ) {
			return 24;
		}
		if( strFieldName.equals("RiskType") ) {
			return 25;
		}
		if( strFieldName.equals("RiskType1") ) {
			return 26;
		}
		if( strFieldName.equals("RiskType2") ) {
			return 27;
		}
		if( strFieldName.equals("SubRiskType") ) {
			return 28;
		}
		if( strFieldName.equals("RiskPeriod") ) {
			return 29;
		}
		if( strFieldName.equals("RiskMainFlag") ) {
			return 30;
		}
		if( strFieldName.equals("SaleChnlType") ) {
			return 31;
		}
		if( strFieldName.equals("PremiumType") ) {
			return 32;
		}
		if( strFieldName.equals("PolYear") ) {
			return 33;
		}
		if( strFieldName.equals("FirstYear") ) {
			return 34;
		}
		if( strFieldName.equals("OperationType") ) {
			return 35;
		}
		if( strFieldName.equals("Budget") ) {
			return 36;
		}
		if( strFieldName.equals("CustomerID") ) {
			return 37;
		}
		if( strFieldName.equals("MarketType") ) {
			return 38;
		}
		if( strFieldName.equals("CashFlow") ) {
			return 39;
		}
		if( strFieldName.equals("FIDate") ) {
			return 40;
		}
		if( strFieldName.equals("Currency") ) {
			return 41;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ASerialNo";
				break;
			case 1:
				strFieldName = "SubPhysicalTable";
				break;
			case 2:
				strFieldName = "IndexCode";
				break;
			case 3:
				strFieldName = "IndexNo";
				break;
			case 4:
				strFieldName = "TrandFlag";
				break;
			case 5:
				strFieldName = "BusinessCode";
				break;
			case 6:
				strFieldName = "SubBusinessCode";
				break;
			case 7:
				strFieldName = "BusDetail";
				break;
			case 8:
				strFieldName = "FeeType";
				break;
			case 9:
				strFieldName = "FeeDetail";
				break;
			case 10:
				strFieldName = "SumActuMoney";
				break;
			case 11:
				strFieldName = "ListFlag";
				break;
			case 12:
				strFieldName = "ContNo";
				break;
			case 13:
				strFieldName = "TempFeeFlag";
				break;
			case 14:
				strFieldName = "YCFlag";
				break;
			case 15:
				strFieldName = "PayMode";
				break;
			case 16:
				strFieldName = "GDCardFlag";
				break;
			case 17:
				strFieldName = "XQDealState";
				break;
			case 18:
				strFieldName = "ZengSongFlag";
				break;
			case 19:
				strFieldName = "GBType";
				break;
			case 20:
				strFieldName = "FirstYearFlag";
				break;
			case 21:
				strFieldName = "FirstTermFlag";
				break;
			case 22:
				strFieldName = "DifComFlag";
				break;
			case 23:
				strFieldName = "ExecuteCom";
				break;
			case 24:
				strFieldName = "Years";
				break;
			case 25:
				strFieldName = "RiskType";
				break;
			case 26:
				strFieldName = "RiskType1";
				break;
			case 27:
				strFieldName = "RiskType2";
				break;
			case 28:
				strFieldName = "SubRiskType";
				break;
			case 29:
				strFieldName = "RiskPeriod";
				break;
			case 30:
				strFieldName = "RiskMainFlag";
				break;
			case 31:
				strFieldName = "SaleChnlType";
				break;
			case 32:
				strFieldName = "PremiumType";
				break;
			case 33:
				strFieldName = "PolYear";
				break;
			case 34:
				strFieldName = "FirstYear";
				break;
			case 35:
				strFieldName = "OperationType";
				break;
			case 36:
				strFieldName = "Budget";
				break;
			case 37:
				strFieldName = "CustomerID";
				break;
			case 38:
				strFieldName = "MarketType";
				break;
			case 39:
				strFieldName = "CashFlow";
				break;
			case 40:
				strFieldName = "FIDate";
				break;
			case 41:
				strFieldName = "Currency";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ASerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubPhysicalTable") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrandFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubBusinessCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumActuMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ListFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempFeeFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("YCFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GDCardFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("XQDealState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZengSongFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GBType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstTermFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DifComFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Years") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RiskType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubRiskType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskPeriod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskMainFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnlType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremiumType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("FirstYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OperationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Budget") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CashFlow") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FIDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_INT;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_INT;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
