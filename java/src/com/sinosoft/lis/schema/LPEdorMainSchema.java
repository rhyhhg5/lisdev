/*
 * <p>ClassName: LPEdorMainSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 保全主表时间字段类型
 * @CreateDate：2005-01-13
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LPEdorMainDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LPEdorMainSchema implements Schema
{
    // @Field
    /** 保全受理号 */
    private String EdorAcceptNo;
    /** 批单号 */
    private String EdorNo;
    /** 合同号码 */
    private String ContNo;
    /** 批改申请号 */
    private String EdorAppNo;
    /** 申请人名称 */
    private String EdorAppName;
    /** 管理机构 */
    private String ManageCom;
    /** 变动的保费 */
    private double ChgPrem;
    /** 变动的保额 */
    private double ChgAmnt;
    /** 补/退费金额 */
    private double GetMoney;
    /** 补/退费利息 */
    private double GetInterest;
    /** 批改申请日期 */
    private Date EdorAppDate;
    /** 批改生效日期 */
    private Date EdorValiDate;
    /** 批改状态 */
    private String EdorState;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 银行帐户名 */
    private String AccName;
    /** 通讯地址 */
    private String PostalAddress;
    /** 通讯邮编 */
    private String ZipCode;
    /** 通讯电话 */
    private String Phone;
    /** 打印标志 */
    private String PrintFlag;
    /** 核保级别 */
    private String UWGrade;
    /** 申请级别 */
    private String AppGrade;
    /** 核保标志 */
    private String UWState;
    /** 核保人 */
    private String UWOperator;
    /** 核保日期 */
    private Date UWDate;
    /** 核保时间 */
    private String UWTime;
    /** 确认人 */
    private String ConfOperator;
    /** 确认日期 */
    private Date ConfDate;
    /** 确认时间 */
    private String ConfTime;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 34; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPEdorMainSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "EdorAcceptNo";
        pk[1] = "EdorNo";
        pk[2] = "ContNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorAcceptNo()
    {
        if (EdorAcceptNo != null && !EdorAcceptNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EdorAcceptNo = StrTool.unicodeToGBK(EdorAcceptNo);
        }
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String aEdorAcceptNo)
    {
        EdorAcceptNo = aEdorAcceptNo;
    }

    public String getEdorNo()
    {
        if (EdorNo != null && !EdorNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getEdorAppNo()
    {
        if (EdorAppNo != null && !EdorAppNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EdorAppNo = StrTool.unicodeToGBK(EdorAppNo);
        }
        return EdorAppNo;
    }

    public void setEdorAppNo(String aEdorAppNo)
    {
        EdorAppNo = aEdorAppNo;
    }

    public String getEdorAppName()
    {
        if (EdorAppName != null && !EdorAppName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EdorAppName = StrTool.unicodeToGBK(EdorAppName);
        }
        return EdorAppName;
    }

    public void setEdorAppName(String aEdorAppName)
    {
        EdorAppName = aEdorAppName;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public double getChgPrem()
    {
        return ChgPrem;
    }

    public void setChgPrem(double aChgPrem)
    {
        ChgPrem = aChgPrem;
    }

    public void setChgPrem(String aChgPrem)
    {
        if (aChgPrem != null && !aChgPrem.equals(""))
        {
            Double tDouble = new Double(aChgPrem);
            double d = tDouble.doubleValue();
            ChgPrem = d;
        }
    }

    public double getChgAmnt()
    {
        return ChgAmnt;
    }

    public void setChgAmnt(double aChgAmnt)
    {
        ChgAmnt = aChgAmnt;
    }

    public void setChgAmnt(String aChgAmnt)
    {
        if (aChgAmnt != null && !aChgAmnt.equals(""))
        {
            Double tDouble = new Double(aChgAmnt);
            double d = tDouble.doubleValue();
            ChgAmnt = d;
        }
    }

    public double getGetMoney()
    {
        return GetMoney;
    }

    public void setGetMoney(double aGetMoney)
    {
        GetMoney = aGetMoney;
    }

    public void setGetMoney(String aGetMoney)
    {
        if (aGetMoney != null && !aGetMoney.equals(""))
        {
            Double tDouble = new Double(aGetMoney);
            double d = tDouble.doubleValue();
            GetMoney = d;
        }
    }

    public double getGetInterest()
    {
        return GetInterest;
    }

    public void setGetInterest(double aGetInterest)
    {
        GetInterest = aGetInterest;
    }

    public void setGetInterest(String aGetInterest)
    {
        if (aGetInterest != null && !aGetInterest.equals(""))
        {
            Double tDouble = new Double(aGetInterest);
            double d = tDouble.doubleValue();
            GetInterest = d;
        }
    }

    public String getEdorAppDate()
    {
        if (EdorAppDate != null)
        {
            return fDate.getString(EdorAppDate);
        }
        else
        {
            return null;
        }
    }

    public void setEdorAppDate(Date aEdorAppDate)
    {
        EdorAppDate = aEdorAppDate;
    }

    public void setEdorAppDate(String aEdorAppDate)
    {
        if (aEdorAppDate != null && !aEdorAppDate.equals(""))
        {
            EdorAppDate = fDate.getDate(aEdorAppDate);
        }
        else
        {
            EdorAppDate = null;
        }
    }

    public String getEdorValiDate()
    {
        if (EdorValiDate != null)
        {
            return fDate.getString(EdorValiDate);
        }
        else
        {
            return null;
        }
    }

    public void setEdorValiDate(Date aEdorValiDate)
    {
        EdorValiDate = aEdorValiDate;
    }

    public void setEdorValiDate(String aEdorValiDate)
    {
        if (aEdorValiDate != null && !aEdorValiDate.equals(""))
        {
            EdorValiDate = fDate.getDate(aEdorValiDate);
        }
        else
        {
            EdorValiDate = null;
        }
    }

    public String getEdorState()
    {
        if (EdorState != null && !EdorState.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EdorState = StrTool.unicodeToGBK(EdorState);
        }
        return EdorState;
    }

    public void setEdorState(String aEdorState)
    {
        EdorState = aEdorState;
    }

    public String getBankCode()
    {
        if (BankCode != null && !BankCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            BankCode = StrTool.unicodeToGBK(BankCode);
        }
        return BankCode;
    }

    public void setBankCode(String aBankCode)
    {
        BankCode = aBankCode;
    }

    public String getBankAccNo()
    {
        if (BankAccNo != null && !BankAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BankAccNo = StrTool.unicodeToGBK(BankAccNo);
        }
        return BankAccNo;
    }

    public void setBankAccNo(String aBankAccNo)
    {
        BankAccNo = aBankAccNo;
    }

    public String getAccName()
    {
        if (AccName != null && !AccName.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccName = StrTool.unicodeToGBK(AccName);
        }
        return AccName;
    }

    public void setAccName(String aAccName)
    {
        AccName = aAccName;
    }

    public String getPostalAddress()
    {
        if (PostalAddress != null && !PostalAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PostalAddress = StrTool.unicodeToGBK(PostalAddress);
        }
        return PostalAddress;
    }

    public void setPostalAddress(String aPostalAddress)
    {
        PostalAddress = aPostalAddress;
    }

    public String getZipCode()
    {
        if (ZipCode != null && !ZipCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ZipCode = StrTool.unicodeToGBK(ZipCode);
        }
        return ZipCode;
    }

    public void setZipCode(String aZipCode)
    {
        ZipCode = aZipCode;
    }

    public String getPhone()
    {
        if (Phone != null && !Phone.equals("") && SysConst.CHANGECHARSET == true)
        {
            Phone = StrTool.unicodeToGBK(Phone);
        }
        return Phone;
    }

    public void setPhone(String aPhone)
    {
        Phone = aPhone;
    }

    public String getPrintFlag()
    {
        if (PrintFlag != null && !PrintFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PrintFlag = StrTool.unicodeToGBK(PrintFlag);
        }
        return PrintFlag;
    }

    public void setPrintFlag(String aPrintFlag)
    {
        PrintFlag = aPrintFlag;
    }

    public String getUWGrade()
    {
        if (UWGrade != null && !UWGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWGrade = StrTool.unicodeToGBK(UWGrade);
        }
        return UWGrade;
    }

    public void setUWGrade(String aUWGrade)
    {
        UWGrade = aUWGrade;
    }

    public String getAppGrade()
    {
        if (AppGrade != null && !AppGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppGrade = StrTool.unicodeToGBK(AppGrade);
        }
        return AppGrade;
    }

    public void setAppGrade(String aAppGrade)
    {
        AppGrade = aAppGrade;
    }

    public String getUWState()
    {
        if (UWState != null && !UWState.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWState = StrTool.unicodeToGBK(UWState);
        }
        return UWState;
    }

    public void setUWState(String aUWState)
    {
        UWState = aUWState;
    }

    public String getUWOperator()
    {
        if (UWOperator != null && !UWOperator.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UWOperator = StrTool.unicodeToGBK(UWOperator);
        }
        return UWOperator;
    }

    public void setUWOperator(String aUWOperator)
    {
        UWOperator = aUWOperator;
    }

    public String getUWDate()
    {
        if (UWDate != null)
        {
            return fDate.getString(UWDate);
        }
        else
        {
            return null;
        }
    }

    public void setUWDate(Date aUWDate)
    {
        UWDate = aUWDate;
    }

    public void setUWDate(String aUWDate)
    {
        if (aUWDate != null && !aUWDate.equals(""))
        {
            UWDate = fDate.getDate(aUWDate);
        }
        else
        {
            UWDate = null;
        }
    }

    public String getUWTime()
    {
        if (UWTime != null && !UWTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWTime = StrTool.unicodeToGBK(UWTime);
        }
        return UWTime;
    }

    public void setUWTime(String aUWTime)
    {
        UWTime = aUWTime;
    }

    public String getConfOperator()
    {
        if (ConfOperator != null && !ConfOperator.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ConfOperator = StrTool.unicodeToGBK(ConfOperator);
        }
        return ConfOperator;
    }

    public void setConfOperator(String aConfOperator)
    {
        ConfOperator = aConfOperator;
    }

    public String getConfDate()
    {
        if (ConfDate != null)
        {
            return fDate.getString(ConfDate);
        }
        else
        {
            return null;
        }
    }

    public void setConfDate(Date aConfDate)
    {
        ConfDate = aConfDate;
    }

    public void setConfDate(String aConfDate)
    {
        if (aConfDate != null && !aConfDate.equals(""))
        {
            ConfDate = fDate.getDate(aConfDate);
        }
        else
        {
            ConfDate = null;
        }
    }

    public String getConfTime()
    {
        if (ConfTime != null && !ConfTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            ConfTime = StrTool.unicodeToGBK(ConfTime);
        }
        return ConfTime;
    }

    public void setConfTime(String aConfTime)
    {
        ConfTime = aConfTime;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LPEdorMainSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LPEdorMainSchema aLPEdorMainSchema)
    {
        this.EdorAcceptNo = aLPEdorMainSchema.getEdorAcceptNo();
        this.EdorNo = aLPEdorMainSchema.getEdorNo();
        this.ContNo = aLPEdorMainSchema.getContNo();
        this.EdorAppNo = aLPEdorMainSchema.getEdorAppNo();
        this.EdorAppName = aLPEdorMainSchema.getEdorAppName();
        this.ManageCom = aLPEdorMainSchema.getManageCom();
        this.ChgPrem = aLPEdorMainSchema.getChgPrem();
        this.ChgAmnt = aLPEdorMainSchema.getChgAmnt();
        this.GetMoney = aLPEdorMainSchema.getGetMoney();
        this.GetInterest = aLPEdorMainSchema.getGetInterest();
        this.EdorAppDate = fDate.getDate(aLPEdorMainSchema.getEdorAppDate());
        this.EdorValiDate = fDate.getDate(aLPEdorMainSchema.getEdorValiDate());
        this.EdorState = aLPEdorMainSchema.getEdorState();
        this.BankCode = aLPEdorMainSchema.getBankCode();
        this.BankAccNo = aLPEdorMainSchema.getBankAccNo();
        this.AccName = aLPEdorMainSchema.getAccName();
        this.PostalAddress = aLPEdorMainSchema.getPostalAddress();
        this.ZipCode = aLPEdorMainSchema.getZipCode();
        this.Phone = aLPEdorMainSchema.getPhone();
        this.PrintFlag = aLPEdorMainSchema.getPrintFlag();
        this.UWGrade = aLPEdorMainSchema.getUWGrade();
        this.AppGrade = aLPEdorMainSchema.getAppGrade();
        this.UWState = aLPEdorMainSchema.getUWState();
        this.UWOperator = aLPEdorMainSchema.getUWOperator();
        this.UWDate = fDate.getDate(aLPEdorMainSchema.getUWDate());
        this.UWTime = aLPEdorMainSchema.getUWTime();
        this.ConfOperator = aLPEdorMainSchema.getConfOperator();
        this.ConfDate = fDate.getDate(aLPEdorMainSchema.getConfDate());
        this.ConfTime = aLPEdorMainSchema.getConfTime();
        this.Operator = aLPEdorMainSchema.getOperator();
        this.MakeDate = fDate.getDate(aLPEdorMainSchema.getMakeDate());
        this.MakeTime = aLPEdorMainSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPEdorMainSchema.getModifyDate());
        this.ModifyTime = aLPEdorMainSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorAcceptNo") == null)
            {
                this.EdorAcceptNo = null;
            }
            else
            {
                this.EdorAcceptNo = rs.getString("EdorAcceptNo").trim();
            }

            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("EdorAppNo") == null)
            {
                this.EdorAppNo = null;
            }
            else
            {
                this.EdorAppNo = rs.getString("EdorAppNo").trim();
            }

            if (rs.getString("EdorAppName") == null)
            {
                this.EdorAppName = null;
            }
            else
            {
                this.EdorAppName = rs.getString("EdorAppName").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.ChgPrem = rs.getDouble("ChgPrem");
            this.ChgAmnt = rs.getDouble("ChgAmnt");
            this.GetMoney = rs.getDouble("GetMoney");
            this.GetInterest = rs.getDouble("GetInterest");
            this.EdorAppDate = rs.getDate("EdorAppDate");
            this.EdorValiDate = rs.getDate("EdorValiDate");
            if (rs.getString("EdorState") == null)
            {
                this.EdorState = null;
            }
            else
            {
                this.EdorState = rs.getString("EdorState").trim();
            }

            if (rs.getString("BankCode") == null)
            {
                this.BankCode = null;
            }
            else
            {
                this.BankCode = rs.getString("BankCode").trim();
            }

            if (rs.getString("BankAccNo") == null)
            {
                this.BankAccNo = null;
            }
            else
            {
                this.BankAccNo = rs.getString("BankAccNo").trim();
            }

            if (rs.getString("AccName") == null)
            {
                this.AccName = null;
            }
            else
            {
                this.AccName = rs.getString("AccName").trim();
            }

            if (rs.getString("PostalAddress") == null)
            {
                this.PostalAddress = null;
            }
            else
            {
                this.PostalAddress = rs.getString("PostalAddress").trim();
            }

            if (rs.getString("ZipCode") == null)
            {
                this.ZipCode = null;
            }
            else
            {
                this.ZipCode = rs.getString("ZipCode").trim();
            }

            if (rs.getString("Phone") == null)
            {
                this.Phone = null;
            }
            else
            {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("PrintFlag") == null)
            {
                this.PrintFlag = null;
            }
            else
            {
                this.PrintFlag = rs.getString("PrintFlag").trim();
            }

            if (rs.getString("UWGrade") == null)
            {
                this.UWGrade = null;
            }
            else
            {
                this.UWGrade = rs.getString("UWGrade").trim();
            }

            if (rs.getString("AppGrade") == null)
            {
                this.AppGrade = null;
            }
            else
            {
                this.AppGrade = rs.getString("AppGrade").trim();
            }

            if (rs.getString("UWState") == null)
            {
                this.UWState = null;
            }
            else
            {
                this.UWState = rs.getString("UWState").trim();
            }

            if (rs.getString("UWOperator") == null)
            {
                this.UWOperator = null;
            }
            else
            {
                this.UWOperator = rs.getString("UWOperator").trim();
            }

            this.UWDate = rs.getDate("UWDate");
            if (rs.getString("UWTime") == null)
            {
                this.UWTime = null;
            }
            else
            {
                this.UWTime = rs.getString("UWTime").trim();
            }

            if (rs.getString("ConfOperator") == null)
            {
                this.ConfOperator = null;
            }
            else
            {
                this.ConfOperator = rs.getString("ConfOperator").trim();
            }

            this.ConfDate = rs.getDate("ConfDate");
            if (rs.getString("ConfTime") == null)
            {
                this.ConfTime = null;
            }
            else
            {
                this.ConfTime = rs.getString("ConfTime").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPEdorMainSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPEdorMainSchema getSchema()
    {
        LPEdorMainSchema aLPEdorMainSchema = new LPEdorMainSchema();
        aLPEdorMainSchema.setSchema(this);
        return aLPEdorMainSchema;
    }

    public LPEdorMainDB getDB()
    {
        LPEdorMainDB aDBOper = new LPEdorMainDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPEdorMain描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(EdorAcceptNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorAppNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorAppName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ChgPrem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(ChgAmnt) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetInterest) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EdorAppDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EdorValiDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankAccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PostalAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ZipCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Phone)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrintFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWOperator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(UWDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ConfOperator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ConfDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ConfTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPEdorMain>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            EdorAppNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            EdorAppName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            ChgPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            ChgAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            GetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            GetInterest = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            EdorAppDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            EdorValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            EdorState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                       SysConst.PACKAGESPILTER);
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                       SysConst.PACKAGESPILTER);
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                     SysConst.PACKAGESPILTER);
            PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                           SysConst.PACKAGESPILTER);
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                   SysConst.PACKAGESPILTER);
            PrintFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                       SysConst.PACKAGESPILTER);
            UWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                     SysConst.PACKAGESPILTER);
            AppGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            UWState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                     SysConst.PACKAGESPILTER);
            UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                        SysConst.PACKAGESPILTER);
            UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                    SysConst.PACKAGESPILTER);
            ConfOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                          SysConst.PACKAGESPILTER);
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 28, SysConst.PACKAGESPILTER));
            ConfTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 31, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 33, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPEdorMainSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorAcceptNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorAcceptNo));
        }
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("EdorAppNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorAppNo));
        }
        if (FCode.equals("EdorAppName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorAppName));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("ChgPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChgPrem));
        }
        if (FCode.equals("ChgAmnt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChgAmnt));
        }
        if (FCode.equals("GetMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetMoney));
        }
        if (FCode.equals("GetInterest"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetInterest));
        }
        if (FCode.equals("EdorAppDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getEdorAppDate()));
        }
        if (FCode.equals("EdorValiDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getEdorValiDate()));
        }
        if (FCode.equals("EdorState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorState));
        }
        if (FCode.equals("BankCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankCode));
        }
        if (FCode.equals("BankAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankAccNo));
        }
        if (FCode.equals("AccName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccName));
        }
        if (FCode.equals("PostalAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PostalAddress));
        }
        if (FCode.equals("ZipCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ZipCode));
        }
        if (FCode.equals("Phone"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Phone));
        }
        if (FCode.equals("PrintFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrintFlag));
        }
        if (FCode.equals("UWGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWGrade));
        }
        if (FCode.equals("AppGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppGrade));
        }
        if (FCode.equals("UWState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWState));
        }
        if (FCode.equals("UWOperator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWOperator));
        }
        if (FCode.equals("UWDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getUWDate()));
        }
        if (FCode.equals("UWTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWTime));
        }
        if (FCode.equals("ConfOperator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ConfOperator));
        }
        if (FCode.equals("ConfDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getConfDate()));
        }
        if (FCode.equals("ConfTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ConfTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(EdorAppNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(EdorAppName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 6:
                strFieldValue = String.valueOf(ChgPrem);
                break;
            case 7:
                strFieldValue = String.valueOf(ChgAmnt);
                break;
            case 8:
                strFieldValue = String.valueOf(GetMoney);
                break;
            case 9:
                strFieldValue = String.valueOf(GetInterest);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEdorAppDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEdorValiDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(EdorState);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(PostalAddress);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(PrintFlag);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(UWGrade);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(AppGrade);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(UWState);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(UWOperator);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getUWDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(UWTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(ConfOperator);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfDate()));
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(ConfTime);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorAcceptNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAcceptNo = FValue.trim();
            }
            else
            {
                EdorAcceptNo = null;
            }
        }
        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("EdorAppNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAppNo = FValue.trim();
            }
            else
            {
                EdorAppNo = null;
            }
        }
        if (FCode.equals("EdorAppName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAppName = FValue.trim();
            }
            else
            {
                EdorAppName = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("ChgPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ChgPrem = d;
            }
        }
        if (FCode.equals("ChgAmnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ChgAmnt = d;
            }
        }
        if (FCode.equals("GetMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetMoney = d;
            }
        }
        if (FCode.equals("GetInterest"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetInterest = d;
            }
        }
        if (FCode.equals("EdorAppDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorAppDate = fDate.getDate(FValue);
            }
            else
            {
                EdorAppDate = null;
            }
        }
        if (FCode.equals("EdorValiDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorValiDate = fDate.getDate(FValue);
            }
            else
            {
                EdorValiDate = null;
            }
        }
        if (FCode.equals("EdorState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorState = FValue.trim();
            }
            else
            {
                EdorState = null;
            }
        }
        if (FCode.equals("BankCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
            {
                BankCode = null;
            }
        }
        if (FCode.equals("BankAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
            {
                BankAccNo = null;
            }
        }
        if (FCode.equals("AccName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
            {
                AccName = null;
            }
        }
        if (FCode.equals("PostalAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PostalAddress = FValue.trim();
            }
            else
            {
                PostalAddress = null;
            }
        }
        if (FCode.equals("ZipCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
            {
                ZipCode = null;
            }
        }
        if (FCode.equals("Phone"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
            {
                Phone = null;
            }
        }
        if (FCode.equals("PrintFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrintFlag = FValue.trim();
            }
            else
            {
                PrintFlag = null;
            }
        }
        if (FCode.equals("UWGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
            {
                UWGrade = null;
            }
        }
        if (FCode.equals("AppGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppGrade = FValue.trim();
            }
            else
            {
                AppGrade = null;
            }
        }
        if (FCode.equals("UWState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWState = FValue.trim();
            }
            else
            {
                UWState = null;
            }
        }
        if (FCode.equals("UWOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWOperator = FValue.trim();
            }
            else
            {
                UWOperator = null;
            }
        }
        if (FCode.equals("UWDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWDate = fDate.getDate(FValue);
            }
            else
            {
                UWDate = null;
            }
        }
        if (FCode.equals("UWTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWTime = FValue.trim();
            }
            else
            {
                UWTime = null;
            }
        }
        if (FCode.equals("ConfOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfOperator = FValue.trim();
            }
            else
            {
                ConfOperator = null;
            }
        }
        if (FCode.equals("ConfDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfDate = fDate.getDate(FValue);
            }
            else
            {
                ConfDate = null;
            }
        }
        if (FCode.equals("ConfTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfTime = FValue.trim();
            }
            else
            {
                ConfTime = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPEdorMainSchema other = (LPEdorMainSchema) otherObject;
        return
                EdorAcceptNo.equals(other.getEdorAcceptNo())
                && EdorNo.equals(other.getEdorNo())
                && ContNo.equals(other.getContNo())
                && EdorAppNo.equals(other.getEdorAppNo())
                && EdorAppName.equals(other.getEdorAppName())
                && ManageCom.equals(other.getManageCom())
                && ChgPrem == other.getChgPrem()
                && ChgAmnt == other.getChgAmnt()
                && GetMoney == other.getGetMoney()
                && GetInterest == other.getGetInterest()
                && fDate.getString(EdorAppDate).equals(other.getEdorAppDate())
                && fDate.getString(EdorValiDate).equals(other.getEdorValiDate())
                && EdorState.equals(other.getEdorState())
                && BankCode.equals(other.getBankCode())
                && BankAccNo.equals(other.getBankAccNo())
                && AccName.equals(other.getAccName())
                && PostalAddress.equals(other.getPostalAddress())
                && ZipCode.equals(other.getZipCode())
                && Phone.equals(other.getPhone())
                && PrintFlag.equals(other.getPrintFlag())
                && UWGrade.equals(other.getUWGrade())
                && AppGrade.equals(other.getAppGrade())
                && UWState.equals(other.getUWState())
                && UWOperator.equals(other.getUWOperator())
                && fDate.getString(UWDate).equals(other.getUWDate())
                && UWTime.equals(other.getUWTime())
                && ConfOperator.equals(other.getConfOperator())
                && fDate.getString(ConfDate).equals(other.getConfDate())
                && ConfTime.equals(other.getConfTime())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorAcceptNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 2;
        }
        if (strFieldName.equals("EdorAppNo"))
        {
            return 3;
        }
        if (strFieldName.equals("EdorAppName"))
        {
            return 4;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 5;
        }
        if (strFieldName.equals("ChgPrem"))
        {
            return 6;
        }
        if (strFieldName.equals("ChgAmnt"))
        {
            return 7;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return 8;
        }
        if (strFieldName.equals("GetInterest"))
        {
            return 9;
        }
        if (strFieldName.equals("EdorAppDate"))
        {
            return 10;
        }
        if (strFieldName.equals("EdorValiDate"))
        {
            return 11;
        }
        if (strFieldName.equals("EdorState"))
        {
            return 12;
        }
        if (strFieldName.equals("BankCode"))
        {
            return 13;
        }
        if (strFieldName.equals("BankAccNo"))
        {
            return 14;
        }
        if (strFieldName.equals("AccName"))
        {
            return 15;
        }
        if (strFieldName.equals("PostalAddress"))
        {
            return 16;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return 17;
        }
        if (strFieldName.equals("Phone"))
        {
            return 18;
        }
        if (strFieldName.equals("PrintFlag"))
        {
            return 19;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return 20;
        }
        if (strFieldName.equals("AppGrade"))
        {
            return 21;
        }
        if (strFieldName.equals("UWState"))
        {
            return 22;
        }
        if (strFieldName.equals("UWOperator"))
        {
            return 23;
        }
        if (strFieldName.equals("UWDate"))
        {
            return 24;
        }
        if (strFieldName.equals("UWTime"))
        {
            return 25;
        }
        if (strFieldName.equals("ConfOperator"))
        {
            return 26;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return 27;
        }
        if (strFieldName.equals("ConfTime"))
        {
            return 28;
        }
        if (strFieldName.equals("Operator"))
        {
            return 29;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 30;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 31;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 32;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 33;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorAcceptNo";
                break;
            case 1:
                strFieldName = "EdorNo";
                break;
            case 2:
                strFieldName = "ContNo";
                break;
            case 3:
                strFieldName = "EdorAppNo";
                break;
            case 4:
                strFieldName = "EdorAppName";
                break;
            case 5:
                strFieldName = "ManageCom";
                break;
            case 6:
                strFieldName = "ChgPrem";
                break;
            case 7:
                strFieldName = "ChgAmnt";
                break;
            case 8:
                strFieldName = "GetMoney";
                break;
            case 9:
                strFieldName = "GetInterest";
                break;
            case 10:
                strFieldName = "EdorAppDate";
                break;
            case 11:
                strFieldName = "EdorValiDate";
                break;
            case 12:
                strFieldName = "EdorState";
                break;
            case 13:
                strFieldName = "BankCode";
                break;
            case 14:
                strFieldName = "BankAccNo";
                break;
            case 15:
                strFieldName = "AccName";
                break;
            case 16:
                strFieldName = "PostalAddress";
                break;
            case 17:
                strFieldName = "ZipCode";
                break;
            case 18:
                strFieldName = "Phone";
                break;
            case 19:
                strFieldName = "PrintFlag";
                break;
            case 20:
                strFieldName = "UWGrade";
                break;
            case 21:
                strFieldName = "AppGrade";
                break;
            case 22:
                strFieldName = "UWState";
                break;
            case 23:
                strFieldName = "UWOperator";
                break;
            case 24:
                strFieldName = "UWDate";
                break;
            case 25:
                strFieldName = "UWTime";
                break;
            case 26:
                strFieldName = "ConfOperator";
                break;
            case 27:
                strFieldName = "ConfDate";
                break;
            case 28:
                strFieldName = "ConfTime";
                break;
            case 29:
                strFieldName = "Operator";
                break;
            case 30:
                strFieldName = "MakeDate";
                break;
            case 31:
                strFieldName = "MakeTime";
                break;
            case 32:
                strFieldName = "ModifyDate";
                break;
            case 33:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorAcceptNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorAppNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorAppName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChgPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ChgAmnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetInterest"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("EdorAppDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EdorValiDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EdorState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PostalAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrintFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("UWTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ConfTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 33:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
