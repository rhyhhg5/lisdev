/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLClaimErrorDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LLClaimErrorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-22
 */
public class LLClaimErrorSchema implements Schema
{
    // @Field
    /** 流水号 */
    private String SerialNo;
    /** 赔案号 */
    private String ClmNo;
    /** 受理事故号 */
    private String CaseRelaNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体保单号码 */
    private String GrpPolNo;
    /** 个单合同号码 */
    private String ContNo;
    /** 保单号码 */
    private String PolNo;
    /** 给付责任类型 */
    private String GetDutyKind;
    /** 被保人客户号码 */
    private String InsuredNo;
    /** 被保人名称 */
    private String InsuredName;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 投保人名称 */
    private String AppntName;
    /** 管理机构 */
    private String ManageCom;
    /** 核赔规则编码 */
    private String UWRuleCode;
    /** 核赔出错信息 */
    private String UWError;
    /** 当前值 */
    private String CurrValue;
    /** 核赔可通过标记（分保） */
    private String UWPassFlag;
    /** 核赔级别 */
    private String UWGrade;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLClaimErrorSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "SerialNo";
        pk[1] = "ClmNo";
        pk[2] = "CaseRelaNo";
        pk[3] = "PolNo";
        pk[4] = "GetDutyKind";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getClmNo()
    {
        if (ClmNo != null && !ClmNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmNo = StrTool.unicodeToGBK(ClmNo);
        }
        return ClmNo;
    }

    public void setClmNo(String aClmNo)
    {
        ClmNo = aClmNo;
    }

    public String getCaseRelaNo()
    {
        if (CaseRelaNo != null && !CaseRelaNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CaseRelaNo = StrTool.unicodeToGBK(CaseRelaNo);
        }
        return CaseRelaNo;
    }

    public void setCaseRelaNo(String aCaseRelaNo)
    {
        CaseRelaNo = aCaseRelaNo;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getGetDutyKind()
    {
        if (GetDutyKind != null && !GetDutyKind.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyKind = StrTool.unicodeToGBK(GetDutyKind);
        }
        return GetDutyKind;
    }

    public void setGetDutyKind(String aGetDutyKind)
    {
        GetDutyKind = aGetDutyKind;
    }

    public String getInsuredNo()
    {
        if (InsuredNo != null && !InsuredNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredNo = StrTool.unicodeToGBK(InsuredNo);
        }
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getInsuredName()
    {
        if (InsuredName != null && !InsuredName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredName = StrTool.unicodeToGBK(InsuredName);
        }
        return InsuredName;
    }

    public void setInsuredName(String aInsuredName)
    {
        InsuredName = aInsuredName;
    }

    public String getAppntNo()
    {
        if (AppntNo != null && !AppntNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public String getAppntName()
    {
        if (AppntName != null && !AppntName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppntName = StrTool.unicodeToGBK(AppntName);
        }
        return AppntName;
    }

    public void setAppntName(String aAppntName)
    {
        AppntName = aAppntName;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getUWRuleCode()
    {
        if (UWRuleCode != null && !UWRuleCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UWRuleCode = StrTool.unicodeToGBK(UWRuleCode);
        }
        return UWRuleCode;
    }

    public void setUWRuleCode(String aUWRuleCode)
    {
        UWRuleCode = aUWRuleCode;
    }

    public String getUWError()
    {
        if (UWError != null && !UWError.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWError = StrTool.unicodeToGBK(UWError);
        }
        return UWError;
    }

    public void setUWError(String aUWError)
    {
        UWError = aUWError;
    }

    public String getCurrValue()
    {
        if (CurrValue != null && !CurrValue.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CurrValue = StrTool.unicodeToGBK(CurrValue);
        }
        return CurrValue;
    }

    public void setCurrValue(String aCurrValue)
    {
        CurrValue = aCurrValue;
    }

    public String getUWPassFlag()
    {
        if (UWPassFlag != null && !UWPassFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UWPassFlag = StrTool.unicodeToGBK(UWPassFlag);
        }
        return UWPassFlag;
    }

    public void setUWPassFlag(String aUWPassFlag)
    {
        UWPassFlag = aUWPassFlag;
    }

    public String getUWGrade()
    {
        if (UWGrade != null && !UWGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWGrade = StrTool.unicodeToGBK(UWGrade);
        }
        return UWGrade;
    }

    public void setUWGrade(String aUWGrade)
    {
        UWGrade = aUWGrade;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLClaimErrorSchema 对象给 Schema 赋值
     * @param: aLLClaimErrorSchema LLClaimErrorSchema
     **/
    public void setSchema(LLClaimErrorSchema aLLClaimErrorSchema)
    {
        this.SerialNo = aLLClaimErrorSchema.getSerialNo();
        this.ClmNo = aLLClaimErrorSchema.getClmNo();
        this.CaseRelaNo = aLLClaimErrorSchema.getCaseRelaNo();
        this.GrpContNo = aLLClaimErrorSchema.getGrpContNo();
        this.GrpPolNo = aLLClaimErrorSchema.getGrpPolNo();
        this.ContNo = aLLClaimErrorSchema.getContNo();
        this.PolNo = aLLClaimErrorSchema.getPolNo();
        this.GetDutyKind = aLLClaimErrorSchema.getGetDutyKind();
        this.InsuredNo = aLLClaimErrorSchema.getInsuredNo();
        this.InsuredName = aLLClaimErrorSchema.getInsuredName();
        this.AppntNo = aLLClaimErrorSchema.getAppntNo();
        this.AppntName = aLLClaimErrorSchema.getAppntName();
        this.ManageCom = aLLClaimErrorSchema.getManageCom();
        this.UWRuleCode = aLLClaimErrorSchema.getUWRuleCode();
        this.UWError = aLLClaimErrorSchema.getUWError();
        this.CurrValue = aLLClaimErrorSchema.getCurrValue();
        this.UWPassFlag = aLLClaimErrorSchema.getUWPassFlag();
        this.UWGrade = aLLClaimErrorSchema.getUWGrade();
        this.MakeDate = fDate.getDate(aLLClaimErrorSchema.getMakeDate());
        this.MakeTime = aLLClaimErrorSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLClaimErrorSchema.getModifyDate());
        this.ModifyTime = aLLClaimErrorSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("ClmNo") == null)
            {
                this.ClmNo = null;
            }
            else
            {
                this.ClmNo = rs.getString("ClmNo").trim();
            }

            if (rs.getString("CaseRelaNo") == null)
            {
                this.CaseRelaNo = null;
            }
            else
            {
                this.CaseRelaNo = rs.getString("CaseRelaNo").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("GetDutyKind") == null)
            {
                this.GetDutyKind = null;
            }
            else
            {
                this.GetDutyKind = rs.getString("GetDutyKind").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("InsuredName") == null)
            {
                this.InsuredName = null;
            }
            else
            {
                this.InsuredName = rs.getString("InsuredName").trim();
            }

            if (rs.getString("AppntNo") == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("AppntName") == null)
            {
                this.AppntName = null;
            }
            else
            {
                this.AppntName = rs.getString("AppntName").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("UWRuleCode") == null)
            {
                this.UWRuleCode = null;
            }
            else
            {
                this.UWRuleCode = rs.getString("UWRuleCode").trim();
            }

            if (rs.getString("UWError") == null)
            {
                this.UWError = null;
            }
            else
            {
                this.UWError = rs.getString("UWError").trim();
            }

            if (rs.getString("CurrValue") == null)
            {
                this.CurrValue = null;
            }
            else
            {
                this.CurrValue = rs.getString("CurrValue").trim();
            }

            if (rs.getString("UWPassFlag") == null)
            {
                this.UWPassFlag = null;
            }
            else
            {
                this.UWPassFlag = rs.getString("UWPassFlag").trim();
            }

            if (rs.getString("UWGrade") == null)
            {
                this.UWGrade = null;
            }
            else
            {
                this.UWGrade = rs.getString("UWGrade").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimErrorSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLClaimErrorSchema getSchema()
    {
        LLClaimErrorSchema aLLClaimErrorSchema = new LLClaimErrorSchema();
        aLLClaimErrorSchema.setSchema(this);
        return aLLClaimErrorSchema;
    }

    public LLClaimErrorDB getDB()
    {
        LLClaimErrorDB aDBOper = new LLClaimErrorDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimError描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CaseRelaNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWRuleCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWError)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CurrValue)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWPassFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWGrade)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimError>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            CaseRelaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                       SysConst.PACKAGESPILTER);
            UWRuleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            UWError = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                     SysConst.PACKAGESPILTER);
            CurrValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                       SysConst.PACKAGESPILTER);
            UWPassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            UWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                     SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimErrorSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("ClmNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmNo));
        }
        if (FCode.equals("CaseRelaNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseRelaNo));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equals("GetDutyKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equals("InsuredName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equals("AppntName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("UWRuleCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWRuleCode));
        }
        if (FCode.equals("UWError"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWError));
        }
        if (FCode.equals("CurrValue"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CurrValue));
        }
        if (FCode.equals("UWPassFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWPassFlag));
        }
        if (FCode.equals("UWGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWGrade));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ClmNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CaseRelaNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(UWRuleCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(UWError);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(CurrValue);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(UWPassFlag);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(UWGrade);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("ClmNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmNo = FValue.trim();
            }
            else
            {
                ClmNo = null;
            }
        }
        if (FCode.equals("CaseRelaNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseRelaNo = FValue.trim();
            }
            else
            {
                CaseRelaNo = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("GetDutyKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
            {
                GetDutyKind = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("InsuredName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
            {
                InsuredName = null;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("AppntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
            {
                AppntName = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("UWRuleCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWRuleCode = FValue.trim();
            }
            else
            {
                UWRuleCode = null;
            }
        }
        if (FCode.equals("UWError"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWError = FValue.trim();
            }
            else
            {
                UWError = null;
            }
        }
        if (FCode.equals("CurrValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CurrValue = FValue.trim();
            }
            else
            {
                CurrValue = null;
            }
        }
        if (FCode.equals("UWPassFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWPassFlag = FValue.trim();
            }
            else
            {
                UWPassFlag = null;
            }
        }
        if (FCode.equals("UWGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
            {
                UWGrade = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLClaimErrorSchema other = (LLClaimErrorSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && ClmNo.equals(other.getClmNo())
                && CaseRelaNo.equals(other.getCaseRelaNo())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && ContNo.equals(other.getContNo())
                && PolNo.equals(other.getPolNo())
                && GetDutyKind.equals(other.getGetDutyKind())
                && InsuredNo.equals(other.getInsuredNo())
                && InsuredName.equals(other.getInsuredName())
                && AppntNo.equals(other.getAppntNo())
                && AppntName.equals(other.getAppntName())
                && ManageCom.equals(other.getManageCom())
                && UWRuleCode.equals(other.getUWRuleCode())
                && UWError.equals(other.getUWError())
                && CurrValue.equals(other.getCurrValue())
                && UWPassFlag.equals(other.getUWPassFlag())
                && UWGrade.equals(other.getUWGrade())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ClmNo"))
        {
            return 1;
        }
        if (strFieldName.equals("CaseRelaNo"))
        {
            return 2;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 3;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 4;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 5;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 6;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return 7;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 8;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return 9;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 10;
        }
        if (strFieldName.equals("AppntName"))
        {
            return 11;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 12;
        }
        if (strFieldName.equals("UWRuleCode"))
        {
            return 13;
        }
        if (strFieldName.equals("UWError"))
        {
            return 14;
        }
        if (strFieldName.equals("CurrValue"))
        {
            return 15;
        }
        if (strFieldName.equals("UWPassFlag"))
        {
            return 16;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "ClmNo";
                break;
            case 2:
                strFieldName = "CaseRelaNo";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "GrpPolNo";
                break;
            case 5:
                strFieldName = "ContNo";
                break;
            case 6:
                strFieldName = "PolNo";
                break;
            case 7:
                strFieldName = "GetDutyKind";
                break;
            case 8:
                strFieldName = "InsuredNo";
                break;
            case 9:
                strFieldName = "InsuredName";
                break;
            case 10:
                strFieldName = "AppntNo";
                break;
            case 11:
                strFieldName = "AppntName";
                break;
            case 12:
                strFieldName = "ManageCom";
                break;
            case 13:
                strFieldName = "UWRuleCode";
                break;
            case 14:
                strFieldName = "UWError";
                break;
            case 15:
                strFieldName = "CurrValue";
                break;
            case 16:
                strFieldName = "UWPassFlag";
                break;
            case 17:
                strFieldName = "UWGrade";
                break;
            case 18:
                strFieldName = "MakeDate";
                break;
            case 19:
                strFieldName = "MakeTime";
                break;
            case 20:
                strFieldName = "ModifyDate";
                break;
            case 21:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseRelaNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWRuleCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWError"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CurrValue"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWPassFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
