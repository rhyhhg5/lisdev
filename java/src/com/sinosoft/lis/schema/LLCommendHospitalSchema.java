/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLCommendHospitalDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLCommendHospitalSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 推荐医院列表
 * @CreateDate：2005-05-26
 */
public class LLCommendHospitalSchema implements Schema, Cloneable
{
    // @Field
    /** 咨询通知号码 */
    private String ConsultNo;
    /** 医院代码 */
    private String HospitalCode;
    /** 医院名称 */
    private String HospitalName;
    /** 医院属性 */
    private String HosAtti;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String MngCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 科室 */
    private String SessionRoom;
    /** 床位 */
    private String Bed;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLCommendHospitalSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ConsultNo";
        pk[1] = "HospitalCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLCommendHospitalSchema cloned = (LLCommendHospitalSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getConsultNo()
    {
        if (SysConst.CHANGECHARSET && ConsultNo != null && !ConsultNo.equals(""))
        {
            ConsultNo = StrTool.unicodeToGBK(ConsultNo);
        }
        return ConsultNo;
    }

    public void setConsultNo(String aConsultNo)
    {
        ConsultNo = aConsultNo;
    }

    public String getHospitalCode()
    {
        if (SysConst.CHANGECHARSET && HospitalCode != null &&
            !HospitalCode.equals(""))
        {
            HospitalCode = StrTool.unicodeToGBK(HospitalCode);
        }
        return HospitalCode;
    }

    public void setHospitalCode(String aHospitalCode)
    {
        HospitalCode = aHospitalCode;
    }

    public String getHospitalName()
    {
        if (SysConst.CHANGECHARSET && HospitalName != null &&
            !HospitalName.equals(""))
        {
            HospitalName = StrTool.unicodeToGBK(HospitalName);
        }
        return HospitalName;
    }

    public void setHospitalName(String aHospitalName)
    {
        HospitalName = aHospitalName;
    }

    public String getHosAtti()
    {
        if (SysConst.CHANGECHARSET && HosAtti != null && !HosAtti.equals(""))
        {
            HosAtti = StrTool.unicodeToGBK(HosAtti);
        }
        return HosAtti;
    }

    public void setHosAtti(String aHosAtti)
    {
        HosAtti = aHosAtti;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMngCom()
    {
        if (SysConst.CHANGECHARSET && MngCom != null && !MngCom.equals(""))
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getSessionRoom()
    {
        if (SysConst.CHANGECHARSET && SessionRoom != null &&
            !SessionRoom.equals(""))
        {
            SessionRoom = StrTool.unicodeToGBK(SessionRoom);
        }
        return SessionRoom;
    }

    public void setSessionRoom(String aSessionRoom)
    {
        SessionRoom = aSessionRoom;
    }

    public String getBed()
    {
        if (SysConst.CHANGECHARSET && Bed != null && !Bed.equals(""))
        {
            Bed = StrTool.unicodeToGBK(Bed);
        }
        return Bed;
    }

    public void setBed(String aBed)
    {
        Bed = aBed;
    }

    /**
     * 使用另外一个 LLCommendHospitalSchema 对象给 Schema 赋值
     * @param: aLLCommendHospitalSchema LLCommendHospitalSchema
     **/
    public void setSchema(LLCommendHospitalSchema aLLCommendHospitalSchema)
    {
        this.ConsultNo = aLLCommendHospitalSchema.getConsultNo();
        this.HospitalCode = aLLCommendHospitalSchema.getHospitalCode();
        this.HospitalName = aLLCommendHospitalSchema.getHospitalName();
        this.HosAtti = aLLCommendHospitalSchema.getHosAtti();
        this.Operator = aLLCommendHospitalSchema.getOperator();
        this.MngCom = aLLCommendHospitalSchema.getMngCom();
        this.MakeDate = fDate.getDate(aLLCommendHospitalSchema.getMakeDate());
        this.MakeTime = aLLCommendHospitalSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLCommendHospitalSchema.getModifyDate());
        this.ModifyTime = aLLCommendHospitalSchema.getModifyTime();
        this.SessionRoom = aLLCommendHospitalSchema.getSessionRoom();
        this.Bed = aLLCommendHospitalSchema.getBed();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ConsultNo") == null)
            {
                this.ConsultNo = null;
            }
            else
            {
                this.ConsultNo = rs.getString("ConsultNo").trim();
            }

            if (rs.getString("HospitalCode") == null)
            {
                this.HospitalCode = null;
            }
            else
            {
                this.HospitalCode = rs.getString("HospitalCode").trim();
            }

            if (rs.getString("HospitalName") == null)
            {
                this.HospitalName = null;
            }
            else
            {
                this.HospitalName = rs.getString("HospitalName").trim();
            }

            if (rs.getString("HosAtti") == null)
            {
                this.HosAtti = null;
            }
            else
            {
                this.HosAtti = rs.getString("HosAtti").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("SessionRoom") == null)
            {
                this.SessionRoom = null;
            }
            else
            {
                this.SessionRoom = rs.getString("SessionRoom").trim();
            }

            if (rs.getString("Bed") == null)
            {
                this.Bed = null;
            }
            else
            {
                this.Bed = rs.getString("Bed").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LLCommendHospital表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCommendHospitalSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLCommendHospitalSchema getSchema()
    {
        LLCommendHospitalSchema aLLCommendHospitalSchema = new
                LLCommendHospitalSchema();
        aLLCommendHospitalSchema.setSchema(this);
        return aLLCommendHospitalSchema;
    }

    public LLCommendHospitalDB getDB()
    {
        LLCommendHospitalDB aDBOper = new LLCommendHospitalDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCommendHospital描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ConsultNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(HospitalCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(HospitalName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(HosAtti)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MngCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(SessionRoom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Bed)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCommendHospital>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ConsultNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            HosAtti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            SessionRoom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
            Bed = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                 SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCommendHospitalSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ConsultNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConsultNo));
        }
        if (FCode.equals("HospitalCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
        }
        if (FCode.equals("HospitalName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
        }
        if (FCode.equals("HosAtti"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HosAtti));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("SessionRoom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SessionRoom));
        }
        if (FCode.equals("Bed"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Bed));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ConsultNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(HospitalCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(HospitalName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(HosAtti);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(SessionRoom);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Bed);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ConsultNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConsultNo = FValue.trim();
            }
            else
            {
                ConsultNo = null;
            }
        }
        if (FCode.equals("HospitalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HospitalCode = FValue.trim();
            }
            else
            {
                HospitalCode = null;
            }
        }
        if (FCode.equals("HospitalName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HospitalName = FValue.trim();
            }
            else
            {
                HospitalName = null;
            }
        }
        if (FCode.equals("HosAtti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HosAtti = FValue.trim();
            }
            else
            {
                HosAtti = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("SessionRoom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SessionRoom = FValue.trim();
            }
            else
            {
                SessionRoom = null;
            }
        }
        if (FCode.equals("Bed"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Bed = FValue.trim();
            }
            else
            {
                Bed = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLCommendHospitalSchema other = (LLCommendHospitalSchema) otherObject;
        return
                ConsultNo.equals(other.getConsultNo())
                && HospitalCode.equals(other.getHospitalCode())
                && HospitalName.equals(other.getHospitalName())
                && HosAtti.equals(other.getHosAtti())
                && Operator.equals(other.getOperator())
                && MngCom.equals(other.getMngCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && SessionRoom.equals(other.getSessionRoom())
                && Bed.equals(other.getBed());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ConsultNo"))
        {
            return 0;
        }
        if (strFieldName.equals("HospitalCode"))
        {
            return 1;
        }
        if (strFieldName.equals("HospitalName"))
        {
            return 2;
        }
        if (strFieldName.equals("HosAtti"))
        {
            return 3;
        }
        if (strFieldName.equals("Operator"))
        {
            return 4;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 7;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 9;
        }
        if (strFieldName.equals("SessionRoom"))
        {
            return 10;
        }
        if (strFieldName.equals("Bed"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ConsultNo";
                break;
            case 1:
                strFieldName = "HospitalCode";
                break;
            case 2:
                strFieldName = "HospitalName";
                break;
            case 3:
                strFieldName = "HosAtti";
                break;
            case 4:
                strFieldName = "Operator";
                break;
            case 5:
                strFieldName = "MngCom";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            case 10:
                strFieldName = "SessionRoom";
                break;
            case 11:
                strFieldName = "Bed";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ConsultNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HospitalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HospitalName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HosAtti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SessionRoom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Bed"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
