/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLReportAffixDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLReportAffixSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 报案信息
 * @CreateDate：2005-04-22
 */
public class LLReportAffixSchema implements Schema, Cloneable
{
    // @Field
    /** 附件号码 */
    private String AffixNo;
    /** 流水号 */
    private int SerialNo;
    /** 报案号 */
    private String RptNo;
    /** 原因代码 */
    private String ReasonCode;
    /** 附件类型 */
    private String AffixType;
    /** 附件代码 */
    private String AffixCode;
    /** 附件名称 */
    private String AffixName;
    /** 属性 */
    private String Property;
    /** 齐备件数 */
    private int Count;
    /** 缺少件数 */
    private int ShortCount;
    /** 提供日期 */
    private Date SupplyDate;
    /** 起用日期 */
    private Date FileStartDate;
    /** 停止日期 */
    private Date FileEndDate;
    /** 内容概述 */
    private String FileSummary;
    /** 备注 */
    private String Remark;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 21; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLReportAffixSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AffixNo";
        pk[1] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLReportAffixSchema cloned = (LLReportAffixSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAffixNo()
    {
        if (SysConst.CHANGECHARSET && AffixNo != null && !AffixNo.equals(""))
        {
            AffixNo = StrTool.unicodeToGBK(AffixNo);
        }
        return AffixNo;
    }

    public void setAffixNo(String aAffixNo)
    {
        AffixNo = aAffixNo;
    }

    public int getSerialNo()
    {
        return SerialNo;
    }

    public void setSerialNo(int aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        if (aSerialNo != null && !aSerialNo.equals(""))
        {
            Integer tInteger = new Integer(aSerialNo);
            int i = tInteger.intValue();
            SerialNo = i;
        }
    }

    public String getRptNo()
    {
        if (SysConst.CHANGECHARSET && RptNo != null && !RptNo.equals(""))
        {
            RptNo = StrTool.unicodeToGBK(RptNo);
        }
        return RptNo;
    }

    public void setRptNo(String aRptNo)
    {
        RptNo = aRptNo;
    }

    public String getReasonCode()
    {
        if (SysConst.CHANGECHARSET && ReasonCode != null &&
            !ReasonCode.equals(""))
        {
            ReasonCode = StrTool.unicodeToGBK(ReasonCode);
        }
        return ReasonCode;
    }

    public void setReasonCode(String aReasonCode)
    {
        ReasonCode = aReasonCode;
    }

    public String getAffixType()
    {
        if (SysConst.CHANGECHARSET && AffixType != null && !AffixType.equals(""))
        {
            AffixType = StrTool.unicodeToGBK(AffixType);
        }
        return AffixType;
    }

    public void setAffixType(String aAffixType)
    {
        AffixType = aAffixType;
    }

    public String getAffixCode()
    {
        if (SysConst.CHANGECHARSET && AffixCode != null && !AffixCode.equals(""))
        {
            AffixCode = StrTool.unicodeToGBK(AffixCode);
        }
        return AffixCode;
    }

    public void setAffixCode(String aAffixCode)
    {
        AffixCode = aAffixCode;
    }

    public String getAffixName()
    {
        if (SysConst.CHANGECHARSET && AffixName != null && !AffixName.equals(""))
        {
            AffixName = StrTool.unicodeToGBK(AffixName);
        }
        return AffixName;
    }

    public void setAffixName(String aAffixName)
    {
        AffixName = aAffixName;
    }

    public String getProperty()
    {
        if (SysConst.CHANGECHARSET && Property != null && !Property.equals(""))
        {
            Property = StrTool.unicodeToGBK(Property);
        }
        return Property;
    }

    public void setProperty(String aProperty)
    {
        Property = aProperty;
    }

    public int getCount()
    {
        return Count;
    }

    public void setCount(int aCount)
    {
        Count = aCount;
    }

    public void setCount(String aCount)
    {
        if (aCount != null && !aCount.equals(""))
        {
            Integer tInteger = new Integer(aCount);
            int i = tInteger.intValue();
            Count = i;
        }
    }

    public int getShortCount()
    {
        return ShortCount;
    }

    public void setShortCount(int aShortCount)
    {
        ShortCount = aShortCount;
    }

    public void setShortCount(String aShortCount)
    {
        if (aShortCount != null && !aShortCount.equals(""))
        {
            Integer tInteger = new Integer(aShortCount);
            int i = tInteger.intValue();
            ShortCount = i;
        }
    }

    public String getSupplyDate()
    {
        if (SupplyDate != null)
        {
            return fDate.getString(SupplyDate);
        }
        else
        {
            return null;
        }
    }

    public void setSupplyDate(Date aSupplyDate)
    {
        SupplyDate = aSupplyDate;
    }

    public void setSupplyDate(String aSupplyDate)
    {
        if (aSupplyDate != null && !aSupplyDate.equals(""))
        {
            SupplyDate = fDate.getDate(aSupplyDate);
        }
        else
        {
            SupplyDate = null;
        }
    }

    public String getFileStartDate()
    {
        if (FileStartDate != null)
        {
            return fDate.getString(FileStartDate);
        }
        else
        {
            return null;
        }
    }

    public void setFileStartDate(Date aFileStartDate)
    {
        FileStartDate = aFileStartDate;
    }

    public void setFileStartDate(String aFileStartDate)
    {
        if (aFileStartDate != null && !aFileStartDate.equals(""))
        {
            FileStartDate = fDate.getDate(aFileStartDate);
        }
        else
        {
            FileStartDate = null;
        }
    }

    public String getFileEndDate()
    {
        if (FileEndDate != null)
        {
            return fDate.getString(FileEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setFileEndDate(Date aFileEndDate)
    {
        FileEndDate = aFileEndDate;
    }

    public void setFileEndDate(String aFileEndDate)
    {
        if (aFileEndDate != null && !aFileEndDate.equals(""))
        {
            FileEndDate = fDate.getDate(aFileEndDate);
        }
        else
        {
            FileEndDate = null;
        }
    }

    public String getFileSummary()
    {
        if (SysConst.CHANGECHARSET && FileSummary != null &&
            !FileSummary.equals(""))
        {
            FileSummary = StrTool.unicodeToGBK(FileSummary);
        }
        return FileSummary;
    }

    public void setFileSummary(String aFileSummary)
    {
        FileSummary = aFileSummary;
    }

    public String getRemark()
    {
        if (SysConst.CHANGECHARSET && Remark != null && !Remark.equals(""))
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getMngCom()
    {
        if (SysConst.CHANGECHARSET && MngCom != null && !MngCom.equals(""))
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLReportAffixSchema 对象给 Schema 赋值
     * @param: aLLReportAffixSchema LLReportAffixSchema
     **/
    public void setSchema(LLReportAffixSchema aLLReportAffixSchema)
    {
        this.AffixNo = aLLReportAffixSchema.getAffixNo();
        this.SerialNo = aLLReportAffixSchema.getSerialNo();
        this.RptNo = aLLReportAffixSchema.getRptNo();
        this.ReasonCode = aLLReportAffixSchema.getReasonCode();
        this.AffixType = aLLReportAffixSchema.getAffixType();
        this.AffixCode = aLLReportAffixSchema.getAffixCode();
        this.AffixName = aLLReportAffixSchema.getAffixName();
        this.Property = aLLReportAffixSchema.getProperty();
        this.Count = aLLReportAffixSchema.getCount();
        this.ShortCount = aLLReportAffixSchema.getShortCount();
        this.SupplyDate = fDate.getDate(aLLReportAffixSchema.getSupplyDate());
        this.FileStartDate = fDate.getDate(aLLReportAffixSchema.
                                           getFileStartDate());
        this.FileEndDate = fDate.getDate(aLLReportAffixSchema.getFileEndDate());
        this.FileSummary = aLLReportAffixSchema.getFileSummary();
        this.Remark = aLLReportAffixSchema.getRemark();
        this.MngCom = aLLReportAffixSchema.getMngCom();
        this.Operator = aLLReportAffixSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLReportAffixSchema.getMakeDate());
        this.MakeTime = aLLReportAffixSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLReportAffixSchema.getModifyDate());
        this.ModifyTime = aLLReportAffixSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AffixNo") == null)
            {
                this.AffixNo = null;
            }
            else
            {
                this.AffixNo = rs.getString("AffixNo").trim();
            }

            this.SerialNo = rs.getInt("SerialNo");
            if (rs.getString("RptNo") == null)
            {
                this.RptNo = null;
            }
            else
            {
                this.RptNo = rs.getString("RptNo").trim();
            }

            if (rs.getString("ReasonCode") == null)
            {
                this.ReasonCode = null;
            }
            else
            {
                this.ReasonCode = rs.getString("ReasonCode").trim();
            }

            if (rs.getString("AffixType") == null)
            {
                this.AffixType = null;
            }
            else
            {
                this.AffixType = rs.getString("AffixType").trim();
            }

            if (rs.getString("AffixCode") == null)
            {
                this.AffixCode = null;
            }
            else
            {
                this.AffixCode = rs.getString("AffixCode").trim();
            }

            if (rs.getString("AffixName") == null)
            {
                this.AffixName = null;
            }
            else
            {
                this.AffixName = rs.getString("AffixName").trim();
            }

            if (rs.getString("Property") == null)
            {
                this.Property = null;
            }
            else
            {
                this.Property = rs.getString("Property").trim();
            }

            this.Count = rs.getInt("Count");
            this.ShortCount = rs.getInt("ShortCount");
            this.SupplyDate = rs.getDate("SupplyDate");
            this.FileStartDate = rs.getDate("FileStartDate");
            this.FileEndDate = rs.getDate("FileEndDate");
            if (rs.getString("FileSummary") == null)
            {
                this.FileSummary = null;
            }
            else
            {
                this.FileSummary = rs.getString("FileSummary").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLReportAffixSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLReportAffixSchema getSchema()
    {
        LLReportAffixSchema aLLReportAffixSchema = new LLReportAffixSchema();
        aLLReportAffixSchema.setSchema(this);
        return aLLReportAffixSchema;
    }

    public LLReportAffixDB getDB()
    {
        LLReportAffixDB aDBOper = new LLReportAffixDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLReportAffix描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AffixNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RptNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ReasonCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AffixType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AffixCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AffixName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Property)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Count));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ShortCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                SupplyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                FileStartDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                FileEndDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(FileSummary)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Remark)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MngCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLReportAffix>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AffixNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            SerialNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).intValue();
            RptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            ReasonCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            AffixType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            AffixCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            AffixName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            Property = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            Count = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    9, SysConst.PACKAGESPILTER))).intValue();
            ShortCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            SupplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            FileStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            FileEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            FileSummary = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                         SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                    SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLReportAffixSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AffixNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AffixNo));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("RptNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptNo));
        }
        if (FCode.equals("ReasonCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReasonCode));
        }
        if (FCode.equals("AffixType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AffixType));
        }
        if (FCode.equals("AffixCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AffixCode));
        }
        if (FCode.equals("AffixName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AffixName));
        }
        if (FCode.equals("Property"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Property));
        }
        if (FCode.equals("Count"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Count));
        }
        if (FCode.equals("ShortCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ShortCount));
        }
        if (FCode.equals("SupplyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getSupplyDate()));
        }
        if (FCode.equals("FileStartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getFileStartDate()));
        }
        if (FCode.equals("FileEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getFileEndDate()));
        }
        if (FCode.equals("FileSummary"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FileSummary));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AffixNo);
                break;
            case 1:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RptNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ReasonCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AffixType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AffixCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AffixName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Property);
                break;
            case 8:
                strFieldValue = String.valueOf(Count);
                break;
            case 9:
                strFieldValue = String.valueOf(ShortCount);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSupplyDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFileStartDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFileEndDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(FileSummary);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AffixNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixNo = FValue.trim();
            }
            else
            {
                AffixNo = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SerialNo = i;
            }
        }
        if (FCode.equals("RptNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RptNo = FValue.trim();
            }
            else
            {
                RptNo = null;
            }
        }
        if (FCode.equals("ReasonCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReasonCode = FValue.trim();
            }
            else
            {
                ReasonCode = null;
            }
        }
        if (FCode.equals("AffixType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixType = FValue.trim();
            }
            else
            {
                AffixType = null;
            }
        }
        if (FCode.equals("AffixCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixCode = FValue.trim();
            }
            else
            {
                AffixCode = null;
            }
        }
        if (FCode.equals("AffixName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixName = FValue.trim();
            }
            else
            {
                AffixName = null;
            }
        }
        if (FCode.equals("Property"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Property = FValue.trim();
            }
            else
            {
                Property = null;
            }
        }
        if (FCode.equals("Count"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Count = i;
            }
        }
        if (FCode.equals("ShortCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ShortCount = i;
            }
        }
        if (FCode.equals("SupplyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SupplyDate = fDate.getDate(FValue);
            }
            else
            {
                SupplyDate = null;
            }
        }
        if (FCode.equals("FileStartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FileStartDate = fDate.getDate(FValue);
            }
            else
            {
                FileStartDate = null;
            }
        }
        if (FCode.equals("FileEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FileEndDate = fDate.getDate(FValue);
            }
            else
            {
                FileEndDate = null;
            }
        }
        if (FCode.equals("FileSummary"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FileSummary = FValue.trim();
            }
            else
            {
                FileSummary = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLReportAffixSchema other = (LLReportAffixSchema) otherObject;
        return
                AffixNo.equals(other.getAffixNo())
                && SerialNo == other.getSerialNo()
                && RptNo.equals(other.getRptNo())
                && ReasonCode.equals(other.getReasonCode())
                && AffixType.equals(other.getAffixType())
                && AffixCode.equals(other.getAffixCode())
                && AffixName.equals(other.getAffixName())
                && Property.equals(other.getProperty())
                && Count == other.getCount()
                && ShortCount == other.getShortCount()
                && fDate.getString(SupplyDate).equals(other.getSupplyDate())
                && fDate.getString(FileStartDate).equals(other.getFileStartDate())
                && fDate.getString(FileEndDate).equals(other.getFileEndDate())
                && FileSummary.equals(other.getFileSummary())
                && Remark.equals(other.getRemark())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AffixNo"))
        {
            return 0;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 1;
        }
        if (strFieldName.equals("RptNo"))
        {
            return 2;
        }
        if (strFieldName.equals("ReasonCode"))
        {
            return 3;
        }
        if (strFieldName.equals("AffixType"))
        {
            return 4;
        }
        if (strFieldName.equals("AffixCode"))
        {
            return 5;
        }
        if (strFieldName.equals("AffixName"))
        {
            return 6;
        }
        if (strFieldName.equals("Property"))
        {
            return 7;
        }
        if (strFieldName.equals("Count"))
        {
            return 8;
        }
        if (strFieldName.equals("ShortCount"))
        {
            return 9;
        }
        if (strFieldName.equals("SupplyDate"))
        {
            return 10;
        }
        if (strFieldName.equals("FileStartDate"))
        {
            return 11;
        }
        if (strFieldName.equals("FileEndDate"))
        {
            return 12;
        }
        if (strFieldName.equals("FileSummary"))
        {
            return 13;
        }
        if (strFieldName.equals("Remark"))
        {
            return 14;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 15;
        }
        if (strFieldName.equals("Operator"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 20;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AffixNo";
                break;
            case 1:
                strFieldName = "SerialNo";
                break;
            case 2:
                strFieldName = "RptNo";
                break;
            case 3:
                strFieldName = "ReasonCode";
                break;
            case 4:
                strFieldName = "AffixType";
                break;
            case 5:
                strFieldName = "AffixCode";
                break;
            case 6:
                strFieldName = "AffixName";
                break;
            case 7:
                strFieldName = "Property";
                break;
            case 8:
                strFieldName = "Count";
                break;
            case 9:
                strFieldName = "ShortCount";
                break;
            case 10:
                strFieldName = "SupplyDate";
                break;
            case 11:
                strFieldName = "FileStartDate";
                break;
            case 12:
                strFieldName = "FileEndDate";
                break;
            case 13:
                strFieldName = "FileSummary";
                break;
            case 14:
                strFieldName = "Remark";
                break;
            case 15:
                strFieldName = "MngCom";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MakeDate";
                break;
            case 18:
                strFieldName = "MakeTime";
                break;
            case 19:
                strFieldName = "ModifyDate";
                break;
            case 20:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AffixNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RptNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReasonCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AffixType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AffixCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AffixName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Property"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Count"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ShortCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("SupplyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FileStartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FileEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FileSummary"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_INT;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
