/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.BPOLCPolDB;

/*
 * <p>ClassName: BPOLCPolSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2014-09-02
 */
public class BPOLCPolSchema implements Schema, Cloneable
{
	// @Field
	/** 外包批次 */
	private String BPOBatchNo;
	/** 险种id */
	private String PolID;
	/** 合同id */
	private String ContID;
	/** 印刷号 */
	private String PrtNo;
	/** 险种代码 */
	private String RiskCode;
	/** 保单类型 */
	private String FamilyType;
	/** 主险id */
	private String MainPolID;
	/** 被保人id */
	private String InsuredID;
	/** 与投保人关系 */
	private String RelationToAppnt;
	/** 与主被保人关系 */
	private String RelationToMainInsured;
	/** 投保人id */
	private String AppntID;
	/** 连身被保险人id */
	private String RelaId;
	/** 投保申请日期 */
	private String PolApplyDate;
	/** 生效日期 */
	private String CValiDate;
	/** 初审人 */
	private String FirstTrialOperator;
	/** 初审时间 */
	private String FirstTrialTime;
	/** 收单人 */
	private String ReceiveOperator;
	/** 收单日期 */
	private String ReceiveDate;
	/** 收单时间 */
	private String ReceiveTime;
	/** 暂收据号 */
	private String TempFeeNo;
	/** 交费方式 */
	private String PayMode;
	/** 生存保险金领取方式 */
	private String LiveGetMode;
	/** 身故金领取方式 */
	private String DeadGetMode;
	/** 交费间隔 */
	private String PayIntv;
	/** 保险期间 */
	private String InsuYear;
	/** 保险期间标志 */
	private String InsuYearFlag;
	/** 交费年期 */
	private String PayEndYear;
	/** 交费年期标志 */
	private String PayEndYearFlag;
	/** 年金开始领取年龄/年期 */
	private String GetYear;
	/** 年金开始领取标志 */
	private String GetYearFlag;
	/** 起领日期计算类型 */
	private String GetStartType;
	/** 年金领取类型 */
	private String GetDutyKind;
	/** 红利领取方式 */
	private String BonusGetMode;
	/** 计算方向 */
	private String PremToAmnt;
	/** 档次 */
	private String Mult;
	/** 保费 */
	private String Prem;
	/** 保额 */
	private String Amnt;
	/** 计算规则 */
	private String CalRule;
	/** 费率 */
	private String FloatRate;
	/** 免赔额 */
	private String GetLimit;
	/** 赔付比例 */
	private String GetRate;
	/** 是否体检 */
	private String HealthCheckFlag;
	/** 溢交保费方式 */
	private String OutPayFlag;
	/** 管理机构 */
	private String ManageCom;
	/** 销售渠道 */
	private String SaleChnl;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人姓名 */
	private String AgentName;
	/** 代理机构 */
	private String AgentCom;
	/** 银行营业网点 */
	private String BankWorkSite;
	/** 备用字段1 */
	private String StandbyFlag1;
	/** 备用字段2 */
	private String StandbyFlag2;
	/** 备用字段3 */
	private String StandbyFlag3;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 保险责任终止日期 */
	private String CInValiDate;
	/** 保险份数 */
	private String Copys;
	/** 初审日期 */
	private String FirstTrialDate;
	/** 追加保费 */
	private double SupplementaryPrem;
	/** 扩展缴费方式 */
	private String ExPayMode;
	/** 集团交叉销售渠道 */
	private String Crs_SaleChnl;
	/** 集团销售业务类型 */
	private String Crs_BussType;
	/** 集团代理机构 */
	private String GrpAgentCom;
	/** 集团代理人 */
	private String GrpAgentCode;
	/** 集团代理人姓名 */
	private String GrpAgentName;
	/** 集团代理人身份证 */
	private String GrpAgentIDNo;

	public static final int FIELDNUM = 68;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public BPOLCPolSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "BPOBatchNo";
		pk[1] = "PolID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		BPOLCPolSchema cloned = (BPOLCPolSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBPOBatchNo()
	{
		return BPOBatchNo;
	}
	public void setBPOBatchNo(String aBPOBatchNo)
	{
		BPOBatchNo = aBPOBatchNo;
	}
	public String getPolID()
	{
		return PolID;
	}
	public void setPolID(String aPolID)
	{
		PolID = aPolID;
	}
	public String getContID()
	{
		return ContID;
	}
	public void setContID(String aContID)
	{
		ContID = aContID;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getFamilyType()
	{
		return FamilyType;
	}
	public void setFamilyType(String aFamilyType)
	{
		FamilyType = aFamilyType;
	}
	public String getMainPolID()
	{
		return MainPolID;
	}
	public void setMainPolID(String aMainPolID)
	{
		MainPolID = aMainPolID;
	}
	public String getInsuredID()
	{
		return InsuredID;
	}
	public void setInsuredID(String aInsuredID)
	{
		InsuredID = aInsuredID;
	}
	public String getRelationToAppnt()
	{
		return RelationToAppnt;
	}
	public void setRelationToAppnt(String aRelationToAppnt)
	{
		RelationToAppnt = aRelationToAppnt;
	}
	public String getRelationToMainInsured()
	{
		return RelationToMainInsured;
	}
	public void setRelationToMainInsured(String aRelationToMainInsured)
	{
		RelationToMainInsured = aRelationToMainInsured;
	}
	public String getAppntID()
	{
		return AppntID;
	}
	public void setAppntID(String aAppntID)
	{
		AppntID = aAppntID;
	}
	public String getRelaId()
	{
		return RelaId;
	}
	public void setRelaId(String aRelaId)
	{
		RelaId = aRelaId;
	}
	public String getPolApplyDate()
	{
		return PolApplyDate;
	}
	public void setPolApplyDate(String aPolApplyDate)
	{
		PolApplyDate = aPolApplyDate;
	}
	public String getCValiDate()
	{
		return CValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public String getFirstTrialOperator()
	{
		return FirstTrialOperator;
	}
	public void setFirstTrialOperator(String aFirstTrialOperator)
	{
		FirstTrialOperator = aFirstTrialOperator;
	}
	public String getFirstTrialTime()
	{
		return FirstTrialTime;
	}
	public void setFirstTrialTime(String aFirstTrialTime)
	{
		FirstTrialTime = aFirstTrialTime;
	}
	public String getReceiveOperator()
	{
		return ReceiveOperator;
	}
	public void setReceiveOperator(String aReceiveOperator)
	{
		ReceiveOperator = aReceiveOperator;
	}
	public String getReceiveDate()
	{
		return ReceiveDate;
	}
	public void setReceiveDate(String aReceiveDate)
	{
		ReceiveDate = aReceiveDate;
	}
	public String getReceiveTime()
	{
		return ReceiveTime;
	}
	public void setReceiveTime(String aReceiveTime)
	{
		ReceiveTime = aReceiveTime;
	}
	public String getTempFeeNo()
	{
		return TempFeeNo;
	}
	public void setTempFeeNo(String aTempFeeNo)
	{
		TempFeeNo = aTempFeeNo;
	}
	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getLiveGetMode()
	{
		return LiveGetMode;
	}
	public void setLiveGetMode(String aLiveGetMode)
	{
		LiveGetMode = aLiveGetMode;
	}
	public String getDeadGetMode()
	{
		return DeadGetMode;
	}
	public void setDeadGetMode(String aDeadGetMode)
	{
		DeadGetMode = aDeadGetMode;
	}
	public String getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public String getInsuYear()
	{
		return InsuYear;
	}
	public void setInsuYear(String aInsuYear)
	{
		InsuYear = aInsuYear;
	}
	public String getInsuYearFlag()
	{
		return InsuYearFlag;
	}
	public void setInsuYearFlag(String aInsuYearFlag)
	{
		InsuYearFlag = aInsuYearFlag;
	}
	public String getPayEndYear()
	{
		return PayEndYear;
	}
	public void setPayEndYear(String aPayEndYear)
	{
		PayEndYear = aPayEndYear;
	}
	public String getPayEndYearFlag()
	{
		return PayEndYearFlag;
	}
	public void setPayEndYearFlag(String aPayEndYearFlag)
	{
		PayEndYearFlag = aPayEndYearFlag;
	}
	public String getGetYear()
	{
		return GetYear;
	}
	public void setGetYear(String aGetYear)
	{
		GetYear = aGetYear;
	}
	public String getGetYearFlag()
	{
		return GetYearFlag;
	}
	public void setGetYearFlag(String aGetYearFlag)
	{
		GetYearFlag = aGetYearFlag;
	}
	public String getGetStartType()
	{
		return GetStartType;
	}
	public void setGetStartType(String aGetStartType)
	{
		GetStartType = aGetStartType;
	}
	public String getGetDutyKind()
	{
		return GetDutyKind;
	}
	public void setGetDutyKind(String aGetDutyKind)
	{
		GetDutyKind = aGetDutyKind;
	}
	public String getBonusGetMode()
	{
		return BonusGetMode;
	}
	public void setBonusGetMode(String aBonusGetMode)
	{
		BonusGetMode = aBonusGetMode;
	}
	public String getPremToAmnt()
	{
		return PremToAmnt;
	}
	public void setPremToAmnt(String aPremToAmnt)
	{
		PremToAmnt = aPremToAmnt;
	}
	public String getMult()
	{
		return Mult;
	}
	public void setMult(String aMult)
	{
		Mult = aMult;
	}
	public String getPrem()
	{
		return Prem;
	}
	public void setPrem(String aPrem)
	{
		Prem = aPrem;
	}
	public String getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(String aAmnt)
	{
		Amnt = aAmnt;
	}
	public String getCalRule()
	{
		return CalRule;
	}
	public void setCalRule(String aCalRule)
	{
		CalRule = aCalRule;
	}
	public String getFloatRate()
	{
		return FloatRate;
	}
	public void setFloatRate(String aFloatRate)
	{
		FloatRate = aFloatRate;
	}
	public String getGetLimit()
	{
		return GetLimit;
	}
	public void setGetLimit(String aGetLimit)
	{
		GetLimit = aGetLimit;
	}
	public String getGetRate()
	{
		return GetRate;
	}
	public void setGetRate(String aGetRate)
	{
		GetRate = aGetRate;
	}
	public String getHealthCheckFlag()
	{
		return HealthCheckFlag;
	}
	public void setHealthCheckFlag(String aHealthCheckFlag)
	{
		HealthCheckFlag = aHealthCheckFlag;
	}
	public String getOutPayFlag()
	{
		return OutPayFlag;
	}
	public void setOutPayFlag(String aOutPayFlag)
	{
		OutPayFlag = aOutPayFlag;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentName()
	{
		return AgentName;
	}
	public void setAgentName(String aAgentName)
	{
		AgentName = aAgentName;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getBankWorkSite()
	{
		return BankWorkSite;
	}
	public void setBankWorkSite(String aBankWorkSite)
	{
		BankWorkSite = aBankWorkSite;
	}
	public String getStandbyFlag1()
	{
		return StandbyFlag1;
	}
	public void setStandbyFlag1(String aStandbyFlag1)
	{
		StandbyFlag1 = aStandbyFlag1;
	}
	public String getStandbyFlag2()
	{
		return StandbyFlag2;
	}
	public void setStandbyFlag2(String aStandbyFlag2)
	{
		StandbyFlag2 = aStandbyFlag2;
	}
	public String getStandbyFlag3()
	{
		return StandbyFlag3;
	}
	public void setStandbyFlag3(String aStandbyFlag3)
	{
		StandbyFlag3 = aStandbyFlag3;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getCInValiDate()
	{
		return CInValiDate;
	}
	public void setCInValiDate(String aCInValiDate)
	{
		CInValiDate = aCInValiDate;
	}
	public String getCopys()
	{
		return Copys;
	}
	public void setCopys(String aCopys)
	{
		Copys = aCopys;
	}
	public String getFirstTrialDate()
	{
		return FirstTrialDate;
	}
	public void setFirstTrialDate(String aFirstTrialDate)
	{
		FirstTrialDate = aFirstTrialDate;
	}
	public double getSupplementaryPrem()
	{
		return SupplementaryPrem;
	}
	public void setSupplementaryPrem(double aSupplementaryPrem)
	{
		SupplementaryPrem = Arith.round(aSupplementaryPrem, 2);
	}
	public void setSupplementaryPrem(String aSupplementaryPrem)
	{
		if (aSupplementaryPrem != null && !aSupplementaryPrem.equals(""))
		{
			Double tDouble = new Double(aSupplementaryPrem);
			double d = tDouble.doubleValue();
                SupplementaryPrem = Arith.round(d, 2);
		}
	}

	public String getExPayMode()
	{
		return ExPayMode;
	}
	public void setExPayMode(String aExPayMode)
	{
		ExPayMode = aExPayMode;
	}
	public String getCrs_SaleChnl()
	{
		return Crs_SaleChnl;
	}
	public void setCrs_SaleChnl(String aCrs_SaleChnl)
	{
		Crs_SaleChnl = aCrs_SaleChnl;
	}
	public String getCrs_BussType()
	{
		return Crs_BussType;
	}
	public void setCrs_BussType(String aCrs_BussType)
	{
		Crs_BussType = aCrs_BussType;
	}
	public String getGrpAgentCom()
	{
		return GrpAgentCom;
	}
	public void setGrpAgentCom(String aGrpAgentCom)
	{
		GrpAgentCom = aGrpAgentCom;
	}
	public String getGrpAgentCode()
	{
		return GrpAgentCode;
	}
	public void setGrpAgentCode(String aGrpAgentCode)
	{
		GrpAgentCode = aGrpAgentCode;
	}
	public String getGrpAgentName()
	{
		return GrpAgentName;
	}
	public void setGrpAgentName(String aGrpAgentName)
	{
		GrpAgentName = aGrpAgentName;
	}
	public String getGrpAgentIDNo()
	{
		return GrpAgentIDNo;
	}
	public void setGrpAgentIDNo(String aGrpAgentIDNo)
	{
		GrpAgentIDNo = aGrpAgentIDNo;
	}

	/**
	* 使用另外一个 BPOLCPolSchema 对象给 Schema 赋值
	* @param: aBPOLCPolSchema BPOLCPolSchema
	**/
	public void setSchema(BPOLCPolSchema aBPOLCPolSchema)
	{
		this.BPOBatchNo = aBPOLCPolSchema.getBPOBatchNo();
		this.PolID = aBPOLCPolSchema.getPolID();
		this.ContID = aBPOLCPolSchema.getContID();
		this.PrtNo = aBPOLCPolSchema.getPrtNo();
		this.RiskCode = aBPOLCPolSchema.getRiskCode();
		this.FamilyType = aBPOLCPolSchema.getFamilyType();
		this.MainPolID = aBPOLCPolSchema.getMainPolID();
		this.InsuredID = aBPOLCPolSchema.getInsuredID();
		this.RelationToAppnt = aBPOLCPolSchema.getRelationToAppnt();
		this.RelationToMainInsured = aBPOLCPolSchema.getRelationToMainInsured();
		this.AppntID = aBPOLCPolSchema.getAppntID();
		this.RelaId = aBPOLCPolSchema.getRelaId();
		this.PolApplyDate = aBPOLCPolSchema.getPolApplyDate();
		this.CValiDate = aBPOLCPolSchema.getCValiDate();
		this.FirstTrialOperator = aBPOLCPolSchema.getFirstTrialOperator();
		this.FirstTrialTime = aBPOLCPolSchema.getFirstTrialTime();
		this.ReceiveOperator = aBPOLCPolSchema.getReceiveOperator();
		this.ReceiveDate = aBPOLCPolSchema.getReceiveDate();
		this.ReceiveTime = aBPOLCPolSchema.getReceiveTime();
		this.TempFeeNo = aBPOLCPolSchema.getTempFeeNo();
		this.PayMode = aBPOLCPolSchema.getPayMode();
		this.LiveGetMode = aBPOLCPolSchema.getLiveGetMode();
		this.DeadGetMode = aBPOLCPolSchema.getDeadGetMode();
		this.PayIntv = aBPOLCPolSchema.getPayIntv();
		this.InsuYear = aBPOLCPolSchema.getInsuYear();
		this.InsuYearFlag = aBPOLCPolSchema.getInsuYearFlag();
		this.PayEndYear = aBPOLCPolSchema.getPayEndYear();
		this.PayEndYearFlag = aBPOLCPolSchema.getPayEndYearFlag();
		this.GetYear = aBPOLCPolSchema.getGetYear();
		this.GetYearFlag = aBPOLCPolSchema.getGetYearFlag();
		this.GetStartType = aBPOLCPolSchema.getGetStartType();
		this.GetDutyKind = aBPOLCPolSchema.getGetDutyKind();
		this.BonusGetMode = aBPOLCPolSchema.getBonusGetMode();
		this.PremToAmnt = aBPOLCPolSchema.getPremToAmnt();
		this.Mult = aBPOLCPolSchema.getMult();
		this.Prem = aBPOLCPolSchema.getPrem();
		this.Amnt = aBPOLCPolSchema.getAmnt();
		this.CalRule = aBPOLCPolSchema.getCalRule();
		this.FloatRate = aBPOLCPolSchema.getFloatRate();
		this.GetLimit = aBPOLCPolSchema.getGetLimit();
		this.GetRate = aBPOLCPolSchema.getGetRate();
		this.HealthCheckFlag = aBPOLCPolSchema.getHealthCheckFlag();
		this.OutPayFlag = aBPOLCPolSchema.getOutPayFlag();
		this.ManageCom = aBPOLCPolSchema.getManageCom();
		this.SaleChnl = aBPOLCPolSchema.getSaleChnl();
		this.AgentCode = aBPOLCPolSchema.getAgentCode();
		this.AgentName = aBPOLCPolSchema.getAgentName();
		this.AgentCom = aBPOLCPolSchema.getAgentCom();
		this.BankWorkSite = aBPOLCPolSchema.getBankWorkSite();
		this.StandbyFlag1 = aBPOLCPolSchema.getStandbyFlag1();
		this.StandbyFlag2 = aBPOLCPolSchema.getStandbyFlag2();
		this.StandbyFlag3 = aBPOLCPolSchema.getStandbyFlag3();
		this.Operator = aBPOLCPolSchema.getOperator();
		this.MakeDate = fDate.getDate( aBPOLCPolSchema.getMakeDate());
		this.MakeTime = aBPOLCPolSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aBPOLCPolSchema.getModifyDate());
		this.ModifyTime = aBPOLCPolSchema.getModifyTime();
		this.CInValiDate = aBPOLCPolSchema.getCInValiDate();
		this.Copys = aBPOLCPolSchema.getCopys();
		this.FirstTrialDate = aBPOLCPolSchema.getFirstTrialDate();
		this.SupplementaryPrem = aBPOLCPolSchema.getSupplementaryPrem();
		this.ExPayMode = aBPOLCPolSchema.getExPayMode();
		this.Crs_SaleChnl = aBPOLCPolSchema.getCrs_SaleChnl();
		this.Crs_BussType = aBPOLCPolSchema.getCrs_BussType();
		this.GrpAgentCom = aBPOLCPolSchema.getGrpAgentCom();
		this.GrpAgentCode = aBPOLCPolSchema.getGrpAgentCode();
		this.GrpAgentName = aBPOLCPolSchema.getGrpAgentName();
		this.GrpAgentIDNo = aBPOLCPolSchema.getGrpAgentIDNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BPOBatchNo") == null )
				this.BPOBatchNo = null;
			else
				this.BPOBatchNo = rs.getString("BPOBatchNo").trim();

			if( rs.getString("PolID") == null )
				this.PolID = null;
			else
				this.PolID = rs.getString("PolID").trim();

			if( rs.getString("ContID") == null )
				this.ContID = null;
			else
				this.ContID = rs.getString("ContID").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("FamilyType") == null )
				this.FamilyType = null;
			else
				this.FamilyType = rs.getString("FamilyType").trim();

			if( rs.getString("MainPolID") == null )
				this.MainPolID = null;
			else
				this.MainPolID = rs.getString("MainPolID").trim();

			if( rs.getString("InsuredID") == null )
				this.InsuredID = null;
			else
				this.InsuredID = rs.getString("InsuredID").trim();

			if( rs.getString("RelationToAppnt") == null )
				this.RelationToAppnt = null;
			else
				this.RelationToAppnt = rs.getString("RelationToAppnt").trim();

			if( rs.getString("RelationToMainInsured") == null )
				this.RelationToMainInsured = null;
			else
				this.RelationToMainInsured = rs.getString("RelationToMainInsured").trim();

			if( rs.getString("AppntID") == null )
				this.AppntID = null;
			else
				this.AppntID = rs.getString("AppntID").trim();

			if( rs.getString("RelaId") == null )
				this.RelaId = null;
			else
				this.RelaId = rs.getString("RelaId").trim();

			if( rs.getString("PolApplyDate") == null )
				this.PolApplyDate = null;
			else
				this.PolApplyDate = rs.getString("PolApplyDate").trim();

			if( rs.getString("CValiDate") == null )
				this.CValiDate = null;
			else
				this.CValiDate = rs.getString("CValiDate").trim();

			if( rs.getString("FirstTrialOperator") == null )
				this.FirstTrialOperator = null;
			else
				this.FirstTrialOperator = rs.getString("FirstTrialOperator").trim();

			if( rs.getString("FirstTrialTime") == null )
				this.FirstTrialTime = null;
			else
				this.FirstTrialTime = rs.getString("FirstTrialTime").trim();

			if( rs.getString("ReceiveOperator") == null )
				this.ReceiveOperator = null;
			else
				this.ReceiveOperator = rs.getString("ReceiveOperator").trim();

			if( rs.getString("ReceiveDate") == null )
				this.ReceiveDate = null;
			else
				this.ReceiveDate = rs.getString("ReceiveDate").trim();

			if( rs.getString("ReceiveTime") == null )
				this.ReceiveTime = null;
			else
				this.ReceiveTime = rs.getString("ReceiveTime").trim();

			if( rs.getString("TempFeeNo") == null )
				this.TempFeeNo = null;
			else
				this.TempFeeNo = rs.getString("TempFeeNo").trim();

			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("LiveGetMode") == null )
				this.LiveGetMode = null;
			else
				this.LiveGetMode = rs.getString("LiveGetMode").trim();

			if( rs.getString("DeadGetMode") == null )
				this.DeadGetMode = null;
			else
				this.DeadGetMode = rs.getString("DeadGetMode").trim();

			if( rs.getString("PayIntv") == null )
				this.PayIntv = null;
			else
				this.PayIntv = rs.getString("PayIntv").trim();

			if( rs.getString("InsuYear") == null )
				this.InsuYear = null;
			else
				this.InsuYear = rs.getString("InsuYear").trim();

			if( rs.getString("InsuYearFlag") == null )
				this.InsuYearFlag = null;
			else
				this.InsuYearFlag = rs.getString("InsuYearFlag").trim();

			if( rs.getString("PayEndYear") == null )
				this.PayEndYear = null;
			else
				this.PayEndYear = rs.getString("PayEndYear").trim();

			if( rs.getString("PayEndYearFlag") == null )
				this.PayEndYearFlag = null;
			else
				this.PayEndYearFlag = rs.getString("PayEndYearFlag").trim();

			if( rs.getString("GetYear") == null )
				this.GetYear = null;
			else
				this.GetYear = rs.getString("GetYear").trim();

			if( rs.getString("GetYearFlag") == null )
				this.GetYearFlag = null;
			else
				this.GetYearFlag = rs.getString("GetYearFlag").trim();

			if( rs.getString("GetStartType") == null )
				this.GetStartType = null;
			else
				this.GetStartType = rs.getString("GetStartType").trim();

			if( rs.getString("GetDutyKind") == null )
				this.GetDutyKind = null;
			else
				this.GetDutyKind = rs.getString("GetDutyKind").trim();

			if( rs.getString("BonusGetMode") == null )
				this.BonusGetMode = null;
			else
				this.BonusGetMode = rs.getString("BonusGetMode").trim();

			if( rs.getString("PremToAmnt") == null )
				this.PremToAmnt = null;
			else
				this.PremToAmnt = rs.getString("PremToAmnt").trim();

			if( rs.getString("Mult") == null )
				this.Mult = null;
			else
				this.Mult = rs.getString("Mult").trim();

			if( rs.getString("Prem") == null )
				this.Prem = null;
			else
				this.Prem = rs.getString("Prem").trim();

			if( rs.getString("Amnt") == null )
				this.Amnt = null;
			else
				this.Amnt = rs.getString("Amnt").trim();

			if( rs.getString("CalRule") == null )
				this.CalRule = null;
			else
				this.CalRule = rs.getString("CalRule").trim();

			if( rs.getString("FloatRate") == null )
				this.FloatRate = null;
			else
				this.FloatRate = rs.getString("FloatRate").trim();

			if( rs.getString("GetLimit") == null )
				this.GetLimit = null;
			else
				this.GetLimit = rs.getString("GetLimit").trim();

			if( rs.getString("GetRate") == null )
				this.GetRate = null;
			else
				this.GetRate = rs.getString("GetRate").trim();

			if( rs.getString("HealthCheckFlag") == null )
				this.HealthCheckFlag = null;
			else
				this.HealthCheckFlag = rs.getString("HealthCheckFlag").trim();

			if( rs.getString("OutPayFlag") == null )
				this.OutPayFlag = null;
			else
				this.OutPayFlag = rs.getString("OutPayFlag").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentName") == null )
				this.AgentName = null;
			else
				this.AgentName = rs.getString("AgentName").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("BankWorkSite") == null )
				this.BankWorkSite = null;
			else
				this.BankWorkSite = rs.getString("BankWorkSite").trim();

			if( rs.getString("StandbyFlag1") == null )
				this.StandbyFlag1 = null;
			else
				this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

			if( rs.getString("StandbyFlag2") == null )
				this.StandbyFlag2 = null;
			else
				this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

			if( rs.getString("StandbyFlag3") == null )
				this.StandbyFlag3 = null;
			else
				this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("CInValiDate") == null )
				this.CInValiDate = null;
			else
				this.CInValiDate = rs.getString("CInValiDate").trim();

			if( rs.getString("Copys") == null )
				this.Copys = null;
			else
				this.Copys = rs.getString("Copys").trim();

			if( rs.getString("FirstTrialDate") == null )
				this.FirstTrialDate = null;
			else
				this.FirstTrialDate = rs.getString("FirstTrialDate").trim();

			this.SupplementaryPrem = rs.getDouble("SupplementaryPrem");
			if( rs.getString("ExPayMode") == null )
				this.ExPayMode = null;
			else
				this.ExPayMode = rs.getString("ExPayMode").trim();

			if( rs.getString("Crs_SaleChnl") == null )
				this.Crs_SaleChnl = null;
			else
				this.Crs_SaleChnl = rs.getString("Crs_SaleChnl").trim();

			if( rs.getString("Crs_BussType") == null )
				this.Crs_BussType = null;
			else
				this.Crs_BussType = rs.getString("Crs_BussType").trim();

			if( rs.getString("GrpAgentCom") == null )
				this.GrpAgentCom = null;
			else
				this.GrpAgentCom = rs.getString("GrpAgentCom").trim();

			if( rs.getString("GrpAgentCode") == null )
				this.GrpAgentCode = null;
			else
				this.GrpAgentCode = rs.getString("GrpAgentCode").trim();

			if( rs.getString("GrpAgentName") == null )
				this.GrpAgentName = null;
			else
				this.GrpAgentName = rs.getString("GrpAgentName").trim();

			if( rs.getString("GrpAgentIDNo") == null )
				this.GrpAgentIDNo = null;
			else
				this.GrpAgentIDNo = rs.getString("GrpAgentIDNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的BPOLCPol表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOLCPolSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public BPOLCPolSchema getSchema()
	{
		BPOLCPolSchema aBPOLCPolSchema = new BPOLCPolSchema();
		aBPOLCPolSchema.setSchema(this);
		return aBPOLCPolSchema;
	}

	public BPOLCPolDB getDB()
	{
		BPOLCPolDB aDBOper = new BPOLCPolDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOLCPol描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BPOBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FamilyType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainPolID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelationToAppnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelationToMainInsured)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RelaId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolApplyDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstTrialOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstTrialTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiveOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiveDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiveTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LiveGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DeadGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayIntv)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayEndYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayEndYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetStartType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BonusGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremToAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mult)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Prem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Amnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalRule)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FloatRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetLimit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HealthCheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OutPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankWorkSite)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CInValiDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Copys)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstTrialDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SupplementaryPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExPayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_BussType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentIDNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOLCPol>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BPOBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PolID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ContID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			FamilyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			MainPolID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			InsuredID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RelationToAppnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			RelationToMainInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AppntID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			RelaId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			PolApplyDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			CValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			FirstTrialOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			FirstTrialTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ReceiveOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ReceiveDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ReceiveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			LiveGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			DeadGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			PayIntv = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			InsuYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			PayEndYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			PayEndYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			GetYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			GetYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			GetStartType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			BonusGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			PremToAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			Mult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			Prem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			Amnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			CalRule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			FloatRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			GetLimit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			GetRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			HealthCheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			OutPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			BankWorkSite = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			CInValiDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			Copys = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			FirstTrialDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			SupplementaryPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,61,SysConst.PACKAGESPILTER))).doubleValue();
			ExPayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			Crs_SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
			Crs_BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			GrpAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			GrpAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
			GrpAgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
			GrpAgentIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOLCPolSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BPOBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BPOBatchNo));
		}
		if (FCode.equals("PolID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolID));
		}
		if (FCode.equals("ContID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("FamilyType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyType));
		}
		if (FCode.equals("MainPolID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolID));
		}
		if (FCode.equals("InsuredID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredID));
		}
		if (FCode.equals("RelationToAppnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToAppnt));
		}
		if (FCode.equals("RelationToMainInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelationToMainInsured));
		}
		if (FCode.equals("AppntID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntID));
		}
		if (FCode.equals("RelaId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaId));
		}
		if (FCode.equals("PolApplyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolApplyDate));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CValiDate));
		}
		if (FCode.equals("FirstTrialOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialOperator));
		}
		if (FCode.equals("FirstTrialTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialTime));
		}
		if (FCode.equals("ReceiveOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveOperator));
		}
		if (FCode.equals("ReceiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveDate));
		}
		if (FCode.equals("ReceiveTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
		}
		if (FCode.equals("TempFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("LiveGetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LiveGetMode));
		}
		if (FCode.equals("DeadGetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeadGetMode));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("InsuYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
		}
		if (FCode.equals("InsuYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
		}
		if (FCode.equals("PayEndYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYear));
		}
		if (FCode.equals("PayEndYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayEndYearFlag));
		}
		if (FCode.equals("GetYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetYear));
		}
		if (FCode.equals("GetYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetYearFlag));
		}
		if (FCode.equals("GetStartType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetStartType));
		}
		if (FCode.equals("GetDutyKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
		}
		if (FCode.equals("BonusGetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BonusGetMode));
		}
		if (FCode.equals("PremToAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremToAmnt));
		}
		if (FCode.equals("Mult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("CalRule"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalRule));
		}
		if (FCode.equals("FloatRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FloatRate));
		}
		if (FCode.equals("GetLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
		}
		if (FCode.equals("GetRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetRate));
		}
		if (FCode.equals("HealthCheckFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HealthCheckFlag));
		}
		if (FCode.equals("OutPayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutPayFlag));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("BankWorkSite"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankWorkSite));
		}
		if (FCode.equals("StandbyFlag1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
		}
		if (FCode.equals("StandbyFlag2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
		}
		if (FCode.equals("StandbyFlag3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("CInValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CInValiDate));
		}
		if (FCode.equals("Copys"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Copys));
		}
		if (FCode.equals("FirstTrialDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialDate));
		}
		if (FCode.equals("SupplementaryPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SupplementaryPrem));
		}
		if (FCode.equals("ExPayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExPayMode));
		}
		if (FCode.equals("Crs_SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_SaleChnl));
		}
		if (FCode.equals("Crs_BussType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_BussType));
		}
		if (FCode.equals("GrpAgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCom));
		}
		if (FCode.equals("GrpAgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCode));
		}
		if (FCode.equals("GrpAgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentName));
		}
		if (FCode.equals("GrpAgentIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentIDNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BPOBatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PolID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ContID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(FamilyType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(MainPolID);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(InsuredID);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RelationToAppnt);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(RelationToMainInsured);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AppntID);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(RelaId);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(PolApplyDate);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(CValiDate);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(FirstTrialOperator);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(FirstTrialTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ReceiveOperator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ReceiveDate);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ReceiveTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(LiveGetMode);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(DeadGetMode);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(PayIntv);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(InsuYear);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(PayEndYear);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(PayEndYearFlag);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(GetYear);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(GetYearFlag);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(GetStartType);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(BonusGetMode);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(PremToAmnt);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(Mult);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(Prem);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(Amnt);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(CalRule);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(FloatRate);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(GetLimit);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(GetRate);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(HealthCheckFlag);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(OutPayFlag);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(AgentName);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(BankWorkSite);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(CInValiDate);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(Copys);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(FirstTrialDate);
				break;
			case 60:
				strFieldValue = String.valueOf(SupplementaryPrem);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(ExPayMode);
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(Crs_SaleChnl);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(Crs_BussType);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCom);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCode);
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentName);
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentIDNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BPOBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BPOBatchNo = FValue.trim();
			}
			else
				BPOBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("PolID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolID = FValue.trim();
			}
			else
				PolID = null;
		}
		if (FCode.equalsIgnoreCase("ContID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContID = FValue.trim();
			}
			else
				ContID = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("FamilyType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FamilyType = FValue.trim();
			}
			else
				FamilyType = null;
		}
		if (FCode.equalsIgnoreCase("MainPolID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainPolID = FValue.trim();
			}
			else
				MainPolID = null;
		}
		if (FCode.equalsIgnoreCase("InsuredID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredID = FValue.trim();
			}
			else
				InsuredID = null;
		}
		if (FCode.equalsIgnoreCase("RelationToAppnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationToAppnt = FValue.trim();
			}
			else
				RelationToAppnt = null;
		}
		if (FCode.equalsIgnoreCase("RelationToMainInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelationToMainInsured = FValue.trim();
			}
			else
				RelationToMainInsured = null;
		}
		if (FCode.equalsIgnoreCase("AppntID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntID = FValue.trim();
			}
			else
				AppntID = null;
		}
		if (FCode.equalsIgnoreCase("RelaId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RelaId = FValue.trim();
			}
			else
				RelaId = null;
		}
		if (FCode.equalsIgnoreCase("PolApplyDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolApplyDate = FValue.trim();
			}
			else
				PolApplyDate = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CValiDate = FValue.trim();
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("FirstTrialOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTrialOperator = FValue.trim();
			}
			else
				FirstTrialOperator = null;
		}
		if (FCode.equalsIgnoreCase("FirstTrialTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTrialTime = FValue.trim();
			}
			else
				FirstTrialTime = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveOperator = FValue.trim();
			}
			else
				ReceiveOperator = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveDate = FValue.trim();
			}
			else
				ReceiveDate = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveTime = FValue.trim();
			}
			else
				ReceiveTime = null;
		}
		if (FCode.equalsIgnoreCase("TempFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempFeeNo = FValue.trim();
			}
			else
				TempFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("LiveGetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LiveGetMode = FValue.trim();
			}
			else
				LiveGetMode = null;
		}
		if (FCode.equalsIgnoreCase("DeadGetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DeadGetMode = FValue.trim();
			}
			else
				DeadGetMode = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayIntv = FValue.trim();
			}
			else
				PayIntv = null;
		}
		if (FCode.equalsIgnoreCase("InsuYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuYear = FValue.trim();
			}
			else
				InsuYear = null;
		}
		if (FCode.equalsIgnoreCase("InsuYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuYearFlag = FValue.trim();
			}
			else
				InsuYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayEndYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayEndYear = FValue.trim();
			}
			else
				PayEndYear = null;
		}
		if (FCode.equalsIgnoreCase("PayEndYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayEndYearFlag = FValue.trim();
			}
			else
				PayEndYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("GetYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetYear = FValue.trim();
			}
			else
				GetYear = null;
		}
		if (FCode.equalsIgnoreCase("GetYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetYearFlag = FValue.trim();
			}
			else
				GetYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("GetStartType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetStartType = FValue.trim();
			}
			else
				GetStartType = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyKind = FValue.trim();
			}
			else
				GetDutyKind = null;
		}
		if (FCode.equalsIgnoreCase("BonusGetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BonusGetMode = FValue.trim();
			}
			else
				BonusGetMode = null;
		}
		if (FCode.equalsIgnoreCase("PremToAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremToAmnt = FValue.trim();
			}
			else
				PremToAmnt = null;
		}
		if (FCode.equalsIgnoreCase("Mult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mult = FValue.trim();
			}
			else
				Mult = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Prem = FValue.trim();
			}
			else
				Prem = null;
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Amnt = FValue.trim();
			}
			else
				Amnt = null;
		}
		if (FCode.equalsIgnoreCase("CalRule"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalRule = FValue.trim();
			}
			else
				CalRule = null;
		}
		if (FCode.equalsIgnoreCase("FloatRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FloatRate = FValue.trim();
			}
			else
				FloatRate = null;
		}
		if (FCode.equalsIgnoreCase("GetLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetLimit = FValue.trim();
			}
			else
				GetLimit = null;
		}
		if (FCode.equalsIgnoreCase("GetRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetRate = FValue.trim();
			}
			else
				GetRate = null;
		}
		if (FCode.equalsIgnoreCase("HealthCheckFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HealthCheckFlag = FValue.trim();
			}
			else
				HealthCheckFlag = null;
		}
		if (FCode.equalsIgnoreCase("OutPayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OutPayFlag = FValue.trim();
			}
			else
				OutPayFlag = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentName = FValue.trim();
			}
			else
				AgentName = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("BankWorkSite"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankWorkSite = FValue.trim();
			}
			else
				BankWorkSite = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag1 = FValue.trim();
			}
			else
				StandbyFlag1 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag2 = FValue.trim();
			}
			else
				StandbyFlag2 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag3 = FValue.trim();
			}
			else
				StandbyFlag3 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("CInValiDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CInValiDate = FValue.trim();
			}
			else
				CInValiDate = null;
		}
		if (FCode.equalsIgnoreCase("Copys"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Copys = FValue.trim();
			}
			else
				Copys = null;
		}
		if (FCode.equalsIgnoreCase("FirstTrialDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTrialDate = FValue.trim();
			}
			else
				FirstTrialDate = null;
		}
		if (FCode.equalsIgnoreCase("SupplementaryPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SupplementaryPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExPayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExPayMode = FValue.trim();
			}
			else
				ExPayMode = null;
		}
		if (FCode.equalsIgnoreCase("Crs_SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_SaleChnl = FValue.trim();
			}
			else
				Crs_SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("Crs_BussType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_BussType = FValue.trim();
			}
			else
				Crs_BussType = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCom = FValue.trim();
			}
			else
				GrpAgentCom = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCode = FValue.trim();
			}
			else
				GrpAgentCode = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentName = FValue.trim();
			}
			else
				GrpAgentName = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentIDNo = FValue.trim();
			}
			else
				GrpAgentIDNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		BPOLCPolSchema other = (BPOLCPolSchema)otherObject;
		return
			(BPOBatchNo == null ? other.getBPOBatchNo() == null : BPOBatchNo.equals(other.getBPOBatchNo()))
			&& (PolID == null ? other.getPolID() == null : PolID.equals(other.getPolID()))
			&& (ContID == null ? other.getContID() == null : ContID.equals(other.getContID()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (FamilyType == null ? other.getFamilyType() == null : FamilyType.equals(other.getFamilyType()))
			&& (MainPolID == null ? other.getMainPolID() == null : MainPolID.equals(other.getMainPolID()))
			&& (InsuredID == null ? other.getInsuredID() == null : InsuredID.equals(other.getInsuredID()))
			&& (RelationToAppnt == null ? other.getRelationToAppnt() == null : RelationToAppnt.equals(other.getRelationToAppnt()))
			&& (RelationToMainInsured == null ? other.getRelationToMainInsured() == null : RelationToMainInsured.equals(other.getRelationToMainInsured()))
			&& (AppntID == null ? other.getAppntID() == null : AppntID.equals(other.getAppntID()))
			&& (RelaId == null ? other.getRelaId() == null : RelaId.equals(other.getRelaId()))
			&& (PolApplyDate == null ? other.getPolApplyDate() == null : PolApplyDate.equals(other.getPolApplyDate()))
			&& (CValiDate == null ? other.getCValiDate() == null : CValiDate.equals(other.getCValiDate()))
			&& (FirstTrialOperator == null ? other.getFirstTrialOperator() == null : FirstTrialOperator.equals(other.getFirstTrialOperator()))
			&& (FirstTrialTime == null ? other.getFirstTrialTime() == null : FirstTrialTime.equals(other.getFirstTrialTime()))
			&& (ReceiveOperator == null ? other.getReceiveOperator() == null : ReceiveOperator.equals(other.getReceiveOperator()))
			&& (ReceiveDate == null ? other.getReceiveDate() == null : ReceiveDate.equals(other.getReceiveDate()))
			&& (ReceiveTime == null ? other.getReceiveTime() == null : ReceiveTime.equals(other.getReceiveTime()))
			&& (TempFeeNo == null ? other.getTempFeeNo() == null : TempFeeNo.equals(other.getTempFeeNo()))
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (LiveGetMode == null ? other.getLiveGetMode() == null : LiveGetMode.equals(other.getLiveGetMode()))
			&& (DeadGetMode == null ? other.getDeadGetMode() == null : DeadGetMode.equals(other.getDeadGetMode()))
			&& (PayIntv == null ? other.getPayIntv() == null : PayIntv.equals(other.getPayIntv()))
			&& (InsuYear == null ? other.getInsuYear() == null : InsuYear.equals(other.getInsuYear()))
			&& (InsuYearFlag == null ? other.getInsuYearFlag() == null : InsuYearFlag.equals(other.getInsuYearFlag()))
			&& (PayEndYear == null ? other.getPayEndYear() == null : PayEndYear.equals(other.getPayEndYear()))
			&& (PayEndYearFlag == null ? other.getPayEndYearFlag() == null : PayEndYearFlag.equals(other.getPayEndYearFlag()))
			&& (GetYear == null ? other.getGetYear() == null : GetYear.equals(other.getGetYear()))
			&& (GetYearFlag == null ? other.getGetYearFlag() == null : GetYearFlag.equals(other.getGetYearFlag()))
			&& (GetStartType == null ? other.getGetStartType() == null : GetStartType.equals(other.getGetStartType()))
			&& (GetDutyKind == null ? other.getGetDutyKind() == null : GetDutyKind.equals(other.getGetDutyKind()))
			&& (BonusGetMode == null ? other.getBonusGetMode() == null : BonusGetMode.equals(other.getBonusGetMode()))
			&& (PremToAmnt == null ? other.getPremToAmnt() == null : PremToAmnt.equals(other.getPremToAmnt()))
			&& (Mult == null ? other.getMult() == null : Mult.equals(other.getMult()))
			&& (Prem == null ? other.getPrem() == null : Prem.equals(other.getPrem()))
			&& (Amnt == null ? other.getAmnt() == null : Amnt.equals(other.getAmnt()))
			&& (CalRule == null ? other.getCalRule() == null : CalRule.equals(other.getCalRule()))
			&& (FloatRate == null ? other.getFloatRate() == null : FloatRate.equals(other.getFloatRate()))
			&& (GetLimit == null ? other.getGetLimit() == null : GetLimit.equals(other.getGetLimit()))
			&& (GetRate == null ? other.getGetRate() == null : GetRate.equals(other.getGetRate()))
			&& (HealthCheckFlag == null ? other.getHealthCheckFlag() == null : HealthCheckFlag.equals(other.getHealthCheckFlag()))
			&& (OutPayFlag == null ? other.getOutPayFlag() == null : OutPayFlag.equals(other.getOutPayFlag()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentName == null ? other.getAgentName() == null : AgentName.equals(other.getAgentName()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (BankWorkSite == null ? other.getBankWorkSite() == null : BankWorkSite.equals(other.getBankWorkSite()))
			&& (StandbyFlag1 == null ? other.getStandbyFlag1() == null : StandbyFlag1.equals(other.getStandbyFlag1()))
			&& (StandbyFlag2 == null ? other.getStandbyFlag2() == null : StandbyFlag2.equals(other.getStandbyFlag2()))
			&& (StandbyFlag3 == null ? other.getStandbyFlag3() == null : StandbyFlag3.equals(other.getStandbyFlag3()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (CInValiDate == null ? other.getCInValiDate() == null : CInValiDate.equals(other.getCInValiDate()))
			&& (Copys == null ? other.getCopys() == null : Copys.equals(other.getCopys()))
			&& (FirstTrialDate == null ? other.getFirstTrialDate() == null : FirstTrialDate.equals(other.getFirstTrialDate()))
			&& SupplementaryPrem == other.getSupplementaryPrem()
			&& (ExPayMode == null ? other.getExPayMode() == null : ExPayMode.equals(other.getExPayMode()))
			&& (Crs_SaleChnl == null ? other.getCrs_SaleChnl() == null : Crs_SaleChnl.equals(other.getCrs_SaleChnl()))
			&& (Crs_BussType == null ? other.getCrs_BussType() == null : Crs_BussType.equals(other.getCrs_BussType()))
			&& (GrpAgentCom == null ? other.getGrpAgentCom() == null : GrpAgentCom.equals(other.getGrpAgentCom()))
			&& (GrpAgentCode == null ? other.getGrpAgentCode() == null : GrpAgentCode.equals(other.getGrpAgentCode()))
			&& (GrpAgentName == null ? other.getGrpAgentName() == null : GrpAgentName.equals(other.getGrpAgentName()))
			&& (GrpAgentIDNo == null ? other.getGrpAgentIDNo() == null : GrpAgentIDNo.equals(other.getGrpAgentIDNo()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BPOBatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("PolID") ) {
			return 1;
		}
		if( strFieldName.equals("ContID") ) {
			return 2;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 3;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 4;
		}
		if( strFieldName.equals("FamilyType") ) {
			return 5;
		}
		if( strFieldName.equals("MainPolID") ) {
			return 6;
		}
		if( strFieldName.equals("InsuredID") ) {
			return 7;
		}
		if( strFieldName.equals("RelationToAppnt") ) {
			return 8;
		}
		if( strFieldName.equals("RelationToMainInsured") ) {
			return 9;
		}
		if( strFieldName.equals("AppntID") ) {
			return 10;
		}
		if( strFieldName.equals("RelaId") ) {
			return 11;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return 12;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 13;
		}
		if( strFieldName.equals("FirstTrialOperator") ) {
			return 14;
		}
		if( strFieldName.equals("FirstTrialTime") ) {
			return 15;
		}
		if( strFieldName.equals("ReceiveOperator") ) {
			return 16;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return 17;
		}
		if( strFieldName.equals("ReceiveTime") ) {
			return 18;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return 19;
		}
		if( strFieldName.equals("PayMode") ) {
			return 20;
		}
		if( strFieldName.equals("LiveGetMode") ) {
			return 21;
		}
		if( strFieldName.equals("DeadGetMode") ) {
			return 22;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 23;
		}
		if( strFieldName.equals("InsuYear") ) {
			return 24;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return 25;
		}
		if( strFieldName.equals("PayEndYear") ) {
			return 26;
		}
		if( strFieldName.equals("PayEndYearFlag") ) {
			return 27;
		}
		if( strFieldName.equals("GetYear") ) {
			return 28;
		}
		if( strFieldName.equals("GetYearFlag") ) {
			return 29;
		}
		if( strFieldName.equals("GetStartType") ) {
			return 30;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return 31;
		}
		if( strFieldName.equals("BonusGetMode") ) {
			return 32;
		}
		if( strFieldName.equals("PremToAmnt") ) {
			return 33;
		}
		if( strFieldName.equals("Mult") ) {
			return 34;
		}
		if( strFieldName.equals("Prem") ) {
			return 35;
		}
		if( strFieldName.equals("Amnt") ) {
			return 36;
		}
		if( strFieldName.equals("CalRule") ) {
			return 37;
		}
		if( strFieldName.equals("FloatRate") ) {
			return 38;
		}
		if( strFieldName.equals("GetLimit") ) {
			return 39;
		}
		if( strFieldName.equals("GetRate") ) {
			return 40;
		}
		if( strFieldName.equals("HealthCheckFlag") ) {
			return 41;
		}
		if( strFieldName.equals("OutPayFlag") ) {
			return 42;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 43;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 44;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 45;
		}
		if( strFieldName.equals("AgentName") ) {
			return 46;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 47;
		}
		if( strFieldName.equals("BankWorkSite") ) {
			return 48;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return 49;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return 50;
		}
		if( strFieldName.equals("StandbyFlag3") ) {
			return 51;
		}
		if( strFieldName.equals("Operator") ) {
			return 52;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 53;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 54;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 55;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 56;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return 57;
		}
		if( strFieldName.equals("Copys") ) {
			return 58;
		}
		if( strFieldName.equals("FirstTrialDate") ) {
			return 59;
		}
		if( strFieldName.equals("SupplementaryPrem") ) {
			return 60;
		}
		if( strFieldName.equals("ExPayMode") ) {
			return 61;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return 62;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return 63;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return 64;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return 65;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return 66;
		}
		if( strFieldName.equals("GrpAgentIDNo") ) {
			return 67;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BPOBatchNo";
				break;
			case 1:
				strFieldName = "PolID";
				break;
			case 2:
				strFieldName = "ContID";
				break;
			case 3:
				strFieldName = "PrtNo";
				break;
			case 4:
				strFieldName = "RiskCode";
				break;
			case 5:
				strFieldName = "FamilyType";
				break;
			case 6:
				strFieldName = "MainPolID";
				break;
			case 7:
				strFieldName = "InsuredID";
				break;
			case 8:
				strFieldName = "RelationToAppnt";
				break;
			case 9:
				strFieldName = "RelationToMainInsured";
				break;
			case 10:
				strFieldName = "AppntID";
				break;
			case 11:
				strFieldName = "RelaId";
				break;
			case 12:
				strFieldName = "PolApplyDate";
				break;
			case 13:
				strFieldName = "CValiDate";
				break;
			case 14:
				strFieldName = "FirstTrialOperator";
				break;
			case 15:
				strFieldName = "FirstTrialTime";
				break;
			case 16:
				strFieldName = "ReceiveOperator";
				break;
			case 17:
				strFieldName = "ReceiveDate";
				break;
			case 18:
				strFieldName = "ReceiveTime";
				break;
			case 19:
				strFieldName = "TempFeeNo";
				break;
			case 20:
				strFieldName = "PayMode";
				break;
			case 21:
				strFieldName = "LiveGetMode";
				break;
			case 22:
				strFieldName = "DeadGetMode";
				break;
			case 23:
				strFieldName = "PayIntv";
				break;
			case 24:
				strFieldName = "InsuYear";
				break;
			case 25:
				strFieldName = "InsuYearFlag";
				break;
			case 26:
				strFieldName = "PayEndYear";
				break;
			case 27:
				strFieldName = "PayEndYearFlag";
				break;
			case 28:
				strFieldName = "GetYear";
				break;
			case 29:
				strFieldName = "GetYearFlag";
				break;
			case 30:
				strFieldName = "GetStartType";
				break;
			case 31:
				strFieldName = "GetDutyKind";
				break;
			case 32:
				strFieldName = "BonusGetMode";
				break;
			case 33:
				strFieldName = "PremToAmnt";
				break;
			case 34:
				strFieldName = "Mult";
				break;
			case 35:
				strFieldName = "Prem";
				break;
			case 36:
				strFieldName = "Amnt";
				break;
			case 37:
				strFieldName = "CalRule";
				break;
			case 38:
				strFieldName = "FloatRate";
				break;
			case 39:
				strFieldName = "GetLimit";
				break;
			case 40:
				strFieldName = "GetRate";
				break;
			case 41:
				strFieldName = "HealthCheckFlag";
				break;
			case 42:
				strFieldName = "OutPayFlag";
				break;
			case 43:
				strFieldName = "ManageCom";
				break;
			case 44:
				strFieldName = "SaleChnl";
				break;
			case 45:
				strFieldName = "AgentCode";
				break;
			case 46:
				strFieldName = "AgentName";
				break;
			case 47:
				strFieldName = "AgentCom";
				break;
			case 48:
				strFieldName = "BankWorkSite";
				break;
			case 49:
				strFieldName = "StandbyFlag1";
				break;
			case 50:
				strFieldName = "StandbyFlag2";
				break;
			case 51:
				strFieldName = "StandbyFlag3";
				break;
			case 52:
				strFieldName = "Operator";
				break;
			case 53:
				strFieldName = "MakeDate";
				break;
			case 54:
				strFieldName = "MakeTime";
				break;
			case 55:
				strFieldName = "ModifyDate";
				break;
			case 56:
				strFieldName = "ModifyTime";
				break;
			case 57:
				strFieldName = "CInValiDate";
				break;
			case 58:
				strFieldName = "Copys";
				break;
			case 59:
				strFieldName = "FirstTrialDate";
				break;
			case 60:
				strFieldName = "SupplementaryPrem";
				break;
			case 61:
				strFieldName = "ExPayMode";
				break;
			case 62:
				strFieldName = "Crs_SaleChnl";
				break;
			case 63:
				strFieldName = "Crs_BussType";
				break;
			case 64:
				strFieldName = "GrpAgentCom";
				break;
			case 65:
				strFieldName = "GrpAgentCode";
				break;
			case 66:
				strFieldName = "GrpAgentName";
				break;
			case 67:
				strFieldName = "GrpAgentIDNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BPOBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FamilyType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainPolID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelationToAppnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelationToMainInsured") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RelaId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstTrialOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstTrialTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LiveGetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeadGetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayEndYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayEndYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetStartType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BonusGetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremToAmnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mult") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalRule") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FloatRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetLimit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HealthCheckFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OutPayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankWorkSite") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Copys") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstTrialDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SupplementaryPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExPayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentIDNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 66:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 67:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
