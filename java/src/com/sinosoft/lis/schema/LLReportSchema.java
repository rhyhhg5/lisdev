/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLReportDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLReportSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 报案信息
 * @CreateDate：2005-04-22
 */
public class LLReportSchema implements Schema, Cloneable
{
    // @Field
    /** 报案号 */
    private String RptNo;
    /** 报案方式 */
    private String RptMode;
    /** 报案人与被保人关系 */
    private String Relation;
    /** 报案人姓名 */
    private String RptorName;
    /** 报案人通讯地址 */
    private String RptorAddress;
    /** 报案人电话 */
    private String RptorPhone;
    /** 报案人手机 */
    private String RptorMobile;
    /** 报案人电邮 */
    private String Email;
    /** 邮政编码 */
    private String PostCode;
    /** 回执发送方式 */
    private String ReturnMode;
    /** 出险日期 */
    private Date AccidentDate;
    /** 出险地点 */
    private String AccidentSite;
    /** 出险原因 */
    private String AccidentReason;
    /** 出险过程和结果 */
    private String AccidentCourse;
    /** 报案日期 */
    private Date RptDate;
    /** 报备产生日期 */
    private Date CaseNoDate;
    /** 报备结束日期 */
    private Date CaseEndDate;
    /** 报案撤销原因 */
    private String RgtReason;
    /** 不立案原因 */
    private String NoRgtReason;
    /** 备注 */
    private String Remark;
    /** 立案标志 */
    private String RgtFlag;
    /** 案件有效标志 */
    private String AvaiFlag;
    /** 案件有效原因 */
    private String AvaliReason;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 29; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLReportSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "RptNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLReportSchema cloned = (LLReportSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRptNo()
    {
        if (SysConst.CHANGECHARSET && RptNo != null && !RptNo.equals(""))
        {
            RptNo = StrTool.unicodeToGBK(RptNo);
        }
        return RptNo;
    }

    public void setRptNo(String aRptNo)
    {
        RptNo = aRptNo;
    }

    public String getRptMode()
    {
        if (SysConst.CHANGECHARSET && RptMode != null && !RptMode.equals(""))
        {
            RptMode = StrTool.unicodeToGBK(RptMode);
        }
        return RptMode;
    }

    public void setRptMode(String aRptMode)
    {
        RptMode = aRptMode;
    }

    public String getRelation()
    {
        if (SysConst.CHANGECHARSET && Relation != null && !Relation.equals(""))
        {
            Relation = StrTool.unicodeToGBK(Relation);
        }
        return Relation;
    }

    public void setRelation(String aRelation)
    {
        Relation = aRelation;
    }

    public String getRptorName()
    {
        if (SysConst.CHANGECHARSET && RptorName != null && !RptorName.equals(""))
        {
            RptorName = StrTool.unicodeToGBK(RptorName);
        }
        return RptorName;
    }

    public void setRptorName(String aRptorName)
    {
        RptorName = aRptorName;
    }

    public String getRptorAddress()
    {
        if (SysConst.CHANGECHARSET && RptorAddress != null &&
            !RptorAddress.equals(""))
        {
            RptorAddress = StrTool.unicodeToGBK(RptorAddress);
        }
        return RptorAddress;
    }

    public void setRptorAddress(String aRptorAddress)
    {
        RptorAddress = aRptorAddress;
    }

    public String getRptorPhone()
    {
        if (SysConst.CHANGECHARSET && RptorPhone != null &&
            !RptorPhone.equals(""))
        {
            RptorPhone = StrTool.unicodeToGBK(RptorPhone);
        }
        return RptorPhone;
    }

    public void setRptorPhone(String aRptorPhone)
    {
        RptorPhone = aRptorPhone;
    }

    public String getRptorMobile()
    {
        if (SysConst.CHANGECHARSET && RptorMobile != null &&
            !RptorMobile.equals(""))
        {
            RptorMobile = StrTool.unicodeToGBK(RptorMobile);
        }
        return RptorMobile;
    }

    public void setRptorMobile(String aRptorMobile)
    {
        RptorMobile = aRptorMobile;
    }

    public String getEmail()
    {
        if (SysConst.CHANGECHARSET && Email != null && !Email.equals(""))
        {
            Email = StrTool.unicodeToGBK(Email);
        }
        return Email;
    }

    public void setEmail(String aEmail)
    {
        Email = aEmail;
    }

    public String getPostCode()
    {
        if (SysConst.CHANGECHARSET && PostCode != null && !PostCode.equals(""))
        {
            PostCode = StrTool.unicodeToGBK(PostCode);
        }
        return PostCode;
    }

    public void setPostCode(String aPostCode)
    {
        PostCode = aPostCode;
    }

    public String getReturnMode()
    {
        if (SysConst.CHANGECHARSET && ReturnMode != null &&
            !ReturnMode.equals(""))
        {
            ReturnMode = StrTool.unicodeToGBK(ReturnMode);
        }
        return ReturnMode;
    }

    public void setReturnMode(String aReturnMode)
    {
        ReturnMode = aReturnMode;
    }

    public String getAccidentDate()
    {
        if (AccidentDate != null)
        {
            return fDate.getString(AccidentDate);
        }
        else
        {
            return null;
        }
    }

    public void setAccidentDate(Date aAccidentDate)
    {
        AccidentDate = aAccidentDate;
    }

    public void setAccidentDate(String aAccidentDate)
    {
        if (aAccidentDate != null && !aAccidentDate.equals(""))
        {
            AccidentDate = fDate.getDate(aAccidentDate);
        }
        else
        {
            AccidentDate = null;
        }
    }

    public String getAccidentSite()
    {
        if (SysConst.CHANGECHARSET && AccidentSite != null &&
            !AccidentSite.equals(""))
        {
            AccidentSite = StrTool.unicodeToGBK(AccidentSite);
        }
        return AccidentSite;
    }

    public void setAccidentSite(String aAccidentSite)
    {
        AccidentSite = aAccidentSite;
    }

    public String getAccidentReason()
    {
        if (SysConst.CHANGECHARSET && AccidentReason != null &&
            !AccidentReason.equals(""))
        {
            AccidentReason = StrTool.unicodeToGBK(AccidentReason);
        }
        return AccidentReason;
    }

    public void setAccidentReason(String aAccidentReason)
    {
        AccidentReason = aAccidentReason;
    }

    public String getAccidentCourse()
    {
        if (SysConst.CHANGECHARSET && AccidentCourse != null &&
            !AccidentCourse.equals(""))
        {
            AccidentCourse = StrTool.unicodeToGBK(AccidentCourse);
        }
        return AccidentCourse;
    }

    public void setAccidentCourse(String aAccidentCourse)
    {
        AccidentCourse = aAccidentCourse;
    }

    public String getRptDate()
    {
        if (RptDate != null)
        {
            return fDate.getString(RptDate);
        }
        else
        {
            return null;
        }
    }

    public void setRptDate(Date aRptDate)
    {
        RptDate = aRptDate;
    }

    public void setRptDate(String aRptDate)
    {
        if (aRptDate != null && !aRptDate.equals(""))
        {
            RptDate = fDate.getDate(aRptDate);
        }
        else
        {
            RptDate = null;
        }
    }

    public String getCaseNoDate()
    {
        if (CaseNoDate != null)
        {
            return fDate.getString(CaseNoDate);
        }
        else
        {
            return null;
        }
    }

    public void setCaseNoDate(Date aCaseNoDate)
    {
        CaseNoDate = aCaseNoDate;
    }

    public void setCaseNoDate(String aCaseNoDate)
    {
        if (aCaseNoDate != null && !aCaseNoDate.equals(""))
        {
            CaseNoDate = fDate.getDate(aCaseNoDate);
        }
        else
        {
            CaseNoDate = null;
        }
    }

    public String getCaseEndDate()
    {
        if (CaseEndDate != null)
        {
            return fDate.getString(CaseEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setCaseEndDate(Date aCaseEndDate)
    {
        CaseEndDate = aCaseEndDate;
    }

    public void setCaseEndDate(String aCaseEndDate)
    {
        if (aCaseEndDate != null && !aCaseEndDate.equals(""))
        {
            CaseEndDate = fDate.getDate(aCaseEndDate);
        }
        else
        {
            CaseEndDate = null;
        }
    }

    public String getRgtReason()
    {
        if (SysConst.CHANGECHARSET && RgtReason != null && !RgtReason.equals(""))
        {
            RgtReason = StrTool.unicodeToGBK(RgtReason);
        }
        return RgtReason;
    }

    public void setRgtReason(String aRgtReason)
    {
        RgtReason = aRgtReason;
    }

    public String getNoRgtReason()
    {
        if (SysConst.CHANGECHARSET && NoRgtReason != null &&
            !NoRgtReason.equals(""))
        {
            NoRgtReason = StrTool.unicodeToGBK(NoRgtReason);
        }
        return NoRgtReason;
    }

    public void setNoRgtReason(String aNoRgtReason)
    {
        NoRgtReason = aNoRgtReason;
    }

    public String getRemark()
    {
        if (SysConst.CHANGECHARSET && Remark != null && !Remark.equals(""))
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getRgtFlag()
    {
        if (SysConst.CHANGECHARSET && RgtFlag != null && !RgtFlag.equals(""))
        {
            RgtFlag = StrTool.unicodeToGBK(RgtFlag);
        }
        return RgtFlag;
    }

    public void setRgtFlag(String aRgtFlag)
    {
        RgtFlag = aRgtFlag;
    }

    public String getAvaiFlag()
    {
        if (SysConst.CHANGECHARSET && AvaiFlag != null && !AvaiFlag.equals(""))
        {
            AvaiFlag = StrTool.unicodeToGBK(AvaiFlag);
        }
        return AvaiFlag;
    }

    public void setAvaiFlag(String aAvaiFlag)
    {
        AvaiFlag = aAvaiFlag;
    }

    public String getAvaliReason()
    {
        if (SysConst.CHANGECHARSET && AvaliReason != null &&
            !AvaliReason.equals(""))
        {
            AvaliReason = StrTool.unicodeToGBK(AvaliReason);
        }
        return AvaliReason;
    }

    public void setAvaliReason(String aAvaliReason)
    {
        AvaliReason = aAvaliReason;
    }

    public String getMngCom()
    {
        if (SysConst.CHANGECHARSET && MngCom != null && !MngCom.equals(""))
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLReportSchema 对象给 Schema 赋值
     * @param: aLLReportSchema LLReportSchema
     **/
    public void setSchema(LLReportSchema aLLReportSchema)
    {
        this.RptNo = aLLReportSchema.getRptNo();
        this.RptMode = aLLReportSchema.getRptMode();
        this.Relation = aLLReportSchema.getRelation();
        this.RptorName = aLLReportSchema.getRptorName();
        this.RptorAddress = aLLReportSchema.getRptorAddress();
        this.RptorPhone = aLLReportSchema.getRptorPhone();
        this.RptorMobile = aLLReportSchema.getRptorMobile();
        this.Email = aLLReportSchema.getEmail();
        this.PostCode = aLLReportSchema.getPostCode();
        this.ReturnMode = aLLReportSchema.getReturnMode();
        this.AccidentDate = fDate.getDate(aLLReportSchema.getAccidentDate());
        this.AccidentSite = aLLReportSchema.getAccidentSite();
        this.AccidentReason = aLLReportSchema.getAccidentReason();
        this.AccidentCourse = aLLReportSchema.getAccidentCourse();
        this.RptDate = fDate.getDate(aLLReportSchema.getRptDate());
        this.CaseNoDate = fDate.getDate(aLLReportSchema.getCaseNoDate());
        this.CaseEndDate = fDate.getDate(aLLReportSchema.getCaseEndDate());
        this.RgtReason = aLLReportSchema.getRgtReason();
        this.NoRgtReason = aLLReportSchema.getNoRgtReason();
        this.Remark = aLLReportSchema.getRemark();
        this.RgtFlag = aLLReportSchema.getRgtFlag();
        this.AvaiFlag = aLLReportSchema.getAvaiFlag();
        this.AvaliReason = aLLReportSchema.getAvaliReason();
        this.MngCom = aLLReportSchema.getMngCom();
        this.Operator = aLLReportSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLReportSchema.getMakeDate());
        this.MakeTime = aLLReportSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLReportSchema.getModifyDate());
        this.ModifyTime = aLLReportSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RptNo") == null)
            {
                this.RptNo = null;
            }
            else
            {
                this.RptNo = rs.getString("RptNo").trim();
            }

            if (rs.getString("RptMode") == null)
            {
                this.RptMode = null;
            }
            else
            {
                this.RptMode = rs.getString("RptMode").trim();
            }

            if (rs.getString("Relation") == null)
            {
                this.Relation = null;
            }
            else
            {
                this.Relation = rs.getString("Relation").trim();
            }

            if (rs.getString("RptorName") == null)
            {
                this.RptorName = null;
            }
            else
            {
                this.RptorName = rs.getString("RptorName").trim();
            }

            if (rs.getString("RptorAddress") == null)
            {
                this.RptorAddress = null;
            }
            else
            {
                this.RptorAddress = rs.getString("RptorAddress").trim();
            }

            if (rs.getString("RptorPhone") == null)
            {
                this.RptorPhone = null;
            }
            else
            {
                this.RptorPhone = rs.getString("RptorPhone").trim();
            }

            if (rs.getString("RptorMobile") == null)
            {
                this.RptorMobile = null;
            }
            else
            {
                this.RptorMobile = rs.getString("RptorMobile").trim();
            }

            if (rs.getString("Email") == null)
            {
                this.Email = null;
            }
            else
            {
                this.Email = rs.getString("Email").trim();
            }

            if (rs.getString("PostCode") == null)
            {
                this.PostCode = null;
            }
            else
            {
                this.PostCode = rs.getString("PostCode").trim();
            }

            if (rs.getString("ReturnMode") == null)
            {
                this.ReturnMode = null;
            }
            else
            {
                this.ReturnMode = rs.getString("ReturnMode").trim();
            }

            this.AccidentDate = rs.getDate("AccidentDate");
            if (rs.getString("AccidentSite") == null)
            {
                this.AccidentSite = null;
            }
            else
            {
                this.AccidentSite = rs.getString("AccidentSite").trim();
            }

            if (rs.getString("AccidentReason") == null)
            {
                this.AccidentReason = null;
            }
            else
            {
                this.AccidentReason = rs.getString("AccidentReason").trim();
            }

            if (rs.getString("AccidentCourse") == null)
            {
                this.AccidentCourse = null;
            }
            else
            {
                this.AccidentCourse = rs.getString("AccidentCourse").trim();
            }

            this.RptDate = rs.getDate("RptDate");
            this.CaseNoDate = rs.getDate("CaseNoDate");
            this.CaseEndDate = rs.getDate("CaseEndDate");
            if (rs.getString("RgtReason") == null)
            {
                this.RgtReason = null;
            }
            else
            {
                this.RgtReason = rs.getString("RgtReason").trim();
            }

            if (rs.getString("NoRgtReason") == null)
            {
                this.NoRgtReason = null;
            }
            else
            {
                this.NoRgtReason = rs.getString("NoRgtReason").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("RgtFlag") == null)
            {
                this.RgtFlag = null;
            }
            else
            {
                this.RgtFlag = rs.getString("RgtFlag").trim();
            }

            if (rs.getString("AvaiFlag") == null)
            {
                this.AvaiFlag = null;
            }
            else
            {
                this.AvaiFlag = rs.getString("AvaiFlag").trim();
            }

            if (rs.getString("AvaliReason") == null)
            {
                this.AvaliReason = null;
            }
            else
            {
                this.AvaliReason = rs.getString("AvaliReason").trim();
            }

            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLReportSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLReportSchema getSchema()
    {
        LLReportSchema aLLReportSchema = new LLReportSchema();
        aLLReportSchema.setSchema(this);
        return aLLReportSchema;
    }

    public LLReportDB getDB()
    {
        LLReportDB aDBOper = new LLReportDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLReport描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RptNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RptMode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Relation)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RptorName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RptorAddress)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RptorPhone)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RptorMobile)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Email)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(PostCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ReturnMode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                AccidentDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AccidentSite)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AccidentReason)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AccidentCourse)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                RptDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                CaseNoDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                CaseEndDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RgtReason)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(NoRgtReason)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Remark)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RgtFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AvaiFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AvaliReason)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MngCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLReport>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            RptMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            Relation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RptorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            RptorAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            RptorPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            RptorMobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            Email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                   SysConst.PACKAGESPILTER);
            PostCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ReturnMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            AccidentDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            AccidentSite = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            AccidentReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            13, SysConst.PACKAGESPILTER);
            AccidentCourse = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            14, SysConst.PACKAGESPILTER);
            RptDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            CaseNoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            CaseEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            RgtReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                       SysConst.PACKAGESPILTER);
            NoRgtReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                         SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                    SysConst.PACKAGESPILTER);
            RgtFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                     SysConst.PACKAGESPILTER);
            AvaiFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            AvaliReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                         SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 26, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 28, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLReportSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RptNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptNo));
        }
        if (FCode.equals("RptMode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptMode));
        }
        if (FCode.equals("Relation"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
        }
        if (FCode.equals("RptorName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorName));
        }
        if (FCode.equals("RptorAddress"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorAddress));
        }
        if (FCode.equals("RptorPhone"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorPhone));
        }
        if (FCode.equals("RptorMobile"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RptorMobile));
        }
        if (FCode.equals("Email"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
        }
        if (FCode.equals("PostCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostCode));
        }
        if (FCode.equals("ReturnMode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnMode));
        }
        if (FCode.equals("AccidentDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getAccidentDate()));
        }
        if (FCode.equals("AccidentSite"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentSite));
        }
        if (FCode.equals("AccidentReason"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentReason));
        }
        if (FCode.equals("AccidentCourse"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentCourse));
        }
        if (FCode.equals("RptDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getRptDate()));
        }
        if (FCode.equals("CaseNoDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCaseNoDate()));
        }
        if (FCode.equals("CaseEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCaseEndDate()));
        }
        if (FCode.equals("RgtReason"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtReason));
        }
        if (FCode.equals("NoRgtReason"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NoRgtReason));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("RgtFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtFlag));
        }
        if (FCode.equals("AvaiFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AvaiFlag));
        }
        if (FCode.equals("AvaliReason"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AvaliReason));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RptNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RptMode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Relation);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RptorName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RptorAddress);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(RptorPhone);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(RptorMobile);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Email);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PostCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ReturnMode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getAccidentDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AccidentSite);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AccidentReason);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AccidentCourse);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getRptDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCaseNoDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCaseEndDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(RgtReason);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(NoRgtReason);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(RgtFlag);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(AvaiFlag);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(AvaliReason);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RptNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RptNo = FValue.trim();
            }
            else
            {
                RptNo = null;
            }
        }
        if (FCode.equals("RptMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RptMode = FValue.trim();
            }
            else
            {
                RptMode = null;
            }
        }
        if (FCode.equals("Relation"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Relation = FValue.trim();
            }
            else
            {
                Relation = null;
            }
        }
        if (FCode.equals("RptorName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RptorName = FValue.trim();
            }
            else
            {
                RptorName = null;
            }
        }
        if (FCode.equals("RptorAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RptorAddress = FValue.trim();
            }
            else
            {
                RptorAddress = null;
            }
        }
        if (FCode.equals("RptorPhone"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RptorPhone = FValue.trim();
            }
            else
            {
                RptorPhone = null;
            }
        }
        if (FCode.equals("RptorMobile"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RptorMobile = FValue.trim();
            }
            else
            {
                RptorMobile = null;
            }
        }
        if (FCode.equals("Email"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Email = FValue.trim();
            }
            else
            {
                Email = null;
            }
        }
        if (FCode.equals("PostCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PostCode = FValue.trim();
            }
            else
            {
                PostCode = null;
            }
        }
        if (FCode.equals("ReturnMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReturnMode = FValue.trim();
            }
            else
            {
                ReturnMode = null;
            }
        }
        if (FCode.equals("AccidentDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccidentDate = fDate.getDate(FValue);
            }
            else
            {
                AccidentDate = null;
            }
        }
        if (FCode.equals("AccidentSite"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccidentSite = FValue.trim();
            }
            else
            {
                AccidentSite = null;
            }
        }
        if (FCode.equals("AccidentReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccidentReason = FValue.trim();
            }
            else
            {
                AccidentReason = null;
            }
        }
        if (FCode.equals("AccidentCourse"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccidentCourse = FValue.trim();
            }
            else
            {
                AccidentCourse = null;
            }
        }
        if (FCode.equals("RptDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RptDate = fDate.getDate(FValue);
            }
            else
            {
                RptDate = null;
            }
        }
        if (FCode.equals("CaseNoDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNoDate = fDate.getDate(FValue);
            }
            else
            {
                CaseNoDate = null;
            }
        }
        if (FCode.equals("CaseEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseEndDate = fDate.getDate(FValue);
            }
            else
            {
                CaseEndDate = null;
            }
        }
        if (FCode.equals("RgtReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtReason = FValue.trim();
            }
            else
            {
                RgtReason = null;
            }
        }
        if (FCode.equals("NoRgtReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NoRgtReason = FValue.trim();
            }
            else
            {
                NoRgtReason = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("RgtFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtFlag = FValue.trim();
            }
            else
            {
                RgtFlag = null;
            }
        }
        if (FCode.equals("AvaiFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AvaiFlag = FValue.trim();
            }
            else
            {
                AvaiFlag = null;
            }
        }
        if (FCode.equals("AvaliReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AvaliReason = FValue.trim();
            }
            else
            {
                AvaliReason = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLReportSchema other = (LLReportSchema) otherObject;
        return
                RptNo.equals(other.getRptNo())
                && RptMode.equals(other.getRptMode())
                && Relation.equals(other.getRelation())
                && RptorName.equals(other.getRptorName())
                && RptorAddress.equals(other.getRptorAddress())
                && RptorPhone.equals(other.getRptorPhone())
                && RptorMobile.equals(other.getRptorMobile())
                && Email.equals(other.getEmail())
                && PostCode.equals(other.getPostCode())
                && ReturnMode.equals(other.getReturnMode())
                && fDate.getString(AccidentDate).equals(other.getAccidentDate())
                && AccidentSite.equals(other.getAccidentSite())
                && AccidentReason.equals(other.getAccidentReason())
                && AccidentCourse.equals(other.getAccidentCourse())
                && fDate.getString(RptDate).equals(other.getRptDate())
                && fDate.getString(CaseNoDate).equals(other.getCaseNoDate())
                && fDate.getString(CaseEndDate).equals(other.getCaseEndDate())
                && RgtReason.equals(other.getRgtReason())
                && NoRgtReason.equals(other.getNoRgtReason())
                && Remark.equals(other.getRemark())
                && RgtFlag.equals(other.getRgtFlag())
                && AvaiFlag.equals(other.getAvaiFlag())
                && AvaliReason.equals(other.getAvaliReason())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RptNo"))
        {
            return 0;
        }
        if (strFieldName.equals("RptMode"))
        {
            return 1;
        }
        if (strFieldName.equals("Relation"))
        {
            return 2;
        }
        if (strFieldName.equals("RptorName"))
        {
            return 3;
        }
        if (strFieldName.equals("RptorAddress"))
        {
            return 4;
        }
        if (strFieldName.equals("RptorPhone"))
        {
            return 5;
        }
        if (strFieldName.equals("RptorMobile"))
        {
            return 6;
        }
        if (strFieldName.equals("Email"))
        {
            return 7;
        }
        if (strFieldName.equals("PostCode"))
        {
            return 8;
        }
        if (strFieldName.equals("ReturnMode"))
        {
            return 9;
        }
        if (strFieldName.equals("AccidentDate"))
        {
            return 10;
        }
        if (strFieldName.equals("AccidentSite"))
        {
            return 11;
        }
        if (strFieldName.equals("AccidentReason"))
        {
            return 12;
        }
        if (strFieldName.equals("AccidentCourse"))
        {
            return 13;
        }
        if (strFieldName.equals("RptDate"))
        {
            return 14;
        }
        if (strFieldName.equals("CaseNoDate"))
        {
            return 15;
        }
        if (strFieldName.equals("CaseEndDate"))
        {
            return 16;
        }
        if (strFieldName.equals("RgtReason"))
        {
            return 17;
        }
        if (strFieldName.equals("NoRgtReason"))
        {
            return 18;
        }
        if (strFieldName.equals("Remark"))
        {
            return 19;
        }
        if (strFieldName.equals("RgtFlag"))
        {
            return 20;
        }
        if (strFieldName.equals("AvaiFlag"))
        {
            return 21;
        }
        if (strFieldName.equals("AvaliReason"))
        {
            return 22;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 23;
        }
        if (strFieldName.equals("Operator"))
        {
            return 24;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 25;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 26;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 27;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 28;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RptNo";
                break;
            case 1:
                strFieldName = "RptMode";
                break;
            case 2:
                strFieldName = "Relation";
                break;
            case 3:
                strFieldName = "RptorName";
                break;
            case 4:
                strFieldName = "RptorAddress";
                break;
            case 5:
                strFieldName = "RptorPhone";
                break;
            case 6:
                strFieldName = "RptorMobile";
                break;
            case 7:
                strFieldName = "Email";
                break;
            case 8:
                strFieldName = "PostCode";
                break;
            case 9:
                strFieldName = "ReturnMode";
                break;
            case 10:
                strFieldName = "AccidentDate";
                break;
            case 11:
                strFieldName = "AccidentSite";
                break;
            case 12:
                strFieldName = "AccidentReason";
                break;
            case 13:
                strFieldName = "AccidentCourse";
                break;
            case 14:
                strFieldName = "RptDate";
                break;
            case 15:
                strFieldName = "CaseNoDate";
                break;
            case 16:
                strFieldName = "CaseEndDate";
                break;
            case 17:
                strFieldName = "RgtReason";
                break;
            case 18:
                strFieldName = "NoRgtReason";
                break;
            case 19:
                strFieldName = "Remark";
                break;
            case 20:
                strFieldName = "RgtFlag";
                break;
            case 21:
                strFieldName = "AvaiFlag";
                break;
            case 22:
                strFieldName = "AvaliReason";
                break;
            case 23:
                strFieldName = "MngCom";
                break;
            case 24:
                strFieldName = "Operator";
                break;
            case 25:
                strFieldName = "MakeDate";
                break;
            case 26:
                strFieldName = "MakeTime";
                break;
            case 27:
                strFieldName = "ModifyDate";
                break;
            case 28:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RptNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Relation"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorPhone"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptorMobile"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Email"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PostCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReturnMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccidentDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AccidentSite"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccidentReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccidentCourse"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RptDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CaseNoDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CaseEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("RgtReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NoRgtReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AvaiFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AvaliReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
