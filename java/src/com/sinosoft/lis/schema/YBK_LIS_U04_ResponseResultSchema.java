/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.YBK_LIS_U04_ResponseResultDB;

/*
 * <p>ClassName: YBK_LIS_U04_ResponseResultSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2016-12-15
 */
public class YBK_LIS_U04_ResponseResultSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String DataBatchNo;
	/** 流水号 */
	private String DataSerialNo;
	/** 印刷号 */
	private String PrtNo;
	/** 投保流水号 */
	private String PolicySequenceNo;
	/** 客户编码 */
	private String CustomerSequenceNo;
	/** 请求类型 */
	private String RequestType;
	/** 任务号 */
	private String TaskNo;
	/** 响应编码 */
	private String ResponseCode;
	/** 错误描述 */
	private String ResultMessage;
	/** 自核结论 */
	private String Message;
	/** 自核结论描述 */
	private String DealResullt;
	/** 其他标识 */
	private String OtherSign;
	/** 返回日期 */
	private String MakeDate;
	/** 返回时间 */
	private String MakeTime;
	/** 最后更新日期 */
	private String ModifyDate;
	/** 最后更新时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public YBK_LIS_U04_ResponseResultSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "DataBatchNo";
		pk[1] = "DataSerialNo";
		pk[2] = "PrtNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		YBK_LIS_U04_ResponseResultSchema cloned = (YBK_LIS_U04_ResponseResultSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getDataBatchNo()
	{
		return DataBatchNo;
	}
	public void setDataBatchNo(String aDataBatchNo)
	{
		DataBatchNo = aDataBatchNo;
	}
	public String getDataSerialNo()
	{
		return DataSerialNo;
	}
	public void setDataSerialNo(String aDataSerialNo)
	{
		DataSerialNo = aDataSerialNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getPolicySequenceNo()
	{
		return PolicySequenceNo;
	}
	public void setPolicySequenceNo(String aPolicySequenceNo)
	{
		PolicySequenceNo = aPolicySequenceNo;
	}
	public String getCustomerSequenceNo()
	{
		return CustomerSequenceNo;
	}
	public void setCustomerSequenceNo(String aCustomerSequenceNo)
	{
		CustomerSequenceNo = aCustomerSequenceNo;
	}
	public String getRequestType()
	{
		return RequestType;
	}
	public void setRequestType(String aRequestType)
	{
		RequestType = aRequestType;
	}
	public String getTaskNo()
	{
		return TaskNo;
	}
	public void setTaskNo(String aTaskNo)
	{
		TaskNo = aTaskNo;
	}
	public String getResponseCode()
	{
		return ResponseCode;
	}
	public void setResponseCode(String aResponseCode)
	{
		ResponseCode = aResponseCode;
	}
	public String getResultMessage()
	{
		return ResultMessage;
	}
	public void setResultMessage(String aResultMessage)
	{
		ResultMessage = aResultMessage;
	}
	public String getMessage()
	{
		return Message;
	}
	public void setMessage(String aMessage)
	{
		Message = aMessage;
	}
	public String getDealResullt()
	{
		return DealResullt;
	}
	public void setDealResullt(String aDealResullt)
	{
		DealResullt = aDealResullt;
	}
	public String getOtherSign()
	{
		return OtherSign;
	}
	public void setOtherSign(String aOtherSign)
	{
		OtherSign = aOtherSign;
	}
	public String getMakeDate()
	{
		return MakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		return ModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 YBK_LIS_U04_ResponseResultSchema 对象给 Schema 赋值
	* @param: aYBK_LIS_U04_ResponseResultSchema YBK_LIS_U04_ResponseResultSchema
	**/
	public void setSchema(YBK_LIS_U04_ResponseResultSchema aYBK_LIS_U04_ResponseResultSchema)
	{
		this.DataBatchNo = aYBK_LIS_U04_ResponseResultSchema.getDataBatchNo();
		this.DataSerialNo = aYBK_LIS_U04_ResponseResultSchema.getDataSerialNo();
		this.PrtNo = aYBK_LIS_U04_ResponseResultSchema.getPrtNo();
		this.PolicySequenceNo = aYBK_LIS_U04_ResponseResultSchema.getPolicySequenceNo();
		this.CustomerSequenceNo = aYBK_LIS_U04_ResponseResultSchema.getCustomerSequenceNo();
		this.RequestType = aYBK_LIS_U04_ResponseResultSchema.getRequestType();
		this.TaskNo = aYBK_LIS_U04_ResponseResultSchema.getTaskNo();
		this.ResponseCode = aYBK_LIS_U04_ResponseResultSchema.getResponseCode();
		this.ResultMessage = aYBK_LIS_U04_ResponseResultSchema.getResultMessage();
		this.Message = aYBK_LIS_U04_ResponseResultSchema.getMessage();
		this.DealResullt = aYBK_LIS_U04_ResponseResultSchema.getDealResullt();
		this.OtherSign = aYBK_LIS_U04_ResponseResultSchema.getOtherSign();
		this.MakeDate = aYBK_LIS_U04_ResponseResultSchema.getMakeDate();
		this.MakeTime = aYBK_LIS_U04_ResponseResultSchema.getMakeTime();
		this.ModifyDate = aYBK_LIS_U04_ResponseResultSchema.getModifyDate();
		this.ModifyTime = aYBK_LIS_U04_ResponseResultSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("DataBatchNo") == null )
				this.DataBatchNo = null;
			else
				this.DataBatchNo = rs.getString("DataBatchNo").trim();

			if( rs.getString("DataSerialNo") == null )
				this.DataSerialNo = null;
			else
				this.DataSerialNo = rs.getString("DataSerialNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("PolicySequenceNo") == null )
				this.PolicySequenceNo = null;
			else
				this.PolicySequenceNo = rs.getString("PolicySequenceNo").trim();

			if( rs.getString("CustomerSequenceNo") == null )
				this.CustomerSequenceNo = null;
			else
				this.CustomerSequenceNo = rs.getString("CustomerSequenceNo").trim();

			if( rs.getString("RequestType") == null )
				this.RequestType = null;
			else
				this.RequestType = rs.getString("RequestType").trim();

			if( rs.getString("TaskNo") == null )
				this.TaskNo = null;
			else
				this.TaskNo = rs.getString("TaskNo").trim();

			if( rs.getString("ResponseCode") == null )
				this.ResponseCode = null;
			else
				this.ResponseCode = rs.getString("ResponseCode").trim();

			if( rs.getString("ResultMessage") == null )
				this.ResultMessage = null;
			else
				this.ResultMessage = rs.getString("ResultMessage").trim();

			if( rs.getString("Message") == null )
				this.Message = null;
			else
				this.Message = rs.getString("Message").trim();

			if( rs.getString("DealResullt") == null )
				this.DealResullt = null;
			else
				this.DealResullt = rs.getString("DealResullt").trim();

			if( rs.getString("OtherSign") == null )
				this.OtherSign = null;
			else
				this.OtherSign = rs.getString("OtherSign").trim();

			if( rs.getString("MakeDate") == null )
				this.MakeDate = null;
			else
				this.MakeDate = rs.getString("MakeDate").trim();

			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			if( rs.getString("ModifyDate") == null )
				this.ModifyDate = null;
			else
				this.ModifyDate = rs.getString("ModifyDate").trim();

			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的YBK_LIS_U04_ResponseResult表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YBK_LIS_U04_ResponseResultSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public YBK_LIS_U04_ResponseResultSchema getSchema()
	{
		YBK_LIS_U04_ResponseResultSchema aYBK_LIS_U04_ResponseResultSchema = new YBK_LIS_U04_ResponseResultSchema();
		aYBK_LIS_U04_ResponseResultSchema.setSchema(this);
		return aYBK_LIS_U04_ResponseResultSchema;
	}

	public YBK_LIS_U04_ResponseResultDB getDB()
	{
		YBK_LIS_U04_ResponseResultDB aDBOper = new YBK_LIS_U04_ResponseResultDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYBK_LIS_U04_ResponseResult描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(DataBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicySequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerSequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RequestType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaskNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponseCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResultMessage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Message)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealResullt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherSign)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYBK_LIS_U04_ResponseResult>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			DataBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			DataSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PolicySequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CustomerSequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RequestType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			TaskNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ResponseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ResultMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Message = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			DealResullt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			OtherSign = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YBK_LIS_U04_ResponseResultSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("DataBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataBatchNo));
		}
		if (FCode.equals("DataSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataSerialNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("PolicySequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicySequenceNo));
		}
		if (FCode.equals("CustomerSequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerSequenceNo));
		}
		if (FCode.equals("RequestType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RequestType));
		}
		if (FCode.equals("TaskNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaskNo));
		}
		if (FCode.equals("ResponseCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponseCode));
		}
		if (FCode.equals("ResultMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResultMessage));
		}
		if (FCode.equals("Message"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Message));
		}
		if (FCode.equals("DealResullt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealResullt));
		}
		if (FCode.equals("OtherSign"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherSign));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeDate));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyDate));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(DataBatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(DataSerialNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PolicySequenceNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CustomerSequenceNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RequestType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(TaskNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ResponseCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ResultMessage);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Message);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(DealResullt);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(OtherSign);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeDate);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyDate);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("DataBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataBatchNo = FValue.trim();
			}
			else
				DataBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("DataSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataSerialNo = FValue.trim();
			}
			else
				DataSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("PolicySequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicySequenceNo = FValue.trim();
			}
			else
				PolicySequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerSequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerSequenceNo = FValue.trim();
			}
			else
				CustomerSequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("RequestType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RequestType = FValue.trim();
			}
			else
				RequestType = null;
		}
		if (FCode.equalsIgnoreCase("TaskNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaskNo = FValue.trim();
			}
			else
				TaskNo = null;
		}
		if (FCode.equalsIgnoreCase("ResponseCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponseCode = FValue.trim();
			}
			else
				ResponseCode = null;
		}
		if (FCode.equalsIgnoreCase("ResultMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResultMessage = FValue.trim();
			}
			else
				ResultMessage = null;
		}
		if (FCode.equalsIgnoreCase("Message"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Message = FValue.trim();
			}
			else
				Message = null;
		}
		if (FCode.equalsIgnoreCase("DealResullt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealResullt = FValue.trim();
			}
			else
				DealResullt = null;
		}
		if (FCode.equalsIgnoreCase("OtherSign"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherSign = FValue.trim();
			}
			else
				OtherSign = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeDate = FValue.trim();
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyDate = FValue.trim();
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		YBK_LIS_U04_ResponseResultSchema other = (YBK_LIS_U04_ResponseResultSchema)otherObject;
		return
			(DataBatchNo == null ? other.getDataBatchNo() == null : DataBatchNo.equals(other.getDataBatchNo()))
			&& (DataSerialNo == null ? other.getDataSerialNo() == null : DataSerialNo.equals(other.getDataSerialNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (PolicySequenceNo == null ? other.getPolicySequenceNo() == null : PolicySequenceNo.equals(other.getPolicySequenceNo()))
			&& (CustomerSequenceNo == null ? other.getCustomerSequenceNo() == null : CustomerSequenceNo.equals(other.getCustomerSequenceNo()))
			&& (RequestType == null ? other.getRequestType() == null : RequestType.equals(other.getRequestType()))
			&& (TaskNo == null ? other.getTaskNo() == null : TaskNo.equals(other.getTaskNo()))
			&& (ResponseCode == null ? other.getResponseCode() == null : ResponseCode.equals(other.getResponseCode()))
			&& (ResultMessage == null ? other.getResultMessage() == null : ResultMessage.equals(other.getResultMessage()))
			&& (Message == null ? other.getMessage() == null : Message.equals(other.getMessage()))
			&& (DealResullt == null ? other.getDealResullt() == null : DealResullt.equals(other.getDealResullt()))
			&& (OtherSign == null ? other.getOtherSign() == null : OtherSign.equals(other.getOtherSign()))
			&& (MakeDate == null ? other.getMakeDate() == null : MakeDate.equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : ModifyDate.equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("DataBatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("DataSerialNo") ) {
			return 1;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 2;
		}
		if( strFieldName.equals("PolicySequenceNo") ) {
			return 3;
		}
		if( strFieldName.equals("CustomerSequenceNo") ) {
			return 4;
		}
		if( strFieldName.equals("RequestType") ) {
			return 5;
		}
		if( strFieldName.equals("TaskNo") ) {
			return 6;
		}
		if( strFieldName.equals("ResponseCode") ) {
			return 7;
		}
		if( strFieldName.equals("ResultMessage") ) {
			return 8;
		}
		if( strFieldName.equals("Message") ) {
			return 9;
		}
		if( strFieldName.equals("DealResullt") ) {
			return 10;
		}
		if( strFieldName.equals("OtherSign") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "DataBatchNo";
				break;
			case 1:
				strFieldName = "DataSerialNo";
				break;
			case 2:
				strFieldName = "PrtNo";
				break;
			case 3:
				strFieldName = "PolicySequenceNo";
				break;
			case 4:
				strFieldName = "CustomerSequenceNo";
				break;
			case 5:
				strFieldName = "RequestType";
				break;
			case 6:
				strFieldName = "TaskNo";
				break;
			case 7:
				strFieldName = "ResponseCode";
				break;
			case 8:
				strFieldName = "ResultMessage";
				break;
			case 9:
				strFieldName = "Message";
				break;
			case 10:
				strFieldName = "DealResullt";
				break;
			case 11:
				strFieldName = "OtherSign";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("DataBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicySequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerSequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RequestType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaskNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResponseCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResultMessage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Message") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealResullt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherSign") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
