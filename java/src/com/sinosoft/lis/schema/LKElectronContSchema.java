/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKElectronContDB;

/*
 * <p>ClassName: LKElectronContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 银保通电子保单发送信息
 * @CreateDate：2016-01-12
 */
public class LKElectronContSchema implements Schema, Cloneable
{
	// @Field
	/** 交易流水 */
	private String TransNo;
	/** 保单号 */
	private String ContNo;
	/** 签单日期 */
	private String SignDate;
	/** 客户姓名 */
	private String CustomerName;
	/** 保单发送日期 */
	private String ContSendDate;
	/** 客户邮箱 */
	private String CustomerEmail;
	/** 身份证号码 */
	private String CustomerNo;
	/** 银行名称 */
	private String BankName;
	/** 备用字段3 */
	private String Temp3;
	/** 备用字段1 */
	private String Temp1;
	/** 备用字段6 */
	private String Temp6;
	/** 备用字段7 */
	private String Temp7;
	/** 备用字段4 */
	private String Temp4;
	/** 备用字段2 */
	private String Temp2;
	/** 备用字段5 */
	private String Temp5;
	/** 备用字段10 */
	private String Temp10;
	/** 备用字段9 */
	private String Temp9;
	/** 备用字段8 */
	private String Temp8;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKElectronContSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "TransNo";
		pk[1] = "ContNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LKElectronContSchema cloned = (LKElectronContSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTransNo()
	{
		return TransNo;
	}
	public void setTransNo(String aTransNo)
	{
		TransNo = aTransNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getSignDate()
	{
		return SignDate;
	}
	public void setSignDate(String aSignDate)
	{
		SignDate = aSignDate;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
		CustomerName = aCustomerName;
	}
	public String getContSendDate()
	{
		return ContSendDate;
	}
	public void setContSendDate(String aContSendDate)
	{
		ContSendDate = aContSendDate;
	}
	public String getCustomerEmail()
	{
		return CustomerEmail;
	}
	public void setCustomerEmail(String aCustomerEmail)
	{
		CustomerEmail = aCustomerEmail;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getBankName()
	{
		return BankName;
	}
	public void setBankName(String aBankName)
	{
		BankName = aBankName;
	}
	public String getTemp3()
	{
		return Temp3;
	}
	public void setTemp3(String aTemp3)
	{
		Temp3 = aTemp3;
	}
	public String getTemp1()
	{
		return Temp1;
	}
	public void setTemp1(String aTemp1)
	{
		Temp1 = aTemp1;
	}
	public String getTemp6()
	{
		return Temp6;
	}
	public void setTemp6(String aTemp6)
	{
		Temp6 = aTemp6;
	}
	public String getTemp7()
	{
		return Temp7;
	}
	public void setTemp7(String aTemp7)
	{
		Temp7 = aTemp7;
	}
	public String getTemp4()
	{
		return Temp4;
	}
	public void setTemp4(String aTemp4)
	{
		Temp4 = aTemp4;
	}
	public String getTemp2()
	{
		return Temp2;
	}
	public void setTemp2(String aTemp2)
	{
		Temp2 = aTemp2;
	}
	public String getTemp5()
	{
		return Temp5;
	}
	public void setTemp5(String aTemp5)
	{
		Temp5 = aTemp5;
	}
	public String getTemp10()
	{
		return Temp10;
	}
	public void setTemp10(String aTemp10)
	{
		Temp10 = aTemp10;
	}
	public String getTemp9()
	{
		return Temp9;
	}
	public void setTemp9(String aTemp9)
	{
		Temp9 = aTemp9;
	}
	public String getTemp8()
	{
		return Temp8;
	}
	public void setTemp8(String aTemp8)
	{
		Temp8 = aTemp8;
	}

	/**
	* 使用另外一个 LKElectronContSchema 对象给 Schema 赋值
	* @param: aLKElectronContSchema LKElectronContSchema
	**/
	public void setSchema(LKElectronContSchema aLKElectronContSchema)
	{
		this.TransNo = aLKElectronContSchema.getTransNo();
		this.ContNo = aLKElectronContSchema.getContNo();
		this.SignDate = aLKElectronContSchema.getSignDate();
		this.CustomerName = aLKElectronContSchema.getCustomerName();
		this.ContSendDate = aLKElectronContSchema.getContSendDate();
		this.CustomerEmail = aLKElectronContSchema.getCustomerEmail();
		this.CustomerNo = aLKElectronContSchema.getCustomerNo();
		this.BankName = aLKElectronContSchema.getBankName();
		this.Temp3 = aLKElectronContSchema.getTemp3();
		this.Temp1 = aLKElectronContSchema.getTemp1();
		this.Temp6 = aLKElectronContSchema.getTemp6();
		this.Temp7 = aLKElectronContSchema.getTemp7();
		this.Temp4 = aLKElectronContSchema.getTemp4();
		this.Temp2 = aLKElectronContSchema.getTemp2();
		this.Temp5 = aLKElectronContSchema.getTemp5();
		this.Temp10 = aLKElectronContSchema.getTemp10();
		this.Temp9 = aLKElectronContSchema.getTemp9();
		this.Temp8 = aLKElectronContSchema.getTemp8();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TransNo") == null )
				this.TransNo = null;
			else
				this.TransNo = rs.getString("TransNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("SignDate") == null )
				this.SignDate = null;
			else
				this.SignDate = rs.getString("SignDate").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if( rs.getString("ContSendDate") == null )
				this.ContSendDate = null;
			else
				this.ContSendDate = rs.getString("ContSendDate").trim();

			if( rs.getString("CustomerEmail") == null )
				this.CustomerEmail = null;
			else
				this.CustomerEmail = rs.getString("CustomerEmail").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("BankName") == null )
				this.BankName = null;
			else
				this.BankName = rs.getString("BankName").trim();

			if( rs.getString("Temp3") == null )
				this.Temp3 = null;
			else
				this.Temp3 = rs.getString("Temp3").trim();

			if( rs.getString("Temp1") == null )
				this.Temp1 = null;
			else
				this.Temp1 = rs.getString("Temp1").trim();

			if( rs.getString("Temp6") == null )
				this.Temp6 = null;
			else
				this.Temp6 = rs.getString("Temp6").trim();

			if( rs.getString("Temp7") == null )
				this.Temp7 = null;
			else
				this.Temp7 = rs.getString("Temp7").trim();

			if( rs.getString("Temp4") == null )
				this.Temp4 = null;
			else
				this.Temp4 = rs.getString("Temp4").trim();

			if( rs.getString("Temp2") == null )
				this.Temp2 = null;
			else
				this.Temp2 = rs.getString("Temp2").trim();

			if( rs.getString("Temp5") == null )
				this.Temp5 = null;
			else
				this.Temp5 = rs.getString("Temp5").trim();

			if( rs.getString("Temp10") == null )
				this.Temp10 = null;
			else
				this.Temp10 = rs.getString("Temp10").trim();

			if( rs.getString("Temp9") == null )
				this.Temp9 = null;
			else
				this.Temp9 = rs.getString("Temp9").trim();

			if( rs.getString("Temp8") == null )
				this.Temp8 = null;
			else
				this.Temp8 = rs.getString("Temp8").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LKElectronCont表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKElectronContSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LKElectronContSchema getSchema()
	{
		LKElectronContSchema aLKElectronContSchema = new LKElectronContSchema();
		aLKElectronContSchema.setSchema(this);
		return aLKElectronContSchema;
	}

	public LKElectronContDB getDB()
	{
		LKElectronContDB aDBOper = new LKElectronContDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKElectronCont描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TransNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SignDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContSendDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerEmail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp7)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp10)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp8));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKElectronCont>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TransNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SignDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ContSendDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CustomerEmail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BankName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Temp3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Temp1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Temp6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Temp7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Temp4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Temp2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Temp5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Temp10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Temp9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Temp8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKElectronContSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TransNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("SignDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignDate));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("ContSendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContSendDate));
		}
		if (FCode.equals("CustomerEmail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerEmail));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("BankName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankName));
		}
		if (FCode.equals("Temp3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp3));
		}
		if (FCode.equals("Temp1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp1));
		}
		if (FCode.equals("Temp6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp6));
		}
		if (FCode.equals("Temp7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp7));
		}
		if (FCode.equals("Temp4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp4));
		}
		if (FCode.equals("Temp2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp2));
		}
		if (FCode.equals("Temp5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp5));
		}
		if (FCode.equals("Temp10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp10));
		}
		if (FCode.equals("Temp9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp9));
		}
		if (FCode.equals("Temp8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp8));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TransNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SignDate);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ContSendDate);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CustomerEmail);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BankName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Temp3);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Temp1);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Temp6);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Temp7);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Temp4);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Temp2);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Temp5);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Temp10);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Temp9);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Temp8);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TransNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransNo = FValue.trim();
			}
			else
				TransNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("SignDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SignDate = FValue.trim();
			}
			else
				SignDate = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("ContSendDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContSendDate = FValue.trim();
			}
			else
				ContSendDate = null;
		}
		if (FCode.equalsIgnoreCase("CustomerEmail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerEmail = FValue.trim();
			}
			else
				CustomerEmail = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("BankName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankName = FValue.trim();
			}
			else
				BankName = null;
		}
		if (FCode.equalsIgnoreCase("Temp3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp3 = FValue.trim();
			}
			else
				Temp3 = null;
		}
		if (FCode.equalsIgnoreCase("Temp1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp1 = FValue.trim();
			}
			else
				Temp1 = null;
		}
		if (FCode.equalsIgnoreCase("Temp6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp6 = FValue.trim();
			}
			else
				Temp6 = null;
		}
		if (FCode.equalsIgnoreCase("Temp7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp7 = FValue.trim();
			}
			else
				Temp7 = null;
		}
		if (FCode.equalsIgnoreCase("Temp4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp4 = FValue.trim();
			}
			else
				Temp4 = null;
		}
		if (FCode.equalsIgnoreCase("Temp2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp2 = FValue.trim();
			}
			else
				Temp2 = null;
		}
		if (FCode.equalsIgnoreCase("Temp5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp5 = FValue.trim();
			}
			else
				Temp5 = null;
		}
		if (FCode.equalsIgnoreCase("Temp10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp10 = FValue.trim();
			}
			else
				Temp10 = null;
		}
		if (FCode.equalsIgnoreCase("Temp9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp9 = FValue.trim();
			}
			else
				Temp9 = null;
		}
		if (FCode.equalsIgnoreCase("Temp8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp8 = FValue.trim();
			}
			else
				Temp8 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKElectronContSchema other = (LKElectronContSchema)otherObject;
		return
			(TransNo == null ? other.getTransNo() == null : TransNo.equals(other.getTransNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (SignDate == null ? other.getSignDate() == null : SignDate.equals(other.getSignDate()))
			&& (CustomerName == null ? other.getCustomerName() == null : CustomerName.equals(other.getCustomerName()))
			&& (ContSendDate == null ? other.getContSendDate() == null : ContSendDate.equals(other.getContSendDate()))
			&& (CustomerEmail == null ? other.getCustomerEmail() == null : CustomerEmail.equals(other.getCustomerEmail()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (BankName == null ? other.getBankName() == null : BankName.equals(other.getBankName()))
			&& (Temp3 == null ? other.getTemp3() == null : Temp3.equals(other.getTemp3()))
			&& (Temp1 == null ? other.getTemp1() == null : Temp1.equals(other.getTemp1()))
			&& (Temp6 == null ? other.getTemp6() == null : Temp6.equals(other.getTemp6()))
			&& (Temp7 == null ? other.getTemp7() == null : Temp7.equals(other.getTemp7()))
			&& (Temp4 == null ? other.getTemp4() == null : Temp4.equals(other.getTemp4()))
			&& (Temp2 == null ? other.getTemp2() == null : Temp2.equals(other.getTemp2()))
			&& (Temp5 == null ? other.getTemp5() == null : Temp5.equals(other.getTemp5()))
			&& (Temp10 == null ? other.getTemp10() == null : Temp10.equals(other.getTemp10()))
			&& (Temp9 == null ? other.getTemp9() == null : Temp9.equals(other.getTemp9()))
			&& (Temp8 == null ? other.getTemp8() == null : Temp8.equals(other.getTemp8()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TransNo") ) {
			return 0;
		}
		if( strFieldName.equals("ContNo") ) {
			return 1;
		}
		if( strFieldName.equals("SignDate") ) {
			return 2;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 3;
		}
		if( strFieldName.equals("ContSendDate") ) {
			return 4;
		}
		if( strFieldName.equals("CustomerEmail") ) {
			return 5;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 6;
		}
		if( strFieldName.equals("BankName") ) {
			return 7;
		}
		if( strFieldName.equals("Temp3") ) {
			return 8;
		}
		if( strFieldName.equals("Temp1") ) {
			return 9;
		}
		if( strFieldName.equals("Temp6") ) {
			return 10;
		}
		if( strFieldName.equals("Temp7") ) {
			return 11;
		}
		if( strFieldName.equals("Temp4") ) {
			return 12;
		}
		if( strFieldName.equals("Temp2") ) {
			return 13;
		}
		if( strFieldName.equals("Temp5") ) {
			return 14;
		}
		if( strFieldName.equals("Temp10") ) {
			return 15;
		}
		if( strFieldName.equals("Temp9") ) {
			return 16;
		}
		if( strFieldName.equals("Temp8") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TransNo";
				break;
			case 1:
				strFieldName = "ContNo";
				break;
			case 2:
				strFieldName = "SignDate";
				break;
			case 3:
				strFieldName = "CustomerName";
				break;
			case 4:
				strFieldName = "ContSendDate";
				break;
			case 5:
				strFieldName = "CustomerEmail";
				break;
			case 6:
				strFieldName = "CustomerNo";
				break;
			case 7:
				strFieldName = "BankName";
				break;
			case 8:
				strFieldName = "Temp3";
				break;
			case 9:
				strFieldName = "Temp1";
				break;
			case 10:
				strFieldName = "Temp6";
				break;
			case 11:
				strFieldName = "Temp7";
				break;
			case 12:
				strFieldName = "Temp4";
				break;
			case 13:
				strFieldName = "Temp2";
				break;
			case 14:
				strFieldName = "Temp5";
				break;
			case 15:
				strFieldName = "Temp10";
				break;
			case 16:
				strFieldName = "Temp9";
				break;
			case 17:
				strFieldName = "Temp8";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TransNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContSendDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerEmail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp10") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp8") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
