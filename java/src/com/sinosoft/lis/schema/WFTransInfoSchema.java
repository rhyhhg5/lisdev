/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.WFTransInfoDB;

/*
 * <p>ClassName: WFTransInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2010-06-02
 */
public class WFTransInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 信息类型 */
	private String TransType;
	/** 类型中文说明 */
	private String TransName;
	/** 类型编码 */
	private String TransCode;
	/** 类型展现方式 */
	private String TransState;
	/** 类型具体描述 */
	private String TransDetail;
	/** 类型具体描述2 */
	private String TransDetail2;
	/** 备注 */
	private String remark;

	public static final int FIELDNUM = 7;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public WFTransInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "TransType";
		pk[1] = "TransCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		WFTransInfoSchema cloned = (WFTransInfoSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTransType()
	{
		return TransType;
	}
	public void setTransType(String aTransType)
	{
		TransType = aTransType;
	}
	public String getTransName()
	{
		return TransName;
	}
	public void setTransName(String aTransName)
	{
		TransName = aTransName;
	}
	public String getTransCode()
	{
		return TransCode;
	}
	public void setTransCode(String aTransCode)
	{
		TransCode = aTransCode;
	}
	public String getTransState()
	{
		return TransState;
	}
	public void setTransState(String aTransState)
	{
		TransState = aTransState;
	}
	public String getTransDetail()
	{
		return TransDetail;
	}
	public void setTransDetail(String aTransDetail)
	{
		TransDetail = aTransDetail;
	}
	public String getTransDetail2()
	{
		return TransDetail2;
	}
	public void setTransDetail2(String aTransDetail2)
	{
		TransDetail2 = aTransDetail2;
	}
	public String getremark()
	{
		return remark;
	}
	public void setremark(String aremark)
	{
		remark = aremark;
	}

	/**
	* 使用另外一个 WFTransInfoSchema 对象给 Schema 赋值
	* @param: aWFTransInfoSchema WFTransInfoSchema
	**/
	public void setSchema(WFTransInfoSchema aWFTransInfoSchema)
	{
		this.TransType = aWFTransInfoSchema.getTransType();
		this.TransName = aWFTransInfoSchema.getTransName();
		this.TransCode = aWFTransInfoSchema.getTransCode();
		this.TransState = aWFTransInfoSchema.getTransState();
		this.TransDetail = aWFTransInfoSchema.getTransDetail();
		this.TransDetail2 = aWFTransInfoSchema.getTransDetail2();
		this.remark = aWFTransInfoSchema.getremark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TransType") == null )
				this.TransType = null;
			else
				this.TransType = rs.getString("TransType").trim();

			if( rs.getString("TransName") == null )
				this.TransName = null;
			else
				this.TransName = rs.getString("TransName").trim();

			if( rs.getString("TransCode") == null )
				this.TransCode = null;
			else
				this.TransCode = rs.getString("TransCode").trim();

			if( rs.getString("TransState") == null )
				this.TransState = null;
			else
				this.TransState = rs.getString("TransState").trim();

			if( rs.getString("TransDetail") == null )
				this.TransDetail = null;
			else
				this.TransDetail = rs.getString("TransDetail").trim();

			if( rs.getString("TransDetail2") == null )
				this.TransDetail2 = null;
			else
				this.TransDetail2 = rs.getString("TransDetail2").trim();

			if( rs.getString("remark") == null )
				this.remark = null;
			else
				this.remark = rs.getString("remark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的WFTransInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "WFTransInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public WFTransInfoSchema getSchema()
	{
		WFTransInfoSchema aWFTransInfoSchema = new WFTransInfoSchema();
		aWFTransInfoSchema.setSchema(this);
		return aWFTransInfoSchema;
	}

	public WFTransInfoDB getDB()
	{
		WFTransInfoDB aDBOper = new WFTransInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpWFTransInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TransType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransDetail2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(remark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpWFTransInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TransName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			TransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TransState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			TransDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			TransDetail2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "WFTransInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TransType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
		}
		if (FCode.equals("TransName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransName));
		}
		if (FCode.equals("TransCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransCode));
		}
		if (FCode.equals("TransState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransState));
		}
		if (FCode.equals("TransDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransDetail));
		}
		if (FCode.equals("TransDetail2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransDetail2));
		}
		if (FCode.equals("remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(remark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TransType);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TransName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(TransCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TransState);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(TransDetail);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(TransDetail2);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(remark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TransType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransType = FValue.trim();
			}
			else
				TransType = null;
		}
		if (FCode.equalsIgnoreCase("TransName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransName = FValue.trim();
			}
			else
				TransName = null;
		}
		if (FCode.equalsIgnoreCase("TransCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransCode = FValue.trim();
			}
			else
				TransCode = null;
		}
		if (FCode.equalsIgnoreCase("TransState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransState = FValue.trim();
			}
			else
				TransState = null;
		}
		if (FCode.equalsIgnoreCase("TransDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransDetail = FValue.trim();
			}
			else
				TransDetail = null;
		}
		if (FCode.equalsIgnoreCase("TransDetail2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransDetail2 = FValue.trim();
			}
			else
				TransDetail2 = null;
		}
		if (FCode.equalsIgnoreCase("remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				remark = FValue.trim();
			}
			else
				remark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		WFTransInfoSchema other = (WFTransInfoSchema)otherObject;
		return
			(TransType == null ? other.getTransType() == null : TransType.equals(other.getTransType()))
			&& (TransName == null ? other.getTransName() == null : TransName.equals(other.getTransName()))
			&& (TransCode == null ? other.getTransCode() == null : TransCode.equals(other.getTransCode()))
			&& (TransState == null ? other.getTransState() == null : TransState.equals(other.getTransState()))
			&& (TransDetail == null ? other.getTransDetail() == null : TransDetail.equals(other.getTransDetail()))
			&& (TransDetail2 == null ? other.getTransDetail2() == null : TransDetail2.equals(other.getTransDetail2()))
			&& (remark == null ? other.getremark() == null : remark.equals(other.getremark()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TransType") ) {
			return 0;
		}
		if( strFieldName.equals("TransName") ) {
			return 1;
		}
		if( strFieldName.equals("TransCode") ) {
			return 2;
		}
		if( strFieldName.equals("TransState") ) {
			return 3;
		}
		if( strFieldName.equals("TransDetail") ) {
			return 4;
		}
		if( strFieldName.equals("TransDetail2") ) {
			return 5;
		}
		if( strFieldName.equals("remark") ) {
			return 6;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TransType";
				break;
			case 1:
				strFieldName = "TransName";
				break;
			case 2:
				strFieldName = "TransCode";
				break;
			case 3:
				strFieldName = "TransState";
				break;
			case 4:
				strFieldName = "TransDetail";
				break;
			case 5:
				strFieldName = "TransDetail2";
				break;
			case 6:
				strFieldName = "remark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TransType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransDetail2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("remark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
