/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLInqReasonDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLInqReasonSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 理赔调查相关表
 * @CreateDate：2005-06-02
 */
public class LLInqReasonSchema implements Schema, Cloneable
{
    // @Field
    /** 对应号码 */
    private String OtherNo;
    /** 其他对应号码类型 */
    private String OtherNoType;
    /** 调查号 */
    private String SurveyNo;
    /** 调查原因编号 */
    private String InqRNo;
    /** 调查原因 */
    private String InqRCode;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 备注 */
    private String Remark;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLInqReasonSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SurveyNo";
        pk[1] = "InqRNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLInqReasonSchema cloned = (LLInqReasonSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getOtherNo()
    {
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType()
    {
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType)
    {
        OtherNoType = aOtherNoType;
    }

    public String getSurveyNo()
    {
        return SurveyNo;
    }

    public void setSurveyNo(String aSurveyNo)
    {
        SurveyNo = aSurveyNo;
    }

    public String getInqRNo()
    {
        return InqRNo;
    }

    public void setInqRNo(String aInqRNo)
    {
        InqRNo = aInqRNo;
    }

    public String getInqRCode()
    {
        return InqRCode;
    }

    public void setInqRCode(String aInqRCode)
    {
        InqRCode = aInqRCode;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getRemark()
    {
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    /**
     * 使用另外一个 LLInqReasonSchema 对象给 Schema 赋值
     * @param: aLLInqReasonSchema LLInqReasonSchema
     **/
    public void setSchema(LLInqReasonSchema aLLInqReasonSchema)
    {
        this.OtherNo = aLLInqReasonSchema.getOtherNo();
        this.OtherNoType = aLLInqReasonSchema.getOtherNoType();
        this.SurveyNo = aLLInqReasonSchema.getSurveyNo();
        this.InqRNo = aLLInqReasonSchema.getInqRNo();
        this.InqRCode = aLLInqReasonSchema.getInqRCode();
        this.Operator = aLLInqReasonSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLInqReasonSchema.getMakeDate());
        this.MakeTime = aLLInqReasonSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLInqReasonSchema.getModifyDate());
        this.ModifyTime = aLLInqReasonSchema.getModifyTime();
        this.Remark = aLLInqReasonSchema.getRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null)
            {
                this.OtherNoType = null;
            }
            else
            {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("SurveyNo") == null)
            {
                this.SurveyNo = null;
            }
            else
            {
                this.SurveyNo = rs.getString("SurveyNo").trim();
            }

            if (rs.getString("InqRNo") == null)
            {
                this.InqRNo = null;
            }
            else
            {
                this.InqRNo = rs.getString("InqRNo").trim();
            }

            if (rs.getString("InqRCode") == null)
            {
                this.InqRCode = null;
            }
            else
            {
                this.InqRCode = rs.getString("InqRCode").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LLInqReason表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLInqReasonSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLInqReasonSchema getSchema()
    {
        LLInqReasonSchema aLLInqReasonSchema = new LLInqReasonSchema();
        aLLInqReasonSchema.setSchema(this);
        return aLLInqReasonSchema;
    }

    public LLInqReasonDB getDB()
    {
        LLInqReasonDB aDBOper = new LLInqReasonDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqReason描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(OtherNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqRNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqRCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqReason>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            SurveyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            InqRNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            InqRCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLInqReasonSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equals("SurveyNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyNo));
        }
        if (FCode.equals("InqRNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqRNo));
        }
        if (FCode.equals("InqRCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqRCode));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SurveyNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(InqRNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(InqRCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
            {
                OtherNoType = null;
            }
        }
        if (FCode.equals("SurveyNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyNo = FValue.trim();
            }
            else
            {
                SurveyNo = null;
            }
        }
        if (FCode.equals("InqRNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqRNo = FValue.trim();
            }
            else
            {
                InqRNo = null;
            }
        }
        if (FCode.equals("InqRCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqRCode = FValue.trim();
            }
            else
            {
                InqRCode = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLInqReasonSchema other = (LLInqReasonSchema) otherObject;
        return
                OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && SurveyNo.equals(other.getSurveyNo())
                && InqRNo.equals(other.getInqRNo())
                && InqRCode.equals(other.getInqRCode())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Remark.equals(other.getRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("OtherNo"))
        {
            return 0;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return 1;
        }
        if (strFieldName.equals("SurveyNo"))
        {
            return 2;
        }
        if (strFieldName.equals("InqRNo"))
        {
            return 3;
        }
        if (strFieldName.equals("InqRCode"))
        {
            return 4;
        }
        if (strFieldName.equals("Operator"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 7;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 9;
        }
        if (strFieldName.equals("Remark"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "OtherNo";
                break;
            case 1:
                strFieldName = "OtherNoType";
                break;
            case 2:
                strFieldName = "SurveyNo";
                break;
            case 3:
                strFieldName = "InqRNo";
                break;
            case 4:
                strFieldName = "InqRCode";
                break;
            case 5:
                strFieldName = "Operator";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            case 10:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqRNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqRCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
