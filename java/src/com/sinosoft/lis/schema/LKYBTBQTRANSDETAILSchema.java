/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKYBTBQTRANSDETAILDB;

/*
 * <p>ClassName: LKYBTBQTRANSDETAILSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2018-03-09
 */
public class LKYBTBQTRANSDETAILSchema implements Schema, Cloneable
{
	// @Field
	/** 银行代码 */
	private String BANKCODE;
	/** 交易类型 */
	private String STATETYPE;
	/** 保单号码 */
	private String CONTNO;
	/** 投保单号码 */
	private String PRTNO;
	/** 投保人姓名 */
	private String APPNTNAME;
	/** 投保人证件类型 */
	private String APPNTIDTYPE;
	/** 投保人证件号码 */
	private String APPNTIDNO;
	/** 入库日期 */
	private String MAKEDATE;
	/** 入库时间 */
	private String MAKETIME;
	/** 修改日期 */
	private String MODIFYDATE;
	/** 修改时间 */
	private String MODIFYTIME;
	/** 回盘状态 */
	private String RETURNSTATE;
	/** 回盘失败原因 */
	private String RETURNERRINFO;
	/** Bak_1 */
	private String bak_1;
	/** Bak_2 */
	private String bak_2;
	/** Bak_3 */
	private String bak_3;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKYBTBQTRANSDETAILSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CONTNO";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LKYBTBQTRANSDETAILSchema cloned = (LKYBTBQTRANSDETAILSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBANKCODE()
	{
		return BANKCODE;
	}
	public void setBANKCODE(String aBANKCODE)
	{
		BANKCODE = aBANKCODE;
	}
	public String getSTATETYPE()
	{
		return STATETYPE;
	}
	public void setSTATETYPE(String aSTATETYPE)
	{
		STATETYPE = aSTATETYPE;
	}
	public String getCONTNO()
	{
		return CONTNO;
	}
	public void setCONTNO(String aCONTNO)
	{
		CONTNO = aCONTNO;
	}
	public String getPRTNO()
	{
		return PRTNO;
	}
	public void setPRTNO(String aPRTNO)
	{
		PRTNO = aPRTNO;
	}
	public String getAPPNTNAME()
	{
		return APPNTNAME;
	}
	public void setAPPNTNAME(String aAPPNTNAME)
	{
		APPNTNAME = aAPPNTNAME;
	}
	public String getAPPNTIDTYPE()
	{
		return APPNTIDTYPE;
	}
	public void setAPPNTIDTYPE(String aAPPNTIDTYPE)
	{
		APPNTIDTYPE = aAPPNTIDTYPE;
	}
	public String getAPPNTIDNO()
	{
		return APPNTIDNO;
	}
	public void setAPPNTIDNO(String aAPPNTIDNO)
	{
		APPNTIDNO = aAPPNTIDNO;
	}
	public String getMAKEDATE()
	{
		return MAKEDATE;
	}
	public void setMAKEDATE(String aMAKEDATE)
	{
		MAKEDATE = aMAKEDATE;
	}
	public String getMAKETIME()
	{
		return MAKETIME;
	}
	public void setMAKETIME(String aMAKETIME)
	{
		MAKETIME = aMAKETIME;
	}
	public String getMODIFYDATE()
	{
		return MODIFYDATE;
	}
	public void setMODIFYDATE(String aMODIFYDATE)
	{
		MODIFYDATE = aMODIFYDATE;
	}
	public String getMODIFYTIME()
	{
		return MODIFYTIME;
	}
	public void setMODIFYTIME(String aMODIFYTIME)
	{
		MODIFYTIME = aMODIFYTIME;
	}
	public String getRETURNSTATE()
	{
		return RETURNSTATE;
	}
	public void setRETURNSTATE(String aRETURNSTATE)
	{
		RETURNSTATE = aRETURNSTATE;
	}
	public String getRETURNERRINFO()
	{
		return RETURNERRINFO;
	}
	public void setRETURNERRINFO(String aRETURNERRINFO)
	{
		RETURNERRINFO = aRETURNERRINFO;
	}
	public String getbak_1()
	{
		return bak_1;
	}
	public void setbak_1(String abak_1)
	{
		bak_1 = abak_1;
	}
	public String getbak_2()
	{
		return bak_2;
	}
	public void setbak_2(String abak_2)
	{
		bak_2 = abak_2;
	}
	public String getbak_3()
	{
		return bak_3;
	}
	public void setbak_3(String abak_3)
	{
		bak_3 = abak_3;
	}

	/**
	* 使用另外一个 LKYBTBQTRANSDETAILSchema 对象给 Schema 赋值
	* @param: aLKYBTBQTRANSDETAILSchema LKYBTBQTRANSDETAILSchema
	**/
	public void setSchema(LKYBTBQTRANSDETAILSchema aLKYBTBQTRANSDETAILSchema)
	{
		this.BANKCODE = aLKYBTBQTRANSDETAILSchema.getBANKCODE();
		this.STATETYPE = aLKYBTBQTRANSDETAILSchema.getSTATETYPE();
		this.CONTNO = aLKYBTBQTRANSDETAILSchema.getCONTNO();
		this.PRTNO = aLKYBTBQTRANSDETAILSchema.getPRTNO();
		this.APPNTNAME = aLKYBTBQTRANSDETAILSchema.getAPPNTNAME();
		this.APPNTIDTYPE = aLKYBTBQTRANSDETAILSchema.getAPPNTIDTYPE();
		this.APPNTIDNO = aLKYBTBQTRANSDETAILSchema.getAPPNTIDNO();
		this.MAKEDATE = aLKYBTBQTRANSDETAILSchema.getMAKEDATE();
		this.MAKETIME = aLKYBTBQTRANSDETAILSchema.getMAKETIME();
		this.MODIFYDATE = aLKYBTBQTRANSDETAILSchema.getMODIFYDATE();
		this.MODIFYTIME = aLKYBTBQTRANSDETAILSchema.getMODIFYTIME();
		this.RETURNSTATE = aLKYBTBQTRANSDETAILSchema.getRETURNSTATE();
		this.RETURNERRINFO = aLKYBTBQTRANSDETAILSchema.getRETURNERRINFO();
		this.bak_1 = aLKYBTBQTRANSDETAILSchema.getbak_1();
		this.bak_2 = aLKYBTBQTRANSDETAILSchema.getbak_2();
		this.bak_3 = aLKYBTBQTRANSDETAILSchema.getbak_3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BANKCODE") == null )
				this.BANKCODE = null;
			else
				this.BANKCODE = rs.getString("BANKCODE").trim();

			if( rs.getString("STATETYPE") == null )
				this.STATETYPE = null;
			else
				this.STATETYPE = rs.getString("STATETYPE").trim();

			if( rs.getString("CONTNO") == null )
				this.CONTNO = null;
			else
				this.CONTNO = rs.getString("CONTNO").trim();

			if( rs.getString("PRTNO") == null )
				this.PRTNO = null;
			else
				this.PRTNO = rs.getString("PRTNO").trim();

			if( rs.getString("APPNTNAME") == null )
				this.APPNTNAME = null;
			else
				this.APPNTNAME = rs.getString("APPNTNAME").trim();

			if( rs.getString("APPNTIDTYPE") == null )
				this.APPNTIDTYPE = null;
			else
				this.APPNTIDTYPE = rs.getString("APPNTIDTYPE").trim();

			if( rs.getString("APPNTIDNO") == null )
				this.APPNTIDNO = null;
			else
				this.APPNTIDNO = rs.getString("APPNTIDNO").trim();

			if( rs.getString("MAKEDATE") == null )
				this.MAKEDATE = null;
			else
				this.MAKEDATE = rs.getString("MAKEDATE").trim();

			if( rs.getString("MAKETIME") == null )
				this.MAKETIME = null;
			else
				this.MAKETIME = rs.getString("MAKETIME").trim();

			if( rs.getString("MODIFYDATE") == null )
				this.MODIFYDATE = null;
			else
				this.MODIFYDATE = rs.getString("MODIFYDATE").trim();

			if( rs.getString("MODIFYTIME") == null )
				this.MODIFYTIME = null;
			else
				this.MODIFYTIME = rs.getString("MODIFYTIME").trim();

			if( rs.getString("RETURNSTATE") == null )
				this.RETURNSTATE = null;
			else
				this.RETURNSTATE = rs.getString("RETURNSTATE").trim();

			if( rs.getString("RETURNERRINFO") == null )
				this.RETURNERRINFO = null;
			else
				this.RETURNERRINFO = rs.getString("RETURNERRINFO").trim();

			if( rs.getString("bak_1") == null )
				this.bak_1 = null;
			else
				this.bak_1 = rs.getString("bak_1").trim();

			if( rs.getString("bak_2") == null )
				this.bak_2 = null;
			else
				this.bak_2 = rs.getString("bak_2").trim();

			if( rs.getString("bak_3") == null )
				this.bak_3 = null;
			else
				this.bak_3 = rs.getString("bak_3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LKYBTBQTRANSDETAIL表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKYBTBQTRANSDETAILSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LKYBTBQTRANSDETAILSchema getSchema()
	{
		LKYBTBQTRANSDETAILSchema aLKYBTBQTRANSDETAILSchema = new LKYBTBQTRANSDETAILSchema();
		aLKYBTBQTRANSDETAILSchema.setSchema(this);
		return aLKYBTBQTRANSDETAILSchema;
	}

	public LKYBTBQTRANSDETAILDB getDB()
	{
		LKYBTBQTRANSDETAILDB aDBOper = new LKYBTBQTRANSDETAILDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKYBTBQTRANSDETAIL描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BANKCODE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(STATETYPE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CONTNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PRTNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(APPNTNAME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(APPNTIDTYPE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(APPNTIDNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MAKEDATE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MAKETIME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MODIFYDATE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MODIFYTIME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RETURNSTATE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RETURNERRINFO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak_1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak_2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak_3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKYBTBQTRANSDETAIL>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BANKCODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			STATETYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CONTNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PRTNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			APPNTNAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			APPNTIDTYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			APPNTIDNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MAKEDATE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MAKETIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MODIFYDATE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MODIFYTIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			RETURNSTATE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			RETURNERRINFO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			bak_1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			bak_2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			bak_3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKYBTBQTRANSDETAILSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BANKCODE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BANKCODE));
		}
		if (FCode.equals("STATETYPE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(STATETYPE));
		}
		if (FCode.equals("CONTNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CONTNO));
		}
		if (FCode.equals("PRTNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PRTNO));
		}
		if (FCode.equals("APPNTNAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(APPNTNAME));
		}
		if (FCode.equals("APPNTIDTYPE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(APPNTIDTYPE));
		}
		if (FCode.equals("APPNTIDNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(APPNTIDNO));
		}
		if (FCode.equals("MAKEDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MAKEDATE));
		}
		if (FCode.equals("MAKETIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
		}
		if (FCode.equals("MODIFYDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYDATE));
		}
		if (FCode.equals("MODIFYTIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYTIME));
		}
		if (FCode.equals("RETURNSTATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RETURNSTATE));
		}
		if (FCode.equals("RETURNERRINFO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RETURNERRINFO));
		}
		if (FCode.equals("bak_1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak_1));
		}
		if (FCode.equals("bak_2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak_2));
		}
		if (FCode.equals("bak_3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak_3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BANKCODE);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(STATETYPE);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CONTNO);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PRTNO);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(APPNTNAME);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(APPNTIDTYPE);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(APPNTIDNO);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MAKEDATE);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(MAKETIME);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MODIFYDATE);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MODIFYTIME);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(RETURNSTATE);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(RETURNERRINFO);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(bak_1);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(bak_2);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(bak_3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BANKCODE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BANKCODE = FValue.trim();
			}
			else
				BANKCODE = null;
		}
		if (FCode.equalsIgnoreCase("STATETYPE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				STATETYPE = FValue.trim();
			}
			else
				STATETYPE = null;
		}
		if (FCode.equalsIgnoreCase("CONTNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CONTNO = FValue.trim();
			}
			else
				CONTNO = null;
		}
		if (FCode.equalsIgnoreCase("PRTNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PRTNO = FValue.trim();
			}
			else
				PRTNO = null;
		}
		if (FCode.equalsIgnoreCase("APPNTNAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				APPNTNAME = FValue.trim();
			}
			else
				APPNTNAME = null;
		}
		if (FCode.equalsIgnoreCase("APPNTIDTYPE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				APPNTIDTYPE = FValue.trim();
			}
			else
				APPNTIDTYPE = null;
		}
		if (FCode.equalsIgnoreCase("APPNTIDNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				APPNTIDNO = FValue.trim();
			}
			else
				APPNTIDNO = null;
		}
		if (FCode.equalsIgnoreCase("MAKEDATE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MAKEDATE = FValue.trim();
			}
			else
				MAKEDATE = null;
		}
		if (FCode.equalsIgnoreCase("MAKETIME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MAKETIME = FValue.trim();
			}
			else
				MAKETIME = null;
		}
		if (FCode.equalsIgnoreCase("MODIFYDATE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MODIFYDATE = FValue.trim();
			}
			else
				MODIFYDATE = null;
		}
		if (FCode.equalsIgnoreCase("MODIFYTIME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MODIFYTIME = FValue.trim();
			}
			else
				MODIFYTIME = null;
		}
		if (FCode.equalsIgnoreCase("RETURNSTATE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RETURNSTATE = FValue.trim();
			}
			else
				RETURNSTATE = null;
		}
		if (FCode.equalsIgnoreCase("RETURNERRINFO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RETURNERRINFO = FValue.trim();
			}
			else
				RETURNERRINFO = null;
		}
		if (FCode.equalsIgnoreCase("bak_1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak_1 = FValue.trim();
			}
			else
				bak_1 = null;
		}
		if (FCode.equalsIgnoreCase("bak_2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak_2 = FValue.trim();
			}
			else
				bak_2 = null;
		}
		if (FCode.equalsIgnoreCase("bak_3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak_3 = FValue.trim();
			}
			else
				bak_3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKYBTBQTRANSDETAILSchema other = (LKYBTBQTRANSDETAILSchema)otherObject;
		return
			(BANKCODE == null ? other.getBANKCODE() == null : BANKCODE.equals(other.getBANKCODE()))
			&& (STATETYPE == null ? other.getSTATETYPE() == null : STATETYPE.equals(other.getSTATETYPE()))
			&& (CONTNO == null ? other.getCONTNO() == null : CONTNO.equals(other.getCONTNO()))
			&& (PRTNO == null ? other.getPRTNO() == null : PRTNO.equals(other.getPRTNO()))
			&& (APPNTNAME == null ? other.getAPPNTNAME() == null : APPNTNAME.equals(other.getAPPNTNAME()))
			&& (APPNTIDTYPE == null ? other.getAPPNTIDTYPE() == null : APPNTIDTYPE.equals(other.getAPPNTIDTYPE()))
			&& (APPNTIDNO == null ? other.getAPPNTIDNO() == null : APPNTIDNO.equals(other.getAPPNTIDNO()))
			&& (MAKEDATE == null ? other.getMAKEDATE() == null : MAKEDATE.equals(other.getMAKEDATE()))
			&& (MAKETIME == null ? other.getMAKETIME() == null : MAKETIME.equals(other.getMAKETIME()))
			&& (MODIFYDATE == null ? other.getMODIFYDATE() == null : MODIFYDATE.equals(other.getMODIFYDATE()))
			&& (MODIFYTIME == null ? other.getMODIFYTIME() == null : MODIFYTIME.equals(other.getMODIFYTIME()))
			&& (RETURNSTATE == null ? other.getRETURNSTATE() == null : RETURNSTATE.equals(other.getRETURNSTATE()))
			&& (RETURNERRINFO == null ? other.getRETURNERRINFO() == null : RETURNERRINFO.equals(other.getRETURNERRINFO()))
			&& (bak_1 == null ? other.getbak_1() == null : bak_1.equals(other.getbak_1()))
			&& (bak_2 == null ? other.getbak_2() == null : bak_2.equals(other.getbak_2()))
			&& (bak_3 == null ? other.getbak_3() == null : bak_3.equals(other.getbak_3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BANKCODE") ) {
			return 0;
		}
		if( strFieldName.equals("STATETYPE") ) {
			return 1;
		}
		if( strFieldName.equals("CONTNO") ) {
			return 2;
		}
		if( strFieldName.equals("PRTNO") ) {
			return 3;
		}
		if( strFieldName.equals("APPNTNAME") ) {
			return 4;
		}
		if( strFieldName.equals("APPNTIDTYPE") ) {
			return 5;
		}
		if( strFieldName.equals("APPNTIDNO") ) {
			return 6;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return 7;
		}
		if( strFieldName.equals("MAKETIME") ) {
			return 8;
		}
		if( strFieldName.equals("MODIFYDATE") ) {
			return 9;
		}
		if( strFieldName.equals("MODIFYTIME") ) {
			return 10;
		}
		if( strFieldName.equals("RETURNSTATE") ) {
			return 11;
		}
		if( strFieldName.equals("RETURNERRINFO") ) {
			return 12;
		}
		if( strFieldName.equals("bak_1") ) {
			return 13;
		}
		if( strFieldName.equals("bak_2") ) {
			return 14;
		}
		if( strFieldName.equals("bak_3") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BANKCODE";
				break;
			case 1:
				strFieldName = "STATETYPE";
				break;
			case 2:
				strFieldName = "CONTNO";
				break;
			case 3:
				strFieldName = "PRTNO";
				break;
			case 4:
				strFieldName = "APPNTNAME";
				break;
			case 5:
				strFieldName = "APPNTIDTYPE";
				break;
			case 6:
				strFieldName = "APPNTIDNO";
				break;
			case 7:
				strFieldName = "MAKEDATE";
				break;
			case 8:
				strFieldName = "MAKETIME";
				break;
			case 9:
				strFieldName = "MODIFYDATE";
				break;
			case 10:
				strFieldName = "MODIFYTIME";
				break;
			case 11:
				strFieldName = "RETURNSTATE";
				break;
			case 12:
				strFieldName = "RETURNERRINFO";
				break;
			case 13:
				strFieldName = "bak_1";
				break;
			case 14:
				strFieldName = "bak_2";
				break;
			case 15:
				strFieldName = "bak_3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BANKCODE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("STATETYPE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CONTNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PRTNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("APPNTNAME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("APPNTIDTYPE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("APPNTIDNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MAKETIME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MODIFYDATE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MODIFYTIME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RETURNSTATE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RETURNERRINFO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak_1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak_2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak_3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
