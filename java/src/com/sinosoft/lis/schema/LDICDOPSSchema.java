/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDICDOPSDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LDICDOPSSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2005-03-08
 */
public class LDICDOPSSchema implements Schema
{
    // @Field
    /** 手术icd代码 */
    private String ICDOPSCode;
    /** 手术icd名称 */
    private String ICDOPSName;
    /** 手术等级 */
    private String OpsGrag;
    /** 说明 */
    private String Explai;
    /** 使用频率级别 */
    private int FrequencyFlag;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDICDOPSSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ICDOPSCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getICDOPSCode()
    {
        if (ICDOPSCode != null && !ICDOPSCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ICDOPSCode = StrTool.unicodeToGBK(ICDOPSCode);
        }
        return ICDOPSCode;
    }

    public void setICDOPSCode(String aICDOPSCode)
    {
        ICDOPSCode = aICDOPSCode;
    }

    public String getICDOPSName()
    {
        if (ICDOPSName != null && !ICDOPSName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ICDOPSName = StrTool.unicodeToGBK(ICDOPSName);
        }
        return ICDOPSName;
    }

    public void setICDOPSName(String aICDOPSName)
    {
        ICDOPSName = aICDOPSName;
    }

    public String getOpsGrag()
    {
        if (OpsGrag != null && !OpsGrag.equals("") && SysConst.CHANGECHARSET == true)
        {
            OpsGrag = StrTool.unicodeToGBK(OpsGrag);
        }
        return OpsGrag;
    }

    public void setOpsGrag(String aOpsGrag)
    {
        OpsGrag = aOpsGrag;
    }

    public String getExplai()
    {
        if (Explai != null && !Explai.equals("") && SysConst.CHANGECHARSET == true)
        {
            Explai = StrTool.unicodeToGBK(Explai);
        }
        return Explai;
    }

    public void setExplai(String aExplai)
    {
        Explai = aExplai;
    }

    public int getFrequencyFlag()
    {
        return FrequencyFlag;
    }

    public void setFrequencyFlag(int aFrequencyFlag)
    {
        FrequencyFlag = aFrequencyFlag;
    }

    public void setFrequencyFlag(String aFrequencyFlag)
    {
        if (aFrequencyFlag != null && !aFrequencyFlag.equals(""))
        {
            Integer tInteger = new Integer(aFrequencyFlag);
            int i = tInteger.intValue();
            FrequencyFlag = i;
        }
    }


    /**
     * 使用另外一个 LDICDOPSSchema 对象给 Schema 赋值
     * @param: aLDICDOPSSchema LDICDOPSSchema
     **/
    public void setSchema(LDICDOPSSchema aLDICDOPSSchema)
    {
        this.ICDOPSCode = aLDICDOPSSchema.getICDOPSCode();
        this.ICDOPSName = aLDICDOPSSchema.getICDOPSName();
        this.OpsGrag = aLDICDOPSSchema.getOpsGrag();
        this.Explai = aLDICDOPSSchema.getExplai();
        this.FrequencyFlag = aLDICDOPSSchema.getFrequencyFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ICDOPSCode") == null)
            {
                this.ICDOPSCode = null;
            }
            else
            {
                this.ICDOPSCode = rs.getString("ICDOPSCode").trim();
            }

            if (rs.getString("ICDOPSName") == null)
            {
                this.ICDOPSName = null;
            }
            else
            {
                this.ICDOPSName = rs.getString("ICDOPSName").trim();
            }

            if (rs.getString("OpsGrag") == null)
            {
                this.OpsGrag = null;
            }
            else
            {
                this.OpsGrag = rs.getString("OpsGrag").trim();
            }

            if (rs.getString("Explai") == null)
            {
                this.Explai = null;
            }
            else
            {
                this.Explai = rs.getString("Explai").trim();
            }

            this.FrequencyFlag = rs.getInt("FrequencyFlag");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDICDOPSSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDICDOPSSchema getSchema()
    {
        LDICDOPSSchema aLDICDOPSSchema = new LDICDOPSSchema();
        aLDICDOPSSchema.setSchema(this);
        return aLDICDOPSSchema;
    }

    public LDICDOPSDB getDB()
    {
        LDICDOPSDB aDBOper = new LDICDOPSDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDICDOPS描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ICDOPSCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ICDOPSName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OpsGrag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Explai)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(FrequencyFlag);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDICDOPS>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ICDOPSCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            ICDOPSName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            OpsGrag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            Explai = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            FrequencyFlag = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDICDOPSSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ICDOPSCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ICDOPSCode));
        }
        if (FCode.equals("ICDOPSName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ICDOPSName));
        }
        if (FCode.equals("OpsGrag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OpsGrag));
        }
        if (FCode.equals("Explai"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Explai));
        }
        if (FCode.equals("FrequencyFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FrequencyFlag));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ICDOPSCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ICDOPSName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OpsGrag);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Explai);
                break;
            case 4:
                strFieldValue = String.valueOf(FrequencyFlag);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ICDOPSCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ICDOPSCode = FValue.trim();
            }
            else
            {
                ICDOPSCode = null;
            }
        }
        if (FCode.equals("ICDOPSName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ICDOPSName = FValue.trim();
            }
            else
            {
                ICDOPSName = null;
            }
        }
        if (FCode.equals("OpsGrag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OpsGrag = FValue.trim();
            }
            else
            {
                OpsGrag = null;
            }
        }
        if (FCode.equals("Explai"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Explai = FValue.trim();
            }
            else
            {
                Explai = null;
            }
        }
        if (FCode.equals("FrequencyFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                FrequencyFlag = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDICDOPSSchema other = (LDICDOPSSchema) otherObject;
        return
                ICDOPSCode.equals(other.getICDOPSCode())
                && ICDOPSName.equals(other.getICDOPSName())
                && OpsGrag.equals(other.getOpsGrag())
                && Explai.equals(other.getExplai())
                && FrequencyFlag == other.getFrequencyFlag();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ICDOPSCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ICDOPSName"))
        {
            return 1;
        }
        if (strFieldName.equals("OpsGrag"))
        {
            return 2;
        }
        if (strFieldName.equals("Explai"))
        {
            return 3;
        }
        if (strFieldName.equals("FrequencyFlag"))
        {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ICDOPSCode";
                break;
            case 1:
                strFieldName = "ICDOPSName";
                break;
            case 2:
                strFieldName = "OpsGrag";
                break;
            case 3:
                strFieldName = "Explai";
                break;
            case 4:
                strFieldName = "FrequencyFlag";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ICDOPSCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ICDOPSName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OpsGrag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Explai"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FrequencyFlag"))
        {
            return Schema.TYPE_INT;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
