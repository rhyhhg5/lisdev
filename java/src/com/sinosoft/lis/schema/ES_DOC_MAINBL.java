package com.sinosoft.lis.schema;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ES_DOC_MAINBL {
	 public CErrors mErrors = new CErrors();
	 private VData Result = new VData();
	 /** 往后面传输数据的容器 */
	 private VData tVData;
	
	 /** 业务处理相关变量 */
	 private String Flag,Prtno,FlagStr,Content;
	 private String Docid_tb27,Docid_tb28; 
	 private int NumPages_tb27,NumPages_tb28;
	 //装载TB27类型影像件的pageid
	 public SSRS PageId;
	 //数据库查询对象
	 public ExeSQL exeSql = new ExeSQL();
	 TransferData tTransferData = new TransferData();
	 PubSubmit tPubSubmit = new PubSubmit();
	 MMap tmap = new MMap();
	 /**
		 * 数据提交：外部数据通过该接口传递进来进行业务处理
		 * @param cInputData
		 * @return
		 */
		public boolean submitData(VData cInputData) {
			this.tVData = cInputData;
			//1、获取数据
			if(!getInputData()) {
				return false;
			}
			//2、检查数据
			if(!checkData()) {
				return false;
			}
			//3、处理数据
			if(!dealData()) {
				return false;
			}
			return true;
		}

		/**
		 * 业务处理
		 * 1、直接添加补扫影像件
		 * 2、替换原有扫描件
		 * 3、删除指定页码影像件
		 * @return
		 */
		private boolean dealData() {
			if("1".equals(Flag)) {
				return insert();
			}
			if("2".equals(Flag)) {
				return update();
			}
			if("3".equals(Flag)) {
				return delete();
			}
			return true;
		}
	  
	    /**
		 * 检查是什么业务形式
		 * @return
		 */
		private boolean checkData() {
			System.out.println("开始检查数据...");
			if("1".equals(Flag)) {
				System.out.println("业务形式：直接添加补扫影像件！");
			}
			if("2".equals(Flag)) {
				System.out.println("业务形式：替换原有扫描件！");
			}
			if("3".equals(Flag)) {
				System.out.println("业务形式：删除指定页码影像件！");
			}
			System.out.println("数据检查完毕...");
			return true;
		}
	   //添加处理逻辑 
	   public boolean insert(){
		   String getTb27Sql = " select docid,numpages from es_doc_main where doccode like '"+Prtno+"%' and subtype = 'TB27' with ur ";
		   SSRS ssrs = exeSql.execSQL(getTb27Sql);
		   this.Docid_tb27 = ssrs.GetText(1, 1);
		   this.NumPages_tb27 = Integer.parseInt(ssrs.GetText(1, 2));
		   String sql1 = "delete from es_doc_main where docid = '"+Docid_tb27+"' and subtype='TB27'";
		   String sql2 = "update es_doc_main set numpages='"+(NumPages_tb27+NumPages_tb28)+"',modifydate=current date, modifytime=current time where Docid='"+Docid_tb28+"' and subtype='TB28' with ur";
		   tmap.put(sql1, "DELETE");
		   tmap.put(sql2, "UPDATE");
		   for(int i = 1;i<=NumPages_tb27;i++){
			   String sql3 = "update es_doc_pages set docid = '"+Docid_tb28+"',pagecode ='"+(NumPages_tb28+i)+"',modifydate=current date, modifytime=current time where docid ='"+Docid_tb27+"' and pagecode ='" +(i)+"' with ur";
			   tmap.put(sql3, "UPDATE");
		   }
		   String sql4 = "delete from es_doc_relation where docid='"+Docid_tb27+"' and subtype='TB27' with ur";
		   tmap.put(sql4, "DELETE");
		   Result.clear();
		   Result.add(tmap);
		   if(!tPubSubmit.submitData(Result, "")){
				System.out.println("数据出错");
				FlagStr="Fail";
				Content="处理失败!";
			}else{
				FlagStr="true";
				Content="处理成功!";
			}

		  return true;
	   }
	   //替换处理逻辑
	   public boolean update(){
		   String getTb27Sql = " select docid,numpages from es_doc_main where doccode like '"+Prtno+"%' and subtype = 'TB27' with ur ";
		   SSRS ssrs = exeSql.execSQL(getTb27Sql);
		   this.Docid_tb27 = ssrs.GetText(1, 1);
		   this.NumPages_tb27 = Integer.parseInt(ssrs.GetText(1, 2));
		   int page1 = Integer.parseInt((String)tTransferData.getValueByName("Page1"));
		   int page2 = Integer.parseInt((String)tTransferData.getValueByName("Page2"));
		   System.out.println("page1:"+page1);
		   System.out.println("page2:"+page2);
		   String sql1 = "delete from es_doc_main where Docid='"+Docid_tb27+"' and subtype='TB27'";
		   tmap.put(sql1, "DELETE");
		   /*
		    * 此处循环，从所需要调整的首页开始调整
		    */
		   for(int i = 0;i<=(page2-page1);i++){
			   String sql2 = "delete from es_doc_pages where pagecode = '"+(page1+i)+"' and docid ='"+Docid_tb28+"'";
			   String sql3 = "update es_doc_pages set docid = '"+Docid_tb28+"',pagecode ='"+(page1+i)+"',modifydate=current date, modifytime=current time where pagecode ='" +(i+1)+"' and docid ='"+Docid_tb27+"'";
			   tmap.put(sql2, "DELETE");
			   tmap.put(sql3, "UPDATE");
		   }
		   String sql4 = "delete from es_doc_relation where docid='"+Docid_tb27+"' and subtype='TB27'";
		   tmap.put(sql4, "DELETE");
		   Result.clear();
		   Result.add(tmap);
		   tPubSubmit.submitData(Result, "UPDATE");
		  /* if(!tPubSubmit.submitData(Result, "UPDATE")){
				System.out.println("数据出错");
				FlagStr="Fail";
				Content="处理失败!";
			}else{
				FlagStr="true";
				Content="处理成功!";
			}*/
		  return true;
	   }
	   //删除处理
	   public boolean delete(){
		   int page3 = Integer.parseInt((String)tTransferData.getValueByName("Page3"));
		   int page4 = Integer.parseInt((String)tTransferData.getValueByName("Page4"));
		   String sql1 = "update es_doc_main set numpages='"+(NumPages_tb28-page4+page3-1)+"' where docid ='"+Docid_tb28+"'";
		   tmap.put(sql1, "UPDATE");
		   for(int i = page3;i<=page4;i++){
			   String sql2 = "delete from es_doc_pages where pagecode='"+i+"' and docid='"+Docid_tb28+"'";
			   tmap.put(sql2, "DELETE");
		   }
		   for(int i = 0;i<(NumPages_tb28-page4);i++){
			   String sql3 = "update es_doc_pages set pagecode = '"+(page3+i)+"' where pagecode = '"+(page4+i+1)+"' and docid='"+Docid_tb28+"'";
			   tmap.put(sql3, "UPDATE");
		   }
		   Result.clear();
		   Result.add(tmap);
		   if(!tPubSubmit.submitData(Result, "")){
				System.out.println("数据出错");
				FlagStr="Fail";
				Content="处理失败!";
			}else{
				FlagStr="true";
				Content="处理成功!";
			}
		  return true;
	   }
	   
	   /**
		 * 获取传递过来的数据
		 * @return
		 */
		private boolean getInputData() {
			System.out.println("开始获取数据...");
			tTransferData = (TransferData)tVData.getObjectByObjectName("TransferData", 0);
			Prtno = (String)tTransferData.getValueByName("Prtno");
			Flag = (String)tTransferData.getValueByName("Flag");
			System.out.println("获取到的Prtno为："+Prtno);
			System.out.println("获取到的Flag为："+Flag);
			String getTb28Sql = " select docid,numpages from es_doc_main where doccode like '"+Prtno+"%' and subtype = 'TB28' with ur ";
			//SSRS ssrs = exeSql.execSQL(getTb27Sql);
			SSRS ssrs2 = exeSql.execSQL(getTb28Sql);		
			//this.Docid_tb27 = ssrs.GetText(1, 1);
			this.Docid_tb28 = ssrs2.GetText(1, 1);
			//this.NumPages_tb27 = Integer.parseInt(ssrs.GetText(1, 2));
			this.NumPages_tb28 = Integer.parseInt(ssrs2.GetText(1, 2));
			/*String getPageId = "select pageid from es_doc_pages where docid = '"+Docid_tb27+"' with ur ";
			this.PageId = exeSql.execSQL(getPageId);
			System.out.println("Docid_tb27:"+Docid_tb27+" NumPages_tb27:"+NumPages_tb27+"\r\nDocid_tb28:"+Docid_tb28+" NumPages_tb28:"+NumPages_tb28);
			System.out.println("输出获取到的pageid：");
			for(int i=1; i<=PageId.getMaxRow(); i++) {
				System.out.println("pageid_"+i+": "+PageId.GetText(i, 1));
			}*/
			System.out.println("数据获取完毕...");
			return true;
		}
		

}
