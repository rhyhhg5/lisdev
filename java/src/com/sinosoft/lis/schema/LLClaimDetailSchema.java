/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLClaimDetailDB;

/*
 * <p>ClassName: LLClaimDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: llclaim
 * @CreateDate：2012-11-02
 */
public class LLClaimDetailSchema implements Schema, Cloneable
{
	// @Field
	/** 立案号 */
	private String RgtNo;
	/** 拒赔号 */
	private String DeclineNo;
	/** 赔案号 */
	private String ClmNo;
	/** 保单号 */
	private String PolNo;
	/** 给付责任编码 */
	private String GetDutyCode;
	/** 给付责任类型 */
	private String GetDutyKind;
	/** 分案号 */
	private String CaseNo;
	/** 受理事故号 */
	private String CaseRelaNo;
	/** 统计类别 */
	private String StatType;
	/** 集体合同号 */
	private String GrpContNo;
	/** 集体保单号 */
	private String GrpPolNo;
	/** 个单合同号 */
	private String ContNo;
	/** 险类代码 */
	private String KindCode;
	/** 险种代码 */
	private String RiskCode;
	/** 险种版本号 */
	private String RiskVer;
	/** 保单管理机构 */
	private String PolMngCom;
	/** 销售渠道 */
	private String SaleChnl;
	/** 代理人代码 */
	private String AgentCode;
	/** 代理人组别 */
	private String AgentGroup;
	/** 账单金额 */
	private double TabFeeMoney;
	/** 理算金额 */
	private double ClaimMoney;
	/** 拒付金额 */
	private double DeclineAmnt;
	/** 溢额 */
	private double OverAmnt;
	/** 核算赔付金额 */
	private double StandPay;
	/** 核赔赔付金额 */
	private double RealPay;
	/** 先期给付金额 */
	private double PreGiveAmnt;
	/** 自费给付金额 */
	private double SelfGiveAmnt;
	/** 不合理费用金额 */
	private double RefuseAmnt;
	/** 其它已给付 */
	private double OtherAmnt;
	/** 免赔额 */
	private double OutDutyAmnt;
	/** 免赔比例 */
	private double OutDutyRate;
	/** 通融给付金额 */
	private double ApproveAmnt;
	/** 通融给付原因代码 */
	private String ApproveCode;
	/** 协议给付 */
	private double AgreeAmnt;
	/** 协议给付原因代码 */
	private String AgreeCode;
	/** 责任编码 */
	private String DutyCode;
	/** 赔付结论 */
	private String GiveType;
	/** 赔付结论描述 */
	private String GiveTypeDesc;
	/** 赔付结论依据 */
	private String GiveReason;
	/** 赔付结论依据描述 */
	private String GiveReasonDesc;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 备注框 */
	private String REMARK;

	public static final int FIELDNUM = 47;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLClaimDetailSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[6];
		pk[0] = "ClmNo";
		pk[1] = "PolNo";
		pk[2] = "GetDutyCode";
		pk[3] = "GetDutyKind";
		pk[4] = "CaseNo";
		pk[5] = "CaseRelaNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLClaimDetailSchema cloned = (LLClaimDetailSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
		RgtNo = aRgtNo;
	}
	public String getDeclineNo()
	{
		return DeclineNo;
	}
	public void setDeclineNo(String aDeclineNo)
	{
		DeclineNo = aDeclineNo;
	}
	public String getClmNo()
	{
		return ClmNo;
	}
	public void setClmNo(String aClmNo)
	{
		ClmNo = aClmNo;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getGetDutyCode()
	{
		return GetDutyCode;
	}
	public void setGetDutyCode(String aGetDutyCode)
	{
		GetDutyCode = aGetDutyCode;
	}
	public String getGetDutyKind()
	{
		return GetDutyKind;
	}
	public void setGetDutyKind(String aGetDutyKind)
	{
		GetDutyKind = aGetDutyKind;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getCaseRelaNo()
	{
		return CaseRelaNo;
	}
	public void setCaseRelaNo(String aCaseRelaNo)
	{
		CaseRelaNo = aCaseRelaNo;
	}
	public String getStatType()
	{
		return StatType;
	}
	public void setStatType(String aStatType)
	{
		StatType = aStatType;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpPolNo()
	{
		return GrpPolNo;
	}
	public void setGrpPolNo(String aGrpPolNo)
	{
		GrpPolNo = aGrpPolNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getKindCode()
	{
		return KindCode;
	}
	public void setKindCode(String aKindCode)
	{
		KindCode = aKindCode;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskVer()
	{
		return RiskVer;
	}
	public void setRiskVer(String aRiskVer)
	{
		RiskVer = aRiskVer;
	}
	public String getPolMngCom()
	{
		return PolMngCom;
	}
	public void setPolMngCom(String aPolMngCom)
	{
		PolMngCom = aPolMngCom;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public double getTabFeeMoney()
	{
		return TabFeeMoney;
	}
	public void setTabFeeMoney(double aTabFeeMoney)
	{
		TabFeeMoney = Arith.round(aTabFeeMoney,2);
	}
	public void setTabFeeMoney(String aTabFeeMoney)
	{
		if (aTabFeeMoney != null && !aTabFeeMoney.equals(""))
		{
			Double tDouble = new Double(aTabFeeMoney);
			double d = tDouble.doubleValue();
                TabFeeMoney = Arith.round(d,2);
		}
	}

	public double getClaimMoney()
	{
		return ClaimMoney;
	}
	public void setClaimMoney(double aClaimMoney)
	{
		ClaimMoney = Arith.round(aClaimMoney,2);
	}
	public void setClaimMoney(String aClaimMoney)
	{
		if (aClaimMoney != null && !aClaimMoney.equals(""))
		{
			Double tDouble = new Double(aClaimMoney);
			double d = tDouble.doubleValue();
                ClaimMoney = Arith.round(d,2);
		}
	}

	public double getDeclineAmnt()
	{
		return DeclineAmnt;
	}
	public void setDeclineAmnt(double aDeclineAmnt)
	{
		DeclineAmnt = Arith.round(aDeclineAmnt,2);
	}
	public void setDeclineAmnt(String aDeclineAmnt)
	{
		if (aDeclineAmnt != null && !aDeclineAmnt.equals(""))
		{
			Double tDouble = new Double(aDeclineAmnt);
			double d = tDouble.doubleValue();
                DeclineAmnt = Arith.round(d,2);
		}
	}

	public double getOverAmnt()
	{
		return OverAmnt;
	}
	public void setOverAmnt(double aOverAmnt)
	{
		OverAmnt = Arith.round(aOverAmnt,2);
	}
	public void setOverAmnt(String aOverAmnt)
	{
		if (aOverAmnt != null && !aOverAmnt.equals(""))
		{
			Double tDouble = new Double(aOverAmnt);
			double d = tDouble.doubleValue();
                OverAmnt = Arith.round(d,2);
		}
	}

	public double getStandPay()
	{
		return StandPay;
	}
	public void setStandPay(double aStandPay)
	{
		StandPay = Arith.round(aStandPay,2);
	}
	public void setStandPay(String aStandPay)
	{
		if (aStandPay != null && !aStandPay.equals(""))
		{
			Double tDouble = new Double(aStandPay);
			double d = tDouble.doubleValue();
                StandPay = Arith.round(d,2);
		}
	}

	public double getRealPay()
	{
		return RealPay;
	}
	public void setRealPay(double aRealPay)
	{
		RealPay = Arith.round(aRealPay,2);
	}
	public void setRealPay(String aRealPay)
	{
		if (aRealPay != null && !aRealPay.equals(""))
		{
			Double tDouble = new Double(aRealPay);
			double d = tDouble.doubleValue();
                RealPay = Arith.round(d,2);
		}
	}

	public double getPreGiveAmnt()
	{
		return PreGiveAmnt;
	}
	public void setPreGiveAmnt(double aPreGiveAmnt)
	{
		PreGiveAmnt = Arith.round(aPreGiveAmnt,2);
	}
	public void setPreGiveAmnt(String aPreGiveAmnt)
	{
		if (aPreGiveAmnt != null && !aPreGiveAmnt.equals(""))
		{
			Double tDouble = new Double(aPreGiveAmnt);
			double d = tDouble.doubleValue();
                PreGiveAmnt = Arith.round(d,2);
		}
	}

	public double getSelfGiveAmnt()
	{
		return SelfGiveAmnt;
	}
	public void setSelfGiveAmnt(double aSelfGiveAmnt)
	{
		SelfGiveAmnt = Arith.round(aSelfGiveAmnt,2);
	}
	public void setSelfGiveAmnt(String aSelfGiveAmnt)
	{
		if (aSelfGiveAmnt != null && !aSelfGiveAmnt.equals(""))
		{
			Double tDouble = new Double(aSelfGiveAmnt);
			double d = tDouble.doubleValue();
                SelfGiveAmnt = Arith.round(d,2);
		}
	}

	public double getRefuseAmnt()
	{
		return RefuseAmnt;
	}
	public void setRefuseAmnt(double aRefuseAmnt)
	{
		RefuseAmnt = Arith.round(aRefuseAmnt,2);
	}
	public void setRefuseAmnt(String aRefuseAmnt)
	{
		if (aRefuseAmnt != null && !aRefuseAmnt.equals(""))
		{
			Double tDouble = new Double(aRefuseAmnt);
			double d = tDouble.doubleValue();
                RefuseAmnt = Arith.round(d,2);
		}
	}

	public double getOtherAmnt()
	{
		return OtherAmnt;
	}
	public void setOtherAmnt(double aOtherAmnt)
	{
		OtherAmnt = Arith.round(aOtherAmnt,2);
	}
	public void setOtherAmnt(String aOtherAmnt)
	{
		if (aOtherAmnt != null && !aOtherAmnt.equals(""))
		{
			Double tDouble = new Double(aOtherAmnt);
			double d = tDouble.doubleValue();
                OtherAmnt = Arith.round(d,2);
		}
	}

	public double getOutDutyAmnt()
	{
		return OutDutyAmnt;
	}
	public void setOutDutyAmnt(double aOutDutyAmnt)
	{
		OutDutyAmnt = Arith.round(aOutDutyAmnt,2);
	}
	public void setOutDutyAmnt(String aOutDutyAmnt)
	{
		if (aOutDutyAmnt != null && !aOutDutyAmnt.equals(""))
		{
			Double tDouble = new Double(aOutDutyAmnt);
			double d = tDouble.doubleValue();
                OutDutyAmnt = Arith.round(d,2);
		}
	}

	public double getOutDutyRate()
	{
		return OutDutyRate;
	}
	public void setOutDutyRate(double aOutDutyRate)
	{
		OutDutyRate = Arith.round(aOutDutyRate,2);
	}
	public void setOutDutyRate(String aOutDutyRate)
	{
		if (aOutDutyRate != null && !aOutDutyRate.equals(""))
		{
			Double tDouble = new Double(aOutDutyRate);
			double d = tDouble.doubleValue();
                OutDutyRate = Arith.round(d,2);
		}
	}

	public double getApproveAmnt()
	{
		return ApproveAmnt;
	}
	public void setApproveAmnt(double aApproveAmnt)
	{
		ApproveAmnt = Arith.round(aApproveAmnt,2);
	}
	public void setApproveAmnt(String aApproveAmnt)
	{
		if (aApproveAmnt != null && !aApproveAmnt.equals(""))
		{
			Double tDouble = new Double(aApproveAmnt);
			double d = tDouble.doubleValue();
                ApproveAmnt = Arith.round(d,2);
		}
	}

	public String getApproveCode()
	{
		return ApproveCode;
	}
	public void setApproveCode(String aApproveCode)
	{
		ApproveCode = aApproveCode;
	}
	public double getAgreeAmnt()
	{
		return AgreeAmnt;
	}
	public void setAgreeAmnt(double aAgreeAmnt)
	{
		AgreeAmnt = Arith.round(aAgreeAmnt,2);
	}
	public void setAgreeAmnt(String aAgreeAmnt)
	{
		if (aAgreeAmnt != null && !aAgreeAmnt.equals(""))
		{
			Double tDouble = new Double(aAgreeAmnt);
			double d = tDouble.doubleValue();
                AgreeAmnt = Arith.round(d,2);
		}
	}

	public String getAgreeCode()
	{
		return AgreeCode;
	}
	public void setAgreeCode(String aAgreeCode)
	{
		AgreeCode = aAgreeCode;
	}
	public String getDutyCode()
	{
		return DutyCode;
	}
	public void setDutyCode(String aDutyCode)
	{
		DutyCode = aDutyCode;
	}
	public String getGiveType()
	{
		return GiveType;
	}
	public void setGiveType(String aGiveType)
	{
		GiveType = aGiveType;
	}
	public String getGiveTypeDesc()
	{
		return GiveTypeDesc;
	}
	public void setGiveTypeDesc(String aGiveTypeDesc)
	{
		GiveTypeDesc = aGiveTypeDesc;
	}
	public String getGiveReason()
	{
		return GiveReason;
	}
	public void setGiveReason(String aGiveReason)
	{
		GiveReason = aGiveReason;
	}
	public String getGiveReasonDesc()
	{
		return GiveReasonDesc;
	}
	public void setGiveReasonDesc(String aGiveReasonDesc)
	{
		GiveReasonDesc = aGiveReasonDesc;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getREMARK()
	{
		return REMARK;
	}
	public void setREMARK(String aREMARK)
	{
		REMARK = aREMARK;
	}

	/**
	* 使用另外一个 LLClaimDetailSchema 对象给 Schema 赋值
	* @param: aLLClaimDetailSchema LLClaimDetailSchema
	**/
	public void setSchema(LLClaimDetailSchema aLLClaimDetailSchema)
	{
		this.RgtNo = aLLClaimDetailSchema.getRgtNo();
		this.DeclineNo = aLLClaimDetailSchema.getDeclineNo();
		this.ClmNo = aLLClaimDetailSchema.getClmNo();
		this.PolNo = aLLClaimDetailSchema.getPolNo();
		this.GetDutyCode = aLLClaimDetailSchema.getGetDutyCode();
		this.GetDutyKind = aLLClaimDetailSchema.getGetDutyKind();
		this.CaseNo = aLLClaimDetailSchema.getCaseNo();
		this.CaseRelaNo = aLLClaimDetailSchema.getCaseRelaNo();
		this.StatType = aLLClaimDetailSchema.getStatType();
		this.GrpContNo = aLLClaimDetailSchema.getGrpContNo();
		this.GrpPolNo = aLLClaimDetailSchema.getGrpPolNo();
		this.ContNo = aLLClaimDetailSchema.getContNo();
		this.KindCode = aLLClaimDetailSchema.getKindCode();
		this.RiskCode = aLLClaimDetailSchema.getRiskCode();
		this.RiskVer = aLLClaimDetailSchema.getRiskVer();
		this.PolMngCom = aLLClaimDetailSchema.getPolMngCom();
		this.SaleChnl = aLLClaimDetailSchema.getSaleChnl();
		this.AgentCode = aLLClaimDetailSchema.getAgentCode();
		this.AgentGroup = aLLClaimDetailSchema.getAgentGroup();
		this.TabFeeMoney = aLLClaimDetailSchema.getTabFeeMoney();
		this.ClaimMoney = aLLClaimDetailSchema.getClaimMoney();
		this.DeclineAmnt = aLLClaimDetailSchema.getDeclineAmnt();
		this.OverAmnt = aLLClaimDetailSchema.getOverAmnt();
		this.StandPay = aLLClaimDetailSchema.getStandPay();
		this.RealPay = aLLClaimDetailSchema.getRealPay();
		this.PreGiveAmnt = aLLClaimDetailSchema.getPreGiveAmnt();
		this.SelfGiveAmnt = aLLClaimDetailSchema.getSelfGiveAmnt();
		this.RefuseAmnt = aLLClaimDetailSchema.getRefuseAmnt();
		this.OtherAmnt = aLLClaimDetailSchema.getOtherAmnt();
		this.OutDutyAmnt = aLLClaimDetailSchema.getOutDutyAmnt();
		this.OutDutyRate = aLLClaimDetailSchema.getOutDutyRate();
		this.ApproveAmnt = aLLClaimDetailSchema.getApproveAmnt();
		this.ApproveCode = aLLClaimDetailSchema.getApproveCode();
		this.AgreeAmnt = aLLClaimDetailSchema.getAgreeAmnt();
		this.AgreeCode = aLLClaimDetailSchema.getAgreeCode();
		this.DutyCode = aLLClaimDetailSchema.getDutyCode();
		this.GiveType = aLLClaimDetailSchema.getGiveType();
		this.GiveTypeDesc = aLLClaimDetailSchema.getGiveTypeDesc();
		this.GiveReason = aLLClaimDetailSchema.getGiveReason();
		this.GiveReasonDesc = aLLClaimDetailSchema.getGiveReasonDesc();
		this.MngCom = aLLClaimDetailSchema.getMngCom();
		this.Operator = aLLClaimDetailSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLClaimDetailSchema.getMakeDate());
		this.MakeTime = aLLClaimDetailSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLClaimDetailSchema.getModifyDate());
		this.ModifyTime = aLLClaimDetailSchema.getModifyTime();
		this.REMARK = aLLClaimDetailSchema.getREMARK();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("DeclineNo") == null )
				this.DeclineNo = null;
			else
				this.DeclineNo = rs.getString("DeclineNo").trim();

			if( rs.getString("ClmNo") == null )
				this.ClmNo = null;
			else
				this.ClmNo = rs.getString("ClmNo").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("GetDutyCode") == null )
				this.GetDutyCode = null;
			else
				this.GetDutyCode = rs.getString("GetDutyCode").trim();

			if( rs.getString("GetDutyKind") == null )
				this.GetDutyKind = null;
			else
				this.GetDutyKind = rs.getString("GetDutyKind").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("CaseRelaNo") == null )
				this.CaseRelaNo = null;
			else
				this.CaseRelaNo = rs.getString("CaseRelaNo").trim();

			if( rs.getString("StatType") == null )
				this.StatType = null;
			else
				this.StatType = rs.getString("StatType").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpPolNo") == null )
				this.GrpPolNo = null;
			else
				this.GrpPolNo = rs.getString("GrpPolNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("KindCode") == null )
				this.KindCode = null;
			else
				this.KindCode = rs.getString("KindCode").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskVer") == null )
				this.RiskVer = null;
			else
				this.RiskVer = rs.getString("RiskVer").trim();

			if( rs.getString("PolMngCom") == null )
				this.PolMngCom = null;
			else
				this.PolMngCom = rs.getString("PolMngCom").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			this.TabFeeMoney = rs.getDouble("TabFeeMoney");
			this.ClaimMoney = rs.getDouble("ClaimMoney");
			this.DeclineAmnt = rs.getDouble("DeclineAmnt");
			this.OverAmnt = rs.getDouble("OverAmnt");
			this.StandPay = rs.getDouble("StandPay");
			this.RealPay = rs.getDouble("RealPay");
			this.PreGiveAmnt = rs.getDouble("PreGiveAmnt");
			this.SelfGiveAmnt = rs.getDouble("SelfGiveAmnt");
			this.RefuseAmnt = rs.getDouble("RefuseAmnt");
			this.OtherAmnt = rs.getDouble("OtherAmnt");
			this.OutDutyAmnt = rs.getDouble("OutDutyAmnt");
			this.OutDutyRate = rs.getDouble("OutDutyRate");
			this.ApproveAmnt = rs.getDouble("ApproveAmnt");
			if( rs.getString("ApproveCode") == null )
				this.ApproveCode = null;
			else
				this.ApproveCode = rs.getString("ApproveCode").trim();

			this.AgreeAmnt = rs.getDouble("AgreeAmnt");
			if( rs.getString("AgreeCode") == null )
				this.AgreeCode = null;
			else
				this.AgreeCode = rs.getString("AgreeCode").trim();

			if( rs.getString("DutyCode") == null )
				this.DutyCode = null;
			else
				this.DutyCode = rs.getString("DutyCode").trim();

			if( rs.getString("GiveType") == null )
				this.GiveType = null;
			else
				this.GiveType = rs.getString("GiveType").trim();

			if( rs.getString("GiveTypeDesc") == null )
				this.GiveTypeDesc = null;
			else
				this.GiveTypeDesc = rs.getString("GiveTypeDesc").trim();

			if( rs.getString("GiveReason") == null )
				this.GiveReason = null;
			else
				this.GiveReason = rs.getString("GiveReason").trim();

			if( rs.getString("GiveReasonDesc") == null )
				this.GiveReasonDesc = null;
			else
				this.GiveReasonDesc = rs.getString("GiveReasonDesc").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("REMARK") == null )
				this.REMARK = null;
			else
				this.REMARK = rs.getString("REMARK").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLClaimDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLClaimDetailSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLClaimDetailSchema getSchema()
	{
		LLClaimDetailSchema aLLClaimDetailSchema = new LLClaimDetailSchema();
		aLLClaimDetailSchema.setSchema(this);
		return aLLClaimDetailSchema;
	}

	public LLClaimDetailDB getDB()
	{
		LLClaimDetailDB aDBOper = new LLClaimDetailDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimDetail描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DeclineNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClmNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseRelaNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StatType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(KindCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskVer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolMngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TabFeeMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ClaimMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DeclineAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OverAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RealPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PreGiveAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SelfGiveAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RefuseAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OutDutyAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OutDutyRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ApproveAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AgreeAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgreeCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DutyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GiveType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GiveTypeDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GiveReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GiveReasonDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(REMARK));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimDetail>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			DeclineNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CaseRelaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			StatType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			PolMngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			TabFeeMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			ClaimMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			DeclineAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			OverAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			StandPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			RealPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			PreGiveAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			SelfGiveAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			RefuseAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			OtherAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			OutDutyAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			OutDutyRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			ApproveAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			AgreeAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).doubleValue();
			AgreeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			GiveType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			GiveTypeDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			GiveReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			GiveReasonDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			REMARK = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLClaimDetailSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("DeclineNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeclineNo));
		}
		if (FCode.equals("ClmNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClmNo));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("GetDutyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
		}
		if (FCode.equals("GetDutyKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("CaseRelaNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseRelaNo));
		}
		if (FCode.equals("StatType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StatType));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("KindCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskVer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
		}
		if (FCode.equals("PolMngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolMngCom));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("TabFeeMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TabFeeMoney));
		}
		if (FCode.equals("ClaimMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimMoney));
		}
		if (FCode.equals("DeclineAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeclineAmnt));
		}
		if (FCode.equals("OverAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OverAmnt));
		}
		if (FCode.equals("StandPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandPay));
		}
		if (FCode.equals("RealPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RealPay));
		}
		if (FCode.equals("PreGiveAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PreGiveAmnt));
		}
		if (FCode.equals("SelfGiveAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SelfGiveAmnt));
		}
		if (FCode.equals("RefuseAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RefuseAmnt));
		}
		if (FCode.equals("OtherAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherAmnt));
		}
		if (FCode.equals("OutDutyAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutDutyAmnt));
		}
		if (FCode.equals("OutDutyRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutDutyRate));
		}
		if (FCode.equals("ApproveAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveAmnt));
		}
		if (FCode.equals("ApproveCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
		}
		if (FCode.equals("AgreeAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgreeAmnt));
		}
		if (FCode.equals("AgreeCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgreeCode));
		}
		if (FCode.equals("DutyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DutyCode));
		}
		if (FCode.equals("GiveType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GiveType));
		}
		if (FCode.equals("GiveTypeDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GiveTypeDesc));
		}
		if (FCode.equals("GiveReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GiveReason));
		}
		if (FCode.equals("GiveReasonDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GiveReasonDesc));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("REMARK"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(REMARK));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(DeclineNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ClmNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CaseRelaNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(StatType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(KindCode);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(RiskVer);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(PolMngCom);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 19:
				strFieldValue = String.valueOf(TabFeeMoney);
				break;
			case 20:
				strFieldValue = String.valueOf(ClaimMoney);
				break;
			case 21:
				strFieldValue = String.valueOf(DeclineAmnt);
				break;
			case 22:
				strFieldValue = String.valueOf(OverAmnt);
				break;
			case 23:
				strFieldValue = String.valueOf(StandPay);
				break;
			case 24:
				strFieldValue = String.valueOf(RealPay);
				break;
			case 25:
				strFieldValue = String.valueOf(PreGiveAmnt);
				break;
			case 26:
				strFieldValue = String.valueOf(SelfGiveAmnt);
				break;
			case 27:
				strFieldValue = String.valueOf(RefuseAmnt);
				break;
			case 28:
				strFieldValue = String.valueOf(OtherAmnt);
				break;
			case 29:
				strFieldValue = String.valueOf(OutDutyAmnt);
				break;
			case 30:
				strFieldValue = String.valueOf(OutDutyRate);
				break;
			case 31:
				strFieldValue = String.valueOf(ApproveAmnt);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(ApproveCode);
				break;
			case 33:
				strFieldValue = String.valueOf(AgreeAmnt);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(AgreeCode);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(DutyCode);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(GiveType);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(GiveTypeDesc);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(GiveReason);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(GiveReasonDesc);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(REMARK);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("DeclineNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DeclineNo = FValue.trim();
			}
			else
				DeclineNo = null;
		}
		if (FCode.equalsIgnoreCase("ClmNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClmNo = FValue.trim();
			}
			else
				ClmNo = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyCode = FValue.trim();
			}
			else
				GetDutyCode = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyKind = FValue.trim();
			}
			else
				GetDutyKind = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseRelaNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseRelaNo = FValue.trim();
			}
			else
				CaseRelaNo = null;
		}
		if (FCode.equalsIgnoreCase("StatType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StatType = FValue.trim();
			}
			else
				StatType = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpPolNo = FValue.trim();
			}
			else
				GrpPolNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("KindCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				KindCode = FValue.trim();
			}
			else
				KindCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskVer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskVer = FValue.trim();
			}
			else
				RiskVer = null;
		}
		if (FCode.equalsIgnoreCase("PolMngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolMngCom = FValue.trim();
			}
			else
				PolMngCom = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("TabFeeMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TabFeeMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClaimMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ClaimMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("DeclineAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DeclineAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("OverAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OverAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("RealPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RealPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("PreGiveAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PreGiveAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("SelfGiveAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SelfGiveAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("RefuseAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RefuseAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("OutDutyAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OutDutyAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("OutDutyRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OutDutyRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ApproveAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ApproveAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("ApproveCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApproveCode = FValue.trim();
			}
			else
				ApproveCode = null;
		}
		if (FCode.equalsIgnoreCase("AgreeAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AgreeAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("AgreeCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgreeCode = FValue.trim();
			}
			else
				AgreeCode = null;
		}
		if (FCode.equalsIgnoreCase("DutyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DutyCode = FValue.trim();
			}
			else
				DutyCode = null;
		}
		if (FCode.equalsIgnoreCase("GiveType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GiveType = FValue.trim();
			}
			else
				GiveType = null;
		}
		if (FCode.equalsIgnoreCase("GiveTypeDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GiveTypeDesc = FValue.trim();
			}
			else
				GiveTypeDesc = null;
		}
		if (FCode.equalsIgnoreCase("GiveReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GiveReason = FValue.trim();
			}
			else
				GiveReason = null;
		}
		if (FCode.equalsIgnoreCase("GiveReasonDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GiveReasonDesc = FValue.trim();
			}
			else
				GiveReasonDesc = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("REMARK"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				REMARK = FValue.trim();
			}
			else
				REMARK = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLClaimDetailSchema other = (LLClaimDetailSchema)otherObject;
		return
			(RgtNo == null ? other.getRgtNo() == null : RgtNo.equals(other.getRgtNo()))
			&& (DeclineNo == null ? other.getDeclineNo() == null : DeclineNo.equals(other.getDeclineNo()))
			&& (ClmNo == null ? other.getClmNo() == null : ClmNo.equals(other.getClmNo()))
			&& (PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (GetDutyCode == null ? other.getGetDutyCode() == null : GetDutyCode.equals(other.getGetDutyCode()))
			&& (GetDutyKind == null ? other.getGetDutyKind() == null : GetDutyKind.equals(other.getGetDutyKind()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (CaseRelaNo == null ? other.getCaseRelaNo() == null : CaseRelaNo.equals(other.getCaseRelaNo()))
			&& (StatType == null ? other.getStatType() == null : StatType.equals(other.getStatType()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (GrpPolNo == null ? other.getGrpPolNo() == null : GrpPolNo.equals(other.getGrpPolNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (KindCode == null ? other.getKindCode() == null : KindCode.equals(other.getKindCode()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskVer == null ? other.getRiskVer() == null : RiskVer.equals(other.getRiskVer()))
			&& (PolMngCom == null ? other.getPolMngCom() == null : PolMngCom.equals(other.getPolMngCom()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& TabFeeMoney == other.getTabFeeMoney()
			&& ClaimMoney == other.getClaimMoney()
			&& DeclineAmnt == other.getDeclineAmnt()
			&& OverAmnt == other.getOverAmnt()
			&& StandPay == other.getStandPay()
			&& RealPay == other.getRealPay()
			&& PreGiveAmnt == other.getPreGiveAmnt()
			&& SelfGiveAmnt == other.getSelfGiveAmnt()
			&& RefuseAmnt == other.getRefuseAmnt()
			&& OtherAmnt == other.getOtherAmnt()
			&& OutDutyAmnt == other.getOutDutyAmnt()
			&& OutDutyRate == other.getOutDutyRate()
			&& ApproveAmnt == other.getApproveAmnt()
			&& (ApproveCode == null ? other.getApproveCode() == null : ApproveCode.equals(other.getApproveCode()))
			&& AgreeAmnt == other.getAgreeAmnt()
			&& (AgreeCode == null ? other.getAgreeCode() == null : AgreeCode.equals(other.getAgreeCode()))
			&& (DutyCode == null ? other.getDutyCode() == null : DutyCode.equals(other.getDutyCode()))
			&& (GiveType == null ? other.getGiveType() == null : GiveType.equals(other.getGiveType()))
			&& (GiveTypeDesc == null ? other.getGiveTypeDesc() == null : GiveTypeDesc.equals(other.getGiveTypeDesc()))
			&& (GiveReason == null ? other.getGiveReason() == null : GiveReason.equals(other.getGiveReason()))
			&& (GiveReasonDesc == null ? other.getGiveReasonDesc() == null : GiveReasonDesc.equals(other.getGiveReasonDesc()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (REMARK == null ? other.getREMARK() == null : REMARK.equals(other.getREMARK()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RgtNo") ) {
			return 0;
		}
		if( strFieldName.equals("DeclineNo") ) {
			return 1;
		}
		if( strFieldName.equals("ClmNo") ) {
			return 2;
		}
		if( strFieldName.equals("PolNo") ) {
			return 3;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return 4;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return 5;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 6;
		}
		if( strFieldName.equals("CaseRelaNo") ) {
			return 7;
		}
		if( strFieldName.equals("StatType") ) {
			return 8;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 9;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return 10;
		}
		if( strFieldName.equals("ContNo") ) {
			return 11;
		}
		if( strFieldName.equals("KindCode") ) {
			return 12;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 13;
		}
		if( strFieldName.equals("RiskVer") ) {
			return 14;
		}
		if( strFieldName.equals("PolMngCom") ) {
			return 15;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 16;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 17;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 18;
		}
		if( strFieldName.equals("TabFeeMoney") ) {
			return 19;
		}
		if( strFieldName.equals("ClaimMoney") ) {
			return 20;
		}
		if( strFieldName.equals("DeclineAmnt") ) {
			return 21;
		}
		if( strFieldName.equals("OverAmnt") ) {
			return 22;
		}
		if( strFieldName.equals("StandPay") ) {
			return 23;
		}
		if( strFieldName.equals("RealPay") ) {
			return 24;
		}
		if( strFieldName.equals("PreGiveAmnt") ) {
			return 25;
		}
		if( strFieldName.equals("SelfGiveAmnt") ) {
			return 26;
		}
		if( strFieldName.equals("RefuseAmnt") ) {
			return 27;
		}
		if( strFieldName.equals("OtherAmnt") ) {
			return 28;
		}
		if( strFieldName.equals("OutDutyAmnt") ) {
			return 29;
		}
		if( strFieldName.equals("OutDutyRate") ) {
			return 30;
		}
		if( strFieldName.equals("ApproveAmnt") ) {
			return 31;
		}
		if( strFieldName.equals("ApproveCode") ) {
			return 32;
		}
		if( strFieldName.equals("AgreeAmnt") ) {
			return 33;
		}
		if( strFieldName.equals("AgreeCode") ) {
			return 34;
		}
		if( strFieldName.equals("DutyCode") ) {
			return 35;
		}
		if( strFieldName.equals("GiveType") ) {
			return 36;
		}
		if( strFieldName.equals("GiveTypeDesc") ) {
			return 37;
		}
		if( strFieldName.equals("GiveReason") ) {
			return 38;
		}
		if( strFieldName.equals("GiveReasonDesc") ) {
			return 39;
		}
		if( strFieldName.equals("MngCom") ) {
			return 40;
		}
		if( strFieldName.equals("Operator") ) {
			return 41;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 42;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 43;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 44;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 45;
		}
		if( strFieldName.equals("REMARK") ) {
			return 46;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RgtNo";
				break;
			case 1:
				strFieldName = "DeclineNo";
				break;
			case 2:
				strFieldName = "ClmNo";
				break;
			case 3:
				strFieldName = "PolNo";
				break;
			case 4:
				strFieldName = "GetDutyCode";
				break;
			case 5:
				strFieldName = "GetDutyKind";
				break;
			case 6:
				strFieldName = "CaseNo";
				break;
			case 7:
				strFieldName = "CaseRelaNo";
				break;
			case 8:
				strFieldName = "StatType";
				break;
			case 9:
				strFieldName = "GrpContNo";
				break;
			case 10:
				strFieldName = "GrpPolNo";
				break;
			case 11:
				strFieldName = "ContNo";
				break;
			case 12:
				strFieldName = "KindCode";
				break;
			case 13:
				strFieldName = "RiskCode";
				break;
			case 14:
				strFieldName = "RiskVer";
				break;
			case 15:
				strFieldName = "PolMngCom";
				break;
			case 16:
				strFieldName = "SaleChnl";
				break;
			case 17:
				strFieldName = "AgentCode";
				break;
			case 18:
				strFieldName = "AgentGroup";
				break;
			case 19:
				strFieldName = "TabFeeMoney";
				break;
			case 20:
				strFieldName = "ClaimMoney";
				break;
			case 21:
				strFieldName = "DeclineAmnt";
				break;
			case 22:
				strFieldName = "OverAmnt";
				break;
			case 23:
				strFieldName = "StandPay";
				break;
			case 24:
				strFieldName = "RealPay";
				break;
			case 25:
				strFieldName = "PreGiveAmnt";
				break;
			case 26:
				strFieldName = "SelfGiveAmnt";
				break;
			case 27:
				strFieldName = "RefuseAmnt";
				break;
			case 28:
				strFieldName = "OtherAmnt";
				break;
			case 29:
				strFieldName = "OutDutyAmnt";
				break;
			case 30:
				strFieldName = "OutDutyRate";
				break;
			case 31:
				strFieldName = "ApproveAmnt";
				break;
			case 32:
				strFieldName = "ApproveCode";
				break;
			case 33:
				strFieldName = "AgreeAmnt";
				break;
			case 34:
				strFieldName = "AgreeCode";
				break;
			case 35:
				strFieldName = "DutyCode";
				break;
			case 36:
				strFieldName = "GiveType";
				break;
			case 37:
				strFieldName = "GiveTypeDesc";
				break;
			case 38:
				strFieldName = "GiveReason";
				break;
			case 39:
				strFieldName = "GiveReasonDesc";
				break;
			case 40:
				strFieldName = "MngCom";
				break;
			case 41:
				strFieldName = "Operator";
				break;
			case 42:
				strFieldName = "MakeDate";
				break;
			case 43:
				strFieldName = "MakeTime";
				break;
			case 44:
				strFieldName = "ModifyDate";
				break;
			case 45:
				strFieldName = "ModifyTime";
				break;
			case 46:
				strFieldName = "REMARK";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeclineNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClmNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseRelaNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StatType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("KindCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskVer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolMngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TabFeeMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClaimMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DeclineAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OverAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RealPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PreGiveAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SelfGiveAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RefuseAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OutDutyAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OutDutyRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ApproveAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ApproveCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgreeAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AgreeCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DutyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GiveType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GiveTypeDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GiveReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GiveReasonDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("REMARK") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
