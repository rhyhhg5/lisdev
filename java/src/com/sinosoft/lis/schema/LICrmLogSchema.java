/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LICrmLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LICrmLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-11-09
 */
public class LICrmLogSchema implements Schema, Cloneable {
    // @Field
    /** 批次号 */
    private String BatchNo;
    /** 文件代码 */
    private String FileCode;
    /** 文件名 */
    private String FileName;
    /** 记录数 */
    private int RecordNum;
    /** 批数据的时间戳 */
    private Date BatchDate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 8; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LICrmLogSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "BatchNo";
        pk[1] = "FileCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LICrmLogSchema cloned = (LICrmLogSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getBatchNo() {
        return BatchNo;
    }

    public void setBatchNo(String aBatchNo) {
        BatchNo = aBatchNo;
    }

    public String getFileCode() {
        return FileCode;
    }

    public void setFileCode(String aFileCode) {
        FileCode = aFileCode;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String aFileName) {
        FileName = aFileName;
    }

    public int getRecordNum() {
        return RecordNum;
    }

    public void setRecordNum(int aRecordNum) {
        RecordNum = aRecordNum;
    }

    public void setRecordNum(String aRecordNum) {
        if (aRecordNum != null && !aRecordNum.equals("")) {
            Integer tInteger = new Integer(aRecordNum);
            int i = tInteger.intValue();
            RecordNum = i;
        }
    }

    public String getBatchDate() {
        if (BatchDate != null) {
            return fDate.getString(BatchDate);
        } else {
            return null;
        }
    }

    public void setBatchDate(Date aBatchDate) {
        BatchDate = aBatchDate;
    }

    public void setBatchDate(String aBatchDate) {
        if (aBatchDate != null && !aBatchDate.equals("")) {
            BatchDate = fDate.getDate(aBatchDate);
        } else {
            BatchDate = null;
        }
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LICrmLogSchema 对象给 Schema 赋值
     * @param: aLICrmLogSchema LICrmLogSchema
     **/
    public void setSchema(LICrmLogSchema aLICrmLogSchema) {
        this.BatchNo = aLICrmLogSchema.getBatchNo();
        this.FileCode = aLICrmLogSchema.getFileCode();
        this.FileName = aLICrmLogSchema.getFileName();
        this.RecordNum = aLICrmLogSchema.getRecordNum();
        this.BatchDate = fDate.getDate(aLICrmLogSchema.getBatchDate());
        this.Operator = aLICrmLogSchema.getOperator();
        this.MakeDate = fDate.getDate(aLICrmLogSchema.getMakeDate());
        this.MakeTime = aLICrmLogSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("BatchNo") == null) {
                this.BatchNo = null;
            } else {
                this.BatchNo = rs.getString("BatchNo").trim();
            }

            if (rs.getString("FileCode") == null) {
                this.FileCode = null;
            } else {
                this.FileCode = rs.getString("FileCode").trim();
            }

            if (rs.getString("FileName") == null) {
                this.FileName = null;
            } else {
                this.FileName = rs.getString("FileName").trim();
            }

            this.RecordNum = rs.getInt("RecordNum");
            this.BatchDate = rs.getDate("BatchDate");
            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LICrmLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LICrmLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LICrmLogSchema getSchema() {
        LICrmLogSchema aLICrmLogSchema = new LICrmLogSchema();
        aLICrmLogSchema.setSchema(this);
        return aLICrmLogSchema;
    }

    public LICrmLogDB getDB() {
        LICrmLogDB aDBOper = new LICrmLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLICrmLog描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(BatchNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FileCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FileName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RecordNum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(BatchDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLICrmLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            FileCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RecordNum = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            BatchDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LICrmLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("BatchNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
        }
        if (FCode.equals("FileCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FileCode));
        }
        if (FCode.equals("FileName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FileName));
        }
        if (FCode.equals("RecordNum")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecordNum));
        }
        if (FCode.equals("BatchDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getBatchDate()));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(BatchNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(FileCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(FileName);
            break;
        case 3:
            strFieldValue = String.valueOf(RecordNum);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getBatchDate()));
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("BatchNo")) {
            if (FValue != null && !FValue.equals("")) {
                BatchNo = FValue.trim();
            } else {
                BatchNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("FileCode")) {
            if (FValue != null && !FValue.equals("")) {
                FileCode = FValue.trim();
            } else {
                FileCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("FileName")) {
            if (FValue != null && !FValue.equals("")) {
                FileName = FValue.trim();
            } else {
                FileName = null;
            }
        }
        if (FCode.equalsIgnoreCase("RecordNum")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RecordNum = i;
            }
        }
        if (FCode.equalsIgnoreCase("BatchDate")) {
            if (FValue != null && !FValue.equals("")) {
                BatchDate = fDate.getDate(FValue);
            } else {
                BatchDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LICrmLogSchema other = (LICrmLogSchema) otherObject;
        return
                BatchNo.equals(other.getBatchNo())
                && FileCode.equals(other.getFileCode())
                && FileName.equals(other.getFileName())
                && RecordNum == other.getRecordNum()
                && fDate.getString(BatchDate).equals(other.getBatchDate())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("BatchNo")) {
            return 0;
        }
        if (strFieldName.equals("FileCode")) {
            return 1;
        }
        if (strFieldName.equals("FileName")) {
            return 2;
        }
        if (strFieldName.equals("RecordNum")) {
            return 3;
        }
        if (strFieldName.equals("BatchDate")) {
            return 4;
        }
        if (strFieldName.equals("Operator")) {
            return 5;
        }
        if (strFieldName.equals("MakeDate")) {
            return 6;
        }
        if (strFieldName.equals("MakeTime")) {
            return 7;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "BatchNo";
            break;
        case 1:
            strFieldName = "FileCode";
            break;
        case 2:
            strFieldName = "FileName";
            break;
        case 3:
            strFieldName = "RecordNum";
            break;
        case 4:
            strFieldName = "BatchDate";
            break;
        case 5:
            strFieldName = "Operator";
            break;
        case 6:
            strFieldName = "MakeDate";
            break;
        case 7:
            strFieldName = "MakeTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("BatchNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FileCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FileName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RecordNum")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("BatchDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_INT;
            break;
        case 4:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
