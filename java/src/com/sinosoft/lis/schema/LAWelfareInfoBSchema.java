/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAWelfareInfoBDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAWelfareInfoBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAWelfareInfoBSchema implements Schema
{
    // @Field
    /** 转储号码 */
    private String EdorNo;
    /** 转储类型 */
    private String EdorType;
    /** 指标计算编码 */
    private String IndexCalNo;
    /** 展业类型 */
    private String BranchType;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人级别 */
    private String AgentGrade;
    /** 管理机构 */
    private String ManageCom;
    /** 展业机构外部编码 */
    private String BranchAttr;
    /** 福利类别 */
    private String AClass;
    /** 福利发放类型 */
    private String WelfareType;
    /** 福利金额 */
    private double SumMoney;
    /** 服务期限 */
    private double ServeYear;
    /** 福利个人提供金额 */
    private double PerMoney;
    /** 福利集体提供金额 */
    private double GrpMoney;
    /** 福利保障 */
    private double WAmnt;
    /** 福利发放日期 */
    private Date WSendDate;
    /** 福利发放终止日期 */
    private Date WSendEndDate;
    /** 福利生效日期 */
    private Date WValidDate;
    /** 福利生效终止日期 */
    private Date WValidEndDate;
    /** 批注 */
    private String Noti;
    /** 结清标志 */
    private String DoneFlag;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 27; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAWelfareInfoBSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "EdorNo";
        pk[1] = "EdorType";
        pk[2] = "IndexCalNo";
        pk[3] = "AgentCode";
        pk[4] = "AClass";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorNo()
    {
        if (SysConst.CHANGECHARSET && EdorNo != null && !EdorNo.equals(""))
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getEdorType()
    {
        if (SysConst.CHANGECHARSET && EdorType != null && !EdorType.equals(""))
        {
            EdorType = StrTool.unicodeToGBK(EdorType);
        }
        return EdorType;
    }

    public void setEdorType(String aEdorType)
    {
        EdorType = aEdorType;
    }

    public String getIndexCalNo()
    {
        if (SysConst.CHANGECHARSET && IndexCalNo != null &&
            !IndexCalNo.equals(""))
        {
            IndexCalNo = StrTool.unicodeToGBK(IndexCalNo);
        }
        return IndexCalNo;
    }

    public void setIndexCalNo(String aIndexCalNo)
    {
        IndexCalNo = aIndexCalNo;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getAgentCode()
    {
        if (SysConst.CHANGECHARSET && AgentCode != null && !AgentCode.equals(""))
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getBranchAttr()
    {
        if (SysConst.CHANGECHARSET && BranchAttr != null &&
            !BranchAttr.equals(""))
        {
            BranchAttr = StrTool.unicodeToGBK(BranchAttr);
        }
        return BranchAttr;
    }

    public void setBranchAttr(String aBranchAttr)
    {
        BranchAttr = aBranchAttr;
    }

    public String getAClass()
    {
        if (SysConst.CHANGECHARSET && AClass != null && !AClass.equals(""))
        {
            AClass = StrTool.unicodeToGBK(AClass);
        }
        return AClass;
    }

    public void setAClass(String aAClass)
    {
        AClass = aAClass;
    }

    public String getWelfareType()
    {
        if (SysConst.CHANGECHARSET && WelfareType != null &&
            !WelfareType.equals(""))
        {
            WelfareType = StrTool.unicodeToGBK(WelfareType);
        }
        return WelfareType;
    }

    public void setWelfareType(String aWelfareType)
    {
        WelfareType = aWelfareType;
    }

    public double getSumMoney()
    {
        return SumMoney;
    }

    public void setSumMoney(double aSumMoney)
    {
        SumMoney = aSumMoney;
    }

    public void setSumMoney(String aSumMoney)
    {
        if (aSumMoney != null && !aSumMoney.equals(""))
        {
            Double tDouble = new Double(aSumMoney);
            double d = tDouble.doubleValue();
            SumMoney = d;
        }
    }

    public double getServeYear()
    {
        return ServeYear;
    }

    public void setServeYear(double aServeYear)
    {
        ServeYear = aServeYear;
    }

    public void setServeYear(String aServeYear)
    {
        if (aServeYear != null && !aServeYear.equals(""))
        {
            Double tDouble = new Double(aServeYear);
            double d = tDouble.doubleValue();
            ServeYear = d;
        }
    }

    public double getPerMoney()
    {
        return PerMoney;
    }

    public void setPerMoney(double aPerMoney)
    {
        PerMoney = aPerMoney;
    }

    public void setPerMoney(String aPerMoney)
    {
        if (aPerMoney != null && !aPerMoney.equals(""))
        {
            Double tDouble = new Double(aPerMoney);
            double d = tDouble.doubleValue();
            PerMoney = d;
        }
    }

    public double getGrpMoney()
    {
        return GrpMoney;
    }

    public void setGrpMoney(double aGrpMoney)
    {
        GrpMoney = aGrpMoney;
    }

    public void setGrpMoney(String aGrpMoney)
    {
        if (aGrpMoney != null && !aGrpMoney.equals(""))
        {
            Double tDouble = new Double(aGrpMoney);
            double d = tDouble.doubleValue();
            GrpMoney = d;
        }
    }

    public double getWAmnt()
    {
        return WAmnt;
    }

    public void setWAmnt(double aWAmnt)
    {
        WAmnt = aWAmnt;
    }

    public void setWAmnt(String aWAmnt)
    {
        if (aWAmnt != null && !aWAmnt.equals(""))
        {
            Double tDouble = new Double(aWAmnt);
            double d = tDouble.doubleValue();
            WAmnt = d;
        }
    }

    public String getWSendDate()
    {
        if (WSendDate != null)
        {
            return fDate.getString(WSendDate);
        }
        else
        {
            return null;
        }
    }

    public void setWSendDate(Date aWSendDate)
    {
        WSendDate = aWSendDate;
    }

    public void setWSendDate(String aWSendDate)
    {
        if (aWSendDate != null && !aWSendDate.equals(""))
        {
            WSendDate = fDate.getDate(aWSendDate);
        }
        else
        {
            WSendDate = null;
        }
    }

    public String getWSendEndDate()
    {
        if (WSendEndDate != null)
        {
            return fDate.getString(WSendEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setWSendEndDate(Date aWSendEndDate)
    {
        WSendEndDate = aWSendEndDate;
    }

    public void setWSendEndDate(String aWSendEndDate)
    {
        if (aWSendEndDate != null && !aWSendEndDate.equals(""))
        {
            WSendEndDate = fDate.getDate(aWSendEndDate);
        }
        else
        {
            WSendEndDate = null;
        }
    }

    public String getWValidDate()
    {
        if (WValidDate != null)
        {
            return fDate.getString(WValidDate);
        }
        else
        {
            return null;
        }
    }

    public void setWValidDate(Date aWValidDate)
    {
        WValidDate = aWValidDate;
    }

    public void setWValidDate(String aWValidDate)
    {
        if (aWValidDate != null && !aWValidDate.equals(""))
        {
            WValidDate = fDate.getDate(aWValidDate);
        }
        else
        {
            WValidDate = null;
        }
    }

    public String getWValidEndDate()
    {
        if (WValidEndDate != null)
        {
            return fDate.getString(WValidEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setWValidEndDate(Date aWValidEndDate)
    {
        WValidEndDate = aWValidEndDate;
    }

    public void setWValidEndDate(String aWValidEndDate)
    {
        if (aWValidEndDate != null && !aWValidEndDate.equals(""))
        {
            WValidEndDate = fDate.getDate(aWValidEndDate);
        }
        else
        {
            WValidEndDate = null;
        }
    }

    public String getNoti()
    {
        if (SysConst.CHANGECHARSET && Noti != null && !Noti.equals(""))
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getDoneFlag()
    {
        if (SysConst.CHANGECHARSET && DoneFlag != null && !DoneFlag.equals(""))
        {
            DoneFlag = StrTool.unicodeToGBK(DoneFlag);
        }
        return DoneFlag;
    }

    public void setDoneFlag(String aDoneFlag)
    {
        DoneFlag = aDoneFlag;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAWelfareInfoBSchema 对象给 Schema 赋值
     * @param: aLAWelfareInfoBSchema LAWelfareInfoBSchema
     **/
    public void setSchema(LAWelfareInfoBSchema aLAWelfareInfoBSchema)
    {
        this.EdorNo = aLAWelfareInfoBSchema.getEdorNo();
        this.EdorType = aLAWelfareInfoBSchema.getEdorType();
        this.IndexCalNo = aLAWelfareInfoBSchema.getIndexCalNo();
        this.BranchType = aLAWelfareInfoBSchema.getBranchType();
        this.AgentCode = aLAWelfareInfoBSchema.getAgentCode();
        this.AgentGrade = aLAWelfareInfoBSchema.getAgentGrade();
        this.ManageCom = aLAWelfareInfoBSchema.getManageCom();
        this.BranchAttr = aLAWelfareInfoBSchema.getBranchAttr();
        this.AClass = aLAWelfareInfoBSchema.getAClass();
        this.WelfareType = aLAWelfareInfoBSchema.getWelfareType();
        this.SumMoney = aLAWelfareInfoBSchema.getSumMoney();
        this.ServeYear = aLAWelfareInfoBSchema.getServeYear();
        this.PerMoney = aLAWelfareInfoBSchema.getPerMoney();
        this.GrpMoney = aLAWelfareInfoBSchema.getGrpMoney();
        this.WAmnt = aLAWelfareInfoBSchema.getWAmnt();
        this.WSendDate = fDate.getDate(aLAWelfareInfoBSchema.getWSendDate());
        this.WSendEndDate = fDate.getDate(aLAWelfareInfoBSchema.getWSendEndDate());
        this.WValidDate = fDate.getDate(aLAWelfareInfoBSchema.getWValidDate());
        this.WValidEndDate = fDate.getDate(aLAWelfareInfoBSchema.
                                           getWValidEndDate());
        this.Noti = aLAWelfareInfoBSchema.getNoti();
        this.DoneFlag = aLAWelfareInfoBSchema.getDoneFlag();
        this.Operator = aLAWelfareInfoBSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAWelfareInfoBSchema.getMakeDate());
        this.MakeTime = aLAWelfareInfoBSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAWelfareInfoBSchema.getModifyDate());
        this.ModifyTime = aLAWelfareInfoBSchema.getModifyTime();
        this.BranchType2 = aLAWelfareInfoBSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("EdorType") == null)
            {
                this.EdorType = null;
            }
            else
            {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("IndexCalNo") == null)
            {
                this.IndexCalNo = null;
            }
            else
            {
                this.IndexCalNo = rs.getString("IndexCalNo").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("BranchAttr") == null)
            {
                this.BranchAttr = null;
            }
            else
            {
                this.BranchAttr = rs.getString("BranchAttr").trim();
            }

            if (rs.getString("AClass") == null)
            {
                this.AClass = null;
            }
            else
            {
                this.AClass = rs.getString("AClass").trim();
            }

            if (rs.getString("WelfareType") == null)
            {
                this.WelfareType = null;
            }
            else
            {
                this.WelfareType = rs.getString("WelfareType").trim();
            }

            this.SumMoney = rs.getDouble("SumMoney");
            this.ServeYear = rs.getDouble("ServeYear");
            this.PerMoney = rs.getDouble("PerMoney");
            this.GrpMoney = rs.getDouble("GrpMoney");
            this.WAmnt = rs.getDouble("WAmnt");
            this.WSendDate = rs.getDate("WSendDate");
            this.WSendEndDate = rs.getDate("WSendEndDate");
            this.WValidDate = rs.getDate("WValidDate");
            this.WValidEndDate = rs.getDate("WValidEndDate");
            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("DoneFlag") == null)
            {
                this.DoneFlag = null;
            }
            else
            {
                this.DoneFlag = rs.getString("DoneFlag").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareInfoBSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAWelfareInfoBSchema getSchema()
    {
        LAWelfareInfoBSchema aLAWelfareInfoBSchema = new LAWelfareInfoBSchema();
        aLAWelfareInfoBSchema.setSchema(this);
        return aLAWelfareInfoBSchema;
    }

    public LAWelfareInfoBDB getDB()
    {
        LAWelfareInfoBDB aDBOper = new LAWelfareInfoBDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWelfareInfoB描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(EdorType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IndexCalNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchAttr)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AClass)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(WelfareType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ServeYear));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PerMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GrpMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(WAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                WSendDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                WSendEndDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                WValidDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                WValidEndDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Noti)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DoneFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWelfareInfoB>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            IndexCalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            AClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                    SysConst.PACKAGESPILTER);
            WelfareType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            SumMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            ServeYear = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            PerMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            GrpMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            WAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    15, SysConst.PACKAGESPILTER))).doubleValue();
            WSendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            WSendEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            WValidDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            WValidEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                  SysConst.PACKAGESPILTER);
            DoneFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                        SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWelfareInfoBSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("EdorType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equals("IndexCalNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCalNo));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("BranchAttr"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equals("AClass"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AClass));
        }
        if (FCode.equals("WelfareType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WelfareType));
        }
        if (FCode.equals("SumMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumMoney));
        }
        if (FCode.equals("ServeYear"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServeYear));
        }
        if (FCode.equals("PerMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PerMoney));
        }
        if (FCode.equals("GrpMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpMoney));
        }
        if (FCode.equals("WAmnt"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WAmnt));
        }
        if (FCode.equals("WSendDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getWSendDate()));
        }
        if (FCode.equals("WSendEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getWSendEndDate()));
        }
        if (FCode.equals("WValidDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getWValidDate()));
        }
        if (FCode.equals("WValidEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getWValidEndDate()));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equals("DoneFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoneFlag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(IndexCalNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(BranchAttr);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AClass);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(WelfareType);
                break;
            case 10:
                strFieldValue = String.valueOf(SumMoney);
                break;
            case 11:
                strFieldValue = String.valueOf(ServeYear);
                break;
            case 12:
                strFieldValue = String.valueOf(PerMoney);
                break;
            case 13:
                strFieldValue = String.valueOf(GrpMoney);
                break;
            case 14:
                strFieldValue = String.valueOf(WAmnt);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getWSendDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getWSendEndDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getWValidDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getWValidEndDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(DoneFlag);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("EdorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorType = FValue.trim();
            }
            else
            {
                EdorType = null;
            }
        }
        if (FCode.equals("IndexCalNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexCalNo = FValue.trim();
            }
            else
            {
                IndexCalNo = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("BranchAttr"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchAttr = FValue.trim();
            }
            else
            {
                BranchAttr = null;
            }
        }
        if (FCode.equals("AClass"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AClass = FValue.trim();
            }
            else
            {
                AClass = null;
            }
        }
        if (FCode.equals("WelfareType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WelfareType = FValue.trim();
            }
            else
            {
                WelfareType = null;
            }
        }
        if (FCode.equals("SumMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumMoney = d;
            }
        }
        if (FCode.equals("ServeYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ServeYear = d;
            }
        }
        if (FCode.equals("PerMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PerMoney = d;
            }
        }
        if (FCode.equals("GrpMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GrpMoney = d;
            }
        }
        if (FCode.equals("WAmnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                WAmnt = d;
            }
        }
        if (FCode.equals("WSendDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WSendDate = fDate.getDate(FValue);
            }
            else
            {
                WSendDate = null;
            }
        }
        if (FCode.equals("WSendEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WSendEndDate = fDate.getDate(FValue);
            }
            else
            {
                WSendEndDate = null;
            }
        }
        if (FCode.equals("WValidDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WValidDate = fDate.getDate(FValue);
            }
            else
            {
                WValidDate = null;
            }
        }
        if (FCode.equals("WValidEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WValidEndDate = fDate.getDate(FValue);
            }
            else
            {
                WValidEndDate = null;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equals("DoneFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DoneFlag = FValue.trim();
            }
            else
            {
                DoneFlag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAWelfareInfoBSchema other = (LAWelfareInfoBSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && EdorType.equals(other.getEdorType())
                && IndexCalNo.equals(other.getIndexCalNo())
                && BranchType.equals(other.getBranchType())
                && AgentCode.equals(other.getAgentCode())
                && AgentGrade.equals(other.getAgentGrade())
                && ManageCom.equals(other.getManageCom())
                && BranchAttr.equals(other.getBranchAttr())
                && AClass.equals(other.getAClass())
                && WelfareType.equals(other.getWelfareType())
                && SumMoney == other.getSumMoney()
                && ServeYear == other.getServeYear()
                && PerMoney == other.getPerMoney()
                && GrpMoney == other.getGrpMoney()
                && WAmnt == other.getWAmnt()
                && fDate.getString(WSendDate).equals(other.getWSendDate())
                && fDate.getString(WSendEndDate).equals(other.getWSendEndDate())
                && fDate.getString(WValidDate).equals(other.getWValidDate())
                && fDate.getString(WValidEndDate).equals(other.getWValidEndDate())
                && Noti.equals(other.getNoti())
                && DoneFlag.equals(other.getDoneFlag())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorType"))
        {
            return 1;
        }
        if (strFieldName.equals("IndexCalNo"))
        {
            return 2;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 3;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 4;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 5;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 6;
        }
        if (strFieldName.equals("BranchAttr"))
        {
            return 7;
        }
        if (strFieldName.equals("AClass"))
        {
            return 8;
        }
        if (strFieldName.equals("WelfareType"))
        {
            return 9;
        }
        if (strFieldName.equals("SumMoney"))
        {
            return 10;
        }
        if (strFieldName.equals("ServeYear"))
        {
            return 11;
        }
        if (strFieldName.equals("PerMoney"))
        {
            return 12;
        }
        if (strFieldName.equals("GrpMoney"))
        {
            return 13;
        }
        if (strFieldName.equals("WAmnt"))
        {
            return 14;
        }
        if (strFieldName.equals("WSendDate"))
        {
            return 15;
        }
        if (strFieldName.equals("WSendEndDate"))
        {
            return 16;
        }
        if (strFieldName.equals("WValidDate"))
        {
            return 17;
        }
        if (strFieldName.equals("WValidEndDate"))
        {
            return 18;
        }
        if (strFieldName.equals("Noti"))
        {
            return 19;
        }
        if (strFieldName.equals("DoneFlag"))
        {
            return 20;
        }
        if (strFieldName.equals("Operator"))
        {
            return 21;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 22;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 23;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 24;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 25;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 26;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "EdorType";
                break;
            case 2:
                strFieldName = "IndexCalNo";
                break;
            case 3:
                strFieldName = "BranchType";
                break;
            case 4:
                strFieldName = "AgentCode";
                break;
            case 5:
                strFieldName = "AgentGrade";
                break;
            case 6:
                strFieldName = "ManageCom";
                break;
            case 7:
                strFieldName = "BranchAttr";
                break;
            case 8:
                strFieldName = "AClass";
                break;
            case 9:
                strFieldName = "WelfareType";
                break;
            case 10:
                strFieldName = "SumMoney";
                break;
            case 11:
                strFieldName = "ServeYear";
                break;
            case 12:
                strFieldName = "PerMoney";
                break;
            case 13:
                strFieldName = "GrpMoney";
                break;
            case 14:
                strFieldName = "WAmnt";
                break;
            case 15:
                strFieldName = "WSendDate";
                break;
            case 16:
                strFieldName = "WSendEndDate";
                break;
            case 17:
                strFieldName = "WValidDate";
                break;
            case 18:
                strFieldName = "WValidEndDate";
                break;
            case 19:
                strFieldName = "Noti";
                break;
            case 20:
                strFieldName = "DoneFlag";
                break;
            case 21:
                strFieldName = "Operator";
                break;
            case 22:
                strFieldName = "MakeDate";
                break;
            case 23:
                strFieldName = "MakeTime";
                break;
            case 24:
                strFieldName = "ModifyDate";
                break;
            case 25:
                strFieldName = "ModifyTime";
                break;
            case 26:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IndexCalNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchAttr"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AClass"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WelfareType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SumMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ServeYear"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PerMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GrpMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("WAmnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("WSendDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("WSendEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("WValidDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("WValidEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DoneFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
