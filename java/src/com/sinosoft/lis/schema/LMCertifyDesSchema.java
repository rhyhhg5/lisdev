/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMCertifyDesDB;

/*
 * <p>ClassName: LMCertifyDesSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2014-02-25
 */
public class LMCertifyDesSchema implements Schema, Cloneable
{
	// @Field
	/** 单证编码 */
	private String CertifyCode;
	/** 附标号 */
	private String SubCode;
	/** 险种编码 */
	private String RiskCode;
	/** 险种版本 */
	private String RiskVersion;
	/** 单证名称 */
	private String CertifyName;
	/** 总保费 */
	private double Prem;
	/** 总保额 */
	private double Amnt;
	/** 单证类型 */
	private String CertifyClass;
	/** 注释 */
	private String Note;
	/** 重要级别 */
	private String ImportantLevel;
	/** 状态 */
	private String State;
	/** 管理机构 */
	private String ManageCom;
	/** 单证内部编码 */
	private String InnerCertifyCode;
	/** 单证号码长度 */
	private double CertifyLength;
	/** 是否是有号单证 */
	private String HaveNumber;
	/** 录单标志 */
	private String InputFlag;
	/** 单位 */
	private String Unit;
	/** 业务单证类型 */
	private String CertifyClass2;
	/** 核销校验标志 */
	private String VerifyFlag;
	/** 保险期间 */
	private int PolPeriod;
	/** 保险期间单位 */
	private String PolPeriodFlag;
	/** 校验规则 */
	private String CheckRule;
	/** 业务类型 */
	private String OperateType;
	/** 最大数量 */
	private String CertifyMaxNo;
	/** 最大数量配置人 */
	private String MaxNoOperator;
	/** 最大数量配置日期 */
	private Date MaxNoMakeDate;
	/** 最大数量配置时间 */
	private String MaxNoMakeTime;
	/** 最大数量修改日期 */
	private Date MaxNoModifyDate;
	/** 最大数量修改时间 */
	private String MaxNoModifyTime;

	public static final int FIELDNUM = 29;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LMCertifyDesSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CertifyCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LMCertifyDesSchema cloned = (LMCertifyDesSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCertifyCode()
	{
		return CertifyCode;
	}
	public void setCertifyCode(String aCertifyCode)
	{
		CertifyCode = aCertifyCode;
	}
	public String getSubCode()
	{
		return SubCode;
	}
	public void setSubCode(String aSubCode)
	{
		SubCode = aSubCode;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskVersion()
	{
		return RiskVersion;
	}
	public void setRiskVersion(String aRiskVersion)
	{
		RiskVersion = aRiskVersion;
	}
	public String getCertifyName()
	{
		return CertifyName;
	}
	public void setCertifyName(String aCertifyName)
	{
		CertifyName = aCertifyName;
	}
	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
		Prem = Arith.round(aPrem, 2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d, 2);
		}
	}

	public double getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(double aAmnt)
	{
		Amnt = Arith.round(aAmnt, 2);
	}
	public void setAmnt(String aAmnt)
	{
		if (aAmnt != null && !aAmnt.equals(""))
		{
			Double tDouble = new Double(aAmnt);
			double d = tDouble.doubleValue();
                Amnt = Arith.round(d, 2);
		}
	}

	public String getCertifyClass()
	{
		return CertifyClass;
	}
	public void setCertifyClass(String aCertifyClass)
	{
		CertifyClass = aCertifyClass;
	}
	public String getNote()
	{
		return Note;
	}
	public void setNote(String aNote)
	{
		Note = aNote;
	}
	public String getImportantLevel()
	{
		return ImportantLevel;
	}
	public void setImportantLevel(String aImportantLevel)
	{
		ImportantLevel = aImportantLevel;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getInnerCertifyCode()
	{
		return InnerCertifyCode;
	}
	public void setInnerCertifyCode(String aInnerCertifyCode)
	{
		InnerCertifyCode = aInnerCertifyCode;
	}
	public double getCertifyLength()
	{
		return CertifyLength;
	}
	public void setCertifyLength(double aCertifyLength)
	{
		CertifyLength = Arith.round(aCertifyLength, 0);
	}
	public void setCertifyLength(String aCertifyLength)
	{
		if (aCertifyLength != null && !aCertifyLength.equals(""))
		{
			Double tDouble = new Double(aCertifyLength);
			double d = tDouble.doubleValue();
                CertifyLength = Arith.round(d, 0);
		}
	}

	public String getHaveNumber()
	{
		return HaveNumber;
	}
	public void setHaveNumber(String aHaveNumber)
	{
		HaveNumber = aHaveNumber;
	}
	public String getInputFlag()
	{
		return InputFlag;
	}
	public void setInputFlag(String aInputFlag)
	{
		InputFlag = aInputFlag;
	}
	public String getUnit()
	{
		return Unit;
	}
	public void setUnit(String aUnit)
	{
		Unit = aUnit;
	}
	public String getCertifyClass2()
	{
		return CertifyClass2;
	}
	public void setCertifyClass2(String aCertifyClass2)
	{
		CertifyClass2 = aCertifyClass2;
	}
	public String getVerifyFlag()
	{
		return VerifyFlag;
	}
	public void setVerifyFlag(String aVerifyFlag)
	{
		VerifyFlag = aVerifyFlag;
	}
	public int getPolPeriod()
	{
		return PolPeriod;
	}
	public void setPolPeriod(int aPolPeriod)
	{
		PolPeriod = aPolPeriod;
	}
	public void setPolPeriod(String aPolPeriod)
	{
		if (aPolPeriod != null && !aPolPeriod.equals(""))
		{
			Integer tInteger = new Integer(aPolPeriod);
			int i = tInteger.intValue();
			PolPeriod = i;
		}
	}

	public String getPolPeriodFlag()
	{
		return PolPeriodFlag;
	}
	public void setPolPeriodFlag(String aPolPeriodFlag)
	{
		PolPeriodFlag = aPolPeriodFlag;
	}
	public String getCheckRule()
	{
		return CheckRule;
	}
	public void setCheckRule(String aCheckRule)
	{
		CheckRule = aCheckRule;
	}
	public String getOperateType()
	{
		return OperateType;
	}
	public void setOperateType(String aOperateType)
	{
		OperateType = aOperateType;
	}
	public String getCertifyMaxNo()
	{
		return CertifyMaxNo;
	}
	public void setCertifyMaxNo(String aCertifyMaxNo)
	{
		CertifyMaxNo = aCertifyMaxNo;
	}
	public String getMaxNoOperator()
	{
		return MaxNoOperator;
	}
	public void setMaxNoOperator(String aMaxNoOperator)
	{
		MaxNoOperator = aMaxNoOperator;
	}
	public String getMaxNoMakeDate()
	{
		if( MaxNoMakeDate != null )
			return fDate.getString(MaxNoMakeDate);
		else
			return null;
	}
	public void setMaxNoMakeDate(Date aMaxNoMakeDate)
	{
		MaxNoMakeDate = aMaxNoMakeDate;
	}
	public void setMaxNoMakeDate(String aMaxNoMakeDate)
	{
		if (aMaxNoMakeDate != null && !aMaxNoMakeDate.equals("") )
		{
			MaxNoMakeDate = fDate.getDate( aMaxNoMakeDate );
		}
		else
			MaxNoMakeDate = null;
	}

	public String getMaxNoMakeTime()
	{
		return MaxNoMakeTime;
	}
	public void setMaxNoMakeTime(String aMaxNoMakeTime)
	{
		MaxNoMakeTime = aMaxNoMakeTime;
	}
	public String getMaxNoModifyDate()
	{
		if( MaxNoModifyDate != null )
			return fDate.getString(MaxNoModifyDate);
		else
			return null;
	}
	public void setMaxNoModifyDate(Date aMaxNoModifyDate)
	{
		MaxNoModifyDate = aMaxNoModifyDate;
	}
	public void setMaxNoModifyDate(String aMaxNoModifyDate)
	{
		if (aMaxNoModifyDate != null && !aMaxNoModifyDate.equals("") )
		{
			MaxNoModifyDate = fDate.getDate( aMaxNoModifyDate );
		}
		else
			MaxNoModifyDate = null;
	}

	public String getMaxNoModifyTime()
	{
		return MaxNoModifyTime;
	}
	public void setMaxNoModifyTime(String aMaxNoModifyTime)
	{
		MaxNoModifyTime = aMaxNoModifyTime;
	}

	/**
	* 使用另外一个 LMCertifyDesSchema 对象给 Schema 赋值
	* @param: aLMCertifyDesSchema LMCertifyDesSchema
	**/
	public void setSchema(LMCertifyDesSchema aLMCertifyDesSchema)
	{
		this.CertifyCode = aLMCertifyDesSchema.getCertifyCode();
		this.SubCode = aLMCertifyDesSchema.getSubCode();
		this.RiskCode = aLMCertifyDesSchema.getRiskCode();
		this.RiskVersion = aLMCertifyDesSchema.getRiskVersion();
		this.CertifyName = aLMCertifyDesSchema.getCertifyName();
		this.Prem = aLMCertifyDesSchema.getPrem();
		this.Amnt = aLMCertifyDesSchema.getAmnt();
		this.CertifyClass = aLMCertifyDesSchema.getCertifyClass();
		this.Note = aLMCertifyDesSchema.getNote();
		this.ImportantLevel = aLMCertifyDesSchema.getImportantLevel();
		this.State = aLMCertifyDesSchema.getState();
		this.ManageCom = aLMCertifyDesSchema.getManageCom();
		this.InnerCertifyCode = aLMCertifyDesSchema.getInnerCertifyCode();
		this.CertifyLength = aLMCertifyDesSchema.getCertifyLength();
		this.HaveNumber = aLMCertifyDesSchema.getHaveNumber();
		this.InputFlag = aLMCertifyDesSchema.getInputFlag();
		this.Unit = aLMCertifyDesSchema.getUnit();
		this.CertifyClass2 = aLMCertifyDesSchema.getCertifyClass2();
		this.VerifyFlag = aLMCertifyDesSchema.getVerifyFlag();
		this.PolPeriod = aLMCertifyDesSchema.getPolPeriod();
		this.PolPeriodFlag = aLMCertifyDesSchema.getPolPeriodFlag();
		this.CheckRule = aLMCertifyDesSchema.getCheckRule();
		this.OperateType = aLMCertifyDesSchema.getOperateType();
		this.CertifyMaxNo = aLMCertifyDesSchema.getCertifyMaxNo();
		this.MaxNoOperator = aLMCertifyDesSchema.getMaxNoOperator();
		this.MaxNoMakeDate = fDate.getDate( aLMCertifyDesSchema.getMaxNoMakeDate());
		this.MaxNoMakeTime = aLMCertifyDesSchema.getMaxNoMakeTime();
		this.MaxNoModifyDate = fDate.getDate( aLMCertifyDesSchema.getMaxNoModifyDate());
		this.MaxNoModifyTime = aLMCertifyDesSchema.getMaxNoModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CertifyCode") == null )
				this.CertifyCode = null;
			else
				this.CertifyCode = rs.getString("CertifyCode").trim();

			if( rs.getString("SubCode") == null )
				this.SubCode = null;
			else
				this.SubCode = rs.getString("SubCode").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskVersion") == null )
				this.RiskVersion = null;
			else
				this.RiskVersion = rs.getString("RiskVersion").trim();

			if( rs.getString("CertifyName") == null )
				this.CertifyName = null;
			else
				this.CertifyName = rs.getString("CertifyName").trim();

			this.Prem = rs.getDouble("Prem");
			this.Amnt = rs.getDouble("Amnt");
			if( rs.getString("CertifyClass") == null )
				this.CertifyClass = null;
			else
				this.CertifyClass = rs.getString("CertifyClass").trim();

			if( rs.getString("Note") == null )
				this.Note = null;
			else
				this.Note = rs.getString("Note").trim();

			if( rs.getString("ImportantLevel") == null )
				this.ImportantLevel = null;
			else
				this.ImportantLevel = rs.getString("ImportantLevel").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("InnerCertifyCode") == null )
				this.InnerCertifyCode = null;
			else
				this.InnerCertifyCode = rs.getString("InnerCertifyCode").trim();

			this.CertifyLength = rs.getDouble("CertifyLength");
			if( rs.getString("HaveNumber") == null )
				this.HaveNumber = null;
			else
				this.HaveNumber = rs.getString("HaveNumber").trim();

			if( rs.getString("InputFlag") == null )
				this.InputFlag = null;
			else
				this.InputFlag = rs.getString("InputFlag").trim();

			if( rs.getString("Unit") == null )
				this.Unit = null;
			else
				this.Unit = rs.getString("Unit").trim();

			if( rs.getString("CertifyClass2") == null )
				this.CertifyClass2 = null;
			else
				this.CertifyClass2 = rs.getString("CertifyClass2").trim();

			if( rs.getString("VerifyFlag") == null )
				this.VerifyFlag = null;
			else
				this.VerifyFlag = rs.getString("VerifyFlag").trim();

			this.PolPeriod = rs.getInt("PolPeriod");
			if( rs.getString("PolPeriodFlag") == null )
				this.PolPeriodFlag = null;
			else
				this.PolPeriodFlag = rs.getString("PolPeriodFlag").trim();

			if( rs.getString("CheckRule") == null )
				this.CheckRule = null;
			else
				this.CheckRule = rs.getString("CheckRule").trim();

			if( rs.getString("OperateType") == null )
				this.OperateType = null;
			else
				this.OperateType = rs.getString("OperateType").trim();

			if( rs.getString("CertifyMaxNo") == null )
				this.CertifyMaxNo = null;
			else
				this.CertifyMaxNo = rs.getString("CertifyMaxNo").trim();

			if( rs.getString("MaxNoOperator") == null )
				this.MaxNoOperator = null;
			else
				this.MaxNoOperator = rs.getString("MaxNoOperator").trim();

			this.MaxNoMakeDate = rs.getDate("MaxNoMakeDate");
			if( rs.getString("MaxNoMakeTime") == null )
				this.MaxNoMakeTime = null;
			else
				this.MaxNoMakeTime = rs.getString("MaxNoMakeTime").trim();

			this.MaxNoModifyDate = rs.getDate("MaxNoModifyDate");
			if( rs.getString("MaxNoModifyTime") == null )
				this.MaxNoModifyTime = null;
			else
				this.MaxNoModifyTime = rs.getString("MaxNoModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LMCertifyDes表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMCertifyDesSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LMCertifyDesSchema getSchema()
	{
		LMCertifyDesSchema aLMCertifyDesSchema = new LMCertifyDesSchema();
		aLMCertifyDesSchema.setSchema(this);
		return aLMCertifyDesSchema;
	}

	public LMCertifyDesDB getDB()
	{
		LMCertifyDesDB aDBOper = new LMCertifyDesDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMCertifyDes描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyClass)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Note)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ImportantLevel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InnerCertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CertifyLength));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HaveNumber)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Unit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyClass2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VerifyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolPeriod));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolPeriodFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckRule)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OperateType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyMaxNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MaxNoOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MaxNoMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MaxNoMakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MaxNoModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MaxNoModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMCertifyDes>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SubCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CertifyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			CertifyClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Note = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ImportantLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			InnerCertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			CertifyLength = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			HaveNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			InputFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Unit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			CertifyClass2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			VerifyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			PolPeriod= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).intValue();
			PolPeriodFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			CheckRule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			OperateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			CertifyMaxNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			MaxNoOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			MaxNoMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			MaxNoMakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			MaxNoModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			MaxNoModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMCertifyDesSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CertifyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
		}
		if (FCode.equals("SubCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubCode));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskVersion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
		}
		if (FCode.equals("CertifyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyName));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("CertifyClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyClass));
		}
		if (FCode.equals("Note"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Note));
		}
		if (FCode.equals("ImportantLevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImportantLevel));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("InnerCertifyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InnerCertifyCode));
		}
		if (FCode.equals("CertifyLength"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyLength));
		}
		if (FCode.equals("HaveNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HaveNumber));
		}
		if (FCode.equals("InputFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputFlag));
		}
		if (FCode.equals("Unit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Unit));
		}
		if (FCode.equals("CertifyClass2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyClass2));
		}
		if (FCode.equals("VerifyFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VerifyFlag));
		}
		if (FCode.equals("PolPeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolPeriod));
		}
		if (FCode.equals("PolPeriodFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolPeriodFlag));
		}
		if (FCode.equals("CheckRule"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckRule));
		}
		if (FCode.equals("OperateType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OperateType));
		}
		if (FCode.equals("CertifyMaxNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyMaxNo));
		}
		if (FCode.equals("MaxNoOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxNoOperator));
		}
		if (FCode.equals("MaxNoMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMaxNoMakeDate()));
		}
		if (FCode.equals("MaxNoMakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxNoMakeTime));
		}
		if (FCode.equals("MaxNoModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMaxNoModifyDate()));
		}
		if (FCode.equals("MaxNoModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxNoModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CertifyCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SubCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RiskVersion);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CertifyName);
				break;
			case 5:
				strFieldValue = String.valueOf(Prem);
				break;
			case 6:
				strFieldValue = String.valueOf(Amnt);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CertifyClass);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Note);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ImportantLevel);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(InnerCertifyCode);
				break;
			case 13:
				strFieldValue = String.valueOf(CertifyLength);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(HaveNumber);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(InputFlag);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Unit);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(CertifyClass2);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(VerifyFlag);
				break;
			case 19:
				strFieldValue = String.valueOf(PolPeriod);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(PolPeriodFlag);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(CheckRule);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(OperateType);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(CertifyMaxNo);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(MaxNoOperator);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMaxNoMakeDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(MaxNoMakeTime);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMaxNoModifyDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(MaxNoModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CertifyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyCode = FValue.trim();
			}
			else
				CertifyCode = null;
		}
		if (FCode.equalsIgnoreCase("SubCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubCode = FValue.trim();
			}
			else
				SubCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskVersion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskVersion = FValue.trim();
			}
			else
				RiskVersion = null;
		}
		if (FCode.equalsIgnoreCase("CertifyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyName = FValue.trim();
			}
			else
				CertifyName = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Amnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("CertifyClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyClass = FValue.trim();
			}
			else
				CertifyClass = null;
		}
		if (FCode.equalsIgnoreCase("Note"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Note = FValue.trim();
			}
			else
				Note = null;
		}
		if (FCode.equalsIgnoreCase("ImportantLevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ImportantLevel = FValue.trim();
			}
			else
				ImportantLevel = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("InnerCertifyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InnerCertifyCode = FValue.trim();
			}
			else
				InnerCertifyCode = null;
		}
		if (FCode.equalsIgnoreCase("CertifyLength"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CertifyLength = d;
			}
		}
		if (FCode.equalsIgnoreCase("HaveNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HaveNumber = FValue.trim();
			}
			else
				HaveNumber = null;
		}
		if (FCode.equalsIgnoreCase("InputFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputFlag = FValue.trim();
			}
			else
				InputFlag = null;
		}
		if (FCode.equalsIgnoreCase("Unit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Unit = FValue.trim();
			}
			else
				Unit = null;
		}
		if (FCode.equalsIgnoreCase("CertifyClass2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyClass2 = FValue.trim();
			}
			else
				CertifyClass2 = null;
		}
		if (FCode.equalsIgnoreCase("VerifyFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VerifyFlag = FValue.trim();
			}
			else
				VerifyFlag = null;
		}
		if (FCode.equalsIgnoreCase("PolPeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolPeriod = i;
			}
		}
		if (FCode.equalsIgnoreCase("PolPeriodFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolPeriodFlag = FValue.trim();
			}
			else
				PolPeriodFlag = null;
		}
		if (FCode.equalsIgnoreCase("CheckRule"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckRule = FValue.trim();
			}
			else
				CheckRule = null;
		}
		if (FCode.equalsIgnoreCase("OperateType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OperateType = FValue.trim();
			}
			else
				OperateType = null;
		}
		if (FCode.equalsIgnoreCase("CertifyMaxNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyMaxNo = FValue.trim();
			}
			else
				CertifyMaxNo = null;
		}
		if (FCode.equalsIgnoreCase("MaxNoOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaxNoOperator = FValue.trim();
			}
			else
				MaxNoOperator = null;
		}
		if (FCode.equalsIgnoreCase("MaxNoMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MaxNoMakeDate = fDate.getDate( FValue );
			}
			else
				MaxNoMakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MaxNoMakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaxNoMakeTime = FValue.trim();
			}
			else
				MaxNoMakeTime = null;
		}
		if (FCode.equalsIgnoreCase("MaxNoModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MaxNoModifyDate = fDate.getDate( FValue );
			}
			else
				MaxNoModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("MaxNoModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaxNoModifyTime = FValue.trim();
			}
			else
				MaxNoModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LMCertifyDesSchema other = (LMCertifyDesSchema)otherObject;
		return
			(CertifyCode == null ? other.getCertifyCode() == null : CertifyCode.equals(other.getCertifyCode()))
			&& (SubCode == null ? other.getSubCode() == null : SubCode.equals(other.getSubCode()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskVersion == null ? other.getRiskVersion() == null : RiskVersion.equals(other.getRiskVersion()))
			&& (CertifyName == null ? other.getCertifyName() == null : CertifyName.equals(other.getCertifyName()))
			&& Prem == other.getPrem()
			&& Amnt == other.getAmnt()
			&& (CertifyClass == null ? other.getCertifyClass() == null : CertifyClass.equals(other.getCertifyClass()))
			&& (Note == null ? other.getNote() == null : Note.equals(other.getNote()))
			&& (ImportantLevel == null ? other.getImportantLevel() == null : ImportantLevel.equals(other.getImportantLevel()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (InnerCertifyCode == null ? other.getInnerCertifyCode() == null : InnerCertifyCode.equals(other.getInnerCertifyCode()))
			&& CertifyLength == other.getCertifyLength()
			&& (HaveNumber == null ? other.getHaveNumber() == null : HaveNumber.equals(other.getHaveNumber()))
			&& (InputFlag == null ? other.getInputFlag() == null : InputFlag.equals(other.getInputFlag()))
			&& (Unit == null ? other.getUnit() == null : Unit.equals(other.getUnit()))
			&& (CertifyClass2 == null ? other.getCertifyClass2() == null : CertifyClass2.equals(other.getCertifyClass2()))
			&& (VerifyFlag == null ? other.getVerifyFlag() == null : VerifyFlag.equals(other.getVerifyFlag()))
			&& PolPeriod == other.getPolPeriod()
			&& (PolPeriodFlag == null ? other.getPolPeriodFlag() == null : PolPeriodFlag.equals(other.getPolPeriodFlag()))
			&& (CheckRule == null ? other.getCheckRule() == null : CheckRule.equals(other.getCheckRule()))
			&& (OperateType == null ? other.getOperateType() == null : OperateType.equals(other.getOperateType()))
			&& (CertifyMaxNo == null ? other.getCertifyMaxNo() == null : CertifyMaxNo.equals(other.getCertifyMaxNo()))
			&& (MaxNoOperator == null ? other.getMaxNoOperator() == null : MaxNoOperator.equals(other.getMaxNoOperator()))
			&& (MaxNoMakeDate == null ? other.getMaxNoMakeDate() == null : fDate.getString(MaxNoMakeDate).equals(other.getMaxNoMakeDate()))
			&& (MaxNoMakeTime == null ? other.getMaxNoMakeTime() == null : MaxNoMakeTime.equals(other.getMaxNoMakeTime()))
			&& (MaxNoModifyDate == null ? other.getMaxNoModifyDate() == null : fDate.getString(MaxNoModifyDate).equals(other.getMaxNoModifyDate()))
			&& (MaxNoModifyTime == null ? other.getMaxNoModifyTime() == null : MaxNoModifyTime.equals(other.getMaxNoModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CertifyCode") ) {
			return 0;
		}
		if( strFieldName.equals("SubCode") ) {
			return 1;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 2;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return 3;
		}
		if( strFieldName.equals("CertifyName") ) {
			return 4;
		}
		if( strFieldName.equals("Prem") ) {
			return 5;
		}
		if( strFieldName.equals("Amnt") ) {
			return 6;
		}
		if( strFieldName.equals("CertifyClass") ) {
			return 7;
		}
		if( strFieldName.equals("Note") ) {
			return 8;
		}
		if( strFieldName.equals("ImportantLevel") ) {
			return 9;
		}
		if( strFieldName.equals("State") ) {
			return 10;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 11;
		}
		if( strFieldName.equals("InnerCertifyCode") ) {
			return 12;
		}
		if( strFieldName.equals("CertifyLength") ) {
			return 13;
		}
		if( strFieldName.equals("HaveNumber") ) {
			return 14;
		}
		if( strFieldName.equals("InputFlag") ) {
			return 15;
		}
		if( strFieldName.equals("Unit") ) {
			return 16;
		}
		if( strFieldName.equals("CertifyClass2") ) {
			return 17;
		}
		if( strFieldName.equals("VerifyFlag") ) {
			return 18;
		}
		if( strFieldName.equals("PolPeriod") ) {
			return 19;
		}
		if( strFieldName.equals("PolPeriodFlag") ) {
			return 20;
		}
		if( strFieldName.equals("CheckRule") ) {
			return 21;
		}
		if( strFieldName.equals("OperateType") ) {
			return 22;
		}
		if( strFieldName.equals("CertifyMaxNo") ) {
			return 23;
		}
		if( strFieldName.equals("MaxNoOperator") ) {
			return 24;
		}
		if( strFieldName.equals("MaxNoMakeDate") ) {
			return 25;
		}
		if( strFieldName.equals("MaxNoMakeTime") ) {
			return 26;
		}
		if( strFieldName.equals("MaxNoModifyDate") ) {
			return 27;
		}
		if( strFieldName.equals("MaxNoModifyTime") ) {
			return 28;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CertifyCode";
				break;
			case 1:
				strFieldName = "SubCode";
				break;
			case 2:
				strFieldName = "RiskCode";
				break;
			case 3:
				strFieldName = "RiskVersion";
				break;
			case 4:
				strFieldName = "CertifyName";
				break;
			case 5:
				strFieldName = "Prem";
				break;
			case 6:
				strFieldName = "Amnt";
				break;
			case 7:
				strFieldName = "CertifyClass";
				break;
			case 8:
				strFieldName = "Note";
				break;
			case 9:
				strFieldName = "ImportantLevel";
				break;
			case 10:
				strFieldName = "State";
				break;
			case 11:
				strFieldName = "ManageCom";
				break;
			case 12:
				strFieldName = "InnerCertifyCode";
				break;
			case 13:
				strFieldName = "CertifyLength";
				break;
			case 14:
				strFieldName = "HaveNumber";
				break;
			case 15:
				strFieldName = "InputFlag";
				break;
			case 16:
				strFieldName = "Unit";
				break;
			case 17:
				strFieldName = "CertifyClass2";
				break;
			case 18:
				strFieldName = "VerifyFlag";
				break;
			case 19:
				strFieldName = "PolPeriod";
				break;
			case 20:
				strFieldName = "PolPeriodFlag";
				break;
			case 21:
				strFieldName = "CheckRule";
				break;
			case 22:
				strFieldName = "OperateType";
				break;
			case 23:
				strFieldName = "CertifyMaxNo";
				break;
			case 24:
				strFieldName = "MaxNoOperator";
				break;
			case 25:
				strFieldName = "MaxNoMakeDate";
				break;
			case 26:
				strFieldName = "MaxNoMakeTime";
				break;
			case 27:
				strFieldName = "MaxNoModifyDate";
				break;
			case 28:
				strFieldName = "MaxNoModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CertifyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CertifyClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Note") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImportantLevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InnerCertifyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyLength") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("HaveNumber") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Unit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyClass2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VerifyFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolPeriod") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PolPeriodFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckRule") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OperateType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyMaxNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MaxNoOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MaxNoMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MaxNoMakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MaxNoModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MaxNoModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_INT;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
