/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDBankUniteDB;

/*
 * <p>ClassName: LDBankUniteSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-11-18
 */
public class LDBankUniteSchema implements Schema, Cloneable {
    // @Field
    /** 银联编码 */
    private String BankUniteCode;
    /** 银联名称 */
    private String BankUniteName;
    /** 内部银行编码 */
    private String BankCode;
    /** 银联银行编码 */
    private String UniteBankCode;
    /** 银联银行名称 */
    private String UniteBankName;
    /** 机构编码 */
    private String ComCode;
    /** 银联单位代码 */
    private String UniteGroupCode;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDBankUniteSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "BankUniteCode";
        pk[1] = "BankCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDBankUniteSchema cloned = (LDBankUniteSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getBankUniteCode() {
        return BankUniteCode;
    }

    public void setBankUniteCode(String aBankUniteCode) {
        BankUniteCode = aBankUniteCode;
    }

    public String getBankUniteName() {
        return BankUniteName;
    }

    public void setBankUniteName(String aBankUniteName) {
        BankUniteName = aBankUniteName;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String aBankCode) {
        BankCode = aBankCode;
    }

    public String getUniteBankCode() {
        return UniteBankCode;
    }

    public void setUniteBankCode(String aUniteBankCode) {
        UniteBankCode = aUniteBankCode;
    }

    public String getUniteBankName() {
        return UniteBankName;
    }

    public void setUniteBankName(String aUniteBankName) {
        UniteBankName = aUniteBankName;
    }

    public String getComCode() {
        return ComCode;
    }

    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }

    public String getUniteGroupCode() {
        return UniteGroupCode;
    }

    public void setUniteGroupCode(String aUniteGroupCode) {
        UniteGroupCode = aUniteGroupCode;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LDBankUniteSchema 对象给 Schema 赋值
     * @param: aLDBankUniteSchema LDBankUniteSchema
     **/
    public void setSchema(LDBankUniteSchema aLDBankUniteSchema) {
        this.BankUniteCode = aLDBankUniteSchema.getBankUniteCode();
        this.BankUniteName = aLDBankUniteSchema.getBankUniteName();
        this.BankCode = aLDBankUniteSchema.getBankCode();
        this.UniteBankCode = aLDBankUniteSchema.getUniteBankCode();
        this.UniteBankName = aLDBankUniteSchema.getUniteBankName();
        this.ComCode = aLDBankUniteSchema.getComCode();
        this.UniteGroupCode = aLDBankUniteSchema.getUniteGroupCode();
        this.Operator = aLDBankUniteSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDBankUniteSchema.getMakeDate());
        this.MakeTime = aLDBankUniteSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDBankUniteSchema.getModifyDate());
        this.ModifyTime = aLDBankUniteSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("BankUniteCode") == null) {
                this.BankUniteCode = null;
            } else {
                this.BankUniteCode = rs.getString("BankUniteCode").trim();
            }

            if (rs.getString("BankUniteName") == null) {
                this.BankUniteName = null;
            } else {
                this.BankUniteName = rs.getString("BankUniteName").trim();
            }

            if (rs.getString("BankCode") == null) {
                this.BankCode = null;
            } else {
                this.BankCode = rs.getString("BankCode").trim();
            }

            if (rs.getString("UniteBankCode") == null) {
                this.UniteBankCode = null;
            } else {
                this.UniteBankCode = rs.getString("UniteBankCode").trim();
            }

            if (rs.getString("UniteBankName") == null) {
                this.UniteBankName = null;
            } else {
                this.UniteBankName = rs.getString("UniteBankName").trim();
            }

            if (rs.getString("ComCode") == null) {
                this.ComCode = null;
            } else {
                this.ComCode = rs.getString("ComCode").trim();
            }

            if (rs.getString("UniteGroupCode") == null) {
                this.UniteGroupCode = null;
            } else {
                this.UniteGroupCode = rs.getString("UniteGroupCode").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDBankUnite表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBankUniteSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDBankUniteSchema getSchema() {
        LDBankUniteSchema aLDBankUniteSchema = new LDBankUniteSchema();
        aLDBankUniteSchema.setSchema(this);
        return aLDBankUniteSchema;
    }

    public LDBankUniteDB getDB() {
        LDBankUniteDB aDBOper = new LDBankUniteDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBankUnite描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(BankUniteCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankUniteName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BankCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UniteBankCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UniteBankName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UniteGroupCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDBankUnite>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            BankUniteCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            BankUniteName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                           SysConst.PACKAGESPILTER);
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            UniteBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            UniteBankName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            UniteGroupCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                            SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDBankUniteSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("BankUniteCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankUniteCode));
        }
        if (FCode.equals("BankUniteName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankUniteName));
        }
        if (FCode.equals("BankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
        }
        if (FCode.equals("UniteBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UniteBankCode));
        }
        if (FCode.equals("UniteBankName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UniteBankName));
        }
        if (FCode.equals("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equals("UniteGroupCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UniteGroupCode));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(BankUniteCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(BankUniteName);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(BankCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(UniteBankCode);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(UniteBankName);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ComCode);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(UniteGroupCode);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("BankUniteCode")) {
            if (FValue != null && !FValue.equals("")) {
                BankUniteCode = FValue.trim();
            } else {
                BankUniteCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BankUniteName")) {
            if (FValue != null && !FValue.equals("")) {
                BankUniteName = FValue.trim();
            } else {
                BankUniteName = null;
            }
        }
        if (FCode.equalsIgnoreCase("BankCode")) {
            if (FValue != null && !FValue.equals("")) {
                BankCode = FValue.trim();
            } else {
                BankCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("UniteBankCode")) {
            if (FValue != null && !FValue.equals("")) {
                UniteBankCode = FValue.trim();
            } else {
                UniteBankCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("UniteBankName")) {
            if (FValue != null && !FValue.equals("")) {
                UniteBankName = FValue.trim();
            } else {
                UniteBankName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if (FValue != null && !FValue.equals("")) {
                ComCode = FValue.trim();
            } else {
                ComCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("UniteGroupCode")) {
            if (FValue != null && !FValue.equals("")) {
                UniteGroupCode = FValue.trim();
            } else {
                UniteGroupCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDBankUniteSchema other = (LDBankUniteSchema) otherObject;
        return
                BankUniteCode.equals(other.getBankUniteCode())
                && BankUniteName.equals(other.getBankUniteName())
                && BankCode.equals(other.getBankCode())
                && UniteBankCode.equals(other.getUniteBankCode())
                && UniteBankName.equals(other.getUniteBankName())
                && ComCode.equals(other.getComCode())
                && UniteGroupCode.equals(other.getUniteGroupCode())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("BankUniteCode")) {
            return 0;
        }
        if (strFieldName.equals("BankUniteName")) {
            return 1;
        }
        if (strFieldName.equals("BankCode")) {
            return 2;
        }
        if (strFieldName.equals("UniteBankCode")) {
            return 3;
        }
        if (strFieldName.equals("UniteBankName")) {
            return 4;
        }
        if (strFieldName.equals("ComCode")) {
            return 5;
        }
        if (strFieldName.equals("UniteGroupCode")) {
            return 6;
        }
        if (strFieldName.equals("Operator")) {
            return 7;
        }
        if (strFieldName.equals("MakeDate")) {
            return 8;
        }
        if (strFieldName.equals("MakeTime")) {
            return 9;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 10;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "BankUniteCode";
            break;
        case 1:
            strFieldName = "BankUniteName";
            break;
        case 2:
            strFieldName = "BankCode";
            break;
        case 3:
            strFieldName = "UniteBankCode";
            break;
        case 4:
            strFieldName = "UniteBankName";
            break;
        case 5:
            strFieldName = "ComCode";
            break;
        case 6:
            strFieldName = "UniteGroupCode";
            break;
        case 7:
            strFieldName = "Operator";
            break;
        case 8:
            strFieldName = "MakeDate";
            break;
        case 9:
            strFieldName = "MakeTime";
            break;
        case 10:
            strFieldName = "ModifyDate";
            break;
        case 11:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("BankUniteCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankUniteName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UniteBankCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UniteBankName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UniteGroupCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
