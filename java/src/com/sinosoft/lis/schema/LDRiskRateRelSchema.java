/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDRiskRateRelDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;


/*
 * <p>ClassName: LDRiskRateRelSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 费率表动态显示
 * @CreateDate：2005-04-08
 */
public class LDRiskRateRelSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;

    /** 险种名称 */
    private String RiskName;

    /** 费率表编码 */
    private String RateTableCode;

    /** 费率表名 */
    private String RateTableName;

    /** 备注 */
    private String Remark;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息


    // @Constructor
    public LDRiskRateRelSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "RateTableCode";

        PK = pk;
    }


    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (SysConst.CHANGECHARSET && RiskCode != null && !RiskCode.equals(""))
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskName()
    {
        if (SysConst.CHANGECHARSET && RiskName != null && !RiskName.equals(""))
        {
            RiskName = StrTool.unicodeToGBK(RiskName);
        }
        return RiskName;
    }

    public void setRiskName(String aRiskName)
    {
        RiskName = aRiskName;
    }

    public String getRateTableCode()
    {
        if (SysConst.CHANGECHARSET && RateTableCode != null &&
            !RateTableCode.equals(""))
        {
            RateTableCode = StrTool.unicodeToGBK(RateTableCode);
        }
        return RateTableCode;
    }

    public void setRateTableCode(String aRateTableCode)
    {
        RateTableCode = aRateTableCode;
    }

    public String getRateTableName()
    {
        if (SysConst.CHANGECHARSET && RateTableName != null &&
            !RateTableName.equals(""))
        {
            RateTableName = StrTool.unicodeToGBK(RateTableName);
        }
        return RateTableName;
    }

    public void setRateTableName(String aRateTableName)
    {
        RateTableName = aRateTableName;
    }

    public String getRemark()
    {
        if (SysConst.CHANGECHARSET && Remark != null && !Remark.equals(""))
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }


    /**
     * 使用另外一个 LDRiskRateRelSchema 对象给 Schema 赋值
     * @param: aLDRiskRateRelSchema LDRiskRateRelSchema
     **/
    public void setSchema(LDRiskRateRelSchema aLDRiskRateRelSchema)
    {
        this.RiskCode = aLDRiskRateRelSchema.getRiskCode();
        this.RiskName = aLDRiskRateRelSchema.getRiskName();
        this.RateTableCode = aLDRiskRateRelSchema.getRateTableCode();
        this.RateTableName = aLDRiskRateRelSchema.getRateTableName();
        this.Remark = aLDRiskRateRelSchema.getRemark();
    }


    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskName") == null)
            {
                this.RiskName = null;
            }
            else
            {
                this.RiskName = rs.getString("RiskName").trim();
            }

            if (rs.getString("RateTableCode") == null)
            {
                this.RateTableCode = null;
            }
            else
            {
                this.RateTableCode = rs.getString("RateTableCode").trim();
            }

            if (rs.getString("RateTableName") == null)
            {
                this.RateTableName = null;
            }
            else
            {
                this.RateTableName = rs.getString("RateTableName").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskRateRelSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDRiskRateRelSchema getSchema()
    {
        LDRiskRateRelSchema aLDRiskRateRelSchema = new LDRiskRateRelSchema();
        aLDRiskRateRelSchema.setSchema(this);
        return aLDRiskRateRelSchema;
    }

    public LDRiskRateRelDB getDB()
    {
        LDRiskRateRelDB aDBOper = new LDRiskRateRelDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskRateRel描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RiskName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RateTableCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RateTableName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Remark)));
        return strReturn.toString();
    }


    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskRateRel>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            RateTableCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            RateTableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskRateRelSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }


    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("RiskName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
        }
        if (FCode.equals("RateTableCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RateTableCode));
        }
        if (FCode.equals("RateTableName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RateTableName));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RateTableCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RateTableName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }


    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
            {
                RiskName = null;
            }
        }
        if (FCode.equals("RateTableCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RateTableCode = FValue.trim();
            }
            else
            {
                RateTableCode = null;
            }
        }
        if (FCode.equals("RateTableName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RateTableName = FValue.trim();
            }
            else
            {
                RateTableName = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDRiskRateRelSchema other = (LDRiskRateRelSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskName.equals(other.getRiskName())
                && RateTableCode.equals(other.getRateTableCode())
                && RateTableName.equals(other.getRateTableName())
                && Remark.equals(other.getRemark());
    }


    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }


    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskName"))
        {
            return 1;
        }
        if (strFieldName.equals("RateTableCode"))
        {
            return 2;
        }
        if (strFieldName.equals("RateTableName"))
        {
            return 3;
        }
        if (strFieldName.equals("Remark"))
        {
            return 4;
        }
        return -1;
    }


    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskName";
                break;
            case 2:
                strFieldName = "RateTableCode";
                break;
            case 3:
                strFieldName = "RateTableName";
                break;
            case 4:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }


    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RateTableCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RateTableName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }


    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
