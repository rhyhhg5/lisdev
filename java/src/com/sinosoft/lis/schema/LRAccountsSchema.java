/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRAccountsDB;

/*
 * <p>ClassName: LRAccountsSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 再保财务接口
 * @CreateDate：2008-07-11
 */
public class LRAccountsSchema implements Schema, Cloneable
{
	// @Field
	/** 实付号码 */
	private String ActuGetNo;
	/** 账单编号 */
	private String FeeNo;
	/** 合同编码 */
	private String RecontCode;
	/** 合同名称 */
	private String RecontName;
	/** 再保公司编码 */
	private String ReComCode;
	/** 再保公司名称 */
	private String ReComName;
	/** 账单起始日期 */
	private Date StartDate;
	/** 账单结束日期 */
	private Date EndData;
	/** 账单年度 */
	private String OPerationYear;
	/** 币种 */
	private String MoneyType;
	/** 分保费 */
	private double CessPrem;
	/** 备用字段1 */
	private double Standby1;
	/** 贷款合计 */
	private double SumLoan;
	/** 分保手续费 */
	private double ReProcFee;
	/** 分摊赔款 */
	private double ClaimBackFee;
	/** 分保余额 */
	private double BalanceLend;
	/** 备用字段2 */
	private double Standby2;
	/** 借款合计 */
	private double SumLead;
	/** 给付标记 */
	private String PayFlag;
	/** 财务结账日期 */
	private Date PayDate;
	/** 备用字段3 */
	private String Standby3;
	/** 操作员 */
	private String Operator;
	/** 制单日期 */
	private Date MakeDate;
	/** 制单时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 26;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRAccountsSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ActuGetNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LRAccountsSchema cloned = (LRAccountsSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getActuGetNo()
	{
		return ActuGetNo;
	}
	public void setActuGetNo(String aActuGetNo)
	{
            ActuGetNo = aActuGetNo;
	}
	public String getFeeNo()
	{
		return FeeNo;
	}
	public void setFeeNo(String aFeeNo)
	{
            FeeNo = aFeeNo;
	}
	public String getRecontCode()
	{
		return RecontCode;
	}
	public void setRecontCode(String aRecontCode)
	{
            RecontCode = aRecontCode;
	}
	public String getRecontName()
	{
		return RecontName;
	}
	public void setRecontName(String aRecontName)
	{
            RecontName = aRecontName;
	}
	public String getReComCode()
	{
		return ReComCode;
	}
	public void setReComCode(String aReComCode)
	{
            ReComCode = aReComCode;
	}
	public String getReComName()
	{
		return ReComName;
	}
	public void setReComName(String aReComName)
	{
            ReComName = aReComName;
	}
	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
            StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getEndData()
	{
		if( EndData != null )
			return fDate.getString(EndData);
		else
			return null;
	}
	public void setEndData(Date aEndData)
	{
            EndData = aEndData;
	}
	public void setEndData(String aEndData)
	{
		if (aEndData != null && !aEndData.equals("") )
		{
			EndData = fDate.getDate( aEndData );
		}
		else
			EndData = null;
	}

	public String getOPerationYear()
	{
		return OPerationYear;
	}
	public void setOPerationYear(String aOPerationYear)
	{
            OPerationYear = aOPerationYear;
	}
	public String getMoneyType()
	{
		return MoneyType;
	}
	public void setMoneyType(String aMoneyType)
	{
            MoneyType = aMoneyType;
	}
	public double getCessPrem()
	{
		return CessPrem;
	}
	public void setCessPrem(double aCessPrem)
	{
            CessPrem = Arith.round(aCessPrem,2);
	}
	public void setCessPrem(String aCessPrem)
	{
		if (aCessPrem != null && !aCessPrem.equals(""))
		{
			Double tDouble = new Double(aCessPrem);
			double d = tDouble.doubleValue();
                CessPrem = Arith.round(d,2);
		}
	}

	public double getStandby1()
	{
		return Standby1;
	}
	public void setStandby1(double aStandby1)
	{
            Standby1 = Arith.round(aStandby1,2);
	}
	public void setStandby1(String aStandby1)
	{
		if (aStandby1 != null && !aStandby1.equals(""))
		{
			Double tDouble = new Double(aStandby1);
			double d = tDouble.doubleValue();
                Standby1 = Arith.round(d,2);
		}
	}

	public double getSumLoan()
	{
		return SumLoan;
	}
	public void setSumLoan(double aSumLoan)
	{
            SumLoan = Arith.round(aSumLoan,2);
	}
	public void setSumLoan(String aSumLoan)
	{
		if (aSumLoan != null && !aSumLoan.equals(""))
		{
			Double tDouble = new Double(aSumLoan);
			double d = tDouble.doubleValue();
                SumLoan = Arith.round(d,2);
		}
	}

	public double getReProcFee()
	{
		return ReProcFee;
	}
	public void setReProcFee(double aReProcFee)
	{
            ReProcFee = Arith.round(aReProcFee,2);
	}
	public void setReProcFee(String aReProcFee)
	{
		if (aReProcFee != null && !aReProcFee.equals(""))
		{
			Double tDouble = new Double(aReProcFee);
			double d = tDouble.doubleValue();
                ReProcFee = Arith.round(d,2);
		}
	}

	public double getClaimBackFee()
	{
		return ClaimBackFee;
	}
	public void setClaimBackFee(double aClaimBackFee)
	{
            ClaimBackFee = Arith.round(aClaimBackFee,2);
	}
	public void setClaimBackFee(String aClaimBackFee)
	{
		if (aClaimBackFee != null && !aClaimBackFee.equals(""))
		{
			Double tDouble = new Double(aClaimBackFee);
			double d = tDouble.doubleValue();
                ClaimBackFee = Arith.round(d,2);
		}
	}

	public double getBalanceLend()
	{
		return BalanceLend;
	}
	public void setBalanceLend(double aBalanceLend)
	{
            BalanceLend = Arith.round(aBalanceLend,2);
	}
	public void setBalanceLend(String aBalanceLend)
	{
		if (aBalanceLend != null && !aBalanceLend.equals(""))
		{
			Double tDouble = new Double(aBalanceLend);
			double d = tDouble.doubleValue();
                BalanceLend = Arith.round(d,2);
		}
	}

	public double getStandby2()
	{
		return Standby2;
	}
	public void setStandby2(double aStandby2)
	{
            Standby2 = Arith.round(aStandby2,2);
	}
	public void setStandby2(String aStandby2)
	{
		if (aStandby2 != null && !aStandby2.equals(""))
		{
			Double tDouble = new Double(aStandby2);
			double d = tDouble.doubleValue();
                Standby2 = Arith.round(d,2);
		}
	}

	public double getSumLead()
	{
		return SumLead;
	}
	public void setSumLead(double aSumLead)
	{
            SumLead = Arith.round(aSumLead,2);
	}
	public void setSumLead(String aSumLead)
	{
		if (aSumLead != null && !aSumLead.equals(""))
		{
			Double tDouble = new Double(aSumLead);
			double d = tDouble.doubleValue();
                SumLead = Arith.round(d,2);
		}
	}

	public String getPayFlag()
	{
		return PayFlag;
	}
	public void setPayFlag(String aPayFlag)
	{
            PayFlag = aPayFlag;
	}
	public String getPayDate()
	{
		if( PayDate != null )
			return fDate.getString(PayDate);
		else
			return null;
	}
	public void setPayDate(Date aPayDate)
	{
            PayDate = aPayDate;
	}
	public void setPayDate(String aPayDate)
	{
		if (aPayDate != null && !aPayDate.equals("") )
		{
			PayDate = fDate.getDate( aPayDate );
		}
		else
			PayDate = null;
	}

	public String getStandby3()
	{
		return Standby3;
	}
	public void setStandby3(String aStandby3)
	{
            Standby3 = aStandby3;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LRAccountsSchema 对象给 Schema 赋值
	* @param: aLRAccountsSchema LRAccountsSchema
	**/
	public void setSchema(LRAccountsSchema aLRAccountsSchema)
	{
		this.ActuGetNo = aLRAccountsSchema.getActuGetNo();
		this.FeeNo = aLRAccountsSchema.getFeeNo();
		this.RecontCode = aLRAccountsSchema.getRecontCode();
		this.RecontName = aLRAccountsSchema.getRecontName();
		this.ReComCode = aLRAccountsSchema.getReComCode();
		this.ReComName = aLRAccountsSchema.getReComName();
		this.StartDate = fDate.getDate( aLRAccountsSchema.getStartDate());
		this.EndData = fDate.getDate( aLRAccountsSchema.getEndData());
		this.OPerationYear = aLRAccountsSchema.getOPerationYear();
		this.MoneyType = aLRAccountsSchema.getMoneyType();
		this.CessPrem = aLRAccountsSchema.getCessPrem();
		this.Standby1 = aLRAccountsSchema.getStandby1();
		this.SumLoan = aLRAccountsSchema.getSumLoan();
		this.ReProcFee = aLRAccountsSchema.getReProcFee();
		this.ClaimBackFee = aLRAccountsSchema.getClaimBackFee();
		this.BalanceLend = aLRAccountsSchema.getBalanceLend();
		this.Standby2 = aLRAccountsSchema.getStandby2();
		this.SumLead = aLRAccountsSchema.getSumLead();
		this.PayFlag = aLRAccountsSchema.getPayFlag();
		this.PayDate = fDate.getDate( aLRAccountsSchema.getPayDate());
		this.Standby3 = aLRAccountsSchema.getStandby3();
		this.Operator = aLRAccountsSchema.getOperator();
		this.MakeDate = fDate.getDate( aLRAccountsSchema.getMakeDate());
		this.MakeTime = aLRAccountsSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRAccountsSchema.getModifyDate());
		this.ModifyTime = aLRAccountsSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ActuGetNo") == null )
				this.ActuGetNo = null;
			else
				this.ActuGetNo = rs.getString("ActuGetNo").trim();

			if( rs.getString("FeeNo") == null )
				this.FeeNo = null;
			else
				this.FeeNo = rs.getString("FeeNo").trim();

			if( rs.getString("RecontCode") == null )
				this.RecontCode = null;
			else
				this.RecontCode = rs.getString("RecontCode").trim();

			if( rs.getString("RecontName") == null )
				this.RecontName = null;
			else
				this.RecontName = rs.getString("RecontName").trim();

			if( rs.getString("ReComCode") == null )
				this.ReComCode = null;
			else
				this.ReComCode = rs.getString("ReComCode").trim();

			if( rs.getString("ReComName") == null )
				this.ReComName = null;
			else
				this.ReComName = rs.getString("ReComName").trim();

			this.StartDate = rs.getDate("StartDate");
			this.EndData = rs.getDate("EndData");
			if( rs.getString("OPerationYear") == null )
				this.OPerationYear = null;
			else
				this.OPerationYear = rs.getString("OPerationYear").trim();

			if( rs.getString("MoneyType") == null )
				this.MoneyType = null;
			else
				this.MoneyType = rs.getString("MoneyType").trim();

			this.CessPrem = rs.getDouble("CessPrem");
			this.Standby1 = rs.getDouble("Standby1");
			this.SumLoan = rs.getDouble("SumLoan");
			this.ReProcFee = rs.getDouble("ReProcFee");
			this.ClaimBackFee = rs.getDouble("ClaimBackFee");
			this.BalanceLend = rs.getDouble("BalanceLend");
			this.Standby2 = rs.getDouble("Standby2");
			this.SumLead = rs.getDouble("SumLead");
			if( rs.getString("PayFlag") == null )
				this.PayFlag = null;
			else
				this.PayFlag = rs.getString("PayFlag").trim();

			this.PayDate = rs.getDate("PayDate");
			if( rs.getString("Standby3") == null )
				this.Standby3 = null;
			else
				this.Standby3 = rs.getString("Standby3").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRAccounts表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRAccountsSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRAccountsSchema getSchema()
	{
		LRAccountsSchema aLRAccountsSchema = new LRAccountsSchema();
		aLRAccountsSchema.setSchema(this);
		return aLRAccountsSchema;
	}

	public LRAccountsDB getDB()
	{
		LRAccountsDB aDBOper = new LRAccountsDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRAccounts描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ActuGetNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RecontCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RecontName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ReComCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ReComName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( EndData ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OPerationYear)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MoneyType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(CessPrem));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(Standby1));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SumLoan));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ReProcFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ClaimBackFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(BalanceLend));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(Standby2));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SumLead));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( PayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Standby3)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRAccounts>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ActuGetNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			FeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RecontCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RecontName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ReComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ReComName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			EndData = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			OPerationYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MoneyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CessPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			Standby1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			SumLoan = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			ReProcFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			ClaimBackFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			BalanceLend = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			Standby2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			SumLead = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			PayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			Standby3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRAccountsSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ActuGetNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetNo));
		}
		if (FCode.equals("FeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeNo));
		}
		if (FCode.equals("RecontCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecontCode));
		}
		if (FCode.equals("RecontName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecontName));
		}
		if (FCode.equals("ReComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReComCode));
		}
		if (FCode.equals("ReComName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReComName));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("EndData"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndData()));
		}
		if (FCode.equals("OPerationYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OPerationYear));
		}
		if (FCode.equals("MoneyType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MoneyType));
		}
		if (FCode.equals("CessPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessPrem));
		}
		if (FCode.equals("Standby1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standby1));
		}
		if (FCode.equals("SumLoan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumLoan));
		}
		if (FCode.equals("ReProcFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReProcFee));
		}
		if (FCode.equals("ClaimBackFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimBackFee));
		}
		if (FCode.equals("BalanceLend"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceLend));
		}
		if (FCode.equals("Standby2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standby2));
		}
		if (FCode.equals("SumLead"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumLead));
		}
		if (FCode.equals("PayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayFlag));
		}
		if (FCode.equals("PayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
		}
		if (FCode.equals("Standby3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standby3));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ActuGetNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(FeeNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RecontCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RecontName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ReComCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ReComName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndData()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(OPerationYear);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MoneyType);
				break;
			case 10:
				strFieldValue = String.valueOf(CessPrem);
				break;
			case 11:
				strFieldValue = String.valueOf(Standby1);
				break;
			case 12:
				strFieldValue = String.valueOf(SumLoan);
				break;
			case 13:
				strFieldValue = String.valueOf(ReProcFee);
				break;
			case 14:
				strFieldValue = String.valueOf(ClaimBackFee);
				break;
			case 15:
				strFieldValue = String.valueOf(BalanceLend);
				break;
			case 16:
				strFieldValue = String.valueOf(Standby2);
				break;
			case 17:
				strFieldValue = String.valueOf(SumLead);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(PayFlag);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPayDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Standby3);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ActuGetNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActuGetNo = FValue.trim();
			}
			else
				ActuGetNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeNo = FValue.trim();
			}
			else
				FeeNo = null;
		}
		if (FCode.equalsIgnoreCase("RecontCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RecontCode = FValue.trim();
			}
			else
				RecontCode = null;
		}
		if (FCode.equalsIgnoreCase("RecontName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RecontName = FValue.trim();
			}
			else
				RecontName = null;
		}
		if (FCode.equalsIgnoreCase("ReComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReComCode = FValue.trim();
			}
			else
				ReComCode = null;
		}
		if (FCode.equalsIgnoreCase("ReComName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReComName = FValue.trim();
			}
			else
				ReComName = null;
		}
		if (FCode.equalsIgnoreCase("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equalsIgnoreCase("EndData"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndData = fDate.getDate( FValue );
			}
			else
				EndData = null;
		}
		if (FCode.equalsIgnoreCase("OPerationYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OPerationYear = FValue.trim();
			}
			else
				OPerationYear = null;
		}
		if (FCode.equalsIgnoreCase("MoneyType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MoneyType = FValue.trim();
			}
			else
				MoneyType = null;
		}
		if (FCode.equalsIgnoreCase("CessPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standby1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standby1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumLoan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumLoan = d;
			}
		}
		if (FCode.equalsIgnoreCase("ReProcFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ReProcFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClaimBackFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ClaimBackFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("BalanceLend"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				BalanceLend = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standby2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standby2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumLead"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumLead = d;
			}
		}
		if (FCode.equalsIgnoreCase("PayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayFlag = FValue.trim();
			}
			else
				PayFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PayDate = fDate.getDate( FValue );
			}
			else
				PayDate = null;
		}
		if (FCode.equalsIgnoreCase("Standby3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standby3 = FValue.trim();
			}
			else
				Standby3 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRAccountsSchema other = (LRAccountsSchema)otherObject;
		return
			ActuGetNo.equals(other.getActuGetNo())
			&& FeeNo.equals(other.getFeeNo())
			&& RecontCode.equals(other.getRecontCode())
			&& RecontName.equals(other.getRecontName())
			&& ReComCode.equals(other.getReComCode())
			&& ReComName.equals(other.getReComName())
			&& fDate.getString(StartDate).equals(other.getStartDate())
			&& fDate.getString(EndData).equals(other.getEndData())
			&& OPerationYear.equals(other.getOPerationYear())
			&& MoneyType.equals(other.getMoneyType())
			&& CessPrem == other.getCessPrem()
			&& Standby1 == other.getStandby1()
			&& SumLoan == other.getSumLoan()
			&& ReProcFee == other.getReProcFee()
			&& ClaimBackFee == other.getClaimBackFee()
			&& BalanceLend == other.getBalanceLend()
			&& Standby2 == other.getStandby2()
			&& SumLead == other.getSumLead()
			&& PayFlag.equals(other.getPayFlag())
			&& fDate.getString(PayDate).equals(other.getPayDate())
			&& Standby3.equals(other.getStandby3())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ActuGetNo") ) {
			return 0;
		}
		if( strFieldName.equals("FeeNo") ) {
			return 1;
		}
		if( strFieldName.equals("RecontCode") ) {
			return 2;
		}
		if( strFieldName.equals("RecontName") ) {
			return 3;
		}
		if( strFieldName.equals("ReComCode") ) {
			return 4;
		}
		if( strFieldName.equals("ReComName") ) {
			return 5;
		}
		if( strFieldName.equals("StartDate") ) {
			return 6;
		}
		if( strFieldName.equals("EndData") ) {
			return 7;
		}
		if( strFieldName.equals("OPerationYear") ) {
			return 8;
		}
		if( strFieldName.equals("MoneyType") ) {
			return 9;
		}
		if( strFieldName.equals("CessPrem") ) {
			return 10;
		}
		if( strFieldName.equals("Standby1") ) {
			return 11;
		}
		if( strFieldName.equals("SumLoan") ) {
			return 12;
		}
		if( strFieldName.equals("ReProcFee") ) {
			return 13;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return 14;
		}
		if( strFieldName.equals("BalanceLend") ) {
			return 15;
		}
		if( strFieldName.equals("Standby2") ) {
			return 16;
		}
		if( strFieldName.equals("SumLead") ) {
			return 17;
		}
		if( strFieldName.equals("PayFlag") ) {
			return 18;
		}
		if( strFieldName.equals("PayDate") ) {
			return 19;
		}
		if( strFieldName.equals("Standby3") ) {
			return 20;
		}
		if( strFieldName.equals("Operator") ) {
			return 21;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 22;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 25;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ActuGetNo";
				break;
			case 1:
				strFieldName = "FeeNo";
				break;
			case 2:
				strFieldName = "RecontCode";
				break;
			case 3:
				strFieldName = "RecontName";
				break;
			case 4:
				strFieldName = "ReComCode";
				break;
			case 5:
				strFieldName = "ReComName";
				break;
			case 6:
				strFieldName = "StartDate";
				break;
			case 7:
				strFieldName = "EndData";
				break;
			case 8:
				strFieldName = "OPerationYear";
				break;
			case 9:
				strFieldName = "MoneyType";
				break;
			case 10:
				strFieldName = "CessPrem";
				break;
			case 11:
				strFieldName = "Standby1";
				break;
			case 12:
				strFieldName = "SumLoan";
				break;
			case 13:
				strFieldName = "ReProcFee";
				break;
			case 14:
				strFieldName = "ClaimBackFee";
				break;
			case 15:
				strFieldName = "BalanceLend";
				break;
			case 16:
				strFieldName = "Standby2";
				break;
			case 17:
				strFieldName = "SumLead";
				break;
			case 18:
				strFieldName = "PayFlag";
				break;
			case 19:
				strFieldName = "PayDate";
				break;
			case 20:
				strFieldName = "Standby3";
				break;
			case 21:
				strFieldName = "Operator";
				break;
			case 22:
				strFieldName = "MakeDate";
				break;
			case 23:
				strFieldName = "MakeTime";
				break;
			case 24:
				strFieldName = "ModifyDate";
				break;
			case 25:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ActuGetNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecontCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecontName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReComName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndData") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OPerationYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MoneyType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standby1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumLoan") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ReProcFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BalanceLend") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standby2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumLead") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Standby3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
