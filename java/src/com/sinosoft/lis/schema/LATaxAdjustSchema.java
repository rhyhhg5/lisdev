/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATaxAdjustDB;

/*
 * <p>ClassName: LATaxAdjustSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 应纳税调整表
 * @CreateDate：2016-03-31
 */
public class LATaxAdjustSchema implements Schema, Cloneable
{
	// @Field
	/** 业务员代码 */
	private String AgentCode;
	/** 薪资月 */
	private String WageNo;
	/** 管理机构 */
	private String ManageCom;
	/** 展业类型 */
	private String BranchType;
	/** 渠道 */
	private String BranchType2;
	/** 总公司奖励应纳税项 */
	private double CBPTaxes;
	/** 省公司奖励应纳税项 */
	private double PCBPTaxes;
	/** 中支奖励应纳税项 */
	private double ITBPTaxes;
	/** 其它应纳税项1 */
	private double Taxes1;
	/** 其它应纳税项2 */
	private double Taxes2;
	/** 其它应纳税项3 */
	private double Taxes3;
	/** 其它应纳税项4 */
	private double Taxes4;
	/** 备用字符串1 */
	private String P01;
	/** 备用字符串2 */
	private String P02;
	/** 备用字符串3 */
	private String P03;
	/** 备用字符串4 */
	private String P04;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 21;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LATaxAdjustSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[5];
		pk[0] = "AgentCode";
		pk[1] = "WageNo";
		pk[2] = "ManageCom";
		pk[3] = "BranchType";
		pk[4] = "BranchType2";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LATaxAdjustSchema cloned = (LATaxAdjustSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public double getCBPTaxes()
	{
		return CBPTaxes;
	}
	public void setCBPTaxes(double aCBPTaxes)
	{
		CBPTaxes = Arith.round(aCBPTaxes,2);
	}
	public void setCBPTaxes(String aCBPTaxes)
	{
		if (aCBPTaxes != null && !aCBPTaxes.equals(""))
		{
			Double tDouble = new Double(aCBPTaxes);
			double d = tDouble.doubleValue();
                CBPTaxes = Arith.round(d,2);
		}
	}

	public double getPCBPTaxes()
	{
		return PCBPTaxes;
	}
	public void setPCBPTaxes(double aPCBPTaxes)
	{
		PCBPTaxes = Arith.round(aPCBPTaxes,2);
	}
	public void setPCBPTaxes(String aPCBPTaxes)
	{
		if (aPCBPTaxes != null && !aPCBPTaxes.equals(""))
		{
			Double tDouble = new Double(aPCBPTaxes);
			double d = tDouble.doubleValue();
                PCBPTaxes = Arith.round(d,2);
		}
	}

	public double getITBPTaxes()
	{
		return ITBPTaxes;
	}
	public void setITBPTaxes(double aITBPTaxes)
	{
		ITBPTaxes = Arith.round(aITBPTaxes,2);
	}
	public void setITBPTaxes(String aITBPTaxes)
	{
		if (aITBPTaxes != null && !aITBPTaxes.equals(""))
		{
			Double tDouble = new Double(aITBPTaxes);
			double d = tDouble.doubleValue();
                ITBPTaxes = Arith.round(d,2);
		}
	}

	public double getTaxes1()
	{
		return Taxes1;
	}
	public void setTaxes1(double aTaxes1)
	{
		Taxes1 = Arith.round(aTaxes1,2);
	}
	public void setTaxes1(String aTaxes1)
	{
		if (aTaxes1 != null && !aTaxes1.equals(""))
		{
			Double tDouble = new Double(aTaxes1);
			double d = tDouble.doubleValue();
                Taxes1 = Arith.round(d,2);
		}
	}

	public double getTaxes2()
	{
		return Taxes2;
	}
	public void setTaxes2(double aTaxes2)
	{
		Taxes2 = Arith.round(aTaxes2,2);
	}
	public void setTaxes2(String aTaxes2)
	{
		if (aTaxes2 != null && !aTaxes2.equals(""))
		{
			Double tDouble = new Double(aTaxes2);
			double d = tDouble.doubleValue();
                Taxes2 = Arith.round(d,2);
		}
	}

	public double getTaxes3()
	{
		return Taxes3;
	}
	public void setTaxes3(double aTaxes3)
	{
		Taxes3 = Arith.round(aTaxes3,2);
	}
	public void setTaxes3(String aTaxes3)
	{
		if (aTaxes3 != null && !aTaxes3.equals(""))
		{
			Double tDouble = new Double(aTaxes3);
			double d = tDouble.doubleValue();
                Taxes3 = Arith.round(d,2);
		}
	}

	public double getTaxes4()
	{
		return Taxes4;
	}
	public void setTaxes4(double aTaxes4)
	{
		Taxes4 = Arith.round(aTaxes4,2);
	}
	public void setTaxes4(String aTaxes4)
	{
		if (aTaxes4 != null && !aTaxes4.equals(""))
		{
			Double tDouble = new Double(aTaxes4);
			double d = tDouble.doubleValue();
                Taxes4 = Arith.round(d,2);
		}
	}

	public String getP01()
	{
		return P01;
	}
	public void setP01(String aP01)
	{
		P01 = aP01;
	}
	public String getP02()
	{
		return P02;
	}
	public void setP02(String aP02)
	{
		P02 = aP02;
	}
	public String getP03()
	{
		return P03;
	}
	public void setP03(String aP03)
	{
		P03 = aP03;
	}
	public String getP04()
	{
		return P04;
	}
	public void setP04(String aP04)
	{
		P04 = aP04;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LATaxAdjustSchema 对象给 Schema 赋值
	* @param: aLATaxAdjustSchema LATaxAdjustSchema
	**/
	public void setSchema(LATaxAdjustSchema aLATaxAdjustSchema)
	{
		this.AgentCode = aLATaxAdjustSchema.getAgentCode();
		this.WageNo = aLATaxAdjustSchema.getWageNo();
		this.ManageCom = aLATaxAdjustSchema.getManageCom();
		this.BranchType = aLATaxAdjustSchema.getBranchType();
		this.BranchType2 = aLATaxAdjustSchema.getBranchType2();
		this.CBPTaxes = aLATaxAdjustSchema.getCBPTaxes();
		this.PCBPTaxes = aLATaxAdjustSchema.getPCBPTaxes();
		this.ITBPTaxes = aLATaxAdjustSchema.getITBPTaxes();
		this.Taxes1 = aLATaxAdjustSchema.getTaxes1();
		this.Taxes2 = aLATaxAdjustSchema.getTaxes2();
		this.Taxes3 = aLATaxAdjustSchema.getTaxes3();
		this.Taxes4 = aLATaxAdjustSchema.getTaxes4();
		this.P01 = aLATaxAdjustSchema.getP01();
		this.P02 = aLATaxAdjustSchema.getP02();
		this.P03 = aLATaxAdjustSchema.getP03();
		this.P04 = aLATaxAdjustSchema.getP04();
		this.Operator = aLATaxAdjustSchema.getOperator();
		this.MakeDate = fDate.getDate( aLATaxAdjustSchema.getMakeDate());
		this.MakeTime = aLATaxAdjustSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLATaxAdjustSchema.getModifyDate());
		this.ModifyTime = aLATaxAdjustSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			this.CBPTaxes = rs.getDouble("CBPTaxes");
			this.PCBPTaxes = rs.getDouble("PCBPTaxes");
			this.ITBPTaxes = rs.getDouble("ITBPTaxes");
			this.Taxes1 = rs.getDouble("Taxes1");
			this.Taxes2 = rs.getDouble("Taxes2");
			this.Taxes3 = rs.getDouble("Taxes3");
			this.Taxes4 = rs.getDouble("Taxes4");
			if( rs.getString("P01") == null )
				this.P01 = null;
			else
				this.P01 = rs.getString("P01").trim();

			if( rs.getString("P02") == null )
				this.P02 = null;
			else
				this.P02 = rs.getString("P02").trim();

			if( rs.getString("P03") == null )
				this.P03 = null;
			else
				this.P03 = rs.getString("P03").trim();

			if( rs.getString("P04") == null )
				this.P04 = null;
			else
				this.P04 = rs.getString("P04").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LATaxAdjust表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATaxAdjustSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LATaxAdjustSchema getSchema()
	{
		LATaxAdjustSchema aLATaxAdjustSchema = new LATaxAdjustSchema();
		aLATaxAdjustSchema.setSchema(this);
		return aLATaxAdjustSchema;
	}

	public LATaxAdjustDB getDB()
	{
		LATaxAdjustDB aDBOper = new LATaxAdjustDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATaxAdjust描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CBPTaxes));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PCBPTaxes));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ITBPTaxes));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Taxes1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Taxes2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Taxes3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Taxes4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P01)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P02)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P03)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P04)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATaxAdjust>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CBPTaxes = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			PCBPTaxes = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			ITBPTaxes = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			Taxes1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			Taxes2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			Taxes3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			Taxes4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			P01 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			P02 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			P03 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			P04 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATaxAdjustSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("CBPTaxes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CBPTaxes));
		}
		if (FCode.equals("PCBPTaxes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PCBPTaxes));
		}
		if (FCode.equals("ITBPTaxes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ITBPTaxes));
		}
		if (FCode.equals("Taxes1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Taxes1));
		}
		if (FCode.equals("Taxes2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Taxes2));
		}
		if (FCode.equals("Taxes3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Taxes3));
		}
		if (FCode.equals("Taxes4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Taxes4));
		}
		if (FCode.equals("P01"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P01));
		}
		if (FCode.equals("P02"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P02));
		}
		if (FCode.equals("P03"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P03));
		}
		if (FCode.equals("P04"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P04));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 5:
				strFieldValue = String.valueOf(CBPTaxes);
				break;
			case 6:
				strFieldValue = String.valueOf(PCBPTaxes);
				break;
			case 7:
				strFieldValue = String.valueOf(ITBPTaxes);
				break;
			case 8:
				strFieldValue = String.valueOf(Taxes1);
				break;
			case 9:
				strFieldValue = String.valueOf(Taxes2);
				break;
			case 10:
				strFieldValue = String.valueOf(Taxes3);
				break;
			case 11:
				strFieldValue = String.valueOf(Taxes4);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(P01);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(P02);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(P03);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(P04);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("CBPTaxes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CBPTaxes = d;
			}
		}
		if (FCode.equalsIgnoreCase("PCBPTaxes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PCBPTaxes = d;
			}
		}
		if (FCode.equalsIgnoreCase("ITBPTaxes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ITBPTaxes = d;
			}
		}
		if (FCode.equalsIgnoreCase("Taxes1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Taxes1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Taxes2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Taxes2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Taxes3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Taxes3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Taxes4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Taxes4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("P01"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P01 = FValue.trim();
			}
			else
				P01 = null;
		}
		if (FCode.equalsIgnoreCase("P02"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P02 = FValue.trim();
			}
			else
				P02 = null;
		}
		if (FCode.equalsIgnoreCase("P03"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P03 = FValue.trim();
			}
			else
				P03 = null;
		}
		if (FCode.equalsIgnoreCase("P04"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P04 = FValue.trim();
			}
			else
				P04 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LATaxAdjustSchema other = (LATaxAdjustSchema)otherObject;
		return
			(AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& CBPTaxes == other.getCBPTaxes()
			&& PCBPTaxes == other.getPCBPTaxes()
			&& ITBPTaxes == other.getITBPTaxes()
			&& Taxes1 == other.getTaxes1()
			&& Taxes2 == other.getTaxes2()
			&& Taxes3 == other.getTaxes3()
			&& Taxes4 == other.getTaxes4()
			&& (P01 == null ? other.getP01() == null : P01.equals(other.getP01()))
			&& (P02 == null ? other.getP02() == null : P02.equals(other.getP02()))
			&& (P03 == null ? other.getP03() == null : P03.equals(other.getP03()))
			&& (P04 == null ? other.getP04() == null : P04.equals(other.getP04()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("AgentCode") ) {
			return 0;
		}
		if( strFieldName.equals("WageNo") ) {
			return 1;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("BranchType") ) {
			return 3;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 4;
		}
		if( strFieldName.equals("CBPTaxes") ) {
			return 5;
		}
		if( strFieldName.equals("PCBPTaxes") ) {
			return 6;
		}
		if( strFieldName.equals("ITBPTaxes") ) {
			return 7;
		}
		if( strFieldName.equals("Taxes1") ) {
			return 8;
		}
		if( strFieldName.equals("Taxes2") ) {
			return 9;
		}
		if( strFieldName.equals("Taxes3") ) {
			return 10;
		}
		if( strFieldName.equals("Taxes4") ) {
			return 11;
		}
		if( strFieldName.equals("P01") ) {
			return 12;
		}
		if( strFieldName.equals("P02") ) {
			return 13;
		}
		if( strFieldName.equals("P03") ) {
			return 14;
		}
		if( strFieldName.equals("P04") ) {
			return 15;
		}
		if( strFieldName.equals("Operator") ) {
			return 16;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 17;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 20;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "AgentCode";
				break;
			case 1:
				strFieldName = "WageNo";
				break;
			case 2:
				strFieldName = "ManageCom";
				break;
			case 3:
				strFieldName = "BranchType";
				break;
			case 4:
				strFieldName = "BranchType2";
				break;
			case 5:
				strFieldName = "CBPTaxes";
				break;
			case 6:
				strFieldName = "PCBPTaxes";
				break;
			case 7:
				strFieldName = "ITBPTaxes";
				break;
			case 8:
				strFieldName = "Taxes1";
				break;
			case 9:
				strFieldName = "Taxes2";
				break;
			case 10:
				strFieldName = "Taxes3";
				break;
			case 11:
				strFieldName = "Taxes4";
				break;
			case 12:
				strFieldName = "P01";
				break;
			case 13:
				strFieldName = "P02";
				break;
			case 14:
				strFieldName = "P03";
				break;
			case 15:
				strFieldName = "P04";
				break;
			case 16:
				strFieldName = "Operator";
				break;
			case 17:
				strFieldName = "MakeDate";
				break;
			case 18:
				strFieldName = "MakeTime";
				break;
			case 19:
				strFieldName = "ModifyDate";
				break;
			case 20:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CBPTaxes") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PCBPTaxes") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ITBPTaxes") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Taxes1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Taxes2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Taxes3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Taxes4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P01") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P02") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P03") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P04") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
