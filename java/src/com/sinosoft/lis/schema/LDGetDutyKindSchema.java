/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDGetDutyKindDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDGetDutyKindSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 给付责任类型匹配表
 * @CreateDate：2005-04-27
 */
public class LDGetDutyKindSchema implements Schema, Cloneable
{
    // @Field
    /** 出险原因 */
    private String CauseCode;
    /** 理赔类型 */
    private String ClaimReason;
    /** 给付责任类型 */
    private String GetDutyKind;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDGetDutyKindSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "CauseCode";
        pk[1] = "ClaimReason";
        pk[2] = "GetDutyKind";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LDGetDutyKindSchema cloned = (LDGetDutyKindSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCauseCode()
    {
        if (SysConst.CHANGECHARSET && CauseCode != null && !CauseCode.equals(""))
        {
            CauseCode = StrTool.unicodeToGBK(CauseCode);
        }
        return CauseCode;
    }

    public void setCauseCode(String aCauseCode)
    {
        CauseCode = aCauseCode;
    }

    public String getClaimReason()
    {
        if (SysConst.CHANGECHARSET && ClaimReason != null &&
            !ClaimReason.equals(""))
        {
            ClaimReason = StrTool.unicodeToGBK(ClaimReason);
        }
        return ClaimReason;
    }

    public void setClaimReason(String aClaimReason)
    {
        ClaimReason = aClaimReason;
    }

    public String getGetDutyKind()
    {
        if (SysConst.CHANGECHARSET && GetDutyKind != null &&
            !GetDutyKind.equals(""))
        {
            GetDutyKind = StrTool.unicodeToGBK(GetDutyKind);
        }
        return GetDutyKind;
    }

    public void setGetDutyKind(String aGetDutyKind)
    {
        GetDutyKind = aGetDutyKind;
    }

    /**
     * 使用另外一个 LDGetDutyKindSchema 对象给 Schema 赋值
     * @param: aLDGetDutyKindSchema LDGetDutyKindSchema
     **/
    public void setSchema(LDGetDutyKindSchema aLDGetDutyKindSchema)
    {
        this.CauseCode = aLDGetDutyKindSchema.getCauseCode();
        this.ClaimReason = aLDGetDutyKindSchema.getClaimReason();
        this.GetDutyKind = aLDGetDutyKindSchema.getGetDutyKind();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CauseCode") == null)
            {
                this.CauseCode = null;
            }
            else
            {
                this.CauseCode = rs.getString("CauseCode").trim();
            }

            if (rs.getString("ClaimReason") == null)
            {
                this.ClaimReason = null;
            }
            else
            {
                this.ClaimReason = rs.getString("ClaimReason").trim();
            }

            if (rs.getString("GetDutyKind") == null)
            {
                this.GetDutyKind = null;
            }
            else
            {
                this.GetDutyKind = rs.getString("GetDutyKind").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGetDutyKindSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDGetDutyKindSchema getSchema()
    {
        LDGetDutyKindSchema aLDGetDutyKindSchema = new LDGetDutyKindSchema();
        aLDGetDutyKindSchema.setSchema(this);
        return aLDGetDutyKindSchema;
    }

    public LDGetDutyKindDB getDB()
    {
        LDGetDutyKindDB aDBOper = new LDGetDutyKindDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDGetDutyKind描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CauseCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ClaimReason)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GetDutyKind)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDGetDutyKind>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CauseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ClaimReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGetDutyKindSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CauseCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CauseCode));
        }
        if (FCode.equals("ClaimReason"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimReason));
        }
        if (FCode.equals("GetDutyKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CauseCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ClaimReason);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CauseCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CauseCode = FValue.trim();
            }
            else
            {
                CauseCode = null;
            }
        }
        if (FCode.equals("ClaimReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClaimReason = FValue.trim();
            }
            else
            {
                ClaimReason = null;
            }
        }
        if (FCode.equals("GetDutyKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
            {
                GetDutyKind = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDGetDutyKindSchema other = (LDGetDutyKindSchema) otherObject;
        return
                CauseCode.equals(other.getCauseCode())
                && ClaimReason.equals(other.getClaimReason())
                && GetDutyKind.equals(other.getGetDutyKind());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CauseCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ClaimReason"))
        {
            return 1;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CauseCode";
                break;
            case 1:
                strFieldName = "ClaimReason";
                break;
            case 2:
                strFieldName = "GetDutyKind";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CauseCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClaimReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
