/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIRuleDealErrLogDB;

/*
 * <p>ClassName: FIRuleDealErrLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIRuleDealErrLogSchema implements Schema, Cloneable
{
	// @Field
	/** 校验批次号码 */
	private String CheckBatchNo;
	/** 日志错误流水号码 */
	private String ErrSerialNo;
	/** 事件流水号 */
	private String EventNo;
	/** 校验事件结点 */
	private String CallPointID;
	/** 业务号码类型 */
	private String IndexCode;
	/** 业务号码 */
	private String BusinessNo;
	/** 关联的流水号码 */
	private String Aserialno;
	/** 关联cq号码 */
	private String CQNo;
	/** 校验规则 */
	private String RuleID;
	/** 错误信息 */
	private String ErrInfo;
	/** 校验计划 */
	private String RulePlanID;
	/** 处理状态 */
	private String DealState;
	/** 凭证类型 */
	private String CertificateID;

	public static final int FIELDNUM = 13;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIRuleDealErrLogSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ErrSerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIRuleDealErrLogSchema cloned = (FIRuleDealErrLogSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCheckBatchNo()
	{
		return CheckBatchNo;
	}
	public void setCheckBatchNo(String aCheckBatchNo)
	{
		CheckBatchNo = aCheckBatchNo;
	}
	public String getErrSerialNo()
	{
		return ErrSerialNo;
	}
	public void setErrSerialNo(String aErrSerialNo)
	{
		ErrSerialNo = aErrSerialNo;
	}
	public String getEventNo()
	{
		return EventNo;
	}
	public void setEventNo(String aEventNo)
	{
		EventNo = aEventNo;
	}
	public String getCallPointID()
	{
		return CallPointID;
	}
	public void setCallPointID(String aCallPointID)
	{
		CallPointID = aCallPointID;
	}
	public String getIndexCode()
	{
		return IndexCode;
	}
	public void setIndexCode(String aIndexCode)
	{
		IndexCode = aIndexCode;
	}
	public String getBusinessNo()
	{
		return BusinessNo;
	}
	public void setBusinessNo(String aBusinessNo)
	{
		BusinessNo = aBusinessNo;
	}
	public String getAserialno()
	{
		return Aserialno;
	}
	public void setAserialno(String aAserialno)
	{
		Aserialno = aAserialno;
	}
	public String getCQNo()
	{
		return CQNo;
	}
	public void setCQNo(String aCQNo)
	{
		CQNo = aCQNo;
	}
	public String getRuleID()
	{
		return RuleID;
	}
	public void setRuleID(String aRuleID)
	{
		RuleID = aRuleID;
	}
	public String getErrInfo()
	{
		return ErrInfo;
	}
	public void setErrInfo(String aErrInfo)
	{
		ErrInfo = aErrInfo;
	}
	public String getRulePlanID()
	{
		return RulePlanID;
	}
	public void setRulePlanID(String aRulePlanID)
	{
		RulePlanID = aRulePlanID;
	}
	public String getDealState()
	{
		return DealState;
	}
	public void setDealState(String aDealState)
	{
		DealState = aDealState;
	}
	public String getCertificateID()
	{
		return CertificateID;
	}
	public void setCertificateID(String aCertificateID)
	{
		CertificateID = aCertificateID;
	}

	/**
	* 使用另外一个 FIRuleDealErrLogSchema 对象给 Schema 赋值
	* @param: aFIRuleDealErrLogSchema FIRuleDealErrLogSchema
	**/
	public void setSchema(FIRuleDealErrLogSchema aFIRuleDealErrLogSchema)
	{
		this.CheckBatchNo = aFIRuleDealErrLogSchema.getCheckBatchNo();
		this.ErrSerialNo = aFIRuleDealErrLogSchema.getErrSerialNo();
		this.EventNo = aFIRuleDealErrLogSchema.getEventNo();
		this.CallPointID = aFIRuleDealErrLogSchema.getCallPointID();
		this.IndexCode = aFIRuleDealErrLogSchema.getIndexCode();
		this.BusinessNo = aFIRuleDealErrLogSchema.getBusinessNo();
		this.Aserialno = aFIRuleDealErrLogSchema.getAserialno();
		this.CQNo = aFIRuleDealErrLogSchema.getCQNo();
		this.RuleID = aFIRuleDealErrLogSchema.getRuleID();
		this.ErrInfo = aFIRuleDealErrLogSchema.getErrInfo();
		this.RulePlanID = aFIRuleDealErrLogSchema.getRulePlanID();
		this.DealState = aFIRuleDealErrLogSchema.getDealState();
		this.CertificateID = aFIRuleDealErrLogSchema.getCertificateID();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CheckBatchNo") == null )
				this.CheckBatchNo = null;
			else
				this.CheckBatchNo = rs.getString("CheckBatchNo").trim();

			if( rs.getString("ErrSerialNo") == null )
				this.ErrSerialNo = null;
			else
				this.ErrSerialNo = rs.getString("ErrSerialNo").trim();

			if( rs.getString("EventNo") == null )
				this.EventNo = null;
			else
				this.EventNo = rs.getString("EventNo").trim();

			if( rs.getString("CallPointID") == null )
				this.CallPointID = null;
			else
				this.CallPointID = rs.getString("CallPointID").trim();

			if( rs.getString("IndexCode") == null )
				this.IndexCode = null;
			else
				this.IndexCode = rs.getString("IndexCode").trim();

			if( rs.getString("BusinessNo") == null )
				this.BusinessNo = null;
			else
				this.BusinessNo = rs.getString("BusinessNo").trim();

			if( rs.getString("Aserialno") == null )
				this.Aserialno = null;
			else
				this.Aserialno = rs.getString("Aserialno").trim();

			if( rs.getString("CQNo") == null )
				this.CQNo = null;
			else
				this.CQNo = rs.getString("CQNo").trim();

			if( rs.getString("RuleID") == null )
				this.RuleID = null;
			else
				this.RuleID = rs.getString("RuleID").trim();

			if( rs.getString("ErrInfo") == null )
				this.ErrInfo = null;
			else
				this.ErrInfo = rs.getString("ErrInfo").trim();

			if( rs.getString("RulePlanID") == null )
				this.RulePlanID = null;
			else
				this.RulePlanID = rs.getString("RulePlanID").trim();

			if( rs.getString("DealState") == null )
				this.DealState = null;
			else
				this.DealState = rs.getString("DealState").trim();

			if( rs.getString("CertificateID") == null )
				this.CertificateID = null;
			else
				this.CertificateID = rs.getString("CertificateID").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIRuleDealErrLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIRuleDealErrLogSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIRuleDealErrLogSchema getSchema()
	{
		FIRuleDealErrLogSchema aFIRuleDealErrLogSchema = new FIRuleDealErrLogSchema();
		aFIRuleDealErrLogSchema.setSchema(this);
		return aFIRuleDealErrLogSchema;
	}

	public FIRuleDealErrLogDB getDB()
	{
		FIRuleDealErrLogDB aDBOper = new FIRuleDealErrLogDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIRuleDealErrLog描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CheckBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EventNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CallPointID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Aserialno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CQNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrInfo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RulePlanID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertificateID));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIRuleDealErrLog>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CheckBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ErrSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			EventNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CallPointID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BusinessNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Aserialno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CQNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RuleID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ErrInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			RulePlanID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			DealState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			CertificateID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIRuleDealErrLogSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CheckBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckBatchNo));
		}
		if (FCode.equals("ErrSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrSerialNo));
		}
		if (FCode.equals("EventNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EventNo));
		}
		if (FCode.equals("CallPointID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CallPointID));
		}
		if (FCode.equals("IndexCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
		}
		if (FCode.equals("BusinessNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessNo));
		}
		if (FCode.equals("Aserialno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Aserialno));
		}
		if (FCode.equals("CQNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CQNo));
		}
		if (FCode.equals("RuleID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleID));
		}
		if (FCode.equals("ErrInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrInfo));
		}
		if (FCode.equals("RulePlanID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RulePlanID));
		}
		if (FCode.equals("DealState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealState));
		}
		if (FCode.equals("CertificateID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertificateID));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CheckBatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ErrSerialNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(EventNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CallPointID);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(IndexCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BusinessNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Aserialno);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CQNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RuleID);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ErrInfo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(RulePlanID);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(DealState);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(CertificateID);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CheckBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckBatchNo = FValue.trim();
			}
			else
				CheckBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("ErrSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrSerialNo = FValue.trim();
			}
			else
				ErrSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("EventNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EventNo = FValue.trim();
			}
			else
				EventNo = null;
		}
		if (FCode.equalsIgnoreCase("CallPointID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CallPointID = FValue.trim();
			}
			else
				CallPointID = null;
		}
		if (FCode.equalsIgnoreCase("IndexCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCode = FValue.trim();
			}
			else
				IndexCode = null;
		}
		if (FCode.equalsIgnoreCase("BusinessNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessNo = FValue.trim();
			}
			else
				BusinessNo = null;
		}
		if (FCode.equalsIgnoreCase("Aserialno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Aserialno = FValue.trim();
			}
			else
				Aserialno = null;
		}
		if (FCode.equalsIgnoreCase("CQNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CQNo = FValue.trim();
			}
			else
				CQNo = null;
		}
		if (FCode.equalsIgnoreCase("RuleID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleID = FValue.trim();
			}
			else
				RuleID = null;
		}
		if (FCode.equalsIgnoreCase("ErrInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrInfo = FValue.trim();
			}
			else
				ErrInfo = null;
		}
		if (FCode.equalsIgnoreCase("RulePlanID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RulePlanID = FValue.trim();
			}
			else
				RulePlanID = null;
		}
		if (FCode.equalsIgnoreCase("DealState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealState = FValue.trim();
			}
			else
				DealState = null;
		}
		if (FCode.equalsIgnoreCase("CertificateID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertificateID = FValue.trim();
			}
			else
				CertificateID = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIRuleDealErrLogSchema other = (FIRuleDealErrLogSchema)otherObject;
		return
			(CheckBatchNo == null ? other.getCheckBatchNo() == null : CheckBatchNo.equals(other.getCheckBatchNo()))
			&& (ErrSerialNo == null ? other.getErrSerialNo() == null : ErrSerialNo.equals(other.getErrSerialNo()))
			&& (EventNo == null ? other.getEventNo() == null : EventNo.equals(other.getEventNo()))
			&& (CallPointID == null ? other.getCallPointID() == null : CallPointID.equals(other.getCallPointID()))
			&& (IndexCode == null ? other.getIndexCode() == null : IndexCode.equals(other.getIndexCode()))
			&& (BusinessNo == null ? other.getBusinessNo() == null : BusinessNo.equals(other.getBusinessNo()))
			&& (Aserialno == null ? other.getAserialno() == null : Aserialno.equals(other.getAserialno()))
			&& (CQNo == null ? other.getCQNo() == null : CQNo.equals(other.getCQNo()))
			&& (RuleID == null ? other.getRuleID() == null : RuleID.equals(other.getRuleID()))
			&& (ErrInfo == null ? other.getErrInfo() == null : ErrInfo.equals(other.getErrInfo()))
			&& (RulePlanID == null ? other.getRulePlanID() == null : RulePlanID.equals(other.getRulePlanID()))
			&& (DealState == null ? other.getDealState() == null : DealState.equals(other.getDealState()))
			&& (CertificateID == null ? other.getCertificateID() == null : CertificateID.equals(other.getCertificateID()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CheckBatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("ErrSerialNo") ) {
			return 1;
		}
		if( strFieldName.equals("EventNo") ) {
			return 2;
		}
		if( strFieldName.equals("CallPointID") ) {
			return 3;
		}
		if( strFieldName.equals("IndexCode") ) {
			return 4;
		}
		if( strFieldName.equals("BusinessNo") ) {
			return 5;
		}
		if( strFieldName.equals("Aserialno") ) {
			return 6;
		}
		if( strFieldName.equals("CQNo") ) {
			return 7;
		}
		if( strFieldName.equals("RuleID") ) {
			return 8;
		}
		if( strFieldName.equals("ErrInfo") ) {
			return 9;
		}
		if( strFieldName.equals("RulePlanID") ) {
			return 10;
		}
		if( strFieldName.equals("DealState") ) {
			return 11;
		}
		if( strFieldName.equals("CertificateID") ) {
			return 12;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CheckBatchNo";
				break;
			case 1:
				strFieldName = "ErrSerialNo";
				break;
			case 2:
				strFieldName = "EventNo";
				break;
			case 3:
				strFieldName = "CallPointID";
				break;
			case 4:
				strFieldName = "IndexCode";
				break;
			case 5:
				strFieldName = "BusinessNo";
				break;
			case 6:
				strFieldName = "Aserialno";
				break;
			case 7:
				strFieldName = "CQNo";
				break;
			case 8:
				strFieldName = "RuleID";
				break;
			case 9:
				strFieldName = "ErrInfo";
				break;
			case 10:
				strFieldName = "RulePlanID";
				break;
			case 11:
				strFieldName = "DealState";
				break;
			case 12:
				strFieldName = "CertificateID";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CheckBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EventNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CallPointID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Aserialno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CQNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RulePlanID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertificateID") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
