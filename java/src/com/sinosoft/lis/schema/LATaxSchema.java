/*
 * <p>ClassName: LATaxSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LATaxDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

public class LATaxSchema implements Schema
{
    // @Field
    /** 地区编码 */
    private String ManageCom;
    /** 税率类型 */
    private String TaxType;
    /** 税率代码 */
    private int TaxCode;
    /** 基数上限 */
    private double BaseMax;
    /** 基数下限 */
    private double BaseMin;
    /** 计算比率1 */
    private double CalRate1;
    /** 计算比率2 */
    private double CalRate2;
    /** 计算基数1 */
    private double CalValue1;
    /** 计算基数2 */
    private double CalValue2;
    /** 税率 */
    private double TaxRate;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LATaxSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ManageCom";
        pk[1] = "TaxType";
        pk[2] = "TaxCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getTaxType()
    {
        if (TaxType != null && !TaxType.equals("") && SysConst.CHANGECHARSET)
        {
            TaxType = StrTool.unicodeToGBK(TaxType);
        }
        return TaxType;
    }

    public void setTaxType(String aTaxType)
    {
        TaxType = aTaxType;
    }

    public int getTaxCode()
    {
        return TaxCode;
    }

    public void setTaxCode(int aTaxCode)
    {
        TaxCode = aTaxCode;
    }

    public void setTaxCode(String aTaxCode)
    {
        if (aTaxCode != null && !aTaxCode.equals(""))
        {
            Integer tInteger = new Integer(aTaxCode);
            int i = tInteger.intValue();
            TaxCode = i;
        }
    }

    public double getBaseMax()
    {
        return BaseMax;
    }

    public void setBaseMax(double aBaseMax)
    {
        BaseMax = aBaseMax;
    }

    public void setBaseMax(String aBaseMax)
    {
        if (aBaseMax != null && !aBaseMax.equals(""))
        {
            Double tDouble = new Double(aBaseMax);
            double d = tDouble.doubleValue();
            BaseMax = d;
        }
    }

    public double getBaseMin()
    {
        return BaseMin;
    }

    public void setBaseMin(double aBaseMin)
    {
        BaseMin = aBaseMin;
    }

    public void setBaseMin(String aBaseMin)
    {
        if (aBaseMin != null && !aBaseMin.equals(""))
        {
            Double tDouble = new Double(aBaseMin);
            double d = tDouble.doubleValue();
            BaseMin = d;
        }
    }

    public double getCalRate1()
    {
        return CalRate1;
    }

    public void setCalRate1(double aCalRate1)
    {
        CalRate1 = aCalRate1;
    }

    public void setCalRate1(String aCalRate1)
    {
        if (aCalRate1 != null && !aCalRate1.equals(""))
        {
            Double tDouble = new Double(aCalRate1);
            double d = tDouble.doubleValue();
            CalRate1 = d;
        }
    }

    public double getCalRate2()
    {
        return CalRate2;
    }

    public void setCalRate2(double aCalRate2)
    {
        CalRate2 = aCalRate2;
    }

    public void setCalRate2(String aCalRate2)
    {
        if (aCalRate2 != null && !aCalRate2.equals(""))
        {
            Double tDouble = new Double(aCalRate2);
            double d = tDouble.doubleValue();
            CalRate2 = d;
        }
    }

    public double getCalValue1()
    {
        return CalValue1;
    }

    public void setCalValue1(double aCalValue1)
    {
        CalValue1 = aCalValue1;
    }

    public void setCalValue1(String aCalValue1)
    {
        if (aCalValue1 != null && !aCalValue1.equals(""))
        {
            Double tDouble = new Double(aCalValue1);
            double d = tDouble.doubleValue();
            CalValue1 = d;
        }
    }

    public double getCalValue2()
    {
        return CalValue2;
    }

    public void setCalValue2(double aCalValue2)
    {
        CalValue2 = aCalValue2;
    }

    public void setCalValue2(String aCalValue2)
    {
        if (aCalValue2 != null && !aCalValue2.equals(""))
        {
            Double tDouble = new Double(aCalValue2);
            double d = tDouble.doubleValue();
            CalValue2 = d;
        }
    }

    public double getTaxRate()
    {
        return TaxRate;
    }

    public void setTaxRate(double aTaxRate)
    {
        TaxRate = aTaxRate;
    }

    public void setTaxRate(String aTaxRate)
    {
        if (aTaxRate != null && !aTaxRate.equals(""))
        {
            Double tDouble = new Double(aTaxRate);
            double d = tDouble.doubleValue();
            TaxRate = d;
        }
    }


    /**
     * 使用另外一个 LATaxSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LATaxSchema aLATaxSchema)
    {
        this.ManageCom = aLATaxSchema.getManageCom();
        this.TaxType = aLATaxSchema.getTaxType();
        this.TaxCode = aLATaxSchema.getTaxCode();
        this.BaseMax = aLATaxSchema.getBaseMax();
        this.BaseMin = aLATaxSchema.getBaseMin();
        this.CalRate1 = aLATaxSchema.getCalRate1();
        this.CalRate2 = aLATaxSchema.getCalRate2();
        this.CalValue1 = aLATaxSchema.getCalValue1();
        this.CalValue2 = aLATaxSchema.getCalValue2();
        this.TaxRate = aLATaxSchema.getTaxRate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("TaxType") == null)
            {
                this.TaxType = null;
            }
            else
            {
                this.TaxType = rs.getString("TaxType").trim();
            }

            this.TaxCode = rs.getInt("TaxCode");
            this.BaseMax = rs.getDouble("BaseMax");
            this.BaseMin = rs.getDouble("BaseMin");
            this.CalRate1 = rs.getDouble("CalRate1");
            this.CalRate2 = rs.getDouble("CalRate2");
            this.CalValue1 = rs.getDouble("CalValue1");
            this.CalValue2 = rs.getDouble("CalValue2");
            this.TaxRate = rs.getDouble("TaxRate");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATaxSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LATaxSchema getSchema()
    {
        LATaxSchema aLATaxSchema = new LATaxSchema();
        aLATaxSchema.setSchema(this);
        return aLATaxSchema;
    }

    public LATaxDB getDB()
    {
        LATaxDB aDBOper = new LATaxDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATax描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TaxType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(TaxCode) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(BaseMax) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(BaseMin) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CalRate1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CalRate2) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CalValue1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CalValue2) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(TaxRate);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATax>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            TaxType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            TaxCode = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).intValue();
            BaseMax = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            BaseMin = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            CalRate1 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            CalRate2 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            CalValue1 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            CalValue2 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            TaxRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATaxSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("TaxType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxType));
        }
        if (FCode.equals("TaxCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxCode));
        }
        if (FCode.equals("BaseMax"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BaseMax));
        }
        if (FCode.equals("BaseMin"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BaseMin));
        }
        if (FCode.equals("CalRate1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalRate1));
        }
        if (FCode.equals("CalRate2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalRate2));
        }
        if (FCode.equals("CalValue1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalValue1));
        }
        if (FCode.equals("CalValue2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalValue2));
        }
        if (FCode.equals("TaxRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaxRate));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TaxType);
                break;
            case 2:
                strFieldValue = String.valueOf(TaxCode);
                break;
            case 3:
                strFieldValue = String.valueOf(BaseMax);
                break;
            case 4:
                strFieldValue = String.valueOf(BaseMin);
                break;
            case 5:
                strFieldValue = String.valueOf(CalRate1);
                break;
            case 6:
                strFieldValue = String.valueOf(CalRate2);
                break;
            case 7:
                strFieldValue = String.valueOf(CalValue1);
                break;
            case 8:
                strFieldValue = String.valueOf(CalValue2);
                break;
            case 9:
                strFieldValue = String.valueOf(TaxRate);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("TaxType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TaxType = FValue.trim();
            }
            else
            {
                TaxType = null;
            }
        }
        if (FCode.equals("TaxCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                TaxCode = i;
            }
        }
        if (FCode.equals("BaseMax"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BaseMax = d;
            }
        }
        if (FCode.equals("BaseMin"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BaseMin = d;
            }
        }
        if (FCode.equals("CalRate1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CalRate1 = d;
            }
        }
        if (FCode.equals("CalRate2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CalRate2 = d;
            }
        }
        if (FCode.equals("CalValue1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CalValue1 = d;
            }
        }
        if (FCode.equals("CalValue2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CalValue2 = d;
            }
        }
        if (FCode.equals("TaxRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TaxRate = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LATaxSchema other = (LATaxSchema) otherObject;
        return
                ManageCom.equals(other.getManageCom())
                && TaxType.equals(other.getTaxType())
                && TaxCode == other.getTaxCode()
                && BaseMax == other.getBaseMax()
                && BaseMin == other.getBaseMin()
                && CalRate1 == other.getCalRate1()
                && CalRate2 == other.getCalRate2()
                && CalValue1 == other.getCalValue1()
                && CalValue2 == other.getCalValue2()
                && TaxRate == other.getTaxRate();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return 0;
        }
        if (strFieldName.equals("TaxType"))
        {
            return 1;
        }
        if (strFieldName.equals("TaxCode"))
        {
            return 2;
        }
        if (strFieldName.equals("BaseMax"))
        {
            return 3;
        }
        if (strFieldName.equals("BaseMin"))
        {
            return 4;
        }
        if (strFieldName.equals("CalRate1"))
        {
            return 5;
        }
        if (strFieldName.equals("CalRate2"))
        {
            return 6;
        }
        if (strFieldName.equals("CalValue1"))
        {
            return 7;
        }
        if (strFieldName.equals("CalValue2"))
        {
            return 8;
        }
        if (strFieldName.equals("TaxRate"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "TaxType";
                break;
            case 2:
                strFieldName = "TaxCode";
                break;
            case 3:
                strFieldName = "BaseMax";
                break;
            case 4:
                strFieldName = "BaseMin";
                break;
            case 5:
                strFieldName = "CalRate1";
                break;
            case 6:
                strFieldName = "CalRate2";
                break;
            case 7:
                strFieldName = "CalValue1";
                break;
            case 8:
                strFieldName = "CalValue2";
                break;
            case 9:
                strFieldName = "TaxRate";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TaxType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TaxCode"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("BaseMax"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BaseMin"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CalRate1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CalRate2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CalValue1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CalValue2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("TaxRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_INT;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
