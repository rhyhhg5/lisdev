/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBGrpIssuePolDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LOBGrpIssuePolSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 问题件
 * @CreateDate：2005-04-02
 */
public class LOBGrpIssuePolSchema implements Schema
{
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体合同投保单号码 */
    private String ProposalGrpContNo;
    /** 打印流水号 */
    private String PrtSeq;
    /** 流水号 */
    private String SerialNo;
    /** 问题件字段 */
    private String FieldName;
    /** 字段页面位置 */
    private String Location;
    /** 问题件类型 */
    private String IssueType;
    /** 操作位置 */
    private String OperatePos;
    /** 退回对象类型 */
    private String BackObjType;
    /** 退回对象 */
    private String BackObj;
    /** 问题件所在管理机构 */
    private String IsueManageCom;
    /** 问题件内容 */
    private String IssueCont;
    /** 打印次数 */
    private int PrintCount;
    /** 是否打印标志 */
    private String NeedPrint;
    /** 回复人 */
    private String ReplyMan;
    /** 回复结果 */
    private String ReplyResult;
    /** 状态 */
    private String State;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String ManageCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 问题对象 */
    private String QuestionObj;
    /** 错误字段 */
    private String ErrField;
    /** 错误字段名称 */
    private String ErrFieldName;
    /** 原填写内容 */
    private String ErrContent;

    public static final int FIELDNUM = 27; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBGrpIssuePolSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ProposalGrpContNo";
        pk[1] = "SerialNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        if (SysConst.CHANGECHARSET && GrpContNo != null && !GrpContNo.equals(""))
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getProposalGrpContNo()
    {
        if (SysConst.CHANGECHARSET && ProposalGrpContNo != null &&
            !ProposalGrpContNo.equals(""))
        {
            ProposalGrpContNo = StrTool.unicodeToGBK(ProposalGrpContNo);
        }
        return ProposalGrpContNo;
    }

    public void setProposalGrpContNo(String aProposalGrpContNo)
    {
        ProposalGrpContNo = aProposalGrpContNo;
    }

    public String getPrtSeq()
    {
        if (SysConst.CHANGECHARSET && PrtSeq != null && !PrtSeq.equals(""))
        {
            PrtSeq = StrTool.unicodeToGBK(PrtSeq);
        }
        return PrtSeq;
    }

    public void setPrtSeq(String aPrtSeq)
    {
        PrtSeq = aPrtSeq;
    }

    public String getSerialNo()
    {
        if (SysConst.CHANGECHARSET && SerialNo != null && !SerialNo.equals(""))
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getFieldName()
    {
        if (SysConst.CHANGECHARSET && FieldName != null && !FieldName.equals(""))
        {
            FieldName = StrTool.unicodeToGBK(FieldName);
        }
        return FieldName;
    }

    public void setFieldName(String aFieldName)
    {
        FieldName = aFieldName;
    }

    public String getLocation()
    {
        if (SysConst.CHANGECHARSET && Location != null && !Location.equals(""))
        {
            Location = StrTool.unicodeToGBK(Location);
        }
        return Location;
    }

    public void setLocation(String aLocation)
    {
        Location = aLocation;
    }

    public String getIssueType()
    {
        if (SysConst.CHANGECHARSET && IssueType != null && !IssueType.equals(""))
        {
            IssueType = StrTool.unicodeToGBK(IssueType);
        }
        return IssueType;
    }

    public void setIssueType(String aIssueType)
    {
        IssueType = aIssueType;
    }

    public String getOperatePos()
    {
        if (SysConst.CHANGECHARSET && OperatePos != null &&
            !OperatePos.equals(""))
        {
            OperatePos = StrTool.unicodeToGBK(OperatePos);
        }
        return OperatePos;
    }

    public void setOperatePos(String aOperatePos)
    {
        OperatePos = aOperatePos;
    }

    public String getBackObjType()
    {
        if (SysConst.CHANGECHARSET && BackObjType != null &&
            !BackObjType.equals(""))
        {
            BackObjType = StrTool.unicodeToGBK(BackObjType);
        }
        return BackObjType;
    }

    public void setBackObjType(String aBackObjType)
    {
        BackObjType = aBackObjType;
    }

    public String getBackObj()
    {
        if (SysConst.CHANGECHARSET && BackObj != null && !BackObj.equals(""))
        {
            BackObj = StrTool.unicodeToGBK(BackObj);
        }
        return BackObj;
    }

    public void setBackObj(String aBackObj)
    {
        BackObj = aBackObj;
    }

    public String getIsueManageCom()
    {
        if (SysConst.CHANGECHARSET && IsueManageCom != null &&
            !IsueManageCom.equals(""))
        {
            IsueManageCom = StrTool.unicodeToGBK(IsueManageCom);
        }
        return IsueManageCom;
    }

    public void setIsueManageCom(String aIsueManageCom)
    {
        IsueManageCom = aIsueManageCom;
    }

    public String getIssueCont()
    {
        if (SysConst.CHANGECHARSET && IssueCont != null && !IssueCont.equals(""))
        {
            IssueCont = StrTool.unicodeToGBK(IssueCont);
        }
        return IssueCont;
    }

    public void setIssueCont(String aIssueCont)
    {
        IssueCont = aIssueCont;
    }

    public int getPrintCount()
    {
        return PrintCount;
    }

    public void setPrintCount(int aPrintCount)
    {
        PrintCount = aPrintCount;
    }

    public void setPrintCount(String aPrintCount)
    {
        if (aPrintCount != null && !aPrintCount.equals(""))
        {
            Integer tInteger = new Integer(aPrintCount);
            int i = tInteger.intValue();
            PrintCount = i;
        }
    }

    public String getNeedPrint()
    {
        if (SysConst.CHANGECHARSET && NeedPrint != null && !NeedPrint.equals(""))
        {
            NeedPrint = StrTool.unicodeToGBK(NeedPrint);
        }
        return NeedPrint;
    }

    public void setNeedPrint(String aNeedPrint)
    {
        NeedPrint = aNeedPrint;
    }

    public String getReplyMan()
    {
        if (SysConst.CHANGECHARSET && ReplyMan != null && !ReplyMan.equals(""))
        {
            ReplyMan = StrTool.unicodeToGBK(ReplyMan);
        }
        return ReplyMan;
    }

    public void setReplyMan(String aReplyMan)
    {
        ReplyMan = aReplyMan;
    }

    public String getReplyResult()
    {
        if (SysConst.CHANGECHARSET && ReplyResult != null &&
            !ReplyResult.equals(""))
        {
            ReplyResult = StrTool.unicodeToGBK(ReplyResult);
        }
        return ReplyResult;
    }

    public void setReplyResult(String aReplyResult)
    {
        ReplyResult = aReplyResult;
    }

    public String getState()
    {
        if (SysConst.CHANGECHARSET && State != null && !State.equals(""))
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getQuestionObj()
    {
        if (SysConst.CHANGECHARSET && QuestionObj != null &&
            !QuestionObj.equals(""))
        {
            QuestionObj = StrTool.unicodeToGBK(QuestionObj);
        }
        return QuestionObj;
    }

    public void setQuestionObj(String aQuestionObj)
    {
        QuestionObj = aQuestionObj;
    }

    public String getErrField()
    {
        if (SysConst.CHANGECHARSET && ErrField != null && !ErrField.equals(""))
        {
            ErrField = StrTool.unicodeToGBK(ErrField);
        }
        return ErrField;
    }

    public void setErrField(String aErrField)
    {
        ErrField = aErrField;
    }

    public String getErrFieldName()
    {
        if (SysConst.CHANGECHARSET && ErrFieldName != null &&
            !ErrFieldName.equals(""))
        {
            ErrFieldName = StrTool.unicodeToGBK(ErrFieldName);
        }
        return ErrFieldName;
    }

    public void setErrFieldName(String aErrFieldName)
    {
        ErrFieldName = aErrFieldName;
    }

    public String getErrContent()
    {
        if (SysConst.CHANGECHARSET && ErrContent != null &&
            !ErrContent.equals(""))
        {
            ErrContent = StrTool.unicodeToGBK(ErrContent);
        }
        return ErrContent;
    }

    public void setErrContent(String aErrContent)
    {
        ErrContent = aErrContent;
    }

    /**
     * 使用另外一个 LOBGrpIssuePolSchema 对象给 Schema 赋值
     * @param: aLOBGrpIssuePolSchema LOBGrpIssuePolSchema
     **/
    public void setSchema(LOBGrpIssuePolSchema aLOBGrpIssuePolSchema)
    {
        this.GrpContNo = aLOBGrpIssuePolSchema.getGrpContNo();
        this.ProposalGrpContNo = aLOBGrpIssuePolSchema.getProposalGrpContNo();
        this.PrtSeq = aLOBGrpIssuePolSchema.getPrtSeq();
        this.SerialNo = aLOBGrpIssuePolSchema.getSerialNo();
        this.FieldName = aLOBGrpIssuePolSchema.getFieldName();
        this.Location = aLOBGrpIssuePolSchema.getLocation();
        this.IssueType = aLOBGrpIssuePolSchema.getIssueType();
        this.OperatePos = aLOBGrpIssuePolSchema.getOperatePos();
        this.BackObjType = aLOBGrpIssuePolSchema.getBackObjType();
        this.BackObj = aLOBGrpIssuePolSchema.getBackObj();
        this.IsueManageCom = aLOBGrpIssuePolSchema.getIsueManageCom();
        this.IssueCont = aLOBGrpIssuePolSchema.getIssueCont();
        this.PrintCount = aLOBGrpIssuePolSchema.getPrintCount();
        this.NeedPrint = aLOBGrpIssuePolSchema.getNeedPrint();
        this.ReplyMan = aLOBGrpIssuePolSchema.getReplyMan();
        this.ReplyResult = aLOBGrpIssuePolSchema.getReplyResult();
        this.State = aLOBGrpIssuePolSchema.getState();
        this.Operator = aLOBGrpIssuePolSchema.getOperator();
        this.ManageCom = aLOBGrpIssuePolSchema.getManageCom();
        this.MakeDate = fDate.getDate(aLOBGrpIssuePolSchema.getMakeDate());
        this.MakeTime = aLOBGrpIssuePolSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBGrpIssuePolSchema.getModifyDate());
        this.ModifyTime = aLOBGrpIssuePolSchema.getModifyTime();
        this.QuestionObj = aLOBGrpIssuePolSchema.getQuestionObj();
        this.ErrField = aLOBGrpIssuePolSchema.getErrField();
        this.ErrFieldName = aLOBGrpIssuePolSchema.getErrFieldName();
        this.ErrContent = aLOBGrpIssuePolSchema.getErrContent();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ProposalGrpContNo") == null)
            {
                this.ProposalGrpContNo = null;
            }
            else
            {
                this.ProposalGrpContNo = rs.getString("ProposalGrpContNo").trim();
            }

            if (rs.getString("PrtSeq") == null)
            {
                this.PrtSeq = null;
            }
            else
            {
                this.PrtSeq = rs.getString("PrtSeq").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("FieldName") == null)
            {
                this.FieldName = null;
            }
            else
            {
                this.FieldName = rs.getString("FieldName").trim();
            }

            if (rs.getString("Location") == null)
            {
                this.Location = null;
            }
            else
            {
                this.Location = rs.getString("Location").trim();
            }

            if (rs.getString("IssueType") == null)
            {
                this.IssueType = null;
            }
            else
            {
                this.IssueType = rs.getString("IssueType").trim();
            }

            if (rs.getString("OperatePos") == null)
            {
                this.OperatePos = null;
            }
            else
            {
                this.OperatePos = rs.getString("OperatePos").trim();
            }

            if (rs.getString("BackObjType") == null)
            {
                this.BackObjType = null;
            }
            else
            {
                this.BackObjType = rs.getString("BackObjType").trim();
            }

            if (rs.getString("BackObj") == null)
            {
                this.BackObj = null;
            }
            else
            {
                this.BackObj = rs.getString("BackObj").trim();
            }

            if (rs.getString("IsueManageCom") == null)
            {
                this.IsueManageCom = null;
            }
            else
            {
                this.IsueManageCom = rs.getString("IsueManageCom").trim();
            }

            if (rs.getString("IssueCont") == null)
            {
                this.IssueCont = null;
            }
            else
            {
                this.IssueCont = rs.getString("IssueCont").trim();
            }

            this.PrintCount = rs.getInt("PrintCount");
            if (rs.getString("NeedPrint") == null)
            {
                this.NeedPrint = null;
            }
            else
            {
                this.NeedPrint = rs.getString("NeedPrint").trim();
            }

            if (rs.getString("ReplyMan") == null)
            {
                this.ReplyMan = null;
            }
            else
            {
                this.ReplyMan = rs.getString("ReplyMan").trim();
            }

            if (rs.getString("ReplyResult") == null)
            {
                this.ReplyResult = null;
            }
            else
            {
                this.ReplyResult = rs.getString("ReplyResult").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("QuestionObj") == null)
            {
                this.QuestionObj = null;
            }
            else
            {
                this.QuestionObj = rs.getString("QuestionObj").trim();
            }

            if (rs.getString("ErrField") == null)
            {
                this.ErrField = null;
            }
            else
            {
                this.ErrField = rs.getString("ErrField").trim();
            }

            if (rs.getString("ErrFieldName") == null)
            {
                this.ErrFieldName = null;
            }
            else
            {
                this.ErrFieldName = rs.getString("ErrFieldName").trim();
            }

            if (rs.getString("ErrContent") == null)
            {
                this.ErrContent = null;
            }
            else
            {
                this.ErrContent = rs.getString("ErrContent").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBGrpIssuePolSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOBGrpIssuePolSchema getSchema()
    {
        LOBGrpIssuePolSchema aLOBGrpIssuePolSchema = new LOBGrpIssuePolSchema();
        aLOBGrpIssuePolSchema.setSchema(this);
        return aLOBGrpIssuePolSchema;
    }

    public LOBGrpIssuePolDB getDB()
    {
        LOBGrpIssuePolDB aDBOper = new LOBGrpIssuePolDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBGrpIssuePol描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ProposalGrpContNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(PrtSeq)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(FieldName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Location)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IssueType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(OperatePos)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BackObjType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BackObj)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IsueManageCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(IssueCont)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PrintCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(NeedPrint)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ReplyMan)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ReplyResult)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(State)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(QuestionObj)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ErrField)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ErrFieldName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ErrContent)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBGrpIssuePol>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ProposalGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               2, SysConst.PACKAGESPILTER);
            PrtSeq = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            FieldName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            Location = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            IssueType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            OperatePos = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            BackObjType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            BackObj = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            IsueManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                           SysConst.PACKAGESPILTER);
            IssueCont = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            PrintCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).intValue();
            NeedPrint = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            ReplyMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ReplyResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                   SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                        SysConst.PACKAGESPILTER);
            QuestionObj = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                         SysConst.PACKAGESPILTER);
            ErrField = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                      SysConst.PACKAGESPILTER);
            ErrFieldName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                          SysConst.PACKAGESPILTER);
            ErrContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBGrpIssuePolSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("ProposalGrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalGrpContNo));
        }
        if (FCode.equals("PrtSeq"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtSeq));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("FieldName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FieldName));
        }
        if (FCode.equals("Location"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Location));
        }
        if (FCode.equals("IssueType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IssueType));
        }
        if (FCode.equals("OperatePos"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OperatePos));
        }
        if (FCode.equals("BackObjType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackObjType));
        }
        if (FCode.equals("BackObj"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackObj));
        }
        if (FCode.equals("IsueManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsueManageCom));
        }
        if (FCode.equals("IssueCont"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IssueCont));
        }
        if (FCode.equals("PrintCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCount));
        }
        if (FCode.equals("NeedPrint"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NeedPrint));
        }
        if (FCode.equals("ReplyMan"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyMan));
        }
        if (FCode.equals("ReplyResult"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReplyResult));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("QuestionObj"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuestionObj));
        }
        if (FCode.equals("ErrField"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrField));
        }
        if (FCode.equals("ErrFieldName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrFieldName));
        }
        if (FCode.equals("ErrContent"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrContent));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProposalGrpContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PrtSeq);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(FieldName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Location);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(IssueType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(OperatePos);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(BackObjType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(BackObj);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(IsueManageCom);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(IssueCont);
                break;
            case 12:
                strFieldValue = String.valueOf(PrintCount);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(NeedPrint);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ReplyMan);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ReplyResult);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(QuestionObj);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(ErrField);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ErrFieldName);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(ErrContent);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ProposalGrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalGrpContNo = FValue.trim();
            }
            else
            {
                ProposalGrpContNo = null;
            }
        }
        if (FCode.equals("PrtSeq"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtSeq = FValue.trim();
            }
            else
            {
                PrtSeq = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("FieldName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FieldName = FValue.trim();
            }
            else
            {
                FieldName = null;
            }
        }
        if (FCode.equals("Location"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Location = FValue.trim();
            }
            else
            {
                Location = null;
            }
        }
        if (FCode.equals("IssueType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IssueType = FValue.trim();
            }
            else
            {
                IssueType = null;
            }
        }
        if (FCode.equals("OperatePos"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OperatePos = FValue.trim();
            }
            else
            {
                OperatePos = null;
            }
        }
        if (FCode.equals("BackObjType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BackObjType = FValue.trim();
            }
            else
            {
                BackObjType = null;
            }
        }
        if (FCode.equals("BackObj"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BackObj = FValue.trim();
            }
            else
            {
                BackObj = null;
            }
        }
        if (FCode.equals("IsueManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IsueManageCom = FValue.trim();
            }
            else
            {
                IsueManageCom = null;
            }
        }
        if (FCode.equals("IssueCont"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IssueCont = FValue.trim();
            }
            else
            {
                IssueCont = null;
            }
        }
        if (FCode.equals("PrintCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PrintCount = i;
            }
        }
        if (FCode.equals("NeedPrint"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NeedPrint = FValue.trim();
            }
            else
            {
                NeedPrint = null;
            }
        }
        if (FCode.equals("ReplyMan"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyMan = FValue.trim();
            }
            else
            {
                ReplyMan = null;
            }
        }
        if (FCode.equals("ReplyResult"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReplyResult = FValue.trim();
            }
            else
            {
                ReplyResult = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("QuestionObj"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                QuestionObj = FValue.trim();
            }
            else
            {
                QuestionObj = null;
            }
        }
        if (FCode.equals("ErrField"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrField = FValue.trim();
            }
            else
            {
                ErrField = null;
            }
        }
        if (FCode.equals("ErrFieldName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrFieldName = FValue.trim();
            }
            else
            {
                ErrFieldName = null;
            }
        }
        if (FCode.equals("ErrContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrContent = FValue.trim();
            }
            else
            {
                ErrContent = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOBGrpIssuePolSchema other = (LOBGrpIssuePolSchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && ProposalGrpContNo.equals(other.getProposalGrpContNo())
                && PrtSeq.equals(other.getPrtSeq())
                && SerialNo.equals(other.getSerialNo())
                && FieldName.equals(other.getFieldName())
                && Location.equals(other.getLocation())
                && IssueType.equals(other.getIssueType())
                && OperatePos.equals(other.getOperatePos())
                && BackObjType.equals(other.getBackObjType())
                && BackObj.equals(other.getBackObj())
                && IsueManageCom.equals(other.getIsueManageCom())
                && IssueCont.equals(other.getIssueCont())
                && PrintCount == other.getPrintCount()
                && NeedPrint.equals(other.getNeedPrint())
                && ReplyMan.equals(other.getReplyMan())
                && ReplyResult.equals(other.getReplyResult())
                && State.equals(other.getState())
                && Operator.equals(other.getOperator())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && QuestionObj.equals(other.getQuestionObj())
                && ErrField.equals(other.getErrField())
                && ErrFieldName.equals(other.getErrFieldName())
                && ErrContent.equals(other.getErrContent());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ProposalGrpContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return 2;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 3;
        }
        if (strFieldName.equals("FieldName"))
        {
            return 4;
        }
        if (strFieldName.equals("Location"))
        {
            return 5;
        }
        if (strFieldName.equals("IssueType"))
        {
            return 6;
        }
        if (strFieldName.equals("OperatePos"))
        {
            return 7;
        }
        if (strFieldName.equals("BackObjType"))
        {
            return 8;
        }
        if (strFieldName.equals("BackObj"))
        {
            return 9;
        }
        if (strFieldName.equals("IsueManageCom"))
        {
            return 10;
        }
        if (strFieldName.equals("IssueCont"))
        {
            return 11;
        }
        if (strFieldName.equals("PrintCount"))
        {
            return 12;
        }
        if (strFieldName.equals("NeedPrint"))
        {
            return 13;
        }
        if (strFieldName.equals("ReplyMan"))
        {
            return 14;
        }
        if (strFieldName.equals("ReplyResult"))
        {
            return 15;
        }
        if (strFieldName.equals("State"))
        {
            return 16;
        }
        if (strFieldName.equals("Operator"))
        {
            return 17;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 19;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 21;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 22;
        }
        if (strFieldName.equals("QuestionObj"))
        {
            return 23;
        }
        if (strFieldName.equals("ErrField"))
        {
            return 24;
        }
        if (strFieldName.equals("ErrFieldName"))
        {
            return 25;
        }
        if (strFieldName.equals("ErrContent"))
        {
            return 26;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ProposalGrpContNo";
                break;
            case 2:
                strFieldName = "PrtSeq";
                break;
            case 3:
                strFieldName = "SerialNo";
                break;
            case 4:
                strFieldName = "FieldName";
                break;
            case 5:
                strFieldName = "Location";
                break;
            case 6:
                strFieldName = "IssueType";
                break;
            case 7:
                strFieldName = "OperatePos";
                break;
            case 8:
                strFieldName = "BackObjType";
                break;
            case 9:
                strFieldName = "BackObj";
                break;
            case 10:
                strFieldName = "IsueManageCom";
                break;
            case 11:
                strFieldName = "IssueCont";
                break;
            case 12:
                strFieldName = "PrintCount";
                break;
            case 13:
                strFieldName = "NeedPrint";
                break;
            case 14:
                strFieldName = "ReplyMan";
                break;
            case 15:
                strFieldName = "ReplyResult";
                break;
            case 16:
                strFieldName = "State";
                break;
            case 17:
                strFieldName = "Operator";
                break;
            case 18:
                strFieldName = "ManageCom";
                break;
            case 19:
                strFieldName = "MakeDate";
                break;
            case 20:
                strFieldName = "MakeTime";
                break;
            case 21:
                strFieldName = "ModifyDate";
                break;
            case 22:
                strFieldName = "ModifyTime";
                break;
            case 23:
                strFieldName = "QuestionObj";
                break;
            case 24:
                strFieldName = "ErrField";
                break;
            case 25:
                strFieldName = "ErrFieldName";
                break;
            case 26:
                strFieldName = "ErrContent";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalGrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtSeq"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FieldName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Location"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IssueType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OperatePos"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BackObjType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BackObj"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IsueManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IssueCont"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrintCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("NeedPrint"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReplyMan"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReplyResult"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QuestionObj"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrField"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrFieldName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrContent"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_INT;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
