/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCEstimateDB;

/*
 * <p>ClassName: LCEstimateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 预计保费回补金额
 * @CreateDate：2017-08-03
 */
public class LCEstimateSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerNo;
	/** 保单号码 */
	private String GrpContNo;
	/** 项目名称 */
	private String ProjectName;
	/** 项目编码 */
	private String ProjectNo;
	/** 预估赔付率 */
	private String EstimateRate;
	/** 预估赔付率（含结余返还） */
	private String EstimateBalanceRate;
	/** 保单预估结余返还金额 */
	private String ContEstimatePrem;
	/** 险种编码 */
	private String RiskCode;
	/** 险种首期保费占比 */
	private String RiskPremRate;
	/** 险种预估结余返还金额 */
	private String RiskEstimatePrem;
	/** 录入原因 */
	private String Reason;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 预计保费回补金额 */
	private String BackEstimatePrem;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCEstimateSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCEstimateSchema cloned = (LCEstimateSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerNo()
	{
		return SerNo;
	}
	public void setSerNo(String aSerNo)
	{
		SerNo = aSerNo;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getProjectName()
	{
		return ProjectName;
	}
	public void setProjectName(String aProjectName)
	{
		ProjectName = aProjectName;
	}
	public String getProjectNo()
	{
		return ProjectNo;
	}
	public void setProjectNo(String aProjectNo)
	{
		ProjectNo = aProjectNo;
	}
	public String getEstimateRate()
	{
		return EstimateRate;
	}
	public void setEstimateRate(String aEstimateRate)
	{
		EstimateRate = aEstimateRate;
	}
	public String getEstimateBalanceRate()
	{
		return EstimateBalanceRate;
	}
	public void setEstimateBalanceRate(String aEstimateBalanceRate)
	{
		EstimateBalanceRate = aEstimateBalanceRate;
	}
	public String getContEstimatePrem()
	{
		return ContEstimatePrem;
	}
	public void setContEstimatePrem(String aContEstimatePrem)
	{
		ContEstimatePrem = aContEstimatePrem;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskPremRate()
	{
		return RiskPremRate;
	}
	public void setRiskPremRate(String aRiskPremRate)
	{
		RiskPremRate = aRiskPremRate;
	}
	public String getRiskEstimatePrem()
	{
		return RiskEstimatePrem;
	}
	public void setRiskEstimatePrem(String aRiskEstimatePrem)
	{
		RiskEstimatePrem = aRiskEstimatePrem;
	}
	public String getReason()
	{
		return Reason;
	}
	public void setReason(String aReason)
	{
		Reason = aReason;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBackEstimatePrem()
	{
		return BackEstimatePrem;
	}
	public void setBackEstimatePrem(String aBackEstimatePrem)
	{
		BackEstimatePrem = aBackEstimatePrem;
	}

	/**
	* 使用另外一个 LCEstimateSchema 对象给 Schema 赋值
	* @param: aLCEstimateSchema LCEstimateSchema
	**/
	public void setSchema(LCEstimateSchema aLCEstimateSchema)
	{
		this.SerNo = aLCEstimateSchema.getSerNo();
		this.GrpContNo = aLCEstimateSchema.getGrpContNo();
		this.ProjectName = aLCEstimateSchema.getProjectName();
		this.ProjectNo = aLCEstimateSchema.getProjectNo();
		this.EstimateRate = aLCEstimateSchema.getEstimateRate();
		this.EstimateBalanceRate = aLCEstimateSchema.getEstimateBalanceRate();
		this.ContEstimatePrem = aLCEstimateSchema.getContEstimatePrem();
		this.RiskCode = aLCEstimateSchema.getRiskCode();
		this.RiskPremRate = aLCEstimateSchema.getRiskPremRate();
		this.RiskEstimatePrem = aLCEstimateSchema.getRiskEstimatePrem();
		this.Reason = aLCEstimateSchema.getReason();
		this.ManageCom = aLCEstimateSchema.getManageCom();
		this.Operator = aLCEstimateSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCEstimateSchema.getMakeDate());
		this.MakeTime = aLCEstimateSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCEstimateSchema.getModifyDate());
		this.ModifyTime = aLCEstimateSchema.getModifyTime();
		this.BackEstimatePrem = aLCEstimateSchema.getBackEstimatePrem();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerNo") == null )
				this.SerNo = null;
			else
				this.SerNo = rs.getString("SerNo").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("ProjectName") == null )
				this.ProjectName = null;
			else
				this.ProjectName = rs.getString("ProjectName").trim();

			if( rs.getString("ProjectNo") == null )
				this.ProjectNo = null;
			else
				this.ProjectNo = rs.getString("ProjectNo").trim();

			if( rs.getString("EstimateRate") == null )
				this.EstimateRate = null;
			else
				this.EstimateRate = rs.getString("EstimateRate").trim();

			if( rs.getString("EstimateBalanceRate") == null )
				this.EstimateBalanceRate = null;
			else
				this.EstimateBalanceRate = rs.getString("EstimateBalanceRate").trim();

			if( rs.getString("ContEstimatePrem") == null )
				this.ContEstimatePrem = null;
			else
				this.ContEstimatePrem = rs.getString("ContEstimatePrem").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskPremRate") == null )
				this.RiskPremRate = null;
			else
				this.RiskPremRate = rs.getString("RiskPremRate").trim();

			if( rs.getString("RiskEstimatePrem") == null )
				this.RiskEstimatePrem = null;
			else
				this.RiskEstimatePrem = rs.getString("RiskEstimatePrem").trim();

			if( rs.getString("Reason") == null )
				this.Reason = null;
			else
				this.Reason = rs.getString("Reason").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BackEstimatePrem") == null )
				this.BackEstimatePrem = null;
			else
				this.BackEstimatePrem = rs.getString("BackEstimatePrem").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCEstimate表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCEstimateSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCEstimateSchema getSchema()
	{
		LCEstimateSchema aLCEstimateSchema = new LCEstimateSchema();
		aLCEstimateSchema.setSchema(this);
		return aLCEstimateSchema;
	}

	public LCEstimateDB getDB()
	{
		LCEstimateDB aDBOper = new LCEstimateDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCEstimate描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EstimateRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EstimateBalanceRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContEstimatePrem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskPremRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskEstimatePrem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Reason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BackEstimatePrem));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCEstimate>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ProjectName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ProjectNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			EstimateRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			EstimateBalanceRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ContEstimatePrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RiskPremRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			RiskEstimatePrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BackEstimatePrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCEstimateSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerNo));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("ProjectName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectName));
		}
		if (FCode.equals("ProjectNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectNo));
		}
		if (FCode.equals("EstimateRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EstimateRate));
		}
		if (FCode.equals("EstimateBalanceRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EstimateBalanceRate));
		}
		if (FCode.equals("ContEstimatePrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContEstimatePrem));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskPremRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskPremRate));
		}
		if (FCode.equals("RiskEstimatePrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskEstimatePrem));
		}
		if (FCode.equals("Reason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reason));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BackEstimatePrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackEstimatePrem));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ProjectName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ProjectNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(EstimateRate);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(EstimateBalanceRate);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ContEstimatePrem);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RiskPremRate);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(RiskEstimatePrem);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Reason);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BackEstimatePrem);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerNo = FValue.trim();
			}
			else
				SerNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("ProjectName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectName = FValue.trim();
			}
			else
				ProjectName = null;
		}
		if (FCode.equalsIgnoreCase("ProjectNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectNo = FValue.trim();
			}
			else
				ProjectNo = null;
		}
		if (FCode.equalsIgnoreCase("EstimateRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EstimateRate = FValue.trim();
			}
			else
				EstimateRate = null;
		}
		if (FCode.equalsIgnoreCase("EstimateBalanceRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EstimateBalanceRate = FValue.trim();
			}
			else
				EstimateBalanceRate = null;
		}
		if (FCode.equalsIgnoreCase("ContEstimatePrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContEstimatePrem = FValue.trim();
			}
			else
				ContEstimatePrem = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskPremRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskPremRate = FValue.trim();
			}
			else
				RiskPremRate = null;
		}
		if (FCode.equalsIgnoreCase("RiskEstimatePrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskEstimatePrem = FValue.trim();
			}
			else
				RiskEstimatePrem = null;
		}
		if (FCode.equalsIgnoreCase("Reason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Reason = FValue.trim();
			}
			else
				Reason = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BackEstimatePrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackEstimatePrem = FValue.trim();
			}
			else
				BackEstimatePrem = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCEstimateSchema other = (LCEstimateSchema)otherObject;
		return
			(SerNo == null ? other.getSerNo() == null : SerNo.equals(other.getSerNo()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (ProjectName == null ? other.getProjectName() == null : ProjectName.equals(other.getProjectName()))
			&& (ProjectNo == null ? other.getProjectNo() == null : ProjectNo.equals(other.getProjectNo()))
			&& (EstimateRate == null ? other.getEstimateRate() == null : EstimateRate.equals(other.getEstimateRate()))
			&& (EstimateBalanceRate == null ? other.getEstimateBalanceRate() == null : EstimateBalanceRate.equals(other.getEstimateBalanceRate()))
			&& (ContEstimatePrem == null ? other.getContEstimatePrem() == null : ContEstimatePrem.equals(other.getContEstimatePrem()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskPremRate == null ? other.getRiskPremRate() == null : RiskPremRate.equals(other.getRiskPremRate()))
			&& (RiskEstimatePrem == null ? other.getRiskEstimatePrem() == null : RiskEstimatePrem.equals(other.getRiskEstimatePrem()))
			&& (Reason == null ? other.getReason() == null : Reason.equals(other.getReason()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BackEstimatePrem == null ? other.getBackEstimatePrem() == null : BackEstimatePrem.equals(other.getBackEstimatePrem()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return 0;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 1;
		}
		if( strFieldName.equals("ProjectName") ) {
			return 2;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return 3;
		}
		if( strFieldName.equals("EstimateRate") ) {
			return 4;
		}
		if( strFieldName.equals("EstimateBalanceRate") ) {
			return 5;
		}
		if( strFieldName.equals("ContEstimatePrem") ) {
			return 6;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 7;
		}
		if( strFieldName.equals("RiskPremRate") ) {
			return 8;
		}
		if( strFieldName.equals("RiskEstimatePrem") ) {
			return 9;
		}
		if( strFieldName.equals("Reason") ) {
			return 10;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 13;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 16;
		}
		if( strFieldName.equals("BackEstimatePrem") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerNo";
				break;
			case 1:
				strFieldName = "GrpContNo";
				break;
			case 2:
				strFieldName = "ProjectName";
				break;
			case 3:
				strFieldName = "ProjectNo";
				break;
			case 4:
				strFieldName = "EstimateRate";
				break;
			case 5:
				strFieldName = "EstimateBalanceRate";
				break;
			case 6:
				strFieldName = "ContEstimatePrem";
				break;
			case 7:
				strFieldName = "RiskCode";
				break;
			case 8:
				strFieldName = "RiskPremRate";
				break;
			case 9:
				strFieldName = "RiskEstimatePrem";
				break;
			case 10:
				strFieldName = "Reason";
				break;
			case 11:
				strFieldName = "ManageCom";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "MakeDate";
				break;
			case 14:
				strFieldName = "MakeTime";
				break;
			case 15:
				strFieldName = "ModifyDate";
				break;
			case 16:
				strFieldName = "ModifyTime";
				break;
			case 17:
				strFieldName = "BackEstimatePrem";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EstimateRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EstimateBalanceRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContEstimatePrem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskPremRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskEstimatePrem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Reason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackEstimatePrem") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
