/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDDLLocDB;

/*
 * <p>ClassName: LDDLLocSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: downloadreport
 * @CreateDate：2010-08-05
 */
public class LDDLLocSchema implements Schema, Cloneable
{
	// @Field
	/** 文件名 */
	private String FileName;
	/** 报表名称 */
	private String ReportName;
	/** 下载地址 */
	private String Location;
	/** 备注1 */
	private String Remark1;
	/** 备注2 */
	private String Remark2;
	/** 备注3 */
	private String Remark3;

	public static final int FIELDNUM = 6;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDDLLocSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "FileName";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDDLLocSchema cloned = (LDDLLocSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getFileName()
	{
		return FileName;
	}
	public void setFileName(String aFileName)
	{
		FileName = aFileName;
	}
	public String getReportName()
	{
		return ReportName;
	}
	public void setReportName(String aReportName)
	{
		ReportName = aReportName;
	}
	public String getLocation()
	{
		return Location;
	}
	public void setLocation(String aLocation)
	{
		Location = aLocation;
	}
	public String getRemark1()
	{
		return Remark1;
	}
	public void setRemark1(String aRemark1)
	{
		Remark1 = aRemark1;
	}
	public String getRemark2()
	{
		return Remark2;
	}
	public void setRemark2(String aRemark2)
	{
		Remark2 = aRemark2;
	}
	public String getRemark3()
	{
		return Remark3;
	}
	public void setRemark3(String aRemark3)
	{
		Remark3 = aRemark3;
	}

	/**
	* 使用另外一个 LDDLLocSchema 对象给 Schema 赋值
	* @param: aLDDLLocSchema LDDLLocSchema
	**/
	public void setSchema(LDDLLocSchema aLDDLLocSchema)
	{
		this.FileName = aLDDLLocSchema.getFileName();
		this.ReportName = aLDDLLocSchema.getReportName();
		this.Location = aLDDLLocSchema.getLocation();
		this.Remark1 = aLDDLLocSchema.getRemark1();
		this.Remark2 = aLDDLLocSchema.getRemark2();
		this.Remark3 = aLDDLLocSchema.getRemark3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("FileName") == null )
				this.FileName = null;
			else
				this.FileName = rs.getString("FileName").trim();

			if( rs.getString("ReportName") == null )
				this.ReportName = null;
			else
				this.ReportName = rs.getString("ReportName").trim();

			if( rs.getString("Location") == null )
				this.Location = null;
			else
				this.Location = rs.getString("Location").trim();

			if( rs.getString("Remark1") == null )
				this.Remark1 = null;
			else
				this.Remark1 = rs.getString("Remark1").trim();

			if( rs.getString("Remark2") == null )
				this.Remark2 = null;
			else
				this.Remark2 = rs.getString("Remark2").trim();

			if( rs.getString("Remark3") == null )
				this.Remark3 = null;
			else
				this.Remark3 = rs.getString("Remark3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDDLLoc表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDDLLocSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDDLLocSchema getSchema()
	{
		LDDLLocSchema aLDDLLocSchema = new LDDLLocSchema();
		aLDDLLocSchema.setSchema(this);
		return aLDDLLocSchema;
	}

	public LDDLLocDB getDB()
	{
		LDDLLocDB aDBOper = new LDDLLocDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDLLoc描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(FileName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReportName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Location)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDLLoc>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			FileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ReportName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Location = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Remark3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDDLLocSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("FileName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FileName));
		}
		if (FCode.equals("ReportName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReportName));
		}
		if (FCode.equals("Location"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Location));
		}
		if (FCode.equals("Remark1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
		}
		if (FCode.equals("Remark2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
		}
		if (FCode.equals("Remark3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(FileName);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ReportName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Location);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Remark1);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Remark2);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Remark3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("FileName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FileName = FValue.trim();
			}
			else
				FileName = null;
		}
		if (FCode.equalsIgnoreCase("ReportName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReportName = FValue.trim();
			}
			else
				ReportName = null;
		}
		if (FCode.equalsIgnoreCase("Location"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Location = FValue.trim();
			}
			else
				Location = null;
		}
		if (FCode.equalsIgnoreCase("Remark1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark1 = FValue.trim();
			}
			else
				Remark1 = null;
		}
		if (FCode.equalsIgnoreCase("Remark2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark2 = FValue.trim();
			}
			else
				Remark2 = null;
		}
		if (FCode.equalsIgnoreCase("Remark3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark3 = FValue.trim();
			}
			else
				Remark3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDDLLocSchema other = (LDDLLocSchema)otherObject;
		return
			(FileName == null ? other.getFileName() == null : FileName.equals(other.getFileName()))
			&& (ReportName == null ? other.getReportName() == null : ReportName.equals(other.getReportName()))
			&& (Location == null ? other.getLocation() == null : Location.equals(other.getLocation()))
			&& (Remark1 == null ? other.getRemark1() == null : Remark1.equals(other.getRemark1()))
			&& (Remark2 == null ? other.getRemark2() == null : Remark2.equals(other.getRemark2()))
			&& (Remark3 == null ? other.getRemark3() == null : Remark3.equals(other.getRemark3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("FileName") ) {
			return 0;
		}
		if( strFieldName.equals("ReportName") ) {
			return 1;
		}
		if( strFieldName.equals("Location") ) {
			return 2;
		}
		if( strFieldName.equals("Remark1") ) {
			return 3;
		}
		if( strFieldName.equals("Remark2") ) {
			return 4;
		}
		if( strFieldName.equals("Remark3") ) {
			return 5;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "FileName";
				break;
			case 1:
				strFieldName = "ReportName";
				break;
			case 2:
				strFieldName = "Location";
				break;
			case 3:
				strFieldName = "Remark1";
				break;
			case 4:
				strFieldName = "Remark2";
				break;
			case 5:
				strFieldName = "Remark3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("FileName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReportName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Location") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
