/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FMVoucherBnDB;

/*
 * <p>ClassName: FMVoucherBnSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FMVoucherBnSchema implements Schema, Cloneable
{
	// @Field
	/** 维护明细号 */
	private String MaintNo;
	/** 版本编号 */
	private String VersionNo;
	/** 凭证编号 */
	private String VoucherID;
	/** 凭证名称 */
	private String VoucherName;
	/** 业务交易编号 */
	private String BusinessID;
	/** 业务交易名称 */
	private String BusinessName;

	public static final int FIELDNUM = 6;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FMVoucherBnSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "MaintNo";
		pk[1] = "VersionNo";
		pk[2] = "VoucherID";
		pk[3] = "BusinessID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FMVoucherBnSchema cloned = (FMVoucherBnSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMaintNo()
	{
		return MaintNo;
	}
	public void setMaintNo(String aMaintNo)
	{
		MaintNo = aMaintNo;
	}
	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getVoucherID()
	{
		return VoucherID;
	}
	public void setVoucherID(String aVoucherID)
	{
		VoucherID = aVoucherID;
	}
	public String getVoucherName()
	{
		return VoucherName;
	}
	public void setVoucherName(String aVoucherName)
	{
		VoucherName = aVoucherName;
	}
	public String getBusinessID()
	{
		return BusinessID;
	}
	public void setBusinessID(String aBusinessID)
	{
		BusinessID = aBusinessID;
	}
	public String getBusinessName()
	{
		return BusinessName;
	}
	public void setBusinessName(String aBusinessName)
	{
		BusinessName = aBusinessName;
	}

	/**
	* 使用另外一个 FMVoucherBnSchema 对象给 Schema 赋值
	* @param: aFMVoucherBnSchema FMVoucherBnSchema
	**/
	public void setSchema(FMVoucherBnSchema aFMVoucherBnSchema)
	{
		this.MaintNo = aFMVoucherBnSchema.getMaintNo();
		this.VersionNo = aFMVoucherBnSchema.getVersionNo();
		this.VoucherID = aFMVoucherBnSchema.getVoucherID();
		this.VoucherName = aFMVoucherBnSchema.getVoucherName();
		this.BusinessID = aFMVoucherBnSchema.getBusinessID();
		this.BusinessName = aFMVoucherBnSchema.getBusinessName();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MaintNo") == null )
				this.MaintNo = null;
			else
				this.MaintNo = rs.getString("MaintNo").trim();

			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("VoucherID") == null )
				this.VoucherID = null;
			else
				this.VoucherID = rs.getString("VoucherID").trim();

			if( rs.getString("VoucherName") == null )
				this.VoucherName = null;
			else
				this.VoucherName = rs.getString("VoucherName").trim();

			if( rs.getString("BusinessID") == null )
				this.BusinessID = null;
			else
				this.BusinessID = rs.getString("BusinessID").trim();

			if( rs.getString("BusinessName") == null )
				this.BusinessName = null;
			else
				this.BusinessName = rs.getString("BusinessName").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FMVoucherBn表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FMVoucherBnSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FMVoucherBnSchema getSchema()
	{
		FMVoucherBnSchema aFMVoucherBnSchema = new FMVoucherBnSchema();
		aFMVoucherBnSchema.setSchema(this);
		return aFMVoucherBnSchema;
	}

	public FMVoucherBnDB getDB()
	{
		FMVoucherBnDB aDBOper = new FMVoucherBnDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFMVoucherBn描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(MaintNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessName));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFMVoucherBn>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MaintNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			VoucherID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			VoucherName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BusinessID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BusinessName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FMVoucherBnSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MaintNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaintNo));
		}
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("VoucherID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherID));
		}
		if (FCode.equals("VoucherName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherName));
		}
		if (FCode.equals("BusinessID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessID));
		}
		if (FCode.equals("BusinessName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessName));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MaintNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(VoucherID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(VoucherName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BusinessID);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BusinessName);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MaintNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaintNo = FValue.trim();
			}
			else
				MaintNo = null;
		}
		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("VoucherID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherID = FValue.trim();
			}
			else
				VoucherID = null;
		}
		if (FCode.equalsIgnoreCase("VoucherName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherName = FValue.trim();
			}
			else
				VoucherName = null;
		}
		if (FCode.equalsIgnoreCase("BusinessID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessID = FValue.trim();
			}
			else
				BusinessID = null;
		}
		if (FCode.equalsIgnoreCase("BusinessName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessName = FValue.trim();
			}
			else
				BusinessName = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FMVoucherBnSchema other = (FMVoucherBnSchema)otherObject;
		return
			(MaintNo == null ? other.getMaintNo() == null : MaintNo.equals(other.getMaintNo()))
			&& (VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (VoucherID == null ? other.getVoucherID() == null : VoucherID.equals(other.getVoucherID()))
			&& (VoucherName == null ? other.getVoucherName() == null : VoucherName.equals(other.getVoucherName()))
			&& (BusinessID == null ? other.getBusinessID() == null : BusinessID.equals(other.getBusinessID()))
			&& (BusinessName == null ? other.getBusinessName() == null : BusinessName.equals(other.getBusinessName()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MaintNo") ) {
			return 0;
		}
		if( strFieldName.equals("VersionNo") ) {
			return 1;
		}
		if( strFieldName.equals("VoucherID") ) {
			return 2;
		}
		if( strFieldName.equals("VoucherName") ) {
			return 3;
		}
		if( strFieldName.equals("BusinessID") ) {
			return 4;
		}
		if( strFieldName.equals("BusinessName") ) {
			return 5;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MaintNo";
				break;
			case 1:
				strFieldName = "VersionNo";
				break;
			case 2:
				strFieldName = "VoucherID";
				break;
			case 3:
				strFieldName = "VoucherName";
				break;
			case 4:
				strFieldName = "BusinessID";
				break;
			case 5:
				strFieldName = "BusinessName";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MaintNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessName") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
