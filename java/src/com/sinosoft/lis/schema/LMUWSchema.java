/*
 * <p>ClassName: LMUWSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMUWDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMUWSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 核保编码 */
    private String UWCode;
    /** 险种名称 */
    private String RiskName;
    /** 关联保单类型 */
    private String RelaPolType;
    /** 核保类型 */
    private String UWType;
    /** 核保顺序号 */
    private int UWOrder;
    /** 算法 */
    private String CalCode;
    /** 其他算法 */
    private String OthCalCode;
    /** 备注 */
    private String Remark;
    /** 核保级别 */
    private String UWGrade;
    /** 核保结果控制 */
    private String UWResult;
    /** 核保通过标记 */
    private String PassFlag;
    /** 险种版本 */
    private String RiskVer;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMUWSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "UWCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getUWCode()
    {
        if (UWCode != null && !UWCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWCode = StrTool.unicodeToGBK(UWCode);
        }
        return UWCode;
    }

    public void setUWCode(String aUWCode)
    {
        UWCode = aUWCode;
    }

    public String getRiskName()
    {
        if (RiskName != null && !RiskName.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskName = StrTool.unicodeToGBK(RiskName);
        }
        return RiskName;
    }

    public void setRiskName(String aRiskName)
    {
        RiskName = aRiskName;
    }

    public String getRelaPolType()
    {
        if (RelaPolType != null && !RelaPolType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RelaPolType = StrTool.unicodeToGBK(RelaPolType);
        }
        return RelaPolType;
    }

    public void setRelaPolType(String aRelaPolType)
    {
        RelaPolType = aRelaPolType;
    }

    public String getUWType()
    {
        if (UWType != null && !UWType.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWType = StrTool.unicodeToGBK(UWType);
        }
        return UWType;
    }

    public void setUWType(String aUWType)
    {
        UWType = aUWType;
    }

    public int getUWOrder()
    {
        return UWOrder;
    }

    public void setUWOrder(int aUWOrder)
    {
        UWOrder = aUWOrder;
    }

    public void setUWOrder(String aUWOrder)
    {
        if (aUWOrder != null && !aUWOrder.equals(""))
        {
            Integer tInteger = new Integer(aUWOrder);
            int i = tInteger.intValue();
            UWOrder = i;
        }
    }

    public String getCalCode()
    {
        if (CalCode != null && !CalCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public String getOthCalCode()
    {
        if (OthCalCode != null && !OthCalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OthCalCode = StrTool.unicodeToGBK(OthCalCode);
        }
        return OthCalCode;
    }

    public void setOthCalCode(String aOthCalCode)
    {
        OthCalCode = aOthCalCode;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getUWGrade()
    {
        if (UWGrade != null && !UWGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWGrade = StrTool.unicodeToGBK(UWGrade);
        }
        return UWGrade;
    }

    public void setUWGrade(String aUWGrade)
    {
        UWGrade = aUWGrade;
    }

    public String getUWResult()
    {
        if (UWResult != null && !UWResult.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWResult = StrTool.unicodeToGBK(UWResult);
        }
        return UWResult;
    }

    public void setUWResult(String aUWResult)
    {
        UWResult = aUWResult;
    }

    public String getPassFlag()
    {
        if (PassFlag != null && !PassFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            PassFlag = StrTool.unicodeToGBK(PassFlag);
        }
        return PassFlag;
    }

    public void setPassFlag(String aPassFlag)
    {
        PassFlag = aPassFlag;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    /**
     * 使用另外一个 LMUWSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMUWSchema aLMUWSchema)
    {
        this.RiskCode = aLMUWSchema.getRiskCode();
        this.UWCode = aLMUWSchema.getUWCode();
        this.RiskName = aLMUWSchema.getRiskName();
        this.RelaPolType = aLMUWSchema.getRelaPolType();
        this.UWType = aLMUWSchema.getUWType();
        this.UWOrder = aLMUWSchema.getUWOrder();
        this.CalCode = aLMUWSchema.getCalCode();
        this.OthCalCode = aLMUWSchema.getOthCalCode();
        this.Remark = aLMUWSchema.getRemark();
        this.UWGrade = aLMUWSchema.getUWGrade();
        this.UWResult = aLMUWSchema.getUWResult();
        this.PassFlag = aLMUWSchema.getPassFlag();
        this.RiskVer = aLMUWSchema.getRiskVer();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("UWCode") == null)
            {
                this.UWCode = null;
            }
            else
            {
                this.UWCode = rs.getString("UWCode").trim();
            }

            if (rs.getString("RiskName") == null)
            {
                this.RiskName = null;
            }
            else
            {
                this.RiskName = rs.getString("RiskName").trim();
            }

            if (rs.getString("RelaPolType") == null)
            {
                this.RelaPolType = null;
            }
            else
            {
                this.RelaPolType = rs.getString("RelaPolType").trim();
            }

            if (rs.getString("UWType") == null)
            {
                this.UWType = null;
            }
            else
            {
                this.UWType = rs.getString("UWType").trim();
            }

            this.UWOrder = rs.getInt("UWOrder");
            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("OthCalCode") == null)
            {
                this.OthCalCode = null;
            }
            else
            {
                this.OthCalCode = rs.getString("OthCalCode").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("UWGrade") == null)
            {
                this.UWGrade = null;
            }
            else
            {
                this.UWGrade = rs.getString("UWGrade").trim();
            }

            if (rs.getString("UWResult") == null)
            {
                this.UWResult = null;
            }
            else
            {
                this.UWResult = rs.getString("UWResult").trim();
            }

            if (rs.getString("PassFlag") == null)
            {
                this.PassFlag = null;
            }
            else
            {
                this.PassFlag = rs.getString("PassFlag").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMUWSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMUWSchema getSchema()
    {
        LMUWSchema aLMUWSchema = new LMUWSchema();
        aLMUWSchema.setSchema(this);
        return aLMUWSchema;
    }

    public LMUWDB getDB()
    {
        LMUWDB aDBOper = new LMUWDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMUW描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RelaPolType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(UWOrder) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OthCalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWResult)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PassFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMUW>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            UWCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RelaPolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            UWType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            UWOrder = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).intValue();
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            OthCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                    SysConst.PACKAGESPILTER);
            UWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            UWResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            PassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMUWSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("UWCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWCode));
        }
        if (FCode.equals("RiskName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskName));
        }
        if (FCode.equals("RelaPolType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RelaPolType));
        }
        if (FCode.equals("UWType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWType));
        }
        if (FCode.equals("UWOrder"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWOrder));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCode));
        }
        if (FCode.equals("OthCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OthCalCode));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("UWGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWGrade));
        }
        if (FCode.equals("UWResult"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWResult));
        }
        if (FCode.equals("PassFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PassFlag));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(UWCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RelaPolType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(UWType);
                break;
            case 5:
                strFieldValue = String.valueOf(UWOrder);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(OthCalCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(UWGrade);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(UWResult);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(PassFlag);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("UWCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWCode = FValue.trim();
            }
            else
            {
                UWCode = null;
            }
        }
        if (FCode.equals("RiskName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
            {
                RiskName = null;
            }
        }
        if (FCode.equals("RelaPolType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelaPolType = FValue.trim();
            }
            else
            {
                RelaPolType = null;
            }
        }
        if (FCode.equals("UWType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWType = FValue.trim();
            }
            else
            {
                UWType = null;
            }
        }
        if (FCode.equals("UWOrder"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                UWOrder = i;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("OthCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OthCalCode = FValue.trim();
            }
            else
            {
                OthCalCode = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("UWGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
            {
                UWGrade = null;
            }
        }
        if (FCode.equals("UWResult"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWResult = FValue.trim();
            }
            else
            {
                UWResult = null;
            }
        }
        if (FCode.equals("PassFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PassFlag = FValue.trim();
            }
            else
            {
                PassFlag = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMUWSchema other = (LMUWSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && UWCode.equals(other.getUWCode())
                && RiskName.equals(other.getRiskName())
                && RelaPolType.equals(other.getRelaPolType())
                && UWType.equals(other.getUWType())
                && UWOrder == other.getUWOrder()
                && CalCode.equals(other.getCalCode())
                && OthCalCode.equals(other.getOthCalCode())
                && Remark.equals(other.getRemark())
                && UWGrade.equals(other.getUWGrade())
                && UWResult.equals(other.getUWResult())
                && PassFlag.equals(other.getPassFlag())
                && RiskVer.equals(other.getRiskVer());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("UWCode"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskName"))
        {
            return 2;
        }
        if (strFieldName.equals("RelaPolType"))
        {
            return 3;
        }
        if (strFieldName.equals("UWType"))
        {
            return 4;
        }
        if (strFieldName.equals("UWOrder"))
        {
            return 5;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 6;
        }
        if (strFieldName.equals("OthCalCode"))
        {
            return 7;
        }
        if (strFieldName.equals("Remark"))
        {
            return 8;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return 9;
        }
        if (strFieldName.equals("UWResult"))
        {
            return 10;
        }
        if (strFieldName.equals("PassFlag"))
        {
            return 11;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "UWCode";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "RelaPolType";
                break;
            case 4:
                strFieldName = "UWType";
                break;
            case 5:
                strFieldName = "UWOrder";
                break;
            case 6:
                strFieldName = "CalCode";
                break;
            case 7:
                strFieldName = "OthCalCode";
                break;
            case 8:
                strFieldName = "Remark";
                break;
            case 9:
                strFieldName = "UWGrade";
                break;
            case 10:
                strFieldName = "UWResult";
                break;
            case 11:
                strFieldName = "PassFlag";
                break;
            case 12:
                strFieldName = "RiskVer";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaPolType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWOrder"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OthCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWResult"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PassFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
