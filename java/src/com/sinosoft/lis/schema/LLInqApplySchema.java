/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLInqApplyDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLInqApplySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 理赔调查相关表
 * @CreateDate：2005-06-02
 */
public class LLInqApplySchema implements Schema, Cloneable
{
    // @Field
    /** 对应号码 */
    private String OtherNo;
    /** 其他对应号码类型 */
    private String OtherNoType;
    /** 调查号 */
    private String SurveyNo;
    /** 调查项目号 */
    private String InqNo;
    /** 调查项目内容 */
    private String InqItem;
    /** 调查描述 */
    private String InqDesc;
    /** 调查地点 */
    private String SurveySite;
    /** 调查机构 */
    private String InqDept;
    /** 调查状态 */
    private String InqState;
    /** 本地标志 */
    private String LocFlag;
    /** 调查分配人 */
    private String InqPer;
    /** 调查分配日期 */
    private Date InqStartDate;
    /** 调查结束日期 */
    private Date InqEndDate;
    /** 调查结论 */
    private String InqConclusion;
    /** 结论确定人 */
    private String ConPer;
    /** 结论确定时间 */
    private Date ConDate;
    /** 上报标记 */
    private String SubFlag;
    /** 上报人 */
    private String Suber;
    /** 上报日期 */
    private Date SubDate;
    /** 转发机构 */
    private String DispatchCom;
    /** 转发人 */
    private String Dipatcher;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 27; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLInqApplySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SurveyNo";
        pk[1] = "InqNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLInqApplySchema cloned = (LLInqApplySchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getOtherNo()
    {
        return OtherNo;
    }

    public void setOtherNo(String aOtherNo)
    {
        OtherNo = aOtherNo;
    }

    public String getOtherNoType()
    {
        return OtherNoType;
    }

    public void setOtherNoType(String aOtherNoType)
    {
        OtherNoType = aOtherNoType;
    }

    public String getSurveyNo()
    {
        return SurveyNo;
    }

    public void setSurveyNo(String aSurveyNo)
    {
        SurveyNo = aSurveyNo;
    }

    public String getInqNo()
    {
        return InqNo;
    }

    public void setInqNo(String aInqNo)
    {
        InqNo = aInqNo;
    }

    public String getInqItem()
    {
        return InqItem;
    }

    public void setInqItem(String aInqItem)
    {
        InqItem = aInqItem;
    }

    public String getInqDesc()
    {
        return InqDesc;
    }

    public void setInqDesc(String aInqDesc)
    {
        InqDesc = aInqDesc;
    }

    public String getSurveySite()
    {
        return SurveySite;
    }

    public void setSurveySite(String aSurveySite)
    {
        SurveySite = aSurveySite;
    }

    public String getInqDept()
    {
        return InqDept;
    }

    public void setInqDept(String aInqDept)
    {
        InqDept = aInqDept;
    }

    public String getInqState()
    {
        return InqState;
    }

    public void setInqState(String aInqState)
    {
        InqState = aInqState;
    }

    public String getLocFlag()
    {
        return LocFlag;
    }

    public void setLocFlag(String aLocFlag)
    {
        LocFlag = aLocFlag;
    }

    public String getInqPer()
    {
        return InqPer;
    }

    public void setInqPer(String aInqPer)
    {
        InqPer = aInqPer;
    }

    public String getInqStartDate()
    {
        if (InqStartDate != null)
        {
            return fDate.getString(InqStartDate);
        }
        else
        {
            return null;
        }
    }

    public void setInqStartDate(Date aInqStartDate)
    {
        InqStartDate = aInqStartDate;
    }

    public void setInqStartDate(String aInqStartDate)
    {
        if (aInqStartDate != null && !aInqStartDate.equals(""))
        {
            InqStartDate = fDate.getDate(aInqStartDate);
        }
        else
        {
            InqStartDate = null;
        }
    }

    public String getInqEndDate()
    {
        if (InqEndDate != null)
        {
            return fDate.getString(InqEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setInqEndDate(Date aInqEndDate)
    {
        InqEndDate = aInqEndDate;
    }

    public void setInqEndDate(String aInqEndDate)
    {
        if (aInqEndDate != null && !aInqEndDate.equals(""))
        {
            InqEndDate = fDate.getDate(aInqEndDate);
        }
        else
        {
            InqEndDate = null;
        }
    }

    public String getInqConclusion()
    {
        return InqConclusion;
    }

    public void setInqConclusion(String aInqConclusion)
    {
        InqConclusion = aInqConclusion;
    }

    public String getConPer()
    {
        return ConPer;
    }

    public void setConPer(String aConPer)
    {
        ConPer = aConPer;
    }

    public String getConDate()
    {
        if (ConDate != null)
        {
            return fDate.getString(ConDate);
        }
        else
        {
            return null;
        }
    }

    public void setConDate(Date aConDate)
    {
        ConDate = aConDate;
    }

    public void setConDate(String aConDate)
    {
        if (aConDate != null && !aConDate.equals(""))
        {
            ConDate = fDate.getDate(aConDate);
        }
        else
        {
            ConDate = null;
        }
    }

    public String getSubFlag()
    {
        return SubFlag;
    }

    public void setSubFlag(String aSubFlag)
    {
        SubFlag = aSubFlag;
    }

    public String getSuber()
    {
        return Suber;
    }

    public void setSuber(String aSuber)
    {
        Suber = aSuber;
    }

    public String getSubDate()
    {
        if (SubDate != null)
        {
            return fDate.getString(SubDate);
        }
        else
        {
            return null;
        }
    }

    public void setSubDate(Date aSubDate)
    {
        SubDate = aSubDate;
    }

    public void setSubDate(String aSubDate)
    {
        if (aSubDate != null && !aSubDate.equals(""))
        {
            SubDate = fDate.getDate(aSubDate);
        }
        else
        {
            SubDate = null;
        }
    }

    public String getDispatchCom()
    {
        return DispatchCom;
    }

    public void setDispatchCom(String aDispatchCom)
    {
        DispatchCom = aDispatchCom;
    }

    public String getDipatcher()
    {
        return Dipatcher;
    }

    public void setDipatcher(String aDipatcher)
    {
        Dipatcher = aDipatcher;
    }

    public String getRemark()
    {
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLInqApplySchema 对象给 Schema 赋值
     * @param: aLLInqApplySchema LLInqApplySchema
     **/
    public void setSchema(LLInqApplySchema aLLInqApplySchema)
    {
        this.OtherNo = aLLInqApplySchema.getOtherNo();
        this.OtherNoType = aLLInqApplySchema.getOtherNoType();
        this.SurveyNo = aLLInqApplySchema.getSurveyNo();
        this.InqNo = aLLInqApplySchema.getInqNo();
        this.InqItem = aLLInqApplySchema.getInqItem();
        this.InqDesc = aLLInqApplySchema.getInqDesc();
        this.SurveySite = aLLInqApplySchema.getSurveySite();
        this.InqDept = aLLInqApplySchema.getInqDept();
        this.InqState = aLLInqApplySchema.getInqState();
        this.LocFlag = aLLInqApplySchema.getLocFlag();
        this.InqPer = aLLInqApplySchema.getInqPer();
        this.InqStartDate = fDate.getDate(aLLInqApplySchema.getInqStartDate());
        this.InqEndDate = fDate.getDate(aLLInqApplySchema.getInqEndDate());
        this.InqConclusion = aLLInqApplySchema.getInqConclusion();
        this.ConPer = aLLInqApplySchema.getConPer();
        this.ConDate = fDate.getDate(aLLInqApplySchema.getConDate());
        this.SubFlag = aLLInqApplySchema.getSubFlag();
        this.Suber = aLLInqApplySchema.getSuber();
        this.SubDate = fDate.getDate(aLLInqApplySchema.getSubDate());
        this.DispatchCom = aLLInqApplySchema.getDispatchCom();
        this.Dipatcher = aLLInqApplySchema.getDipatcher();
        this.Remark = aLLInqApplySchema.getRemark();
        this.Operator = aLLInqApplySchema.getOperator();
        this.MakeDate = fDate.getDate(aLLInqApplySchema.getMakeDate());
        this.MakeTime = aLLInqApplySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLInqApplySchema.getModifyDate());
        this.ModifyTime = aLLInqApplySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("OtherNo") == null)
            {
                this.OtherNo = null;
            }
            else
            {
                this.OtherNo = rs.getString("OtherNo").trim();
            }

            if (rs.getString("OtherNoType") == null)
            {
                this.OtherNoType = null;
            }
            else
            {
                this.OtherNoType = rs.getString("OtherNoType").trim();
            }

            if (rs.getString("SurveyNo") == null)
            {
                this.SurveyNo = null;
            }
            else
            {
                this.SurveyNo = rs.getString("SurveyNo").trim();
            }

            if (rs.getString("InqNo") == null)
            {
                this.InqNo = null;
            }
            else
            {
                this.InqNo = rs.getString("InqNo").trim();
            }

            if (rs.getString("InqItem") == null)
            {
                this.InqItem = null;
            }
            else
            {
                this.InqItem = rs.getString("InqItem").trim();
            }

            if (rs.getString("InqDesc") == null)
            {
                this.InqDesc = null;
            }
            else
            {
                this.InqDesc = rs.getString("InqDesc").trim();
            }

            if (rs.getString("SurveySite") == null)
            {
                this.SurveySite = null;
            }
            else
            {
                this.SurveySite = rs.getString("SurveySite").trim();
            }

            if (rs.getString("InqDept") == null)
            {
                this.InqDept = null;
            }
            else
            {
                this.InqDept = rs.getString("InqDept").trim();
            }

            if (rs.getString("InqState") == null)
            {
                this.InqState = null;
            }
            else
            {
                this.InqState = rs.getString("InqState").trim();
            }

            if (rs.getString("LocFlag") == null)
            {
                this.LocFlag = null;
            }
            else
            {
                this.LocFlag = rs.getString("LocFlag").trim();
            }

            if (rs.getString("InqPer") == null)
            {
                this.InqPer = null;
            }
            else
            {
                this.InqPer = rs.getString("InqPer").trim();
            }

            this.InqStartDate = rs.getDate("InqStartDate");
            this.InqEndDate = rs.getDate("InqEndDate");
            if (rs.getString("InqConclusion") == null)
            {
                this.InqConclusion = null;
            }
            else
            {
                this.InqConclusion = rs.getString("InqConclusion").trim();
            }

            if (rs.getString("ConPer") == null)
            {
                this.ConPer = null;
            }
            else
            {
                this.ConPer = rs.getString("ConPer").trim();
            }

            this.ConDate = rs.getDate("ConDate");
            if (rs.getString("SubFlag") == null)
            {
                this.SubFlag = null;
            }
            else
            {
                this.SubFlag = rs.getString("SubFlag").trim();
            }

            if (rs.getString("Suber") == null)
            {
                this.Suber = null;
            }
            else
            {
                this.Suber = rs.getString("Suber").trim();
            }

            this.SubDate = rs.getDate("SubDate");
            if (rs.getString("DispatchCom") == null)
            {
                this.DispatchCom = null;
            }
            else
            {
                this.DispatchCom = rs.getString("DispatchCom").trim();
            }

            if (rs.getString("Dipatcher") == null)
            {
                this.Dipatcher = null;
            }
            else
            {
                this.Dipatcher = rs.getString("Dipatcher").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LLInqApply表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLInqApplySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLInqApplySchema getSchema()
    {
        LLInqApplySchema aLLInqApplySchema = new LLInqApplySchema();
        aLLInqApplySchema.setSchema(this);
        return aLLInqApplySchema;
    }

    public LLInqApplyDB getDB()
    {
        LLInqApplyDB aDBOper = new LLInqApplyDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqApply描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(OtherNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherNoType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveyNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqItem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqDesc));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SurveySite));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqDept));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LocFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqPer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(InqStartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(InqEndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InqConclusion));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ConPer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ConDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SubFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Suber));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(SubDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DispatchCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Dipatcher));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqApply>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            SurveyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            InqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            InqItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            InqDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            SurveySite = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            InqDept = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            InqState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            LocFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            InqPer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                    SysConst.PACKAGESPILTER);
            InqStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            InqEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            InqConclusion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                           SysConst.PACKAGESPILTER);
            ConPer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                    SysConst.PACKAGESPILTER);
            ConDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            SubFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                     SysConst.PACKAGESPILTER);
            Suber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                   SysConst.PACKAGESPILTER);
            SubDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            DispatchCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                         SysConst.PACKAGESPILTER);
            Dipatcher = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                       SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 24, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 26, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLInqApplySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("OtherNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
        }
        if (FCode.equals("OtherNoType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
        }
        if (FCode.equals("SurveyNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyNo));
        }
        if (FCode.equals("InqNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqNo));
        }
        if (FCode.equals("InqItem"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqItem));
        }
        if (FCode.equals("InqDesc"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqDesc));
        }
        if (FCode.equals("SurveySite"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SurveySite));
        }
        if (FCode.equals("InqDept"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqDept));
        }
        if (FCode.equals("InqState"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqState));
        }
        if (FCode.equals("LocFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LocFlag));
        }
        if (FCode.equals("InqPer"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqPer));
        }
        if (FCode.equals("InqStartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getInqStartDate()));
        }
        if (FCode.equals("InqEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getInqEndDate()));
        }
        if (FCode.equals("InqConclusion"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InqConclusion));
        }
        if (FCode.equals("ConPer"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ConPer));
        }
        if (FCode.equals("ConDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getConDate()));
        }
        if (FCode.equals("SubFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubFlag));
        }
        if (FCode.equals("Suber"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Suber));
        }
        if (FCode.equals("SubDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getSubDate()));
        }
        if (FCode.equals("DispatchCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DispatchCom));
        }
        if (FCode.equals("Dipatcher"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dipatcher));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(OtherNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(OtherNoType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SurveyNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(InqNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(InqItem);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(InqDesc);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(SurveySite);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(InqDept);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(InqState);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(LocFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InqPer);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInqStartDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInqEndDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(InqConclusion);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ConPer);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(SubFlag);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(Suber);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSubDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(DispatchCom);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(Dipatcher);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("OtherNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNo = FValue.trim();
            }
            else
            {
                OtherNo = null;
            }
        }
        if (FCode.equals("OtherNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OtherNoType = FValue.trim();
            }
            else
            {
                OtherNoType = null;
            }
        }
        if (FCode.equals("SurveyNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyNo = FValue.trim();
            }
            else
            {
                SurveyNo = null;
            }
        }
        if (FCode.equals("InqNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqNo = FValue.trim();
            }
            else
            {
                InqNo = null;
            }
        }
        if (FCode.equals("InqItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqItem = FValue.trim();
            }
            else
            {
                InqItem = null;
            }
        }
        if (FCode.equals("InqDesc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqDesc = FValue.trim();
            }
            else
            {
                InqDesc = null;
            }
        }
        if (FCode.equals("SurveySite"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveySite = FValue.trim();
            }
            else
            {
                SurveySite = null;
            }
        }
        if (FCode.equals("InqDept"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqDept = FValue.trim();
            }
            else
            {
                InqDept = null;
            }
        }
        if (FCode.equals("InqState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqState = FValue.trim();
            }
            else
            {
                InqState = null;
            }
        }
        if (FCode.equals("LocFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LocFlag = FValue.trim();
            }
            else
            {
                LocFlag = null;
            }
        }
        if (FCode.equals("InqPer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqPer = FValue.trim();
            }
            else
            {
                InqPer = null;
            }
        }
        if (FCode.equals("InqStartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqStartDate = fDate.getDate(FValue);
            }
            else
            {
                InqStartDate = null;
            }
        }
        if (FCode.equals("InqEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqEndDate = fDate.getDate(FValue);
            }
            else
            {
                InqEndDate = null;
            }
        }
        if (FCode.equals("InqConclusion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InqConclusion = FValue.trim();
            }
            else
            {
                InqConclusion = null;
            }
        }
        if (FCode.equals("ConPer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConPer = FValue.trim();
            }
            else
            {
                ConPer = null;
            }
        }
        if (FCode.equals("ConDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConDate = fDate.getDate(FValue);
            }
            else
            {
                ConDate = null;
            }
        }
        if (FCode.equals("SubFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubFlag = FValue.trim();
            }
            else
            {
                SubFlag = null;
            }
        }
        if (FCode.equals("Suber"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Suber = FValue.trim();
            }
            else
            {
                Suber = null;
            }
        }
        if (FCode.equals("SubDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubDate = fDate.getDate(FValue);
            }
            else
            {
                SubDate = null;
            }
        }
        if (FCode.equals("DispatchCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DispatchCom = FValue.trim();
            }
            else
            {
                DispatchCom = null;
            }
        }
        if (FCode.equals("Dipatcher"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Dipatcher = FValue.trim();
            }
            else
            {
                Dipatcher = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLInqApplySchema other = (LLInqApplySchema) otherObject;
        return
                OtherNo.equals(other.getOtherNo())
                && OtherNoType.equals(other.getOtherNoType())
                && SurveyNo.equals(other.getSurveyNo())
                && InqNo.equals(other.getInqNo())
                && InqItem.equals(other.getInqItem())
                && InqDesc.equals(other.getInqDesc())
                && SurveySite.equals(other.getSurveySite())
                && InqDept.equals(other.getInqDept())
                && InqState.equals(other.getInqState())
                && LocFlag.equals(other.getLocFlag())
                && InqPer.equals(other.getInqPer())
                && fDate.getString(InqStartDate).equals(other.getInqStartDate())
                && fDate.getString(InqEndDate).equals(other.getInqEndDate())
                && InqConclusion.equals(other.getInqConclusion())
                && ConPer.equals(other.getConPer())
                && fDate.getString(ConDate).equals(other.getConDate())
                && SubFlag.equals(other.getSubFlag())
                && Suber.equals(other.getSuber())
                && fDate.getString(SubDate).equals(other.getSubDate())
                && DispatchCom.equals(other.getDispatchCom())
                && Dipatcher.equals(other.getDipatcher())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("OtherNo"))
        {
            return 0;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return 1;
        }
        if (strFieldName.equals("SurveyNo"))
        {
            return 2;
        }
        if (strFieldName.equals("InqNo"))
        {
            return 3;
        }
        if (strFieldName.equals("InqItem"))
        {
            return 4;
        }
        if (strFieldName.equals("InqDesc"))
        {
            return 5;
        }
        if (strFieldName.equals("SurveySite"))
        {
            return 6;
        }
        if (strFieldName.equals("InqDept"))
        {
            return 7;
        }
        if (strFieldName.equals("InqState"))
        {
            return 8;
        }
        if (strFieldName.equals("LocFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("InqPer"))
        {
            return 10;
        }
        if (strFieldName.equals("InqStartDate"))
        {
            return 11;
        }
        if (strFieldName.equals("InqEndDate"))
        {
            return 12;
        }
        if (strFieldName.equals("InqConclusion"))
        {
            return 13;
        }
        if (strFieldName.equals("ConPer"))
        {
            return 14;
        }
        if (strFieldName.equals("ConDate"))
        {
            return 15;
        }
        if (strFieldName.equals("SubFlag"))
        {
            return 16;
        }
        if (strFieldName.equals("Suber"))
        {
            return 17;
        }
        if (strFieldName.equals("SubDate"))
        {
            return 18;
        }
        if (strFieldName.equals("DispatchCom"))
        {
            return 19;
        }
        if (strFieldName.equals("Dipatcher"))
        {
            return 20;
        }
        if (strFieldName.equals("Remark"))
        {
            return 21;
        }
        if (strFieldName.equals("Operator"))
        {
            return 22;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 23;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 24;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 25;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 26;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "OtherNo";
                break;
            case 1:
                strFieldName = "OtherNoType";
                break;
            case 2:
                strFieldName = "SurveyNo";
                break;
            case 3:
                strFieldName = "InqNo";
                break;
            case 4:
                strFieldName = "InqItem";
                break;
            case 5:
                strFieldName = "InqDesc";
                break;
            case 6:
                strFieldName = "SurveySite";
                break;
            case 7:
                strFieldName = "InqDept";
                break;
            case 8:
                strFieldName = "InqState";
                break;
            case 9:
                strFieldName = "LocFlag";
                break;
            case 10:
                strFieldName = "InqPer";
                break;
            case 11:
                strFieldName = "InqStartDate";
                break;
            case 12:
                strFieldName = "InqEndDate";
                break;
            case 13:
                strFieldName = "InqConclusion";
                break;
            case 14:
                strFieldName = "ConPer";
                break;
            case 15:
                strFieldName = "ConDate";
                break;
            case 16:
                strFieldName = "SubFlag";
                break;
            case 17:
                strFieldName = "Suber";
                break;
            case 18:
                strFieldName = "SubDate";
                break;
            case 19:
                strFieldName = "DispatchCom";
                break;
            case 20:
                strFieldName = "Dipatcher";
                break;
            case 21:
                strFieldName = "Remark";
                break;
            case 22:
                strFieldName = "Operator";
                break;
            case 23:
                strFieldName = "MakeDate";
                break;
            case 24:
                strFieldName = "MakeTime";
                break;
            case 25:
                strFieldName = "ModifyDate";
                break;
            case 26:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("OtherNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqDesc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveySite"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqDept"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LocFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqPer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InqStartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InqEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InqConclusion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConPer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SubFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Suber"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("DispatchCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Dipatcher"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
