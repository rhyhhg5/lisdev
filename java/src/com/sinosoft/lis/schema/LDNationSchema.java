/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDNationDB;

/*
 * <p>ClassName: LDNationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-09-23
 */
public class LDNationSchema implements Schema, Cloneable {
    // @Field
    /** 国家编码 */
    private String NationNo;
    /** 国家中文名称 */
    private String ChineseName;
    /** 国家英文名称 */
    private String EnglishName;
    /** 国家中文简称 */
    private String ChineseSimpleName;
    /** 国家英文简称 */
    private String EnglishSimpleName;
    /** 所属大洲 */
    private String Continent;
    /** 所属地区 */
    private String Area;
    /** 国家类型 */
    private String NationType;
    /** 国家等级 */
    private String Grade;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 境外救援支持标记 */
    private String TravelInsuranceFlag;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDNationSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "NationNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDNationSchema cloned = (LDNationSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getNationNo() {
        return NationNo;
    }

    public void setNationNo(String aNationNo) {
        NationNo = aNationNo;
    }

    public String getChineseName() {
        return ChineseName;
    }

    public void setChineseName(String aChineseName) {
        ChineseName = aChineseName;
    }

    public String getEnglishName() {
        return EnglishName;
    }

    public void setEnglishName(String aEnglishName) {
        EnglishName = aEnglishName;
    }

    public String getChineseSimpleName() {
        return ChineseSimpleName;
    }

    public void setChineseSimpleName(String aChineseSimpleName) {
        ChineseSimpleName = aChineseSimpleName;
    }

    public String getEnglishSimpleName() {
        return EnglishSimpleName;
    }

    public void setEnglishSimpleName(String aEnglishSimpleName) {
        EnglishSimpleName = aEnglishSimpleName;
    }

    public String getContinent() {
        return Continent;
    }

    public void setContinent(String aContinent) {
        Continent = aContinent;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String aArea) {
        Area = aArea;
    }

    public String getNationType() {
        return NationType;
    }

    public void setNationType(String aNationType) {
        NationType = aNationType;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String aGrade) {
        Grade = aGrade;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getTravelInsuranceFlag() {
        return TravelInsuranceFlag;
    }

    public void setTravelInsuranceFlag(String aTravelInsuranceFlag) {
        TravelInsuranceFlag = aTravelInsuranceFlag;
    }

    /**
     * 使用另外一个 LDNationSchema 对象给 Schema 赋值
     * @param: aLDNationSchema LDNationSchema
     **/
    public void setSchema(LDNationSchema aLDNationSchema) {
        this.NationNo = aLDNationSchema.getNationNo();
        this.ChineseName = aLDNationSchema.getChineseName();
        this.EnglishName = aLDNationSchema.getEnglishName();
        this.ChineseSimpleName = aLDNationSchema.getChineseSimpleName();
        this.EnglishSimpleName = aLDNationSchema.getEnglishSimpleName();
        this.Continent = aLDNationSchema.getContinent();
        this.Area = aLDNationSchema.getArea();
        this.NationType = aLDNationSchema.getNationType();
        this.Grade = aLDNationSchema.getGrade();
        this.Operator = aLDNationSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDNationSchema.getMakeDate());
        this.MakeTime = aLDNationSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDNationSchema.getModifyDate());
        this.ModifyTime = aLDNationSchema.getModifyTime();
        this.TravelInsuranceFlag = aLDNationSchema.getTravelInsuranceFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("NationNo") == null) {
                this.NationNo = null;
            } else {
                this.NationNo = rs.getString("NationNo").trim();
            }

            if (rs.getString("ChineseName") == null) {
                this.ChineseName = null;
            } else {
                this.ChineseName = rs.getString("ChineseName").trim();
            }

            if (rs.getString("EnglishName") == null) {
                this.EnglishName = null;
            } else {
                this.EnglishName = rs.getString("EnglishName").trim();
            }

            if (rs.getString("ChineseSimpleName") == null) {
                this.ChineseSimpleName = null;
            } else {
                this.ChineseSimpleName = rs.getString("ChineseSimpleName").trim();
            }

            if (rs.getString("EnglishSimpleName") == null) {
                this.EnglishSimpleName = null;
            } else {
                this.EnglishSimpleName = rs.getString("EnglishSimpleName").trim();
            }

            if (rs.getString("Continent") == null) {
                this.Continent = null;
            } else {
                this.Continent = rs.getString("Continent").trim();
            }

            if (rs.getString("Area") == null) {
                this.Area = null;
            } else {
                this.Area = rs.getString("Area").trim();
            }

            if (rs.getString("NationType") == null) {
                this.NationType = null;
            } else {
                this.NationType = rs.getString("NationType").trim();
            }

            if (rs.getString("Grade") == null) {
                this.Grade = null;
            } else {
                this.Grade = rs.getString("Grade").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("TravelInsuranceFlag") == null) {
                this.TravelInsuranceFlag = null;
            } else {
                this.TravelInsuranceFlag = rs.getString("TravelInsuranceFlag").
                                           trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDNation表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDNationSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDNationSchema getSchema() {
        LDNationSchema aLDNationSchema = new LDNationSchema();
        aLDNationSchema.setSchema(this);
        return aLDNationSchema;
    }

    public LDNationDB getDB() {
        LDNationDB aDBOper = new LDNationDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDNation描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(NationNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChineseName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EnglishName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChineseSimpleName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EnglishSimpleName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Continent));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Area));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NationType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Grade));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TravelInsuranceFlag));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDNation>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            NationNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ChineseName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            EnglishName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            ChineseSimpleName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               4, SysConst.PACKAGESPILTER);
            EnglishSimpleName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               5, SysConst.PACKAGESPILTER);
            Continent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            Area = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            NationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            Grade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            TravelInsuranceFlag = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDNationSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("NationNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NationNo));
        }
        if (FCode.equals("ChineseName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChineseName));
        }
        if (FCode.equals("EnglishName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnglishName));
        }
        if (FCode.equals("ChineseSimpleName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChineseSimpleName));
        }
        if (FCode.equals("EnglishSimpleName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnglishSimpleName));
        }
        if (FCode.equals("Continent")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Continent));
        }
        if (FCode.equals("Area")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Area));
        }
        if (FCode.equals("NationType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NationType));
        }
        if (FCode.equals("Grade")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Grade));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("TravelInsuranceFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TravelInsuranceFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(NationNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ChineseName);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(EnglishName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ChineseSimpleName);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(EnglishSimpleName);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Continent);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Area);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(NationType);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(Grade);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(TravelInsuranceFlag);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("NationNo")) {
            if (FValue != null && !FValue.equals("")) {
                NationNo = FValue.trim();
            } else {
                NationNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ChineseName")) {
            if (FValue != null && !FValue.equals("")) {
                ChineseName = FValue.trim();
            } else {
                ChineseName = null;
            }
        }
        if (FCode.equalsIgnoreCase("EnglishName")) {
            if (FValue != null && !FValue.equals("")) {
                EnglishName = FValue.trim();
            } else {
                EnglishName = null;
            }
        }
        if (FCode.equalsIgnoreCase("ChineseSimpleName")) {
            if (FValue != null && !FValue.equals("")) {
                ChineseSimpleName = FValue.trim();
            } else {
                ChineseSimpleName = null;
            }
        }
        if (FCode.equalsIgnoreCase("EnglishSimpleName")) {
            if (FValue != null && !FValue.equals("")) {
                EnglishSimpleName = FValue.trim();
            } else {
                EnglishSimpleName = null;
            }
        }
        if (FCode.equalsIgnoreCase("Continent")) {
            if (FValue != null && !FValue.equals("")) {
                Continent = FValue.trim();
            } else {
                Continent = null;
            }
        }
        if (FCode.equalsIgnoreCase("Area")) {
            if (FValue != null && !FValue.equals("")) {
                Area = FValue.trim();
            } else {
                Area = null;
            }
        }
        if (FCode.equalsIgnoreCase("NationType")) {
            if (FValue != null && !FValue.equals("")) {
                NationType = FValue.trim();
            } else {
                NationType = null;
            }
        }
        if (FCode.equalsIgnoreCase("Grade")) {
            if (FValue != null && !FValue.equals("")) {
                Grade = FValue.trim();
            } else {
                Grade = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("TravelInsuranceFlag")) {
            if (FValue != null && !FValue.equals("")) {
                TravelInsuranceFlag = FValue.trim();
            } else {
                TravelInsuranceFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDNationSchema other = (LDNationSchema) otherObject;
        return
                NationNo.equals(other.getNationNo())
                && ChineseName.equals(other.getChineseName())
                && EnglishName.equals(other.getEnglishName())
                && ChineseSimpleName.equals(other.getChineseSimpleName())
                && EnglishSimpleName.equals(other.getEnglishSimpleName())
                && Continent.equals(other.getContinent())
                && Area.equals(other.getArea())
                && NationType.equals(other.getNationType())
                && Grade.equals(other.getGrade())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && TravelInsuranceFlag.equals(other.getTravelInsuranceFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("NationNo")) {
            return 0;
        }
        if (strFieldName.equals("ChineseName")) {
            return 1;
        }
        if (strFieldName.equals("EnglishName")) {
            return 2;
        }
        if (strFieldName.equals("ChineseSimpleName")) {
            return 3;
        }
        if (strFieldName.equals("EnglishSimpleName")) {
            return 4;
        }
        if (strFieldName.equals("Continent")) {
            return 5;
        }
        if (strFieldName.equals("Area")) {
            return 6;
        }
        if (strFieldName.equals("NationType")) {
            return 7;
        }
        if (strFieldName.equals("Grade")) {
            return 8;
        }
        if (strFieldName.equals("Operator")) {
            return 9;
        }
        if (strFieldName.equals("MakeDate")) {
            return 10;
        }
        if (strFieldName.equals("MakeTime")) {
            return 11;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 12;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 13;
        }
        if (strFieldName.equals("TravelInsuranceFlag")) {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "NationNo";
            break;
        case 1:
            strFieldName = "ChineseName";
            break;
        case 2:
            strFieldName = "EnglishName";
            break;
        case 3:
            strFieldName = "ChineseSimpleName";
            break;
        case 4:
            strFieldName = "EnglishSimpleName";
            break;
        case 5:
            strFieldName = "Continent";
            break;
        case 6:
            strFieldName = "Area";
            break;
        case 7:
            strFieldName = "NationType";
            break;
        case 8:
            strFieldName = "Grade";
            break;
        case 9:
            strFieldName = "Operator";
            break;
        case 10:
            strFieldName = "MakeDate";
            break;
        case 11:
            strFieldName = "MakeTime";
            break;
        case 12:
            strFieldName = "ModifyDate";
            break;
        case 13:
            strFieldName = "ModifyTime";
            break;
        case 14:
            strFieldName = "TravelInsuranceFlag";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("NationNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChineseName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EnglishName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChineseSimpleName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EnglishSimpleName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Continent")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Area")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NationType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Grade")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TravelInsuranceFlag")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
