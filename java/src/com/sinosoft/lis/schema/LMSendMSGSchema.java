/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMSendMSGDB;

/*
 * <p>ClassName: LMSendMSGSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2014-02-07
 */
public class LMSendMSGSchema implements Schema, Cloneable
{
	// @Field
	/** 短险任务号 */
	private String Taskcode;
	/** 发送业务类型 */
	private String SendFlag;
	/** 短信内容 */
	private String MSGContents;
	/** 管理机构 */
	private String ManageCom;
	/** 发送业务描述 */
	private String SendDescribe;
	/** 是否一次发送 */
	private String IsOnlySend;
	/** 有效状态 */
	private String StateFlag;
	/** 备用字段1 */
	private String StandBy1;
	/** 备用字段2 */
	private String StandBy2;
	/** 备用字段3 */
	private String StandBy3;
	/** 备用字段4 */
	private String StandBy4;
	/** 操作者 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LMSendMSGSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Taskcode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LMSendMSGSchema cloned = (LMSendMSGSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTaskcode()
	{
		return Taskcode;
	}
	public void setTaskcode(String aTaskcode)
	{
		Taskcode = aTaskcode;
	}
	public String getSendFlag()
	{
		return SendFlag;
	}
	public void setSendFlag(String aSendFlag)
	{
		SendFlag = aSendFlag;
	}
	public String getMSGContents()
	{
		return MSGContents;
	}
	public void setMSGContents(String aMSGContents)
	{
		MSGContents = aMSGContents;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getSendDescribe()
	{
		return SendDescribe;
	}
	public void setSendDescribe(String aSendDescribe)
	{
		SendDescribe = aSendDescribe;
	}
	public String getIsOnlySend()
	{
		return IsOnlySend;
	}
	public void setIsOnlySend(String aIsOnlySend)
	{
		IsOnlySend = aIsOnlySend;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public String getStandBy1()
	{
		return StandBy1;
	}
	public void setStandBy1(String aStandBy1)
	{
		StandBy1 = aStandBy1;
	}
	public String getStandBy2()
	{
		return StandBy2;
	}
	public void setStandBy2(String aStandBy2)
	{
		StandBy2 = aStandBy2;
	}
	public String getStandBy3()
	{
		return StandBy3;
	}
	public void setStandBy3(String aStandBy3)
	{
		StandBy3 = aStandBy3;
	}
	public String getStandBy4()
	{
		return StandBy4;
	}
	public void setStandBy4(String aStandBy4)
	{
		StandBy4 = aStandBy4;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LMSendMSGSchema 对象给 Schema 赋值
	* @param: aLMSendMSGSchema LMSendMSGSchema
	**/
	public void setSchema(LMSendMSGSchema aLMSendMSGSchema)
	{
		this.Taskcode = aLMSendMSGSchema.getTaskcode();
		this.SendFlag = aLMSendMSGSchema.getSendFlag();
		this.MSGContents = aLMSendMSGSchema.getMSGContents();
		this.ManageCom = aLMSendMSGSchema.getManageCom();
		this.SendDescribe = aLMSendMSGSchema.getSendDescribe();
		this.IsOnlySend = aLMSendMSGSchema.getIsOnlySend();
		this.StateFlag = aLMSendMSGSchema.getStateFlag();
		this.StandBy1 = aLMSendMSGSchema.getStandBy1();
		this.StandBy2 = aLMSendMSGSchema.getStandBy2();
		this.StandBy3 = aLMSendMSGSchema.getStandBy3();
		this.StandBy4 = aLMSendMSGSchema.getStandBy4();
		this.Operator = aLMSendMSGSchema.getOperator();
		this.MakeDate = fDate.getDate( aLMSendMSGSchema.getMakeDate());
		this.MakeTime = aLMSendMSGSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLMSendMSGSchema.getModifyDate());
		this.ModifyTime = aLMSendMSGSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Taskcode") == null )
				this.Taskcode = null;
			else
				this.Taskcode = rs.getString("Taskcode").trim();

			if( rs.getString("SendFlag") == null )
				this.SendFlag = null;
			else
				this.SendFlag = rs.getString("SendFlag").trim();

			if( rs.getString("MSGContents") == null )
				this.MSGContents = null;
			else
				this.MSGContents = rs.getString("MSGContents").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("SendDescribe") == null )
				this.SendDescribe = null;
			else
				this.SendDescribe = rs.getString("SendDescribe").trim();

			if( rs.getString("IsOnlySend") == null )
				this.IsOnlySend = null;
			else
				this.IsOnlySend = rs.getString("IsOnlySend").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			if( rs.getString("StandBy1") == null )
				this.StandBy1 = null;
			else
				this.StandBy1 = rs.getString("StandBy1").trim();

			if( rs.getString("StandBy2") == null )
				this.StandBy2 = null;
			else
				this.StandBy2 = rs.getString("StandBy2").trim();

			if( rs.getString("StandBy3") == null )
				this.StandBy3 = null;
			else
				this.StandBy3 = rs.getString("StandBy3").trim();

			if( rs.getString("StandBy4") == null )
				this.StandBy4 = null;
			else
				this.StandBy4 = rs.getString("StandBy4").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LMSendMSG表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMSendMSGSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LMSendMSGSchema getSchema()
	{
		LMSendMSGSchema aLMSendMSGSchema = new LMSendMSGSchema();
		aLMSendMSGSchema.setSchema(this);
		return aLMSendMSGSchema;
	}

	public LMSendMSGDB getDB()
	{
		LMSendMSGDB aDBOper = new LMSendMSGDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMSendMSG描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Taskcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MSGContents)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendDescribe)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IsOnlySend)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandBy1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandBy2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandBy3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandBy4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMSendMSG>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Taskcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SendFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			MSGContents = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SendDescribe = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IsOnlySend = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			StandBy1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			StandBy2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			StandBy3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			StandBy4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMSendMSGSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Taskcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Taskcode));
		}
		if (FCode.equals("SendFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendFlag));
		}
		if (FCode.equals("MSGContents"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MSGContents));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("SendDescribe"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendDescribe));
		}
		if (FCode.equals("IsOnlySend"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IsOnlySend));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("StandBy1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandBy1));
		}
		if (FCode.equals("StandBy2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandBy2));
		}
		if (FCode.equals("StandBy3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandBy3));
		}
		if (FCode.equals("StandBy4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandBy4));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Taskcode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SendFlag);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(MSGContents);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SendDescribe);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IsOnlySend);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(StandBy1);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(StandBy2);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(StandBy3);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(StandBy4);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Taskcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Taskcode = FValue.trim();
			}
			else
				Taskcode = null;
		}
		if (FCode.equalsIgnoreCase("SendFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendFlag = FValue.trim();
			}
			else
				SendFlag = null;
		}
		if (FCode.equalsIgnoreCase("MSGContents"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MSGContents = FValue.trim();
			}
			else
				MSGContents = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("SendDescribe"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendDescribe = FValue.trim();
			}
			else
				SendDescribe = null;
		}
		if (FCode.equalsIgnoreCase("IsOnlySend"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IsOnlySend = FValue.trim();
			}
			else
				IsOnlySend = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("StandBy1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandBy1 = FValue.trim();
			}
			else
				StandBy1 = null;
		}
		if (FCode.equalsIgnoreCase("StandBy2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandBy2 = FValue.trim();
			}
			else
				StandBy2 = null;
		}
		if (FCode.equalsIgnoreCase("StandBy3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandBy3 = FValue.trim();
			}
			else
				StandBy3 = null;
		}
		if (FCode.equalsIgnoreCase("StandBy4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandBy4 = FValue.trim();
			}
			else
				StandBy4 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LMSendMSGSchema other = (LMSendMSGSchema)otherObject;
		return
			(Taskcode == null ? other.getTaskcode() == null : Taskcode.equals(other.getTaskcode()))
			&& (SendFlag == null ? other.getSendFlag() == null : SendFlag.equals(other.getSendFlag()))
			&& (MSGContents == null ? other.getMSGContents() == null : MSGContents.equals(other.getMSGContents()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (SendDescribe == null ? other.getSendDescribe() == null : SendDescribe.equals(other.getSendDescribe()))
			&& (IsOnlySend == null ? other.getIsOnlySend() == null : IsOnlySend.equals(other.getIsOnlySend()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& (StandBy1 == null ? other.getStandBy1() == null : StandBy1.equals(other.getStandBy1()))
			&& (StandBy2 == null ? other.getStandBy2() == null : StandBy2.equals(other.getStandBy2()))
			&& (StandBy3 == null ? other.getStandBy3() == null : StandBy3.equals(other.getStandBy3()))
			&& (StandBy4 == null ? other.getStandBy4() == null : StandBy4.equals(other.getStandBy4()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Taskcode") ) {
			return 0;
		}
		if( strFieldName.equals("SendFlag") ) {
			return 1;
		}
		if( strFieldName.equals("MSGContents") ) {
			return 2;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 3;
		}
		if( strFieldName.equals("SendDescribe") ) {
			return 4;
		}
		if( strFieldName.equals("IsOnlySend") ) {
			return 5;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 6;
		}
		if( strFieldName.equals("StandBy1") ) {
			return 7;
		}
		if( strFieldName.equals("StandBy2") ) {
			return 8;
		}
		if( strFieldName.equals("StandBy3") ) {
			return 9;
		}
		if( strFieldName.equals("StandBy4") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Taskcode";
				break;
			case 1:
				strFieldName = "SendFlag";
				break;
			case 2:
				strFieldName = "MSGContents";
				break;
			case 3:
				strFieldName = "ManageCom";
				break;
			case 4:
				strFieldName = "SendDescribe";
				break;
			case 5:
				strFieldName = "IsOnlySend";
				break;
			case 6:
				strFieldName = "StateFlag";
				break;
			case 7:
				strFieldName = "StandBy1";
				break;
			case 8:
				strFieldName = "StandBy2";
				break;
			case 9:
				strFieldName = "StandBy3";
				break;
			case 10:
				strFieldName = "StandBy4";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Taskcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MSGContents") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendDescribe") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IsOnlySend") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandBy1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandBy2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandBy3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandBy4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
