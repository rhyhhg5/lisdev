/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LPPersonDB;

/*
 * <p>ClassName: LPPersonSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 港澳台
 * @CreateDate：2019-01-15
 */
public class LPPersonSchema implements Schema, Cloneable
{
	// @Field
	/** 批单号 */
	private String EdorNo;
	/** 批改类型 */
	private String EdorType;
	/** 客户号码 */
	private String CustomerNo;
	/** 客户姓名 */
	private String Name;
	/** 客户性别 */
	private String Sex;
	/** 客户出生日期 */
	private Date Birthday;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 密码 */
	private String Password;
	/** 国籍 */
	private String NativePlace;
	/** 民族 */
	private String Nationality;
	/** 户口所在地 */
	private String RgtAddress;
	/** 婚姻状况 */
	private String Marriage;
	/** 结婚日期 */
	private Date MarriageDate;
	/** 健康状况 */
	private String Health;
	/** 身高 */
	private double Stature;
	/** 体重 */
	private double Avoirdupois;
	/** 学历 */
	private String Degree;
	/** 信用等级 */
	private String CreditGrade;
	/** 其它证件类型 */
	private String OthIDType;
	/** 其它证件号码 */
	private String OthIDNo;
	/** Ic卡号 */
	private String ICNo;
	/** 单位编码 */
	private String GrpNo;
	/** 入司日期 */
	private Date JoinCompanyDate;
	/** 参加工作日期 */
	private Date StartWorkDate;
	/** 职位 */
	private String Position;
	/** 工资 */
	private double Salary;
	/** 职业类别 */
	private String OccupationType;
	/** 职业代码 */
	private String OccupationCode;
	/** 职业（工种） */
	private String WorkType;
	/** 兼职（工种） */
	private String PluralityType;
	/** 死亡日期 */
	private Date DeathDate;
	/** 是否吸烟标志 */
	private String SmokeFlag;
	/** 黑名单标记 */
	private String BlacklistFlag;
	/** 属性 */
	private String Proterty;
	/** 备注 */
	private String Remark;
	/** 状态 */
	private String State;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 单位名称 */
	private String GrpName;
	/** 英文名称 */
	private String EnglishName;
	/** 投保次数 */
	private int AppntNum;
	/** 社保分中心代码 */
	private String SocialCenterNo;
	/** 社保中心名称 */
	private String SocialCenterName;
	/** 客户类别 */
	private String CustomerType;
	/** 国家 */
	private String NativeCity;
	/** 平台返回客户号 */
	private String PlatformCustomerNo;
	/** 医保卡平台客户号 */
	private String YBKCustomerNo;
	/** 客户授权标识 */
	private String Authorization;
	/** 原通行证号码 */
	private String OriginalIDNo;

	public static final int FIELDNUM = 53;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LPPersonSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "EdorNo";
		pk[1] = "EdorType";
		pk[2] = "CustomerNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LPPersonSchema cloned = (LPPersonSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getEdorType()
	{
		return EdorType;
	}
	public void setEdorType(String aEdorType)
	{
		EdorType = aEdorType;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
		Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String aPassword)
	{
		Password = aPassword;
	}
	public String getNativePlace()
	{
		return NativePlace;
	}
	public void setNativePlace(String aNativePlace)
	{
		NativePlace = aNativePlace;
	}
	public String getNationality()
	{
		return Nationality;
	}
	public void setNationality(String aNationality)
	{
		Nationality = aNationality;
	}
	public String getRgtAddress()
	{
		return RgtAddress;
	}
	public void setRgtAddress(String aRgtAddress)
	{
		RgtAddress = aRgtAddress;
	}
	public String getMarriage()
	{
		return Marriage;
	}
	public void setMarriage(String aMarriage)
	{
		Marriage = aMarriage;
	}
	public String getMarriageDate()
	{
		if( MarriageDate != null )
			return fDate.getString(MarriageDate);
		else
			return null;
	}
	public void setMarriageDate(Date aMarriageDate)
	{
		MarriageDate = aMarriageDate;
	}
	public void setMarriageDate(String aMarriageDate)
	{
		if (aMarriageDate != null && !aMarriageDate.equals("") )
		{
			MarriageDate = fDate.getDate( aMarriageDate );
		}
		else
			MarriageDate = null;
	}

	public String getHealth()
	{
		return Health;
	}
	public void setHealth(String aHealth)
	{
		Health = aHealth;
	}
	public double getStature()
	{
		return Stature;
	}
	public void setStature(double aStature)
	{
		Stature = Arith.round(aStature,2);
	}
	public void setStature(String aStature)
	{
		if (aStature != null && !aStature.equals(""))
		{
			Double tDouble = new Double(aStature);
			double d = tDouble.doubleValue();
                Stature = Arith.round(d,2);
		}
	}

	public double getAvoirdupois()
	{
		return Avoirdupois;
	}
	public void setAvoirdupois(double aAvoirdupois)
	{
		Avoirdupois = Arith.round(aAvoirdupois,2);
	}
	public void setAvoirdupois(String aAvoirdupois)
	{
		if (aAvoirdupois != null && !aAvoirdupois.equals(""))
		{
			Double tDouble = new Double(aAvoirdupois);
			double d = tDouble.doubleValue();
                Avoirdupois = Arith.round(d,2);
		}
	}

	public String getDegree()
	{
		return Degree;
	}
	public void setDegree(String aDegree)
	{
		Degree = aDegree;
	}
	public String getCreditGrade()
	{
		return CreditGrade;
	}
	public void setCreditGrade(String aCreditGrade)
	{
		CreditGrade = aCreditGrade;
	}
	public String getOthIDType()
	{
		return OthIDType;
	}
	public void setOthIDType(String aOthIDType)
	{
		OthIDType = aOthIDType;
	}
	public String getOthIDNo()
	{
		return OthIDNo;
	}
	public void setOthIDNo(String aOthIDNo)
	{
		OthIDNo = aOthIDNo;
	}
	public String getICNo()
	{
		return ICNo;
	}
	public void setICNo(String aICNo)
	{
		ICNo = aICNo;
	}
	public String getGrpNo()
	{
		return GrpNo;
	}
	public void setGrpNo(String aGrpNo)
	{
		GrpNo = aGrpNo;
	}
	public String getJoinCompanyDate()
	{
		if( JoinCompanyDate != null )
			return fDate.getString(JoinCompanyDate);
		else
			return null;
	}
	public void setJoinCompanyDate(Date aJoinCompanyDate)
	{
		JoinCompanyDate = aJoinCompanyDate;
	}
	public void setJoinCompanyDate(String aJoinCompanyDate)
	{
		if (aJoinCompanyDate != null && !aJoinCompanyDate.equals("") )
		{
			JoinCompanyDate = fDate.getDate( aJoinCompanyDate );
		}
		else
			JoinCompanyDate = null;
	}

	public String getStartWorkDate()
	{
		if( StartWorkDate != null )
			return fDate.getString(StartWorkDate);
		else
			return null;
	}
	public void setStartWorkDate(Date aStartWorkDate)
	{
		StartWorkDate = aStartWorkDate;
	}
	public void setStartWorkDate(String aStartWorkDate)
	{
		if (aStartWorkDate != null && !aStartWorkDate.equals("") )
		{
			StartWorkDate = fDate.getDate( aStartWorkDate );
		}
		else
			StartWorkDate = null;
	}

	public String getPosition()
	{
		return Position;
	}
	public void setPosition(String aPosition)
	{
		Position = aPosition;
	}
	public double getSalary()
	{
		return Salary;
	}
	public void setSalary(double aSalary)
	{
		Salary = Arith.round(aSalary,2);
	}
	public void setSalary(String aSalary)
	{
		if (aSalary != null && !aSalary.equals(""))
		{
			Double tDouble = new Double(aSalary);
			double d = tDouble.doubleValue();
                Salary = Arith.round(d,2);
		}
	}

	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}
	public String getOccupationCode()
	{
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
		OccupationCode = aOccupationCode;
	}
	public String getWorkType()
	{
		return WorkType;
	}
	public void setWorkType(String aWorkType)
	{
		WorkType = aWorkType;
	}
	public String getPluralityType()
	{
		return PluralityType;
	}
	public void setPluralityType(String aPluralityType)
	{
		PluralityType = aPluralityType;
	}
	public String getDeathDate()
	{
		if( DeathDate != null )
			return fDate.getString(DeathDate);
		else
			return null;
	}
	public void setDeathDate(Date aDeathDate)
	{
		DeathDate = aDeathDate;
	}
	public void setDeathDate(String aDeathDate)
	{
		if (aDeathDate != null && !aDeathDate.equals("") )
		{
			DeathDate = fDate.getDate( aDeathDate );
		}
		else
			DeathDate = null;
	}

	public String getSmokeFlag()
	{
		return SmokeFlag;
	}
	public void setSmokeFlag(String aSmokeFlag)
	{
		SmokeFlag = aSmokeFlag;
	}
	public String getBlacklistFlag()
	{
		return BlacklistFlag;
	}
	public void setBlacklistFlag(String aBlacklistFlag)
	{
		BlacklistFlag = aBlacklistFlag;
	}
	public String getProterty()
	{
		return Proterty;
	}
	public void setProterty(String aProterty)
	{
		Proterty = aProterty;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getEnglishName()
	{
		return EnglishName;
	}
	public void setEnglishName(String aEnglishName)
	{
		EnglishName = aEnglishName;
	}
	public int getAppntNum()
	{
		return AppntNum;
	}
	public void setAppntNum(int aAppntNum)
	{
		AppntNum = aAppntNum;
	}
	public void setAppntNum(String aAppntNum)
	{
		if (aAppntNum != null && !aAppntNum.equals(""))
		{
			Integer tInteger = new Integer(aAppntNum);
			int i = tInteger.intValue();
			AppntNum = i;
		}
	}

	public String getSocialCenterNo()
	{
		return SocialCenterNo;
	}
	public void setSocialCenterNo(String aSocialCenterNo)
	{
		SocialCenterNo = aSocialCenterNo;
	}
	public String getSocialCenterName()
	{
		return SocialCenterName;
	}
	public void setSocialCenterName(String aSocialCenterName)
	{
		SocialCenterName = aSocialCenterName;
	}
	public String getCustomerType()
	{
		return CustomerType;
	}
	public void setCustomerType(String aCustomerType)
	{
		CustomerType = aCustomerType;
	}
	public String getNativeCity()
	{
		return NativeCity;
	}
	public void setNativeCity(String aNativeCity)
	{
		NativeCity = aNativeCity;
	}
	public String getPlatformCustomerNo()
	{
		return PlatformCustomerNo;
	}
	public void setPlatformCustomerNo(String aPlatformCustomerNo)
	{
		PlatformCustomerNo = aPlatformCustomerNo;
	}
	public String getYBKCustomerNo()
	{
		return YBKCustomerNo;
	}
	public void setYBKCustomerNo(String aYBKCustomerNo)
	{
		YBKCustomerNo = aYBKCustomerNo;
	}
	public String getAuthorization()
	{
		return Authorization;
	}
	public void setAuthorization(String aAuthorization)
	{
		Authorization = aAuthorization;
	}
	public String getOriginalIDNo()
	{
		return OriginalIDNo;
	}
	public void setOriginalIDNo(String aOriginalIDNo)
	{
		OriginalIDNo = aOriginalIDNo;
	}

	/**
	* 使用另外一个 LPPersonSchema 对象给 Schema 赋值
	* @param: aLPPersonSchema LPPersonSchema
	**/
	public void setSchema(LPPersonSchema aLPPersonSchema)
	{
		this.EdorNo = aLPPersonSchema.getEdorNo();
		this.EdorType = aLPPersonSchema.getEdorType();
		this.CustomerNo = aLPPersonSchema.getCustomerNo();
		this.Name = aLPPersonSchema.getName();
		this.Sex = aLPPersonSchema.getSex();
		this.Birthday = fDate.getDate( aLPPersonSchema.getBirthday());
		this.IDType = aLPPersonSchema.getIDType();
		this.IDNo = aLPPersonSchema.getIDNo();
		this.Password = aLPPersonSchema.getPassword();
		this.NativePlace = aLPPersonSchema.getNativePlace();
		this.Nationality = aLPPersonSchema.getNationality();
		this.RgtAddress = aLPPersonSchema.getRgtAddress();
		this.Marriage = aLPPersonSchema.getMarriage();
		this.MarriageDate = fDate.getDate( aLPPersonSchema.getMarriageDate());
		this.Health = aLPPersonSchema.getHealth();
		this.Stature = aLPPersonSchema.getStature();
		this.Avoirdupois = aLPPersonSchema.getAvoirdupois();
		this.Degree = aLPPersonSchema.getDegree();
		this.CreditGrade = aLPPersonSchema.getCreditGrade();
		this.OthIDType = aLPPersonSchema.getOthIDType();
		this.OthIDNo = aLPPersonSchema.getOthIDNo();
		this.ICNo = aLPPersonSchema.getICNo();
		this.GrpNo = aLPPersonSchema.getGrpNo();
		this.JoinCompanyDate = fDate.getDate( aLPPersonSchema.getJoinCompanyDate());
		this.StartWorkDate = fDate.getDate( aLPPersonSchema.getStartWorkDate());
		this.Position = aLPPersonSchema.getPosition();
		this.Salary = aLPPersonSchema.getSalary();
		this.OccupationType = aLPPersonSchema.getOccupationType();
		this.OccupationCode = aLPPersonSchema.getOccupationCode();
		this.WorkType = aLPPersonSchema.getWorkType();
		this.PluralityType = aLPPersonSchema.getPluralityType();
		this.DeathDate = fDate.getDate( aLPPersonSchema.getDeathDate());
		this.SmokeFlag = aLPPersonSchema.getSmokeFlag();
		this.BlacklistFlag = aLPPersonSchema.getBlacklistFlag();
		this.Proterty = aLPPersonSchema.getProterty();
		this.Remark = aLPPersonSchema.getRemark();
		this.State = aLPPersonSchema.getState();
		this.Operator = aLPPersonSchema.getOperator();
		this.MakeDate = fDate.getDate( aLPPersonSchema.getMakeDate());
		this.MakeTime = aLPPersonSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLPPersonSchema.getModifyDate());
		this.ModifyTime = aLPPersonSchema.getModifyTime();
		this.GrpName = aLPPersonSchema.getGrpName();
		this.EnglishName = aLPPersonSchema.getEnglishName();
		this.AppntNum = aLPPersonSchema.getAppntNum();
		this.SocialCenterNo = aLPPersonSchema.getSocialCenterNo();
		this.SocialCenterName = aLPPersonSchema.getSocialCenterName();
		this.CustomerType = aLPPersonSchema.getCustomerType();
		this.NativeCity = aLPPersonSchema.getNativeCity();
		this.PlatformCustomerNo = aLPPersonSchema.getPlatformCustomerNo();
		this.YBKCustomerNo = aLPPersonSchema.getYBKCustomerNo();
		this.Authorization = aLPPersonSchema.getAuthorization();
		this.OriginalIDNo = aLPPersonSchema.getOriginalIDNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("EdorType") == null )
				this.EdorType = null;
			else
				this.EdorType = rs.getString("EdorType").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			this.Birthday = rs.getDate("Birthday");
			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("Password") == null )
				this.Password = null;
			else
				this.Password = rs.getString("Password").trim();

			if( rs.getString("NativePlace") == null )
				this.NativePlace = null;
			else
				this.NativePlace = rs.getString("NativePlace").trim();

			if( rs.getString("Nationality") == null )
				this.Nationality = null;
			else
				this.Nationality = rs.getString("Nationality").trim();

			if( rs.getString("RgtAddress") == null )
				this.RgtAddress = null;
			else
				this.RgtAddress = rs.getString("RgtAddress").trim();

			if( rs.getString("Marriage") == null )
				this.Marriage = null;
			else
				this.Marriage = rs.getString("Marriage").trim();

			this.MarriageDate = rs.getDate("MarriageDate");
			if( rs.getString("Health") == null )
				this.Health = null;
			else
				this.Health = rs.getString("Health").trim();

			this.Stature = rs.getDouble("Stature");
			this.Avoirdupois = rs.getDouble("Avoirdupois");
			if( rs.getString("Degree") == null )
				this.Degree = null;
			else
				this.Degree = rs.getString("Degree").trim();

			if( rs.getString("CreditGrade") == null )
				this.CreditGrade = null;
			else
				this.CreditGrade = rs.getString("CreditGrade").trim();

			if( rs.getString("OthIDType") == null )
				this.OthIDType = null;
			else
				this.OthIDType = rs.getString("OthIDType").trim();

			if( rs.getString("OthIDNo") == null )
				this.OthIDNo = null;
			else
				this.OthIDNo = rs.getString("OthIDNo").trim();

			if( rs.getString("ICNo") == null )
				this.ICNo = null;
			else
				this.ICNo = rs.getString("ICNo").trim();

			if( rs.getString("GrpNo") == null )
				this.GrpNo = null;
			else
				this.GrpNo = rs.getString("GrpNo").trim();

			this.JoinCompanyDate = rs.getDate("JoinCompanyDate");
			this.StartWorkDate = rs.getDate("StartWorkDate");
			if( rs.getString("Position") == null )
				this.Position = null;
			else
				this.Position = rs.getString("Position").trim();

			this.Salary = rs.getDouble("Salary");
			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			if( rs.getString("OccupationCode") == null )
				this.OccupationCode = null;
			else
				this.OccupationCode = rs.getString("OccupationCode").trim();

			if( rs.getString("WorkType") == null )
				this.WorkType = null;
			else
				this.WorkType = rs.getString("WorkType").trim();

			if( rs.getString("PluralityType") == null )
				this.PluralityType = null;
			else
				this.PluralityType = rs.getString("PluralityType").trim();

			this.DeathDate = rs.getDate("DeathDate");
			if( rs.getString("SmokeFlag") == null )
				this.SmokeFlag = null;
			else
				this.SmokeFlag = rs.getString("SmokeFlag").trim();

			if( rs.getString("BlacklistFlag") == null )
				this.BlacklistFlag = null;
			else
				this.BlacklistFlag = rs.getString("BlacklistFlag").trim();

			if( rs.getString("Proterty") == null )
				this.Proterty = null;
			else
				this.Proterty = rs.getString("Proterty").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("EnglishName") == null )
				this.EnglishName = null;
			else
				this.EnglishName = rs.getString("EnglishName").trim();

			this.AppntNum = rs.getInt("AppntNum");
			if( rs.getString("SocialCenterNo") == null )
				this.SocialCenterNo = null;
			else
				this.SocialCenterNo = rs.getString("SocialCenterNo").trim();

			if( rs.getString("SocialCenterName") == null )
				this.SocialCenterName = null;
			else
				this.SocialCenterName = rs.getString("SocialCenterName").trim();

			if( rs.getString("CustomerType") == null )
				this.CustomerType = null;
			else
				this.CustomerType = rs.getString("CustomerType").trim();

			if( rs.getString("NativeCity") == null )
				this.NativeCity = null;
			else
				this.NativeCity = rs.getString("NativeCity").trim();

			if( rs.getString("PlatformCustomerNo") == null )
				this.PlatformCustomerNo = null;
			else
				this.PlatformCustomerNo = rs.getString("PlatformCustomerNo").trim();

			if( rs.getString("YBKCustomerNo") == null )
				this.YBKCustomerNo = null;
			else
				this.YBKCustomerNo = rs.getString("YBKCustomerNo").trim();

			if( rs.getString("Authorization") == null )
				this.Authorization = null;
			else
				this.Authorization = rs.getString("Authorization").trim();

			if( rs.getString("OriginalIDNo") == null )
				this.OriginalIDNo = null;
			else
				this.OriginalIDNo = rs.getString("OriginalIDNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LPPerson表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LPPersonSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LPPersonSchema getSchema()
	{
		LPPersonSchema aLPPersonSchema = new LPPersonSchema();
		aLPPersonSchema.setSchema(this);
		return aLPPersonSchema;
	}

	public LPPersonDB getDB()
	{
		LPPersonDB aDBOper = new LPPersonDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPPerson描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Nationality)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Marriage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MarriageDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Health)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Stature));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Avoirdupois));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Degree)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CreditGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ICNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( JoinCompanyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartWorkDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Salary));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WorkType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PluralityType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DeathDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SmokeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BlacklistFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Proterty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EnglishName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppntNum));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialCenterNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SocialCenterName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativeCity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PlatformCustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(YBKCustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Authorization)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OriginalIDNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPPerson>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			RgtAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Marriage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MarriageDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			Health = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Stature = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			Avoirdupois = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			Degree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			CreditGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			OthIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			OthIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ICNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			JoinCompanyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			StartWorkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Salary = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			WorkType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			PluralityType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			DeathDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,SysConst.PACKAGESPILTER));
			SmokeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			BlacklistFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			Proterty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			EnglishName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			AppntNum= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,45,SysConst.PACKAGESPILTER))).intValue();
			SocialCenterNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			SocialCenterName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			CustomerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			NativeCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			PlatformCustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			YBKCustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			Authorization = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			OriginalIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LPPersonSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("EdorType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("Password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
		}
		if (FCode.equals("NativePlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
		}
		if (FCode.equals("Nationality"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
		}
		if (FCode.equals("RgtAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
		}
		if (FCode.equals("Marriage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
		}
		if (FCode.equals("MarriageDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMarriageDate()));
		}
		if (FCode.equals("Health"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Health));
		}
		if (FCode.equals("Stature"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Stature));
		}
		if (FCode.equals("Avoirdupois"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Avoirdupois));
		}
		if (FCode.equals("Degree"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
		}
		if (FCode.equals("CreditGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CreditGrade));
		}
		if (FCode.equals("OthIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDType));
		}
		if (FCode.equals("OthIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDNo));
		}
		if (FCode.equals("ICNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ICNo));
		}
		if (FCode.equals("GrpNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
		}
		if (FCode.equals("JoinCompanyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getJoinCompanyDate()));
		}
		if (FCode.equals("StartWorkDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartWorkDate()));
		}
		if (FCode.equals("Position"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
		}
		if (FCode.equals("Salary"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("OccupationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
		}
		if (FCode.equals("WorkType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WorkType));
		}
		if (FCode.equals("PluralityType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PluralityType));
		}
		if (FCode.equals("DeathDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDeathDate()));
		}
		if (FCode.equals("SmokeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
		}
		if (FCode.equals("BlacklistFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistFlag));
		}
		if (FCode.equals("Proterty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Proterty));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("EnglishName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EnglishName));
		}
		if (FCode.equals("AppntNum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNum));
		}
		if (FCode.equals("SocialCenterNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialCenterNo));
		}
		if (FCode.equals("SocialCenterName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SocialCenterName));
		}
		if (FCode.equals("CustomerType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerType));
		}
		if (FCode.equals("NativeCity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativeCity));
		}
		if (FCode.equals("PlatformCustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PlatformCustomerNo));
		}
		if (FCode.equals("YBKCustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YBKCustomerNo));
		}
		if (FCode.equals("Authorization"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Authorization));
		}
		if (FCode.equals("OriginalIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OriginalIDNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(EdorType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Password);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(NativePlace);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Nationality);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(RgtAddress);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Marriage);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMarriageDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Health);
				break;
			case 15:
				strFieldValue = String.valueOf(Stature);
				break;
			case 16:
				strFieldValue = String.valueOf(Avoirdupois);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Degree);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(CreditGrade);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(OthIDType);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(OthIDNo);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ICNo);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(GrpNo);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getJoinCompanyDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartWorkDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Position);
				break;
			case 26:
				strFieldValue = String.valueOf(Salary);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(OccupationCode);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(WorkType);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(PluralityType);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDeathDate()));
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(SmokeFlag);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(BlacklistFlag);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(Proterty);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(EnglishName);
				break;
			case 44:
				strFieldValue = String.valueOf(AppntNum);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(SocialCenterNo);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(SocialCenterName);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(CustomerType);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(NativeCity);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(PlatformCustomerNo);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(YBKCustomerNo);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(Authorization);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(OriginalIDNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorType = FValue.trim();
			}
			else
				EdorType = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("Password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Password = FValue.trim();
			}
			else
				Password = null;
		}
		if (FCode.equalsIgnoreCase("NativePlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativePlace = FValue.trim();
			}
			else
				NativePlace = null;
		}
		if (FCode.equalsIgnoreCase("Nationality"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Nationality = FValue.trim();
			}
			else
				Nationality = null;
		}
		if (FCode.equalsIgnoreCase("RgtAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtAddress = FValue.trim();
			}
			else
				RgtAddress = null;
		}
		if (FCode.equalsIgnoreCase("Marriage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Marriage = FValue.trim();
			}
			else
				Marriage = null;
		}
		if (FCode.equalsIgnoreCase("MarriageDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MarriageDate = fDate.getDate( FValue );
			}
			else
				MarriageDate = null;
		}
		if (FCode.equalsIgnoreCase("Health"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Health = FValue.trim();
			}
			else
				Health = null;
		}
		if (FCode.equalsIgnoreCase("Stature"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Stature = d;
			}
		}
		if (FCode.equalsIgnoreCase("Avoirdupois"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Avoirdupois = d;
			}
		}
		if (FCode.equalsIgnoreCase("Degree"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Degree = FValue.trim();
			}
			else
				Degree = null;
		}
		if (FCode.equalsIgnoreCase("CreditGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CreditGrade = FValue.trim();
			}
			else
				CreditGrade = null;
		}
		if (FCode.equalsIgnoreCase("OthIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDType = FValue.trim();
			}
			else
				OthIDType = null;
		}
		if (FCode.equalsIgnoreCase("OthIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDNo = FValue.trim();
			}
			else
				OthIDNo = null;
		}
		if (FCode.equalsIgnoreCase("ICNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ICNo = FValue.trim();
			}
			else
				ICNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNo = FValue.trim();
			}
			else
				GrpNo = null;
		}
		if (FCode.equalsIgnoreCase("JoinCompanyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				JoinCompanyDate = fDate.getDate( FValue );
			}
			else
				JoinCompanyDate = null;
		}
		if (FCode.equalsIgnoreCase("StartWorkDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartWorkDate = fDate.getDate( FValue );
			}
			else
				StartWorkDate = null;
		}
		if (FCode.equalsIgnoreCase("Position"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Position = FValue.trim();
			}
			else
				Position = null;
		}
		if (FCode.equalsIgnoreCase("Salary"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Salary = d;
			}
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("OccupationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationCode = FValue.trim();
			}
			else
				OccupationCode = null;
		}
		if (FCode.equalsIgnoreCase("WorkType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WorkType = FValue.trim();
			}
			else
				WorkType = null;
		}
		if (FCode.equalsIgnoreCase("PluralityType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PluralityType = FValue.trim();
			}
			else
				PluralityType = null;
		}
		if (FCode.equalsIgnoreCase("DeathDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DeathDate = fDate.getDate( FValue );
			}
			else
				DeathDate = null;
		}
		if (FCode.equalsIgnoreCase("SmokeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SmokeFlag = FValue.trim();
			}
			else
				SmokeFlag = null;
		}
		if (FCode.equalsIgnoreCase("BlacklistFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BlacklistFlag = FValue.trim();
			}
			else
				BlacklistFlag = null;
		}
		if (FCode.equalsIgnoreCase("Proterty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Proterty = FValue.trim();
			}
			else
				Proterty = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("EnglishName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EnglishName = FValue.trim();
			}
			else
				EnglishName = null;
		}
		if (FCode.equalsIgnoreCase("AppntNum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AppntNum = i;
			}
		}
		if (FCode.equalsIgnoreCase("SocialCenterNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialCenterNo = FValue.trim();
			}
			else
				SocialCenterNo = null;
		}
		if (FCode.equalsIgnoreCase("SocialCenterName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SocialCenterName = FValue.trim();
			}
			else
				SocialCenterName = null;
		}
		if (FCode.equalsIgnoreCase("CustomerType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerType = FValue.trim();
			}
			else
				CustomerType = null;
		}
		if (FCode.equalsIgnoreCase("NativeCity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativeCity = FValue.trim();
			}
			else
				NativeCity = null;
		}
		if (FCode.equalsIgnoreCase("PlatformCustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PlatformCustomerNo = FValue.trim();
			}
			else
				PlatformCustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("YBKCustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				YBKCustomerNo = FValue.trim();
			}
			else
				YBKCustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("Authorization"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Authorization = FValue.trim();
			}
			else
				Authorization = null;
		}
		if (FCode.equalsIgnoreCase("OriginalIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OriginalIDNo = FValue.trim();
			}
			else
				OriginalIDNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LPPersonSchema other = (LPPersonSchema)otherObject;
		return
			(EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (EdorType == null ? other.getEdorType() == null : EdorType.equals(other.getEdorType()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (Birthday == null ? other.getBirthday() == null : fDate.getString(Birthday).equals(other.getBirthday()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (Password == null ? other.getPassword() == null : Password.equals(other.getPassword()))
			&& (NativePlace == null ? other.getNativePlace() == null : NativePlace.equals(other.getNativePlace()))
			&& (Nationality == null ? other.getNationality() == null : Nationality.equals(other.getNationality()))
			&& (RgtAddress == null ? other.getRgtAddress() == null : RgtAddress.equals(other.getRgtAddress()))
			&& (Marriage == null ? other.getMarriage() == null : Marriage.equals(other.getMarriage()))
			&& (MarriageDate == null ? other.getMarriageDate() == null : fDate.getString(MarriageDate).equals(other.getMarriageDate()))
			&& (Health == null ? other.getHealth() == null : Health.equals(other.getHealth()))
			&& Stature == other.getStature()
			&& Avoirdupois == other.getAvoirdupois()
			&& (Degree == null ? other.getDegree() == null : Degree.equals(other.getDegree()))
			&& (CreditGrade == null ? other.getCreditGrade() == null : CreditGrade.equals(other.getCreditGrade()))
			&& (OthIDType == null ? other.getOthIDType() == null : OthIDType.equals(other.getOthIDType()))
			&& (OthIDNo == null ? other.getOthIDNo() == null : OthIDNo.equals(other.getOthIDNo()))
			&& (ICNo == null ? other.getICNo() == null : ICNo.equals(other.getICNo()))
			&& (GrpNo == null ? other.getGrpNo() == null : GrpNo.equals(other.getGrpNo()))
			&& (JoinCompanyDate == null ? other.getJoinCompanyDate() == null : fDate.getString(JoinCompanyDate).equals(other.getJoinCompanyDate()))
			&& (StartWorkDate == null ? other.getStartWorkDate() == null : fDate.getString(StartWorkDate).equals(other.getStartWorkDate()))
			&& (Position == null ? other.getPosition() == null : Position.equals(other.getPosition()))
			&& Salary == other.getSalary()
			&& (OccupationType == null ? other.getOccupationType() == null : OccupationType.equals(other.getOccupationType()))
			&& (OccupationCode == null ? other.getOccupationCode() == null : OccupationCode.equals(other.getOccupationCode()))
			&& (WorkType == null ? other.getWorkType() == null : WorkType.equals(other.getWorkType()))
			&& (PluralityType == null ? other.getPluralityType() == null : PluralityType.equals(other.getPluralityType()))
			&& (DeathDate == null ? other.getDeathDate() == null : fDate.getString(DeathDate).equals(other.getDeathDate()))
			&& (SmokeFlag == null ? other.getSmokeFlag() == null : SmokeFlag.equals(other.getSmokeFlag()))
			&& (BlacklistFlag == null ? other.getBlacklistFlag() == null : BlacklistFlag.equals(other.getBlacklistFlag()))
			&& (Proterty == null ? other.getProterty() == null : Proterty.equals(other.getProterty()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (EnglishName == null ? other.getEnglishName() == null : EnglishName.equals(other.getEnglishName()))
			&& AppntNum == other.getAppntNum()
			&& (SocialCenterNo == null ? other.getSocialCenterNo() == null : SocialCenterNo.equals(other.getSocialCenterNo()))
			&& (SocialCenterName == null ? other.getSocialCenterName() == null : SocialCenterName.equals(other.getSocialCenterName()))
			&& (CustomerType == null ? other.getCustomerType() == null : CustomerType.equals(other.getCustomerType()))
			&& (NativeCity == null ? other.getNativeCity() == null : NativeCity.equals(other.getNativeCity()))
			&& (PlatformCustomerNo == null ? other.getPlatformCustomerNo() == null : PlatformCustomerNo.equals(other.getPlatformCustomerNo()))
			&& (YBKCustomerNo == null ? other.getYBKCustomerNo() == null : YBKCustomerNo.equals(other.getYBKCustomerNo()))
			&& (Authorization == null ? other.getAuthorization() == null : Authorization.equals(other.getAuthorization()))
			&& (OriginalIDNo == null ? other.getOriginalIDNo() == null : OriginalIDNo.equals(other.getOriginalIDNo()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("EdorType") ) {
			return 1;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 2;
		}
		if( strFieldName.equals("Name") ) {
			return 3;
		}
		if( strFieldName.equals("Sex") ) {
			return 4;
		}
		if( strFieldName.equals("Birthday") ) {
			return 5;
		}
		if( strFieldName.equals("IDType") ) {
			return 6;
		}
		if( strFieldName.equals("IDNo") ) {
			return 7;
		}
		if( strFieldName.equals("Password") ) {
			return 8;
		}
		if( strFieldName.equals("NativePlace") ) {
			return 9;
		}
		if( strFieldName.equals("Nationality") ) {
			return 10;
		}
		if( strFieldName.equals("RgtAddress") ) {
			return 11;
		}
		if( strFieldName.equals("Marriage") ) {
			return 12;
		}
		if( strFieldName.equals("MarriageDate") ) {
			return 13;
		}
		if( strFieldName.equals("Health") ) {
			return 14;
		}
		if( strFieldName.equals("Stature") ) {
			return 15;
		}
		if( strFieldName.equals("Avoirdupois") ) {
			return 16;
		}
		if( strFieldName.equals("Degree") ) {
			return 17;
		}
		if( strFieldName.equals("CreditGrade") ) {
			return 18;
		}
		if( strFieldName.equals("OthIDType") ) {
			return 19;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return 20;
		}
		if( strFieldName.equals("ICNo") ) {
			return 21;
		}
		if( strFieldName.equals("GrpNo") ) {
			return 22;
		}
		if( strFieldName.equals("JoinCompanyDate") ) {
			return 23;
		}
		if( strFieldName.equals("StartWorkDate") ) {
			return 24;
		}
		if( strFieldName.equals("Position") ) {
			return 25;
		}
		if( strFieldName.equals("Salary") ) {
			return 26;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 27;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return 28;
		}
		if( strFieldName.equals("WorkType") ) {
			return 29;
		}
		if( strFieldName.equals("PluralityType") ) {
			return 30;
		}
		if( strFieldName.equals("DeathDate") ) {
			return 31;
		}
		if( strFieldName.equals("SmokeFlag") ) {
			return 32;
		}
		if( strFieldName.equals("BlacklistFlag") ) {
			return 33;
		}
		if( strFieldName.equals("Proterty") ) {
			return 34;
		}
		if( strFieldName.equals("Remark") ) {
			return 35;
		}
		if( strFieldName.equals("State") ) {
			return 36;
		}
		if( strFieldName.equals("Operator") ) {
			return 37;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 38;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 39;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 40;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 41;
		}
		if( strFieldName.equals("GrpName") ) {
			return 42;
		}
		if( strFieldName.equals("EnglishName") ) {
			return 43;
		}
		if( strFieldName.equals("AppntNum") ) {
			return 44;
		}
		if( strFieldName.equals("SocialCenterNo") ) {
			return 45;
		}
		if( strFieldName.equals("SocialCenterName") ) {
			return 46;
		}
		if( strFieldName.equals("CustomerType") ) {
			return 47;
		}
		if( strFieldName.equals("NativeCity") ) {
			return 48;
		}
		if( strFieldName.equals("PlatformCustomerNo") ) {
			return 49;
		}
		if( strFieldName.equals("YBKCustomerNo") ) {
			return 50;
		}
		if( strFieldName.equals("Authorization") ) {
			return 51;
		}
		if( strFieldName.equals("OriginalIDNo") ) {
			return 52;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "EdorType";
				break;
			case 2:
				strFieldName = "CustomerNo";
				break;
			case 3:
				strFieldName = "Name";
				break;
			case 4:
				strFieldName = "Sex";
				break;
			case 5:
				strFieldName = "Birthday";
				break;
			case 6:
				strFieldName = "IDType";
				break;
			case 7:
				strFieldName = "IDNo";
				break;
			case 8:
				strFieldName = "Password";
				break;
			case 9:
				strFieldName = "NativePlace";
				break;
			case 10:
				strFieldName = "Nationality";
				break;
			case 11:
				strFieldName = "RgtAddress";
				break;
			case 12:
				strFieldName = "Marriage";
				break;
			case 13:
				strFieldName = "MarriageDate";
				break;
			case 14:
				strFieldName = "Health";
				break;
			case 15:
				strFieldName = "Stature";
				break;
			case 16:
				strFieldName = "Avoirdupois";
				break;
			case 17:
				strFieldName = "Degree";
				break;
			case 18:
				strFieldName = "CreditGrade";
				break;
			case 19:
				strFieldName = "OthIDType";
				break;
			case 20:
				strFieldName = "OthIDNo";
				break;
			case 21:
				strFieldName = "ICNo";
				break;
			case 22:
				strFieldName = "GrpNo";
				break;
			case 23:
				strFieldName = "JoinCompanyDate";
				break;
			case 24:
				strFieldName = "StartWorkDate";
				break;
			case 25:
				strFieldName = "Position";
				break;
			case 26:
				strFieldName = "Salary";
				break;
			case 27:
				strFieldName = "OccupationType";
				break;
			case 28:
				strFieldName = "OccupationCode";
				break;
			case 29:
				strFieldName = "WorkType";
				break;
			case 30:
				strFieldName = "PluralityType";
				break;
			case 31:
				strFieldName = "DeathDate";
				break;
			case 32:
				strFieldName = "SmokeFlag";
				break;
			case 33:
				strFieldName = "BlacklistFlag";
				break;
			case 34:
				strFieldName = "Proterty";
				break;
			case 35:
				strFieldName = "Remark";
				break;
			case 36:
				strFieldName = "State";
				break;
			case 37:
				strFieldName = "Operator";
				break;
			case 38:
				strFieldName = "MakeDate";
				break;
			case 39:
				strFieldName = "MakeTime";
				break;
			case 40:
				strFieldName = "ModifyDate";
				break;
			case 41:
				strFieldName = "ModifyTime";
				break;
			case 42:
				strFieldName = "GrpName";
				break;
			case 43:
				strFieldName = "EnglishName";
				break;
			case 44:
				strFieldName = "AppntNum";
				break;
			case 45:
				strFieldName = "SocialCenterNo";
				break;
			case 46:
				strFieldName = "SocialCenterName";
				break;
			case 47:
				strFieldName = "CustomerType";
				break;
			case 48:
				strFieldName = "NativeCity";
				break;
			case 49:
				strFieldName = "PlatformCustomerNo";
				break;
			case 50:
				strFieldName = "YBKCustomerNo";
				break;
			case 51:
				strFieldName = "Authorization";
				break;
			case 52:
				strFieldName = "OriginalIDNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NativePlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Nationality") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Marriage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarriageDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Health") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Stature") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Avoirdupois") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Degree") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CreditGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ICNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JoinCompanyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StartWorkDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Position") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Salary") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WorkType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PluralityType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeathDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SmokeFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BlacklistFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Proterty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnglishName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntNum") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SocialCenterNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SocialCenterName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NativeCity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PlatformCustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("YBKCustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Authorization") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OriginalIDNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_INT;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
