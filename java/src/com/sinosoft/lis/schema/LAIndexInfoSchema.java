/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAIndexInfoDB;

/*
 * <p>ClassName: LAIndexInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-20
 */
public class LAIndexInfoSchema implements Schema
{
	// @Field
	/** 指标计算编码 */
	private String IndexCalNo;
	/** 指标类型 */
	private String IndexType;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人展业机构代码 */
	private String AgentGroup;
	/** 管理机构 */
	private String ManageCom;
	/** 统计起期 */
	private Date StartDate;
	/** 统计止期 */
	private Date StartEnd;
	/** 个人累计fyc */
	private double IndFYCSum;
	/** 个人月平均fyc */
	private double MonAvgFYC;
	/** 本人及所增人员累计fyc */
	private double IndAddFYCSum;
	/** 直辖组累计fyc */
	private double DirTeamFYCSum;
	/** 直辖及育成组累计fyc */
	private double DRFYCSum;
	/** 直辖部累计fyc */
	private double DepFYCSum;
	/** 直辖部月均fyc */
	private double DirDepMonAvgFYC;
	/** Fyc 挂零月数 */
	private int FYCZeroMon;
	/** 个人继续率 */
	private double IndRate;
	/** 直辖组平均继续率 */
	private double TeamAvgRate;
	/** 直辖及育成组平均继续率 */
	private double DRTeamAvgRate;
	/** 直辖部平均继续率 */
	private double DepAvgRate;
	/** 直辖部及育成部平均继续率 */
	private double DRDepAvgRate;
	/** 区平均继续率 */
	private double AreaRate;
	/** 个人累计客户人数 */
	private int IndCustSum;
	/** 月平均客户人数 */
	private int MonAvgCust;
	/** 直接增员人数 */
	private int DirAddCount;
	/** 增员人数 */
	private int AddCount;
	/** 增员人员中包括的理财主任人数 */
	private int IncFinaMngCount;
	/** 所辖人数 */
	private int MngAgentCount;
	/** 所辖人员中理财主任人数 */
	private int MngFinaCount;
	/** 所辖人员中直接推荐人数 */
	private int DirRmdCount;
	/** 直辖人数 */
	private int DirMngCount;
	/** 直辖人员中理财主任人数 */
	private int DirMngFinaCount;
	/** 所辖营业组数 */
	private int TeamCount;
	/** 直接育成营业组数 */
	private int DRTeamCount;
	/** 合计育成组数 */
	private int RearTeamSum;
	/** 直接育成营销服务部个数 */
	private int DRearDepCount;
	/** 育成营销服务部数 */
	private int RearDepCount;
	/** 育成营销服务部中直接育成营销服务部数 */
	private int DInRCount;
	/** 所辖营销服务部理财服务主任人数 */
	private int DepFinaCount;
	/** 所辖支公司理财服务主任人数 */
	private int BranComFinaCount;
	/** 育成督导长人数 */
	private int RearAdmCount;
	/** 直接育成督导长人数 */
	private int DRearAdmCount;
	/** 月均有效人力人数 */
	private int MonAvgLabor;
	/** 直辖及育成组月均有效人力人数 */
	private int DRTeamMonLabor;
	/** 直辖部及育成部月均有效人力人数 */
	private int DRDepMonLabor;
	/** 创业扶持金 */
	private double InitPension;
	/** 创业金 */
	private double StartCareerRwd;
	/** 转正奖 */
	private double ToFormalRwd;
	/** 初年度佣金 */
	private double FirstPension;
	/** 续年度佣金 */
	private double ContiuePension;
	/** 个人年终奖 */
	private double PersonBonus;
	/** 伯乐奖金 */
	private double WiseBonus;
	/** 育才奖金 */
	private double RearBonus;
	/** 增员年终奖 */
	private double AddBonus;
	/** 见习业务经理特别津贴 */
	private double SpecialPension;
	/** 职务津贴 */
	private double PosSdy;
	/** 直辖组管理津贴 */
	private double TeamDirManaSdy;
	/** 组育成津贴 */
	private double RearedSdy;
	/** 小组年终奖金 */
	private double TeamBonus;
	/** 部直辖管理津贴 */
	private double DepDirManaSdy;
	/** 增部津贴 */
	private double AddDepSdy;
	/** 营销部年终奖金 */
	private double DepBonus;
	/** 督导长职务底薪 */
	private double BaseWage;
	/** 区域督导长职务底薪 */
	private double DisBaseWage;
	/** 督导长育成津贴 */
	private double RearAreaSdy;
	/** 补发初年度佣金 */
	private double AddFYC;
	/** 其他1 */
	private double T1;
	/** 其他2 */
	private double T2;
	/** 其他3 */
	private double T3;
	/** 其他4 */
	private double T4;
	/** 育成超出组指标 */
	private double T5;
	/** 营业税及附加 */
	private double T6;
	/** 销费费用 */
	private double T7;
	/** 个人所得税 */
	private double T8;
	/** 本次实发佣金指标 */
	private double T9;
	/** 银代基薪 */
	private double T10;
	/** 绩效 */
	private double T11;
	/** 直接育成督导长数 */
	private double T12;
	/** 直接育成部回算金额 */
	private double T13;
	/** 上次应发 */
	private double T14;
	/** 本期应发 */
	private double T15;
	/** 直辖部及育成部有效人力//衔接资金加扣款 */
	private double T16;
	/** 超额绩效//奖惩加扣款 */
	private double T17;
	/** T18 */
	private double T18;
	/** 直辖部合计fyc */
	private double T19;
	/** 最近12个月直接育成部数 */
	private double T20;
	/** 部门网点达成率 */
	private double T21;
	/** 部门业务达成率 */
	private double T22;
	/** 小组网点达成率 */
	private double T23;
	/** 小组业务达成率 */
	private double T24;
	/** 个人网点达成率 */
	private double T25;
	/** 个人业务达成率 */
	private double T26;
	/** 考评分数 */
	private double T27;
	/** 考勤扣款 */
	private double T28;
	/** 加扣款 */
	private double T29;
	/** 税前工资 */
	private double T30;
	/** 状态 */
	private String State;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 考核年月代码 */
	private String AssessMonth;
	/** 支助薪资//养老保险(银代法人） */
	private double T31;
	/** 同业招募专项基金//失业保险(银代法人） */
	private double T32;
	/** 同业创业奖//医疗保险(银代法人） */
	private double T33;
	/** 工伤保险（银代法人） */
	private double T34;
	/** 住房公积金（银代法人） */
	private double T35;
	/** 退休金 */
	private double T36;
	/** 差勤加扣款 */
	private double T37;
	/** 税前基薪加扣款 */
	private double T38;
	/** 税后基薪加扣款 */
	private double T39;
	/** 税前绩效加扣款 */
	private double T40;
	/** 员工制加扣款 */
	private double T41;
	/** 个人首期保费 */
	private double T42;
	/** 个人首年续期保费 */
	private double T43;
	/** 个人续年续期保费 */
	private double T44;
	/** 个人首年续佣 */
	private double T45;
	/** 个人续年续佣 */
	private double T46;
	/** 个人标保 */
	private double T47;
	/** 员工制社保扶持金 */
	private double T48;
	/** 员工制基本养老保险个人部分 */
	private double T49;
	/** 员工制基本养老保险公司部分 */
	private double T50;
	/** 员工制基本医疗保险个人部分 */
	private double T51;
	/** 员工制基本医疗保险公司部分 */
	private double T52;
	/** 员工制失业保险个人部分 */
	private double T53;
	/** 员工制失业保险公司部分 */
	private double T54;
	/** 员工制工伤保险个人部分 */
	private double T55;
	/** 员工制工伤保险公司部分 */
	private double T56;
	/** 员工制住房公积金个人部分 */
	private double T57;
	/** 本期余额 */
	private double T58;
	/** 展业机构外部编码 */
	private String BranchAttr;
	/** 代理人级别 */
	private String AgentGrade;
	/** 员工制住房公积金公司部分 */
	private double T59;
	/** 员工制补充商业医疗保险 */
	private double T60;
	/** 员工制中介费 */
	private double T61;
	/** 员工制四险一金扶持金 */
	private double T62;
	/** 员工制基薪 */
	private double T63;
	/** 组fyc合计 */
	private double T64;
	/** 部fyc合计 */
	private double T65;
	/** 督导区fyc合计 */
	private double T66;
	/** 业务拓展津贴 */
	private double T67;
	/** 其他67 */
	private double T68;
	/** 其他68 */
	private double T69;
	/** 其他69 */
	private double T70;
	/** 其他70 */
	private double T71;
	/** 展业机构序列编码 */
	private String BranchSeries;
	/** 展业类型 */
	private String BranchType;
	/** 展业渠道 */
	private String BranchType2;

	public static final int FIELDNUM = 148;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAIndexInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "IndexCalNo";
		pk[1] = "IndexType";
		pk[2] = "AgentCode";

		PK = pk;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getIndexCalNo()
	{
		if (SysConst.CHANGECHARSET && IndexCalNo != null && !IndexCalNo.equals(""))
		{
			IndexCalNo = StrTool.unicodeToGBK(IndexCalNo);
		}
		return IndexCalNo;
	}
	public void setIndexCalNo(String aIndexCalNo)
	{
		IndexCalNo = aIndexCalNo;
	}
	public String getIndexType()
	{
		if (SysConst.CHANGECHARSET && IndexType != null && !IndexType.equals(""))
		{
			IndexType = StrTool.unicodeToGBK(IndexType);
		}
		return IndexType;
	}
	public void setIndexType(String aIndexType)
	{
		IndexType = aIndexType;
	}
	public String getAgentCode()
	{
		if (SysConst.CHANGECHARSET && AgentCode != null && !AgentCode.equals(""))
		{
			AgentCode = StrTool.unicodeToGBK(AgentCode);
		}
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		if (SysConst.CHANGECHARSET && AgentGroup != null && !AgentGroup.equals(""))
		{
			AgentGroup = StrTool.unicodeToGBK(AgentGroup);
		}
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getManageCom()
	{
		if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
		{
			ManageCom = StrTool.unicodeToGBK(ManageCom);
		}
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
		StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getStartEnd()
	{
		if( StartEnd != null )
			return fDate.getString(StartEnd);
		else
			return null;
	}
	public void setStartEnd(Date aStartEnd)
	{
		StartEnd = aStartEnd;
	}
	public void setStartEnd(String aStartEnd)
	{
		if (aStartEnd != null && !aStartEnd.equals("") )
		{
			StartEnd = fDate.getDate( aStartEnd );
		}
		else
			StartEnd = null;
	}

	public double getIndFYCSum()
	{
		return IndFYCSum;
	}
	public void setIndFYCSum(double aIndFYCSum)
	{
		IndFYCSum = aIndFYCSum;
	}
	public void setIndFYCSum(String aIndFYCSum)
	{
		if (aIndFYCSum != null && !aIndFYCSum.equals(""))
		{
			Double tDouble = new Double(aIndFYCSum);
			double d = tDouble.doubleValue();
			IndFYCSum = d;
		}
	}

	public double getMonAvgFYC()
	{
		return MonAvgFYC;
	}
	public void setMonAvgFYC(double aMonAvgFYC)
	{
		MonAvgFYC = aMonAvgFYC;
	}
	public void setMonAvgFYC(String aMonAvgFYC)
	{
		if (aMonAvgFYC != null && !aMonAvgFYC.equals(""))
		{
			Double tDouble = new Double(aMonAvgFYC);
			double d = tDouble.doubleValue();
			MonAvgFYC = d;
		}
	}

	public double getIndAddFYCSum()
	{
		return IndAddFYCSum;
	}
	public void setIndAddFYCSum(double aIndAddFYCSum)
	{
		IndAddFYCSum = aIndAddFYCSum;
	}
	public void setIndAddFYCSum(String aIndAddFYCSum)
	{
		if (aIndAddFYCSum != null && !aIndAddFYCSum.equals(""))
		{
			Double tDouble = new Double(aIndAddFYCSum);
			double d = tDouble.doubleValue();
			IndAddFYCSum = d;
		}
	}

	public double getDirTeamFYCSum()
	{
		return DirTeamFYCSum;
	}
	public void setDirTeamFYCSum(double aDirTeamFYCSum)
	{
		DirTeamFYCSum = aDirTeamFYCSum;
	}
	public void setDirTeamFYCSum(String aDirTeamFYCSum)
	{
		if (aDirTeamFYCSum != null && !aDirTeamFYCSum.equals(""))
		{
			Double tDouble = new Double(aDirTeamFYCSum);
			double d = tDouble.doubleValue();
			DirTeamFYCSum = d;
		}
	}

	public double getDRFYCSum()
	{
		return DRFYCSum;
	}
	public void setDRFYCSum(double aDRFYCSum)
	{
		DRFYCSum = aDRFYCSum;
	}
	public void setDRFYCSum(String aDRFYCSum)
	{
		if (aDRFYCSum != null && !aDRFYCSum.equals(""))
		{
			Double tDouble = new Double(aDRFYCSum);
			double d = tDouble.doubleValue();
			DRFYCSum = d;
		}
	}

	public double getDepFYCSum()
	{
		return DepFYCSum;
	}
	public void setDepFYCSum(double aDepFYCSum)
	{
		DepFYCSum = aDepFYCSum;
	}
	public void setDepFYCSum(String aDepFYCSum)
	{
		if (aDepFYCSum != null && !aDepFYCSum.equals(""))
		{
			Double tDouble = new Double(aDepFYCSum);
			double d = tDouble.doubleValue();
			DepFYCSum = d;
		}
	}

	public double getDirDepMonAvgFYC()
	{
		return DirDepMonAvgFYC;
	}
	public void setDirDepMonAvgFYC(double aDirDepMonAvgFYC)
	{
		DirDepMonAvgFYC = aDirDepMonAvgFYC;
	}
	public void setDirDepMonAvgFYC(String aDirDepMonAvgFYC)
	{
		if (aDirDepMonAvgFYC != null && !aDirDepMonAvgFYC.equals(""))
		{
			Double tDouble = new Double(aDirDepMonAvgFYC);
			double d = tDouble.doubleValue();
			DirDepMonAvgFYC = d;
		}
	}

	public int getFYCZeroMon()
	{
		return FYCZeroMon;
	}
	public void setFYCZeroMon(int aFYCZeroMon)
	{
		FYCZeroMon = aFYCZeroMon;
	}
	public void setFYCZeroMon(String aFYCZeroMon)
	{
		if (aFYCZeroMon != null && !aFYCZeroMon.equals(""))
		{
			Integer tInteger = new Integer(aFYCZeroMon);
			int i = tInteger.intValue();
			FYCZeroMon = i;
		}
	}

	public double getIndRate()
	{
		return IndRate;
	}
	public void setIndRate(double aIndRate)
	{
		IndRate = aIndRate;
	}
	public void setIndRate(String aIndRate)
	{
		if (aIndRate != null && !aIndRate.equals(""))
		{
			Double tDouble = new Double(aIndRate);
			double d = tDouble.doubleValue();
			IndRate = d;
		}
	}

	public double getTeamAvgRate()
	{
		return TeamAvgRate;
	}
	public void setTeamAvgRate(double aTeamAvgRate)
	{
		TeamAvgRate = aTeamAvgRate;
	}
	public void setTeamAvgRate(String aTeamAvgRate)
	{
		if (aTeamAvgRate != null && !aTeamAvgRate.equals(""))
		{
			Double tDouble = new Double(aTeamAvgRate);
			double d = tDouble.doubleValue();
			TeamAvgRate = d;
		}
	}

	public double getDRTeamAvgRate()
	{
		return DRTeamAvgRate;
	}
	public void setDRTeamAvgRate(double aDRTeamAvgRate)
	{
		DRTeamAvgRate = aDRTeamAvgRate;
	}
	public void setDRTeamAvgRate(String aDRTeamAvgRate)
	{
		if (aDRTeamAvgRate != null && !aDRTeamAvgRate.equals(""))
		{
			Double tDouble = new Double(aDRTeamAvgRate);
			double d = tDouble.doubleValue();
			DRTeamAvgRate = d;
		}
	}

	public double getDepAvgRate()
	{
		return DepAvgRate;
	}
	public void setDepAvgRate(double aDepAvgRate)
	{
		DepAvgRate = aDepAvgRate;
	}
	public void setDepAvgRate(String aDepAvgRate)
	{
		if (aDepAvgRate != null && !aDepAvgRate.equals(""))
		{
			Double tDouble = new Double(aDepAvgRate);
			double d = tDouble.doubleValue();
			DepAvgRate = d;
		}
	}

	public double getDRDepAvgRate()
	{
		return DRDepAvgRate;
	}
	public void setDRDepAvgRate(double aDRDepAvgRate)
	{
		DRDepAvgRate = aDRDepAvgRate;
	}
	public void setDRDepAvgRate(String aDRDepAvgRate)
	{
		if (aDRDepAvgRate != null && !aDRDepAvgRate.equals(""))
		{
			Double tDouble = new Double(aDRDepAvgRate);
			double d = tDouble.doubleValue();
			DRDepAvgRate = d;
		}
	}

	public double getAreaRate()
	{
		return AreaRate;
	}
	public void setAreaRate(double aAreaRate)
	{
		AreaRate = aAreaRate;
	}
	public void setAreaRate(String aAreaRate)
	{
		if (aAreaRate != null && !aAreaRate.equals(""))
		{
			Double tDouble = new Double(aAreaRate);
			double d = tDouble.doubleValue();
			AreaRate = d;
		}
	}

	public int getIndCustSum()
	{
		return IndCustSum;
	}
	public void setIndCustSum(int aIndCustSum)
	{
		IndCustSum = aIndCustSum;
	}
	public void setIndCustSum(String aIndCustSum)
	{
		if (aIndCustSum != null && !aIndCustSum.equals(""))
		{
			Integer tInteger = new Integer(aIndCustSum);
			int i = tInteger.intValue();
			IndCustSum = i;
		}
	}

	public int getMonAvgCust()
	{
		return MonAvgCust;
	}
	public void setMonAvgCust(int aMonAvgCust)
	{
		MonAvgCust = aMonAvgCust;
	}
	public void setMonAvgCust(String aMonAvgCust)
	{
		if (aMonAvgCust != null && !aMonAvgCust.equals(""))
		{
			Integer tInteger = new Integer(aMonAvgCust);
			int i = tInteger.intValue();
			MonAvgCust = i;
		}
	}

	public int getDirAddCount()
	{
		return DirAddCount;
	}
	public void setDirAddCount(int aDirAddCount)
	{
		DirAddCount = aDirAddCount;
	}
	public void setDirAddCount(String aDirAddCount)
	{
		if (aDirAddCount != null && !aDirAddCount.equals(""))
		{
			Integer tInteger = new Integer(aDirAddCount);
			int i = tInteger.intValue();
			DirAddCount = i;
		}
	}

	public int getAddCount()
	{
		return AddCount;
	}
	public void setAddCount(int aAddCount)
	{
		AddCount = aAddCount;
	}
	public void setAddCount(String aAddCount)
	{
		if (aAddCount != null && !aAddCount.equals(""))
		{
			Integer tInteger = new Integer(aAddCount);
			int i = tInteger.intValue();
			AddCount = i;
		}
	}

	public int getIncFinaMngCount()
	{
		return IncFinaMngCount;
	}
	public void setIncFinaMngCount(int aIncFinaMngCount)
	{
		IncFinaMngCount = aIncFinaMngCount;
	}
	public void setIncFinaMngCount(String aIncFinaMngCount)
	{
		if (aIncFinaMngCount != null && !aIncFinaMngCount.equals(""))
		{
			Integer tInteger = new Integer(aIncFinaMngCount);
			int i = tInteger.intValue();
			IncFinaMngCount = i;
		}
	}

	public int getMngAgentCount()
	{
		return MngAgentCount;
	}
	public void setMngAgentCount(int aMngAgentCount)
	{
		MngAgentCount = aMngAgentCount;
	}
	public void setMngAgentCount(String aMngAgentCount)
	{
		if (aMngAgentCount != null && !aMngAgentCount.equals(""))
		{
			Integer tInteger = new Integer(aMngAgentCount);
			int i = tInteger.intValue();
			MngAgentCount = i;
		}
	}

	public int getMngFinaCount()
	{
		return MngFinaCount;
	}
	public void setMngFinaCount(int aMngFinaCount)
	{
		MngFinaCount = aMngFinaCount;
	}
	public void setMngFinaCount(String aMngFinaCount)
	{
		if (aMngFinaCount != null && !aMngFinaCount.equals(""))
		{
			Integer tInteger = new Integer(aMngFinaCount);
			int i = tInteger.intValue();
			MngFinaCount = i;
		}
	}

	public int getDirRmdCount()
	{
		return DirRmdCount;
	}
	public void setDirRmdCount(int aDirRmdCount)
	{
		DirRmdCount = aDirRmdCount;
	}
	public void setDirRmdCount(String aDirRmdCount)
	{
		if (aDirRmdCount != null && !aDirRmdCount.equals(""))
		{
			Integer tInteger = new Integer(aDirRmdCount);
			int i = tInteger.intValue();
			DirRmdCount = i;
		}
	}

	public int getDirMngCount()
	{
		return DirMngCount;
	}
	public void setDirMngCount(int aDirMngCount)
	{
		DirMngCount = aDirMngCount;
	}
	public void setDirMngCount(String aDirMngCount)
	{
		if (aDirMngCount != null && !aDirMngCount.equals(""))
		{
			Integer tInteger = new Integer(aDirMngCount);
			int i = tInteger.intValue();
			DirMngCount = i;
		}
	}

	public int getDirMngFinaCount()
	{
		return DirMngFinaCount;
	}
	public void setDirMngFinaCount(int aDirMngFinaCount)
	{
		DirMngFinaCount = aDirMngFinaCount;
	}
	public void setDirMngFinaCount(String aDirMngFinaCount)
	{
		if (aDirMngFinaCount != null && !aDirMngFinaCount.equals(""))
		{
			Integer tInteger = new Integer(aDirMngFinaCount);
			int i = tInteger.intValue();
			DirMngFinaCount = i;
		}
	}

	public int getTeamCount()
	{
		return TeamCount;
	}
	public void setTeamCount(int aTeamCount)
	{
		TeamCount = aTeamCount;
	}
	public void setTeamCount(String aTeamCount)
	{
		if (aTeamCount != null && !aTeamCount.equals(""))
		{
			Integer tInteger = new Integer(aTeamCount);
			int i = tInteger.intValue();
			TeamCount = i;
		}
	}

	public int getDRTeamCount()
	{
		return DRTeamCount;
	}
	public void setDRTeamCount(int aDRTeamCount)
	{
		DRTeamCount = aDRTeamCount;
	}
	public void setDRTeamCount(String aDRTeamCount)
	{
		if (aDRTeamCount != null && !aDRTeamCount.equals(""))
		{
			Integer tInteger = new Integer(aDRTeamCount);
			int i = tInteger.intValue();
			DRTeamCount = i;
		}
	}

	public int getRearTeamSum()
	{
		return RearTeamSum;
	}
	public void setRearTeamSum(int aRearTeamSum)
	{
		RearTeamSum = aRearTeamSum;
	}
	public void setRearTeamSum(String aRearTeamSum)
	{
		if (aRearTeamSum != null && !aRearTeamSum.equals(""))
		{
			Integer tInteger = new Integer(aRearTeamSum);
			int i = tInteger.intValue();
			RearTeamSum = i;
		}
	}

	public int getDRearDepCount()
	{
		return DRearDepCount;
	}
	public void setDRearDepCount(int aDRearDepCount)
	{
		DRearDepCount = aDRearDepCount;
	}
	public void setDRearDepCount(String aDRearDepCount)
	{
		if (aDRearDepCount != null && !aDRearDepCount.equals(""))
		{
			Integer tInteger = new Integer(aDRearDepCount);
			int i = tInteger.intValue();
			DRearDepCount = i;
		}
	}

	public int getRearDepCount()
	{
		return RearDepCount;
	}
	public void setRearDepCount(int aRearDepCount)
	{
		RearDepCount = aRearDepCount;
	}
	public void setRearDepCount(String aRearDepCount)
	{
		if (aRearDepCount != null && !aRearDepCount.equals(""))
		{
			Integer tInteger = new Integer(aRearDepCount);
			int i = tInteger.intValue();
			RearDepCount = i;
		}
	}

	public int getDInRCount()
	{
		return DInRCount;
	}
	public void setDInRCount(int aDInRCount)
	{
		DInRCount = aDInRCount;
	}
	public void setDInRCount(String aDInRCount)
	{
		if (aDInRCount != null && !aDInRCount.equals(""))
		{
			Integer tInteger = new Integer(aDInRCount);
			int i = tInteger.intValue();
			DInRCount = i;
		}
	}

	public int getDepFinaCount()
	{
		return DepFinaCount;
	}
	public void setDepFinaCount(int aDepFinaCount)
	{
		DepFinaCount = aDepFinaCount;
	}
	public void setDepFinaCount(String aDepFinaCount)
	{
		if (aDepFinaCount != null && !aDepFinaCount.equals(""))
		{
			Integer tInteger = new Integer(aDepFinaCount);
			int i = tInteger.intValue();
			DepFinaCount = i;
		}
	}

	public int getBranComFinaCount()
	{
		return BranComFinaCount;
	}
	public void setBranComFinaCount(int aBranComFinaCount)
	{
		BranComFinaCount = aBranComFinaCount;
	}
	public void setBranComFinaCount(String aBranComFinaCount)
	{
		if (aBranComFinaCount != null && !aBranComFinaCount.equals(""))
		{
			Integer tInteger = new Integer(aBranComFinaCount);
			int i = tInteger.intValue();
			BranComFinaCount = i;
		}
	}

	public int getRearAdmCount()
	{
		return RearAdmCount;
	}
	public void setRearAdmCount(int aRearAdmCount)
	{
		RearAdmCount = aRearAdmCount;
	}
	public void setRearAdmCount(String aRearAdmCount)
	{
		if (aRearAdmCount != null && !aRearAdmCount.equals(""))
		{
			Integer tInteger = new Integer(aRearAdmCount);
			int i = tInteger.intValue();
			RearAdmCount = i;
		}
	}

	public int getDRearAdmCount()
	{
		return DRearAdmCount;
	}
	public void setDRearAdmCount(int aDRearAdmCount)
	{
		DRearAdmCount = aDRearAdmCount;
	}
	public void setDRearAdmCount(String aDRearAdmCount)
	{
		if (aDRearAdmCount != null && !aDRearAdmCount.equals(""))
		{
			Integer tInteger = new Integer(aDRearAdmCount);
			int i = tInteger.intValue();
			DRearAdmCount = i;
		}
	}

	public int getMonAvgLabor()
	{
		return MonAvgLabor;
	}
	public void setMonAvgLabor(int aMonAvgLabor)
	{
		MonAvgLabor = aMonAvgLabor;
	}
	public void setMonAvgLabor(String aMonAvgLabor)
	{
		if (aMonAvgLabor != null && !aMonAvgLabor.equals(""))
		{
			Integer tInteger = new Integer(aMonAvgLabor);
			int i = tInteger.intValue();
			MonAvgLabor = i;
		}
	}

	public int getDRTeamMonLabor()
	{
		return DRTeamMonLabor;
	}
	public void setDRTeamMonLabor(int aDRTeamMonLabor)
	{
		DRTeamMonLabor = aDRTeamMonLabor;
	}
	public void setDRTeamMonLabor(String aDRTeamMonLabor)
	{
		if (aDRTeamMonLabor != null && !aDRTeamMonLabor.equals(""))
		{
			Integer tInteger = new Integer(aDRTeamMonLabor);
			int i = tInteger.intValue();
			DRTeamMonLabor = i;
		}
	}

	public int getDRDepMonLabor()
	{
		return DRDepMonLabor;
	}
	public void setDRDepMonLabor(int aDRDepMonLabor)
	{
		DRDepMonLabor = aDRDepMonLabor;
	}
	public void setDRDepMonLabor(String aDRDepMonLabor)
	{
		if (aDRDepMonLabor != null && !aDRDepMonLabor.equals(""))
		{
			Integer tInteger = new Integer(aDRDepMonLabor);
			int i = tInteger.intValue();
			DRDepMonLabor = i;
		}
	}

	public double getInitPension()
	{
		return InitPension;
	}
	public void setInitPension(double aInitPension)
	{
		InitPension = aInitPension;
	}
	public void setInitPension(String aInitPension)
	{
		if (aInitPension != null && !aInitPension.equals(""))
		{
			Double tDouble = new Double(aInitPension);
			double d = tDouble.doubleValue();
			InitPension = d;
		}
	}

	public double getStartCareerRwd()
	{
		return StartCareerRwd;
	}
	public void setStartCareerRwd(double aStartCareerRwd)
	{
		StartCareerRwd = aStartCareerRwd;
	}
	public void setStartCareerRwd(String aStartCareerRwd)
	{
		if (aStartCareerRwd != null && !aStartCareerRwd.equals(""))
		{
			Double tDouble = new Double(aStartCareerRwd);
			double d = tDouble.doubleValue();
			StartCareerRwd = d;
		}
	}

	public double getToFormalRwd()
	{
		return ToFormalRwd;
	}
	public void setToFormalRwd(double aToFormalRwd)
	{
		ToFormalRwd = aToFormalRwd;
	}
	public void setToFormalRwd(String aToFormalRwd)
	{
		if (aToFormalRwd != null && !aToFormalRwd.equals(""))
		{
			Double tDouble = new Double(aToFormalRwd);
			double d = tDouble.doubleValue();
			ToFormalRwd = d;
		}
	}

	public double getFirstPension()
	{
		return FirstPension;
	}
	public void setFirstPension(double aFirstPension)
	{
		FirstPension = aFirstPension;
	}
	public void setFirstPension(String aFirstPension)
	{
		if (aFirstPension != null && !aFirstPension.equals(""))
		{
			Double tDouble = new Double(aFirstPension);
			double d = tDouble.doubleValue();
			FirstPension = d;
		}
	}

	public double getContiuePension()
	{
		return ContiuePension;
	}
	public void setContiuePension(double aContiuePension)
	{
		ContiuePension = aContiuePension;
	}
	public void setContiuePension(String aContiuePension)
	{
		if (aContiuePension != null && !aContiuePension.equals(""))
		{
			Double tDouble = new Double(aContiuePension);
			double d = tDouble.doubleValue();
			ContiuePension = d;
		}
	}

	public double getPersonBonus()
	{
		return PersonBonus;
	}
	public void setPersonBonus(double aPersonBonus)
	{
		PersonBonus = aPersonBonus;
	}
	public void setPersonBonus(String aPersonBonus)
	{
		if (aPersonBonus != null && !aPersonBonus.equals(""))
		{
			Double tDouble = new Double(aPersonBonus);
			double d = tDouble.doubleValue();
			PersonBonus = d;
		}
	}

	public double getWiseBonus()
	{
		return WiseBonus;
	}
	public void setWiseBonus(double aWiseBonus)
	{
		WiseBonus = aWiseBonus;
	}
	public void setWiseBonus(String aWiseBonus)
	{
		if (aWiseBonus != null && !aWiseBonus.equals(""))
		{
			Double tDouble = new Double(aWiseBonus);
			double d = tDouble.doubleValue();
			WiseBonus = d;
		}
	}

	public double getRearBonus()
	{
		return RearBonus;
	}
	public void setRearBonus(double aRearBonus)
	{
		RearBonus = aRearBonus;
	}
	public void setRearBonus(String aRearBonus)
	{
		if (aRearBonus != null && !aRearBonus.equals(""))
		{
			Double tDouble = new Double(aRearBonus);
			double d = tDouble.doubleValue();
			RearBonus = d;
		}
	}

	public double getAddBonus()
	{
		return AddBonus;
	}
	public void setAddBonus(double aAddBonus)
	{
		AddBonus = aAddBonus;
	}
	public void setAddBonus(String aAddBonus)
	{
		if (aAddBonus != null && !aAddBonus.equals(""))
		{
			Double tDouble = new Double(aAddBonus);
			double d = tDouble.doubleValue();
			AddBonus = d;
		}
	}

	public double getSpecialPension()
	{
		return SpecialPension;
	}
	public void setSpecialPension(double aSpecialPension)
	{
		SpecialPension = aSpecialPension;
	}
	public void setSpecialPension(String aSpecialPension)
	{
		if (aSpecialPension != null && !aSpecialPension.equals(""))
		{
			Double tDouble = new Double(aSpecialPension);
			double d = tDouble.doubleValue();
			SpecialPension = d;
		}
	}

	public double getPosSdy()
	{
		return PosSdy;
	}
	public void setPosSdy(double aPosSdy)
	{
		PosSdy = aPosSdy;
	}
	public void setPosSdy(String aPosSdy)
	{
		if (aPosSdy != null && !aPosSdy.equals(""))
		{
			Double tDouble = new Double(aPosSdy);
			double d = tDouble.doubleValue();
			PosSdy = d;
		}
	}

	public double getTeamDirManaSdy()
	{
		return TeamDirManaSdy;
	}
	public void setTeamDirManaSdy(double aTeamDirManaSdy)
	{
		TeamDirManaSdy = aTeamDirManaSdy;
	}
	public void setTeamDirManaSdy(String aTeamDirManaSdy)
	{
		if (aTeamDirManaSdy != null && !aTeamDirManaSdy.equals(""))
		{
			Double tDouble = new Double(aTeamDirManaSdy);
			double d = tDouble.doubleValue();
			TeamDirManaSdy = d;
		}
	}

	public double getRearedSdy()
	{
		return RearedSdy;
	}
	public void setRearedSdy(double aRearedSdy)
	{
		RearedSdy = aRearedSdy;
	}
	public void setRearedSdy(String aRearedSdy)
	{
		if (aRearedSdy != null && !aRearedSdy.equals(""))
		{
			Double tDouble = new Double(aRearedSdy);
			double d = tDouble.doubleValue();
			RearedSdy = d;
		}
	}

	public double getTeamBonus()
	{
		return TeamBonus;
	}
	public void setTeamBonus(double aTeamBonus)
	{
		TeamBonus = aTeamBonus;
	}
	public void setTeamBonus(String aTeamBonus)
	{
		if (aTeamBonus != null && !aTeamBonus.equals(""))
		{
			Double tDouble = new Double(aTeamBonus);
			double d = tDouble.doubleValue();
			TeamBonus = d;
		}
	}

	public double getDepDirManaSdy()
	{
		return DepDirManaSdy;
	}
	public void setDepDirManaSdy(double aDepDirManaSdy)
	{
		DepDirManaSdy = aDepDirManaSdy;
	}
	public void setDepDirManaSdy(String aDepDirManaSdy)
	{
		if (aDepDirManaSdy != null && !aDepDirManaSdy.equals(""))
		{
			Double tDouble = new Double(aDepDirManaSdy);
			double d = tDouble.doubleValue();
			DepDirManaSdy = d;
		}
	}

	public double getAddDepSdy()
	{
		return AddDepSdy;
	}
	public void setAddDepSdy(double aAddDepSdy)
	{
		AddDepSdy = aAddDepSdy;
	}
	public void setAddDepSdy(String aAddDepSdy)
	{
		if (aAddDepSdy != null && !aAddDepSdy.equals(""))
		{
			Double tDouble = new Double(aAddDepSdy);
			double d = tDouble.doubleValue();
			AddDepSdy = d;
		}
	}

	public double getDepBonus()
	{
		return DepBonus;
	}
	public void setDepBonus(double aDepBonus)
	{
		DepBonus = aDepBonus;
	}
	public void setDepBonus(String aDepBonus)
	{
		if (aDepBonus != null && !aDepBonus.equals(""))
		{
			Double tDouble = new Double(aDepBonus);
			double d = tDouble.doubleValue();
			DepBonus = d;
		}
	}

	public double getBaseWage()
	{
		return BaseWage;
	}
	public void setBaseWage(double aBaseWage)
	{
		BaseWage = aBaseWage;
	}
	public void setBaseWage(String aBaseWage)
	{
		if (aBaseWage != null && !aBaseWage.equals(""))
		{
			Double tDouble = new Double(aBaseWage);
			double d = tDouble.doubleValue();
			BaseWage = d;
		}
	}

	public double getDisBaseWage()
	{
		return DisBaseWage;
	}
	public void setDisBaseWage(double aDisBaseWage)
	{
		DisBaseWage = aDisBaseWage;
	}
	public void setDisBaseWage(String aDisBaseWage)
	{
		if (aDisBaseWage != null && !aDisBaseWage.equals(""))
		{
			Double tDouble = new Double(aDisBaseWage);
			double d = tDouble.doubleValue();
			DisBaseWage = d;
		}
	}

	public double getRearAreaSdy()
	{
		return RearAreaSdy;
	}
	public void setRearAreaSdy(double aRearAreaSdy)
	{
		RearAreaSdy = aRearAreaSdy;
	}
	public void setRearAreaSdy(String aRearAreaSdy)
	{
		if (aRearAreaSdy != null && !aRearAreaSdy.equals(""))
		{
			Double tDouble = new Double(aRearAreaSdy);
			double d = tDouble.doubleValue();
			RearAreaSdy = d;
		}
	}

	public double getAddFYC()
	{
		return AddFYC;
	}
	public void setAddFYC(double aAddFYC)
	{
		AddFYC = aAddFYC;
	}
	public void setAddFYC(String aAddFYC)
	{
		if (aAddFYC != null && !aAddFYC.equals(""))
		{
			Double tDouble = new Double(aAddFYC);
			double d = tDouble.doubleValue();
			AddFYC = d;
		}
	}

	public double getT1()
	{
		return T1;
	}
	public void setT1(double aT1)
	{
		T1 = aT1;
	}
	public void setT1(String aT1)
	{
		if (aT1 != null && !aT1.equals(""))
		{
			Double tDouble = new Double(aT1);
			double d = tDouble.doubleValue();
			T1 = d;
		}
	}

	public double getT2()
	{
		return T2;
	}
	public void setT2(double aT2)
	{
		T2 = aT2;
	}
	public void setT2(String aT2)
	{
		if (aT2 != null && !aT2.equals(""))
		{
			Double tDouble = new Double(aT2);
			double d = tDouble.doubleValue();
			T2 = d;
		}
	}

	public double getT3()
	{
		return T3;
	}
	public void setT3(double aT3)
	{
		T3 = aT3;
	}
	public void setT3(String aT3)
	{
		if (aT3 != null && !aT3.equals(""))
		{
			Double tDouble = new Double(aT3);
			double d = tDouble.doubleValue();
			T3 = d;
		}
	}

	public double getT4()
	{
		return T4;
	}
	public void setT4(double aT4)
	{
		T4 = aT4;
	}
	public void setT4(String aT4)
	{
		if (aT4 != null && !aT4.equals(""))
		{
			Double tDouble = new Double(aT4);
			double d = tDouble.doubleValue();
			T4 = d;
		}
	}

	public double getT5()
	{
		return T5;
	}
	public void setT5(double aT5)
	{
		T5 = aT5;
	}
	public void setT5(String aT5)
	{
		if (aT5 != null && !aT5.equals(""))
		{
			Double tDouble = new Double(aT5);
			double d = tDouble.doubleValue();
			T5 = d;
		}
	}

	public double getT6()
	{
		return T6;
	}
	public void setT6(double aT6)
	{
		T6 = aT6;
	}
	public void setT6(String aT6)
	{
		if (aT6 != null && !aT6.equals(""))
		{
			Double tDouble = new Double(aT6);
			double d = tDouble.doubleValue();
			T6 = d;
		}
	}

	public double getT7()
	{
		return T7;
	}
	public void setT7(double aT7)
	{
		T7 = aT7;
	}
	public void setT7(String aT7)
	{
		if (aT7 != null && !aT7.equals(""))
		{
			Double tDouble = new Double(aT7);
			double d = tDouble.doubleValue();
			T7 = d;
		}
	}

	public double getT8()
	{
		return T8;
	}
	public void setT8(double aT8)
	{
		T8 = aT8;
	}
	public void setT8(String aT8)
	{
		if (aT8 != null && !aT8.equals(""))
		{
			Double tDouble = new Double(aT8);
			double d = tDouble.doubleValue();
			T8 = d;
		}
	}

	public double getT9()
	{
		return T9;
	}
	public void setT9(double aT9)
	{
		T9 = aT9;
	}
	public void setT9(String aT9)
	{
		if (aT9 != null && !aT9.equals(""))
		{
			Double tDouble = new Double(aT9);
			double d = tDouble.doubleValue();
			T9 = d;
		}
	}

	public double getT10()
	{
		return T10;
	}
	public void setT10(double aT10)
	{
		T10 = aT10;
	}
	public void setT10(String aT10)
	{
		if (aT10 != null && !aT10.equals(""))
		{
			Double tDouble = new Double(aT10);
			double d = tDouble.doubleValue();
			T10 = d;
		}
	}

	public double getT11()
	{
		return T11;
	}
	public void setT11(double aT11)
	{
		T11 = aT11;
	}
	public void setT11(String aT11)
	{
		if (aT11 != null && !aT11.equals(""))
		{
			Double tDouble = new Double(aT11);
			double d = tDouble.doubleValue();
			T11 = d;
		}
	}

	public double getT12()
	{
		return T12;
	}
	public void setT12(double aT12)
	{
		T12 = aT12;
	}
	public void setT12(String aT12)
	{
		if (aT12 != null && !aT12.equals(""))
		{
			Double tDouble = new Double(aT12);
			double d = tDouble.doubleValue();
			T12 = d;
		}
	}

	public double getT13()
	{
		return T13;
	}
	public void setT13(double aT13)
	{
		T13 = aT13;
	}
	public void setT13(String aT13)
	{
		if (aT13 != null && !aT13.equals(""))
		{
			Double tDouble = new Double(aT13);
			double d = tDouble.doubleValue();
			T13 = d;
		}
	}

	public double getT14()
	{
		return T14;
	}
	public void setT14(double aT14)
	{
		T14 = aT14;
	}
	public void setT14(String aT14)
	{
		if (aT14 != null && !aT14.equals(""))
		{
			Double tDouble = new Double(aT14);
			double d = tDouble.doubleValue();
			T14 = d;
		}
	}

	public double getT15()
	{
		return T15;
	}
	public void setT15(double aT15)
	{
		T15 = aT15;
	}
	public void setT15(String aT15)
	{
		if (aT15 != null && !aT15.equals(""))
		{
			Double tDouble = new Double(aT15);
			double d = tDouble.doubleValue();
			T15 = d;
		}
	}

	public double getT16()
	{
		return T16;
	}
	public void setT16(double aT16)
	{
		T16 = aT16;
	}
	public void setT16(String aT16)
	{
		if (aT16 != null && !aT16.equals(""))
		{
			Double tDouble = new Double(aT16);
			double d = tDouble.doubleValue();
			T16 = d;
		}
	}

	public double getT17()
	{
		return T17;
	}
	public void setT17(double aT17)
	{
		T17 = aT17;
	}
	public void setT17(String aT17)
	{
		if (aT17 != null && !aT17.equals(""))
		{
			Double tDouble = new Double(aT17);
			double d = tDouble.doubleValue();
			T17 = d;
		}
	}

	public double getT18()
	{
		return T18;
	}
	public void setT18(double aT18)
	{
		T18 = aT18;
	}
	public void setT18(String aT18)
	{
		if (aT18 != null && !aT18.equals(""))
		{
			Double tDouble = new Double(aT18);
			double d = tDouble.doubleValue();
			T18 = d;
		}
	}

	public double getT19()
	{
		return T19;
	}
	public void setT19(double aT19)
	{
		T19 = aT19;
	}
	public void setT19(String aT19)
	{
		if (aT19 != null && !aT19.equals(""))
		{
			Double tDouble = new Double(aT19);
			double d = tDouble.doubleValue();
			T19 = d;
		}
	}

	public double getT20()
	{
		return T20;
	}
	public void setT20(double aT20)
	{
		T20 = aT20;
	}
	public void setT20(String aT20)
	{
		if (aT20 != null && !aT20.equals(""))
		{
			Double tDouble = new Double(aT20);
			double d = tDouble.doubleValue();
			T20 = d;
		}
	}

	public double getT21()
	{
		return T21;
	}
	public void setT21(double aT21)
	{
		T21 = aT21;
	}
	public void setT21(String aT21)
	{
		if (aT21 != null && !aT21.equals(""))
		{
			Double tDouble = new Double(aT21);
			double d = tDouble.doubleValue();
			T21 = d;
		}
	}

	public double getT22()
	{
		return T22;
	}
	public void setT22(double aT22)
	{
		T22 = aT22;
	}
	public void setT22(String aT22)
	{
		if (aT22 != null && !aT22.equals(""))
		{
			Double tDouble = new Double(aT22);
			double d = tDouble.doubleValue();
			T22 = d;
		}
	}

	public double getT23()
	{
		return T23;
	}
	public void setT23(double aT23)
	{
		T23 = aT23;
	}
	public void setT23(String aT23)
	{
		if (aT23 != null && !aT23.equals(""))
		{
			Double tDouble = new Double(aT23);
			double d = tDouble.doubleValue();
			T23 = d;
		}
	}

	public double getT24()
	{
		return T24;
	}
	public void setT24(double aT24)
	{
		T24 = aT24;
	}
	public void setT24(String aT24)
	{
		if (aT24 != null && !aT24.equals(""))
		{
			Double tDouble = new Double(aT24);
			double d = tDouble.doubleValue();
			T24 = d;
		}
	}

	public double getT25()
	{
		return T25;
	}
	public void setT25(double aT25)
	{
		T25 = aT25;
	}
	public void setT25(String aT25)
	{
		if (aT25 != null && !aT25.equals(""))
		{
			Double tDouble = new Double(aT25);
			double d = tDouble.doubleValue();
			T25 = d;
		}
	}

	public double getT26()
	{
		return T26;
	}
	public void setT26(double aT26)
	{
		T26 = aT26;
	}
	public void setT26(String aT26)
	{
		if (aT26 != null && !aT26.equals(""))
		{
			Double tDouble = new Double(aT26);
			double d = tDouble.doubleValue();
			T26 = d;
		}
	}

	public double getT27()
	{
		return T27;
	}
	public void setT27(double aT27)
	{
		T27 = aT27;
	}
	public void setT27(String aT27)
	{
		if (aT27 != null && !aT27.equals(""))
		{
			Double tDouble = new Double(aT27);
			double d = tDouble.doubleValue();
			T27 = d;
		}
	}

	public double getT28()
	{
		return T28;
	}
	public void setT28(double aT28)
	{
		T28 = aT28;
	}
	public void setT28(String aT28)
	{
		if (aT28 != null && !aT28.equals(""))
		{
			Double tDouble = new Double(aT28);
			double d = tDouble.doubleValue();
			T28 = d;
		}
	}

	public double getT29()
	{
		return T29;
	}
	public void setT29(double aT29)
	{
		T29 = aT29;
	}
	public void setT29(String aT29)
	{
		if (aT29 != null && !aT29.equals(""))
		{
			Double tDouble = new Double(aT29);
			double d = tDouble.doubleValue();
			T29 = d;
		}
	}

	public double getT30()
	{
		return T30;
	}
	public void setT30(double aT30)
	{
		T30 = aT30;
	}
	public void setT30(String aT30)
	{
		if (aT30 != null && !aT30.equals(""))
		{
			Double tDouble = new Double(aT30);
			double d = tDouble.doubleValue();
			T30 = d;
		}
	}

	public String getState()
	{
		if (SysConst.CHANGECHARSET && State != null && !State.equals(""))
		{
			State = StrTool.unicodeToGBK(State);
		}
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
		{
			Operator = StrTool.unicodeToGBK(Operator);
		}
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
		{
			MakeTime = StrTool.unicodeToGBK(MakeTime);
		}
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		if (SysConst.CHANGECHARSET && ModifyTime != null && !ModifyTime.equals(""))
		{
			ModifyTime = StrTool.unicodeToGBK(ModifyTime);
		}
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getAssessMonth()
	{
		if (SysConst.CHANGECHARSET && AssessMonth != null && !AssessMonth.equals(""))
		{
			AssessMonth = StrTool.unicodeToGBK(AssessMonth);
		}
		return AssessMonth;
	}
	public void setAssessMonth(String aAssessMonth)
	{
		AssessMonth = aAssessMonth;
	}
	public double getT31()
	{
		return T31;
	}
	public void setT31(double aT31)
	{
		T31 = aT31;
	}
	public void setT31(String aT31)
	{
		if (aT31 != null && !aT31.equals(""))
		{
			Double tDouble = new Double(aT31);
			double d = tDouble.doubleValue();
			T31 = d;
		}
	}

	public double getT32()
	{
		return T32;
	}
	public void setT32(double aT32)
	{
		T32 = aT32;
	}
	public void setT32(String aT32)
	{
		if (aT32 != null && !aT32.equals(""))
		{
			Double tDouble = new Double(aT32);
			double d = tDouble.doubleValue();
			T32 = d;
		}
	}

	public double getT33()
	{
		return T33;
	}
	public void setT33(double aT33)
	{
		T33 = aT33;
	}
	public void setT33(String aT33)
	{
		if (aT33 != null && !aT33.equals(""))
		{
			Double tDouble = new Double(aT33);
			double d = tDouble.doubleValue();
			T33 = d;
		}
	}

	public double getT34()
	{
		return T34;
	}
	public void setT34(double aT34)
	{
		T34 = aT34;
	}
	public void setT34(String aT34)
	{
		if (aT34 != null && !aT34.equals(""))
		{
			Double tDouble = new Double(aT34);
			double d = tDouble.doubleValue();
			T34 = d;
		}
	}

	public double getT35()
	{
		return T35;
	}
	public void setT35(double aT35)
	{
		T35 = aT35;
	}
	public void setT35(String aT35)
	{
		if (aT35 != null && !aT35.equals(""))
		{
			Double tDouble = new Double(aT35);
			double d = tDouble.doubleValue();
			T35 = d;
		}
	}

	public double getT36()
	{
		return T36;
	}
	public void setT36(double aT36)
	{
		T36 = aT36;
	}
	public void setT36(String aT36)
	{
		if (aT36 != null && !aT36.equals(""))
		{
			Double tDouble = new Double(aT36);
			double d = tDouble.doubleValue();
			T36 = d;
		}
	}

	public double getT37()
	{
		return T37;
	}
	public void setT37(double aT37)
	{
		T37 = aT37;
	}
	public void setT37(String aT37)
	{
		if (aT37 != null && !aT37.equals(""))
		{
			Double tDouble = new Double(aT37);
			double d = tDouble.doubleValue();
			T37 = d;
		}
	}

	public double getT38()
	{
		return T38;
	}
	public void setT38(double aT38)
	{
		T38 = aT38;
	}
	public void setT38(String aT38)
	{
		if (aT38 != null && !aT38.equals(""))
		{
			Double tDouble = new Double(aT38);
			double d = tDouble.doubleValue();
			T38 = d;
		}
	}

	public double getT39()
	{
		return T39;
	}
	public void setT39(double aT39)
	{
		T39 = aT39;
	}
	public void setT39(String aT39)
	{
		if (aT39 != null && !aT39.equals(""))
		{
			Double tDouble = new Double(aT39);
			double d = tDouble.doubleValue();
			T39 = d;
		}
	}

	public double getT40()
	{
		return T40;
	}
	public void setT40(double aT40)
	{
		T40 = aT40;
	}
	public void setT40(String aT40)
	{
		if (aT40 != null && !aT40.equals(""))
		{
			Double tDouble = new Double(aT40);
			double d = tDouble.doubleValue();
			T40 = d;
		}
	}

	public double getT41()
	{
		return T41;
	}
	public void setT41(double aT41)
	{
		T41 = aT41;
	}
	public void setT41(String aT41)
	{
		if (aT41 != null && !aT41.equals(""))
		{
			Double tDouble = new Double(aT41);
			double d = tDouble.doubleValue();
			T41 = d;
		}
	}

	public double getT42()
	{
		return T42;
	}
	public void setT42(double aT42)
	{
		T42 = aT42;
	}
	public void setT42(String aT42)
	{
		if (aT42 != null && !aT42.equals(""))
		{
			Double tDouble = new Double(aT42);
			double d = tDouble.doubleValue();
			T42 = d;
		}
	}

	public double getT43()
	{
		return T43;
	}
	public void setT43(double aT43)
	{
		T43 = aT43;
	}
	public void setT43(String aT43)
	{
		if (aT43 != null && !aT43.equals(""))
		{
			Double tDouble = new Double(aT43);
			double d = tDouble.doubleValue();
			T43 = d;
		}
	}

	public double getT44()
	{
		return T44;
	}
	public void setT44(double aT44)
	{
		T44 = aT44;
	}
	public void setT44(String aT44)
	{
		if (aT44 != null && !aT44.equals(""))
		{
			Double tDouble = new Double(aT44);
			double d = tDouble.doubleValue();
			T44 = d;
		}
	}

	public double getT45()
	{
		return T45;
	}
	public void setT45(double aT45)
	{
		T45 = aT45;
	}
	public void setT45(String aT45)
	{
		if (aT45 != null && !aT45.equals(""))
		{
			Double tDouble = new Double(aT45);
			double d = tDouble.doubleValue();
			T45 = d;
		}
	}

	public double getT46()
	{
		return T46;
	}
	public void setT46(double aT46)
	{
		T46 = aT46;
	}
	public void setT46(String aT46)
	{
		if (aT46 != null && !aT46.equals(""))
		{
			Double tDouble = new Double(aT46);
			double d = tDouble.doubleValue();
			T46 = d;
		}
	}

	public double getT47()
	{
		return T47;
	}
	public void setT47(double aT47)
	{
		T47 = aT47;
	}
	public void setT47(String aT47)
	{
		if (aT47 != null && !aT47.equals(""))
		{
			Double tDouble = new Double(aT47);
			double d = tDouble.doubleValue();
			T47 = d;
		}
	}

	public double getT48()
	{
		return T48;
	}
	public void setT48(double aT48)
	{
		T48 = aT48;
	}
	public void setT48(String aT48)
	{
		if (aT48 != null && !aT48.equals(""))
		{
			Double tDouble = new Double(aT48);
			double d = tDouble.doubleValue();
			T48 = d;
		}
	}

	public double getT49()
	{
		return T49;
	}
	public void setT49(double aT49)
	{
		T49 = aT49;
	}
	public void setT49(String aT49)
	{
		if (aT49 != null && !aT49.equals(""))
		{
			Double tDouble = new Double(aT49);
			double d = tDouble.doubleValue();
			T49 = d;
		}
	}

	public double getT50()
	{
		return T50;
	}
	public void setT50(double aT50)
	{
		T50 = aT50;
	}
	public void setT50(String aT50)
	{
		if (aT50 != null && !aT50.equals(""))
		{
			Double tDouble = new Double(aT50);
			double d = tDouble.doubleValue();
			T50 = d;
		}
	}

	public double getT51()
	{
		return T51;
	}
	public void setT51(double aT51)
	{
		T51 = aT51;
	}
	public void setT51(String aT51)
	{
		if (aT51 != null && !aT51.equals(""))
		{
			Double tDouble = new Double(aT51);
			double d = tDouble.doubleValue();
			T51 = d;
		}
	}

	public double getT52()
	{
		return T52;
	}
	public void setT52(double aT52)
	{
		T52 = aT52;
	}
	public void setT52(String aT52)
	{
		if (aT52 != null && !aT52.equals(""))
		{
			Double tDouble = new Double(aT52);
			double d = tDouble.doubleValue();
			T52 = d;
		}
	}

	public double getT53()
	{
		return T53;
	}
	public void setT53(double aT53)
	{
		T53 = aT53;
	}
	public void setT53(String aT53)
	{
		if (aT53 != null && !aT53.equals(""))
		{
			Double tDouble = new Double(aT53);
			double d = tDouble.doubleValue();
			T53 = d;
		}
	}

	public double getT54()
	{
		return T54;
	}
	public void setT54(double aT54)
	{
		T54 = aT54;
	}
	public void setT54(String aT54)
	{
		if (aT54 != null && !aT54.equals(""))
		{
			Double tDouble = new Double(aT54);
			double d = tDouble.doubleValue();
			T54 = d;
		}
	}

	public double getT55()
	{
		return T55;
	}
	public void setT55(double aT55)
	{
		T55 = aT55;
	}
	public void setT55(String aT55)
	{
		if (aT55 != null && !aT55.equals(""))
		{
			Double tDouble = new Double(aT55);
			double d = tDouble.doubleValue();
			T55 = d;
		}
	}

	public double getT56()
	{
		return T56;
	}
	public void setT56(double aT56)
	{
		T56 = aT56;
	}
	public void setT56(String aT56)
	{
		if (aT56 != null && !aT56.equals(""))
		{
			Double tDouble = new Double(aT56);
			double d = tDouble.doubleValue();
			T56 = d;
		}
	}

	public double getT57()
	{
		return T57;
	}
	public void setT57(double aT57)
	{
		T57 = aT57;
	}
	public void setT57(String aT57)
	{
		if (aT57 != null && !aT57.equals(""))
		{
			Double tDouble = new Double(aT57);
			double d = tDouble.doubleValue();
			T57 = d;
		}
	}

	public double getT58()
	{
		return T58;
	}
	public void setT58(double aT58)
	{
		T58 = aT58;
	}
	public void setT58(String aT58)
	{
		if (aT58 != null && !aT58.equals(""))
		{
			Double tDouble = new Double(aT58);
			double d = tDouble.doubleValue();
			T58 = d;
		}
	}

	public String getBranchAttr()
	{
		if (SysConst.CHANGECHARSET && BranchAttr != null && !BranchAttr.equals(""))
		{
			BranchAttr = StrTool.unicodeToGBK(BranchAttr);
		}
		return BranchAttr;
	}
	public void setBranchAttr(String aBranchAttr)
	{
		BranchAttr = aBranchAttr;
	}
	public String getAgentGrade()
	{
		if (SysConst.CHANGECHARSET && AgentGrade != null && !AgentGrade.equals(""))
		{
			AgentGrade = StrTool.unicodeToGBK(AgentGrade);
		}
		return AgentGrade;
	}
	public void setAgentGrade(String aAgentGrade)
	{
		AgentGrade = aAgentGrade;
	}
	public double getT59()
	{
		return T59;
	}
	public void setT59(double aT59)
	{
		T59 = aT59;
	}
	public void setT59(String aT59)
	{
		if (aT59 != null && !aT59.equals(""))
		{
			Double tDouble = new Double(aT59);
			double d = tDouble.doubleValue();
			T59 = d;
		}
	}

	public double getT60()
	{
		return T60;
	}
	public void setT60(double aT60)
	{
		T60 = aT60;
	}
	public void setT60(String aT60)
	{
		if (aT60 != null && !aT60.equals(""))
		{
			Double tDouble = new Double(aT60);
			double d = tDouble.doubleValue();
			T60 = d;
		}
	}

	public double getT61()
	{
		return T61;
	}
	public void setT61(double aT61)
	{
		T61 = aT61;
	}
	public void setT61(String aT61)
	{
		if (aT61 != null && !aT61.equals(""))
		{
			Double tDouble = new Double(aT61);
			double d = tDouble.doubleValue();
			T61 = d;
		}
	}

	public double getT62()
	{
		return T62;
	}
	public void setT62(double aT62)
	{
		T62 = aT62;
	}
	public void setT62(String aT62)
	{
		if (aT62 != null && !aT62.equals(""))
		{
			Double tDouble = new Double(aT62);
			double d = tDouble.doubleValue();
			T62 = d;
		}
	}

	public double getT63()
	{
		return T63;
	}
	public void setT63(double aT63)
	{
		T63 = aT63;
	}
	public void setT63(String aT63)
	{
		if (aT63 != null && !aT63.equals(""))
		{
			Double tDouble = new Double(aT63);
			double d = tDouble.doubleValue();
			T63 = d;
		}
	}

	public double getT64()
	{
		return T64;
	}
	public void setT64(double aT64)
	{
		T64 = aT64;
	}
	public void setT64(String aT64)
	{
		if (aT64 != null && !aT64.equals(""))
		{
			Double tDouble = new Double(aT64);
			double d = tDouble.doubleValue();
			T64 = d;
		}
	}

	public double getT65()
	{
		return T65;
	}
	public void setT65(double aT65)
	{
		T65 = aT65;
	}
	public void setT65(String aT65)
	{
		if (aT65 != null && !aT65.equals(""))
		{
			Double tDouble = new Double(aT65);
			double d = tDouble.doubleValue();
			T65 = d;
		}
	}

	public double getT66()
	{
		return T66;
	}
	public void setT66(double aT66)
	{
		T66 = aT66;
	}
	public void setT66(String aT66)
	{
		if (aT66 != null && !aT66.equals(""))
		{
			Double tDouble = new Double(aT66);
			double d = tDouble.doubleValue();
			T66 = d;
		}
	}

	public double getT67()
	{
		return T67;
	}
	public void setT67(double aT67)
	{
		T67 = aT67;
	}
	public void setT67(String aT67)
	{
		if (aT67 != null && !aT67.equals(""))
		{
			Double tDouble = new Double(aT67);
			double d = tDouble.doubleValue();
			T67 = d;
		}
	}

	public double getT68()
	{
		return T68;
	}
	public void setT68(double aT68)
	{
		T68 = aT68;
	}
	public void setT68(String aT68)
	{
		if (aT68 != null && !aT68.equals(""))
		{
			Double tDouble = new Double(aT68);
			double d = tDouble.doubleValue();
			T68 = d;
		}
	}

	public double getT69()
	{
		return T69;
	}
	public void setT69(double aT69)
	{
		T69 = aT69;
	}
	public void setT69(String aT69)
	{
		if (aT69 != null && !aT69.equals(""))
		{
			Double tDouble = new Double(aT69);
			double d = tDouble.doubleValue();
			T69 = d;
		}
	}

	public double getT70()
	{
		return T70;
	}
	public void setT70(double aT70)
	{
		T70 = aT70;
	}
	public void setT70(String aT70)
	{
		if (aT70 != null && !aT70.equals(""))
		{
			Double tDouble = new Double(aT70);
			double d = tDouble.doubleValue();
			T70 = d;
		}
	}

	public double getT71()
	{
		return T71;
	}
	public void setT71(double aT71)
	{
		T71 = aT71;
	}
	public void setT71(String aT71)
	{
		if (aT71 != null && !aT71.equals(""))
		{
			Double tDouble = new Double(aT71);
			double d = tDouble.doubleValue();
			T71 = d;
		}
	}

	public String getBranchSeries()
	{
		if (SysConst.CHANGECHARSET && BranchSeries != null && !BranchSeries.equals(""))
		{
			BranchSeries = StrTool.unicodeToGBK(BranchSeries);
		}
		return BranchSeries;
	}
	public void setBranchSeries(String aBranchSeries)
	{
		BranchSeries = aBranchSeries;
	}
	public String getBranchType()
	{
		if (SysConst.CHANGECHARSET && BranchType != null && !BranchType.equals(""))
		{
			BranchType = StrTool.unicodeToGBK(BranchType);
		}
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		if (SysConst.CHANGECHARSET && BranchType2 != null && !BranchType2.equals(""))
		{
			BranchType2 = StrTool.unicodeToGBK(BranchType2);
		}
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}

	/**
	* 使用另外一个 LAIndexInfoSchema 对象给 Schema 赋值
	* @param: aLAIndexInfoSchema LAIndexInfoSchema
	**/
	public void setSchema(LAIndexInfoSchema aLAIndexInfoSchema)
	{
		this.IndexCalNo = aLAIndexInfoSchema.getIndexCalNo();
		this.IndexType = aLAIndexInfoSchema.getIndexType();
		this.AgentCode = aLAIndexInfoSchema.getAgentCode();
		this.AgentGroup = aLAIndexInfoSchema.getAgentGroup();
		this.ManageCom = aLAIndexInfoSchema.getManageCom();
		this.StartDate = fDate.getDate( aLAIndexInfoSchema.getStartDate());
		this.StartEnd = fDate.getDate( aLAIndexInfoSchema.getStartEnd());
		this.IndFYCSum = aLAIndexInfoSchema.getIndFYCSum();
		this.MonAvgFYC = aLAIndexInfoSchema.getMonAvgFYC();
		this.IndAddFYCSum = aLAIndexInfoSchema.getIndAddFYCSum();
		this.DirTeamFYCSum = aLAIndexInfoSchema.getDirTeamFYCSum();
		this.DRFYCSum = aLAIndexInfoSchema.getDRFYCSum();
		this.DepFYCSum = aLAIndexInfoSchema.getDepFYCSum();
		this.DirDepMonAvgFYC = aLAIndexInfoSchema.getDirDepMonAvgFYC();
		this.FYCZeroMon = aLAIndexInfoSchema.getFYCZeroMon();
		this.IndRate = aLAIndexInfoSchema.getIndRate();
		this.TeamAvgRate = aLAIndexInfoSchema.getTeamAvgRate();
		this.DRTeamAvgRate = aLAIndexInfoSchema.getDRTeamAvgRate();
		this.DepAvgRate = aLAIndexInfoSchema.getDepAvgRate();
		this.DRDepAvgRate = aLAIndexInfoSchema.getDRDepAvgRate();
		this.AreaRate = aLAIndexInfoSchema.getAreaRate();
		this.IndCustSum = aLAIndexInfoSchema.getIndCustSum();
		this.MonAvgCust = aLAIndexInfoSchema.getMonAvgCust();
		this.DirAddCount = aLAIndexInfoSchema.getDirAddCount();
		this.AddCount = aLAIndexInfoSchema.getAddCount();
		this.IncFinaMngCount = aLAIndexInfoSchema.getIncFinaMngCount();
		this.MngAgentCount = aLAIndexInfoSchema.getMngAgentCount();
		this.MngFinaCount = aLAIndexInfoSchema.getMngFinaCount();
		this.DirRmdCount = aLAIndexInfoSchema.getDirRmdCount();
		this.DirMngCount = aLAIndexInfoSchema.getDirMngCount();
		this.DirMngFinaCount = aLAIndexInfoSchema.getDirMngFinaCount();
		this.TeamCount = aLAIndexInfoSchema.getTeamCount();
		this.DRTeamCount = aLAIndexInfoSchema.getDRTeamCount();
		this.RearTeamSum = aLAIndexInfoSchema.getRearTeamSum();
		this.DRearDepCount = aLAIndexInfoSchema.getDRearDepCount();
		this.RearDepCount = aLAIndexInfoSchema.getRearDepCount();
		this.DInRCount = aLAIndexInfoSchema.getDInRCount();
		this.DepFinaCount = aLAIndexInfoSchema.getDepFinaCount();
		this.BranComFinaCount = aLAIndexInfoSchema.getBranComFinaCount();
		this.RearAdmCount = aLAIndexInfoSchema.getRearAdmCount();
		this.DRearAdmCount = aLAIndexInfoSchema.getDRearAdmCount();
		this.MonAvgLabor = aLAIndexInfoSchema.getMonAvgLabor();
		this.DRTeamMonLabor = aLAIndexInfoSchema.getDRTeamMonLabor();
		this.DRDepMonLabor = aLAIndexInfoSchema.getDRDepMonLabor();
		this.InitPension = aLAIndexInfoSchema.getInitPension();
		this.StartCareerRwd = aLAIndexInfoSchema.getStartCareerRwd();
		this.ToFormalRwd = aLAIndexInfoSchema.getToFormalRwd();
		this.FirstPension = aLAIndexInfoSchema.getFirstPension();
		this.ContiuePension = aLAIndexInfoSchema.getContiuePension();
		this.PersonBonus = aLAIndexInfoSchema.getPersonBonus();
		this.WiseBonus = aLAIndexInfoSchema.getWiseBonus();
		this.RearBonus = aLAIndexInfoSchema.getRearBonus();
		this.AddBonus = aLAIndexInfoSchema.getAddBonus();
		this.SpecialPension = aLAIndexInfoSchema.getSpecialPension();
		this.PosSdy = aLAIndexInfoSchema.getPosSdy();
		this.TeamDirManaSdy = aLAIndexInfoSchema.getTeamDirManaSdy();
		this.RearedSdy = aLAIndexInfoSchema.getRearedSdy();
		this.TeamBonus = aLAIndexInfoSchema.getTeamBonus();
		this.DepDirManaSdy = aLAIndexInfoSchema.getDepDirManaSdy();
		this.AddDepSdy = aLAIndexInfoSchema.getAddDepSdy();
		this.DepBonus = aLAIndexInfoSchema.getDepBonus();
		this.BaseWage = aLAIndexInfoSchema.getBaseWage();
		this.DisBaseWage = aLAIndexInfoSchema.getDisBaseWage();
		this.RearAreaSdy = aLAIndexInfoSchema.getRearAreaSdy();
		this.AddFYC = aLAIndexInfoSchema.getAddFYC();
		this.T1 = aLAIndexInfoSchema.getT1();
		this.T2 = aLAIndexInfoSchema.getT2();
		this.T3 = aLAIndexInfoSchema.getT3();
		this.T4 = aLAIndexInfoSchema.getT4();
		this.T5 = aLAIndexInfoSchema.getT5();
		this.T6 = aLAIndexInfoSchema.getT6();
		this.T7 = aLAIndexInfoSchema.getT7();
		this.T8 = aLAIndexInfoSchema.getT8();
		this.T9 = aLAIndexInfoSchema.getT9();
		this.T10 = aLAIndexInfoSchema.getT10();
		this.T11 = aLAIndexInfoSchema.getT11();
		this.T12 = aLAIndexInfoSchema.getT12();
		this.T13 = aLAIndexInfoSchema.getT13();
		this.T14 = aLAIndexInfoSchema.getT14();
		this.T15 = aLAIndexInfoSchema.getT15();
		this.T16 = aLAIndexInfoSchema.getT16();
		this.T17 = aLAIndexInfoSchema.getT17();
		this.T18 = aLAIndexInfoSchema.getT18();
		this.T19 = aLAIndexInfoSchema.getT19();
		this.T20 = aLAIndexInfoSchema.getT20();
		this.T21 = aLAIndexInfoSchema.getT21();
		this.T22 = aLAIndexInfoSchema.getT22();
		this.T23 = aLAIndexInfoSchema.getT23();
		this.T24 = aLAIndexInfoSchema.getT24();
		this.T25 = aLAIndexInfoSchema.getT25();
		this.T26 = aLAIndexInfoSchema.getT26();
		this.T27 = aLAIndexInfoSchema.getT27();
		this.T28 = aLAIndexInfoSchema.getT28();
		this.T29 = aLAIndexInfoSchema.getT29();
		this.T30 = aLAIndexInfoSchema.getT30();
		this.State = aLAIndexInfoSchema.getState();
		this.Operator = aLAIndexInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAIndexInfoSchema.getMakeDate());
		this.MakeTime = aLAIndexInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAIndexInfoSchema.getModifyDate());
		this.ModifyTime = aLAIndexInfoSchema.getModifyTime();
		this.AssessMonth = aLAIndexInfoSchema.getAssessMonth();
		this.T31 = aLAIndexInfoSchema.getT31();
		this.T32 = aLAIndexInfoSchema.getT32();
		this.T33 = aLAIndexInfoSchema.getT33();
		this.T34 = aLAIndexInfoSchema.getT34();
		this.T35 = aLAIndexInfoSchema.getT35();
		this.T36 = aLAIndexInfoSchema.getT36();
		this.T37 = aLAIndexInfoSchema.getT37();
		this.T38 = aLAIndexInfoSchema.getT38();
		this.T39 = aLAIndexInfoSchema.getT39();
		this.T40 = aLAIndexInfoSchema.getT40();
		this.T41 = aLAIndexInfoSchema.getT41();
		this.T42 = aLAIndexInfoSchema.getT42();
		this.T43 = aLAIndexInfoSchema.getT43();
		this.T44 = aLAIndexInfoSchema.getT44();
		this.T45 = aLAIndexInfoSchema.getT45();
		this.T46 = aLAIndexInfoSchema.getT46();
		this.T47 = aLAIndexInfoSchema.getT47();
		this.T48 = aLAIndexInfoSchema.getT48();
		this.T49 = aLAIndexInfoSchema.getT49();
		this.T50 = aLAIndexInfoSchema.getT50();
		this.T51 = aLAIndexInfoSchema.getT51();
		this.T52 = aLAIndexInfoSchema.getT52();
		this.T53 = aLAIndexInfoSchema.getT53();
		this.T54 = aLAIndexInfoSchema.getT54();
		this.T55 = aLAIndexInfoSchema.getT55();
		this.T56 = aLAIndexInfoSchema.getT56();
		this.T57 = aLAIndexInfoSchema.getT57();
		this.T58 = aLAIndexInfoSchema.getT58();
		this.BranchAttr = aLAIndexInfoSchema.getBranchAttr();
		this.AgentGrade = aLAIndexInfoSchema.getAgentGrade();
		this.T59 = aLAIndexInfoSchema.getT59();
		this.T60 = aLAIndexInfoSchema.getT60();
		this.T61 = aLAIndexInfoSchema.getT61();
		this.T62 = aLAIndexInfoSchema.getT62();
		this.T63 = aLAIndexInfoSchema.getT63();
		this.T64 = aLAIndexInfoSchema.getT64();
		this.T65 = aLAIndexInfoSchema.getT65();
		this.T66 = aLAIndexInfoSchema.getT66();
		this.T67 = aLAIndexInfoSchema.getT67();
		this.T68 = aLAIndexInfoSchema.getT68();
		this.T69 = aLAIndexInfoSchema.getT69();
		this.T70 = aLAIndexInfoSchema.getT70();
		this.T71 = aLAIndexInfoSchema.getT71();
		this.BranchSeries = aLAIndexInfoSchema.getBranchSeries();
		this.BranchType = aLAIndexInfoSchema.getBranchType();
		this.BranchType2 = aLAIndexInfoSchema.getBranchType2();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("IndexCalNo") == null )
				this.IndexCalNo = null;
			else
				this.IndexCalNo = rs.getString("IndexCalNo").trim();

			if( rs.getString("IndexType") == null )
				this.IndexType = null;
			else
				this.IndexType = rs.getString("IndexType").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.StartDate = rs.getDate("StartDate");
			this.StartEnd = rs.getDate("StartEnd");
			this.IndFYCSum = rs.getDouble("IndFYCSum");
			this.MonAvgFYC = rs.getDouble("MonAvgFYC");
			this.IndAddFYCSum = rs.getDouble("IndAddFYCSum");
			this.DirTeamFYCSum = rs.getDouble("DirTeamFYCSum");
			this.DRFYCSum = rs.getDouble("DRFYCSum");
			this.DepFYCSum = rs.getDouble("DepFYCSum");
			this.DirDepMonAvgFYC = rs.getDouble("DirDepMonAvgFYC");
			this.FYCZeroMon = rs.getInt("FYCZeroMon");
			this.IndRate = rs.getDouble("IndRate");
			this.TeamAvgRate = rs.getDouble("TeamAvgRate");
			this.DRTeamAvgRate = rs.getDouble("DRTeamAvgRate");
			this.DepAvgRate = rs.getDouble("DepAvgRate");
			this.DRDepAvgRate = rs.getDouble("DRDepAvgRate");
			this.AreaRate = rs.getDouble("AreaRate");
			this.IndCustSum = rs.getInt("IndCustSum");
			this.MonAvgCust = rs.getInt("MonAvgCust");
			this.DirAddCount = rs.getInt("DirAddCount");
			this.AddCount = rs.getInt("AddCount");
			this.IncFinaMngCount = rs.getInt("IncFinaMngCount");
			this.MngAgentCount = rs.getInt("MngAgentCount");
			this.MngFinaCount = rs.getInt("MngFinaCount");
			this.DirRmdCount = rs.getInt("DirRmdCount");
			this.DirMngCount = rs.getInt("DirMngCount");
			this.DirMngFinaCount = rs.getInt("DirMngFinaCount");
			this.TeamCount = rs.getInt("TeamCount");
			this.DRTeamCount = rs.getInt("DRTeamCount");
			this.RearTeamSum = rs.getInt("RearTeamSum");
			this.DRearDepCount = rs.getInt("DRearDepCount");
			this.RearDepCount = rs.getInt("RearDepCount");
			this.DInRCount = rs.getInt("DInRCount");
			this.DepFinaCount = rs.getInt("DepFinaCount");
			this.BranComFinaCount = rs.getInt("BranComFinaCount");
			this.RearAdmCount = rs.getInt("RearAdmCount");
			this.DRearAdmCount = rs.getInt("DRearAdmCount");
			this.MonAvgLabor = rs.getInt("MonAvgLabor");
			this.DRTeamMonLabor = rs.getInt("DRTeamMonLabor");
			this.DRDepMonLabor = rs.getInt("DRDepMonLabor");
			this.InitPension = rs.getDouble("InitPension");
			this.StartCareerRwd = rs.getDouble("StartCareerRwd");
			this.ToFormalRwd = rs.getDouble("ToFormalRwd");
			this.FirstPension = rs.getDouble("FirstPension");
			this.ContiuePension = rs.getDouble("ContiuePension");
			this.PersonBonus = rs.getDouble("PersonBonus");
			this.WiseBonus = rs.getDouble("WiseBonus");
			this.RearBonus = rs.getDouble("RearBonus");
			this.AddBonus = rs.getDouble("AddBonus");
			this.SpecialPension = rs.getDouble("SpecialPension");
			this.PosSdy = rs.getDouble("PosSdy");
			this.TeamDirManaSdy = rs.getDouble("TeamDirManaSdy");
			this.RearedSdy = rs.getDouble("RearedSdy");
			this.TeamBonus = rs.getDouble("TeamBonus");
			this.DepDirManaSdy = rs.getDouble("DepDirManaSdy");
			this.AddDepSdy = rs.getDouble("AddDepSdy");
			this.DepBonus = rs.getDouble("DepBonus");
			this.BaseWage = rs.getDouble("BaseWage");
			this.DisBaseWage = rs.getDouble("DisBaseWage");
			this.RearAreaSdy = rs.getDouble("RearAreaSdy");
			this.AddFYC = rs.getDouble("AddFYC");
			this.T1 = rs.getDouble("T1");
			this.T2 = rs.getDouble("T2");
			this.T3 = rs.getDouble("T3");
			this.T4 = rs.getDouble("T4");
			this.T5 = rs.getDouble("T5");
			this.T6 = rs.getDouble("T6");
			this.T7 = rs.getDouble("T7");
			this.T8 = rs.getDouble("T8");
			this.T9 = rs.getDouble("T9");
			this.T10 = rs.getDouble("T10");
			this.T11 = rs.getDouble("T11");
			this.T12 = rs.getDouble("T12");
			this.T13 = rs.getDouble("T13");
			this.T14 = rs.getDouble("T14");
			this.T15 = rs.getDouble("T15");
			this.T16 = rs.getDouble("T16");
			this.T17 = rs.getDouble("T17");
			this.T18 = rs.getDouble("T18");
			this.T19 = rs.getDouble("T19");
			this.T20 = rs.getDouble("T20");
			this.T21 = rs.getDouble("T21");
			this.T22 = rs.getDouble("T22");
			this.T23 = rs.getDouble("T23");
			this.T24 = rs.getDouble("T24");
			this.T25 = rs.getDouble("T25");
			this.T26 = rs.getDouble("T26");
			this.T27 = rs.getDouble("T27");
			this.T28 = rs.getDouble("T28");
			this.T29 = rs.getDouble("T29");
			this.T30 = rs.getDouble("T30");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("AssessMonth") == null )
				this.AssessMonth = null;
			else
				this.AssessMonth = rs.getString("AssessMonth").trim();

			this.T31 = rs.getDouble("T31");
			this.T32 = rs.getDouble("T32");
			this.T33 = rs.getDouble("T33");
			this.T34 = rs.getDouble("T34");
			this.T35 = rs.getDouble("T35");
			this.T36 = rs.getDouble("T36");
			this.T37 = rs.getDouble("T37");
			this.T38 = rs.getDouble("T38");
			this.T39 = rs.getDouble("T39");
			this.T40 = rs.getDouble("T40");
			this.T41 = rs.getDouble("T41");
			this.T42 = rs.getDouble("T42");
			this.T43 = rs.getDouble("T43");
			this.T44 = rs.getDouble("T44");
			this.T45 = rs.getDouble("T45");
			this.T46 = rs.getDouble("T46");
			this.T47 = rs.getDouble("T47");
			this.T48 = rs.getDouble("T48");
			this.T49 = rs.getDouble("T49");
			this.T50 = rs.getDouble("T50");
			this.T51 = rs.getDouble("T51");
			this.T52 = rs.getDouble("T52");
			this.T53 = rs.getDouble("T53");
			this.T54 = rs.getDouble("T54");
			this.T55 = rs.getDouble("T55");
			this.T56 = rs.getDouble("T56");
			this.T57 = rs.getDouble("T57");
			this.T58 = rs.getDouble("T58");
			if( rs.getString("BranchAttr") == null )
				this.BranchAttr = null;
			else
				this.BranchAttr = rs.getString("BranchAttr").trim();

			if( rs.getString("AgentGrade") == null )
				this.AgentGrade = null;
			else
				this.AgentGrade = rs.getString("AgentGrade").trim();

			this.T59 = rs.getDouble("T59");
			this.T60 = rs.getDouble("T60");
			this.T61 = rs.getDouble("T61");
			this.T62 = rs.getDouble("T62");
			this.T63 = rs.getDouble("T63");
			this.T64 = rs.getDouble("T64");
			this.T65 = rs.getDouble("T65");
			this.T66 = rs.getDouble("T66");
			this.T67 = rs.getDouble("T67");
			this.T68 = rs.getDouble("T68");
			this.T69 = rs.getDouble("T69");
			this.T70 = rs.getDouble("T70");
			this.T71 = rs.getDouble("T71");
			if( rs.getString("BranchSeries") == null )
				this.BranchSeries = null;
			else
				this.BranchSeries = rs.getString("BranchSeries").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

		}
		catch(SQLException sqle)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAIndexInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	public LAIndexInfoSchema getSchema()
	{
		LAIndexInfoSchema aLAIndexInfoSchema = new LAIndexInfoSchema();
		aLAIndexInfoSchema.setSchema(this);
		return aLAIndexInfoSchema;
	}

	public LAIndexInfoDB getDB()
	{
		LAIndexInfoDB aDBOper = new LAIndexInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAIndexInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(IndexCalNo))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(IndexType))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(AgentCode))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(AgentGroup))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(ManageCom))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( StartDate )))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( StartEnd )))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(IndFYCSum));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MonAvgFYC));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(IndAddFYCSum));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DirTeamFYCSum));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DRFYCSum));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DepFYCSum));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DirDepMonAvgFYC));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(FYCZeroMon));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(IndRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(TeamAvgRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DRTeamAvgRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DepAvgRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DRDepAvgRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AreaRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(IndCustSum));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MonAvgCust));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DirAddCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AddCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(IncFinaMngCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MngAgentCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MngFinaCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DirRmdCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DirMngCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DirMngFinaCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(TeamCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DRTeamCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RearTeamSum));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DRearDepCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RearDepCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DInRCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DepFinaCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(BranComFinaCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RearAdmCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DRearAdmCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MonAvgLabor));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DRTeamMonLabor));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DRDepMonLabor));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(InitPension));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(StartCareerRwd));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ToFormalRwd));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(FirstPension));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ContiuePension));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PersonBonus));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(WiseBonus));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RearBonus));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AddBonus));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SpecialPension));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PosSdy));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(TeamDirManaSdy));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RearedSdy));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(TeamBonus));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DepDirManaSdy));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AddDepSdy));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DepBonus));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(BaseWage));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DisBaseWage));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RearAreaSdy));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AddFYC));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T1));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T2));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T3));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T4));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T5));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T6));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T7));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T8));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T9));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T10));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T11));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T12));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T13));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T14));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T15));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T16));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T17));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T18));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T19));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T20));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T21));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T22));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T23));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T24));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T25));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T26));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T27));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T28));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T29));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T30));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(State))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(Operator))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( MakeDate )))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(MakeTime))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( ModifyDate )))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(ModifyTime))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(AssessMonth))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T31));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T32));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T33));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T34));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T35));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T36));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T37));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T38));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T39));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T40));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T41));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T42));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T43));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T44));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T45));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T46));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T47));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T48));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T49));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T50));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T51));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T52));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T53));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T54));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T55));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T56));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T57));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T58));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(BranchAttr))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(AgentGrade))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T59));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T60));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T61));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T62));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T63));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T64));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T65));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T66));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T67));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T68));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T69));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T70));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(T71));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(BranchSeries))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(BranchType))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(BranchType2)));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAIndexInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			IndexCalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			IndexType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			StartEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			IndFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			MonAvgFYC = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			IndAddFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			DirTeamFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			DRFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			DepFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			DirDepMonAvgFYC = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			FYCZeroMon= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).intValue();
			IndRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			TeamAvgRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			DRTeamAvgRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			DepAvgRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			DRDepAvgRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			AreaRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			IndCustSum= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).intValue();
			MonAvgCust= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).intValue();
			DirAddCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).intValue();
			AddCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).intValue();
			IncFinaMngCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).intValue();
			MngAgentCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).intValue();
			MngFinaCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).intValue();
			DirRmdCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).intValue();
			DirMngCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).intValue();
			DirMngFinaCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).intValue();
			TeamCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).intValue();
			DRTeamCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).intValue();
			RearTeamSum= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).intValue();
			DRearDepCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).intValue();
			RearDepCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).intValue();
			DInRCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).intValue();
			DepFinaCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,38,SysConst.PACKAGESPILTER))).intValue();
			BranComFinaCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,39,SysConst.PACKAGESPILTER))).intValue();
			RearAdmCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,40,SysConst.PACKAGESPILTER))).intValue();
			DRearAdmCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).intValue();
			MonAvgLabor= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,42,SysConst.PACKAGESPILTER))).intValue();
			DRTeamMonLabor= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,43,SysConst.PACKAGESPILTER))).intValue();
			DRDepMonLabor= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,44,SysConst.PACKAGESPILTER))).intValue();
			InitPension = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,45,SysConst.PACKAGESPILTER))).doubleValue();
			StartCareerRwd = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,46,SysConst.PACKAGESPILTER))).doubleValue();
			ToFormalRwd = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).doubleValue();
			FirstPension = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,48,SysConst.PACKAGESPILTER))).doubleValue();
			ContiuePension = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,49,SysConst.PACKAGESPILTER))).doubleValue();
			PersonBonus = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,50,SysConst.PACKAGESPILTER))).doubleValue();
			WiseBonus = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,51,SysConst.PACKAGESPILTER))).doubleValue();
			RearBonus = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,52,SysConst.PACKAGESPILTER))).doubleValue();
			AddBonus = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,53,SysConst.PACKAGESPILTER))).doubleValue();
			SpecialPension = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,54,SysConst.PACKAGESPILTER))).doubleValue();
			PosSdy = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,55,SysConst.PACKAGESPILTER))).doubleValue();
			TeamDirManaSdy = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,56,SysConst.PACKAGESPILTER))).doubleValue();
			RearedSdy = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,57,SysConst.PACKAGESPILTER))).doubleValue();
			TeamBonus = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,58,SysConst.PACKAGESPILTER))).doubleValue();
			DepDirManaSdy = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,59,SysConst.PACKAGESPILTER))).doubleValue();
			AddDepSdy = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,60,SysConst.PACKAGESPILTER))).doubleValue();
			DepBonus = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,61,SysConst.PACKAGESPILTER))).doubleValue();
			BaseWage = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,62,SysConst.PACKAGESPILTER))).doubleValue();
			DisBaseWage = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,63,SysConst.PACKAGESPILTER))).doubleValue();
			RearAreaSdy = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,64,SysConst.PACKAGESPILTER))).doubleValue();
			AddFYC = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,65,SysConst.PACKAGESPILTER))).doubleValue();
			T1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,66,SysConst.PACKAGESPILTER))).doubleValue();
			T2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,67,SysConst.PACKAGESPILTER))).doubleValue();
			T3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,68,SysConst.PACKAGESPILTER))).doubleValue();
			T4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,69,SysConst.PACKAGESPILTER))).doubleValue();
			T5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,70,SysConst.PACKAGESPILTER))).doubleValue();
			T6 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,71,SysConst.PACKAGESPILTER))).doubleValue();
			T7 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,72,SysConst.PACKAGESPILTER))).doubleValue();
			T8 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,73,SysConst.PACKAGESPILTER))).doubleValue();
			T9 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,74,SysConst.PACKAGESPILTER))).doubleValue();
			T10 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,75,SysConst.PACKAGESPILTER))).doubleValue();
			T11 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,76,SysConst.PACKAGESPILTER))).doubleValue();
			T12 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,77,SysConst.PACKAGESPILTER))).doubleValue();
			T13 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,78,SysConst.PACKAGESPILTER))).doubleValue();
			T14 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,79,SysConst.PACKAGESPILTER))).doubleValue();
			T15 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,80,SysConst.PACKAGESPILTER))).doubleValue();
			T16 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,81,SysConst.PACKAGESPILTER))).doubleValue();
			T17 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,82,SysConst.PACKAGESPILTER))).doubleValue();
			T18 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,83,SysConst.PACKAGESPILTER))).doubleValue();
			T19 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,84,SysConst.PACKAGESPILTER))).doubleValue();
			T20 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,85,SysConst.PACKAGESPILTER))).doubleValue();
			T21 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,86,SysConst.PACKAGESPILTER))).doubleValue();
			T22 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,87,SysConst.PACKAGESPILTER))).doubleValue();
			T23 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,88,SysConst.PACKAGESPILTER))).doubleValue();
			T24 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,89,SysConst.PACKAGESPILTER))).doubleValue();
			T25 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,90,SysConst.PACKAGESPILTER))).doubleValue();
			T26 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,91,SysConst.PACKAGESPILTER))).doubleValue();
			T27 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,92,SysConst.PACKAGESPILTER))).doubleValue();
			T28 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,93,SysConst.PACKAGESPILTER))).doubleValue();
			T29 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,94,SysConst.PACKAGESPILTER))).doubleValue();
			T30 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,95,SysConst.PACKAGESPILTER))).doubleValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 96, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 97, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 98,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 99, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 100,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 101, SysConst.PACKAGESPILTER );
			AssessMonth = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 102, SysConst.PACKAGESPILTER );
			T31 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,103,SysConst.PACKAGESPILTER))).doubleValue();
			T32 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,104,SysConst.PACKAGESPILTER))).doubleValue();
			T33 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,105,SysConst.PACKAGESPILTER))).doubleValue();
			T34 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,106,SysConst.PACKAGESPILTER))).doubleValue();
			T35 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,107,SysConst.PACKAGESPILTER))).doubleValue();
			T36 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,108,SysConst.PACKAGESPILTER))).doubleValue();
			T37 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,109,SysConst.PACKAGESPILTER))).doubleValue();
			T38 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,110,SysConst.PACKAGESPILTER))).doubleValue();
			T39 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,111,SysConst.PACKAGESPILTER))).doubleValue();
			T40 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,112,SysConst.PACKAGESPILTER))).doubleValue();
			T41 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,113,SysConst.PACKAGESPILTER))).doubleValue();
			T42 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,114,SysConst.PACKAGESPILTER))).doubleValue();
			T43 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,115,SysConst.PACKAGESPILTER))).doubleValue();
			T44 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,116,SysConst.PACKAGESPILTER))).doubleValue();
			T45 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,117,SysConst.PACKAGESPILTER))).doubleValue();
			T46 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,118,SysConst.PACKAGESPILTER))).doubleValue();
			T47 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,119,SysConst.PACKAGESPILTER))).doubleValue();
			T48 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,120,SysConst.PACKAGESPILTER))).doubleValue();
			T49 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,121,SysConst.PACKAGESPILTER))).doubleValue();
			T50 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,122,SysConst.PACKAGESPILTER))).doubleValue();
			T51 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,123,SysConst.PACKAGESPILTER))).doubleValue();
			T52 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,124,SysConst.PACKAGESPILTER))).doubleValue();
			T53 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,125,SysConst.PACKAGESPILTER))).doubleValue();
			T54 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,126,SysConst.PACKAGESPILTER))).doubleValue();
			T55 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,127,SysConst.PACKAGESPILTER))).doubleValue();
			T56 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,128,SysConst.PACKAGESPILTER))).doubleValue();
			T57 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,129,SysConst.PACKAGESPILTER))).doubleValue();
			T58 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,130,SysConst.PACKAGESPILTER))).doubleValue();
			BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 131, SysConst.PACKAGESPILTER );
			AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 132, SysConst.PACKAGESPILTER );
			T59 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,133,SysConst.PACKAGESPILTER))).doubleValue();
			T60 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,134,SysConst.PACKAGESPILTER))).doubleValue();
			T61 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,135,SysConst.PACKAGESPILTER))).doubleValue();
			T62 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,136,SysConst.PACKAGESPILTER))).doubleValue();
			T63 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,137,SysConst.PACKAGESPILTER))).doubleValue();
			T64 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,138,SysConst.PACKAGESPILTER))).doubleValue();
			T65 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,139,SysConst.PACKAGESPILTER))).doubleValue();
			T66 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,140,SysConst.PACKAGESPILTER))).doubleValue();
			T67 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,141,SysConst.PACKAGESPILTER))).doubleValue();
			T68 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,142,SysConst.PACKAGESPILTER))).doubleValue();
			T69 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,143,SysConst.PACKAGESPILTER))).doubleValue();
			T70 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,144,SysConst.PACKAGESPILTER))).doubleValue();
			T71 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,145,SysConst.PACKAGESPILTER))).doubleValue();
			BranchSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 146, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 147, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 148, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAIndexInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("IndexCalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCalNo));
		}
		if (FCode.equals("IndexType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexType));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("StartEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartEnd()));
		}
		if (FCode.equals("IndFYCSum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndFYCSum));
		}
		if (FCode.equals("MonAvgFYC"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MonAvgFYC));
		}
		if (FCode.equals("IndAddFYCSum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndAddFYCSum));
		}
		if (FCode.equals("DirTeamFYCSum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DirTeamFYCSum));
		}
		if (FCode.equals("DRFYCSum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DRFYCSum));
		}
		if (FCode.equals("DepFYCSum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepFYCSum));
		}
		if (FCode.equals("DirDepMonAvgFYC"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DirDepMonAvgFYC));
		}
		if (FCode.equals("FYCZeroMon"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FYCZeroMon));
		}
		if (FCode.equals("IndRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndRate));
		}
		if (FCode.equals("TeamAvgRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TeamAvgRate));
		}
		if (FCode.equals("DRTeamAvgRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DRTeamAvgRate));
		}
		if (FCode.equals("DepAvgRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepAvgRate));
		}
		if (FCode.equals("DRDepAvgRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DRDepAvgRate));
		}
		if (FCode.equals("AreaRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaRate));
		}
		if (FCode.equals("IndCustSum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndCustSum));
		}
		if (FCode.equals("MonAvgCust"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MonAvgCust));
		}
		if (FCode.equals("DirAddCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DirAddCount));
		}
		if (FCode.equals("AddCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddCount));
		}
		if (FCode.equals("IncFinaMngCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IncFinaMngCount));
		}
		if (FCode.equals("MngAgentCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngAgentCount));
		}
		if (FCode.equals("MngFinaCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngFinaCount));
		}
		if (FCode.equals("DirRmdCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DirRmdCount));
		}
		if (FCode.equals("DirMngCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DirMngCount));
		}
		if (FCode.equals("DirMngFinaCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DirMngFinaCount));
		}
		if (FCode.equals("TeamCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TeamCount));
		}
		if (FCode.equals("DRTeamCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DRTeamCount));
		}
		if (FCode.equals("RearTeamSum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RearTeamSum));
		}
		if (FCode.equals("DRearDepCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DRearDepCount));
		}
		if (FCode.equals("RearDepCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RearDepCount));
		}
		if (FCode.equals("DInRCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DInRCount));
		}
		if (FCode.equals("DepFinaCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepFinaCount));
		}
		if (FCode.equals("BranComFinaCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranComFinaCount));
		}
		if (FCode.equals("RearAdmCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RearAdmCount));
		}
		if (FCode.equals("DRearAdmCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DRearAdmCount));
		}
		if (FCode.equals("MonAvgLabor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MonAvgLabor));
		}
		if (FCode.equals("DRTeamMonLabor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DRTeamMonLabor));
		}
		if (FCode.equals("DRDepMonLabor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DRDepMonLabor));
		}
		if (FCode.equals("InitPension"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InitPension));
		}
		if (FCode.equals("StartCareerRwd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StartCareerRwd));
		}
		if (FCode.equals("ToFormalRwd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ToFormalRwd));
		}
		if (FCode.equals("FirstPension"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstPension));
		}
		if (FCode.equals("ContiuePension"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContiuePension));
		}
		if (FCode.equals("PersonBonus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersonBonus));
		}
		if (FCode.equals("WiseBonus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WiseBonus));
		}
		if (FCode.equals("RearBonus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RearBonus));
		}
		if (FCode.equals("AddBonus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddBonus));
		}
		if (FCode.equals("SpecialPension"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialPension));
		}
		if (FCode.equals("PosSdy"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PosSdy));
		}
		if (FCode.equals("TeamDirManaSdy"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TeamDirManaSdy));
		}
		if (FCode.equals("RearedSdy"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RearedSdy));
		}
		if (FCode.equals("TeamBonus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TeamBonus));
		}
		if (FCode.equals("DepDirManaSdy"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepDirManaSdy));
		}
		if (FCode.equals("AddDepSdy"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddDepSdy));
		}
		if (FCode.equals("DepBonus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DepBonus));
		}
		if (FCode.equals("BaseWage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BaseWage));
		}
		if (FCode.equals("DisBaseWage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DisBaseWage));
		}
		if (FCode.equals("RearAreaSdy"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RearAreaSdy));
		}
		if (FCode.equals("AddFYC"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddFYC));
		}
		if (FCode.equals("T1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T1));
		}
		if (FCode.equals("T2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T2));
		}
		if (FCode.equals("T3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T3));
		}
		if (FCode.equals("T4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T4));
		}
		if (FCode.equals("T5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T5));
		}
		if (FCode.equals("T6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T6));
		}
		if (FCode.equals("T7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T7));
		}
		if (FCode.equals("T8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T8));
		}
		if (FCode.equals("T9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T9));
		}
		if (FCode.equals("T10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T10));
		}
		if (FCode.equals("T11"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T11));
		}
		if (FCode.equals("T12"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T12));
		}
		if (FCode.equals("T13"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T13));
		}
		if (FCode.equals("T14"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T14));
		}
		if (FCode.equals("T15"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T15));
		}
		if (FCode.equals("T16"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T16));
		}
		if (FCode.equals("T17"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T17));
		}
		if (FCode.equals("T18"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T18));
		}
		if (FCode.equals("T19"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T19));
		}
		if (FCode.equals("T20"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T20));
		}
		if (FCode.equals("T21"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T21));
		}
		if (FCode.equals("T22"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T22));
		}
		if (FCode.equals("T23"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T23));
		}
		if (FCode.equals("T24"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T24));
		}
		if (FCode.equals("T25"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T25));
		}
		if (FCode.equals("T26"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T26));
		}
		if (FCode.equals("T27"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T27));
		}
		if (FCode.equals("T28"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T28));
		}
		if (FCode.equals("T29"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T29));
		}
		if (FCode.equals("T30"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T30));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("AssessMonth"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AssessMonth));
		}
		if (FCode.equals("T31"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T31));
		}
		if (FCode.equals("T32"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T32));
		}
		if (FCode.equals("T33"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T33));
		}
		if (FCode.equals("T34"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T34));
		}
		if (FCode.equals("T35"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T35));
		}
		if (FCode.equals("T36"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T36));
		}
		if (FCode.equals("T37"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T37));
		}
		if (FCode.equals("T38"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T38));
		}
		if (FCode.equals("T39"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T39));
		}
		if (FCode.equals("T40"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T40));
		}
		if (FCode.equals("T41"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T41));
		}
		if (FCode.equals("T42"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T42));
		}
		if (FCode.equals("T43"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T43));
		}
		if (FCode.equals("T44"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T44));
		}
		if (FCode.equals("T45"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T45));
		}
		if (FCode.equals("T46"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T46));
		}
		if (FCode.equals("T47"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T47));
		}
		if (FCode.equals("T48"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T48));
		}
		if (FCode.equals("T49"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T49));
		}
		if (FCode.equals("T50"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T50));
		}
		if (FCode.equals("T51"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T51));
		}
		if (FCode.equals("T52"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T52));
		}
		if (FCode.equals("T53"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T53));
		}
		if (FCode.equals("T54"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T54));
		}
		if (FCode.equals("T55"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T55));
		}
		if (FCode.equals("T56"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T56));
		}
		if (FCode.equals("T57"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T57));
		}
		if (FCode.equals("T58"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T58));
		}
		if (FCode.equals("BranchAttr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
		}
		if (FCode.equals("AgentGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
		}
		if (FCode.equals("T59"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T59));
		}
		if (FCode.equals("T60"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T60));
		}
		if (FCode.equals("T61"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T61));
		}
		if (FCode.equals("T62"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T62));
		}
		if (FCode.equals("T63"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T63));
		}
		if (FCode.equals("T64"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T64));
		}
		if (FCode.equals("T65"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T65));
		}
		if (FCode.equals("T66"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T66));
		}
		if (FCode.equals("T67"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T67));
		}
		if (FCode.equals("T68"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T68));
		}
		if (FCode.equals("T69"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T69));
		}
		if (FCode.equals("T70"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T70));
		}
		if (FCode.equals("T71"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T71));
		}
		if (FCode.equals("BranchSeries"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchSeries));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(IndexCalNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(IndexType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartEnd()));
				break;
			case 7:
				strFieldValue = String.valueOf(IndFYCSum);
				break;
			case 8:
				strFieldValue = String.valueOf(MonAvgFYC);
				break;
			case 9:
				strFieldValue = String.valueOf(IndAddFYCSum);
				break;
			case 10:
				strFieldValue = String.valueOf(DirTeamFYCSum);
				break;
			case 11:
				strFieldValue = String.valueOf(DRFYCSum);
				break;
			case 12:
				strFieldValue = String.valueOf(DepFYCSum);
				break;
			case 13:
				strFieldValue = String.valueOf(DirDepMonAvgFYC);
				break;
			case 14:
				strFieldValue = String.valueOf(FYCZeroMon);
				break;
			case 15:
				strFieldValue = String.valueOf(IndRate);
				break;
			case 16:
				strFieldValue = String.valueOf(TeamAvgRate);
				break;
			case 17:
				strFieldValue = String.valueOf(DRTeamAvgRate);
				break;
			case 18:
				strFieldValue = String.valueOf(DepAvgRate);
				break;
			case 19:
				strFieldValue = String.valueOf(DRDepAvgRate);
				break;
			case 20:
				strFieldValue = String.valueOf(AreaRate);
				break;
			case 21:
				strFieldValue = String.valueOf(IndCustSum);
				break;
			case 22:
				strFieldValue = String.valueOf(MonAvgCust);
				break;
			case 23:
				strFieldValue = String.valueOf(DirAddCount);
				break;
			case 24:
				strFieldValue = String.valueOf(AddCount);
				break;
			case 25:
				strFieldValue = String.valueOf(IncFinaMngCount);
				break;
			case 26:
				strFieldValue = String.valueOf(MngAgentCount);
				break;
			case 27:
				strFieldValue = String.valueOf(MngFinaCount);
				break;
			case 28:
				strFieldValue = String.valueOf(DirRmdCount);
				break;
			case 29:
				strFieldValue = String.valueOf(DirMngCount);
				break;
			case 30:
				strFieldValue = String.valueOf(DirMngFinaCount);
				break;
			case 31:
				strFieldValue = String.valueOf(TeamCount);
				break;
			case 32:
				strFieldValue = String.valueOf(DRTeamCount);
				break;
			case 33:
				strFieldValue = String.valueOf(RearTeamSum);
				break;
			case 34:
				strFieldValue = String.valueOf(DRearDepCount);
				break;
			case 35:
				strFieldValue = String.valueOf(RearDepCount);
				break;
			case 36:
				strFieldValue = String.valueOf(DInRCount);
				break;
			case 37:
				strFieldValue = String.valueOf(DepFinaCount);
				break;
			case 38:
				strFieldValue = String.valueOf(BranComFinaCount);
				break;
			case 39:
				strFieldValue = String.valueOf(RearAdmCount);
				break;
			case 40:
				strFieldValue = String.valueOf(DRearAdmCount);
				break;
			case 41:
				strFieldValue = String.valueOf(MonAvgLabor);
				break;
			case 42:
				strFieldValue = String.valueOf(DRTeamMonLabor);
				break;
			case 43:
				strFieldValue = String.valueOf(DRDepMonLabor);
				break;
			case 44:
				strFieldValue = String.valueOf(InitPension);
				break;
			case 45:
				strFieldValue = String.valueOf(StartCareerRwd);
				break;
			case 46:
				strFieldValue = String.valueOf(ToFormalRwd);
				break;
			case 47:
				strFieldValue = String.valueOf(FirstPension);
				break;
			case 48:
				strFieldValue = String.valueOf(ContiuePension);
				break;
			case 49:
				strFieldValue = String.valueOf(PersonBonus);
				break;
			case 50:
				strFieldValue = String.valueOf(WiseBonus);
				break;
			case 51:
				strFieldValue = String.valueOf(RearBonus);
				break;
			case 52:
				strFieldValue = String.valueOf(AddBonus);
				break;
			case 53:
				strFieldValue = String.valueOf(SpecialPension);
				break;
			case 54:
				strFieldValue = String.valueOf(PosSdy);
				break;
			case 55:
				strFieldValue = String.valueOf(TeamDirManaSdy);
				break;
			case 56:
				strFieldValue = String.valueOf(RearedSdy);
				break;
			case 57:
				strFieldValue = String.valueOf(TeamBonus);
				break;
			case 58:
				strFieldValue = String.valueOf(DepDirManaSdy);
				break;
			case 59:
				strFieldValue = String.valueOf(AddDepSdy);
				break;
			case 60:
				strFieldValue = String.valueOf(DepBonus);
				break;
			case 61:
				strFieldValue = String.valueOf(BaseWage);
				break;
			case 62:
				strFieldValue = String.valueOf(DisBaseWage);
				break;
			case 63:
				strFieldValue = String.valueOf(RearAreaSdy);
				break;
			case 64:
				strFieldValue = String.valueOf(AddFYC);
				break;
			case 65:
				strFieldValue = String.valueOf(T1);
				break;
			case 66:
				strFieldValue = String.valueOf(T2);
				break;
			case 67:
				strFieldValue = String.valueOf(T3);
				break;
			case 68:
				strFieldValue = String.valueOf(T4);
				break;
			case 69:
				strFieldValue = String.valueOf(T5);
				break;
			case 70:
				strFieldValue = String.valueOf(T6);
				break;
			case 71:
				strFieldValue = String.valueOf(T7);
				break;
			case 72:
				strFieldValue = String.valueOf(T8);
				break;
			case 73:
				strFieldValue = String.valueOf(T9);
				break;
			case 74:
				strFieldValue = String.valueOf(T10);
				break;
			case 75:
				strFieldValue = String.valueOf(T11);
				break;
			case 76:
				strFieldValue = String.valueOf(T12);
				break;
			case 77:
				strFieldValue = String.valueOf(T13);
				break;
			case 78:
				strFieldValue = String.valueOf(T14);
				break;
			case 79:
				strFieldValue = String.valueOf(T15);
				break;
			case 80:
				strFieldValue = String.valueOf(T16);
				break;
			case 81:
				strFieldValue = String.valueOf(T17);
				break;
			case 82:
				strFieldValue = String.valueOf(T18);
				break;
			case 83:
				strFieldValue = String.valueOf(T19);
				break;
			case 84:
				strFieldValue = String.valueOf(T20);
				break;
			case 85:
				strFieldValue = String.valueOf(T21);
				break;
			case 86:
				strFieldValue = String.valueOf(T22);
				break;
			case 87:
				strFieldValue = String.valueOf(T23);
				break;
			case 88:
				strFieldValue = String.valueOf(T24);
				break;
			case 89:
				strFieldValue = String.valueOf(T25);
				break;
			case 90:
				strFieldValue = String.valueOf(T26);
				break;
			case 91:
				strFieldValue = String.valueOf(T27);
				break;
			case 92:
				strFieldValue = String.valueOf(T28);
				break;
			case 93:
				strFieldValue = String.valueOf(T29);
				break;
			case 94:
				strFieldValue = String.valueOf(T30);
				break;
			case 95:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 96:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 97:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 98:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 99:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 100:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 101:
				strFieldValue = StrTool.GBKToUnicode(AssessMonth);
				break;
			case 102:
				strFieldValue = String.valueOf(T31);
				break;
			case 103:
				strFieldValue = String.valueOf(T32);
				break;
			case 104:
				strFieldValue = String.valueOf(T33);
				break;
			case 105:
				strFieldValue = String.valueOf(T34);
				break;
			case 106:
				strFieldValue = String.valueOf(T35);
				break;
			case 107:
				strFieldValue = String.valueOf(T36);
				break;
			case 108:
				strFieldValue = String.valueOf(T37);
				break;
			case 109:
				strFieldValue = String.valueOf(T38);
				break;
			case 110:
				strFieldValue = String.valueOf(T39);
				break;
			case 111:
				strFieldValue = String.valueOf(T40);
				break;
			case 112:
				strFieldValue = String.valueOf(T41);
				break;
			case 113:
				strFieldValue = String.valueOf(T42);
				break;
			case 114:
				strFieldValue = String.valueOf(T43);
				break;
			case 115:
				strFieldValue = String.valueOf(T44);
				break;
			case 116:
				strFieldValue = String.valueOf(T45);
				break;
			case 117:
				strFieldValue = String.valueOf(T46);
				break;
			case 118:
				strFieldValue = String.valueOf(T47);
				break;
			case 119:
				strFieldValue = String.valueOf(T48);
				break;
			case 120:
				strFieldValue = String.valueOf(T49);
				break;
			case 121:
				strFieldValue = String.valueOf(T50);
				break;
			case 122:
				strFieldValue = String.valueOf(T51);
				break;
			case 123:
				strFieldValue = String.valueOf(T52);
				break;
			case 124:
				strFieldValue = String.valueOf(T53);
				break;
			case 125:
				strFieldValue = String.valueOf(T54);
				break;
			case 126:
				strFieldValue = String.valueOf(T55);
				break;
			case 127:
				strFieldValue = String.valueOf(T56);
				break;
			case 128:
				strFieldValue = String.valueOf(T57);
				break;
			case 129:
				strFieldValue = String.valueOf(T58);
				break;
			case 130:
				strFieldValue = StrTool.GBKToUnicode(BranchAttr);
				break;
			case 131:
				strFieldValue = StrTool.GBKToUnicode(AgentGrade);
				break;
			case 132:
				strFieldValue = String.valueOf(T59);
				break;
			case 133:
				strFieldValue = String.valueOf(T60);
				break;
			case 134:
				strFieldValue = String.valueOf(T61);
				break;
			case 135:
				strFieldValue = String.valueOf(T62);
				break;
			case 136:
				strFieldValue = String.valueOf(T63);
				break;
			case 137:
				strFieldValue = String.valueOf(T64);
				break;
			case 138:
				strFieldValue = String.valueOf(T65);
				break;
			case 139:
				strFieldValue = String.valueOf(T66);
				break;
			case 140:
				strFieldValue = String.valueOf(T67);
				break;
			case 141:
				strFieldValue = String.valueOf(T68);
				break;
			case 142:
				strFieldValue = String.valueOf(T69);
				break;
			case 143:
				strFieldValue = String.valueOf(T70);
				break;
			case 144:
				strFieldValue = String.valueOf(T71);
				break;
			case 145:
				strFieldValue = StrTool.GBKToUnicode(BranchSeries);
				break;
			case 146:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 147:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equals("IndexCalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCalNo = FValue.trim();
			}
			else
				IndexCalNo = null;
		}
		if (FCode.equals("IndexType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexType = FValue.trim();
			}
			else
				IndexType = null;
		}
		if (FCode.equals("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equals("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equals("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equals("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equals("StartEnd"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartEnd = fDate.getDate( FValue );
			}
			else
				StartEnd = null;
		}
		if (FCode.equals("IndFYCSum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				IndFYCSum = d;
			}
		}
		if (FCode.equals("MonAvgFYC"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MonAvgFYC = d;
			}
		}
		if (FCode.equals("IndAddFYCSum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				IndAddFYCSum = d;
			}
		}
		if (FCode.equals("DirTeamFYCSum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DirTeamFYCSum = d;
			}
		}
		if (FCode.equals("DRFYCSum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DRFYCSum = d;
			}
		}
		if (FCode.equals("DepFYCSum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DepFYCSum = d;
			}
		}
		if (FCode.equals("DirDepMonAvgFYC"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DirDepMonAvgFYC = d;
			}
		}
		if (FCode.equals("FYCZeroMon"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				FYCZeroMon = i;
			}
		}
		if (FCode.equals("IndRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				IndRate = d;
			}
		}
		if (FCode.equals("TeamAvgRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TeamAvgRate = d;
			}
		}
		if (FCode.equals("DRTeamAvgRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DRTeamAvgRate = d;
			}
		}
		if (FCode.equals("DepAvgRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DepAvgRate = d;
			}
		}
		if (FCode.equals("DRDepAvgRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DRDepAvgRate = d;
			}
		}
		if (FCode.equals("AreaRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AreaRate = d;
			}
		}
		if (FCode.equals("IndCustSum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				IndCustSum = i;
			}
		}
		if (FCode.equals("MonAvgCust"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MonAvgCust = i;
			}
		}
		if (FCode.equals("DirAddCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DirAddCount = i;
			}
		}
		if (FCode.equals("AddCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AddCount = i;
			}
		}
		if (FCode.equals("IncFinaMngCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				IncFinaMngCount = i;
			}
		}
		if (FCode.equals("MngAgentCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MngAgentCount = i;
			}
		}
		if (FCode.equals("MngFinaCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MngFinaCount = i;
			}
		}
		if (FCode.equals("DirRmdCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DirRmdCount = i;
			}
		}
		if (FCode.equals("DirMngCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DirMngCount = i;
			}
		}
		if (FCode.equals("DirMngFinaCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DirMngFinaCount = i;
			}
		}
		if (FCode.equals("TeamCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				TeamCount = i;
			}
		}
		if (FCode.equals("DRTeamCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DRTeamCount = i;
			}
		}
		if (FCode.equals("RearTeamSum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RearTeamSum = i;
			}
		}
		if (FCode.equals("DRearDepCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DRearDepCount = i;
			}
		}
		if (FCode.equals("RearDepCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RearDepCount = i;
			}
		}
		if (FCode.equals("DInRCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DInRCount = i;
			}
		}
		if (FCode.equals("DepFinaCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DepFinaCount = i;
			}
		}
		if (FCode.equals("BranComFinaCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				BranComFinaCount = i;
			}
		}
		if (FCode.equals("RearAdmCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RearAdmCount = i;
			}
		}
		if (FCode.equals("DRearAdmCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DRearAdmCount = i;
			}
		}
		if (FCode.equals("MonAvgLabor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MonAvgLabor = i;
			}
		}
		if (FCode.equals("DRTeamMonLabor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DRTeamMonLabor = i;
			}
		}
		if (FCode.equals("DRDepMonLabor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DRDepMonLabor = i;
			}
		}
		if (FCode.equals("InitPension"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				InitPension = d;
			}
		}
		if (FCode.equals("StartCareerRwd"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StartCareerRwd = d;
			}
		}
		if (FCode.equals("ToFormalRwd"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ToFormalRwd = d;
			}
		}
		if (FCode.equals("FirstPension"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FirstPension = d;
			}
		}
		if (FCode.equals("ContiuePension"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ContiuePension = d;
			}
		}
		if (FCode.equals("PersonBonus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PersonBonus = d;
			}
		}
		if (FCode.equals("WiseBonus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				WiseBonus = d;
			}
		}
		if (FCode.equals("RearBonus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RearBonus = d;
			}
		}
		if (FCode.equals("AddBonus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AddBonus = d;
			}
		}
		if (FCode.equals("SpecialPension"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SpecialPension = d;
			}
		}
		if (FCode.equals("PosSdy"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PosSdy = d;
			}
		}
		if (FCode.equals("TeamDirManaSdy"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TeamDirManaSdy = d;
			}
		}
		if (FCode.equals("RearedSdy"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RearedSdy = d;
			}
		}
		if (FCode.equals("TeamBonus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TeamBonus = d;
			}
		}
		if (FCode.equals("DepDirManaSdy"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DepDirManaSdy = d;
			}
		}
		if (FCode.equals("AddDepSdy"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AddDepSdy = d;
			}
		}
		if (FCode.equals("DepBonus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DepBonus = d;
			}
		}
		if (FCode.equals("BaseWage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				BaseWage = d;
			}
		}
		if (FCode.equals("DisBaseWage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DisBaseWage = d;
			}
		}
		if (FCode.equals("RearAreaSdy"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RearAreaSdy = d;
			}
		}
		if (FCode.equals("AddFYC"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AddFYC = d;
			}
		}
		if (FCode.equals("T1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T1 = d;
			}
		}
		if (FCode.equals("T2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T2 = d;
			}
		}
		if (FCode.equals("T3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T3 = d;
			}
		}
		if (FCode.equals("T4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T4 = d;
			}
		}
		if (FCode.equals("T5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T5 = d;
			}
		}
		if (FCode.equals("T6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T6 = d;
			}
		}
		if (FCode.equals("T7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T7 = d;
			}
		}
		if (FCode.equals("T8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T8 = d;
			}
		}
		if (FCode.equals("T9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T9 = d;
			}
		}
		if (FCode.equals("T10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T10 = d;
			}
		}
		if (FCode.equals("T11"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T11 = d;
			}
		}
		if (FCode.equals("T12"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T12 = d;
			}
		}
		if (FCode.equals("T13"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T13 = d;
			}
		}
		if (FCode.equals("T14"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T14 = d;
			}
		}
		if (FCode.equals("T15"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T15 = d;
			}
		}
		if (FCode.equals("T16"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T16 = d;
			}
		}
		if (FCode.equals("T17"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T17 = d;
			}
		}
		if (FCode.equals("T18"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T18 = d;
			}
		}
		if (FCode.equals("T19"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T19 = d;
			}
		}
		if (FCode.equals("T20"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T20 = d;
			}
		}
		if (FCode.equals("T21"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T21 = d;
			}
		}
		if (FCode.equals("T22"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T22 = d;
			}
		}
		if (FCode.equals("T23"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T23 = d;
			}
		}
		if (FCode.equals("T24"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T24 = d;
			}
		}
		if (FCode.equals("T25"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T25 = d;
			}
		}
		if (FCode.equals("T26"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T26 = d;
			}
		}
		if (FCode.equals("T27"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T27 = d;
			}
		}
		if (FCode.equals("T28"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T28 = d;
			}
		}
		if (FCode.equals("T29"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T29 = d;
			}
		}
		if (FCode.equals("T30"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T30 = d;
			}
		}
		if (FCode.equals("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equals("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equals("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equals("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equals("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equals("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equals("AssessMonth"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AssessMonth = FValue.trim();
			}
			else
				AssessMonth = null;
		}
		if (FCode.equals("T31"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T31 = d;
			}
		}
		if (FCode.equals("T32"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T32 = d;
			}
		}
		if (FCode.equals("T33"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T33 = d;
			}
		}
		if (FCode.equals("T34"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T34 = d;
			}
		}
		if (FCode.equals("T35"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T35 = d;
			}
		}
		if (FCode.equals("T36"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T36 = d;
			}
		}
		if (FCode.equals("T37"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T37 = d;
			}
		}
		if (FCode.equals("T38"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T38 = d;
			}
		}
		if (FCode.equals("T39"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T39 = d;
			}
		}
		if (FCode.equals("T40"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T40 = d;
			}
		}
		if (FCode.equals("T41"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T41 = d;
			}
		}
		if (FCode.equals("T42"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T42 = d;
			}
		}
		if (FCode.equals("T43"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T43 = d;
			}
		}
		if (FCode.equals("T44"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T44 = d;
			}
		}
		if (FCode.equals("T45"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T45 = d;
			}
		}
		if (FCode.equals("T46"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T46 = d;
			}
		}
		if (FCode.equals("T47"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T47 = d;
			}
		}
		if (FCode.equals("T48"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T48 = d;
			}
		}
		if (FCode.equals("T49"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T49 = d;
			}
		}
		if (FCode.equals("T50"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T50 = d;
			}
		}
		if (FCode.equals("T51"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T51 = d;
			}
		}
		if (FCode.equals("T52"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T52 = d;
			}
		}
		if (FCode.equals("T53"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T53 = d;
			}
		}
		if (FCode.equals("T54"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T54 = d;
			}
		}
		if (FCode.equals("T55"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T55 = d;
			}
		}
		if (FCode.equals("T56"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T56 = d;
			}
		}
		if (FCode.equals("T57"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T57 = d;
			}
		}
		if (FCode.equals("T58"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T58 = d;
			}
		}
		if (FCode.equals("BranchAttr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchAttr = FValue.trim();
			}
			else
				BranchAttr = null;
		}
		if (FCode.equals("AgentGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGrade = FValue.trim();
			}
			else
				AgentGrade = null;
		}
		if (FCode.equals("T59"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T59 = d;
			}
		}
		if (FCode.equals("T60"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T60 = d;
			}
		}
		if (FCode.equals("T61"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T61 = d;
			}
		}
		if (FCode.equals("T62"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T62 = d;
			}
		}
		if (FCode.equals("T63"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T63 = d;
			}
		}
		if (FCode.equals("T64"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T64 = d;
			}
		}
		if (FCode.equals("T65"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T65 = d;
			}
		}
		if (FCode.equals("T66"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T66 = d;
			}
		}
		if (FCode.equals("T67"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T67 = d;
			}
		}
		if (FCode.equals("T68"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T68 = d;
			}
		}
		if (FCode.equals("T69"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T69 = d;
			}
		}
		if (FCode.equals("T70"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T70 = d;
			}
		}
		if (FCode.equals("T71"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T71 = d;
			}
		}
		if (FCode.equals("BranchSeries"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchSeries = FValue.trim();
			}
			else
				BranchSeries = null;
		}
		if (FCode.equals("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equals("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAIndexInfoSchema other = (LAIndexInfoSchema)otherObject;
		return
			IndexCalNo.equals(other.getIndexCalNo())
			&& IndexType.equals(other.getIndexType())
			&& AgentCode.equals(other.getAgentCode())
			&& AgentGroup.equals(other.getAgentGroup())
			&& ManageCom.equals(other.getManageCom())
			&& fDate.getString(StartDate).equals(other.getStartDate())
			&& fDate.getString(StartEnd).equals(other.getStartEnd())
			&& IndFYCSum == other.getIndFYCSum()
			&& MonAvgFYC == other.getMonAvgFYC()
			&& IndAddFYCSum == other.getIndAddFYCSum()
			&& DirTeamFYCSum == other.getDirTeamFYCSum()
			&& DRFYCSum == other.getDRFYCSum()
			&& DepFYCSum == other.getDepFYCSum()
			&& DirDepMonAvgFYC == other.getDirDepMonAvgFYC()
			&& FYCZeroMon == other.getFYCZeroMon()
			&& IndRate == other.getIndRate()
			&& TeamAvgRate == other.getTeamAvgRate()
			&& DRTeamAvgRate == other.getDRTeamAvgRate()
			&& DepAvgRate == other.getDepAvgRate()
			&& DRDepAvgRate == other.getDRDepAvgRate()
			&& AreaRate == other.getAreaRate()
			&& IndCustSum == other.getIndCustSum()
			&& MonAvgCust == other.getMonAvgCust()
			&& DirAddCount == other.getDirAddCount()
			&& AddCount == other.getAddCount()
			&& IncFinaMngCount == other.getIncFinaMngCount()
			&& MngAgentCount == other.getMngAgentCount()
			&& MngFinaCount == other.getMngFinaCount()
			&& DirRmdCount == other.getDirRmdCount()
			&& DirMngCount == other.getDirMngCount()
			&& DirMngFinaCount == other.getDirMngFinaCount()
			&& TeamCount == other.getTeamCount()
			&& DRTeamCount == other.getDRTeamCount()
			&& RearTeamSum == other.getRearTeamSum()
			&& DRearDepCount == other.getDRearDepCount()
			&& RearDepCount == other.getRearDepCount()
			&& DInRCount == other.getDInRCount()
			&& DepFinaCount == other.getDepFinaCount()
			&& BranComFinaCount == other.getBranComFinaCount()
			&& RearAdmCount == other.getRearAdmCount()
			&& DRearAdmCount == other.getDRearAdmCount()
			&& MonAvgLabor == other.getMonAvgLabor()
			&& DRTeamMonLabor == other.getDRTeamMonLabor()
			&& DRDepMonLabor == other.getDRDepMonLabor()
			&& InitPension == other.getInitPension()
			&& StartCareerRwd == other.getStartCareerRwd()
			&& ToFormalRwd == other.getToFormalRwd()
			&& FirstPension == other.getFirstPension()
			&& ContiuePension == other.getContiuePension()
			&& PersonBonus == other.getPersonBonus()
			&& WiseBonus == other.getWiseBonus()
			&& RearBonus == other.getRearBonus()
			&& AddBonus == other.getAddBonus()
			&& SpecialPension == other.getSpecialPension()
			&& PosSdy == other.getPosSdy()
			&& TeamDirManaSdy == other.getTeamDirManaSdy()
			&& RearedSdy == other.getRearedSdy()
			&& TeamBonus == other.getTeamBonus()
			&& DepDirManaSdy == other.getDepDirManaSdy()
			&& AddDepSdy == other.getAddDepSdy()
			&& DepBonus == other.getDepBonus()
			&& BaseWage == other.getBaseWage()
			&& DisBaseWage == other.getDisBaseWage()
			&& RearAreaSdy == other.getRearAreaSdy()
			&& AddFYC == other.getAddFYC()
			&& T1 == other.getT1()
			&& T2 == other.getT2()
			&& T3 == other.getT3()
			&& T4 == other.getT4()
			&& T5 == other.getT5()
			&& T6 == other.getT6()
			&& T7 == other.getT7()
			&& T8 == other.getT8()
			&& T9 == other.getT9()
			&& T10 == other.getT10()
			&& T11 == other.getT11()
			&& T12 == other.getT12()
			&& T13 == other.getT13()
			&& T14 == other.getT14()
			&& T15 == other.getT15()
			&& T16 == other.getT16()
			&& T17 == other.getT17()
			&& T18 == other.getT18()
			&& T19 == other.getT19()
			&& T20 == other.getT20()
			&& T21 == other.getT21()
			&& T22 == other.getT22()
			&& T23 == other.getT23()
			&& T24 == other.getT24()
			&& T25 == other.getT25()
			&& T26 == other.getT26()
			&& T27 == other.getT27()
			&& T28 == other.getT28()
			&& T29 == other.getT29()
			&& T30 == other.getT30()
			&& State.equals(other.getState())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& AssessMonth.equals(other.getAssessMonth())
			&& T31 == other.getT31()
			&& T32 == other.getT32()
			&& T33 == other.getT33()
			&& T34 == other.getT34()
			&& T35 == other.getT35()
			&& T36 == other.getT36()
			&& T37 == other.getT37()
			&& T38 == other.getT38()
			&& T39 == other.getT39()
			&& T40 == other.getT40()
			&& T41 == other.getT41()
			&& T42 == other.getT42()
			&& T43 == other.getT43()
			&& T44 == other.getT44()
			&& T45 == other.getT45()
			&& T46 == other.getT46()
			&& T47 == other.getT47()
			&& T48 == other.getT48()
			&& T49 == other.getT49()
			&& T50 == other.getT50()
			&& T51 == other.getT51()
			&& T52 == other.getT52()
			&& T53 == other.getT53()
			&& T54 == other.getT54()
			&& T55 == other.getT55()
			&& T56 == other.getT56()
			&& T57 == other.getT57()
			&& T58 == other.getT58()
			&& BranchAttr.equals(other.getBranchAttr())
			&& AgentGrade.equals(other.getAgentGrade())
			&& T59 == other.getT59()
			&& T60 == other.getT60()
			&& T61 == other.getT61()
			&& T62 == other.getT62()
			&& T63 == other.getT63()
			&& T64 == other.getT64()
			&& T65 == other.getT65()
			&& T66 == other.getT66()
			&& T67 == other.getT67()
			&& T68 == other.getT68()
			&& T69 == other.getT69()
			&& T70 == other.getT70()
			&& T71 == other.getT71()
			&& BranchSeries.equals(other.getBranchSeries())
			&& BranchType.equals(other.getBranchType())
			&& BranchType2.equals(other.getBranchType2());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("IndexCalNo") ) {
			return 0;
		}
		if( strFieldName.equals("IndexType") ) {
			return 1;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 2;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 3;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 4;
		}
		if( strFieldName.equals("StartDate") ) {
			return 5;
		}
		if( strFieldName.equals("StartEnd") ) {
			return 6;
		}
		if( strFieldName.equals("IndFYCSum") ) {
			return 7;
		}
		if( strFieldName.equals("MonAvgFYC") ) {
			return 8;
		}
		if( strFieldName.equals("IndAddFYCSum") ) {
			return 9;
		}
		if( strFieldName.equals("DirTeamFYCSum") ) {
			return 10;
		}
		if( strFieldName.equals("DRFYCSum") ) {
			return 11;
		}
		if( strFieldName.equals("DepFYCSum") ) {
			return 12;
		}
		if( strFieldName.equals("DirDepMonAvgFYC") ) {
			return 13;
		}
		if( strFieldName.equals("FYCZeroMon") ) {
			return 14;
		}
		if( strFieldName.equals("IndRate") ) {
			return 15;
		}
		if( strFieldName.equals("TeamAvgRate") ) {
			return 16;
		}
		if( strFieldName.equals("DRTeamAvgRate") ) {
			return 17;
		}
		if( strFieldName.equals("DepAvgRate") ) {
			return 18;
		}
		if( strFieldName.equals("DRDepAvgRate") ) {
			return 19;
		}
		if( strFieldName.equals("AreaRate") ) {
			return 20;
		}
		if( strFieldName.equals("IndCustSum") ) {
			return 21;
		}
		if( strFieldName.equals("MonAvgCust") ) {
			return 22;
		}
		if( strFieldName.equals("DirAddCount") ) {
			return 23;
		}
		if( strFieldName.equals("AddCount") ) {
			return 24;
		}
		if( strFieldName.equals("IncFinaMngCount") ) {
			return 25;
		}
		if( strFieldName.equals("MngAgentCount") ) {
			return 26;
		}
		if( strFieldName.equals("MngFinaCount") ) {
			return 27;
		}
		if( strFieldName.equals("DirRmdCount") ) {
			return 28;
		}
		if( strFieldName.equals("DirMngCount") ) {
			return 29;
		}
		if( strFieldName.equals("DirMngFinaCount") ) {
			return 30;
		}
		if( strFieldName.equals("TeamCount") ) {
			return 31;
		}
		if( strFieldName.equals("DRTeamCount") ) {
			return 32;
		}
		if( strFieldName.equals("RearTeamSum") ) {
			return 33;
		}
		if( strFieldName.equals("DRearDepCount") ) {
			return 34;
		}
		if( strFieldName.equals("RearDepCount") ) {
			return 35;
		}
		if( strFieldName.equals("DInRCount") ) {
			return 36;
		}
		if( strFieldName.equals("DepFinaCount") ) {
			return 37;
		}
		if( strFieldName.equals("BranComFinaCount") ) {
			return 38;
		}
		if( strFieldName.equals("RearAdmCount") ) {
			return 39;
		}
		if( strFieldName.equals("DRearAdmCount") ) {
			return 40;
		}
		if( strFieldName.equals("MonAvgLabor") ) {
			return 41;
		}
		if( strFieldName.equals("DRTeamMonLabor") ) {
			return 42;
		}
		if( strFieldName.equals("DRDepMonLabor") ) {
			return 43;
		}
		if( strFieldName.equals("InitPension") ) {
			return 44;
		}
		if( strFieldName.equals("StartCareerRwd") ) {
			return 45;
		}
		if( strFieldName.equals("ToFormalRwd") ) {
			return 46;
		}
		if( strFieldName.equals("FirstPension") ) {
			return 47;
		}
		if( strFieldName.equals("ContiuePension") ) {
			return 48;
		}
		if( strFieldName.equals("PersonBonus") ) {
			return 49;
		}
		if( strFieldName.equals("WiseBonus") ) {
			return 50;
		}
		if( strFieldName.equals("RearBonus") ) {
			return 51;
		}
		if( strFieldName.equals("AddBonus") ) {
			return 52;
		}
		if( strFieldName.equals("SpecialPension") ) {
			return 53;
		}
		if( strFieldName.equals("PosSdy") ) {
			return 54;
		}
		if( strFieldName.equals("TeamDirManaSdy") ) {
			return 55;
		}
		if( strFieldName.equals("RearedSdy") ) {
			return 56;
		}
		if( strFieldName.equals("TeamBonus") ) {
			return 57;
		}
		if( strFieldName.equals("DepDirManaSdy") ) {
			return 58;
		}
		if( strFieldName.equals("AddDepSdy") ) {
			return 59;
		}
		if( strFieldName.equals("DepBonus") ) {
			return 60;
		}
		if( strFieldName.equals("BaseWage") ) {
			return 61;
		}
		if( strFieldName.equals("DisBaseWage") ) {
			return 62;
		}
		if( strFieldName.equals("RearAreaSdy") ) {
			return 63;
		}
		if( strFieldName.equals("AddFYC") ) {
			return 64;
		}
		if( strFieldName.equals("T1") ) {
			return 65;
		}
		if( strFieldName.equals("T2") ) {
			return 66;
		}
		if( strFieldName.equals("T3") ) {
			return 67;
		}
		if( strFieldName.equals("T4") ) {
			return 68;
		}
		if( strFieldName.equals("T5") ) {
			return 69;
		}
		if( strFieldName.equals("T6") ) {
			return 70;
		}
		if( strFieldName.equals("T7") ) {
			return 71;
		}
		if( strFieldName.equals("T8") ) {
			return 72;
		}
		if( strFieldName.equals("T9") ) {
			return 73;
		}
		if( strFieldName.equals("T10") ) {
			return 74;
		}
		if( strFieldName.equals("T11") ) {
			return 75;
		}
		if( strFieldName.equals("T12") ) {
			return 76;
		}
		if( strFieldName.equals("T13") ) {
			return 77;
		}
		if( strFieldName.equals("T14") ) {
			return 78;
		}
		if( strFieldName.equals("T15") ) {
			return 79;
		}
		if( strFieldName.equals("T16") ) {
			return 80;
		}
		if( strFieldName.equals("T17") ) {
			return 81;
		}
		if( strFieldName.equals("T18") ) {
			return 82;
		}
		if( strFieldName.equals("T19") ) {
			return 83;
		}
		if( strFieldName.equals("T20") ) {
			return 84;
		}
		if( strFieldName.equals("T21") ) {
			return 85;
		}
		if( strFieldName.equals("T22") ) {
			return 86;
		}
		if( strFieldName.equals("T23") ) {
			return 87;
		}
		if( strFieldName.equals("T24") ) {
			return 88;
		}
		if( strFieldName.equals("T25") ) {
			return 89;
		}
		if( strFieldName.equals("T26") ) {
			return 90;
		}
		if( strFieldName.equals("T27") ) {
			return 91;
		}
		if( strFieldName.equals("T28") ) {
			return 92;
		}
		if( strFieldName.equals("T29") ) {
			return 93;
		}
		if( strFieldName.equals("T30") ) {
			return 94;
		}
		if( strFieldName.equals("State") ) {
			return 95;
		}
		if( strFieldName.equals("Operator") ) {
			return 96;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 97;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 98;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 99;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 100;
		}
		if( strFieldName.equals("AssessMonth") ) {
			return 101;
		}
		if( strFieldName.equals("T31") ) {
			return 102;
		}
		if( strFieldName.equals("T32") ) {
			return 103;
		}
		if( strFieldName.equals("T33") ) {
			return 104;
		}
		if( strFieldName.equals("T34") ) {
			return 105;
		}
		if( strFieldName.equals("T35") ) {
			return 106;
		}
		if( strFieldName.equals("T36") ) {
			return 107;
		}
		if( strFieldName.equals("T37") ) {
			return 108;
		}
		if( strFieldName.equals("T38") ) {
			return 109;
		}
		if( strFieldName.equals("T39") ) {
			return 110;
		}
		if( strFieldName.equals("T40") ) {
			return 111;
		}
		if( strFieldName.equals("T41") ) {
			return 112;
		}
		if( strFieldName.equals("T42") ) {
			return 113;
		}
		if( strFieldName.equals("T43") ) {
			return 114;
		}
		if( strFieldName.equals("T44") ) {
			return 115;
		}
		if( strFieldName.equals("T45") ) {
			return 116;
		}
		if( strFieldName.equals("T46") ) {
			return 117;
		}
		if( strFieldName.equals("T47") ) {
			return 118;
		}
		if( strFieldName.equals("T48") ) {
			return 119;
		}
		if( strFieldName.equals("T49") ) {
			return 120;
		}
		if( strFieldName.equals("T50") ) {
			return 121;
		}
		if( strFieldName.equals("T51") ) {
			return 122;
		}
		if( strFieldName.equals("T52") ) {
			return 123;
		}
		if( strFieldName.equals("T53") ) {
			return 124;
		}
		if( strFieldName.equals("T54") ) {
			return 125;
		}
		if( strFieldName.equals("T55") ) {
			return 126;
		}
		if( strFieldName.equals("T56") ) {
			return 127;
		}
		if( strFieldName.equals("T57") ) {
			return 128;
		}
		if( strFieldName.equals("T58") ) {
			return 129;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return 130;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return 131;
		}
		if( strFieldName.equals("T59") ) {
			return 132;
		}
		if( strFieldName.equals("T60") ) {
			return 133;
		}
		if( strFieldName.equals("T61") ) {
			return 134;
		}
		if( strFieldName.equals("T62") ) {
			return 135;
		}
		if( strFieldName.equals("T63") ) {
			return 136;
		}
		if( strFieldName.equals("T64") ) {
			return 137;
		}
		if( strFieldName.equals("T65") ) {
			return 138;
		}
		if( strFieldName.equals("T66") ) {
			return 139;
		}
		if( strFieldName.equals("T67") ) {
			return 140;
		}
		if( strFieldName.equals("T68") ) {
			return 141;
		}
		if( strFieldName.equals("T69") ) {
			return 142;
		}
		if( strFieldName.equals("T70") ) {
			return 143;
		}
		if( strFieldName.equals("T71") ) {
			return 144;
		}
		if( strFieldName.equals("BranchSeries") ) {
			return 145;
		}
		if( strFieldName.equals("BranchType") ) {
			return 146;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 147;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "IndexCalNo";
				break;
			case 1:
				strFieldName = "IndexType";
				break;
			case 2:
				strFieldName = "AgentCode";
				break;
			case 3:
				strFieldName = "AgentGroup";
				break;
			case 4:
				strFieldName = "ManageCom";
				break;
			case 5:
				strFieldName = "StartDate";
				break;
			case 6:
				strFieldName = "StartEnd";
				break;
			case 7:
				strFieldName = "IndFYCSum";
				break;
			case 8:
				strFieldName = "MonAvgFYC";
				break;
			case 9:
				strFieldName = "IndAddFYCSum";
				break;
			case 10:
				strFieldName = "DirTeamFYCSum";
				break;
			case 11:
				strFieldName = "DRFYCSum";
				break;
			case 12:
				strFieldName = "DepFYCSum";
				break;
			case 13:
				strFieldName = "DirDepMonAvgFYC";
				break;
			case 14:
				strFieldName = "FYCZeroMon";
				break;
			case 15:
				strFieldName = "IndRate";
				break;
			case 16:
				strFieldName = "TeamAvgRate";
				break;
			case 17:
				strFieldName = "DRTeamAvgRate";
				break;
			case 18:
				strFieldName = "DepAvgRate";
				break;
			case 19:
				strFieldName = "DRDepAvgRate";
				break;
			case 20:
				strFieldName = "AreaRate";
				break;
			case 21:
				strFieldName = "IndCustSum";
				break;
			case 22:
				strFieldName = "MonAvgCust";
				break;
			case 23:
				strFieldName = "DirAddCount";
				break;
			case 24:
				strFieldName = "AddCount";
				break;
			case 25:
				strFieldName = "IncFinaMngCount";
				break;
			case 26:
				strFieldName = "MngAgentCount";
				break;
			case 27:
				strFieldName = "MngFinaCount";
				break;
			case 28:
				strFieldName = "DirRmdCount";
				break;
			case 29:
				strFieldName = "DirMngCount";
				break;
			case 30:
				strFieldName = "DirMngFinaCount";
				break;
			case 31:
				strFieldName = "TeamCount";
				break;
			case 32:
				strFieldName = "DRTeamCount";
				break;
			case 33:
				strFieldName = "RearTeamSum";
				break;
			case 34:
				strFieldName = "DRearDepCount";
				break;
			case 35:
				strFieldName = "RearDepCount";
				break;
			case 36:
				strFieldName = "DInRCount";
				break;
			case 37:
				strFieldName = "DepFinaCount";
				break;
			case 38:
				strFieldName = "BranComFinaCount";
				break;
			case 39:
				strFieldName = "RearAdmCount";
				break;
			case 40:
				strFieldName = "DRearAdmCount";
				break;
			case 41:
				strFieldName = "MonAvgLabor";
				break;
			case 42:
				strFieldName = "DRTeamMonLabor";
				break;
			case 43:
				strFieldName = "DRDepMonLabor";
				break;
			case 44:
				strFieldName = "InitPension";
				break;
			case 45:
				strFieldName = "StartCareerRwd";
				break;
			case 46:
				strFieldName = "ToFormalRwd";
				break;
			case 47:
				strFieldName = "FirstPension";
				break;
			case 48:
				strFieldName = "ContiuePension";
				break;
			case 49:
				strFieldName = "PersonBonus";
				break;
			case 50:
				strFieldName = "WiseBonus";
				break;
			case 51:
				strFieldName = "RearBonus";
				break;
			case 52:
				strFieldName = "AddBonus";
				break;
			case 53:
				strFieldName = "SpecialPension";
				break;
			case 54:
				strFieldName = "PosSdy";
				break;
			case 55:
				strFieldName = "TeamDirManaSdy";
				break;
			case 56:
				strFieldName = "RearedSdy";
				break;
			case 57:
				strFieldName = "TeamBonus";
				break;
			case 58:
				strFieldName = "DepDirManaSdy";
				break;
			case 59:
				strFieldName = "AddDepSdy";
				break;
			case 60:
				strFieldName = "DepBonus";
				break;
			case 61:
				strFieldName = "BaseWage";
				break;
			case 62:
				strFieldName = "DisBaseWage";
				break;
			case 63:
				strFieldName = "RearAreaSdy";
				break;
			case 64:
				strFieldName = "AddFYC";
				break;
			case 65:
				strFieldName = "T1";
				break;
			case 66:
				strFieldName = "T2";
				break;
			case 67:
				strFieldName = "T3";
				break;
			case 68:
				strFieldName = "T4";
				break;
			case 69:
				strFieldName = "T5";
				break;
			case 70:
				strFieldName = "T6";
				break;
			case 71:
				strFieldName = "T7";
				break;
			case 72:
				strFieldName = "T8";
				break;
			case 73:
				strFieldName = "T9";
				break;
			case 74:
				strFieldName = "T10";
				break;
			case 75:
				strFieldName = "T11";
				break;
			case 76:
				strFieldName = "T12";
				break;
			case 77:
				strFieldName = "T13";
				break;
			case 78:
				strFieldName = "T14";
				break;
			case 79:
				strFieldName = "T15";
				break;
			case 80:
				strFieldName = "T16";
				break;
			case 81:
				strFieldName = "T17";
				break;
			case 82:
				strFieldName = "T18";
				break;
			case 83:
				strFieldName = "T19";
				break;
			case 84:
				strFieldName = "T20";
				break;
			case 85:
				strFieldName = "T21";
				break;
			case 86:
				strFieldName = "T22";
				break;
			case 87:
				strFieldName = "T23";
				break;
			case 88:
				strFieldName = "T24";
				break;
			case 89:
				strFieldName = "T25";
				break;
			case 90:
				strFieldName = "T26";
				break;
			case 91:
				strFieldName = "T27";
				break;
			case 92:
				strFieldName = "T28";
				break;
			case 93:
				strFieldName = "T29";
				break;
			case 94:
				strFieldName = "T30";
				break;
			case 95:
				strFieldName = "State";
				break;
			case 96:
				strFieldName = "Operator";
				break;
			case 97:
				strFieldName = "MakeDate";
				break;
			case 98:
				strFieldName = "MakeTime";
				break;
			case 99:
				strFieldName = "ModifyDate";
				break;
			case 100:
				strFieldName = "ModifyTime";
				break;
			case 101:
				strFieldName = "AssessMonth";
				break;
			case 102:
				strFieldName = "T31";
				break;
			case 103:
				strFieldName = "T32";
				break;
			case 104:
				strFieldName = "T33";
				break;
			case 105:
				strFieldName = "T34";
				break;
			case 106:
				strFieldName = "T35";
				break;
			case 107:
				strFieldName = "T36";
				break;
			case 108:
				strFieldName = "T37";
				break;
			case 109:
				strFieldName = "T38";
				break;
			case 110:
				strFieldName = "T39";
				break;
			case 111:
				strFieldName = "T40";
				break;
			case 112:
				strFieldName = "T41";
				break;
			case 113:
				strFieldName = "T42";
				break;
			case 114:
				strFieldName = "T43";
				break;
			case 115:
				strFieldName = "T44";
				break;
			case 116:
				strFieldName = "T45";
				break;
			case 117:
				strFieldName = "T46";
				break;
			case 118:
				strFieldName = "T47";
				break;
			case 119:
				strFieldName = "T48";
				break;
			case 120:
				strFieldName = "T49";
				break;
			case 121:
				strFieldName = "T50";
				break;
			case 122:
				strFieldName = "T51";
				break;
			case 123:
				strFieldName = "T52";
				break;
			case 124:
				strFieldName = "T53";
				break;
			case 125:
				strFieldName = "T54";
				break;
			case 126:
				strFieldName = "T55";
				break;
			case 127:
				strFieldName = "T56";
				break;
			case 128:
				strFieldName = "T57";
				break;
			case 129:
				strFieldName = "T58";
				break;
			case 130:
				strFieldName = "BranchAttr";
				break;
			case 131:
				strFieldName = "AgentGrade";
				break;
			case 132:
				strFieldName = "T59";
				break;
			case 133:
				strFieldName = "T60";
				break;
			case 134:
				strFieldName = "T61";
				break;
			case 135:
				strFieldName = "T62";
				break;
			case 136:
				strFieldName = "T63";
				break;
			case 137:
				strFieldName = "T64";
				break;
			case 138:
				strFieldName = "T65";
				break;
			case 139:
				strFieldName = "T66";
				break;
			case 140:
				strFieldName = "T67";
				break;
			case 141:
				strFieldName = "T68";
				break;
			case 142:
				strFieldName = "T69";
				break;
			case 143:
				strFieldName = "T70";
				break;
			case 144:
				strFieldName = "T71";
				break;
			case 145:
				strFieldName = "BranchSeries";
				break;
			case 146:
				strFieldName = "BranchType";
				break;
			case 147:
				strFieldName = "BranchType2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("IndexCalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StartEnd") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IndFYCSum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MonAvgFYC") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("IndAddFYCSum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DirTeamFYCSum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DRFYCSum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DepFYCSum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DirDepMonAvgFYC") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FYCZeroMon") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("IndRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TeamAvgRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DRTeamAvgRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DepAvgRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DRDepAvgRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AreaRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("IndCustSum") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MonAvgCust") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DirAddCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AddCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("IncFinaMngCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MngAgentCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MngFinaCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DirRmdCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DirMngCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DirMngFinaCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("TeamCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DRTeamCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RearTeamSum") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DRearDepCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RearDepCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DInRCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DepFinaCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BranComFinaCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RearAdmCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DRearAdmCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MonAvgLabor") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DRTeamMonLabor") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DRDepMonLabor") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InitPension") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StartCareerRwd") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ToFormalRwd") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("FirstPension") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ContiuePension") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PersonBonus") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("WiseBonus") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RearBonus") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AddBonus") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SpecialPension") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PosSdy") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TeamDirManaSdy") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RearedSdy") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TeamBonus") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DepDirManaSdy") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AddDepSdy") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DepBonus") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BaseWage") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DisBaseWage") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RearAreaSdy") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AddFYC") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T5") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T6") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T7") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T8") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T9") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T10") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T11") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T12") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T13") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T14") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T15") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T16") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T17") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T18") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T19") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T20") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T21") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T22") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T23") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T24") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T25") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T26") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T27") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T28") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T29") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T30") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AssessMonth") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("T31") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T32") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T33") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T34") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T35") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T36") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T37") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T38") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T39") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T40") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T41") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T42") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T43") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T44") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T45") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T46") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T47") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T48") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T49") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T50") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T51") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T52") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T53") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T54") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T55") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T56") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T57") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T58") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("T59") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T60") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T61") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T62") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T63") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T64") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T65") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T66") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T67") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T68") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T69") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T70") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T71") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BranchSeries") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_INT;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_INT;
				break;
			case 22:
				nFieldType = Schema.TYPE_INT;
				break;
			case 23:
				nFieldType = Schema.TYPE_INT;
				break;
			case 24:
				nFieldType = Schema.TYPE_INT;
				break;
			case 25:
				nFieldType = Schema.TYPE_INT;
				break;
			case 26:
				nFieldType = Schema.TYPE_INT;
				break;
			case 27:
				nFieldType = Schema.TYPE_INT;
				break;
			case 28:
				nFieldType = Schema.TYPE_INT;
				break;
			case 29:
				nFieldType = Schema.TYPE_INT;
				break;
			case 30:
				nFieldType = Schema.TYPE_INT;
				break;
			case 31:
				nFieldType = Schema.TYPE_INT;
				break;
			case 32:
				nFieldType = Schema.TYPE_INT;
				break;
			case 33:
				nFieldType = Schema.TYPE_INT;
				break;
			case 34:
				nFieldType = Schema.TYPE_INT;
				break;
			case 35:
				nFieldType = Schema.TYPE_INT;
				break;
			case 36:
				nFieldType = Schema.TYPE_INT;
				break;
			case 37:
				nFieldType = Schema.TYPE_INT;
				break;
			case 38:
				nFieldType = Schema.TYPE_INT;
				break;
			case 39:
				nFieldType = Schema.TYPE_INT;
				break;
			case 40:
				nFieldType = Schema.TYPE_INT;
				break;
			case 41:
				nFieldType = Schema.TYPE_INT;
				break;
			case 42:
				nFieldType = Schema.TYPE_INT;
				break;
			case 43:
				nFieldType = Schema.TYPE_INT;
				break;
			case 44:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 45:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 46:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 47:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 48:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 49:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 50:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 51:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 52:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 53:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 54:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 55:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 56:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 57:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 58:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 59:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 60:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 61:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 62:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 63:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 64:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 65:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 66:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 67:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 68:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 69:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 70:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 71:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 72:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 73:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 74:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 75:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 76:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 77:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 78:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 79:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 80:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 81:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 82:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 83:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 84:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 85:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 86:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 87:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 88:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 89:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 90:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 91:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 92:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 93:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 94:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 95:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 96:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 97:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 98:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 99:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 100:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 101:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 102:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 103:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 104:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 105:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 106:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 107:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 108:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 109:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 110:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 111:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 112:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 113:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 114:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 115:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 116:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 117:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 118:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 119:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 120:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 121:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 122:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 123:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 124:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 125:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 126:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 127:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 128:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 129:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 130:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 131:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 132:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 133:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 134:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 135:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 136:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 137:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 138:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 139:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 140:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 141:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 142:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 143:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 144:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 145:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 146:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 147:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
