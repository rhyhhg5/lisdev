/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHFDoctorCustormrelaDB;

/*
 * <p>ClassName: LHFDoctorCustormrelaSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-11-30
 */
public class LHFDoctorCustormrelaSchema implements Schema, Cloneable {
    // @Field
    /** 流水号 */
    private String SerialNo;
    /** 家庭医生代码 */
    private String FamilyDoctorNo;
    /** 家庭医生姓名 */
    private String FamilyDoctorName;
    /** 客户号 */
    private String CustomerNo;
    /** 客户姓名 */
    private String Name;
    /** 服务起始时间 */
    private Date FDSStart;
    /** 服务终止时间 */
    private Date FDSEnd;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHFDoctorCustormrelaSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHFDoctorCustormrelaSchema cloned = (LHFDoctorCustormrelaSchema)super.
                                            clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    public String getFamilyDoctorNo() {
        return FamilyDoctorNo;
    }

    public void setFamilyDoctorNo(String aFamilyDoctorNo) {
        FamilyDoctorNo = aFamilyDoctorNo;
    }

    public String getFamilyDoctorName() {
        return FamilyDoctorName;
    }

    public void setFamilyDoctorName(String aFamilyDoctorName) {
        FamilyDoctorName = aFamilyDoctorName;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String aName) {
        Name = aName;
    }

    public String getFDSStart() {
        if (FDSStart != null) {
            return fDate.getString(FDSStart);
        } else {
            return null;
        }
    }

    public void setFDSStart(Date aFDSStart) {
        FDSStart = aFDSStart;
    }

    public void setFDSStart(String aFDSStart) {
        if (aFDSStart != null && !aFDSStart.equals("")) {
            FDSStart = fDate.getDate(aFDSStart);
        } else {
            FDSStart = null;
        }
    }

    public String getFDSEnd() {
        if (FDSEnd != null) {
            return fDate.getString(FDSEnd);
        } else {
            return null;
        }
    }

    public void setFDSEnd(Date aFDSEnd) {
        FDSEnd = aFDSEnd;
    }

    public void setFDSEnd(String aFDSEnd) {
        if (aFDSEnd != null && !aFDSEnd.equals("")) {
            FDSEnd = fDate.getDate(aFDSEnd);
        } else {
            FDSEnd = null;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHFDoctorCustormrelaSchema 对象给 Schema 赋值
     * @param: aLHFDoctorCustormrelaSchema LHFDoctorCustormrelaSchema
     **/
    public void setSchema(LHFDoctorCustormrelaSchema
                          aLHFDoctorCustormrelaSchema) {
        this.SerialNo = aLHFDoctorCustormrelaSchema.getSerialNo();
        this.FamilyDoctorNo = aLHFDoctorCustormrelaSchema.getFamilyDoctorNo();
        this.FamilyDoctorName = aLHFDoctorCustormrelaSchema.getFamilyDoctorName();
        this.CustomerNo = aLHFDoctorCustormrelaSchema.getCustomerNo();
        this.Name = aLHFDoctorCustormrelaSchema.getName();
        this.FDSStart = fDate.getDate(aLHFDoctorCustormrelaSchema.getFDSStart());
        this.FDSEnd = fDate.getDate(aLHFDoctorCustormrelaSchema.getFDSEnd());
        this.ManageCom = aLHFDoctorCustormrelaSchema.getManageCom();
        this.Operator = aLHFDoctorCustormrelaSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHFDoctorCustormrelaSchema.getMakeDate());
        this.MakeTime = aLHFDoctorCustormrelaSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHFDoctorCustormrelaSchema.
                                        getModifyDate());
        this.ModifyTime = aLHFDoctorCustormrelaSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null) {
                this.SerialNo = null;
            } else {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("FamilyDoctorNo") == null) {
                this.FamilyDoctorNo = null;
            } else {
                this.FamilyDoctorNo = rs.getString("FamilyDoctorNo").trim();
            }

            if (rs.getString("FamilyDoctorName") == null) {
                this.FamilyDoctorName = null;
            } else {
                this.FamilyDoctorName = rs.getString("FamilyDoctorName").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("Name") == null) {
                this.Name = null;
            } else {
                this.Name = rs.getString("Name").trim();
            }

            this.FDSStart = rs.getDate("FDSStart");
            this.FDSEnd = rs.getDate("FDSEnd");
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHFDoctorCustormrela表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHFDoctorCustormrelaSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHFDoctorCustormrelaSchema getSchema() {
        LHFDoctorCustormrelaSchema aLHFDoctorCustormrelaSchema = new
                LHFDoctorCustormrelaSchema();
        aLHFDoctorCustormrelaSchema.setSchema(this);
        return aLHFDoctorCustormrelaSchema;
    }

    public LHFDoctorCustormrelaDB getDB() {
        LHFDoctorCustormrelaDB aDBOper = new LHFDoctorCustormrelaDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHFDoctorCustormrela描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FamilyDoctorNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FamilyDoctorName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Name));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(FDSStart)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(FDSEnd)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHFDoctorCustormrela>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            FamilyDoctorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                            SysConst.PACKAGESPILTER);
            FamilyDoctorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              3, SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                  SysConst.PACKAGESPILTER);
            FDSStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            FDSEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHFDoctorCustormrelaSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("FamilyDoctorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyDoctorNo));
        }
        if (FCode.equals("FamilyDoctorName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyDoctorName));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("Name")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equals("FDSStart")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getFDSStart()));
        }
        if (FCode.equals("FDSEnd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getFDSEnd()));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SerialNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(FamilyDoctorNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(FamilyDoctorName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(Name);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getFDSStart()));
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getFDSEnd()));
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNo = FValue.trim();
            } else {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("FamilyDoctorNo")) {
            if (FValue != null && !FValue.equals("")) {
                FamilyDoctorNo = FValue.trim();
            } else {
                FamilyDoctorNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("FamilyDoctorName")) {
            if (FValue != null && !FValue.equals("")) {
                FamilyDoctorName = FValue.trim();
            } else {
                FamilyDoctorName = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("Name")) {
            if (FValue != null && !FValue.equals("")) {
                Name = FValue.trim();
            } else {
                Name = null;
            }
        }
        if (FCode.equalsIgnoreCase("FDSStart")) {
            if (FValue != null && !FValue.equals("")) {
                FDSStart = fDate.getDate(FValue);
            } else {
                FDSStart = null;
            }
        }
        if (FCode.equalsIgnoreCase("FDSEnd")) {
            if (FValue != null && !FValue.equals("")) {
                FDSEnd = fDate.getDate(FValue);
            } else {
                FDSEnd = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHFDoctorCustormrelaSchema other = (LHFDoctorCustormrelaSchema)
                                           otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && FamilyDoctorNo.equals(other.getFamilyDoctorNo())
                && FamilyDoctorName.equals(other.getFamilyDoctorName())
                && CustomerNo.equals(other.getCustomerNo())
                && Name.equals(other.getName())
                && fDate.getString(FDSStart).equals(other.getFDSStart())
                && fDate.getString(FDSEnd).equals(other.getFDSEnd())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return 0;
        }
        if (strFieldName.equals("FamilyDoctorNo")) {
            return 1;
        }
        if (strFieldName.equals("FamilyDoctorName")) {
            return 2;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 3;
        }
        if (strFieldName.equals("Name")) {
            return 4;
        }
        if (strFieldName.equals("FDSStart")) {
            return 5;
        }
        if (strFieldName.equals("FDSEnd")) {
            return 6;
        }
        if (strFieldName.equals("ManageCom")) {
            return 7;
        }
        if (strFieldName.equals("Operator")) {
            return 8;
        }
        if (strFieldName.equals("MakeDate")) {
            return 9;
        }
        if (strFieldName.equals("MakeTime")) {
            return 10;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 11;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SerialNo";
            break;
        case 1:
            strFieldName = "FamilyDoctorNo";
            break;
        case 2:
            strFieldName = "FamilyDoctorName";
            break;
        case 3:
            strFieldName = "CustomerNo";
            break;
        case 4:
            strFieldName = "Name";
            break;
        case 5:
            strFieldName = "FDSStart";
            break;
        case 6:
            strFieldName = "FDSEnd";
            break;
        case 7:
            strFieldName = "ManageCom";
            break;
        case 8:
            strFieldName = "Operator";
            break;
        case 9:
            strFieldName = "MakeDate";
            break;
        case 10:
            strFieldName = "MakeTime";
            break;
        case 11:
            strFieldName = "ModifyDate";
            break;
        case 12:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FamilyDoctorNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FamilyDoctorName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FDSStart")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FDSEnd")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 6:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
