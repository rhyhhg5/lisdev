/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.WFContListDB;

/*
 * <p>ClassName: WFContListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 电子商务
 * @CreateDate：2011-11-03
 */
public class WFContListSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 报文发送日期 */
	private String SendDate;
	/** 报文发送时间 */
	private String SendTime;
	/** 网点 */
	private String BranchCode;
	/** 操作员 */
	private String SendOperator;
	/** 报文类型 */
	private String MessageType;
	/** 保险卡信息处理方式 */
	private String CardDealType;
	/** 保险卡号 */
	private String CardNo;
	/** 保险卡密码 */
	private String Password;
	/** 保险卡类型- 单证编码 */
	private String CertifyCode;
	/** 保险卡名称 */
	private String CertifyName;
	/** 套餐编码 */
	private String RiskCode;
	/** 激活日期 */
	private String ActiveDate;
	/** 管理机构 */
	private String ManageCom;
	/** 销售渠道 */
	private String SaleChnl;
	/** 中介机构 */
	private String AgentCom;
	/** 业务员代码 */
	private String AgentCode;
	/** 业务员姓名 */
	private String AgentName;
	/** 缴费方式 */
	private String PayMode;
	/** 缴费频次 */
	private String PayIntv;
	/** 缴费年期 */
	private int PayYear;
	/** 缴费年期标志 */
	private String PayYearFlag;
	/** 生效日期 */
	private String CvaliDate;
	/** 生效时间 */
	private String CvaliDateTime;
	/** 保障期间 */
	private int InsuYear;
	/** 保障期间标志 */
	private String InsuYearFlag;
	/** 保险卡保费 */
	private double Prem;
	/** 保险卡保额 */
	private double Amnt;
	/** 保险卡险种档次 */
	private int Mult;
	/** 份数 */
	private int Copys;
	/** 乘意险票号 */
	private String TicketNo;
	/** 班次 */
	private String TeamNo;
	/** 座位号 */
	private String SeatNo;
	/** 起始站 */
	private String From;
	/** 终点站 */
	private String To;
	/** 说明(备注) */
	private String Remark;
	/** 承保区域 */
	private String CardArea;
	/** 投保人号码 */
	private String AppntNo;
	/** 备用字段1 */
	private String Bak1;
	/** 备用字段2 */
	private String Bak2;
	/** 备用字段3 */
	private String Bak3;
	/** 备用字段4 */
	private String Bak4;
	/** 备用字段5 */
	private String Bak5;
	/** 备用字段6 */
	private String Bak6;
	/** 备用字段7 */
	private String Bak7;
	/** 备用字段8 */
	private String Bak8;
	/** 备用字段9 */
	private String Bak9;
	/** 备用字段10 */
	private String Bak10;

	public static final int FIELDNUM = 48;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public WFContListSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BatchNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		WFContListSchema cloned = (WFContListSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getSendDate()
	{
		return SendDate;
	}
	public void setSendDate(String aSendDate)
	{
		SendDate = aSendDate;
	}
	public String getSendTime()
	{
		return SendTime;
	}
	public void setSendTime(String aSendTime)
	{
		SendTime = aSendTime;
	}
	public String getBranchCode()
	{
		return BranchCode;
	}
	public void setBranchCode(String aBranchCode)
	{
		BranchCode = aBranchCode;
	}
	public String getSendOperator()
	{
		return SendOperator;
	}
	public void setSendOperator(String aSendOperator)
	{
		SendOperator = aSendOperator;
	}
	public String getMessageType()
	{
		return MessageType;
	}
	public void setMessageType(String aMessageType)
	{
		MessageType = aMessageType;
	}
	public String getCardDealType()
	{
		return CardDealType;
	}
	public void setCardDealType(String aCardDealType)
	{
		CardDealType = aCardDealType;
	}
	public String getCardNo()
	{
		return CardNo;
	}
	public void setCardNo(String aCardNo)
	{
		CardNo = aCardNo;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String aPassword)
	{
		Password = aPassword;
	}
	public String getCertifyCode()
	{
		return CertifyCode;
	}
	public void setCertifyCode(String aCertifyCode)
	{
		CertifyCode = aCertifyCode;
	}
	public String getCertifyName()
	{
		return CertifyName;
	}
	public void setCertifyName(String aCertifyName)
	{
		CertifyName = aCertifyName;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getActiveDate()
	{
		return ActiveDate;
	}
	public void setActiveDate(String aActiveDate)
	{
		ActiveDate = aActiveDate;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentName()
	{
		return AgentName;
	}
	public void setAgentName(String aAgentName)
	{
		AgentName = aAgentName;
	}
	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public int getPayYear()
	{
		return PayYear;
	}
	public void setPayYear(int aPayYear)
	{
		PayYear = aPayYear;
	}
	public void setPayYear(String aPayYear)
	{
		if (aPayYear != null && !aPayYear.equals(""))
		{
			Integer tInteger = new Integer(aPayYear);
			int i = tInteger.intValue();
			PayYear = i;
		}
	}

	public String getPayYearFlag()
	{
		return PayYearFlag;
	}
	public void setPayYearFlag(String aPayYearFlag)
	{
		PayYearFlag = aPayYearFlag;
	}
	public String getCvaliDate()
	{
		return CvaliDate;
	}
	public void setCvaliDate(String aCvaliDate)
	{
		CvaliDate = aCvaliDate;
	}
	public String getCvaliDateTime()
	{
		return CvaliDateTime;
	}
	public void setCvaliDateTime(String aCvaliDateTime)
	{
		CvaliDateTime = aCvaliDateTime;
	}
	public int getInsuYear()
	{
		return InsuYear;
	}
	public void setInsuYear(int aInsuYear)
	{
		InsuYear = aInsuYear;
	}
	public void setInsuYear(String aInsuYear)
	{
		if (aInsuYear != null && !aInsuYear.equals(""))
		{
			Integer tInteger = new Integer(aInsuYear);
			int i = tInteger.intValue();
			InsuYear = i;
		}
	}

	public String getInsuYearFlag()
	{
		return InsuYearFlag;
	}
	public void setInsuYearFlag(String aInsuYearFlag)
	{
		InsuYearFlag = aInsuYearFlag;
	}
	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
		Prem = Arith.round(aPrem,2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d,2);
		}
	}

	public double getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(double aAmnt)
	{
		Amnt = Arith.round(aAmnt,2);
	}
	public void setAmnt(String aAmnt)
	{
		if (aAmnt != null && !aAmnt.equals(""))
		{
			Double tDouble = new Double(aAmnt);
			double d = tDouble.doubleValue();
                Amnt = Arith.round(d,2);
		}
	}

	public int getMult()
	{
		return Mult;
	}
	public void setMult(int aMult)
	{
		Mult = aMult;
	}
	public void setMult(String aMult)
	{
		if (aMult != null && !aMult.equals(""))
		{
			Integer tInteger = new Integer(aMult);
			int i = tInteger.intValue();
			Mult = i;
		}
	}

	public int getCopys()
	{
		return Copys;
	}
	public void setCopys(int aCopys)
	{
		Copys = aCopys;
	}
	public void setCopys(String aCopys)
	{
		if (aCopys != null && !aCopys.equals(""))
		{
			Integer tInteger = new Integer(aCopys);
			int i = tInteger.intValue();
			Copys = i;
		}
	}

	public String getTicketNo()
	{
		return TicketNo;
	}
	public void setTicketNo(String aTicketNo)
	{
		TicketNo = aTicketNo;
	}
	public String getTeamNo()
	{
		return TeamNo;
	}
	public void setTeamNo(String aTeamNo)
	{
		TeamNo = aTeamNo;
	}
	public String getSeatNo()
	{
		return SeatNo;
	}
	public void setSeatNo(String aSeatNo)
	{
		SeatNo = aSeatNo;
	}
	public String getFrom()
	{
		return From;
	}
	public void setFrom(String aFrom)
	{
		From = aFrom;
	}
	public String getTo()
	{
		return To;
	}
	public void setTo(String aTo)
	{
		To = aTo;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getCardArea()
	{
		return CardArea;
	}
	public void setCardArea(String aCardArea)
	{
		CardArea = aCardArea;
	}
	public String getAppntNo()
	{
		return AppntNo;
	}
	public void setAppntNo(String aAppntNo)
	{
		AppntNo = aAppntNo;
	}
	public String getBak1()
	{
		return Bak1;
	}
	public void setBak1(String aBak1)
	{
		Bak1 = aBak1;
	}
	public String getBak2()
	{
		return Bak2;
	}
	public void setBak2(String aBak2)
	{
		Bak2 = aBak2;
	}
	public String getBak3()
	{
		return Bak3;
	}
	public void setBak3(String aBak3)
	{
		Bak3 = aBak3;
	}
	public String getBak4()
	{
		return Bak4;
	}
	public void setBak4(String aBak4)
	{
		Bak4 = aBak4;
	}
	public String getBak5()
	{
		return Bak5;
	}
	public void setBak5(String aBak5)
	{
		Bak5 = aBak5;
	}
	public String getBak6()
	{
		return Bak6;
	}
	public void setBak6(String aBak6)
	{
		Bak6 = aBak6;
	}
	public String getBak7()
	{
		return Bak7;
	}
	public void setBak7(String aBak7)
	{
		Bak7 = aBak7;
	}
	public String getBak8()
	{
		return Bak8;
	}
	public void setBak8(String aBak8)
	{
		Bak8 = aBak8;
	}
	public String getBak9()
	{
		return Bak9;
	}
	public void setBak9(String aBak9)
	{
		Bak9 = aBak9;
	}
	public String getBak10()
	{
		return Bak10;
	}
	public void setBak10(String aBak10)
	{
		Bak10 = aBak10;
	}

	/**
	* 使用另外一个 WFContListSchema 对象给 Schema 赋值
	* @param: aWFContListSchema WFContListSchema
	**/
	public void setSchema(WFContListSchema aWFContListSchema)
	{
		this.BatchNo = aWFContListSchema.getBatchNo();
		this.SendDate = aWFContListSchema.getSendDate();
		this.SendTime = aWFContListSchema.getSendTime();
		this.BranchCode = aWFContListSchema.getBranchCode();
		this.SendOperator = aWFContListSchema.getSendOperator();
		this.MessageType = aWFContListSchema.getMessageType();
		this.CardDealType = aWFContListSchema.getCardDealType();
		this.CardNo = aWFContListSchema.getCardNo();
		this.Password = aWFContListSchema.getPassword();
		this.CertifyCode = aWFContListSchema.getCertifyCode();
		this.CertifyName = aWFContListSchema.getCertifyName();
		this.RiskCode = aWFContListSchema.getRiskCode();
		this.ActiveDate = aWFContListSchema.getActiveDate();
		this.ManageCom = aWFContListSchema.getManageCom();
		this.SaleChnl = aWFContListSchema.getSaleChnl();
		this.AgentCom = aWFContListSchema.getAgentCom();
		this.AgentCode = aWFContListSchema.getAgentCode();
		this.AgentName = aWFContListSchema.getAgentName();
		this.PayMode = aWFContListSchema.getPayMode();
		this.PayIntv = aWFContListSchema.getPayIntv();
		this.PayYear = aWFContListSchema.getPayYear();
		this.PayYearFlag = aWFContListSchema.getPayYearFlag();
		this.CvaliDate = aWFContListSchema.getCvaliDate();
		this.CvaliDateTime = aWFContListSchema.getCvaliDateTime();
		this.InsuYear = aWFContListSchema.getInsuYear();
		this.InsuYearFlag = aWFContListSchema.getInsuYearFlag();
		this.Prem = aWFContListSchema.getPrem();
		this.Amnt = aWFContListSchema.getAmnt();
		this.Mult = aWFContListSchema.getMult();
		this.Copys = aWFContListSchema.getCopys();
		this.TicketNo = aWFContListSchema.getTicketNo();
		this.TeamNo = aWFContListSchema.getTeamNo();
		this.SeatNo = aWFContListSchema.getSeatNo();
		this.From = aWFContListSchema.getFrom();
		this.To = aWFContListSchema.getTo();
		this.Remark = aWFContListSchema.getRemark();
		this.CardArea = aWFContListSchema.getCardArea();
		this.AppntNo = aWFContListSchema.getAppntNo();
		this.Bak1 = aWFContListSchema.getBak1();
		this.Bak2 = aWFContListSchema.getBak2();
		this.Bak3 = aWFContListSchema.getBak3();
		this.Bak4 = aWFContListSchema.getBak4();
		this.Bak5 = aWFContListSchema.getBak5();
		this.Bak6 = aWFContListSchema.getBak6();
		this.Bak7 = aWFContListSchema.getBak7();
		this.Bak8 = aWFContListSchema.getBak8();
		this.Bak9 = aWFContListSchema.getBak9();
		this.Bak10 = aWFContListSchema.getBak10();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("SendDate") == null )
				this.SendDate = null;
			else
				this.SendDate = rs.getString("SendDate").trim();

			if( rs.getString("SendTime") == null )
				this.SendTime = null;
			else
				this.SendTime = rs.getString("SendTime").trim();

			if( rs.getString("BranchCode") == null )
				this.BranchCode = null;
			else
				this.BranchCode = rs.getString("BranchCode").trim();

			if( rs.getString("SendOperator") == null )
				this.SendOperator = null;
			else
				this.SendOperator = rs.getString("SendOperator").trim();

			if( rs.getString("MessageType") == null )
				this.MessageType = null;
			else
				this.MessageType = rs.getString("MessageType").trim();

			if( rs.getString("CardDealType") == null )
				this.CardDealType = null;
			else
				this.CardDealType = rs.getString("CardDealType").trim();

			if( rs.getString("CardNo") == null )
				this.CardNo = null;
			else
				this.CardNo = rs.getString("CardNo").trim();

			if( rs.getString("Password") == null )
				this.Password = null;
			else
				this.Password = rs.getString("Password").trim();

			if( rs.getString("CertifyCode") == null )
				this.CertifyCode = null;
			else
				this.CertifyCode = rs.getString("CertifyCode").trim();

			if( rs.getString("CertifyName") == null )
				this.CertifyName = null;
			else
				this.CertifyName = rs.getString("CertifyName").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("ActiveDate") == null )
				this.ActiveDate = null;
			else
				this.ActiveDate = rs.getString("ActiveDate").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentName") == null )
				this.AgentName = null;
			else
				this.AgentName = rs.getString("AgentName").trim();

			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("PayIntv") == null )
				this.PayIntv = null;
			else
				this.PayIntv = rs.getString("PayIntv").trim();

			this.PayYear = rs.getInt("PayYear");
			if( rs.getString("PayYearFlag") == null )
				this.PayYearFlag = null;
			else
				this.PayYearFlag = rs.getString("PayYearFlag").trim();

			if( rs.getString("CvaliDate") == null )
				this.CvaliDate = null;
			else
				this.CvaliDate = rs.getString("CvaliDate").trim();

			if( rs.getString("CvaliDateTime") == null )
				this.CvaliDateTime = null;
			else
				this.CvaliDateTime = rs.getString("CvaliDateTime").trim();

			this.InsuYear = rs.getInt("InsuYear");
			if( rs.getString("InsuYearFlag") == null )
				this.InsuYearFlag = null;
			else
				this.InsuYearFlag = rs.getString("InsuYearFlag").trim();

			this.Prem = rs.getDouble("Prem");
			this.Amnt = rs.getDouble("Amnt");
			this.Mult = rs.getInt("Mult");
			this.Copys = rs.getInt("Copys");
			if( rs.getString("TicketNo") == null )
				this.TicketNo = null;
			else
				this.TicketNo = rs.getString("TicketNo").trim();

			if( rs.getString("TeamNo") == null )
				this.TeamNo = null;
			else
				this.TeamNo = rs.getString("TeamNo").trim();

			if( rs.getString("SeatNo") == null )
				this.SeatNo = null;
			else
				this.SeatNo = rs.getString("SeatNo").trim();

			if( rs.getString("From") == null )
				this.From = null;
			else
				this.From = rs.getString("From").trim();

			if( rs.getString("To") == null )
				this.To = null;
			else
				this.To = rs.getString("To").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("CardArea") == null )
				this.CardArea = null;
			else
				this.CardArea = rs.getString("CardArea").trim();

			if( rs.getString("AppntNo") == null )
				this.AppntNo = null;
			else
				this.AppntNo = rs.getString("AppntNo").trim();

			if( rs.getString("Bak1") == null )
				this.Bak1 = null;
			else
				this.Bak1 = rs.getString("Bak1").trim();

			if( rs.getString("Bak2") == null )
				this.Bak2 = null;
			else
				this.Bak2 = rs.getString("Bak2").trim();

			if( rs.getString("Bak3") == null )
				this.Bak3 = null;
			else
				this.Bak3 = rs.getString("Bak3").trim();

			if( rs.getString("Bak4") == null )
				this.Bak4 = null;
			else
				this.Bak4 = rs.getString("Bak4").trim();

			if( rs.getString("Bak5") == null )
				this.Bak5 = null;
			else
				this.Bak5 = rs.getString("Bak5").trim();

			if( rs.getString("Bak6") == null )
				this.Bak6 = null;
			else
				this.Bak6 = rs.getString("Bak6").trim();

			if( rs.getString("Bak7") == null )
				this.Bak7 = null;
			else
				this.Bak7 = rs.getString("Bak7").trim();

			if( rs.getString("Bak8") == null )
				this.Bak8 = null;
			else
				this.Bak8 = rs.getString("Bak8").trim();

			if( rs.getString("Bak9") == null )
				this.Bak9 = null;
			else
				this.Bak9 = rs.getString("Bak9").trim();

			if( rs.getString("Bak10") == null )
				this.Bak10 = null;
			else
				this.Bak10 = rs.getString("Bak10").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的WFContList表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "WFContListSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public WFContListSchema getSchema()
	{
		WFContListSchema aWFContListSchema = new WFContListSchema();
		aWFContListSchema.setSchema(this);
		return aWFContListSchema;
	}

	public WFContListDB getDB()
	{
		WFContListDB aDBOper = new WFContListDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpWFContList描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MessageType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardDealType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActiveDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayIntv)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CvaliDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CvaliDateTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuYearFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Mult));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Copys));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TicketNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TeamNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SeatNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(From)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(To)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardArea)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak7)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak8)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak10));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpWFContList>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SendDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SendTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BranchCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SendOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			MessageType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CardDealType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CertifyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ActiveDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			PayIntv = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PayYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).intValue();
			PayYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			CvaliDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			CvaliDateTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			InsuYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).intValue();
			InsuYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			Mult= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).intValue();
			Copys= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).intValue();
			TicketNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			TeamNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			SeatNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			From = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			To = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			CardArea = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			Bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			Bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			Bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			Bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			Bak5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			Bak6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			Bak7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			Bak8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			Bak9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			Bak10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "WFContListSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("SendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendDate));
		}
		if (FCode.equals("SendTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendTime));
		}
		if (FCode.equals("BranchCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchCode));
		}
		if (FCode.equals("SendOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendOperator));
		}
		if (FCode.equals("MessageType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MessageType));
		}
		if (FCode.equals("CardDealType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardDealType));
		}
		if (FCode.equals("CardNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardNo));
		}
		if (FCode.equals("Password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
		}
		if (FCode.equals("CertifyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
		}
		if (FCode.equals("CertifyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyName));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("ActiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActiveDate));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("PayYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayYear));
		}
		if (FCode.equals("PayYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayYearFlag));
		}
		if (FCode.equals("CvaliDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CvaliDate));
		}
		if (FCode.equals("CvaliDateTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CvaliDateTime));
		}
		if (FCode.equals("InsuYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYear));
		}
		if (FCode.equals("InsuYearFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYearFlag));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("Mult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
		}
		if (FCode.equals("Copys"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Copys));
		}
		if (FCode.equals("TicketNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TicketNo));
		}
		if (FCode.equals("TeamNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TeamNo));
		}
		if (FCode.equals("SeatNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SeatNo));
		}
		if (FCode.equals("From"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(From));
		}
		if (FCode.equals("To"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(To));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("CardArea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardArea));
		}
		if (FCode.equals("AppntNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
		}
		if (FCode.equals("Bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
		}
		if (FCode.equals("Bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
		}
		if (FCode.equals("Bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak3));
		}
		if (FCode.equals("Bak4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak4));
		}
		if (FCode.equals("Bak5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak5));
		}
		if (FCode.equals("Bak6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak6));
		}
		if (FCode.equals("Bak7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak7));
		}
		if (FCode.equals("Bak8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak8));
		}
		if (FCode.equals("Bak9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak9));
		}
		if (FCode.equals("Bak10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak10));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SendDate);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SendTime);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BranchCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SendOperator);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(MessageType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CardDealType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CardNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Password);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CertifyCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(CertifyName);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ActiveDate);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(AgentName);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(PayIntv);
				break;
			case 20:
				strFieldValue = String.valueOf(PayYear);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(PayYearFlag);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(CvaliDate);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(CvaliDateTime);
				break;
			case 24:
				strFieldValue = String.valueOf(InsuYear);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(InsuYearFlag);
				break;
			case 26:
				strFieldValue = String.valueOf(Prem);
				break;
			case 27:
				strFieldValue = String.valueOf(Amnt);
				break;
			case 28:
				strFieldValue = String.valueOf(Mult);
				break;
			case 29:
				strFieldValue = String.valueOf(Copys);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(TicketNo);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(TeamNo);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(SeatNo);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(From);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(To);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(CardArea);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(AppntNo);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(Bak1);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(Bak2);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(Bak3);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(Bak4);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(Bak5);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(Bak6);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(Bak7);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(Bak8);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(Bak9);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(Bak10);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("SendDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendDate = FValue.trim();
			}
			else
				SendDate = null;
		}
		if (FCode.equalsIgnoreCase("SendTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendTime = FValue.trim();
			}
			else
				SendTime = null;
		}
		if (FCode.equalsIgnoreCase("BranchCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchCode = FValue.trim();
			}
			else
				BranchCode = null;
		}
		if (FCode.equalsIgnoreCase("SendOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendOperator = FValue.trim();
			}
			else
				SendOperator = null;
		}
		if (FCode.equalsIgnoreCase("MessageType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MessageType = FValue.trim();
			}
			else
				MessageType = null;
		}
		if (FCode.equalsIgnoreCase("CardDealType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardDealType = FValue.trim();
			}
			else
				CardDealType = null;
		}
		if (FCode.equalsIgnoreCase("CardNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardNo = FValue.trim();
			}
			else
				CardNo = null;
		}
		if (FCode.equalsIgnoreCase("Password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Password = FValue.trim();
			}
			else
				Password = null;
		}
		if (FCode.equalsIgnoreCase("CertifyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyCode = FValue.trim();
			}
			else
				CertifyCode = null;
		}
		if (FCode.equalsIgnoreCase("CertifyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyName = FValue.trim();
			}
			else
				CertifyName = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("ActiveDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActiveDate = FValue.trim();
			}
			else
				ActiveDate = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentName = FValue.trim();
			}
			else
				AgentName = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayIntv = FValue.trim();
			}
			else
				PayIntv = null;
		}
		if (FCode.equalsIgnoreCase("PayYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayYearFlag = FValue.trim();
			}
			else
				PayYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("CvaliDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CvaliDate = FValue.trim();
			}
			else
				CvaliDate = null;
		}
		if (FCode.equalsIgnoreCase("CvaliDateTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CvaliDateTime = FValue.trim();
			}
			else
				CvaliDateTime = null;
		}
		if (FCode.equalsIgnoreCase("InsuYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("InsuYearFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuYearFlag = FValue.trim();
			}
			else
				InsuYearFlag = null;
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Amnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("Mult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Mult = i;
			}
		}
		if (FCode.equalsIgnoreCase("Copys"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Copys = i;
			}
		}
		if (FCode.equalsIgnoreCase("TicketNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TicketNo = FValue.trim();
			}
			else
				TicketNo = null;
		}
		if (FCode.equalsIgnoreCase("TeamNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TeamNo = FValue.trim();
			}
			else
				TeamNo = null;
		}
		if (FCode.equalsIgnoreCase("SeatNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SeatNo = FValue.trim();
			}
			else
				SeatNo = null;
		}
		if (FCode.equalsIgnoreCase("From"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				From = FValue.trim();
			}
			else
				From = null;
		}
		if (FCode.equalsIgnoreCase("To"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				To = FValue.trim();
			}
			else
				To = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("CardArea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardArea = FValue.trim();
			}
			else
				CardArea = null;
		}
		if (FCode.equalsIgnoreCase("AppntNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntNo = FValue.trim();
			}
			else
				AppntNo = null;
		}
		if (FCode.equalsIgnoreCase("Bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak1 = FValue.trim();
			}
			else
				Bak1 = null;
		}
		if (FCode.equalsIgnoreCase("Bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak2 = FValue.trim();
			}
			else
				Bak2 = null;
		}
		if (FCode.equalsIgnoreCase("Bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak3 = FValue.trim();
			}
			else
				Bak3 = null;
		}
		if (FCode.equalsIgnoreCase("Bak4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak4 = FValue.trim();
			}
			else
				Bak4 = null;
		}
		if (FCode.equalsIgnoreCase("Bak5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak5 = FValue.trim();
			}
			else
				Bak5 = null;
		}
		if (FCode.equalsIgnoreCase("Bak6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak6 = FValue.trim();
			}
			else
				Bak6 = null;
		}
		if (FCode.equalsIgnoreCase("Bak7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak7 = FValue.trim();
			}
			else
				Bak7 = null;
		}
		if (FCode.equalsIgnoreCase("Bak8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak8 = FValue.trim();
			}
			else
				Bak8 = null;
		}
		if (FCode.equalsIgnoreCase("Bak9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak9 = FValue.trim();
			}
			else
				Bak9 = null;
		}
		if (FCode.equalsIgnoreCase("Bak10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bak10 = FValue.trim();
			}
			else
				Bak10 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		WFContListSchema other = (WFContListSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (SendDate == null ? other.getSendDate() == null : SendDate.equals(other.getSendDate()))
			&& (SendTime == null ? other.getSendTime() == null : SendTime.equals(other.getSendTime()))
			&& (BranchCode == null ? other.getBranchCode() == null : BranchCode.equals(other.getBranchCode()))
			&& (SendOperator == null ? other.getSendOperator() == null : SendOperator.equals(other.getSendOperator()))
			&& (MessageType == null ? other.getMessageType() == null : MessageType.equals(other.getMessageType()))
			&& (CardDealType == null ? other.getCardDealType() == null : CardDealType.equals(other.getCardDealType()))
			&& (CardNo == null ? other.getCardNo() == null : CardNo.equals(other.getCardNo()))
			&& (Password == null ? other.getPassword() == null : Password.equals(other.getPassword()))
			&& (CertifyCode == null ? other.getCertifyCode() == null : CertifyCode.equals(other.getCertifyCode()))
			&& (CertifyName == null ? other.getCertifyName() == null : CertifyName.equals(other.getCertifyName()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (ActiveDate == null ? other.getActiveDate() == null : ActiveDate.equals(other.getActiveDate()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentName == null ? other.getAgentName() == null : AgentName.equals(other.getAgentName()))
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (PayIntv == null ? other.getPayIntv() == null : PayIntv.equals(other.getPayIntv()))
			&& PayYear == other.getPayYear()
			&& (PayYearFlag == null ? other.getPayYearFlag() == null : PayYearFlag.equals(other.getPayYearFlag()))
			&& (CvaliDate == null ? other.getCvaliDate() == null : CvaliDate.equals(other.getCvaliDate()))
			&& (CvaliDateTime == null ? other.getCvaliDateTime() == null : CvaliDateTime.equals(other.getCvaliDateTime()))
			&& InsuYear == other.getInsuYear()
			&& (InsuYearFlag == null ? other.getInsuYearFlag() == null : InsuYearFlag.equals(other.getInsuYearFlag()))
			&& Prem == other.getPrem()
			&& Amnt == other.getAmnt()
			&& Mult == other.getMult()
			&& Copys == other.getCopys()
			&& (TicketNo == null ? other.getTicketNo() == null : TicketNo.equals(other.getTicketNo()))
			&& (TeamNo == null ? other.getTeamNo() == null : TeamNo.equals(other.getTeamNo()))
			&& (SeatNo == null ? other.getSeatNo() == null : SeatNo.equals(other.getSeatNo()))
			&& (From == null ? other.getFrom() == null : From.equals(other.getFrom()))
			&& (To == null ? other.getTo() == null : To.equals(other.getTo()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (CardArea == null ? other.getCardArea() == null : CardArea.equals(other.getCardArea()))
			&& (AppntNo == null ? other.getAppntNo() == null : AppntNo.equals(other.getAppntNo()))
			&& (Bak1 == null ? other.getBak1() == null : Bak1.equals(other.getBak1()))
			&& (Bak2 == null ? other.getBak2() == null : Bak2.equals(other.getBak2()))
			&& (Bak3 == null ? other.getBak3() == null : Bak3.equals(other.getBak3()))
			&& (Bak4 == null ? other.getBak4() == null : Bak4.equals(other.getBak4()))
			&& (Bak5 == null ? other.getBak5() == null : Bak5.equals(other.getBak5()))
			&& (Bak6 == null ? other.getBak6() == null : Bak6.equals(other.getBak6()))
			&& (Bak7 == null ? other.getBak7() == null : Bak7.equals(other.getBak7()))
			&& (Bak8 == null ? other.getBak8() == null : Bak8.equals(other.getBak8()))
			&& (Bak9 == null ? other.getBak9() == null : Bak9.equals(other.getBak9()))
			&& (Bak10 == null ? other.getBak10() == null : Bak10.equals(other.getBak10()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("SendDate") ) {
			return 1;
		}
		if( strFieldName.equals("SendTime") ) {
			return 2;
		}
		if( strFieldName.equals("BranchCode") ) {
			return 3;
		}
		if( strFieldName.equals("SendOperator") ) {
			return 4;
		}
		if( strFieldName.equals("MessageType") ) {
			return 5;
		}
		if( strFieldName.equals("CardDealType") ) {
			return 6;
		}
		if( strFieldName.equals("CardNo") ) {
			return 7;
		}
		if( strFieldName.equals("Password") ) {
			return 8;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return 9;
		}
		if( strFieldName.equals("CertifyName") ) {
			return 10;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 11;
		}
		if( strFieldName.equals("ActiveDate") ) {
			return 12;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 13;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 14;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 15;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 16;
		}
		if( strFieldName.equals("AgentName") ) {
			return 17;
		}
		if( strFieldName.equals("PayMode") ) {
			return 18;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 19;
		}
		if( strFieldName.equals("PayYear") ) {
			return 20;
		}
		if( strFieldName.equals("PayYearFlag") ) {
			return 21;
		}
		if( strFieldName.equals("CvaliDate") ) {
			return 22;
		}
		if( strFieldName.equals("CvaliDateTime") ) {
			return 23;
		}
		if( strFieldName.equals("InsuYear") ) {
			return 24;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return 25;
		}
		if( strFieldName.equals("Prem") ) {
			return 26;
		}
		if( strFieldName.equals("Amnt") ) {
			return 27;
		}
		if( strFieldName.equals("Mult") ) {
			return 28;
		}
		if( strFieldName.equals("Copys") ) {
			return 29;
		}
		if( strFieldName.equals("TicketNo") ) {
			return 30;
		}
		if( strFieldName.equals("TeamNo") ) {
			return 31;
		}
		if( strFieldName.equals("SeatNo") ) {
			return 32;
		}
		if( strFieldName.equals("From") ) {
			return 33;
		}
		if( strFieldName.equals("To") ) {
			return 34;
		}
		if( strFieldName.equals("Remark") ) {
			return 35;
		}
		if( strFieldName.equals("CardArea") ) {
			return 36;
		}
		if( strFieldName.equals("AppntNo") ) {
			return 37;
		}
		if( strFieldName.equals("Bak1") ) {
			return 38;
		}
		if( strFieldName.equals("Bak2") ) {
			return 39;
		}
		if( strFieldName.equals("Bak3") ) {
			return 40;
		}
		if( strFieldName.equals("Bak4") ) {
			return 41;
		}
		if( strFieldName.equals("Bak5") ) {
			return 42;
		}
		if( strFieldName.equals("Bak6") ) {
			return 43;
		}
		if( strFieldName.equals("Bak7") ) {
			return 44;
		}
		if( strFieldName.equals("Bak8") ) {
			return 45;
		}
		if( strFieldName.equals("Bak9") ) {
			return 46;
		}
		if( strFieldName.equals("Bak10") ) {
			return 47;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "SendDate";
				break;
			case 2:
				strFieldName = "SendTime";
				break;
			case 3:
				strFieldName = "BranchCode";
				break;
			case 4:
				strFieldName = "SendOperator";
				break;
			case 5:
				strFieldName = "MessageType";
				break;
			case 6:
				strFieldName = "CardDealType";
				break;
			case 7:
				strFieldName = "CardNo";
				break;
			case 8:
				strFieldName = "Password";
				break;
			case 9:
				strFieldName = "CertifyCode";
				break;
			case 10:
				strFieldName = "CertifyName";
				break;
			case 11:
				strFieldName = "RiskCode";
				break;
			case 12:
				strFieldName = "ActiveDate";
				break;
			case 13:
				strFieldName = "ManageCom";
				break;
			case 14:
				strFieldName = "SaleChnl";
				break;
			case 15:
				strFieldName = "AgentCom";
				break;
			case 16:
				strFieldName = "AgentCode";
				break;
			case 17:
				strFieldName = "AgentName";
				break;
			case 18:
				strFieldName = "PayMode";
				break;
			case 19:
				strFieldName = "PayIntv";
				break;
			case 20:
				strFieldName = "PayYear";
				break;
			case 21:
				strFieldName = "PayYearFlag";
				break;
			case 22:
				strFieldName = "CvaliDate";
				break;
			case 23:
				strFieldName = "CvaliDateTime";
				break;
			case 24:
				strFieldName = "InsuYear";
				break;
			case 25:
				strFieldName = "InsuYearFlag";
				break;
			case 26:
				strFieldName = "Prem";
				break;
			case 27:
				strFieldName = "Amnt";
				break;
			case 28:
				strFieldName = "Mult";
				break;
			case 29:
				strFieldName = "Copys";
				break;
			case 30:
				strFieldName = "TicketNo";
				break;
			case 31:
				strFieldName = "TeamNo";
				break;
			case 32:
				strFieldName = "SeatNo";
				break;
			case 33:
				strFieldName = "From";
				break;
			case 34:
				strFieldName = "To";
				break;
			case 35:
				strFieldName = "Remark";
				break;
			case 36:
				strFieldName = "CardArea";
				break;
			case 37:
				strFieldName = "AppntNo";
				break;
			case 38:
				strFieldName = "Bak1";
				break;
			case 39:
				strFieldName = "Bak2";
				break;
			case 40:
				strFieldName = "Bak3";
				break;
			case 41:
				strFieldName = "Bak4";
				break;
			case 42:
				strFieldName = "Bak5";
				break;
			case 43:
				strFieldName = "Bak6";
				break;
			case 44:
				strFieldName = "Bak7";
				break;
			case 45:
				strFieldName = "Bak8";
				break;
			case 46:
				strFieldName = "Bak9";
				break;
			case 47:
				strFieldName = "Bak10";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MessageType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardDealType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActiveDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CvaliDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CvaliDateTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("InsuYearFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Mult") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Copys") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("TicketNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TeamNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SeatNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("From") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("To") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardArea") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak8") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bak10") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_INT;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_INT;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_INT;
				break;
			case 29:
				nFieldType = Schema.TYPE_INT;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
