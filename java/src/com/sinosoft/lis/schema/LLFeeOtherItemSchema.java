/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLFeeOtherItemDB;

/*
 * <p>ClassName: LLFeeOtherItemSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-05-20
 */
public class LLFeeOtherItemSchema implements Schema, Cloneable
{
	// @Field
	/** 序号 */
	private String CDSerialNo;
	/** 账单序号 */
	private String MainFeeNo;
	/** 分案号 */
	private String CaseNo;
	/** 立案号(申请登记号) */
	private String RgtNo;
	/** 项目类型 */
	private String ItemClass;
	/** 项目代码 */
	private String ItemCode;
	/** 项目名称 */
	private String DrugName;
	/** 金额 */
	private double FeeMoney;
	/** 处理标记 */
	private String AvliFlag;
	/** 处理原因代码 */
	private String AvliFlagCode;
	/** 处理原因 */
	private String AvliReason;
	/** 管理机构 */
	private String MngCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 17;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLFeeOtherItemSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CDSerialNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LLFeeOtherItemSchema cloned = (LLFeeOtherItemSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCDSerialNo()
	{
		return CDSerialNo;
	}
	public void setCDSerialNo(String aCDSerialNo)
	{
            CDSerialNo = aCDSerialNo;
	}
	public String getMainFeeNo()
	{
		return MainFeeNo;
	}
	public void setMainFeeNo(String aMainFeeNo)
	{
            MainFeeNo = aMainFeeNo;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
            CaseNo = aCaseNo;
	}
	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
            RgtNo = aRgtNo;
	}
	public String getItemClass()
	{
		return ItemClass;
	}
	public void setItemClass(String aItemClass)
	{
            ItemClass = aItemClass;
	}
	public String getItemCode()
	{
		return ItemCode;
	}
	public void setItemCode(String aItemCode)
	{
            ItemCode = aItemCode;
	}
	public String getDrugName()
	{
		return DrugName;
	}
	public void setDrugName(String aDrugName)
	{
            DrugName = aDrugName;
	}
	public double getFeeMoney()
	{
		return FeeMoney;
	}
	public void setFeeMoney(double aFeeMoney)
	{
            FeeMoney = Arith.round(aFeeMoney,2);
	}
	public void setFeeMoney(String aFeeMoney)
	{
		if (aFeeMoney != null && !aFeeMoney.equals(""))
		{
			Double tDouble = new Double(aFeeMoney);
			double d = tDouble.doubleValue();
                FeeMoney = Arith.round(d,2);
		}
	}

	public String getAvliFlag()
	{
		return AvliFlag;
	}
	public void setAvliFlag(String aAvliFlag)
	{
            AvliFlag = aAvliFlag;
	}
	public String getAvliFlagCode()
	{
		return AvliFlagCode;
	}
	public void setAvliFlagCode(String aAvliFlagCode)
	{
            AvliFlagCode = aAvliFlagCode;
	}
	public String getAvliReason()
	{
		return AvliReason;
	}
	public void setAvliReason(String aAvliReason)
	{
            AvliReason = aAvliReason;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
            MngCom = aMngCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLFeeOtherItemSchema 对象给 Schema 赋值
	* @param: aLLFeeOtherItemSchema LLFeeOtherItemSchema
	**/
	public void setSchema(LLFeeOtherItemSchema aLLFeeOtherItemSchema)
	{
		this.CDSerialNo = aLLFeeOtherItemSchema.getCDSerialNo();
		this.MainFeeNo = aLLFeeOtherItemSchema.getMainFeeNo();
		this.CaseNo = aLLFeeOtherItemSchema.getCaseNo();
		this.RgtNo = aLLFeeOtherItemSchema.getRgtNo();
		this.ItemClass = aLLFeeOtherItemSchema.getItemClass();
		this.ItemCode = aLLFeeOtherItemSchema.getItemCode();
		this.DrugName = aLLFeeOtherItemSchema.getDrugName();
		this.FeeMoney = aLLFeeOtherItemSchema.getFeeMoney();
		this.AvliFlag = aLLFeeOtherItemSchema.getAvliFlag();
		this.AvliFlagCode = aLLFeeOtherItemSchema.getAvliFlagCode();
		this.AvliReason = aLLFeeOtherItemSchema.getAvliReason();
		this.MngCom = aLLFeeOtherItemSchema.getMngCom();
		this.Operator = aLLFeeOtherItemSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLFeeOtherItemSchema.getMakeDate());
		this.MakeTime = aLLFeeOtherItemSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLFeeOtherItemSchema.getModifyDate());
		this.ModifyTime = aLLFeeOtherItemSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CDSerialNo") == null )
				this.CDSerialNo = null;
			else
				this.CDSerialNo = rs.getString("CDSerialNo").trim();

			if( rs.getString("MainFeeNo") == null )
				this.MainFeeNo = null;
			else
				this.MainFeeNo = rs.getString("MainFeeNo").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("ItemClass") == null )
				this.ItemClass = null;
			else
				this.ItemClass = rs.getString("ItemClass").trim();

			if( rs.getString("ItemCode") == null )
				this.ItemCode = null;
			else
				this.ItemCode = rs.getString("ItemCode").trim();

			if( rs.getString("DrugName") == null )
				this.DrugName = null;
			else
				this.DrugName = rs.getString("DrugName").trim();

			this.FeeMoney = rs.getDouble("FeeMoney");
			if( rs.getString("AvliFlag") == null )
				this.AvliFlag = null;
			else
				this.AvliFlag = rs.getString("AvliFlag").trim();

			if( rs.getString("AvliFlagCode") == null )
				this.AvliFlagCode = null;
			else
				this.AvliFlagCode = rs.getString("AvliFlagCode").trim();

			if( rs.getString("AvliReason") == null )
				this.AvliReason = null;
			else
				this.AvliReason = rs.getString("AvliReason").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLFeeOtherItem表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLFeeOtherItemSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLFeeOtherItemSchema getSchema()
	{
		LLFeeOtherItemSchema aLLFeeOtherItemSchema = new LLFeeOtherItemSchema();
		aLLFeeOtherItemSchema.setSchema(this);
		return aLLFeeOtherItemSchema;
	}

	public LLFeeOtherItemDB getDB()
	{
		LLFeeOtherItemDB aDBOper = new LLFeeOtherItemDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLFeeOtherItem描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(CDSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MainFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ItemClass)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ItemCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DrugName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(FeeMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AvliFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AvliFlagCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AvliReason)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLFeeOtherItem>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CDSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			MainFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ItemClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			DrugName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			FeeMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			AvliFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AvliFlagCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AvliReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLFeeOtherItemSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CDSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CDSerialNo));
		}
		if (FCode.equals("MainFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainFeeNo));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("ItemClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemClass));
		}
		if (FCode.equals("ItemCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemCode));
		}
		if (FCode.equals("DrugName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrugName));
		}
		if (FCode.equals("FeeMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeMoney));
		}
		if (FCode.equals("AvliFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AvliFlag));
		}
		if (FCode.equals("AvliFlagCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AvliFlagCode));
		}
		if (FCode.equals("AvliReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AvliReason));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CDSerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(MainFeeNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ItemClass);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ItemCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DrugName);
				break;
			case 7:
				strFieldValue = String.valueOf(FeeMoney);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(AvliFlag);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AvliFlagCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AvliReason);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CDSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CDSerialNo = FValue.trim();
			}
			else
				CDSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("MainFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainFeeNo = FValue.trim();
			}
			else
				MainFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("ItemClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemClass = FValue.trim();
			}
			else
				ItemClass = null;
		}
		if (FCode.equalsIgnoreCase("ItemCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemCode = FValue.trim();
			}
			else
				ItemCode = null;
		}
		if (FCode.equalsIgnoreCase("DrugName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrugName = FValue.trim();
			}
			else
				DrugName = null;
		}
		if (FCode.equalsIgnoreCase("FeeMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FeeMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("AvliFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AvliFlag = FValue.trim();
			}
			else
				AvliFlag = null;
		}
		if (FCode.equalsIgnoreCase("AvliFlagCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AvliFlagCode = FValue.trim();
			}
			else
				AvliFlagCode = null;
		}
		if (FCode.equalsIgnoreCase("AvliReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AvliReason = FValue.trim();
			}
			else
				AvliReason = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLFeeOtherItemSchema other = (LLFeeOtherItemSchema)otherObject;
		return
			CDSerialNo.equals(other.getCDSerialNo())
			&& MainFeeNo.equals(other.getMainFeeNo())
			&& CaseNo.equals(other.getCaseNo())
			&& RgtNo.equals(other.getRgtNo())
			&& ItemClass.equals(other.getItemClass())
			&& ItemCode.equals(other.getItemCode())
			&& DrugName.equals(other.getDrugName())
			&& FeeMoney == other.getFeeMoney()
			&& AvliFlag.equals(other.getAvliFlag())
			&& AvliFlagCode.equals(other.getAvliFlagCode())
			&& AvliReason.equals(other.getAvliReason())
			&& MngCom.equals(other.getMngCom())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CDSerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("MainFeeNo") ) {
			return 1;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 2;
		}
		if( strFieldName.equals("RgtNo") ) {
			return 3;
		}
		if( strFieldName.equals("ItemClass") ) {
			return 4;
		}
		if( strFieldName.equals("ItemCode") ) {
			return 5;
		}
		if( strFieldName.equals("DrugName") ) {
			return 6;
		}
		if( strFieldName.equals("FeeMoney") ) {
			return 7;
		}
		if( strFieldName.equals("AvliFlag") ) {
			return 8;
		}
		if( strFieldName.equals("AvliFlagCode") ) {
			return 9;
		}
		if( strFieldName.equals("AvliReason") ) {
			return 10;
		}
		if( strFieldName.equals("MngCom") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 13;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 16;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CDSerialNo";
				break;
			case 1:
				strFieldName = "MainFeeNo";
				break;
			case 2:
				strFieldName = "CaseNo";
				break;
			case 3:
				strFieldName = "RgtNo";
				break;
			case 4:
				strFieldName = "ItemClass";
				break;
			case 5:
				strFieldName = "ItemCode";
				break;
			case 6:
				strFieldName = "DrugName";
				break;
			case 7:
				strFieldName = "FeeMoney";
				break;
			case 8:
				strFieldName = "AvliFlag";
				break;
			case 9:
				strFieldName = "AvliFlagCode";
				break;
			case 10:
				strFieldName = "AvliReason";
				break;
			case 11:
				strFieldName = "MngCom";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "MakeDate";
				break;
			case 14:
				strFieldName = "MakeTime";
				break;
			case 15:
				strFieldName = "ModifyDate";
				break;
			case 16:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CDSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrugName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AvliFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AvliFlagCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AvliReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
