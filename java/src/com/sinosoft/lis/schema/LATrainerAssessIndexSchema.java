/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATrainerAssessIndexDB;

/*
 * <p>ClassName: LATrainerAssessIndexSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险培训组训信息表
 * @CreateDate：2018-05-03
 */
public class LATrainerAssessIndexSchema implements Schema, Cloneable
{
	// @Field
	/** 指标编码 */
	private String IndexCode;
	/** 指标名称 */
	private String IndexName;
	/** 指标类型 */
	private String IndexType;
	/** 展业类型 */
	private String BranchType;
	/** 指标对应表名 */
	private String ITableName;
	/** 指标对应字段名 */
	private String IColName;
	/** 相关指标 */
	private String IndexSet;
	/** 计算类型 */
	private String CalType;
	/** 计算编码 */
	private String CalCode;
	/** 计算结果值 */
	private String DefaultValue;
	/** 指标计算属性 */
	private String CalPrpty;
	/** 渠道 */
	private String BranchType2;

	public static final int FIELDNUM = 12;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LATrainerAssessIndexSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "IndexCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LATrainerAssessIndexSchema cloned = (LATrainerAssessIndexSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getIndexCode()
	{
		return IndexCode;
	}
	public void setIndexCode(String aIndexCode)
	{
		IndexCode = aIndexCode;
	}
	public String getIndexName()
	{
		return IndexName;
	}
	public void setIndexName(String aIndexName)
	{
		IndexName = aIndexName;
	}
	public String getIndexType()
	{
		return IndexType;
	}
	public void setIndexType(String aIndexType)
	{
		IndexType = aIndexType;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getITableName()
	{
		return ITableName;
	}
	public void setITableName(String aITableName)
	{
		ITableName = aITableName;
	}
	public String getIColName()
	{
		return IColName;
	}
	public void setIColName(String aIColName)
	{
		IColName = aIColName;
	}
	public String getIndexSet()
	{
		return IndexSet;
	}
	public void setIndexSet(String aIndexSet)
	{
		IndexSet = aIndexSet;
	}
	public String getCalType()
	{
		return CalType;
	}
	public void setCalType(String aCalType)
	{
		CalType = aCalType;
	}
	public String getCalCode()
	{
		return CalCode;
	}
	public void setCalCode(String aCalCode)
	{
		CalCode = aCalCode;
	}
	public String getDefaultValue()
	{
		return DefaultValue;
	}
	public void setDefaultValue(String aDefaultValue)
	{
		DefaultValue = aDefaultValue;
	}
	public String getCalPrpty()
	{
		return CalPrpty;
	}
	public void setCalPrpty(String aCalPrpty)
	{
		CalPrpty = aCalPrpty;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}

	/**
	* 使用另外一个 LATrainerAssessIndexSchema 对象给 Schema 赋值
	* @param: aLATrainerAssessIndexSchema LATrainerAssessIndexSchema
	**/
	public void setSchema(LATrainerAssessIndexSchema aLATrainerAssessIndexSchema)
	{
		this.IndexCode = aLATrainerAssessIndexSchema.getIndexCode();
		this.IndexName = aLATrainerAssessIndexSchema.getIndexName();
		this.IndexType = aLATrainerAssessIndexSchema.getIndexType();
		this.BranchType = aLATrainerAssessIndexSchema.getBranchType();
		this.ITableName = aLATrainerAssessIndexSchema.getITableName();
		this.IColName = aLATrainerAssessIndexSchema.getIColName();
		this.IndexSet = aLATrainerAssessIndexSchema.getIndexSet();
		this.CalType = aLATrainerAssessIndexSchema.getCalType();
		this.CalCode = aLATrainerAssessIndexSchema.getCalCode();
		this.DefaultValue = aLATrainerAssessIndexSchema.getDefaultValue();
		this.CalPrpty = aLATrainerAssessIndexSchema.getCalPrpty();
		this.BranchType2 = aLATrainerAssessIndexSchema.getBranchType2();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("IndexCode") == null )
				this.IndexCode = null;
			else
				this.IndexCode = rs.getString("IndexCode").trim();

			if( rs.getString("IndexName") == null )
				this.IndexName = null;
			else
				this.IndexName = rs.getString("IndexName").trim();

			if( rs.getString("IndexType") == null )
				this.IndexType = null;
			else
				this.IndexType = rs.getString("IndexType").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("ITableName") == null )
				this.ITableName = null;
			else
				this.ITableName = rs.getString("ITableName").trim();

			if( rs.getString("IColName") == null )
				this.IColName = null;
			else
				this.IColName = rs.getString("IColName").trim();

			if( rs.getString("IndexSet") == null )
				this.IndexSet = null;
			else
				this.IndexSet = rs.getString("IndexSet").trim();

			if( rs.getString("CalType") == null )
				this.CalType = null;
			else
				this.CalType = rs.getString("CalType").trim();

			if( rs.getString("CalCode") == null )
				this.CalCode = null;
			else
				this.CalCode = rs.getString("CalCode").trim();

			if( rs.getString("DefaultValue") == null )
				this.DefaultValue = null;
			else
				this.DefaultValue = rs.getString("DefaultValue").trim();

			if( rs.getString("CalPrpty") == null )
				this.CalPrpty = null;
			else
				this.CalPrpty = rs.getString("CalPrpty").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LATrainerAssessIndex表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainerAssessIndexSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LATrainerAssessIndexSchema getSchema()
	{
		LATrainerAssessIndexSchema aLATrainerAssessIndexSchema = new LATrainerAssessIndexSchema();
		aLATrainerAssessIndexSchema.setSchema(this);
		return aLATrainerAssessIndexSchema;
	}

	public LATrainerAssessIndexDB getDB()
	{
		LATrainerAssessIndexDB aDBOper = new LATrainerAssessIndexDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainerAssessIndex描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(IndexCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ITableName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IColName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexSet)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DefaultValue)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalPrpty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainerAssessIndex>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			IndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			IndexName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			IndexType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ITableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IColName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			IndexSet = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			DefaultValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CalPrpty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainerAssessIndexSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("IndexCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCode));
		}
		if (FCode.equals("IndexName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexName));
		}
		if (FCode.equals("IndexType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexType));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("ITableName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ITableName));
		}
		if (FCode.equals("IColName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IColName));
		}
		if (FCode.equals("IndexSet"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexSet));
		}
		if (FCode.equals("CalType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalType));
		}
		if (FCode.equals("CalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalCode));
		}
		if (FCode.equals("DefaultValue"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultValue));
		}
		if (FCode.equals("CalPrpty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalPrpty));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(IndexCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(IndexName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(IndexType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ITableName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IColName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(IndexSet);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CalType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CalCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(DefaultValue);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(CalPrpty);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("IndexCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCode = FValue.trim();
			}
			else
				IndexCode = null;
		}
		if (FCode.equalsIgnoreCase("IndexName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexName = FValue.trim();
			}
			else
				IndexName = null;
		}
		if (FCode.equalsIgnoreCase("IndexType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexType = FValue.trim();
			}
			else
				IndexType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("ITableName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ITableName = FValue.trim();
			}
			else
				ITableName = null;
		}
		if (FCode.equalsIgnoreCase("IColName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IColName = FValue.trim();
			}
			else
				IColName = null;
		}
		if (FCode.equalsIgnoreCase("IndexSet"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexSet = FValue.trim();
			}
			else
				IndexSet = null;
		}
		if (FCode.equalsIgnoreCase("CalType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalType = FValue.trim();
			}
			else
				CalType = null;
		}
		if (FCode.equalsIgnoreCase("CalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalCode = FValue.trim();
			}
			else
				CalCode = null;
		}
		if (FCode.equalsIgnoreCase("DefaultValue"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DefaultValue = FValue.trim();
			}
			else
				DefaultValue = null;
		}
		if (FCode.equalsIgnoreCase("CalPrpty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalPrpty = FValue.trim();
			}
			else
				CalPrpty = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LATrainerAssessIndexSchema other = (LATrainerAssessIndexSchema)otherObject;
		return
			(IndexCode == null ? other.getIndexCode() == null : IndexCode.equals(other.getIndexCode()))
			&& (IndexName == null ? other.getIndexName() == null : IndexName.equals(other.getIndexName()))
			&& (IndexType == null ? other.getIndexType() == null : IndexType.equals(other.getIndexType()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (ITableName == null ? other.getITableName() == null : ITableName.equals(other.getITableName()))
			&& (IColName == null ? other.getIColName() == null : IColName.equals(other.getIColName()))
			&& (IndexSet == null ? other.getIndexSet() == null : IndexSet.equals(other.getIndexSet()))
			&& (CalType == null ? other.getCalType() == null : CalType.equals(other.getCalType()))
			&& (CalCode == null ? other.getCalCode() == null : CalCode.equals(other.getCalCode()))
			&& (DefaultValue == null ? other.getDefaultValue() == null : DefaultValue.equals(other.getDefaultValue()))
			&& (CalPrpty == null ? other.getCalPrpty() == null : CalPrpty.equals(other.getCalPrpty()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("IndexCode") ) {
			return 0;
		}
		if( strFieldName.equals("IndexName") ) {
			return 1;
		}
		if( strFieldName.equals("IndexType") ) {
			return 2;
		}
		if( strFieldName.equals("BranchType") ) {
			return 3;
		}
		if( strFieldName.equals("ITableName") ) {
			return 4;
		}
		if( strFieldName.equals("IColName") ) {
			return 5;
		}
		if( strFieldName.equals("IndexSet") ) {
			return 6;
		}
		if( strFieldName.equals("CalType") ) {
			return 7;
		}
		if( strFieldName.equals("CalCode") ) {
			return 8;
		}
		if( strFieldName.equals("DefaultValue") ) {
			return 9;
		}
		if( strFieldName.equals("CalPrpty") ) {
			return 10;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 11;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "IndexCode";
				break;
			case 1:
				strFieldName = "IndexName";
				break;
			case 2:
				strFieldName = "IndexType";
				break;
			case 3:
				strFieldName = "BranchType";
				break;
			case 4:
				strFieldName = "ITableName";
				break;
			case 5:
				strFieldName = "IColName";
				break;
			case 6:
				strFieldName = "IndexSet";
				break;
			case 7:
				strFieldName = "CalType";
				break;
			case 8:
				strFieldName = "CalCode";
				break;
			case 9:
				strFieldName = "DefaultValue";
				break;
			case 10:
				strFieldName = "CalPrpty";
				break;
			case 11:
				strFieldName = "BranchType2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("IndexCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ITableName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IColName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexSet") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DefaultValue") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalPrpty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
