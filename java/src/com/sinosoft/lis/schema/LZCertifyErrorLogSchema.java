/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZCertifyErrorLogDB;

/*
 * <p>ClassName: LZCertifyErrorLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-03-18
 */
public class LZCertifyErrorLogSchema implements Schema, Cloneable
{
	// @Field
	/** 错误日志号 */
	private String ErrLogNo;
	/** 单证编码 */
	private String CertifyCode;
	/** 起始号 */
	private String StartNo;
	/** 终止号 */
	private String EndNo;
	/** 单证持有机构 */
	private String PossessCom;
	/** 出错信息 */
	private String ErrorInfo;
	/** 状态 */
	private String State;
	/** 错误处理标志 */
	private String HandleFlag;
	/** 单证处理日期 */
	private String CertifyMakeData;
	/** 单证处理时间 */
	private String CertifyMakeTime;
	/** 单证业务处理机构 */
	private String CertifyCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date Makedate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LZCertifyErrorLogSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LZCertifyErrorLogSchema cloned = (LZCertifyErrorLogSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getErrLogNo()
	{
		return ErrLogNo;
	}
	public void setErrLogNo(String aErrLogNo)
	{
            ErrLogNo = aErrLogNo;
	}
	public String getCertifyCode()
	{
		return CertifyCode;
	}
	public void setCertifyCode(String aCertifyCode)
	{
            CertifyCode = aCertifyCode;
	}
	public String getStartNo()
	{
		return StartNo;
	}
	public void setStartNo(String aStartNo)
	{
            StartNo = aStartNo;
	}
	public String getEndNo()
	{
		return EndNo;
	}
	public void setEndNo(String aEndNo)
	{
            EndNo = aEndNo;
	}
	public String getPossessCom()
	{
		return PossessCom;
	}
	public void setPossessCom(String aPossessCom)
	{
            PossessCom = aPossessCom;
	}
	public String getErrorInfo()
	{
		return ErrorInfo;
	}
	public void setErrorInfo(String aErrorInfo)
	{
            ErrorInfo = aErrorInfo;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
            State = aState;
	}
	public String getHandleFlag()
	{
		return HandleFlag;
	}
	public void setHandleFlag(String aHandleFlag)
	{
            HandleFlag = aHandleFlag;
	}
	public String getCertifyMakeData()
	{
		return CertifyMakeData;
	}
	public void setCertifyMakeData(String aCertifyMakeData)
	{
            CertifyMakeData = aCertifyMakeData;
	}
	public String getCertifyMakeTime()
	{
		return CertifyMakeTime;
	}
	public void setCertifyMakeTime(String aCertifyMakeTime)
	{
            CertifyMakeTime = aCertifyMakeTime;
	}
	public String getCertifyCom()
	{
		return CertifyCom;
	}
	public void setCertifyCom(String aCertifyCom)
	{
            CertifyCom = aCertifyCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakedate()
	{
		if( Makedate != null )
			return fDate.getString(Makedate);
		else
			return null;
	}
	public void setMakedate(Date aMakedate)
	{
            Makedate = aMakedate;
	}
	public void setMakedate(String aMakedate)
	{
		if (aMakedate != null && !aMakedate.equals("") )
		{
			Makedate = fDate.getDate( aMakedate );
		}
		else
			Makedate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LZCertifyErrorLogSchema 对象给 Schema 赋值
	* @param: aLZCertifyErrorLogSchema LZCertifyErrorLogSchema
	**/
	public void setSchema(LZCertifyErrorLogSchema aLZCertifyErrorLogSchema)
	{
		this.ErrLogNo = aLZCertifyErrorLogSchema.getErrLogNo();
		this.CertifyCode = aLZCertifyErrorLogSchema.getCertifyCode();
		this.StartNo = aLZCertifyErrorLogSchema.getStartNo();
		this.EndNo = aLZCertifyErrorLogSchema.getEndNo();
		this.PossessCom = aLZCertifyErrorLogSchema.getPossessCom();
		this.ErrorInfo = aLZCertifyErrorLogSchema.getErrorInfo();
		this.State = aLZCertifyErrorLogSchema.getState();
		this.HandleFlag = aLZCertifyErrorLogSchema.getHandleFlag();
		this.CertifyMakeData = aLZCertifyErrorLogSchema.getCertifyMakeData();
		this.CertifyMakeTime = aLZCertifyErrorLogSchema.getCertifyMakeTime();
		this.CertifyCom = aLZCertifyErrorLogSchema.getCertifyCom();
		this.Operator = aLZCertifyErrorLogSchema.getOperator();
		this.Makedate = fDate.getDate( aLZCertifyErrorLogSchema.getMakedate());
		this.MakeTime = aLZCertifyErrorLogSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLZCertifyErrorLogSchema.getModifyDate());
		this.ModifyTime = aLZCertifyErrorLogSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ErrLogNo") == null )
				this.ErrLogNo = null;
			else
				this.ErrLogNo = rs.getString("ErrLogNo").trim();

			if( rs.getString("CertifyCode") == null )
				this.CertifyCode = null;
			else
				this.CertifyCode = rs.getString("CertifyCode").trim();

			if( rs.getString("StartNo") == null )
				this.StartNo = null;
			else
				this.StartNo = rs.getString("StartNo").trim();

			if( rs.getString("EndNo") == null )
				this.EndNo = null;
			else
				this.EndNo = rs.getString("EndNo").trim();

			if( rs.getString("PossessCom") == null )
				this.PossessCom = null;
			else
				this.PossessCom = rs.getString("PossessCom").trim();

			if( rs.getString("ErrorInfo") == null )
				this.ErrorInfo = null;
			else
				this.ErrorInfo = rs.getString("ErrorInfo").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("HandleFlag") == null )
				this.HandleFlag = null;
			else
				this.HandleFlag = rs.getString("HandleFlag").trim();

			if( rs.getString("CertifyMakeData") == null )
				this.CertifyMakeData = null;
			else
				this.CertifyMakeData = rs.getString("CertifyMakeData").trim();

			if( rs.getString("CertifyMakeTime") == null )
				this.CertifyMakeTime = null;
			else
				this.CertifyMakeTime = rs.getString("CertifyMakeTime").trim();

			if( rs.getString("CertifyCom") == null )
				this.CertifyCom = null;
			else
				this.CertifyCom = rs.getString("CertifyCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.Makedate = rs.getDate("Makedate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LZCertifyErrorLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LZCertifyErrorLogSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LZCertifyErrorLogSchema getSchema()
	{
		LZCertifyErrorLogSchema aLZCertifyErrorLogSchema = new LZCertifyErrorLogSchema();
		aLZCertifyErrorLogSchema.setSchema(this);
		return aLZCertifyErrorLogSchema;
	}

	public LZCertifyErrorLogDB getDB()
	{
		LZCertifyErrorLogDB aDBOper = new LZCertifyErrorLogDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCertifyErrorLog描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ErrLogNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(StartNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(EndNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PossessCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ErrorInfo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(HandleFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CertifyMakeData)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CertifyMakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CertifyCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( Makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCertifyErrorLog>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ErrLogNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			StartNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			EndNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PossessCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ErrorInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			HandleFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CertifyMakeData = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CertifyMakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CertifyCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LZCertifyErrorLogSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ErrLogNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrLogNo));
		}
		if (FCode.equals("CertifyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
		}
		if (FCode.equals("StartNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StartNo));
		}
		if (FCode.equals("EndNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndNo));
		}
		if (FCode.equals("PossessCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PossessCom));
		}
		if (FCode.equals("ErrorInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorInfo));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("HandleFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandleFlag));
		}
		if (FCode.equals("CertifyMakeData"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyMakeData));
		}
		if (FCode.equals("CertifyMakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyMakeTime));
		}
		if (FCode.equals("CertifyCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ErrLogNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CertifyCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(StartNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(EndNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(PossessCom);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ErrorInfo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(HandleFlag);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CertifyMakeData);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CertifyMakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(CertifyCom);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ErrLogNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrLogNo = FValue.trim();
			}
			else
				ErrLogNo = null;
		}
		if (FCode.equalsIgnoreCase("CertifyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyCode = FValue.trim();
			}
			else
				CertifyCode = null;
		}
		if (FCode.equalsIgnoreCase("StartNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StartNo = FValue.trim();
			}
			else
				StartNo = null;
		}
		if (FCode.equalsIgnoreCase("EndNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndNo = FValue.trim();
			}
			else
				EndNo = null;
		}
		if (FCode.equalsIgnoreCase("PossessCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PossessCom = FValue.trim();
			}
			else
				PossessCom = null;
		}
		if (FCode.equalsIgnoreCase("ErrorInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorInfo = FValue.trim();
			}
			else
				ErrorInfo = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("HandleFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandleFlag = FValue.trim();
			}
			else
				HandleFlag = null;
		}
		if (FCode.equalsIgnoreCase("CertifyMakeData"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyMakeData = FValue.trim();
			}
			else
				CertifyMakeData = null;
		}
		if (FCode.equalsIgnoreCase("CertifyMakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyMakeTime = FValue.trim();
			}
			else
				CertifyMakeTime = null;
		}
		if (FCode.equalsIgnoreCase("CertifyCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyCom = FValue.trim();
			}
			else
				CertifyCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Makedate = fDate.getDate( FValue );
			}
			else
				Makedate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LZCertifyErrorLogSchema other = (LZCertifyErrorLogSchema)otherObject;
		return
			ErrLogNo.equals(other.getErrLogNo())
			&& CertifyCode.equals(other.getCertifyCode())
			&& StartNo.equals(other.getStartNo())
			&& EndNo.equals(other.getEndNo())
			&& PossessCom.equals(other.getPossessCom())
			&& ErrorInfo.equals(other.getErrorInfo())
			&& State.equals(other.getState())
			&& HandleFlag.equals(other.getHandleFlag())
			&& CertifyMakeData.equals(other.getCertifyMakeData())
			&& CertifyMakeTime.equals(other.getCertifyMakeTime())
			&& CertifyCom.equals(other.getCertifyCom())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(Makedate).equals(other.getMakedate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ErrLogNo") ) {
			return 0;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return 1;
		}
		if( strFieldName.equals("StartNo") ) {
			return 2;
		}
		if( strFieldName.equals("EndNo") ) {
			return 3;
		}
		if( strFieldName.equals("PossessCom") ) {
			return 4;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return 5;
		}
		if( strFieldName.equals("State") ) {
			return 6;
		}
		if( strFieldName.equals("HandleFlag") ) {
			return 7;
		}
		if( strFieldName.equals("CertifyMakeData") ) {
			return 8;
		}
		if( strFieldName.equals("CertifyMakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("CertifyCom") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("Makedate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ErrLogNo";
				break;
			case 1:
				strFieldName = "CertifyCode";
				break;
			case 2:
				strFieldName = "StartNo";
				break;
			case 3:
				strFieldName = "EndNo";
				break;
			case 4:
				strFieldName = "PossessCom";
				break;
			case 5:
				strFieldName = "ErrorInfo";
				break;
			case 6:
				strFieldName = "State";
				break;
			case 7:
				strFieldName = "HandleFlag";
				break;
			case 8:
				strFieldName = "CertifyMakeData";
				break;
			case 9:
				strFieldName = "CertifyMakeTime";
				break;
			case 10:
				strFieldName = "CertifyCom";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "Makedate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ErrLogNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PossessCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandleFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyMakeData") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyMakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
