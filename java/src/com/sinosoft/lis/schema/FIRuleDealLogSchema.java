/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIRuleDealLogDB;

/*
 * <p>ClassName: FIRuleDealLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIRuleDealLogSchema implements Schema, Cloneable
{
	// @Field
	/** 版本编号 */
	private String VersionNo;
	/** 校验批次号码 */
	private String CheckBatchNo;
	/** 事件号码 */
	private String EventNo;
	/** 校检类型 */
	private String CheckType;
	/** 数据来源 */
	private String DataSource;
	/** 校验规则 */
	private String RuleID;
	/** 校验事件结点 */
	private String CallPointID;
	/** 校验结果 */
	private String RuleResult;
	/** 校验人 */
	private String Operator;
	/** 校验计划 */
	private String RulePlanID;
	/** 校验日期 */
	private Date MakeDate;
	/** 校验时间 */
	private String MakeTime;
	/** 日志文件路径 */
	private String LogFilePath;
	/** 日志文件名称 */
	private String LogFileName;

	public static final int FIELDNUM = 14;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIRuleDealLogSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CheckBatchNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIRuleDealLogSchema cloned = (FIRuleDealLogSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getCheckBatchNo()
	{
		return CheckBatchNo;
	}
	public void setCheckBatchNo(String aCheckBatchNo)
	{
		CheckBatchNo = aCheckBatchNo;
	}
	public String getEventNo()
	{
		return EventNo;
	}
	public void setEventNo(String aEventNo)
	{
		EventNo = aEventNo;
	}
	public String getCheckType()
	{
		return CheckType;
	}
	public void setCheckType(String aCheckType)
	{
		CheckType = aCheckType;
	}
	public String getDataSource()
	{
		return DataSource;
	}
	public void setDataSource(String aDataSource)
	{
		DataSource = aDataSource;
	}
	public String getRuleID()
	{
		return RuleID;
	}
	public void setRuleID(String aRuleID)
	{
		RuleID = aRuleID;
	}
	public String getCallPointID()
	{
		return CallPointID;
	}
	public void setCallPointID(String aCallPointID)
	{
		CallPointID = aCallPointID;
	}
	public String getRuleResult()
	{
		return RuleResult;
	}
	public void setRuleResult(String aRuleResult)
	{
		RuleResult = aRuleResult;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getRulePlanID()
	{
		return RulePlanID;
	}
	public void setRulePlanID(String aRulePlanID)
	{
		RulePlanID = aRulePlanID;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getLogFilePath()
	{
		return LogFilePath;
	}
	public void setLogFilePath(String aLogFilePath)
	{
		LogFilePath = aLogFilePath;
	}
	public String getLogFileName()
	{
		return LogFileName;
	}
	public void setLogFileName(String aLogFileName)
	{
		LogFileName = aLogFileName;
	}

	/**
	* 使用另外一个 FIRuleDealLogSchema 对象给 Schema 赋值
	* @param: aFIRuleDealLogSchema FIRuleDealLogSchema
	**/
	public void setSchema(FIRuleDealLogSchema aFIRuleDealLogSchema)
	{
		this.VersionNo = aFIRuleDealLogSchema.getVersionNo();
		this.CheckBatchNo = aFIRuleDealLogSchema.getCheckBatchNo();
		this.EventNo = aFIRuleDealLogSchema.getEventNo();
		this.CheckType = aFIRuleDealLogSchema.getCheckType();
		this.DataSource = aFIRuleDealLogSchema.getDataSource();
		this.RuleID = aFIRuleDealLogSchema.getRuleID();
		this.CallPointID = aFIRuleDealLogSchema.getCallPointID();
		this.RuleResult = aFIRuleDealLogSchema.getRuleResult();
		this.Operator = aFIRuleDealLogSchema.getOperator();
		this.RulePlanID = aFIRuleDealLogSchema.getRulePlanID();
		this.MakeDate = fDate.getDate( aFIRuleDealLogSchema.getMakeDate());
		this.MakeTime = aFIRuleDealLogSchema.getMakeTime();
		this.LogFilePath = aFIRuleDealLogSchema.getLogFilePath();
		this.LogFileName = aFIRuleDealLogSchema.getLogFileName();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("CheckBatchNo") == null )
				this.CheckBatchNo = null;
			else
				this.CheckBatchNo = rs.getString("CheckBatchNo").trim();

			if( rs.getString("EventNo") == null )
				this.EventNo = null;
			else
				this.EventNo = rs.getString("EventNo").trim();

			if( rs.getString("CheckType") == null )
				this.CheckType = null;
			else
				this.CheckType = rs.getString("CheckType").trim();

			if( rs.getString("DataSource") == null )
				this.DataSource = null;
			else
				this.DataSource = rs.getString("DataSource").trim();

			if( rs.getString("RuleID") == null )
				this.RuleID = null;
			else
				this.RuleID = rs.getString("RuleID").trim();

			if( rs.getString("CallPointID") == null )
				this.CallPointID = null;
			else
				this.CallPointID = rs.getString("CallPointID").trim();

			if( rs.getString("RuleResult") == null )
				this.RuleResult = null;
			else
				this.RuleResult = rs.getString("RuleResult").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("RulePlanID") == null )
				this.RulePlanID = null;
			else
				this.RulePlanID = rs.getString("RulePlanID").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			if( rs.getString("LogFilePath") == null )
				this.LogFilePath = null;
			else
				this.LogFilePath = rs.getString("LogFilePath").trim();

			if( rs.getString("LogFileName") == null )
				this.LogFileName = null;
			else
				this.LogFileName = rs.getString("LogFileName").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIRuleDealLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIRuleDealLogSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIRuleDealLogSchema getSchema()
	{
		FIRuleDealLogSchema aFIRuleDealLogSchema = new FIRuleDealLogSchema();
		aFIRuleDealLogSchema.setSchema(this);
		return aFIRuleDealLogSchema;
	}

	public FIRuleDealLogDB getDB()
	{
		FIRuleDealLogDB aDBOper = new FIRuleDealLogDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIRuleDealLog描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EventNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataSource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CallPointID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RuleResult)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RulePlanID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LogFilePath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LogFileName));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIRuleDealLog>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CheckBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			EventNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CheckType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			DataSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RuleID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CallPointID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			RuleResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			RulePlanID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			LogFilePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			LogFileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIRuleDealLogSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("CheckBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckBatchNo));
		}
		if (FCode.equals("EventNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EventNo));
		}
		if (FCode.equals("CheckType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckType));
		}
		if (FCode.equals("DataSource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataSource));
		}
		if (FCode.equals("RuleID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleID));
		}
		if (FCode.equals("CallPointID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CallPointID));
		}
		if (FCode.equals("RuleResult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RuleResult));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("RulePlanID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RulePlanID));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("LogFilePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LogFilePath));
		}
		if (FCode.equals("LogFileName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LogFileName));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CheckBatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(EventNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CheckType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(DataSource);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RuleID);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CallPointID);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(RuleResult);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(RulePlanID);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(LogFilePath);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(LogFileName);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("CheckBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckBatchNo = FValue.trim();
			}
			else
				CheckBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("EventNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EventNo = FValue.trim();
			}
			else
				EventNo = null;
		}
		if (FCode.equalsIgnoreCase("CheckType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckType = FValue.trim();
			}
			else
				CheckType = null;
		}
		if (FCode.equalsIgnoreCase("DataSource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataSource = FValue.trim();
			}
			else
				DataSource = null;
		}
		if (FCode.equalsIgnoreCase("RuleID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleID = FValue.trim();
			}
			else
				RuleID = null;
		}
		if (FCode.equalsIgnoreCase("CallPointID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CallPointID = FValue.trim();
			}
			else
				CallPointID = null;
		}
		if (FCode.equalsIgnoreCase("RuleResult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RuleResult = FValue.trim();
			}
			else
				RuleResult = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("RulePlanID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RulePlanID = FValue.trim();
			}
			else
				RulePlanID = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("LogFilePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LogFilePath = FValue.trim();
			}
			else
				LogFilePath = null;
		}
		if (FCode.equalsIgnoreCase("LogFileName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LogFileName = FValue.trim();
			}
			else
				LogFileName = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIRuleDealLogSchema other = (FIRuleDealLogSchema)otherObject;
		return
			(VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (CheckBatchNo == null ? other.getCheckBatchNo() == null : CheckBatchNo.equals(other.getCheckBatchNo()))
			&& (EventNo == null ? other.getEventNo() == null : EventNo.equals(other.getEventNo()))
			&& (CheckType == null ? other.getCheckType() == null : CheckType.equals(other.getCheckType()))
			&& (DataSource == null ? other.getDataSource() == null : DataSource.equals(other.getDataSource()))
			&& (RuleID == null ? other.getRuleID() == null : RuleID.equals(other.getRuleID()))
			&& (CallPointID == null ? other.getCallPointID() == null : CallPointID.equals(other.getCallPointID()))
			&& (RuleResult == null ? other.getRuleResult() == null : RuleResult.equals(other.getRuleResult()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (RulePlanID == null ? other.getRulePlanID() == null : RulePlanID.equals(other.getRulePlanID()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (LogFilePath == null ? other.getLogFilePath() == null : LogFilePath.equals(other.getLogFilePath()))
			&& (LogFileName == null ? other.getLogFileName() == null : LogFileName.equals(other.getLogFileName()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return 0;
		}
		if( strFieldName.equals("CheckBatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("EventNo") ) {
			return 2;
		}
		if( strFieldName.equals("CheckType") ) {
			return 3;
		}
		if( strFieldName.equals("DataSource") ) {
			return 4;
		}
		if( strFieldName.equals("RuleID") ) {
			return 5;
		}
		if( strFieldName.equals("CallPointID") ) {
			return 6;
		}
		if( strFieldName.equals("RuleResult") ) {
			return 7;
		}
		if( strFieldName.equals("Operator") ) {
			return 8;
		}
		if( strFieldName.equals("RulePlanID") ) {
			return 9;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 11;
		}
		if( strFieldName.equals("LogFilePath") ) {
			return 12;
		}
		if( strFieldName.equals("LogFileName") ) {
			return 13;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "VersionNo";
				break;
			case 1:
				strFieldName = "CheckBatchNo";
				break;
			case 2:
				strFieldName = "EventNo";
				break;
			case 3:
				strFieldName = "CheckType";
				break;
			case 4:
				strFieldName = "DataSource";
				break;
			case 5:
				strFieldName = "RuleID";
				break;
			case 6:
				strFieldName = "CallPointID";
				break;
			case 7:
				strFieldName = "RuleResult";
				break;
			case 8:
				strFieldName = "Operator";
				break;
			case 9:
				strFieldName = "RulePlanID";
				break;
			case 10:
				strFieldName = "MakeDate";
				break;
			case 11:
				strFieldName = "MakeTime";
				break;
			case 12:
				strFieldName = "LogFilePath";
				break;
			case 13:
				strFieldName = "LogFileName";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EventNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataSource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CallPointID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RuleResult") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RulePlanID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LogFilePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LogFileName") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
