/*
 * <p>ClassName: LLCaseAccSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 理赔表结构调整
 * @CreateDate：2004-12-16
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLCaseAccDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LLCaseAccSchema implements Schema
{
    // @Field
    /** 分案号 */
    private String CaseNo;
    /** 集体合同号 */
    private String GrpContNo;
    /** 集体保单号 */
    private String GrpPolNo;
    /** 个单合同号 */
    private String ContNo;
    /** 保单号 */
    private String PolNo;
    /** 险类代码 */
    private String KindCode;
    /** 险种版本号 */
    private String RiskVer;
    /** 险种代码 */
    private String RiskCode;
    /** 给付责任代码 */
    private String GetDutyCode;
    /** 给付责任类型 */
    private String GetDutyKind;
    /** 被保人客户号 */
    private String InsuredNo;
    /** 被保人名称 */
    private String InsuredName;
    /** 投保人客户号 */
    private String AppntNo;
    /** 投保人名称 */
    private String AppntName;
    /** 账号类型 */
    private String AccType;
    /** 保险账户号码 */
    private String InsuAccNo;
    /** 账户本金 */
    private double AccBJMoney;
    /** 账户利息 */
    private double InterestMoney;
    /** 账户金额合计 */
    private double TotalMoney;
    /** 最初理算金额 */
    private double BasePay;
    /** 实际理算金额 */
    private double RealPay;
    /** 关联保单号码 */
    private String MasterPolNo;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 28; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLCaseAccSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[7];
        pk[0] = "CaseNo";
        pk[1] = "PolNo";
        pk[2] = "GetDutyCode";
        pk[3] = "GetDutyKind";
        pk[4] = "AccType";
        pk[5] = "InsuAccNo";
        pk[6] = "MasterPolNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCaseNo()
    {
        if (CaseNo != null && !CaseNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            CaseNo = StrTool.unicodeToGBK(CaseNo);
        }
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo)
    {
        CaseNo = aCaseNo;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getKindCode()
    {
        if (KindCode != null && !KindCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            KindCode = StrTool.unicodeToGBK(KindCode);
        }
        return KindCode;
    }

    public void setKindCode(String aKindCode)
    {
        KindCode = aKindCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getGetDutyCode()
    {
        if (GetDutyCode != null && !GetDutyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyCode = StrTool.unicodeToGBK(GetDutyCode);
        }
        return GetDutyCode;
    }

    public void setGetDutyCode(String aGetDutyCode)
    {
        GetDutyCode = aGetDutyCode;
    }

    public String getGetDutyKind()
    {
        if (GetDutyKind != null && !GetDutyKind.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyKind = StrTool.unicodeToGBK(GetDutyKind);
        }
        return GetDutyKind;
    }

    public void setGetDutyKind(String aGetDutyKind)
    {
        GetDutyKind = aGetDutyKind;
    }

    public String getInsuredNo()
    {
        if (InsuredNo != null && !InsuredNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredNo = StrTool.unicodeToGBK(InsuredNo);
        }
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getInsuredName()
    {
        if (InsuredName != null && !InsuredName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredName = StrTool.unicodeToGBK(InsuredName);
        }
        return InsuredName;
    }

    public void setInsuredName(String aInsuredName)
    {
        InsuredName = aInsuredName;
    }

    public String getAppntNo()
    {
        if (AppntNo != null && !AppntNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public String getAppntName()
    {
        if (AppntName != null && !AppntName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppntName = StrTool.unicodeToGBK(AppntName);
        }
        return AppntName;
    }

    public void setAppntName(String aAppntName)
    {
        AppntName = aAppntName;
    }

    public String getAccType()
    {
        if (AccType != null && !AccType.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccType = StrTool.unicodeToGBK(AccType);
        }
        return AccType;
    }

    public void setAccType(String aAccType)
    {
        AccType = aAccType;
    }

    public String getInsuAccNo()
    {
        if (InsuAccNo != null && !InsuAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuAccNo = StrTool.unicodeToGBK(InsuAccNo);
        }
        return InsuAccNo;
    }

    public void setInsuAccNo(String aInsuAccNo)
    {
        InsuAccNo = aInsuAccNo;
    }

    public double getAccBJMoney()
    {
        return AccBJMoney;
    }

    public void setAccBJMoney(double aAccBJMoney)
    {
        AccBJMoney = aAccBJMoney;
    }

    public void setAccBJMoney(String aAccBJMoney)
    {
        if (aAccBJMoney != null && !aAccBJMoney.equals(""))
        {
            Double tDouble = new Double(aAccBJMoney);
            double d = tDouble.doubleValue();
            AccBJMoney = d;
        }
    }

    public double getInterestMoney()
    {
        return InterestMoney;
    }

    public void setInterestMoney(double aInterestMoney)
    {
        InterestMoney = aInterestMoney;
    }

    public void setInterestMoney(String aInterestMoney)
    {
        if (aInterestMoney != null && !aInterestMoney.equals(""))
        {
            Double tDouble = new Double(aInterestMoney);
            double d = tDouble.doubleValue();
            InterestMoney = d;
        }
    }

    public double getTotalMoney()
    {
        return TotalMoney;
    }

    public void setTotalMoney(double aTotalMoney)
    {
        TotalMoney = aTotalMoney;
    }

    public void setTotalMoney(String aTotalMoney)
    {
        if (aTotalMoney != null && !aTotalMoney.equals(""))
        {
            Double tDouble = new Double(aTotalMoney);
            double d = tDouble.doubleValue();
            TotalMoney = d;
        }
    }

    public double getBasePay()
    {
        return BasePay;
    }

    public void setBasePay(double aBasePay)
    {
        BasePay = aBasePay;
    }

    public void setBasePay(String aBasePay)
    {
        if (aBasePay != null && !aBasePay.equals(""))
        {
            Double tDouble = new Double(aBasePay);
            double d = tDouble.doubleValue();
            BasePay = d;
        }
    }

    public double getRealPay()
    {
        return RealPay;
    }

    public void setRealPay(double aRealPay)
    {
        RealPay = aRealPay;
    }

    public void setRealPay(String aRealPay)
    {
        if (aRealPay != null && !aRealPay.equals(""))
        {
            Double tDouble = new Double(aRealPay);
            double d = tDouble.doubleValue();
            RealPay = d;
        }
    }

    public String getMasterPolNo()
    {
        if (MasterPolNo != null && !MasterPolNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            MasterPolNo = StrTool.unicodeToGBK(MasterPolNo);
        }
        return MasterPolNo;
    }

    public void setMasterPolNo(String aMasterPolNo)
    {
        MasterPolNo = aMasterPolNo;
    }

    public String getMngCom()
    {
        if (MngCom != null && !MngCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLCaseAccSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLCaseAccSchema aLLCaseAccSchema)
    {
        this.CaseNo = aLLCaseAccSchema.getCaseNo();
        this.GrpContNo = aLLCaseAccSchema.getGrpContNo();
        this.GrpPolNo = aLLCaseAccSchema.getGrpPolNo();
        this.ContNo = aLLCaseAccSchema.getContNo();
        this.PolNo = aLLCaseAccSchema.getPolNo();
        this.KindCode = aLLCaseAccSchema.getKindCode();
        this.RiskVer = aLLCaseAccSchema.getRiskVer();
        this.RiskCode = aLLCaseAccSchema.getRiskCode();
        this.GetDutyCode = aLLCaseAccSchema.getGetDutyCode();
        this.GetDutyKind = aLLCaseAccSchema.getGetDutyKind();
        this.InsuredNo = aLLCaseAccSchema.getInsuredNo();
        this.InsuredName = aLLCaseAccSchema.getInsuredName();
        this.AppntNo = aLLCaseAccSchema.getAppntNo();
        this.AppntName = aLLCaseAccSchema.getAppntName();
        this.AccType = aLLCaseAccSchema.getAccType();
        this.InsuAccNo = aLLCaseAccSchema.getInsuAccNo();
        this.AccBJMoney = aLLCaseAccSchema.getAccBJMoney();
        this.InterestMoney = aLLCaseAccSchema.getInterestMoney();
        this.TotalMoney = aLLCaseAccSchema.getTotalMoney();
        this.BasePay = aLLCaseAccSchema.getBasePay();
        this.RealPay = aLLCaseAccSchema.getRealPay();
        this.MasterPolNo = aLLCaseAccSchema.getMasterPolNo();
        this.MngCom = aLLCaseAccSchema.getMngCom();
        this.Operator = aLLCaseAccSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLCaseAccSchema.getMakeDate());
        this.MakeTime = aLLCaseAccSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLCaseAccSchema.getModifyDate());
        this.ModifyTime = aLLCaseAccSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CaseNo") == null)
            {
                this.CaseNo = null;
            }
            else
            {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("KindCode") == null)
            {
                this.KindCode = null;
            }
            else
            {
                this.KindCode = rs.getString("KindCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("GetDutyCode") == null)
            {
                this.GetDutyCode = null;
            }
            else
            {
                this.GetDutyCode = rs.getString("GetDutyCode").trim();
            }

            if (rs.getString("GetDutyKind") == null)
            {
                this.GetDutyKind = null;
            }
            else
            {
                this.GetDutyKind = rs.getString("GetDutyKind").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("InsuredName") == null)
            {
                this.InsuredName = null;
            }
            else
            {
                this.InsuredName = rs.getString("InsuredName").trim();
            }

            if (rs.getString("AppntNo") == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("AppntName") == null)
            {
                this.AppntName = null;
            }
            else
            {
                this.AppntName = rs.getString("AppntName").trim();
            }

            if (rs.getString("AccType") == null)
            {
                this.AccType = null;
            }
            else
            {
                this.AccType = rs.getString("AccType").trim();
            }

            if (rs.getString("InsuAccNo") == null)
            {
                this.InsuAccNo = null;
            }
            else
            {
                this.InsuAccNo = rs.getString("InsuAccNo").trim();
            }

            this.AccBJMoney = rs.getDouble("AccBJMoney");
            this.InterestMoney = rs.getDouble("InterestMoney");
            this.TotalMoney = rs.getDouble("TotalMoney");
            this.BasePay = rs.getDouble("BasePay");
            this.RealPay = rs.getDouble("RealPay");
            if (rs.getString("MasterPolNo") == null)
            {
                this.MasterPolNo = null;
            }
            else
            {
                this.MasterPolNo = rs.getString("MasterPolNo").trim();
            }

            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseAccSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLCaseAccSchema getSchema()
    {
        LLCaseAccSchema aLLCaseAccSchema = new LLCaseAccSchema();
        aLLCaseAccSchema.setSchema(this);
        return aLLCaseAccSchema;
    }

    public LLCaseAccDB getDB()
    {
        LLCaseAccDB aDBOper = new LLCaseAccDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseAcc描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(CaseNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(KindCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuAccNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(AccBJMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(InterestMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(TotalMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(BasePay) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(RealPay) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MasterPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MngCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseAcc>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                     SysConst.PACKAGESPILTER);
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                       SysConst.PACKAGESPILTER);
            AccBJMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).doubleValue();
            InterestMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            TotalMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            BasePay = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            RealPay = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 21, SysConst.PACKAGESPILTER))).doubleValue();
            MasterPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                         SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 27, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseAccSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CaseNo));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("KindCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(KindCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("GetDutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyCode));
        }
        if (FCode.equals("GetDutyKind"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyKind));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredNo));
        }
        if (FCode.equals("InsuredName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredName));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntNo));
        }
        if (FCode.equals("AppntName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntName));
        }
        if (FCode.equals("AccType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccType));
        }
        if (FCode.equals("InsuAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuAccNo));
        }
        if (FCode.equals("AccBJMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccBJMoney));
        }
        if (FCode.equals("InterestMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InterestMoney));
        }
        if (FCode.equals("TotalMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TotalMoney));
        }
        if (FCode.equals("BasePay"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BasePay));
        }
        if (FCode.equals("RealPay"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RealPay));
        }
        if (FCode.equals("MasterPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MasterPolNo));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MngCom));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(KindCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AccType);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 16:
                strFieldValue = String.valueOf(AccBJMoney);
                break;
            case 17:
                strFieldValue = String.valueOf(InterestMoney);
                break;
            case 18:
                strFieldValue = String.valueOf(TotalMoney);
                break;
            case 19:
                strFieldValue = String.valueOf(BasePay);
                break;
            case 20:
                strFieldValue = String.valueOf(RealPay);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MasterPolNo);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
            {
                CaseNo = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("KindCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
            {
                KindCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("GetDutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
            {
                GetDutyCode = null;
            }
        }
        if (FCode.equals("GetDutyKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyKind = FValue.trim();
            }
            else
            {
                GetDutyKind = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("InsuredName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
            {
                InsuredName = null;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("AppntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
            {
                AppntName = null;
            }
        }
        if (FCode.equals("AccType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
            {
                AccType = null;
            }
        }
        if (FCode.equals("InsuAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
            {
                InsuAccNo = null;
            }
        }
        if (FCode.equals("AccBJMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AccBJMoney = d;
            }
        }
        if (FCode.equals("InterestMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                InterestMoney = d;
            }
        }
        if (FCode.equals("TotalMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TotalMoney = d;
            }
        }
        if (FCode.equals("BasePay"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BasePay = d;
            }
        }
        if (FCode.equals("RealPay"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RealPay = d;
            }
        }
        if (FCode.equals("MasterPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MasterPolNo = FValue.trim();
            }
            else
            {
                MasterPolNo = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLCaseAccSchema other = (LLCaseAccSchema) otherObject;
        return
                CaseNo.equals(other.getCaseNo())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && ContNo.equals(other.getContNo())
                && PolNo.equals(other.getPolNo())
                && KindCode.equals(other.getKindCode())
                && RiskVer.equals(other.getRiskVer())
                && RiskCode.equals(other.getRiskCode())
                && GetDutyCode.equals(other.getGetDutyCode())
                && GetDutyKind.equals(other.getGetDutyKind())
                && InsuredNo.equals(other.getInsuredNo())
                && InsuredName.equals(other.getInsuredName())
                && AppntNo.equals(other.getAppntNo())
                && AppntName.equals(other.getAppntName())
                && AccType.equals(other.getAccType())
                && InsuAccNo.equals(other.getInsuAccNo())
                && AccBJMoney == other.getAccBJMoney()
                && InterestMoney == other.getInterestMoney()
                && TotalMoney == other.getTotalMoney()
                && BasePay == other.getBasePay()
                && RealPay == other.getRealPay()
                && MasterPolNo.equals(other.getMasterPolNo())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CaseNo"))
        {
            return 0;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 2;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 3;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 4;
        }
        if (strFieldName.equals("KindCode"))
        {
            return 5;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 6;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 7;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return 8;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return 9;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 10;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return 11;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 12;
        }
        if (strFieldName.equals("AppntName"))
        {
            return 13;
        }
        if (strFieldName.equals("AccType"))
        {
            return 14;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return 15;
        }
        if (strFieldName.equals("AccBJMoney"))
        {
            return 16;
        }
        if (strFieldName.equals("InterestMoney"))
        {
            return 17;
        }
        if (strFieldName.equals("TotalMoney"))
        {
            return 18;
        }
        if (strFieldName.equals("BasePay"))
        {
            return 19;
        }
        if (strFieldName.equals("RealPay"))
        {
            return 20;
        }
        if (strFieldName.equals("MasterPolNo"))
        {
            return 21;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 22;
        }
        if (strFieldName.equals("Operator"))
        {
            return 23;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 24;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 25;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 26;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 27;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CaseNo";
                break;
            case 1:
                strFieldName = "GrpContNo";
                break;
            case 2:
                strFieldName = "GrpPolNo";
                break;
            case 3:
                strFieldName = "ContNo";
                break;
            case 4:
                strFieldName = "PolNo";
                break;
            case 5:
                strFieldName = "KindCode";
                break;
            case 6:
                strFieldName = "RiskVer";
                break;
            case 7:
                strFieldName = "RiskCode";
                break;
            case 8:
                strFieldName = "GetDutyCode";
                break;
            case 9:
                strFieldName = "GetDutyKind";
                break;
            case 10:
                strFieldName = "InsuredNo";
                break;
            case 11:
                strFieldName = "InsuredName";
                break;
            case 12:
                strFieldName = "AppntNo";
                break;
            case 13:
                strFieldName = "AppntName";
                break;
            case 14:
                strFieldName = "AccType";
                break;
            case 15:
                strFieldName = "InsuAccNo";
                break;
            case 16:
                strFieldName = "AccBJMoney";
                break;
            case 17:
                strFieldName = "InterestMoney";
                break;
            case 18:
                strFieldName = "TotalMoney";
                break;
            case 19:
                strFieldName = "BasePay";
                break;
            case 20:
                strFieldName = "RealPay";
                break;
            case 21:
                strFieldName = "MasterPolNo";
                break;
            case 22:
                strFieldName = "MngCom";
                break;
            case 23:
                strFieldName = "Operator";
                break;
            case 24:
                strFieldName = "MakeDate";
                break;
            case 25:
                strFieldName = "MakeTime";
                break;
            case 26:
                strFieldName = "ModifyDate";
                break;
            case 27:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("KindCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccBJMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("InterestMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("TotalMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BasePay"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RealPay"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MasterPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
