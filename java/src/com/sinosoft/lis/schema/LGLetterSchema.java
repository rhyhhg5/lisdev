/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LGLetterDB;

/*
 * <p>ClassName: LGLetterSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-03-04
 */
public class LGLetterSchema implements Schema, Cloneable
{
	// @Field
	/** 受理号 */
	private String EdorAcceptNo;
	/** 函件序号 */
	private String SerialNumber;
	/** 函件分类 */
	private String LetterType;
	/** 函件子类型 */
	private String LetterSubType;
	/** 函件状态 */
	private String State;
	/** 下发对象 */
	private String SendObj;
	/** 下发地址编号 */
	private String AddressNo;
	/** 是否需回销 */
	private String BackFlag;
	/** 应回销日期 */
	private Date BackDate;
	/** 应回销时间 */
	private String BackTime;
	/** 实回销日期 */
	private Date RealBackDate;
	/** 实回销时间 */
	private String RealBackTime;
	/** 下发日期 */
	private Date SendDate;
	/** 下发时间 */
	private String SendTime;
	/** 函件信息 */
	private String LetterInfo;
	/** 反馈信息 */
	private String FeedBackInfo;
	/** 备注 */
	private String Remark;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 22;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LGLetterSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "EdorAcceptNo";
		pk[1] = "SerialNumber";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LGLetterSchema cloned = (LGLetterSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorAcceptNo()
	{
		return EdorAcceptNo;
	}
	public void setEdorAcceptNo(String aEdorAcceptNo)
	{
            EdorAcceptNo = aEdorAcceptNo;
	}
	public String getSerialNumber()
	{
		return SerialNumber;
	}
	public void setSerialNumber(String aSerialNumber)
	{
            SerialNumber = aSerialNumber;
	}
	public String getLetterType()
	{
		return LetterType;
	}
	public void setLetterType(String aLetterType)
	{
            LetterType = aLetterType;
	}
	public String getLetterSubType()
	{
		return LetterSubType;
	}
	public void setLetterSubType(String aLetterSubType)
	{
            LetterSubType = aLetterSubType;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
            State = aState;
	}
	public String getSendObj()
	{
		return SendObj;
	}
	public void setSendObj(String aSendObj)
	{
            SendObj = aSendObj;
	}
	public String getAddressNo()
	{
		return AddressNo;
	}
	public void setAddressNo(String aAddressNo)
	{
            AddressNo = aAddressNo;
	}
	public String getBackFlag()
	{
		return BackFlag;
	}
	public void setBackFlag(String aBackFlag)
	{
            BackFlag = aBackFlag;
	}
	public String getBackDate()
	{
		if( BackDate != null )
			return fDate.getString(BackDate);
		else
			return null;
	}
	public void setBackDate(Date aBackDate)
	{
            BackDate = aBackDate;
	}
	public void setBackDate(String aBackDate)
	{
		if (aBackDate != null && !aBackDate.equals("") )
		{
			BackDate = fDate.getDate( aBackDate );
		}
		else
			BackDate = null;
	}

	public String getBackTime()
	{
		return BackTime;
	}
	public void setBackTime(String aBackTime)
	{
            BackTime = aBackTime;
	}
	public String getRealBackDate()
	{
		if( RealBackDate != null )
			return fDate.getString(RealBackDate);
		else
			return null;
	}
	public void setRealBackDate(Date aRealBackDate)
	{
            RealBackDate = aRealBackDate;
	}
	public void setRealBackDate(String aRealBackDate)
	{
		if (aRealBackDate != null && !aRealBackDate.equals("") )
		{
			RealBackDate = fDate.getDate( aRealBackDate );
		}
		else
			RealBackDate = null;
	}

	public String getRealBackTime()
	{
		return RealBackTime;
	}
	public void setRealBackTime(String aRealBackTime)
	{
            RealBackTime = aRealBackTime;
	}
	public String getSendDate()
	{
		if( SendDate != null )
			return fDate.getString(SendDate);
		else
			return null;
	}
	public void setSendDate(Date aSendDate)
	{
            SendDate = aSendDate;
	}
	public void setSendDate(String aSendDate)
	{
		if (aSendDate != null && !aSendDate.equals("") )
		{
			SendDate = fDate.getDate( aSendDate );
		}
		else
			SendDate = null;
	}

	public String getSendTime()
	{
		return SendTime;
	}
	public void setSendTime(String aSendTime)
	{
            SendTime = aSendTime;
	}
	public String getLetterInfo()
	{
		return LetterInfo;
	}
	public void setLetterInfo(String aLetterInfo)
	{
            LetterInfo = aLetterInfo;
	}
	public String getFeedBackInfo()
	{
		return FeedBackInfo;
	}
	public void setFeedBackInfo(String aFeedBackInfo)
	{
            FeedBackInfo = aFeedBackInfo;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
            Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LGLetterSchema 对象给 Schema 赋值
	* @param: aLGLetterSchema LGLetterSchema
	**/
	public void setSchema(LGLetterSchema aLGLetterSchema)
	{
		this.EdorAcceptNo = aLGLetterSchema.getEdorAcceptNo();
		this.SerialNumber = aLGLetterSchema.getSerialNumber();
		this.LetterType = aLGLetterSchema.getLetterType();
		this.LetterSubType = aLGLetterSchema.getLetterSubType();
		this.State = aLGLetterSchema.getState();
		this.SendObj = aLGLetterSchema.getSendObj();
		this.AddressNo = aLGLetterSchema.getAddressNo();
		this.BackFlag = aLGLetterSchema.getBackFlag();
		this.BackDate = fDate.getDate( aLGLetterSchema.getBackDate());
		this.BackTime = aLGLetterSchema.getBackTime();
		this.RealBackDate = fDate.getDate( aLGLetterSchema.getRealBackDate());
		this.RealBackTime = aLGLetterSchema.getRealBackTime();
		this.SendDate = fDate.getDate( aLGLetterSchema.getSendDate());
		this.SendTime = aLGLetterSchema.getSendTime();
		this.LetterInfo = aLGLetterSchema.getLetterInfo();
		this.FeedBackInfo = aLGLetterSchema.getFeedBackInfo();
		this.Remark = aLGLetterSchema.getRemark();
		this.Operator = aLGLetterSchema.getOperator();
		this.MakeDate = fDate.getDate( aLGLetterSchema.getMakeDate());
		this.MakeTime = aLGLetterSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLGLetterSchema.getModifyDate());
		this.ModifyTime = aLGLetterSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorAcceptNo") == null )
				this.EdorAcceptNo = null;
			else
				this.EdorAcceptNo = rs.getString("EdorAcceptNo").trim();

			if( rs.getString("SerialNumber") == null )
				this.SerialNumber = null;
			else
				this.SerialNumber = rs.getString("SerialNumber").trim();

			if( rs.getString("LetterType") == null )
				this.LetterType = null;
			else
				this.LetterType = rs.getString("LetterType").trim();

			if( rs.getString("LetterSubType") == null )
				this.LetterSubType = null;
			else
				this.LetterSubType = rs.getString("LetterSubType").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("SendObj") == null )
				this.SendObj = null;
			else
				this.SendObj = rs.getString("SendObj").trim();

			if( rs.getString("AddressNo") == null )
				this.AddressNo = null;
			else
				this.AddressNo = rs.getString("AddressNo").trim();

			if( rs.getString("BackFlag") == null )
				this.BackFlag = null;
			else
				this.BackFlag = rs.getString("BackFlag").trim();

			this.BackDate = rs.getDate("BackDate");
			if( rs.getString("BackTime") == null )
				this.BackTime = null;
			else
				this.BackTime = rs.getString("BackTime").trim();

			this.RealBackDate = rs.getDate("RealBackDate");
			if( rs.getString("RealBackTime") == null )
				this.RealBackTime = null;
			else
				this.RealBackTime = rs.getString("RealBackTime").trim();

			this.SendDate = rs.getDate("SendDate");
			if( rs.getString("SendTime") == null )
				this.SendTime = null;
			else
				this.SendTime = rs.getString("SendTime").trim();

			if( rs.getString("LetterInfo") == null )
				this.LetterInfo = null;
			else
				this.LetterInfo = rs.getString("LetterInfo").trim();

			if( rs.getString("FeedBackInfo") == null )
				this.FeedBackInfo = null;
			else
				this.FeedBackInfo = rs.getString("FeedBackInfo").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LGLetter表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LGLetterSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LGLetterSchema getSchema()
	{
		LGLetterSchema aLGLetterSchema = new LGLetterSchema();
		aLGLetterSchema.setSchema(this);
		return aLGLetterSchema;
	}

	public LGLetterDB getDB()
	{
		LGLetterDB aDBOper = new LGLetterDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGLetter描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(EdorAcceptNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SerialNumber)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(LetterType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(LetterSubType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SendObj)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AddressNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BackFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( BackDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BackTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( RealBackDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RealBackTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( SendDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SendTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(LetterInfo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FeedBackInfo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGLetter>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SerialNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			LetterType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			LetterSubType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			SendObj = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BackFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			BackDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			BackTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			RealBackDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			RealBackTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			SendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			SendTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			LetterInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			FeedBackInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LGLetterSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorAcceptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
		}
		if (FCode.equals("SerialNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNumber));
		}
		if (FCode.equals("LetterType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LetterType));
		}
		if (FCode.equals("LetterSubType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LetterSubType));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("SendObj"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendObj));
		}
		if (FCode.equals("AddressNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
		}
		if (FCode.equals("BackFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackFlag));
		}
		if (FCode.equals("BackDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBackDate()));
		}
		if (FCode.equals("BackTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackTime));
		}
		if (FCode.equals("RealBackDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRealBackDate()));
		}
		if (FCode.equals("RealBackTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RealBackTime));
		}
		if (FCode.equals("SendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
		}
		if (FCode.equals("SendTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendTime));
		}
		if (FCode.equals("LetterInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LetterInfo));
		}
		if (FCode.equals("FeedBackInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeedBackInfo));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SerialNumber);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(LetterType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(LetterSubType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(SendObj);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AddressNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BackFlag);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBackDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(BackTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRealBackDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(RealBackTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(SendTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(LetterInfo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(FeedBackInfo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorAcceptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorAcceptNo = FValue.trim();
			}
			else
				EdorAcceptNo = null;
		}
		if (FCode.equalsIgnoreCase("SerialNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNumber = FValue.trim();
			}
			else
				SerialNumber = null;
		}
		if (FCode.equalsIgnoreCase("LetterType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LetterType = FValue.trim();
			}
			else
				LetterType = null;
		}
		if (FCode.equalsIgnoreCase("LetterSubType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LetterSubType = FValue.trim();
			}
			else
				LetterSubType = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("SendObj"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendObj = FValue.trim();
			}
			else
				SendObj = null;
		}
		if (FCode.equalsIgnoreCase("AddressNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AddressNo = FValue.trim();
			}
			else
				AddressNo = null;
		}
		if (FCode.equalsIgnoreCase("BackFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackFlag = FValue.trim();
			}
			else
				BackFlag = null;
		}
		if (FCode.equalsIgnoreCase("BackDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BackDate = fDate.getDate( FValue );
			}
			else
				BackDate = null;
		}
		if (FCode.equalsIgnoreCase("BackTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackTime = FValue.trim();
			}
			else
				BackTime = null;
		}
		if (FCode.equalsIgnoreCase("RealBackDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RealBackDate = fDate.getDate( FValue );
			}
			else
				RealBackDate = null;
		}
		if (FCode.equalsIgnoreCase("RealBackTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RealBackTime = FValue.trim();
			}
			else
				RealBackTime = null;
		}
		if (FCode.equalsIgnoreCase("SendDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SendDate = fDate.getDate( FValue );
			}
			else
				SendDate = null;
		}
		if (FCode.equalsIgnoreCase("SendTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendTime = FValue.trim();
			}
			else
				SendTime = null;
		}
		if (FCode.equalsIgnoreCase("LetterInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LetterInfo = FValue.trim();
			}
			else
				LetterInfo = null;
		}
		if (FCode.equalsIgnoreCase("FeedBackInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeedBackInfo = FValue.trim();
			}
			else
				FeedBackInfo = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LGLetterSchema other = (LGLetterSchema)otherObject;
		return
			EdorAcceptNo.equals(other.getEdorAcceptNo())
			&& SerialNumber.equals(other.getSerialNumber())
			&& LetterType.equals(other.getLetterType())
			&& LetterSubType.equals(other.getLetterSubType())
			&& State.equals(other.getState())
			&& SendObj.equals(other.getSendObj())
			&& AddressNo.equals(other.getAddressNo())
			&& BackFlag.equals(other.getBackFlag())
			&& fDate.getString(BackDate).equals(other.getBackDate())
			&& BackTime.equals(other.getBackTime())
			&& fDate.getString(RealBackDate).equals(other.getRealBackDate())
			&& RealBackTime.equals(other.getRealBackTime())
			&& fDate.getString(SendDate).equals(other.getSendDate())
			&& SendTime.equals(other.getSendTime())
			&& LetterInfo.equals(other.getLetterInfo())
			&& FeedBackInfo.equals(other.getFeedBackInfo())
			&& Remark.equals(other.getRemark())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorAcceptNo") ) {
			return 0;
		}
		if( strFieldName.equals("SerialNumber") ) {
			return 1;
		}
		if( strFieldName.equals("LetterType") ) {
			return 2;
		}
		if( strFieldName.equals("LetterSubType") ) {
			return 3;
		}
		if( strFieldName.equals("State") ) {
			return 4;
		}
		if( strFieldName.equals("SendObj") ) {
			return 5;
		}
		if( strFieldName.equals("AddressNo") ) {
			return 6;
		}
		if( strFieldName.equals("BackFlag") ) {
			return 7;
		}
		if( strFieldName.equals("BackDate") ) {
			return 8;
		}
		if( strFieldName.equals("BackTime") ) {
			return 9;
		}
		if( strFieldName.equals("RealBackDate") ) {
			return 10;
		}
		if( strFieldName.equals("RealBackTime") ) {
			return 11;
		}
		if( strFieldName.equals("SendDate") ) {
			return 12;
		}
		if( strFieldName.equals("SendTime") ) {
			return 13;
		}
		if( strFieldName.equals("LetterInfo") ) {
			return 14;
		}
		if( strFieldName.equals("FeedBackInfo") ) {
			return 15;
		}
		if( strFieldName.equals("Remark") ) {
			return 16;
		}
		if( strFieldName.equals("Operator") ) {
			return 17;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 18;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 20;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 21;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorAcceptNo";
				break;
			case 1:
				strFieldName = "SerialNumber";
				break;
			case 2:
				strFieldName = "LetterType";
				break;
			case 3:
				strFieldName = "LetterSubType";
				break;
			case 4:
				strFieldName = "State";
				break;
			case 5:
				strFieldName = "SendObj";
				break;
			case 6:
				strFieldName = "AddressNo";
				break;
			case 7:
				strFieldName = "BackFlag";
				break;
			case 8:
				strFieldName = "BackDate";
				break;
			case 9:
				strFieldName = "BackTime";
				break;
			case 10:
				strFieldName = "RealBackDate";
				break;
			case 11:
				strFieldName = "RealBackTime";
				break;
			case 12:
				strFieldName = "SendDate";
				break;
			case 13:
				strFieldName = "SendTime";
				break;
			case 14:
				strFieldName = "LetterInfo";
				break;
			case 15:
				strFieldName = "FeedBackInfo";
				break;
			case 16:
				strFieldName = "Remark";
				break;
			case 17:
				strFieldName = "Operator";
				break;
			case 18:
				strFieldName = "MakeDate";
				break;
			case 19:
				strFieldName = "MakeTime";
				break;
			case 20:
				strFieldName = "ModifyDate";
				break;
			case 21:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorAcceptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SerialNumber") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LetterType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LetterSubType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendObj") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AddressNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BackTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RealBackDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RealBackTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SendTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LetterInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeedBackInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
