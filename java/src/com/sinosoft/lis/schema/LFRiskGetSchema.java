/*
 * <p>ClassName: LFRiskGetSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LFRiskGetDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFRiskGetSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 管理机构 */
    private String ManageCom;
    /** 缴费间隔 */
    private int PayIntv;
    /** 销售渠道 */
    private String SaleChnl;
    /** 赔付类型标志 */
    private String GetFlag;
    /** 报表日期 */
    private Date ReportDate;
    /** 赔付金额 */
    private double GetMoney;
    /** 返还金额 */
    private double ReturnMoney;
    /** 赔付人数 */
    private double InsuredCount;
    /** 赔付保单件数 */
    private double PolCount;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFRiskGetSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "RiskCode";
        pk[1] = "ManageCom";
        pk[2] = "PayIntv";
        pk[3] = "SaleChnl";
        pk[4] = "GetFlag";
        pk[5] = "ReportDate";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public int getPayIntv()
    {
        return PayIntv;
    }

    public void setPayIntv(int aPayIntv)
    {
        PayIntv = aPayIntv;
    }

    public void setPayIntv(String aPayIntv)
    {
        if (aPayIntv != null && !aPayIntv.equals(""))
        {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getSaleChnl()
    {
        if (SaleChnl != null && !SaleChnl.equals("") && SysConst.CHANGECHARSET == true)
        {
            SaleChnl = StrTool.unicodeToGBK(SaleChnl);
        }
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl)
    {
        SaleChnl = aSaleChnl;
    }

    public String getGetFlag()
    {
        if (GetFlag != null && !GetFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            GetFlag = StrTool.unicodeToGBK(GetFlag);
        }
        return GetFlag;
    }

    public void setGetFlag(String aGetFlag)
    {
        GetFlag = aGetFlag;
    }

    public String getReportDate()
    {
        if (ReportDate != null)
        {
            return fDate.getString(ReportDate);
        }
        else
        {
            return null;
        }
    }

    public void setReportDate(Date aReportDate)
    {
        ReportDate = aReportDate;
    }

    public void setReportDate(String aReportDate)
    {
        if (aReportDate != null && !aReportDate.equals(""))
        {
            ReportDate = fDate.getDate(aReportDate);
        }
        else
        {
            ReportDate = null;
        }
    }

    public double getGetMoney()
    {
        return GetMoney;
    }

    public void setGetMoney(double aGetMoney)
    {
        GetMoney = aGetMoney;
    }

    public void setGetMoney(String aGetMoney)
    {
        if (aGetMoney != null && !aGetMoney.equals(""))
        {
            Double tDouble = new Double(aGetMoney);
            double d = tDouble.doubleValue();
            GetMoney = d;
        }
    }

    public double getReturnMoney()
    {
        return ReturnMoney;
    }

    public void setReturnMoney(double aReturnMoney)
    {
        ReturnMoney = aReturnMoney;
    }

    public void setReturnMoney(String aReturnMoney)
    {
        if (aReturnMoney != null && !aReturnMoney.equals(""))
        {
            Double tDouble = new Double(aReturnMoney);
            double d = tDouble.doubleValue();
            ReturnMoney = d;
        }
    }

    public double getInsuredCount()
    {
        return InsuredCount;
    }

    public void setInsuredCount(double aInsuredCount)
    {
        InsuredCount = aInsuredCount;
    }

    public void setInsuredCount(String aInsuredCount)
    {
        if (aInsuredCount != null && !aInsuredCount.equals(""))
        {
            Double tDouble = new Double(aInsuredCount);
            double d = tDouble.doubleValue();
            InsuredCount = d;
        }
    }

    public double getPolCount()
    {
        return PolCount;
    }

    public void setPolCount(double aPolCount)
    {
        PolCount = aPolCount;
    }

    public void setPolCount(String aPolCount)
    {
        if (aPolCount != null && !aPolCount.equals(""))
        {
            Double tDouble = new Double(aPolCount);
            double d = tDouble.doubleValue();
            PolCount = d;
        }
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LFRiskGetSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFRiskGetSchema aLFRiskGetSchema)
    {
        this.RiskCode = aLFRiskGetSchema.getRiskCode();
        this.ManageCom = aLFRiskGetSchema.getManageCom();
        this.PayIntv = aLFRiskGetSchema.getPayIntv();
        this.SaleChnl = aLFRiskGetSchema.getSaleChnl();
        this.GetFlag = aLFRiskGetSchema.getGetFlag();
        this.ReportDate = fDate.getDate(aLFRiskGetSchema.getReportDate());
        this.GetMoney = aLFRiskGetSchema.getGetMoney();
        this.ReturnMoney = aLFRiskGetSchema.getReturnMoney();
        this.InsuredCount = aLFRiskGetSchema.getInsuredCount();
        this.PolCount = aLFRiskGetSchema.getPolCount();
        this.MakeDate = fDate.getDate(aLFRiskGetSchema.getMakeDate());
        this.MakeTime = aLFRiskGetSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.PayIntv = rs.getInt("PayIntv");
            if (rs.getString("SaleChnl") == null)
            {
                this.SaleChnl = null;
            }
            else
            {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            if (rs.getString("GetFlag") == null)
            {
                this.GetFlag = null;
            }
            else
            {
                this.GetFlag = rs.getString("GetFlag").trim();
            }

            this.ReportDate = rs.getDate("ReportDate");
            this.GetMoney = rs.getDouble("GetMoney");
            this.ReturnMoney = rs.getDouble("ReturnMoney");
            this.InsuredCount = rs.getDouble("InsuredCount");
            this.PolCount = rs.getDouble("PolCount");
            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFRiskGetSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFRiskGetSchema getSchema()
    {
        LFRiskGetSchema aLFRiskGetSchema = new LFRiskGetSchema();
        aLFRiskGetSchema.setSchema(this);
        return aLFRiskGetSchema;
    }

    public LFRiskGetDB getDB()
    {
        LFRiskGetDB aDBOper = new LFRiskGetDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFRiskGet描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayIntv) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SaleChnl)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetFlag)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ReportDate))) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(ReturnMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(InsuredCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PolCount) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFRiskGet>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).intValue();
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            GetFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            ReportDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            GetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            ReturnMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            InsuredCount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            PolCount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFRiskGetSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("PayIntv"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayIntv));
        }
        if (FCode.equals("SaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SaleChnl));
        }
        if (FCode.equals("GetFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetFlag));
        }
        if (FCode.equals("ReportDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getReportDate()));
        }
        if (FCode.equals("GetMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetMoney));
        }
        if (FCode.equals("ReturnMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReturnMoney));
        }
        if (FCode.equals("InsuredCount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredCount));
        }
        if (FCode.equals("PolCount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolCount));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 2:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GetFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getReportDate()));
                break;
            case 6:
                strFieldValue = String.valueOf(GetMoney);
                break;
            case 7:
                strFieldValue = String.valueOf(ReturnMoney);
                break;
            case 8:
                strFieldValue = String.valueOf(InsuredCount);
                break;
            case 9:
                strFieldValue = String.valueOf(PolCount);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("PayIntv"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equals("SaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
            {
                SaleChnl = null;
            }
        }
        if (FCode.equals("GetFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetFlag = FValue.trim();
            }
            else
            {
                GetFlag = null;
            }
        }
        if (FCode.equals("ReportDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReportDate = fDate.getDate(FValue);
            }
            else
            {
                ReportDate = null;
            }
        }
        if (FCode.equals("GetMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetMoney = d;
            }
        }
        if (FCode.equals("ReturnMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ReturnMoney = d;
            }
        }
        if (FCode.equals("InsuredCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                InsuredCount = d;
            }
        }
        if (FCode.equals("PolCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PolCount = d;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFRiskGetSchema other = (LFRiskGetSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && ManageCom.equals(other.getManageCom())
                && PayIntv == other.getPayIntv()
                && SaleChnl.equals(other.getSaleChnl())
                && GetFlag.equals(other.getGetFlag())
                && fDate.getString(ReportDate).equals(other.getReportDate())
                && GetMoney == other.getGetMoney()
                && ReturnMoney == other.getReturnMoney()
                && InsuredCount == other.getInsuredCount()
                && PolCount == other.getPolCount()
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 1;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return 2;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return 3;
        }
        if (strFieldName.equals("GetFlag"))
        {
            return 4;
        }
        if (strFieldName.equals("ReportDate"))
        {
            return 5;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return 6;
        }
        if (strFieldName.equals("ReturnMoney"))
        {
            return 7;
        }
        if (strFieldName.equals("InsuredCount"))
        {
            return 8;
        }
        if (strFieldName.equals("PolCount"))
        {
            return 9;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "ManageCom";
                break;
            case 2:
                strFieldName = "PayIntv";
                break;
            case 3:
                strFieldName = "SaleChnl";
                break;
            case 4:
                strFieldName = "GetFlag";
                break;
            case 5:
                strFieldName = "ReportDate";
                break;
            case 6:
                strFieldName = "GetMoney";
                break;
            case 7:
                strFieldName = "ReturnMoney";
                break;
            case 8:
                strFieldName = "InsuredCount";
                break;
            case 9:
                strFieldName = "PolCount";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReportDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ReturnMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("InsuredCount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PolCount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_INT;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
