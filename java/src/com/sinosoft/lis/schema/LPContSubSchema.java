/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LPContSubDB;

/*
 * <p>ClassName: LPContSubSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2019-09-12
 */
public class LPContSubSchema implements Schema, Cloneable
{
	// @Field
	/** 批单号 */
	private String EdorNo;
	/** 批改类型 */
	private String EdorType;
	/** 印刷号码 */
	private String PrtNo;
	/** 居民类型 */
	private String CountyType;
	/** 家庭收入 */
	private double FamilySalary;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 税优投保标识 */
	private String TaxFlag;
	/** 个人税务登记号 */
	private String TaxNo;
	/** 所在税优批次 */
	private String BatchNo;
	/** 个税征收方式 */
	private String TaxPayerType;
	/** 税优识别码 */
	private String TaxCode;
	/** 获取税优码成功标识 */
	private String SuccFlag;
	/** 错误信息 */
	private String ErrorInfo;
	/** 预约码 */
	private String BookingNo;
	/** 被保人序号 */
	private String InsuredId;
	/** 团体编号 */
	private String GrpNo;
	/** 转移保单标识 */
	private String TransFlag;
	/** 客户验证标识 */
	private String CheckFlag;
	/** 验证返回信息 */
	private String CheckInfo;
	/** 个人社会信用代码 */
	private String CreditCode;
	/** 单位税务登记证代码 */
	private String GTaxNo;
	/** 单位社会信用代码 */
	private String GOrgancomCode;
	/** Uuid */
	private String Uuid;
	/** 年龄折扣因子 */
	private String Agediscountfactor;
	/** 补充医疗保险折扣因子 */
	private String Supdiscountfactor;
	/** 团体投保人数折扣因子 */
	private String Grpdiscountfactor;
	/** 税优总折扣因子 */
	private String Totaldiscountfactor;
	/** 保单打印类型 */
	private String PrintType;
	/** 上海人身险医保卡号 */
	private String MedicalCode;
	/** 上海人身险扣款方式 */
	private String PayMode;
	/** 上海人身险是否自动续保 */
	private String IfAutoPay;
	/** 上海人身险续保扣款方式 */
	private String RenemalPayMethod;
	/** 上海人身险投保流水号 */
	private String YbkSerialno;
	/** 被保险人风险保险费档次 */
	private String PremMult;
	/** 双录标识 */
	private String RecordFlag;
	/** 沈阳智慧缴费单号 */
	private String SYMedicalTempfeeno;
	/** 折扣方式 */
	private String DiscountMode;
	/** 影像复查标志 */
	private String ScanCheckFlag;
	/** 约定缴费标识 */
	private String AgreedPayFlag;
	/** 约定缴费日期 */
	private Date AgreedPayDate;
	/** 保单来源 */
	private String PolicySource;

	public static final int FIELDNUM = 46;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LPContSubSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "EdorNo";
		pk[1] = "EdorType";
		pk[2] = "PrtNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LPContSubSchema cloned = (LPContSubSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getEdorType()
	{
		return EdorType;
	}
	public void setEdorType(String aEdorType)
	{
		EdorType = aEdorType;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getCountyType()
	{
		return CountyType;
	}
	public void setCountyType(String aCountyType)
	{
		CountyType = aCountyType;
	}
	public double getFamilySalary()
	{
		return FamilySalary;
	}
	public void setFamilySalary(double aFamilySalary)
	{
		FamilySalary = Arith.round(aFamilySalary,2);
	}
	public void setFamilySalary(String aFamilySalary)
	{
		if (aFamilySalary != null && !aFamilySalary.equals(""))
		{
			Double tDouble = new Double(aFamilySalary);
			double d = tDouble.doubleValue();
                FamilySalary = Arith.round(d,2);
		}
	}

	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getTaxFlag()
	{
		return TaxFlag;
	}
	public void setTaxFlag(String aTaxFlag)
	{
		TaxFlag = aTaxFlag;
	}
	public String getTaxNo()
	{
		return TaxNo;
	}
	public void setTaxNo(String aTaxNo)
	{
		TaxNo = aTaxNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getTaxPayerType()
	{
		return TaxPayerType;
	}
	public void setTaxPayerType(String aTaxPayerType)
	{
		TaxPayerType = aTaxPayerType;
	}
	public String getTaxCode()
	{
		return TaxCode;
	}
	public void setTaxCode(String aTaxCode)
	{
		TaxCode = aTaxCode;
	}
	public String getSuccFlag()
	{
		return SuccFlag;
	}
	public void setSuccFlag(String aSuccFlag)
	{
		SuccFlag = aSuccFlag;
	}
	public String getErrorInfo()
	{
		return ErrorInfo;
	}
	public void setErrorInfo(String aErrorInfo)
	{
		ErrorInfo = aErrorInfo;
	}
	public String getBookingNo()
	{
		return BookingNo;
	}
	public void setBookingNo(String aBookingNo)
	{
		BookingNo = aBookingNo;
	}
	public String getInsuredId()
	{
		return InsuredId;
	}
	public void setInsuredId(String aInsuredId)
	{
		InsuredId = aInsuredId;
	}
	public String getGrpNo()
	{
		return GrpNo;
	}
	public void setGrpNo(String aGrpNo)
	{
		GrpNo = aGrpNo;
	}
	public String getTransFlag()
	{
		return TransFlag;
	}
	public void setTransFlag(String aTransFlag)
	{
		TransFlag = aTransFlag;
	}
	public String getCheckFlag()
	{
		return CheckFlag;
	}
	public void setCheckFlag(String aCheckFlag)
	{
		CheckFlag = aCheckFlag;
	}
	public String getCheckInfo()
	{
		return CheckInfo;
	}
	public void setCheckInfo(String aCheckInfo)
	{
		CheckInfo = aCheckInfo;
	}
	public String getCreditCode()
	{
		return CreditCode;
	}
	public void setCreditCode(String aCreditCode)
	{
		CreditCode = aCreditCode;
	}
	public String getGTaxNo()
	{
		return GTaxNo;
	}
	public void setGTaxNo(String aGTaxNo)
	{
		GTaxNo = aGTaxNo;
	}
	public String getGOrgancomCode()
	{
		return GOrgancomCode;
	}
	public void setGOrgancomCode(String aGOrgancomCode)
	{
		GOrgancomCode = aGOrgancomCode;
	}
	public String getUuid()
	{
		return Uuid;
	}
	public void setUuid(String aUuid)
	{
		Uuid = aUuid;
	}
	public String getAgediscountfactor()
	{
		return Agediscountfactor;
	}
	public void setAgediscountfactor(String aAgediscountfactor)
	{
		Agediscountfactor = aAgediscountfactor;
	}
	public String getSupdiscountfactor()
	{
		return Supdiscountfactor;
	}
	public void setSupdiscountfactor(String aSupdiscountfactor)
	{
		Supdiscountfactor = aSupdiscountfactor;
	}
	public String getGrpdiscountfactor()
	{
		return Grpdiscountfactor;
	}
	public void setGrpdiscountfactor(String aGrpdiscountfactor)
	{
		Grpdiscountfactor = aGrpdiscountfactor;
	}
	public String getTotaldiscountfactor()
	{
		return Totaldiscountfactor;
	}
	public void setTotaldiscountfactor(String aTotaldiscountfactor)
	{
		Totaldiscountfactor = aTotaldiscountfactor;
	}
	public String getPrintType()
	{
		return PrintType;
	}
	public void setPrintType(String aPrintType)
	{
		PrintType = aPrintType;
	}
	public String getMedicalCode()
	{
		return MedicalCode;
	}
	public void setMedicalCode(String aMedicalCode)
	{
		MedicalCode = aMedicalCode;
	}
	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getIfAutoPay()
	{
		return IfAutoPay;
	}
	public void setIfAutoPay(String aIfAutoPay)
	{
		IfAutoPay = aIfAutoPay;
	}
	public String getRenemalPayMethod()
	{
		return RenemalPayMethod;
	}
	public void setRenemalPayMethod(String aRenemalPayMethod)
	{
		RenemalPayMethod = aRenemalPayMethod;
	}
	public String getYbkSerialno()
	{
		return YbkSerialno;
	}
	public void setYbkSerialno(String aYbkSerialno)
	{
		YbkSerialno = aYbkSerialno;
	}
	public String getPremMult()
	{
		return PremMult;
	}
	public void setPremMult(String aPremMult)
	{
		PremMult = aPremMult;
	}
	public String getRecordFlag()
	{
		return RecordFlag;
	}
	public void setRecordFlag(String aRecordFlag)
	{
		RecordFlag = aRecordFlag;
	}
	public String getSYMedicalTempfeeno()
	{
		return SYMedicalTempfeeno;
	}
	public void setSYMedicalTempfeeno(String aSYMedicalTempfeeno)
	{
		SYMedicalTempfeeno = aSYMedicalTempfeeno;
	}
	public String getDiscountMode()
	{
		return DiscountMode;
	}
	public void setDiscountMode(String aDiscountMode)
	{
		DiscountMode = aDiscountMode;
	}
	public String getScanCheckFlag()
	{
		return ScanCheckFlag;
	}
	public void setScanCheckFlag(String aScanCheckFlag)
	{
		ScanCheckFlag = aScanCheckFlag;
	}
	public String getAgreedPayFlag()
	{
		return AgreedPayFlag;
	}
	public void setAgreedPayFlag(String aAgreedPayFlag)
	{
		AgreedPayFlag = aAgreedPayFlag;
	}
	public String getAgreedPayDate()
	{
		if( AgreedPayDate != null )
			return fDate.getString(AgreedPayDate);
		else
			return null;
	}
	public void setAgreedPayDate(Date aAgreedPayDate)
	{
		AgreedPayDate = aAgreedPayDate;
	}
	public void setAgreedPayDate(String aAgreedPayDate)
	{
		if (aAgreedPayDate != null && !aAgreedPayDate.equals("") )
		{
			AgreedPayDate = fDate.getDate( aAgreedPayDate );
		}
		else
			AgreedPayDate = null;
	}

	public String getPolicySource()
	{
		return PolicySource;
	}
	public void setPolicySource(String aPolicySource)
	{
		PolicySource = aPolicySource;
	}

	/**
	* 使用另外一个 LPContSubSchema 对象给 Schema 赋值
	* @param: aLPContSubSchema LPContSubSchema
	**/
	public void setSchema(LPContSubSchema aLPContSubSchema)
	{
		this.EdorNo = aLPContSubSchema.getEdorNo();
		this.EdorType = aLPContSubSchema.getEdorType();
		this.PrtNo = aLPContSubSchema.getPrtNo();
		this.CountyType = aLPContSubSchema.getCountyType();
		this.FamilySalary = aLPContSubSchema.getFamilySalary();
		this.ManageCom = aLPContSubSchema.getManageCom();
		this.Operator = aLPContSubSchema.getOperator();
		this.MakeDate = fDate.getDate( aLPContSubSchema.getMakeDate());
		this.MakeTime = aLPContSubSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLPContSubSchema.getModifyDate());
		this.ModifyTime = aLPContSubSchema.getModifyTime();
		this.TaxFlag = aLPContSubSchema.getTaxFlag();
		this.TaxNo = aLPContSubSchema.getTaxNo();
		this.BatchNo = aLPContSubSchema.getBatchNo();
		this.TaxPayerType = aLPContSubSchema.getTaxPayerType();
		this.TaxCode = aLPContSubSchema.getTaxCode();
		this.SuccFlag = aLPContSubSchema.getSuccFlag();
		this.ErrorInfo = aLPContSubSchema.getErrorInfo();
		this.BookingNo = aLPContSubSchema.getBookingNo();
		this.InsuredId = aLPContSubSchema.getInsuredId();
		this.GrpNo = aLPContSubSchema.getGrpNo();
		this.TransFlag = aLPContSubSchema.getTransFlag();
		this.CheckFlag = aLPContSubSchema.getCheckFlag();
		this.CheckInfo = aLPContSubSchema.getCheckInfo();
		this.CreditCode = aLPContSubSchema.getCreditCode();
		this.GTaxNo = aLPContSubSchema.getGTaxNo();
		this.GOrgancomCode = aLPContSubSchema.getGOrgancomCode();
		this.Uuid = aLPContSubSchema.getUuid();
		this.Agediscountfactor = aLPContSubSchema.getAgediscountfactor();
		this.Supdiscountfactor = aLPContSubSchema.getSupdiscountfactor();
		this.Grpdiscountfactor = aLPContSubSchema.getGrpdiscountfactor();
		this.Totaldiscountfactor = aLPContSubSchema.getTotaldiscountfactor();
		this.PrintType = aLPContSubSchema.getPrintType();
		this.MedicalCode = aLPContSubSchema.getMedicalCode();
		this.PayMode = aLPContSubSchema.getPayMode();
		this.IfAutoPay = aLPContSubSchema.getIfAutoPay();
		this.RenemalPayMethod = aLPContSubSchema.getRenemalPayMethod();
		this.YbkSerialno = aLPContSubSchema.getYbkSerialno();
		this.PremMult = aLPContSubSchema.getPremMult();
		this.RecordFlag = aLPContSubSchema.getRecordFlag();
		this.SYMedicalTempfeeno = aLPContSubSchema.getSYMedicalTempfeeno();
		this.DiscountMode = aLPContSubSchema.getDiscountMode();
		this.ScanCheckFlag = aLPContSubSchema.getScanCheckFlag();
		this.AgreedPayFlag = aLPContSubSchema.getAgreedPayFlag();
		this.AgreedPayDate = fDate.getDate( aLPContSubSchema.getAgreedPayDate());
		this.PolicySource = aLPContSubSchema.getPolicySource();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("EdorType") == null )
				this.EdorType = null;
			else
				this.EdorType = rs.getString("EdorType").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("CountyType") == null )
				this.CountyType = null;
			else
				this.CountyType = rs.getString("CountyType").trim();

			this.FamilySalary = rs.getDouble("FamilySalary");
			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("TaxFlag") == null )
				this.TaxFlag = null;
			else
				this.TaxFlag = rs.getString("TaxFlag").trim();

			if( rs.getString("TaxNo") == null )
				this.TaxNo = null;
			else
				this.TaxNo = rs.getString("TaxNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("TaxPayerType") == null )
				this.TaxPayerType = null;
			else
				this.TaxPayerType = rs.getString("TaxPayerType").trim();

			if( rs.getString("TaxCode") == null )
				this.TaxCode = null;
			else
				this.TaxCode = rs.getString("TaxCode").trim();

			if( rs.getString("SuccFlag") == null )
				this.SuccFlag = null;
			else
				this.SuccFlag = rs.getString("SuccFlag").trim();

			if( rs.getString("ErrorInfo") == null )
				this.ErrorInfo = null;
			else
				this.ErrorInfo = rs.getString("ErrorInfo").trim();

			if( rs.getString("BookingNo") == null )
				this.BookingNo = null;
			else
				this.BookingNo = rs.getString("BookingNo").trim();

			if( rs.getString("InsuredId") == null )
				this.InsuredId = null;
			else
				this.InsuredId = rs.getString("InsuredId").trim();

			if( rs.getString("GrpNo") == null )
				this.GrpNo = null;
			else
				this.GrpNo = rs.getString("GrpNo").trim();

			if( rs.getString("TransFlag") == null )
				this.TransFlag = null;
			else
				this.TransFlag = rs.getString("TransFlag").trim();

			if( rs.getString("CheckFlag") == null )
				this.CheckFlag = null;
			else
				this.CheckFlag = rs.getString("CheckFlag").trim();

			if( rs.getString("CheckInfo") == null )
				this.CheckInfo = null;
			else
				this.CheckInfo = rs.getString("CheckInfo").trim();

			if( rs.getString("CreditCode") == null )
				this.CreditCode = null;
			else
				this.CreditCode = rs.getString("CreditCode").trim();

			if( rs.getString("GTaxNo") == null )
				this.GTaxNo = null;
			else
				this.GTaxNo = rs.getString("GTaxNo").trim();

			if( rs.getString("GOrgancomCode") == null )
				this.GOrgancomCode = null;
			else
				this.GOrgancomCode = rs.getString("GOrgancomCode").trim();

			if( rs.getString("Uuid") == null )
				this.Uuid = null;
			else
				this.Uuid = rs.getString("Uuid").trim();

			if( rs.getString("Agediscountfactor") == null )
				this.Agediscountfactor = null;
			else
				this.Agediscountfactor = rs.getString("Agediscountfactor").trim();

			if( rs.getString("Supdiscountfactor") == null )
				this.Supdiscountfactor = null;
			else
				this.Supdiscountfactor = rs.getString("Supdiscountfactor").trim();

			if( rs.getString("Grpdiscountfactor") == null )
				this.Grpdiscountfactor = null;
			else
				this.Grpdiscountfactor = rs.getString("Grpdiscountfactor").trim();

			if( rs.getString("Totaldiscountfactor") == null )
				this.Totaldiscountfactor = null;
			else
				this.Totaldiscountfactor = rs.getString("Totaldiscountfactor").trim();

			if( rs.getString("PrintType") == null )
				this.PrintType = null;
			else
				this.PrintType = rs.getString("PrintType").trim();

			if( rs.getString("MedicalCode") == null )
				this.MedicalCode = null;
			else
				this.MedicalCode = rs.getString("MedicalCode").trim();

			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("IfAutoPay") == null )
				this.IfAutoPay = null;
			else
				this.IfAutoPay = rs.getString("IfAutoPay").trim();

			if( rs.getString("RenemalPayMethod") == null )
				this.RenemalPayMethod = null;
			else
				this.RenemalPayMethod = rs.getString("RenemalPayMethod").trim();

			if( rs.getString("YbkSerialno") == null )
				this.YbkSerialno = null;
			else
				this.YbkSerialno = rs.getString("YbkSerialno").trim();

			if( rs.getString("PremMult") == null )
				this.PremMult = null;
			else
				this.PremMult = rs.getString("PremMult").trim();

			if( rs.getString("RecordFlag") == null )
				this.RecordFlag = null;
			else
				this.RecordFlag = rs.getString("RecordFlag").trim();

			if( rs.getString("SYMedicalTempfeeno") == null )
				this.SYMedicalTempfeeno = null;
			else
				this.SYMedicalTempfeeno = rs.getString("SYMedicalTempfeeno").trim();

			if( rs.getString("DiscountMode") == null )
				this.DiscountMode = null;
			else
				this.DiscountMode = rs.getString("DiscountMode").trim();

			if( rs.getString("ScanCheckFlag") == null )
				this.ScanCheckFlag = null;
			else
				this.ScanCheckFlag = rs.getString("ScanCheckFlag").trim();

			if( rs.getString("AgreedPayFlag") == null )
				this.AgreedPayFlag = null;
			else
				this.AgreedPayFlag = rs.getString("AgreedPayFlag").trim();

			this.AgreedPayDate = rs.getDate("AgreedPayDate");
			if( rs.getString("PolicySource") == null )
				this.PolicySource = null;
			else
				this.PolicySource = rs.getString("PolicySource").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LPContSub表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LPContSubSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LPContSubSchema getSchema()
	{
		LPContSubSchema aLPContSubSchema = new LPContSubSchema();
		aLPContSubSchema.setSchema(this);
		return aLPContSubSchema;
	}

	public LPContSubDB getDB()
	{
		LPContSubDB aDBOper = new LPContSubDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPContSub描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CountyType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FamilySalary));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxPayerType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SuccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrorInfo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BookingNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckInfo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CreditCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GTaxNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GOrgancomCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Uuid)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Agediscountfactor)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Supdiscountfactor)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Grpdiscountfactor)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Totaldiscountfactor)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrintType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MedicalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IfAutoPay)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RenemalPayMethod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(YbkSerialno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremMult)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RecordFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SYMedicalTempfeeno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DiscountMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ScanCheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgreedPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AgreedPayDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicySource));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPContSub>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CountyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			FamilySalary = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			TaxFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			TaxNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			TaxPayerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			TaxCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			SuccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ErrorInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			BookingNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			InsuredId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			TransFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			CheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			CheckInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			CreditCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			GTaxNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			GOrgancomCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Uuid = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			Agediscountfactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			Supdiscountfactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			Grpdiscountfactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			Totaldiscountfactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			PrintType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			MedicalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			IfAutoPay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			RenemalPayMethod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			YbkSerialno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			PremMult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			RecordFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			SYMedicalTempfeeno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			DiscountMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			ScanCheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			AgreedPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			AgreedPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45,SysConst.PACKAGESPILTER));
			PolicySource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LPContSubSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("EdorType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("CountyType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CountyType));
		}
		if (FCode.equals("FamilySalary"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FamilySalary));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("TaxFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxFlag));
		}
		if (FCode.equals("TaxNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("TaxPayerType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxPayerType));
		}
		if (FCode.equals("TaxCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxCode));
		}
		if (FCode.equals("SuccFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SuccFlag));
		}
		if (FCode.equals("ErrorInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorInfo));
		}
		if (FCode.equals("BookingNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BookingNo));
		}
		if (FCode.equals("InsuredId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredId));
		}
		if (FCode.equals("GrpNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
		}
		if (FCode.equals("TransFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransFlag));
		}
		if (FCode.equals("CheckFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
		}
		if (FCode.equals("CheckInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckInfo));
		}
		if (FCode.equals("CreditCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CreditCode));
		}
		if (FCode.equals("GTaxNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GTaxNo));
		}
		if (FCode.equals("GOrgancomCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GOrgancomCode));
		}
		if (FCode.equals("Uuid"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Uuid));
		}
		if (FCode.equals("Agediscountfactor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Agediscountfactor));
		}
		if (FCode.equals("Supdiscountfactor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Supdiscountfactor));
		}
		if (FCode.equals("Grpdiscountfactor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Grpdiscountfactor));
		}
		if (FCode.equals("Totaldiscountfactor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Totaldiscountfactor));
		}
		if (FCode.equals("PrintType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrintType));
		}
		if (FCode.equals("MedicalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MedicalCode));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("IfAutoPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IfAutoPay));
		}
		if (FCode.equals("RenemalPayMethod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RenemalPayMethod));
		}
		if (FCode.equals("YbkSerialno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YbkSerialno));
		}
		if (FCode.equals("PremMult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremMult));
		}
		if (FCode.equals("RecordFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RecordFlag));
		}
		if (FCode.equals("SYMedicalTempfeeno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SYMedicalTempfeeno));
		}
		if (FCode.equals("DiscountMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiscountMode));
		}
		if (FCode.equals("ScanCheckFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ScanCheckFlag));
		}
		if (FCode.equals("AgreedPayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgreedPayFlag));
		}
		if (FCode.equals("AgreedPayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAgreedPayDate()));
		}
		if (FCode.equals("PolicySource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicySource));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(EdorType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CountyType);
				break;
			case 4:
				strFieldValue = String.valueOf(FamilySalary);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(TaxFlag);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(TaxNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(TaxPayerType);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(TaxCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(SuccFlag);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ErrorInfo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(BookingNo);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(InsuredId);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(GrpNo);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(TransFlag);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(CheckFlag);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(CheckInfo);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(CreditCode);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(GTaxNo);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(GOrgancomCode);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Uuid);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(Agediscountfactor);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(Supdiscountfactor);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(Grpdiscountfactor);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(Totaldiscountfactor);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(PrintType);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(MedicalCode);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(IfAutoPay);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(RenemalPayMethod);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(YbkSerialno);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(PremMult);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(RecordFlag);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(SYMedicalTempfeeno);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(DiscountMode);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(ScanCheckFlag);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(AgreedPayFlag);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAgreedPayDate()));
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(PolicySource);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorType = FValue.trim();
			}
			else
				EdorType = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("CountyType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CountyType = FValue.trim();
			}
			else
				CountyType = null;
		}
		if (FCode.equalsIgnoreCase("FamilySalary"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FamilySalary = d;
			}
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("TaxFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxFlag = FValue.trim();
			}
			else
				TaxFlag = null;
		}
		if (FCode.equalsIgnoreCase("TaxNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxNo = FValue.trim();
			}
			else
				TaxNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("TaxPayerType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxPayerType = FValue.trim();
			}
			else
				TaxPayerType = null;
		}
		if (FCode.equalsIgnoreCase("TaxCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxCode = FValue.trim();
			}
			else
				TaxCode = null;
		}
		if (FCode.equalsIgnoreCase("SuccFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SuccFlag = FValue.trim();
			}
			else
				SuccFlag = null;
		}
		if (FCode.equalsIgnoreCase("ErrorInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorInfo = FValue.trim();
			}
			else
				ErrorInfo = null;
		}
		if (FCode.equalsIgnoreCase("BookingNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BookingNo = FValue.trim();
			}
			else
				BookingNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredId = FValue.trim();
			}
			else
				InsuredId = null;
		}
		if (FCode.equalsIgnoreCase("GrpNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNo = FValue.trim();
			}
			else
				GrpNo = null;
		}
		if (FCode.equalsIgnoreCase("TransFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransFlag = FValue.trim();
			}
			else
				TransFlag = null;
		}
		if (FCode.equalsIgnoreCase("CheckFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckFlag = FValue.trim();
			}
			else
				CheckFlag = null;
		}
		if (FCode.equalsIgnoreCase("CheckInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckInfo = FValue.trim();
			}
			else
				CheckInfo = null;
		}
		if (FCode.equalsIgnoreCase("CreditCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CreditCode = FValue.trim();
			}
			else
				CreditCode = null;
		}
		if (FCode.equalsIgnoreCase("GTaxNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GTaxNo = FValue.trim();
			}
			else
				GTaxNo = null;
		}
		if (FCode.equalsIgnoreCase("GOrgancomCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GOrgancomCode = FValue.trim();
			}
			else
				GOrgancomCode = null;
		}
		if (FCode.equalsIgnoreCase("Uuid"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Uuid = FValue.trim();
			}
			else
				Uuid = null;
		}
		if (FCode.equalsIgnoreCase("Agediscountfactor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Agediscountfactor = FValue.trim();
			}
			else
				Agediscountfactor = null;
		}
		if (FCode.equalsIgnoreCase("Supdiscountfactor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Supdiscountfactor = FValue.trim();
			}
			else
				Supdiscountfactor = null;
		}
		if (FCode.equalsIgnoreCase("Grpdiscountfactor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Grpdiscountfactor = FValue.trim();
			}
			else
				Grpdiscountfactor = null;
		}
		if (FCode.equalsIgnoreCase("Totaldiscountfactor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Totaldiscountfactor = FValue.trim();
			}
			else
				Totaldiscountfactor = null;
		}
		if (FCode.equalsIgnoreCase("PrintType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrintType = FValue.trim();
			}
			else
				PrintType = null;
		}
		if (FCode.equalsIgnoreCase("MedicalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MedicalCode = FValue.trim();
			}
			else
				MedicalCode = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("IfAutoPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IfAutoPay = FValue.trim();
			}
			else
				IfAutoPay = null;
		}
		if (FCode.equalsIgnoreCase("RenemalPayMethod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RenemalPayMethod = FValue.trim();
			}
			else
				RenemalPayMethod = null;
		}
		if (FCode.equalsIgnoreCase("YbkSerialno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				YbkSerialno = FValue.trim();
			}
			else
				YbkSerialno = null;
		}
		if (FCode.equalsIgnoreCase("PremMult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremMult = FValue.trim();
			}
			else
				PremMult = null;
		}
		if (FCode.equalsIgnoreCase("RecordFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RecordFlag = FValue.trim();
			}
			else
				RecordFlag = null;
		}
		if (FCode.equalsIgnoreCase("SYMedicalTempfeeno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SYMedicalTempfeeno = FValue.trim();
			}
			else
				SYMedicalTempfeeno = null;
		}
		if (FCode.equalsIgnoreCase("DiscountMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiscountMode = FValue.trim();
			}
			else
				DiscountMode = null;
		}
		if (FCode.equalsIgnoreCase("ScanCheckFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ScanCheckFlag = FValue.trim();
			}
			else
				ScanCheckFlag = null;
		}
		if (FCode.equalsIgnoreCase("AgreedPayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgreedPayFlag = FValue.trim();
			}
			else
				AgreedPayFlag = null;
		}
		if (FCode.equalsIgnoreCase("AgreedPayDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AgreedPayDate = fDate.getDate( FValue );
			}
			else
				AgreedPayDate = null;
		}
		if (FCode.equalsIgnoreCase("PolicySource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicySource = FValue.trim();
			}
			else
				PolicySource = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LPContSubSchema other = (LPContSubSchema)otherObject;
		return
			(EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (EdorType == null ? other.getEdorType() == null : EdorType.equals(other.getEdorType()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (CountyType == null ? other.getCountyType() == null : CountyType.equals(other.getCountyType()))
			&& FamilySalary == other.getFamilySalary()
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (TaxFlag == null ? other.getTaxFlag() == null : TaxFlag.equals(other.getTaxFlag()))
			&& (TaxNo == null ? other.getTaxNo() == null : TaxNo.equals(other.getTaxNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (TaxPayerType == null ? other.getTaxPayerType() == null : TaxPayerType.equals(other.getTaxPayerType()))
			&& (TaxCode == null ? other.getTaxCode() == null : TaxCode.equals(other.getTaxCode()))
			&& (SuccFlag == null ? other.getSuccFlag() == null : SuccFlag.equals(other.getSuccFlag()))
			&& (ErrorInfo == null ? other.getErrorInfo() == null : ErrorInfo.equals(other.getErrorInfo()))
			&& (BookingNo == null ? other.getBookingNo() == null : BookingNo.equals(other.getBookingNo()))
			&& (InsuredId == null ? other.getInsuredId() == null : InsuredId.equals(other.getInsuredId()))
			&& (GrpNo == null ? other.getGrpNo() == null : GrpNo.equals(other.getGrpNo()))
			&& (TransFlag == null ? other.getTransFlag() == null : TransFlag.equals(other.getTransFlag()))
			&& (CheckFlag == null ? other.getCheckFlag() == null : CheckFlag.equals(other.getCheckFlag()))
			&& (CheckInfo == null ? other.getCheckInfo() == null : CheckInfo.equals(other.getCheckInfo()))
			&& (CreditCode == null ? other.getCreditCode() == null : CreditCode.equals(other.getCreditCode()))
			&& (GTaxNo == null ? other.getGTaxNo() == null : GTaxNo.equals(other.getGTaxNo()))
			&& (GOrgancomCode == null ? other.getGOrgancomCode() == null : GOrgancomCode.equals(other.getGOrgancomCode()))
			&& (Uuid == null ? other.getUuid() == null : Uuid.equals(other.getUuid()))
			&& (Agediscountfactor == null ? other.getAgediscountfactor() == null : Agediscountfactor.equals(other.getAgediscountfactor()))
			&& (Supdiscountfactor == null ? other.getSupdiscountfactor() == null : Supdiscountfactor.equals(other.getSupdiscountfactor()))
			&& (Grpdiscountfactor == null ? other.getGrpdiscountfactor() == null : Grpdiscountfactor.equals(other.getGrpdiscountfactor()))
			&& (Totaldiscountfactor == null ? other.getTotaldiscountfactor() == null : Totaldiscountfactor.equals(other.getTotaldiscountfactor()))
			&& (PrintType == null ? other.getPrintType() == null : PrintType.equals(other.getPrintType()))
			&& (MedicalCode == null ? other.getMedicalCode() == null : MedicalCode.equals(other.getMedicalCode()))
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (IfAutoPay == null ? other.getIfAutoPay() == null : IfAutoPay.equals(other.getIfAutoPay()))
			&& (RenemalPayMethod == null ? other.getRenemalPayMethod() == null : RenemalPayMethod.equals(other.getRenemalPayMethod()))
			&& (YbkSerialno == null ? other.getYbkSerialno() == null : YbkSerialno.equals(other.getYbkSerialno()))
			&& (PremMult == null ? other.getPremMult() == null : PremMult.equals(other.getPremMult()))
			&& (RecordFlag == null ? other.getRecordFlag() == null : RecordFlag.equals(other.getRecordFlag()))
			&& (SYMedicalTempfeeno == null ? other.getSYMedicalTempfeeno() == null : SYMedicalTempfeeno.equals(other.getSYMedicalTempfeeno()))
			&& (DiscountMode == null ? other.getDiscountMode() == null : DiscountMode.equals(other.getDiscountMode()))
			&& (ScanCheckFlag == null ? other.getScanCheckFlag() == null : ScanCheckFlag.equals(other.getScanCheckFlag()))
			&& (AgreedPayFlag == null ? other.getAgreedPayFlag() == null : AgreedPayFlag.equals(other.getAgreedPayFlag()))
			&& (AgreedPayDate == null ? other.getAgreedPayDate() == null : fDate.getString(AgreedPayDate).equals(other.getAgreedPayDate()))
			&& (PolicySource == null ? other.getPolicySource() == null : PolicySource.equals(other.getPolicySource()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("EdorType") ) {
			return 1;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 2;
		}
		if( strFieldName.equals("CountyType") ) {
			return 3;
		}
		if( strFieldName.equals("FamilySalary") ) {
			return 4;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 5;
		}
		if( strFieldName.equals("Operator") ) {
			return 6;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 7;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 8;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 10;
		}
		if( strFieldName.equals("TaxFlag") ) {
			return 11;
		}
		if( strFieldName.equals("TaxNo") ) {
			return 12;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 13;
		}
		if( strFieldName.equals("TaxPayerType") ) {
			return 14;
		}
		if( strFieldName.equals("TaxCode") ) {
			return 15;
		}
		if( strFieldName.equals("SuccFlag") ) {
			return 16;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return 17;
		}
		if( strFieldName.equals("BookingNo") ) {
			return 18;
		}
		if( strFieldName.equals("InsuredId") ) {
			return 19;
		}
		if( strFieldName.equals("GrpNo") ) {
			return 20;
		}
		if( strFieldName.equals("TransFlag") ) {
			return 21;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return 22;
		}
		if( strFieldName.equals("CheckInfo") ) {
			return 23;
		}
		if( strFieldName.equals("CreditCode") ) {
			return 24;
		}
		if( strFieldName.equals("GTaxNo") ) {
			return 25;
		}
		if( strFieldName.equals("GOrgancomCode") ) {
			return 26;
		}
		if( strFieldName.equals("Uuid") ) {
			return 27;
		}
		if( strFieldName.equals("Agediscountfactor") ) {
			return 28;
		}
		if( strFieldName.equals("Supdiscountfactor") ) {
			return 29;
		}
		if( strFieldName.equals("Grpdiscountfactor") ) {
			return 30;
		}
		if( strFieldName.equals("Totaldiscountfactor") ) {
			return 31;
		}
		if( strFieldName.equals("PrintType") ) {
			return 32;
		}
		if( strFieldName.equals("MedicalCode") ) {
			return 33;
		}
		if( strFieldName.equals("PayMode") ) {
			return 34;
		}
		if( strFieldName.equals("IfAutoPay") ) {
			return 35;
		}
		if( strFieldName.equals("RenemalPayMethod") ) {
			return 36;
		}
		if( strFieldName.equals("YbkSerialno") ) {
			return 37;
		}
		if( strFieldName.equals("PremMult") ) {
			return 38;
		}
		if( strFieldName.equals("RecordFlag") ) {
			return 39;
		}
		if( strFieldName.equals("SYMedicalTempfeeno") ) {
			return 40;
		}
		if( strFieldName.equals("DiscountMode") ) {
			return 41;
		}
		if( strFieldName.equals("ScanCheckFlag") ) {
			return 42;
		}
		if( strFieldName.equals("AgreedPayFlag") ) {
			return 43;
		}
		if( strFieldName.equals("AgreedPayDate") ) {
			return 44;
		}
		if( strFieldName.equals("PolicySource") ) {
			return 45;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "EdorType";
				break;
			case 2:
				strFieldName = "PrtNo";
				break;
			case 3:
				strFieldName = "CountyType";
				break;
			case 4:
				strFieldName = "FamilySalary";
				break;
			case 5:
				strFieldName = "ManageCom";
				break;
			case 6:
				strFieldName = "Operator";
				break;
			case 7:
				strFieldName = "MakeDate";
				break;
			case 8:
				strFieldName = "MakeTime";
				break;
			case 9:
				strFieldName = "ModifyDate";
				break;
			case 10:
				strFieldName = "ModifyTime";
				break;
			case 11:
				strFieldName = "TaxFlag";
				break;
			case 12:
				strFieldName = "TaxNo";
				break;
			case 13:
				strFieldName = "BatchNo";
				break;
			case 14:
				strFieldName = "TaxPayerType";
				break;
			case 15:
				strFieldName = "TaxCode";
				break;
			case 16:
				strFieldName = "SuccFlag";
				break;
			case 17:
				strFieldName = "ErrorInfo";
				break;
			case 18:
				strFieldName = "BookingNo";
				break;
			case 19:
				strFieldName = "InsuredId";
				break;
			case 20:
				strFieldName = "GrpNo";
				break;
			case 21:
				strFieldName = "TransFlag";
				break;
			case 22:
				strFieldName = "CheckFlag";
				break;
			case 23:
				strFieldName = "CheckInfo";
				break;
			case 24:
				strFieldName = "CreditCode";
				break;
			case 25:
				strFieldName = "GTaxNo";
				break;
			case 26:
				strFieldName = "GOrgancomCode";
				break;
			case 27:
				strFieldName = "Uuid";
				break;
			case 28:
				strFieldName = "Agediscountfactor";
				break;
			case 29:
				strFieldName = "Supdiscountfactor";
				break;
			case 30:
				strFieldName = "Grpdiscountfactor";
				break;
			case 31:
				strFieldName = "Totaldiscountfactor";
				break;
			case 32:
				strFieldName = "PrintType";
				break;
			case 33:
				strFieldName = "MedicalCode";
				break;
			case 34:
				strFieldName = "PayMode";
				break;
			case 35:
				strFieldName = "IfAutoPay";
				break;
			case 36:
				strFieldName = "RenemalPayMethod";
				break;
			case 37:
				strFieldName = "YbkSerialno";
				break;
			case 38:
				strFieldName = "PremMult";
				break;
			case 39:
				strFieldName = "RecordFlag";
				break;
			case 40:
				strFieldName = "SYMedicalTempfeeno";
				break;
			case 41:
				strFieldName = "DiscountMode";
				break;
			case 42:
				strFieldName = "ScanCheckFlag";
				break;
			case 43:
				strFieldName = "AgreedPayFlag";
				break;
			case 44:
				strFieldName = "AgreedPayDate";
				break;
			case 45:
				strFieldName = "PolicySource";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CountyType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FamilySalary") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxPayerType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SuccFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BookingNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CreditCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GTaxNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GOrgancomCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Uuid") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Agediscountfactor") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Supdiscountfactor") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Grpdiscountfactor") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Totaldiscountfactor") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrintType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MedicalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IfAutoPay") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RenemalPayMethod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("YbkSerialno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremMult") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecordFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SYMedicalTempfeeno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiscountMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ScanCheckFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgreedPayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgreedPayDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PolicySource") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
