/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCaseReceiptDB;

/*
 * <p>ClassName: LLCaseReceiptSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-10-21
 */
public class LLCaseReceiptSchema implements Schema, Cloneable {
    // @Field
    /** 账单费用明细 */
    private String FeeDetailNo;
    /** 账单号 */
    private String MainFeeNo;
    /** 立案号(申请登记号) */
    private String RgtNo;
    /** 分案号 */
    private String CaseNo;
    /** 费用项目类型 */
    private String FeeItemType;
    /** 费用项目代码 */
    private String FeeItemCode;
    /** 费用项目名称 */
    private String FeeItemName;
    /** 费用金额 */
    private double Fee;
    /** 有效标记 */
    private String AvaliFlag;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 先期给付 */
    private double PreAmnt;
    /** 自费金额 */
    private double SelfAmnt;
    /** 不合理费用 */
    private double RefuseAmnt;
    /** 社会统筹 */
    private double SocialPlanAmnt;
    /** 附加支付 */
    private double AppendAmnt;
    /** 其他机构报销 */
    private double OtherOrganAmnt;
    /** 其它 */
    private double OtherAmnt;
    /** 自费项目描述 */
    private String SelfStatement;
    /** 不合理费用描述 */
    private String RefuseStatement;
    /** 先期给付描述 */
    private String PreStatement;
    /** 全额统筹 */
    private double WholePlanFee;
    /** 部分统筹 */
    private double PartPlanFee;
    /** 自负2 */
    private double SelfPay2;
    /** 自费 */
    private double SelfFee;

    public static final int FIELDNUM = 29; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLCaseReceiptSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "FeeDetailNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LLCaseReceiptSchema cloned = (LLCaseReceiptSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getFeeDetailNo() {
        return FeeDetailNo;
    }

    public void setFeeDetailNo(String aFeeDetailNo) {
        FeeDetailNo = aFeeDetailNo;
    }

    public String getMainFeeNo() {
        return MainFeeNo;
    }

    public void setMainFeeNo(String aMainFeeNo) {
        MainFeeNo = aMainFeeNo;
    }

    public String getRgtNo() {
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo) {
        RgtNo = aRgtNo;
    }

    public String getCaseNo() {
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo) {
        CaseNo = aCaseNo;
    }

    public String getFeeItemType() {
        return FeeItemType;
    }

    public void setFeeItemType(String aFeeItemType) {
        FeeItemType = aFeeItemType;
    }

    public String getFeeItemCode() {
        return FeeItemCode;
    }

    public void setFeeItemCode(String aFeeItemCode) {
        FeeItemCode = aFeeItemCode;
    }

    public String getFeeItemName() {
        return FeeItemName;
    }

    public void setFeeItemName(String aFeeItemName) {
        FeeItemName = aFeeItemName;
    }

    public double getFee() {
        return Fee;
    }

    public void setFee(double aFee) {
        Fee = Arith.round(aFee, 2);
    }

    public void setFee(String aFee) {
        if (aFee != null && !aFee.equals("")) {
            Double tDouble = new Double(aFee);
            double d = tDouble.doubleValue();
            Fee = Arith.round(d, 2);
        }
    }

    public String getAvaliFlag() {
        return AvaliFlag;
    }

    public void setAvaliFlag(String aAvaliFlag) {
        AvaliFlag = aAvaliFlag;
    }

    public String getMngCom() {
        return MngCom;
    }

    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public double getPreAmnt() {
        return PreAmnt;
    }

    public void setPreAmnt(double aPreAmnt) {
        PreAmnt = Arith.round(aPreAmnt, 2);
    }

    public void setPreAmnt(String aPreAmnt) {
        if (aPreAmnt != null && !aPreAmnt.equals("")) {
            Double tDouble = new Double(aPreAmnt);
            double d = tDouble.doubleValue();
            PreAmnt = Arith.round(d, 2);
        }
    }

    public double getSelfAmnt() {
        return SelfAmnt;
    }

    public void setSelfAmnt(double aSelfAmnt) {
        SelfAmnt = Arith.round(aSelfAmnt, 2);
    }

    public void setSelfAmnt(String aSelfAmnt) {
        if (aSelfAmnt != null && !aSelfAmnt.equals("")) {
            Double tDouble = new Double(aSelfAmnt);
            double d = tDouble.doubleValue();
            SelfAmnt = Arith.round(d, 2);
        }
    }

    public double getRefuseAmnt() {
        return RefuseAmnt;
    }

    public void setRefuseAmnt(double aRefuseAmnt) {
        RefuseAmnt = Arith.round(aRefuseAmnt, 2);
    }

    public void setRefuseAmnt(String aRefuseAmnt) {
        if (aRefuseAmnt != null && !aRefuseAmnt.equals("")) {
            Double tDouble = new Double(aRefuseAmnt);
            double d = tDouble.doubleValue();
            RefuseAmnt = Arith.round(d, 2);
        }
    }

    public double getSocialPlanAmnt() {
        return SocialPlanAmnt;
    }

    public void setSocialPlanAmnt(double aSocialPlanAmnt) {
        SocialPlanAmnt = Arith.round(aSocialPlanAmnt, 2);
    }

    public void setSocialPlanAmnt(String aSocialPlanAmnt) {
        if (aSocialPlanAmnt != null && !aSocialPlanAmnt.equals("")) {
            Double tDouble = new Double(aSocialPlanAmnt);
            double d = tDouble.doubleValue();
            SocialPlanAmnt = Arith.round(d, 2);
        }
    }

    public double getAppendAmnt() {
        return AppendAmnt;
    }

    public void setAppendAmnt(double aAppendAmnt) {
        AppendAmnt = Arith.round(aAppendAmnt, 2);
    }

    public void setAppendAmnt(String aAppendAmnt) {
        if (aAppendAmnt != null && !aAppendAmnt.equals("")) {
            Double tDouble = new Double(aAppendAmnt);
            double d = tDouble.doubleValue();
            AppendAmnt = Arith.round(d, 2);
        }
    }

    public double getOtherOrganAmnt() {
        return OtherOrganAmnt;
    }

    public void setOtherOrganAmnt(double aOtherOrganAmnt) {
        OtherOrganAmnt = Arith.round(aOtherOrganAmnt, 2);
    }

    public void setOtherOrganAmnt(String aOtherOrganAmnt) {
        if (aOtherOrganAmnt != null && !aOtherOrganAmnt.equals("")) {
            Double tDouble = new Double(aOtherOrganAmnt);
            double d = tDouble.doubleValue();
            OtherOrganAmnt = Arith.round(d, 2);
        }
    }

    public double getOtherAmnt() {
        return OtherAmnt;
    }

    public void setOtherAmnt(double aOtherAmnt) {
        OtherAmnt = Arith.round(aOtherAmnt, 2);
    }

    public void setOtherAmnt(String aOtherAmnt) {
        if (aOtherAmnt != null && !aOtherAmnt.equals("")) {
            Double tDouble = new Double(aOtherAmnt);
            double d = tDouble.doubleValue();
            OtherAmnt = Arith.round(d, 2);
        }
    }

    public String getSelfStatement() {
        return SelfStatement;
    }

    public void setSelfStatement(String aSelfStatement) {
        SelfStatement = aSelfStatement;
    }

    public String getRefuseStatement() {
        return RefuseStatement;
    }

    public void setRefuseStatement(String aRefuseStatement) {
        RefuseStatement = aRefuseStatement;
    }

    public String getPreStatement() {
        return PreStatement;
    }

    public void setPreStatement(String aPreStatement) {
        PreStatement = aPreStatement;
    }

    public double getWholePlanFee() {
        return WholePlanFee;
    }

    public void setWholePlanFee(double aWholePlanFee) {
        WholePlanFee = Arith.round(aWholePlanFee, 2);
    }

    public void setWholePlanFee(String aWholePlanFee) {
        if (aWholePlanFee != null && !aWholePlanFee.equals("")) {
            Double tDouble = new Double(aWholePlanFee);
            double d = tDouble.doubleValue();
            WholePlanFee = Arith.round(d, 2);
        }
    }

    public double getPartPlanFee() {
        return PartPlanFee;
    }

    public void setPartPlanFee(double aPartPlanFee) {
        PartPlanFee = Arith.round(aPartPlanFee, 2);
    }

    public void setPartPlanFee(String aPartPlanFee) {
        if (aPartPlanFee != null && !aPartPlanFee.equals("")) {
            Double tDouble = new Double(aPartPlanFee);
            double d = tDouble.doubleValue();
            PartPlanFee = Arith.round(d, 2);
        }
    }

    public double getSelfPay2() {
        return SelfPay2;
    }

    public void setSelfPay2(double aSelfPay2) {
        SelfPay2 = Arith.round(aSelfPay2, 2);
    }

    public void setSelfPay2(String aSelfPay2) {
        if (aSelfPay2 != null && !aSelfPay2.equals("")) {
            Double tDouble = new Double(aSelfPay2);
            double d = tDouble.doubleValue();
            SelfPay2 = Arith.round(d, 2);
        }
    }

    public double getSelfFee() {
        return SelfFee;
    }

    public void setSelfFee(double aSelfFee) {
        SelfFee = Arith.round(aSelfFee, 2);
    }

    public void setSelfFee(String aSelfFee) {
        if (aSelfFee != null && !aSelfFee.equals("")) {
            Double tDouble = new Double(aSelfFee);
            double d = tDouble.doubleValue();
            SelfFee = Arith.round(d, 2);
        }
    }


    /**
     * 使用另外一个 LLCaseReceiptSchema 对象给 Schema 赋值
     * @param: aLLCaseReceiptSchema LLCaseReceiptSchema
     **/
    public void setSchema(LLCaseReceiptSchema aLLCaseReceiptSchema) {
        this.FeeDetailNo = aLLCaseReceiptSchema.getFeeDetailNo();
        this.MainFeeNo = aLLCaseReceiptSchema.getMainFeeNo();
        this.RgtNo = aLLCaseReceiptSchema.getRgtNo();
        this.CaseNo = aLLCaseReceiptSchema.getCaseNo();
        this.FeeItemType = aLLCaseReceiptSchema.getFeeItemType();
        this.FeeItemCode = aLLCaseReceiptSchema.getFeeItemCode();
        this.FeeItemName = aLLCaseReceiptSchema.getFeeItemName();
        this.Fee = aLLCaseReceiptSchema.getFee();
        this.AvaliFlag = aLLCaseReceiptSchema.getAvaliFlag();
        this.MngCom = aLLCaseReceiptSchema.getMngCom();
        this.Operator = aLLCaseReceiptSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLCaseReceiptSchema.getMakeDate());
        this.MakeTime = aLLCaseReceiptSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLCaseReceiptSchema.getModifyDate());
        this.ModifyTime = aLLCaseReceiptSchema.getModifyTime();
        this.PreAmnt = aLLCaseReceiptSchema.getPreAmnt();
        this.SelfAmnt = aLLCaseReceiptSchema.getSelfAmnt();
        this.RefuseAmnt = aLLCaseReceiptSchema.getRefuseAmnt();
        this.SocialPlanAmnt = aLLCaseReceiptSchema.getSocialPlanAmnt();
        this.AppendAmnt = aLLCaseReceiptSchema.getAppendAmnt();
        this.OtherOrganAmnt = aLLCaseReceiptSchema.getOtherOrganAmnt();
        this.OtherAmnt = aLLCaseReceiptSchema.getOtherAmnt();
        this.SelfStatement = aLLCaseReceiptSchema.getSelfStatement();
        this.RefuseStatement = aLLCaseReceiptSchema.getRefuseStatement();
        this.PreStatement = aLLCaseReceiptSchema.getPreStatement();
        this.WholePlanFee = aLLCaseReceiptSchema.getWholePlanFee();
        this.PartPlanFee = aLLCaseReceiptSchema.getPartPlanFee();
        this.SelfPay2 = aLLCaseReceiptSchema.getSelfPay2();
        this.SelfFee = aLLCaseReceiptSchema.getSelfFee();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("FeeDetailNo") == null) {
                this.FeeDetailNo = null;
            } else {
                this.FeeDetailNo = rs.getString("FeeDetailNo").trim();
            }

            if (rs.getString("MainFeeNo") == null) {
                this.MainFeeNo = null;
            } else {
                this.MainFeeNo = rs.getString("MainFeeNo").trim();
            }

            if (rs.getString("RgtNo") == null) {
                this.RgtNo = null;
            } else {
                this.RgtNo = rs.getString("RgtNo").trim();
            }

            if (rs.getString("CaseNo") == null) {
                this.CaseNo = null;
            } else {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("FeeItemType") == null) {
                this.FeeItemType = null;
            } else {
                this.FeeItemType = rs.getString("FeeItemType").trim();
            }

            if (rs.getString("FeeItemCode") == null) {
                this.FeeItemCode = null;
            } else {
                this.FeeItemCode = rs.getString("FeeItemCode").trim();
            }

            if (rs.getString("FeeItemName") == null) {
                this.FeeItemName = null;
            } else {
                this.FeeItemName = rs.getString("FeeItemName").trim();
            }

            this.Fee = rs.getDouble("Fee");
            if (rs.getString("AvaliFlag") == null) {
                this.AvaliFlag = null;
            } else {
                this.AvaliFlag = rs.getString("AvaliFlag").trim();
            }

            if (rs.getString("MngCom") == null) {
                this.MngCom = null;
            } else {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.PreAmnt = rs.getDouble("PreAmnt");
            this.SelfAmnt = rs.getDouble("SelfAmnt");
            this.RefuseAmnt = rs.getDouble("RefuseAmnt");
            this.SocialPlanAmnt = rs.getDouble("SocialPlanAmnt");
            this.AppendAmnt = rs.getDouble("AppendAmnt");
            this.OtherOrganAmnt = rs.getDouble("OtherOrganAmnt");
            this.OtherAmnt = rs.getDouble("OtherAmnt");
            if (rs.getString("SelfStatement") == null) {
                this.SelfStatement = null;
            } else {
                this.SelfStatement = rs.getString("SelfStatement").trim();
            }

            if (rs.getString("RefuseStatement") == null) {
                this.RefuseStatement = null;
            } else {
                this.RefuseStatement = rs.getString("RefuseStatement").trim();
            }

            if (rs.getString("PreStatement") == null) {
                this.PreStatement = null;
            } else {
                this.PreStatement = rs.getString("PreStatement").trim();
            }

            this.WholePlanFee = rs.getDouble("WholePlanFee");
            this.PartPlanFee = rs.getDouble("PartPlanFee");
            this.SelfPay2 = rs.getDouble("SelfPay2");
            this.SelfFee = rs.getDouble("SelfFee");
        } catch (SQLException sqle) {
            System.out.println("数据库中的LLCaseReceipt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseReceiptSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLCaseReceiptSchema getSchema() {
        LLCaseReceiptSchema aLLCaseReceiptSchema = new LLCaseReceiptSchema();
        aLLCaseReceiptSchema.setSchema(this);
        return aLLCaseReceiptSchema;
    }

    public LLCaseReceiptDB getDB() {
        LLCaseReceiptDB aDBOper = new LLCaseReceiptDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseReceipt描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(FeeDetailNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainFeeNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CaseNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FeeItemType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FeeItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FeeItemName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Fee));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AvaliFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MngCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PreAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SelfAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RefuseAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SocialPlanAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AppendAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OtherOrganAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OtherAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SelfStatement));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RefuseStatement));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PreStatement));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(WholePlanFee));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PartPlanFee));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SelfPay2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SelfFee));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseReceipt>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            FeeDetailNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            MainFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            FeeItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            FeeItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            FeeItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            Fee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage, 8,
                    SysConst.PACKAGESPILTER))).doubleValue();
            AvaliFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            PreAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 16, SysConst.PACKAGESPILTER))).doubleValue();
            SelfAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).doubleValue();
            RefuseAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            SocialPlanAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            AppendAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            OtherOrganAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 21, SysConst.PACKAGESPILTER))).doubleValue();
            OtherAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 22, SysConst.PACKAGESPILTER))).doubleValue();
            SelfStatement = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                           SysConst.PACKAGESPILTER);
            RefuseStatement = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             24, SysConst.PACKAGESPILTER);
            PreStatement = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                          SysConst.PACKAGESPILTER);
            WholePlanFee = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 26, SysConst.PACKAGESPILTER))).doubleValue();
            PartPlanFee = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 27, SysConst.PACKAGESPILTER))).doubleValue();
            SelfPay2 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 28, SysConst.PACKAGESPILTER))).doubleValue();
            SelfFee = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 29, SysConst.PACKAGESPILTER))).doubleValue();
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseReceiptSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("FeeDetailNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeDetailNo));
        }
        if (FCode.equals("MainFeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainFeeNo));
        }
        if (FCode.equals("RgtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
        }
        if (FCode.equals("CaseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("FeeItemType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeItemType));
        }
        if (FCode.equals("FeeItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeItemCode));
        }
        if (FCode.equals("FeeItemName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeItemName));
        }
        if (FCode.equals("Fee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fee));
        }
        if (FCode.equals("AvaliFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AvaliFlag));
        }
        if (FCode.equals("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("PreAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PreAmnt));
        }
        if (FCode.equals("SelfAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SelfAmnt));
        }
        if (FCode.equals("RefuseAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RefuseAmnt));
        }
        if (FCode.equals("SocialPlanAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SocialPlanAmnt));
        }
        if (FCode.equals("AppendAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppendAmnt));
        }
        if (FCode.equals("OtherOrganAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherOrganAmnt));
        }
        if (FCode.equals("OtherAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherAmnt));
        }
        if (FCode.equals("SelfStatement")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SelfStatement));
        }
        if (FCode.equals("RefuseStatement")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RefuseStatement));
        }
        if (FCode.equals("PreStatement")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PreStatement));
        }
        if (FCode.equals("WholePlanFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WholePlanFee));
        }
        if (FCode.equals("PartPlanFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PartPlanFee));
        }
        if (FCode.equals("SelfPay2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SelfPay2));
        }
        if (FCode.equals("SelfFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SelfFee));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(FeeDetailNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(MainFeeNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(RgtNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(CaseNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(FeeItemType);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(FeeItemCode);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(FeeItemName);
            break;
        case 7:
            strFieldValue = String.valueOf(Fee);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(AvaliFlag);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(MngCom);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 15:
            strFieldValue = String.valueOf(PreAmnt);
            break;
        case 16:
            strFieldValue = String.valueOf(SelfAmnt);
            break;
        case 17:
            strFieldValue = String.valueOf(RefuseAmnt);
            break;
        case 18:
            strFieldValue = String.valueOf(SocialPlanAmnt);
            break;
        case 19:
            strFieldValue = String.valueOf(AppendAmnt);
            break;
        case 20:
            strFieldValue = String.valueOf(OtherOrganAmnt);
            break;
        case 21:
            strFieldValue = String.valueOf(OtherAmnt);
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(SelfStatement);
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(RefuseStatement);
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(PreStatement);
            break;
        case 25:
            strFieldValue = String.valueOf(WholePlanFee);
            break;
        case 26:
            strFieldValue = String.valueOf(PartPlanFee);
            break;
        case 27:
            strFieldValue = String.valueOf(SelfPay2);
            break;
        case 28:
            strFieldValue = String.valueOf(SelfFee);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("FeeDetailNo")) {
            if (FValue != null && !FValue.equals("")) {
                FeeDetailNo = FValue.trim();
            } else {
                FeeDetailNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("MainFeeNo")) {
            if (FValue != null && !FValue.equals("")) {
                MainFeeNo = FValue.trim();
            } else {
                MainFeeNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("RgtNo")) {
            if (FValue != null && !FValue.equals("")) {
                RgtNo = FValue.trim();
            } else {
                RgtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CaseNo")) {
            if (FValue != null && !FValue.equals("")) {
                CaseNo = FValue.trim();
            } else {
                CaseNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("FeeItemType")) {
            if (FValue != null && !FValue.equals("")) {
                FeeItemType = FValue.trim();
            } else {
                FeeItemType = null;
            }
        }
        if (FCode.equalsIgnoreCase("FeeItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                FeeItemCode = FValue.trim();
            } else {
                FeeItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("FeeItemName")) {
            if (FValue != null && !FValue.equals("")) {
                FeeItemName = FValue.trim();
            } else {
                FeeItemName = null;
            }
        }
        if (FCode.equalsIgnoreCase("Fee")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Fee = d;
            }
        }
        if (FCode.equalsIgnoreCase("AvaliFlag")) {
            if (FValue != null && !FValue.equals("")) {
                AvaliFlag = FValue.trim();
            } else {
                AvaliFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if (FValue != null && !FValue.equals("")) {
                MngCom = FValue.trim();
            } else {
                MngCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("PreAmnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PreAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SelfAmnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SelfAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("RefuseAmnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RefuseAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SocialPlanAmnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SocialPlanAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("AppendAmnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AppendAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("OtherOrganAmnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                OtherOrganAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("OtherAmnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                OtherAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SelfStatement")) {
            if (FValue != null && !FValue.equals("")) {
                SelfStatement = FValue.trim();
            } else {
                SelfStatement = null;
            }
        }
        if (FCode.equalsIgnoreCase("RefuseStatement")) {
            if (FValue != null && !FValue.equals("")) {
                RefuseStatement = FValue.trim();
            } else {
                RefuseStatement = null;
            }
        }
        if (FCode.equalsIgnoreCase("PreStatement")) {
            if (FValue != null && !FValue.equals("")) {
                PreStatement = FValue.trim();
            } else {
                PreStatement = null;
            }
        }
        if (FCode.equalsIgnoreCase("WholePlanFee")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                WholePlanFee = d;
            }
        }
        if (FCode.equalsIgnoreCase("PartPlanFee")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PartPlanFee = d;
            }
        }
        if (FCode.equalsIgnoreCase("SelfPay2")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SelfPay2 = d;
            }
        }
        if (FCode.equalsIgnoreCase("SelfFee")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SelfFee = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LLCaseReceiptSchema other = (LLCaseReceiptSchema) otherObject;
        return
                FeeDetailNo.equals(other.getFeeDetailNo())
                && MainFeeNo.equals(other.getMainFeeNo())
                && RgtNo.equals(other.getRgtNo())
                && CaseNo.equals(other.getCaseNo())
                && FeeItemType.equals(other.getFeeItemType())
                && FeeItemCode.equals(other.getFeeItemCode())
                && FeeItemName.equals(other.getFeeItemName())
                && Fee == other.getFee()
                && AvaliFlag.equals(other.getAvaliFlag())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && PreAmnt == other.getPreAmnt()
                && SelfAmnt == other.getSelfAmnt()
                && RefuseAmnt == other.getRefuseAmnt()
                && SocialPlanAmnt == other.getSocialPlanAmnt()
                && AppendAmnt == other.getAppendAmnt()
                && OtherOrganAmnt == other.getOtherOrganAmnt()
                && OtherAmnt == other.getOtherAmnt()
                && SelfStatement.equals(other.getSelfStatement())
                && RefuseStatement.equals(other.getRefuseStatement())
                && PreStatement.equals(other.getPreStatement())
                && WholePlanFee == other.getWholePlanFee()
                && PartPlanFee == other.getPartPlanFee()
                && SelfPay2 == other.getSelfPay2()
                && SelfFee == other.getSelfFee();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("FeeDetailNo")) {
            return 0;
        }
        if (strFieldName.equals("MainFeeNo")) {
            return 1;
        }
        if (strFieldName.equals("RgtNo")) {
            return 2;
        }
        if (strFieldName.equals("CaseNo")) {
            return 3;
        }
        if (strFieldName.equals("FeeItemType")) {
            return 4;
        }
        if (strFieldName.equals("FeeItemCode")) {
            return 5;
        }
        if (strFieldName.equals("FeeItemName")) {
            return 6;
        }
        if (strFieldName.equals("Fee")) {
            return 7;
        }
        if (strFieldName.equals("AvaliFlag")) {
            return 8;
        }
        if (strFieldName.equals("MngCom")) {
            return 9;
        }
        if (strFieldName.equals("Operator")) {
            return 10;
        }
        if (strFieldName.equals("MakeDate")) {
            return 11;
        }
        if (strFieldName.equals("MakeTime")) {
            return 12;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 13;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 14;
        }
        if (strFieldName.equals("PreAmnt")) {
            return 15;
        }
        if (strFieldName.equals("SelfAmnt")) {
            return 16;
        }
        if (strFieldName.equals("RefuseAmnt")) {
            return 17;
        }
        if (strFieldName.equals("SocialPlanAmnt")) {
            return 18;
        }
        if (strFieldName.equals("AppendAmnt")) {
            return 19;
        }
        if (strFieldName.equals("OtherOrganAmnt")) {
            return 20;
        }
        if (strFieldName.equals("OtherAmnt")) {
            return 21;
        }
        if (strFieldName.equals("SelfStatement")) {
            return 22;
        }
        if (strFieldName.equals("RefuseStatement")) {
            return 23;
        }
        if (strFieldName.equals("PreStatement")) {
            return 24;
        }
        if (strFieldName.equals("WholePlanFee")) {
            return 25;
        }
        if (strFieldName.equals("PartPlanFee")) {
            return 26;
        }
        if (strFieldName.equals("SelfPay2")) {
            return 27;
        }
        if (strFieldName.equals("SelfFee")) {
            return 28;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "FeeDetailNo";
            break;
        case 1:
            strFieldName = "MainFeeNo";
            break;
        case 2:
            strFieldName = "RgtNo";
            break;
        case 3:
            strFieldName = "CaseNo";
            break;
        case 4:
            strFieldName = "FeeItemType";
            break;
        case 5:
            strFieldName = "FeeItemCode";
            break;
        case 6:
            strFieldName = "FeeItemName";
            break;
        case 7:
            strFieldName = "Fee";
            break;
        case 8:
            strFieldName = "AvaliFlag";
            break;
        case 9:
            strFieldName = "MngCom";
            break;
        case 10:
            strFieldName = "Operator";
            break;
        case 11:
            strFieldName = "MakeDate";
            break;
        case 12:
            strFieldName = "MakeTime";
            break;
        case 13:
            strFieldName = "ModifyDate";
            break;
        case 14:
            strFieldName = "ModifyTime";
            break;
        case 15:
            strFieldName = "PreAmnt";
            break;
        case 16:
            strFieldName = "SelfAmnt";
            break;
        case 17:
            strFieldName = "RefuseAmnt";
            break;
        case 18:
            strFieldName = "SocialPlanAmnt";
            break;
        case 19:
            strFieldName = "AppendAmnt";
            break;
        case 20:
            strFieldName = "OtherOrganAmnt";
            break;
        case 21:
            strFieldName = "OtherAmnt";
            break;
        case 22:
            strFieldName = "SelfStatement";
            break;
        case 23:
            strFieldName = "RefuseStatement";
            break;
        case 24:
            strFieldName = "PreStatement";
            break;
        case 25:
            strFieldName = "WholePlanFee";
            break;
        case 26:
            strFieldName = "PartPlanFee";
            break;
        case 27:
            strFieldName = "SelfPay2";
            break;
        case 28:
            strFieldName = "SelfFee";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("FeeDetailNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MainFeeNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeItemType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeItemName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Fee")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AvaliFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PreAmnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SelfAmnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RefuseAmnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SocialPlanAmnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AppendAmnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("OtherOrganAmnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("OtherAmnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SelfStatement")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RefuseStatement")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PreStatement")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WholePlanFee")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PartPlanFee")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SelfPay2")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SelfFee")) {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 16:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 17:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 18:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 19:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 20:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 21:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 22:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 23:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 24:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 25:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 26:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 27:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 28:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
