/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAManagerDB;

/*
 * <p>ClassName: LAManagerSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险营业部经理信息表
 * @CreateDate：2018-08-21
 */
public class LAManagerSchema implements Schema, Cloneable
{
	// @Field
	/** 用户工号 */
	private String ManagerCode;
	/** 省公司 */
	private String PManageCom;
	/** 支公司代码 */
	private String CManageCom;
	/** 营业部 */
	private String AgentGroup;
	/** 营业部编码 */
	private String BranchAttr;
	/** 姓名 */
	private String ManagerName;
	/** 性别 */
	private String Sex;
	/** 岗位职级 */
	private String ManagerGrade;
	/** 任本职级日期 */
	private Date TakeOfficeDate;
	/** 政治面貌 */
	private String PolityVisage;
	/** 出生年月 */
	private Date Birthday;
	/** 入司日期 */
	private Date InsureDate;
	/** 从事保险工作年限 */
	private String WorkYear;
	/** 参加工作时间 */
	private Date WorkDate;
	/** 毕业院校 */
	private String GraduateSchool;
	/** 毕业时间 */
	private Date GraduateDate;
	/** 所学专业 */
	private String Speciality;
	/** 最高学历 */
	private String Education;
	/** 最高学位 */
	private String Degree;
	/** 手机号码 */
	private String Mobile;
	/** 年度考核 */
	private String Assess;
	/** 兼职身份 */
	private String MulitId;
	/** 在职状态 */
	private String ManagerState;
	/** 证件类型 */
	private String IDNoType;
	/** 证件号码 */
	private String IDNO;
	/** 劳动合同影印件 */
	private String LaborContract;
	/** 展业类型 */
	private String BranchType;
	/** 销售渠道 */
	private String BranchType2;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 备注1 */
	private String B1;
	/** 备注2 */
	private String B2;
	/** 备注3 */
	private String B3;
	/** 备注4 */
	private String B4;
	/** 备注5 */
	private Date B5;
	/** 备注6 */
	private Date B6;

	public static final int FIELDNUM = 39;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAManagerSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ManagerCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAManagerSchema cloned = (LAManagerSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getManagerCode()
	{
		return ManagerCode;
	}
	public void setManagerCode(String aManagerCode)
	{
		ManagerCode = aManagerCode;
	}
	public String getPManageCom()
	{
		return PManageCom;
	}
	public void setPManageCom(String aPManageCom)
	{
		PManageCom = aPManageCom;
	}
	public String getCManageCom()
	{
		return CManageCom;
	}
	public void setCManageCom(String aCManageCom)
	{
		CManageCom = aCManageCom;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getBranchAttr()
	{
		return BranchAttr;
	}
	public void setBranchAttr(String aBranchAttr)
	{
		BranchAttr = aBranchAttr;
	}
	public String getManagerName()
	{
		return ManagerName;
	}
	public void setManagerName(String aManagerName)
	{
		ManagerName = aManagerName;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getManagerGrade()
	{
		return ManagerGrade;
	}
	public void setManagerGrade(String aManagerGrade)
	{
		ManagerGrade = aManagerGrade;
	}
	public String getTakeOfficeDate()
	{
		if( TakeOfficeDate != null )
			return fDate.getString(TakeOfficeDate);
		else
			return null;
	}
	public void setTakeOfficeDate(Date aTakeOfficeDate)
	{
		TakeOfficeDate = aTakeOfficeDate;
	}
	public void setTakeOfficeDate(String aTakeOfficeDate)
	{
		if (aTakeOfficeDate != null && !aTakeOfficeDate.equals("") )
		{
			TakeOfficeDate = fDate.getDate( aTakeOfficeDate );
		}
		else
			TakeOfficeDate = null;
	}

	public String getPolityVisage()
	{
		return PolityVisage;
	}
	public void setPolityVisage(String aPolityVisage)
	{
		PolityVisage = aPolityVisage;
	}
	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
		Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getInsureDate()
	{
		if( InsureDate != null )
			return fDate.getString(InsureDate);
		else
			return null;
	}
	public void setInsureDate(Date aInsureDate)
	{
		InsureDate = aInsureDate;
	}
	public void setInsureDate(String aInsureDate)
	{
		if (aInsureDate != null && !aInsureDate.equals("") )
		{
			InsureDate = fDate.getDate( aInsureDate );
		}
		else
			InsureDate = null;
	}

	public String getWorkYear()
	{
		return WorkYear;
	}
	public void setWorkYear(String aWorkYear)
	{
		WorkYear = aWorkYear;
	}
	public String getWorkDate()
	{
		if( WorkDate != null )
			return fDate.getString(WorkDate);
		else
			return null;
	}
	public void setWorkDate(Date aWorkDate)
	{
		WorkDate = aWorkDate;
	}
	public void setWorkDate(String aWorkDate)
	{
		if (aWorkDate != null && !aWorkDate.equals("") )
		{
			WorkDate = fDate.getDate( aWorkDate );
		}
		else
			WorkDate = null;
	}

	public String getGraduateSchool()
	{
		return GraduateSchool;
	}
	public void setGraduateSchool(String aGraduateSchool)
	{
		GraduateSchool = aGraduateSchool;
	}
	public String getGraduateDate()
	{
		if( GraduateDate != null )
			return fDate.getString(GraduateDate);
		else
			return null;
	}
	public void setGraduateDate(Date aGraduateDate)
	{
		GraduateDate = aGraduateDate;
	}
	public void setGraduateDate(String aGraduateDate)
	{
		if (aGraduateDate != null && !aGraduateDate.equals("") )
		{
			GraduateDate = fDate.getDate( aGraduateDate );
		}
		else
			GraduateDate = null;
	}

	public String getSpeciality()
	{
		return Speciality;
	}
	public void setSpeciality(String aSpeciality)
	{
		Speciality = aSpeciality;
	}
	public String getEducation()
	{
		return Education;
	}
	public void setEducation(String aEducation)
	{
		Education = aEducation;
	}
	public String getDegree()
	{
		return Degree;
	}
	public void setDegree(String aDegree)
	{
		Degree = aDegree;
	}
	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getAssess()
	{
		return Assess;
	}
	public void setAssess(String aAssess)
	{
		Assess = aAssess;
	}
	public String getMulitId()
	{
		return MulitId;
	}
	public void setMulitId(String aMulitId)
	{
		MulitId = aMulitId;
	}
	public String getManagerState()
	{
		return ManagerState;
	}
	public void setManagerState(String aManagerState)
	{
		ManagerState = aManagerState;
	}
	public String getIDNoType()
	{
		return IDNoType;
	}
	public void setIDNoType(String aIDNoType)
	{
		IDNoType = aIDNoType;
	}
	public String getIDNO()
	{
		return IDNO;
	}
	public void setIDNO(String aIDNO)
	{
		IDNO = aIDNO;
	}
	public String getLaborContract()
	{
		return LaborContract;
	}
	public void setLaborContract(String aLaborContract)
	{
		LaborContract = aLaborContract;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getB1()
	{
		return B1;
	}
	public void setB1(String aB1)
	{
		B1 = aB1;
	}
	public String getB2()
	{
		return B2;
	}
	public void setB2(String aB2)
	{
		B2 = aB2;
	}
	public String getB3()
	{
		return B3;
	}
	public void setB3(String aB3)
	{
		B3 = aB3;
	}
	public String getB4()
	{
		return B4;
	}
	public void setB4(String aB4)
	{
		B4 = aB4;
	}
	public String getB5()
	{
		if( B5 != null )
			return fDate.getString(B5);
		else
			return null;
	}
	public void setB5(Date aB5)
	{
		B5 = aB5;
	}
	public void setB5(String aB5)
	{
		if (aB5 != null && !aB5.equals("") )
		{
			B5 = fDate.getDate( aB5 );
		}
		else
			B5 = null;
	}

	public String getB6()
	{
		if( B6 != null )
			return fDate.getString(B6);
		else
			return null;
	}
	public void setB6(Date aB6)
	{
		B6 = aB6;
	}
	public void setB6(String aB6)
	{
		if (aB6 != null && !aB6.equals("") )
		{
			B6 = fDate.getDate( aB6 );
		}
		else
			B6 = null;
	}


	/**
	* 使用另外一个 LAManagerSchema 对象给 Schema 赋值
	* @param: aLAManagerSchema LAManagerSchema
	**/
	public void setSchema(LAManagerSchema aLAManagerSchema)
	{
		this.ManagerCode = aLAManagerSchema.getManagerCode();
		this.PManageCom = aLAManagerSchema.getPManageCom();
		this.CManageCom = aLAManagerSchema.getCManageCom();
		this.AgentGroup = aLAManagerSchema.getAgentGroup();
		this.BranchAttr = aLAManagerSchema.getBranchAttr();
		this.ManagerName = aLAManagerSchema.getManagerName();
		this.Sex = aLAManagerSchema.getSex();
		this.ManagerGrade = aLAManagerSchema.getManagerGrade();
		this.TakeOfficeDate = fDate.getDate( aLAManagerSchema.getTakeOfficeDate());
		this.PolityVisage = aLAManagerSchema.getPolityVisage();
		this.Birthday = fDate.getDate( aLAManagerSchema.getBirthday());
		this.InsureDate = fDate.getDate( aLAManagerSchema.getInsureDate());
		this.WorkYear = aLAManagerSchema.getWorkYear();
		this.WorkDate = fDate.getDate( aLAManagerSchema.getWorkDate());
		this.GraduateSchool = aLAManagerSchema.getGraduateSchool();
		this.GraduateDate = fDate.getDate( aLAManagerSchema.getGraduateDate());
		this.Speciality = aLAManagerSchema.getSpeciality();
		this.Education = aLAManagerSchema.getEducation();
		this.Degree = aLAManagerSchema.getDegree();
		this.Mobile = aLAManagerSchema.getMobile();
		this.Assess = aLAManagerSchema.getAssess();
		this.MulitId = aLAManagerSchema.getMulitId();
		this.ManagerState = aLAManagerSchema.getManagerState();
		this.IDNoType = aLAManagerSchema.getIDNoType();
		this.IDNO = aLAManagerSchema.getIDNO();
		this.LaborContract = aLAManagerSchema.getLaborContract();
		this.BranchType = aLAManagerSchema.getBranchType();
		this.BranchType2 = aLAManagerSchema.getBranchType2();
		this.Operator = aLAManagerSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAManagerSchema.getMakeDate());
		this.MakeTime = aLAManagerSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAManagerSchema.getModifyDate());
		this.ModifyTime = aLAManagerSchema.getModifyTime();
		this.B1 = aLAManagerSchema.getB1();
		this.B2 = aLAManagerSchema.getB2();
		this.B3 = aLAManagerSchema.getB3();
		this.B4 = aLAManagerSchema.getB4();
		this.B5 = fDate.getDate( aLAManagerSchema.getB5());
		this.B6 = fDate.getDate( aLAManagerSchema.getB6());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ManagerCode") == null )
				this.ManagerCode = null;
			else
				this.ManagerCode = rs.getString("ManagerCode").trim();

			if( rs.getString("PManageCom") == null )
				this.PManageCom = null;
			else
				this.PManageCom = rs.getString("PManageCom").trim();

			if( rs.getString("CManageCom") == null )
				this.CManageCom = null;
			else
				this.CManageCom = rs.getString("CManageCom").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("BranchAttr") == null )
				this.BranchAttr = null;
			else
				this.BranchAttr = rs.getString("BranchAttr").trim();

			if( rs.getString("ManagerName") == null )
				this.ManagerName = null;
			else
				this.ManagerName = rs.getString("ManagerName").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			if( rs.getString("ManagerGrade") == null )
				this.ManagerGrade = null;
			else
				this.ManagerGrade = rs.getString("ManagerGrade").trim();

			this.TakeOfficeDate = rs.getDate("TakeOfficeDate");
			if( rs.getString("PolityVisage") == null )
				this.PolityVisage = null;
			else
				this.PolityVisage = rs.getString("PolityVisage").trim();

			this.Birthday = rs.getDate("Birthday");
			this.InsureDate = rs.getDate("InsureDate");
			if( rs.getString("WorkYear") == null )
				this.WorkYear = null;
			else
				this.WorkYear = rs.getString("WorkYear").trim();

			this.WorkDate = rs.getDate("WorkDate");
			if( rs.getString("GraduateSchool") == null )
				this.GraduateSchool = null;
			else
				this.GraduateSchool = rs.getString("GraduateSchool").trim();

			this.GraduateDate = rs.getDate("GraduateDate");
			if( rs.getString("Speciality") == null )
				this.Speciality = null;
			else
				this.Speciality = rs.getString("Speciality").trim();

			if( rs.getString("Education") == null )
				this.Education = null;
			else
				this.Education = rs.getString("Education").trim();

			if( rs.getString("Degree") == null )
				this.Degree = null;
			else
				this.Degree = rs.getString("Degree").trim();

			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("Assess") == null )
				this.Assess = null;
			else
				this.Assess = rs.getString("Assess").trim();

			if( rs.getString("MulitId") == null )
				this.MulitId = null;
			else
				this.MulitId = rs.getString("MulitId").trim();

			if( rs.getString("ManagerState") == null )
				this.ManagerState = null;
			else
				this.ManagerState = rs.getString("ManagerState").trim();

			if( rs.getString("IDNoType") == null )
				this.IDNoType = null;
			else
				this.IDNoType = rs.getString("IDNoType").trim();

			if( rs.getString("IDNO") == null )
				this.IDNO = null;
			else
				this.IDNO = rs.getString("IDNO").trim();

			if( rs.getString("LaborContract") == null )
				this.LaborContract = null;
			else
				this.LaborContract = rs.getString("LaborContract").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("B1") == null )
				this.B1 = null;
			else
				this.B1 = rs.getString("B1").trim();

			if( rs.getString("B2") == null )
				this.B2 = null;
			else
				this.B2 = rs.getString("B2").trim();

			if( rs.getString("B3") == null )
				this.B3 = null;
			else
				this.B3 = rs.getString("B3").trim();

			if( rs.getString("B4") == null )
				this.B4 = null;
			else
				this.B4 = rs.getString("B4").trim();

			this.B5 = rs.getDate("B5");
			this.B6 = rs.getDate("B6");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAManager表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAManagerSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAManagerSchema getSchema()
	{
		LAManagerSchema aLAManagerSchema = new LAManagerSchema();
		aLAManagerSchema.setSchema(this);
		return aLAManagerSchema;
	}

	public LAManagerDB getDB()
	{
		LAManagerDB aDBOper = new LAManagerDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAManager描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ManagerCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchAttr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManagerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManagerGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TakeOfficeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolityVisage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InsureDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WorkYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( WorkDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GraduateSchool)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GraduateDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Speciality)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Education)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Degree)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Assess)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MulitId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManagerState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LaborContract)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(B1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(B2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(B3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(B4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( B5 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( B6 )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAManager>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ManagerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ManagerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ManagerGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			TakeOfficeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			PolityVisage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			InsureDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			WorkYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			WorkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			GraduateSchool = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			GraduateDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			Speciality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Education = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Degree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Assess = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			MulitId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ManagerState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			IDNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			IDNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			LaborContract = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			B1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			B2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			B3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			B4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			B5 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,SysConst.PACKAGESPILTER));
			B6 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAManagerSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ManagerCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManagerCode));
		}
		if (FCode.equals("PManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PManageCom));
		}
		if (FCode.equals("CManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CManageCom));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("BranchAttr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
		}
		if (FCode.equals("ManagerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManagerName));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("ManagerGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManagerGrade));
		}
		if (FCode.equals("TakeOfficeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTakeOfficeDate()));
		}
		if (FCode.equals("PolityVisage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolityVisage));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("InsureDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInsureDate()));
		}
		if (FCode.equals("WorkYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WorkYear));
		}
		if (FCode.equals("WorkDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getWorkDate()));
		}
		if (FCode.equals("GraduateSchool"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GraduateSchool));
		}
		if (FCode.equals("GraduateDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGraduateDate()));
		}
		if (FCode.equals("Speciality"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Speciality));
		}
		if (FCode.equals("Education"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Education));
		}
		if (FCode.equals("Degree"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("Assess"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Assess));
		}
		if (FCode.equals("MulitId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MulitId));
		}
		if (FCode.equals("ManagerState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManagerState));
		}
		if (FCode.equals("IDNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNoType));
		}
		if (FCode.equals("IDNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNO));
		}
		if (FCode.equals("LaborContract"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LaborContract));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("B1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(B1));
		}
		if (FCode.equals("B2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(B2));
		}
		if (FCode.equals("B3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(B3));
		}
		if (FCode.equals("B4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(B4));
		}
		if (FCode.equals("B5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getB5()));
		}
		if (FCode.equals("B6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getB6()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ManagerCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PManageCom);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CManageCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BranchAttr);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ManagerName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ManagerGrade);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTakeOfficeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(PolityVisage);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInsureDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(WorkYear);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getWorkDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(GraduateSchool);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGraduateDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Speciality);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Education);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Degree);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Assess);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(MulitId);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ManagerState);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(IDNoType);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(IDNO);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(LaborContract);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(B1);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(B2);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(B3);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(B4);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getB5()));
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getB6()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ManagerCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManagerCode = FValue.trim();
			}
			else
				ManagerCode = null;
		}
		if (FCode.equalsIgnoreCase("PManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PManageCom = FValue.trim();
			}
			else
				PManageCom = null;
		}
		if (FCode.equalsIgnoreCase("CManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CManageCom = FValue.trim();
			}
			else
				CManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("BranchAttr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchAttr = FValue.trim();
			}
			else
				BranchAttr = null;
		}
		if (FCode.equalsIgnoreCase("ManagerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManagerName = FValue.trim();
			}
			else
				ManagerName = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("ManagerGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManagerGrade = FValue.trim();
			}
			else
				ManagerGrade = null;
		}
		if (FCode.equalsIgnoreCase("TakeOfficeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TakeOfficeDate = fDate.getDate( FValue );
			}
			else
				TakeOfficeDate = null;
		}
		if (FCode.equalsIgnoreCase("PolityVisage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolityVisage = FValue.trim();
			}
			else
				PolityVisage = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("InsureDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InsureDate = fDate.getDate( FValue );
			}
			else
				InsureDate = null;
		}
		if (FCode.equalsIgnoreCase("WorkYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WorkYear = FValue.trim();
			}
			else
				WorkYear = null;
		}
		if (FCode.equalsIgnoreCase("WorkDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				WorkDate = fDate.getDate( FValue );
			}
			else
				WorkDate = null;
		}
		if (FCode.equalsIgnoreCase("GraduateSchool"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GraduateSchool = FValue.trim();
			}
			else
				GraduateSchool = null;
		}
		if (FCode.equalsIgnoreCase("GraduateDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GraduateDate = fDate.getDate( FValue );
			}
			else
				GraduateDate = null;
		}
		if (FCode.equalsIgnoreCase("Speciality"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Speciality = FValue.trim();
			}
			else
				Speciality = null;
		}
		if (FCode.equalsIgnoreCase("Education"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Education = FValue.trim();
			}
			else
				Education = null;
		}
		if (FCode.equalsIgnoreCase("Degree"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Degree = FValue.trim();
			}
			else
				Degree = null;
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("Assess"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Assess = FValue.trim();
			}
			else
				Assess = null;
		}
		if (FCode.equalsIgnoreCase("MulitId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MulitId = FValue.trim();
			}
			else
				MulitId = null;
		}
		if (FCode.equalsIgnoreCase("ManagerState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManagerState = FValue.trim();
			}
			else
				ManagerState = null;
		}
		if (FCode.equalsIgnoreCase("IDNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNoType = FValue.trim();
			}
			else
				IDNoType = null;
		}
		if (FCode.equalsIgnoreCase("IDNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNO = FValue.trim();
			}
			else
				IDNO = null;
		}
		if (FCode.equalsIgnoreCase("LaborContract"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LaborContract = FValue.trim();
			}
			else
				LaborContract = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("B1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				B1 = FValue.trim();
			}
			else
				B1 = null;
		}
		if (FCode.equalsIgnoreCase("B2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				B2 = FValue.trim();
			}
			else
				B2 = null;
		}
		if (FCode.equalsIgnoreCase("B3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				B3 = FValue.trim();
			}
			else
				B3 = null;
		}
		if (FCode.equalsIgnoreCase("B4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				B4 = FValue.trim();
			}
			else
				B4 = null;
		}
		if (FCode.equalsIgnoreCase("B5"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				B5 = fDate.getDate( FValue );
			}
			else
				B5 = null;
		}
		if (FCode.equalsIgnoreCase("B6"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				B6 = fDate.getDate( FValue );
			}
			else
				B6 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAManagerSchema other = (LAManagerSchema)otherObject;
		return
			(ManagerCode == null ? other.getManagerCode() == null : ManagerCode.equals(other.getManagerCode()))
			&& (PManageCom == null ? other.getPManageCom() == null : PManageCom.equals(other.getPManageCom()))
			&& (CManageCom == null ? other.getCManageCom() == null : CManageCom.equals(other.getCManageCom()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (BranchAttr == null ? other.getBranchAttr() == null : BranchAttr.equals(other.getBranchAttr()))
			&& (ManagerName == null ? other.getManagerName() == null : ManagerName.equals(other.getManagerName()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (ManagerGrade == null ? other.getManagerGrade() == null : ManagerGrade.equals(other.getManagerGrade()))
			&& (TakeOfficeDate == null ? other.getTakeOfficeDate() == null : fDate.getString(TakeOfficeDate).equals(other.getTakeOfficeDate()))
			&& (PolityVisage == null ? other.getPolityVisage() == null : PolityVisage.equals(other.getPolityVisage()))
			&& (Birthday == null ? other.getBirthday() == null : fDate.getString(Birthday).equals(other.getBirthday()))
			&& (InsureDate == null ? other.getInsureDate() == null : fDate.getString(InsureDate).equals(other.getInsureDate()))
			&& (WorkYear == null ? other.getWorkYear() == null : WorkYear.equals(other.getWorkYear()))
			&& (WorkDate == null ? other.getWorkDate() == null : fDate.getString(WorkDate).equals(other.getWorkDate()))
			&& (GraduateSchool == null ? other.getGraduateSchool() == null : GraduateSchool.equals(other.getGraduateSchool()))
			&& (GraduateDate == null ? other.getGraduateDate() == null : fDate.getString(GraduateDate).equals(other.getGraduateDate()))
			&& (Speciality == null ? other.getSpeciality() == null : Speciality.equals(other.getSpeciality()))
			&& (Education == null ? other.getEducation() == null : Education.equals(other.getEducation()))
			&& (Degree == null ? other.getDegree() == null : Degree.equals(other.getDegree()))
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (Assess == null ? other.getAssess() == null : Assess.equals(other.getAssess()))
			&& (MulitId == null ? other.getMulitId() == null : MulitId.equals(other.getMulitId()))
			&& (ManagerState == null ? other.getManagerState() == null : ManagerState.equals(other.getManagerState()))
			&& (IDNoType == null ? other.getIDNoType() == null : IDNoType.equals(other.getIDNoType()))
			&& (IDNO == null ? other.getIDNO() == null : IDNO.equals(other.getIDNO()))
			&& (LaborContract == null ? other.getLaborContract() == null : LaborContract.equals(other.getLaborContract()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (B1 == null ? other.getB1() == null : B1.equals(other.getB1()))
			&& (B2 == null ? other.getB2() == null : B2.equals(other.getB2()))
			&& (B3 == null ? other.getB3() == null : B3.equals(other.getB3()))
			&& (B4 == null ? other.getB4() == null : B4.equals(other.getB4()))
			&& (B5 == null ? other.getB5() == null : fDate.getString(B5).equals(other.getB5()))
			&& (B6 == null ? other.getB6() == null : fDate.getString(B6).equals(other.getB6()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ManagerCode") ) {
			return 0;
		}
		if( strFieldName.equals("PManageCom") ) {
			return 1;
		}
		if( strFieldName.equals("CManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 3;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return 4;
		}
		if( strFieldName.equals("ManagerName") ) {
			return 5;
		}
		if( strFieldName.equals("Sex") ) {
			return 6;
		}
		if( strFieldName.equals("ManagerGrade") ) {
			return 7;
		}
		if( strFieldName.equals("TakeOfficeDate") ) {
			return 8;
		}
		if( strFieldName.equals("PolityVisage") ) {
			return 9;
		}
		if( strFieldName.equals("Birthday") ) {
			return 10;
		}
		if( strFieldName.equals("InsureDate") ) {
			return 11;
		}
		if( strFieldName.equals("WorkYear") ) {
			return 12;
		}
		if( strFieldName.equals("WorkDate") ) {
			return 13;
		}
		if( strFieldName.equals("GraduateSchool") ) {
			return 14;
		}
		if( strFieldName.equals("GraduateDate") ) {
			return 15;
		}
		if( strFieldName.equals("Speciality") ) {
			return 16;
		}
		if( strFieldName.equals("Education") ) {
			return 17;
		}
		if( strFieldName.equals("Degree") ) {
			return 18;
		}
		if( strFieldName.equals("Mobile") ) {
			return 19;
		}
		if( strFieldName.equals("Assess") ) {
			return 20;
		}
		if( strFieldName.equals("MulitId") ) {
			return 21;
		}
		if( strFieldName.equals("ManagerState") ) {
			return 22;
		}
		if( strFieldName.equals("IDNoType") ) {
			return 23;
		}
		if( strFieldName.equals("IDNO") ) {
			return 24;
		}
		if( strFieldName.equals("LaborContract") ) {
			return 25;
		}
		if( strFieldName.equals("BranchType") ) {
			return 26;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 27;
		}
		if( strFieldName.equals("Operator") ) {
			return 28;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 29;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 30;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 31;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 32;
		}
		if( strFieldName.equals("B1") ) {
			return 33;
		}
		if( strFieldName.equals("B2") ) {
			return 34;
		}
		if( strFieldName.equals("B3") ) {
			return 35;
		}
		if( strFieldName.equals("B4") ) {
			return 36;
		}
		if( strFieldName.equals("B5") ) {
			return 37;
		}
		if( strFieldName.equals("B6") ) {
			return 38;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ManagerCode";
				break;
			case 1:
				strFieldName = "PManageCom";
				break;
			case 2:
				strFieldName = "CManageCom";
				break;
			case 3:
				strFieldName = "AgentGroup";
				break;
			case 4:
				strFieldName = "BranchAttr";
				break;
			case 5:
				strFieldName = "ManagerName";
				break;
			case 6:
				strFieldName = "Sex";
				break;
			case 7:
				strFieldName = "ManagerGrade";
				break;
			case 8:
				strFieldName = "TakeOfficeDate";
				break;
			case 9:
				strFieldName = "PolityVisage";
				break;
			case 10:
				strFieldName = "Birthday";
				break;
			case 11:
				strFieldName = "InsureDate";
				break;
			case 12:
				strFieldName = "WorkYear";
				break;
			case 13:
				strFieldName = "WorkDate";
				break;
			case 14:
				strFieldName = "GraduateSchool";
				break;
			case 15:
				strFieldName = "GraduateDate";
				break;
			case 16:
				strFieldName = "Speciality";
				break;
			case 17:
				strFieldName = "Education";
				break;
			case 18:
				strFieldName = "Degree";
				break;
			case 19:
				strFieldName = "Mobile";
				break;
			case 20:
				strFieldName = "Assess";
				break;
			case 21:
				strFieldName = "MulitId";
				break;
			case 22:
				strFieldName = "ManagerState";
				break;
			case 23:
				strFieldName = "IDNoType";
				break;
			case 24:
				strFieldName = "IDNO";
				break;
			case 25:
				strFieldName = "LaborContract";
				break;
			case 26:
				strFieldName = "BranchType";
				break;
			case 27:
				strFieldName = "BranchType2";
				break;
			case 28:
				strFieldName = "Operator";
				break;
			case 29:
				strFieldName = "MakeDate";
				break;
			case 30:
				strFieldName = "MakeTime";
				break;
			case 31:
				strFieldName = "ModifyDate";
				break;
			case 32:
				strFieldName = "ModifyTime";
				break;
			case 33:
				strFieldName = "B1";
				break;
			case 34:
				strFieldName = "B2";
				break;
			case 35:
				strFieldName = "B3";
				break;
			case 36:
				strFieldName = "B4";
				break;
			case 37:
				strFieldName = "B5";
				break;
			case 38:
				strFieldName = "B6";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ManagerCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManagerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManagerGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TakeOfficeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PolityVisage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InsureDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("WorkYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WorkDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GraduateSchool") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GraduateDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Speciality") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Education") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Degree") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Assess") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MulitId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManagerState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LaborContract") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B5") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("B6") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 38:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
