/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCProjectMonthInfoDB;

/*
 * <p>ClassName: LCProjectMonthInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2013-03-13
 */
public class LCProjectMonthInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 月度费用编号 */
	private String MonthFeeNo;
	/** 项目编码 */
	private String ProjectNo;
	/** 年度 */
	private String ProjectYear;
	/** 月度 */
	private String ProjectMonth;
	/** 费用类型 */
	private String ProjectType;
	/** 费用值 */
	private double Cost;
	/** 状态 */
	private String State;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 费用年度 */
	private String PremYear;

	public static final int FIELDNUM = 14;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCProjectMonthInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "MonthFeeNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCProjectMonthInfoSchema cloned = (LCProjectMonthInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMonthFeeNo()
	{
		return MonthFeeNo;
	}
	public void setMonthFeeNo(String aMonthFeeNo)
	{
		MonthFeeNo = aMonthFeeNo;
	}
	public String getProjectNo()
	{
		return ProjectNo;
	}
	public void setProjectNo(String aProjectNo)
	{
		ProjectNo = aProjectNo;
	}
	public String getProjectYear()
	{
		return ProjectYear;
	}
	public void setProjectYear(String aProjectYear)
	{
		ProjectYear = aProjectYear;
	}
	public String getProjectMonth()
	{
		return ProjectMonth;
	}
	public void setProjectMonth(String aProjectMonth)
	{
		ProjectMonth = aProjectMonth;
	}
	public String getProjectType()
	{
		return ProjectType;
	}
	public void setProjectType(String aProjectType)
	{
		ProjectType = aProjectType;
	}
	public double getCost()
	{
		return Cost;
	}
	public void setCost(double aCost)
	{
		Cost = Arith.round(aCost,2);
	}
	public void setCost(String aCost)
	{
		if (aCost != null && !aCost.equals(""))
		{
			Double tDouble = new Double(aCost);
			double d = tDouble.doubleValue();
                Cost = Arith.round(d,2);
		}
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getPremYear()
	{
		return PremYear;
	}
	public void setPremYear(String aPremYear)
	{
		PremYear = aPremYear;
	}

	/**
	* 使用另外一个 LCProjectMonthInfoSchema 对象给 Schema 赋值
	* @param: aLCProjectMonthInfoSchema LCProjectMonthInfoSchema
	**/
	public void setSchema(LCProjectMonthInfoSchema aLCProjectMonthInfoSchema)
	{
		this.MonthFeeNo = aLCProjectMonthInfoSchema.getMonthFeeNo();
		this.ProjectNo = aLCProjectMonthInfoSchema.getProjectNo();
		this.ProjectYear = aLCProjectMonthInfoSchema.getProjectYear();
		this.ProjectMonth = aLCProjectMonthInfoSchema.getProjectMonth();
		this.ProjectType = aLCProjectMonthInfoSchema.getProjectType();
		this.Cost = aLCProjectMonthInfoSchema.getCost();
		this.State = aLCProjectMonthInfoSchema.getState();
		this.ManageCom = aLCProjectMonthInfoSchema.getManageCom();
		this.Operator = aLCProjectMonthInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCProjectMonthInfoSchema.getMakeDate());
		this.MakeTime = aLCProjectMonthInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCProjectMonthInfoSchema.getModifyDate());
		this.ModifyTime = aLCProjectMonthInfoSchema.getModifyTime();
		this.PremYear = aLCProjectMonthInfoSchema.getPremYear();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MonthFeeNo") == null )
				this.MonthFeeNo = null;
			else
				this.MonthFeeNo = rs.getString("MonthFeeNo").trim();

			if( rs.getString("ProjectNo") == null )
				this.ProjectNo = null;
			else
				this.ProjectNo = rs.getString("ProjectNo").trim();

			if( rs.getString("ProjectYear") == null )
				this.ProjectYear = null;
			else
				this.ProjectYear = rs.getString("ProjectYear").trim();

			if( rs.getString("ProjectMonth") == null )
				this.ProjectMonth = null;
			else
				this.ProjectMonth = rs.getString("ProjectMonth").trim();

			if( rs.getString("ProjectType") == null )
				this.ProjectType = null;
			else
				this.ProjectType = rs.getString("ProjectType").trim();

			this.Cost = rs.getDouble("Cost");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("PremYear") == null )
				this.PremYear = null;
			else
				this.PremYear = rs.getString("PremYear").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCProjectMonthInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectMonthInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCProjectMonthInfoSchema getSchema()
	{
		LCProjectMonthInfoSchema aLCProjectMonthInfoSchema = new LCProjectMonthInfoSchema();
		aLCProjectMonthInfoSchema.setSchema(this);
		return aLCProjectMonthInfoSchema;
	}

	public LCProjectMonthInfoDB getDB()
	{
		LCProjectMonthInfoDB aDBOper = new LCProjectMonthInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCProjectMonthInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(MonthFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectMonth)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Cost));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremYear));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCProjectMonthInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MonthFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ProjectNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ProjectYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ProjectMonth = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ProjectType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Cost = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			PremYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectMonthInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MonthFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MonthFeeNo));
		}
		if (FCode.equals("ProjectNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectNo));
		}
		if (FCode.equals("ProjectYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectYear));
		}
		if (FCode.equals("ProjectMonth"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectMonth));
		}
		if (FCode.equals("ProjectType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectType));
		}
		if (FCode.equals("Cost"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Cost));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("PremYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremYear));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MonthFeeNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ProjectNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ProjectYear);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ProjectMonth);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ProjectType);
				break;
			case 5:
				strFieldValue = String.valueOf(Cost);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(PremYear);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MonthFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MonthFeeNo = FValue.trim();
			}
			else
				MonthFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("ProjectNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectNo = FValue.trim();
			}
			else
				ProjectNo = null;
		}
		if (FCode.equalsIgnoreCase("ProjectYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectYear = FValue.trim();
			}
			else
				ProjectYear = null;
		}
		if (FCode.equalsIgnoreCase("ProjectMonth"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectMonth = FValue.trim();
			}
			else
				ProjectMonth = null;
		}
		if (FCode.equalsIgnoreCase("ProjectType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectType = FValue.trim();
			}
			else
				ProjectType = null;
		}
		if (FCode.equalsIgnoreCase("Cost"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Cost = d;
			}
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("PremYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremYear = FValue.trim();
			}
			else
				PremYear = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCProjectMonthInfoSchema other = (LCProjectMonthInfoSchema)otherObject;
		return
			(MonthFeeNo == null ? other.getMonthFeeNo() == null : MonthFeeNo.equals(other.getMonthFeeNo()))
			&& (ProjectNo == null ? other.getProjectNo() == null : ProjectNo.equals(other.getProjectNo()))
			&& (ProjectYear == null ? other.getProjectYear() == null : ProjectYear.equals(other.getProjectYear()))
			&& (ProjectMonth == null ? other.getProjectMonth() == null : ProjectMonth.equals(other.getProjectMonth()))
			&& (ProjectType == null ? other.getProjectType() == null : ProjectType.equals(other.getProjectType()))
			&& Cost == other.getCost()
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (PremYear == null ? other.getPremYear() == null : PremYear.equals(other.getPremYear()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MonthFeeNo") ) {
			return 0;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return 1;
		}
		if( strFieldName.equals("ProjectYear") ) {
			return 2;
		}
		if( strFieldName.equals("ProjectMonth") ) {
			return 3;
		}
		if( strFieldName.equals("ProjectType") ) {
			return 4;
		}
		if( strFieldName.equals("Cost") ) {
			return 5;
		}
		if( strFieldName.equals("State") ) {
			return 6;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 7;
		}
		if( strFieldName.equals("Operator") ) {
			return 8;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 9;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 12;
		}
		if( strFieldName.equals("PremYear") ) {
			return 13;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MonthFeeNo";
				break;
			case 1:
				strFieldName = "ProjectNo";
				break;
			case 2:
				strFieldName = "ProjectYear";
				break;
			case 3:
				strFieldName = "ProjectMonth";
				break;
			case 4:
				strFieldName = "ProjectType";
				break;
			case 5:
				strFieldName = "Cost";
				break;
			case 6:
				strFieldName = "State";
				break;
			case 7:
				strFieldName = "ManageCom";
				break;
			case 8:
				strFieldName = "Operator";
				break;
			case 9:
				strFieldName = "MakeDate";
				break;
			case 10:
				strFieldName = "MakeTime";
				break;
			case 11:
				strFieldName = "ModifyDate";
				break;
			case 12:
				strFieldName = "ModifyTime";
				break;
			case 13:
				strFieldName = "PremYear";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MonthFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectMonth") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Cost") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremYear") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
