/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLScanSortDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLScanSortSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-09-15
 */
public class LLScanSortSchema implements Schema, Cloneable {
    // @Field
    /** 页编号 */
    private double PageID;
    /** 单证编号 */
    private double DocID;
    /** 页号码 */
    private double PageCode;
    /** 图像文件名 */
    private String PageName;
    /** 页状态 */
    private String PageFlag;
    /** 理赔资料类别 */
    private String SortType;
    /** 理赔资料页码 */
    private String SortPage;
    /** 理赔资料事件序号 */
    private String SortNo;
    /** 理赔事件关联号 */
    private String CaseRelaNo;
    /** 管理机构 */
    private String ManageCom;
    /** 扫描操作员 */
    private String Operator;
    /** 扫描日期 */
    private Date MakeDate;
    /** 扫描时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLScanSortSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "PageID";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LLScanSortSchema cloned = (LLScanSortSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public double getPageID() {
        return PageID;
    }

    public void setPageID(double aPageID) {
        PageID = Arith.round(aPageID, 0);
    }

    public void setPageID(String aPageID) {
        if (aPageID != null && !aPageID.equals("")) {
            Double tDouble = new Double(aPageID);
            double d = tDouble.doubleValue();
            PageID = Arith.round(d, 0);
        }
    }

    public double getDocID() {
        return DocID;
    }

    public void setDocID(double aDocID) {
        DocID = Arith.round(aDocID, 0);
    }

    public void setDocID(String aDocID) {
        if (aDocID != null && !aDocID.equals("")) {
            Double tDouble = new Double(aDocID);
            double d = tDouble.doubleValue();
            DocID = Arith.round(d, 0);
        }
    }

    public double getPageCode() {
        return PageCode;
    }

    public void setPageCode(double aPageCode) {
        PageCode = Arith.round(aPageCode, 0);
    }

    public void setPageCode(String aPageCode) {
        if (aPageCode != null && !aPageCode.equals("")) {
            Double tDouble = new Double(aPageCode);
            double d = tDouble.doubleValue();
            PageCode = Arith.round(d, 0);
        }
    }

    public String getPageName() {
        return PageName;
    }

    public void setPageName(String aPageName) {
        PageName = aPageName;
    }

    public String getPageFlag() {
        return PageFlag;
    }

    public void setPageFlag(String aPageFlag) {
        PageFlag = aPageFlag;
    }

    public String getSortType() {
        return SortType;
    }

    public void setSortType(String aSortType) {
        SortType = aSortType;
    }

    public String getSortPage() {
        return SortPage;
    }

    public void setSortPage(String aSortPage) {
        SortPage = aSortPage;
    }

    public String getSortNo() {
        return SortNo;
    }

    public void setSortNo(String aSortNo) {
        SortNo = aSortNo;
    }

    public String getCaseRelaNo() {
        return CaseRelaNo;
    }

    public void setCaseRelaNo(String aCaseRelaNo) {
        CaseRelaNo = aCaseRelaNo;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLScanSortSchema 对象给 Schema 赋值
     * @param: aLLScanSortSchema LLScanSortSchema
     **/
    public void setSchema(LLScanSortSchema aLLScanSortSchema) {
        this.PageID = aLLScanSortSchema.getPageID();
        this.DocID = aLLScanSortSchema.getDocID();
        this.PageCode = aLLScanSortSchema.getPageCode();
        this.PageName = aLLScanSortSchema.getPageName();
        this.PageFlag = aLLScanSortSchema.getPageFlag();
        this.SortType = aLLScanSortSchema.getSortType();
        this.SortPage = aLLScanSortSchema.getSortPage();
        this.SortNo = aLLScanSortSchema.getSortNo();
        this.CaseRelaNo = aLLScanSortSchema.getCaseRelaNo();
        this.ManageCom = aLLScanSortSchema.getManageCom();
        this.Operator = aLLScanSortSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLScanSortSchema.getMakeDate());
        this.MakeTime = aLLScanSortSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLScanSortSchema.getModifyDate());
        this.ModifyTime = aLLScanSortSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            this.PageID = rs.getDouble("PageID");
            this.DocID = rs.getDouble("DocID");
            this.PageCode = rs.getDouble("PageCode");
            if (rs.getString("PageName") == null) {
                this.PageName = null;
            } else {
                this.PageName = rs.getString("PageName").trim();
            }

            if (rs.getString("PageFlag") == null) {
                this.PageFlag = null;
            } else {
                this.PageFlag = rs.getString("PageFlag").trim();
            }

            if (rs.getString("SortType") == null) {
                this.SortType = null;
            } else {
                this.SortType = rs.getString("SortType").trim();
            }

            if (rs.getString("SortPage") == null) {
                this.SortPage = null;
            } else {
                this.SortPage = rs.getString("SortPage").trim();
            }

            if (rs.getString("SortNo") == null) {
                this.SortNo = null;
            } else {
                this.SortNo = rs.getString("SortNo").trim();
            }

            if (rs.getString("CaseRelaNo") == null) {
                this.CaseRelaNo = null;
            } else {
                this.CaseRelaNo = rs.getString("CaseRelaNo").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LLScanSort表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLScanSortSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LLScanSortSchema getSchema() {
        LLScanSortSchema aLLScanSortSchema = new LLScanSortSchema();
        aLLScanSortSchema.setSchema(this);
        return aLLScanSortSchema;
    }

    public LLScanSortDB getDB() {
        LLScanSortDB aDBOper = new LLScanSortDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLScanSort描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(PageID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DocID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PageCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PageName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PageFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SortType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SortPage));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SortNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CaseRelaNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLScanSort>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            PageID = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    1, SysConst.PACKAGESPILTER))).doubleValue();
            DocID = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    2, SysConst.PACKAGESPILTER))).doubleValue();
            PageCode = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).doubleValue();
            PageName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            PageFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            SortType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            SortPage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            SortNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            CaseRelaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLScanSortSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("PageID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PageID));
        }
        if (FCode.equals("DocID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocID));
        }
        if (FCode.equals("PageCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PageCode));
        }
        if (FCode.equals("PageName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PageName));
        }
        if (FCode.equals("PageFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PageFlag));
        }
        if (FCode.equals("SortType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SortType));
        }
        if (FCode.equals("SortPage")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SortPage));
        }
        if (FCode.equals("SortNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SortNo));
        }
        if (FCode.equals("CaseRelaNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseRelaNo));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = String.valueOf(PageID);
            break;
        case 1:
            strFieldValue = String.valueOf(DocID);
            break;
        case 2:
            strFieldValue = String.valueOf(PageCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(PageName);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(PageFlag);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(SortType);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(SortPage);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(SortNo);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(CaseRelaNo);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("PageID")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PageID = d;
            }
        }
        if (FCode.equalsIgnoreCase("DocID")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DocID = d;
            }
        }
        if (FCode.equalsIgnoreCase("PageCode")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PageCode = d;
            }
        }
        if (FCode.equalsIgnoreCase("PageName")) {
            if (FValue != null && !FValue.equals("")) {
                PageName = FValue.trim();
            } else {
                PageName = null;
            }
        }
        if (FCode.equalsIgnoreCase("PageFlag")) {
            if (FValue != null && !FValue.equals("")) {
                PageFlag = FValue.trim();
            } else {
                PageFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("SortType")) {
            if (FValue != null && !FValue.equals("")) {
                SortType = FValue.trim();
            } else {
                SortType = null;
            }
        }
        if (FCode.equalsIgnoreCase("SortPage")) {
            if (FValue != null && !FValue.equals("")) {
                SortPage = FValue.trim();
            } else {
                SortPage = null;
            }
        }
        if (FCode.equalsIgnoreCase("SortNo")) {
            if (FValue != null && !FValue.equals("")) {
                SortNo = FValue.trim();
            } else {
                SortNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CaseRelaNo")) {
            if (FValue != null && !FValue.equals("")) {
                CaseRelaNo = FValue.trim();
            } else {
                CaseRelaNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LLScanSortSchema other = (LLScanSortSchema) otherObject;
        return
                PageID == other.getPageID()
                && DocID == other.getDocID()
                && PageCode == other.getPageCode()
                && PageName.equals(other.getPageName())
                && PageFlag.equals(other.getPageFlag())
                && SortType.equals(other.getSortType())
                && SortPage.equals(other.getSortPage())
                && SortNo.equals(other.getSortNo())
                && CaseRelaNo.equals(other.getCaseRelaNo())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("PageID")) {
            return 0;
        }
        if (strFieldName.equals("DocID")) {
            return 1;
        }
        if (strFieldName.equals("PageCode")) {
            return 2;
        }
        if (strFieldName.equals("PageName")) {
            return 3;
        }
        if (strFieldName.equals("PageFlag")) {
            return 4;
        }
        if (strFieldName.equals("SortType")) {
            return 5;
        }
        if (strFieldName.equals("SortPage")) {
            return 6;
        }
        if (strFieldName.equals("SortNo")) {
            return 7;
        }
        if (strFieldName.equals("CaseRelaNo")) {
            return 8;
        }
        if (strFieldName.equals("ManageCom")) {
            return 9;
        }
        if (strFieldName.equals("Operator")) {
            return 10;
        }
        if (strFieldName.equals("MakeDate")) {
            return 11;
        }
        if (strFieldName.equals("MakeTime")) {
            return 12;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 13;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "PageID";
            break;
        case 1:
            strFieldName = "DocID";
            break;
        case 2:
            strFieldName = "PageCode";
            break;
        case 3:
            strFieldName = "PageName";
            break;
        case 4:
            strFieldName = "PageFlag";
            break;
        case 5:
            strFieldName = "SortType";
            break;
        case 6:
            strFieldName = "SortPage";
            break;
        case 7:
            strFieldName = "SortNo";
            break;
        case 8:
            strFieldName = "CaseRelaNo";
            break;
        case 9:
            strFieldName = "ManageCom";
            break;
        case 10:
            strFieldName = "Operator";
            break;
        case 11:
            strFieldName = "MakeDate";
            break;
        case 12:
            strFieldName = "MakeTime";
            break;
        case 13:
            strFieldName = "ModifyDate";
            break;
        case 14:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("PageID")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DocID")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PageCode")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PageName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PageFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SortType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SortPage")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SortNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseRelaNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 1:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 2:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
