/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZCardTempDB;

/*
 * <p>ClassName: LZCardTempSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2009-06-10
 */
public class LZCardTempSchema implements Schema, Cloneable
{
	// @Field
	/** 单证编码 */
	private String CertifyCode;
	/** 附标号 */
	private String SubCode;
	/** 险种编码 */
	private String RiskCode;
	/** 险种版本 */
	private String RiskVersion;
	/** 起始号 */
	private String StartNo;
	/** 终止号 */
	private String EndNo;
	/** 发放机构 */
	private String SendOutCom;
	/** 接受机构 */
	private String ReceiveCom;
	/** 数量 */
	private int SumCount;
	/** 总保费 */
	private double Prem;
	/** 总保额 */
	private double Amnt;
	/** 经办人 */
	private String Handler;
	/** 经办日期 */
	private Date HandleDate;
	/** 失效日期 */
	private Date InvaliDate;
	/** 回收清算单号 */
	private String TakeBackNo;
	/** 销售渠道 */
	private String SaleChnl;
	/** 状态标志 */
	private String StateFlag;
	/** 操作类型标志 */
	private String OperateFlag;
	/** 交费标志 */
	private String PayFlag;
	/** 到帐标志 */
	private String EnterAccFlag;
	/** 反发放原因 */
	private String Reason;
	/** 状态 */
	private String State;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 最大号 */
	private double Maxcount;
	/** 已使用号 */
	private double Overcount;
	/** 授权编号 */
	private String AuthorizeNo;
	/** 查询操作员 */
	private String QueryOper;

	public static final int FIELDNUM = 31;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LZCardTempSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[10];
		pk[0] = "CertifyCode";
		pk[1] = "SubCode";
		pk[2] = "RiskCode";
		pk[3] = "RiskVersion";
		pk[4] = "StartNo";
		pk[5] = "EndNo";
		pk[6] = "SendOutCom";
		pk[7] = "ReceiveCom";
		pk[8] = "MakeDate";
		pk[9] = "MakeTime";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LZCardTempSchema cloned = (LZCardTempSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCertifyCode()
	{
		return CertifyCode;
	}
	public void setCertifyCode(String aCertifyCode)
	{
		CertifyCode = aCertifyCode;
	}
	public String getSubCode()
	{
		return SubCode;
	}
	public void setSubCode(String aSubCode)
	{
		SubCode = aSubCode;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskVersion()
	{
		return RiskVersion;
	}
	public void setRiskVersion(String aRiskVersion)
	{
		RiskVersion = aRiskVersion;
	}
	public String getStartNo()
	{
		return StartNo;
	}
	public void setStartNo(String aStartNo)
	{
		StartNo = aStartNo;
	}
	public String getEndNo()
	{
		return EndNo;
	}
	public void setEndNo(String aEndNo)
	{
		EndNo = aEndNo;
	}
	public String getSendOutCom()
	{
		return SendOutCom;
	}
	public void setSendOutCom(String aSendOutCom)
	{
		SendOutCom = aSendOutCom;
	}
	public String getReceiveCom()
	{
		return ReceiveCom;
	}
	public void setReceiveCom(String aReceiveCom)
	{
		ReceiveCom = aReceiveCom;
	}
	public int getSumCount()
	{
		return SumCount;
	}
	public void setSumCount(int aSumCount)
	{
		SumCount = aSumCount;
	}
	public void setSumCount(String aSumCount)
	{
		if (aSumCount != null && !aSumCount.equals(""))
		{
			Integer tInteger = new Integer(aSumCount);
			int i = tInteger.intValue();
			SumCount = i;
		}
	}

	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
		Prem = Arith.round(aPrem,2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d,2);
		}
	}

	public double getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(double aAmnt)
	{
		Amnt = Arith.round(aAmnt,2);
	}
	public void setAmnt(String aAmnt)
	{
		if (aAmnt != null && !aAmnt.equals(""))
		{
			Double tDouble = new Double(aAmnt);
			double d = tDouble.doubleValue();
                Amnt = Arith.round(d,2);
		}
	}

	public String getHandler()
	{
		return Handler;
	}
	public void setHandler(String aHandler)
	{
		Handler = aHandler;
	}
	public String getHandleDate()
	{
		if( HandleDate != null )
			return fDate.getString(HandleDate);
		else
			return null;
	}
	public void setHandleDate(Date aHandleDate)
	{
		HandleDate = aHandleDate;
	}
	public void setHandleDate(String aHandleDate)
	{
		if (aHandleDate != null && !aHandleDate.equals("") )
		{
			HandleDate = fDate.getDate( aHandleDate );
		}
		else
			HandleDate = null;
	}

	public String getInvaliDate()
	{
		if( InvaliDate != null )
			return fDate.getString(InvaliDate);
		else
			return null;
	}
	public void setInvaliDate(Date aInvaliDate)
	{
		InvaliDate = aInvaliDate;
	}
	public void setInvaliDate(String aInvaliDate)
	{
		if (aInvaliDate != null && !aInvaliDate.equals("") )
		{
			InvaliDate = fDate.getDate( aInvaliDate );
		}
		else
			InvaliDate = null;
	}

	public String getTakeBackNo()
	{
		return TakeBackNo;
	}
	public void setTakeBackNo(String aTakeBackNo)
	{
		TakeBackNo = aTakeBackNo;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public String getOperateFlag()
	{
		return OperateFlag;
	}
	public void setOperateFlag(String aOperateFlag)
	{
		OperateFlag = aOperateFlag;
	}
	public String getPayFlag()
	{
		return PayFlag;
	}
	public void setPayFlag(String aPayFlag)
	{
		PayFlag = aPayFlag;
	}
	public String getEnterAccFlag()
	{
		return EnterAccFlag;
	}
	public void setEnterAccFlag(String aEnterAccFlag)
	{
		EnterAccFlag = aEnterAccFlag;
	}
	public String getReason()
	{
		return Reason;
	}
	public void setReason(String aReason)
	{
		Reason = aReason;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public double getMaxcount()
	{
		return Maxcount;
	}
	public void setMaxcount(double aMaxcount)
	{
		Maxcount = Arith.round(aMaxcount,0);
	}
	public void setMaxcount(String aMaxcount)
	{
		if (aMaxcount != null && !aMaxcount.equals(""))
		{
			Double tDouble = new Double(aMaxcount);
			double d = tDouble.doubleValue();
                Maxcount = Arith.round(d,0);
		}
	}

	public double getOvercount()
	{
		return Overcount;
	}
	public void setOvercount(double aOvercount)
	{
		Overcount = Arith.round(aOvercount,0);
	}
	public void setOvercount(String aOvercount)
	{
		if (aOvercount != null && !aOvercount.equals(""))
		{
			Double tDouble = new Double(aOvercount);
			double d = tDouble.doubleValue();
                Overcount = Arith.round(d,0);
		}
	}

	public String getAuthorizeNo()
	{
		return AuthorizeNo;
	}
	public void setAuthorizeNo(String aAuthorizeNo)
	{
		AuthorizeNo = aAuthorizeNo;
	}
	public String getQueryOper()
	{
		return QueryOper;
	}
	public void setQueryOper(String aQueryOper)
	{
		QueryOper = aQueryOper;
	}

	/**
	* 使用另外一个 LZCardTempSchema 对象给 Schema 赋值
	* @param: aLZCardTempSchema LZCardTempSchema
	**/
	public void setSchema(LZCardTempSchema aLZCardTempSchema)
	{
		this.CertifyCode = aLZCardTempSchema.getCertifyCode();
		this.SubCode = aLZCardTempSchema.getSubCode();
		this.RiskCode = aLZCardTempSchema.getRiskCode();
		this.RiskVersion = aLZCardTempSchema.getRiskVersion();
		this.StartNo = aLZCardTempSchema.getStartNo();
		this.EndNo = aLZCardTempSchema.getEndNo();
		this.SendOutCom = aLZCardTempSchema.getSendOutCom();
		this.ReceiveCom = aLZCardTempSchema.getReceiveCom();
		this.SumCount = aLZCardTempSchema.getSumCount();
		this.Prem = aLZCardTempSchema.getPrem();
		this.Amnt = aLZCardTempSchema.getAmnt();
		this.Handler = aLZCardTempSchema.getHandler();
		this.HandleDate = fDate.getDate( aLZCardTempSchema.getHandleDate());
		this.InvaliDate = fDate.getDate( aLZCardTempSchema.getInvaliDate());
		this.TakeBackNo = aLZCardTempSchema.getTakeBackNo();
		this.SaleChnl = aLZCardTempSchema.getSaleChnl();
		this.StateFlag = aLZCardTempSchema.getStateFlag();
		this.OperateFlag = aLZCardTempSchema.getOperateFlag();
		this.PayFlag = aLZCardTempSchema.getPayFlag();
		this.EnterAccFlag = aLZCardTempSchema.getEnterAccFlag();
		this.Reason = aLZCardTempSchema.getReason();
		this.State = aLZCardTempSchema.getState();
		this.Operator = aLZCardTempSchema.getOperator();
		this.MakeDate = fDate.getDate( aLZCardTempSchema.getMakeDate());
		this.MakeTime = aLZCardTempSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLZCardTempSchema.getModifyDate());
		this.ModifyTime = aLZCardTempSchema.getModifyTime();
		this.Maxcount = aLZCardTempSchema.getMaxcount();
		this.Overcount = aLZCardTempSchema.getOvercount();
		this.AuthorizeNo = aLZCardTempSchema.getAuthorizeNo();
		this.QueryOper = aLZCardTempSchema.getQueryOper();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CertifyCode") == null )
				this.CertifyCode = null;
			else
				this.CertifyCode = rs.getString("CertifyCode").trim();

			if( rs.getString("SubCode") == null )
				this.SubCode = null;
			else
				this.SubCode = rs.getString("SubCode").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskVersion") == null )
				this.RiskVersion = null;
			else
				this.RiskVersion = rs.getString("RiskVersion").trim();

			if( rs.getString("StartNo") == null )
				this.StartNo = null;
			else
				this.StartNo = rs.getString("StartNo").trim();

			if( rs.getString("EndNo") == null )
				this.EndNo = null;
			else
				this.EndNo = rs.getString("EndNo").trim();

			if( rs.getString("SendOutCom") == null )
				this.SendOutCom = null;
			else
				this.SendOutCom = rs.getString("SendOutCom").trim();

			if( rs.getString("ReceiveCom") == null )
				this.ReceiveCom = null;
			else
				this.ReceiveCom = rs.getString("ReceiveCom").trim();

			this.SumCount = rs.getInt("SumCount");
			this.Prem = rs.getDouble("Prem");
			this.Amnt = rs.getDouble("Amnt");
			if( rs.getString("Handler") == null )
				this.Handler = null;
			else
				this.Handler = rs.getString("Handler").trim();

			this.HandleDate = rs.getDate("HandleDate");
			this.InvaliDate = rs.getDate("InvaliDate");
			if( rs.getString("TakeBackNo") == null )
				this.TakeBackNo = null;
			else
				this.TakeBackNo = rs.getString("TakeBackNo").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			if( rs.getString("OperateFlag") == null )
				this.OperateFlag = null;
			else
				this.OperateFlag = rs.getString("OperateFlag").trim();

			if( rs.getString("PayFlag") == null )
				this.PayFlag = null;
			else
				this.PayFlag = rs.getString("PayFlag").trim();

			if( rs.getString("EnterAccFlag") == null )
				this.EnterAccFlag = null;
			else
				this.EnterAccFlag = rs.getString("EnterAccFlag").trim();

			if( rs.getString("Reason") == null )
				this.Reason = null;
			else
				this.Reason = rs.getString("Reason").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.Maxcount = rs.getDouble("Maxcount");
			this.Overcount = rs.getDouble("Overcount");
			if( rs.getString("AuthorizeNo") == null )
				this.AuthorizeNo = null;
			else
				this.AuthorizeNo = rs.getString("AuthorizeNo").trim();

			if( rs.getString("QueryOper") == null )
				this.QueryOper = null;
			else
				this.QueryOper = rs.getString("QueryOper").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LZCardTemp表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LZCardTempSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LZCardTempSchema getSchema()
	{
		LZCardTempSchema aLZCardTempSchema = new LZCardTempSchema();
		aLZCardTempSchema.setSchema(this);
		return aLZCardTempSchema;
	}

	public LZCardTempDB getDB()
	{
		LZCardTempDB aDBOper = new LZCardTempDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardTemp描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StartNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EndNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendOutCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiveCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HandleDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InvaliDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TakeBackNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OperateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EnterAccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Reason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Maxcount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Overcount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AuthorizeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(QueryOper));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardTemp>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SubCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			StartNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			EndNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			SendOutCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ReceiveCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			SumCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			HandleDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			InvaliDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			TakeBackNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			OperateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			PayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			EnterAccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Maxcount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			Overcount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			AuthorizeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			QueryOper = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LZCardTempSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CertifyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
		}
		if (FCode.equals("SubCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubCode));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskVersion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
		}
		if (FCode.equals("StartNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StartNo));
		}
		if (FCode.equals("EndNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndNo));
		}
		if (FCode.equals("SendOutCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendOutCom));
		}
		if (FCode.equals("ReceiveCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveCom));
		}
		if (FCode.equals("SumCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumCount));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("Handler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
		}
		if (FCode.equals("HandleDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHandleDate()));
		}
		if (FCode.equals("InvaliDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInvaliDate()));
		}
		if (FCode.equals("TakeBackNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TakeBackNo));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("OperateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OperateFlag));
		}
		if (FCode.equals("PayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayFlag));
		}
		if (FCode.equals("EnterAccFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EnterAccFlag));
		}
		if (FCode.equals("Reason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reason));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Maxcount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Maxcount));
		}
		if (FCode.equals("Overcount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Overcount));
		}
		if (FCode.equals("AuthorizeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AuthorizeNo));
		}
		if (FCode.equals("QueryOper"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(QueryOper));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CertifyCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SubCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RiskVersion);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(StartNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(EndNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(SendOutCom);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ReceiveCom);
				break;
			case 8:
				strFieldValue = String.valueOf(SumCount);
				break;
			case 9:
				strFieldValue = String.valueOf(Prem);
				break;
			case 10:
				strFieldValue = String.valueOf(Amnt);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Handler);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHandleDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInvaliDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(TakeBackNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(OperateFlag);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(PayFlag);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(EnterAccFlag);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Reason);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 27:
				strFieldValue = String.valueOf(Maxcount);
				break;
			case 28:
				strFieldValue = String.valueOf(Overcount);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(AuthorizeNo);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(QueryOper);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CertifyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyCode = FValue.trim();
			}
			else
				CertifyCode = null;
		}
		if (FCode.equalsIgnoreCase("SubCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubCode = FValue.trim();
			}
			else
				SubCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskVersion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskVersion = FValue.trim();
			}
			else
				RiskVersion = null;
		}
		if (FCode.equalsIgnoreCase("StartNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StartNo = FValue.trim();
			}
			else
				StartNo = null;
		}
		if (FCode.equalsIgnoreCase("EndNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndNo = FValue.trim();
			}
			else
				EndNo = null;
		}
		if (FCode.equalsIgnoreCase("SendOutCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendOutCom = FValue.trim();
			}
			else
				SendOutCom = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveCom = FValue.trim();
			}
			else
				ReceiveCom = null;
		}
		if (FCode.equalsIgnoreCase("SumCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SumCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Amnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("Handler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler = FValue.trim();
			}
			else
				Handler = null;
		}
		if (FCode.equalsIgnoreCase("HandleDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HandleDate = fDate.getDate( FValue );
			}
			else
				HandleDate = null;
		}
		if (FCode.equalsIgnoreCase("InvaliDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InvaliDate = fDate.getDate( FValue );
			}
			else
				InvaliDate = null;
		}
		if (FCode.equalsIgnoreCase("TakeBackNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TakeBackNo = FValue.trim();
			}
			else
				TakeBackNo = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("OperateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OperateFlag = FValue.trim();
			}
			else
				OperateFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayFlag = FValue.trim();
			}
			else
				PayFlag = null;
		}
		if (FCode.equalsIgnoreCase("EnterAccFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EnterAccFlag = FValue.trim();
			}
			else
				EnterAccFlag = null;
		}
		if (FCode.equalsIgnoreCase("Reason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Reason = FValue.trim();
			}
			else
				Reason = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Maxcount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Maxcount = d;
			}
		}
		if (FCode.equalsIgnoreCase("Overcount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Overcount = d;
			}
		}
		if (FCode.equalsIgnoreCase("AuthorizeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AuthorizeNo = FValue.trim();
			}
			else
				AuthorizeNo = null;
		}
		if (FCode.equalsIgnoreCase("QueryOper"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				QueryOper = FValue.trim();
			}
			else
				QueryOper = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LZCardTempSchema other = (LZCardTempSchema)otherObject;
		return
			(CertifyCode == null ? other.getCertifyCode() == null : CertifyCode.equals(other.getCertifyCode()))
			&& (SubCode == null ? other.getSubCode() == null : SubCode.equals(other.getSubCode()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (RiskVersion == null ? other.getRiskVersion() == null : RiskVersion.equals(other.getRiskVersion()))
			&& (StartNo == null ? other.getStartNo() == null : StartNo.equals(other.getStartNo()))
			&& (EndNo == null ? other.getEndNo() == null : EndNo.equals(other.getEndNo()))
			&& (SendOutCom == null ? other.getSendOutCom() == null : SendOutCom.equals(other.getSendOutCom()))
			&& (ReceiveCom == null ? other.getReceiveCom() == null : ReceiveCom.equals(other.getReceiveCom()))
			&& SumCount == other.getSumCount()
			&& Prem == other.getPrem()
			&& Amnt == other.getAmnt()
			&& (Handler == null ? other.getHandler() == null : Handler.equals(other.getHandler()))
			&& (HandleDate == null ? other.getHandleDate() == null : fDate.getString(HandleDate).equals(other.getHandleDate()))
			&& (InvaliDate == null ? other.getInvaliDate() == null : fDate.getString(InvaliDate).equals(other.getInvaliDate()))
			&& (TakeBackNo == null ? other.getTakeBackNo() == null : TakeBackNo.equals(other.getTakeBackNo()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& (OperateFlag == null ? other.getOperateFlag() == null : OperateFlag.equals(other.getOperateFlag()))
			&& (PayFlag == null ? other.getPayFlag() == null : PayFlag.equals(other.getPayFlag()))
			&& (EnterAccFlag == null ? other.getEnterAccFlag() == null : EnterAccFlag.equals(other.getEnterAccFlag()))
			&& (Reason == null ? other.getReason() == null : Reason.equals(other.getReason()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& Maxcount == other.getMaxcount()
			&& Overcount == other.getOvercount()
			&& (AuthorizeNo == null ? other.getAuthorizeNo() == null : AuthorizeNo.equals(other.getAuthorizeNo()))
			&& (QueryOper == null ? other.getQueryOper() == null : QueryOper.equals(other.getQueryOper()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CertifyCode") ) {
			return 0;
		}
		if( strFieldName.equals("SubCode") ) {
			return 1;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 2;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return 3;
		}
		if( strFieldName.equals("StartNo") ) {
			return 4;
		}
		if( strFieldName.equals("EndNo") ) {
			return 5;
		}
		if( strFieldName.equals("SendOutCom") ) {
			return 6;
		}
		if( strFieldName.equals("ReceiveCom") ) {
			return 7;
		}
		if( strFieldName.equals("SumCount") ) {
			return 8;
		}
		if( strFieldName.equals("Prem") ) {
			return 9;
		}
		if( strFieldName.equals("Amnt") ) {
			return 10;
		}
		if( strFieldName.equals("Handler") ) {
			return 11;
		}
		if( strFieldName.equals("HandleDate") ) {
			return 12;
		}
		if( strFieldName.equals("InvaliDate") ) {
			return 13;
		}
		if( strFieldName.equals("TakeBackNo") ) {
			return 14;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 15;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 16;
		}
		if( strFieldName.equals("OperateFlag") ) {
			return 17;
		}
		if( strFieldName.equals("PayFlag") ) {
			return 18;
		}
		if( strFieldName.equals("EnterAccFlag") ) {
			return 19;
		}
		if( strFieldName.equals("Reason") ) {
			return 20;
		}
		if( strFieldName.equals("State") ) {
			return 21;
		}
		if( strFieldName.equals("Operator") ) {
			return 22;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 23;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 25;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 26;
		}
		if( strFieldName.equals("Maxcount") ) {
			return 27;
		}
		if( strFieldName.equals("Overcount") ) {
			return 28;
		}
		if( strFieldName.equals("AuthorizeNo") ) {
			return 29;
		}
		if( strFieldName.equals("QueryOper") ) {
			return 30;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CertifyCode";
				break;
			case 1:
				strFieldName = "SubCode";
				break;
			case 2:
				strFieldName = "RiskCode";
				break;
			case 3:
				strFieldName = "RiskVersion";
				break;
			case 4:
				strFieldName = "StartNo";
				break;
			case 5:
				strFieldName = "EndNo";
				break;
			case 6:
				strFieldName = "SendOutCom";
				break;
			case 7:
				strFieldName = "ReceiveCom";
				break;
			case 8:
				strFieldName = "SumCount";
				break;
			case 9:
				strFieldName = "Prem";
				break;
			case 10:
				strFieldName = "Amnt";
				break;
			case 11:
				strFieldName = "Handler";
				break;
			case 12:
				strFieldName = "HandleDate";
				break;
			case 13:
				strFieldName = "InvaliDate";
				break;
			case 14:
				strFieldName = "TakeBackNo";
				break;
			case 15:
				strFieldName = "SaleChnl";
				break;
			case 16:
				strFieldName = "StateFlag";
				break;
			case 17:
				strFieldName = "OperateFlag";
				break;
			case 18:
				strFieldName = "PayFlag";
				break;
			case 19:
				strFieldName = "EnterAccFlag";
				break;
			case 20:
				strFieldName = "Reason";
				break;
			case 21:
				strFieldName = "State";
				break;
			case 22:
				strFieldName = "Operator";
				break;
			case 23:
				strFieldName = "MakeDate";
				break;
			case 24:
				strFieldName = "MakeTime";
				break;
			case 25:
				strFieldName = "ModifyDate";
				break;
			case 26:
				strFieldName = "ModifyTime";
				break;
			case 27:
				strFieldName = "Maxcount";
				break;
			case 28:
				strFieldName = "Overcount";
				break;
			case 29:
				strFieldName = "AuthorizeNo";
				break;
			case 30:
				strFieldName = "QueryOper";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CertifyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendOutCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Handler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandleDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InvaliDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TakeBackNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OperateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnterAccFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Reason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Maxcount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Overcount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AuthorizeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("QueryOper") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
