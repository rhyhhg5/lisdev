/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATrainerDB;

/*
 * <p>ClassName: LATrainerSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险培训组训信息表
 * @CreateDate：2018-08-21
 */
public class LATrainerSchema implements Schema, Cloneable
{
	// @Field
	/** 用户工号 */
	private String TrainerNo;
	/** 省公司 */
	private String PManageCom;
	/** 支公司代码 */
	private String CManageCom;
	/** 营业部 */
	private String AgentGroup;
	/** 组训姓名 */
	private String TrainerName;
	/** 组训性别 */
	private String Sex;
	/** 组训职级 */
	private String TrainerGrade;
	/** 政治面貌 */
	private String PolityVisage;
	/** 出生年月 */
	private Date Birthday;
	/** 参加工作时间 */
	private Date WorkDate;
	/** 从事保险工作时间 */
	private Date InsureDate;
	/** 毕业院校 */
	private String GraduateSchool;
	/** 所学专业 */
	private String Speciality;
	/** 最高学历 */
	private String Education;
	/** 最高学位 */
	private String Degree;
	/** 手机号码 */
	private String Mobile;
	/** 年度考核 */
	private String Assess;
	/** 兼职身份 */
	private String MulitId;
	/** 在职状态 */
	private String TrainerState;
	/** 入司前保险工作年限 */
	private String WorkYear;
	/** 展业类型 */
	private String BranchType;
	/** 销售渠道 */
	private String BranchType2;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 营业部经理姓名 */
	private String BusinessManagerName;
	/** 任本职级日期 */
	private Date TakeOfficeDate;
	/** 劳动合同影印件 */
	private String LaborContract;
	/** 证件类型 */
	private String IDNoType;
	/** 证件号码 */
	private String IDNO;
	/** 组训人员工号 */
	private String TrainerCode;
	/** 组训状态 */
	private String TrainerFlag;
	/** 备注1 */
	private String B1;
	/** 备注2 */
	private String B2;
	/** 备注3 */
	private String B3;
	/** 备注4 */
	private String B4;
	/** 备注5 */
	private String B5;
	/** 备注6 */
	private String B6;
	/** 组训入职日期 */
	private Date TrainerWorkDate;

	public static final int FIELDNUM = 41;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LATrainerSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "TrainerNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LATrainerSchema cloned = (LATrainerSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTrainerNo()
	{
		return TrainerNo;
	}
	public void setTrainerNo(String aTrainerNo)
	{
		TrainerNo = aTrainerNo;
	}
	public String getPManageCom()
	{
		return PManageCom;
	}
	public void setPManageCom(String aPManageCom)
	{
		PManageCom = aPManageCom;
	}
	public String getCManageCom()
	{
		return CManageCom;
	}
	public void setCManageCom(String aCManageCom)
	{
		CManageCom = aCManageCom;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getTrainerName()
	{
		return TrainerName;
	}
	public void setTrainerName(String aTrainerName)
	{
		TrainerName = aTrainerName;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getTrainerGrade()
	{
		return TrainerGrade;
	}
	public void setTrainerGrade(String aTrainerGrade)
	{
		TrainerGrade = aTrainerGrade;
	}
	public String getPolityVisage()
	{
		return PolityVisage;
	}
	public void setPolityVisage(String aPolityVisage)
	{
		PolityVisage = aPolityVisage;
	}
	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
		Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getWorkDate()
	{
		if( WorkDate != null )
			return fDate.getString(WorkDate);
		else
			return null;
	}
	public void setWorkDate(Date aWorkDate)
	{
		WorkDate = aWorkDate;
	}
	public void setWorkDate(String aWorkDate)
	{
		if (aWorkDate != null && !aWorkDate.equals("") )
		{
			WorkDate = fDate.getDate( aWorkDate );
		}
		else
			WorkDate = null;
	}

	public String getInsureDate()
	{
		if( InsureDate != null )
			return fDate.getString(InsureDate);
		else
			return null;
	}
	public void setInsureDate(Date aInsureDate)
	{
		InsureDate = aInsureDate;
	}
	public void setInsureDate(String aInsureDate)
	{
		if (aInsureDate != null && !aInsureDate.equals("") )
		{
			InsureDate = fDate.getDate( aInsureDate );
		}
		else
			InsureDate = null;
	}

	public String getGraduateSchool()
	{
		return GraduateSchool;
	}
	public void setGraduateSchool(String aGraduateSchool)
	{
		GraduateSchool = aGraduateSchool;
	}
	public String getSpeciality()
	{
		return Speciality;
	}
	public void setSpeciality(String aSpeciality)
	{
		Speciality = aSpeciality;
	}
	public String getEducation()
	{
		return Education;
	}
	public void setEducation(String aEducation)
	{
		Education = aEducation;
	}
	public String getDegree()
	{
		return Degree;
	}
	public void setDegree(String aDegree)
	{
		Degree = aDegree;
	}
	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getAssess()
	{
		return Assess;
	}
	public void setAssess(String aAssess)
	{
		Assess = aAssess;
	}
	public String getMulitId()
	{
		return MulitId;
	}
	public void setMulitId(String aMulitId)
	{
		MulitId = aMulitId;
	}
	public String getTrainerState()
	{
		return TrainerState;
	}
	public void setTrainerState(String aTrainerState)
	{
		TrainerState = aTrainerState;
	}
	public String getWorkYear()
	{
		return WorkYear;
	}
	public void setWorkYear(String aWorkYear)
	{
		WorkYear = aWorkYear;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBusinessManagerName()
	{
		return BusinessManagerName;
	}
	public void setBusinessManagerName(String aBusinessManagerName)
	{
		BusinessManagerName = aBusinessManagerName;
	}
	public String getTakeOfficeDate()
	{
		if( TakeOfficeDate != null )
			return fDate.getString(TakeOfficeDate);
		else
			return null;
	}
	public void setTakeOfficeDate(Date aTakeOfficeDate)
	{
		TakeOfficeDate = aTakeOfficeDate;
	}
	public void setTakeOfficeDate(String aTakeOfficeDate)
	{
		if (aTakeOfficeDate != null && !aTakeOfficeDate.equals("") )
		{
			TakeOfficeDate = fDate.getDate( aTakeOfficeDate );
		}
		else
			TakeOfficeDate = null;
	}

	public String getLaborContract()
	{
		return LaborContract;
	}
	public void setLaborContract(String aLaborContract)
	{
		LaborContract = aLaborContract;
	}
	public String getIDNoType()
	{
		return IDNoType;
	}
	public void setIDNoType(String aIDNoType)
	{
		IDNoType = aIDNoType;
	}
	public String getIDNO()
	{
		return IDNO;
	}
	public void setIDNO(String aIDNO)
	{
		IDNO = aIDNO;
	}
	public String getTrainerCode()
	{
		return TrainerCode;
	}
	public void setTrainerCode(String aTrainerCode)
	{
		TrainerCode = aTrainerCode;
	}
	public String getTrainerFlag()
	{
		return TrainerFlag;
	}
	public void setTrainerFlag(String aTrainerFlag)
	{
		TrainerFlag = aTrainerFlag;
	}
	public String getB1()
	{
		return B1;
	}
	public void setB1(String aB1)
	{
		B1 = aB1;
	}
	public String getB2()
	{
		return B2;
	}
	public void setB2(String aB2)
	{
		B2 = aB2;
	}
	public String getB3()
	{
		return B3;
	}
	public void setB3(String aB3)
	{
		B3 = aB3;
	}
	public String getB4()
	{
		return B4;
	}
	public void setB4(String aB4)
	{
		B4 = aB4;
	}
	public String getB5()
	{
		return B5;
	}
	public void setB5(String aB5)
	{
		B5 = aB5;
	}
	public String getB6()
	{
		return B6;
	}
	public void setB6(String aB6)
	{
		B6 = aB6;
	}
	public String getTrainerWorkDate()
	{
		if( TrainerWorkDate != null )
			return fDate.getString(TrainerWorkDate);
		else
			return null;
	}
	public void setTrainerWorkDate(Date aTrainerWorkDate)
	{
		TrainerWorkDate = aTrainerWorkDate;
	}
	public void setTrainerWorkDate(String aTrainerWorkDate)
	{
		if (aTrainerWorkDate != null && !aTrainerWorkDate.equals("") )
		{
			TrainerWorkDate = fDate.getDate( aTrainerWorkDate );
		}
		else
			TrainerWorkDate = null;
	}


	/**
	* 使用另外一个 LATrainerSchema 对象给 Schema 赋值
	* @param: aLATrainerSchema LATrainerSchema
	**/
	public void setSchema(LATrainerSchema aLATrainerSchema)
	{
		this.TrainerNo = aLATrainerSchema.getTrainerNo();
		this.PManageCom = aLATrainerSchema.getPManageCom();
		this.CManageCom = aLATrainerSchema.getCManageCom();
		this.AgentGroup = aLATrainerSchema.getAgentGroup();
		this.TrainerName = aLATrainerSchema.getTrainerName();
		this.Sex = aLATrainerSchema.getSex();
		this.TrainerGrade = aLATrainerSchema.getTrainerGrade();
		this.PolityVisage = aLATrainerSchema.getPolityVisage();
		this.Birthday = fDate.getDate( aLATrainerSchema.getBirthday());
		this.WorkDate = fDate.getDate( aLATrainerSchema.getWorkDate());
		this.InsureDate = fDate.getDate( aLATrainerSchema.getInsureDate());
		this.GraduateSchool = aLATrainerSchema.getGraduateSchool();
		this.Speciality = aLATrainerSchema.getSpeciality();
		this.Education = aLATrainerSchema.getEducation();
		this.Degree = aLATrainerSchema.getDegree();
		this.Mobile = aLATrainerSchema.getMobile();
		this.Assess = aLATrainerSchema.getAssess();
		this.MulitId = aLATrainerSchema.getMulitId();
		this.TrainerState = aLATrainerSchema.getTrainerState();
		this.WorkYear = aLATrainerSchema.getWorkYear();
		this.BranchType = aLATrainerSchema.getBranchType();
		this.BranchType2 = aLATrainerSchema.getBranchType2();
		this.Operator = aLATrainerSchema.getOperator();
		this.MakeDate = fDate.getDate( aLATrainerSchema.getMakeDate());
		this.MakeTime = aLATrainerSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLATrainerSchema.getModifyDate());
		this.ModifyTime = aLATrainerSchema.getModifyTime();
		this.BusinessManagerName = aLATrainerSchema.getBusinessManagerName();
		this.TakeOfficeDate = fDate.getDate( aLATrainerSchema.getTakeOfficeDate());
		this.LaborContract = aLATrainerSchema.getLaborContract();
		this.IDNoType = aLATrainerSchema.getIDNoType();
		this.IDNO = aLATrainerSchema.getIDNO();
		this.TrainerCode = aLATrainerSchema.getTrainerCode();
		this.TrainerFlag = aLATrainerSchema.getTrainerFlag();
		this.B1 = aLATrainerSchema.getB1();
		this.B2 = aLATrainerSchema.getB2();
		this.B3 = aLATrainerSchema.getB3();
		this.B4 = aLATrainerSchema.getB4();
		this.B5 = aLATrainerSchema.getB5();
		this.B6 = aLATrainerSchema.getB6();
		this.TrainerWorkDate = fDate.getDate( aLATrainerSchema.getTrainerWorkDate());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TrainerNo") == null )
				this.TrainerNo = null;
			else
				this.TrainerNo = rs.getString("TrainerNo").trim();

			if( rs.getString("PManageCom") == null )
				this.PManageCom = null;
			else
				this.PManageCom = rs.getString("PManageCom").trim();

			if( rs.getString("CManageCom") == null )
				this.CManageCom = null;
			else
				this.CManageCom = rs.getString("CManageCom").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("TrainerName") == null )
				this.TrainerName = null;
			else
				this.TrainerName = rs.getString("TrainerName").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			if( rs.getString("TrainerGrade") == null )
				this.TrainerGrade = null;
			else
				this.TrainerGrade = rs.getString("TrainerGrade").trim();

			if( rs.getString("PolityVisage") == null )
				this.PolityVisage = null;
			else
				this.PolityVisage = rs.getString("PolityVisage").trim();

			this.Birthday = rs.getDate("Birthday");
			this.WorkDate = rs.getDate("WorkDate");
			this.InsureDate = rs.getDate("InsureDate");
			if( rs.getString("GraduateSchool") == null )
				this.GraduateSchool = null;
			else
				this.GraduateSchool = rs.getString("GraduateSchool").trim();

			if( rs.getString("Speciality") == null )
				this.Speciality = null;
			else
				this.Speciality = rs.getString("Speciality").trim();

			if( rs.getString("Education") == null )
				this.Education = null;
			else
				this.Education = rs.getString("Education").trim();

			if( rs.getString("Degree") == null )
				this.Degree = null;
			else
				this.Degree = rs.getString("Degree").trim();

			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("Assess") == null )
				this.Assess = null;
			else
				this.Assess = rs.getString("Assess").trim();

			if( rs.getString("MulitId") == null )
				this.MulitId = null;
			else
				this.MulitId = rs.getString("MulitId").trim();

			if( rs.getString("TrainerState") == null )
				this.TrainerState = null;
			else
				this.TrainerState = rs.getString("TrainerState").trim();

			if( rs.getString("WorkYear") == null )
				this.WorkYear = null;
			else
				this.WorkYear = rs.getString("WorkYear").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BusinessManagerName") == null )
				this.BusinessManagerName = null;
			else
				this.BusinessManagerName = rs.getString("BusinessManagerName").trim();

			this.TakeOfficeDate = rs.getDate("TakeOfficeDate");
			if( rs.getString("LaborContract") == null )
				this.LaborContract = null;
			else
				this.LaborContract = rs.getString("LaborContract").trim();

			if( rs.getString("IDNoType") == null )
				this.IDNoType = null;
			else
				this.IDNoType = rs.getString("IDNoType").trim();

			if( rs.getString("IDNO") == null )
				this.IDNO = null;
			else
				this.IDNO = rs.getString("IDNO").trim();

			if( rs.getString("TrainerCode") == null )
				this.TrainerCode = null;
			else
				this.TrainerCode = rs.getString("TrainerCode").trim();

			if( rs.getString("TrainerFlag") == null )
				this.TrainerFlag = null;
			else
				this.TrainerFlag = rs.getString("TrainerFlag").trim();

			if( rs.getString("B1") == null )
				this.B1 = null;
			else
				this.B1 = rs.getString("B1").trim();

			if( rs.getString("B2") == null )
				this.B2 = null;
			else
				this.B2 = rs.getString("B2").trim();

			if( rs.getString("B3") == null )
				this.B3 = null;
			else
				this.B3 = rs.getString("B3").trim();

			if( rs.getString("B4") == null )
				this.B4 = null;
			else
				this.B4 = rs.getString("B4").trim();

			if( rs.getString("B5") == null )
				this.B5 = null;
			else
				this.B5 = rs.getString("B5").trim();

			if( rs.getString("B6") == null )
				this.B6 = null;
			else
				this.B6 = rs.getString("B6").trim();

			this.TrainerWorkDate = rs.getDate("TrainerWorkDate");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LATrainer表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainerSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LATrainerSchema getSchema()
	{
		LATrainerSchema aLATrainerSchema = new LATrainerSchema();
		aLATrainerSchema.setSchema(this);
		return aLATrainerSchema;
	}

	public LATrainerDB getDB()
	{
		LATrainerDB aDBOper = new LATrainerDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainer描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TrainerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainerGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolityVisage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( WorkDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InsureDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GraduateSchool)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Speciality)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Education)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Degree)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Assess)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MulitId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainerState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WorkYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessManagerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TakeOfficeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LaborContract)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainerCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainerFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(B1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(B2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(B3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(B4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(B5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(B6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TrainerWorkDate )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainer>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TrainerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			TrainerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			TrainerGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			PolityVisage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			WorkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			InsureDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			GraduateSchool = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Speciality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Education = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Degree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Assess = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			MulitId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			TrainerState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			WorkYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			BusinessManagerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			TakeOfficeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			LaborContract = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			IDNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			IDNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			TrainerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			TrainerFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			B1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			B2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			B3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			B4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			B5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			B6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			TrainerWorkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainerSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TrainerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerNo));
		}
		if (FCode.equals("PManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PManageCom));
		}
		if (FCode.equals("CManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CManageCom));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("TrainerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerName));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("TrainerGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerGrade));
		}
		if (FCode.equals("PolityVisage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolityVisage));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("WorkDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getWorkDate()));
		}
		if (FCode.equals("InsureDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInsureDate()));
		}
		if (FCode.equals("GraduateSchool"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GraduateSchool));
		}
		if (FCode.equals("Speciality"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Speciality));
		}
		if (FCode.equals("Education"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Education));
		}
		if (FCode.equals("Degree"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("Assess"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Assess));
		}
		if (FCode.equals("MulitId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MulitId));
		}
		if (FCode.equals("TrainerState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerState));
		}
		if (FCode.equals("WorkYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WorkYear));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BusinessManagerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessManagerName));
		}
		if (FCode.equals("TakeOfficeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTakeOfficeDate()));
		}
		if (FCode.equals("LaborContract"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LaborContract));
		}
		if (FCode.equals("IDNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNoType));
		}
		if (FCode.equals("IDNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNO));
		}
		if (FCode.equals("TrainerCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerCode));
		}
		if (FCode.equals("TrainerFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerFlag));
		}
		if (FCode.equals("B1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(B1));
		}
		if (FCode.equals("B2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(B2));
		}
		if (FCode.equals("B3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(B3));
		}
		if (FCode.equals("B4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(B4));
		}
		if (FCode.equals("B5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(B5));
		}
		if (FCode.equals("B6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(B6));
		}
		if (FCode.equals("TrainerWorkDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTrainerWorkDate()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TrainerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PManageCom);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CManageCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(TrainerName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(TrainerGrade);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(PolityVisage);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getWorkDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInsureDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(GraduateSchool);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Speciality);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Education);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Degree);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Assess);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(MulitId);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(TrainerState);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(WorkYear);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(BusinessManagerName);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTakeOfficeDate()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(LaborContract);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(IDNoType);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(IDNO);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(TrainerCode);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(TrainerFlag);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(B1);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(B2);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(B3);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(B4);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(B5);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(B6);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTrainerWorkDate()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TrainerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerNo = FValue.trim();
			}
			else
				TrainerNo = null;
		}
		if (FCode.equalsIgnoreCase("PManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PManageCom = FValue.trim();
			}
			else
				PManageCom = null;
		}
		if (FCode.equalsIgnoreCase("CManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CManageCom = FValue.trim();
			}
			else
				CManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("TrainerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerName = FValue.trim();
			}
			else
				TrainerName = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("TrainerGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerGrade = FValue.trim();
			}
			else
				TrainerGrade = null;
		}
		if (FCode.equalsIgnoreCase("PolityVisage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolityVisage = FValue.trim();
			}
			else
				PolityVisage = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("WorkDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				WorkDate = fDate.getDate( FValue );
			}
			else
				WorkDate = null;
		}
		if (FCode.equalsIgnoreCase("InsureDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InsureDate = fDate.getDate( FValue );
			}
			else
				InsureDate = null;
		}
		if (FCode.equalsIgnoreCase("GraduateSchool"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GraduateSchool = FValue.trim();
			}
			else
				GraduateSchool = null;
		}
		if (FCode.equalsIgnoreCase("Speciality"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Speciality = FValue.trim();
			}
			else
				Speciality = null;
		}
		if (FCode.equalsIgnoreCase("Education"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Education = FValue.trim();
			}
			else
				Education = null;
		}
		if (FCode.equalsIgnoreCase("Degree"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Degree = FValue.trim();
			}
			else
				Degree = null;
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("Assess"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Assess = FValue.trim();
			}
			else
				Assess = null;
		}
		if (FCode.equalsIgnoreCase("MulitId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MulitId = FValue.trim();
			}
			else
				MulitId = null;
		}
		if (FCode.equalsIgnoreCase("TrainerState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerState = FValue.trim();
			}
			else
				TrainerState = null;
		}
		if (FCode.equalsIgnoreCase("WorkYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WorkYear = FValue.trim();
			}
			else
				WorkYear = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BusinessManagerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessManagerName = FValue.trim();
			}
			else
				BusinessManagerName = null;
		}
		if (FCode.equalsIgnoreCase("TakeOfficeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TakeOfficeDate = fDate.getDate( FValue );
			}
			else
				TakeOfficeDate = null;
		}
		if (FCode.equalsIgnoreCase("LaborContract"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LaborContract = FValue.trim();
			}
			else
				LaborContract = null;
		}
		if (FCode.equalsIgnoreCase("IDNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNoType = FValue.trim();
			}
			else
				IDNoType = null;
		}
		if (FCode.equalsIgnoreCase("IDNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNO = FValue.trim();
			}
			else
				IDNO = null;
		}
		if (FCode.equalsIgnoreCase("TrainerCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerCode = FValue.trim();
			}
			else
				TrainerCode = null;
		}
		if (FCode.equalsIgnoreCase("TrainerFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerFlag = FValue.trim();
			}
			else
				TrainerFlag = null;
		}
		if (FCode.equalsIgnoreCase("B1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				B1 = FValue.trim();
			}
			else
				B1 = null;
		}
		if (FCode.equalsIgnoreCase("B2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				B2 = FValue.trim();
			}
			else
				B2 = null;
		}
		if (FCode.equalsIgnoreCase("B3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				B3 = FValue.trim();
			}
			else
				B3 = null;
		}
		if (FCode.equalsIgnoreCase("B4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				B4 = FValue.trim();
			}
			else
				B4 = null;
		}
		if (FCode.equalsIgnoreCase("B5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				B5 = FValue.trim();
			}
			else
				B5 = null;
		}
		if (FCode.equalsIgnoreCase("B6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				B6 = FValue.trim();
			}
			else
				B6 = null;
		}
		if (FCode.equalsIgnoreCase("TrainerWorkDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TrainerWorkDate = fDate.getDate( FValue );
			}
			else
				TrainerWorkDate = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LATrainerSchema other = (LATrainerSchema)otherObject;
		return
			(TrainerNo == null ? other.getTrainerNo() == null : TrainerNo.equals(other.getTrainerNo()))
			&& (PManageCom == null ? other.getPManageCom() == null : PManageCom.equals(other.getPManageCom()))
			&& (CManageCom == null ? other.getCManageCom() == null : CManageCom.equals(other.getCManageCom()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (TrainerName == null ? other.getTrainerName() == null : TrainerName.equals(other.getTrainerName()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (TrainerGrade == null ? other.getTrainerGrade() == null : TrainerGrade.equals(other.getTrainerGrade()))
			&& (PolityVisage == null ? other.getPolityVisage() == null : PolityVisage.equals(other.getPolityVisage()))
			&& (Birthday == null ? other.getBirthday() == null : fDate.getString(Birthday).equals(other.getBirthday()))
			&& (WorkDate == null ? other.getWorkDate() == null : fDate.getString(WorkDate).equals(other.getWorkDate()))
			&& (InsureDate == null ? other.getInsureDate() == null : fDate.getString(InsureDate).equals(other.getInsureDate()))
			&& (GraduateSchool == null ? other.getGraduateSchool() == null : GraduateSchool.equals(other.getGraduateSchool()))
			&& (Speciality == null ? other.getSpeciality() == null : Speciality.equals(other.getSpeciality()))
			&& (Education == null ? other.getEducation() == null : Education.equals(other.getEducation()))
			&& (Degree == null ? other.getDegree() == null : Degree.equals(other.getDegree()))
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (Assess == null ? other.getAssess() == null : Assess.equals(other.getAssess()))
			&& (MulitId == null ? other.getMulitId() == null : MulitId.equals(other.getMulitId()))
			&& (TrainerState == null ? other.getTrainerState() == null : TrainerState.equals(other.getTrainerState()))
			&& (WorkYear == null ? other.getWorkYear() == null : WorkYear.equals(other.getWorkYear()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BusinessManagerName == null ? other.getBusinessManagerName() == null : BusinessManagerName.equals(other.getBusinessManagerName()))
			&& (TakeOfficeDate == null ? other.getTakeOfficeDate() == null : fDate.getString(TakeOfficeDate).equals(other.getTakeOfficeDate()))
			&& (LaborContract == null ? other.getLaborContract() == null : LaborContract.equals(other.getLaborContract()))
			&& (IDNoType == null ? other.getIDNoType() == null : IDNoType.equals(other.getIDNoType()))
			&& (IDNO == null ? other.getIDNO() == null : IDNO.equals(other.getIDNO()))
			&& (TrainerCode == null ? other.getTrainerCode() == null : TrainerCode.equals(other.getTrainerCode()))
			&& (TrainerFlag == null ? other.getTrainerFlag() == null : TrainerFlag.equals(other.getTrainerFlag()))
			&& (B1 == null ? other.getB1() == null : B1.equals(other.getB1()))
			&& (B2 == null ? other.getB2() == null : B2.equals(other.getB2()))
			&& (B3 == null ? other.getB3() == null : B3.equals(other.getB3()))
			&& (B4 == null ? other.getB4() == null : B4.equals(other.getB4()))
			&& (B5 == null ? other.getB5() == null : B5.equals(other.getB5()))
			&& (B6 == null ? other.getB6() == null : B6.equals(other.getB6()))
			&& (TrainerWorkDate == null ? other.getTrainerWorkDate() == null : fDate.getString(TrainerWorkDate).equals(other.getTrainerWorkDate()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TrainerNo") ) {
			return 0;
		}
		if( strFieldName.equals("PManageCom") ) {
			return 1;
		}
		if( strFieldName.equals("CManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 3;
		}
		if( strFieldName.equals("TrainerName") ) {
			return 4;
		}
		if( strFieldName.equals("Sex") ) {
			return 5;
		}
		if( strFieldName.equals("TrainerGrade") ) {
			return 6;
		}
		if( strFieldName.equals("PolityVisage") ) {
			return 7;
		}
		if( strFieldName.equals("Birthday") ) {
			return 8;
		}
		if( strFieldName.equals("WorkDate") ) {
			return 9;
		}
		if( strFieldName.equals("InsureDate") ) {
			return 10;
		}
		if( strFieldName.equals("GraduateSchool") ) {
			return 11;
		}
		if( strFieldName.equals("Speciality") ) {
			return 12;
		}
		if( strFieldName.equals("Education") ) {
			return 13;
		}
		if( strFieldName.equals("Degree") ) {
			return 14;
		}
		if( strFieldName.equals("Mobile") ) {
			return 15;
		}
		if( strFieldName.equals("Assess") ) {
			return 16;
		}
		if( strFieldName.equals("MulitId") ) {
			return 17;
		}
		if( strFieldName.equals("TrainerState") ) {
			return 18;
		}
		if( strFieldName.equals("WorkYear") ) {
			return 19;
		}
		if( strFieldName.equals("BranchType") ) {
			return 20;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 21;
		}
		if( strFieldName.equals("Operator") ) {
			return 22;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 23;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 25;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 26;
		}
		if( strFieldName.equals("BusinessManagerName") ) {
			return 27;
		}
		if( strFieldName.equals("TakeOfficeDate") ) {
			return 28;
		}
		if( strFieldName.equals("LaborContract") ) {
			return 29;
		}
		if( strFieldName.equals("IDNoType") ) {
			return 30;
		}
		if( strFieldName.equals("IDNO") ) {
			return 31;
		}
		if( strFieldName.equals("TrainerCode") ) {
			return 32;
		}
		if( strFieldName.equals("TrainerFlag") ) {
			return 33;
		}
		if( strFieldName.equals("B1") ) {
			return 34;
		}
		if( strFieldName.equals("B2") ) {
			return 35;
		}
		if( strFieldName.equals("B3") ) {
			return 36;
		}
		if( strFieldName.equals("B4") ) {
			return 37;
		}
		if( strFieldName.equals("B5") ) {
			return 38;
		}
		if( strFieldName.equals("B6") ) {
			return 39;
		}
		if( strFieldName.equals("TrainerWorkDate") ) {
			return 40;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TrainerNo";
				break;
			case 1:
				strFieldName = "PManageCom";
				break;
			case 2:
				strFieldName = "CManageCom";
				break;
			case 3:
				strFieldName = "AgentGroup";
				break;
			case 4:
				strFieldName = "TrainerName";
				break;
			case 5:
				strFieldName = "Sex";
				break;
			case 6:
				strFieldName = "TrainerGrade";
				break;
			case 7:
				strFieldName = "PolityVisage";
				break;
			case 8:
				strFieldName = "Birthday";
				break;
			case 9:
				strFieldName = "WorkDate";
				break;
			case 10:
				strFieldName = "InsureDate";
				break;
			case 11:
				strFieldName = "GraduateSchool";
				break;
			case 12:
				strFieldName = "Speciality";
				break;
			case 13:
				strFieldName = "Education";
				break;
			case 14:
				strFieldName = "Degree";
				break;
			case 15:
				strFieldName = "Mobile";
				break;
			case 16:
				strFieldName = "Assess";
				break;
			case 17:
				strFieldName = "MulitId";
				break;
			case 18:
				strFieldName = "TrainerState";
				break;
			case 19:
				strFieldName = "WorkYear";
				break;
			case 20:
				strFieldName = "BranchType";
				break;
			case 21:
				strFieldName = "BranchType2";
				break;
			case 22:
				strFieldName = "Operator";
				break;
			case 23:
				strFieldName = "MakeDate";
				break;
			case 24:
				strFieldName = "MakeTime";
				break;
			case 25:
				strFieldName = "ModifyDate";
				break;
			case 26:
				strFieldName = "ModifyTime";
				break;
			case 27:
				strFieldName = "BusinessManagerName";
				break;
			case 28:
				strFieldName = "TakeOfficeDate";
				break;
			case 29:
				strFieldName = "LaborContract";
				break;
			case 30:
				strFieldName = "IDNoType";
				break;
			case 31:
				strFieldName = "IDNO";
				break;
			case 32:
				strFieldName = "TrainerCode";
				break;
			case 33:
				strFieldName = "TrainerFlag";
				break;
			case 34:
				strFieldName = "B1";
				break;
			case 35:
				strFieldName = "B2";
				break;
			case 36:
				strFieldName = "B3";
				break;
			case 37:
				strFieldName = "B4";
				break;
			case 38:
				strFieldName = "B5";
				break;
			case 39:
				strFieldName = "B6";
				break;
			case 40:
				strFieldName = "TrainerWorkDate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TrainerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolityVisage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("WorkDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InsureDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GraduateSchool") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Speciality") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Education") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Degree") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Assess") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MulitId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WorkYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessManagerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TakeOfficeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LaborContract") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("B6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerWorkDate") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
