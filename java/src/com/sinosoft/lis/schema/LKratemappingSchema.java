/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKratemappingDB;

/*
 * <p>ClassName: LKratemappingSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2017-06-20
 */
public class LKratemappingSchema implements Schema, Cloneable
{
	// @Field
	/** 主键 */
	private int id;
	/** 银行代码 */
	private String bankcode;
	/** 机构编码 */
	private String managecom;
	/** 套餐编码 */
	private String riskwrapcode;
	/** 启用标志 */
	private String enableflag;
	/** 备用1 */
	private String bak_1;
	/** 备用2 */
	private String bak_2;
	/** 备用3 */
	private String bak_3;
	/** 备用4 */
	private String bak_4;
	/** 备用5 */
	private String bak_5;

	public static final int FIELDNUM = 10;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKratemappingSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "id";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LKratemappingSchema cloned = (LKratemappingSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public int getid()
	{
		return id;
	}
	public void setid(int aid)
	{
		id = aid;
	}
	public void setid(String aid)
	{
		if (aid != null && !aid.equals(""))
		{
			Integer tInteger = new Integer(aid);
			int i = tInteger.intValue();
			id = i;
		}
	}

	public String getbankcode()
	{
		return bankcode;
	}
	public void setbankcode(String abankcode)
	{
		bankcode = abankcode;
	}
	public String getmanagecom()
	{
		return managecom;
	}
	public void setmanagecom(String amanagecom)
	{
		managecom = amanagecom;
	}
	public String getriskwrapcode()
	{
		return riskwrapcode;
	}
	public void setriskwrapcode(String ariskwrapcode)
	{
		riskwrapcode = ariskwrapcode;
	}
	public String getenableflag()
	{
		return enableflag;
	}
	public void setenableflag(String aenableflag)
	{
		enableflag = aenableflag;
	}
	public String getbak_1()
	{
		return bak_1;
	}
	public void setbak_1(String abak_1)
	{
		bak_1 = abak_1;
	}
	public String getbak_2()
	{
		return bak_2;
	}
	public void setbak_2(String abak_2)
	{
		bak_2 = abak_2;
	}
	public String getbak_3()
	{
		return bak_3;
	}
	public void setbak_3(String abak_3)
	{
		bak_3 = abak_3;
	}
	public String getbak_4()
	{
		return bak_4;
	}
	public void setbak_4(String abak_4)
	{
		bak_4 = abak_4;
	}
	public String getbak_5()
	{
		return bak_5;
	}
	public void setbak_5(String abak_5)
	{
		bak_5 = abak_5;
	}

	/**
	* 使用另外一个 LKratemappingSchema 对象给 Schema 赋值
	* @param: aLKratemappingSchema LKratemappingSchema
	**/
	public void setSchema(LKratemappingSchema aLKratemappingSchema)
	{
		this.id = aLKratemappingSchema.getid();
		this.bankcode = aLKratemappingSchema.getbankcode();
		this.managecom = aLKratemappingSchema.getmanagecom();
		this.riskwrapcode = aLKratemappingSchema.getriskwrapcode();
		this.enableflag = aLKratemappingSchema.getenableflag();
		this.bak_1 = aLKratemappingSchema.getbak_1();
		this.bak_2 = aLKratemappingSchema.getbak_2();
		this.bak_3 = aLKratemappingSchema.getbak_3();
		this.bak_4 = aLKratemappingSchema.getbak_4();
		this.bak_5 = aLKratemappingSchema.getbak_5();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			this.id = rs.getInt("id");
			if( rs.getString("bankcode") == null )
				this.bankcode = null;
			else
				this.bankcode = rs.getString("bankcode").trim();

			if( rs.getString("managecom") == null )
				this.managecom = null;
			else
				this.managecom = rs.getString("managecom").trim();

			if( rs.getString("riskwrapcode") == null )
				this.riskwrapcode = null;
			else
				this.riskwrapcode = rs.getString("riskwrapcode").trim();

			if( rs.getString("enableflag") == null )
				this.enableflag = null;
			else
				this.enableflag = rs.getString("enableflag").trim();

			if( rs.getString("bak_1") == null )
				this.bak_1 = null;
			else
				this.bak_1 = rs.getString("bak_1").trim();

			if( rs.getString("bak_2") == null )
				this.bak_2 = null;
			else
				this.bak_2 = rs.getString("bak_2").trim();

			if( rs.getString("bak_3") == null )
				this.bak_3 = null;
			else
				this.bak_3 = rs.getString("bak_3").trim();

			if( rs.getString("bak_4") == null )
				this.bak_4 = null;
			else
				this.bak_4 = rs.getString("bak_4").trim();

			if( rs.getString("bak_5") == null )
				this.bak_5 = null;
			else
				this.bak_5 = rs.getString("bak_5").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LKratemapping表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKratemappingSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LKratemappingSchema getSchema()
	{
		LKratemappingSchema aLKratemappingSchema = new LKratemappingSchema();
		aLKratemappingSchema.setSchema(this);
		return aLKratemappingSchema;
	}

	public LKratemappingDB getDB()
	{
		LKratemappingDB aDBOper = new LKratemappingDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKratemapping描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append( ChgData.chgData(id));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bankcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(managecom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(riskwrapcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(enableflag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak_1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak_2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak_3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak_4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak_5));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKratemapping>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			id= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).intValue();
			bankcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			riskwrapcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			enableflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			bak_1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			bak_2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			bak_3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			bak_4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			bak_5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKratemappingSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(id));
		}
		if (FCode.equals("bankcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankcode));
		}
		if (FCode.equals("managecom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(managecom));
		}
		if (FCode.equals("riskwrapcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(riskwrapcode));
		}
		if (FCode.equals("enableflag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(enableflag));
		}
		if (FCode.equals("bak_1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak_1));
		}
		if (FCode.equals("bak_2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak_2));
		}
		if (FCode.equals("bak_3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak_3));
		}
		if (FCode.equals("bak_4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak_4));
		}
		if (FCode.equals("bak_5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak_5));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = String.valueOf(id);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(bankcode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(managecom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(riskwrapcode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(enableflag);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(bak_1);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(bak_2);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(bak_3);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(bak_4);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(bak_5);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				id = i;
			}
		}
		if (FCode.equalsIgnoreCase("bankcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankcode = FValue.trim();
			}
			else
				bankcode = null;
		}
		if (FCode.equalsIgnoreCase("managecom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				managecom = FValue.trim();
			}
			else
				managecom = null;
		}
		if (FCode.equalsIgnoreCase("riskwrapcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				riskwrapcode = FValue.trim();
			}
			else
				riskwrapcode = null;
		}
		if (FCode.equalsIgnoreCase("enableflag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				enableflag = FValue.trim();
			}
			else
				enableflag = null;
		}
		if (FCode.equalsIgnoreCase("bak_1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak_1 = FValue.trim();
			}
			else
				bak_1 = null;
		}
		if (FCode.equalsIgnoreCase("bak_2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak_2 = FValue.trim();
			}
			else
				bak_2 = null;
		}
		if (FCode.equalsIgnoreCase("bak_3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak_3 = FValue.trim();
			}
			else
				bak_3 = null;
		}
		if (FCode.equalsIgnoreCase("bak_4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak_4 = FValue.trim();
			}
			else
				bak_4 = null;
		}
		if (FCode.equalsIgnoreCase("bak_5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak_5 = FValue.trim();
			}
			else
				bak_5 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKratemappingSchema other = (LKratemappingSchema)otherObject;
		return
			id == other.getid()
			&& (bankcode == null ? other.getbankcode() == null : bankcode.equals(other.getbankcode()))
			&& (managecom == null ? other.getmanagecom() == null : managecom.equals(other.getmanagecom()))
			&& (riskwrapcode == null ? other.getriskwrapcode() == null : riskwrapcode.equals(other.getriskwrapcode()))
			&& (enableflag == null ? other.getenableflag() == null : enableflag.equals(other.getenableflag()))
			&& (bak_1 == null ? other.getbak_1() == null : bak_1.equals(other.getbak_1()))
			&& (bak_2 == null ? other.getbak_2() == null : bak_2.equals(other.getbak_2()))
			&& (bak_3 == null ? other.getbak_3() == null : bak_3.equals(other.getbak_3()))
			&& (bak_4 == null ? other.getbak_4() == null : bak_4.equals(other.getbak_4()))
			&& (bak_5 == null ? other.getbak_5() == null : bak_5.equals(other.getbak_5()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("id") ) {
			return 0;
		}
		if( strFieldName.equals("bankcode") ) {
			return 1;
		}
		if( strFieldName.equals("managecom") ) {
			return 2;
		}
		if( strFieldName.equals("riskwrapcode") ) {
			return 3;
		}
		if( strFieldName.equals("enableflag") ) {
			return 4;
		}
		if( strFieldName.equals("bak_1") ) {
			return 5;
		}
		if( strFieldName.equals("bak_2") ) {
			return 6;
		}
		if( strFieldName.equals("bak_3") ) {
			return 7;
		}
		if( strFieldName.equals("bak_4") ) {
			return 8;
		}
		if( strFieldName.equals("bak_5") ) {
			return 9;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "id";
				break;
			case 1:
				strFieldName = "bankcode";
				break;
			case 2:
				strFieldName = "managecom";
				break;
			case 3:
				strFieldName = "riskwrapcode";
				break;
			case 4:
				strFieldName = "enableflag";
				break;
			case 5:
				strFieldName = "bak_1";
				break;
			case 6:
				strFieldName = "bak_2";
				break;
			case 7:
				strFieldName = "bak_3";
				break;
			case 8:
				strFieldName = "bak_4";
				break;
			case 9:
				strFieldName = "bak_5";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("id") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("bankcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("managecom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("riskwrapcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("enableflag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak_1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak_2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak_3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak_4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak_5") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_INT;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
