/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LZCardPayDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LZCardPaySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-09-14
 */
public class LZCardPaySchema implements Schema, Cloneable {
    // @Field
    /** 结算单号 */
    private String PayNo;
    /** 单证编码 */
    private String CertifyCode;
    /** 单证编码类型 */
    private String CardType;
    /** 管理机构 */
    private String ManageCom;
    /** 起始号 */
    private String StartNo;
    /** 终止号 */
    private String EndNo;
    /** 数量 */
    private int SumCount;
    /** 应收总金额 */
    private double DuePayMoney;
    /** 结算总金额 */
    private double ActPayMoney;
    /** 经办人代码 */
    private String HandlerCode;
    /** 经办人名称 */
    private String Handler;
    /** 代理机构代码 */
    private String AgentCom;
    /** 代理机构名称 */
    private String AgentComName;
    /** 经办日期 */
    private Date HandleDate;
    /** 操作类型标志 */
    private String OperateFlag;
    /** 交费标志 */
    private String PayFlag;
    /** 到帐标志 */
    private String EnterAccFlag;
    /** 到帐日期 */
    private Date ConfDate;
    /** 状态 */
    private String State;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 24; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LZCardPaySchema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "PayNo";
        pk[1] = "CardType";
        pk[2] = "StartNo";
        pk[3] = "EndNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LZCardPaySchema cloned = (LZCardPaySchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getPayNo() {
        return PayNo;
    }

    public void setPayNo(String aPayNo) {
        PayNo = aPayNo;
    }

    public String getCertifyCode() {
        return CertifyCode;
    }

    public void setCertifyCode(String aCertifyCode) {
        CertifyCode = aCertifyCode;
    }

    public String getCardType() {
        return CardType;
    }

    public void setCardType(String aCardType) {
        CardType = aCardType;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getStartNo() {
        return StartNo;
    }

    public void setStartNo(String aStartNo) {
        StartNo = aStartNo;
    }

    public String getEndNo() {
        return EndNo;
    }

    public void setEndNo(String aEndNo) {
        EndNo = aEndNo;
    }

    public int getSumCount() {
        return SumCount;
    }

    public void setSumCount(int aSumCount) {
        SumCount = aSumCount;
    }

    public void setSumCount(String aSumCount) {
        if (aSumCount != null && !aSumCount.equals("")) {
            Integer tInteger = new Integer(aSumCount);
            int i = tInteger.intValue();
            SumCount = i;
        }
    }

    public double getDuePayMoney() {
        return DuePayMoney;
    }

    public void setDuePayMoney(double aDuePayMoney) {
        DuePayMoney = Arith.round(aDuePayMoney, 2);
    }

    public void setDuePayMoney(String aDuePayMoney) {
        if (aDuePayMoney != null && !aDuePayMoney.equals("")) {
            Double tDouble = new Double(aDuePayMoney);
            double d = tDouble.doubleValue();
            DuePayMoney = Arith.round(d, 2);
        }
    }

    public double getActPayMoney() {
        return ActPayMoney;
    }

    public void setActPayMoney(double aActPayMoney) {
        ActPayMoney = Arith.round(aActPayMoney, 2);
    }

    public void setActPayMoney(String aActPayMoney) {
        if (aActPayMoney != null && !aActPayMoney.equals("")) {
            Double tDouble = new Double(aActPayMoney);
            double d = tDouble.doubleValue();
            ActPayMoney = Arith.round(d, 2);
        }
    }

    public String getHandlerCode() {
        return HandlerCode;
    }

    public void setHandlerCode(String aHandlerCode) {
        HandlerCode = aHandlerCode;
    }

    public String getHandler() {
        return Handler;
    }

    public void setHandler(String aHandler) {
        Handler = aHandler;
    }

    public String getAgentCom() {
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }

    public String getAgentComName() {
        return AgentComName;
    }

    public void setAgentComName(String aAgentComName) {
        AgentComName = aAgentComName;
    }

    public String getHandleDate() {
        if (HandleDate != null) {
            return fDate.getString(HandleDate);
        } else {
            return null;
        }
    }

    public void setHandleDate(Date aHandleDate) {
        HandleDate = aHandleDate;
    }

    public void setHandleDate(String aHandleDate) {
        if (aHandleDate != null && !aHandleDate.equals("")) {
            HandleDate = fDate.getDate(aHandleDate);
        } else {
            HandleDate = null;
        }
    }

    public String getOperateFlag() {
        return OperateFlag;
    }

    public void setOperateFlag(String aOperateFlag) {
        OperateFlag = aOperateFlag;
    }

    public String getPayFlag() {
        return PayFlag;
    }

    public void setPayFlag(String aPayFlag) {
        PayFlag = aPayFlag;
    }

    public String getEnterAccFlag() {
        return EnterAccFlag;
    }

    public void setEnterAccFlag(String aEnterAccFlag) {
        EnterAccFlag = aEnterAccFlag;
    }

    public String getConfDate() {
        if (ConfDate != null) {
            return fDate.getString(ConfDate);
        } else {
            return null;
        }
    }

    public void setConfDate(Date aConfDate) {
        ConfDate = aConfDate;
    }

    public void setConfDate(String aConfDate) {
        if (aConfDate != null && !aConfDate.equals("")) {
            ConfDate = fDate.getDate(aConfDate);
        } else {
            ConfDate = null;
        }
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LZCardPaySchema 对象给 Schema 赋值
     * @param: aLZCardPaySchema LZCardPaySchema
     **/
    public void setSchema(LZCardPaySchema aLZCardPaySchema) {
        this.PayNo = aLZCardPaySchema.getPayNo();
        this.CertifyCode = aLZCardPaySchema.getCertifyCode();
        this.CardType = aLZCardPaySchema.getCardType();
        this.ManageCom = aLZCardPaySchema.getManageCom();
        this.StartNo = aLZCardPaySchema.getStartNo();
        this.EndNo = aLZCardPaySchema.getEndNo();
        this.SumCount = aLZCardPaySchema.getSumCount();
        this.DuePayMoney = aLZCardPaySchema.getDuePayMoney();
        this.ActPayMoney = aLZCardPaySchema.getActPayMoney();
        this.HandlerCode = aLZCardPaySchema.getHandlerCode();
        this.Handler = aLZCardPaySchema.getHandler();
        this.AgentCom = aLZCardPaySchema.getAgentCom();
        this.AgentComName = aLZCardPaySchema.getAgentComName();
        this.HandleDate = fDate.getDate(aLZCardPaySchema.getHandleDate());
        this.OperateFlag = aLZCardPaySchema.getOperateFlag();
        this.PayFlag = aLZCardPaySchema.getPayFlag();
        this.EnterAccFlag = aLZCardPaySchema.getEnterAccFlag();
        this.ConfDate = fDate.getDate(aLZCardPaySchema.getConfDate());
        this.State = aLZCardPaySchema.getState();
        this.Operator = aLZCardPaySchema.getOperator();
        this.MakeDate = fDate.getDate(aLZCardPaySchema.getMakeDate());
        this.MakeTime = aLZCardPaySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLZCardPaySchema.getModifyDate());
        this.ModifyTime = aLZCardPaySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PayNo") == null) {
                this.PayNo = null;
            } else {
                this.PayNo = rs.getString("PayNo").trim();
            }

            if (rs.getString("CertifyCode") == null) {
                this.CertifyCode = null;
            } else {
                this.CertifyCode = rs.getString("CertifyCode").trim();
            }

            if (rs.getString("CardType") == null) {
                this.CardType = null;
            } else {
                this.CardType = rs.getString("CardType").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("StartNo") == null) {
                this.StartNo = null;
            } else {
                this.StartNo = rs.getString("StartNo").trim();
            }

            if (rs.getString("EndNo") == null) {
                this.EndNo = null;
            } else {
                this.EndNo = rs.getString("EndNo").trim();
            }

            this.SumCount = rs.getInt("SumCount");
            this.DuePayMoney = rs.getDouble("DuePayMoney");
            this.ActPayMoney = rs.getDouble("ActPayMoney");
            if (rs.getString("HandlerCode") == null) {
                this.HandlerCode = null;
            } else {
                this.HandlerCode = rs.getString("HandlerCode").trim();
            }

            if (rs.getString("Handler") == null) {
                this.Handler = null;
            } else {
                this.Handler = rs.getString("Handler").trim();
            }

            if (rs.getString("AgentCom") == null) {
                this.AgentCom = null;
            } else {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("AgentComName") == null) {
                this.AgentComName = null;
            } else {
                this.AgentComName = rs.getString("AgentComName").trim();
            }

            this.HandleDate = rs.getDate("HandleDate");
            if (rs.getString("OperateFlag") == null) {
                this.OperateFlag = null;
            } else {
                this.OperateFlag = rs.getString("OperateFlag").trim();
            }

            if (rs.getString("PayFlag") == null) {
                this.PayFlag = null;
            } else {
                this.PayFlag = rs.getString("PayFlag").trim();
            }

            if (rs.getString("EnterAccFlag") == null) {
                this.EnterAccFlag = null;
            } else {
                this.EnterAccFlag = rs.getString("EnterAccFlag").trim();
            }

            this.ConfDate = rs.getDate("ConfDate");
            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LZCardPay表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZCardPaySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LZCardPaySchema getSchema() {
        LZCardPaySchema aLZCardPaySchema = new LZCardPaySchema();
        aLZCardPaySchema.setSchema(this);
        return aLZCardPaySchema;
    }

    public LZCardPayDB getDB() {
        LZCardPayDB aDBOper = new LZCardPayDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardPay描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(PayNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CertifyCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CardType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StartNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EndNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DuePayMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ActPayMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HandlerCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Handler));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentComName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(HandleDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OperateFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EnterAccFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ConfDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardPay>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            PayNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            CardType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            StartNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            EndNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                   SysConst.PACKAGESPILTER);
            SumCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            DuePayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            ActPayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            HandlerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            AgentComName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                          SysConst.PACKAGESPILTER);
            HandleDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            OperateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                         SysConst.PACKAGESPILTER);
            PayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                     SysConst.PACKAGESPILTER);
            EnterAccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                          SysConst.PACKAGESPILTER);
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                   SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZCardPaySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("PayNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayNo));
        }
        if (FCode.equals("CertifyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
        }
        if (FCode.equals("CardType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CardType));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("StartNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartNo));
        }
        if (FCode.equals("EndNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndNo));
        }
        if (FCode.equals("SumCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumCount));
        }
        if (FCode.equals("DuePayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DuePayMoney));
        }
        if (FCode.equals("ActPayMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActPayMoney));
        }
        if (FCode.equals("HandlerCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerCode));
        }
        if (FCode.equals("Handler")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
        }
        if (FCode.equals("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equals("AgentComName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentComName));
        }
        if (FCode.equals("HandleDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getHandleDate()));
        }
        if (FCode.equals("OperateFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OperateFlag));
        }
        if (FCode.equals("PayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayFlag));
        }
        if (FCode.equals("EnterAccFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EnterAccFlag));
        }
        if (FCode.equals("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getConfDate()));
        }
        if (FCode.equals("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(PayNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CertifyCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(CardType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(StartNo);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(EndNo);
            break;
        case 6:
            strFieldValue = String.valueOf(SumCount);
            break;
        case 7:
            strFieldValue = String.valueOf(DuePayMoney);
            break;
        case 8:
            strFieldValue = String.valueOf(ActPayMoney);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(HandlerCode);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Handler);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(AgentCom);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(AgentComName);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getHandleDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(OperateFlag);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(PayFlag);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(EnterAccFlag);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getConfDate()));
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("PayNo")) {
            if (FValue != null && !FValue.equals("")) {
                PayNo = FValue.trim();
            } else {
                PayNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            if (FValue != null && !FValue.equals("")) {
                CertifyCode = FValue.trim();
            } else {
                CertifyCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("CardType")) {
            if (FValue != null && !FValue.equals("")) {
                CardType = FValue.trim();
            } else {
                CardType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartNo")) {
            if (FValue != null && !FValue.equals("")) {
                StartNo = FValue.trim();
            } else {
                StartNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndNo")) {
            if (FValue != null && !FValue.equals("")) {
                EndNo = FValue.trim();
            } else {
                EndNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("SumCount")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SumCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("DuePayMoney")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DuePayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("ActPayMoney")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ActPayMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("HandlerCode")) {
            if (FValue != null && !FValue.equals("")) {
                HandlerCode = FValue.trim();
            } else {
                HandlerCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Handler")) {
            if (FValue != null && !FValue.equals("")) {
                Handler = FValue.trim();
            } else {
                Handler = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCom = FValue.trim();
            } else {
                AgentCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentComName")) {
            if (FValue != null && !FValue.equals("")) {
                AgentComName = FValue.trim();
            } else {
                AgentComName = null;
            }
        }
        if (FCode.equalsIgnoreCase("HandleDate")) {
            if (FValue != null && !FValue.equals("")) {
                HandleDate = fDate.getDate(FValue);
            } else {
                HandleDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("OperateFlag")) {
            if (FValue != null && !FValue.equals("")) {
                OperateFlag = FValue.trim();
            } else {
                OperateFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("PayFlag")) {
            if (FValue != null && !FValue.equals("")) {
                PayFlag = FValue.trim();
            } else {
                PayFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("EnterAccFlag")) {
            if (FValue != null && !FValue.equals("")) {
                EnterAccFlag = FValue.trim();
            } else {
                EnterAccFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if (FValue != null && !FValue.equals("")) {
                ConfDate = fDate.getDate(FValue);
            } else {
                ConfDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LZCardPaySchema other = (LZCardPaySchema) otherObject;
        return
                PayNo.equals(other.getPayNo())
                && CertifyCode.equals(other.getCertifyCode())
                && CardType.equals(other.getCardType())
                && ManageCom.equals(other.getManageCom())
                && StartNo.equals(other.getStartNo())
                && EndNo.equals(other.getEndNo())
                && SumCount == other.getSumCount()
                && DuePayMoney == other.getDuePayMoney()
                && ActPayMoney == other.getActPayMoney()
                && HandlerCode.equals(other.getHandlerCode())
                && Handler.equals(other.getHandler())
                && AgentCom.equals(other.getAgentCom())
                && AgentComName.equals(other.getAgentComName())
                && fDate.getString(HandleDate).equals(other.getHandleDate())
                && OperateFlag.equals(other.getOperateFlag())
                && PayFlag.equals(other.getPayFlag())
                && EnterAccFlag.equals(other.getEnterAccFlag())
                && fDate.getString(ConfDate).equals(other.getConfDate())
                && State.equals(other.getState())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("PayNo")) {
            return 0;
        }
        if (strFieldName.equals("CertifyCode")) {
            return 1;
        }
        if (strFieldName.equals("CardType")) {
            return 2;
        }
        if (strFieldName.equals("ManageCom")) {
            return 3;
        }
        if (strFieldName.equals("StartNo")) {
            return 4;
        }
        if (strFieldName.equals("EndNo")) {
            return 5;
        }
        if (strFieldName.equals("SumCount")) {
            return 6;
        }
        if (strFieldName.equals("DuePayMoney")) {
            return 7;
        }
        if (strFieldName.equals("ActPayMoney")) {
            return 8;
        }
        if (strFieldName.equals("HandlerCode")) {
            return 9;
        }
        if (strFieldName.equals("Handler")) {
            return 10;
        }
        if (strFieldName.equals("AgentCom")) {
            return 11;
        }
        if (strFieldName.equals("AgentComName")) {
            return 12;
        }
        if (strFieldName.equals("HandleDate")) {
            return 13;
        }
        if (strFieldName.equals("OperateFlag")) {
            return 14;
        }
        if (strFieldName.equals("PayFlag")) {
            return 15;
        }
        if (strFieldName.equals("EnterAccFlag")) {
            return 16;
        }
        if (strFieldName.equals("ConfDate")) {
            return 17;
        }
        if (strFieldName.equals("State")) {
            return 18;
        }
        if (strFieldName.equals("Operator")) {
            return 19;
        }
        if (strFieldName.equals("MakeDate")) {
            return 20;
        }
        if (strFieldName.equals("MakeTime")) {
            return 21;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 22;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 23;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "PayNo";
            break;
        case 1:
            strFieldName = "CertifyCode";
            break;
        case 2:
            strFieldName = "CardType";
            break;
        case 3:
            strFieldName = "ManageCom";
            break;
        case 4:
            strFieldName = "StartNo";
            break;
        case 5:
            strFieldName = "EndNo";
            break;
        case 6:
            strFieldName = "SumCount";
            break;
        case 7:
            strFieldName = "DuePayMoney";
            break;
        case 8:
            strFieldName = "ActPayMoney";
            break;
        case 9:
            strFieldName = "HandlerCode";
            break;
        case 10:
            strFieldName = "Handler";
            break;
        case 11:
            strFieldName = "AgentCom";
            break;
        case 12:
            strFieldName = "AgentComName";
            break;
        case 13:
            strFieldName = "HandleDate";
            break;
        case 14:
            strFieldName = "OperateFlag";
            break;
        case 15:
            strFieldName = "PayFlag";
            break;
        case 16:
            strFieldName = "EnterAccFlag";
            break;
        case 17:
            strFieldName = "ConfDate";
            break;
        case 18:
            strFieldName = "State";
            break;
        case 19:
            strFieldName = "Operator";
            break;
        case 20:
            strFieldName = "MakeDate";
            break;
        case 21:
            strFieldName = "MakeTime";
            break;
        case 22:
            strFieldName = "ModifyDate";
            break;
        case 23:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("PayNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CertifyCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CardType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SumCount")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DuePayMoney")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ActPayMoney")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("HandlerCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Handler")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentComName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HandleDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("OperateFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EnterAccFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_INT;
            break;
        case 7:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 8:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 21:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 22:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 23:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
