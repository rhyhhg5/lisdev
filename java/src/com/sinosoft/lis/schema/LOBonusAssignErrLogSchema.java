/*
 * <p>ClassName: LOBonusAssignErrLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBonusAssignErrLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LOBonusAssignErrLogSchema implements Schema
{
    // @Field
    /** 流水号 */
    private String SerialNo;
    /** 保单号 */
    private String PolNo;
    /** 领取类型 */
    private String GetMode;
    /** 错误原因 */
    private String errMsg;
    /** 入机日期 */
    private Date makedate;
    /** 入机时间 */
    private String maketime;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBonusAssignErrLogSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SerialNo";
        pk[1] = "PolNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getGetMode()
    {
        if (GetMode != null && !GetMode.equals("") && SysConst.CHANGECHARSET == true)
        {
            GetMode = StrTool.unicodeToGBK(GetMode);
        }
        return GetMode;
    }

    public void setGetMode(String aGetMode)
    {
        GetMode = aGetMode;
    }

    public String geterrMsg()
    {
        if (errMsg != null && !errMsg.equals("") && SysConst.CHANGECHARSET == true)
        {
            errMsg = StrTool.unicodeToGBK(errMsg);
        }
        return errMsg;
    }

    public void seterrMsg(String aerrMsg)
    {
        errMsg = aerrMsg;
    }

    public String getmakedate()
    {
        if (makedate != null)
        {
            return fDate.getString(makedate);
        }
        else
        {
            return null;
        }
    }

    public void setmakedate(Date amakedate)
    {
        makedate = amakedate;
    }

    public void setmakedate(String amakedate)
    {
        if (amakedate != null && !amakedate.equals(""))
        {
            makedate = fDate.getDate(amakedate);
        }
        else
        {
            makedate = null;
        }
    }

    public String getmaketime()
    {
        if (maketime != null && !maketime.equals("") && SysConst.CHANGECHARSET == true)
        {
            maketime = StrTool.unicodeToGBK(maketime);
        }
        return maketime;
    }

    public void setmaketime(String amaketime)
    {
        maketime = amaketime;
    }

    /**
     * 使用另外一个 LOBonusAssignErrLogSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LOBonusAssignErrLogSchema aLOBonusAssignErrLogSchema)
    {
        this.SerialNo = aLOBonusAssignErrLogSchema.getSerialNo();
        this.PolNo = aLOBonusAssignErrLogSchema.getPolNo();
        this.GetMode = aLOBonusAssignErrLogSchema.getGetMode();
        this.errMsg = aLOBonusAssignErrLogSchema.geterrMsg();
        this.makedate = fDate.getDate(aLOBonusAssignErrLogSchema.getmakedate());
        this.maketime = aLOBonusAssignErrLogSchema.getmaketime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("GetMode") == null)
            {
                this.GetMode = null;
            }
            else
            {
                this.GetMode = rs.getString("GetMode").trim();
            }

            if (rs.getString("errMsg") == null)
            {
                this.errMsg = null;
            }
            else
            {
                this.errMsg = rs.getString("errMsg").trim();
            }

            this.makedate = rs.getDate("makedate");
            if (rs.getString("maketime") == null)
            {
                this.maketime = null;
            }
            else
            {
                this.maketime = rs.getString("maketime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBonusAssignErrLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOBonusAssignErrLogSchema getSchema()
    {
        LOBonusAssignErrLogSchema aLOBonusAssignErrLogSchema = new
                LOBonusAssignErrLogSchema();
        aLOBonusAssignErrLogSchema.setSchema(this);
        return aLOBonusAssignErrLogSchema;
    }

    public LOBonusAssignErrLogDB getDB()
    {
        LOBonusAssignErrLogDB aDBOper = new LOBonusAssignErrLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBonusAssignErrLog描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetMode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(errMsg)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(makedate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(maketime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBonusAssignErrLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            GetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            errMsg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBonusAssignErrLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("GetMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetMode));
        }
        if (FCode.equals("errMsg"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(errMsg));
        }
        if (FCode.equals("makedate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getmakedate()));
        }
        if (FCode.equals("maketime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(maketime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GetMode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(errMsg);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getmakedate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(maketime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("GetMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetMode = FValue.trim();
            }
            else
            {
                GetMode = null;
            }
        }
        if (FCode.equals("errMsg"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                errMsg = FValue.trim();
            }
            else
            {
                errMsg = null;
            }
        }
        if (FCode.equals("makedate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                makedate = fDate.getDate(FValue);
            }
            else
            {
                makedate = null;
            }
        }
        if (FCode.equals("maketime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                maketime = FValue.trim();
            }
            else
            {
                maketime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOBonusAssignErrLogSchema other = (LOBonusAssignErrLogSchema)
                                          otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && PolNo.equals(other.getPolNo())
                && GetMode.equals(other.getGetMode())
                && errMsg.equals(other.geterrMsg())
                && fDate.getString(makedate).equals(other.getmakedate())
                && maketime.equals(other.getmaketime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 1;
        }
        if (strFieldName.equals("GetMode"))
        {
            return 2;
        }
        if (strFieldName.equals("errMsg"))
        {
            return 3;
        }
        if (strFieldName.equals("makedate"))
        {
            return 4;
        }
        if (strFieldName.equals("maketime"))
        {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "PolNo";
                break;
            case 2:
                strFieldName = "GetMode";
                break;
            case 3:
                strFieldName = "errMsg";
                break;
            case 4:
                strFieldName = "makedate";
                break;
            case 5:
                strFieldName = "maketime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("errMsg"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("makedate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("maketime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
