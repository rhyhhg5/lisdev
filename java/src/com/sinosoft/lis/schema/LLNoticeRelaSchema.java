/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LLNoticeRelaDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LLNoticeRelaSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LLNoticeRelaSchema implements Schema
{
    // @Field
    /** 分报案号(事件号) */
    private String SubRptNo;
    /** 通知号 */
    private String NoticeNo;

    public static final int FIELDNUM = 2; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLNoticeRelaSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SubRptNo";
        pk[1] = "NoticeNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSubRptNo()
    {
        if (SubRptNo != null && !SubRptNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SubRptNo = StrTool.unicodeToGBK(SubRptNo);
        }
        return SubRptNo;
    }

    public void setSubRptNo(String aSubRptNo)
    {
        SubRptNo = aSubRptNo;
    }

    public String getNoticeNo()
    {
        if (NoticeNo != null && !NoticeNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            NoticeNo = StrTool.unicodeToGBK(NoticeNo);
        }
        return NoticeNo;
    }

    public void setNoticeNo(String aNoticeNo)
    {
        NoticeNo = aNoticeNo;
    }

    /**
     * 使用另外一个 LLNoticeRelaSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLNoticeRelaSchema aLLNoticeRelaSchema)
    {
        this.SubRptNo = aLLNoticeRelaSchema.getSubRptNo();
        this.NoticeNo = aLLNoticeRelaSchema.getNoticeNo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SubRptNo") == null)
            {
                this.SubRptNo = null;
            }
            else
            {
                this.SubRptNo = rs.getString("SubRptNo").trim();
            }

            if (rs.getString("NoticeNo") == null)
            {
                this.NoticeNo = null;
            }
            else
            {
                this.NoticeNo = rs.getString("NoticeNo").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLNoticeRelaSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLNoticeRelaSchema getSchema()
    {
        LLNoticeRelaSchema aLLNoticeRelaSchema = new LLNoticeRelaSchema();
        aLLNoticeRelaSchema.setSchema(this);
        return aLLNoticeRelaSchema;
    }

    public LLNoticeRelaDB getDB()
    {
        LLNoticeRelaDB aDBOper = new LLNoticeRelaDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLNoticeRela描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SubRptNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NoticeNo));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLNoticeRela>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SubRptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            NoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLNoticeRelaSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SubRptNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SubRptNo));
        }
        if (FCode.equals("NoticeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NoticeNo));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SubRptNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(NoticeNo);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SubRptNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubRptNo = FValue.trim();
            }
            else
            {
                SubRptNo = null;
            }
        }
        if (FCode.equals("NoticeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NoticeNo = FValue.trim();
            }
            else
            {
                NoticeNo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLNoticeRelaSchema other = (LLNoticeRelaSchema) otherObject;
        return
                SubRptNo.equals(other.getSubRptNo())
                && NoticeNo.equals(other.getNoticeNo());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SubRptNo"))
        {
            return 0;
        }
        if (strFieldName.equals("NoticeNo"))
        {
            return 1;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SubRptNo";
                break;
            case 1:
                strFieldName = "NoticeNo";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SubRptNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NoticeNo"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
