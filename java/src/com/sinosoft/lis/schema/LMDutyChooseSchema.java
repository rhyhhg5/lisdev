/*
 * <p>ClassName: LMDutyChooseSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMDutyChooseDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMDutyChooseSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 责任代码 */
    private String DutyCode;
    /** 责任名称 */
    private String DutyName;
    /** 关联责任 */
    private String RelaDutyCode;
    /** 关联责任名称 */
    private String RelaDutyName;
    /** 关联关系 */
    private String Relation;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMDutyChooseSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "RiskCode";
        pk[1] = "RiskVer";
        pk[2] = "DutyCode";
        pk[3] = "RelaDutyCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getDutyCode()
    {
        if (DutyCode != null && !DutyCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyCode = StrTool.unicodeToGBK(DutyCode);
        }
        return DutyCode;
    }

    public void setDutyCode(String aDutyCode)
    {
        DutyCode = aDutyCode;
    }

    public String getDutyName()
    {
        if (DutyName != null && !DutyName.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyName = StrTool.unicodeToGBK(DutyName);
        }
        return DutyName;
    }

    public void setDutyName(String aDutyName)
    {
        DutyName = aDutyName;
    }

    public String getRelaDutyCode()
    {
        if (RelaDutyCode != null && !RelaDutyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RelaDutyCode = StrTool.unicodeToGBK(RelaDutyCode);
        }
        return RelaDutyCode;
    }

    public void setRelaDutyCode(String aRelaDutyCode)
    {
        RelaDutyCode = aRelaDutyCode;
    }

    public String getRelaDutyName()
    {
        if (RelaDutyName != null && !RelaDutyName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RelaDutyName = StrTool.unicodeToGBK(RelaDutyName);
        }
        return RelaDutyName;
    }

    public void setRelaDutyName(String aRelaDutyName)
    {
        RelaDutyName = aRelaDutyName;
    }

    public String getRelation()
    {
        if (Relation != null && !Relation.equals("") && SysConst.CHANGECHARSET == true)
        {
            Relation = StrTool.unicodeToGBK(Relation);
        }
        return Relation;
    }

    public void setRelation(String aRelation)
    {
        Relation = aRelation;
    }

    /**
     * 使用另外一个 LMDutyChooseSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMDutyChooseSchema aLMDutyChooseSchema)
    {
        this.RiskCode = aLMDutyChooseSchema.getRiskCode();
        this.RiskVer = aLMDutyChooseSchema.getRiskVer();
        this.DutyCode = aLMDutyChooseSchema.getDutyCode();
        this.DutyName = aLMDutyChooseSchema.getDutyName();
        this.RelaDutyCode = aLMDutyChooseSchema.getRelaDutyCode();
        this.RelaDutyName = aLMDutyChooseSchema.getRelaDutyName();
        this.Relation = aLMDutyChooseSchema.getRelation();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("DutyCode") == null)
            {
                this.DutyCode = null;
            }
            else
            {
                this.DutyCode = rs.getString("DutyCode").trim();
            }

            if (rs.getString("DutyName") == null)
            {
                this.DutyName = null;
            }
            else
            {
                this.DutyName = rs.getString("DutyName").trim();
            }

            if (rs.getString("RelaDutyCode") == null)
            {
                this.RelaDutyCode = null;
            }
            else
            {
                this.RelaDutyCode = rs.getString("RelaDutyCode").trim();
            }

            if (rs.getString("RelaDutyName") == null)
            {
                this.RelaDutyName = null;
            }
            else
            {
                this.RelaDutyName = rs.getString("RelaDutyName").trim();
            }

            if (rs.getString("Relation") == null)
            {
                this.Relation = null;
            }
            else
            {
                this.Relation = rs.getString("Relation").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMDutyChooseSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMDutyChooseSchema getSchema()
    {
        LMDutyChooseSchema aLMDutyChooseSchema = new LMDutyChooseSchema();
        aLMDutyChooseSchema.setSchema(this);
        return aLMDutyChooseSchema;
    }

    public LMDutyChooseDB getDB()
    {
        LMDutyChooseDB aDBOper = new LMDutyChooseDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMDutyChoose描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DutyName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RelaDutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RelaDutyName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Relation));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMDutyChoose>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            DutyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            RelaDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            RelaDutyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            Relation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMDutyChooseSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("DutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyCode));
        }
        if (FCode.equals("DutyName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyName));
        }
        if (FCode.equals("RelaDutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RelaDutyCode));
        }
        if (FCode.equals("RelaDutyName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RelaDutyName));
        }
        if (FCode.equals("Relation"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Relation));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(DutyName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RelaDutyCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(RelaDutyName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Relation);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("DutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
            {
                DutyCode = null;
            }
        }
        if (FCode.equals("DutyName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyName = FValue.trim();
            }
            else
            {
                DutyName = null;
            }
        }
        if (FCode.equals("RelaDutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelaDutyCode = FValue.trim();
            }
            else
            {
                RelaDutyCode = null;
            }
        }
        if (FCode.equals("RelaDutyName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelaDutyName = FValue.trim();
            }
            else
            {
                RelaDutyName = null;
            }
        }
        if (FCode.equals("Relation"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Relation = FValue.trim();
            }
            else
            {
                Relation = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMDutyChooseSchema other = (LMDutyChooseSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && DutyCode.equals(other.getDutyCode())
                && DutyName.equals(other.getDutyName())
                && RelaDutyCode.equals(other.getRelaDutyCode())
                && RelaDutyName.equals(other.getRelaDutyName())
                && Relation.equals(other.getRelation());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 1;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return 2;
        }
        if (strFieldName.equals("DutyName"))
        {
            return 3;
        }
        if (strFieldName.equals("RelaDutyCode"))
        {
            return 4;
        }
        if (strFieldName.equals("RelaDutyName"))
        {
            return 5;
        }
        if (strFieldName.equals("Relation"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "DutyCode";
                break;
            case 3:
                strFieldName = "DutyName";
                break;
            case 4:
                strFieldName = "RelaDutyCode";
                break;
            case 5:
                strFieldName = "RelaDutyName";
                break;
            case 6:
                strFieldName = "Relation";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaDutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelaDutyName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Relation"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
