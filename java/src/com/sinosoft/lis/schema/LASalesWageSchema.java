/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LASalesWageDB;

/*
 * <p>ClassName: LASalesWageSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2014-01-14
 */
public class LASalesWageSchema implements Schema, Cloneable
{
	// @Field
	/** 业务员编码 */
	private String SalesCode;
	/** 所属团队 */
	private String AgentGroup;
	/** 管理机构 */
	private String ManageCom;
	/** 所属年月 */
	private String IndexCalNo;
	/** 固定工资 */
	private double RegularWage;
	/** 绩效提奖 */
	private double MonthPerformance;
	/** 其他季、年绩效 */
	private double OtherPerformance;
	/** 续期保费提奖 */
	private double RenewalWage;
	/** 期末所辖人力 */
	private double TeamCount;
	/** T1 */
	private double T1;
	/** T2 */
	private double T2;
	/** T3 */
	private double T3;
	/** T4 */
	private double T4;
	/** T5 */
	private double T5;
	/** T6 */
	private double T6;
	/** T7 */
	private double T7;
	/** 所属渠道 */
	private String BranchType;
	/** 人员类别 */
	private String BranchType2;
	/** 审核级别 */
	private String ExamineState;
	/** 薪资状态 */
	private String State;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LASalesWageSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "SalesCode";
		pk[1] = "IndexCalNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LASalesWageSchema cloned = (LASalesWageSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSalesCode()
	{
		return SalesCode;
	}
	public void setSalesCode(String aSalesCode)
	{
		SalesCode = aSalesCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getIndexCalNo()
	{
		return IndexCalNo;
	}
	public void setIndexCalNo(String aIndexCalNo)
	{
		IndexCalNo = aIndexCalNo;
	}
	public double getRegularWage()
	{
		return RegularWage;
	}
	public void setRegularWage(double aRegularWage)
	{
		RegularWage = Arith.round(aRegularWage,2);
	}
	public void setRegularWage(String aRegularWage)
	{
		if (aRegularWage != null && !aRegularWage.equals(""))
		{
			Double tDouble = new Double(aRegularWage);
			double d = tDouble.doubleValue();
                RegularWage = Arith.round(d,2);
		}
	}

	public double getMonthPerformance()
	{
		return MonthPerformance;
	}
	public void setMonthPerformance(double aMonthPerformance)
	{
		MonthPerformance = Arith.round(aMonthPerformance,2);
	}
	public void setMonthPerformance(String aMonthPerformance)
	{
		if (aMonthPerformance != null && !aMonthPerformance.equals(""))
		{
			Double tDouble = new Double(aMonthPerformance);
			double d = tDouble.doubleValue();
                MonthPerformance = Arith.round(d,2);
		}
	}

	public double getOtherPerformance()
	{
		return OtherPerformance;
	}
	public void setOtherPerformance(double aOtherPerformance)
	{
		OtherPerformance = Arith.round(aOtherPerformance,2);
	}
	public void setOtherPerformance(String aOtherPerformance)
	{
		if (aOtherPerformance != null && !aOtherPerformance.equals(""))
		{
			Double tDouble = new Double(aOtherPerformance);
			double d = tDouble.doubleValue();
                OtherPerformance = Arith.round(d,2);
		}
	}

	public double getRenewalWage()
	{
		return RenewalWage;
	}
	public void setRenewalWage(double aRenewalWage)
	{
		RenewalWage = Arith.round(aRenewalWage,2);
	}
	public void setRenewalWage(String aRenewalWage)
	{
		if (aRenewalWage != null && !aRenewalWage.equals(""))
		{
			Double tDouble = new Double(aRenewalWage);
			double d = tDouble.doubleValue();
                RenewalWage = Arith.round(d,2);
		}
	}

	public double getTeamCount()
	{
		return TeamCount;
	}
	public void setTeamCount(double aTeamCount)
	{
		TeamCount = Arith.round(aTeamCount,2);
	}
	public void setTeamCount(String aTeamCount)
	{
		if (aTeamCount != null && !aTeamCount.equals(""))
		{
			Double tDouble = new Double(aTeamCount);
			double d = tDouble.doubleValue();
                TeamCount = Arith.round(d,2);
		}
	}

	public double getT1()
	{
		return T1;
	}
	public void setT1(double aT1)
	{
		T1 = Arith.round(aT1,2);
	}
	public void setT1(String aT1)
	{
		if (aT1 != null && !aT1.equals(""))
		{
			Double tDouble = new Double(aT1);
			double d = tDouble.doubleValue();
                T1 = Arith.round(d,2);
		}
	}

	public double getT2()
	{
		return T2;
	}
	public void setT2(double aT2)
	{
		T2 = Arith.round(aT2,2);
	}
	public void setT2(String aT2)
	{
		if (aT2 != null && !aT2.equals(""))
		{
			Double tDouble = new Double(aT2);
			double d = tDouble.doubleValue();
                T2 = Arith.round(d,2);
		}
	}

	public double getT3()
	{
		return T3;
	}
	public void setT3(double aT3)
	{
		T3 = Arith.round(aT3,2);
	}
	public void setT3(String aT3)
	{
		if (aT3 != null && !aT3.equals(""))
		{
			Double tDouble = new Double(aT3);
			double d = tDouble.doubleValue();
                T3 = Arith.round(d,2);
		}
	}

	public double getT4()
	{
		return T4;
	}
	public void setT4(double aT4)
	{
		T4 = Arith.round(aT4,2);
	}
	public void setT4(String aT4)
	{
		if (aT4 != null && !aT4.equals(""))
		{
			Double tDouble = new Double(aT4);
			double d = tDouble.doubleValue();
                T4 = Arith.round(d,2);
		}
	}

	public double getT5()
	{
		return T5;
	}
	public void setT5(double aT5)
	{
		T5 = Arith.round(aT5,2);
	}
	public void setT5(String aT5)
	{
		if (aT5 != null && !aT5.equals(""))
		{
			Double tDouble = new Double(aT5);
			double d = tDouble.doubleValue();
                T5 = Arith.round(d,2);
		}
	}

	public double getT6()
	{
		return T6;
	}
	public void setT6(double aT6)
	{
		T6 = Arith.round(aT6,2);
	}
	public void setT6(String aT6)
	{
		if (aT6 != null && !aT6.equals(""))
		{
			Double tDouble = new Double(aT6);
			double d = tDouble.doubleValue();
                T6 = Arith.round(d,2);
		}
	}

	public double getT7()
	{
		return T7;
	}
	public void setT7(double aT7)
	{
		T7 = Arith.round(aT7,2);
	}
	public void setT7(String aT7)
	{
		if (aT7 != null && !aT7.equals(""))
		{
			Double tDouble = new Double(aT7);
			double d = tDouble.doubleValue();
                T7 = Arith.round(d,2);
		}
	}

	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getExamineState()
	{
		return ExamineState;
	}
	public void setExamineState(String aExamineState)
	{
		ExamineState = aExamineState;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LASalesWageSchema 对象给 Schema 赋值
	* @param: aLASalesWageSchema LASalesWageSchema
	**/
	public void setSchema(LASalesWageSchema aLASalesWageSchema)
	{
		this.SalesCode = aLASalesWageSchema.getSalesCode();
		this.AgentGroup = aLASalesWageSchema.getAgentGroup();
		this.ManageCom = aLASalesWageSchema.getManageCom();
		this.IndexCalNo = aLASalesWageSchema.getIndexCalNo();
		this.RegularWage = aLASalesWageSchema.getRegularWage();
		this.MonthPerformance = aLASalesWageSchema.getMonthPerformance();
		this.OtherPerformance = aLASalesWageSchema.getOtherPerformance();
		this.RenewalWage = aLASalesWageSchema.getRenewalWage();
		this.TeamCount = aLASalesWageSchema.getTeamCount();
		this.T1 = aLASalesWageSchema.getT1();
		this.T2 = aLASalesWageSchema.getT2();
		this.T3 = aLASalesWageSchema.getT3();
		this.T4 = aLASalesWageSchema.getT4();
		this.T5 = aLASalesWageSchema.getT5();
		this.T6 = aLASalesWageSchema.getT6();
		this.T7 = aLASalesWageSchema.getT7();
		this.BranchType = aLASalesWageSchema.getBranchType();
		this.BranchType2 = aLASalesWageSchema.getBranchType2();
		this.ExamineState = aLASalesWageSchema.getExamineState();
		this.State = aLASalesWageSchema.getState();
		this.Operator = aLASalesWageSchema.getOperator();
		this.MakeDate = fDate.getDate( aLASalesWageSchema.getMakeDate());
		this.MakeTime = aLASalesWageSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLASalesWageSchema.getModifyDate());
		this.ModifyTime = aLASalesWageSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SalesCode") == null )
				this.SalesCode = null;
			else
				this.SalesCode = rs.getString("SalesCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("IndexCalNo") == null )
				this.IndexCalNo = null;
			else
				this.IndexCalNo = rs.getString("IndexCalNo").trim();

			this.RegularWage = rs.getDouble("RegularWage");
			this.MonthPerformance = rs.getDouble("MonthPerformance");
			this.OtherPerformance = rs.getDouble("OtherPerformance");
			this.RenewalWage = rs.getDouble("RenewalWage");
			this.TeamCount = rs.getDouble("TeamCount");
			this.T1 = rs.getDouble("T1");
			this.T2 = rs.getDouble("T2");
			this.T3 = rs.getDouble("T3");
			this.T4 = rs.getDouble("T4");
			this.T5 = rs.getDouble("T5");
			this.T6 = rs.getDouble("T6");
			this.T7 = rs.getDouble("T7");
			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("ExamineState") == null )
				this.ExamineState = null;
			else
				this.ExamineState = rs.getString("ExamineState").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LASalesWage表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LASalesWageSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LASalesWageSchema getSchema()
	{
		LASalesWageSchema aLASalesWageSchema = new LASalesWageSchema();
		aLASalesWageSchema.setSchema(this);
		return aLASalesWageSchema;
	}

	public LASalesWageDB getDB()
	{
		LASalesWageDB aDBOper = new LASalesWageDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLASalesWage描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SalesCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexCalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RegularWage));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MonthPerformance));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherPerformance));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RenewalWage));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TeamCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(T1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(T2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(T3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(T4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(T5));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(T6));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(T7));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExamineState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLASalesWage>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SalesCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			IndexCalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RegularWage = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			MonthPerformance = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			OtherPerformance = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			RenewalWage = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			TeamCount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			T1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			T2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			T3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			T4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			T5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			T6 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			T7 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ExamineState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LASalesWageSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SalesCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SalesCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("IndexCalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCalNo));
		}
		if (FCode.equals("RegularWage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RegularWage));
		}
		if (FCode.equals("MonthPerformance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MonthPerformance));
		}
		if (FCode.equals("OtherPerformance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPerformance));
		}
		if (FCode.equals("RenewalWage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RenewalWage));
		}
		if (FCode.equals("TeamCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TeamCount));
		}
		if (FCode.equals("T1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T1));
		}
		if (FCode.equals("T2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T2));
		}
		if (FCode.equals("T3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T3));
		}
		if (FCode.equals("T4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T4));
		}
		if (FCode.equals("T5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T5));
		}
		if (FCode.equals("T6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T6));
		}
		if (FCode.equals("T7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T7));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("ExamineState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExamineState));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SalesCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(IndexCalNo);
				break;
			case 4:
				strFieldValue = String.valueOf(RegularWage);
				break;
			case 5:
				strFieldValue = String.valueOf(MonthPerformance);
				break;
			case 6:
				strFieldValue = String.valueOf(OtherPerformance);
				break;
			case 7:
				strFieldValue = String.valueOf(RenewalWage);
				break;
			case 8:
				strFieldValue = String.valueOf(TeamCount);
				break;
			case 9:
				strFieldValue = String.valueOf(T1);
				break;
			case 10:
				strFieldValue = String.valueOf(T2);
				break;
			case 11:
				strFieldValue = String.valueOf(T3);
				break;
			case 12:
				strFieldValue = String.valueOf(T4);
				break;
			case 13:
				strFieldValue = String.valueOf(T5);
				break;
			case 14:
				strFieldValue = String.valueOf(T6);
				break;
			case 15:
				strFieldValue = String.valueOf(T7);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ExamineState);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SalesCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SalesCode = FValue.trim();
			}
			else
				SalesCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("IndexCalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexCalNo = FValue.trim();
			}
			else
				IndexCalNo = null;
		}
		if (FCode.equalsIgnoreCase("RegularWage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RegularWage = d;
			}
		}
		if (FCode.equalsIgnoreCase("MonthPerformance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MonthPerformance = d;
			}
		}
		if (FCode.equalsIgnoreCase("OtherPerformance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				OtherPerformance = d;
			}
		}
		if (FCode.equalsIgnoreCase("RenewalWage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RenewalWage = d;
			}
		}
		if (FCode.equalsIgnoreCase("TeamCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TeamCount = d;
			}
		}
		if (FCode.equalsIgnoreCase("T1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("T2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("T3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("T4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("T5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T5 = d;
			}
		}
		if (FCode.equalsIgnoreCase("T6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T6 = d;
			}
		}
		if (FCode.equalsIgnoreCase("T7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				T7 = d;
			}
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("ExamineState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExamineState = FValue.trim();
			}
			else
				ExamineState = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LASalesWageSchema other = (LASalesWageSchema)otherObject;
		return
			(SalesCode == null ? other.getSalesCode() == null : SalesCode.equals(other.getSalesCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (IndexCalNo == null ? other.getIndexCalNo() == null : IndexCalNo.equals(other.getIndexCalNo()))
			&& RegularWage == other.getRegularWage()
			&& MonthPerformance == other.getMonthPerformance()
			&& OtherPerformance == other.getOtherPerformance()
			&& RenewalWage == other.getRenewalWage()
			&& TeamCount == other.getTeamCount()
			&& T1 == other.getT1()
			&& T2 == other.getT2()
			&& T3 == other.getT3()
			&& T4 == other.getT4()
			&& T5 == other.getT5()
			&& T6 == other.getT6()
			&& T7 == other.getT7()
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (ExamineState == null ? other.getExamineState() == null : ExamineState.equals(other.getExamineState()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SalesCode") ) {
			return 0;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 1;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("IndexCalNo") ) {
			return 3;
		}
		if( strFieldName.equals("RegularWage") ) {
			return 4;
		}
		if( strFieldName.equals("MonthPerformance") ) {
			return 5;
		}
		if( strFieldName.equals("OtherPerformance") ) {
			return 6;
		}
		if( strFieldName.equals("RenewalWage") ) {
			return 7;
		}
		if( strFieldName.equals("TeamCount") ) {
			return 8;
		}
		if( strFieldName.equals("T1") ) {
			return 9;
		}
		if( strFieldName.equals("T2") ) {
			return 10;
		}
		if( strFieldName.equals("T3") ) {
			return 11;
		}
		if( strFieldName.equals("T4") ) {
			return 12;
		}
		if( strFieldName.equals("T5") ) {
			return 13;
		}
		if( strFieldName.equals("T6") ) {
			return 14;
		}
		if( strFieldName.equals("T7") ) {
			return 15;
		}
		if( strFieldName.equals("BranchType") ) {
			return 16;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 17;
		}
		if( strFieldName.equals("ExamineState") ) {
			return 18;
		}
		if( strFieldName.equals("State") ) {
			return 19;
		}
		if( strFieldName.equals("Operator") ) {
			return 20;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 21;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SalesCode";
				break;
			case 1:
				strFieldName = "AgentGroup";
				break;
			case 2:
				strFieldName = "ManageCom";
				break;
			case 3:
				strFieldName = "IndexCalNo";
				break;
			case 4:
				strFieldName = "RegularWage";
				break;
			case 5:
				strFieldName = "MonthPerformance";
				break;
			case 6:
				strFieldName = "OtherPerformance";
				break;
			case 7:
				strFieldName = "RenewalWage";
				break;
			case 8:
				strFieldName = "TeamCount";
				break;
			case 9:
				strFieldName = "T1";
				break;
			case 10:
				strFieldName = "T2";
				break;
			case 11:
				strFieldName = "T3";
				break;
			case 12:
				strFieldName = "T4";
				break;
			case 13:
				strFieldName = "T5";
				break;
			case 14:
				strFieldName = "T6";
				break;
			case 15:
				strFieldName = "T7";
				break;
			case 16:
				strFieldName = "BranchType";
				break;
			case 17:
				strFieldName = "BranchType2";
				break;
			case 18:
				strFieldName = "ExamineState";
				break;
			case 19:
				strFieldName = "State";
				break;
			case 20:
				strFieldName = "Operator";
				break;
			case 21:
				strFieldName = "MakeDate";
				break;
			case 22:
				strFieldName = "MakeTime";
				break;
			case 23:
				strFieldName = "ModifyDate";
				break;
			case 24:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SalesCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexCalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RegularWage") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MonthPerformance") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OtherPerformance") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RenewalWage") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TeamCount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T5") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T6") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("T7") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExamineState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
