/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRPolEdorDB;

/*
 * <p>ClassName: LRPolEdorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 模拟表结构更改
 * @CreateDate：2008-12-03
 */
public class LRPolEdorSchema implements Schema, Cloneable
{
	// @Field
	/** 团体合同号 */
	private String GrpContNo;
	/** 集体保单号码 */
	private String GrpPolNo;
	/** 总单/合同号码 */
	private String ContNo;
	/** 保单号码 */
	private String PolNo;
	/** 保全批单号 */
	private String EdorNo;
	/** 再保合同号 */
	private String ReContCode;
	/** 再保项目 */
	private String ReinsureItem;
	/** 再保公司号 */
	private String ReComCode;
	/** 险种计算分类 */
	private String RiskCalSort;
	/** 主险保单号码 */
	private String MainPolNo;
	/** 主被保人保单号码 */
	private String MasterPolNo;
	/** 险种编码 */
	private String RiskCode;
	/** 险种版本 */
	private String RiskVersion;
	/** 保全生效日期 */
	private Date EdorValidate;
	/** 未经过天数 */
	private int NoPassDays;
	/** 补/退费业务类型 */
	private String FeeoperationType;
	/** 退保金额 */
	private double GetMoney;
	/** 退保退费日期 */
	private Date GetConfDate;
	/** 累计退保保额 */
	private double SumBackAmnt;
	/** 数据提取日期 */
	private Date GetDataDate;
	/** 操作人 */
	private String Operator;
	/** 管理机构 */
	private String ManageCom;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 续保次数 */
	private int ReNewCount;
	/** 成本中心编码 */
	private String CostCenter;
	/** 分保规则 */
	private String ReType;
	/** 结算标记 */
	private String ActuGetState;
	/** 结算号码 */
	private String ActuGetNo;

	public static final int FIELDNUM = 31;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRPolEdorSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "PolNo";
		pk[1] = "EdorNo";
		pk[2] = "ReContCode";
		pk[3] = "ReinsureItem";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRPolEdorSchema cloned = (LRPolEdorSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpPolNo()
	{
		return GrpPolNo;
	}
	public void setGrpPolNo(String aGrpPolNo)
	{
		GrpPolNo = aGrpPolNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getReContCode()
	{
		return ReContCode;
	}
	public void setReContCode(String aReContCode)
	{
		ReContCode = aReContCode;
	}
	public String getReinsureItem()
	{
		return ReinsureItem;
	}
	public void setReinsureItem(String aReinsureItem)
	{
		ReinsureItem = aReinsureItem;
	}
	public String getReComCode()
	{
		return ReComCode;
	}
	public void setReComCode(String aReComCode)
	{
		ReComCode = aReComCode;
	}
	public String getRiskCalSort()
	{
		return RiskCalSort;
	}
	public void setRiskCalSort(String aRiskCalSort)
	{
		RiskCalSort = aRiskCalSort;
	}
	public String getMainPolNo()
	{
		return MainPolNo;
	}
	public void setMainPolNo(String aMainPolNo)
	{
		MainPolNo = aMainPolNo;
	}
	public String getMasterPolNo()
	{
		return MasterPolNo;
	}
	public void setMasterPolNo(String aMasterPolNo)
	{
		MasterPolNo = aMasterPolNo;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskVersion()
	{
		return RiskVersion;
	}
	public void setRiskVersion(String aRiskVersion)
	{
		RiskVersion = aRiskVersion;
	}
	public String getEdorValidate()
	{
		if( EdorValidate != null )
			return fDate.getString(EdorValidate);
		else
			return null;
	}
	public void setEdorValidate(Date aEdorValidate)
	{
		EdorValidate = aEdorValidate;
	}
	public void setEdorValidate(String aEdorValidate)
	{
		if (aEdorValidate != null && !aEdorValidate.equals("") )
		{
			EdorValidate = fDate.getDate( aEdorValidate );
		}
		else
			EdorValidate = null;
	}

	public int getNoPassDays()
	{
		return NoPassDays;
	}
	public void setNoPassDays(int aNoPassDays)
	{
		NoPassDays = aNoPassDays;
	}
	public void setNoPassDays(String aNoPassDays)
	{
		if (aNoPassDays != null && !aNoPassDays.equals(""))
		{
			Integer tInteger = new Integer(aNoPassDays);
			int i = tInteger.intValue();
			NoPassDays = i;
		}
	}

	public String getFeeoperationType()
	{
		return FeeoperationType;
	}
	public void setFeeoperationType(String aFeeoperationType)
	{
		FeeoperationType = aFeeoperationType;
	}
	public double getGetMoney()
	{
		return GetMoney;
	}
	public void setGetMoney(double aGetMoney)
	{
		GetMoney = Arith.round(aGetMoney,2);
	}
	public void setGetMoney(String aGetMoney)
	{
		if (aGetMoney != null && !aGetMoney.equals(""))
		{
			Double tDouble = new Double(aGetMoney);
			double d = tDouble.doubleValue();
                GetMoney = Arith.round(d,2);
		}
	}

	public String getGetConfDate()
	{
		if( GetConfDate != null )
			return fDate.getString(GetConfDate);
		else
			return null;
	}
	public void setGetConfDate(Date aGetConfDate)
	{
		GetConfDate = aGetConfDate;
	}
	public void setGetConfDate(String aGetConfDate)
	{
		if (aGetConfDate != null && !aGetConfDate.equals("") )
		{
			GetConfDate = fDate.getDate( aGetConfDate );
		}
		else
			GetConfDate = null;
	}

	public double getSumBackAmnt()
	{
		return SumBackAmnt;
	}
	public void setSumBackAmnt(double aSumBackAmnt)
	{
		SumBackAmnt = Arith.round(aSumBackAmnt,2);
	}
	public void setSumBackAmnt(String aSumBackAmnt)
	{
		if (aSumBackAmnt != null && !aSumBackAmnt.equals(""))
		{
			Double tDouble = new Double(aSumBackAmnt);
			double d = tDouble.doubleValue();
                SumBackAmnt = Arith.round(d,2);
		}
	}

	public String getGetDataDate()
	{
		if( GetDataDate != null )
			return fDate.getString(GetDataDate);
		else
			return null;
	}
	public void setGetDataDate(Date aGetDataDate)
	{
		GetDataDate = aGetDataDate;
	}
	public void setGetDataDate(String aGetDataDate)
	{
		if (aGetDataDate != null && !aGetDataDate.equals("") )
		{
			GetDataDate = fDate.getDate( aGetDataDate );
		}
		else
			GetDataDate = null;
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public int getReNewCount()
	{
		return ReNewCount;
	}
	public void setReNewCount(int aReNewCount)
	{
		ReNewCount = aReNewCount;
	}
	public void setReNewCount(String aReNewCount)
	{
		if (aReNewCount != null && !aReNewCount.equals(""))
		{
			Integer tInteger = new Integer(aReNewCount);
			int i = tInteger.intValue();
			ReNewCount = i;
		}
	}

	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getReType()
	{
		return ReType;
	}
	public void setReType(String aReType)
	{
		ReType = aReType;
	}
	public String getActuGetState()
	{
		return ActuGetState;
	}
	public void setActuGetState(String aActuGetState)
	{
		ActuGetState = aActuGetState;
	}
	public String getActuGetNo()
	{
		return ActuGetNo;
	}
	public void setActuGetNo(String aActuGetNo)
	{
		ActuGetNo = aActuGetNo;
	}

	/**
	* 使用另外一个 LRPolEdorSchema 对象给 Schema 赋值
	* @param: aLRPolEdorSchema LRPolEdorSchema
	**/
	public void setSchema(LRPolEdorSchema aLRPolEdorSchema)
	{
		this.GrpContNo = aLRPolEdorSchema.getGrpContNo();
		this.GrpPolNo = aLRPolEdorSchema.getGrpPolNo();
		this.ContNo = aLRPolEdorSchema.getContNo();
		this.PolNo = aLRPolEdorSchema.getPolNo();
		this.EdorNo = aLRPolEdorSchema.getEdorNo();
		this.ReContCode = aLRPolEdorSchema.getReContCode();
		this.ReinsureItem = aLRPolEdorSchema.getReinsureItem();
		this.ReComCode = aLRPolEdorSchema.getReComCode();
		this.RiskCalSort = aLRPolEdorSchema.getRiskCalSort();
		this.MainPolNo = aLRPolEdorSchema.getMainPolNo();
		this.MasterPolNo = aLRPolEdorSchema.getMasterPolNo();
		this.RiskCode = aLRPolEdorSchema.getRiskCode();
		this.RiskVersion = aLRPolEdorSchema.getRiskVersion();
		this.EdorValidate = fDate.getDate( aLRPolEdorSchema.getEdorValidate());
		this.NoPassDays = aLRPolEdorSchema.getNoPassDays();
		this.FeeoperationType = aLRPolEdorSchema.getFeeoperationType();
		this.GetMoney = aLRPolEdorSchema.getGetMoney();
		this.GetConfDate = fDate.getDate( aLRPolEdorSchema.getGetConfDate());
		this.SumBackAmnt = aLRPolEdorSchema.getSumBackAmnt();
		this.GetDataDate = fDate.getDate( aLRPolEdorSchema.getGetDataDate());
		this.Operator = aLRPolEdorSchema.getOperator();
		this.ManageCom = aLRPolEdorSchema.getManageCom();
		this.MakeDate = fDate.getDate( aLRPolEdorSchema.getMakeDate());
		this.MakeTime = aLRPolEdorSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRPolEdorSchema.getModifyDate());
		this.ModifyTime = aLRPolEdorSchema.getModifyTime();
		this.ReNewCount = aLRPolEdorSchema.getReNewCount();
		this.CostCenter = aLRPolEdorSchema.getCostCenter();
		this.ReType = aLRPolEdorSchema.getReType();
		this.ActuGetState = aLRPolEdorSchema.getActuGetState();
		this.ActuGetNo = aLRPolEdorSchema.getActuGetNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpPolNo") == null )
				this.GrpPolNo = null;
			else
				this.GrpPolNo = rs.getString("GrpPolNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("ReContCode") == null )
				this.ReContCode = null;
			else
				this.ReContCode = rs.getString("ReContCode").trim();

			if( rs.getString("ReinsureItem") == null )
				this.ReinsureItem = null;
			else
				this.ReinsureItem = rs.getString("ReinsureItem").trim();

			if( rs.getString("ReComCode") == null )
				this.ReComCode = null;
			else
				this.ReComCode = rs.getString("ReComCode").trim();

			if( rs.getString("RiskCalSort") == null )
				this.RiskCalSort = null;
			else
				this.RiskCalSort = rs.getString("RiskCalSort").trim();

			if( rs.getString("MainPolNo") == null )
				this.MainPolNo = null;
			else
				this.MainPolNo = rs.getString("MainPolNo").trim();

			if( rs.getString("MasterPolNo") == null )
				this.MasterPolNo = null;
			else
				this.MasterPolNo = rs.getString("MasterPolNo").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskVersion") == null )
				this.RiskVersion = null;
			else
				this.RiskVersion = rs.getString("RiskVersion").trim();

			this.EdorValidate = rs.getDate("EdorValidate");
			this.NoPassDays = rs.getInt("NoPassDays");
			if( rs.getString("FeeoperationType") == null )
				this.FeeoperationType = null;
			else
				this.FeeoperationType = rs.getString("FeeoperationType").trim();

			this.GetMoney = rs.getDouble("GetMoney");
			this.GetConfDate = rs.getDate("GetConfDate");
			this.SumBackAmnt = rs.getDouble("SumBackAmnt");
			this.GetDataDate = rs.getDate("GetDataDate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.ReNewCount = rs.getInt("ReNewCount");
			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("ReType") == null )
				this.ReType = null;
			else
				this.ReType = rs.getString("ReType").trim();

			if( rs.getString("ActuGetState") == null )
				this.ActuGetState = null;
			else
				this.ActuGetState = rs.getString("ActuGetState").trim();

			if( rs.getString("ActuGetNo") == null )
				this.ActuGetNo = null;
			else
				this.ActuGetNo = rs.getString("ActuGetNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRPolEdor表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRPolEdorSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRPolEdorSchema getSchema()
	{
		LRPolEdorSchema aLRPolEdorSchema = new LRPolEdorSchema();
		aLRPolEdorSchema.setSchema(this);
		return aLRPolEdorSchema;
	}

	public LRPolEdorDB getDB()
	{
		LRPolEdorDB aDBOper = new LRPolEdorDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRPolEdor描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReContCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReinsureItem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCalSort)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MasterPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskVersion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EdorValidate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(NoPassDays));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeoperationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GetMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GetConfDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumBackAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GetDataDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReNewCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActuGetState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ActuGetNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRPolEdor>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ReContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ReinsureItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ReComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RiskCalSort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MainPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MasterPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			EdorValidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			NoPassDays= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).intValue();
			FeeoperationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			GetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			GetConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			SumBackAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			GetDataDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			ReNewCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).intValue();
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			ReType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			ActuGetState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			ActuGetNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRPolEdorSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("ReContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContCode));
		}
		if (FCode.equals("ReinsureItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReinsureItem));
		}
		if (FCode.equals("ReComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReComCode));
		}
		if (FCode.equals("RiskCalSort"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCalSort));
		}
		if (FCode.equals("MainPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainPolNo));
		}
		if (FCode.equals("MasterPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MasterPolNo));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("RiskVersion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
		}
		if (FCode.equals("EdorValidate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEdorValidate()));
		}
		if (FCode.equals("NoPassDays"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NoPassDays));
		}
		if (FCode.equals("FeeoperationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeoperationType));
		}
		if (FCode.equals("GetMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetMoney));
		}
		if (FCode.equals("GetConfDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetConfDate()));
		}
		if (FCode.equals("SumBackAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumBackAmnt));
		}
		if (FCode.equals("GetDataDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetDataDate()));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ReNewCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReNewCount));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("ReType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReType));
		}
		if (FCode.equals("ActuGetState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetState));
		}
		if (FCode.equals("ActuGetNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActuGetNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ReContCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ReinsureItem);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ReComCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RiskCalSort);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MainPolNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MasterPolNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(RiskVersion);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEdorValidate()));
				break;
			case 14:
				strFieldValue = String.valueOf(NoPassDays);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(FeeoperationType);
				break;
			case 16:
				strFieldValue = String.valueOf(GetMoney);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetConfDate()));
				break;
			case 18:
				strFieldValue = String.valueOf(SumBackAmnt);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetDataDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 26:
				strFieldValue = String.valueOf(ReNewCount);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(ReType);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(ActuGetState);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(ActuGetNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpPolNo = FValue.trim();
			}
			else
				GrpPolNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("ReContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContCode = FValue.trim();
			}
			else
				ReContCode = null;
		}
		if (FCode.equalsIgnoreCase("ReinsureItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReinsureItem = FValue.trim();
			}
			else
				ReinsureItem = null;
		}
		if (FCode.equalsIgnoreCase("ReComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReComCode = FValue.trim();
			}
			else
				ReComCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskCalSort"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCalSort = FValue.trim();
			}
			else
				RiskCalSort = null;
		}
		if (FCode.equalsIgnoreCase("MainPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainPolNo = FValue.trim();
			}
			else
				MainPolNo = null;
		}
		if (FCode.equalsIgnoreCase("MasterPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MasterPolNo = FValue.trim();
			}
			else
				MasterPolNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskVersion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskVersion = FValue.trim();
			}
			else
				RiskVersion = null;
		}
		if (FCode.equalsIgnoreCase("EdorValidate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EdorValidate = fDate.getDate( FValue );
			}
			else
				EdorValidate = null;
		}
		if (FCode.equalsIgnoreCase("NoPassDays"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				NoPassDays = i;
			}
		}
		if (FCode.equalsIgnoreCase("FeeoperationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeoperationType = FValue.trim();
			}
			else
				FeeoperationType = null;
		}
		if (FCode.equalsIgnoreCase("GetMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GetMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("GetConfDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GetConfDate = fDate.getDate( FValue );
			}
			else
				GetConfDate = null;
		}
		if (FCode.equalsIgnoreCase("SumBackAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumBackAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("GetDataDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GetDataDate = fDate.getDate( FValue );
			}
			else
				GetDataDate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ReNewCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ReNewCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("ReType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReType = FValue.trim();
			}
			else
				ReType = null;
		}
		if (FCode.equalsIgnoreCase("ActuGetState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActuGetState = FValue.trim();
			}
			else
				ActuGetState = null;
		}
		if (FCode.equalsIgnoreCase("ActuGetNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActuGetNo = FValue.trim();
			}
			else
				ActuGetNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRPolEdorSchema other = (LRPolEdorSchema)otherObject;
		return
			GrpContNo.equals(other.getGrpContNo())
			&& GrpPolNo.equals(other.getGrpPolNo())
			&& ContNo.equals(other.getContNo())
			&& PolNo.equals(other.getPolNo())
			&& EdorNo.equals(other.getEdorNo())
			&& ReContCode.equals(other.getReContCode())
			&& ReinsureItem.equals(other.getReinsureItem())
			&& ReComCode.equals(other.getReComCode())
			&& RiskCalSort.equals(other.getRiskCalSort())
			&& MainPolNo.equals(other.getMainPolNo())
			&& MasterPolNo.equals(other.getMasterPolNo())
			&& RiskCode.equals(other.getRiskCode())
			&& RiskVersion.equals(other.getRiskVersion())
			&& fDate.getString(EdorValidate).equals(other.getEdorValidate())
			&& NoPassDays == other.getNoPassDays()
			&& FeeoperationType.equals(other.getFeeoperationType())
			&& GetMoney == other.getGetMoney()
			&& fDate.getString(GetConfDate).equals(other.getGetConfDate())
			&& SumBackAmnt == other.getSumBackAmnt()
			&& fDate.getString(GetDataDate).equals(other.getGetDataDate())
			&& Operator.equals(other.getOperator())
			&& ManageCom.equals(other.getManageCom())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& ReNewCount == other.getReNewCount()
			&& CostCenter.equals(other.getCostCenter())
			&& ReType.equals(other.getReType())
			&& ActuGetState.equals(other.getActuGetState())
			&& ActuGetNo.equals(other.getActuGetNo());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return 1;
		}
		if( strFieldName.equals("ContNo") ) {
			return 2;
		}
		if( strFieldName.equals("PolNo") ) {
			return 3;
		}
		if( strFieldName.equals("EdorNo") ) {
			return 4;
		}
		if( strFieldName.equals("ReContCode") ) {
			return 5;
		}
		if( strFieldName.equals("ReinsureItem") ) {
			return 6;
		}
		if( strFieldName.equals("ReComCode") ) {
			return 7;
		}
		if( strFieldName.equals("RiskCalSort") ) {
			return 8;
		}
		if( strFieldName.equals("MainPolNo") ) {
			return 9;
		}
		if( strFieldName.equals("MasterPolNo") ) {
			return 10;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 11;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return 12;
		}
		if( strFieldName.equals("EdorValidate") ) {
			return 13;
		}
		if( strFieldName.equals("NoPassDays") ) {
			return 14;
		}
		if( strFieldName.equals("FeeoperationType") ) {
			return 15;
		}
		if( strFieldName.equals("GetMoney") ) {
			return 16;
		}
		if( strFieldName.equals("GetConfDate") ) {
			return 17;
		}
		if( strFieldName.equals("SumBackAmnt") ) {
			return 18;
		}
		if( strFieldName.equals("GetDataDate") ) {
			return 19;
		}
		if( strFieldName.equals("Operator") ) {
			return 20;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 21;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 22;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 25;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return 26;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 27;
		}
		if( strFieldName.equals("ReType") ) {
			return 28;
		}
		if( strFieldName.equals("ActuGetState") ) {
			return 29;
		}
		if( strFieldName.equals("ActuGetNo") ) {
			return 30;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "GrpPolNo";
				break;
			case 2:
				strFieldName = "ContNo";
				break;
			case 3:
				strFieldName = "PolNo";
				break;
			case 4:
				strFieldName = "EdorNo";
				break;
			case 5:
				strFieldName = "ReContCode";
				break;
			case 6:
				strFieldName = "ReinsureItem";
				break;
			case 7:
				strFieldName = "ReComCode";
				break;
			case 8:
				strFieldName = "RiskCalSort";
				break;
			case 9:
				strFieldName = "MainPolNo";
				break;
			case 10:
				strFieldName = "MasterPolNo";
				break;
			case 11:
				strFieldName = "RiskCode";
				break;
			case 12:
				strFieldName = "RiskVersion";
				break;
			case 13:
				strFieldName = "EdorValidate";
				break;
			case 14:
				strFieldName = "NoPassDays";
				break;
			case 15:
				strFieldName = "FeeoperationType";
				break;
			case 16:
				strFieldName = "GetMoney";
				break;
			case 17:
				strFieldName = "GetConfDate";
				break;
			case 18:
				strFieldName = "SumBackAmnt";
				break;
			case 19:
				strFieldName = "GetDataDate";
				break;
			case 20:
				strFieldName = "Operator";
				break;
			case 21:
				strFieldName = "ManageCom";
				break;
			case 22:
				strFieldName = "MakeDate";
				break;
			case 23:
				strFieldName = "MakeTime";
				break;
			case 24:
				strFieldName = "ModifyDate";
				break;
			case 25:
				strFieldName = "ModifyTime";
				break;
			case 26:
				strFieldName = "ReNewCount";
				break;
			case 27:
				strFieldName = "CostCenter";
				break;
			case 28:
				strFieldName = "ReType";
				break;
			case 29:
				strFieldName = "ActuGetState";
				break;
			case 30:
				strFieldName = "ActuGetNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReinsureItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCalSort") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MainPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MasterPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskVersion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorValidate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("NoPassDays") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("FeeoperationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GetConfDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SumBackAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GetDataDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActuGetState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActuGetNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_INT;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_INT;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
