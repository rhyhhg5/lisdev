/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.BPOLCAppntBDB;

/*
 * <p>ClassName: BPOLCAppntBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-10-20
 */
public class BPOLCAppntBSchema implements Schema, Cloneable
{
	// @Field
	/** 处理次数 */
	private int DealCount;
	/** 外包批次 */
	private String BPOBatchNo;
	/** 投保人id */
	private String AppntID;
	/** 合同id */
	private String ContID;
	/** 客户号 */
	private String CustomerNo;
	/** 姓名 */
	private String Name;
	/** 性别 */
	private String Sex;
	/** 出生日期 */
	private String Birthday;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 国籍 */
	private String NativePlace;
	/** 户口所在地 */
	private String RgtAddress;
	/** 婚姻状况 */
	private String Marriage;
	/** 民族 */
	private String Nationality;
	/** 职业代码 */
	private String OccupationCode;
	/** 职业类别 */
	private String OccupationType;
	/** 职业(工种) */
	private String WorkType;
	/** 兼职 */
	private String PluralityType;
	/** 开户行 */
	private String BankCode;
	/** 银行帐号 */
	private String BankAccNo;
	/** 户名 */
	private String AccName;
	/** 是否吸烟 */
	private String SmokeFlag;
	/** 联系地址 */
	private String PostalAddress;
	/** 邮编 */
	private String ZipCode;
	/** 联系电话 */
	private String Phone;
	/** 传真 */
	private String Fax;
	/** 手机 */
	private String Mobile;
	/** 电子邮箱 */
	private String EMail;
	/** 家庭邮编 */
	private String HomeZipCode;
	/** 家庭传真 */
	private String HomeFax;
	/** 工作单位 */
	private String GrpName;
	/** 单位邮编 */
	private String CompanyZipCode;
	/** 单位传真 */
	private String CompanyFax;
	/** 单位编码 */
	private String GrpNo;
	/** 入司日期 */
	private String JoinCompanyDate;
	/** 学历 */
	private String Degree;
	/** 岗位职务 */
	private String Position;
	/** 年收入 */
	private String Salary;
	/** 特别约定 */
	private String Remark;
	/** 整单保费 */
	private String PremScope;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 印刷号 */
	private String PrtNo;
	/** 保单类型 */
	private String ContType;
	/** 保单类型标记 */
	private String PolType;
	/** 保单密码 */
	private String Password;
	/** 保单送达方式 */
	private String GetPolMode;
	/** 银行委托书号码 */
	private String ConsignNo;
	/** 语种标记 */
	private String Lang;
	/** 币别 */
	private String Currency;
	/** 被保人个数 */
	private String Peoples;
	/** 缴费凭证号 */
	private String TempFeeNo;
	/** 保费收据号 */
	private String ContPremFeeNo;
	/** 投保次数类型 */
	private String DegreeType;
	/** 投保单填写日期 */
	private String HandlerDate;
	/** 投保人签字 */
	private String HandlerPrint;
	/** 国际业务标志 */
	private String IntlFlag;
	/** 核保审核号 */
	private String UWConfirmNo;
	/** 支付者 */
	private String PayerType;
	/** 客户地址号码 */
	private String AddressNo;
	/** 结婚日期 */
	private String MarriageDate;
	/** 健康状况 */
	private String Health;
	/** 身高 */
	private String Stature;
	/** 体重 */
	private String Avoirdupois;
	/** 信用等级 */
	private String CreditGrade;
	/** 参加工作日期 */
	private String StartWorkDate;
	/** 身体指标 */
	private String BMI;
	/** 其它证件类型 */
	private String OthIDType;
	/** 其它证件号码 */
	private String OthIDNo;
	/** 英文名称 */
	private String EnglishName;
	/** 家庭电话 */
	private String HomePhone;
	/** 单位地址 */
	private String CompanyAddress;
	/** 单位电话 */
	private String CompanyPhone;
	/** 自动发送续期通知书标志 */
	private String DueFeeMsgFlag;
	/** 投保备注项 */
	private String ExiSpec;
	/** 证件有效期起期 */
	private Date IDStartDate;
	/** 证件有效期止期 */
	private Date IDEndDate;
	/** 国家 */
	private String NativeCity;
	/** 联系省份 */
	private String PostalProvince;
	/** 联系市 */
	private String PostalCity;
	/** 联系县 */
	private String PostalCounty;
	/** 联系乡镇 */
	private String PostalStreet;
	/** 联系村（社区） */
	private String PostalCommunity;

	public static final int FIELDNUM = 86;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public BPOLCAppntBSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "DealCount";
		pk[1] = "BPOBatchNo";
		pk[2] = "AppntID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		BPOLCAppntBSchema cloned = (BPOLCAppntBSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public int getDealCount()
	{
		return DealCount;
	}
	public void setDealCount(int aDealCount)
	{
		DealCount = aDealCount;
	}
	public void setDealCount(String aDealCount)
	{
		if (aDealCount != null && !aDealCount.equals(""))
		{
			Integer tInteger = new Integer(aDealCount);
			int i = tInteger.intValue();
			DealCount = i;
		}
	}

	public String getBPOBatchNo()
	{
		return BPOBatchNo;
	}
	public void setBPOBatchNo(String aBPOBatchNo)
	{
		BPOBatchNo = aBPOBatchNo;
	}
	public String getAppntID()
	{
		return AppntID;
	}
	public void setAppntID(String aAppntID)
	{
		AppntID = aAppntID;
	}
	public String getContID()
	{
		return ContID;
	}
	public void setContID(String aContID)
	{
		ContID = aContID;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getBirthday()
	{
		return Birthday;
	}
	public void setBirthday(String aBirthday)
	{
		Birthday = aBirthday;
	}
	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getNativePlace()
	{
		return NativePlace;
	}
	public void setNativePlace(String aNativePlace)
	{
		NativePlace = aNativePlace;
	}
	public String getRgtAddress()
	{
		return RgtAddress;
	}
	public void setRgtAddress(String aRgtAddress)
	{
		RgtAddress = aRgtAddress;
	}
	public String getMarriage()
	{
		return Marriage;
	}
	public void setMarriage(String aMarriage)
	{
		Marriage = aMarriage;
	}
	public String getNationality()
	{
		return Nationality;
	}
	public void setNationality(String aNationality)
	{
		Nationality = aNationality;
	}
	public String getOccupationCode()
	{
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
		OccupationCode = aOccupationCode;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}
	public String getWorkType()
	{
		return WorkType;
	}
	public void setWorkType(String aWorkType)
	{
		WorkType = aWorkType;
	}
	public String getPluralityType()
	{
		return PluralityType;
	}
	public void setPluralityType(String aPluralityType)
	{
		PluralityType = aPluralityType;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getSmokeFlag()
	{
		return SmokeFlag;
	}
	public void setSmokeFlag(String aSmokeFlag)
	{
		SmokeFlag = aSmokeFlag;
	}
	public String getPostalAddress()
	{
		return PostalAddress;
	}
	public void setPostalAddress(String aPostalAddress)
	{
		PostalAddress = aPostalAddress;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getFax()
	{
		return Fax;
	}
	public void setFax(String aFax)
	{
		Fax = aFax;
	}
	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getEMail()
	{
		return EMail;
	}
	public void setEMail(String aEMail)
	{
		EMail = aEMail;
	}
	public String getHomeZipCode()
	{
		return HomeZipCode;
	}
	public void setHomeZipCode(String aHomeZipCode)
	{
		HomeZipCode = aHomeZipCode;
	}
	public String getHomeFax()
	{
		return HomeFax;
	}
	public void setHomeFax(String aHomeFax)
	{
		HomeFax = aHomeFax;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getCompanyZipCode()
	{
		return CompanyZipCode;
	}
	public void setCompanyZipCode(String aCompanyZipCode)
	{
		CompanyZipCode = aCompanyZipCode;
	}
	public String getCompanyFax()
	{
		return CompanyFax;
	}
	public void setCompanyFax(String aCompanyFax)
	{
		CompanyFax = aCompanyFax;
	}
	public String getGrpNo()
	{
		return GrpNo;
	}
	public void setGrpNo(String aGrpNo)
	{
		GrpNo = aGrpNo;
	}
	public String getJoinCompanyDate()
	{
		return JoinCompanyDate;
	}
	public void setJoinCompanyDate(String aJoinCompanyDate)
	{
		JoinCompanyDate = aJoinCompanyDate;
	}
	public String getDegree()
	{
		return Degree;
	}
	public void setDegree(String aDegree)
	{
		Degree = aDegree;
	}
	public String getPosition()
	{
		return Position;
	}
	public void setPosition(String aPosition)
	{
		Position = aPosition;
	}
	public String getSalary()
	{
		return Salary;
	}
	public void setSalary(String aSalary)
	{
		Salary = aSalary;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getPremScope()
	{
		return PremScope;
	}
	public void setPremScope(String aPremScope)
	{
		PremScope = aPremScope;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getContType()
	{
		return ContType;
	}
	public void setContType(String aContType)
	{
		ContType = aContType;
	}
	public String getPolType()
	{
		return PolType;
	}
	public void setPolType(String aPolType)
	{
		PolType = aPolType;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String aPassword)
	{
		Password = aPassword;
	}
	public String getGetPolMode()
	{
		return GetPolMode;
	}
	public void setGetPolMode(String aGetPolMode)
	{
		GetPolMode = aGetPolMode;
	}
	public String getConsignNo()
	{
		return ConsignNo;
	}
	public void setConsignNo(String aConsignNo)
	{
		ConsignNo = aConsignNo;
	}
	public String getLang()
	{
		return Lang;
	}
	public void setLang(String aLang)
	{
		Lang = aLang;
	}
	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}
	public String getPeoples()
	{
		return Peoples;
	}
	public void setPeoples(String aPeoples)
	{
		Peoples = aPeoples;
	}
	public String getTempFeeNo()
	{
		return TempFeeNo;
	}
	public void setTempFeeNo(String aTempFeeNo)
	{
		TempFeeNo = aTempFeeNo;
	}
	public String getContPremFeeNo()
	{
		return ContPremFeeNo;
	}
	public void setContPremFeeNo(String aContPremFeeNo)
	{
		ContPremFeeNo = aContPremFeeNo;
	}
	public String getDegreeType()
	{
		return DegreeType;
	}
	public void setDegreeType(String aDegreeType)
	{
		DegreeType = aDegreeType;
	}
	public String getHandlerDate()
	{
		return HandlerDate;
	}
	public void setHandlerDate(String aHandlerDate)
	{
		HandlerDate = aHandlerDate;
	}
	public String getHandlerPrint()
	{
		return HandlerPrint;
	}
	public void setHandlerPrint(String aHandlerPrint)
	{
		HandlerPrint = aHandlerPrint;
	}
	public String getIntlFlag()
	{
		return IntlFlag;
	}
	public void setIntlFlag(String aIntlFlag)
	{
		IntlFlag = aIntlFlag;
	}
	public String getUWConfirmNo()
	{
		return UWConfirmNo;
	}
	public void setUWConfirmNo(String aUWConfirmNo)
	{
		UWConfirmNo = aUWConfirmNo;
	}
	public String getPayerType()
	{
		return PayerType;
	}
	public void setPayerType(String aPayerType)
	{
		PayerType = aPayerType;
	}
	public String getAddressNo()
	{
		return AddressNo;
	}
	public void setAddressNo(String aAddressNo)
	{
		AddressNo = aAddressNo;
	}
	public String getMarriageDate()
	{
		return MarriageDate;
	}
	public void setMarriageDate(String aMarriageDate)
	{
		MarriageDate = aMarriageDate;
	}
	public String getHealth()
	{
		return Health;
	}
	public void setHealth(String aHealth)
	{
		Health = aHealth;
	}
	public String getStature()
	{
		return Stature;
	}
	public void setStature(String aStature)
	{
		Stature = aStature;
	}
	public String getAvoirdupois()
	{
		return Avoirdupois;
	}
	public void setAvoirdupois(String aAvoirdupois)
	{
		Avoirdupois = aAvoirdupois;
	}
	public String getCreditGrade()
	{
		return CreditGrade;
	}
	public void setCreditGrade(String aCreditGrade)
	{
		CreditGrade = aCreditGrade;
	}
	public String getStartWorkDate()
	{
		return StartWorkDate;
	}
	public void setStartWorkDate(String aStartWorkDate)
	{
		StartWorkDate = aStartWorkDate;
	}
	public String getBMI()
	{
		return BMI;
	}
	public void setBMI(String aBMI)
	{
		BMI = aBMI;
	}
	public String getOthIDType()
	{
		return OthIDType;
	}
	public void setOthIDType(String aOthIDType)
	{
		OthIDType = aOthIDType;
	}
	public String getOthIDNo()
	{
		return OthIDNo;
	}
	public void setOthIDNo(String aOthIDNo)
	{
		OthIDNo = aOthIDNo;
	}
	public String getEnglishName()
	{
		return EnglishName;
	}
	public void setEnglishName(String aEnglishName)
	{
		EnglishName = aEnglishName;
	}
	public String getHomePhone()
	{
		return HomePhone;
	}
	public void setHomePhone(String aHomePhone)
	{
		HomePhone = aHomePhone;
	}
	public String getCompanyAddress()
	{
		return CompanyAddress;
	}
	public void setCompanyAddress(String aCompanyAddress)
	{
		CompanyAddress = aCompanyAddress;
	}
	public String getCompanyPhone()
	{
		return CompanyPhone;
	}
	public void setCompanyPhone(String aCompanyPhone)
	{
		CompanyPhone = aCompanyPhone;
	}
	public String getDueFeeMsgFlag()
	{
		return DueFeeMsgFlag;
	}
	public void setDueFeeMsgFlag(String aDueFeeMsgFlag)
	{
		DueFeeMsgFlag = aDueFeeMsgFlag;
	}
	public String getExiSpec()
	{
		return ExiSpec;
	}
	public void setExiSpec(String aExiSpec)
	{
		ExiSpec = aExiSpec;
	}
	public String getIDStartDate()
	{
		if( IDStartDate != null )
			return fDate.getString(IDStartDate);
		else
			return null;
	}
	public void setIDStartDate(Date aIDStartDate)
	{
		IDStartDate = aIDStartDate;
	}
	public void setIDStartDate(String aIDStartDate)
	{
		if (aIDStartDate != null && !aIDStartDate.equals("") )
		{
			IDStartDate = fDate.getDate( aIDStartDate );
		}
		else
			IDStartDate = null;
	}

	public String getIDEndDate()
	{
		if( IDEndDate != null )
			return fDate.getString(IDEndDate);
		else
			return null;
	}
	public void setIDEndDate(Date aIDEndDate)
	{
		IDEndDate = aIDEndDate;
	}
	public void setIDEndDate(String aIDEndDate)
	{
		if (aIDEndDate != null && !aIDEndDate.equals("") )
		{
			IDEndDate = fDate.getDate( aIDEndDate );
		}
		else
			IDEndDate = null;
	}

	public String getNativeCity()
	{
		return NativeCity;
	}
	public void setNativeCity(String aNativeCity)
	{
		NativeCity = aNativeCity;
	}
	public String getPostalProvince()
	{
		return PostalProvince;
	}
	public void setPostalProvince(String aPostalProvince)
	{
		PostalProvince = aPostalProvince;
	}
	public String getPostalCity()
	{
		return PostalCity;
	}
	public void setPostalCity(String aPostalCity)
	{
		PostalCity = aPostalCity;
	}
	public String getPostalCounty()
	{
		return PostalCounty;
	}
	public void setPostalCounty(String aPostalCounty)
	{
		PostalCounty = aPostalCounty;
	}
	public String getPostalStreet()
	{
		return PostalStreet;
	}
	public void setPostalStreet(String aPostalStreet)
	{
		PostalStreet = aPostalStreet;
	}
	public String getPostalCommunity()
	{
		return PostalCommunity;
	}
	public void setPostalCommunity(String aPostalCommunity)
	{
		PostalCommunity = aPostalCommunity;
	}

	/**
	* 使用另外一个 BPOLCAppntBSchema 对象给 Schema 赋值
	* @param: aBPOLCAppntBSchema BPOLCAppntBSchema
	**/
	public void setSchema(BPOLCAppntBSchema aBPOLCAppntBSchema)
	{
		this.DealCount = aBPOLCAppntBSchema.getDealCount();
		this.BPOBatchNo = aBPOLCAppntBSchema.getBPOBatchNo();
		this.AppntID = aBPOLCAppntBSchema.getAppntID();
		this.ContID = aBPOLCAppntBSchema.getContID();
		this.CustomerNo = aBPOLCAppntBSchema.getCustomerNo();
		this.Name = aBPOLCAppntBSchema.getName();
		this.Sex = aBPOLCAppntBSchema.getSex();
		this.Birthday = aBPOLCAppntBSchema.getBirthday();
		this.IDType = aBPOLCAppntBSchema.getIDType();
		this.IDNo = aBPOLCAppntBSchema.getIDNo();
		this.NativePlace = aBPOLCAppntBSchema.getNativePlace();
		this.RgtAddress = aBPOLCAppntBSchema.getRgtAddress();
		this.Marriage = aBPOLCAppntBSchema.getMarriage();
		this.Nationality = aBPOLCAppntBSchema.getNationality();
		this.OccupationCode = aBPOLCAppntBSchema.getOccupationCode();
		this.OccupationType = aBPOLCAppntBSchema.getOccupationType();
		this.WorkType = aBPOLCAppntBSchema.getWorkType();
		this.PluralityType = aBPOLCAppntBSchema.getPluralityType();
		this.BankCode = aBPOLCAppntBSchema.getBankCode();
		this.BankAccNo = aBPOLCAppntBSchema.getBankAccNo();
		this.AccName = aBPOLCAppntBSchema.getAccName();
		this.SmokeFlag = aBPOLCAppntBSchema.getSmokeFlag();
		this.PostalAddress = aBPOLCAppntBSchema.getPostalAddress();
		this.ZipCode = aBPOLCAppntBSchema.getZipCode();
		this.Phone = aBPOLCAppntBSchema.getPhone();
		this.Fax = aBPOLCAppntBSchema.getFax();
		this.Mobile = aBPOLCAppntBSchema.getMobile();
		this.EMail = aBPOLCAppntBSchema.getEMail();
		this.HomeZipCode = aBPOLCAppntBSchema.getHomeZipCode();
		this.HomeFax = aBPOLCAppntBSchema.getHomeFax();
		this.GrpName = aBPOLCAppntBSchema.getGrpName();
		this.CompanyZipCode = aBPOLCAppntBSchema.getCompanyZipCode();
		this.CompanyFax = aBPOLCAppntBSchema.getCompanyFax();
		this.GrpNo = aBPOLCAppntBSchema.getGrpNo();
		this.JoinCompanyDate = aBPOLCAppntBSchema.getJoinCompanyDate();
		this.Degree = aBPOLCAppntBSchema.getDegree();
		this.Position = aBPOLCAppntBSchema.getPosition();
		this.Salary = aBPOLCAppntBSchema.getSalary();
		this.Remark = aBPOLCAppntBSchema.getRemark();
		this.PremScope = aBPOLCAppntBSchema.getPremScope();
		this.Operator = aBPOLCAppntBSchema.getOperator();
		this.MakeDate = fDate.getDate( aBPOLCAppntBSchema.getMakeDate());
		this.MakeTime = aBPOLCAppntBSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aBPOLCAppntBSchema.getModifyDate());
		this.ModifyTime = aBPOLCAppntBSchema.getModifyTime();
		this.PrtNo = aBPOLCAppntBSchema.getPrtNo();
		this.ContType = aBPOLCAppntBSchema.getContType();
		this.PolType = aBPOLCAppntBSchema.getPolType();
		this.Password = aBPOLCAppntBSchema.getPassword();
		this.GetPolMode = aBPOLCAppntBSchema.getGetPolMode();
		this.ConsignNo = aBPOLCAppntBSchema.getConsignNo();
		this.Lang = aBPOLCAppntBSchema.getLang();
		this.Currency = aBPOLCAppntBSchema.getCurrency();
		this.Peoples = aBPOLCAppntBSchema.getPeoples();
		this.TempFeeNo = aBPOLCAppntBSchema.getTempFeeNo();
		this.ContPremFeeNo = aBPOLCAppntBSchema.getContPremFeeNo();
		this.DegreeType = aBPOLCAppntBSchema.getDegreeType();
		this.HandlerDate = aBPOLCAppntBSchema.getHandlerDate();
		this.HandlerPrint = aBPOLCAppntBSchema.getHandlerPrint();
		this.IntlFlag = aBPOLCAppntBSchema.getIntlFlag();
		this.UWConfirmNo = aBPOLCAppntBSchema.getUWConfirmNo();
		this.PayerType = aBPOLCAppntBSchema.getPayerType();
		this.AddressNo = aBPOLCAppntBSchema.getAddressNo();
		this.MarriageDate = aBPOLCAppntBSchema.getMarriageDate();
		this.Health = aBPOLCAppntBSchema.getHealth();
		this.Stature = aBPOLCAppntBSchema.getStature();
		this.Avoirdupois = aBPOLCAppntBSchema.getAvoirdupois();
		this.CreditGrade = aBPOLCAppntBSchema.getCreditGrade();
		this.StartWorkDate = aBPOLCAppntBSchema.getStartWorkDate();
		this.BMI = aBPOLCAppntBSchema.getBMI();
		this.OthIDType = aBPOLCAppntBSchema.getOthIDType();
		this.OthIDNo = aBPOLCAppntBSchema.getOthIDNo();
		this.EnglishName = aBPOLCAppntBSchema.getEnglishName();
		this.HomePhone = aBPOLCAppntBSchema.getHomePhone();
		this.CompanyAddress = aBPOLCAppntBSchema.getCompanyAddress();
		this.CompanyPhone = aBPOLCAppntBSchema.getCompanyPhone();
		this.DueFeeMsgFlag = aBPOLCAppntBSchema.getDueFeeMsgFlag();
		this.ExiSpec = aBPOLCAppntBSchema.getExiSpec();
		this.IDStartDate = fDate.getDate( aBPOLCAppntBSchema.getIDStartDate());
		this.IDEndDate = fDate.getDate( aBPOLCAppntBSchema.getIDEndDate());
		this.NativeCity = aBPOLCAppntBSchema.getNativeCity();
		this.PostalProvince = aBPOLCAppntBSchema.getPostalProvince();
		this.PostalCity = aBPOLCAppntBSchema.getPostalCity();
		this.PostalCounty = aBPOLCAppntBSchema.getPostalCounty();
		this.PostalStreet = aBPOLCAppntBSchema.getPostalStreet();
		this.PostalCommunity = aBPOLCAppntBSchema.getPostalCommunity();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			this.DealCount = rs.getInt("DealCount");
			if( rs.getString("BPOBatchNo") == null )
				this.BPOBatchNo = null;
			else
				this.BPOBatchNo = rs.getString("BPOBatchNo").trim();

			if( rs.getString("AppntID") == null )
				this.AppntID = null;
			else
				this.AppntID = rs.getString("AppntID").trim();

			if( rs.getString("ContID") == null )
				this.ContID = null;
			else
				this.ContID = rs.getString("ContID").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			if( rs.getString("Birthday") == null )
				this.Birthday = null;
			else
				this.Birthday = rs.getString("Birthday").trim();

			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("NativePlace") == null )
				this.NativePlace = null;
			else
				this.NativePlace = rs.getString("NativePlace").trim();

			if( rs.getString("RgtAddress") == null )
				this.RgtAddress = null;
			else
				this.RgtAddress = rs.getString("RgtAddress").trim();

			if( rs.getString("Marriage") == null )
				this.Marriage = null;
			else
				this.Marriage = rs.getString("Marriage").trim();

			if( rs.getString("Nationality") == null )
				this.Nationality = null;
			else
				this.Nationality = rs.getString("Nationality").trim();

			if( rs.getString("OccupationCode") == null )
				this.OccupationCode = null;
			else
				this.OccupationCode = rs.getString("OccupationCode").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			if( rs.getString("WorkType") == null )
				this.WorkType = null;
			else
				this.WorkType = rs.getString("WorkType").trim();

			if( rs.getString("PluralityType") == null )
				this.PluralityType = null;
			else
				this.PluralityType = rs.getString("PluralityType").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("SmokeFlag") == null )
				this.SmokeFlag = null;
			else
				this.SmokeFlag = rs.getString("SmokeFlag").trim();

			if( rs.getString("PostalAddress") == null )
				this.PostalAddress = null;
			else
				this.PostalAddress = rs.getString("PostalAddress").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("Fax") == null )
				this.Fax = null;
			else
				this.Fax = rs.getString("Fax").trim();

			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("EMail") == null )
				this.EMail = null;
			else
				this.EMail = rs.getString("EMail").trim();

			if( rs.getString("HomeZipCode") == null )
				this.HomeZipCode = null;
			else
				this.HomeZipCode = rs.getString("HomeZipCode").trim();

			if( rs.getString("HomeFax") == null )
				this.HomeFax = null;
			else
				this.HomeFax = rs.getString("HomeFax").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("CompanyZipCode") == null )
				this.CompanyZipCode = null;
			else
				this.CompanyZipCode = rs.getString("CompanyZipCode").trim();

			if( rs.getString("CompanyFax") == null )
				this.CompanyFax = null;
			else
				this.CompanyFax = rs.getString("CompanyFax").trim();

			if( rs.getString("GrpNo") == null )
				this.GrpNo = null;
			else
				this.GrpNo = rs.getString("GrpNo").trim();

			if( rs.getString("JoinCompanyDate") == null )
				this.JoinCompanyDate = null;
			else
				this.JoinCompanyDate = rs.getString("JoinCompanyDate").trim();

			if( rs.getString("Degree") == null )
				this.Degree = null;
			else
				this.Degree = rs.getString("Degree").trim();

			if( rs.getString("Position") == null )
				this.Position = null;
			else
				this.Position = rs.getString("Position").trim();

			if( rs.getString("Salary") == null )
				this.Salary = null;
			else
				this.Salary = rs.getString("Salary").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("PremScope") == null )
				this.PremScope = null;
			else
				this.PremScope = rs.getString("PremScope").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("ContType") == null )
				this.ContType = null;
			else
				this.ContType = rs.getString("ContType").trim();

			if( rs.getString("PolType") == null )
				this.PolType = null;
			else
				this.PolType = rs.getString("PolType").trim();

			if( rs.getString("Password") == null )
				this.Password = null;
			else
				this.Password = rs.getString("Password").trim();

			if( rs.getString("GetPolMode") == null )
				this.GetPolMode = null;
			else
				this.GetPolMode = rs.getString("GetPolMode").trim();

			if( rs.getString("ConsignNo") == null )
				this.ConsignNo = null;
			else
				this.ConsignNo = rs.getString("ConsignNo").trim();

			if( rs.getString("Lang") == null )
				this.Lang = null;
			else
				this.Lang = rs.getString("Lang").trim();

			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

			if( rs.getString("Peoples") == null )
				this.Peoples = null;
			else
				this.Peoples = rs.getString("Peoples").trim();

			if( rs.getString("TempFeeNo") == null )
				this.TempFeeNo = null;
			else
				this.TempFeeNo = rs.getString("TempFeeNo").trim();

			if( rs.getString("ContPremFeeNo") == null )
				this.ContPremFeeNo = null;
			else
				this.ContPremFeeNo = rs.getString("ContPremFeeNo").trim();

			if( rs.getString("DegreeType") == null )
				this.DegreeType = null;
			else
				this.DegreeType = rs.getString("DegreeType").trim();

			if( rs.getString("HandlerDate") == null )
				this.HandlerDate = null;
			else
				this.HandlerDate = rs.getString("HandlerDate").trim();

			if( rs.getString("HandlerPrint") == null )
				this.HandlerPrint = null;
			else
				this.HandlerPrint = rs.getString("HandlerPrint").trim();

			if( rs.getString("IntlFlag") == null )
				this.IntlFlag = null;
			else
				this.IntlFlag = rs.getString("IntlFlag").trim();

			if( rs.getString("UWConfirmNo") == null )
				this.UWConfirmNo = null;
			else
				this.UWConfirmNo = rs.getString("UWConfirmNo").trim();

			if( rs.getString("PayerType") == null )
				this.PayerType = null;
			else
				this.PayerType = rs.getString("PayerType").trim();

			if( rs.getString("AddressNo") == null )
				this.AddressNo = null;
			else
				this.AddressNo = rs.getString("AddressNo").trim();

			if( rs.getString("MarriageDate") == null )
				this.MarriageDate = null;
			else
				this.MarriageDate = rs.getString("MarriageDate").trim();

			if( rs.getString("Health") == null )
				this.Health = null;
			else
				this.Health = rs.getString("Health").trim();

			if( rs.getString("Stature") == null )
				this.Stature = null;
			else
				this.Stature = rs.getString("Stature").trim();

			if( rs.getString("Avoirdupois") == null )
				this.Avoirdupois = null;
			else
				this.Avoirdupois = rs.getString("Avoirdupois").trim();

			if( rs.getString("CreditGrade") == null )
				this.CreditGrade = null;
			else
				this.CreditGrade = rs.getString("CreditGrade").trim();

			if( rs.getString("StartWorkDate") == null )
				this.StartWorkDate = null;
			else
				this.StartWorkDate = rs.getString("StartWorkDate").trim();

			if( rs.getString("BMI") == null )
				this.BMI = null;
			else
				this.BMI = rs.getString("BMI").trim();

			if( rs.getString("OthIDType") == null )
				this.OthIDType = null;
			else
				this.OthIDType = rs.getString("OthIDType").trim();

			if( rs.getString("OthIDNo") == null )
				this.OthIDNo = null;
			else
				this.OthIDNo = rs.getString("OthIDNo").trim();

			if( rs.getString("EnglishName") == null )
				this.EnglishName = null;
			else
				this.EnglishName = rs.getString("EnglishName").trim();

			if( rs.getString("HomePhone") == null )
				this.HomePhone = null;
			else
				this.HomePhone = rs.getString("HomePhone").trim();

			if( rs.getString("CompanyAddress") == null )
				this.CompanyAddress = null;
			else
				this.CompanyAddress = rs.getString("CompanyAddress").trim();

			if( rs.getString("CompanyPhone") == null )
				this.CompanyPhone = null;
			else
				this.CompanyPhone = rs.getString("CompanyPhone").trim();

			if( rs.getString("DueFeeMsgFlag") == null )
				this.DueFeeMsgFlag = null;
			else
				this.DueFeeMsgFlag = rs.getString("DueFeeMsgFlag").trim();

			if( rs.getString("ExiSpec") == null )
				this.ExiSpec = null;
			else
				this.ExiSpec = rs.getString("ExiSpec").trim();

			this.IDStartDate = rs.getDate("IDStartDate");
			this.IDEndDate = rs.getDate("IDEndDate");
			if( rs.getString("NativeCity") == null )
				this.NativeCity = null;
			else
				this.NativeCity = rs.getString("NativeCity").trim();

			if( rs.getString("PostalProvince") == null )
				this.PostalProvince = null;
			else
				this.PostalProvince = rs.getString("PostalProvince").trim();

			if( rs.getString("PostalCity") == null )
				this.PostalCity = null;
			else
				this.PostalCity = rs.getString("PostalCity").trim();

			if( rs.getString("PostalCounty") == null )
				this.PostalCounty = null;
			else
				this.PostalCounty = rs.getString("PostalCounty").trim();

			if( rs.getString("PostalStreet") == null )
				this.PostalStreet = null;
			else
				this.PostalStreet = rs.getString("PostalStreet").trim();

			if( rs.getString("PostalCommunity") == null )
				this.PostalCommunity = null;
			else
				this.PostalCommunity = rs.getString("PostalCommunity").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的BPOLCAppntB表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOLCAppntBSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public BPOLCAppntBSchema getSchema()
	{
		BPOLCAppntBSchema aBPOLCAppntBSchema = new BPOLCAppntBSchema();
		aBPOLCAppntBSchema.setSchema(this);
		return aBPOLCAppntBSchema;
	}

	public BPOLCAppntBDB getDB()
	{
		BPOLCAppntBDB aDBOper = new BPOLCAppntBDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOLCAppntB描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append( ChgData.chgData(DealCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BPOBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Birthday)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Marriage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Nationality)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WorkType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PluralityType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SmokeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Fax)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HomeZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HomeFax)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyFax)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(JoinCompanyDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Degree)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Salary)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremScope)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetPolMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ConsignNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Lang)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Peoples)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPremFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DegreeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerPrint)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IntlFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWConfirmNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayerType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AddressNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarriageDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Health)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Stature)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Avoirdupois)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CreditGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StartWorkDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BMI)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EnglishName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HomePhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DueFeeMsgFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExiSpec)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativeCity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalProvince)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCounty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalStreet)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCommunity));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOLCAppntB>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			DealCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).intValue();
			BPOBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AppntID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ContID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Birthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			RgtAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Marriage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			WorkType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			PluralityType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			SmokeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			HomeZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			HomeFax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			CompanyZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			CompanyFax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			JoinCompanyDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			Degree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			Salary = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			PremScope = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			GetPolMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			ConsignNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			Lang = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			Peoples = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			ContPremFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			DegreeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			HandlerDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			HandlerPrint = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			IntlFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			UWConfirmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			PayerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
			MarriageDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			Health = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			Stature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
			Avoirdupois = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
			CreditGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
			StartWorkDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
			BMI = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70, SysConst.PACKAGESPILTER );
			OthIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
			OthIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
			EnglishName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER );
			HomePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74, SysConst.PACKAGESPILTER );
			CompanyAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER );
			CompanyPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76, SysConst.PACKAGESPILTER );
			DueFeeMsgFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER );
			ExiSpec = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 78, SysConst.PACKAGESPILTER );
			IDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 79,SysConst.PACKAGESPILTER));
			IDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 80,SysConst.PACKAGESPILTER));
			NativeCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 81, SysConst.PACKAGESPILTER );
			PostalProvince = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 82, SysConst.PACKAGESPILTER );
			PostalCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 83, SysConst.PACKAGESPILTER );
			PostalCounty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 84, SysConst.PACKAGESPILTER );
			PostalStreet = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 85, SysConst.PACKAGESPILTER );
			PostalCommunity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 86, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOLCAppntBSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("DealCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealCount));
		}
		if (FCode.equals("BPOBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BPOBatchNo));
		}
		if (FCode.equals("AppntID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntID));
		}
		if (FCode.equals("ContID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContID));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Birthday));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("NativePlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
		}
		if (FCode.equals("RgtAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
		}
		if (FCode.equals("Marriage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Marriage));
		}
		if (FCode.equals("Nationality"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
		}
		if (FCode.equals("OccupationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("WorkType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WorkType));
		}
		if (FCode.equals("PluralityType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PluralityType));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("SmokeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SmokeFlag));
		}
		if (FCode.equals("PostalAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("Fax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("EMail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
		}
		if (FCode.equals("HomeZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HomeZipCode));
		}
		if (FCode.equals("HomeFax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HomeFax));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("CompanyZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyZipCode));
		}
		if (FCode.equals("CompanyFax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyFax));
		}
		if (FCode.equals("GrpNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNo));
		}
		if (FCode.equals("JoinCompanyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(JoinCompanyDate));
		}
		if (FCode.equals("Degree"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
		}
		if (FCode.equals("Position"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
		}
		if (FCode.equals("Salary"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Salary));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("PremScope"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremScope));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("ContType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
		}
		if (FCode.equals("PolType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
		}
		if (FCode.equals("Password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
		}
		if (FCode.equals("GetPolMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolMode));
		}
		if (FCode.equals("ConsignNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConsignNo));
		}
		if (FCode.equals("Lang"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Lang));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (FCode.equals("Peoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
		}
		if (FCode.equals("TempFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
		}
		if (FCode.equals("ContPremFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPremFeeNo));
		}
		if (FCode.equals("DegreeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DegreeType));
		}
		if (FCode.equals("HandlerDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerDate));
		}
		if (FCode.equals("HandlerPrint"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerPrint));
		}
		if (FCode.equals("IntlFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IntlFlag));
		}
		if (FCode.equals("UWConfirmNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWConfirmNo));
		}
		if (FCode.equals("PayerType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayerType));
		}
		if (FCode.equals("AddressNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
		}
		if (FCode.equals("MarriageDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarriageDate));
		}
		if (FCode.equals("Health"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Health));
		}
		if (FCode.equals("Stature"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Stature));
		}
		if (FCode.equals("Avoirdupois"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Avoirdupois));
		}
		if (FCode.equals("CreditGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CreditGrade));
		}
		if (FCode.equals("StartWorkDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StartWorkDate));
		}
		if (FCode.equals("BMI"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BMI));
		}
		if (FCode.equals("OthIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDType));
		}
		if (FCode.equals("OthIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDNo));
		}
		if (FCode.equals("EnglishName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EnglishName));
		}
		if (FCode.equals("HomePhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HomePhone));
		}
		if (FCode.equals("CompanyAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyAddress));
		}
		if (FCode.equals("CompanyPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyPhone));
		}
		if (FCode.equals("DueFeeMsgFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DueFeeMsgFlag));
		}
		if (FCode.equals("ExiSpec"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExiSpec));
		}
		if (FCode.equals("IDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
		}
		if (FCode.equals("IDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
		}
		if (FCode.equals("NativeCity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativeCity));
		}
		if (FCode.equals("PostalProvince"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalProvince));
		}
		if (FCode.equals("PostalCity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCity));
		}
		if (FCode.equals("PostalCounty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCounty));
		}
		if (FCode.equals("PostalStreet"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalStreet));
		}
		if (FCode.equals("PostalCommunity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCommunity));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = String.valueOf(DealCount);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BPOBatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AppntID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ContID);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Birthday);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(NativePlace);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(RgtAddress);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Marriage);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Nationality);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(OccupationCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(WorkType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(PluralityType);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(SmokeFlag);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(PostalAddress);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Fax);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(EMail);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(HomeZipCode);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(HomeFax);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(CompanyZipCode);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(CompanyFax);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(GrpNo);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(JoinCompanyDate);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(Degree);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(Position);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(Salary);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(PremScope);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(ContType);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(PolType);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(Password);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(GetPolMode);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(ConsignNo);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(Lang);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(Peoples);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(ContPremFeeNo);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(DegreeType);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(HandlerDate);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(HandlerPrint);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(IntlFlag);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(UWConfirmNo);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(PayerType);
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(AddressNo);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(MarriageDate);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(Health);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(Stature);
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(Avoirdupois);
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(CreditGrade);
				break;
			case 68:
				strFieldValue = StrTool.GBKToUnicode(StartWorkDate);
				break;
			case 69:
				strFieldValue = StrTool.GBKToUnicode(BMI);
				break;
			case 70:
				strFieldValue = StrTool.GBKToUnicode(OthIDType);
				break;
			case 71:
				strFieldValue = StrTool.GBKToUnicode(OthIDNo);
				break;
			case 72:
				strFieldValue = StrTool.GBKToUnicode(EnglishName);
				break;
			case 73:
				strFieldValue = StrTool.GBKToUnicode(HomePhone);
				break;
			case 74:
				strFieldValue = StrTool.GBKToUnicode(CompanyAddress);
				break;
			case 75:
				strFieldValue = StrTool.GBKToUnicode(CompanyPhone);
				break;
			case 76:
				strFieldValue = StrTool.GBKToUnicode(DueFeeMsgFlag);
				break;
			case 77:
				strFieldValue = StrTool.GBKToUnicode(ExiSpec);
				break;
			case 78:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
				break;
			case 79:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
				break;
			case 80:
				strFieldValue = StrTool.GBKToUnicode(NativeCity);
				break;
			case 81:
				strFieldValue = StrTool.GBKToUnicode(PostalProvince);
				break;
			case 82:
				strFieldValue = StrTool.GBKToUnicode(PostalCity);
				break;
			case 83:
				strFieldValue = StrTool.GBKToUnicode(PostalCounty);
				break;
			case 84:
				strFieldValue = StrTool.GBKToUnicode(PostalStreet);
				break;
			case 85:
				strFieldValue = StrTool.GBKToUnicode(PostalCommunity);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("DealCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DealCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("BPOBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BPOBatchNo = FValue.trim();
			}
			else
				BPOBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("AppntID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntID = FValue.trim();
			}
			else
				AppntID = null;
		}
		if (FCode.equalsIgnoreCase("ContID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContID = FValue.trim();
			}
			else
				ContID = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Birthday = FValue.trim();
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("NativePlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativePlace = FValue.trim();
			}
			else
				NativePlace = null;
		}
		if (FCode.equalsIgnoreCase("RgtAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtAddress = FValue.trim();
			}
			else
				RgtAddress = null;
		}
		if (FCode.equalsIgnoreCase("Marriage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Marriage = FValue.trim();
			}
			else
				Marriage = null;
		}
		if (FCode.equalsIgnoreCase("Nationality"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Nationality = FValue.trim();
			}
			else
				Nationality = null;
		}
		if (FCode.equalsIgnoreCase("OccupationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationCode = FValue.trim();
			}
			else
				OccupationCode = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("WorkType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WorkType = FValue.trim();
			}
			else
				WorkType = null;
		}
		if (FCode.equalsIgnoreCase("PluralityType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PluralityType = FValue.trim();
			}
			else
				PluralityType = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("SmokeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SmokeFlag = FValue.trim();
			}
			else
				SmokeFlag = null;
		}
		if (FCode.equalsIgnoreCase("PostalAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalAddress = FValue.trim();
			}
			else
				PostalAddress = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("Fax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Fax = FValue.trim();
			}
			else
				Fax = null;
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("EMail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EMail = FValue.trim();
			}
			else
				EMail = null;
		}
		if (FCode.equalsIgnoreCase("HomeZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HomeZipCode = FValue.trim();
			}
			else
				HomeZipCode = null;
		}
		if (FCode.equalsIgnoreCase("HomeFax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HomeFax = FValue.trim();
			}
			else
				HomeFax = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("CompanyZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyZipCode = FValue.trim();
			}
			else
				CompanyZipCode = null;
		}
		if (FCode.equalsIgnoreCase("CompanyFax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyFax = FValue.trim();
			}
			else
				CompanyFax = null;
		}
		if (FCode.equalsIgnoreCase("GrpNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNo = FValue.trim();
			}
			else
				GrpNo = null;
		}
		if (FCode.equalsIgnoreCase("JoinCompanyDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				JoinCompanyDate = FValue.trim();
			}
			else
				JoinCompanyDate = null;
		}
		if (FCode.equalsIgnoreCase("Degree"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Degree = FValue.trim();
			}
			else
				Degree = null;
		}
		if (FCode.equalsIgnoreCase("Position"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Position = FValue.trim();
			}
			else
				Position = null;
		}
		if (FCode.equalsIgnoreCase("Salary"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Salary = FValue.trim();
			}
			else
				Salary = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("PremScope"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremScope = FValue.trim();
			}
			else
				PremScope = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("ContType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContType = FValue.trim();
			}
			else
				ContType = null;
		}
		if (FCode.equalsIgnoreCase("PolType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolType = FValue.trim();
			}
			else
				PolType = null;
		}
		if (FCode.equalsIgnoreCase("Password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Password = FValue.trim();
			}
			else
				Password = null;
		}
		if (FCode.equalsIgnoreCase("GetPolMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetPolMode = FValue.trim();
			}
			else
				GetPolMode = null;
		}
		if (FCode.equalsIgnoreCase("ConsignNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ConsignNo = FValue.trim();
			}
			else
				ConsignNo = null;
		}
		if (FCode.equalsIgnoreCase("Lang"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Lang = FValue.trim();
			}
			else
				Lang = null;
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		if (FCode.equalsIgnoreCase("Peoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Peoples = FValue.trim();
			}
			else
				Peoples = null;
		}
		if (FCode.equalsIgnoreCase("TempFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempFeeNo = FValue.trim();
			}
			else
				TempFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("ContPremFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPremFeeNo = FValue.trim();
			}
			else
				ContPremFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("DegreeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DegreeType = FValue.trim();
			}
			else
				DegreeType = null;
		}
		if (FCode.equalsIgnoreCase("HandlerDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerDate = FValue.trim();
			}
			else
				HandlerDate = null;
		}
		if (FCode.equalsIgnoreCase("HandlerPrint"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerPrint = FValue.trim();
			}
			else
				HandlerPrint = null;
		}
		if (FCode.equalsIgnoreCase("IntlFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IntlFlag = FValue.trim();
			}
			else
				IntlFlag = null;
		}
		if (FCode.equalsIgnoreCase("UWConfirmNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWConfirmNo = FValue.trim();
			}
			else
				UWConfirmNo = null;
		}
		if (FCode.equalsIgnoreCase("PayerType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayerType = FValue.trim();
			}
			else
				PayerType = null;
		}
		if (FCode.equalsIgnoreCase("AddressNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AddressNo = FValue.trim();
			}
			else
				AddressNo = null;
		}
		if (FCode.equalsIgnoreCase("MarriageDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarriageDate = FValue.trim();
			}
			else
				MarriageDate = null;
		}
		if (FCode.equalsIgnoreCase("Health"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Health = FValue.trim();
			}
			else
				Health = null;
		}
		if (FCode.equalsIgnoreCase("Stature"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Stature = FValue.trim();
			}
			else
				Stature = null;
		}
		if (FCode.equalsIgnoreCase("Avoirdupois"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Avoirdupois = FValue.trim();
			}
			else
				Avoirdupois = null;
		}
		if (FCode.equalsIgnoreCase("CreditGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CreditGrade = FValue.trim();
			}
			else
				CreditGrade = null;
		}
		if (FCode.equalsIgnoreCase("StartWorkDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StartWorkDate = FValue.trim();
			}
			else
				StartWorkDate = null;
		}
		if (FCode.equalsIgnoreCase("BMI"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BMI = FValue.trim();
			}
			else
				BMI = null;
		}
		if (FCode.equalsIgnoreCase("OthIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDType = FValue.trim();
			}
			else
				OthIDType = null;
		}
		if (FCode.equalsIgnoreCase("OthIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDNo = FValue.trim();
			}
			else
				OthIDNo = null;
		}
		if (FCode.equalsIgnoreCase("EnglishName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EnglishName = FValue.trim();
			}
			else
				EnglishName = null;
		}
		if (FCode.equalsIgnoreCase("HomePhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HomePhone = FValue.trim();
			}
			else
				HomePhone = null;
		}
		if (FCode.equalsIgnoreCase("CompanyAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyAddress = FValue.trim();
			}
			else
				CompanyAddress = null;
		}
		if (FCode.equalsIgnoreCase("CompanyPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyPhone = FValue.trim();
			}
			else
				CompanyPhone = null;
		}
		if (FCode.equalsIgnoreCase("DueFeeMsgFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DueFeeMsgFlag = FValue.trim();
			}
			else
				DueFeeMsgFlag = null;
		}
		if (FCode.equalsIgnoreCase("ExiSpec"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExiSpec = FValue.trim();
			}
			else
				ExiSpec = null;
		}
		if (FCode.equalsIgnoreCase("IDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDStartDate = fDate.getDate( FValue );
			}
			else
				IDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("IDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDEndDate = fDate.getDate( FValue );
			}
			else
				IDEndDate = null;
		}
		if (FCode.equalsIgnoreCase("NativeCity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativeCity = FValue.trim();
			}
			else
				NativeCity = null;
		}
		if (FCode.equalsIgnoreCase("PostalProvince"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalProvince = FValue.trim();
			}
			else
				PostalProvince = null;
		}
		if (FCode.equalsIgnoreCase("PostalCity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCity = FValue.trim();
			}
			else
				PostalCity = null;
		}
		if (FCode.equalsIgnoreCase("PostalCounty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCounty = FValue.trim();
			}
			else
				PostalCounty = null;
		}
		if (FCode.equalsIgnoreCase("PostalStreet"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalStreet = FValue.trim();
			}
			else
				PostalStreet = null;
		}
		if (FCode.equalsIgnoreCase("PostalCommunity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCommunity = FValue.trim();
			}
			else
				PostalCommunity = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		BPOLCAppntBSchema other = (BPOLCAppntBSchema)otherObject;
		return
			DealCount == other.getDealCount()
			&& (BPOBatchNo == null ? other.getBPOBatchNo() == null : BPOBatchNo.equals(other.getBPOBatchNo()))
			&& (AppntID == null ? other.getAppntID() == null : AppntID.equals(other.getAppntID()))
			&& (ContID == null ? other.getContID() == null : ContID.equals(other.getContID()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (Birthday == null ? other.getBirthday() == null : Birthday.equals(other.getBirthday()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (NativePlace == null ? other.getNativePlace() == null : NativePlace.equals(other.getNativePlace()))
			&& (RgtAddress == null ? other.getRgtAddress() == null : RgtAddress.equals(other.getRgtAddress()))
			&& (Marriage == null ? other.getMarriage() == null : Marriage.equals(other.getMarriage()))
			&& (Nationality == null ? other.getNationality() == null : Nationality.equals(other.getNationality()))
			&& (OccupationCode == null ? other.getOccupationCode() == null : OccupationCode.equals(other.getOccupationCode()))
			&& (OccupationType == null ? other.getOccupationType() == null : OccupationType.equals(other.getOccupationType()))
			&& (WorkType == null ? other.getWorkType() == null : WorkType.equals(other.getWorkType()))
			&& (PluralityType == null ? other.getPluralityType() == null : PluralityType.equals(other.getPluralityType()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (SmokeFlag == null ? other.getSmokeFlag() == null : SmokeFlag.equals(other.getSmokeFlag()))
			&& (PostalAddress == null ? other.getPostalAddress() == null : PostalAddress.equals(other.getPostalAddress()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (Fax == null ? other.getFax() == null : Fax.equals(other.getFax()))
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (EMail == null ? other.getEMail() == null : EMail.equals(other.getEMail()))
			&& (HomeZipCode == null ? other.getHomeZipCode() == null : HomeZipCode.equals(other.getHomeZipCode()))
			&& (HomeFax == null ? other.getHomeFax() == null : HomeFax.equals(other.getHomeFax()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (CompanyZipCode == null ? other.getCompanyZipCode() == null : CompanyZipCode.equals(other.getCompanyZipCode()))
			&& (CompanyFax == null ? other.getCompanyFax() == null : CompanyFax.equals(other.getCompanyFax()))
			&& (GrpNo == null ? other.getGrpNo() == null : GrpNo.equals(other.getGrpNo()))
			&& (JoinCompanyDate == null ? other.getJoinCompanyDate() == null : JoinCompanyDate.equals(other.getJoinCompanyDate()))
			&& (Degree == null ? other.getDegree() == null : Degree.equals(other.getDegree()))
			&& (Position == null ? other.getPosition() == null : Position.equals(other.getPosition()))
			&& (Salary == null ? other.getSalary() == null : Salary.equals(other.getSalary()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (PremScope == null ? other.getPremScope() == null : PremScope.equals(other.getPremScope()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (ContType == null ? other.getContType() == null : ContType.equals(other.getContType()))
			&& (PolType == null ? other.getPolType() == null : PolType.equals(other.getPolType()))
			&& (Password == null ? other.getPassword() == null : Password.equals(other.getPassword()))
			&& (GetPolMode == null ? other.getGetPolMode() == null : GetPolMode.equals(other.getGetPolMode()))
			&& (ConsignNo == null ? other.getConsignNo() == null : ConsignNo.equals(other.getConsignNo()))
			&& (Lang == null ? other.getLang() == null : Lang.equals(other.getLang()))
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()))
			&& (Peoples == null ? other.getPeoples() == null : Peoples.equals(other.getPeoples()))
			&& (TempFeeNo == null ? other.getTempFeeNo() == null : TempFeeNo.equals(other.getTempFeeNo()))
			&& (ContPremFeeNo == null ? other.getContPremFeeNo() == null : ContPremFeeNo.equals(other.getContPremFeeNo()))
			&& (DegreeType == null ? other.getDegreeType() == null : DegreeType.equals(other.getDegreeType()))
			&& (HandlerDate == null ? other.getHandlerDate() == null : HandlerDate.equals(other.getHandlerDate()))
			&& (HandlerPrint == null ? other.getHandlerPrint() == null : HandlerPrint.equals(other.getHandlerPrint()))
			&& (IntlFlag == null ? other.getIntlFlag() == null : IntlFlag.equals(other.getIntlFlag()))
			&& (UWConfirmNo == null ? other.getUWConfirmNo() == null : UWConfirmNo.equals(other.getUWConfirmNo()))
			&& (PayerType == null ? other.getPayerType() == null : PayerType.equals(other.getPayerType()))
			&& (AddressNo == null ? other.getAddressNo() == null : AddressNo.equals(other.getAddressNo()))
			&& (MarriageDate == null ? other.getMarriageDate() == null : MarriageDate.equals(other.getMarriageDate()))
			&& (Health == null ? other.getHealth() == null : Health.equals(other.getHealth()))
			&& (Stature == null ? other.getStature() == null : Stature.equals(other.getStature()))
			&& (Avoirdupois == null ? other.getAvoirdupois() == null : Avoirdupois.equals(other.getAvoirdupois()))
			&& (CreditGrade == null ? other.getCreditGrade() == null : CreditGrade.equals(other.getCreditGrade()))
			&& (StartWorkDate == null ? other.getStartWorkDate() == null : StartWorkDate.equals(other.getStartWorkDate()))
			&& (BMI == null ? other.getBMI() == null : BMI.equals(other.getBMI()))
			&& (OthIDType == null ? other.getOthIDType() == null : OthIDType.equals(other.getOthIDType()))
			&& (OthIDNo == null ? other.getOthIDNo() == null : OthIDNo.equals(other.getOthIDNo()))
			&& (EnglishName == null ? other.getEnglishName() == null : EnglishName.equals(other.getEnglishName()))
			&& (HomePhone == null ? other.getHomePhone() == null : HomePhone.equals(other.getHomePhone()))
			&& (CompanyAddress == null ? other.getCompanyAddress() == null : CompanyAddress.equals(other.getCompanyAddress()))
			&& (CompanyPhone == null ? other.getCompanyPhone() == null : CompanyPhone.equals(other.getCompanyPhone()))
			&& (DueFeeMsgFlag == null ? other.getDueFeeMsgFlag() == null : DueFeeMsgFlag.equals(other.getDueFeeMsgFlag()))
			&& (ExiSpec == null ? other.getExiSpec() == null : ExiSpec.equals(other.getExiSpec()))
			&& (IDStartDate == null ? other.getIDStartDate() == null : fDate.getString(IDStartDate).equals(other.getIDStartDate()))
			&& (IDEndDate == null ? other.getIDEndDate() == null : fDate.getString(IDEndDate).equals(other.getIDEndDate()))
			&& (NativeCity == null ? other.getNativeCity() == null : NativeCity.equals(other.getNativeCity()))
			&& (PostalProvince == null ? other.getPostalProvince() == null : PostalProvince.equals(other.getPostalProvince()))
			&& (PostalCity == null ? other.getPostalCity() == null : PostalCity.equals(other.getPostalCity()))
			&& (PostalCounty == null ? other.getPostalCounty() == null : PostalCounty.equals(other.getPostalCounty()))
			&& (PostalStreet == null ? other.getPostalStreet() == null : PostalStreet.equals(other.getPostalStreet()))
			&& (PostalCommunity == null ? other.getPostalCommunity() == null : PostalCommunity.equals(other.getPostalCommunity()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("DealCount") ) {
			return 0;
		}
		if( strFieldName.equals("BPOBatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("AppntID") ) {
			return 2;
		}
		if( strFieldName.equals("ContID") ) {
			return 3;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 4;
		}
		if( strFieldName.equals("Name") ) {
			return 5;
		}
		if( strFieldName.equals("Sex") ) {
			return 6;
		}
		if( strFieldName.equals("Birthday") ) {
			return 7;
		}
		if( strFieldName.equals("IDType") ) {
			return 8;
		}
		if( strFieldName.equals("IDNo") ) {
			return 9;
		}
		if( strFieldName.equals("NativePlace") ) {
			return 10;
		}
		if( strFieldName.equals("RgtAddress") ) {
			return 11;
		}
		if( strFieldName.equals("Marriage") ) {
			return 12;
		}
		if( strFieldName.equals("Nationality") ) {
			return 13;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return 14;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 15;
		}
		if( strFieldName.equals("WorkType") ) {
			return 16;
		}
		if( strFieldName.equals("PluralityType") ) {
			return 17;
		}
		if( strFieldName.equals("BankCode") ) {
			return 18;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 19;
		}
		if( strFieldName.equals("AccName") ) {
			return 20;
		}
		if( strFieldName.equals("SmokeFlag") ) {
			return 21;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return 22;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 23;
		}
		if( strFieldName.equals("Phone") ) {
			return 24;
		}
		if( strFieldName.equals("Fax") ) {
			return 25;
		}
		if( strFieldName.equals("Mobile") ) {
			return 26;
		}
		if( strFieldName.equals("EMail") ) {
			return 27;
		}
		if( strFieldName.equals("HomeZipCode") ) {
			return 28;
		}
		if( strFieldName.equals("HomeFax") ) {
			return 29;
		}
		if( strFieldName.equals("GrpName") ) {
			return 30;
		}
		if( strFieldName.equals("CompanyZipCode") ) {
			return 31;
		}
		if( strFieldName.equals("CompanyFax") ) {
			return 32;
		}
		if( strFieldName.equals("GrpNo") ) {
			return 33;
		}
		if( strFieldName.equals("JoinCompanyDate") ) {
			return 34;
		}
		if( strFieldName.equals("Degree") ) {
			return 35;
		}
		if( strFieldName.equals("Position") ) {
			return 36;
		}
		if( strFieldName.equals("Salary") ) {
			return 37;
		}
		if( strFieldName.equals("Remark") ) {
			return 38;
		}
		if( strFieldName.equals("PremScope") ) {
			return 39;
		}
		if( strFieldName.equals("Operator") ) {
			return 40;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 41;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 42;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 43;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 44;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 45;
		}
		if( strFieldName.equals("ContType") ) {
			return 46;
		}
		if( strFieldName.equals("PolType") ) {
			return 47;
		}
		if( strFieldName.equals("Password") ) {
			return 48;
		}
		if( strFieldName.equals("GetPolMode") ) {
			return 49;
		}
		if( strFieldName.equals("ConsignNo") ) {
			return 50;
		}
		if( strFieldName.equals("Lang") ) {
			return 51;
		}
		if( strFieldName.equals("Currency") ) {
			return 52;
		}
		if( strFieldName.equals("Peoples") ) {
			return 53;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return 54;
		}
		if( strFieldName.equals("ContPremFeeNo") ) {
			return 55;
		}
		if( strFieldName.equals("DegreeType") ) {
			return 56;
		}
		if( strFieldName.equals("HandlerDate") ) {
			return 57;
		}
		if( strFieldName.equals("HandlerPrint") ) {
			return 58;
		}
		if( strFieldName.equals("IntlFlag") ) {
			return 59;
		}
		if( strFieldName.equals("UWConfirmNo") ) {
			return 60;
		}
		if( strFieldName.equals("PayerType") ) {
			return 61;
		}
		if( strFieldName.equals("AddressNo") ) {
			return 62;
		}
		if( strFieldName.equals("MarriageDate") ) {
			return 63;
		}
		if( strFieldName.equals("Health") ) {
			return 64;
		}
		if( strFieldName.equals("Stature") ) {
			return 65;
		}
		if( strFieldName.equals("Avoirdupois") ) {
			return 66;
		}
		if( strFieldName.equals("CreditGrade") ) {
			return 67;
		}
		if( strFieldName.equals("StartWorkDate") ) {
			return 68;
		}
		if( strFieldName.equals("BMI") ) {
			return 69;
		}
		if( strFieldName.equals("OthIDType") ) {
			return 70;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return 71;
		}
		if( strFieldName.equals("EnglishName") ) {
			return 72;
		}
		if( strFieldName.equals("HomePhone") ) {
			return 73;
		}
		if( strFieldName.equals("CompanyAddress") ) {
			return 74;
		}
		if( strFieldName.equals("CompanyPhone") ) {
			return 75;
		}
		if( strFieldName.equals("DueFeeMsgFlag") ) {
			return 76;
		}
		if( strFieldName.equals("ExiSpec") ) {
			return 77;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return 78;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return 79;
		}
		if( strFieldName.equals("NativeCity") ) {
			return 80;
		}
		if( strFieldName.equals("PostalProvince") ) {
			return 81;
		}
		if( strFieldName.equals("PostalCity") ) {
			return 82;
		}
		if( strFieldName.equals("PostalCounty") ) {
			return 83;
		}
		if( strFieldName.equals("PostalStreet") ) {
			return 84;
		}
		if( strFieldName.equals("PostalCommunity") ) {
			return 85;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "DealCount";
				break;
			case 1:
				strFieldName = "BPOBatchNo";
				break;
			case 2:
				strFieldName = "AppntID";
				break;
			case 3:
				strFieldName = "ContID";
				break;
			case 4:
				strFieldName = "CustomerNo";
				break;
			case 5:
				strFieldName = "Name";
				break;
			case 6:
				strFieldName = "Sex";
				break;
			case 7:
				strFieldName = "Birthday";
				break;
			case 8:
				strFieldName = "IDType";
				break;
			case 9:
				strFieldName = "IDNo";
				break;
			case 10:
				strFieldName = "NativePlace";
				break;
			case 11:
				strFieldName = "RgtAddress";
				break;
			case 12:
				strFieldName = "Marriage";
				break;
			case 13:
				strFieldName = "Nationality";
				break;
			case 14:
				strFieldName = "OccupationCode";
				break;
			case 15:
				strFieldName = "OccupationType";
				break;
			case 16:
				strFieldName = "WorkType";
				break;
			case 17:
				strFieldName = "PluralityType";
				break;
			case 18:
				strFieldName = "BankCode";
				break;
			case 19:
				strFieldName = "BankAccNo";
				break;
			case 20:
				strFieldName = "AccName";
				break;
			case 21:
				strFieldName = "SmokeFlag";
				break;
			case 22:
				strFieldName = "PostalAddress";
				break;
			case 23:
				strFieldName = "ZipCode";
				break;
			case 24:
				strFieldName = "Phone";
				break;
			case 25:
				strFieldName = "Fax";
				break;
			case 26:
				strFieldName = "Mobile";
				break;
			case 27:
				strFieldName = "EMail";
				break;
			case 28:
				strFieldName = "HomeZipCode";
				break;
			case 29:
				strFieldName = "HomeFax";
				break;
			case 30:
				strFieldName = "GrpName";
				break;
			case 31:
				strFieldName = "CompanyZipCode";
				break;
			case 32:
				strFieldName = "CompanyFax";
				break;
			case 33:
				strFieldName = "GrpNo";
				break;
			case 34:
				strFieldName = "JoinCompanyDate";
				break;
			case 35:
				strFieldName = "Degree";
				break;
			case 36:
				strFieldName = "Position";
				break;
			case 37:
				strFieldName = "Salary";
				break;
			case 38:
				strFieldName = "Remark";
				break;
			case 39:
				strFieldName = "PremScope";
				break;
			case 40:
				strFieldName = "Operator";
				break;
			case 41:
				strFieldName = "MakeDate";
				break;
			case 42:
				strFieldName = "MakeTime";
				break;
			case 43:
				strFieldName = "ModifyDate";
				break;
			case 44:
				strFieldName = "ModifyTime";
				break;
			case 45:
				strFieldName = "PrtNo";
				break;
			case 46:
				strFieldName = "ContType";
				break;
			case 47:
				strFieldName = "PolType";
				break;
			case 48:
				strFieldName = "Password";
				break;
			case 49:
				strFieldName = "GetPolMode";
				break;
			case 50:
				strFieldName = "ConsignNo";
				break;
			case 51:
				strFieldName = "Lang";
				break;
			case 52:
				strFieldName = "Currency";
				break;
			case 53:
				strFieldName = "Peoples";
				break;
			case 54:
				strFieldName = "TempFeeNo";
				break;
			case 55:
				strFieldName = "ContPremFeeNo";
				break;
			case 56:
				strFieldName = "DegreeType";
				break;
			case 57:
				strFieldName = "HandlerDate";
				break;
			case 58:
				strFieldName = "HandlerPrint";
				break;
			case 59:
				strFieldName = "IntlFlag";
				break;
			case 60:
				strFieldName = "UWConfirmNo";
				break;
			case 61:
				strFieldName = "PayerType";
				break;
			case 62:
				strFieldName = "AddressNo";
				break;
			case 63:
				strFieldName = "MarriageDate";
				break;
			case 64:
				strFieldName = "Health";
				break;
			case 65:
				strFieldName = "Stature";
				break;
			case 66:
				strFieldName = "Avoirdupois";
				break;
			case 67:
				strFieldName = "CreditGrade";
				break;
			case 68:
				strFieldName = "StartWorkDate";
				break;
			case 69:
				strFieldName = "BMI";
				break;
			case 70:
				strFieldName = "OthIDType";
				break;
			case 71:
				strFieldName = "OthIDNo";
				break;
			case 72:
				strFieldName = "EnglishName";
				break;
			case 73:
				strFieldName = "HomePhone";
				break;
			case 74:
				strFieldName = "CompanyAddress";
				break;
			case 75:
				strFieldName = "CompanyPhone";
				break;
			case 76:
				strFieldName = "DueFeeMsgFlag";
				break;
			case 77:
				strFieldName = "ExiSpec";
				break;
			case 78:
				strFieldName = "IDStartDate";
				break;
			case 79:
				strFieldName = "IDEndDate";
				break;
			case 80:
				strFieldName = "NativeCity";
				break;
			case 81:
				strFieldName = "PostalProvince";
				break;
			case 82:
				strFieldName = "PostalCity";
				break;
			case 83:
				strFieldName = "PostalCounty";
				break;
			case 84:
				strFieldName = "PostalStreet";
				break;
			case 85:
				strFieldName = "PostalCommunity";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("DealCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BPOBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NativePlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Marriage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Nationality") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WorkType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PluralityType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SmokeFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Fax") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EMail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HomeZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HomeFax") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyFax") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JoinCompanyDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Degree") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Position") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Salary") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremScope") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetPolMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ConsignNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Lang") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Peoples") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPremFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DegreeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerPrint") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IntlFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWConfirmNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayerType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AddressNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarriageDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Health") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Stature") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Avoirdupois") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CreditGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartWorkDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BMI") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnglishName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HomePhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DueFeeMsgFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExiSpec") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("NativeCity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalProvince") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCounty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalStreet") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCommunity") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_INT;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 66:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 67:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 68:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 69:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 70:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 71:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 72:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 73:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 74:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 75:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 76:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 77:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 78:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 79:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 80:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 81:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 82:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 83:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 84:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 85:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
