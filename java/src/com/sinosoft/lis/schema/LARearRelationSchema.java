/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LARearRelationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-16
 */
public class LARearRelationSchema implements Schema
{
    // @Field
    /** 育成级别 */
    private String RearLevel;
    /** 育成代数目 */
    private int RearedGens;
    /** 被育成人 */
    private String AgentCode;
    /** 育成人 */
    private String RearAgentCode;
    /** 被育成机构 */
    private String AgentGroup;
    /** 育成起期 */
    private Date startDate;
    /** 育成止期 */
    private Date EndDate;
    /** 育成关系存在标记 */
    private String RearFlag;
    /** 育成津贴抽取标记 */
    private String RearCommFlag;
    /** 育成津贴抽取起始年度 */
    private int RearStartYear;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最近修改日期 */
    private Date ModifyDate;
    /** 最近修改时间 */
    private String ModifyTime;
    /** 操作员代码 */
    private String Operator;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LARearRelationSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "RearLevel";
        pk[1] = "RearedGens";
        pk[2] = "AgentCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRearLevel()
    {
        if (RearLevel != null && !RearLevel.equals("") &&
            SysConst.CHANGECHARSET)
        {
            RearLevel = StrTool.unicodeToGBK(RearLevel);
        }
        return RearLevel;
    }

    public void setRearLevel(String aRearLevel)
    {
        RearLevel = aRearLevel;
    }

    public int getRearedGens()
    {
        return RearedGens;
    }

    public void setRearedGens(int aRearedGens)
    {
        RearedGens = aRearedGens;
    }

    public void setRearedGens(String aRearedGens)
    {
        if (aRearedGens != null && !aRearedGens.equals(""))
        {
            Integer tInteger = new Integer(aRearedGens);
            int i = tInteger.intValue();
            RearedGens = i;
        }
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getRearAgentCode()
    {
        if (RearAgentCode != null && !RearAgentCode.equals("") &&
            SysConst.CHANGECHARSET)
        {
            RearAgentCode = StrTool.unicodeToGBK(RearAgentCode);
        }
        return RearAgentCode;
    }

    public void setRearAgentCode(String aRearAgentCode)
    {
        RearAgentCode = aRearAgentCode;
    }

    public String getAgentGroup()
    {
        if (AgentGroup != null && !AgentGroup.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getstartDate()
    {
        if (startDate != null)
        {
            return fDate.getString(startDate);
        }
        else
        {
            return null;
        }
    }

    public void setstartDate(Date astartDate)
    {
        startDate = astartDate;
    }

    public void setstartDate(String astartDate)
    {
        if (astartDate != null && !astartDate.equals(""))
        {
            startDate = fDate.getDate(astartDate);
        }
        else
        {
            startDate = null;
        }
    }

    public String getEndDate()
    {
        if (EndDate != null)
        {
            return fDate.getString(EndDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndDate(Date aEndDate)
    {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate)
    {
        if (aEndDate != null && !aEndDate.equals(""))
        {
            EndDate = fDate.getDate(aEndDate);
        }
        else
        {
            EndDate = null;
        }
    }

    public String getRearFlag()
    {
        if (RearFlag != null && !RearFlag.equals("") && SysConst.CHANGECHARSET)
        {
            RearFlag = StrTool.unicodeToGBK(RearFlag);
        }
        return RearFlag;
    }

    public void setRearFlag(String aRearFlag)
    {
        RearFlag = aRearFlag;
    }

    public String getRearCommFlag()
    {
        if (RearCommFlag != null && !RearCommFlag.equals("") &&
            SysConst.CHANGECHARSET)
        {
            RearCommFlag = StrTool.unicodeToGBK(RearCommFlag);
        }
        return RearCommFlag;
    }

    public void setRearCommFlag(String aRearCommFlag)
    {
        RearCommFlag = aRearCommFlag;
    }

    public int getRearStartYear()
    {
        return RearStartYear;
    }

    public void setRearStartYear(int aRearStartYear)
    {
        RearStartYear = aRearStartYear;
    }

    public void setRearStartYear(String aRearStartYear)
    {
        if (aRearStartYear != null && !aRearStartYear.equals(""))
        {
            Integer tInteger = new Integer(aRearStartYear);
            int i = tInteger.intValue();
            RearStartYear = i;
        }
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    /**
     * 使用另外一个 LARearRelationSchema 对象给 Schema 赋值
     * @param: aLARearRelationSchema LARearRelationSchema
     **/
    public void setSchema(LARearRelationSchema aLARearRelationSchema)
    {
        this.RearLevel = aLARearRelationSchema.getRearLevel();
        this.RearedGens = aLARearRelationSchema.getRearedGens();
        this.AgentCode = aLARearRelationSchema.getAgentCode();
        this.RearAgentCode = aLARearRelationSchema.getRearAgentCode();
        this.AgentGroup = aLARearRelationSchema.getAgentGroup();
        this.startDate = fDate.getDate(aLARearRelationSchema.getstartDate());
        this.EndDate = fDate.getDate(aLARearRelationSchema.getEndDate());
        this.RearFlag = aLARearRelationSchema.getRearFlag();
        this.RearCommFlag = aLARearRelationSchema.getRearCommFlag();
        this.RearStartYear = aLARearRelationSchema.getRearStartYear();
        this.MakeDate = fDate.getDate(aLARearRelationSchema.getMakeDate());
        this.MakeTime = aLARearRelationSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLARearRelationSchema.getModifyDate());
        this.ModifyTime = aLARearRelationSchema.getModifyTime();
        this.Operator = aLARearRelationSchema.getOperator();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RearLevel") == null)
            {
                this.RearLevel = null;
            }
            else
            {
                this.RearLevel = rs.getString("RearLevel").trim();
            }

            this.RearedGens = rs.getInt("RearedGens");
            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("RearAgentCode") == null)
            {
                this.RearAgentCode = null;
            }
            else
            {
                this.RearAgentCode = rs.getString("RearAgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            this.startDate = rs.getDate("startDate");
            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("RearFlag") == null)
            {
                this.RearFlag = null;
            }
            else
            {
                this.RearFlag = rs.getString("RearFlag").trim();
            }

            if (rs.getString("RearCommFlag") == null)
            {
                this.RearCommFlag = null;
            }
            else
            {
                this.RearCommFlag = rs.getString("RearCommFlag").trim();
            }

            this.RearStartYear = rs.getInt("RearStartYear");
            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LARearRelationSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LARearRelationSchema getSchema()
    {
        LARearRelationSchema aLARearRelationSchema = new LARearRelationSchema();
        aLARearRelationSchema.setSchema(this);
        return aLARearRelationSchema;
    }

    public LARearRelationDB getDB()
    {
        LARearRelationDB aDBOper = new LARearRelationDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARearRelation描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RearLevel)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(RearedGens) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RearAgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(startDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(EndDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RearFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RearCommFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(RearStartYear) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARearRelation>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RearLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            RearedGens = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).intValue();
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            RearAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            startDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            RearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            RearCommFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            RearStartYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LARearRelationSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RearLevel"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearLevel));
        }
        if (FCode.equals("RearedGens"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearedGens));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("RearAgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearAgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("startDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getstartDate()));
        }
        if (FCode.equals("EndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("RearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearFlag));
        }
        if (FCode.equals("RearCommFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearCommFlag));
        }
        if (FCode.equals("RearStartYear"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearStartYear));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RearLevel);
                break;
            case 1:
                strFieldValue = String.valueOf(RearedGens);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RearAgentCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getstartDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(RearFlag);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(RearCommFlag);
                break;
            case 9:
                strFieldValue = String.valueOf(RearStartYear);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RearLevel"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RearLevel = FValue.trim();
            }
            else
            {
                RearLevel = null;
            }
        }
        if (FCode.equals("RearedGens"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RearedGens = i;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("RearAgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RearAgentCode = FValue.trim();
            }
            else
            {
                RearAgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("startDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                startDate = fDate.getDate(FValue);
            }
            else
            {
                startDate = null;
            }
        }
        if (FCode.equals("EndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDate = fDate.getDate(FValue);
            }
            else
            {
                EndDate = null;
            }
        }
        if (FCode.equals("RearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RearFlag = FValue.trim();
            }
            else
            {
                RearFlag = null;
            }
        }
        if (FCode.equals("RearCommFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RearCommFlag = FValue.trim();
            }
            else
            {
                RearCommFlag = null;
            }
        }
        if (FCode.equals("RearStartYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RearStartYear = i;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LARearRelationSchema other = (LARearRelationSchema) otherObject;
        return
                RearLevel.equals(other.getRearLevel())
                && RearedGens == other.getRearedGens()
                && AgentCode.equals(other.getAgentCode())
                && RearAgentCode.equals(other.getRearAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && fDate.getString(startDate).equals(other.getstartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && RearFlag.equals(other.getRearFlag())
                && RearCommFlag.equals(other.getRearCommFlag())
                && RearStartYear == other.getRearStartYear()
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Operator.equals(other.getOperator());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RearLevel"))
        {
            return 0;
        }
        if (strFieldName.equals("RearedGens"))
        {
            return 1;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 2;
        }
        if (strFieldName.equals("RearAgentCode"))
        {
            return 3;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 4;
        }
        if (strFieldName.equals("startDate"))
        {
            return 5;
        }
        if (strFieldName.equals("EndDate"))
        {
            return 6;
        }
        if (strFieldName.equals("RearFlag"))
        {
            return 7;
        }
        if (strFieldName.equals("RearCommFlag"))
        {
            return 8;
        }
        if (strFieldName.equals("RearStartYear"))
        {
            return 9;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 11;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 13;
        }
        if (strFieldName.equals("Operator"))
        {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RearLevel";
                break;
            case 1:
                strFieldName = "RearedGens";
                break;
            case 2:
                strFieldName = "AgentCode";
                break;
            case 3:
                strFieldName = "RearAgentCode";
                break;
            case 4:
                strFieldName = "AgentGroup";
                break;
            case 5:
                strFieldName = "startDate";
                break;
            case 6:
                strFieldName = "EndDate";
                break;
            case 7:
                strFieldName = "RearFlag";
                break;
            case 8:
                strFieldName = "RearCommFlag";
                break;
            case 9:
                strFieldName = "RearStartYear";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            case 12:
                strFieldName = "ModifyDate";
                break;
            case 13:
                strFieldName = "ModifyTime";
                break;
            case 14:
                strFieldName = "Operator";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RearLevel"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RearedGens"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RearAgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("startDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("RearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RearCommFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RearStartYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_INT;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
