/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDUserLogDB;

/*
 * <p>ClassName: LDUserLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 健管工作统计
 * @CreateDate：2011-01-04
 */
public class LDUserLogSchema implements Schema, Cloneable
{
	// @Field
	/** 日志顺序号 */
	private String LogNo;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 日志类型 */
	private String LogType;
	/** 日志内容 */
	private String LogContent;
	/** 客户端ip */
	private String CientIP;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 登出日期 */
	private Date QuitDate;
	/** 登出时间 */
	private String QuitTime;
	/** 在线时长 */
	private int OnlineTime;

	public static final int FIELDNUM = 11;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDUserLogSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "LogNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDUserLogSchema cloned = (LDUserLogSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getLogNo()
	{
		return LogNo;
	}
	public void setLogNo(String aLogNo)
	{
		LogNo = aLogNo;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getLogType()
	{
		return LogType;
	}
	public void setLogType(String aLogType)
	{
		LogType = aLogType;
	}
	public String getLogContent()
	{
		return LogContent;
	}
	public void setLogContent(String aLogContent)
	{
		LogContent = aLogContent;
	}
	public String getCientIP()
	{
		return CientIP;
	}
	public void setCientIP(String aCientIP)
	{
		CientIP = aCientIP;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getQuitDate()
	{
		if( QuitDate != null )
			return fDate.getString(QuitDate);
		else
			return null;
	}
	public void setQuitDate(Date aQuitDate)
	{
		QuitDate = aQuitDate;
	}
	public void setQuitDate(String aQuitDate)
	{
		if (aQuitDate != null && !aQuitDate.equals("") )
		{
			QuitDate = fDate.getDate( aQuitDate );
		}
		else
			QuitDate = null;
	}

	public String getQuitTime()
	{
		return QuitTime;
	}
	public void setQuitTime(String aQuitTime)
	{
		QuitTime = aQuitTime;
	}
	public int getOnlineTime()
	{
		return OnlineTime;
	}
	public void setOnlineTime(int aOnlineTime)
	{
		OnlineTime = aOnlineTime;
	}
	public void setOnlineTime(String aOnlineTime)
	{
		if (aOnlineTime != null && !aOnlineTime.equals(""))
		{
			Integer tInteger = new Integer(aOnlineTime);
			int i = tInteger.intValue();
			OnlineTime = i;
		}
	}


	/**
	* 使用另外一个 LDUserLogSchema 对象给 Schema 赋值
	* @param: aLDUserLogSchema LDUserLogSchema
	**/
	public void setSchema(LDUserLogSchema aLDUserLogSchema)
	{
		this.LogNo = aLDUserLogSchema.getLogNo();
		this.ManageCom = aLDUserLogSchema.getManageCom();
		this.Operator = aLDUserLogSchema.getOperator();
		this.LogType = aLDUserLogSchema.getLogType();
		this.LogContent = aLDUserLogSchema.getLogContent();
		this.CientIP = aLDUserLogSchema.getCientIP();
		this.MakeDate = fDate.getDate( aLDUserLogSchema.getMakeDate());
		this.MakeTime = aLDUserLogSchema.getMakeTime();
		this.QuitDate = fDate.getDate( aLDUserLogSchema.getQuitDate());
		this.QuitTime = aLDUserLogSchema.getQuitTime();
		this.OnlineTime = aLDUserLogSchema.getOnlineTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("LogNo") == null )
				this.LogNo = null;
			else
				this.LogNo = rs.getString("LogNo").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("LogType") == null )
				this.LogType = null;
			else
				this.LogType = rs.getString("LogType").trim();

			if( rs.getString("LogContent") == null )
				this.LogContent = null;
			else
				this.LogContent = rs.getString("LogContent").trim();

			if( rs.getString("CientIP") == null )
				this.CientIP = null;
			else
				this.CientIP = rs.getString("CientIP").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.QuitDate = rs.getDate("QuitDate");
			if( rs.getString("QuitTime") == null )
				this.QuitTime = null;
			else
				this.QuitTime = rs.getString("QuitTime").trim();

			this.OnlineTime = rs.getInt("OnlineTime");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDUserLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDUserLogSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDUserLogSchema getSchema()
	{
		LDUserLogSchema aLDUserLogSchema = new LDUserLogSchema();
		aLDUserLogSchema.setSchema(this);
		return aLDUserLogSchema;
	}

	public LDUserLogDB getDB()
	{
		LDUserLogDB aDBOper = new LDUserLogDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDUserLog描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(LogNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LogType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LogContent)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CientIP)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( QuitDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(QuitTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OnlineTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDUserLog>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			LogNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			LogType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			LogContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CientIP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			QuitDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			QuitTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			OnlineTime= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).intValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDUserLogSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("LogNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LogNo));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("LogType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LogType));
		}
		if (FCode.equals("LogContent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LogContent));
		}
		if (FCode.equals("CientIP"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CientIP));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("QuitDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getQuitDate()));
		}
		if (FCode.equals("QuitTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(QuitTime));
		}
		if (FCode.equals("OnlineTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OnlineTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(LogNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(LogType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(LogContent);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CientIP);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getQuitDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(QuitTime);
				break;
			case 10:
				strFieldValue = String.valueOf(OnlineTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("LogNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LogNo = FValue.trim();
			}
			else
				LogNo = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("LogType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LogType = FValue.trim();
			}
			else
				LogType = null;
		}
		if (FCode.equalsIgnoreCase("LogContent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LogContent = FValue.trim();
			}
			else
				LogContent = null;
		}
		if (FCode.equalsIgnoreCase("CientIP"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CientIP = FValue.trim();
			}
			else
				CientIP = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("QuitDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				QuitDate = fDate.getDate( FValue );
			}
			else
				QuitDate = null;
		}
		if (FCode.equalsIgnoreCase("QuitTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				QuitTime = FValue.trim();
			}
			else
				QuitTime = null;
		}
		if (FCode.equalsIgnoreCase("OnlineTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OnlineTime = i;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDUserLogSchema other = (LDUserLogSchema)otherObject;
		return
			(LogNo == null ? other.getLogNo() == null : LogNo.equals(other.getLogNo()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (LogType == null ? other.getLogType() == null : LogType.equals(other.getLogType()))
			&& (LogContent == null ? other.getLogContent() == null : LogContent.equals(other.getLogContent()))
			&& (CientIP == null ? other.getCientIP() == null : CientIP.equals(other.getCientIP()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (QuitDate == null ? other.getQuitDate() == null : fDate.getString(QuitDate).equals(other.getQuitDate()))
			&& (QuitTime == null ? other.getQuitTime() == null : QuitTime.equals(other.getQuitTime()))
			&& OnlineTime == other.getOnlineTime();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("LogNo") ) {
			return 0;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 1;
		}
		if( strFieldName.equals("Operator") ) {
			return 2;
		}
		if( strFieldName.equals("LogType") ) {
			return 3;
		}
		if( strFieldName.equals("LogContent") ) {
			return 4;
		}
		if( strFieldName.equals("CientIP") ) {
			return 5;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 6;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 7;
		}
		if( strFieldName.equals("QuitDate") ) {
			return 8;
		}
		if( strFieldName.equals("QuitTime") ) {
			return 9;
		}
		if( strFieldName.equals("OnlineTime") ) {
			return 10;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "LogNo";
				break;
			case 1:
				strFieldName = "ManageCom";
				break;
			case 2:
				strFieldName = "Operator";
				break;
			case 3:
				strFieldName = "LogType";
				break;
			case 4:
				strFieldName = "LogContent";
				break;
			case 5:
				strFieldName = "CientIP";
				break;
			case 6:
				strFieldName = "MakeDate";
				break;
			case 7:
				strFieldName = "MakeTime";
				break;
			case 8:
				strFieldName = "QuitDate";
				break;
			case 9:
				strFieldName = "QuitTime";
				break;
			case 10:
				strFieldName = "OnlineTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("LogNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LogType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LogContent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CientIP") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("QuitDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("QuitTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OnlineTime") ) {
			return Schema.TYPE_INT;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_INT;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
