/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMRiskBonusRateDB;

/*
 * <p>ClassName: LMRiskBonusRateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 分红利率描述表变更
 * @CreateDate：2015-04-22
 */
public class LMRiskBonusRateSchema implements Schema, Cloneable
{
	// @Field
	/** 险种代码 */
	private String RiskCode;
	/** 红利年度 */
	private String BonusYear;
	/** 保单承包年度 */
	private String AppYear;
	/** 保险期间 */
	private int InsuYears;
	/** 缴费期间 */
	private int PayYears;
	/** 最小投保年龄 */
	private int MinAge;
	/** 最大投保年龄 */
	private int MaxAge;
	/** 分红率 */
	private double HLRate;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 死差率 */
	private double SCRate;

	public static final int FIELDNUM = 14;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LMRiskBonusRateSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[7];
		pk[0] = "RiskCode";
		pk[1] = "BonusYear";
		pk[2] = "AppYear";
		pk[3] = "InsuYears";
		pk[4] = "PayYears";
		pk[5] = "MinAge";
		pk[6] = "MaxAge";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LMRiskBonusRateSchema cloned = (LMRiskBonusRateSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getBonusYear()
	{
		return BonusYear;
	}
	public void setBonusYear(String aBonusYear)
	{
		BonusYear = aBonusYear;
	}
	public String getAppYear()
	{
		return AppYear;
	}
	public void setAppYear(String aAppYear)
	{
		AppYear = aAppYear;
	}
	public int getInsuYears()
	{
		return InsuYears;
	}
	public void setInsuYears(int aInsuYears)
	{
		InsuYears = aInsuYears;
	}
	public void setInsuYears(String aInsuYears)
	{
		if (aInsuYears != null && !aInsuYears.equals(""))
		{
			Integer tInteger = new Integer(aInsuYears);
			int i = tInteger.intValue();
			InsuYears = i;
		}
	}

	public int getPayYears()
	{
		return PayYears;
	}
	public void setPayYears(int aPayYears)
	{
		PayYears = aPayYears;
	}
	public void setPayYears(String aPayYears)
	{
		if (aPayYears != null && !aPayYears.equals(""))
		{
			Integer tInteger = new Integer(aPayYears);
			int i = tInteger.intValue();
			PayYears = i;
		}
	}

	public int getMinAge()
	{
		return MinAge;
	}
	public void setMinAge(int aMinAge)
	{
		MinAge = aMinAge;
	}
	public void setMinAge(String aMinAge)
	{
		if (aMinAge != null && !aMinAge.equals(""))
		{
			Integer tInteger = new Integer(aMinAge);
			int i = tInteger.intValue();
			MinAge = i;
		}
	}

	public int getMaxAge()
	{
		return MaxAge;
	}
	public void setMaxAge(int aMaxAge)
	{
		MaxAge = aMaxAge;
	}
	public void setMaxAge(String aMaxAge)
	{
		if (aMaxAge != null && !aMaxAge.equals(""))
		{
			Integer tInteger = new Integer(aMaxAge);
			int i = tInteger.intValue();
			MaxAge = i;
		}
	}

	public double getHLRate()
	{
		return HLRate;
	}
	public void setHLRate(double aHLRate)
	{
		HLRate = Arith.round(aHLRate,6);
	}
	public void setHLRate(String aHLRate)
	{
		if (aHLRate != null && !aHLRate.equals(""))
		{
			Double tDouble = new Double(aHLRate);
			double d = tDouble.doubleValue();
                HLRate = Arith.round(d,6);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public double getSCRate()
	{
		return SCRate;
	}
	public void setSCRate(double aSCRate)
	{
		SCRate = Arith.round(aSCRate,6);
	}
	public void setSCRate(String aSCRate)
	{
		if (aSCRate != null && !aSCRate.equals(""))
		{
			Double tDouble = new Double(aSCRate);
			double d = tDouble.doubleValue();
                SCRate = Arith.round(d,6);
		}
	}


	/**
	* 使用另外一个 LMRiskBonusRateSchema 对象给 Schema 赋值
	* @param: aLMRiskBonusRateSchema LMRiskBonusRateSchema
	**/
	public void setSchema(LMRiskBonusRateSchema aLMRiskBonusRateSchema)
	{
		this.RiskCode = aLMRiskBonusRateSchema.getRiskCode();
		this.BonusYear = aLMRiskBonusRateSchema.getBonusYear();
		this.AppYear = aLMRiskBonusRateSchema.getAppYear();
		this.InsuYears = aLMRiskBonusRateSchema.getInsuYears();
		this.PayYears = aLMRiskBonusRateSchema.getPayYears();
		this.MinAge = aLMRiskBonusRateSchema.getMinAge();
		this.MaxAge = aLMRiskBonusRateSchema.getMaxAge();
		this.HLRate = aLMRiskBonusRateSchema.getHLRate();
		this.Operator = aLMRiskBonusRateSchema.getOperator();
		this.MakeDate = fDate.getDate( aLMRiskBonusRateSchema.getMakeDate());
		this.MakeTime = aLMRiskBonusRateSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLMRiskBonusRateSchema.getModifyDate());
		this.ModifyTime = aLMRiskBonusRateSchema.getModifyTime();
		this.SCRate = aLMRiskBonusRateSchema.getSCRate();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("BonusYear") == null )
				this.BonusYear = null;
			else
				this.BonusYear = rs.getString("BonusYear").trim();

			if( rs.getString("AppYear") == null )
				this.AppYear = null;
			else
				this.AppYear = rs.getString("AppYear").trim();

			this.InsuYears = rs.getInt("InsuYears");
			this.PayYears = rs.getInt("PayYears");
			this.MinAge = rs.getInt("MinAge");
			this.MaxAge = rs.getInt("MaxAge");
			this.HLRate = rs.getDouble("HLRate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.SCRate = rs.getDouble("SCRate");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LMRiskBonusRate表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMRiskBonusRateSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LMRiskBonusRateSchema getSchema()
	{
		LMRiskBonusRateSchema aLMRiskBonusRateSchema = new LMRiskBonusRateSchema();
		aLMRiskBonusRateSchema.setSchema(this);
		return aLMRiskBonusRateSchema;
	}

	public LMRiskBonusRateDB getDB()
	{
		LMRiskBonusRateDB aDBOper = new LMRiskBonusRateDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskBonusRate描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BonusYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuYears));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayYears));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MinAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MaxAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(HLRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SCRate));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskBonusRate>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BonusYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AppYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			InsuYears= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			PayYears= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).intValue();
			MinAge= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
			MaxAge= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).intValue();
			HLRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			SCRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMRiskBonusRateSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("BonusYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BonusYear));
		}
		if (FCode.equals("AppYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppYear));
		}
		if (FCode.equals("InsuYears"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuYears));
		}
		if (FCode.equals("PayYears"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayYears));
		}
		if (FCode.equals("MinAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MinAge));
		}
		if (FCode.equals("MaxAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAge));
		}
		if (FCode.equals("HLRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HLRate));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("SCRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SCRate));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BonusYear);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AppYear);
				break;
			case 3:
				strFieldValue = String.valueOf(InsuYears);
				break;
			case 4:
				strFieldValue = String.valueOf(PayYears);
				break;
			case 5:
				strFieldValue = String.valueOf(MinAge);
				break;
			case 6:
				strFieldValue = String.valueOf(MaxAge);
				break;
			case 7:
				strFieldValue = String.valueOf(HLRate);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 13:
				strFieldValue = String.valueOf(SCRate);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("BonusYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BonusYear = FValue.trim();
			}
			else
				BonusYear = null;
		}
		if (FCode.equalsIgnoreCase("AppYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppYear = FValue.trim();
			}
			else
				AppYear = null;
		}
		if (FCode.equalsIgnoreCase("InsuYears"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuYears = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayYears"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayYears = i;
			}
		}
		if (FCode.equalsIgnoreCase("MinAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MinAge = i;
			}
		}
		if (FCode.equalsIgnoreCase("MaxAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MaxAge = i;
			}
		}
		if (FCode.equalsIgnoreCase("HLRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				HLRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("SCRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SCRate = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LMRiskBonusRateSchema other = (LMRiskBonusRateSchema)otherObject;
		return
			(RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (BonusYear == null ? other.getBonusYear() == null : BonusYear.equals(other.getBonusYear()))
			&& (AppYear == null ? other.getAppYear() == null : AppYear.equals(other.getAppYear()))
			&& InsuYears == other.getInsuYears()
			&& PayYears == other.getPayYears()
			&& MinAge == other.getMinAge()
			&& MaxAge == other.getMaxAge()
			&& HLRate == other.getHLRate()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& SCRate == other.getSCRate();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RiskCode") ) {
			return 0;
		}
		if( strFieldName.equals("BonusYear") ) {
			return 1;
		}
		if( strFieldName.equals("AppYear") ) {
			return 2;
		}
		if( strFieldName.equals("InsuYears") ) {
			return 3;
		}
		if( strFieldName.equals("PayYears") ) {
			return 4;
		}
		if( strFieldName.equals("MinAge") ) {
			return 5;
		}
		if( strFieldName.equals("MaxAge") ) {
			return 6;
		}
		if( strFieldName.equals("HLRate") ) {
			return 7;
		}
		if( strFieldName.equals("Operator") ) {
			return 8;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 9;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 12;
		}
		if( strFieldName.equals("SCRate") ) {
			return 13;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RiskCode";
				break;
			case 1:
				strFieldName = "BonusYear";
				break;
			case 2:
				strFieldName = "AppYear";
				break;
			case 3:
				strFieldName = "InsuYears";
				break;
			case 4:
				strFieldName = "PayYears";
				break;
			case 5:
				strFieldName = "MinAge";
				break;
			case 6:
				strFieldName = "MaxAge";
				break;
			case 7:
				strFieldName = "HLRate";
				break;
			case 8:
				strFieldName = "Operator";
				break;
			case 9:
				strFieldName = "MakeDate";
				break;
			case 10:
				strFieldName = "MakeTime";
				break;
			case 11:
				strFieldName = "ModifyDate";
				break;
			case 12:
				strFieldName = "ModifyTime";
				break;
			case 13:
				strFieldName = "SCRate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BonusYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuYears") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayYears") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MinAge") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MaxAge") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("HLRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SCRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_INT;
				break;
			case 5:
				nFieldType = Schema.TYPE_INT;
				break;
			case 6:
				nFieldType = Schema.TYPE_INT;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
