/*
 * <p>ClassName: msh_cashflowSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.msh_cashflowDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class msh_cashflowSchema implements Schema
{
    // @Field
    /** 公司编码 */
    private String unitcode;
    /** 公司名称 */
    private String unitname;
    /** 年度 */
    private String year;
    /** 会计期间 */
    private String month;
    /** 项目编码 */
    private String itemcode;
    /** 项目名称 */
    private String itemame;
    /** 借方发生 */
    private double debitamount;
    /** 贷方发生 */
    private double creditamount;
    /** 借方累计 */
    private double totaldebitamount;
    /** 贷方累计 */
    private double totalcreditamount;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public msh_cashflowSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getunitcode()
    {
        if (unitcode != null && !unitcode.equals("") && SysConst.CHANGECHARSET == true)
        {
            unitcode = StrTool.unicodeToGBK(unitcode);
        }
        return unitcode;
    }

    public void setunitcode(String aunitcode)
    {
        unitcode = aunitcode;
    }

    public String getunitname()
    {
        if (unitname != null && !unitname.equals("") && SysConst.CHANGECHARSET == true)
        {
            unitname = StrTool.unicodeToGBK(unitname);
        }
        return unitname;
    }

    public void setunitname(String aunitname)
    {
        unitname = aunitname;
    }

    public String getyear()
    {
        if (year != null && !year.equals("") && SysConst.CHANGECHARSET == true)
        {
            year = StrTool.unicodeToGBK(year);
        }
        return year;
    }

    public void setyear(String ayear)
    {
        year = ayear;
    }

    public String getmonth()
    {
        if (month != null && !month.equals("") && SysConst.CHANGECHARSET == true)
        {
            month = StrTool.unicodeToGBK(month);
        }
        return month;
    }

    public void setmonth(String amonth)
    {
        month = amonth;
    }

    public String getitemcode()
    {
        if (itemcode != null && !itemcode.equals("") && SysConst.CHANGECHARSET == true)
        {
            itemcode = StrTool.unicodeToGBK(itemcode);
        }
        return itemcode;
    }

    public void setitemcode(String aitemcode)
    {
        itemcode = aitemcode;
    }

    public String getitemame()
    {
        if (itemame != null && !itemame.equals("") && SysConst.CHANGECHARSET == true)
        {
            itemame = StrTool.unicodeToGBK(itemame);
        }
        return itemame;
    }

    public void setitemame(String aitemame)
    {
        itemame = aitemame;
    }

    public double getdebitamount()
    {
        return debitamount;
    }

    public void setdebitamount(double adebitamount)
    {
        debitamount = adebitamount;
    }

    public void setdebitamount(String adebitamount)
    {
        if (adebitamount != null && !adebitamount.equals(""))
        {
            Double tDouble = new Double(adebitamount);
            double d = tDouble.doubleValue();
            debitamount = d;
        }
    }

    public double getcreditamount()
    {
        return creditamount;
    }

    public void setcreditamount(double acreditamount)
    {
        creditamount = acreditamount;
    }

    public void setcreditamount(String acreditamount)
    {
        if (acreditamount != null && !acreditamount.equals(""))
        {
            Double tDouble = new Double(acreditamount);
            double d = tDouble.doubleValue();
            creditamount = d;
        }
    }

    public double gettotaldebitamount()
    {
        return totaldebitamount;
    }

    public void settotaldebitamount(double atotaldebitamount)
    {
        totaldebitamount = atotaldebitamount;
    }

    public void settotaldebitamount(String atotaldebitamount)
    {
        if (atotaldebitamount != null && !atotaldebitamount.equals(""))
        {
            Double tDouble = new Double(atotaldebitamount);
            double d = tDouble.doubleValue();
            totaldebitamount = d;
        }
    }

    public double gettotalcreditamount()
    {
        return totalcreditamount;
    }

    public void settotalcreditamount(double atotalcreditamount)
    {
        totalcreditamount = atotalcreditamount;
    }

    public void settotalcreditamount(String atotalcreditamount)
    {
        if (atotalcreditamount != null && !atotalcreditamount.equals(""))
        {
            Double tDouble = new Double(atotalcreditamount);
            double d = tDouble.doubleValue();
            totalcreditamount = d;
        }
    }


    /**
     * 使用另外一个 msh_cashflowSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(msh_cashflowSchema amsh_cashflowSchema)
    {
        this.unitcode = amsh_cashflowSchema.getunitcode();
        this.unitname = amsh_cashflowSchema.getunitname();
        this.year = amsh_cashflowSchema.getyear();
        this.month = amsh_cashflowSchema.getmonth();
        this.itemcode = amsh_cashflowSchema.getitemcode();
        this.itemame = amsh_cashflowSchema.getitemame();
        this.debitamount = amsh_cashflowSchema.getdebitamount();
        this.creditamount = amsh_cashflowSchema.getcreditamount();
        this.totaldebitamount = amsh_cashflowSchema.gettotaldebitamount();
        this.totalcreditamount = amsh_cashflowSchema.gettotalcreditamount();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("unitcode") == null)
            {
                this.unitcode = null;
            }
            else
            {
                this.unitcode = rs.getString("unitcode").trim();
            }

            if (rs.getString("unitname") == null)
            {
                this.unitname = null;
            }
            else
            {
                this.unitname = rs.getString("unitname").trim();
            }

            if (rs.getString("year") == null)
            {
                this.year = null;
            }
            else
            {
                this.year = rs.getString("year").trim();
            }

            if (rs.getString("month") == null)
            {
                this.month = null;
            }
            else
            {
                this.month = rs.getString("month").trim();
            }

            if (rs.getString("itemcode") == null)
            {
                this.itemcode = null;
            }
            else
            {
                this.itemcode = rs.getString("itemcode").trim();
            }

            if (rs.getString("itemame") == null)
            {
                this.itemame = null;
            }
            else
            {
                this.itemame = rs.getString("itemame").trim();
            }

            this.debitamount = rs.getDouble("debitamount");
            this.creditamount = rs.getDouble("creditamount");
            this.totaldebitamount = rs.getDouble("totaldebitamount");
            this.totalcreditamount = rs.getDouble("totalcreditamount");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "msh_cashflowSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public msh_cashflowSchema getSchema()
    {
        msh_cashflowSchema amsh_cashflowSchema = new msh_cashflowSchema();
        amsh_cashflowSchema.setSchema(this);
        return amsh_cashflowSchema;
    }

    public msh_cashflowDB getDB()
    {
        msh_cashflowDB aDBOper = new msh_cashflowDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpmsh_cashflow描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(unitcode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(unitname)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(year)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(month)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(itemcode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(itemame)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(debitamount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(creditamount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(totaldebitamount) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(totalcreditamount);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpmsh_cashflow>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            unitcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            unitname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            year = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                  SysConst.PACKAGESPILTER);
            month = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            itemcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            itemame = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            debitamount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            creditamount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            totaldebitamount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            totalcreditamount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "msh_cashflowSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("unitcode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(unitcode));
        }
        if (FCode.equals("unitname"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(unitname));
        }
        if (FCode.equals("year"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(year));
        }
        if (FCode.equals("month"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(month));
        }
        if (FCode.equals("itemcode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(itemcode));
        }
        if (FCode.equals("itemame"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(itemame));
        }
        if (FCode.equals("debitamount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(debitamount));
        }
        if (FCode.equals("creditamount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(creditamount));
        }
        if (FCode.equals("totaldebitamount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(totaldebitamount));
        }
        if (FCode.equals("totalcreditamount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    totalcreditamount));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(unitcode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(unitname);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(year);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(month);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(itemcode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(itemame);
                break;
            case 6:
                strFieldValue = String.valueOf(debitamount);
                break;
            case 7:
                strFieldValue = String.valueOf(creditamount);
                break;
            case 8:
                strFieldValue = String.valueOf(totaldebitamount);
                break;
            case 9:
                strFieldValue = String.valueOf(totalcreditamount);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("unitcode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                unitcode = FValue.trim();
            }
            else
            {
                unitcode = null;
            }
        }
        if (FCode.equals("unitname"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                unitname = FValue.trim();
            }
            else
            {
                unitname = null;
            }
        }
        if (FCode.equals("year"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                year = FValue.trim();
            }
            else
            {
                year = null;
            }
        }
        if (FCode.equals("month"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                month = FValue.trim();
            }
            else
            {
                month = null;
            }
        }
        if (FCode.equals("itemcode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                itemcode = FValue.trim();
            }
            else
            {
                itemcode = null;
            }
        }
        if (FCode.equals("itemame"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                itemame = FValue.trim();
            }
            else
            {
                itemame = null;
            }
        }
        if (FCode.equals("debitamount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                debitamount = d;
            }
        }
        if (FCode.equals("creditamount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                creditamount = d;
            }
        }
        if (FCode.equals("totaldebitamount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                totaldebitamount = d;
            }
        }
        if (FCode.equals("totalcreditamount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                totalcreditamount = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        msh_cashflowSchema other = (msh_cashflowSchema) otherObject;
        return
                unitcode.equals(other.getunitcode())
                && unitname.equals(other.getunitname())
                && year.equals(other.getyear())
                && month.equals(other.getmonth())
                && itemcode.equals(other.getitemcode())
                && itemame.equals(other.getitemame())
                && debitamount == other.getdebitamount()
                && creditamount == other.getcreditamount()
                && totaldebitamount == other.gettotaldebitamount()
                && totalcreditamount == other.gettotalcreditamount();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("unitcode"))
        {
            return 0;
        }
        if (strFieldName.equals("unitname"))
        {
            return 1;
        }
        if (strFieldName.equals("year"))
        {
            return 2;
        }
        if (strFieldName.equals("month"))
        {
            return 3;
        }
        if (strFieldName.equals("itemcode"))
        {
            return 4;
        }
        if (strFieldName.equals("itemame"))
        {
            return 5;
        }
        if (strFieldName.equals("debitamount"))
        {
            return 6;
        }
        if (strFieldName.equals("creditamount"))
        {
            return 7;
        }
        if (strFieldName.equals("totaldebitamount"))
        {
            return 8;
        }
        if (strFieldName.equals("totalcreditamount"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "unitcode";
                break;
            case 1:
                strFieldName = "unitname";
                break;
            case 2:
                strFieldName = "year";
                break;
            case 3:
                strFieldName = "month";
                break;
            case 4:
                strFieldName = "itemcode";
                break;
            case 5:
                strFieldName = "itemame";
                break;
            case 6:
                strFieldName = "debitamount";
                break;
            case 7:
                strFieldName = "creditamount";
                break;
            case 8:
                strFieldName = "totaldebitamount";
                break;
            case 9:
                strFieldName = "totalcreditamount";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("unitcode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("unitname"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("year"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("month"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("itemcode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("itemame"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("debitamount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("creditamount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("totaldebitamount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("totalcreditamount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
