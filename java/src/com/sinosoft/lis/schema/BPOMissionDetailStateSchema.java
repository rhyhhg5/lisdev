/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.BPOMissionDetailStateDB;

/*
 * <p>ClassName: BPOMissionDetailStateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-09-28
 */
public class BPOMissionDetailStateSchema implements Schema, Cloneable
{
	// @Field
	/** 任务id */
	private String MissionID;
	/** 子任务id */
	private String SubMissionID;
	/** 活动id */
	private String ActivityID;
	/** 过程id */
	private String ProcessID;
	/** 外包批次 */
	private String BPOBatchNo;
	/** 系统导入批次 */
	private String BatchNo;
	/** 任务导入次数 */
	private int ImportCount;
	/** 任务处理次数 */
	private int DealCount;
	/** 业务号码类型 */
	private String BussNoType;
	/** 业务号码 */
	private String BussNo;
	/** 处理机构 */
	private String Managecom;
	/** 其他号码类型 */
	private String OthernoType;
	/** 其他号码 */
	private String OtherNo;
	/** 操作编号 */
	private int SequenceNo;
	/** 操作类型 */
	private String OperateType;
	/** 操作结果 */
	private String OperateResult;
	/** 操作内容 */
	private String Content;
	/** 备注 */
	private String Remark;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 23;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public BPOMissionDetailStateSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[6];
		pk[0] = "MissionID";
		pk[1] = "BPOBatchNo";
		pk[2] = "DealCount";
		pk[3] = "BussNoType";
		pk[4] = "BussNo";
		pk[5] = "SequenceNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                BPOMissionDetailStateSchema cloned = (BPOMissionDetailStateSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMissionID()
	{
		return MissionID;
	}
	public void setMissionID(String aMissionID)
	{
            MissionID = aMissionID;
	}
	public String getSubMissionID()
	{
		return SubMissionID;
	}
	public void setSubMissionID(String aSubMissionID)
	{
            SubMissionID = aSubMissionID;
	}
	public String getActivityID()
	{
		return ActivityID;
	}
	public void setActivityID(String aActivityID)
	{
            ActivityID = aActivityID;
	}
	public String getProcessID()
	{
		return ProcessID;
	}
	public void setProcessID(String aProcessID)
	{
            ProcessID = aProcessID;
	}
	public String getBPOBatchNo()
	{
		return BPOBatchNo;
	}
	public void setBPOBatchNo(String aBPOBatchNo)
	{
            BPOBatchNo = aBPOBatchNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
            BatchNo = aBatchNo;
	}
	public int getImportCount()
	{
		return ImportCount;
	}
	public void setImportCount(int aImportCount)
	{
            ImportCount = aImportCount;
	}
	public void setImportCount(String aImportCount)
	{
		if (aImportCount != null && !aImportCount.equals(""))
		{
			Integer tInteger = new Integer(aImportCount);
			int i = tInteger.intValue();
			ImportCount = i;
		}
	}

	public int getDealCount()
	{
		return DealCount;
	}
	public void setDealCount(int aDealCount)
	{
            DealCount = aDealCount;
	}
	public void setDealCount(String aDealCount)
	{
		if (aDealCount != null && !aDealCount.equals(""))
		{
			Integer tInteger = new Integer(aDealCount);
			int i = tInteger.intValue();
			DealCount = i;
		}
	}

	public String getBussNoType()
	{
		return BussNoType;
	}
	public void setBussNoType(String aBussNoType)
	{
            BussNoType = aBussNoType;
	}
	public String getBussNo()
	{
		return BussNo;
	}
	public void setBussNo(String aBussNo)
	{
            BussNo = aBussNo;
	}
	public String getManagecom()
	{
		return Managecom;
	}
	public void setManagecom(String aManagecom)
	{
            Managecom = aManagecom;
	}
	public String getOthernoType()
	{
		return OthernoType;
	}
	public void setOthernoType(String aOthernoType)
	{
            OthernoType = aOthernoType;
	}
	public String getOtherNo()
	{
		return OtherNo;
	}
	public void setOtherNo(String aOtherNo)
	{
            OtherNo = aOtherNo;
	}
	public int getSequenceNo()
	{
		return SequenceNo;
	}
	public void setSequenceNo(int aSequenceNo)
	{
            SequenceNo = aSequenceNo;
	}
	public void setSequenceNo(String aSequenceNo)
	{
		if (aSequenceNo != null && !aSequenceNo.equals(""))
		{
			Integer tInteger = new Integer(aSequenceNo);
			int i = tInteger.intValue();
			SequenceNo = i;
		}
	}

	public String getOperateType()
	{
		return OperateType;
	}
	public void setOperateType(String aOperateType)
	{
            OperateType = aOperateType;
	}
	public String getOperateResult()
	{
		return OperateResult;
	}
	public void setOperateResult(String aOperateResult)
	{
            OperateResult = aOperateResult;
	}
	public String getContent()
	{
		return Content;
	}
	public void setContent(String aContent)
	{
            Content = aContent;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
            Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 BPOMissionDetailStateSchema 对象给 Schema 赋值
	* @param: aBPOMissionDetailStateSchema BPOMissionDetailStateSchema
	**/
	public void setSchema(BPOMissionDetailStateSchema aBPOMissionDetailStateSchema)
	{
		this.MissionID = aBPOMissionDetailStateSchema.getMissionID();
		this.SubMissionID = aBPOMissionDetailStateSchema.getSubMissionID();
		this.ActivityID = aBPOMissionDetailStateSchema.getActivityID();
		this.ProcessID = aBPOMissionDetailStateSchema.getProcessID();
		this.BPOBatchNo = aBPOMissionDetailStateSchema.getBPOBatchNo();
		this.BatchNo = aBPOMissionDetailStateSchema.getBatchNo();
		this.ImportCount = aBPOMissionDetailStateSchema.getImportCount();
		this.DealCount = aBPOMissionDetailStateSchema.getDealCount();
		this.BussNoType = aBPOMissionDetailStateSchema.getBussNoType();
		this.BussNo = aBPOMissionDetailStateSchema.getBussNo();
		this.Managecom = aBPOMissionDetailStateSchema.getManagecom();
		this.OthernoType = aBPOMissionDetailStateSchema.getOthernoType();
		this.OtherNo = aBPOMissionDetailStateSchema.getOtherNo();
		this.SequenceNo = aBPOMissionDetailStateSchema.getSequenceNo();
		this.OperateType = aBPOMissionDetailStateSchema.getOperateType();
		this.OperateResult = aBPOMissionDetailStateSchema.getOperateResult();
		this.Content = aBPOMissionDetailStateSchema.getContent();
		this.Remark = aBPOMissionDetailStateSchema.getRemark();
		this.Operator = aBPOMissionDetailStateSchema.getOperator();
		this.MakeDate = fDate.getDate( aBPOMissionDetailStateSchema.getMakeDate());
		this.MakeTime = aBPOMissionDetailStateSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aBPOMissionDetailStateSchema.getModifyDate());
		this.ModifyTime = aBPOMissionDetailStateSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MissionID") == null )
				this.MissionID = null;
			else
				this.MissionID = rs.getString("MissionID").trim();

			if( rs.getString("SubMissionID") == null )
				this.SubMissionID = null;
			else
				this.SubMissionID = rs.getString("SubMissionID").trim();

			if( rs.getString("ActivityID") == null )
				this.ActivityID = null;
			else
				this.ActivityID = rs.getString("ActivityID").trim();

			if( rs.getString("ProcessID") == null )
				this.ProcessID = null;
			else
				this.ProcessID = rs.getString("ProcessID").trim();

			if( rs.getString("BPOBatchNo") == null )
				this.BPOBatchNo = null;
			else
				this.BPOBatchNo = rs.getString("BPOBatchNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			this.ImportCount = rs.getInt("ImportCount");
			this.DealCount = rs.getInt("DealCount");
			if( rs.getString("BussNoType") == null )
				this.BussNoType = null;
			else
				this.BussNoType = rs.getString("BussNoType").trim();

			if( rs.getString("BussNo") == null )
				this.BussNo = null;
			else
				this.BussNo = rs.getString("BussNo").trim();

			if( rs.getString("Managecom") == null )
				this.Managecom = null;
			else
				this.Managecom = rs.getString("Managecom").trim();

			if( rs.getString("OthernoType") == null )
				this.OthernoType = null;
			else
				this.OthernoType = rs.getString("OthernoType").trim();

			if( rs.getString("OtherNo") == null )
				this.OtherNo = null;
			else
				this.OtherNo = rs.getString("OtherNo").trim();

			this.SequenceNo = rs.getInt("SequenceNo");
			if( rs.getString("OperateType") == null )
				this.OperateType = null;
			else
				this.OperateType = rs.getString("OperateType").trim();

			if( rs.getString("OperateResult") == null )
				this.OperateResult = null;
			else
				this.OperateResult = rs.getString("OperateResult").trim();

			if( rs.getString("Content") == null )
				this.Content = null;
			else
				this.Content = rs.getString("Content").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的BPOMissionDetailState表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOMissionDetailStateSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public BPOMissionDetailStateSchema getSchema()
	{
		BPOMissionDetailStateSchema aBPOMissionDetailStateSchema = new BPOMissionDetailStateSchema();
		aBPOMissionDetailStateSchema.setSchema(this);
		return aBPOMissionDetailStateSchema;
	}

	public BPOMissionDetailStateDB getDB()
	{
		BPOMissionDetailStateDB aDBOper = new BPOMissionDetailStateDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOMissionDetailState描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(MissionID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SubMissionID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ActivityID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ProcessID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BPOBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ImportCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DealCount));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BussNoType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BussNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Managecom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OthernoType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SequenceNo));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OperateType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OperateResult)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Content)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOMissionDetailState>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MissionID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SubMissionID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ActivityID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ProcessID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BPOBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ImportCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).intValue();
			DealCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).intValue();
			BussNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			BussNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			OthernoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			SequenceNo= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).intValue();
			OperateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			OperateResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Content = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOMissionDetailStateSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MissionID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MissionID));
		}
		if (FCode.equals("SubMissionID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubMissionID));
		}
		if (FCode.equals("ActivityID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ActivityID));
		}
		if (FCode.equals("ProcessID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProcessID));
		}
		if (FCode.equals("BPOBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BPOBatchNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("ImportCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImportCount));
		}
		if (FCode.equals("DealCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealCount));
		}
		if (FCode.equals("BussNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BussNoType));
		}
		if (FCode.equals("BussNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BussNo));
		}
		if (FCode.equals("Managecom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
		}
		if (FCode.equals("OthernoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthernoType));
		}
		if (FCode.equals("OtherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
		}
		if (FCode.equals("SequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SequenceNo));
		}
		if (FCode.equals("OperateType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OperateType));
		}
		if (FCode.equals("OperateResult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OperateResult));
		}
		if (FCode.equals("Content"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Content));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MissionID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SubMissionID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ActivityID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ProcessID);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BPOBatchNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 6:
				strFieldValue = String.valueOf(ImportCount);
				break;
			case 7:
				strFieldValue = String.valueOf(DealCount);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(BussNoType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(BussNo);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Managecom);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(OthernoType);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(OtherNo);
				break;
			case 13:
				strFieldValue = String.valueOf(SequenceNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(OperateType);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(OperateResult);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Content);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MissionID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MissionID = FValue.trim();
			}
			else
				MissionID = null;
		}
		if (FCode.equalsIgnoreCase("SubMissionID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubMissionID = FValue.trim();
			}
			else
				SubMissionID = null;
		}
		if (FCode.equalsIgnoreCase("ActivityID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ActivityID = FValue.trim();
			}
			else
				ActivityID = null;
		}
		if (FCode.equalsIgnoreCase("ProcessID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProcessID = FValue.trim();
			}
			else
				ProcessID = null;
		}
		if (FCode.equalsIgnoreCase("BPOBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BPOBatchNo = FValue.trim();
			}
			else
				BPOBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("ImportCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ImportCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("DealCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DealCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("BussNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BussNoType = FValue.trim();
			}
			else
				BussNoType = null;
		}
		if (FCode.equalsIgnoreCase("BussNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BussNo = FValue.trim();
			}
			else
				BussNo = null;
		}
		if (FCode.equalsIgnoreCase("Managecom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Managecom = FValue.trim();
			}
			else
				Managecom = null;
		}
		if (FCode.equalsIgnoreCase("OthernoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthernoType = FValue.trim();
			}
			else
				OthernoType = null;
		}
		if (FCode.equalsIgnoreCase("OtherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNo = FValue.trim();
			}
			else
				OtherNo = null;
		}
		if (FCode.equalsIgnoreCase("SequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SequenceNo = i;
			}
		}
		if (FCode.equalsIgnoreCase("OperateType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OperateType = FValue.trim();
			}
			else
				OperateType = null;
		}
		if (FCode.equalsIgnoreCase("OperateResult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OperateResult = FValue.trim();
			}
			else
				OperateResult = null;
		}
		if (FCode.equalsIgnoreCase("Content"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Content = FValue.trim();
			}
			else
				Content = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		BPOMissionDetailStateSchema other = (BPOMissionDetailStateSchema)otherObject;
		return
			MissionID.equals(other.getMissionID())
			&& SubMissionID.equals(other.getSubMissionID())
			&& ActivityID.equals(other.getActivityID())
			&& ProcessID.equals(other.getProcessID())
			&& BPOBatchNo.equals(other.getBPOBatchNo())
			&& BatchNo.equals(other.getBatchNo())
			&& ImportCount == other.getImportCount()
			&& DealCount == other.getDealCount()
			&& BussNoType.equals(other.getBussNoType())
			&& BussNo.equals(other.getBussNo())
			&& Managecom.equals(other.getManagecom())
			&& OthernoType.equals(other.getOthernoType())
			&& OtherNo.equals(other.getOtherNo())
			&& SequenceNo == other.getSequenceNo()
			&& OperateType.equals(other.getOperateType())
			&& OperateResult.equals(other.getOperateResult())
			&& Content.equals(other.getContent())
			&& Remark.equals(other.getRemark())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MissionID") ) {
			return 0;
		}
		if( strFieldName.equals("SubMissionID") ) {
			return 1;
		}
		if( strFieldName.equals("ActivityID") ) {
			return 2;
		}
		if( strFieldName.equals("ProcessID") ) {
			return 3;
		}
		if( strFieldName.equals("BPOBatchNo") ) {
			return 4;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 5;
		}
		if( strFieldName.equals("ImportCount") ) {
			return 6;
		}
		if( strFieldName.equals("DealCount") ) {
			return 7;
		}
		if( strFieldName.equals("BussNoType") ) {
			return 8;
		}
		if( strFieldName.equals("BussNo") ) {
			return 9;
		}
		if( strFieldName.equals("Managecom") ) {
			return 10;
		}
		if( strFieldName.equals("OthernoType") ) {
			return 11;
		}
		if( strFieldName.equals("OtherNo") ) {
			return 12;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return 13;
		}
		if( strFieldName.equals("OperateType") ) {
			return 14;
		}
		if( strFieldName.equals("OperateResult") ) {
			return 15;
		}
		if( strFieldName.equals("Content") ) {
			return 16;
		}
		if( strFieldName.equals("Remark") ) {
			return 17;
		}
		if( strFieldName.equals("Operator") ) {
			return 18;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 19;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 20;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 22;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MissionID";
				break;
			case 1:
				strFieldName = "SubMissionID";
				break;
			case 2:
				strFieldName = "ActivityID";
				break;
			case 3:
				strFieldName = "ProcessID";
				break;
			case 4:
				strFieldName = "BPOBatchNo";
				break;
			case 5:
				strFieldName = "BatchNo";
				break;
			case 6:
				strFieldName = "ImportCount";
				break;
			case 7:
				strFieldName = "DealCount";
				break;
			case 8:
				strFieldName = "BussNoType";
				break;
			case 9:
				strFieldName = "BussNo";
				break;
			case 10:
				strFieldName = "Managecom";
				break;
			case 11:
				strFieldName = "OthernoType";
				break;
			case 12:
				strFieldName = "OtherNo";
				break;
			case 13:
				strFieldName = "SequenceNo";
				break;
			case 14:
				strFieldName = "OperateType";
				break;
			case 15:
				strFieldName = "OperateResult";
				break;
			case 16:
				strFieldName = "Content";
				break;
			case 17:
				strFieldName = "Remark";
				break;
			case 18:
				strFieldName = "Operator";
				break;
			case 19:
				strFieldName = "MakeDate";
				break;
			case 20:
				strFieldName = "MakeTime";
				break;
			case 21:
				strFieldName = "ModifyDate";
				break;
			case 22:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MissionID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubMissionID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ActivityID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProcessID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BPOBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImportCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DealCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BussNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BussNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Managecom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthernoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OperateType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OperateResult") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Content") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_INT;
				break;
			case 7:
				nFieldType = Schema.TYPE_INT;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_INT;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
