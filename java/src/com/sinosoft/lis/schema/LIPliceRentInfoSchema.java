/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIPliceRentInfoDB;

/*
 * <p>ClassName: LIPliceRentInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 职场管理
 * @CreateDate：2012-03-30
 */
public class LIPliceRentInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 职场编码 */
	private String Placeno;
	/** 公司代码 */
	private String Managecom;
	/** 准租批文号 */
	private String Appno;
	/** 状态 */
	private String State;
	/** 公司级别 */
	private String Comlevel;
	/** 机构名称 */
	private String Comname;
	/** 租期起期 */
	private Date Begindate;
	/** 租期止期 */
	private Date Enddate;
	/** 实际租期止期 */
	private Date Actualenddate;
	/** 地址 */
	private String Address;
	/** 个险渠道面积 */
	private double Sigarea;
	/** 团险渠道面积 */
	private double Grparea;
	/** 银保渠道面积 */
	private double Bankarea;
	/** 健康管理面积 */
	private double Healtharea;
	/** 运营客服面积 */
	private double Servicearea;
	/** 后援面积 */
	private double Helparea;
	/** 联合办公面积 */
	private double Jointarea;
	/** 其他面积 */
	private double Otherarea;
	/** 保证金金额 */
	private double Marginfee;
	/** 保证金支付日期 */
	private Date Marginpaydate;
	/** 保证金退还日期 */
	private Date Marginbackdate;
	/** 合同约定违约金 */
	private double Penaltyfee;
	/** 资本化日期 */
	private Date Capdate;
	/** 备注 */
	private String Remake;
	/** 文件名称 */
	private String Filename;
	/** 文件路径 */
	private String Filepath;
	/** 职场操作状态 */
	private String Dealstate;
	/** 审批状态 */
	private String Appstate;
	/** 后续操作类型 */
	private String Nexttype;
	/** 后续相关职场号 */
	private String Nextplaceno;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date Makedate;
	/** 入机时间 */
	private String Maketime;
	/** 修改日期 */
	private Date Modifydate;
	/** 修改时间 */
	private String Modifytime;
	/** 备用字符串属性1 */
	private String Standbystring1;
	/** 备用字符串属性2 */
	private String Standbystring2;
	/** 备用字符串属性3 */
	private String Standbystring3;
	/** 备用数字属性1 */
	private double Standbynum1;
	/** 备用数字属性2 */
	private double Standbynum2;
	/** 备用数字属性3 */
	private double Standbynum3;
	/** 备用日期属性1 */
	private Date Standbydate1;
	/** 备用日期属性2 */
	private Date Standbydate2;
	/** 公共区域 */
	private double Publicarea;
	/** 计划财务部 */
	private double Planfinance;
	/** 人事行政部 */
	private double Departpersno;
	/** 总经理室 */
	private double Manageroffice;

	public static final int FIELDNUM = 47;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIPliceRentInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Placeno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LIPliceRentInfoSchema cloned = (LIPliceRentInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPlaceno()
	{
		return Placeno;
	}
	public void setPlaceno(String aPlaceno)
	{
		Placeno = aPlaceno;
	}
	public String getManagecom()
	{
		return Managecom;
	}
	public void setManagecom(String aManagecom)
	{
		Managecom = aManagecom;
	}
	public String getAppno()
	{
		return Appno;
	}
	public void setAppno(String aAppno)
	{
		Appno = aAppno;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getComlevel()
	{
		return Comlevel;
	}
	public void setComlevel(String aComlevel)
	{
		Comlevel = aComlevel;
	}
	public String getComname()
	{
		return Comname;
	}
	public void setComname(String aComname)
	{
		Comname = aComname;
	}
	public String getBegindate()
	{
		if( Begindate != null )
			return fDate.getString(Begindate);
		else
			return null;
	}
	public void setBegindate(Date aBegindate)
	{
		Begindate = aBegindate;
	}
	public void setBegindate(String aBegindate)
	{
		if (aBegindate != null && !aBegindate.equals("") )
		{
			Begindate = fDate.getDate( aBegindate );
		}
		else
			Begindate = null;
	}

	public String getEnddate()
	{
		if( Enddate != null )
			return fDate.getString(Enddate);
		else
			return null;
	}
	public void setEnddate(Date aEnddate)
	{
		Enddate = aEnddate;
	}
	public void setEnddate(String aEnddate)
	{
		if (aEnddate != null && !aEnddate.equals("") )
		{
			Enddate = fDate.getDate( aEnddate );
		}
		else
			Enddate = null;
	}

	public String getActualenddate()
	{
		if( Actualenddate != null )
			return fDate.getString(Actualenddate);
		else
			return null;
	}
	public void setActualenddate(Date aActualenddate)
	{
		Actualenddate = aActualenddate;
	}
	public void setActualenddate(String aActualenddate)
	{
		if (aActualenddate != null && !aActualenddate.equals("") )
		{
			Actualenddate = fDate.getDate( aActualenddate );
		}
		else
			Actualenddate = null;
	}

	public String getAddress()
	{
		return Address;
	}
	public void setAddress(String aAddress)
	{
		Address = aAddress;
	}
	public double getSigarea()
	{
		return Sigarea;
	}
	public void setSigarea(double aSigarea)
	{
		Sigarea = Arith.round(aSigarea,2);
	}
	public void setSigarea(String aSigarea)
	{
		if (aSigarea != null && !aSigarea.equals(""))
		{
			Double tDouble = new Double(aSigarea);
			double d = tDouble.doubleValue();
                Sigarea = Arith.round(d,2);
		}
	}

	public double getGrparea()
	{
		return Grparea;
	}
	public void setGrparea(double aGrparea)
	{
		Grparea = Arith.round(aGrparea,2);
	}
	public void setGrparea(String aGrparea)
	{
		if (aGrparea != null && !aGrparea.equals(""))
		{
			Double tDouble = new Double(aGrparea);
			double d = tDouble.doubleValue();
                Grparea = Arith.round(d,2);
		}
	}

	public double getBankarea()
	{
		return Bankarea;
	}
	public void setBankarea(double aBankarea)
	{
		Bankarea = Arith.round(aBankarea,2);
	}
	public void setBankarea(String aBankarea)
	{
		if (aBankarea != null && !aBankarea.equals(""))
		{
			Double tDouble = new Double(aBankarea);
			double d = tDouble.doubleValue();
                Bankarea = Arith.round(d,2);
		}
	}

	public double getHealtharea()
	{
		return Healtharea;
	}
	public void setHealtharea(double aHealtharea)
	{
		Healtharea = Arith.round(aHealtharea,2);
	}
	public void setHealtharea(String aHealtharea)
	{
		if (aHealtharea != null && !aHealtharea.equals(""))
		{
			Double tDouble = new Double(aHealtharea);
			double d = tDouble.doubleValue();
                Healtharea = Arith.round(d,2);
		}
	}

	public double getServicearea()
	{
		return Servicearea;
	}
	public void setServicearea(double aServicearea)
	{
		Servicearea = Arith.round(aServicearea,2);
	}
	public void setServicearea(String aServicearea)
	{
		if (aServicearea != null && !aServicearea.equals(""))
		{
			Double tDouble = new Double(aServicearea);
			double d = tDouble.doubleValue();
                Servicearea = Arith.round(d,2);
		}
	}

	public double getHelparea()
	{
		return Helparea;
	}
	public void setHelparea(double aHelparea)
	{
		Helparea = Arith.round(aHelparea,2);
	}
	public void setHelparea(String aHelparea)
	{
		if (aHelparea != null && !aHelparea.equals(""))
		{
			Double tDouble = new Double(aHelparea);
			double d = tDouble.doubleValue();
                Helparea = Arith.round(d,2);
		}
	}

	public double getJointarea()
	{
		return Jointarea;
	}
	public void setJointarea(double aJointarea)
	{
		Jointarea = Arith.round(aJointarea,2);
	}
	public void setJointarea(String aJointarea)
	{
		if (aJointarea != null && !aJointarea.equals(""))
		{
			Double tDouble = new Double(aJointarea);
			double d = tDouble.doubleValue();
                Jointarea = Arith.round(d,2);
		}
	}

	public double getOtherarea()
	{
		return Otherarea;
	}
	public void setOtherarea(double aOtherarea)
	{
		Otherarea = Arith.round(aOtherarea,2);
	}
	public void setOtherarea(String aOtherarea)
	{
		if (aOtherarea != null && !aOtherarea.equals(""))
		{
			Double tDouble = new Double(aOtherarea);
			double d = tDouble.doubleValue();
                Otherarea = Arith.round(d,2);
		}
	}

	public double getMarginfee()
	{
		return Marginfee;
	}
	public void setMarginfee(double aMarginfee)
	{
		Marginfee = Arith.round(aMarginfee,2);
	}
	public void setMarginfee(String aMarginfee)
	{
		if (aMarginfee != null && !aMarginfee.equals(""))
		{
			Double tDouble = new Double(aMarginfee);
			double d = tDouble.doubleValue();
                Marginfee = Arith.round(d,2);
		}
	}

	public String getMarginpaydate()
	{
		if( Marginpaydate != null )
			return fDate.getString(Marginpaydate);
		else
			return null;
	}
	public void setMarginpaydate(Date aMarginpaydate)
	{
		Marginpaydate = aMarginpaydate;
	}
	public void setMarginpaydate(String aMarginpaydate)
	{
		if (aMarginpaydate != null && !aMarginpaydate.equals("") )
		{
			Marginpaydate = fDate.getDate( aMarginpaydate );
		}
		else
			Marginpaydate = null;
	}

	public String getMarginbackdate()
	{
		if( Marginbackdate != null )
			return fDate.getString(Marginbackdate);
		else
			return null;
	}
	public void setMarginbackdate(Date aMarginbackdate)
	{
		Marginbackdate = aMarginbackdate;
	}
	public void setMarginbackdate(String aMarginbackdate)
	{
		if (aMarginbackdate != null && !aMarginbackdate.equals("") )
		{
			Marginbackdate = fDate.getDate( aMarginbackdate );
		}
		else
			Marginbackdate = null;
	}

	public double getPenaltyfee()
	{
		return Penaltyfee;
	}
	public void setPenaltyfee(double aPenaltyfee)
	{
		Penaltyfee = Arith.round(aPenaltyfee,2);
	}
	public void setPenaltyfee(String aPenaltyfee)
	{
		if (aPenaltyfee != null && !aPenaltyfee.equals(""))
		{
			Double tDouble = new Double(aPenaltyfee);
			double d = tDouble.doubleValue();
                Penaltyfee = Arith.round(d,2);
		}
	}

	public String getCapdate()
	{
		if( Capdate != null )
			return fDate.getString(Capdate);
		else
			return null;
	}
	public void setCapdate(Date aCapdate)
	{
		Capdate = aCapdate;
	}
	public void setCapdate(String aCapdate)
	{
		if (aCapdate != null && !aCapdate.equals("") )
		{
			Capdate = fDate.getDate( aCapdate );
		}
		else
			Capdate = null;
	}

	public String getRemake()
	{
		return Remake;
	}
	public void setRemake(String aRemake)
	{
		Remake = aRemake;
	}
	public String getFilename()
	{
		return Filename;
	}
	public void setFilename(String aFilename)
	{
		Filename = aFilename;
	}
	public String getFilepath()
	{
		return Filepath;
	}
	public void setFilepath(String aFilepath)
	{
		Filepath = aFilepath;
	}
	public String getDealstate()
	{
		return Dealstate;
	}
	public void setDealstate(String aDealstate)
	{
		Dealstate = aDealstate;
	}
	public String getAppstate()
	{
		return Appstate;
	}
	public void setAppstate(String aAppstate)
	{
		Appstate = aAppstate;
	}
	public String getNexttype()
	{
		return Nexttype;
	}
	public void setNexttype(String aNexttype)
	{
		Nexttype = aNexttype;
	}
	public String getNextplaceno()
	{
		return Nextplaceno;
	}
	public void setNextplaceno(String aNextplaceno)
	{
		Nextplaceno = aNextplaceno;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakedate()
	{
		if( Makedate != null )
			return fDate.getString(Makedate);
		else
			return null;
	}
	public void setMakedate(Date aMakedate)
	{
		Makedate = aMakedate;
	}
	public void setMakedate(String aMakedate)
	{
		if (aMakedate != null && !aMakedate.equals("") )
		{
			Makedate = fDate.getDate( aMakedate );
		}
		else
			Makedate = null;
	}

	public String getMaketime()
	{
		return Maketime;
	}
	public void setMaketime(String aMaketime)
	{
		Maketime = aMaketime;
	}
	public String getModifydate()
	{
		if( Modifydate != null )
			return fDate.getString(Modifydate);
		else
			return null;
	}
	public void setModifydate(Date aModifydate)
	{
		Modifydate = aModifydate;
	}
	public void setModifydate(String aModifydate)
	{
		if (aModifydate != null && !aModifydate.equals("") )
		{
			Modifydate = fDate.getDate( aModifydate );
		}
		else
			Modifydate = null;
	}

	public String getModifytime()
	{
		return Modifytime;
	}
	public void setModifytime(String aModifytime)
	{
		Modifytime = aModifytime;
	}
	public String getStandbystring1()
	{
		return Standbystring1;
	}
	public void setStandbystring1(String aStandbystring1)
	{
		Standbystring1 = aStandbystring1;
	}
	public String getStandbystring2()
	{
		return Standbystring2;
	}
	public void setStandbystring2(String aStandbystring2)
	{
		Standbystring2 = aStandbystring2;
	}
	public String getStandbystring3()
	{
		return Standbystring3;
	}
	public void setStandbystring3(String aStandbystring3)
	{
		Standbystring3 = aStandbystring3;
	}
	public double getStandbynum1()
	{
		return Standbynum1;
	}
	public void setStandbynum1(double aStandbynum1)
	{
		Standbynum1 = Arith.round(aStandbynum1,2);
	}
	public void setStandbynum1(String aStandbynum1)
	{
		if (aStandbynum1 != null && !aStandbynum1.equals(""))
		{
			Double tDouble = new Double(aStandbynum1);
			double d = tDouble.doubleValue();
                Standbynum1 = Arith.round(d,2);
		}
	}

	public double getStandbynum2()
	{
		return Standbynum2;
	}
	public void setStandbynum2(double aStandbynum2)
	{
		Standbynum2 = Arith.round(aStandbynum2,2);
	}
	public void setStandbynum2(String aStandbynum2)
	{
		if (aStandbynum2 != null && !aStandbynum2.equals(""))
		{
			Double tDouble = new Double(aStandbynum2);
			double d = tDouble.doubleValue();
                Standbynum2 = Arith.round(d,2);
		}
	}

	public double getStandbynum3()
	{
		return Standbynum3;
	}
	public void setStandbynum3(double aStandbynum3)
	{
		Standbynum3 = Arith.round(aStandbynum3,2);
	}
	public void setStandbynum3(String aStandbynum3)
	{
		if (aStandbynum3 != null && !aStandbynum3.equals(""))
		{
			Double tDouble = new Double(aStandbynum3);
			double d = tDouble.doubleValue();
                Standbynum3 = Arith.round(d,2);
		}
	}

	public String getStandbydate1()
	{
		if( Standbydate1 != null )
			return fDate.getString(Standbydate1);
		else
			return null;
	}
	public void setStandbydate1(Date aStandbydate1)
	{
		Standbydate1 = aStandbydate1;
	}
	public void setStandbydate1(String aStandbydate1)
	{
		if (aStandbydate1 != null && !aStandbydate1.equals("") )
		{
			Standbydate1 = fDate.getDate( aStandbydate1 );
		}
		else
			Standbydate1 = null;
	}

	public String getStandbydate2()
	{
		if( Standbydate2 != null )
			return fDate.getString(Standbydate2);
		else
			return null;
	}
	public void setStandbydate2(Date aStandbydate2)
	{
		Standbydate2 = aStandbydate2;
	}
	public void setStandbydate2(String aStandbydate2)
	{
		if (aStandbydate2 != null && !aStandbydate2.equals("") )
		{
			Standbydate2 = fDate.getDate( aStandbydate2 );
		}
		else
			Standbydate2 = null;
	}

	public double getPublicarea()
	{
		return Publicarea;
	}
	public void setPublicarea(double aPublicarea)
	{
		Publicarea = Arith.round(aPublicarea,2);
	}
	public void setPublicarea(String aPublicarea)
	{
		if (aPublicarea != null && !aPublicarea.equals(""))
		{
			Double tDouble = new Double(aPublicarea);
			double d = tDouble.doubleValue();
                Publicarea = Arith.round(d,2);
		}
	}

	public double getPlanfinance()
	{
		return Planfinance;
	}
	public void setPlanfinance(double aPlanfinance)
	{
		Planfinance = Arith.round(aPlanfinance,2);
	}
	public void setPlanfinance(String aPlanfinance)
	{
		if (aPlanfinance != null && !aPlanfinance.equals(""))
		{
			Double tDouble = new Double(aPlanfinance);
			double d = tDouble.doubleValue();
                Planfinance = Arith.round(d,2);
		}
	}

	public double getDepartpersno()
	{
		return Departpersno;
	}
	public void setDepartpersno(double aDepartpersno)
	{
		Departpersno = Arith.round(aDepartpersno,2);
	}
	public void setDepartpersno(String aDepartpersno)
	{
		if (aDepartpersno != null && !aDepartpersno.equals(""))
		{
			Double tDouble = new Double(aDepartpersno);
			double d = tDouble.doubleValue();
                Departpersno = Arith.round(d,2);
		}
	}

	public double getManageroffice()
	{
		return Manageroffice;
	}
	public void setManageroffice(double aManageroffice)
	{
		Manageroffice = Arith.round(aManageroffice,2);
	}
	public void setManageroffice(String aManageroffice)
	{
		if (aManageroffice != null && !aManageroffice.equals(""))
		{
			Double tDouble = new Double(aManageroffice);
			double d = tDouble.doubleValue();
                Manageroffice = Arith.round(d,2);
		}
	}


	/**
	* 使用另外一个 LIPliceRentInfoSchema 对象给 Schema 赋值
	* @param: aLIPliceRentInfoSchema LIPliceRentInfoSchema
	**/
	public void setSchema(LIPliceRentInfoSchema aLIPliceRentInfoSchema)
	{
		this.Placeno = aLIPliceRentInfoSchema.getPlaceno();
		this.Managecom = aLIPliceRentInfoSchema.getManagecom();
		this.Appno = aLIPliceRentInfoSchema.getAppno();
		this.State = aLIPliceRentInfoSchema.getState();
		this.Comlevel = aLIPliceRentInfoSchema.getComlevel();
		this.Comname = aLIPliceRentInfoSchema.getComname();
		this.Begindate = fDate.getDate( aLIPliceRentInfoSchema.getBegindate());
		this.Enddate = fDate.getDate( aLIPliceRentInfoSchema.getEnddate());
		this.Actualenddate = fDate.getDate( aLIPliceRentInfoSchema.getActualenddate());
		this.Address = aLIPliceRentInfoSchema.getAddress();
		this.Sigarea = aLIPliceRentInfoSchema.getSigarea();
		this.Grparea = aLIPliceRentInfoSchema.getGrparea();
		this.Bankarea = aLIPliceRentInfoSchema.getBankarea();
		this.Healtharea = aLIPliceRentInfoSchema.getHealtharea();
		this.Servicearea = aLIPliceRentInfoSchema.getServicearea();
		this.Helparea = aLIPliceRentInfoSchema.getHelparea();
		this.Jointarea = aLIPliceRentInfoSchema.getJointarea();
		this.Otherarea = aLIPliceRentInfoSchema.getOtherarea();
		this.Marginfee = aLIPliceRentInfoSchema.getMarginfee();
		this.Marginpaydate = fDate.getDate( aLIPliceRentInfoSchema.getMarginpaydate());
		this.Marginbackdate = fDate.getDate( aLIPliceRentInfoSchema.getMarginbackdate());
		this.Penaltyfee = aLIPliceRentInfoSchema.getPenaltyfee();
		this.Capdate = fDate.getDate( aLIPliceRentInfoSchema.getCapdate());
		this.Remake = aLIPliceRentInfoSchema.getRemake();
		this.Filename = aLIPliceRentInfoSchema.getFilename();
		this.Filepath = aLIPliceRentInfoSchema.getFilepath();
		this.Dealstate = aLIPliceRentInfoSchema.getDealstate();
		this.Appstate = aLIPliceRentInfoSchema.getAppstate();
		this.Nexttype = aLIPliceRentInfoSchema.getNexttype();
		this.Nextplaceno = aLIPliceRentInfoSchema.getNextplaceno();
		this.Operator = aLIPliceRentInfoSchema.getOperator();
		this.Makedate = fDate.getDate( aLIPliceRentInfoSchema.getMakedate());
		this.Maketime = aLIPliceRentInfoSchema.getMaketime();
		this.Modifydate = fDate.getDate( aLIPliceRentInfoSchema.getModifydate());
		this.Modifytime = aLIPliceRentInfoSchema.getModifytime();
		this.Standbystring1 = aLIPliceRentInfoSchema.getStandbystring1();
		this.Standbystring2 = aLIPliceRentInfoSchema.getStandbystring2();
		this.Standbystring3 = aLIPliceRentInfoSchema.getStandbystring3();
		this.Standbynum1 = aLIPliceRentInfoSchema.getStandbynum1();
		this.Standbynum2 = aLIPliceRentInfoSchema.getStandbynum2();
		this.Standbynum3 = aLIPliceRentInfoSchema.getStandbynum3();
		this.Standbydate1 = fDate.getDate( aLIPliceRentInfoSchema.getStandbydate1());
		this.Standbydate2 = fDate.getDate( aLIPliceRentInfoSchema.getStandbydate2());
		this.Publicarea = aLIPliceRentInfoSchema.getPublicarea();
		this.Planfinance = aLIPliceRentInfoSchema.getPlanfinance();
		this.Departpersno = aLIPliceRentInfoSchema.getDepartpersno();
		this.Manageroffice = aLIPliceRentInfoSchema.getManageroffice();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Placeno") == null )
				this.Placeno = null;
			else
				this.Placeno = rs.getString("Placeno").trim();

			if( rs.getString("Managecom") == null )
				this.Managecom = null;
			else
				this.Managecom = rs.getString("Managecom").trim();

			if( rs.getString("Appno") == null )
				this.Appno = null;
			else
				this.Appno = rs.getString("Appno").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Comlevel") == null )
				this.Comlevel = null;
			else
				this.Comlevel = rs.getString("Comlevel").trim();

			if( rs.getString("Comname") == null )
				this.Comname = null;
			else
				this.Comname = rs.getString("Comname").trim();

			this.Begindate = rs.getDate("Begindate");
			this.Enddate = rs.getDate("Enddate");
			this.Actualenddate = rs.getDate("Actualenddate");
			if( rs.getString("Address") == null )
				this.Address = null;
			else
				this.Address = rs.getString("Address").trim();

			this.Sigarea = rs.getDouble("Sigarea");
			this.Grparea = rs.getDouble("Grparea");
			this.Bankarea = rs.getDouble("Bankarea");
			this.Healtharea = rs.getDouble("Healtharea");
			this.Servicearea = rs.getDouble("Servicearea");
			this.Helparea = rs.getDouble("Helparea");
			this.Jointarea = rs.getDouble("Jointarea");
			this.Otherarea = rs.getDouble("Otherarea");
			this.Marginfee = rs.getDouble("Marginfee");
			this.Marginpaydate = rs.getDate("Marginpaydate");
			this.Marginbackdate = rs.getDate("Marginbackdate");
			this.Penaltyfee = rs.getDouble("Penaltyfee");
			this.Capdate = rs.getDate("Capdate");
			if( rs.getString("Remake") == null )
				this.Remake = null;
			else
				this.Remake = rs.getString("Remake").trim();

			if( rs.getString("Filename") == null )
				this.Filename = null;
			else
				this.Filename = rs.getString("Filename").trim();

			if( rs.getString("Filepath") == null )
				this.Filepath = null;
			else
				this.Filepath = rs.getString("Filepath").trim();

			if( rs.getString("Dealstate") == null )
				this.Dealstate = null;
			else
				this.Dealstate = rs.getString("Dealstate").trim();

			if( rs.getString("Appstate") == null )
				this.Appstate = null;
			else
				this.Appstate = rs.getString("Appstate").trim();

			if( rs.getString("Nexttype") == null )
				this.Nexttype = null;
			else
				this.Nexttype = rs.getString("Nexttype").trim();

			if( rs.getString("Nextplaceno") == null )
				this.Nextplaceno = null;
			else
				this.Nextplaceno = rs.getString("Nextplaceno").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.Makedate = rs.getDate("Makedate");
			if( rs.getString("Maketime") == null )
				this.Maketime = null;
			else
				this.Maketime = rs.getString("Maketime").trim();

			this.Modifydate = rs.getDate("Modifydate");
			if( rs.getString("Modifytime") == null )
				this.Modifytime = null;
			else
				this.Modifytime = rs.getString("Modifytime").trim();

			if( rs.getString("Standbystring1") == null )
				this.Standbystring1 = null;
			else
				this.Standbystring1 = rs.getString("Standbystring1").trim();

			if( rs.getString("Standbystring2") == null )
				this.Standbystring2 = null;
			else
				this.Standbystring2 = rs.getString("Standbystring2").trim();

			if( rs.getString("Standbystring3") == null )
				this.Standbystring3 = null;
			else
				this.Standbystring3 = rs.getString("Standbystring3").trim();

			this.Standbynum1 = rs.getDouble("Standbynum1");
			this.Standbynum2 = rs.getDouble("Standbynum2");
			this.Standbynum3 = rs.getDouble("Standbynum3");
			this.Standbydate1 = rs.getDate("Standbydate1");
			this.Standbydate2 = rs.getDate("Standbydate2");
			this.Publicarea = rs.getDouble("Publicarea");
			this.Planfinance = rs.getDouble("Planfinance");
			this.Departpersno = rs.getDouble("Departpersno");
			this.Manageroffice = rs.getDouble("Manageroffice");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIPliceRentInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIPliceRentInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIPliceRentInfoSchema getSchema()
	{
		LIPliceRentInfoSchema aLIPliceRentInfoSchema = new LIPliceRentInfoSchema();
		aLIPliceRentInfoSchema.setSchema(this);
		return aLIPliceRentInfoSchema;
	}

	public LIPliceRentInfoDB getDB()
	{
		LIPliceRentInfoDB aDBOper = new LIPliceRentInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIPliceRentInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Placeno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Managecom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Appno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Comlevel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Comname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Begindate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Enddate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Actualenddate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Address)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Sigarea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Grparea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Bankarea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Healtharea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Servicearea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Helparea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Jointarea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Otherarea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Marginfee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Marginpaydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Marginbackdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Penaltyfee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Capdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remake)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Filename)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Filepath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Dealstate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Appstate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Nexttype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Nextplaceno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Maketime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Modifydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Modifytime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Standbydate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Standbydate2 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Publicarea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Planfinance));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Departpersno));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Manageroffice));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIPliceRentInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Placeno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Appno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Comlevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Comname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Begindate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			Enddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			Actualenddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Sigarea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			Grparea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			Bankarea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			Healtharea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			Servicearea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			Helparea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			Jointarea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			Otherarea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			Marginfee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			Marginpaydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			Marginbackdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			Penaltyfee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			Capdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			Remake = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Filename = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Filepath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Dealstate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Appstate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			Nexttype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			Nextplaceno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			Makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,SysConst.PACKAGESPILTER));
			Maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			Modifydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,SysConst.PACKAGESPILTER));
			Modifytime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			Standbystring1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			Standbystring2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			Standbystring3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			Standbynum1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,39,SysConst.PACKAGESPILTER))).doubleValue();
			Standbynum2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,40,SysConst.PACKAGESPILTER))).doubleValue();
			Standbynum3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).doubleValue();
			Standbydate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,SysConst.PACKAGESPILTER));
			Standbydate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
			Publicarea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,44,SysConst.PACKAGESPILTER))).doubleValue();
			Planfinance = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,45,SysConst.PACKAGESPILTER))).doubleValue();
			Departpersno = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,46,SysConst.PACKAGESPILTER))).doubleValue();
			Manageroffice = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).doubleValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIPliceRentInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Placeno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Placeno));
		}
		if (FCode.equals("Managecom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
		}
		if (FCode.equals("Appno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Appno));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Comlevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Comlevel));
		}
		if (FCode.equals("Comname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Comname));
		}
		if (FCode.equals("Begindate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBegindate()));
		}
		if (FCode.equals("Enddate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnddate()));
		}
		if (FCode.equals("Actualenddate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getActualenddate()));
		}
		if (FCode.equals("Address"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
		}
		if (FCode.equals("Sigarea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sigarea));
		}
		if (FCode.equals("Grparea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Grparea));
		}
		if (FCode.equals("Bankarea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bankarea));
		}
		if (FCode.equals("Healtharea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Healtharea));
		}
		if (FCode.equals("Servicearea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Servicearea));
		}
		if (FCode.equals("Helparea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Helparea));
		}
		if (FCode.equals("Jointarea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Jointarea));
		}
		if (FCode.equals("Otherarea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Otherarea));
		}
		if (FCode.equals("Marginfee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Marginfee));
		}
		if (FCode.equals("Marginpaydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMarginpaydate()));
		}
		if (FCode.equals("Marginbackdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMarginbackdate()));
		}
		if (FCode.equals("Penaltyfee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Penaltyfee));
		}
		if (FCode.equals("Capdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCapdate()));
		}
		if (FCode.equals("Remake"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remake));
		}
		if (FCode.equals("Filename"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Filename));
		}
		if (FCode.equals("Filepath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Filepath));
		}
		if (FCode.equals("Dealstate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dealstate));
		}
		if (FCode.equals("Appstate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Appstate));
		}
		if (FCode.equals("Nexttype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Nexttype));
		}
		if (FCode.equals("Nextplaceno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Nextplaceno));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
		}
		if (FCode.equals("Maketime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Maketime));
		}
		if (FCode.equals("Modifydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifydate()));
		}
		if (FCode.equals("Modifytime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Modifytime));
		}
		if (FCode.equals("Standbystring1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring1));
		}
		if (FCode.equals("Standbystring2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring2));
		}
		if (FCode.equals("Standbystring3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring3));
		}
		if (FCode.equals("Standbynum1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum1));
		}
		if (FCode.equals("Standbynum2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum2));
		}
		if (FCode.equals("Standbynum3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum3));
		}
		if (FCode.equals("Standbydate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate1()));
		}
		if (FCode.equals("Standbydate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate2()));
		}
		if (FCode.equals("Publicarea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Publicarea));
		}
		if (FCode.equals("Planfinance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Planfinance));
		}
		if (FCode.equals("Departpersno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Departpersno));
		}
		if (FCode.equals("Manageroffice"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Manageroffice));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Placeno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Managecom);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Appno);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Comlevel);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Comname);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBegindate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnddate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getActualenddate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Address);
				break;
			case 10:
				strFieldValue = String.valueOf(Sigarea);
				break;
			case 11:
				strFieldValue = String.valueOf(Grparea);
				break;
			case 12:
				strFieldValue = String.valueOf(Bankarea);
				break;
			case 13:
				strFieldValue = String.valueOf(Healtharea);
				break;
			case 14:
				strFieldValue = String.valueOf(Servicearea);
				break;
			case 15:
				strFieldValue = String.valueOf(Helparea);
				break;
			case 16:
				strFieldValue = String.valueOf(Jointarea);
				break;
			case 17:
				strFieldValue = String.valueOf(Otherarea);
				break;
			case 18:
				strFieldValue = String.valueOf(Marginfee);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMarginpaydate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMarginbackdate()));
				break;
			case 21:
				strFieldValue = String.valueOf(Penaltyfee);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCapdate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(Remake);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Filename);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Filepath);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(Dealstate);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Appstate);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(Nexttype);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(Nextplaceno);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(Maketime);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifydate()));
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(Modifytime);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(Standbystring1);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(Standbystring2);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(Standbystring3);
				break;
			case 38:
				strFieldValue = String.valueOf(Standbynum1);
				break;
			case 39:
				strFieldValue = String.valueOf(Standbynum2);
				break;
			case 40:
				strFieldValue = String.valueOf(Standbynum3);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate1()));
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate2()));
				break;
			case 43:
				strFieldValue = String.valueOf(Publicarea);
				break;
			case 44:
				strFieldValue = String.valueOf(Planfinance);
				break;
			case 45:
				strFieldValue = String.valueOf(Departpersno);
				break;
			case 46:
				strFieldValue = String.valueOf(Manageroffice);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Placeno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Placeno = FValue.trim();
			}
			else
				Placeno = null;
		}
		if (FCode.equalsIgnoreCase("Managecom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Managecom = FValue.trim();
			}
			else
				Managecom = null;
		}
		if (FCode.equalsIgnoreCase("Appno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Appno = FValue.trim();
			}
			else
				Appno = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Comlevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Comlevel = FValue.trim();
			}
			else
				Comlevel = null;
		}
		if (FCode.equalsIgnoreCase("Comname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Comname = FValue.trim();
			}
			else
				Comname = null;
		}
		if (FCode.equalsIgnoreCase("Begindate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Begindate = fDate.getDate( FValue );
			}
			else
				Begindate = null;
		}
		if (FCode.equalsIgnoreCase("Enddate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Enddate = fDate.getDate( FValue );
			}
			else
				Enddate = null;
		}
		if (FCode.equalsIgnoreCase("Actualenddate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Actualenddate = fDate.getDate( FValue );
			}
			else
				Actualenddate = null;
		}
		if (FCode.equalsIgnoreCase("Address"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Address = FValue.trim();
			}
			else
				Address = null;
		}
		if (FCode.equalsIgnoreCase("Sigarea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Sigarea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Grparea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Grparea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Bankarea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Bankarea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Healtharea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Healtharea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Servicearea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Servicearea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Helparea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Helparea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Jointarea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Jointarea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Otherarea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Otherarea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Marginfee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Marginfee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Marginpaydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Marginpaydate = fDate.getDate( FValue );
			}
			else
				Marginpaydate = null;
		}
		if (FCode.equalsIgnoreCase("Marginbackdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Marginbackdate = fDate.getDate( FValue );
			}
			else
				Marginbackdate = null;
		}
		if (FCode.equalsIgnoreCase("Penaltyfee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Penaltyfee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Capdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Capdate = fDate.getDate( FValue );
			}
			else
				Capdate = null;
		}
		if (FCode.equalsIgnoreCase("Remake"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remake = FValue.trim();
			}
			else
				Remake = null;
		}
		if (FCode.equalsIgnoreCase("Filename"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Filename = FValue.trim();
			}
			else
				Filename = null;
		}
		if (FCode.equalsIgnoreCase("Filepath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Filepath = FValue.trim();
			}
			else
				Filepath = null;
		}
		if (FCode.equalsIgnoreCase("Dealstate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dealstate = FValue.trim();
			}
			else
				Dealstate = null;
		}
		if (FCode.equalsIgnoreCase("Appstate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Appstate = FValue.trim();
			}
			else
				Appstate = null;
		}
		if (FCode.equalsIgnoreCase("Nexttype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Nexttype = FValue.trim();
			}
			else
				Nexttype = null;
		}
		if (FCode.equalsIgnoreCase("Nextplaceno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Nextplaceno = FValue.trim();
			}
			else
				Nextplaceno = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Makedate = fDate.getDate( FValue );
			}
			else
				Makedate = null;
		}
		if (FCode.equalsIgnoreCase("Maketime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Maketime = FValue.trim();
			}
			else
				Maketime = null;
		}
		if (FCode.equalsIgnoreCase("Modifydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Modifydate = fDate.getDate( FValue );
			}
			else
				Modifydate = null;
		}
		if (FCode.equalsIgnoreCase("Modifytime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Modifytime = FValue.trim();
			}
			else
				Modifytime = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring1 = FValue.trim();
			}
			else
				Standbystring1 = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring2 = FValue.trim();
			}
			else
				Standbystring2 = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring3 = FValue.trim();
			}
			else
				Standbystring3 = null;
		}
		if (FCode.equalsIgnoreCase("Standbynum1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbynum2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbynum3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbydate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Standbydate1 = fDate.getDate( FValue );
			}
			else
				Standbydate1 = null;
		}
		if (FCode.equalsIgnoreCase("Standbydate2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Standbydate2 = fDate.getDate( FValue );
			}
			else
				Standbydate2 = null;
		}
		if (FCode.equalsIgnoreCase("Publicarea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Publicarea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Planfinance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Planfinance = d;
			}
		}
		if (FCode.equalsIgnoreCase("Departpersno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Departpersno = d;
			}
		}
		if (FCode.equalsIgnoreCase("Manageroffice"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Manageroffice = d;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIPliceRentInfoSchema other = (LIPliceRentInfoSchema)otherObject;
		return
			(Placeno == null ? other.getPlaceno() == null : Placeno.equals(other.getPlaceno()))
			&& (Managecom == null ? other.getManagecom() == null : Managecom.equals(other.getManagecom()))
			&& (Appno == null ? other.getAppno() == null : Appno.equals(other.getAppno()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Comlevel == null ? other.getComlevel() == null : Comlevel.equals(other.getComlevel()))
			&& (Comname == null ? other.getComname() == null : Comname.equals(other.getComname()))
			&& (Begindate == null ? other.getBegindate() == null : fDate.getString(Begindate).equals(other.getBegindate()))
			&& (Enddate == null ? other.getEnddate() == null : fDate.getString(Enddate).equals(other.getEnddate()))
			&& (Actualenddate == null ? other.getActualenddate() == null : fDate.getString(Actualenddate).equals(other.getActualenddate()))
			&& (Address == null ? other.getAddress() == null : Address.equals(other.getAddress()))
			&& Sigarea == other.getSigarea()
			&& Grparea == other.getGrparea()
			&& Bankarea == other.getBankarea()
			&& Healtharea == other.getHealtharea()
			&& Servicearea == other.getServicearea()
			&& Helparea == other.getHelparea()
			&& Jointarea == other.getJointarea()
			&& Otherarea == other.getOtherarea()
			&& Marginfee == other.getMarginfee()
			&& (Marginpaydate == null ? other.getMarginpaydate() == null : fDate.getString(Marginpaydate).equals(other.getMarginpaydate()))
			&& (Marginbackdate == null ? other.getMarginbackdate() == null : fDate.getString(Marginbackdate).equals(other.getMarginbackdate()))
			&& Penaltyfee == other.getPenaltyfee()
			&& (Capdate == null ? other.getCapdate() == null : fDate.getString(Capdate).equals(other.getCapdate()))
			&& (Remake == null ? other.getRemake() == null : Remake.equals(other.getRemake()))
			&& (Filename == null ? other.getFilename() == null : Filename.equals(other.getFilename()))
			&& (Filepath == null ? other.getFilepath() == null : Filepath.equals(other.getFilepath()))
			&& (Dealstate == null ? other.getDealstate() == null : Dealstate.equals(other.getDealstate()))
			&& (Appstate == null ? other.getAppstate() == null : Appstate.equals(other.getAppstate()))
			&& (Nexttype == null ? other.getNexttype() == null : Nexttype.equals(other.getNexttype()))
			&& (Nextplaceno == null ? other.getNextplaceno() == null : Nextplaceno.equals(other.getNextplaceno()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (Makedate == null ? other.getMakedate() == null : fDate.getString(Makedate).equals(other.getMakedate()))
			&& (Maketime == null ? other.getMaketime() == null : Maketime.equals(other.getMaketime()))
			&& (Modifydate == null ? other.getModifydate() == null : fDate.getString(Modifydate).equals(other.getModifydate()))
			&& (Modifytime == null ? other.getModifytime() == null : Modifytime.equals(other.getModifytime()))
			&& (Standbystring1 == null ? other.getStandbystring1() == null : Standbystring1.equals(other.getStandbystring1()))
			&& (Standbystring2 == null ? other.getStandbystring2() == null : Standbystring2.equals(other.getStandbystring2()))
			&& (Standbystring3 == null ? other.getStandbystring3() == null : Standbystring3.equals(other.getStandbystring3()))
			&& Standbynum1 == other.getStandbynum1()
			&& Standbynum2 == other.getStandbynum2()
			&& Standbynum3 == other.getStandbynum3()
			&& (Standbydate1 == null ? other.getStandbydate1() == null : fDate.getString(Standbydate1).equals(other.getStandbydate1()))
			&& (Standbydate2 == null ? other.getStandbydate2() == null : fDate.getString(Standbydate2).equals(other.getStandbydate2()))
			&& Publicarea == other.getPublicarea()
			&& Planfinance == other.getPlanfinance()
			&& Departpersno == other.getDepartpersno()
			&& Manageroffice == other.getManageroffice();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Placeno") ) {
			return 0;
		}
		if( strFieldName.equals("Managecom") ) {
			return 1;
		}
		if( strFieldName.equals("Appno") ) {
			return 2;
		}
		if( strFieldName.equals("State") ) {
			return 3;
		}
		if( strFieldName.equals("Comlevel") ) {
			return 4;
		}
		if( strFieldName.equals("Comname") ) {
			return 5;
		}
		if( strFieldName.equals("Begindate") ) {
			return 6;
		}
		if( strFieldName.equals("Enddate") ) {
			return 7;
		}
		if( strFieldName.equals("Actualenddate") ) {
			return 8;
		}
		if( strFieldName.equals("Address") ) {
			return 9;
		}
		if( strFieldName.equals("Sigarea") ) {
			return 10;
		}
		if( strFieldName.equals("Grparea") ) {
			return 11;
		}
		if( strFieldName.equals("Bankarea") ) {
			return 12;
		}
		if( strFieldName.equals("Healtharea") ) {
			return 13;
		}
		if( strFieldName.equals("Servicearea") ) {
			return 14;
		}
		if( strFieldName.equals("Helparea") ) {
			return 15;
		}
		if( strFieldName.equals("Jointarea") ) {
			return 16;
		}
		if( strFieldName.equals("Otherarea") ) {
			return 17;
		}
		if( strFieldName.equals("Marginfee") ) {
			return 18;
		}
		if( strFieldName.equals("Marginpaydate") ) {
			return 19;
		}
		if( strFieldName.equals("Marginbackdate") ) {
			return 20;
		}
		if( strFieldName.equals("Penaltyfee") ) {
			return 21;
		}
		if( strFieldName.equals("Capdate") ) {
			return 22;
		}
		if( strFieldName.equals("Remake") ) {
			return 23;
		}
		if( strFieldName.equals("Filename") ) {
			return 24;
		}
		if( strFieldName.equals("Filepath") ) {
			return 25;
		}
		if( strFieldName.equals("Dealstate") ) {
			return 26;
		}
		if( strFieldName.equals("Appstate") ) {
			return 27;
		}
		if( strFieldName.equals("Nexttype") ) {
			return 28;
		}
		if( strFieldName.equals("Nextplaceno") ) {
			return 29;
		}
		if( strFieldName.equals("Operator") ) {
			return 30;
		}
		if( strFieldName.equals("Makedate") ) {
			return 31;
		}
		if( strFieldName.equals("Maketime") ) {
			return 32;
		}
		if( strFieldName.equals("Modifydate") ) {
			return 33;
		}
		if( strFieldName.equals("Modifytime") ) {
			return 34;
		}
		if( strFieldName.equals("Standbystring1") ) {
			return 35;
		}
		if( strFieldName.equals("Standbystring2") ) {
			return 36;
		}
		if( strFieldName.equals("Standbystring3") ) {
			return 37;
		}
		if( strFieldName.equals("Standbynum1") ) {
			return 38;
		}
		if( strFieldName.equals("Standbynum2") ) {
			return 39;
		}
		if( strFieldName.equals("Standbynum3") ) {
			return 40;
		}
		if( strFieldName.equals("Standbydate1") ) {
			return 41;
		}
		if( strFieldName.equals("Standbydate2") ) {
			return 42;
		}
		if( strFieldName.equals("Publicarea") ) {
			return 43;
		}
		if( strFieldName.equals("Planfinance") ) {
			return 44;
		}
		if( strFieldName.equals("Departpersno") ) {
			return 45;
		}
		if( strFieldName.equals("Manageroffice") ) {
			return 46;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Placeno";
				break;
			case 1:
				strFieldName = "Managecom";
				break;
			case 2:
				strFieldName = "Appno";
				break;
			case 3:
				strFieldName = "State";
				break;
			case 4:
				strFieldName = "Comlevel";
				break;
			case 5:
				strFieldName = "Comname";
				break;
			case 6:
				strFieldName = "Begindate";
				break;
			case 7:
				strFieldName = "Enddate";
				break;
			case 8:
				strFieldName = "Actualenddate";
				break;
			case 9:
				strFieldName = "Address";
				break;
			case 10:
				strFieldName = "Sigarea";
				break;
			case 11:
				strFieldName = "Grparea";
				break;
			case 12:
				strFieldName = "Bankarea";
				break;
			case 13:
				strFieldName = "Healtharea";
				break;
			case 14:
				strFieldName = "Servicearea";
				break;
			case 15:
				strFieldName = "Helparea";
				break;
			case 16:
				strFieldName = "Jointarea";
				break;
			case 17:
				strFieldName = "Otherarea";
				break;
			case 18:
				strFieldName = "Marginfee";
				break;
			case 19:
				strFieldName = "Marginpaydate";
				break;
			case 20:
				strFieldName = "Marginbackdate";
				break;
			case 21:
				strFieldName = "Penaltyfee";
				break;
			case 22:
				strFieldName = "Capdate";
				break;
			case 23:
				strFieldName = "Remake";
				break;
			case 24:
				strFieldName = "Filename";
				break;
			case 25:
				strFieldName = "Filepath";
				break;
			case 26:
				strFieldName = "Dealstate";
				break;
			case 27:
				strFieldName = "Appstate";
				break;
			case 28:
				strFieldName = "Nexttype";
				break;
			case 29:
				strFieldName = "Nextplaceno";
				break;
			case 30:
				strFieldName = "Operator";
				break;
			case 31:
				strFieldName = "Makedate";
				break;
			case 32:
				strFieldName = "Maketime";
				break;
			case 33:
				strFieldName = "Modifydate";
				break;
			case 34:
				strFieldName = "Modifytime";
				break;
			case 35:
				strFieldName = "Standbystring1";
				break;
			case 36:
				strFieldName = "Standbystring2";
				break;
			case 37:
				strFieldName = "Standbystring3";
				break;
			case 38:
				strFieldName = "Standbynum1";
				break;
			case 39:
				strFieldName = "Standbynum2";
				break;
			case 40:
				strFieldName = "Standbynum3";
				break;
			case 41:
				strFieldName = "Standbydate1";
				break;
			case 42:
				strFieldName = "Standbydate2";
				break;
			case 43:
				strFieldName = "Publicarea";
				break;
			case 44:
				strFieldName = "Planfinance";
				break;
			case 45:
				strFieldName = "Departpersno";
				break;
			case 46:
				strFieldName = "Manageroffice";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Placeno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Managecom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Appno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Comlevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Comname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Begindate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Enddate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Actualenddate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Address") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sigarea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Grparea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Bankarea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Healtharea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Servicearea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Helparea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Jointarea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Otherarea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Marginfee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Marginpaydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Marginbackdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Penaltyfee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Capdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Remake") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Filename") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Filepath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Dealstate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Appstate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Nexttype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Nextplaceno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Maketime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Modifydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Modifytime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbynum1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbynum2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbynum3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbydate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Standbydate2") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Publicarea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Planfinance") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Departpersno") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Manageroffice") ) {
			return Schema.TYPE_DOUBLE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 39:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 40:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 41:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 43:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 44:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 45:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 46:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
