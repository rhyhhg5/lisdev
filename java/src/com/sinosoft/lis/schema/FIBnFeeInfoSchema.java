/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIBnFeeInfoDB;

/*
 * <p>ClassName: FIBnFeeInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIBnFeeInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 版本编号 */
	private String VersionNo;
	/** 费用编码 */
	private String CostID;
	/** 信息编码 */
	private String PropertyCode;
	/** 信息名称 */
	private String PropertyName;
	/** 分类维度 */
	private String BusClass;
	/** 分类名称 */
	private String BusClassName;
	/** 用途 */
	private String Purpose;
	/** 值域 */
	private String FeeValues;
	/** 说明 */
	private String Remark;

	public static final int FIELDNUM = 9;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIBnFeeInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "VersionNo";
		pk[1] = "CostID";
		pk[2] = "PropertyCode";
		pk[3] = "Purpose";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIBnFeeInfoSchema cloned = (FIBnFeeInfoSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getCostID()
	{
		return CostID;
	}
	public void setCostID(String aCostID)
	{
		CostID = aCostID;
	}
	public String getPropertyCode()
	{
		return PropertyCode;
	}
	public void setPropertyCode(String aPropertyCode)
	{
		PropertyCode = aPropertyCode;
	}
	public String getPropertyName()
	{
		return PropertyName;
	}
	public void setPropertyName(String aPropertyName)
	{
		PropertyName = aPropertyName;
	}
	public String getBusClass()
	{
		return BusClass;
	}
	public void setBusClass(String aBusClass)
	{
		BusClass = aBusClass;
	}
	public String getBusClassName()
	{
		return BusClassName;
	}
	public void setBusClassName(String aBusClassName)
	{
		BusClassName = aBusClassName;
	}
	public String getPurpose()
	{
		return Purpose;
	}
	public void setPurpose(String aPurpose)
	{
		Purpose = aPurpose;
	}
	public String getFeeValues()
	{
		return FeeValues;
	}
	public void setFeeValues(String aFeeValues)
	{
		FeeValues = aFeeValues;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}

	/**
	* 使用另外一个 FIBnFeeInfoSchema 对象给 Schema 赋值
	* @param: aFIBnFeeInfoSchema FIBnFeeInfoSchema
	**/
	public void setSchema(FIBnFeeInfoSchema aFIBnFeeInfoSchema)
	{
		this.VersionNo = aFIBnFeeInfoSchema.getVersionNo();
		this.CostID = aFIBnFeeInfoSchema.getCostID();
		this.PropertyCode = aFIBnFeeInfoSchema.getPropertyCode();
		this.PropertyName = aFIBnFeeInfoSchema.getPropertyName();
		this.BusClass = aFIBnFeeInfoSchema.getBusClass();
		this.BusClassName = aFIBnFeeInfoSchema.getBusClassName();
		this.Purpose = aFIBnFeeInfoSchema.getPurpose();
		this.FeeValues = aFIBnFeeInfoSchema.getFeeValues();
		this.Remark = aFIBnFeeInfoSchema.getRemark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("CostID") == null )
				this.CostID = null;
			else
				this.CostID = rs.getString("CostID").trim();

			if( rs.getString("PropertyCode") == null )
				this.PropertyCode = null;
			else
				this.PropertyCode = rs.getString("PropertyCode").trim();

			if( rs.getString("PropertyName") == null )
				this.PropertyName = null;
			else
				this.PropertyName = rs.getString("PropertyName").trim();

			if( rs.getString("BusClass") == null )
				this.BusClass = null;
			else
				this.BusClass = rs.getString("BusClass").trim();

			if( rs.getString("BusClassName") == null )
				this.BusClassName = null;
			else
				this.BusClassName = rs.getString("BusClassName").trim();

			if( rs.getString("Purpose") == null )
				this.Purpose = null;
			else
				this.Purpose = rs.getString("Purpose").trim();

			if( rs.getString("FeeValues") == null )
				this.FeeValues = null;
			else
				this.FeeValues = rs.getString("FeeValues").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIBnFeeInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIBnFeeInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIBnFeeInfoSchema getSchema()
	{
		FIBnFeeInfoSchema aFIBnFeeInfoSchema = new FIBnFeeInfoSchema();
		aFIBnFeeInfoSchema.setSchema(this);
		return aFIBnFeeInfoSchema;
	}

	public FIBnFeeInfoDB getDB()
	{
		FIBnFeeInfoDB aDBOper = new FIBnFeeInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIBnFeeInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PropertyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PropertyName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusClass)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusClassName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Purpose)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeValues)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIBnFeeInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CostID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			PropertyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PropertyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BusClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BusClassName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Purpose = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			FeeValues = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIBnFeeInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("CostID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostID));
		}
		if (FCode.equals("PropertyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PropertyCode));
		}
		if (FCode.equals("PropertyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PropertyName));
		}
		if (FCode.equals("BusClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusClass));
		}
		if (FCode.equals("BusClassName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusClassName));
		}
		if (FCode.equals("Purpose"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Purpose));
		}
		if (FCode.equals("FeeValues"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeValues));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CostID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(PropertyCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PropertyName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BusClass);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BusClassName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Purpose);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(FeeValues);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("CostID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostID = FValue.trim();
			}
			else
				CostID = null;
		}
		if (FCode.equalsIgnoreCase("PropertyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PropertyCode = FValue.trim();
			}
			else
				PropertyCode = null;
		}
		if (FCode.equalsIgnoreCase("PropertyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PropertyName = FValue.trim();
			}
			else
				PropertyName = null;
		}
		if (FCode.equalsIgnoreCase("BusClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusClass = FValue.trim();
			}
			else
				BusClass = null;
		}
		if (FCode.equalsIgnoreCase("BusClassName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusClassName = FValue.trim();
			}
			else
				BusClassName = null;
		}
		if (FCode.equalsIgnoreCase("Purpose"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Purpose = FValue.trim();
			}
			else
				Purpose = null;
		}
		if (FCode.equalsIgnoreCase("FeeValues"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeValues = FValue.trim();
			}
			else
				FeeValues = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIBnFeeInfoSchema other = (FIBnFeeInfoSchema)otherObject;
		return
			(VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (CostID == null ? other.getCostID() == null : CostID.equals(other.getCostID()))
			&& (PropertyCode == null ? other.getPropertyCode() == null : PropertyCode.equals(other.getPropertyCode()))
			&& (PropertyName == null ? other.getPropertyName() == null : PropertyName.equals(other.getPropertyName()))
			&& (BusClass == null ? other.getBusClass() == null : BusClass.equals(other.getBusClass()))
			&& (BusClassName == null ? other.getBusClassName() == null : BusClassName.equals(other.getBusClassName()))
			&& (Purpose == null ? other.getPurpose() == null : Purpose.equals(other.getPurpose()))
			&& (FeeValues == null ? other.getFeeValues() == null : FeeValues.equals(other.getFeeValues()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return 0;
		}
		if( strFieldName.equals("CostID") ) {
			return 1;
		}
		if( strFieldName.equals("PropertyCode") ) {
			return 2;
		}
		if( strFieldName.equals("PropertyName") ) {
			return 3;
		}
		if( strFieldName.equals("BusClass") ) {
			return 4;
		}
		if( strFieldName.equals("BusClassName") ) {
			return 5;
		}
		if( strFieldName.equals("Purpose") ) {
			return 6;
		}
		if( strFieldName.equals("FeeValues") ) {
			return 7;
		}
		if( strFieldName.equals("Remark") ) {
			return 8;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "VersionNo";
				break;
			case 1:
				strFieldName = "CostID";
				break;
			case 2:
				strFieldName = "PropertyCode";
				break;
			case 3:
				strFieldName = "PropertyName";
				break;
			case 4:
				strFieldName = "BusClass";
				break;
			case 5:
				strFieldName = "BusClassName";
				break;
			case 6:
				strFieldName = "Purpose";
				break;
			case 7:
				strFieldName = "FeeValues";
				break;
			case 8:
				strFieldName = "Remark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PropertyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PropertyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusClassName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Purpose") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeValues") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
