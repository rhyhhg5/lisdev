/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCGrpContDB;

/*
 * <p>ClassName: LCGrpContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2012-11-02
 */
public class LCGrpContSchema implements Schema, Cloneable
{
	// @Field
	/** 集体合同号码 */
	private String GrpContNo;
	/** 集体投保单号码 */
	private String ProposalGrpContNo;
	/** 印刷号码 */
	private String PrtNo;
	/** 销售渠道 */
	private String SaleChnl;
	/** 管理机构 */
	private String ManageCom;
	/** 代理机构 */
	private String AgentCom;
	/** 代理机构内部分类 */
	private String AgentType;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人组别 */
	private String AgentGroup;
	/** 联合代理人代码 */
	private String AgentCode1;
	/** 保单口令 */
	private String Password;
	/** 密码 */
	private String Password2;
	/** 客户号码 */
	private String AppntNo;
	/** 地址号码 */
	private String AddressNo;
	/** 投保总人数 */
	private int Peoples2;
	/** 单位名称 */
	private String GrpName;
	/** 行业分类 */
	private String BusinessType;
	/** 单位性质 */
	private String GrpNature;
	/** 注册资本 */
	private double RgtMoney;
	/** 资产总额 */
	private double Asset;
	/** 净资产收益率 */
	private double NetProfitRate;
	/** 主营业务 */
	private String MainBussiness;
	/** 法人 */
	private String Corporation;
	/** 机构分布区域 */
	private String ComAera;
	/** 单位传真 */
	private String Fax;
	/** 单位电话 */
	private String Phone;
	/** 付款方式 */
	private String GetFlag;
	/** 负责人 */
	private String Satrap;
	/** 公司e_mail */
	private String EMail;
	/** 成立日期 */
	private Date FoundDate;
	/** 客户组号码 */
	private String GrpGroupNo;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐号 */
	private String BankAccNo;
	/** 银行帐户名 */
	private String AccName;
	/** 合同争议处理方式 */
	private String DisputedFlag;
	/** 溢交处理方式 */
	private String OutPayFlag;
	/** 保单送达方式 */
	private String GetPolMode;
	/** 语种标记 */
	private String Lang;
	/** 币别 */
	private String Currency;
	/** 遗失补发次数 */
	private int LostTimes;
	/** 保单打印次数 */
	private int PrintCount;
	/** 最后一次催收日期 */
	private Date RegetDate;
	/** 最后一次保全日期 */
	private Date LastEdorDate;
	/** 最后一次给付日期 */
	private Date LastGetDate;
	/** 最后一次借款日期 */
	private Date LastLoanDate;
	/** 团体特殊业务标志 */
	private String SpecFlag;
	/** 集体特约 */
	private String GrpSpec;
	/** 交费方式 */
	private String PayMode;
	/** 签单机构 */
	private String SignCom;
	/** 签单日期 */
	private Date SignDate;
	/** 签单时间 */
	private String SignTime;
	/** 保单生效日期 */
	private Date CValiDate;
	/** 交费间隔 */
	private int PayIntv;
	/** 管理费比例 */
	private double ManageFeeRate;
	/** 预计人数 */
	private int ExpPeoples;
	/** 预计保费 */
	private double ExpPremium;
	/** 预计保额 */
	private double ExpAmnt;
	/** 总人数 */
	private int Peoples;
	/** 总档次 */
	private double Mult;
	/** 总保费 */
	private double Prem;
	/** 总保额 */
	private double Amnt;
	/** 总累计保费 */
	private double SumPrem;
	/** 总累计交费 */
	private double SumPay;
	/** 差额 */
	private double Dif;
	/** 备注 */
	private String Remark;
	/** 备用属性字段1 */
	private String StandbyFlag1;
	/** 备用属性字段2 */
	private String StandbyFlag2;
	/** 备用属性字段3 */
	private String StandbyFlag3;
	/** 录单人 */
	private String InputOperator;
	/** 录单完成日期 */
	private Date InputDate;
	/** 录单完成时间 */
	private String InputTime;
	/** 复核状态 */
	private String ApproveFlag;
	/** 复核人编码 */
	private String ApproveCode;
	/** 复核日期 */
	private Date ApproveDate;
	/** 复核时间 */
	private String ApproveTime;
	/** 核保人 */
	private String UWOperator;
	/** 核保状态 */
	private String UWFlag;
	/** 核保完成日期 */
	private Date UWDate;
	/** 核保完成时间 */
	private String UWTime;
	/** 投保单/保单标志 */
	private String AppFlag;
	/** 投保单申请日期 */
	private Date PolApplyDate;
	/** 保单回执客户签收日期 */
	private Date CustomGetPolDate;
	/** 保单送达日期 */
	private Date GetPolDate;
	/** 保单送达时间 */
	private String GetPolTime;
	/** 状态 */
	private String State;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 参保形式 */
	private String EnterKind;
	/** 保额等级 */
	private String AmntGrade;
	/** 单位可投保人数 */
	private int Peoples3;
	/** 在职投保人数 */
	private int OnWorkPeoples;
	/** 退休投保人数 */
	private int OffWorkPeoples;
	/** 其它投保人数 */
	private int OtherPeoples;
	/** 连带投保人数 */
	private int RelaPeoples;
	/** 连带配偶投保人数 */
	private int RelaMatePeoples;
	/** 连带子女投保人数 */
	private int RelaYoungPeoples;
	/** 连带其它投保人数 */
	private int RelaOtherPeoples;
	/** 初审人 */
	private String FirstTrialOperator;
	/** 初审日期 */
	private Date FirstTrialDate;
	/** 初审时间 */
	private String FirstTrialTime;
	/** 收单人 */
	private String ReceiveOperator;
	/** 收单日期 */
	private Date ReceiveDate;
	/** 收单时间 */
	private String ReceiveTime;
	/** 暂收据号 */
	private String TempFeeNo;
	/** 投保经办人 */
	private String HandlerName;
	/** 投保单填写日期 */
	private Date HandlerDate;
	/** 投保单位章 */
	private String HandlerPrint;
	/** 业务员填写日期 */
	private Date AgentDate;
	/** 行业大类 */
	private String BusinessBigType;
	/** 市场类型 */
	private String MarketType;
	/** 投保书类型 */
	private String ProposalType;
	/** 销售渠道明细 */
	private String SaleChnlDetail;
	/** 合同打印位置标记 */
	private String ContPrintLoFlag;
	/** 保费分摊原则 */
	private String PremApportFlag;
	/** 保费收据号 */
	private String ContPremFeeNo;
	/** 客户回执号 */
	private String CustomerReceiptNo;
	/** 合同终止日期 */
	private Date CInValiDate;
	/** 渠道经理 */
	private String RoleAgentCode;
	/** 询价合同号 */
	private String AskGrpContNo;
	/** 总份数 */
	private double Copys;
	/** 业务主管签字 */
	private String OperationManager;
	/** 信息来源 */
	private String InfoSource;
	/** 资金规模 */
	private double PremScope;
	/** 投保次数类型 */
	private String DegreeType;
	/** 卡单标志 */
	private String CardFlag;
	/** 大项目标识 */
	private String BigProjectFlag;
	/** 保单打印类型 */
	private String ContPrintType;
	/** 保单状态 */
	private String StateFlag;
	/** 集团代理机构 */
	private String GrpAgentCom;
	/** 集团代理人 */
	private String GrpAgentCode;
	/** 集团代理人姓名 */
	private String GrpAgentName;
	/** 开办市县数 */
	private int CTCount;
	/** 共保保单标志 */
	private String CoInsuranceFlag;
	/** 开办市县内容 */
	private String CityInfo;
	/** 集团交叉销售渠道 */
	private String Crs_SaleChnl;
	/** 集团销售业务类型 */
	private String Crs_BussType;
	/** 集团代理人身份证 */
	private String GrpAgentIDNo;
	/** 代理销售业务员编码 */
	private String AgentSaleCode;
	/** 业务经办人身份证号 */
	private String HandlerIDNo;

	public static final int FIELDNUM = 142;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCGrpContSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "GrpContNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCGrpContSchema cloned = (LCGrpContSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getProposalGrpContNo()
	{
		return ProposalGrpContNo;
	}
	public void setProposalGrpContNo(String aProposalGrpContNo)
	{
		ProposalGrpContNo = aProposalGrpContNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentType()
	{
		return AgentType;
	}
	public void setAgentType(String aAgentType)
	{
		AgentType = aAgentType;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getAgentCode1()
	{
		return AgentCode1;
	}
	public void setAgentCode1(String aAgentCode1)
	{
		AgentCode1 = aAgentCode1;
	}
	public String getPassword()
	{
		return Password;
	}
	public void setPassword(String aPassword)
	{
		Password = aPassword;
	}
	public String getPassword2()
	{
		return Password2;
	}
	public void setPassword2(String aPassword2)
	{
		Password2 = aPassword2;
	}
	public String getAppntNo()
	{
		return AppntNo;
	}
	public void setAppntNo(String aAppntNo)
	{
		AppntNo = aAppntNo;
	}
	public String getAddressNo()
	{
		return AddressNo;
	}
	public void setAddressNo(String aAddressNo)
	{
		AddressNo = aAddressNo;
	}
	public int getPeoples2()
	{
		return Peoples2;
	}
	public void setPeoples2(int aPeoples2)
	{
		Peoples2 = aPeoples2;
	}
	public void setPeoples2(String aPeoples2)
	{
		if (aPeoples2 != null && !aPeoples2.equals(""))
		{
			Integer tInteger = new Integer(aPeoples2);
			int i = tInteger.intValue();
			Peoples2 = i;
		}
	}

	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getBusinessType()
	{
		return BusinessType;
	}
	public void setBusinessType(String aBusinessType)
	{
		BusinessType = aBusinessType;
	}
	public String getGrpNature()
	{
		return GrpNature;
	}
	public void setGrpNature(String aGrpNature)
	{
		GrpNature = aGrpNature;
	}
	public double getRgtMoney()
	{
		return RgtMoney;
	}
	public void setRgtMoney(double aRgtMoney)
	{
		RgtMoney = Arith.round(aRgtMoney,2);
	}
	public void setRgtMoney(String aRgtMoney)
	{
		if (aRgtMoney != null && !aRgtMoney.equals(""))
		{
			Double tDouble = new Double(aRgtMoney);
			double d = tDouble.doubleValue();
                RgtMoney = Arith.round(d,2);
		}
	}

	public double getAsset()
	{
		return Asset;
	}
	public void setAsset(double aAsset)
	{
		Asset = Arith.round(aAsset,2);
	}
	public void setAsset(String aAsset)
	{
		if (aAsset != null && !aAsset.equals(""))
		{
			Double tDouble = new Double(aAsset);
			double d = tDouble.doubleValue();
                Asset = Arith.round(d,2);
		}
	}

	public double getNetProfitRate()
	{
		return NetProfitRate;
	}
	public void setNetProfitRate(double aNetProfitRate)
	{
		NetProfitRate = Arith.round(aNetProfitRate,4);
	}
	public void setNetProfitRate(String aNetProfitRate)
	{
		if (aNetProfitRate != null && !aNetProfitRate.equals(""))
		{
			Double tDouble = new Double(aNetProfitRate);
			double d = tDouble.doubleValue();
                NetProfitRate = Arith.round(d,4);
		}
	}

	public String getMainBussiness()
	{
		return MainBussiness;
	}
	public void setMainBussiness(String aMainBussiness)
	{
		MainBussiness = aMainBussiness;
	}
	public String getCorporation()
	{
		return Corporation;
	}
	public void setCorporation(String aCorporation)
	{
		Corporation = aCorporation;
	}
	public String getComAera()
	{
		return ComAera;
	}
	public void setComAera(String aComAera)
	{
		ComAera = aComAera;
	}
	public String getFax()
	{
		return Fax;
	}
	public void setFax(String aFax)
	{
		Fax = aFax;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getGetFlag()
	{
		return GetFlag;
	}
	public void setGetFlag(String aGetFlag)
	{
		GetFlag = aGetFlag;
	}
	public String getSatrap()
	{
		return Satrap;
	}
	public void setSatrap(String aSatrap)
	{
		Satrap = aSatrap;
	}
	public String getEMail()
	{
		return EMail;
	}
	public void setEMail(String aEMail)
	{
		EMail = aEMail;
	}
	public String getFoundDate()
	{
		if( FoundDate != null )
			return fDate.getString(FoundDate);
		else
			return null;
	}
	public void setFoundDate(Date aFoundDate)
	{
		FoundDate = aFoundDate;
	}
	public void setFoundDate(String aFoundDate)
	{
		if (aFoundDate != null && !aFoundDate.equals("") )
		{
			FoundDate = fDate.getDate( aFoundDate );
		}
		else
			FoundDate = null;
	}

	public String getGrpGroupNo()
	{
		return GrpGroupNo;
	}
	public void setGrpGroupNo(String aGrpGroupNo)
	{
		GrpGroupNo = aGrpGroupNo;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getDisputedFlag()
	{
		return DisputedFlag;
	}
	public void setDisputedFlag(String aDisputedFlag)
	{
		DisputedFlag = aDisputedFlag;
	}
	public String getOutPayFlag()
	{
		return OutPayFlag;
	}
	public void setOutPayFlag(String aOutPayFlag)
	{
		OutPayFlag = aOutPayFlag;
	}
	public String getGetPolMode()
	{
		return GetPolMode;
	}
	public void setGetPolMode(String aGetPolMode)
	{
		GetPolMode = aGetPolMode;
	}
	public String getLang()
	{
		return Lang;
	}
	public void setLang(String aLang)
	{
		Lang = aLang;
	}
	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}
	public int getLostTimes()
	{
		return LostTimes;
	}
	public void setLostTimes(int aLostTimes)
	{
		LostTimes = aLostTimes;
	}
	public void setLostTimes(String aLostTimes)
	{
		if (aLostTimes != null && !aLostTimes.equals(""))
		{
			Integer tInteger = new Integer(aLostTimes);
			int i = tInteger.intValue();
			LostTimes = i;
		}
	}

	public int getPrintCount()
	{
		return PrintCount;
	}
	public void setPrintCount(int aPrintCount)
	{
		PrintCount = aPrintCount;
	}
	public void setPrintCount(String aPrintCount)
	{
		if (aPrintCount != null && !aPrintCount.equals(""))
		{
			Integer tInteger = new Integer(aPrintCount);
			int i = tInteger.intValue();
			PrintCount = i;
		}
	}

	public String getRegetDate()
	{
		if( RegetDate != null )
			return fDate.getString(RegetDate);
		else
			return null;
	}
	public void setRegetDate(Date aRegetDate)
	{
		RegetDate = aRegetDate;
	}
	public void setRegetDate(String aRegetDate)
	{
		if (aRegetDate != null && !aRegetDate.equals("") )
		{
			RegetDate = fDate.getDate( aRegetDate );
		}
		else
			RegetDate = null;
	}

	public String getLastEdorDate()
	{
		if( LastEdorDate != null )
			return fDate.getString(LastEdorDate);
		else
			return null;
	}
	public void setLastEdorDate(Date aLastEdorDate)
	{
		LastEdorDate = aLastEdorDate;
	}
	public void setLastEdorDate(String aLastEdorDate)
	{
		if (aLastEdorDate != null && !aLastEdorDate.equals("") )
		{
			LastEdorDate = fDate.getDate( aLastEdorDate );
		}
		else
			LastEdorDate = null;
	}

	public String getLastGetDate()
	{
		if( LastGetDate != null )
			return fDate.getString(LastGetDate);
		else
			return null;
	}
	public void setLastGetDate(Date aLastGetDate)
	{
		LastGetDate = aLastGetDate;
	}
	public void setLastGetDate(String aLastGetDate)
	{
		if (aLastGetDate != null && !aLastGetDate.equals("") )
		{
			LastGetDate = fDate.getDate( aLastGetDate );
		}
		else
			LastGetDate = null;
	}

	public String getLastLoanDate()
	{
		if( LastLoanDate != null )
			return fDate.getString(LastLoanDate);
		else
			return null;
	}
	public void setLastLoanDate(Date aLastLoanDate)
	{
		LastLoanDate = aLastLoanDate;
	}
	public void setLastLoanDate(String aLastLoanDate)
	{
		if (aLastLoanDate != null && !aLastLoanDate.equals("") )
		{
			LastLoanDate = fDate.getDate( aLastLoanDate );
		}
		else
			LastLoanDate = null;
	}

	public String getSpecFlag()
	{
		return SpecFlag;
	}
	public void setSpecFlag(String aSpecFlag)
	{
		SpecFlag = aSpecFlag;
	}
	public String getGrpSpec()
	{
		return GrpSpec;
	}
	public void setGrpSpec(String aGrpSpec)
	{
		GrpSpec = aGrpSpec;
	}
	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getSignCom()
	{
		return SignCom;
	}
	public void setSignCom(String aSignCom)
	{
		SignCom = aSignCom;
	}
	public String getSignDate()
	{
		if( SignDate != null )
			return fDate.getString(SignDate);
		else
			return null;
	}
	public void setSignDate(Date aSignDate)
	{
		SignDate = aSignDate;
	}
	public void setSignDate(String aSignDate)
	{
		if (aSignDate != null && !aSignDate.equals("") )
		{
			SignDate = fDate.getDate( aSignDate );
		}
		else
			SignDate = null;
	}

	public String getSignTime()
	{
		return SignTime;
	}
	public void setSignTime(String aSignTime)
	{
		SignTime = aSignTime;
	}
	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public int getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(int aPayIntv)
	{
		PayIntv = aPayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
		if (aPayIntv != null && !aPayIntv.equals(""))
		{
			Integer tInteger = new Integer(aPayIntv);
			int i = tInteger.intValue();
			PayIntv = i;
		}
	}

	public double getManageFeeRate()
	{
		return ManageFeeRate;
	}
	public void setManageFeeRate(double aManageFeeRate)
	{
		ManageFeeRate = Arith.round(aManageFeeRate,0);
	}
	public void setManageFeeRate(String aManageFeeRate)
	{
		if (aManageFeeRate != null && !aManageFeeRate.equals(""))
		{
			Double tDouble = new Double(aManageFeeRate);
			double d = tDouble.doubleValue();
                ManageFeeRate = Arith.round(d,0);
		}
	}

	public int getExpPeoples()
	{
		return ExpPeoples;
	}
	public void setExpPeoples(int aExpPeoples)
	{
		ExpPeoples = aExpPeoples;
	}
	public void setExpPeoples(String aExpPeoples)
	{
		if (aExpPeoples != null && !aExpPeoples.equals(""))
		{
			Integer tInteger = new Integer(aExpPeoples);
			int i = tInteger.intValue();
			ExpPeoples = i;
		}
	}

	public double getExpPremium()
	{
		return ExpPremium;
	}
	public void setExpPremium(double aExpPremium)
	{
		ExpPremium = Arith.round(aExpPremium,2);
	}
	public void setExpPremium(String aExpPremium)
	{
		if (aExpPremium != null && !aExpPremium.equals(""))
		{
			Double tDouble = new Double(aExpPremium);
			double d = tDouble.doubleValue();
                ExpPremium = Arith.round(d,2);
		}
	}

	public double getExpAmnt()
	{
		return ExpAmnt;
	}
	public void setExpAmnt(double aExpAmnt)
	{
		ExpAmnt = Arith.round(aExpAmnt,2);
	}
	public void setExpAmnt(String aExpAmnt)
	{
		if (aExpAmnt != null && !aExpAmnt.equals(""))
		{
			Double tDouble = new Double(aExpAmnt);
			double d = tDouble.doubleValue();
                ExpAmnt = Arith.round(d,2);
		}
	}

	public int getPeoples()
	{
		return Peoples;
	}
	public void setPeoples(int aPeoples)
	{
		Peoples = aPeoples;
	}
	public void setPeoples(String aPeoples)
	{
		if (aPeoples != null && !aPeoples.equals(""))
		{
			Integer tInteger = new Integer(aPeoples);
			int i = tInteger.intValue();
			Peoples = i;
		}
	}

	public double getMult()
	{
		return Mult;
	}
	public void setMult(double aMult)
	{
		Mult = Arith.round(aMult,5);
	}
	public void setMult(String aMult)
	{
		if (aMult != null && !aMult.equals(""))
		{
			Double tDouble = new Double(aMult);
			double d = tDouble.doubleValue();
                Mult = Arith.round(d,5);
		}
	}

	public double getPrem()
	{
		return Prem;
	}
	public void setPrem(double aPrem)
	{
		Prem = Arith.round(aPrem,2);
	}
	public void setPrem(String aPrem)
	{
		if (aPrem != null && !aPrem.equals(""))
		{
			Double tDouble = new Double(aPrem);
			double d = tDouble.doubleValue();
                Prem = Arith.round(d,2);
		}
	}

	public double getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(double aAmnt)
	{
		Amnt = Arith.round(aAmnt,2);
	}
	public void setAmnt(String aAmnt)
	{
		if (aAmnt != null && !aAmnt.equals(""))
		{
			Double tDouble = new Double(aAmnt);
			double d = tDouble.doubleValue();
                Amnt = Arith.round(d,2);
		}
	}

	public double getSumPrem()
	{
		return SumPrem;
	}
	public void setSumPrem(double aSumPrem)
	{
		SumPrem = Arith.round(aSumPrem,2);
	}
	public void setSumPrem(String aSumPrem)
	{
		if (aSumPrem != null && !aSumPrem.equals(""))
		{
			Double tDouble = new Double(aSumPrem);
			double d = tDouble.doubleValue();
                SumPrem = Arith.round(d,2);
		}
	}

	public double getSumPay()
	{
		return SumPay;
	}
	public void setSumPay(double aSumPay)
	{
		SumPay = Arith.round(aSumPay,2);
	}
	public void setSumPay(String aSumPay)
	{
		if (aSumPay != null && !aSumPay.equals(""))
		{
			Double tDouble = new Double(aSumPay);
			double d = tDouble.doubleValue();
                SumPay = Arith.round(d,2);
		}
	}

	public double getDif()
	{
		return Dif;
	}
	public void setDif(double aDif)
	{
		Dif = Arith.round(aDif,2);
	}
	public void setDif(String aDif)
	{
		if (aDif != null && !aDif.equals(""))
		{
			Double tDouble = new Double(aDif);
			double d = tDouble.doubleValue();
                Dif = Arith.round(d,2);
		}
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getStandbyFlag1()
	{
		return StandbyFlag1;
	}
	public void setStandbyFlag1(String aStandbyFlag1)
	{
		StandbyFlag1 = aStandbyFlag1;
	}
	public String getStandbyFlag2()
	{
		return StandbyFlag2;
	}
	public void setStandbyFlag2(String aStandbyFlag2)
	{
		StandbyFlag2 = aStandbyFlag2;
	}
	public String getStandbyFlag3()
	{
		return StandbyFlag3;
	}
	public void setStandbyFlag3(String aStandbyFlag3)
	{
		StandbyFlag3 = aStandbyFlag3;
	}
	public String getInputOperator()
	{
		return InputOperator;
	}
	public void setInputOperator(String aInputOperator)
	{
		InputOperator = aInputOperator;
	}
	public String getInputDate()
	{
		if( InputDate != null )
			return fDate.getString(InputDate);
		else
			return null;
	}
	public void setInputDate(Date aInputDate)
	{
		InputDate = aInputDate;
	}
	public void setInputDate(String aInputDate)
	{
		if (aInputDate != null && !aInputDate.equals("") )
		{
			InputDate = fDate.getDate( aInputDate );
		}
		else
			InputDate = null;
	}

	public String getInputTime()
	{
		return InputTime;
	}
	public void setInputTime(String aInputTime)
	{
		InputTime = aInputTime;
	}
	public String getApproveFlag()
	{
		return ApproveFlag;
	}
	public void setApproveFlag(String aApproveFlag)
	{
		ApproveFlag = aApproveFlag;
	}
	public String getApproveCode()
	{
		return ApproveCode;
	}
	public void setApproveCode(String aApproveCode)
	{
		ApproveCode = aApproveCode;
	}
	public String getApproveDate()
	{
		if( ApproveDate != null )
			return fDate.getString(ApproveDate);
		else
			return null;
	}
	public void setApproveDate(Date aApproveDate)
	{
		ApproveDate = aApproveDate;
	}
	public void setApproveDate(String aApproveDate)
	{
		if (aApproveDate != null && !aApproveDate.equals("") )
		{
			ApproveDate = fDate.getDate( aApproveDate );
		}
		else
			ApproveDate = null;
	}

	public String getApproveTime()
	{
		return ApproveTime;
	}
	public void setApproveTime(String aApproveTime)
	{
		ApproveTime = aApproveTime;
	}
	public String getUWOperator()
	{
		return UWOperator;
	}
	public void setUWOperator(String aUWOperator)
	{
		UWOperator = aUWOperator;
	}
	public String getUWFlag()
	{
		return UWFlag;
	}
	public void setUWFlag(String aUWFlag)
	{
		UWFlag = aUWFlag;
	}
	public String getUWDate()
	{
		if( UWDate != null )
			return fDate.getString(UWDate);
		else
			return null;
	}
	public void setUWDate(Date aUWDate)
	{
		UWDate = aUWDate;
	}
	public void setUWDate(String aUWDate)
	{
		if (aUWDate != null && !aUWDate.equals("") )
		{
			UWDate = fDate.getDate( aUWDate );
		}
		else
			UWDate = null;
	}

	public String getUWTime()
	{
		return UWTime;
	}
	public void setUWTime(String aUWTime)
	{
		UWTime = aUWTime;
	}
	public String getAppFlag()
	{
		return AppFlag;
	}
	public void setAppFlag(String aAppFlag)
	{
		AppFlag = aAppFlag;
	}
	public String getPolApplyDate()
	{
		if( PolApplyDate != null )
			return fDate.getString(PolApplyDate);
		else
			return null;
	}
	public void setPolApplyDate(Date aPolApplyDate)
	{
		PolApplyDate = aPolApplyDate;
	}
	public void setPolApplyDate(String aPolApplyDate)
	{
		if (aPolApplyDate != null && !aPolApplyDate.equals("") )
		{
			PolApplyDate = fDate.getDate( aPolApplyDate );
		}
		else
			PolApplyDate = null;
	}

	public String getCustomGetPolDate()
	{
		if( CustomGetPolDate != null )
			return fDate.getString(CustomGetPolDate);
		else
			return null;
	}
	public void setCustomGetPolDate(Date aCustomGetPolDate)
	{
		CustomGetPolDate = aCustomGetPolDate;
	}
	public void setCustomGetPolDate(String aCustomGetPolDate)
	{
		if (aCustomGetPolDate != null && !aCustomGetPolDate.equals("") )
		{
			CustomGetPolDate = fDate.getDate( aCustomGetPolDate );
		}
		else
			CustomGetPolDate = null;
	}

	public String getGetPolDate()
	{
		if( GetPolDate != null )
			return fDate.getString(GetPolDate);
		else
			return null;
	}
	public void setGetPolDate(Date aGetPolDate)
	{
		GetPolDate = aGetPolDate;
	}
	public void setGetPolDate(String aGetPolDate)
	{
		if (aGetPolDate != null && !aGetPolDate.equals("") )
		{
			GetPolDate = fDate.getDate( aGetPolDate );
		}
		else
			GetPolDate = null;
	}

	public String getGetPolTime()
	{
		return GetPolTime;
	}
	public void setGetPolTime(String aGetPolTime)
	{
		GetPolTime = aGetPolTime;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getEnterKind()
	{
		return EnterKind;
	}
	public void setEnterKind(String aEnterKind)
	{
		EnterKind = aEnterKind;
	}
	public String getAmntGrade()
	{
		return AmntGrade;
	}
	public void setAmntGrade(String aAmntGrade)
	{
		AmntGrade = aAmntGrade;
	}
	public int getPeoples3()
	{
		return Peoples3;
	}
	public void setPeoples3(int aPeoples3)
	{
		Peoples3 = aPeoples3;
	}
	public void setPeoples3(String aPeoples3)
	{
		if (aPeoples3 != null && !aPeoples3.equals(""))
		{
			Integer tInteger = new Integer(aPeoples3);
			int i = tInteger.intValue();
			Peoples3 = i;
		}
	}

	public int getOnWorkPeoples()
	{
		return OnWorkPeoples;
	}
	public void setOnWorkPeoples(int aOnWorkPeoples)
	{
		OnWorkPeoples = aOnWorkPeoples;
	}
	public void setOnWorkPeoples(String aOnWorkPeoples)
	{
		if (aOnWorkPeoples != null && !aOnWorkPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOnWorkPeoples);
			int i = tInteger.intValue();
			OnWorkPeoples = i;
		}
	}

	public int getOffWorkPeoples()
	{
		return OffWorkPeoples;
	}
	public void setOffWorkPeoples(int aOffWorkPeoples)
	{
		OffWorkPeoples = aOffWorkPeoples;
	}
	public void setOffWorkPeoples(String aOffWorkPeoples)
	{
		if (aOffWorkPeoples != null && !aOffWorkPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOffWorkPeoples);
			int i = tInteger.intValue();
			OffWorkPeoples = i;
		}
	}

	public int getOtherPeoples()
	{
		return OtherPeoples;
	}
	public void setOtherPeoples(int aOtherPeoples)
	{
		OtherPeoples = aOtherPeoples;
	}
	public void setOtherPeoples(String aOtherPeoples)
	{
		if (aOtherPeoples != null && !aOtherPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOtherPeoples);
			int i = tInteger.intValue();
			OtherPeoples = i;
		}
	}

	public int getRelaPeoples()
	{
		return RelaPeoples;
	}
	public void setRelaPeoples(int aRelaPeoples)
	{
		RelaPeoples = aRelaPeoples;
	}
	public void setRelaPeoples(String aRelaPeoples)
	{
		if (aRelaPeoples != null && !aRelaPeoples.equals(""))
		{
			Integer tInteger = new Integer(aRelaPeoples);
			int i = tInteger.intValue();
			RelaPeoples = i;
		}
	}

	public int getRelaMatePeoples()
	{
		return RelaMatePeoples;
	}
	public void setRelaMatePeoples(int aRelaMatePeoples)
	{
		RelaMatePeoples = aRelaMatePeoples;
	}
	public void setRelaMatePeoples(String aRelaMatePeoples)
	{
		if (aRelaMatePeoples != null && !aRelaMatePeoples.equals(""))
		{
			Integer tInteger = new Integer(aRelaMatePeoples);
			int i = tInteger.intValue();
			RelaMatePeoples = i;
		}
	}

	public int getRelaYoungPeoples()
	{
		return RelaYoungPeoples;
	}
	public void setRelaYoungPeoples(int aRelaYoungPeoples)
	{
		RelaYoungPeoples = aRelaYoungPeoples;
	}
	public void setRelaYoungPeoples(String aRelaYoungPeoples)
	{
		if (aRelaYoungPeoples != null && !aRelaYoungPeoples.equals(""))
		{
			Integer tInteger = new Integer(aRelaYoungPeoples);
			int i = tInteger.intValue();
			RelaYoungPeoples = i;
		}
	}

	public int getRelaOtherPeoples()
	{
		return RelaOtherPeoples;
	}
	public void setRelaOtherPeoples(int aRelaOtherPeoples)
	{
		RelaOtherPeoples = aRelaOtherPeoples;
	}
	public void setRelaOtherPeoples(String aRelaOtherPeoples)
	{
		if (aRelaOtherPeoples != null && !aRelaOtherPeoples.equals(""))
		{
			Integer tInteger = new Integer(aRelaOtherPeoples);
			int i = tInteger.intValue();
			RelaOtherPeoples = i;
		}
	}

	public String getFirstTrialOperator()
	{
		return FirstTrialOperator;
	}
	public void setFirstTrialOperator(String aFirstTrialOperator)
	{
		FirstTrialOperator = aFirstTrialOperator;
	}
	public String getFirstTrialDate()
	{
		if( FirstTrialDate != null )
			return fDate.getString(FirstTrialDate);
		else
			return null;
	}
	public void setFirstTrialDate(Date aFirstTrialDate)
	{
		FirstTrialDate = aFirstTrialDate;
	}
	public void setFirstTrialDate(String aFirstTrialDate)
	{
		if (aFirstTrialDate != null && !aFirstTrialDate.equals("") )
		{
			FirstTrialDate = fDate.getDate( aFirstTrialDate );
		}
		else
			FirstTrialDate = null;
	}

	public String getFirstTrialTime()
	{
		return FirstTrialTime;
	}
	public void setFirstTrialTime(String aFirstTrialTime)
	{
		FirstTrialTime = aFirstTrialTime;
	}
	public String getReceiveOperator()
	{
		return ReceiveOperator;
	}
	public void setReceiveOperator(String aReceiveOperator)
	{
		ReceiveOperator = aReceiveOperator;
	}
	public String getReceiveDate()
	{
		if( ReceiveDate != null )
			return fDate.getString(ReceiveDate);
		else
			return null;
	}
	public void setReceiveDate(Date aReceiveDate)
	{
		ReceiveDate = aReceiveDate;
	}
	public void setReceiveDate(String aReceiveDate)
	{
		if (aReceiveDate != null && !aReceiveDate.equals("") )
		{
			ReceiveDate = fDate.getDate( aReceiveDate );
		}
		else
			ReceiveDate = null;
	}

	public String getReceiveTime()
	{
		return ReceiveTime;
	}
	public void setReceiveTime(String aReceiveTime)
	{
		ReceiveTime = aReceiveTime;
	}
	public String getTempFeeNo()
	{
		return TempFeeNo;
	}
	public void setTempFeeNo(String aTempFeeNo)
	{
		TempFeeNo = aTempFeeNo;
	}
	public String getHandlerName()
	{
		return HandlerName;
	}
	public void setHandlerName(String aHandlerName)
	{
		HandlerName = aHandlerName;
	}
	public String getHandlerDate()
	{
		if( HandlerDate != null )
			return fDate.getString(HandlerDate);
		else
			return null;
	}
	public void setHandlerDate(Date aHandlerDate)
	{
		HandlerDate = aHandlerDate;
	}
	public void setHandlerDate(String aHandlerDate)
	{
		if (aHandlerDate != null && !aHandlerDate.equals("") )
		{
			HandlerDate = fDate.getDate( aHandlerDate );
		}
		else
			HandlerDate = null;
	}

	public String getHandlerPrint()
	{
		return HandlerPrint;
	}
	public void setHandlerPrint(String aHandlerPrint)
	{
		HandlerPrint = aHandlerPrint;
	}
	public String getAgentDate()
	{
		if( AgentDate != null )
			return fDate.getString(AgentDate);
		else
			return null;
	}
	public void setAgentDate(Date aAgentDate)
	{
		AgentDate = aAgentDate;
	}
	public void setAgentDate(String aAgentDate)
	{
		if (aAgentDate != null && !aAgentDate.equals("") )
		{
			AgentDate = fDate.getDate( aAgentDate );
		}
		else
			AgentDate = null;
	}

	public String getBusinessBigType()
	{
		return BusinessBigType;
	}
	public void setBusinessBigType(String aBusinessBigType)
	{
		BusinessBigType = aBusinessBigType;
	}
	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getProposalType()
	{
		return ProposalType;
	}
	public void setProposalType(String aProposalType)
	{
		ProposalType = aProposalType;
	}
	public String getSaleChnlDetail()
	{
		return SaleChnlDetail;
	}
	public void setSaleChnlDetail(String aSaleChnlDetail)
	{
		SaleChnlDetail = aSaleChnlDetail;
	}
	public String getContPrintLoFlag()
	{
		return ContPrintLoFlag;
	}
	public void setContPrintLoFlag(String aContPrintLoFlag)
	{
		ContPrintLoFlag = aContPrintLoFlag;
	}
	public String getPremApportFlag()
	{
		return PremApportFlag;
	}
	public void setPremApportFlag(String aPremApportFlag)
	{
		PremApportFlag = aPremApportFlag;
	}
	public String getContPremFeeNo()
	{
		return ContPremFeeNo;
	}
	public void setContPremFeeNo(String aContPremFeeNo)
	{
		ContPremFeeNo = aContPremFeeNo;
	}
	public String getCustomerReceiptNo()
	{
		return CustomerReceiptNo;
	}
	public void setCustomerReceiptNo(String aCustomerReceiptNo)
	{
		CustomerReceiptNo = aCustomerReceiptNo;
	}
	public String getCInValiDate()
	{
		if( CInValiDate != null )
			return fDate.getString(CInValiDate);
		else
			return null;
	}
	public void setCInValiDate(Date aCInValiDate)
	{
		CInValiDate = aCInValiDate;
	}
	public void setCInValiDate(String aCInValiDate)
	{
		if (aCInValiDate != null && !aCInValiDate.equals("") )
		{
			CInValiDate = fDate.getDate( aCInValiDate );
		}
		else
			CInValiDate = null;
	}

	public String getRoleAgentCode()
	{
		return RoleAgentCode;
	}
	public void setRoleAgentCode(String aRoleAgentCode)
	{
		RoleAgentCode = aRoleAgentCode;
	}
	public String getAskGrpContNo()
	{
		return AskGrpContNo;
	}
	public void setAskGrpContNo(String aAskGrpContNo)
	{
		AskGrpContNo = aAskGrpContNo;
	}
	public double getCopys()
	{
		return Copys;
	}
	public void setCopys(double aCopys)
	{
		Copys = Arith.round(aCopys,5);
	}
	public void setCopys(String aCopys)
	{
		if (aCopys != null && !aCopys.equals(""))
		{
			Double tDouble = new Double(aCopys);
			double d = tDouble.doubleValue();
                Copys = Arith.round(d,5);
		}
	}

	public String getOperationManager()
	{
		return OperationManager;
	}
	public void setOperationManager(String aOperationManager)
	{
		OperationManager = aOperationManager;
	}
	public String getInfoSource()
	{
		return InfoSource;
	}
	public void setInfoSource(String aInfoSource)
	{
		InfoSource = aInfoSource;
	}
	public double getPremScope()
	{
		return PremScope;
	}
	public void setPremScope(double aPremScope)
	{
		PremScope = Arith.round(aPremScope,2);
	}
	public void setPremScope(String aPremScope)
	{
		if (aPremScope != null && !aPremScope.equals(""))
		{
			Double tDouble = new Double(aPremScope);
			double d = tDouble.doubleValue();
                PremScope = Arith.round(d,2);
		}
	}

	public String getDegreeType()
	{
		return DegreeType;
	}
	public void setDegreeType(String aDegreeType)
	{
		DegreeType = aDegreeType;
	}
	public String getCardFlag()
	{
		return CardFlag;
	}
	public void setCardFlag(String aCardFlag)
	{
		CardFlag = aCardFlag;
	}
	public String getBigProjectFlag()
	{
		return BigProjectFlag;
	}
	public void setBigProjectFlag(String aBigProjectFlag)
	{
		BigProjectFlag = aBigProjectFlag;
	}
	public String getContPrintType()
	{
		return ContPrintType;
	}
	public void setContPrintType(String aContPrintType)
	{
		ContPrintType = aContPrintType;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public String getGrpAgentCom()
	{
		return GrpAgentCom;
	}
	public void setGrpAgentCom(String aGrpAgentCom)
	{
		GrpAgentCom = aGrpAgentCom;
	}
	public String getGrpAgentCode()
	{
		return GrpAgentCode;
	}
	public void setGrpAgentCode(String aGrpAgentCode)
	{
		GrpAgentCode = aGrpAgentCode;
	}
	public String getGrpAgentName()
	{
		return GrpAgentName;
	}
	public void setGrpAgentName(String aGrpAgentName)
	{
		GrpAgentName = aGrpAgentName;
	}
	public int getCTCount()
	{
		return CTCount;
	}
	public void setCTCount(int aCTCount)
	{
		CTCount = aCTCount;
	}
	public void setCTCount(String aCTCount)
	{
		if (aCTCount != null && !aCTCount.equals(""))
		{
			Integer tInteger = new Integer(aCTCount);
			int i = tInteger.intValue();
			CTCount = i;
		}
	}

	public String getCoInsuranceFlag()
	{
		return CoInsuranceFlag;
	}
	public void setCoInsuranceFlag(String aCoInsuranceFlag)
	{
		CoInsuranceFlag = aCoInsuranceFlag;
	}
	public String getCityInfo()
	{
		return CityInfo;
	}
	public void setCityInfo(String aCityInfo)
	{
		CityInfo = aCityInfo;
	}
	public String getCrs_SaleChnl()
	{
		return Crs_SaleChnl;
	}
	public void setCrs_SaleChnl(String aCrs_SaleChnl)
	{
		Crs_SaleChnl = aCrs_SaleChnl;
	}
	public String getCrs_BussType()
	{
		return Crs_BussType;
	}
	public void setCrs_BussType(String aCrs_BussType)
	{
		Crs_BussType = aCrs_BussType;
	}
	public String getGrpAgentIDNo()
	{
		return GrpAgentIDNo;
	}
	public void setGrpAgentIDNo(String aGrpAgentIDNo)
	{
		GrpAgentIDNo = aGrpAgentIDNo;
	}
	public String getAgentSaleCode()
	{
		return AgentSaleCode;
	}
	public void setAgentSaleCode(String aAgentSaleCode)
	{
		AgentSaleCode = aAgentSaleCode;
	}
	public String getHandlerIDNo()
	{
		return HandlerIDNo;
	}
	public void setHandlerIDNo(String aHandlerIDNo)
	{
		HandlerIDNo = aHandlerIDNo;
	}

	/**
	* 使用另外一个 LCGrpContSchema 对象给 Schema 赋值
	* @param: aLCGrpContSchema LCGrpContSchema
	**/
	public void setSchema(LCGrpContSchema aLCGrpContSchema)
	{
		this.GrpContNo = aLCGrpContSchema.getGrpContNo();
		this.ProposalGrpContNo = aLCGrpContSchema.getProposalGrpContNo();
		this.PrtNo = aLCGrpContSchema.getPrtNo();
		this.SaleChnl = aLCGrpContSchema.getSaleChnl();
		this.ManageCom = aLCGrpContSchema.getManageCom();
		this.AgentCom = aLCGrpContSchema.getAgentCom();
		this.AgentType = aLCGrpContSchema.getAgentType();
		this.AgentCode = aLCGrpContSchema.getAgentCode();
		this.AgentGroup = aLCGrpContSchema.getAgentGroup();
		this.AgentCode1 = aLCGrpContSchema.getAgentCode1();
		this.Password = aLCGrpContSchema.getPassword();
		this.Password2 = aLCGrpContSchema.getPassword2();
		this.AppntNo = aLCGrpContSchema.getAppntNo();
		this.AddressNo = aLCGrpContSchema.getAddressNo();
		this.Peoples2 = aLCGrpContSchema.getPeoples2();
		this.GrpName = aLCGrpContSchema.getGrpName();
		this.BusinessType = aLCGrpContSchema.getBusinessType();
		this.GrpNature = aLCGrpContSchema.getGrpNature();
		this.RgtMoney = aLCGrpContSchema.getRgtMoney();
		this.Asset = aLCGrpContSchema.getAsset();
		this.NetProfitRate = aLCGrpContSchema.getNetProfitRate();
		this.MainBussiness = aLCGrpContSchema.getMainBussiness();
		this.Corporation = aLCGrpContSchema.getCorporation();
		this.ComAera = aLCGrpContSchema.getComAera();
		this.Fax = aLCGrpContSchema.getFax();
		this.Phone = aLCGrpContSchema.getPhone();
		this.GetFlag = aLCGrpContSchema.getGetFlag();
		this.Satrap = aLCGrpContSchema.getSatrap();
		this.EMail = aLCGrpContSchema.getEMail();
		this.FoundDate = fDate.getDate( aLCGrpContSchema.getFoundDate());
		this.GrpGroupNo = aLCGrpContSchema.getGrpGroupNo();
		this.BankCode = aLCGrpContSchema.getBankCode();
		this.BankAccNo = aLCGrpContSchema.getBankAccNo();
		this.AccName = aLCGrpContSchema.getAccName();
		this.DisputedFlag = aLCGrpContSchema.getDisputedFlag();
		this.OutPayFlag = aLCGrpContSchema.getOutPayFlag();
		this.GetPolMode = aLCGrpContSchema.getGetPolMode();
		this.Lang = aLCGrpContSchema.getLang();
		this.Currency = aLCGrpContSchema.getCurrency();
		this.LostTimes = aLCGrpContSchema.getLostTimes();
		this.PrintCount = aLCGrpContSchema.getPrintCount();
		this.RegetDate = fDate.getDate( aLCGrpContSchema.getRegetDate());
		this.LastEdorDate = fDate.getDate( aLCGrpContSchema.getLastEdorDate());
		this.LastGetDate = fDate.getDate( aLCGrpContSchema.getLastGetDate());
		this.LastLoanDate = fDate.getDate( aLCGrpContSchema.getLastLoanDate());
		this.SpecFlag = aLCGrpContSchema.getSpecFlag();
		this.GrpSpec = aLCGrpContSchema.getGrpSpec();
		this.PayMode = aLCGrpContSchema.getPayMode();
		this.SignCom = aLCGrpContSchema.getSignCom();
		this.SignDate = fDate.getDate( aLCGrpContSchema.getSignDate());
		this.SignTime = aLCGrpContSchema.getSignTime();
		this.CValiDate = fDate.getDate( aLCGrpContSchema.getCValiDate());
		this.PayIntv = aLCGrpContSchema.getPayIntv();
		this.ManageFeeRate = aLCGrpContSchema.getManageFeeRate();
		this.ExpPeoples = aLCGrpContSchema.getExpPeoples();
		this.ExpPremium = aLCGrpContSchema.getExpPremium();
		this.ExpAmnt = aLCGrpContSchema.getExpAmnt();
		this.Peoples = aLCGrpContSchema.getPeoples();
		this.Mult = aLCGrpContSchema.getMult();
		this.Prem = aLCGrpContSchema.getPrem();
		this.Amnt = aLCGrpContSchema.getAmnt();
		this.SumPrem = aLCGrpContSchema.getSumPrem();
		this.SumPay = aLCGrpContSchema.getSumPay();
		this.Dif = aLCGrpContSchema.getDif();
		this.Remark = aLCGrpContSchema.getRemark();
		this.StandbyFlag1 = aLCGrpContSchema.getStandbyFlag1();
		this.StandbyFlag2 = aLCGrpContSchema.getStandbyFlag2();
		this.StandbyFlag3 = aLCGrpContSchema.getStandbyFlag3();
		this.InputOperator = aLCGrpContSchema.getInputOperator();
		this.InputDate = fDate.getDate( aLCGrpContSchema.getInputDate());
		this.InputTime = aLCGrpContSchema.getInputTime();
		this.ApproveFlag = aLCGrpContSchema.getApproveFlag();
		this.ApproveCode = aLCGrpContSchema.getApproveCode();
		this.ApproveDate = fDate.getDate( aLCGrpContSchema.getApproveDate());
		this.ApproveTime = aLCGrpContSchema.getApproveTime();
		this.UWOperator = aLCGrpContSchema.getUWOperator();
		this.UWFlag = aLCGrpContSchema.getUWFlag();
		this.UWDate = fDate.getDate( aLCGrpContSchema.getUWDate());
		this.UWTime = aLCGrpContSchema.getUWTime();
		this.AppFlag = aLCGrpContSchema.getAppFlag();
		this.PolApplyDate = fDate.getDate( aLCGrpContSchema.getPolApplyDate());
		this.CustomGetPolDate = fDate.getDate( aLCGrpContSchema.getCustomGetPolDate());
		this.GetPolDate = fDate.getDate( aLCGrpContSchema.getGetPolDate());
		this.GetPolTime = aLCGrpContSchema.getGetPolTime();
		this.State = aLCGrpContSchema.getState();
		this.Operator = aLCGrpContSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCGrpContSchema.getMakeDate());
		this.MakeTime = aLCGrpContSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCGrpContSchema.getModifyDate());
		this.ModifyTime = aLCGrpContSchema.getModifyTime();
		this.EnterKind = aLCGrpContSchema.getEnterKind();
		this.AmntGrade = aLCGrpContSchema.getAmntGrade();
		this.Peoples3 = aLCGrpContSchema.getPeoples3();
		this.OnWorkPeoples = aLCGrpContSchema.getOnWorkPeoples();
		this.OffWorkPeoples = aLCGrpContSchema.getOffWorkPeoples();
		this.OtherPeoples = aLCGrpContSchema.getOtherPeoples();
		this.RelaPeoples = aLCGrpContSchema.getRelaPeoples();
		this.RelaMatePeoples = aLCGrpContSchema.getRelaMatePeoples();
		this.RelaYoungPeoples = aLCGrpContSchema.getRelaYoungPeoples();
		this.RelaOtherPeoples = aLCGrpContSchema.getRelaOtherPeoples();
		this.FirstTrialOperator = aLCGrpContSchema.getFirstTrialOperator();
		this.FirstTrialDate = fDate.getDate( aLCGrpContSchema.getFirstTrialDate());
		this.FirstTrialTime = aLCGrpContSchema.getFirstTrialTime();
		this.ReceiveOperator = aLCGrpContSchema.getReceiveOperator();
		this.ReceiveDate = fDate.getDate( aLCGrpContSchema.getReceiveDate());
		this.ReceiveTime = aLCGrpContSchema.getReceiveTime();
		this.TempFeeNo = aLCGrpContSchema.getTempFeeNo();
		this.HandlerName = aLCGrpContSchema.getHandlerName();
		this.HandlerDate = fDate.getDate( aLCGrpContSchema.getHandlerDate());
		this.HandlerPrint = aLCGrpContSchema.getHandlerPrint();
		this.AgentDate = fDate.getDate( aLCGrpContSchema.getAgentDate());
		this.BusinessBigType = aLCGrpContSchema.getBusinessBigType();
		this.MarketType = aLCGrpContSchema.getMarketType();
		this.ProposalType = aLCGrpContSchema.getProposalType();
		this.SaleChnlDetail = aLCGrpContSchema.getSaleChnlDetail();
		this.ContPrintLoFlag = aLCGrpContSchema.getContPrintLoFlag();
		this.PremApportFlag = aLCGrpContSchema.getPremApportFlag();
		this.ContPremFeeNo = aLCGrpContSchema.getContPremFeeNo();
		this.CustomerReceiptNo = aLCGrpContSchema.getCustomerReceiptNo();
		this.CInValiDate = fDate.getDate( aLCGrpContSchema.getCInValiDate());
		this.RoleAgentCode = aLCGrpContSchema.getRoleAgentCode();
		this.AskGrpContNo = aLCGrpContSchema.getAskGrpContNo();
		this.Copys = aLCGrpContSchema.getCopys();
		this.OperationManager = aLCGrpContSchema.getOperationManager();
		this.InfoSource = aLCGrpContSchema.getInfoSource();
		this.PremScope = aLCGrpContSchema.getPremScope();
		this.DegreeType = aLCGrpContSchema.getDegreeType();
		this.CardFlag = aLCGrpContSchema.getCardFlag();
		this.BigProjectFlag = aLCGrpContSchema.getBigProjectFlag();
		this.ContPrintType = aLCGrpContSchema.getContPrintType();
		this.StateFlag = aLCGrpContSchema.getStateFlag();
		this.GrpAgentCom = aLCGrpContSchema.getGrpAgentCom();
		this.GrpAgentCode = aLCGrpContSchema.getGrpAgentCode();
		this.GrpAgentName = aLCGrpContSchema.getGrpAgentName();
		this.CTCount = aLCGrpContSchema.getCTCount();
		this.CoInsuranceFlag = aLCGrpContSchema.getCoInsuranceFlag();
		this.CityInfo = aLCGrpContSchema.getCityInfo();
		this.Crs_SaleChnl = aLCGrpContSchema.getCrs_SaleChnl();
		this.Crs_BussType = aLCGrpContSchema.getCrs_BussType();
		this.GrpAgentIDNo = aLCGrpContSchema.getGrpAgentIDNo();
		this.AgentSaleCode = aLCGrpContSchema.getAgentSaleCode();
		this.HandlerIDNo = aLCGrpContSchema.getHandlerIDNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("ProposalGrpContNo") == null )
				this.ProposalGrpContNo = null;
			else
				this.ProposalGrpContNo = rs.getString("ProposalGrpContNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentType") == null )
				this.AgentType = null;
			else
				this.AgentType = rs.getString("AgentType").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("AgentCode1") == null )
				this.AgentCode1 = null;
			else
				this.AgentCode1 = rs.getString("AgentCode1").trim();

			if( rs.getString("Password") == null )
				this.Password = null;
			else
				this.Password = rs.getString("Password").trim();

			if( rs.getString("Password2") == null )
				this.Password2 = null;
			else
				this.Password2 = rs.getString("Password2").trim();

			if( rs.getString("AppntNo") == null )
				this.AppntNo = null;
			else
				this.AppntNo = rs.getString("AppntNo").trim();

			if( rs.getString("AddressNo") == null )
				this.AddressNo = null;
			else
				this.AddressNo = rs.getString("AddressNo").trim();

			this.Peoples2 = rs.getInt("Peoples2");
			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			if( rs.getString("BusinessType") == null )
				this.BusinessType = null;
			else
				this.BusinessType = rs.getString("BusinessType").trim();

			if( rs.getString("GrpNature") == null )
				this.GrpNature = null;
			else
				this.GrpNature = rs.getString("GrpNature").trim();

			this.RgtMoney = rs.getDouble("RgtMoney");
			this.Asset = rs.getDouble("Asset");
			this.NetProfitRate = rs.getDouble("NetProfitRate");
			if( rs.getString("MainBussiness") == null )
				this.MainBussiness = null;
			else
				this.MainBussiness = rs.getString("MainBussiness").trim();

			if( rs.getString("Corporation") == null )
				this.Corporation = null;
			else
				this.Corporation = rs.getString("Corporation").trim();

			if( rs.getString("ComAera") == null )
				this.ComAera = null;
			else
				this.ComAera = rs.getString("ComAera").trim();

			if( rs.getString("Fax") == null )
				this.Fax = null;
			else
				this.Fax = rs.getString("Fax").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("GetFlag") == null )
				this.GetFlag = null;
			else
				this.GetFlag = rs.getString("GetFlag").trim();

			if( rs.getString("Satrap") == null )
				this.Satrap = null;
			else
				this.Satrap = rs.getString("Satrap").trim();

			if( rs.getString("EMail") == null )
				this.EMail = null;
			else
				this.EMail = rs.getString("EMail").trim();

			this.FoundDate = rs.getDate("FoundDate");
			if( rs.getString("GrpGroupNo") == null )
				this.GrpGroupNo = null;
			else
				this.GrpGroupNo = rs.getString("GrpGroupNo").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("DisputedFlag") == null )
				this.DisputedFlag = null;
			else
				this.DisputedFlag = rs.getString("DisputedFlag").trim();

			if( rs.getString("OutPayFlag") == null )
				this.OutPayFlag = null;
			else
				this.OutPayFlag = rs.getString("OutPayFlag").trim();

			if( rs.getString("GetPolMode") == null )
				this.GetPolMode = null;
			else
				this.GetPolMode = rs.getString("GetPolMode").trim();

			if( rs.getString("Lang") == null )
				this.Lang = null;
			else
				this.Lang = rs.getString("Lang").trim();

			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

			this.LostTimes = rs.getInt("LostTimes");
			this.PrintCount = rs.getInt("PrintCount");
			this.RegetDate = rs.getDate("RegetDate");
			this.LastEdorDate = rs.getDate("LastEdorDate");
			this.LastGetDate = rs.getDate("LastGetDate");
			this.LastLoanDate = rs.getDate("LastLoanDate");
			if( rs.getString("SpecFlag") == null )
				this.SpecFlag = null;
			else
				this.SpecFlag = rs.getString("SpecFlag").trim();

			if( rs.getString("GrpSpec") == null )
				this.GrpSpec = null;
			else
				this.GrpSpec = rs.getString("GrpSpec").trim();

			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("SignCom") == null )
				this.SignCom = null;
			else
				this.SignCom = rs.getString("SignCom").trim();

			this.SignDate = rs.getDate("SignDate");
			if( rs.getString("SignTime") == null )
				this.SignTime = null;
			else
				this.SignTime = rs.getString("SignTime").trim();

			this.CValiDate = rs.getDate("CValiDate");
			this.PayIntv = rs.getInt("PayIntv");
			this.ManageFeeRate = rs.getDouble("ManageFeeRate");
			this.ExpPeoples = rs.getInt("ExpPeoples");
			this.ExpPremium = rs.getDouble("ExpPremium");
			this.ExpAmnt = rs.getDouble("ExpAmnt");
			this.Peoples = rs.getInt("Peoples");
			this.Mult = rs.getDouble("Mult");
			this.Prem = rs.getDouble("Prem");
			this.Amnt = rs.getDouble("Amnt");
			this.SumPrem = rs.getDouble("SumPrem");
			this.SumPay = rs.getDouble("SumPay");
			this.Dif = rs.getDouble("Dif");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("StandbyFlag1") == null )
				this.StandbyFlag1 = null;
			else
				this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

			if( rs.getString("StandbyFlag2") == null )
				this.StandbyFlag2 = null;
			else
				this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

			if( rs.getString("StandbyFlag3") == null )
				this.StandbyFlag3 = null;
			else
				this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();

			if( rs.getString("InputOperator") == null )
				this.InputOperator = null;
			else
				this.InputOperator = rs.getString("InputOperator").trim();

			this.InputDate = rs.getDate("InputDate");
			if( rs.getString("InputTime") == null )
				this.InputTime = null;
			else
				this.InputTime = rs.getString("InputTime").trim();

			if( rs.getString("ApproveFlag") == null )
				this.ApproveFlag = null;
			else
				this.ApproveFlag = rs.getString("ApproveFlag").trim();

			if( rs.getString("ApproveCode") == null )
				this.ApproveCode = null;
			else
				this.ApproveCode = rs.getString("ApproveCode").trim();

			this.ApproveDate = rs.getDate("ApproveDate");
			if( rs.getString("ApproveTime") == null )
				this.ApproveTime = null;
			else
				this.ApproveTime = rs.getString("ApproveTime").trim();

			if( rs.getString("UWOperator") == null )
				this.UWOperator = null;
			else
				this.UWOperator = rs.getString("UWOperator").trim();

			if( rs.getString("UWFlag") == null )
				this.UWFlag = null;
			else
				this.UWFlag = rs.getString("UWFlag").trim();

			this.UWDate = rs.getDate("UWDate");
			if( rs.getString("UWTime") == null )
				this.UWTime = null;
			else
				this.UWTime = rs.getString("UWTime").trim();

			if( rs.getString("AppFlag") == null )
				this.AppFlag = null;
			else
				this.AppFlag = rs.getString("AppFlag").trim();

			this.PolApplyDate = rs.getDate("PolApplyDate");
			this.CustomGetPolDate = rs.getDate("CustomGetPolDate");
			this.GetPolDate = rs.getDate("GetPolDate");
			if( rs.getString("GetPolTime") == null )
				this.GetPolTime = null;
			else
				this.GetPolTime = rs.getString("GetPolTime").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("EnterKind") == null )
				this.EnterKind = null;
			else
				this.EnterKind = rs.getString("EnterKind").trim();

			if( rs.getString("AmntGrade") == null )
				this.AmntGrade = null;
			else
				this.AmntGrade = rs.getString("AmntGrade").trim();

			this.Peoples3 = rs.getInt("Peoples3");
			this.OnWorkPeoples = rs.getInt("OnWorkPeoples");
			this.OffWorkPeoples = rs.getInt("OffWorkPeoples");
			this.OtherPeoples = rs.getInt("OtherPeoples");
			this.RelaPeoples = rs.getInt("RelaPeoples");
			this.RelaMatePeoples = rs.getInt("RelaMatePeoples");
			this.RelaYoungPeoples = rs.getInt("RelaYoungPeoples");
			this.RelaOtherPeoples = rs.getInt("RelaOtherPeoples");
			if( rs.getString("FirstTrialOperator") == null )
				this.FirstTrialOperator = null;
			else
				this.FirstTrialOperator = rs.getString("FirstTrialOperator").trim();

			this.FirstTrialDate = rs.getDate("FirstTrialDate");
			if( rs.getString("FirstTrialTime") == null )
				this.FirstTrialTime = null;
			else
				this.FirstTrialTime = rs.getString("FirstTrialTime").trim();

			if( rs.getString("ReceiveOperator") == null )
				this.ReceiveOperator = null;
			else
				this.ReceiveOperator = rs.getString("ReceiveOperator").trim();

			this.ReceiveDate = rs.getDate("ReceiveDate");
			if( rs.getString("ReceiveTime") == null )
				this.ReceiveTime = null;
			else
				this.ReceiveTime = rs.getString("ReceiveTime").trim();

			if( rs.getString("TempFeeNo") == null )
				this.TempFeeNo = null;
			else
				this.TempFeeNo = rs.getString("TempFeeNo").trim();

			if( rs.getString("HandlerName") == null )
				this.HandlerName = null;
			else
				this.HandlerName = rs.getString("HandlerName").trim();

			this.HandlerDate = rs.getDate("HandlerDate");
			if( rs.getString("HandlerPrint") == null )
				this.HandlerPrint = null;
			else
				this.HandlerPrint = rs.getString("HandlerPrint").trim();

			this.AgentDate = rs.getDate("AgentDate");
			if( rs.getString("BusinessBigType") == null )
				this.BusinessBigType = null;
			else
				this.BusinessBigType = rs.getString("BusinessBigType").trim();

			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("ProposalType") == null )
				this.ProposalType = null;
			else
				this.ProposalType = rs.getString("ProposalType").trim();

			if( rs.getString("SaleChnlDetail") == null )
				this.SaleChnlDetail = null;
			else
				this.SaleChnlDetail = rs.getString("SaleChnlDetail").trim();

			if( rs.getString("ContPrintLoFlag") == null )
				this.ContPrintLoFlag = null;
			else
				this.ContPrintLoFlag = rs.getString("ContPrintLoFlag").trim();

			if( rs.getString("PremApportFlag") == null )
				this.PremApportFlag = null;
			else
				this.PremApportFlag = rs.getString("PremApportFlag").trim();

			if( rs.getString("ContPremFeeNo") == null )
				this.ContPremFeeNo = null;
			else
				this.ContPremFeeNo = rs.getString("ContPremFeeNo").trim();

			if( rs.getString("CustomerReceiptNo") == null )
				this.CustomerReceiptNo = null;
			else
				this.CustomerReceiptNo = rs.getString("CustomerReceiptNo").trim();

			this.CInValiDate = rs.getDate("CInValiDate");
			if( rs.getString("RoleAgentCode") == null )
				this.RoleAgentCode = null;
			else
				this.RoleAgentCode = rs.getString("RoleAgentCode").trim();

			if( rs.getString("AskGrpContNo") == null )
				this.AskGrpContNo = null;
			else
				this.AskGrpContNo = rs.getString("AskGrpContNo").trim();

			this.Copys = rs.getDouble("Copys");
			if( rs.getString("OperationManager") == null )
				this.OperationManager = null;
			else
				this.OperationManager = rs.getString("OperationManager").trim();

			if( rs.getString("InfoSource") == null )
				this.InfoSource = null;
			else
				this.InfoSource = rs.getString("InfoSource").trim();

			this.PremScope = rs.getDouble("PremScope");
			if( rs.getString("DegreeType") == null )
				this.DegreeType = null;
			else
				this.DegreeType = rs.getString("DegreeType").trim();

			if( rs.getString("CardFlag") == null )
				this.CardFlag = null;
			else
				this.CardFlag = rs.getString("CardFlag").trim();

			if( rs.getString("BigProjectFlag") == null )
				this.BigProjectFlag = null;
			else
				this.BigProjectFlag = rs.getString("BigProjectFlag").trim();

			if( rs.getString("ContPrintType") == null )
				this.ContPrintType = null;
			else
				this.ContPrintType = rs.getString("ContPrintType").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			if( rs.getString("GrpAgentCom") == null )
				this.GrpAgentCom = null;
			else
				this.GrpAgentCom = rs.getString("GrpAgentCom").trim();

			if( rs.getString("GrpAgentCode") == null )
				this.GrpAgentCode = null;
			else
				this.GrpAgentCode = rs.getString("GrpAgentCode").trim();

			if( rs.getString("GrpAgentName") == null )
				this.GrpAgentName = null;
			else
				this.GrpAgentName = rs.getString("GrpAgentName").trim();

			this.CTCount = rs.getInt("CTCount");
			if( rs.getString("CoInsuranceFlag") == null )
				this.CoInsuranceFlag = null;
			else
				this.CoInsuranceFlag = rs.getString("CoInsuranceFlag").trim();

			if( rs.getString("CityInfo") == null )
				this.CityInfo = null;
			else
				this.CityInfo = rs.getString("CityInfo").trim();

			if( rs.getString("Crs_SaleChnl") == null )
				this.Crs_SaleChnl = null;
			else
				this.Crs_SaleChnl = rs.getString("Crs_SaleChnl").trim();

			if( rs.getString("Crs_BussType") == null )
				this.Crs_BussType = null;
			else
				this.Crs_BussType = rs.getString("Crs_BussType").trim();

			if( rs.getString("GrpAgentIDNo") == null )
				this.GrpAgentIDNo = null;
			else
				this.GrpAgentIDNo = rs.getString("GrpAgentIDNo").trim();

			if( rs.getString("AgentSaleCode") == null )
				this.AgentSaleCode = null;
			else
				this.AgentSaleCode = rs.getString("AgentSaleCode").trim();

			if( rs.getString("HandlerIDNo") == null )
				this.HandlerIDNo = null;
			else
				this.HandlerIDNo = rs.getString("HandlerIDNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCGrpCont表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCGrpContSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCGrpContSchema getSchema()
	{
		LCGrpContSchema aLCGrpContSchema = new LCGrpContSchema();
		aLCGrpContSchema.setSchema(this);
		return aLCGrpContSchema;
	}

	public LCGrpContDB getDB()
	{
		LCGrpContDB aDBOper = new LCGrpContDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpCont描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalGrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Password)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Password2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AddressNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Peoples2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpNature)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RgtMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Asset));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(NetProfitRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MainBussiness)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Corporation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComAera)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Fax)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Satrap)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FoundDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpGroupNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DisputedFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OutPayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetPolMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Lang)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LostTimes));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PrintCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RegetDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LastEdorDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LastGetDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LastLoanDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SpecFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpSpec)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SignCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SignTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ManageFeeRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExpPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExpPremium));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExpAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Peoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Mult));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Dif));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InputDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApproveFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApproveCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ApproveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApproveTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( UWDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PolApplyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CustomGetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GetPolDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetPolTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EnterKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AmntGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Peoples3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OnWorkPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OffWorkPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RelaPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RelaMatePeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RelaYoungPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RelaOtherPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstTrialOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FirstTrialDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstTrialTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiveOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ReceiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiveTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HandlerDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerPrint)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AgentDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessBigType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnlDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPrintLoFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremApportFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPremFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CInValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RoleAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AskGrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Copys));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OperationManager)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InfoSource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PremScope));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DegreeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CardFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BigProjectFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPrintType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CTCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CoInsuranceFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CityInfo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_BussType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentSaleCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandlerIDNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpCont>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ProposalGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AgentCode1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Password2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Peoples2= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).intValue();
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			BusinessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			GrpNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			RgtMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			Asset = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			NetProfitRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			MainBussiness = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Corporation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ComAera = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			GetFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Satrap = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			FoundDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			GrpGroupNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			DisputedFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			OutPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			GetPolMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			Lang = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			LostTimes= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,40,SysConst.PACKAGESPILTER))).intValue();
			PrintCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).intValue();
			RegetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,SysConst.PACKAGESPILTER));
			LastEdorDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
			LastGetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,SysConst.PACKAGESPILTER));
			LastLoanDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45,SysConst.PACKAGESPILTER));
			SpecFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			GrpSpec = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			SignCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50,SysConst.PACKAGESPILTER));
			SignTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52,SysConst.PACKAGESPILTER));
			PayIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,53,SysConst.PACKAGESPILTER))).intValue();
			ManageFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,54,SysConst.PACKAGESPILTER))).doubleValue();
			ExpPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,55,SysConst.PACKAGESPILTER))).intValue();
			ExpPremium = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,56,SysConst.PACKAGESPILTER))).doubleValue();
			ExpAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,57,SysConst.PACKAGESPILTER))).doubleValue();
			Peoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,58,SysConst.PACKAGESPILTER))).intValue();
			Mult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,59,SysConst.PACKAGESPILTER))).doubleValue();
			Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,60,SysConst.PACKAGESPILTER))).doubleValue();
			Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,61,SysConst.PACKAGESPILTER))).doubleValue();
			SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,62,SysConst.PACKAGESPILTER))).doubleValue();
			SumPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,63,SysConst.PACKAGESPILTER))).doubleValue();
			Dif = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,64,SysConst.PACKAGESPILTER))).doubleValue();
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
			StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
			StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
			InputOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
			InputDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70,SysConst.PACKAGESPILTER));
			InputTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 71, SysConst.PACKAGESPILTER );
			ApproveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 72, SysConst.PACKAGESPILTER );
			ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 73, SysConst.PACKAGESPILTER );
			ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74,SysConst.PACKAGESPILTER));
			ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 75, SysConst.PACKAGESPILTER );
			UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76, SysConst.PACKAGESPILTER );
			UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 77, SysConst.PACKAGESPILTER );
			UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 78,SysConst.PACKAGESPILTER));
			UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 79, SysConst.PACKAGESPILTER );
			AppFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 80, SysConst.PACKAGESPILTER );
			PolApplyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 81,SysConst.PACKAGESPILTER));
			CustomGetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 82,SysConst.PACKAGESPILTER));
			GetPolDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 83,SysConst.PACKAGESPILTER));
			GetPolTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 84, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 85, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 86, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 87,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 88, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 89,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 90, SysConst.PACKAGESPILTER );
			EnterKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 91, SysConst.PACKAGESPILTER );
			AmntGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 92, SysConst.PACKAGESPILTER );
			Peoples3= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,93,SysConst.PACKAGESPILTER))).intValue();
			OnWorkPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,94,SysConst.PACKAGESPILTER))).intValue();
			OffWorkPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,95,SysConst.PACKAGESPILTER))).intValue();
			OtherPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,96,SysConst.PACKAGESPILTER))).intValue();
			RelaPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,97,SysConst.PACKAGESPILTER))).intValue();
			RelaMatePeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,98,SysConst.PACKAGESPILTER))).intValue();
			RelaYoungPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,99,SysConst.PACKAGESPILTER))).intValue();
			RelaOtherPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,100,SysConst.PACKAGESPILTER))).intValue();
			FirstTrialOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 101, SysConst.PACKAGESPILTER );
			FirstTrialDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 102,SysConst.PACKAGESPILTER));
			FirstTrialTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 103, SysConst.PACKAGESPILTER );
			ReceiveOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 104, SysConst.PACKAGESPILTER );
			ReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 105,SysConst.PACKAGESPILTER));
			ReceiveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 106, SysConst.PACKAGESPILTER );
			TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 107, SysConst.PACKAGESPILTER );
			HandlerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 108, SysConst.PACKAGESPILTER );
			HandlerDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 109,SysConst.PACKAGESPILTER));
			HandlerPrint = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 110, SysConst.PACKAGESPILTER );
			AgentDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 111,SysConst.PACKAGESPILTER));
			BusinessBigType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 112, SysConst.PACKAGESPILTER );
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 113, SysConst.PACKAGESPILTER );
			ProposalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 114, SysConst.PACKAGESPILTER );
			SaleChnlDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 115, SysConst.PACKAGESPILTER );
			ContPrintLoFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 116, SysConst.PACKAGESPILTER );
			PremApportFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 117, SysConst.PACKAGESPILTER );
			ContPremFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 118, SysConst.PACKAGESPILTER );
			CustomerReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 119, SysConst.PACKAGESPILTER );
			CInValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 120,SysConst.PACKAGESPILTER));
			RoleAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 121, SysConst.PACKAGESPILTER );
			AskGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 122, SysConst.PACKAGESPILTER );
			Copys = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,123,SysConst.PACKAGESPILTER))).doubleValue();
			OperationManager = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 124, SysConst.PACKAGESPILTER );
			InfoSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 125, SysConst.PACKAGESPILTER );
			PremScope = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,126,SysConst.PACKAGESPILTER))).doubleValue();
			DegreeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 127, SysConst.PACKAGESPILTER );
			CardFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 128, SysConst.PACKAGESPILTER );
			BigProjectFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 129, SysConst.PACKAGESPILTER );
			ContPrintType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 130, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 131, SysConst.PACKAGESPILTER );
			GrpAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 132, SysConst.PACKAGESPILTER );
			GrpAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 133, SysConst.PACKAGESPILTER );
			GrpAgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 134, SysConst.PACKAGESPILTER );
			CTCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,135,SysConst.PACKAGESPILTER))).intValue();
			CoInsuranceFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 136, SysConst.PACKAGESPILTER );
			CityInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 137, SysConst.PACKAGESPILTER );
			Crs_SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 138, SysConst.PACKAGESPILTER );
			Crs_BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 139, SysConst.PACKAGESPILTER );
			GrpAgentIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 140, SysConst.PACKAGESPILTER );
			AgentSaleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 141, SysConst.PACKAGESPILTER );
			HandlerIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 142, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCGrpContSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("ProposalGrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalGrpContNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("AgentCode1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
		}
		if (FCode.equals("Password"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Password));
		}
		if (FCode.equals("Password2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Password2));
		}
		if (FCode.equals("AppntNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
		}
		if (FCode.equals("AddressNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
		}
		if (FCode.equals("Peoples2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("BusinessType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessType));
		}
		if (FCode.equals("GrpNature"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpNature));
		}
		if (FCode.equals("RgtMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtMoney));
		}
		if (FCode.equals("Asset"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Asset));
		}
		if (FCode.equals("NetProfitRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NetProfitRate));
		}
		if (FCode.equals("MainBussiness"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MainBussiness));
		}
		if (FCode.equals("Corporation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Corporation));
		}
		if (FCode.equals("ComAera"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComAera));
		}
		if (FCode.equals("Fax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("GetFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetFlag));
		}
		if (FCode.equals("Satrap"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Satrap));
		}
		if (FCode.equals("EMail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
		}
		if (FCode.equals("FoundDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFoundDate()));
		}
		if (FCode.equals("GrpGroupNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpGroupNo));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("DisputedFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DisputedFlag));
		}
		if (FCode.equals("OutPayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutPayFlag));
		}
		if (FCode.equals("GetPolMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolMode));
		}
		if (FCode.equals("Lang"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Lang));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (FCode.equals("LostTimes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LostTimes));
		}
		if (FCode.equals("PrintCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCount));
		}
		if (FCode.equals("RegetDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRegetDate()));
		}
		if (FCode.equals("LastEdorDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastEdorDate()));
		}
		if (FCode.equals("LastGetDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastGetDate()));
		}
		if (FCode.equals("LastLoanDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLastLoanDate()));
		}
		if (FCode.equals("SpecFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpecFlag));
		}
		if (FCode.equals("GrpSpec"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpSpec));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("SignCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignCom));
		}
		if (FCode.equals("SignDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
		}
		if (FCode.equals("SignTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignTime));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("ManageFeeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFeeRate));
		}
		if (FCode.equals("ExpPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExpPeoples));
		}
		if (FCode.equals("ExpPremium"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExpPremium));
		}
		if (FCode.equals("ExpAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExpAmnt));
		}
		if (FCode.equals("Peoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
		}
		if (FCode.equals("Mult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
		}
		if (FCode.equals("Prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("SumPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
		}
		if (FCode.equals("SumPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
		}
		if (FCode.equals("Dif"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dif));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("StandbyFlag1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
		}
		if (FCode.equals("StandbyFlag2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
		}
		if (FCode.equals("StandbyFlag3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
		}
		if (FCode.equals("InputOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputOperator));
		}
		if (FCode.equals("InputDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
		}
		if (FCode.equals("InputTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputTime));
		}
		if (FCode.equals("ApproveFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
		}
		if (FCode.equals("ApproveCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
		}
		if (FCode.equals("ApproveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
		}
		if (FCode.equals("ApproveTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
		}
		if (FCode.equals("UWOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
		}
		if (FCode.equals("UWFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
		}
		if (FCode.equals("UWDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
		}
		if (FCode.equals("UWTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
		}
		if (FCode.equals("AppFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
		}
		if (FCode.equals("PolApplyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
		}
		if (FCode.equals("CustomGetPolDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
		}
		if (FCode.equals("GetPolDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
		}
		if (FCode.equals("GetPolTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetPolTime));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("EnterKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EnterKind));
		}
		if (FCode.equals("AmntGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AmntGrade));
		}
		if (FCode.equals("Peoples3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples3));
		}
		if (FCode.equals("OnWorkPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OnWorkPeoples));
		}
		if (FCode.equals("OffWorkPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OffWorkPeoples));
		}
		if (FCode.equals("OtherPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPeoples));
		}
		if (FCode.equals("RelaPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaPeoples));
		}
		if (FCode.equals("RelaMatePeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaMatePeoples));
		}
		if (FCode.equals("RelaYoungPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaYoungPeoples));
		}
		if (FCode.equals("RelaOtherPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RelaOtherPeoples));
		}
		if (FCode.equals("FirstTrialOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialOperator));
		}
		if (FCode.equals("FirstTrialDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFirstTrialDate()));
		}
		if (FCode.equals("FirstTrialTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstTrialTime));
		}
		if (FCode.equals("ReceiveOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveOperator));
		}
		if (FCode.equals("ReceiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
		}
		if (FCode.equals("ReceiveTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
		}
		if (FCode.equals("TempFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
		}
		if (FCode.equals("HandlerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerName));
		}
		if (FCode.equals("HandlerDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHandlerDate()));
		}
		if (FCode.equals("HandlerPrint"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerPrint));
		}
		if (FCode.equals("AgentDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAgentDate()));
		}
		if (FCode.equals("BusinessBigType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessBigType));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("ProposalType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalType));
		}
		if (FCode.equals("SaleChnlDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnlDetail));
		}
		if (FCode.equals("ContPrintLoFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPrintLoFlag));
		}
		if (FCode.equals("PremApportFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremApportFlag));
		}
		if (FCode.equals("ContPremFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPremFeeNo));
		}
		if (FCode.equals("CustomerReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerReceiptNo));
		}
		if (FCode.equals("CInValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCInValiDate()));
		}
		if (FCode.equals("RoleAgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RoleAgentCode));
		}
		if (FCode.equals("AskGrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AskGrpContNo));
		}
		if (FCode.equals("Copys"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Copys));
		}
		if (FCode.equals("OperationManager"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OperationManager));
		}
		if (FCode.equals("InfoSource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InfoSource));
		}
		if (FCode.equals("PremScope"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremScope));
		}
		if (FCode.equals("DegreeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DegreeType));
		}
		if (FCode.equals("CardFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CardFlag));
		}
		if (FCode.equals("BigProjectFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BigProjectFlag));
		}
		if (FCode.equals("ContPrintType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPrintType));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("GrpAgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCom));
		}
		if (FCode.equals("GrpAgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCode));
		}
		if (FCode.equals("GrpAgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentName));
		}
		if (FCode.equals("CTCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CTCount));
		}
		if (FCode.equals("CoInsuranceFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CoInsuranceFlag));
		}
		if (FCode.equals("CityInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CityInfo));
		}
		if (FCode.equals("Crs_SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_SaleChnl));
		}
		if (FCode.equals("Crs_BussType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_BussType));
		}
		if (FCode.equals("GrpAgentIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentIDNo));
		}
		if (FCode.equals("AgentSaleCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSaleCode));
		}
		if (FCode.equals("HandlerIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandlerIDNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ProposalGrpContNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AgentType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AgentCode1);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Password);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Password2);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(AppntNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(AddressNo);
				break;
			case 14:
				strFieldValue = String.valueOf(Peoples2);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(BusinessType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(GrpNature);
				break;
			case 18:
				strFieldValue = String.valueOf(RgtMoney);
				break;
			case 19:
				strFieldValue = String.valueOf(Asset);
				break;
			case 20:
				strFieldValue = String.valueOf(NetProfitRate);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(MainBussiness);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Corporation);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ComAera);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Fax);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(GetFlag);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Satrap);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(EMail);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFoundDate()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(GrpGroupNo);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(DisputedFlag);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(OutPayFlag);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(GetPolMode);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(Lang);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			case 39:
				strFieldValue = String.valueOf(LostTimes);
				break;
			case 40:
				strFieldValue = String.valueOf(PrintCount);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRegetDate()));
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastEdorDate()));
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastGetDate()));
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLastLoanDate()));
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(SpecFlag);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(GrpSpec);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(SignCom);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(SignTime);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 52:
				strFieldValue = String.valueOf(PayIntv);
				break;
			case 53:
				strFieldValue = String.valueOf(ManageFeeRate);
				break;
			case 54:
				strFieldValue = String.valueOf(ExpPeoples);
				break;
			case 55:
				strFieldValue = String.valueOf(ExpPremium);
				break;
			case 56:
				strFieldValue = String.valueOf(ExpAmnt);
				break;
			case 57:
				strFieldValue = String.valueOf(Peoples);
				break;
			case 58:
				strFieldValue = String.valueOf(Mult);
				break;
			case 59:
				strFieldValue = String.valueOf(Prem);
				break;
			case 60:
				strFieldValue = String.valueOf(Amnt);
				break;
			case 61:
				strFieldValue = String.valueOf(SumPrem);
				break;
			case 62:
				strFieldValue = String.valueOf(SumPay);
				break;
			case 63:
				strFieldValue = String.valueOf(Dif);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
				break;
			case 68:
				strFieldValue = StrTool.GBKToUnicode(InputOperator);
				break;
			case 69:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInputDate()));
				break;
			case 70:
				strFieldValue = StrTool.GBKToUnicode(InputTime);
				break;
			case 71:
				strFieldValue = StrTool.GBKToUnicode(ApproveFlag);
				break;
			case 72:
				strFieldValue = StrTool.GBKToUnicode(ApproveCode);
				break;
			case 73:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApproveDate()));
				break;
			case 74:
				strFieldValue = StrTool.GBKToUnicode(ApproveTime);
				break;
			case 75:
				strFieldValue = StrTool.GBKToUnicode(UWOperator);
				break;
			case 76:
				strFieldValue = StrTool.GBKToUnicode(UWFlag);
				break;
			case 77:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUWDate()));
				break;
			case 78:
				strFieldValue = StrTool.GBKToUnicode(UWTime);
				break;
			case 79:
				strFieldValue = StrTool.GBKToUnicode(AppFlag);
				break;
			case 80:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolApplyDate()));
				break;
			case 81:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCustomGetPolDate()));
				break;
			case 82:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetPolDate()));
				break;
			case 83:
				strFieldValue = StrTool.GBKToUnicode(GetPolTime);
				break;
			case 84:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 85:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 86:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 87:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 88:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 89:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 90:
				strFieldValue = StrTool.GBKToUnicode(EnterKind);
				break;
			case 91:
				strFieldValue = StrTool.GBKToUnicode(AmntGrade);
				break;
			case 92:
				strFieldValue = String.valueOf(Peoples3);
				break;
			case 93:
				strFieldValue = String.valueOf(OnWorkPeoples);
				break;
			case 94:
				strFieldValue = String.valueOf(OffWorkPeoples);
				break;
			case 95:
				strFieldValue = String.valueOf(OtherPeoples);
				break;
			case 96:
				strFieldValue = String.valueOf(RelaPeoples);
				break;
			case 97:
				strFieldValue = String.valueOf(RelaMatePeoples);
				break;
			case 98:
				strFieldValue = String.valueOf(RelaYoungPeoples);
				break;
			case 99:
				strFieldValue = String.valueOf(RelaOtherPeoples);
				break;
			case 100:
				strFieldValue = StrTool.GBKToUnicode(FirstTrialOperator);
				break;
			case 101:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFirstTrialDate()));
				break;
			case 102:
				strFieldValue = StrTool.GBKToUnicode(FirstTrialTime);
				break;
			case 103:
				strFieldValue = StrTool.GBKToUnicode(ReceiveOperator);
				break;
			case 104:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
				break;
			case 105:
				strFieldValue = StrTool.GBKToUnicode(ReceiveTime);
				break;
			case 106:
				strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
				break;
			case 107:
				strFieldValue = StrTool.GBKToUnicode(HandlerName);
				break;
			case 108:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHandlerDate()));
				break;
			case 109:
				strFieldValue = StrTool.GBKToUnicode(HandlerPrint);
				break;
			case 110:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAgentDate()));
				break;
			case 111:
				strFieldValue = StrTool.GBKToUnicode(BusinessBigType);
				break;
			case 112:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 113:
				strFieldValue = StrTool.GBKToUnicode(ProposalType);
				break;
			case 114:
				strFieldValue = StrTool.GBKToUnicode(SaleChnlDetail);
				break;
			case 115:
				strFieldValue = StrTool.GBKToUnicode(ContPrintLoFlag);
				break;
			case 116:
				strFieldValue = StrTool.GBKToUnicode(PremApportFlag);
				break;
			case 117:
				strFieldValue = StrTool.GBKToUnicode(ContPremFeeNo);
				break;
			case 118:
				strFieldValue = StrTool.GBKToUnicode(CustomerReceiptNo);
				break;
			case 119:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCInValiDate()));
				break;
			case 120:
				strFieldValue = StrTool.GBKToUnicode(RoleAgentCode);
				break;
			case 121:
				strFieldValue = StrTool.GBKToUnicode(AskGrpContNo);
				break;
			case 122:
				strFieldValue = String.valueOf(Copys);
				break;
			case 123:
				strFieldValue = StrTool.GBKToUnicode(OperationManager);
				break;
			case 124:
				strFieldValue = StrTool.GBKToUnicode(InfoSource);
				break;
			case 125:
				strFieldValue = String.valueOf(PremScope);
				break;
			case 126:
				strFieldValue = StrTool.GBKToUnicode(DegreeType);
				break;
			case 127:
				strFieldValue = StrTool.GBKToUnicode(CardFlag);
				break;
			case 128:
				strFieldValue = StrTool.GBKToUnicode(BigProjectFlag);
				break;
			case 129:
				strFieldValue = StrTool.GBKToUnicode(ContPrintType);
				break;
			case 130:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 131:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCom);
				break;
			case 132:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCode);
				break;
			case 133:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentName);
				break;
			case 134:
				strFieldValue = String.valueOf(CTCount);
				break;
			case 135:
				strFieldValue = StrTool.GBKToUnicode(CoInsuranceFlag);
				break;
			case 136:
				strFieldValue = StrTool.GBKToUnicode(CityInfo);
				break;
			case 137:
				strFieldValue = StrTool.GBKToUnicode(Crs_SaleChnl);
				break;
			case 138:
				strFieldValue = StrTool.GBKToUnicode(Crs_BussType);
				break;
			case 139:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentIDNo);
				break;
			case 140:
				strFieldValue = StrTool.GBKToUnicode(AgentSaleCode);
				break;
			case 141:
				strFieldValue = StrTool.GBKToUnicode(HandlerIDNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("ProposalGrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalGrpContNo = FValue.trim();
			}
			else
				ProposalGrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentType = FValue.trim();
			}
			else
				AgentType = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode1 = FValue.trim();
			}
			else
				AgentCode1 = null;
		}
		if (FCode.equalsIgnoreCase("Password"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Password = FValue.trim();
			}
			else
				Password = null;
		}
		if (FCode.equalsIgnoreCase("Password2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Password2 = FValue.trim();
			}
			else
				Password2 = null;
		}
		if (FCode.equalsIgnoreCase("AppntNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntNo = FValue.trim();
			}
			else
				AppntNo = null;
		}
		if (FCode.equalsIgnoreCase("AddressNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AddressNo = FValue.trim();
			}
			else
				AddressNo = null;
		}
		if (FCode.equalsIgnoreCase("Peoples2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Peoples2 = i;
			}
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("BusinessType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessType = FValue.trim();
			}
			else
				BusinessType = null;
		}
		if (FCode.equalsIgnoreCase("GrpNature"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpNature = FValue.trim();
			}
			else
				GrpNature = null;
		}
		if (FCode.equalsIgnoreCase("RgtMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RgtMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("Asset"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Asset = d;
			}
		}
		if (FCode.equalsIgnoreCase("NetProfitRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				NetProfitRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("MainBussiness"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MainBussiness = FValue.trim();
			}
			else
				MainBussiness = null;
		}
		if (FCode.equalsIgnoreCase("Corporation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Corporation = FValue.trim();
			}
			else
				Corporation = null;
		}
		if (FCode.equalsIgnoreCase("ComAera"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComAera = FValue.trim();
			}
			else
				ComAera = null;
		}
		if (FCode.equalsIgnoreCase("Fax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Fax = FValue.trim();
			}
			else
				Fax = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("GetFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetFlag = FValue.trim();
			}
			else
				GetFlag = null;
		}
		if (FCode.equalsIgnoreCase("Satrap"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Satrap = FValue.trim();
			}
			else
				Satrap = null;
		}
		if (FCode.equalsIgnoreCase("EMail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EMail = FValue.trim();
			}
			else
				EMail = null;
		}
		if (FCode.equalsIgnoreCase("FoundDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FoundDate = fDate.getDate( FValue );
			}
			else
				FoundDate = null;
		}
		if (FCode.equalsIgnoreCase("GrpGroupNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpGroupNo = FValue.trim();
			}
			else
				GrpGroupNo = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("DisputedFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DisputedFlag = FValue.trim();
			}
			else
				DisputedFlag = null;
		}
		if (FCode.equalsIgnoreCase("OutPayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OutPayFlag = FValue.trim();
			}
			else
				OutPayFlag = null;
		}
		if (FCode.equalsIgnoreCase("GetPolMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetPolMode = FValue.trim();
			}
			else
				GetPolMode = null;
		}
		if (FCode.equalsIgnoreCase("Lang"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Lang = FValue.trim();
			}
			else
				Lang = null;
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		if (FCode.equalsIgnoreCase("LostTimes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				LostTimes = i;
			}
		}
		if (FCode.equalsIgnoreCase("PrintCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PrintCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("RegetDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RegetDate = fDate.getDate( FValue );
			}
			else
				RegetDate = null;
		}
		if (FCode.equalsIgnoreCase("LastEdorDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LastEdorDate = fDate.getDate( FValue );
			}
			else
				LastEdorDate = null;
		}
		if (FCode.equalsIgnoreCase("LastGetDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LastGetDate = fDate.getDate( FValue );
			}
			else
				LastGetDate = null;
		}
		if (FCode.equalsIgnoreCase("LastLoanDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LastLoanDate = fDate.getDate( FValue );
			}
			else
				LastLoanDate = null;
		}
		if (FCode.equalsIgnoreCase("SpecFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SpecFlag = FValue.trim();
			}
			else
				SpecFlag = null;
		}
		if (FCode.equalsIgnoreCase("GrpSpec"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpSpec = FValue.trim();
			}
			else
				GrpSpec = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("SignCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SignCom = FValue.trim();
			}
			else
				SignCom = null;
		}
		if (FCode.equalsIgnoreCase("SignDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SignDate = fDate.getDate( FValue );
			}
			else
				SignDate = null;
		}
		if (FCode.equalsIgnoreCase("SignTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SignTime = FValue.trim();
			}
			else
				SignTime = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("ManageFeeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ManageFeeRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExpPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ExpPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("ExpPremium"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExpPremium = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExpAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExpAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("Peoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Peoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("Mult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Mult = d;
			}
		}
		if (FCode.equalsIgnoreCase("Prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Amnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("Dif"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Dif = d;
			}
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag1 = FValue.trim();
			}
			else
				StandbyFlag1 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag2 = FValue.trim();
			}
			else
				StandbyFlag2 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag3 = FValue.trim();
			}
			else
				StandbyFlag3 = null;
		}
		if (FCode.equalsIgnoreCase("InputOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputOperator = FValue.trim();
			}
			else
				InputOperator = null;
		}
		if (FCode.equalsIgnoreCase("InputDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InputDate = fDate.getDate( FValue );
			}
			else
				InputDate = null;
		}
		if (FCode.equalsIgnoreCase("InputTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputTime = FValue.trim();
			}
			else
				InputTime = null;
		}
		if (FCode.equalsIgnoreCase("ApproveFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApproveFlag = FValue.trim();
			}
			else
				ApproveFlag = null;
		}
		if (FCode.equalsIgnoreCase("ApproveCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApproveCode = FValue.trim();
			}
			else
				ApproveCode = null;
		}
		if (FCode.equalsIgnoreCase("ApproveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ApproveDate = fDate.getDate( FValue );
			}
			else
				ApproveDate = null;
		}
		if (FCode.equalsIgnoreCase("ApproveTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApproveTime = FValue.trim();
			}
			else
				ApproveTime = null;
		}
		if (FCode.equalsIgnoreCase("UWOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWOperator = FValue.trim();
			}
			else
				UWOperator = null;
		}
		if (FCode.equalsIgnoreCase("UWFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWFlag = FValue.trim();
			}
			else
				UWFlag = null;
		}
		if (FCode.equalsIgnoreCase("UWDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				UWDate = fDate.getDate( FValue );
			}
			else
				UWDate = null;
		}
		if (FCode.equalsIgnoreCase("UWTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWTime = FValue.trim();
			}
			else
				UWTime = null;
		}
		if (FCode.equalsIgnoreCase("AppFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppFlag = FValue.trim();
			}
			else
				AppFlag = null;
		}
		if (FCode.equalsIgnoreCase("PolApplyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PolApplyDate = fDate.getDate( FValue );
			}
			else
				PolApplyDate = null;
		}
		if (FCode.equalsIgnoreCase("CustomGetPolDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CustomGetPolDate = fDate.getDate( FValue );
			}
			else
				CustomGetPolDate = null;
		}
		if (FCode.equalsIgnoreCase("GetPolDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GetPolDate = fDate.getDate( FValue );
			}
			else
				GetPolDate = null;
		}
		if (FCode.equalsIgnoreCase("GetPolTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetPolTime = FValue.trim();
			}
			else
				GetPolTime = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("EnterKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EnterKind = FValue.trim();
			}
			else
				EnterKind = null;
		}
		if (FCode.equalsIgnoreCase("AmntGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AmntGrade = FValue.trim();
			}
			else
				AmntGrade = null;
		}
		if (FCode.equalsIgnoreCase("Peoples3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Peoples3 = i;
			}
		}
		if (FCode.equalsIgnoreCase("OnWorkPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OnWorkPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("OffWorkPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OffWorkPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("OtherPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OtherPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("RelaPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RelaPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("RelaMatePeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RelaMatePeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("RelaYoungPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RelaYoungPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("RelaOtherPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RelaOtherPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("FirstTrialOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTrialOperator = FValue.trim();
			}
			else
				FirstTrialOperator = null;
		}
		if (FCode.equalsIgnoreCase("FirstTrialDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FirstTrialDate = fDate.getDate( FValue );
			}
			else
				FirstTrialDate = null;
		}
		if (FCode.equalsIgnoreCase("FirstTrialTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstTrialTime = FValue.trim();
			}
			else
				FirstTrialTime = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveOperator = FValue.trim();
			}
			else
				ReceiveOperator = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ReceiveDate = fDate.getDate( FValue );
			}
			else
				ReceiveDate = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveTime = FValue.trim();
			}
			else
				ReceiveTime = null;
		}
		if (FCode.equalsIgnoreCase("TempFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempFeeNo = FValue.trim();
			}
			else
				TempFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("HandlerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerName = FValue.trim();
			}
			else
				HandlerName = null;
		}
		if (FCode.equalsIgnoreCase("HandlerDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HandlerDate = fDate.getDate( FValue );
			}
			else
				HandlerDate = null;
		}
		if (FCode.equalsIgnoreCase("HandlerPrint"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerPrint = FValue.trim();
			}
			else
				HandlerPrint = null;
		}
		if (FCode.equalsIgnoreCase("AgentDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AgentDate = fDate.getDate( FValue );
			}
			else
				AgentDate = null;
		}
		if (FCode.equalsIgnoreCase("BusinessBigType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessBigType = FValue.trim();
			}
			else
				BusinessBigType = null;
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("ProposalType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalType = FValue.trim();
			}
			else
				ProposalType = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnlDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnlDetail = FValue.trim();
			}
			else
				SaleChnlDetail = null;
		}
		if (FCode.equalsIgnoreCase("ContPrintLoFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPrintLoFlag = FValue.trim();
			}
			else
				ContPrintLoFlag = null;
		}
		if (FCode.equalsIgnoreCase("PremApportFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremApportFlag = FValue.trim();
			}
			else
				PremApportFlag = null;
		}
		if (FCode.equalsIgnoreCase("ContPremFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPremFeeNo = FValue.trim();
			}
			else
				ContPremFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerReceiptNo = FValue.trim();
			}
			else
				CustomerReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("CInValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CInValiDate = fDate.getDate( FValue );
			}
			else
				CInValiDate = null;
		}
		if (FCode.equalsIgnoreCase("RoleAgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RoleAgentCode = FValue.trim();
			}
			else
				RoleAgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AskGrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AskGrpContNo = FValue.trim();
			}
			else
				AskGrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("Copys"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Copys = d;
			}
		}
		if (FCode.equalsIgnoreCase("OperationManager"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OperationManager = FValue.trim();
			}
			else
				OperationManager = null;
		}
		if (FCode.equalsIgnoreCase("InfoSource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InfoSource = FValue.trim();
			}
			else
				InfoSource = null;
		}
		if (FCode.equalsIgnoreCase("PremScope"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PremScope = d;
			}
		}
		if (FCode.equalsIgnoreCase("DegreeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DegreeType = FValue.trim();
			}
			else
				DegreeType = null;
		}
		if (FCode.equalsIgnoreCase("CardFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CardFlag = FValue.trim();
			}
			else
				CardFlag = null;
		}
		if (FCode.equalsIgnoreCase("BigProjectFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BigProjectFlag = FValue.trim();
			}
			else
				BigProjectFlag = null;
		}
		if (FCode.equalsIgnoreCase("ContPrintType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPrintType = FValue.trim();
			}
			else
				ContPrintType = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCom = FValue.trim();
			}
			else
				GrpAgentCom = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCode = FValue.trim();
			}
			else
				GrpAgentCode = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentName = FValue.trim();
			}
			else
				GrpAgentName = null;
		}
		if (FCode.equalsIgnoreCase("CTCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				CTCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("CoInsuranceFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CoInsuranceFlag = FValue.trim();
			}
			else
				CoInsuranceFlag = null;
		}
		if (FCode.equalsIgnoreCase("CityInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CityInfo = FValue.trim();
			}
			else
				CityInfo = null;
		}
		if (FCode.equalsIgnoreCase("Crs_SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_SaleChnl = FValue.trim();
			}
			else
				Crs_SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("Crs_BussType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_BussType = FValue.trim();
			}
			else
				Crs_BussType = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentIDNo = FValue.trim();
			}
			else
				GrpAgentIDNo = null;
		}
		if (FCode.equalsIgnoreCase("AgentSaleCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentSaleCode = FValue.trim();
			}
			else
				AgentSaleCode = null;
		}
		if (FCode.equalsIgnoreCase("HandlerIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandlerIDNo = FValue.trim();
			}
			else
				HandlerIDNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCGrpContSchema other = (LCGrpContSchema)otherObject;
		return
			(GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (ProposalGrpContNo == null ? other.getProposalGrpContNo() == null : ProposalGrpContNo.equals(other.getProposalGrpContNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentType == null ? other.getAgentType() == null : AgentType.equals(other.getAgentType()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (AgentCode1 == null ? other.getAgentCode1() == null : AgentCode1.equals(other.getAgentCode1()))
			&& (Password == null ? other.getPassword() == null : Password.equals(other.getPassword()))
			&& (Password2 == null ? other.getPassword2() == null : Password2.equals(other.getPassword2()))
			&& (AppntNo == null ? other.getAppntNo() == null : AppntNo.equals(other.getAppntNo()))
			&& (AddressNo == null ? other.getAddressNo() == null : AddressNo.equals(other.getAddressNo()))
			&& Peoples2 == other.getPeoples2()
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (BusinessType == null ? other.getBusinessType() == null : BusinessType.equals(other.getBusinessType()))
			&& (GrpNature == null ? other.getGrpNature() == null : GrpNature.equals(other.getGrpNature()))
			&& RgtMoney == other.getRgtMoney()
			&& Asset == other.getAsset()
			&& NetProfitRate == other.getNetProfitRate()
			&& (MainBussiness == null ? other.getMainBussiness() == null : MainBussiness.equals(other.getMainBussiness()))
			&& (Corporation == null ? other.getCorporation() == null : Corporation.equals(other.getCorporation()))
			&& (ComAera == null ? other.getComAera() == null : ComAera.equals(other.getComAera()))
			&& (Fax == null ? other.getFax() == null : Fax.equals(other.getFax()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (GetFlag == null ? other.getGetFlag() == null : GetFlag.equals(other.getGetFlag()))
			&& (Satrap == null ? other.getSatrap() == null : Satrap.equals(other.getSatrap()))
			&& (EMail == null ? other.getEMail() == null : EMail.equals(other.getEMail()))
			&& (FoundDate == null ? other.getFoundDate() == null : fDate.getString(FoundDate).equals(other.getFoundDate()))
			&& (GrpGroupNo == null ? other.getGrpGroupNo() == null : GrpGroupNo.equals(other.getGrpGroupNo()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (DisputedFlag == null ? other.getDisputedFlag() == null : DisputedFlag.equals(other.getDisputedFlag()))
			&& (OutPayFlag == null ? other.getOutPayFlag() == null : OutPayFlag.equals(other.getOutPayFlag()))
			&& (GetPolMode == null ? other.getGetPolMode() == null : GetPolMode.equals(other.getGetPolMode()))
			&& (Lang == null ? other.getLang() == null : Lang.equals(other.getLang()))
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()))
			&& LostTimes == other.getLostTimes()
			&& PrintCount == other.getPrintCount()
			&& (RegetDate == null ? other.getRegetDate() == null : fDate.getString(RegetDate).equals(other.getRegetDate()))
			&& (LastEdorDate == null ? other.getLastEdorDate() == null : fDate.getString(LastEdorDate).equals(other.getLastEdorDate()))
			&& (LastGetDate == null ? other.getLastGetDate() == null : fDate.getString(LastGetDate).equals(other.getLastGetDate()))
			&& (LastLoanDate == null ? other.getLastLoanDate() == null : fDate.getString(LastLoanDate).equals(other.getLastLoanDate()))
			&& (SpecFlag == null ? other.getSpecFlag() == null : SpecFlag.equals(other.getSpecFlag()))
			&& (GrpSpec == null ? other.getGrpSpec() == null : GrpSpec.equals(other.getGrpSpec()))
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (SignCom == null ? other.getSignCom() == null : SignCom.equals(other.getSignCom()))
			&& (SignDate == null ? other.getSignDate() == null : fDate.getString(SignDate).equals(other.getSignDate()))
			&& (SignTime == null ? other.getSignTime() == null : SignTime.equals(other.getSignTime()))
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& PayIntv == other.getPayIntv()
			&& ManageFeeRate == other.getManageFeeRate()
			&& ExpPeoples == other.getExpPeoples()
			&& ExpPremium == other.getExpPremium()
			&& ExpAmnt == other.getExpAmnt()
			&& Peoples == other.getPeoples()
			&& Mult == other.getMult()
			&& Prem == other.getPrem()
			&& Amnt == other.getAmnt()
			&& SumPrem == other.getSumPrem()
			&& SumPay == other.getSumPay()
			&& Dif == other.getDif()
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (StandbyFlag1 == null ? other.getStandbyFlag1() == null : StandbyFlag1.equals(other.getStandbyFlag1()))
			&& (StandbyFlag2 == null ? other.getStandbyFlag2() == null : StandbyFlag2.equals(other.getStandbyFlag2()))
			&& (StandbyFlag3 == null ? other.getStandbyFlag3() == null : StandbyFlag3.equals(other.getStandbyFlag3()))
			&& (InputOperator == null ? other.getInputOperator() == null : InputOperator.equals(other.getInputOperator()))
			&& (InputDate == null ? other.getInputDate() == null : fDate.getString(InputDate).equals(other.getInputDate()))
			&& (InputTime == null ? other.getInputTime() == null : InputTime.equals(other.getInputTime()))
			&& (ApproveFlag == null ? other.getApproveFlag() == null : ApproveFlag.equals(other.getApproveFlag()))
			&& (ApproveCode == null ? other.getApproveCode() == null : ApproveCode.equals(other.getApproveCode()))
			&& (ApproveDate == null ? other.getApproveDate() == null : fDate.getString(ApproveDate).equals(other.getApproveDate()))
			&& (ApproveTime == null ? other.getApproveTime() == null : ApproveTime.equals(other.getApproveTime()))
			&& (UWOperator == null ? other.getUWOperator() == null : UWOperator.equals(other.getUWOperator()))
			&& (UWFlag == null ? other.getUWFlag() == null : UWFlag.equals(other.getUWFlag()))
			&& (UWDate == null ? other.getUWDate() == null : fDate.getString(UWDate).equals(other.getUWDate()))
			&& (UWTime == null ? other.getUWTime() == null : UWTime.equals(other.getUWTime()))
			&& (AppFlag == null ? other.getAppFlag() == null : AppFlag.equals(other.getAppFlag()))
			&& (PolApplyDate == null ? other.getPolApplyDate() == null : fDate.getString(PolApplyDate).equals(other.getPolApplyDate()))
			&& (CustomGetPolDate == null ? other.getCustomGetPolDate() == null : fDate.getString(CustomGetPolDate).equals(other.getCustomGetPolDate()))
			&& (GetPolDate == null ? other.getGetPolDate() == null : fDate.getString(GetPolDate).equals(other.getGetPolDate()))
			&& (GetPolTime == null ? other.getGetPolTime() == null : GetPolTime.equals(other.getGetPolTime()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (EnterKind == null ? other.getEnterKind() == null : EnterKind.equals(other.getEnterKind()))
			&& (AmntGrade == null ? other.getAmntGrade() == null : AmntGrade.equals(other.getAmntGrade()))
			&& Peoples3 == other.getPeoples3()
			&& OnWorkPeoples == other.getOnWorkPeoples()
			&& OffWorkPeoples == other.getOffWorkPeoples()
			&& OtherPeoples == other.getOtherPeoples()
			&& RelaPeoples == other.getRelaPeoples()
			&& RelaMatePeoples == other.getRelaMatePeoples()
			&& RelaYoungPeoples == other.getRelaYoungPeoples()
			&& RelaOtherPeoples == other.getRelaOtherPeoples()
			&& (FirstTrialOperator == null ? other.getFirstTrialOperator() == null : FirstTrialOperator.equals(other.getFirstTrialOperator()))
			&& (FirstTrialDate == null ? other.getFirstTrialDate() == null : fDate.getString(FirstTrialDate).equals(other.getFirstTrialDate()))
			&& (FirstTrialTime == null ? other.getFirstTrialTime() == null : FirstTrialTime.equals(other.getFirstTrialTime()))
			&& (ReceiveOperator == null ? other.getReceiveOperator() == null : ReceiveOperator.equals(other.getReceiveOperator()))
			&& (ReceiveDate == null ? other.getReceiveDate() == null : fDate.getString(ReceiveDate).equals(other.getReceiveDate()))
			&& (ReceiveTime == null ? other.getReceiveTime() == null : ReceiveTime.equals(other.getReceiveTime()))
			&& (TempFeeNo == null ? other.getTempFeeNo() == null : TempFeeNo.equals(other.getTempFeeNo()))
			&& (HandlerName == null ? other.getHandlerName() == null : HandlerName.equals(other.getHandlerName()))
			&& (HandlerDate == null ? other.getHandlerDate() == null : fDate.getString(HandlerDate).equals(other.getHandlerDate()))
			&& (HandlerPrint == null ? other.getHandlerPrint() == null : HandlerPrint.equals(other.getHandlerPrint()))
			&& (AgentDate == null ? other.getAgentDate() == null : fDate.getString(AgentDate).equals(other.getAgentDate()))
			&& (BusinessBigType == null ? other.getBusinessBigType() == null : BusinessBigType.equals(other.getBusinessBigType()))
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (ProposalType == null ? other.getProposalType() == null : ProposalType.equals(other.getProposalType()))
			&& (SaleChnlDetail == null ? other.getSaleChnlDetail() == null : SaleChnlDetail.equals(other.getSaleChnlDetail()))
			&& (ContPrintLoFlag == null ? other.getContPrintLoFlag() == null : ContPrintLoFlag.equals(other.getContPrintLoFlag()))
			&& (PremApportFlag == null ? other.getPremApportFlag() == null : PremApportFlag.equals(other.getPremApportFlag()))
			&& (ContPremFeeNo == null ? other.getContPremFeeNo() == null : ContPremFeeNo.equals(other.getContPremFeeNo()))
			&& (CustomerReceiptNo == null ? other.getCustomerReceiptNo() == null : CustomerReceiptNo.equals(other.getCustomerReceiptNo()))
			&& (CInValiDate == null ? other.getCInValiDate() == null : fDate.getString(CInValiDate).equals(other.getCInValiDate()))
			&& (RoleAgentCode == null ? other.getRoleAgentCode() == null : RoleAgentCode.equals(other.getRoleAgentCode()))
			&& (AskGrpContNo == null ? other.getAskGrpContNo() == null : AskGrpContNo.equals(other.getAskGrpContNo()))
			&& Copys == other.getCopys()
			&& (OperationManager == null ? other.getOperationManager() == null : OperationManager.equals(other.getOperationManager()))
			&& (InfoSource == null ? other.getInfoSource() == null : InfoSource.equals(other.getInfoSource()))
			&& PremScope == other.getPremScope()
			&& (DegreeType == null ? other.getDegreeType() == null : DegreeType.equals(other.getDegreeType()))
			&& (CardFlag == null ? other.getCardFlag() == null : CardFlag.equals(other.getCardFlag()))
			&& (BigProjectFlag == null ? other.getBigProjectFlag() == null : BigProjectFlag.equals(other.getBigProjectFlag()))
			&& (ContPrintType == null ? other.getContPrintType() == null : ContPrintType.equals(other.getContPrintType()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& (GrpAgentCom == null ? other.getGrpAgentCom() == null : GrpAgentCom.equals(other.getGrpAgentCom()))
			&& (GrpAgentCode == null ? other.getGrpAgentCode() == null : GrpAgentCode.equals(other.getGrpAgentCode()))
			&& (GrpAgentName == null ? other.getGrpAgentName() == null : GrpAgentName.equals(other.getGrpAgentName()))
			&& CTCount == other.getCTCount()
			&& (CoInsuranceFlag == null ? other.getCoInsuranceFlag() == null : CoInsuranceFlag.equals(other.getCoInsuranceFlag()))
			&& (CityInfo == null ? other.getCityInfo() == null : CityInfo.equals(other.getCityInfo()))
			&& (Crs_SaleChnl == null ? other.getCrs_SaleChnl() == null : Crs_SaleChnl.equals(other.getCrs_SaleChnl()))
			&& (Crs_BussType == null ? other.getCrs_BussType() == null : Crs_BussType.equals(other.getCrs_BussType()))
			&& (GrpAgentIDNo == null ? other.getGrpAgentIDNo() == null : GrpAgentIDNo.equals(other.getGrpAgentIDNo()))
			&& (AgentSaleCode == null ? other.getAgentSaleCode() == null : AgentSaleCode.equals(other.getAgentSaleCode()))
			&& (HandlerIDNo == null ? other.getHandlerIDNo() == null : HandlerIDNo.equals(other.getHandlerIDNo()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("ProposalGrpContNo") ) {
			return 1;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 2;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 3;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 4;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 5;
		}
		if( strFieldName.equals("AgentType") ) {
			return 6;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 7;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 8;
		}
		if( strFieldName.equals("AgentCode1") ) {
			return 9;
		}
		if( strFieldName.equals("Password") ) {
			return 10;
		}
		if( strFieldName.equals("Password2") ) {
			return 11;
		}
		if( strFieldName.equals("AppntNo") ) {
			return 12;
		}
		if( strFieldName.equals("AddressNo") ) {
			return 13;
		}
		if( strFieldName.equals("Peoples2") ) {
			return 14;
		}
		if( strFieldName.equals("GrpName") ) {
			return 15;
		}
		if( strFieldName.equals("BusinessType") ) {
			return 16;
		}
		if( strFieldName.equals("GrpNature") ) {
			return 17;
		}
		if( strFieldName.equals("RgtMoney") ) {
			return 18;
		}
		if( strFieldName.equals("Asset") ) {
			return 19;
		}
		if( strFieldName.equals("NetProfitRate") ) {
			return 20;
		}
		if( strFieldName.equals("MainBussiness") ) {
			return 21;
		}
		if( strFieldName.equals("Corporation") ) {
			return 22;
		}
		if( strFieldName.equals("ComAera") ) {
			return 23;
		}
		if( strFieldName.equals("Fax") ) {
			return 24;
		}
		if( strFieldName.equals("Phone") ) {
			return 25;
		}
		if( strFieldName.equals("GetFlag") ) {
			return 26;
		}
		if( strFieldName.equals("Satrap") ) {
			return 27;
		}
		if( strFieldName.equals("EMail") ) {
			return 28;
		}
		if( strFieldName.equals("FoundDate") ) {
			return 29;
		}
		if( strFieldName.equals("GrpGroupNo") ) {
			return 30;
		}
		if( strFieldName.equals("BankCode") ) {
			return 31;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 32;
		}
		if( strFieldName.equals("AccName") ) {
			return 33;
		}
		if( strFieldName.equals("DisputedFlag") ) {
			return 34;
		}
		if( strFieldName.equals("OutPayFlag") ) {
			return 35;
		}
		if( strFieldName.equals("GetPolMode") ) {
			return 36;
		}
		if( strFieldName.equals("Lang") ) {
			return 37;
		}
		if( strFieldName.equals("Currency") ) {
			return 38;
		}
		if( strFieldName.equals("LostTimes") ) {
			return 39;
		}
		if( strFieldName.equals("PrintCount") ) {
			return 40;
		}
		if( strFieldName.equals("RegetDate") ) {
			return 41;
		}
		if( strFieldName.equals("LastEdorDate") ) {
			return 42;
		}
		if( strFieldName.equals("LastGetDate") ) {
			return 43;
		}
		if( strFieldName.equals("LastLoanDate") ) {
			return 44;
		}
		if( strFieldName.equals("SpecFlag") ) {
			return 45;
		}
		if( strFieldName.equals("GrpSpec") ) {
			return 46;
		}
		if( strFieldName.equals("PayMode") ) {
			return 47;
		}
		if( strFieldName.equals("SignCom") ) {
			return 48;
		}
		if( strFieldName.equals("SignDate") ) {
			return 49;
		}
		if( strFieldName.equals("SignTime") ) {
			return 50;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 51;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 52;
		}
		if( strFieldName.equals("ManageFeeRate") ) {
			return 53;
		}
		if( strFieldName.equals("ExpPeoples") ) {
			return 54;
		}
		if( strFieldName.equals("ExpPremium") ) {
			return 55;
		}
		if( strFieldName.equals("ExpAmnt") ) {
			return 56;
		}
		if( strFieldName.equals("Peoples") ) {
			return 57;
		}
		if( strFieldName.equals("Mult") ) {
			return 58;
		}
		if( strFieldName.equals("Prem") ) {
			return 59;
		}
		if( strFieldName.equals("Amnt") ) {
			return 60;
		}
		if( strFieldName.equals("SumPrem") ) {
			return 61;
		}
		if( strFieldName.equals("SumPay") ) {
			return 62;
		}
		if( strFieldName.equals("Dif") ) {
			return 63;
		}
		if( strFieldName.equals("Remark") ) {
			return 64;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return 65;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return 66;
		}
		if( strFieldName.equals("StandbyFlag3") ) {
			return 67;
		}
		if( strFieldName.equals("InputOperator") ) {
			return 68;
		}
		if( strFieldName.equals("InputDate") ) {
			return 69;
		}
		if( strFieldName.equals("InputTime") ) {
			return 70;
		}
		if( strFieldName.equals("ApproveFlag") ) {
			return 71;
		}
		if( strFieldName.equals("ApproveCode") ) {
			return 72;
		}
		if( strFieldName.equals("ApproveDate") ) {
			return 73;
		}
		if( strFieldName.equals("ApproveTime") ) {
			return 74;
		}
		if( strFieldName.equals("UWOperator") ) {
			return 75;
		}
		if( strFieldName.equals("UWFlag") ) {
			return 76;
		}
		if( strFieldName.equals("UWDate") ) {
			return 77;
		}
		if( strFieldName.equals("UWTime") ) {
			return 78;
		}
		if( strFieldName.equals("AppFlag") ) {
			return 79;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return 80;
		}
		if( strFieldName.equals("CustomGetPolDate") ) {
			return 81;
		}
		if( strFieldName.equals("GetPolDate") ) {
			return 82;
		}
		if( strFieldName.equals("GetPolTime") ) {
			return 83;
		}
		if( strFieldName.equals("State") ) {
			return 84;
		}
		if( strFieldName.equals("Operator") ) {
			return 85;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 86;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 87;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 88;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 89;
		}
		if( strFieldName.equals("EnterKind") ) {
			return 90;
		}
		if( strFieldName.equals("AmntGrade") ) {
			return 91;
		}
		if( strFieldName.equals("Peoples3") ) {
			return 92;
		}
		if( strFieldName.equals("OnWorkPeoples") ) {
			return 93;
		}
		if( strFieldName.equals("OffWorkPeoples") ) {
			return 94;
		}
		if( strFieldName.equals("OtherPeoples") ) {
			return 95;
		}
		if( strFieldName.equals("RelaPeoples") ) {
			return 96;
		}
		if( strFieldName.equals("RelaMatePeoples") ) {
			return 97;
		}
		if( strFieldName.equals("RelaYoungPeoples") ) {
			return 98;
		}
		if( strFieldName.equals("RelaOtherPeoples") ) {
			return 99;
		}
		if( strFieldName.equals("FirstTrialOperator") ) {
			return 100;
		}
		if( strFieldName.equals("FirstTrialDate") ) {
			return 101;
		}
		if( strFieldName.equals("FirstTrialTime") ) {
			return 102;
		}
		if( strFieldName.equals("ReceiveOperator") ) {
			return 103;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return 104;
		}
		if( strFieldName.equals("ReceiveTime") ) {
			return 105;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return 106;
		}
		if( strFieldName.equals("HandlerName") ) {
			return 107;
		}
		if( strFieldName.equals("HandlerDate") ) {
			return 108;
		}
		if( strFieldName.equals("HandlerPrint") ) {
			return 109;
		}
		if( strFieldName.equals("AgentDate") ) {
			return 110;
		}
		if( strFieldName.equals("BusinessBigType") ) {
			return 111;
		}
		if( strFieldName.equals("MarketType") ) {
			return 112;
		}
		if( strFieldName.equals("ProposalType") ) {
			return 113;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return 114;
		}
		if( strFieldName.equals("ContPrintLoFlag") ) {
			return 115;
		}
		if( strFieldName.equals("PremApportFlag") ) {
			return 116;
		}
		if( strFieldName.equals("ContPremFeeNo") ) {
			return 117;
		}
		if( strFieldName.equals("CustomerReceiptNo") ) {
			return 118;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return 119;
		}
		if( strFieldName.equals("RoleAgentCode") ) {
			return 120;
		}
		if( strFieldName.equals("AskGrpContNo") ) {
			return 121;
		}
		if( strFieldName.equals("Copys") ) {
			return 122;
		}
		if( strFieldName.equals("OperationManager") ) {
			return 123;
		}
		if( strFieldName.equals("InfoSource") ) {
			return 124;
		}
		if( strFieldName.equals("PremScope") ) {
			return 125;
		}
		if( strFieldName.equals("DegreeType") ) {
			return 126;
		}
		if( strFieldName.equals("CardFlag") ) {
			return 127;
		}
		if( strFieldName.equals("BigProjectFlag") ) {
			return 128;
		}
		if( strFieldName.equals("ContPrintType") ) {
			return 129;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 130;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return 131;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return 132;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return 133;
		}
		if( strFieldName.equals("CTCount") ) {
			return 134;
		}
		if( strFieldName.equals("CoInsuranceFlag") ) {
			return 135;
		}
		if( strFieldName.equals("CityInfo") ) {
			return 136;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return 137;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return 138;
		}
		if( strFieldName.equals("GrpAgentIDNo") ) {
			return 139;
		}
		if( strFieldName.equals("AgentSaleCode") ) {
			return 140;
		}
		if( strFieldName.equals("HandlerIDNo") ) {
			return 141;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "ProposalGrpContNo";
				break;
			case 2:
				strFieldName = "PrtNo";
				break;
			case 3:
				strFieldName = "SaleChnl";
				break;
			case 4:
				strFieldName = "ManageCom";
				break;
			case 5:
				strFieldName = "AgentCom";
				break;
			case 6:
				strFieldName = "AgentType";
				break;
			case 7:
				strFieldName = "AgentCode";
				break;
			case 8:
				strFieldName = "AgentGroup";
				break;
			case 9:
				strFieldName = "AgentCode1";
				break;
			case 10:
				strFieldName = "Password";
				break;
			case 11:
				strFieldName = "Password2";
				break;
			case 12:
				strFieldName = "AppntNo";
				break;
			case 13:
				strFieldName = "AddressNo";
				break;
			case 14:
				strFieldName = "Peoples2";
				break;
			case 15:
				strFieldName = "GrpName";
				break;
			case 16:
				strFieldName = "BusinessType";
				break;
			case 17:
				strFieldName = "GrpNature";
				break;
			case 18:
				strFieldName = "RgtMoney";
				break;
			case 19:
				strFieldName = "Asset";
				break;
			case 20:
				strFieldName = "NetProfitRate";
				break;
			case 21:
				strFieldName = "MainBussiness";
				break;
			case 22:
				strFieldName = "Corporation";
				break;
			case 23:
				strFieldName = "ComAera";
				break;
			case 24:
				strFieldName = "Fax";
				break;
			case 25:
				strFieldName = "Phone";
				break;
			case 26:
				strFieldName = "GetFlag";
				break;
			case 27:
				strFieldName = "Satrap";
				break;
			case 28:
				strFieldName = "EMail";
				break;
			case 29:
				strFieldName = "FoundDate";
				break;
			case 30:
				strFieldName = "GrpGroupNo";
				break;
			case 31:
				strFieldName = "BankCode";
				break;
			case 32:
				strFieldName = "BankAccNo";
				break;
			case 33:
				strFieldName = "AccName";
				break;
			case 34:
				strFieldName = "DisputedFlag";
				break;
			case 35:
				strFieldName = "OutPayFlag";
				break;
			case 36:
				strFieldName = "GetPolMode";
				break;
			case 37:
				strFieldName = "Lang";
				break;
			case 38:
				strFieldName = "Currency";
				break;
			case 39:
				strFieldName = "LostTimes";
				break;
			case 40:
				strFieldName = "PrintCount";
				break;
			case 41:
				strFieldName = "RegetDate";
				break;
			case 42:
				strFieldName = "LastEdorDate";
				break;
			case 43:
				strFieldName = "LastGetDate";
				break;
			case 44:
				strFieldName = "LastLoanDate";
				break;
			case 45:
				strFieldName = "SpecFlag";
				break;
			case 46:
				strFieldName = "GrpSpec";
				break;
			case 47:
				strFieldName = "PayMode";
				break;
			case 48:
				strFieldName = "SignCom";
				break;
			case 49:
				strFieldName = "SignDate";
				break;
			case 50:
				strFieldName = "SignTime";
				break;
			case 51:
				strFieldName = "CValiDate";
				break;
			case 52:
				strFieldName = "PayIntv";
				break;
			case 53:
				strFieldName = "ManageFeeRate";
				break;
			case 54:
				strFieldName = "ExpPeoples";
				break;
			case 55:
				strFieldName = "ExpPremium";
				break;
			case 56:
				strFieldName = "ExpAmnt";
				break;
			case 57:
				strFieldName = "Peoples";
				break;
			case 58:
				strFieldName = "Mult";
				break;
			case 59:
				strFieldName = "Prem";
				break;
			case 60:
				strFieldName = "Amnt";
				break;
			case 61:
				strFieldName = "SumPrem";
				break;
			case 62:
				strFieldName = "SumPay";
				break;
			case 63:
				strFieldName = "Dif";
				break;
			case 64:
				strFieldName = "Remark";
				break;
			case 65:
				strFieldName = "StandbyFlag1";
				break;
			case 66:
				strFieldName = "StandbyFlag2";
				break;
			case 67:
				strFieldName = "StandbyFlag3";
				break;
			case 68:
				strFieldName = "InputOperator";
				break;
			case 69:
				strFieldName = "InputDate";
				break;
			case 70:
				strFieldName = "InputTime";
				break;
			case 71:
				strFieldName = "ApproveFlag";
				break;
			case 72:
				strFieldName = "ApproveCode";
				break;
			case 73:
				strFieldName = "ApproveDate";
				break;
			case 74:
				strFieldName = "ApproveTime";
				break;
			case 75:
				strFieldName = "UWOperator";
				break;
			case 76:
				strFieldName = "UWFlag";
				break;
			case 77:
				strFieldName = "UWDate";
				break;
			case 78:
				strFieldName = "UWTime";
				break;
			case 79:
				strFieldName = "AppFlag";
				break;
			case 80:
				strFieldName = "PolApplyDate";
				break;
			case 81:
				strFieldName = "CustomGetPolDate";
				break;
			case 82:
				strFieldName = "GetPolDate";
				break;
			case 83:
				strFieldName = "GetPolTime";
				break;
			case 84:
				strFieldName = "State";
				break;
			case 85:
				strFieldName = "Operator";
				break;
			case 86:
				strFieldName = "MakeDate";
				break;
			case 87:
				strFieldName = "MakeTime";
				break;
			case 88:
				strFieldName = "ModifyDate";
				break;
			case 89:
				strFieldName = "ModifyTime";
				break;
			case 90:
				strFieldName = "EnterKind";
				break;
			case 91:
				strFieldName = "AmntGrade";
				break;
			case 92:
				strFieldName = "Peoples3";
				break;
			case 93:
				strFieldName = "OnWorkPeoples";
				break;
			case 94:
				strFieldName = "OffWorkPeoples";
				break;
			case 95:
				strFieldName = "OtherPeoples";
				break;
			case 96:
				strFieldName = "RelaPeoples";
				break;
			case 97:
				strFieldName = "RelaMatePeoples";
				break;
			case 98:
				strFieldName = "RelaYoungPeoples";
				break;
			case 99:
				strFieldName = "RelaOtherPeoples";
				break;
			case 100:
				strFieldName = "FirstTrialOperator";
				break;
			case 101:
				strFieldName = "FirstTrialDate";
				break;
			case 102:
				strFieldName = "FirstTrialTime";
				break;
			case 103:
				strFieldName = "ReceiveOperator";
				break;
			case 104:
				strFieldName = "ReceiveDate";
				break;
			case 105:
				strFieldName = "ReceiveTime";
				break;
			case 106:
				strFieldName = "TempFeeNo";
				break;
			case 107:
				strFieldName = "HandlerName";
				break;
			case 108:
				strFieldName = "HandlerDate";
				break;
			case 109:
				strFieldName = "HandlerPrint";
				break;
			case 110:
				strFieldName = "AgentDate";
				break;
			case 111:
				strFieldName = "BusinessBigType";
				break;
			case 112:
				strFieldName = "MarketType";
				break;
			case 113:
				strFieldName = "ProposalType";
				break;
			case 114:
				strFieldName = "SaleChnlDetail";
				break;
			case 115:
				strFieldName = "ContPrintLoFlag";
				break;
			case 116:
				strFieldName = "PremApportFlag";
				break;
			case 117:
				strFieldName = "ContPremFeeNo";
				break;
			case 118:
				strFieldName = "CustomerReceiptNo";
				break;
			case 119:
				strFieldName = "CInValiDate";
				break;
			case 120:
				strFieldName = "RoleAgentCode";
				break;
			case 121:
				strFieldName = "AskGrpContNo";
				break;
			case 122:
				strFieldName = "Copys";
				break;
			case 123:
				strFieldName = "OperationManager";
				break;
			case 124:
				strFieldName = "InfoSource";
				break;
			case 125:
				strFieldName = "PremScope";
				break;
			case 126:
				strFieldName = "DegreeType";
				break;
			case 127:
				strFieldName = "CardFlag";
				break;
			case 128:
				strFieldName = "BigProjectFlag";
				break;
			case 129:
				strFieldName = "ContPrintType";
				break;
			case 130:
				strFieldName = "StateFlag";
				break;
			case 131:
				strFieldName = "GrpAgentCom";
				break;
			case 132:
				strFieldName = "GrpAgentCode";
				break;
			case 133:
				strFieldName = "GrpAgentName";
				break;
			case 134:
				strFieldName = "CTCount";
				break;
			case 135:
				strFieldName = "CoInsuranceFlag";
				break;
			case 136:
				strFieldName = "CityInfo";
				break;
			case 137:
				strFieldName = "Crs_SaleChnl";
				break;
			case 138:
				strFieldName = "Crs_BussType";
				break;
			case 139:
				strFieldName = "GrpAgentIDNo";
				break;
			case 140:
				strFieldName = "AgentSaleCode";
				break;
			case 141:
				strFieldName = "HandlerIDNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalGrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Password") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Password2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AddressNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Peoples2") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpNature") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Asset") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("NetProfitRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MainBussiness") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Corporation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComAera") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Fax") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Satrap") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EMail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FoundDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GrpGroupNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DisputedFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OutPayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetPolMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Lang") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LostTimes") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PrintCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RegetDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LastEdorDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LastGetDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LastLoanDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SpecFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpSpec") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SignTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ManageFeeRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExpPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ExpPremium") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExpAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Peoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Mult") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Dif") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InputTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApproveFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApproveCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApproveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ApproveTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("UWTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolApplyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CustomGetPolDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GetPolDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GetPolTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EnterKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AmntGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Peoples3") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OnWorkPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OffWorkPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OtherPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RelaPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RelaMatePeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RelaYoungPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RelaOtherPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("FirstTrialOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstTrialDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("FirstTrialTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReceiveTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HandlerPrint") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BusinessBigType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnlDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPrintLoFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremApportFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPremFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RoleAgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AskGrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Copys") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OperationManager") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InfoSource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremScope") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DegreeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CardFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BigProjectFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPrintType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CTCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CoInsuranceFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CityInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentSaleCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandlerIDNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_INT;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_INT;
				break;
			case 40:
				nFieldType = Schema.TYPE_INT;
				break;
			case 41:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 43:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 44:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 52:
				nFieldType = Schema.TYPE_INT;
				break;
			case 53:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 54:
				nFieldType = Schema.TYPE_INT;
				break;
			case 55:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 56:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 57:
				nFieldType = Schema.TYPE_INT;
				break;
			case 58:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 59:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 60:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 61:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 62:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 63:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 66:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 67:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 68:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 69:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 70:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 71:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 72:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 73:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 74:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 75:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 76:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 77:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 78:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 79:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 80:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 81:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 82:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 83:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 84:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 85:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 86:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 87:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 88:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 89:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 90:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 91:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 92:
				nFieldType = Schema.TYPE_INT;
				break;
			case 93:
				nFieldType = Schema.TYPE_INT;
				break;
			case 94:
				nFieldType = Schema.TYPE_INT;
				break;
			case 95:
				nFieldType = Schema.TYPE_INT;
				break;
			case 96:
				nFieldType = Schema.TYPE_INT;
				break;
			case 97:
				nFieldType = Schema.TYPE_INT;
				break;
			case 98:
				nFieldType = Schema.TYPE_INT;
				break;
			case 99:
				nFieldType = Schema.TYPE_INT;
				break;
			case 100:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 101:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 102:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 103:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 104:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 105:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 106:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 107:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 108:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 109:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 110:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 111:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 112:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 113:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 114:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 115:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 116:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 117:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 118:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 119:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 120:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 121:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 122:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 123:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 124:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 125:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 126:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 127:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 128:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 129:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 130:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 131:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 132:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 133:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 134:
				nFieldType = Schema.TYPE_INT;
				break;
			case 135:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 136:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 137:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 138:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 139:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 140:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 141:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
