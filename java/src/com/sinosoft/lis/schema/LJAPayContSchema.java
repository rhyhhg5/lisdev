/*
 * <p>ClassName: LJAPayContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 更新表
 * @CreateDate：2004-12-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LJAPayContDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LJAPayContSchema implements Schema
{
    // @Field
    /** 总单/合同号码 */
    private String ContNo;
    /** 第几次交费 */
    private int PayCount;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 交费收据号码 */
    private String PayNo;
    /** 总应交金额 */
    private double SumDuePayMoney;
    /** 总实交金额 */
    private double SumActuPayMoney;
    /** 交费间隔 */
    private int PayIntv;
    /** 交费日期 */
    private Date PayDate;
    /** 交费类型 */
    private String PayType;
    /** 到帐日期 */
    private Date EnterAccDate;
    /** 确认日期 */
    private Date ConfDate;
    /** 原交至日期 */
    private Date LastPayToDate;
    /** 现交至日期 */
    private Date CurPayToDate;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 通知书号码 */
    private String GetNoticeNo;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 险种编码 */
    private String RiskCode;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 续保收费标记 */
    private String PayTypeFlag;

    public static final int FIELDNUM = 29; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LJAPayContSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ContNo";
        pk[1] = "PayNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public int getPayCount()
    {
        return PayCount;
    }

    public void setPayCount(int aPayCount)
    {
        PayCount = aPayCount;
    }

    public void setPayCount(String aPayCount)
    {
        if (aPayCount != null && !aPayCount.equals(""))
        {
            Integer tInteger = new Integer(aPayCount);
            int i = tInteger.intValue();
            PayCount = i;
        }
    }

    public String getAppntNo()
    {
        if (AppntNo != null && !AppntNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public String getPayNo()
    {
        if (PayNo != null && !PayNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PayNo = StrTool.unicodeToGBK(PayNo);
        }
        return PayNo;
    }

    public void setPayNo(String aPayNo)
    {
        PayNo = aPayNo;
    }

    public double getSumDuePayMoney()
    {
        return SumDuePayMoney;
    }

    public void setSumDuePayMoney(double aSumDuePayMoney)
    {
        SumDuePayMoney = aSumDuePayMoney;
    }

    public void setSumDuePayMoney(String aSumDuePayMoney)
    {
        if (aSumDuePayMoney != null && !aSumDuePayMoney.equals(""))
        {
            Double tDouble = new Double(aSumDuePayMoney);
            double d = tDouble.doubleValue();
            SumDuePayMoney = d;
        }
    }

    public double getSumActuPayMoney()
    {
        return SumActuPayMoney;
    }

    public void setSumActuPayMoney(double aSumActuPayMoney)
    {
        SumActuPayMoney = aSumActuPayMoney;
    }

    public void setSumActuPayMoney(String aSumActuPayMoney)
    {
        if (aSumActuPayMoney != null && !aSumActuPayMoney.equals(""))
        {
            Double tDouble = new Double(aSumActuPayMoney);
            double d = tDouble.doubleValue();
            SumActuPayMoney = d;
        }
    }

    public int getPayIntv()
    {
        return PayIntv;
    }

    public void setPayIntv(int aPayIntv)
    {
        PayIntv = aPayIntv;
    }

    public void setPayIntv(String aPayIntv)
    {
        if (aPayIntv != null && !aPayIntv.equals(""))
        {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getPayDate()
    {
        if (PayDate != null)
        {
            return fDate.getString(PayDate);
        }
        else
        {
            return null;
        }
    }

    public void setPayDate(Date aPayDate)
    {
        PayDate = aPayDate;
    }

    public void setPayDate(String aPayDate)
    {
        if (aPayDate != null && !aPayDate.equals(""))
        {
            PayDate = fDate.getDate(aPayDate);
        }
        else
        {
            PayDate = null;
        }
    }

    public String getPayType()
    {
        if (PayType != null && !PayType.equals("") && SysConst.CHANGECHARSET == true)
        {
            PayType = StrTool.unicodeToGBK(PayType);
        }
        return PayType;
    }

    public void setPayType(String aPayType)
    {
        PayType = aPayType;
    }

    public String getEnterAccDate()
    {
        if (EnterAccDate != null)
        {
            return fDate.getString(EnterAccDate);
        }
        else
        {
            return null;
        }
    }

    public void setEnterAccDate(Date aEnterAccDate)
    {
        EnterAccDate = aEnterAccDate;
    }

    public void setEnterAccDate(String aEnterAccDate)
    {
        if (aEnterAccDate != null && !aEnterAccDate.equals(""))
        {
            EnterAccDate = fDate.getDate(aEnterAccDate);
        }
        else
        {
            EnterAccDate = null;
        }
    }

    public String getConfDate()
    {
        if (ConfDate != null)
        {
            return fDate.getString(ConfDate);
        }
        else
        {
            return null;
        }
    }

    public void setConfDate(Date aConfDate)
    {
        ConfDate = aConfDate;
    }

    public void setConfDate(String aConfDate)
    {
        if (aConfDate != null && !aConfDate.equals(""))
        {
            ConfDate = fDate.getDate(aConfDate);
        }
        else
        {
            ConfDate = null;
        }
    }

    public String getLastPayToDate()
    {
        if (LastPayToDate != null)
        {
            return fDate.getString(LastPayToDate);
        }
        else
        {
            return null;
        }
    }

    public void setLastPayToDate(Date aLastPayToDate)
    {
        LastPayToDate = aLastPayToDate;
    }

    public void setLastPayToDate(String aLastPayToDate)
    {
        if (aLastPayToDate != null && !aLastPayToDate.equals(""))
        {
            LastPayToDate = fDate.getDate(aLastPayToDate);
        }
        else
        {
            LastPayToDate = null;
        }
    }

    public String getCurPayToDate()
    {
        if (CurPayToDate != null)
        {
            return fDate.getString(CurPayToDate);
        }
        else
        {
            return null;
        }
    }

    public void setCurPayToDate(Date aCurPayToDate)
    {
        CurPayToDate = aCurPayToDate;
    }

    public void setCurPayToDate(String aCurPayToDate)
    {
        if (aCurPayToDate != null && !aCurPayToDate.equals(""))
        {
            CurPayToDate = fDate.getDate(aCurPayToDate);
        }
        else
        {
            CurPayToDate = null;
        }
    }

    public String getApproveCode()
    {
        if (ApproveCode != null && !ApproveCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ApproveCode = StrTool.unicodeToGBK(ApproveCode);
        }
        return ApproveCode;
    }

    public void setApproveCode(String aApproveCode)
    {
        ApproveCode = aApproveCode;
    }

    public String getApproveDate()
    {
        if (ApproveDate != null)
        {
            return fDate.getString(ApproveDate);
        }
        else
        {
            return null;
        }
    }

    public void setApproveDate(Date aApproveDate)
    {
        ApproveDate = aApproveDate;
    }

    public void setApproveDate(String aApproveDate)
    {
        if (aApproveDate != null && !aApproveDate.equals(""))
        {
            ApproveDate = fDate.getDate(aApproveDate);
        }
        else
        {
            ApproveDate = null;
        }
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getGetNoticeNo()
    {
        if (GetNoticeNo != null && !GetNoticeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetNoticeNo = StrTool.unicodeToGBK(GetNoticeNo);
        }
        return GetNoticeNo;
    }

    public void setGetNoticeNo(String aGetNoticeNo)
    {
        GetNoticeNo = aGetNoticeNo;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAgentCom()
    {
        if (AgentCom != null && !AgentCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            AgentCom = StrTool.unicodeToGBK(AgentCom);
        }
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom)
    {
        AgentCom = aAgentCom;
    }

    public String getAgentType()
    {
        if (AgentType != null && !AgentType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentType = StrTool.unicodeToGBK(AgentType);
        }
        return AgentType;
    }

    public void setAgentType(String aAgentType)
    {
        AgentType = aAgentType;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup()
    {
        if (AgentGroup != null && !AgentGroup.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getPayTypeFlag()
    {
        if (PayTypeFlag != null && !PayTypeFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayTypeFlag = StrTool.unicodeToGBK(PayTypeFlag);
        }
        return PayTypeFlag;
    }

    public void setPayTypeFlag(String aPayTypeFlag)
    {
        PayTypeFlag = aPayTypeFlag;
    }

    /**
     * 使用另外一个 LJAPayContSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LJAPayContSchema aLJAPayContSchema)
    {
        this.ContNo = aLJAPayContSchema.getContNo();
        this.PayCount = aLJAPayContSchema.getPayCount();
        this.AppntNo = aLJAPayContSchema.getAppntNo();
        this.PayNo = aLJAPayContSchema.getPayNo();
        this.SumDuePayMoney = aLJAPayContSchema.getSumDuePayMoney();
        this.SumActuPayMoney = aLJAPayContSchema.getSumActuPayMoney();
        this.PayIntv = aLJAPayContSchema.getPayIntv();
        this.PayDate = fDate.getDate(aLJAPayContSchema.getPayDate());
        this.PayType = aLJAPayContSchema.getPayType();
        this.EnterAccDate = fDate.getDate(aLJAPayContSchema.getEnterAccDate());
        this.ConfDate = fDate.getDate(aLJAPayContSchema.getConfDate());
        this.LastPayToDate = fDate.getDate(aLJAPayContSchema.getLastPayToDate());
        this.CurPayToDate = fDate.getDate(aLJAPayContSchema.getCurPayToDate());
        this.ApproveCode = aLJAPayContSchema.getApproveCode();
        this.ApproveDate = fDate.getDate(aLJAPayContSchema.getApproveDate());
        this.SerialNo = aLJAPayContSchema.getSerialNo();
        this.Operator = aLJAPayContSchema.getOperator();
        this.MakeDate = fDate.getDate(aLJAPayContSchema.getMakeDate());
        this.MakeTime = aLJAPayContSchema.getMakeTime();
        this.GetNoticeNo = aLJAPayContSchema.getGetNoticeNo();
        this.ModifyDate = fDate.getDate(aLJAPayContSchema.getModifyDate());
        this.ModifyTime = aLJAPayContSchema.getModifyTime();
        this.ManageCom = aLJAPayContSchema.getManageCom();
        this.AgentCom = aLJAPayContSchema.getAgentCom();
        this.AgentType = aLJAPayContSchema.getAgentType();
        this.RiskCode = aLJAPayContSchema.getRiskCode();
        this.AgentCode = aLJAPayContSchema.getAgentCode();
        this.AgentGroup = aLJAPayContSchema.getAgentGroup();
        this.PayTypeFlag = aLJAPayContSchema.getPayTypeFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            this.PayCount = rs.getInt("PayCount");
            if (rs.getString("AppntNo") == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("PayNo") == null)
            {
                this.PayNo = null;
            }
            else
            {
                this.PayNo = rs.getString("PayNo").trim();
            }

            this.SumDuePayMoney = rs.getDouble("SumDuePayMoney");
            this.SumActuPayMoney = rs.getDouble("SumActuPayMoney");
            this.PayIntv = rs.getInt("PayIntv");
            this.PayDate = rs.getDate("PayDate");
            if (rs.getString("PayType") == null)
            {
                this.PayType = null;
            }
            else
            {
                this.PayType = rs.getString("PayType").trim();
            }

            this.EnterAccDate = rs.getDate("EnterAccDate");
            this.ConfDate = rs.getDate("ConfDate");
            this.LastPayToDate = rs.getDate("LastPayToDate");
            this.CurPayToDate = rs.getDate("CurPayToDate");
            if (rs.getString("ApproveCode") == null)
            {
                this.ApproveCode = null;
            }
            else
            {
                this.ApproveCode = rs.getString("ApproveCode").trim();
            }

            this.ApproveDate = rs.getDate("ApproveDate");
            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            if (rs.getString("GetNoticeNo") == null)
            {
                this.GetNoticeNo = null;
            }
            else
            {
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentCom") == null)
            {
                this.AgentCom = null;
            }
            else
            {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("AgentType") == null)
            {
                this.AgentType = null;
            }
            else
            {
                this.AgentType = rs.getString("AgentType").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("PayTypeFlag") == null)
            {
                this.PayTypeFlag = null;
            }
            else
            {
                this.PayTypeFlag = rs.getString("PayTypeFlag").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAPayContSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LJAPayContSchema getSchema()
    {
        LJAPayContSchema aLJAPayContSchema = new LJAPayContSchema();
        aLJAPayContSchema.setSchema(this);
        return aLJAPayContSchema;
    }

    public LJAPayContDB getDB()
    {
        LJAPayContDB aDBOper = new LJAPayContDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJAPayCont描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayCount) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(SumDuePayMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(SumActuPayMoney) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayIntv) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(PayDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayType)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EnterAccDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ConfDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            LastPayToDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            CurPayToDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ApproveCode)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ApproveDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetNoticeNo)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayTypeFlag));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJAPayCont>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            PayCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).intValue();
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            PayNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            SumDuePayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            SumActuPayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            PayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            LastPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            CurPayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                         SysConst.PACKAGESPILTER);
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                         SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                       SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                       SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                      SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                        SysConst.PACKAGESPILTER);
            PayTypeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJAPayContSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("PayCount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayCount));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntNo));
        }
        if (FCode.equals("PayNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayNo));
        }
        if (FCode.equals("SumDuePayMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SumDuePayMoney));
        }
        if (FCode.equals("SumActuPayMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SumActuPayMoney));
        }
        if (FCode.equals("PayIntv"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayIntv));
        }
        if (FCode.equals("PayDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getPayDate()));
        }
        if (FCode.equals("PayType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayType));
        }
        if (FCode.equals("EnterAccDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getEnterAccDate()));
        }
        if (FCode.equals("ConfDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getConfDate()));
        }
        if (FCode.equals("LastPayToDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getLastPayToDate()));
        }
        if (FCode.equals("CurPayToDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getCurPayToDate()));
        }
        if (FCode.equals("ApproveCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ApproveCode));
        }
        if (FCode.equals("ApproveDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getApproveDate()));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("GetNoticeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetNoticeNo));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("AgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCom));
        }
        if (FCode.equals("AgentType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentType));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentGroup));
        }
        if (FCode.equals("PayTypeFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayTypeFlag));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 1:
                strFieldValue = String.valueOf(PayCount);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PayNo);
                break;
            case 4:
                strFieldValue = String.valueOf(SumDuePayMoney);
                break;
            case 5:
                strFieldValue = String.valueOf(SumActuPayMoney);
                break;
            case 6:
                strFieldValue = String.valueOf(PayIntv);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPayDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(PayType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEnterAccDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getLastPayToDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCurPayToDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ApproveCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getApproveDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(PayTypeFlag);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("PayCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayCount = i;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("PayNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayNo = FValue.trim();
            }
            else
            {
                PayNo = null;
            }
        }
        if (FCode.equals("SumDuePayMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumDuePayMoney = d;
            }
        }
        if (FCode.equals("SumActuPayMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumActuPayMoney = d;
            }
        }
        if (FCode.equals("PayIntv"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equals("PayDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayDate = fDate.getDate(FValue);
            }
            else
            {
                PayDate = null;
            }
        }
        if (FCode.equals("PayType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayType = FValue.trim();
            }
            else
            {
                PayType = null;
            }
        }
        if (FCode.equals("EnterAccDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EnterAccDate = fDate.getDate(FValue);
            }
            else
            {
                EnterAccDate = null;
            }
        }
        if (FCode.equals("ConfDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfDate = fDate.getDate(FValue);
            }
            else
            {
                ConfDate = null;
            }
        }
        if (FCode.equals("LastPayToDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LastPayToDate = fDate.getDate(FValue);
            }
            else
            {
                LastPayToDate = null;
            }
        }
        if (FCode.equals("CurPayToDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CurPayToDate = fDate.getDate(FValue);
            }
            else
            {
                CurPayToDate = null;
            }
        }
        if (FCode.equals("ApproveCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveCode = FValue.trim();
            }
            else
            {
                ApproveCode = null;
            }
        }
        if (FCode.equals("ApproveDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveDate = fDate.getDate(FValue);
            }
            else
            {
                ApproveDate = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("GetNoticeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
            {
                GetNoticeNo = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
            {
                AgentCom = null;
            }
        }
        if (FCode.equals("AgentType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
            {
                AgentType = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("PayTypeFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayTypeFlag = FValue.trim();
            }
            else
            {
                PayTypeFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LJAPayContSchema other = (LJAPayContSchema) otherObject;
        return
                ContNo.equals(other.getContNo())
                && PayCount == other.getPayCount()
                && AppntNo.equals(other.getAppntNo())
                && PayNo.equals(other.getPayNo())
                && SumDuePayMoney == other.getSumDuePayMoney()
                && SumActuPayMoney == other.getSumActuPayMoney()
                && PayIntv == other.getPayIntv()
                && fDate.getString(PayDate).equals(other.getPayDate())
                && PayType.equals(other.getPayType())
                && fDate.getString(EnterAccDate).equals(other.getEnterAccDate())
                && fDate.getString(ConfDate).equals(other.getConfDate())
                && fDate.getString(LastPayToDate).equals(other.getLastPayToDate())
                && fDate.getString(CurPayToDate).equals(other.getCurPayToDate())
                && ApproveCode.equals(other.getApproveCode())
                && fDate.getString(ApproveDate).equals(other.getApproveDate())
                && SerialNo.equals(other.getSerialNo())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && GetNoticeNo.equals(other.getGetNoticeNo())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ManageCom.equals(other.getManageCom())
                && AgentCom.equals(other.getAgentCom())
                && AgentType.equals(other.getAgentType())
                && RiskCode.equals(other.getRiskCode())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && PayTypeFlag.equals(other.getPayTypeFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("PayCount"))
        {
            return 1;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 2;
        }
        if (strFieldName.equals("PayNo"))
        {
            return 3;
        }
        if (strFieldName.equals("SumDuePayMoney"))
        {
            return 4;
        }
        if (strFieldName.equals("SumActuPayMoney"))
        {
            return 5;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return 6;
        }
        if (strFieldName.equals("PayDate"))
        {
            return 7;
        }
        if (strFieldName.equals("PayType"))
        {
            return 8;
        }
        if (strFieldName.equals("EnterAccDate"))
        {
            return 9;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return 10;
        }
        if (strFieldName.equals("LastPayToDate"))
        {
            return 11;
        }
        if (strFieldName.equals("CurPayToDate"))
        {
            return 12;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return 13;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return 14;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 15;
        }
        if (strFieldName.equals("Operator"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 18;
        }
        if (strFieldName.equals("GetNoticeNo"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 21;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 22;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return 23;
        }
        if (strFieldName.equals("AgentType"))
        {
            return 24;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 25;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 26;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 27;
        }
        if (strFieldName.equals("PayTypeFlag"))
        {
            return 28;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ContNo";
                break;
            case 1:
                strFieldName = "PayCount";
                break;
            case 2:
                strFieldName = "AppntNo";
                break;
            case 3:
                strFieldName = "PayNo";
                break;
            case 4:
                strFieldName = "SumDuePayMoney";
                break;
            case 5:
                strFieldName = "SumActuPayMoney";
                break;
            case 6:
                strFieldName = "PayIntv";
                break;
            case 7:
                strFieldName = "PayDate";
                break;
            case 8:
                strFieldName = "PayType";
                break;
            case 9:
                strFieldName = "EnterAccDate";
                break;
            case 10:
                strFieldName = "ConfDate";
                break;
            case 11:
                strFieldName = "LastPayToDate";
                break;
            case 12:
                strFieldName = "CurPayToDate";
                break;
            case 13:
                strFieldName = "ApproveCode";
                break;
            case 14:
                strFieldName = "ApproveDate";
                break;
            case 15:
                strFieldName = "SerialNo";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MakeDate";
                break;
            case 18:
                strFieldName = "MakeTime";
                break;
            case 19:
                strFieldName = "GetNoticeNo";
                break;
            case 20:
                strFieldName = "ModifyDate";
                break;
            case 21:
                strFieldName = "ModifyTime";
                break;
            case 22:
                strFieldName = "ManageCom";
                break;
            case 23:
                strFieldName = "AgentCom";
                break;
            case 24:
                strFieldName = "AgentType";
                break;
            case 25:
                strFieldName = "RiskCode";
                break;
            case 26:
                strFieldName = "AgentCode";
                break;
            case 27:
                strFieldName = "AgentGroup";
                break;
            case 28:
                strFieldName = "PayTypeFlag";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SumDuePayMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SumActuPayMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PayIntv"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PayDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PayType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EnterAccDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("LastPayToDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CurPayToDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ApproveCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetNoticeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayTypeFlag"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_INT;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
