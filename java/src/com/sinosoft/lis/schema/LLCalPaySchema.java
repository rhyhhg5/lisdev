/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCalPayDB;

/*
 * <p>ClassName: LLCalPaySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-01-11
 */
public class LLCalPaySchema implements Schema, Cloneable
{
	// @Field
	/** 险种代码 */
	private String riskcode;
	/** 给付责任代码 */
	private String getdutycode;
	/** 给付责任类型 */
	private String GetDutyKind;
	/** 赔付结论代码 */
	private String GiveType;
	/** 赔付结论依据代码 */
	private String GiveReason;
	/** 赔付结论 */
	private String GiveTypeDesc;
	/** 赔付结论依据 */
	private String GiveReasonDesc;
	/** 比例 */
	private String IsRate;
	/** 拒付金额 */
	private double DeclineAmnt;
	/** 通融协议给付比例 */
	private double ApproveAmnt;
	/** 是否为简易案件 */
	private String EasyCase;

	public static final int FIELDNUM = 11;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCalPaySchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLCalPaySchema cloned = (LLCalPaySchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getriskcode()
	{
		return riskcode;
	}
	public void setriskcode(String ariskcode)
	{
		riskcode = ariskcode;
	}
	public String getgetdutycode()
	{
		return getdutycode;
	}
	public void setgetdutycode(String agetdutycode)
	{
		getdutycode = agetdutycode;
	}
	public String getGetDutyKind()
	{
		return GetDutyKind;
	}
	public void setGetDutyKind(String aGetDutyKind)
	{
		GetDutyKind = aGetDutyKind;
	}
	public String getGiveType()
	{
		return GiveType;
	}
	public void setGiveType(String aGiveType)
	{
		GiveType = aGiveType;
	}
	public String getGiveReason()
	{
		return GiveReason;
	}
	public void setGiveReason(String aGiveReason)
	{
		GiveReason = aGiveReason;
	}
	public String getGiveTypeDesc()
	{
		return GiveTypeDesc;
	}
	public void setGiveTypeDesc(String aGiveTypeDesc)
	{
		GiveTypeDesc = aGiveTypeDesc;
	}
	public String getGiveReasonDesc()
	{
		return GiveReasonDesc;
	}
	public void setGiveReasonDesc(String aGiveReasonDesc)
	{
		GiveReasonDesc = aGiveReasonDesc;
	}
	public String getIsRate()
	{
		return IsRate;
	}
	public void setIsRate(String aIsRate)
	{
		IsRate = aIsRate;
	}
	public double getDeclineAmnt()
	{
		return DeclineAmnt;
	}
	public void setDeclineAmnt(double aDeclineAmnt)
	{
		DeclineAmnt = Arith.round(aDeclineAmnt,2);
	}
	public void setDeclineAmnt(String aDeclineAmnt)
	{
		if (aDeclineAmnt != null && !aDeclineAmnt.equals(""))
		{
			Double tDouble = new Double(aDeclineAmnt);
			double d = tDouble.doubleValue();
                DeclineAmnt = Arith.round(d,2);
		}
	}

	public double getApproveAmnt()
	{
		return ApproveAmnt;
	}
	public void setApproveAmnt(double aApproveAmnt)
	{
		ApproveAmnt = Arith.round(aApproveAmnt,2);
	}
	public void setApproveAmnt(String aApproveAmnt)
	{
		if (aApproveAmnt != null && !aApproveAmnt.equals(""))
		{
			Double tDouble = new Double(aApproveAmnt);
			double d = tDouble.doubleValue();
                ApproveAmnt = Arith.round(d,2);
		}
	}

	public String getEasyCase()
	{
		return EasyCase;
	}
	public void setEasyCase(String aEasyCase)
	{
		EasyCase = aEasyCase;
	}

	/**
	* 使用另外一个 LLCalPaySchema 对象给 Schema 赋值
	* @param: aLLCalPaySchema LLCalPaySchema
	**/
	public void setSchema(LLCalPaySchema aLLCalPaySchema)
	{
		this.riskcode = aLLCalPaySchema.getriskcode();
		this.getdutycode = aLLCalPaySchema.getgetdutycode();
		this.GetDutyKind = aLLCalPaySchema.getGetDutyKind();
		this.GiveType = aLLCalPaySchema.getGiveType();
		this.GiveReason = aLLCalPaySchema.getGiveReason();
		this.GiveTypeDesc = aLLCalPaySchema.getGiveTypeDesc();
		this.GiveReasonDesc = aLLCalPaySchema.getGiveReasonDesc();
		this.IsRate = aLLCalPaySchema.getIsRate();
		this.DeclineAmnt = aLLCalPaySchema.getDeclineAmnt();
		this.ApproveAmnt = aLLCalPaySchema.getApproveAmnt();
		this.EasyCase = aLLCalPaySchema.getEasyCase();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("riskcode") == null )
				this.riskcode = null;
			else
				this.riskcode = rs.getString("riskcode").trim();

			if( rs.getString("getdutycode") == null )
				this.getdutycode = null;
			else
				this.getdutycode = rs.getString("getdutycode").trim();

			if( rs.getString("GetDutyKind") == null )
				this.GetDutyKind = null;
			else
				this.GetDutyKind = rs.getString("GetDutyKind").trim();

			if( rs.getString("GiveType") == null )
				this.GiveType = null;
			else
				this.GiveType = rs.getString("GiveType").trim();

			if( rs.getString("GiveReason") == null )
				this.GiveReason = null;
			else
				this.GiveReason = rs.getString("GiveReason").trim();

			if( rs.getString("GiveTypeDesc") == null )
				this.GiveTypeDesc = null;
			else
				this.GiveTypeDesc = rs.getString("GiveTypeDesc").trim();

			if( rs.getString("GiveReasonDesc") == null )
				this.GiveReasonDesc = null;
			else
				this.GiveReasonDesc = rs.getString("GiveReasonDesc").trim();

			if( rs.getString("IsRate") == null )
				this.IsRate = null;
			else
				this.IsRate = rs.getString("IsRate").trim();

			this.DeclineAmnt = rs.getDouble("DeclineAmnt");
			this.ApproveAmnt = rs.getDouble("ApproveAmnt");
			if( rs.getString("EasyCase") == null )
				this.EasyCase = null;
			else
				this.EasyCase = rs.getString("EasyCase").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCalPay表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCalPaySchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCalPaySchema getSchema()
	{
		LLCalPaySchema aLLCalPaySchema = new LLCalPaySchema();
		aLLCalPaySchema.setSchema(this);
		return aLLCalPaySchema;
	}

	public LLCalPayDB getDB()
	{
		LLCalPayDB aDBOper = new LLCalPayDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCalPay描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(riskcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(getdutycode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GiveType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GiveReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GiveTypeDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GiveReasonDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IsRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DeclineAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ApproveAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EasyCase));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCalPay>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			riskcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			getdutycode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			GiveType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			GiveReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			GiveTypeDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			GiveReasonDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			IsRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			DeclineAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			ApproveAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			EasyCase = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCalPaySchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("riskcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(riskcode));
		}
		if (FCode.equals("getdutycode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(getdutycode));
		}
		if (FCode.equals("GetDutyKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
		}
		if (FCode.equals("GiveType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GiveType));
		}
		if (FCode.equals("GiveReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GiveReason));
		}
		if (FCode.equals("GiveTypeDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GiveTypeDesc));
		}
		if (FCode.equals("GiveReasonDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GiveReasonDesc));
		}
		if (FCode.equals("IsRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IsRate));
		}
		if (FCode.equals("DeclineAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeclineAmnt));
		}
		if (FCode.equals("ApproveAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveAmnt));
		}
		if (FCode.equals("EasyCase"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EasyCase));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(riskcode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(getdutycode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(GiveType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(GiveReason);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(GiveTypeDesc);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(GiveReasonDesc);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(IsRate);
				break;
			case 8:
				strFieldValue = String.valueOf(DeclineAmnt);
				break;
			case 9:
				strFieldValue = String.valueOf(ApproveAmnt);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(EasyCase);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("riskcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				riskcode = FValue.trim();
			}
			else
				riskcode = null;
		}
		if (FCode.equalsIgnoreCase("getdutycode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				getdutycode = FValue.trim();
			}
			else
				getdutycode = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyKind = FValue.trim();
			}
			else
				GetDutyKind = null;
		}
		if (FCode.equalsIgnoreCase("GiveType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GiveType = FValue.trim();
			}
			else
				GiveType = null;
		}
		if (FCode.equalsIgnoreCase("GiveReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GiveReason = FValue.trim();
			}
			else
				GiveReason = null;
		}
		if (FCode.equalsIgnoreCase("GiveTypeDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GiveTypeDesc = FValue.trim();
			}
			else
				GiveTypeDesc = null;
		}
		if (FCode.equalsIgnoreCase("GiveReasonDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GiveReasonDesc = FValue.trim();
			}
			else
				GiveReasonDesc = null;
		}
		if (FCode.equalsIgnoreCase("IsRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IsRate = FValue.trim();
			}
			else
				IsRate = null;
		}
		if (FCode.equalsIgnoreCase("DeclineAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DeclineAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("ApproveAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ApproveAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("EasyCase"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EasyCase = FValue.trim();
			}
			else
				EasyCase = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCalPaySchema other = (LLCalPaySchema)otherObject;
		return
			(riskcode == null ? other.getriskcode() == null : riskcode.equals(other.getriskcode()))
			&& (getdutycode == null ? other.getgetdutycode() == null : getdutycode.equals(other.getgetdutycode()))
			&& (GetDutyKind == null ? other.getGetDutyKind() == null : GetDutyKind.equals(other.getGetDutyKind()))
			&& (GiveType == null ? other.getGiveType() == null : GiveType.equals(other.getGiveType()))
			&& (GiveReason == null ? other.getGiveReason() == null : GiveReason.equals(other.getGiveReason()))
			&& (GiveTypeDesc == null ? other.getGiveTypeDesc() == null : GiveTypeDesc.equals(other.getGiveTypeDesc()))
			&& (GiveReasonDesc == null ? other.getGiveReasonDesc() == null : GiveReasonDesc.equals(other.getGiveReasonDesc()))
			&& (IsRate == null ? other.getIsRate() == null : IsRate.equals(other.getIsRate()))
			&& DeclineAmnt == other.getDeclineAmnt()
			&& ApproveAmnt == other.getApproveAmnt()
			&& (EasyCase == null ? other.getEasyCase() == null : EasyCase.equals(other.getEasyCase()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("riskcode") ) {
			return 0;
		}
		if( strFieldName.equals("getdutycode") ) {
			return 1;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return 2;
		}
		if( strFieldName.equals("GiveType") ) {
			return 3;
		}
		if( strFieldName.equals("GiveReason") ) {
			return 4;
		}
		if( strFieldName.equals("GiveTypeDesc") ) {
			return 5;
		}
		if( strFieldName.equals("GiveReasonDesc") ) {
			return 6;
		}
		if( strFieldName.equals("IsRate") ) {
			return 7;
		}
		if( strFieldName.equals("DeclineAmnt") ) {
			return 8;
		}
		if( strFieldName.equals("ApproveAmnt") ) {
			return 9;
		}
		if( strFieldName.equals("EasyCase") ) {
			return 10;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "riskcode";
				break;
			case 1:
				strFieldName = "getdutycode";
				break;
			case 2:
				strFieldName = "GetDutyKind";
				break;
			case 3:
				strFieldName = "GiveType";
				break;
			case 4:
				strFieldName = "GiveReason";
				break;
			case 5:
				strFieldName = "GiveTypeDesc";
				break;
			case 6:
				strFieldName = "GiveReasonDesc";
				break;
			case 7:
				strFieldName = "IsRate";
				break;
			case 8:
				strFieldName = "DeclineAmnt";
				break;
			case 9:
				strFieldName = "ApproveAmnt";
				break;
			case 10:
				strFieldName = "EasyCase";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("riskcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("getdutycode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GiveType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GiveReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GiveTypeDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GiveReasonDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IsRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeclineAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ApproveAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("EasyCase") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
