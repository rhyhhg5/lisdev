/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKBatchAgentDB;

/*
 * <p>ClassName: LKBatchAgentSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 实时交易系统
 * @CreateDate：2005-10-11
 */
public class LKBatchAgentSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String SerialNo;
	/** 交退费编码 */
	private String PayCode;
	/** 银行地区代码 */
	private String BankZoneCode;
	/** 银行网点编码 */
	private String BankNode;
	/** 保单号 */
	private String ContNo;
	/** 投保书号 */
	private String ProposalContNo;
	/** 保险公司区域代码 */
	private String ZoneCode;
	/** 险种编号 */
	private String RiskCode;
	/** 险种名称 */
	private String RiskName;
	/** 受益人 */
	private String SRY;
	/** 处理标志 */
	private String FunctionFlag;
	/** 失败原因描述 */
	private String Descr;
	/** 代付/代扣项目 */
	private String AgentItem;
	/** 应付/应扣日期 */
	private Date Paydate;
	/** 金额 */
	private double Money;
	/** 帐户名 */
	private String AccName;
	/** 帐号 */
	private String AccNo;
	/** 收付标志 */
	private String AgentFlag;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 备用1 */
	private String remark1;
	/** 备用2 */
	private String remark2;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKBatchAgentSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "SerialNo";
		pk[1] = "PayCode";
		pk[2] = "ContNo";
		pk[3] = "AgentFlag";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LKBatchAgentSchema cloned = (LKBatchAgentSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getPayCode()
	{
		return PayCode;
	}
	public void setPayCode(String aPayCode)
	{
		PayCode = aPayCode;
	}
	public String getBankZoneCode()
	{
		return BankZoneCode;
	}
	public void setBankZoneCode(String aBankZoneCode)
	{
		BankZoneCode = aBankZoneCode;
	}
	public String getBankNode()
	{
		return BankNode;
	}
	public void setBankNode(String aBankNode)
	{
		BankNode = aBankNode;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getProposalContNo()
	{
		return ProposalContNo;
	}
	public void setProposalContNo(String aProposalContNo)
	{
		ProposalContNo = aProposalContNo;
	}
	public String getZoneCode()
	{
		return ZoneCode;
	}
	public void setZoneCode(String aZoneCode)
	{
		ZoneCode = aZoneCode;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getRiskName()
	{
		return RiskName;
	}
	public void setRiskName(String aRiskName)
	{
		RiskName = aRiskName;
	}
	public String getSRY()
	{
		return SRY;
	}
	public void setSRY(String aSRY)
	{
		SRY = aSRY;
	}
	public String getFunctionFlag()
	{
		return FunctionFlag;
	}
	public void setFunctionFlag(String aFunctionFlag)
	{
		FunctionFlag = aFunctionFlag;
	}
	public String getDescr()
	{
		return Descr;
	}
	public void setDescr(String aDescr)
	{
		Descr = aDescr;
	}
	public String getAgentItem()
	{
		return AgentItem;
	}
	public void setAgentItem(String aAgentItem)
	{
		AgentItem = aAgentItem;
	}
	public String getPaydate()
	{
		if( Paydate != null )
			return fDate.getString(Paydate);
		else
			return null;
	}
	public void setPaydate(Date aPaydate)
	{
		Paydate = aPaydate;
	}
	public void setPaydate(String aPaydate)
	{
		if (aPaydate != null && !aPaydate.equals("") )
		{
			Paydate = fDate.getDate( aPaydate );
		}
		else
			Paydate = null;
	}

	public double getMoney()
	{
		return Money;
	}
	public void setMoney(double aMoney)
	{
		Money = aMoney;
	}
	public void setMoney(String aMoney)
	{
		if (aMoney != null && !aMoney.equals(""))
		{
			Double tDouble = new Double(aMoney);
			double d = tDouble.doubleValue();
			Money = d;
		}
	}

	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getAccNo()
	{
		return AccNo;
	}
	public void setAccNo(String aAccNo)
	{
		AccNo = aAccNo;
	}
	public String getAgentFlag()
	{
		return AgentFlag;
	}
	public void setAgentFlag(String aAgentFlag)
	{
		AgentFlag = aAgentFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getremark1()
	{
		return remark1;
	}
	public void setremark1(String aremark1)
	{
		remark1 = aremark1;
	}
	public String getremark2()
	{
		return remark2;
	}
	public void setremark2(String aremark2)
	{
		remark2 = aremark2;
	}

	/**
	* 使用另外一个 LKBatchAgentSchema 对象给 Schema 赋值
	* @param: aLKBatchAgentSchema LKBatchAgentSchema
	**/
	public void setSchema(LKBatchAgentSchema aLKBatchAgentSchema)
	{
		this.SerialNo = aLKBatchAgentSchema.getSerialNo();
		this.PayCode = aLKBatchAgentSchema.getPayCode();
		this.BankZoneCode = aLKBatchAgentSchema.getBankZoneCode();
		this.BankNode = aLKBatchAgentSchema.getBankNode();
		this.ContNo = aLKBatchAgentSchema.getContNo();
		this.ProposalContNo = aLKBatchAgentSchema.getProposalContNo();
		this.ZoneCode = aLKBatchAgentSchema.getZoneCode();
		this.RiskCode = aLKBatchAgentSchema.getRiskCode();
		this.RiskName = aLKBatchAgentSchema.getRiskName();
		this.SRY = aLKBatchAgentSchema.getSRY();
		this.FunctionFlag = aLKBatchAgentSchema.getFunctionFlag();
		this.Descr = aLKBatchAgentSchema.getDescr();
		this.AgentItem = aLKBatchAgentSchema.getAgentItem();
		this.Paydate = fDate.getDate( aLKBatchAgentSchema.getPaydate());
		this.Money = aLKBatchAgentSchema.getMoney();
		this.AccName = aLKBatchAgentSchema.getAccName();
		this.AccNo = aLKBatchAgentSchema.getAccNo();
		this.AgentFlag = aLKBatchAgentSchema.getAgentFlag();
		this.Operator = aLKBatchAgentSchema.getOperator();
		this.MakeDate = fDate.getDate( aLKBatchAgentSchema.getMakeDate());
		this.MakeTime = aLKBatchAgentSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLKBatchAgentSchema.getModifyDate());
		this.ModifyTime = aLKBatchAgentSchema.getModifyTime();
		this.remark1 = aLKBatchAgentSchema.getremark1();
		this.remark2 = aLKBatchAgentSchema.getremark2();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("PayCode") == null )
				this.PayCode = null;
			else
				this.PayCode = rs.getString("PayCode").trim();

			if( rs.getString("BankZoneCode") == null )
				this.BankZoneCode = null;
			else
				this.BankZoneCode = rs.getString("BankZoneCode").trim();

			if( rs.getString("BankNode") == null )
				this.BankNode = null;
			else
				this.BankNode = rs.getString("BankNode").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("ProposalContNo") == null )
				this.ProposalContNo = null;
			else
				this.ProposalContNo = rs.getString("ProposalContNo").trim();

			if( rs.getString("ZoneCode") == null )
				this.ZoneCode = null;
			else
				this.ZoneCode = rs.getString("ZoneCode").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("RiskName") == null )
				this.RiskName = null;
			else
				this.RiskName = rs.getString("RiskName").trim();

			if( rs.getString("SRY") == null )
				this.SRY = null;
			else
				this.SRY = rs.getString("SRY").trim();

			if( rs.getString("FunctionFlag") == null )
				this.FunctionFlag = null;
			else
				this.FunctionFlag = rs.getString("FunctionFlag").trim();

			if( rs.getString("Descr") == null )
				this.Descr = null;
			else
				this.Descr = rs.getString("Descr").trim();

			if( rs.getString("AgentItem") == null )
				this.AgentItem = null;
			else
				this.AgentItem = rs.getString("AgentItem").trim();

			this.Paydate = rs.getDate("Paydate");
			this.Money = rs.getDouble("Money");
			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("AccNo") == null )
				this.AccNo = null;
			else
				this.AccNo = rs.getString("AccNo").trim();

			if( rs.getString("AgentFlag") == null )
				this.AgentFlag = null;
			else
				this.AgentFlag = rs.getString("AgentFlag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("remark1") == null )
				this.remark1 = null;
			else
				this.remark1 = rs.getString("remark1").trim();

			if( rs.getString("remark2") == null )
				this.remark2 = null;
			else
				this.remark2 = rs.getString("remark2").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LKBatchAgent表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKBatchAgentSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LKBatchAgentSchema getSchema()
	{
		LKBatchAgentSchema aLKBatchAgentSchema = new LKBatchAgentSchema();
		aLKBatchAgentSchema.setSchema(this);
		return aLKBatchAgentSchema;
	}

	public LKBatchAgentDB getDB()
	{
		LKBatchAgentDB aDBOper = new LKBatchAgentDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKBatchAgent描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BankZoneCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BankNode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ProposalContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ZoneCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RiskName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SRY)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FunctionFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Descr)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentItem)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( Paydate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(Money));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AccNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(remark1)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(remark2));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKBatchAgent>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PayCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BankZoneCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BankNode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ZoneCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			SRY = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			FunctionFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Descr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			AgentItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Paydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			Money = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			AccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			AgentFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKBatchAgentSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equalsIgnoreCase("PayCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayCode));
		}
		if (FCode.equalsIgnoreCase("BankZoneCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankZoneCode));
		}
		if (FCode.equalsIgnoreCase("BankNode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equalsIgnoreCase("ProposalContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalContNo));
		}
		if (FCode.equalsIgnoreCase("ZoneCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZoneCode));
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equalsIgnoreCase("RiskName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskName));
		}
		if (FCode.equalsIgnoreCase("SRY"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SRY));
		}
		if (FCode.equalsIgnoreCase("FunctionFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FunctionFlag));
		}
		if (FCode.equalsIgnoreCase("Descr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Descr));
		}
		if (FCode.equalsIgnoreCase("AgentItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentItem));
		}
		if (FCode.equalsIgnoreCase("Paydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPaydate()));
		}
		if (FCode.equalsIgnoreCase("Money"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Money));
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equalsIgnoreCase("AccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccNo));
		}
		if (FCode.equalsIgnoreCase("AgentFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentFlag));
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equalsIgnoreCase("remark1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(remark1));
		}
		if (FCode.equalsIgnoreCase("remark2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(remark2));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PayCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BankZoneCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BankNode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ZoneCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RiskName);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(SRY);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(FunctionFlag);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Descr);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(AgentItem);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPaydate()));
				break;
			case 14:
				strFieldValue = String.valueOf(Money);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(AccNo);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(AgentFlag);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(remark1);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(remark2);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("PayCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayCode = FValue.trim();
			}
			else
				PayCode = null;
		}
		if (FCode.equalsIgnoreCase("BankZoneCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankZoneCode = FValue.trim();
			}
			else
				BankZoneCode = null;
		}
		if (FCode.equalsIgnoreCase("BankNode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankNode = FValue.trim();
			}
			else
				BankNode = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("ProposalContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalContNo = FValue.trim();
			}
			else
				ProposalContNo = null;
		}
		if (FCode.equalsIgnoreCase("ZoneCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZoneCode = FValue.trim();
			}
			else
				ZoneCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskName = FValue.trim();
			}
			else
				RiskName = null;
		}
		if (FCode.equalsIgnoreCase("SRY"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SRY = FValue.trim();
			}
			else
				SRY = null;
		}
		if (FCode.equalsIgnoreCase("FunctionFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FunctionFlag = FValue.trim();
			}
			else
				FunctionFlag = null;
		}
		if (FCode.equalsIgnoreCase("Descr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Descr = FValue.trim();
			}
			else
				Descr = null;
		}
		if (FCode.equalsIgnoreCase("AgentItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentItem = FValue.trim();
			}
			else
				AgentItem = null;
		}
		if (FCode.equalsIgnoreCase("Paydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Paydate = fDate.getDate( FValue );
			}
			else
				Paydate = null;
		}
		if (FCode.equalsIgnoreCase("Money"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Money = d;
			}
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("AccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccNo = FValue.trim();
			}
			else
				AccNo = null;
		}
		if (FCode.equalsIgnoreCase("AgentFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentFlag = FValue.trim();
			}
			else
				AgentFlag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("remark1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				remark1 = FValue.trim();
			}
			else
				remark1 = null;
		}
		if (FCode.equalsIgnoreCase("remark2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				remark2 = FValue.trim();
			}
			else
				remark2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKBatchAgentSchema other = (LKBatchAgentSchema)otherObject;
		return
			SerialNo.equals(other.getSerialNo())
			&& PayCode.equals(other.getPayCode())
			&& BankZoneCode.equals(other.getBankZoneCode())
			&& BankNode.equals(other.getBankNode())
			&& ContNo.equals(other.getContNo())
			&& ProposalContNo.equals(other.getProposalContNo())
			&& ZoneCode.equals(other.getZoneCode())
			&& RiskCode.equals(other.getRiskCode())
			&& RiskName.equals(other.getRiskName())
			&& SRY.equals(other.getSRY())
			&& FunctionFlag.equals(other.getFunctionFlag())
			&& Descr.equals(other.getDescr())
			&& AgentItem.equals(other.getAgentItem())
			&& fDate.getString(Paydate).equals(other.getPaydate())
			&& Money == other.getMoney()
			&& AccName.equals(other.getAccName())
			&& AccNo.equals(other.getAccNo())
			&& AgentFlag.equals(other.getAgentFlag())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& remark1.equals(other.getremark1())
			&& remark2.equals(other.getremark2());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("PayCode") ) {
			return 1;
		}
		if( strFieldName.equals("BankZoneCode") ) {
			return 2;
		}
		if( strFieldName.equals("BankNode") ) {
			return 3;
		}
		if( strFieldName.equals("ContNo") ) {
			return 4;
		}
		if( strFieldName.equals("ProposalContNo") ) {
			return 5;
		}
		if( strFieldName.equals("ZoneCode") ) {
			return 6;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 7;
		}
		if( strFieldName.equals("RiskName") ) {
			return 8;
		}
		if( strFieldName.equals("SRY") ) {
			return 9;
		}
		if( strFieldName.equals("FunctionFlag") ) {
			return 10;
		}
		if( strFieldName.equals("Descr") ) {
			return 11;
		}
		if( strFieldName.equals("AgentItem") ) {
			return 12;
		}
		if( strFieldName.equals("Paydate") ) {
			return 13;
		}
		if( strFieldName.equals("Money") ) {
			return 14;
		}
		if( strFieldName.equals("AccName") ) {
			return 15;
		}
		if( strFieldName.equals("AccNo") ) {
			return 16;
		}
		if( strFieldName.equals("AgentFlag") ) {
			return 17;
		}
		if( strFieldName.equals("Operator") ) {
			return 18;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 19;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 20;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 22;
		}
		if( strFieldName.equals("remark1") ) {
			return 23;
		}
		if( strFieldName.equals("remark2") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "PayCode";
				break;
			case 2:
				strFieldName = "BankZoneCode";
				break;
			case 3:
				strFieldName = "BankNode";
				break;
			case 4:
				strFieldName = "ContNo";
				break;
			case 5:
				strFieldName = "ProposalContNo";
				break;
			case 6:
				strFieldName = "ZoneCode";
				break;
			case 7:
				strFieldName = "RiskCode";
				break;
			case 8:
				strFieldName = "RiskName";
				break;
			case 9:
				strFieldName = "SRY";
				break;
			case 10:
				strFieldName = "FunctionFlag";
				break;
			case 11:
				strFieldName = "Descr";
				break;
			case 12:
				strFieldName = "AgentItem";
				break;
			case 13:
				strFieldName = "Paydate";
				break;
			case 14:
				strFieldName = "Money";
				break;
			case 15:
				strFieldName = "AccName";
				break;
			case 16:
				strFieldName = "AccNo";
				break;
			case 17:
				strFieldName = "AgentFlag";
				break;
			case 18:
				strFieldName = "Operator";
				break;
			case 19:
				strFieldName = "MakeDate";
				break;
			case 20:
				strFieldName = "MakeTime";
				break;
			case 21:
				strFieldName = "ModifyDate";
				break;
			case 22:
				strFieldName = "ModifyTime";
				break;
			case 23:
				strFieldName = "remark1";
				break;
			case 24:
				strFieldName = "remark2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankZoneCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankNode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZoneCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SRY") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FunctionFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Descr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Paydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Money") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("remark1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("remark2") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
