/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LICaseTimeRemindDB;

/*
 * <p>ClassName: LICaseTimeRemindSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-02-28
 */
public class LICaseTimeRemindSchema implements Schema, Cloneable
{
	// @Field
	/** 理赔流水号 */
	private String SerialNo;
	/** 理赔批次号 */
	private String RgtNo;
	/** 理赔案件号 */
	private String CaseNo;
	/** 团单号 */
	private String GrpContNo;
	/** 保单号 */
	private String ContNo;
	/** 被保险人编码 */
	private String insuredno;
	/** 被保险人姓名 */
	private String insuredname;
	/** 性别 */
	private String sex;
	/** 生日 */
	private Date birthday;
	/** 证件类型 */
	private String idtype;
	/** 证件号 */
	private String idno;
	/** 其他证件类型 */
	private String othidtype;
	/** 其他证件号 */
	private String othidno;
	/** 证件生效日期 */
	private Date IDStartDate;
	/** 证件失效日期 */
	private Date IDEndDate;
	/** 提醒更新日期 */
	private Date Reminddate;
	/** 提醒更新时间 */
	private String RemindTime;
	/** 地址 */
	private String address;
	/** 国籍 */
	private String nativeplace;
	/** 保全号 */
	private String endorno;
	/** 保全类型 */
	private String endortype;
	/** 管理机构 */
	private String ManageCom;
	/** 其他1 */
	private String other1;
	/** 其他2 */
	private String other2;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date modifydate;
	/** 修改时间 */
	private String modifytime;

	public static final int FIELDNUM = 29;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LICaseTimeRemindSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "SerialNo";
		pk[1] = "insuredno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LICaseTimeRemindSchema cloned = (LICaseTimeRemindSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
		RgtNo = aRgtNo;
	}
	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getinsuredno()
	{
		return insuredno;
	}
	public void setinsuredno(String ainsuredno)
	{
		insuredno = ainsuredno;
	}
	public String getinsuredname()
	{
		return insuredname;
	}
	public void setinsuredname(String ainsuredname)
	{
		insuredname = ainsuredname;
	}
	public String getsex()
	{
		return sex;
	}
	public void setsex(String asex)
	{
		sex = asex;
	}
	public String getbirthday()
	{
		if( birthday != null )
			return fDate.getString(birthday);
		else
			return null;
	}
	public void setbirthday(Date abirthday)
	{
		birthday = abirthday;
	}
	public void setbirthday(String abirthday)
	{
		if (abirthday != null && !abirthday.equals("") )
		{
			birthday = fDate.getDate( abirthday );
		}
		else
			birthday = null;
	}

	public String getidtype()
	{
		return idtype;
	}
	public void setidtype(String aidtype)
	{
		idtype = aidtype;
	}
	public String getidno()
	{
		return idno;
	}
	public void setidno(String aidno)
	{
		idno = aidno;
	}
	public String getothidtype()
	{
		return othidtype;
	}
	public void setothidtype(String aothidtype)
	{
		othidtype = aothidtype;
	}
	public String getothidno()
	{
		return othidno;
	}
	public void setothidno(String aothidno)
	{
		othidno = aothidno;
	}
	public String getIDStartDate()
	{
		if( IDStartDate != null )
			return fDate.getString(IDStartDate);
		else
			return null;
	}
	public void setIDStartDate(Date aIDStartDate)
	{
		IDStartDate = aIDStartDate;
	}
	public void setIDStartDate(String aIDStartDate)
	{
		if (aIDStartDate != null && !aIDStartDate.equals("") )
		{
			IDStartDate = fDate.getDate( aIDStartDate );
		}
		else
			IDStartDate = null;
	}

	public String getIDEndDate()
	{
		if( IDEndDate != null )
			return fDate.getString(IDEndDate);
		else
			return null;
	}
	public void setIDEndDate(Date aIDEndDate)
	{
		IDEndDate = aIDEndDate;
	}
	public void setIDEndDate(String aIDEndDate)
	{
		if (aIDEndDate != null && !aIDEndDate.equals("") )
		{
			IDEndDate = fDate.getDate( aIDEndDate );
		}
		else
			IDEndDate = null;
	}

	public String getReminddate()
	{
		if( Reminddate != null )
			return fDate.getString(Reminddate);
		else
			return null;
	}
	public void setReminddate(Date aReminddate)
	{
		Reminddate = aReminddate;
	}
	public void setReminddate(String aReminddate)
	{
		if (aReminddate != null && !aReminddate.equals("") )
		{
			Reminddate = fDate.getDate( aReminddate );
		}
		else
			Reminddate = null;
	}

	public String getRemindTime()
	{
		return RemindTime;
	}
	public void setRemindTime(String aRemindTime)
	{
		RemindTime = aRemindTime;
	}
	public String getaddress()
	{
		return address;
	}
	public void setaddress(String aaddress)
	{
		address = aaddress;
	}
	public String getnativeplace()
	{
		return nativeplace;
	}
	public void setnativeplace(String anativeplace)
	{
		nativeplace = anativeplace;
	}
	public String getendorno()
	{
		return endorno;
	}
	public void setendorno(String aendorno)
	{
		endorno = aendorno;
	}
	public String getendortype()
	{
		return endortype;
	}
	public void setendortype(String aendortype)
	{
		endortype = aendortype;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getother1()
	{
		return other1;
	}
	public void setother1(String aother1)
	{
		other1 = aother1;
	}
	public String getother2()
	{
		return other2;
	}
	public void setother2(String aother2)
	{
		other2 = aother2;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getmodifydate()
	{
		if( modifydate != null )
			return fDate.getString(modifydate);
		else
			return null;
	}
	public void setmodifydate(Date amodifydate)
	{
		modifydate = amodifydate;
	}
	public void setmodifydate(String amodifydate)
	{
		if (amodifydate != null && !amodifydate.equals("") )
		{
			modifydate = fDate.getDate( amodifydate );
		}
		else
			modifydate = null;
	}

	public String getmodifytime()
	{
		return modifytime;
	}
	public void setmodifytime(String amodifytime)
	{
		modifytime = amodifytime;
	}

	/**
	* 使用另外一个 LICaseTimeRemindSchema 对象给 Schema 赋值
	* @param: aLICaseTimeRemindSchema LICaseTimeRemindSchema
	**/
	public void setSchema(LICaseTimeRemindSchema aLICaseTimeRemindSchema)
	{
		this.SerialNo = aLICaseTimeRemindSchema.getSerialNo();
		this.RgtNo = aLICaseTimeRemindSchema.getRgtNo();
		this.CaseNo = aLICaseTimeRemindSchema.getCaseNo();
		this.GrpContNo = aLICaseTimeRemindSchema.getGrpContNo();
		this.ContNo = aLICaseTimeRemindSchema.getContNo();
		this.insuredno = aLICaseTimeRemindSchema.getinsuredno();
		this.insuredname = aLICaseTimeRemindSchema.getinsuredname();
		this.sex = aLICaseTimeRemindSchema.getsex();
		this.birthday = fDate.getDate( aLICaseTimeRemindSchema.getbirthday());
		this.idtype = aLICaseTimeRemindSchema.getidtype();
		this.idno = aLICaseTimeRemindSchema.getidno();
		this.othidtype = aLICaseTimeRemindSchema.getothidtype();
		this.othidno = aLICaseTimeRemindSchema.getothidno();
		this.IDStartDate = fDate.getDate( aLICaseTimeRemindSchema.getIDStartDate());
		this.IDEndDate = fDate.getDate( aLICaseTimeRemindSchema.getIDEndDate());
		this.Reminddate = fDate.getDate( aLICaseTimeRemindSchema.getReminddate());
		this.RemindTime = aLICaseTimeRemindSchema.getRemindTime();
		this.address = aLICaseTimeRemindSchema.getaddress();
		this.nativeplace = aLICaseTimeRemindSchema.getnativeplace();
		this.endorno = aLICaseTimeRemindSchema.getendorno();
		this.endortype = aLICaseTimeRemindSchema.getendortype();
		this.ManageCom = aLICaseTimeRemindSchema.getManageCom();
		this.other1 = aLICaseTimeRemindSchema.getother1();
		this.other2 = aLICaseTimeRemindSchema.getother2();
		this.Operator = aLICaseTimeRemindSchema.getOperator();
		this.MakeDate = fDate.getDate( aLICaseTimeRemindSchema.getMakeDate());
		this.MakeTime = aLICaseTimeRemindSchema.getMakeTime();
		this.modifydate = fDate.getDate( aLICaseTimeRemindSchema.getmodifydate());
		this.modifytime = aLICaseTimeRemindSchema.getmodifytime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("insuredno") == null )
				this.insuredno = null;
			else
				this.insuredno = rs.getString("insuredno").trim();

			if( rs.getString("insuredname") == null )
				this.insuredname = null;
			else
				this.insuredname = rs.getString("insuredname").trim();

			if( rs.getString("sex") == null )
				this.sex = null;
			else
				this.sex = rs.getString("sex").trim();

			this.birthday = rs.getDate("birthday");
			if( rs.getString("idtype") == null )
				this.idtype = null;
			else
				this.idtype = rs.getString("idtype").trim();

			if( rs.getString("idno") == null )
				this.idno = null;
			else
				this.idno = rs.getString("idno").trim();

			if( rs.getString("othidtype") == null )
				this.othidtype = null;
			else
				this.othidtype = rs.getString("othidtype").trim();

			if( rs.getString("othidno") == null )
				this.othidno = null;
			else
				this.othidno = rs.getString("othidno").trim();

			this.IDStartDate = rs.getDate("IDStartDate");
			this.IDEndDate = rs.getDate("IDEndDate");
			this.Reminddate = rs.getDate("Reminddate");
			if( rs.getString("RemindTime") == null )
				this.RemindTime = null;
			else
				this.RemindTime = rs.getString("RemindTime").trim();

			if( rs.getString("address") == null )
				this.address = null;
			else
				this.address = rs.getString("address").trim();

			if( rs.getString("nativeplace") == null )
				this.nativeplace = null;
			else
				this.nativeplace = rs.getString("nativeplace").trim();

			if( rs.getString("endorno") == null )
				this.endorno = null;
			else
				this.endorno = rs.getString("endorno").trim();

			if( rs.getString("endortype") == null )
				this.endortype = null;
			else
				this.endortype = rs.getString("endortype").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("other1") == null )
				this.other1 = null;
			else
				this.other1 = rs.getString("other1").trim();

			if( rs.getString("other2") == null )
				this.other2 = null;
			else
				this.other2 = rs.getString("other2").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.modifydate = rs.getDate("modifydate");
			if( rs.getString("modifytime") == null )
				this.modifytime = null;
			else
				this.modifytime = rs.getString("modifytime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LICaseTimeRemind表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LICaseTimeRemindSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LICaseTimeRemindSchema getSchema()
	{
		LICaseTimeRemindSchema aLICaseTimeRemindSchema = new LICaseTimeRemindSchema();
		aLICaseTimeRemindSchema.setSchema(this);
		return aLICaseTimeRemindSchema;
	}

	public LICaseTimeRemindDB getDB()
	{
		LICaseTimeRemindDB aDBOper = new LICaseTimeRemindDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLICaseTimeRemind描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insuredno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insuredname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(idtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(idno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(othidtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(othidno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Reminddate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RemindTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(address)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(nativeplace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(endorno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(endortype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(other1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(other2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( modifydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(modifytime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLICaseTimeRemind>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			insuredno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			insuredname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			idtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			idno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			othidtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			othidno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			IDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			IDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			Reminddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			RemindTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			nativeplace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			endorno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			endortype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			other1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			other2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			modifydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			modifytime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LICaseTimeRemindSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("insuredno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuredno));
		}
		if (FCode.equals("insuredname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuredname));
		}
		if (FCode.equals("sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sex));
		}
		if (FCode.equals("birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getbirthday()));
		}
		if (FCode.equals("idtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(idtype));
		}
		if (FCode.equals("idno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(idno));
		}
		if (FCode.equals("othidtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(othidtype));
		}
		if (FCode.equals("othidno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(othidno));
		}
		if (FCode.equals("IDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
		}
		if (FCode.equals("IDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
		}
		if (FCode.equals("Reminddate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReminddate()));
		}
		if (FCode.equals("RemindTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemindTime));
		}
		if (FCode.equals("address"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(address));
		}
		if (FCode.equals("nativeplace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(nativeplace));
		}
		if (FCode.equals("endorno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(endorno));
		}
		if (FCode.equals("endortype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(endortype));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("other1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(other1));
		}
		if (FCode.equals("other2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(other2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("modifydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmodifydate()));
		}
		if (FCode.equals("modifytime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(modifytime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(insuredno);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(insuredname);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(sex);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getbirthday()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(idtype);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(idno);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(othidtype);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(othidno);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReminddate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(RemindTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(address);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(nativeplace);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(endorno);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(endortype);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(other1);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(other2);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmodifydate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(modifytime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("insuredno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insuredno = FValue.trim();
			}
			else
				insuredno = null;
		}
		if (FCode.equalsIgnoreCase("insuredname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insuredname = FValue.trim();
			}
			else
				insuredname = null;
		}
		if (FCode.equalsIgnoreCase("sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sex = FValue.trim();
			}
			else
				sex = null;
		}
		if (FCode.equalsIgnoreCase("birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				birthday = fDate.getDate( FValue );
			}
			else
				birthday = null;
		}
		if (FCode.equalsIgnoreCase("idtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				idtype = FValue.trim();
			}
			else
				idtype = null;
		}
		if (FCode.equalsIgnoreCase("idno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				idno = FValue.trim();
			}
			else
				idno = null;
		}
		if (FCode.equalsIgnoreCase("othidtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				othidtype = FValue.trim();
			}
			else
				othidtype = null;
		}
		if (FCode.equalsIgnoreCase("othidno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				othidno = FValue.trim();
			}
			else
				othidno = null;
		}
		if (FCode.equalsIgnoreCase("IDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDStartDate = fDate.getDate( FValue );
			}
			else
				IDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("IDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDEndDate = fDate.getDate( FValue );
			}
			else
				IDEndDate = null;
		}
		if (FCode.equalsIgnoreCase("Reminddate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Reminddate = fDate.getDate( FValue );
			}
			else
				Reminddate = null;
		}
		if (FCode.equalsIgnoreCase("RemindTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RemindTime = FValue.trim();
			}
			else
				RemindTime = null;
		}
		if (FCode.equalsIgnoreCase("address"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				address = FValue.trim();
			}
			else
				address = null;
		}
		if (FCode.equalsIgnoreCase("nativeplace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				nativeplace = FValue.trim();
			}
			else
				nativeplace = null;
		}
		if (FCode.equalsIgnoreCase("endorno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				endorno = FValue.trim();
			}
			else
				endorno = null;
		}
		if (FCode.equalsIgnoreCase("endortype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				endortype = FValue.trim();
			}
			else
				endortype = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("other1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				other1 = FValue.trim();
			}
			else
				other1 = null;
		}
		if (FCode.equalsIgnoreCase("other2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				other2 = FValue.trim();
			}
			else
				other2 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("modifydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				modifydate = fDate.getDate( FValue );
			}
			else
				modifydate = null;
		}
		if (FCode.equalsIgnoreCase("modifytime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				modifytime = FValue.trim();
			}
			else
				modifytime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LICaseTimeRemindSchema other = (LICaseTimeRemindSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (RgtNo == null ? other.getRgtNo() == null : RgtNo.equals(other.getRgtNo()))
			&& (CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (insuredno == null ? other.getinsuredno() == null : insuredno.equals(other.getinsuredno()))
			&& (insuredname == null ? other.getinsuredname() == null : insuredname.equals(other.getinsuredname()))
			&& (sex == null ? other.getsex() == null : sex.equals(other.getsex()))
			&& (birthday == null ? other.getbirthday() == null : fDate.getString(birthday).equals(other.getbirthday()))
			&& (idtype == null ? other.getidtype() == null : idtype.equals(other.getidtype()))
			&& (idno == null ? other.getidno() == null : idno.equals(other.getidno()))
			&& (othidtype == null ? other.getothidtype() == null : othidtype.equals(other.getothidtype()))
			&& (othidno == null ? other.getothidno() == null : othidno.equals(other.getothidno()))
			&& (IDStartDate == null ? other.getIDStartDate() == null : fDate.getString(IDStartDate).equals(other.getIDStartDate()))
			&& (IDEndDate == null ? other.getIDEndDate() == null : fDate.getString(IDEndDate).equals(other.getIDEndDate()))
			&& (Reminddate == null ? other.getReminddate() == null : fDate.getString(Reminddate).equals(other.getReminddate()))
			&& (RemindTime == null ? other.getRemindTime() == null : RemindTime.equals(other.getRemindTime()))
			&& (address == null ? other.getaddress() == null : address.equals(other.getaddress()))
			&& (nativeplace == null ? other.getnativeplace() == null : nativeplace.equals(other.getnativeplace()))
			&& (endorno == null ? other.getendorno() == null : endorno.equals(other.getendorno()))
			&& (endortype == null ? other.getendortype() == null : endortype.equals(other.getendortype()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (other1 == null ? other.getother1() == null : other1.equals(other.getother1()))
			&& (other2 == null ? other.getother2() == null : other2.equals(other.getother2()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (modifydate == null ? other.getmodifydate() == null : fDate.getString(modifydate).equals(other.getmodifydate()))
			&& (modifytime == null ? other.getmodifytime() == null : modifytime.equals(other.getmodifytime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("RgtNo") ) {
			return 1;
		}
		if( strFieldName.equals("CaseNo") ) {
			return 2;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 3;
		}
		if( strFieldName.equals("ContNo") ) {
			return 4;
		}
		if( strFieldName.equals("insuredno") ) {
			return 5;
		}
		if( strFieldName.equals("insuredname") ) {
			return 6;
		}
		if( strFieldName.equals("sex") ) {
			return 7;
		}
		if( strFieldName.equals("birthday") ) {
			return 8;
		}
		if( strFieldName.equals("idtype") ) {
			return 9;
		}
		if( strFieldName.equals("idno") ) {
			return 10;
		}
		if( strFieldName.equals("othidtype") ) {
			return 11;
		}
		if( strFieldName.equals("othidno") ) {
			return 12;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return 13;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return 14;
		}
		if( strFieldName.equals("Reminddate") ) {
			return 15;
		}
		if( strFieldName.equals("RemindTime") ) {
			return 16;
		}
		if( strFieldName.equals("address") ) {
			return 17;
		}
		if( strFieldName.equals("nativeplace") ) {
			return 18;
		}
		if( strFieldName.equals("endorno") ) {
			return 19;
		}
		if( strFieldName.equals("endortype") ) {
			return 20;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 21;
		}
		if( strFieldName.equals("other1") ) {
			return 22;
		}
		if( strFieldName.equals("other2") ) {
			return 23;
		}
		if( strFieldName.equals("Operator") ) {
			return 24;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 25;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 26;
		}
		if( strFieldName.equals("modifydate") ) {
			return 27;
		}
		if( strFieldName.equals("modifytime") ) {
			return 28;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "RgtNo";
				break;
			case 2:
				strFieldName = "CaseNo";
				break;
			case 3:
				strFieldName = "GrpContNo";
				break;
			case 4:
				strFieldName = "ContNo";
				break;
			case 5:
				strFieldName = "insuredno";
				break;
			case 6:
				strFieldName = "insuredname";
				break;
			case 7:
				strFieldName = "sex";
				break;
			case 8:
				strFieldName = "birthday";
				break;
			case 9:
				strFieldName = "idtype";
				break;
			case 10:
				strFieldName = "idno";
				break;
			case 11:
				strFieldName = "othidtype";
				break;
			case 12:
				strFieldName = "othidno";
				break;
			case 13:
				strFieldName = "IDStartDate";
				break;
			case 14:
				strFieldName = "IDEndDate";
				break;
			case 15:
				strFieldName = "Reminddate";
				break;
			case 16:
				strFieldName = "RemindTime";
				break;
			case 17:
				strFieldName = "address";
				break;
			case 18:
				strFieldName = "nativeplace";
				break;
			case 19:
				strFieldName = "endorno";
				break;
			case 20:
				strFieldName = "endortype";
				break;
			case 21:
				strFieldName = "ManageCom";
				break;
			case 22:
				strFieldName = "other1";
				break;
			case 23:
				strFieldName = "other2";
				break;
			case 24:
				strFieldName = "Operator";
				break;
			case 25:
				strFieldName = "MakeDate";
				break;
			case 26:
				strFieldName = "MakeTime";
				break;
			case 27:
				strFieldName = "modifydate";
				break;
			case 28:
				strFieldName = "modifytime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insuredno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insuredname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("idtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("idno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("othidtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("othidno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Reminddate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RemindTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("address") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("nativeplace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endorno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endortype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("other1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("other2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("modifydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("modifytime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
