/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.ES_DOC_QC_DEFDB;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: ES_DOC_QC_DEFSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-04
 */
public class ES_DOC_QC_DEFSchema implements Schema, Cloneable {
    // @Field
    /** 管理机构 */
    private String ManageCom;
    /** 业务类型 */
    private String BussType;
    /** 单证细类 */
    private String SubType;
    /** 备注 */
    private String Remark;
    /** 状态 */
    private String State;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public ES_DOC_QC_DEFSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ManageCom";
        pk[1] = "BussType";
        pk[2] = "SubType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        ES_DOC_QC_DEFSchema cloned = (ES_DOC_QC_DEFSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String aBussType) {
        BussType = aBussType;
    }

    public String getSubType() {
        return SubType;
    }

    public void setSubType(String aSubType) {
        SubType = aSubType;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    /**
     * 使用另外一个 ES_DOC_QC_DEFSchema 对象给 Schema 赋值
     * @param: aES_DOC_QC_DEFSchema ES_DOC_QC_DEFSchema
     **/
    public void setSchema(ES_DOC_QC_DEFSchema aES_DOC_QC_DEFSchema) {
        this.ManageCom = aES_DOC_QC_DEFSchema.getManageCom();
        this.BussType = aES_DOC_QC_DEFSchema.getBussType();
        this.SubType = aES_DOC_QC_DEFSchema.getSubType();
        this.Remark = aES_DOC_QC_DEFSchema.getRemark();
        this.State = aES_DOC_QC_DEFSchema.getState();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("BussType") == null) {
                this.BussType = null;
            } else {
                this.BussType = rs.getString("BussType").trim();
            }

            if (rs.getString("SubType") == null) {
                this.SubType = null;
            } else {
                this.SubType = rs.getString("SubType").trim();
            }

            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的ES_DOC_QC_DEF表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_DEFSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public ES_DOC_QC_DEFSchema getSchema() {
        ES_DOC_QC_DEFSchema aES_DOC_QC_DEFSchema = new ES_DOC_QC_DEFSchema();
        aES_DOC_QC_DEFSchema.setSchema(this);
        return aES_DOC_QC_DEFSchema;
    }

    public ES_DOC_QC_DEFDB getDB() {
        ES_DOC_QC_DEFDB aDBOper = new ES_DOC_QC_DEFDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpES_DOC_QC_DEF描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BussType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SubType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpES_DOC_QC_DEF>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            SubType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ES_DOC_QC_DEFSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equalsIgnoreCase("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BussType));
        }
        if (FCode.equalsIgnoreCase("SubType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubType));
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equalsIgnoreCase("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(BussType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(SubType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            if (FValue != null && !FValue.equals("")) {
                BussType = FValue.trim();
            } else {
                BussType = null;
            }
        }
        if (FCode.equalsIgnoreCase("SubType")) {
            if (FValue != null && !FValue.equals("")) {
                SubType = FValue.trim();
            } else {
                SubType = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        ES_DOC_QC_DEFSchema other = (ES_DOC_QC_DEFSchema) otherObject;
        return
                ManageCom.equals(other.getManageCom())
                && BussType.equals(other.getBussType())
                && SubType.equals(other.getSubType())
                && Remark.equals(other.getRemark())
                && State.equals(other.getState());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ManageCom")) {
            return 0;
        }
        if (strFieldName.equals("BussType")) {
            return 1;
        }
        if (strFieldName.equals("SubType")) {
            return 2;
        }
        if (strFieldName.equals("Remark")) {
            return 3;
        }
        if (strFieldName.equals("State")) {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ManageCom";
            break;
        case 1:
            strFieldName = "BussType";
            break;
        case 2:
            strFieldName = "SubType";
            break;
        case 3:
            strFieldName = "Remark";
            break;
        case 4:
            strFieldName = "State";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BussType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
