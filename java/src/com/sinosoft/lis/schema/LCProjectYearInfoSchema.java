/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCProjectYearInfoDB;

/*
 * <p>ClassName: LCProjectYearInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2013-07-03
 */
public class LCProjectYearInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 项目编码 */
	private String ProjectNo;
	/** 年度 */
	private String ProjectYear;
	/** 被保人人数 */
	private int InsuredPeoples;
	/** 人均保费 */
	private String AveragePrem;
	/** 保证金 */
	private double Bond;
	/** 止损线 */
	private String StopLine;
	/** 预计赔付率 */
	private double ExpecteRate;
	/** 综合赔付率 */
	private double ComplexRate;
	/** 状态 */
	private String State;
	/** 首次创建用户 */
	private String FirstOperator;
	/** 年度创建日期 */
	private Date CreateDate;
	/** 年度创建时间 */
	private String CreateTime;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 联合办公人力数 */
	private int PeoplesNumber;
	/** 结余返还条款标志 */
	private String BalanceTermFlag;
	/** 保费回补条款标志 */
	private String PremCoverTermFlag;
	/** 覆盖市县数 */
	private int CountyCount;
	/** 结余返还条款 */
	private String BalanceTerm;
	/** 保费回补条款 */
	private String PremCoverTerm;
	/** 共保标识 */
	private String CoInsuranceFlag;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCProjectYearInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "ProjectNo";
		pk[1] = "ProjectYear";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCProjectYearInfoSchema cloned = (LCProjectYearInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getProjectNo()
	{
		return ProjectNo;
	}
	public void setProjectNo(String aProjectNo)
	{
		ProjectNo = aProjectNo;
	}
	public String getProjectYear()
	{
		return ProjectYear;
	}
	public void setProjectYear(String aProjectYear)
	{
		ProjectYear = aProjectYear;
	}
	public int getInsuredPeoples()
	{
		return InsuredPeoples;
	}
	public void setInsuredPeoples(int aInsuredPeoples)
	{
		InsuredPeoples = aInsuredPeoples;
	}
	public void setInsuredPeoples(String aInsuredPeoples)
	{
		if (aInsuredPeoples != null && !aInsuredPeoples.equals(""))
		{
			Integer tInteger = new Integer(aInsuredPeoples);
			int i = tInteger.intValue();
			InsuredPeoples = i;
		}
	}

	public String getAveragePrem()
	{
		return AveragePrem;
	}
	public void setAveragePrem(String aAveragePrem)
	{
		AveragePrem = aAveragePrem;
	}
	public double getBond()
	{
		return Bond;
	}
	public void setBond(double aBond)
	{
		Bond = Arith.round(aBond,2);
	}
	public void setBond(String aBond)
	{
		if (aBond != null && !aBond.equals(""))
		{
			Double tDouble = new Double(aBond);
			double d = tDouble.doubleValue();
                Bond = Arith.round(d,2);
		}
	}

	public String getStopLine()
	{
		return StopLine;
	}
	public void setStopLine(String aStopLine)
	{
		StopLine = aStopLine;
	}
	public double getExpecteRate()
	{
		return ExpecteRate;
	}
	public void setExpecteRate(double aExpecteRate)
	{
		ExpecteRate = Arith.round(aExpecteRate,6);
	}
	public void setExpecteRate(String aExpecteRate)
	{
		if (aExpecteRate != null && !aExpecteRate.equals(""))
		{
			Double tDouble = new Double(aExpecteRate);
			double d = tDouble.doubleValue();
                ExpecteRate = Arith.round(d,6);
		}
	}

	public double getComplexRate()
	{
		return ComplexRate;
	}
	public void setComplexRate(double aComplexRate)
	{
		ComplexRate = Arith.round(aComplexRate,6);
	}
	public void setComplexRate(String aComplexRate)
	{
		if (aComplexRate != null && !aComplexRate.equals(""))
		{
			Double tDouble = new Double(aComplexRate);
			double d = tDouble.doubleValue();
                ComplexRate = Arith.round(d,6);
		}
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getFirstOperator()
	{
		return FirstOperator;
	}
	public void setFirstOperator(String aFirstOperator)
	{
		FirstOperator = aFirstOperator;
	}
	public String getCreateDate()
	{
		if( CreateDate != null )
			return fDate.getString(CreateDate);
		else
			return null;
	}
	public void setCreateDate(Date aCreateDate)
	{
		CreateDate = aCreateDate;
	}
	public void setCreateDate(String aCreateDate)
	{
		if (aCreateDate != null && !aCreateDate.equals("") )
		{
			CreateDate = fDate.getDate( aCreateDate );
		}
		else
			CreateDate = null;
	}

	public String getCreateTime()
	{
		return CreateTime;
	}
	public void setCreateTime(String aCreateTime)
	{
		CreateTime = aCreateTime;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public int getPeoplesNumber()
	{
		return PeoplesNumber;
	}
	public void setPeoplesNumber(int aPeoplesNumber)
	{
		PeoplesNumber = aPeoplesNumber;
	}
	public void setPeoplesNumber(String aPeoplesNumber)
	{
		if (aPeoplesNumber != null && !aPeoplesNumber.equals(""))
		{
			Integer tInteger = new Integer(aPeoplesNumber);
			int i = tInteger.intValue();
			PeoplesNumber = i;
		}
	}

	public String getBalanceTermFlag()
	{
		return BalanceTermFlag;
	}
	public void setBalanceTermFlag(String aBalanceTermFlag)
	{
		BalanceTermFlag = aBalanceTermFlag;
	}
	public String getPremCoverTermFlag()
	{
		return PremCoverTermFlag;
	}
	public void setPremCoverTermFlag(String aPremCoverTermFlag)
	{
		PremCoverTermFlag = aPremCoverTermFlag;
	}
	public int getCountyCount()
	{
		return CountyCount;
	}
	public void setCountyCount(int aCountyCount)
	{
		CountyCount = aCountyCount;
	}
	public void setCountyCount(String aCountyCount)
	{
		if (aCountyCount != null && !aCountyCount.equals(""))
		{
			Integer tInteger = new Integer(aCountyCount);
			int i = tInteger.intValue();
			CountyCount = i;
		}
	}

	public String getBalanceTerm()
	{
		return BalanceTerm;
	}
	public void setBalanceTerm(String aBalanceTerm)
	{
		BalanceTerm = aBalanceTerm;
	}
	public String getPremCoverTerm()
	{
		return PremCoverTerm;
	}
	public void setPremCoverTerm(String aPremCoverTerm)
	{
		PremCoverTerm = aPremCoverTerm;
	}
	public String getCoInsuranceFlag()
	{
		return CoInsuranceFlag;
	}
	public void setCoInsuranceFlag(String aCoInsuranceFlag)
	{
		CoInsuranceFlag = aCoInsuranceFlag;
	}

	/**
	* 使用另外一个 LCProjectYearInfoSchema 对象给 Schema 赋值
	* @param: aLCProjectYearInfoSchema LCProjectYearInfoSchema
	**/
	public void setSchema(LCProjectYearInfoSchema aLCProjectYearInfoSchema)
	{
		this.ProjectNo = aLCProjectYearInfoSchema.getProjectNo();
		this.ProjectYear = aLCProjectYearInfoSchema.getProjectYear();
		this.InsuredPeoples = aLCProjectYearInfoSchema.getInsuredPeoples();
		this.AveragePrem = aLCProjectYearInfoSchema.getAveragePrem();
		this.Bond = aLCProjectYearInfoSchema.getBond();
		this.StopLine = aLCProjectYearInfoSchema.getStopLine();
		this.ExpecteRate = aLCProjectYearInfoSchema.getExpecteRate();
		this.ComplexRate = aLCProjectYearInfoSchema.getComplexRate();
		this.State = aLCProjectYearInfoSchema.getState();
		this.FirstOperator = aLCProjectYearInfoSchema.getFirstOperator();
		this.CreateDate = fDate.getDate( aLCProjectYearInfoSchema.getCreateDate());
		this.CreateTime = aLCProjectYearInfoSchema.getCreateTime();
		this.ManageCom = aLCProjectYearInfoSchema.getManageCom();
		this.Operator = aLCProjectYearInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCProjectYearInfoSchema.getMakeDate());
		this.MakeTime = aLCProjectYearInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCProjectYearInfoSchema.getModifyDate());
		this.ModifyTime = aLCProjectYearInfoSchema.getModifyTime();
		this.PeoplesNumber = aLCProjectYearInfoSchema.getPeoplesNumber();
		this.BalanceTermFlag = aLCProjectYearInfoSchema.getBalanceTermFlag();
		this.PremCoverTermFlag = aLCProjectYearInfoSchema.getPremCoverTermFlag();
		this.CountyCount = aLCProjectYearInfoSchema.getCountyCount();
		this.BalanceTerm = aLCProjectYearInfoSchema.getBalanceTerm();
		this.PremCoverTerm = aLCProjectYearInfoSchema.getPremCoverTerm();
		this.CoInsuranceFlag = aLCProjectYearInfoSchema.getCoInsuranceFlag();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ProjectNo") == null )
				this.ProjectNo = null;
			else
				this.ProjectNo = rs.getString("ProjectNo").trim();

			if( rs.getString("ProjectYear") == null )
				this.ProjectYear = null;
			else
				this.ProjectYear = rs.getString("ProjectYear").trim();

			this.InsuredPeoples = rs.getInt("InsuredPeoples");
			if( rs.getString("AveragePrem") == null )
				this.AveragePrem = null;
			else
				this.AveragePrem = rs.getString("AveragePrem").trim();

			this.Bond = rs.getDouble("Bond");
			if( rs.getString("StopLine") == null )
				this.StopLine = null;
			else
				this.StopLine = rs.getString("StopLine").trim();

			this.ExpecteRate = rs.getDouble("ExpecteRate");
			this.ComplexRate = rs.getDouble("ComplexRate");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("FirstOperator") == null )
				this.FirstOperator = null;
			else
				this.FirstOperator = rs.getString("FirstOperator").trim();

			this.CreateDate = rs.getDate("CreateDate");
			if( rs.getString("CreateTime") == null )
				this.CreateTime = null;
			else
				this.CreateTime = rs.getString("CreateTime").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.PeoplesNumber = rs.getInt("PeoplesNumber");
			if( rs.getString("BalanceTermFlag") == null )
				this.BalanceTermFlag = null;
			else
				this.BalanceTermFlag = rs.getString("BalanceTermFlag").trim();

			if( rs.getString("PremCoverTermFlag") == null )
				this.PremCoverTermFlag = null;
			else
				this.PremCoverTermFlag = rs.getString("PremCoverTermFlag").trim();

			this.CountyCount = rs.getInt("CountyCount");
			if( rs.getString("BalanceTerm") == null )
				this.BalanceTerm = null;
			else
				this.BalanceTerm = rs.getString("BalanceTerm").trim();

			if( rs.getString("PremCoverTerm") == null )
				this.PremCoverTerm = null;
			else
				this.PremCoverTerm = rs.getString("PremCoverTerm").trim();

			if( rs.getString("CoInsuranceFlag") == null )
				this.CoInsuranceFlag = null;
			else
				this.CoInsuranceFlag = rs.getString("CoInsuranceFlag").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCProjectYearInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectYearInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCProjectYearInfoSchema getSchema()
	{
		LCProjectYearInfoSchema aLCProjectYearInfoSchema = new LCProjectYearInfoSchema();
		aLCProjectYearInfoSchema.setSchema(this);
		return aLCProjectYearInfoSchema;
	}

	public LCProjectYearInfoDB getDB()
	{
		LCProjectYearInfoDB aDBOper = new LCProjectYearInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCProjectYearInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ProjectNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuredPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AveragePrem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Bond));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StopLine)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExpecteRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ComplexRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CreateDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CreateTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PeoplesNumber));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BalanceTermFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremCoverTermFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CountyCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BalanceTerm)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremCoverTerm)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CoInsuranceFlag));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCProjectYearInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ProjectNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ProjectYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			InsuredPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).intValue();
			AveragePrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Bond = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			StopLine = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ExpecteRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			ComplexRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			FirstOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CreateDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			CreateTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			PeoplesNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).intValue();
			BalanceTermFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PremCoverTermFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			CountyCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).intValue();
			BalanceTerm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			PremCoverTerm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			CoInsuranceFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectYearInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ProjectNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectNo));
		}
		if (FCode.equals("ProjectYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectYear));
		}
		if (FCode.equals("InsuredPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredPeoples));
		}
		if (FCode.equals("AveragePrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AveragePrem));
		}
		if (FCode.equals("Bond"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bond));
		}
		if (FCode.equals("StopLine"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StopLine));
		}
		if (FCode.equals("ExpecteRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExpecteRate));
		}
		if (FCode.equals("ComplexRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComplexRate));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("FirstOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstOperator));
		}
		if (FCode.equals("CreateDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCreateDate()));
		}
		if (FCode.equals("CreateTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CreateTime));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("PeoplesNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PeoplesNumber));
		}
		if (FCode.equals("BalanceTermFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceTermFlag));
		}
		if (FCode.equals("PremCoverTermFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremCoverTermFlag));
		}
		if (FCode.equals("CountyCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CountyCount));
		}
		if (FCode.equals("BalanceTerm"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceTerm));
		}
		if (FCode.equals("PremCoverTerm"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremCoverTerm));
		}
		if (FCode.equals("CoInsuranceFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CoInsuranceFlag));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ProjectNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ProjectYear);
				break;
			case 2:
				strFieldValue = String.valueOf(InsuredPeoples);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AveragePrem);
				break;
			case 4:
				strFieldValue = String.valueOf(Bond);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(StopLine);
				break;
			case 6:
				strFieldValue = String.valueOf(ExpecteRate);
				break;
			case 7:
				strFieldValue = String.valueOf(ComplexRate);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(FirstOperator);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCreateDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(CreateTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 18:
				strFieldValue = String.valueOf(PeoplesNumber);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(BalanceTermFlag);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(PremCoverTermFlag);
				break;
			case 21:
				strFieldValue = String.valueOf(CountyCount);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(BalanceTerm);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(PremCoverTerm);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(CoInsuranceFlag);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ProjectNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectNo = FValue.trim();
			}
			else
				ProjectNo = null;
		}
		if (FCode.equalsIgnoreCase("ProjectYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectYear = FValue.trim();
			}
			else
				ProjectYear = null;
		}
		if (FCode.equalsIgnoreCase("InsuredPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InsuredPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("AveragePrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AveragePrem = FValue.trim();
			}
			else
				AveragePrem = null;
		}
		if (FCode.equalsIgnoreCase("Bond"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Bond = d;
			}
		}
		if (FCode.equalsIgnoreCase("StopLine"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StopLine = FValue.trim();
			}
			else
				StopLine = null;
		}
		if (FCode.equalsIgnoreCase("ExpecteRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExpecteRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("ComplexRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ComplexRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("FirstOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstOperator = FValue.trim();
			}
			else
				FirstOperator = null;
		}
		if (FCode.equalsIgnoreCase("CreateDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CreateDate = fDate.getDate( FValue );
			}
			else
				CreateDate = null;
		}
		if (FCode.equalsIgnoreCase("CreateTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CreateTime = FValue.trim();
			}
			else
				CreateTime = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("PeoplesNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PeoplesNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("BalanceTermFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BalanceTermFlag = FValue.trim();
			}
			else
				BalanceTermFlag = null;
		}
		if (FCode.equalsIgnoreCase("PremCoverTermFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremCoverTermFlag = FValue.trim();
			}
			else
				PremCoverTermFlag = null;
		}
		if (FCode.equalsIgnoreCase("CountyCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				CountyCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("BalanceTerm"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BalanceTerm = FValue.trim();
			}
			else
				BalanceTerm = null;
		}
		if (FCode.equalsIgnoreCase("PremCoverTerm"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremCoverTerm = FValue.trim();
			}
			else
				PremCoverTerm = null;
		}
		if (FCode.equalsIgnoreCase("CoInsuranceFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CoInsuranceFlag = FValue.trim();
			}
			else
				CoInsuranceFlag = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCProjectYearInfoSchema other = (LCProjectYearInfoSchema)otherObject;
		return
			(ProjectNo == null ? other.getProjectNo() == null : ProjectNo.equals(other.getProjectNo()))
			&& (ProjectYear == null ? other.getProjectYear() == null : ProjectYear.equals(other.getProjectYear()))
			&& InsuredPeoples == other.getInsuredPeoples()
			&& (AveragePrem == null ? other.getAveragePrem() == null : AveragePrem.equals(other.getAveragePrem()))
			&& Bond == other.getBond()
			&& (StopLine == null ? other.getStopLine() == null : StopLine.equals(other.getStopLine()))
			&& ExpecteRate == other.getExpecteRate()
			&& ComplexRate == other.getComplexRate()
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (FirstOperator == null ? other.getFirstOperator() == null : FirstOperator.equals(other.getFirstOperator()))
			&& (CreateDate == null ? other.getCreateDate() == null : fDate.getString(CreateDate).equals(other.getCreateDate()))
			&& (CreateTime == null ? other.getCreateTime() == null : CreateTime.equals(other.getCreateTime()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& PeoplesNumber == other.getPeoplesNumber()
			&& (BalanceTermFlag == null ? other.getBalanceTermFlag() == null : BalanceTermFlag.equals(other.getBalanceTermFlag()))
			&& (PremCoverTermFlag == null ? other.getPremCoverTermFlag() == null : PremCoverTermFlag.equals(other.getPremCoverTermFlag()))
			&& CountyCount == other.getCountyCount()
			&& (BalanceTerm == null ? other.getBalanceTerm() == null : BalanceTerm.equals(other.getBalanceTerm()))
			&& (PremCoverTerm == null ? other.getPremCoverTerm() == null : PremCoverTerm.equals(other.getPremCoverTerm()))
			&& (CoInsuranceFlag == null ? other.getCoInsuranceFlag() == null : CoInsuranceFlag.equals(other.getCoInsuranceFlag()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ProjectNo") ) {
			return 0;
		}
		if( strFieldName.equals("ProjectYear") ) {
			return 1;
		}
		if( strFieldName.equals("InsuredPeoples") ) {
			return 2;
		}
		if( strFieldName.equals("AveragePrem") ) {
			return 3;
		}
		if( strFieldName.equals("Bond") ) {
			return 4;
		}
		if( strFieldName.equals("StopLine") ) {
			return 5;
		}
		if( strFieldName.equals("ExpecteRate") ) {
			return 6;
		}
		if( strFieldName.equals("ComplexRate") ) {
			return 7;
		}
		if( strFieldName.equals("State") ) {
			return 8;
		}
		if( strFieldName.equals("FirstOperator") ) {
			return 9;
		}
		if( strFieldName.equals("CreateDate") ) {
			return 10;
		}
		if( strFieldName.equals("CreateTime") ) {
			return 11;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 12;
		}
		if( strFieldName.equals("Operator") ) {
			return 13;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 14;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 17;
		}
		if( strFieldName.equals("PeoplesNumber") ) {
			return 18;
		}
		if( strFieldName.equals("BalanceTermFlag") ) {
			return 19;
		}
		if( strFieldName.equals("PremCoverTermFlag") ) {
			return 20;
		}
		if( strFieldName.equals("CountyCount") ) {
			return 21;
		}
		if( strFieldName.equals("BalanceTerm") ) {
			return 22;
		}
		if( strFieldName.equals("PremCoverTerm") ) {
			return 23;
		}
		if( strFieldName.equals("CoInsuranceFlag") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ProjectNo";
				break;
			case 1:
				strFieldName = "ProjectYear";
				break;
			case 2:
				strFieldName = "InsuredPeoples";
				break;
			case 3:
				strFieldName = "AveragePrem";
				break;
			case 4:
				strFieldName = "Bond";
				break;
			case 5:
				strFieldName = "StopLine";
				break;
			case 6:
				strFieldName = "ExpecteRate";
				break;
			case 7:
				strFieldName = "ComplexRate";
				break;
			case 8:
				strFieldName = "State";
				break;
			case 9:
				strFieldName = "FirstOperator";
				break;
			case 10:
				strFieldName = "CreateDate";
				break;
			case 11:
				strFieldName = "CreateTime";
				break;
			case 12:
				strFieldName = "ManageCom";
				break;
			case 13:
				strFieldName = "Operator";
				break;
			case 14:
				strFieldName = "MakeDate";
				break;
			case 15:
				strFieldName = "MakeTime";
				break;
			case 16:
				strFieldName = "ModifyDate";
				break;
			case 17:
				strFieldName = "ModifyTime";
				break;
			case 18:
				strFieldName = "PeoplesNumber";
				break;
			case 19:
				strFieldName = "BalanceTermFlag";
				break;
			case 20:
				strFieldName = "PremCoverTermFlag";
				break;
			case 21:
				strFieldName = "CountyCount";
				break;
			case 22:
				strFieldName = "BalanceTerm";
				break;
			case 23:
				strFieldName = "PremCoverTerm";
				break;
			case 24:
				strFieldName = "CoInsuranceFlag";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ProjectNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AveragePrem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bond") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StopLine") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExpecteRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ComplexRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CreateDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CreateTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PeoplesNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BalanceTermFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremCoverTermFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CountyCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BalanceTerm") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremCoverTerm") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CoInsuranceFlag") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_INT;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_INT;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_INT;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
