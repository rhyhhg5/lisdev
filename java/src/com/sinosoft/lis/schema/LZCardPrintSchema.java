/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZCardPrintDB;

/*
 * <p>ClassName: LZCardPrintSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 单证编码
 * @CreateDate：2006-09-05
 */
public class LZCardPrintSchema implements Schema, Cloneable {
    // @Field
    /** 印刷批次号 */
    private String PrtNo;
    /** 单证编码 */
    private String CertifyCode;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 附标号 */
    private String SubCode;
    /** 最大金额 */
    private double MaxMoney;
    /** 最大日期 */
    private Date MaxDate;
    /** 厂商编码 */
    private String ComCode;
    /** 电话 */
    private String Phone;
    /** 联系人 */
    private String LinkMan;
    /** 单证价格 */
    private double CertifyPrice;
    /** 管理机构 */
    private String ManageCom;
    /** 定单操作员 */
    private String OperatorInput;
    /** 定单日期 */
    private Date InputDate;
    /** 定单操作日期 */
    private Date InputMakeDate;
    /** 提单人 */
    private String GetMan;
    /** 提单日期 */
    private Date GetDate;
    /** 提单操作员 */
    private String OperatorGet;
    /** 起始号 */
    private String StartNo;
    /** 终止号 */
    private String EndNo;
    /** 提单操作日期 */
    private Date GetMakeDate;
    /** 数量 */
    private int SumCount;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 状态 */
    private String State;
    /** 激活日期 */
    private Date ActiveDate;
    /** 回销期 */
    private int BackDate;

    public static final int FIELDNUM = 27; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LZCardPrintSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "PrtNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LZCardPrintSchema cloned = (LZCardPrintSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }

    public String getCertifyCode() {
        return CertifyCode;
    }

    public void setCertifyCode(String aCertifyCode) {
        CertifyCode = aCertifyCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion() {
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }

    public String getSubCode() {
        return SubCode;
    }

    public void setSubCode(String aSubCode) {
        SubCode = aSubCode;
    }

    public double getMaxMoney() {
        return MaxMoney;
    }

    public void setMaxMoney(double aMaxMoney) {
        MaxMoney = Arith.round(aMaxMoney, 2);
    }

    public void setMaxMoney(String aMaxMoney) {
        if (aMaxMoney != null && !aMaxMoney.equals("")) {
            Double tDouble = new Double(aMaxMoney);
            double d = tDouble.doubleValue();
            MaxMoney = Arith.round(d, 2);
        }
    }

    public String getMaxDate() {
        if (MaxDate != null) {
            return fDate.getString(MaxDate);
        } else {
            return null;
        }
    }

    public void setMaxDate(Date aMaxDate) {
        MaxDate = aMaxDate;
    }

    public void setMaxDate(String aMaxDate) {
        if (aMaxDate != null && !aMaxDate.equals("")) {
            MaxDate = fDate.getDate(aMaxDate);
        } else {
            MaxDate = null;
        }
    }

    public String getComCode() {
        return ComCode;
    }

    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String aPhone) {
        Phone = aPhone;
    }

    public String getLinkMan() {
        return LinkMan;
    }

    public void setLinkMan(String aLinkMan) {
        LinkMan = aLinkMan;
    }

    public double getCertifyPrice() {
        return CertifyPrice;
    }

    public void setCertifyPrice(double aCertifyPrice) {
        CertifyPrice = Arith.round(aCertifyPrice, 2);
    }

    public void setCertifyPrice(String aCertifyPrice) {
        if (aCertifyPrice != null && !aCertifyPrice.equals("")) {
            Double tDouble = new Double(aCertifyPrice);
            double d = tDouble.doubleValue();
            CertifyPrice = Arith.round(d, 2);
        }
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperatorInput() {
        return OperatorInput;
    }

    public void setOperatorInput(String aOperatorInput) {
        OperatorInput = aOperatorInput;
    }

    public String getInputDate() {
        if (InputDate != null) {
            return fDate.getString(InputDate);
        } else {
            return null;
        }
    }

    public void setInputDate(Date aInputDate) {
        InputDate = aInputDate;
    }

    public void setInputDate(String aInputDate) {
        if (aInputDate != null && !aInputDate.equals("")) {
            InputDate = fDate.getDate(aInputDate);
        } else {
            InputDate = null;
        }
    }

    public String getInputMakeDate() {
        if (InputMakeDate != null) {
            return fDate.getString(InputMakeDate);
        } else {
            return null;
        }
    }

    public void setInputMakeDate(Date aInputMakeDate) {
        InputMakeDate = aInputMakeDate;
    }

    public void setInputMakeDate(String aInputMakeDate) {
        if (aInputMakeDate != null && !aInputMakeDate.equals("")) {
            InputMakeDate = fDate.getDate(aInputMakeDate);
        } else {
            InputMakeDate = null;
        }
    }

    public String getGetMan() {
        return GetMan;
    }

    public void setGetMan(String aGetMan) {
        GetMan = aGetMan;
    }

    public String getGetDate() {
        if (GetDate != null) {
            return fDate.getString(GetDate);
        } else {
            return null;
        }
    }

    public void setGetDate(Date aGetDate) {
        GetDate = aGetDate;
    }

    public void setGetDate(String aGetDate) {
        if (aGetDate != null && !aGetDate.equals("")) {
            GetDate = fDate.getDate(aGetDate);
        } else {
            GetDate = null;
        }
    }

    public String getOperatorGet() {
        return OperatorGet;
    }

    public void setOperatorGet(String aOperatorGet) {
        OperatorGet = aOperatorGet;
    }

    public String getStartNo() {
        return StartNo;
    }

    public void setStartNo(String aStartNo) {
        StartNo = aStartNo;
    }

    public String getEndNo() {
        return EndNo;
    }

    public void setEndNo(String aEndNo) {
        EndNo = aEndNo;
    }

    public String getGetMakeDate() {
        if (GetMakeDate != null) {
            return fDate.getString(GetMakeDate);
        } else {
            return null;
        }
    }

    public void setGetMakeDate(Date aGetMakeDate) {
        GetMakeDate = aGetMakeDate;
    }

    public void setGetMakeDate(String aGetMakeDate) {
        if (aGetMakeDate != null && !aGetMakeDate.equals("")) {
            GetMakeDate = fDate.getDate(aGetMakeDate);
        } else {
            GetMakeDate = null;
        }
    }

    public int getSumCount() {
        return SumCount;
    }

    public void setSumCount(int aSumCount) {
        SumCount = aSumCount;
    }

    public void setSumCount(String aSumCount) {
        if (aSumCount != null && !aSumCount.equals("")) {
            Integer tInteger = new Integer(aSumCount);
            int i = tInteger.intValue();
            SumCount = i;
        }
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    public String getActiveDate() {
        if (ActiveDate != null) {
            return fDate.getString(ActiveDate);
        } else {
            return null;
        }
    }

    public void setActiveDate(Date aActiveDate) {
        ActiveDate = aActiveDate;
    }

    public void setActiveDate(String aActiveDate) {
        if (aActiveDate != null && !aActiveDate.equals("")) {
            ActiveDate = fDate.getDate(aActiveDate);
        } else {
            ActiveDate = null;
        }
    }

    public int getBackDate() {
        return BackDate;
    }

    public void setBackDate(int aBackDate) {
        BackDate = aBackDate;
    }

    public void setBackDate(String aBackDate) {
        if (aBackDate != null && !aBackDate.equals("")) {
            Integer tInteger = new Integer(aBackDate);
            int i = tInteger.intValue();
            BackDate = i;
        }
    }


    /**
     * 使用另外一个 LZCardPrintSchema 对象给 Schema 赋值
     * @param: aLZCardPrintSchema LZCardPrintSchema
     **/
    public void setSchema(LZCardPrintSchema aLZCardPrintSchema) {
        this.PrtNo = aLZCardPrintSchema.getPrtNo();
        this.CertifyCode = aLZCardPrintSchema.getCertifyCode();
        this.RiskCode = aLZCardPrintSchema.getRiskCode();
        this.RiskVersion = aLZCardPrintSchema.getRiskVersion();
        this.SubCode = aLZCardPrintSchema.getSubCode();
        this.MaxMoney = aLZCardPrintSchema.getMaxMoney();
        this.MaxDate = fDate.getDate(aLZCardPrintSchema.getMaxDate());
        this.ComCode = aLZCardPrintSchema.getComCode();
        this.Phone = aLZCardPrintSchema.getPhone();
        this.LinkMan = aLZCardPrintSchema.getLinkMan();
        this.CertifyPrice = aLZCardPrintSchema.getCertifyPrice();
        this.ManageCom = aLZCardPrintSchema.getManageCom();
        this.OperatorInput = aLZCardPrintSchema.getOperatorInput();
        this.InputDate = fDate.getDate(aLZCardPrintSchema.getInputDate());
        this.InputMakeDate = fDate.getDate(aLZCardPrintSchema.getInputMakeDate());
        this.GetMan = aLZCardPrintSchema.getGetMan();
        this.GetDate = fDate.getDate(aLZCardPrintSchema.getGetDate());
        this.OperatorGet = aLZCardPrintSchema.getOperatorGet();
        this.StartNo = aLZCardPrintSchema.getStartNo();
        this.EndNo = aLZCardPrintSchema.getEndNo();
        this.GetMakeDate = fDate.getDate(aLZCardPrintSchema.getGetMakeDate());
        this.SumCount = aLZCardPrintSchema.getSumCount();
        this.ModifyDate = fDate.getDate(aLZCardPrintSchema.getModifyDate());
        this.ModifyTime = aLZCardPrintSchema.getModifyTime();
        this.State = aLZCardPrintSchema.getState();
        this.ActiveDate = fDate.getDate(aLZCardPrintSchema.getActiveDate());
        this.BackDate = aLZCardPrintSchema.getBackDate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PrtNo") == null) {
                this.PrtNo = null;
            } else {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("CertifyCode") == null) {
                this.CertifyCode = null;
            } else {
                this.CertifyCode = rs.getString("CertifyCode").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null) {
                this.RiskVersion = null;
            } else {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("SubCode") == null) {
                this.SubCode = null;
            } else {
                this.SubCode = rs.getString("SubCode").trim();
            }

            this.MaxMoney = rs.getDouble("MaxMoney");
            this.MaxDate = rs.getDate("MaxDate");
            if (rs.getString("ComCode") == null) {
                this.ComCode = null;
            } else {
                this.ComCode = rs.getString("ComCode").trim();
            }

            if (rs.getString("Phone") == null) {
                this.Phone = null;
            } else {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("LinkMan") == null) {
                this.LinkMan = null;
            } else {
                this.LinkMan = rs.getString("LinkMan").trim();
            }

            this.CertifyPrice = rs.getDouble("CertifyPrice");
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("OperatorInput") == null) {
                this.OperatorInput = null;
            } else {
                this.OperatorInput = rs.getString("OperatorInput").trim();
            }

            this.InputDate = rs.getDate("InputDate");
            this.InputMakeDate = rs.getDate("InputMakeDate");
            if (rs.getString("GetMan") == null) {
                this.GetMan = null;
            } else {
                this.GetMan = rs.getString("GetMan").trim();
            }

            this.GetDate = rs.getDate("GetDate");
            if (rs.getString("OperatorGet") == null) {
                this.OperatorGet = null;
            } else {
                this.OperatorGet = rs.getString("OperatorGet").trim();
            }

            if (rs.getString("StartNo") == null) {
                this.StartNo = null;
            } else {
                this.StartNo = rs.getString("StartNo").trim();
            }

            if (rs.getString("EndNo") == null) {
                this.EndNo = null;
            } else {
                this.EndNo = rs.getString("EndNo").trim();
            }

            this.GetMakeDate = rs.getDate("GetMakeDate");
            this.SumCount = rs.getInt("SumCount");
            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

            this.ActiveDate = rs.getDate("ActiveDate");
            this.BackDate = rs.getInt("BackDate");
        } catch (SQLException sqle) {
            System.out.println("数据库中的LZCardPrint表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZCardPrintSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LZCardPrintSchema getSchema() {
        LZCardPrintSchema aLZCardPrintSchema = new LZCardPrintSchema();
        aLZCardPrintSchema.setSchema(this);
        return aLZCardPrintSchema;
    }

    public LZCardPrintDB getDB() {
        LZCardPrintDB aDBOper = new LZCardPrintDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardPrint描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CertifyCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskVersion));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SubCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MaxMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MaxDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(LinkMan));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CertifyPrice));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OperatorInput));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(InputDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(InputMakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetMan));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(GetDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OperatorGet));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StartNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EndNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(GetMakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ActiveDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BackDate));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZCardPrint>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            SubCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            MaxMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            MaxDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            LinkMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            CertifyPrice = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            OperatorInput = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                           SysConst.PACKAGESPILTER);
            InputDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            InputMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            GetMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                    SysConst.PACKAGESPILTER);
            GetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            OperatorGet = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                         SysConst.PACKAGESPILTER);
            StartNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                     SysConst.PACKAGESPILTER);
            EndNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                   SysConst.PACKAGESPILTER);
            GetMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            SumCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 22, SysConst.PACKAGESPILTER))).intValue();
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                        SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                   SysConst.PACKAGESPILTER);
            ActiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 26, SysConst.PACKAGESPILTER));
            BackDate = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 27, SysConst.PACKAGESPILTER))).intValue();
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZCardPrintSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("CertifyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equals("SubCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubCode));
        }
        if (FCode.equals("MaxMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxMoney));
        }
        if (FCode.equals("MaxDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMaxDate()));
        }
        if (FCode.equals("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equals("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equals("LinkMan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LinkMan));
        }
        if (FCode.equals("CertifyPrice")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyPrice));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("OperatorInput")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OperatorInput));
        }
        if (FCode.equals("InputDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getInputDate()));
        }
        if (FCode.equals("InputMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getInputMakeDate()));
        }
        if (FCode.equals("GetMan")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetMan));
        }
        if (FCode.equals("GetDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getGetDate()));
        }
        if (FCode.equals("OperatorGet")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OperatorGet));
        }
        if (FCode.equals("StartNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartNo));
        }
        if (FCode.equals("EndNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EndNo));
        }
        if (FCode.equals("GetMakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getGetMakeDate()));
        }
        if (FCode.equals("SumCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumCount));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("ActiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getActiveDate()));
        }
        if (FCode.equals("BackDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackDate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(PrtNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CertifyCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(RiskVersion);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(SubCode);
            break;
        case 5:
            strFieldValue = String.valueOf(MaxMoney);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getMaxDate()));
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ComCode);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(Phone);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(LinkMan);
            break;
        case 10:
            strFieldValue = String.valueOf(CertifyPrice);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(OperatorInput);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getInputDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getInputMakeDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(GetMan);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getGetDate()));
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(OperatorGet);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(StartNo);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(EndNo);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getGetMakeDate()));
            break;
        case 21:
            strFieldValue = String.valueOf(SumCount);
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        case 25:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getActiveDate()));
            break;
        case 26:
            strFieldValue = String.valueOf(BackDate);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("PrtNo")) {
            if (FValue != null && !FValue.equals("")) {
                PrtNo = FValue.trim();
            } else {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            if (FValue != null && !FValue.equals("")) {
                CertifyCode = FValue.trim();
            } else {
                CertifyCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if (FValue != null && !FValue.equals("")) {
                RiskVersion = FValue.trim();
            } else {
                RiskVersion = null;
            }
        }
        if (FCode.equalsIgnoreCase("SubCode")) {
            if (FValue != null && !FValue.equals("")) {
                SubCode = FValue.trim();
            } else {
                SubCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("MaxMoney")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MaxMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("MaxDate")) {
            if (FValue != null && !FValue.equals("")) {
                MaxDate = fDate.getDate(FValue);
            } else {
                MaxDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if (FValue != null && !FValue.equals("")) {
                ComCode = FValue.trim();
            } else {
                ComCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if (FValue != null && !FValue.equals("")) {
                Phone = FValue.trim();
            } else {
                Phone = null;
            }
        }
        if (FCode.equalsIgnoreCase("LinkMan")) {
            if (FValue != null && !FValue.equals("")) {
                LinkMan = FValue.trim();
            } else {
                LinkMan = null;
            }
        }
        if (FCode.equalsIgnoreCase("CertifyPrice")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CertifyPrice = d;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("OperatorInput")) {
            if (FValue != null && !FValue.equals("")) {
                OperatorInput = FValue.trim();
            } else {
                OperatorInput = null;
            }
        }
        if (FCode.equalsIgnoreCase("InputDate")) {
            if (FValue != null && !FValue.equals("")) {
                InputDate = fDate.getDate(FValue);
            } else {
                InputDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("InputMakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                InputMakeDate = fDate.getDate(FValue);
            } else {
                InputMakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetMan")) {
            if (FValue != null && !FValue.equals("")) {
                GetMan = FValue.trim();
            } else {
                GetMan = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetDate")) {
            if (FValue != null && !FValue.equals("")) {
                GetDate = fDate.getDate(FValue);
            } else {
                GetDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("OperatorGet")) {
            if (FValue != null && !FValue.equals("")) {
                OperatorGet = FValue.trim();
            } else {
                OperatorGet = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartNo")) {
            if (FValue != null && !FValue.equals("")) {
                StartNo = FValue.trim();
            } else {
                StartNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndNo")) {
            if (FValue != null && !FValue.equals("")) {
                EndNo = FValue.trim();
            } else {
                EndNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetMakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                GetMakeDate = fDate.getDate(FValue);
            } else {
                GetMakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("SumCount")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SumCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        if (FCode.equalsIgnoreCase("ActiveDate")) {
            if (FValue != null && !FValue.equals("")) {
                ActiveDate = fDate.getDate(FValue);
            } else {
                ActiveDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("BackDate")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                BackDate = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LZCardPrintSchema other = (LZCardPrintSchema) otherObject;
        return
                PrtNo.equals(other.getPrtNo())
                && CertifyCode.equals(other.getCertifyCode())
                && RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && SubCode.equals(other.getSubCode())
                && MaxMoney == other.getMaxMoney()
                && fDate.getString(MaxDate).equals(other.getMaxDate())
                && ComCode.equals(other.getComCode())
                && Phone.equals(other.getPhone())
                && LinkMan.equals(other.getLinkMan())
                && CertifyPrice == other.getCertifyPrice()
                && ManageCom.equals(other.getManageCom())
                && OperatorInput.equals(other.getOperatorInput())
                && fDate.getString(InputDate).equals(other.getInputDate())
                && fDate.getString(InputMakeDate).equals(other.getInputMakeDate())
                && GetMan.equals(other.getGetMan())
                && fDate.getString(GetDate).equals(other.getGetDate())
                && OperatorGet.equals(other.getOperatorGet())
                && StartNo.equals(other.getStartNo())
                && EndNo.equals(other.getEndNo())
                && fDate.getString(GetMakeDate).equals(other.getGetMakeDate())
                && SumCount == other.getSumCount()
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && State.equals(other.getState())
                && fDate.getString(ActiveDate).equals(other.getActiveDate())
                && BackDate == other.getBackDate();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("PrtNo")) {
            return 0;
        }
        if (strFieldName.equals("CertifyCode")) {
            return 1;
        }
        if (strFieldName.equals("RiskCode")) {
            return 2;
        }
        if (strFieldName.equals("RiskVersion")) {
            return 3;
        }
        if (strFieldName.equals("SubCode")) {
            return 4;
        }
        if (strFieldName.equals("MaxMoney")) {
            return 5;
        }
        if (strFieldName.equals("MaxDate")) {
            return 6;
        }
        if (strFieldName.equals("ComCode")) {
            return 7;
        }
        if (strFieldName.equals("Phone")) {
            return 8;
        }
        if (strFieldName.equals("LinkMan")) {
            return 9;
        }
        if (strFieldName.equals("CertifyPrice")) {
            return 10;
        }
        if (strFieldName.equals("ManageCom")) {
            return 11;
        }
        if (strFieldName.equals("OperatorInput")) {
            return 12;
        }
        if (strFieldName.equals("InputDate")) {
            return 13;
        }
        if (strFieldName.equals("InputMakeDate")) {
            return 14;
        }
        if (strFieldName.equals("GetMan")) {
            return 15;
        }
        if (strFieldName.equals("GetDate")) {
            return 16;
        }
        if (strFieldName.equals("OperatorGet")) {
            return 17;
        }
        if (strFieldName.equals("StartNo")) {
            return 18;
        }
        if (strFieldName.equals("EndNo")) {
            return 19;
        }
        if (strFieldName.equals("GetMakeDate")) {
            return 20;
        }
        if (strFieldName.equals("SumCount")) {
            return 21;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 22;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 23;
        }
        if (strFieldName.equals("State")) {
            return 24;
        }
        if (strFieldName.equals("ActiveDate")) {
            return 25;
        }
        if (strFieldName.equals("BackDate")) {
            return 26;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "PrtNo";
            break;
        case 1:
            strFieldName = "CertifyCode";
            break;
        case 2:
            strFieldName = "RiskCode";
            break;
        case 3:
            strFieldName = "RiskVersion";
            break;
        case 4:
            strFieldName = "SubCode";
            break;
        case 5:
            strFieldName = "MaxMoney";
            break;
        case 6:
            strFieldName = "MaxDate";
            break;
        case 7:
            strFieldName = "ComCode";
            break;
        case 8:
            strFieldName = "Phone";
            break;
        case 9:
            strFieldName = "LinkMan";
            break;
        case 10:
            strFieldName = "CertifyPrice";
            break;
        case 11:
            strFieldName = "ManageCom";
            break;
        case 12:
            strFieldName = "OperatorInput";
            break;
        case 13:
            strFieldName = "InputDate";
            break;
        case 14:
            strFieldName = "InputMakeDate";
            break;
        case 15:
            strFieldName = "GetMan";
            break;
        case 16:
            strFieldName = "GetDate";
            break;
        case 17:
            strFieldName = "OperatorGet";
            break;
        case 18:
            strFieldName = "StartNo";
            break;
        case 19:
            strFieldName = "EndNo";
            break;
        case 20:
            strFieldName = "GetMakeDate";
            break;
        case 21:
            strFieldName = "SumCount";
            break;
        case 22:
            strFieldName = "ModifyDate";
            break;
        case 23:
            strFieldName = "ModifyTime";
            break;
        case 24:
            strFieldName = "State";
            break;
        case 25:
            strFieldName = "ActiveDate";
            break;
        case 26:
            strFieldName = "BackDate";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("PrtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CertifyCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MaxMoney")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MaxDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ComCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LinkMan")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CertifyPrice")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OperatorInput")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InputDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InputMakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("GetMan")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("OperatorGet")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetMakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SumCount")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ActiveDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BackDate")) {
            return Schema.TYPE_INT;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 6:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 21:
            nFieldType = Schema.TYPE_INT;
            break;
        case 22:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 23:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 24:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 25:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 26:
            nFieldType = Schema.TYPE_INT;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
