/*
 * <p>ClassName: tab_holidaySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 联泰需要建schema表
 * @CreateDate：2007-08-21
 */
package com.sinosoft.lis.schema;

import java.sql.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.tab_holidayDB;

public class tab_holidaySchema implements Schema
{
	// @Field
	/** 分公司代码 */
	private String COMP_CODE;
	/** 假期类型 */
	private String EVENT_TYPE;
	/** 节假日 */
	private Date EVENT_DATE;
	/** 银行代码 */
	private String BANK_CODE;
	/** 描述 */
	private String DES;
	/** 状态 */
	private String STATUS;
	/** 备用字段 */
	private String bak1;
	/** 备用字段2 */
	private String bak2;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后修改日期 */
	private Date ModifyDate;
	/** 最后修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 12;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public tab_holidaySchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "EVENT_DATE";

		PK = pk;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	/** 分公司代码<P> */
	public String getCOMP_CODE()
	{
		if (COMP_CODE != null && !COMP_CODE.equals("") && SysConst.CHANGECHARSET == true)
		{
			COMP_CODE = StrTool.unicodeToGBK(COMP_CODE);
		}
		return COMP_CODE;
	}
	/** 分公司代码 */
	public void setCOMP_CODE(String aCOMP_CODE)
	{
		COMP_CODE = aCOMP_CODE;
	}
	/** 假期类型<P> */
	public String getEVENT_TYPE()
	{
		if (EVENT_TYPE != null && !EVENT_TYPE.equals("") && SysConst.CHANGECHARSET == true)
		{
			EVENT_TYPE = StrTool.unicodeToGBK(EVENT_TYPE);
		}
		return EVENT_TYPE;
	}
	/** 假期类型 */
	public void setEVENT_TYPE(String aEVENT_TYPE)
	{
		EVENT_TYPE = aEVENT_TYPE;
	}
	/** 节假日<P> */
	public String getEVENT_DATE()
	{
		if( EVENT_DATE != null )
			return fDate.getString(EVENT_DATE);
		else
			return null;
	}
	/** 节假日 */
	public void setEVENT_DATE(Date aEVENT_DATE)
	{
		EVENT_DATE = aEVENT_DATE;
	}
	/** 节假日<P> */
	public void setEVENT_DATE(String aEVENT_DATE)
	{
		if (aEVENT_DATE != null && !aEVENT_DATE.equals("") )
		{
			EVENT_DATE = fDate.getDate( aEVENT_DATE );
		}
		else
			EVENT_DATE = null;
	}

	/** 银行代码<P> */
	public String getBANK_CODE()
	{
		if (BANK_CODE != null && !BANK_CODE.equals("") && SysConst.CHANGECHARSET == true)
		{
			BANK_CODE = StrTool.unicodeToGBK(BANK_CODE);
		}
		return BANK_CODE;
	}
	/** 银行代码 */
	public void setBANK_CODE(String aBANK_CODE)
	{
		BANK_CODE = aBANK_CODE;
	}
	/** 描述<P> */
	public String getDES()
	{
		if (DES != null && !DES.equals("") && SysConst.CHANGECHARSET == true)
		{
			DES = StrTool.unicodeToGBK(DES);
		}
		return DES;
	}
	/** 描述 */
	public void setDES(String aDES)
	{
		DES = aDES;
	}
	/** 状态<P> */
	public String getSTATUS()
	{
		if (STATUS != null && !STATUS.equals("") && SysConst.CHANGECHARSET == true)
		{
			STATUS = StrTool.unicodeToGBK(STATUS);
		}
		return STATUS;
	}
	/** 状态 */
	public void setSTATUS(String aSTATUS)
	{
		STATUS = aSTATUS;
	}
	/** 备用字段<P> */
	public String getbak1()
	{
		if (bak1 != null && !bak1.equals("") && SysConst.CHANGECHARSET == true)
		{
			bak1 = StrTool.unicodeToGBK(bak1);
		}
		return bak1;
	}
	/** 备用字段 */
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	/** 备用字段2<P> */
	public String getbak2()
	{
		if (bak2 != null && !bak2.equals("") && SysConst.CHANGECHARSET == true)
		{
			bak2 = StrTool.unicodeToGBK(bak2);
		}
		return bak2;
	}
	/** 备用字段2 */
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	/** 入机日期<P> */
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	/** 入机日期 */
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	/** 入机日期<P> */
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	/** 入机时间<P> */
	public String getMakeTime()
	{
		if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
		{
			MakeTime = StrTool.unicodeToGBK(MakeTime);
		}
		return MakeTime;
	}
	/** 入机时间 */
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	/** 最后修改日期<P> */
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	/** 最后修改日期 */
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	/** 最后修改日期<P> */
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	/** 最后修改时间<P> */
	public String getModifyTime()
	{
		if (ModifyTime != null && !ModifyTime.equals("") && SysConst.CHANGECHARSET == true)
		{
			ModifyTime = StrTool.unicodeToGBK(ModifyTime);
		}
		return ModifyTime;
	}
	/** 最后修改时间 */
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 tab_holidaySchema 对象给 Schema 赋值
	* @param: Schema 对象
	* @return: 无
	**/
	public void setSchema(tab_holidaySchema atab_holidaySchema)
	{
		this.COMP_CODE = atab_holidaySchema.getCOMP_CODE();
		this.EVENT_TYPE = atab_holidaySchema.getEVENT_TYPE();
		this.EVENT_DATE = fDate.getDate( atab_holidaySchema.getEVENT_DATE());
		this.BANK_CODE = atab_holidaySchema.getBANK_CODE();
		this.DES = atab_holidaySchema.getDES();
		this.STATUS = atab_holidaySchema.getSTATUS();
		this.bak1 = atab_holidaySchema.getbak1();
		this.bak2 = atab_holidaySchema.getbak2();
		this.MakeDate = fDate.getDate( atab_holidaySchema.getMakeDate());
		this.MakeTime = atab_holidaySchema.getMakeTime();
		this.ModifyDate = fDate.getDate( atab_holidaySchema.getModifyDate());
		this.ModifyTime = atab_holidaySchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: ResultSet 对象; i 行数
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("COMP_CODE") == null )
				this.COMP_CODE = null;
			else
				this.COMP_CODE = rs.getString("COMP_CODE").trim();

			if( rs.getString("EVENT_TYPE") == null )
				this.EVENT_TYPE = null;
			else
				this.EVENT_TYPE = rs.getString("EVENT_TYPE").trim();

			this.EVENT_DATE = rs.getDate("EVENT_DATE");
			if( rs.getString("BANK_CODE") == null )
				this.BANK_CODE = null;
			else
				this.BANK_CODE = rs.getString("BANK_CODE").trim();

			if( rs.getString("DES") == null )
				this.DES = null;
			else
				this.DES = rs.getString("DES").trim();

			if( rs.getString("STATUS") == null )
				this.STATUS = null;
			else
				this.STATUS = rs.getString("STATUS").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "tab_holidaySchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	public tab_holidaySchema getSchema()
	{
		tab_holidaySchema atab_holidaySchema = new tab_holidaySchema();
		atab_holidaySchema.setSchema(this);
		return atab_holidaySchema;
	}

	public tab_holidayDB getDB()
	{
		tab_holidayDB aDBOper = new tab_holidayDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prptab_holiday描述/A>表字段
	* @param: 无
	* @return: 返回打包后字符串
	**/
	public String encode()
	{
		String strReturn = "";
		strReturn = StrTool.cTrim( StrTool.unicodeToGBK(COMP_CODE) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(EVENT_TYPE) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( EVENT_DATE )) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(BANK_CODE) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(DES) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(STATUS) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(bak1) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(bak2) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( MakeDate )) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(MakeTime) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( ModifyDate )) ) + SysConst.PACKAGESPILTER
					+ StrTool.cTrim( StrTool.unicodeToGBK(ModifyTime) );
		return strReturn;
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prptab_holiday>历史记账凭证主表信息</A>表字段
	* @param: strMessage：包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			COMP_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			EVENT_TYPE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			EVENT_DATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			BANK_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			DES = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			STATUS = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "tab_holidaySchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode 希望取得的字段名
	* @return: FValue String形式的字段值
	*			FValue = ""		没有匹配的字段
	*			FValue = "null"	字段值为null
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("COMP_CODE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(COMP_CODE));
		}
		if (FCode.equals("EVENT_TYPE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EVENT_TYPE));
		}
		if (FCode.equals("EVENT_DATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEVENT_DATE()));
		}
		if (FCode.equals("BANK_CODE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BANK_CODE));
		}
		if (FCode.equals("DES"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DES));
		}
		if (FCode.equals("STATUS"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(STATUS));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex 指定的字段索引值
	* @return: 字段值。
	*          如果没有对应的字段，返回""
	*          如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(COMP_CODE);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(EVENT_TYPE);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEVENT_DATE()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BANK_CODE);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(DES);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(STATUS);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode 希望取得的字段名
	* @return: FValue String形式的字段值
	*			FValue = ""		没有匹配的字段
	*			FValue = "null"	字段值为null
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equals("COMP_CODE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				COMP_CODE = FValue.trim();
			}
			else
				COMP_CODE = null;
		}
		if (FCode.equals("EVENT_TYPE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EVENT_TYPE = FValue.trim();
			}
			else
				EVENT_TYPE = null;
		}
		if (FCode.equals("EVENT_DATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EVENT_DATE = fDate.getDate( FValue );
			}
			else
				EVENT_DATE = null;
		}
		if (FCode.equals("BANK_CODE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BANK_CODE = FValue.trim();
			}
			else
				BANK_CODE = null;
		}
		if (FCode.equals("DES"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DES = FValue.trim();
			}
			else
				DES = null;
		}
		if (FCode.equals("STATUS"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				STATUS = FValue.trim();
			}
			else
				STATUS = null;
		}
		if (FCode.equals("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equals("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equals("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equals("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equals("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equals("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		tab_holidaySchema other = (tab_holidaySchema)otherObject;
		return
			COMP_CODE.equals(other.getCOMP_CODE())
			&& EVENT_TYPE.equals(other.getEVENT_TYPE())
			&& fDate.getString(EVENT_DATE).equals(other.getEVENT_DATE())
			&& BANK_CODE.equals(other.getBANK_CODE())
			&& DES.equals(other.getDES())
			&& STATUS.equals(other.getSTATUS())
			&& bak1.equals(other.getbak1())
			&& bak2.equals(other.getbak2())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("COMP_CODE") ) {
			return 0;
		}
		if( strFieldName.equals("EVENT_TYPE") ) {
			return 1;
		}
		if( strFieldName.equals("EVENT_DATE") ) {
			return 2;
		}
		if( strFieldName.equals("BANK_CODE") ) {
			return 3;
		}
		if( strFieldName.equals("DES") ) {
			return 4;
		}
		if( strFieldName.equals("STATUS") ) {
			return 5;
		}
		if( strFieldName.equals("bak1") ) {
			return 6;
		}
		if( strFieldName.equals("bak2") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "COMP_CODE";
				break;
			case 1:
				strFieldName = "EVENT_TYPE";
				break;
			case 2:
				strFieldName = "EVENT_DATE";
				break;
			case 3:
				strFieldName = "BANK_CODE";
				break;
			case 4:
				strFieldName = "DES";
				break;
			case 5:
				strFieldName = "STATUS";
				break;
			case 6:
				strFieldName = "bak1";
				break;
			case 7:
				strFieldName = "bak2";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("COMP_CODE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EVENT_TYPE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EVENT_DATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BANK_CODE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DES") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("STATUS") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
