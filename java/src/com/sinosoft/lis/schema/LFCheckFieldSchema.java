/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LFCheckFieldDB;

/*
 * <p>ClassName: LFCheckFieldSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-01-08
 */
public class LFCheckFieldSchema implements Schema, Cloneable {
    // @Field
    /** 规则流水号 */
    private String SerialNO;
    /** 校验计算公式 */
    private String CalFormula;
    /** 校验类型 */
    private String CalType;
    /** 算法说明 */
    private String Remark;
    /** 是否校验 */
    private String isNeedChk;
    /** 新老科目标记 */
    private String ItemFlag;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFCheckFieldSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNO";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LFCheckFieldSchema cloned = (LFCheckFieldSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNO() {
        return SerialNO;
    }

    public void setSerialNO(String aSerialNO) {
        SerialNO = aSerialNO;
    }

    public String getCalFormula() {
        return CalFormula;
    }

    public void setCalFormula(String aCalFormula) {
        CalFormula = aCalFormula;
    }

    public String getCalType() {
        return CalType;
    }

    public void setCalType(String aCalType) {
        CalType = aCalType;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    public String getisNeedChk() {
        return isNeedChk;
    }

    public void setisNeedChk(String aisNeedChk) {
        isNeedChk = aisNeedChk;
    }

    public String getItemFlag() {
        return ItemFlag;
    }

    public void setItemFlag(String aItemFlag) {
        ItemFlag = aItemFlag;
    }

    /**
     * 使用另外一个 LFCheckFieldSchema 对象给 Schema 赋值
     * @param: aLFCheckFieldSchema LFCheckFieldSchema
     **/
    public void setSchema(LFCheckFieldSchema aLFCheckFieldSchema) {
        this.SerialNO = aLFCheckFieldSchema.getSerialNO();
        this.CalFormula = aLFCheckFieldSchema.getCalFormula();
        this.CalType = aLFCheckFieldSchema.getCalType();
        this.Remark = aLFCheckFieldSchema.getRemark();
        this.isNeedChk = aLFCheckFieldSchema.getisNeedChk();
        this.ItemFlag = aLFCheckFieldSchema.getItemFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNO") == null) {
                this.SerialNO = null;
            } else {
                this.SerialNO = rs.getString("SerialNO").trim();
            }

            if (rs.getString("CalFormula") == null) {
                this.CalFormula = null;
            } else {
                this.CalFormula = rs.getString("CalFormula").trim();
            }

            if (rs.getString("CalType") == null) {
                this.CalType = null;
            } else {
                this.CalType = rs.getString("CalType").trim();
            }

            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("isNeedChk") == null) {
                this.isNeedChk = null;
            } else {
                this.isNeedChk = rs.getString("isNeedChk").trim();
            }

            if (rs.getString("ItemFlag") == null) {
                this.ItemFlag = null;
            } else {
                this.ItemFlag = rs.getString("ItemFlag").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LFCheckField表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFCheckFieldSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LFCheckFieldSchema getSchema() {
        LFCheckFieldSchema aLFCheckFieldSchema = new LFCheckFieldSchema();
        aLFCheckFieldSchema.setSchema(this);
        return aLFCheckFieldSchema;
    }

    public LFCheckFieldDB getDB() {
        LFCheckFieldDB aDBOper = new LFCheckFieldDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFCheckField描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNO));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalFormula));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CalType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(isNeedChk));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ItemFlag));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFCheckField>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SerialNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            CalFormula = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            CalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            isNeedChk = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            ItemFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFCheckFieldSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SerialNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNO));
        }
        if (FCode.equals("CalFormula")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalFormula));
        }
        if (FCode.equals("CalType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CalType));
        }
        if (FCode.equals("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("isNeedChk")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(isNeedChk));
        }
        if (FCode.equals("ItemFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ItemFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SerialNO);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CalFormula);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(CalType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(isNeedChk);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ItemFlag);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNO")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNO = FValue.trim();
            } else {
                SerialNO = null;
            }
        }
        if (FCode.equalsIgnoreCase("CalFormula")) {
            if (FValue != null && !FValue.equals("")) {
                CalFormula = FValue.trim();
            } else {
                CalFormula = null;
            }
        }
        if (FCode.equalsIgnoreCase("CalType")) {
            if (FValue != null && !FValue.equals("")) {
                CalType = FValue.trim();
            } else {
                CalType = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("isNeedChk")) {
            if (FValue != null && !FValue.equals("")) {
                isNeedChk = FValue.trim();
            } else {
                isNeedChk = null;
            }
        }
        if (FCode.equalsIgnoreCase("ItemFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ItemFlag = FValue.trim();
            } else {
                ItemFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LFCheckFieldSchema other = (LFCheckFieldSchema) otherObject;
        return
                SerialNO.equals(other.getSerialNO())
                && CalFormula.equals(other.getCalFormula())
                && CalType.equals(other.getCalType())
                && Remark.equals(other.getRemark())
                && isNeedChk.equals(other.getisNeedChk())
                && ItemFlag.equals(other.getItemFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SerialNO")) {
            return 0;
        }
        if (strFieldName.equals("CalFormula")) {
            return 1;
        }
        if (strFieldName.equals("CalType")) {
            return 2;
        }
        if (strFieldName.equals("Remark")) {
            return 3;
        }
        if (strFieldName.equals("isNeedChk")) {
            return 4;
        }
        if (strFieldName.equals("ItemFlag")) {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SerialNO";
            break;
        case 1:
            strFieldName = "CalFormula";
            break;
        case 2:
            strFieldName = "CalType";
            break;
        case 3:
            strFieldName = "Remark";
            break;
        case 4:
            strFieldName = "isNeedChk";
            break;
        case 5:
            strFieldName = "ItemFlag";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SerialNO")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFormula")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("isNeedChk")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemFlag")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
