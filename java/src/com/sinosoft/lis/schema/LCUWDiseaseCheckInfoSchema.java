/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCUWDiseaseCheckInfoDB;

/*
 * <p>ClassName: LCUWDiseaseCheckInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-03-21
 */
public class LCUWDiseaseCheckInfoSchema implements Schema, Cloneable {
    // @Field
    /** 险种号码 */
    private String ProposalNo;
    /** 风险代码 */
    private String DieaseCode;
    /** 评点类型 */
    private String CheckType;
    /** 评点值 */
    private int CheckValue;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String Managecom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCUWDiseaseCheckInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "ProposalNo";
        pk[1] = "DieaseCode";
        pk[2] = "CheckType";
        pk[3] = "CheckValue";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCUWDiseaseCheckInfoSchema cloned = (LCUWDiseaseCheckInfoSchema)super.
                                            clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getProposalNo() {
        return ProposalNo;
    }

    public void setProposalNo(String aProposalNo) {
        ProposalNo = aProposalNo;
    }

    public String getDieaseCode() {
        return DieaseCode;
    }

    public void setDieaseCode(String aDieaseCode) {
        DieaseCode = aDieaseCode;
    }

    public String getCheckType() {
        return CheckType;
    }

    public void setCheckType(String aCheckType) {
        CheckType = aCheckType;
    }

    public int getCheckValue() {
        return CheckValue;
    }

    public void setCheckValue(int aCheckValue) {
        CheckValue = aCheckValue;
    }

    public void setCheckValue(String aCheckValue) {
        if (aCheckValue != null && !aCheckValue.equals("")) {
            Integer tInteger = new Integer(aCheckValue);
            int i = tInteger.intValue();
            CheckValue = i;
        }
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getManagecom() {
        return Managecom;
    }

    public void setManagecom(String aManagecom) {
        Managecom = aManagecom;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCUWDiseaseCheckInfoSchema 对象给 Schema 赋值
     * @param: aLCUWDiseaseCheckInfoSchema LCUWDiseaseCheckInfoSchema
     **/
    public void setSchema(LCUWDiseaseCheckInfoSchema
                          aLCUWDiseaseCheckInfoSchema) {
        this.ProposalNo = aLCUWDiseaseCheckInfoSchema.getProposalNo();
        this.DieaseCode = aLCUWDiseaseCheckInfoSchema.getDieaseCode();
        this.CheckType = aLCUWDiseaseCheckInfoSchema.getCheckType();
        this.CheckValue = aLCUWDiseaseCheckInfoSchema.getCheckValue();
        this.Operator = aLCUWDiseaseCheckInfoSchema.getOperator();
        this.Managecom = aLCUWDiseaseCheckInfoSchema.getManagecom();
        this.MakeDate = fDate.getDate(aLCUWDiseaseCheckInfoSchema.getMakeDate());
        this.MakeTime = aLCUWDiseaseCheckInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCUWDiseaseCheckInfoSchema.
                                        getModifyDate());
        this.ModifyTime = aLCUWDiseaseCheckInfoSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ProposalNo") == null) {
                this.ProposalNo = null;
            } else {
                this.ProposalNo = rs.getString("ProposalNo").trim();
            }

            if (rs.getString("DieaseCode") == null) {
                this.DieaseCode = null;
            } else {
                this.DieaseCode = rs.getString("DieaseCode").trim();
            }

            if (rs.getString("CheckType") == null) {
                this.CheckType = null;
            } else {
                this.CheckType = rs.getString("CheckType").trim();
            }

            this.CheckValue = rs.getInt("CheckValue");
            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("Managecom") == null) {
                this.Managecom = null;
            } else {
                this.Managecom = rs.getString("Managecom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LCUWDiseaseCheckInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCUWDiseaseCheckInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LCUWDiseaseCheckInfoSchema getSchema() {
        LCUWDiseaseCheckInfoSchema aLCUWDiseaseCheckInfoSchema = new
                LCUWDiseaseCheckInfoSchema();
        aLCUWDiseaseCheckInfoSchema.setSchema(this);
        return aLCUWDiseaseCheckInfoSchema;
    }

    public LCUWDiseaseCheckInfoDB getDB() {
        LCUWDiseaseCheckInfoDB aDBOper = new LCUWDiseaseCheckInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCUWDiseaseCheckInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ProposalNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DieaseCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CheckType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CheckValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Managecom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCUWDiseaseCheckInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            DieaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            CheckType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            CheckValue = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            Managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCUWDiseaseCheckInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
        }
        if (FCode.equals("DieaseCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DieaseCode));
        }
        if (FCode.equals("CheckType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckType));
        }
        if (FCode.equals("CheckValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CheckValue));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("Managecom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ProposalNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(DieaseCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(CheckType);
            break;
        case 3:
            strFieldValue = String.valueOf(CheckValue);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Managecom);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ProposalNo")) {
            if (FValue != null && !FValue.equals("")) {
                ProposalNo = FValue.trim();
            } else {
                ProposalNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("DieaseCode")) {
            if (FValue != null && !FValue.equals("")) {
                DieaseCode = FValue.trim();
            } else {
                DieaseCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("CheckType")) {
            if (FValue != null && !FValue.equals("")) {
                CheckType = FValue.trim();
            } else {
                CheckType = null;
            }
        }
        if (FCode.equalsIgnoreCase("CheckValue")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CheckValue = i;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("Managecom")) {
            if (FValue != null && !FValue.equals("")) {
                Managecom = FValue.trim();
            } else {
                Managecom = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LCUWDiseaseCheckInfoSchema other = (LCUWDiseaseCheckInfoSchema)
                                           otherObject;
        return
                ProposalNo.equals(other.getProposalNo())
                && DieaseCode.equals(other.getDieaseCode())
                && CheckType.equals(other.getCheckType())
                && CheckValue == other.getCheckValue()
                && Operator.equals(other.getOperator())
                && Managecom.equals(other.getManagecom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ProposalNo")) {
            return 0;
        }
        if (strFieldName.equals("DieaseCode")) {
            return 1;
        }
        if (strFieldName.equals("CheckType")) {
            return 2;
        }
        if (strFieldName.equals("CheckValue")) {
            return 3;
        }
        if (strFieldName.equals("Operator")) {
            return 4;
        }
        if (strFieldName.equals("Managecom")) {
            return 5;
        }
        if (strFieldName.equals("MakeDate")) {
            return 6;
        }
        if (strFieldName.equals("MakeTime")) {
            return 7;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 8;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ProposalNo";
            break;
        case 1:
            strFieldName = "DieaseCode";
            break;
        case 2:
            strFieldName = "CheckType";
            break;
        case 3:
            strFieldName = "CheckValue";
            break;
        case 4:
            strFieldName = "Operator";
            break;
        case 5:
            strFieldName = "Managecom";
            break;
        case 6:
            strFieldName = "MakeDate";
            break;
        case 7:
            strFieldName = "MakeTime";
            break;
        case 8:
            strFieldName = "ModifyDate";
            break;
        case 9:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ProposalNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DieaseCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CheckType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CheckValue")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Managecom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_INT;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
