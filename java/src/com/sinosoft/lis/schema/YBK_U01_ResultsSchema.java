/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.YBK_U01_ResultsDB;

/*
 * <p>ClassName: YBK_U01_ResultsSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2016-12-14
 */
public class YBK_U01_ResultsSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String DataBatchNo;
	/** 流水号 */
	private String DataSerialNo;
	/** 印刷号 */
	private String PrtNo;
	/** 请求类型 */
	private String RequestType;
	/** 任务编号 */
	private String TaskNo;
	/** 返回类型代码 */
	private String ResponseCode;
	/** 错误描述 */
	private String ErrorMessage;
	/** 核保确认编码 */
	private String UnderwritSequenceNo;
	/** 客户编码 */
	private String CutomerSequenceNo;
	/** 区间起期 */
	private Date StartTime;
	/** 区间止期 */
	private Date EndTime;
	/** 累计寿险风险保额 */
	private double AllLifeSumInsured;
	/** 累计意外险风险保额 */
	private double AllAccidentSumInsured;
	/** 累计健康险风险保额 */
	private double AllHealthSumInsured;
	/** 累计养老险风险保额 */
	private double AllOldSumInsured;
	/** 累计其他人身保险风险保额 */
	private double AllOtherSumInsured;
	/** 累计住院日额 */
	private double AllHospitalAllowance;
	/** 累计赔款 */
	private double AllClaimAmount;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 23;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public YBK_U01_ResultsSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "DataBatchNo";
		pk[1] = "DataSerialNo";
		pk[2] = "PrtNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		YBK_U01_ResultsSchema cloned = (YBK_U01_ResultsSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getDataBatchNo()
	{
		return DataBatchNo;
	}
	public void setDataBatchNo(String aDataBatchNo)
	{
		DataBatchNo = aDataBatchNo;
	}
	public String getDataSerialNo()
	{
		return DataSerialNo;
	}
	public void setDataSerialNo(String aDataSerialNo)
	{
		DataSerialNo = aDataSerialNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getRequestType()
	{
		return RequestType;
	}
	public void setRequestType(String aRequestType)
	{
		RequestType = aRequestType;
	}
	public String getTaskNo()
	{
		return TaskNo;
	}
	public void setTaskNo(String aTaskNo)
	{
		TaskNo = aTaskNo;
	}
	public String getResponseCode()
	{
		return ResponseCode;
	}
	public void setResponseCode(String aResponseCode)
	{
		ResponseCode = aResponseCode;
	}
	public String getErrorMessage()
	{
		return ErrorMessage;
	}
	public void setErrorMessage(String aErrorMessage)
	{
		ErrorMessage = aErrorMessage;
	}
	public String getUnderwritSequenceNo()
	{
		return UnderwritSequenceNo;
	}
	public void setUnderwritSequenceNo(String aUnderwritSequenceNo)
	{
		UnderwritSequenceNo = aUnderwritSequenceNo;
	}
	public String getCutomerSequenceNo()
	{
		return CutomerSequenceNo;
	}
	public void setCutomerSequenceNo(String aCutomerSequenceNo)
	{
		CutomerSequenceNo = aCutomerSequenceNo;
	}
	public String getStartTime()
	{
		if( StartTime != null )
			return fDate.getString(StartTime);
		else
			return null;
	}
	public void setStartTime(Date aStartTime)
	{
		StartTime = aStartTime;
	}
	public void setStartTime(String aStartTime)
	{
		if (aStartTime != null && !aStartTime.equals("") )
		{
			StartTime = fDate.getDate( aStartTime );
		}
		else
			StartTime = null;
	}

	public String getEndTime()
	{
		if( EndTime != null )
			return fDate.getString(EndTime);
		else
			return null;
	}
	public void setEndTime(Date aEndTime)
	{
		EndTime = aEndTime;
	}
	public void setEndTime(String aEndTime)
	{
		if (aEndTime != null && !aEndTime.equals("") )
		{
			EndTime = fDate.getDate( aEndTime );
		}
		else
			EndTime = null;
	}

	public double getAllLifeSumInsured()
	{
		return AllLifeSumInsured;
	}
	public void setAllLifeSumInsured(double aAllLifeSumInsured)
	{
		AllLifeSumInsured = Arith.round(aAllLifeSumInsured,2);
	}
	public void setAllLifeSumInsured(String aAllLifeSumInsured)
	{
		if (aAllLifeSumInsured != null && !aAllLifeSumInsured.equals(""))
		{
			Double tDouble = new Double(aAllLifeSumInsured);
			double d = tDouble.doubleValue();
                AllLifeSumInsured = Arith.round(d,2);
		}
	}

	public double getAllAccidentSumInsured()
	{
		return AllAccidentSumInsured;
	}
	public void setAllAccidentSumInsured(double aAllAccidentSumInsured)
	{
		AllAccidentSumInsured = Arith.round(aAllAccidentSumInsured,2);
	}
	public void setAllAccidentSumInsured(String aAllAccidentSumInsured)
	{
		if (aAllAccidentSumInsured != null && !aAllAccidentSumInsured.equals(""))
		{
			Double tDouble = new Double(aAllAccidentSumInsured);
			double d = tDouble.doubleValue();
                AllAccidentSumInsured = Arith.round(d,2);
		}
	}

	public double getAllHealthSumInsured()
	{
		return AllHealthSumInsured;
	}
	public void setAllHealthSumInsured(double aAllHealthSumInsured)
	{
		AllHealthSumInsured = Arith.round(aAllHealthSumInsured,2);
	}
	public void setAllHealthSumInsured(String aAllHealthSumInsured)
	{
		if (aAllHealthSumInsured != null && !aAllHealthSumInsured.equals(""))
		{
			Double tDouble = new Double(aAllHealthSumInsured);
			double d = tDouble.doubleValue();
                AllHealthSumInsured = Arith.round(d,2);
		}
	}

	public double getAllOldSumInsured()
	{
		return AllOldSumInsured;
	}
	public void setAllOldSumInsured(double aAllOldSumInsured)
	{
		AllOldSumInsured = Arith.round(aAllOldSumInsured,2);
	}
	public void setAllOldSumInsured(String aAllOldSumInsured)
	{
		if (aAllOldSumInsured != null && !aAllOldSumInsured.equals(""))
		{
			Double tDouble = new Double(aAllOldSumInsured);
			double d = tDouble.doubleValue();
                AllOldSumInsured = Arith.round(d,2);
		}
	}

	public double getAllOtherSumInsured()
	{
		return AllOtherSumInsured;
	}
	public void setAllOtherSumInsured(double aAllOtherSumInsured)
	{
		AllOtherSumInsured = Arith.round(aAllOtherSumInsured,2);
	}
	public void setAllOtherSumInsured(String aAllOtherSumInsured)
	{
		if (aAllOtherSumInsured != null && !aAllOtherSumInsured.equals(""))
		{
			Double tDouble = new Double(aAllOtherSumInsured);
			double d = tDouble.doubleValue();
                AllOtherSumInsured = Arith.round(d,2);
		}
	}

	public double getAllHospitalAllowance()
	{
		return AllHospitalAllowance;
	}
	public void setAllHospitalAllowance(double aAllHospitalAllowance)
	{
		AllHospitalAllowance = Arith.round(aAllHospitalAllowance,2);
	}
	public void setAllHospitalAllowance(String aAllHospitalAllowance)
	{
		if (aAllHospitalAllowance != null && !aAllHospitalAllowance.equals(""))
		{
			Double tDouble = new Double(aAllHospitalAllowance);
			double d = tDouble.doubleValue();
                AllHospitalAllowance = Arith.round(d,2);
		}
	}

	public double getAllClaimAmount()
	{
		return AllClaimAmount;
	}
	public void setAllClaimAmount(double aAllClaimAmount)
	{
		AllClaimAmount = Arith.round(aAllClaimAmount,2);
	}
	public void setAllClaimAmount(String aAllClaimAmount)
	{
		if (aAllClaimAmount != null && !aAllClaimAmount.equals(""))
		{
			Double tDouble = new Double(aAllClaimAmount);
			double d = tDouble.doubleValue();
                AllClaimAmount = Arith.round(d,2);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 YBK_U01_ResultsSchema 对象给 Schema 赋值
	* @param: aYBK_U01_ResultsSchema YBK_U01_ResultsSchema
	**/
	public void setSchema(YBK_U01_ResultsSchema aYBK_U01_ResultsSchema)
	{
		this.DataBatchNo = aYBK_U01_ResultsSchema.getDataBatchNo();
		this.DataSerialNo = aYBK_U01_ResultsSchema.getDataSerialNo();
		this.PrtNo = aYBK_U01_ResultsSchema.getPrtNo();
		this.RequestType = aYBK_U01_ResultsSchema.getRequestType();
		this.TaskNo = aYBK_U01_ResultsSchema.getTaskNo();
		this.ResponseCode = aYBK_U01_ResultsSchema.getResponseCode();
		this.ErrorMessage = aYBK_U01_ResultsSchema.getErrorMessage();
		this.UnderwritSequenceNo = aYBK_U01_ResultsSchema.getUnderwritSequenceNo();
		this.CutomerSequenceNo = aYBK_U01_ResultsSchema.getCutomerSequenceNo();
		this.StartTime = fDate.getDate( aYBK_U01_ResultsSchema.getStartTime());
		this.EndTime = fDate.getDate( aYBK_U01_ResultsSchema.getEndTime());
		this.AllLifeSumInsured = aYBK_U01_ResultsSchema.getAllLifeSumInsured();
		this.AllAccidentSumInsured = aYBK_U01_ResultsSchema.getAllAccidentSumInsured();
		this.AllHealthSumInsured = aYBK_U01_ResultsSchema.getAllHealthSumInsured();
		this.AllOldSumInsured = aYBK_U01_ResultsSchema.getAllOldSumInsured();
		this.AllOtherSumInsured = aYBK_U01_ResultsSchema.getAllOtherSumInsured();
		this.AllHospitalAllowance = aYBK_U01_ResultsSchema.getAllHospitalAllowance();
		this.AllClaimAmount = aYBK_U01_ResultsSchema.getAllClaimAmount();
		this.Operator = aYBK_U01_ResultsSchema.getOperator();
		this.MakeDate = fDate.getDate( aYBK_U01_ResultsSchema.getMakeDate());
		this.MakeTime = aYBK_U01_ResultsSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aYBK_U01_ResultsSchema.getModifyDate());
		this.ModifyTime = aYBK_U01_ResultsSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("DataBatchNo") == null )
				this.DataBatchNo = null;
			else
				this.DataBatchNo = rs.getString("DataBatchNo").trim();

			if( rs.getString("DataSerialNo") == null )
				this.DataSerialNo = null;
			else
				this.DataSerialNo = rs.getString("DataSerialNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("RequestType") == null )
				this.RequestType = null;
			else
				this.RequestType = rs.getString("RequestType").trim();

			if( rs.getString("TaskNo") == null )
				this.TaskNo = null;
			else
				this.TaskNo = rs.getString("TaskNo").trim();

			if( rs.getString("ResponseCode") == null )
				this.ResponseCode = null;
			else
				this.ResponseCode = rs.getString("ResponseCode").trim();

			if( rs.getString("ErrorMessage") == null )
				this.ErrorMessage = null;
			else
				this.ErrorMessage = rs.getString("ErrorMessage").trim();

			if( rs.getString("UnderwritSequenceNo") == null )
				this.UnderwritSequenceNo = null;
			else
				this.UnderwritSequenceNo = rs.getString("UnderwritSequenceNo").trim();

			if( rs.getString("CutomerSequenceNo") == null )
				this.CutomerSequenceNo = null;
			else
				this.CutomerSequenceNo = rs.getString("CutomerSequenceNo").trim();

			this.StartTime = rs.getDate("StartTime");
			this.EndTime = rs.getDate("EndTime");
			this.AllLifeSumInsured = rs.getDouble("AllLifeSumInsured");
			this.AllAccidentSumInsured = rs.getDouble("AllAccidentSumInsured");
			this.AllHealthSumInsured = rs.getDouble("AllHealthSumInsured");
			this.AllOldSumInsured = rs.getDouble("AllOldSumInsured");
			this.AllOtherSumInsured = rs.getDouble("AllOtherSumInsured");
			this.AllHospitalAllowance = rs.getDouble("AllHospitalAllowance");
			this.AllClaimAmount = rs.getDouble("AllClaimAmount");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的YBK_U01_Results表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YBK_U01_ResultsSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public YBK_U01_ResultsSchema getSchema()
	{
		YBK_U01_ResultsSchema aYBK_U01_ResultsSchema = new YBK_U01_ResultsSchema();
		aYBK_U01_ResultsSchema.setSchema(this);
		return aYBK_U01_ResultsSchema;
	}

	public YBK_U01_ResultsDB getDB()
	{
		YBK_U01_ResultsDB aDBOper = new YBK_U01_ResultsDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYBK_U01_Results描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(DataBatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RequestType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaskNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponseCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrorMessage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnderwritSequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CutomerSequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartTime ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndTime ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AllLifeSumInsured));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AllAccidentSumInsured));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AllHealthSumInsured));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AllOldSumInsured));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AllOtherSumInsured));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AllHospitalAllowance));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AllClaimAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpYBK_U01_Results>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			DataBatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			DataSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RequestType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			TaskNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ResponseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ErrorMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			UnderwritSequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CutomerSequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			StartTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			EndTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			AllLifeSumInsured = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			AllAccidentSumInsured = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			AllHealthSumInsured = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			AllOldSumInsured = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			AllOtherSumInsured = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			AllHospitalAllowance = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			AllClaimAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "YBK_U01_ResultsSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("DataBatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataBatchNo));
		}
		if (FCode.equals("DataSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataSerialNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("RequestType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RequestType));
		}
		if (FCode.equals("TaskNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaskNo));
		}
		if (FCode.equals("ResponseCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponseCode));
		}
		if (FCode.equals("ErrorMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorMessage));
		}
		if (FCode.equals("UnderwritSequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnderwritSequenceNo));
		}
		if (FCode.equals("CutomerSequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CutomerSequenceNo));
		}
		if (FCode.equals("StartTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartTime()));
		}
		if (FCode.equals("EndTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndTime()));
		}
		if (FCode.equals("AllLifeSumInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AllLifeSumInsured));
		}
		if (FCode.equals("AllAccidentSumInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AllAccidentSumInsured));
		}
		if (FCode.equals("AllHealthSumInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AllHealthSumInsured));
		}
		if (FCode.equals("AllOldSumInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AllOldSumInsured));
		}
		if (FCode.equals("AllOtherSumInsured"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AllOtherSumInsured));
		}
		if (FCode.equals("AllHospitalAllowance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AllHospitalAllowance));
		}
		if (FCode.equals("AllClaimAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AllClaimAmount));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(DataBatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(DataSerialNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RequestType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(TaskNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ResponseCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ErrorMessage);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(UnderwritSequenceNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CutomerSequenceNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartTime()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndTime()));
				break;
			case 11:
				strFieldValue = String.valueOf(AllLifeSumInsured);
				break;
			case 12:
				strFieldValue = String.valueOf(AllAccidentSumInsured);
				break;
			case 13:
				strFieldValue = String.valueOf(AllHealthSumInsured);
				break;
			case 14:
				strFieldValue = String.valueOf(AllOldSumInsured);
				break;
			case 15:
				strFieldValue = String.valueOf(AllOtherSumInsured);
				break;
			case 16:
				strFieldValue = String.valueOf(AllHospitalAllowance);
				break;
			case 17:
				strFieldValue = String.valueOf(AllClaimAmount);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("DataBatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataBatchNo = FValue.trim();
			}
			else
				DataBatchNo = null;
		}
		if (FCode.equalsIgnoreCase("DataSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataSerialNo = FValue.trim();
			}
			else
				DataSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("RequestType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RequestType = FValue.trim();
			}
			else
				RequestType = null;
		}
		if (FCode.equalsIgnoreCase("TaskNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaskNo = FValue.trim();
			}
			else
				TaskNo = null;
		}
		if (FCode.equalsIgnoreCase("ResponseCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponseCode = FValue.trim();
			}
			else
				ResponseCode = null;
		}
		if (FCode.equalsIgnoreCase("ErrorMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorMessage = FValue.trim();
			}
			else
				ErrorMessage = null;
		}
		if (FCode.equalsIgnoreCase("UnderwritSequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnderwritSequenceNo = FValue.trim();
			}
			else
				UnderwritSequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("CutomerSequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CutomerSequenceNo = FValue.trim();
			}
			else
				CutomerSequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("StartTime"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartTime = fDate.getDate( FValue );
			}
			else
				StartTime = null;
		}
		if (FCode.equalsIgnoreCase("EndTime"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndTime = fDate.getDate( FValue );
			}
			else
				EndTime = null;
		}
		if (FCode.equalsIgnoreCase("AllLifeSumInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AllLifeSumInsured = d;
			}
		}
		if (FCode.equalsIgnoreCase("AllAccidentSumInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AllAccidentSumInsured = d;
			}
		}
		if (FCode.equalsIgnoreCase("AllHealthSumInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AllHealthSumInsured = d;
			}
		}
		if (FCode.equalsIgnoreCase("AllOldSumInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AllOldSumInsured = d;
			}
		}
		if (FCode.equalsIgnoreCase("AllOtherSumInsured"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AllOtherSumInsured = d;
			}
		}
		if (FCode.equalsIgnoreCase("AllHospitalAllowance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AllHospitalAllowance = d;
			}
		}
		if (FCode.equalsIgnoreCase("AllClaimAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AllClaimAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		YBK_U01_ResultsSchema other = (YBK_U01_ResultsSchema)otherObject;
		return
			(DataBatchNo == null ? other.getDataBatchNo() == null : DataBatchNo.equals(other.getDataBatchNo()))
			&& (DataSerialNo == null ? other.getDataSerialNo() == null : DataSerialNo.equals(other.getDataSerialNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (RequestType == null ? other.getRequestType() == null : RequestType.equals(other.getRequestType()))
			&& (TaskNo == null ? other.getTaskNo() == null : TaskNo.equals(other.getTaskNo()))
			&& (ResponseCode == null ? other.getResponseCode() == null : ResponseCode.equals(other.getResponseCode()))
			&& (ErrorMessage == null ? other.getErrorMessage() == null : ErrorMessage.equals(other.getErrorMessage()))
			&& (UnderwritSequenceNo == null ? other.getUnderwritSequenceNo() == null : UnderwritSequenceNo.equals(other.getUnderwritSequenceNo()))
			&& (CutomerSequenceNo == null ? other.getCutomerSequenceNo() == null : CutomerSequenceNo.equals(other.getCutomerSequenceNo()))
			&& (StartTime == null ? other.getStartTime() == null : fDate.getString(StartTime).equals(other.getStartTime()))
			&& (EndTime == null ? other.getEndTime() == null : fDate.getString(EndTime).equals(other.getEndTime()))
			&& AllLifeSumInsured == other.getAllLifeSumInsured()
			&& AllAccidentSumInsured == other.getAllAccidentSumInsured()
			&& AllHealthSumInsured == other.getAllHealthSumInsured()
			&& AllOldSumInsured == other.getAllOldSumInsured()
			&& AllOtherSumInsured == other.getAllOtherSumInsured()
			&& AllHospitalAllowance == other.getAllHospitalAllowance()
			&& AllClaimAmount == other.getAllClaimAmount()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("DataBatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("DataSerialNo") ) {
			return 1;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 2;
		}
		if( strFieldName.equals("RequestType") ) {
			return 3;
		}
		if( strFieldName.equals("TaskNo") ) {
			return 4;
		}
		if( strFieldName.equals("ResponseCode") ) {
			return 5;
		}
		if( strFieldName.equals("ErrorMessage") ) {
			return 6;
		}
		if( strFieldName.equals("UnderwritSequenceNo") ) {
			return 7;
		}
		if( strFieldName.equals("CutomerSequenceNo") ) {
			return 8;
		}
		if( strFieldName.equals("StartTime") ) {
			return 9;
		}
		if( strFieldName.equals("EndTime") ) {
			return 10;
		}
		if( strFieldName.equals("AllLifeSumInsured") ) {
			return 11;
		}
		if( strFieldName.equals("AllAccidentSumInsured") ) {
			return 12;
		}
		if( strFieldName.equals("AllHealthSumInsured") ) {
			return 13;
		}
		if( strFieldName.equals("AllOldSumInsured") ) {
			return 14;
		}
		if( strFieldName.equals("AllOtherSumInsured") ) {
			return 15;
		}
		if( strFieldName.equals("AllHospitalAllowance") ) {
			return 16;
		}
		if( strFieldName.equals("AllClaimAmount") ) {
			return 17;
		}
		if( strFieldName.equals("Operator") ) {
			return 18;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 19;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 20;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 22;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "DataBatchNo";
				break;
			case 1:
				strFieldName = "DataSerialNo";
				break;
			case 2:
				strFieldName = "PrtNo";
				break;
			case 3:
				strFieldName = "RequestType";
				break;
			case 4:
				strFieldName = "TaskNo";
				break;
			case 5:
				strFieldName = "ResponseCode";
				break;
			case 6:
				strFieldName = "ErrorMessage";
				break;
			case 7:
				strFieldName = "UnderwritSequenceNo";
				break;
			case 8:
				strFieldName = "CutomerSequenceNo";
				break;
			case 9:
				strFieldName = "StartTime";
				break;
			case 10:
				strFieldName = "EndTime";
				break;
			case 11:
				strFieldName = "AllLifeSumInsured";
				break;
			case 12:
				strFieldName = "AllAccidentSumInsured";
				break;
			case 13:
				strFieldName = "AllHealthSumInsured";
				break;
			case 14:
				strFieldName = "AllOldSumInsured";
				break;
			case 15:
				strFieldName = "AllOtherSumInsured";
				break;
			case 16:
				strFieldName = "AllHospitalAllowance";
				break;
			case 17:
				strFieldName = "AllClaimAmount";
				break;
			case 18:
				strFieldName = "Operator";
				break;
			case 19:
				strFieldName = "MakeDate";
				break;
			case 20:
				strFieldName = "MakeTime";
				break;
			case 21:
				strFieldName = "ModifyDate";
				break;
			case 22:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("DataBatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RequestType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaskNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResponseCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorMessage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnderwritSequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CutomerSequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartTime") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndTime") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AllLifeSumInsured") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AllAccidentSumInsured") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AllHealthSumInsured") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AllOldSumInsured") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AllOtherSumInsured") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AllHospitalAllowance") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AllClaimAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
