/**
 * Copyright (c) 20171111 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;

import com.sinosoft.lis.db.CiitcTaskDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: CiitcTaskSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-08-07
 */
public class CiitcTaskSchema implements Schema, Cloneable {

	/** 流水号 */
	private String TranNo;
	/** 投保单号 */
	private String ProposalNo;
	/** 业务来源 */
	private String TaskSource;
	/** 银行柜员（客户经理）工号 */
	private String CounterId;
	/** 银行编码 */
	private String BankCode;
	/** 分行编码 */
	private String SubBankCode;
	/** 网点编码 */
	private String NodeBankCode;
	/** 保险公司代码（总公司） */
	private String InsurerCode;
	/** 银行 公司代码（省级分公司） */
	private String SubInsurerCode;
	/** 产品名称 */
	private String ProductName;
	/** 产品代码 */
	private String ProductCode;
	/** 业务识别号 */
	private String BusinessNo;
	/** 客户姓名 */
	private String CustomerName;
	/** 客户证件类型 */
	private String CustomerCardType;
	/** 客户证件号码 */
	private String CustomerCardNo;
	/** 客户出生日期 */
	private String CustomerBirthDay;
	/** 视频文件名 */
	private String VideoName;
	/** 视频格式类型 */
	private String VideoType;
	/** 视频大小 */
	private String VideoSize;
	/** 业务流水号 */
	private String BusinessSeriaNo;
	/** 批次号 */
	private String BatchNo;
	/** 文件签名 */
	private String ContentMD5;
	/** 缴费方式 */
	private String PaymentTerm;
	/** 预留字段1 */
	private String Bak1;
	/** 预留字段2 */
	private String Bak2;
	/** 预留字段3 */
	private String Bak3;

	public static final int FIELDNUM = 26; // 数据库表的字段个数

	public CErrors mErrors; // 错误信息

	public String getTranNo() {
		return TranNo;
	}

	public void setTranNo(String tranNo) {
		TranNo = tranNo;
	}

	public String getProposalNo() {
		return ProposalNo;
	}

	public void setProposalNo(String proposalNo) {
		ProposalNo = proposalNo;
	}

	public String getTaskSource() {
		return TaskSource;
	}

	public void setTaskSource(String taskSource) {
		TaskSource = taskSource;
	}

	public String getCounterId() {
		return CounterId;
	}

	public void setCounterId(String counterId) {
		CounterId = counterId;
	}

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getSubBankCode() {
		return SubBankCode;
	}

	public void setSubBankCode(String subBankCode) {
		SubBankCode = subBankCode;
	}

	public String getNodeBankCode() {
		return NodeBankCode;
	}

	public void setNodeBankCode(String nodeBankCode) {
		NodeBankCode = nodeBankCode;
	}

	public String getInsurerCode() {
		return InsurerCode;
	}

	public void setInsurerCode(String insurerCode) {
		InsurerCode = insurerCode;
	}

	public String getSubInsurerCode() {
		return SubInsurerCode;
	}

	public void setSubInsurerCode(String subInsurerCode) {
		SubInsurerCode = subInsurerCode;
	}

	public String getProductName() {
		return ProductName;
	}

	public void setProductName(String productName) {
		ProductName = productName;
	}

	public String getProductCode() {
		return ProductCode;
	}

	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}

	public String getBusinessNo() {
		return BusinessNo;
	}

	public void setBusinessNo(String businessNo) {
		BusinessNo = businessNo;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getCustomerCardType() {
		return CustomerCardType;
	}

	public void setCustomerCardType(String customerCardType) {
		CustomerCardType = customerCardType;
	}

	public String getCustomerCardNo() {
		return CustomerCardNo;
	}

	public void setCustomerCardNo(String customerCardNo) {
		CustomerCardNo = customerCardNo;
	}

	public String getCustomerBirthDay() {
		return CustomerBirthDay;
	}

	public void setCustomerBirthDay(String customerBirthDay) {
		CustomerBirthDay = customerBirthDay;
	}

	public String getVideoName() {
		return VideoName;
	}

	public void setVideoName(String videoName) {
		VideoName = videoName;
	}

	public String getVideoType() {
		return VideoType;
	}

	public void setVideoType(String videoType) {
		VideoType = videoType;
	}

	public String getVideoSize() {
		return VideoSize;
	}

	public void setVideoSize(String videoSize) {
		VideoSize = videoSize;
	}

	public String getBusinessSeriaNo() {
		return BusinessSeriaNo;
	}

	public void setBusinessSeriaNo(String businessSeriaNo) {
		BusinessSeriaNo = businessSeriaNo;
	}

	public String getBatchNo() {
		return BatchNo;
	}

	public void setBatchNo(String batchNo) {
		BatchNo = batchNo;
	}

	public String getContentMD5() {
		return ContentMD5;
	}

	public void setContentMD5(String contentMD5) {
		ContentMD5 = contentMD5;
	}

	public String getPaymentTerm() {
		return PaymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		PaymentTerm = paymentTerm;
	}

	public String getBak1() {
		return Bak1;
	}

	public void setBak1(String bak1) {
		Bak1 = bak1;
	}

	public String getBak2() {
		return Bak2;
	}

	public void setBak2(String bak2) {
		Bak2 = bak2;
	}

	public String getBak3() {
		return Bak3;
	}

	public void setBak3(String bak3) {
		Bak3 = bak3;
	}

	public int getFieldCount() {
		// TODO Auto-generated method stub
		return FIELDNUM;
	}

	public int getFieldIndex(String strFieldName) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getFieldName(int nFieldIndex) {

		String strFieldName = "";
		switch (nFieldIndex) {
		case 0:
			strFieldName = "TranNo";
			break;
		case 1:
			strFieldName = "ProposalNo";
			break;
		case 2:
			strFieldName = "TaskSource";
			break;
		case 3:
			strFieldName = "CounterId";
			break;
		case 4:
			strFieldName = "BankCode";
			break;
		case 5:
			strFieldName = "SubBankCode";
			break;
		case 6:
			strFieldName = "NodeBankCode";
			break;
		case 7:
			strFieldName = "InsurerCode";
			break;
		case 8:
			strFieldName = "SubInsurerCode";
			break;
		case 9:
			strFieldName = "Product_Name";
			break;
		case 10:
			strFieldName = "Product_Code";
			break;
		case 11:
			strFieldName = "BusinessNo";
			break;
		case 12:
			strFieldName = "CustomerName";
			break;
		case 13:
			strFieldName = "CustomerCardType";
			break;
		case 14:
			strFieldName = "CustomerCardNo";
			break;
		case 15:
			strFieldName = "CustomerBirthDay";
			break;
		case 16:
			strFieldName = "VideoName";
			break;
		case 17:
			strFieldName = "VideoType";
			break;
		case 18:
			strFieldName = "VideoSize";
			break;
		case 19:
			strFieldName = "BusinessSeriaNo";
			break;
		case 20:
			strFieldName = "BatchNo";
			break;
		case 21:
			strFieldName = "Content_MD5";
			break;
		case 22:
			strFieldName = "PaymentTerm";
			break;
		case 23:
			strFieldName = "Bak1";
			break;
		case 24:
			strFieldName = "Bak2";
			break;
		case 25:
			strFieldName = "Bak3";
			break;
		default:
			strFieldName = "";
		}
		;
		return strFieldName;

	}

	public int getFieldType(String strFieldName) {

		if (strFieldName.equals("TranNo")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("ProposalNo")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("TaskSource")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("CounterId")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("BankCode")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("SubBankCode")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("NodeBankCode")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("InsurerCode")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("SubInsurerCode")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("ProductName")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("ProductCode")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("BusinessNo")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("CustomerName")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("CustomerCardType")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("CustomerCardNo")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("CustomerBirthDay")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("VideoName")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("VideoType")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("VideoSize")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("BusinessSeriaNo")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("BatchNo")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("ContentMD5")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("PaymentTerm")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("Bak1")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("Bak2")) {
			return Schema.TYPE_STRING;
		}
		if (strFieldName.equals("Bak3")) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	public int getFieldType(int nFieldIndex) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String[] getPK() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getV(String FCode) {

		String strReturn = "";
		if (FCode.equals("TranNo")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(TranNo));
		}
		if (FCode.equals("ProposalNo")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
		}
		if (FCode.equals("TaskSource")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaskSource));
		}
		if (FCode.equals("CounterId")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(CounterId));
		}
		if (FCode.equals("BankCode")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("SubBankCode")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubBankCode));
		}
		if (FCode.equals("NodeBankCode")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(NodeBankCode));
		}
		if (FCode.equals("InsurerCode")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsurerCode));
		}
		if (FCode.equals("SubInsurerCode")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubInsurerCode));
		}
		if (FCode.equals("ProductName")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProductName));
		}
		if (FCode.equals("ProductCode")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProductCode));
		}
		if (FCode.equals("BusinessNo")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessNo));
		}
		if (FCode.equals("CustomerName")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("CustomerCardType")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerCardType));
		}
		if (FCode.equals("CustomerCardNo")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerCardNo));
		}
		if (FCode.equals("CustomerBirthDay")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerBirthDay));
		}
		if (FCode.equals("VideoName")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(VideoName));
		}
		if (FCode.equals("VideoType")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(VideoType));
		}
		if (FCode.equals("VideoSize")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(VideoSize));
		}
		if (FCode.equals("BusinessSeriaNo")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessSeriaNo));
		}
		if (FCode.equals("BatchNo")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("ContentMD5")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContentMD5));
		}
		if (FCode.equals("PaymentTerm")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(PaymentTerm));
		}
		if (FCode.equals("Bak1")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak1));
		}
		if (FCode.equals("Bak2")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak2));
		}
		if (FCode.equals("Bak3")) {
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bak3));
		}

		return strReturn;

	}

	public String getV(int nFieldIndex) {

		String strFieldValue = "";
		switch (nFieldIndex) {
		case 0:
			strFieldValue = StrTool.GBKToUnicode(TranNo);
			break;
		case 1:
			strFieldValue = StrTool.GBKToUnicode(ProposalNo);
			break;
		case 2:
			strFieldValue = StrTool.GBKToUnicode(TaskSource);
			break;
		case 3:
			strFieldValue = StrTool.GBKToUnicode(CounterId);
			break;
		case 4:
			strFieldValue = StrTool.GBKToUnicode(BankCode);
			break;
		case 5:
			strFieldValue = StrTool.GBKToUnicode(SubBankCode);
			break;
		case 6:
			strFieldValue = StrTool.GBKToUnicode(NodeBankCode);
			break;
		case 7:
			strFieldValue = StrTool.GBKToUnicode(InsurerCode);
			break;
		case 8:
			strFieldValue = StrTool.GBKToUnicode(SubInsurerCode);
			break;
		case 9:
			strFieldValue = StrTool.GBKToUnicode(ProductName);
			break;
		case 10:
			strFieldValue = StrTool.GBKToUnicode(ProductCode);
			break;
		case 11:
			strFieldValue = StrTool.GBKToUnicode(BusinessNo);
			break;
		case 12:
			strFieldValue = StrTool.GBKToUnicode(CustomerName);
			break;
		case 13:
			strFieldValue = StrTool.GBKToUnicode(CustomerCardType);
			break;
		case 14:
			strFieldValue = StrTool.GBKToUnicode(CustomerCardNo);
			break;
		case 15:
			strFieldValue = StrTool.GBKToUnicode(CustomerBirthDay);
			break;
		case 16:
			strFieldValue = StrTool.GBKToUnicode(VideoName);
			break;
		case 17:
			strFieldValue = StrTool.GBKToUnicode(VideoType);
			break;
		case 18:
			strFieldValue = StrTool.GBKToUnicode(VideoSize);
			break;
		case 19:
			strFieldValue = StrTool.GBKToUnicode(BusinessSeriaNo);
			break;
		case 20:
			strFieldValue = StrTool.GBKToUnicode(BatchNo);
			break;
		case 21:
			strFieldValue = StrTool.GBKToUnicode(ContentMD5);
			break;
		case 22:
			strFieldValue = StrTool.GBKToUnicode(String.valueOf(PaymentTerm));
			break;
		case 23:
			strFieldValue = StrTool.GBKToUnicode(Bak1);
			break;
		case 24:
			strFieldValue = StrTool.GBKToUnicode(Bak2);
			break;
		case 25:
			strFieldValue = StrTool.GBKToUnicode(Bak3);
			break;
		default:
			strFieldValue = "";
		}
		;
		if (strFieldValue.equals("")) {
			strFieldValue = "null";
		}
		return strFieldValue;

	}

	public boolean setV(String strFieldName, String strFieldValue) {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 使用另外一个 CiitcTaskSchema 对象给 Schema 赋值
	 * 
	 * @param: aCiitcTaskSchema CiitcTaskSchema
	 **/
	public void setSchema(CiitcTaskSchema aCiitcTaskSchema) {
		this.TranNo = aCiitcTaskSchema.getTranNo();
		this.ProposalNo = aCiitcTaskSchema.getProposalNo();
		this.BankCode = aCiitcTaskSchema.getBankCode();
		this.TaskSource = aCiitcTaskSchema.getTaskSource();
		this.CounterId = aCiitcTaskSchema.getCounterId();
		this.BankCode = aCiitcTaskSchema.getBankCode();
		this.SubBankCode = aCiitcTaskSchema.getSubBankCode();
		this.NodeBankCode = aCiitcTaskSchema.getNodeBankCode();
		this.InsurerCode = aCiitcTaskSchema.getInsurerCode();
		this.SubInsurerCode = aCiitcTaskSchema.getSubInsurerCode();
		this.ProductName = aCiitcTaskSchema.getProductName();
		this.ProductCode = aCiitcTaskSchema.getProductCode();
		this.BusinessNo = aCiitcTaskSchema.getBusinessNo();
		this.CustomerName = aCiitcTaskSchema.getCustomerName();
		this.CustomerCardType = aCiitcTaskSchema.getCustomerCardType();
		this.CustomerCardNo = aCiitcTaskSchema.getCustomerCardNo();
		this.CustomerBirthDay = aCiitcTaskSchema.getCustomerBirthDay();
		this.VideoName = aCiitcTaskSchema.getVideoName();
		this.VideoType = aCiitcTaskSchema.getVideoType();
		this.VideoSize = aCiitcTaskSchema.getVideoSize();
		this.BusinessSeriaNo = aCiitcTaskSchema.getBusinessSeriaNo();
		this.BatchNo = aCiitcTaskSchema.getBatchNo();
		this.ContentMD5 = aCiitcTaskSchema.getContentMD5();
		this.PaymentTerm = aCiitcTaskSchema.getPaymentTerm();
		this.Bak1 = aCiitcTaskSchema.getBak1();
		this.Bak2 = aCiitcTaskSchema.getBak2();
		this.Bak3 = aCiitcTaskSchema.getBak3();
	}

	public CiitcTaskSchema getSchema() {
		CiitcTaskSchema aCiitcTaskSchema = new CiitcTaskSchema();
		aCiitcTaskSchema.setSchema(this);
		return aCiitcTaskSchema;
	}

	public boolean setSchema(ResultSet rs, int i) {
		try {
			// rs.absolute(i); // 非滚动游标
			if (rs.getString("TranNo") == null)
				this.TranNo = null;
			else
				this.TranNo = rs.getString("TranNo").trim();

			if (rs.getString("ProposalNo") == null)
				this.ProposalNo = null;
			else
				this.ProposalNo = rs.getString("ProposalNo").trim();

			if (rs.getString("BankCode") == null)
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if (rs.getString("TaskSource") == null)
				this.TaskSource = null;
			else
				this.TaskSource = rs.getString("TaskSource").trim();

			if (rs.getString("CounterId") == null)
				this.CounterId = null;
			else
				this.CounterId = rs.getString("CounterId").trim();

			if (rs.getString("BankCode") == null)
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if (rs.getString("SubBankCode") == null)
				this.SubBankCode = null;
			else
				this.SubBankCode = rs.getString("SubBankCode").trim();

			if (rs.getString("NodeBankCode") == null)
				this.NodeBankCode = null;
			else
				this.NodeBankCode = rs.getString("NodeBankCode").trim();

			if (rs.getString("InsurerCode") == null)
				this.InsurerCode = null;
			else
				this.InsurerCode = rs.getString("InsurerCode").trim();

			if (rs.getString("SubInsurerCode") == null)
				this.SubInsurerCode = null;
			else
				this.SubInsurerCode = rs.getString("SubInsurerCode").trim();

			if (rs.getString("ProductName") == null)
				this.ProductName = null;
			else
				this.ProductName = rs.getString("ProductName").trim();

			if (rs.getString("ProductCode") == null)
				this.ProductCode = null;
			else
				this.ProductCode = rs.getString("ProductCode").trim();

			if (rs.getString("BusinessNo") == null)
				this.BusinessNo = null;
			else
				this.BusinessNo = rs.getString("BusinessNo").trim();

			if (rs.getString("CustomerName") == null)
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if (rs.getString("CustomerCardType") == null)
				this.CustomerCardType = null;
			else
				this.CustomerCardType = rs.getString("CustomerCardType").trim();

			if (rs.getString("CustomerCardNo") == null)
				this.CustomerCardNo = null;
			else
				this.CustomerCardNo = rs.getString("CustomerCardNo").trim();

			if (rs.getString("CustomerBirthDay") == null)
				this.CustomerBirthDay = null;
			else
				this.CustomerBirthDay = rs.getString("CustomerBirthDay").trim();

			if (rs.getString("VideoName") == null)
				this.VideoName = null;
			else
				this.VideoName = rs.getString("VideoName").trim();

			if (rs.getString("VideoType") == null)
				this.VideoType = null;
			else
				this.VideoType = rs.getString("VideoType").trim();

			if (rs.getString("VideoSize") == null)
				this.VideoSize = null;
			else
				this.VideoSize = rs.getString("VideoSize").trim();

			if (rs.getString("BusinessSeriaNo") == null)
				this.BusinessSeriaNo = null;
			else
				this.BusinessSeriaNo = rs.getString("BusinessSeriaNo").trim();

			if (rs.getString("BatchNo") == null)
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if (rs.getString("ContentMD5") == null)
				this.ContentMD5 = null;
			else
				this.ContentMD5 = rs.getString("ContentMD5").trim();

			if (rs.getString("PaymentTerm") == null)
				this.PaymentTerm = null;
			else
				this.PaymentTerm = rs.getString("PaymentTerm");

			if (rs.getString("Bak1") == null)
				this.Bak1 = null;
			else
				this.Bak1 = rs.getString("Bak1").trim();

			if (rs.getString("Bak2") == null)
				this.Bak2 = null;
			else
				this.Bak2 = rs.getString("Bak2").trim();

			if (rs.getString("Bak3") == null)
				this.Bak3 = null;
			else
				this.Bak3 = rs.getString("Bak3").trim();

		} catch (SQLException sqle) {
			System.out
					.println("数据库中的LKTransStatus表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKTransStatusSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}
	 * /dataStructure/tb.html#PrpCiitcTask描述/A>表字段
	 * 
	 * @return: String 返回打包后字符串
	 **/
	public String encode() {
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TranNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaskSource));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CounterId));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubBankCode));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NodeBankCode));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsurerCode));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubInsurerCode));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProductName));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProductCode));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerCardType));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerCardNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerBirthDay));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VideoName));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VideoType));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VideoSize));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessSeriaNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContentMD5));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PaymentTerm));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak1));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak2));
		strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bak3));
		strReturn.append(SysConst.PACKAGESPILTER);
		return strReturn.toString();
	}

	/**
	 * 数据解包，解包顺序参见<A href ={@docRoot}
	 * /dataStructure/tb.html#PrpCiitcTask>历史记账凭证主表信息</A>表字段
	 * 
	 * @param: strMessage String 包含一条纪录数据的字符串
	 * @return: boolean
	 **/
	public boolean decode(String strMessage) {
		try {
			TranNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
					SysConst.PACKAGESPILTER);
			ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
					SysConst.PACKAGESPILTER);
			TaskSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
					SysConst.PACKAGESPILTER);
			CounterId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
					SysConst.PACKAGESPILTER);
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
					SysConst.PACKAGESPILTER);
			SubBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
					SysConst.PACKAGESPILTER);
			NodeBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
					SysConst.PACKAGESPILTER);
			InsurerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
					SysConst.PACKAGESPILTER);
			SubInsurerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
					9, SysConst.PACKAGESPILTER);
			ProductName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
					SysConst.PACKAGESPILTER);
			ProductCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
					SysConst.PACKAGESPILTER);
			BusinessNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
					SysConst.PACKAGESPILTER);
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
					SysConst.PACKAGESPILTER);
			CustomerCardType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
					14, SysConst.PACKAGESPILTER);
			CustomerCardNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
					15, SysConst.PACKAGESPILTER);
			CustomerBirthDay = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
					16, SysConst.PACKAGESPILTER);
			VideoName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
					SysConst.PACKAGESPILTER);
			VideoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
					SysConst.PACKAGESPILTER);
			VideoSize = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
					SysConst.PACKAGESPILTER);
			BusinessSeriaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
					20, SysConst.PACKAGESPILTER);
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
					SysConst.PACKAGESPILTER);
			ContentMD5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
					SysConst.PACKAGESPILTER);
			PaymentTerm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
					SysConst.PACKAGESPILTER);
			Bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
					SysConst.PACKAGESPILTER);
			Bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
					SysConst.PACKAGESPILTER);
			Bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
					SysConst.PACKAGESPILTER);
		} catch (NumberFormatException ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CiitcTaskSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors.addOneError(tError);

			return false;
		}
		return true;
	}
}
