/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.HMVisitInfoDB;

/*
 * <p>ClassName: HMVisitInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-03-09
 */
public class HMVisitInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String FlowNo;
	/** 客户号码 */
	private String CustomerNo;
	/** 客户姓名 */
	private String Name;
	/** 探访医院编码 */
	private String HospitCode;
	/** 探访医院名称 */
	private String HospitName;
	/** 探访时间 */
	private Date VisitDate;
	/** 是否进行客户身份确认 */
	private String Identifiction;
	/** 是否收集相关资料 */
	private String Collection;
	/** 有无挂/压床 */
	private String Bed;
	/** 是否确认就诊断依据和诊疗项目合理性 */
	private String Legitimacy;
	/** 是否提供相关就医和理赔支持服务 */
	private String Support;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 备用字段1 */
	private String StandbyFlag1;
	/** 备用字段2 */
	private String StandbyFlag2;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public HMVisitInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "FlowNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		HMVisitInfoSchema cloned = (HMVisitInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getFlowNo()
	{
		return FlowNo;
	}
	public void setFlowNo(String aFlowNo)
	{
		FlowNo = aFlowNo;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getHospitCode()
	{
		return HospitCode;
	}
	public void setHospitCode(String aHospitCode)
	{
		HospitCode = aHospitCode;
	}
	public String getHospitName()
	{
		return HospitName;
	}
	public void setHospitName(String aHospitName)
	{
		HospitName = aHospitName;
	}
	public String getVisitDate()
	{
		if( VisitDate != null )
			return fDate.getString(VisitDate);
		else
			return null;
	}
	public void setVisitDate(Date aVisitDate)
	{
		VisitDate = aVisitDate;
	}
	public void setVisitDate(String aVisitDate)
	{
		if (aVisitDate != null && !aVisitDate.equals("") )
		{
			VisitDate = fDate.getDate( aVisitDate );
		}
		else
			VisitDate = null;
	}

	public String getIdentifiction()
	{
		return Identifiction;
	}
	public void setIdentifiction(String aIdentifiction)
	{
		Identifiction = aIdentifiction;
	}
	public String getCollection()
	{
		return Collection;
	}
	public void setCollection(String aCollection)
	{
		Collection = aCollection;
	}
	public String getBed()
	{
		return Bed;
	}
	public void setBed(String aBed)
	{
		Bed = aBed;
	}
	public String getLegitimacy()
	{
		return Legitimacy;
	}
	public void setLegitimacy(String aLegitimacy)
	{
		Legitimacy = aLegitimacy;
	}
	public String getSupport()
	{
		return Support;
	}
	public void setSupport(String aSupport)
	{
		Support = aSupport;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getStandbyFlag1()
	{
		return StandbyFlag1;
	}
	public void setStandbyFlag1(String aStandbyFlag1)
	{
		StandbyFlag1 = aStandbyFlag1;
	}
	public String getStandbyFlag2()
	{
		return StandbyFlag2;
	}
	public void setStandbyFlag2(String aStandbyFlag2)
	{
		StandbyFlag2 = aStandbyFlag2;
	}

	/**
	* 使用另外一个 HMVisitInfoSchema 对象给 Schema 赋值
	* @param: aHMVisitInfoSchema HMVisitInfoSchema
	**/
	public void setSchema(HMVisitInfoSchema aHMVisitInfoSchema)
	{
		this.FlowNo = aHMVisitInfoSchema.getFlowNo();
		this.CustomerNo = aHMVisitInfoSchema.getCustomerNo();
		this.Name = aHMVisitInfoSchema.getName();
		this.HospitCode = aHMVisitInfoSchema.getHospitCode();
		this.HospitName = aHMVisitInfoSchema.getHospitName();
		this.VisitDate = fDate.getDate( aHMVisitInfoSchema.getVisitDate());
		this.Identifiction = aHMVisitInfoSchema.getIdentifiction();
		this.Collection = aHMVisitInfoSchema.getCollection();
		this.Bed = aHMVisitInfoSchema.getBed();
		this.Legitimacy = aHMVisitInfoSchema.getLegitimacy();
		this.Support = aHMVisitInfoSchema.getSupport();
		this.ManageCom = aHMVisitInfoSchema.getManageCom();
		this.Operator = aHMVisitInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aHMVisitInfoSchema.getMakeDate());
		this.MakeTime = aHMVisitInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aHMVisitInfoSchema.getModifyDate());
		this.ModifyTime = aHMVisitInfoSchema.getModifyTime();
		this.StandbyFlag1 = aHMVisitInfoSchema.getStandbyFlag1();
		this.StandbyFlag2 = aHMVisitInfoSchema.getStandbyFlag2();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("FlowNo") == null )
				this.FlowNo = null;
			else
				this.FlowNo = rs.getString("FlowNo").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("HospitCode") == null )
				this.HospitCode = null;
			else
				this.HospitCode = rs.getString("HospitCode").trim();

			if( rs.getString("HospitName") == null )
				this.HospitName = null;
			else
				this.HospitName = rs.getString("HospitName").trim();

			this.VisitDate = rs.getDate("VisitDate");
			if( rs.getString("Identifiction") == null )
				this.Identifiction = null;
			else
				this.Identifiction = rs.getString("Identifiction").trim();

			if( rs.getString("Collection") == null )
				this.Collection = null;
			else
				this.Collection = rs.getString("Collection").trim();

			if( rs.getString("Bed") == null )
				this.Bed = null;
			else
				this.Bed = rs.getString("Bed").trim();

			if( rs.getString("Legitimacy") == null )
				this.Legitimacy = null;
			else
				this.Legitimacy = rs.getString("Legitimacy").trim();

			if( rs.getString("Support") == null )
				this.Support = null;
			else
				this.Support = rs.getString("Support").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("StandbyFlag1") == null )
				this.StandbyFlag1 = null;
			else
				this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

			if( rs.getString("StandbyFlag2") == null )
				this.StandbyFlag2 = null;
			else
				this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的HMVisitInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMVisitInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public HMVisitInfoSchema getSchema()
	{
		HMVisitInfoSchema aHMVisitInfoSchema = new HMVisitInfoSchema();
		aHMVisitInfoSchema.setSchema(this);
		return aHMVisitInfoSchema;
	}

	public HMVisitInfoDB getDB()
	{
		HMVisitInfoDB aDBOper = new HMVisitInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHMVisitInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(FlowNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( VisitDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Identifiction)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Collection)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bed)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Legitimacy)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Support)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag2));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHMVisitInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			FlowNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			HospitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			HospitName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			VisitDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			Identifiction = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Collection = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Bed = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Legitimacy = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Support = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMVisitInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("FlowNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FlowNo));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("HospitCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitCode));
		}
		if (FCode.equals("HospitName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitName));
		}
		if (FCode.equals("VisitDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getVisitDate()));
		}
		if (FCode.equals("Identifiction"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Identifiction));
		}
		if (FCode.equals("Collection"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Collection));
		}
		if (FCode.equals("Bed"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bed));
		}
		if (FCode.equals("Legitimacy"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Legitimacy));
		}
		if (FCode.equals("Support"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Support));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("StandbyFlag1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
		}
		if (FCode.equals("StandbyFlag2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(FlowNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(HospitCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(HospitName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getVisitDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Identifiction);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Collection);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Bed);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Legitimacy);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Support);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("FlowNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FlowNo = FValue.trim();
			}
			else
				FlowNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("HospitCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitCode = FValue.trim();
			}
			else
				HospitCode = null;
		}
		if (FCode.equalsIgnoreCase("HospitName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitName = FValue.trim();
			}
			else
				HospitName = null;
		}
		if (FCode.equalsIgnoreCase("VisitDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				VisitDate = fDate.getDate( FValue );
			}
			else
				VisitDate = null;
		}
		if (FCode.equalsIgnoreCase("Identifiction"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Identifiction = FValue.trim();
			}
			else
				Identifiction = null;
		}
		if (FCode.equalsIgnoreCase("Collection"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Collection = FValue.trim();
			}
			else
				Collection = null;
		}
		if (FCode.equalsIgnoreCase("Bed"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bed = FValue.trim();
			}
			else
				Bed = null;
		}
		if (FCode.equalsIgnoreCase("Legitimacy"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Legitimacy = FValue.trim();
			}
			else
				Legitimacy = null;
		}
		if (FCode.equalsIgnoreCase("Support"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Support = FValue.trim();
			}
			else
				Support = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag1 = FValue.trim();
			}
			else
				StandbyFlag1 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag2 = FValue.trim();
			}
			else
				StandbyFlag2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		HMVisitInfoSchema other = (HMVisitInfoSchema)otherObject;
		return
			(FlowNo == null ? other.getFlowNo() == null : FlowNo.equals(other.getFlowNo()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (HospitCode == null ? other.getHospitCode() == null : HospitCode.equals(other.getHospitCode()))
			&& (HospitName == null ? other.getHospitName() == null : HospitName.equals(other.getHospitName()))
			&& (VisitDate == null ? other.getVisitDate() == null : fDate.getString(VisitDate).equals(other.getVisitDate()))
			&& (Identifiction == null ? other.getIdentifiction() == null : Identifiction.equals(other.getIdentifiction()))
			&& (Collection == null ? other.getCollection() == null : Collection.equals(other.getCollection()))
			&& (Bed == null ? other.getBed() == null : Bed.equals(other.getBed()))
			&& (Legitimacy == null ? other.getLegitimacy() == null : Legitimacy.equals(other.getLegitimacy()))
			&& (Support == null ? other.getSupport() == null : Support.equals(other.getSupport()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (StandbyFlag1 == null ? other.getStandbyFlag1() == null : StandbyFlag1.equals(other.getStandbyFlag1()))
			&& (StandbyFlag2 == null ? other.getStandbyFlag2() == null : StandbyFlag2.equals(other.getStandbyFlag2()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("FlowNo") ) {
			return 0;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 1;
		}
		if( strFieldName.equals("Name") ) {
			return 2;
		}
		if( strFieldName.equals("HospitCode") ) {
			return 3;
		}
		if( strFieldName.equals("HospitName") ) {
			return 4;
		}
		if( strFieldName.equals("VisitDate") ) {
			return 5;
		}
		if( strFieldName.equals("Identifiction") ) {
			return 6;
		}
		if( strFieldName.equals("Collection") ) {
			return 7;
		}
		if( strFieldName.equals("Bed") ) {
			return 8;
		}
		if( strFieldName.equals("Legitimacy") ) {
			return 9;
		}
		if( strFieldName.equals("Support") ) {
			return 10;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 13;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 16;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return 17;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "FlowNo";
				break;
			case 1:
				strFieldName = "CustomerNo";
				break;
			case 2:
				strFieldName = "Name";
				break;
			case 3:
				strFieldName = "HospitCode";
				break;
			case 4:
				strFieldName = "HospitName";
				break;
			case 5:
				strFieldName = "VisitDate";
				break;
			case 6:
				strFieldName = "Identifiction";
				break;
			case 7:
				strFieldName = "Collection";
				break;
			case 8:
				strFieldName = "Bed";
				break;
			case 9:
				strFieldName = "Legitimacy";
				break;
			case 10:
				strFieldName = "Support";
				break;
			case 11:
				strFieldName = "ManageCom";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "MakeDate";
				break;
			case 14:
				strFieldName = "MakeTime";
				break;
			case 15:
				strFieldName = "ModifyDate";
				break;
			case 16:
				strFieldName = "ModifyTime";
				break;
			case 17:
				strFieldName = "StandbyFlag1";
				break;
			case 18:
				strFieldName = "StandbyFlag2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("FlowNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VisitDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Identifiction") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Collection") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bed") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Legitimacy") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Support") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
