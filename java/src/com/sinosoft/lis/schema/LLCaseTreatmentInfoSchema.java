/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCaseTreatmentInfoDB;

/*
 * <p>ClassName: LLCaseTreatmentInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2019-08-12
 */
public class LLCaseTreatmentInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 诊疗项目编码 */
	private String TreatmentCode;
	/** 项目名称 */
	private String TreatmentName;
	/** 项目内涵 */
	private String TreatmentDetail;
	/** 说明 */
	private String TreatmentExplain;
	/** 类别 */
	private String TreatmentCategory;
	/** 拼音简码 */
	private String Phonetic;
	/** 打包编码 */
	private String PackagingCode;
	/** 组合描述 */
	private String ComDescription;
	/** 计价单位 */
	private String ChargeUnit;
	/** 单价 */
	private double UnitPrice;
	/** 备注 */
	private String Remark;
	/** 管理机构 */
	private String MngCom;
	/** 地区编码 */
	private String AreaCode;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 费用等级 */
	private String CostLevel;

	public static final int FIELDNUM = 20;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCaseTreatmentInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLCaseTreatmentInfoSchema cloned = (LLCaseTreatmentInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getTreatmentCode()
	{
		return TreatmentCode;
	}
	public void setTreatmentCode(String aTreatmentCode)
	{
		TreatmentCode = aTreatmentCode;
	}
	public String getTreatmentName()
	{
		return TreatmentName;
	}
	public void setTreatmentName(String aTreatmentName)
	{
		TreatmentName = aTreatmentName;
	}
	public String getTreatmentDetail()
	{
		return TreatmentDetail;
	}
	public void setTreatmentDetail(String aTreatmentDetail)
	{
		TreatmentDetail = aTreatmentDetail;
	}
	public String getTreatmentExplain()
	{
		return TreatmentExplain;
	}
	public void setTreatmentExplain(String aTreatmentExplain)
	{
		TreatmentExplain = aTreatmentExplain;
	}
	public String getTreatmentCategory()
	{
		return TreatmentCategory;
	}
	public void setTreatmentCategory(String aTreatmentCategory)
	{
		TreatmentCategory = aTreatmentCategory;
	}
	public String getPhonetic()
	{
		return Phonetic;
	}
	public void setPhonetic(String aPhonetic)
	{
		Phonetic = aPhonetic;
	}
	public String getPackagingCode()
	{
		return PackagingCode;
	}
	public void setPackagingCode(String aPackagingCode)
	{
		PackagingCode = aPackagingCode;
	}
	public String getComDescription()
	{
		return ComDescription;
	}
	public void setComDescription(String aComDescription)
	{
		ComDescription = aComDescription;
	}
	public String getChargeUnit()
	{
		return ChargeUnit;
	}
	public void setChargeUnit(String aChargeUnit)
	{
		ChargeUnit = aChargeUnit;
	}
	public double getUnitPrice()
	{
		return UnitPrice;
	}
	public void setUnitPrice(double aUnitPrice)
	{
		UnitPrice = Arith.round(aUnitPrice,2);
	}
	public void setUnitPrice(String aUnitPrice)
	{
		if (aUnitPrice != null && !aUnitPrice.equals(""))
		{
			Double tDouble = new Double(aUnitPrice);
			double d = tDouble.doubleValue();
                UnitPrice = Arith.round(d,2);
		}
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getAreaCode()
	{
		return AreaCode;
	}
	public void setAreaCode(String aAreaCode)
	{
		AreaCode = aAreaCode;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getCostLevel()
	{
		return CostLevel;
	}
	public void setCostLevel(String aCostLevel)
	{
		CostLevel = aCostLevel;
	}

	/**
	* 使用另外一个 LLCaseTreatmentInfoSchema 对象给 Schema 赋值
	* @param: aLLCaseTreatmentInfoSchema LLCaseTreatmentInfoSchema
	**/
	public void setSchema(LLCaseTreatmentInfoSchema aLLCaseTreatmentInfoSchema)
	{
		this.SerialNo = aLLCaseTreatmentInfoSchema.getSerialNo();
		this.TreatmentCode = aLLCaseTreatmentInfoSchema.getTreatmentCode();
		this.TreatmentName = aLLCaseTreatmentInfoSchema.getTreatmentName();
		this.TreatmentDetail = aLLCaseTreatmentInfoSchema.getTreatmentDetail();
		this.TreatmentExplain = aLLCaseTreatmentInfoSchema.getTreatmentExplain();
		this.TreatmentCategory = aLLCaseTreatmentInfoSchema.getTreatmentCategory();
		this.Phonetic = aLLCaseTreatmentInfoSchema.getPhonetic();
		this.PackagingCode = aLLCaseTreatmentInfoSchema.getPackagingCode();
		this.ComDescription = aLLCaseTreatmentInfoSchema.getComDescription();
		this.ChargeUnit = aLLCaseTreatmentInfoSchema.getChargeUnit();
		this.UnitPrice = aLLCaseTreatmentInfoSchema.getUnitPrice();
		this.Remark = aLLCaseTreatmentInfoSchema.getRemark();
		this.MngCom = aLLCaseTreatmentInfoSchema.getMngCom();
		this.AreaCode = aLLCaseTreatmentInfoSchema.getAreaCode();
		this.Operator = aLLCaseTreatmentInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLCaseTreatmentInfoSchema.getMakeDate());
		this.MakeTime = aLLCaseTreatmentInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLCaseTreatmentInfoSchema.getModifyDate());
		this.ModifyTime = aLLCaseTreatmentInfoSchema.getModifyTime();
		this.CostLevel = aLLCaseTreatmentInfoSchema.getCostLevel();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("TreatmentCode") == null )
				this.TreatmentCode = null;
			else
				this.TreatmentCode = rs.getString("TreatmentCode").trim();

			if( rs.getString("TreatmentName") == null )
				this.TreatmentName = null;
			else
				this.TreatmentName = rs.getString("TreatmentName").trim();

			if( rs.getString("TreatmentDetail") == null )
				this.TreatmentDetail = null;
			else
				this.TreatmentDetail = rs.getString("TreatmentDetail").trim();

			if( rs.getString("TreatmentExplain") == null )
				this.TreatmentExplain = null;
			else
				this.TreatmentExplain = rs.getString("TreatmentExplain").trim();

			if( rs.getString("TreatmentCategory") == null )
				this.TreatmentCategory = null;
			else
				this.TreatmentCategory = rs.getString("TreatmentCategory").trim();

			if( rs.getString("Phonetic") == null )
				this.Phonetic = null;
			else
				this.Phonetic = rs.getString("Phonetic").trim();

			if( rs.getString("PackagingCode") == null )
				this.PackagingCode = null;
			else
				this.PackagingCode = rs.getString("PackagingCode").trim();

			if( rs.getString("ComDescription") == null )
				this.ComDescription = null;
			else
				this.ComDescription = rs.getString("ComDescription").trim();

			if( rs.getString("ChargeUnit") == null )
				this.ChargeUnit = null;
			else
				this.ChargeUnit = rs.getString("ChargeUnit").trim();

			this.UnitPrice = rs.getDouble("UnitPrice");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("AreaCode") == null )
				this.AreaCode = null;
			else
				this.AreaCode = rs.getString("AreaCode").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("CostLevel") == null )
				this.CostLevel = null;
			else
				this.CostLevel = rs.getString("CostLevel").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCaseTreatmentInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseTreatmentInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCaseTreatmentInfoSchema getSchema()
	{
		LLCaseTreatmentInfoSchema aLLCaseTreatmentInfoSchema = new LLCaseTreatmentInfoSchema();
		aLLCaseTreatmentInfoSchema.setSchema(this);
		return aLLCaseTreatmentInfoSchema;
	}

	public LLCaseTreatmentInfoDB getDB()
	{
		LLCaseTreatmentInfoDB aDBOper = new LLCaseTreatmentInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseTreatmentInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TreatmentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TreatmentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TreatmentDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TreatmentExplain)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TreatmentCategory)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phonetic)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PackagingCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComDescription)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChargeUnit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(UnitPrice));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AreaCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostLevel));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseTreatmentInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TreatmentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			TreatmentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TreatmentDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			TreatmentExplain = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			TreatmentCategory = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Phonetic = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			PackagingCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ComDescription = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ChargeUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			UnitPrice = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			AreaCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			CostLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseTreatmentInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("TreatmentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TreatmentCode));
		}
		if (FCode.equals("TreatmentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TreatmentName));
		}
		if (FCode.equals("TreatmentDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TreatmentDetail));
		}
		if (FCode.equals("TreatmentExplain"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TreatmentExplain));
		}
		if (FCode.equals("TreatmentCategory"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TreatmentCategory));
		}
		if (FCode.equals("Phonetic"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phonetic));
		}
		if (FCode.equals("PackagingCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PackagingCode));
		}
		if (FCode.equals("ComDescription"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComDescription));
		}
		if (FCode.equals("ChargeUnit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeUnit));
		}
		if (FCode.equals("UnitPrice"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnitPrice));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("AreaCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaCode));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("CostLevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostLevel));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TreatmentCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(TreatmentName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TreatmentDetail);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(TreatmentExplain);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(TreatmentCategory);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Phonetic);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(PackagingCode);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ComDescription);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ChargeUnit);
				break;
			case 10:
				strFieldValue = String.valueOf(UnitPrice);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(AreaCode);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(CostLevel);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("TreatmentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TreatmentCode = FValue.trim();
			}
			else
				TreatmentCode = null;
		}
		if (FCode.equalsIgnoreCase("TreatmentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TreatmentName = FValue.trim();
			}
			else
				TreatmentName = null;
		}
		if (FCode.equalsIgnoreCase("TreatmentDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TreatmentDetail = FValue.trim();
			}
			else
				TreatmentDetail = null;
		}
		if (FCode.equalsIgnoreCase("TreatmentExplain"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TreatmentExplain = FValue.trim();
			}
			else
				TreatmentExplain = null;
		}
		if (FCode.equalsIgnoreCase("TreatmentCategory"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TreatmentCategory = FValue.trim();
			}
			else
				TreatmentCategory = null;
		}
		if (FCode.equalsIgnoreCase("Phonetic"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phonetic = FValue.trim();
			}
			else
				Phonetic = null;
		}
		if (FCode.equalsIgnoreCase("PackagingCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PackagingCode = FValue.trim();
			}
			else
				PackagingCode = null;
		}
		if (FCode.equalsIgnoreCase("ComDescription"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComDescription = FValue.trim();
			}
			else
				ComDescription = null;
		}
		if (FCode.equalsIgnoreCase("ChargeUnit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChargeUnit = FValue.trim();
			}
			else
				ChargeUnit = null;
		}
		if (FCode.equalsIgnoreCase("UnitPrice"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				UnitPrice = d;
			}
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("AreaCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaCode = FValue.trim();
			}
			else
				AreaCode = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("CostLevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostLevel = FValue.trim();
			}
			else
				CostLevel = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCaseTreatmentInfoSchema other = (LLCaseTreatmentInfoSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (TreatmentCode == null ? other.getTreatmentCode() == null : TreatmentCode.equals(other.getTreatmentCode()))
			&& (TreatmentName == null ? other.getTreatmentName() == null : TreatmentName.equals(other.getTreatmentName()))
			&& (TreatmentDetail == null ? other.getTreatmentDetail() == null : TreatmentDetail.equals(other.getTreatmentDetail()))
			&& (TreatmentExplain == null ? other.getTreatmentExplain() == null : TreatmentExplain.equals(other.getTreatmentExplain()))
			&& (TreatmentCategory == null ? other.getTreatmentCategory() == null : TreatmentCategory.equals(other.getTreatmentCategory()))
			&& (Phonetic == null ? other.getPhonetic() == null : Phonetic.equals(other.getPhonetic()))
			&& (PackagingCode == null ? other.getPackagingCode() == null : PackagingCode.equals(other.getPackagingCode()))
			&& (ComDescription == null ? other.getComDescription() == null : ComDescription.equals(other.getComDescription()))
			&& (ChargeUnit == null ? other.getChargeUnit() == null : ChargeUnit.equals(other.getChargeUnit()))
			&& UnitPrice == other.getUnitPrice()
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (AreaCode == null ? other.getAreaCode() == null : AreaCode.equals(other.getAreaCode()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (CostLevel == null ? other.getCostLevel() == null : CostLevel.equals(other.getCostLevel()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("TreatmentCode") ) {
			return 1;
		}
		if( strFieldName.equals("TreatmentName") ) {
			return 2;
		}
		if( strFieldName.equals("TreatmentDetail") ) {
			return 3;
		}
		if( strFieldName.equals("TreatmentExplain") ) {
			return 4;
		}
		if( strFieldName.equals("TreatmentCategory") ) {
			return 5;
		}
		if( strFieldName.equals("Phonetic") ) {
			return 6;
		}
		if( strFieldName.equals("PackagingCode") ) {
			return 7;
		}
		if( strFieldName.equals("ComDescription") ) {
			return 8;
		}
		if( strFieldName.equals("ChargeUnit") ) {
			return 9;
		}
		if( strFieldName.equals("UnitPrice") ) {
			return 10;
		}
		if( strFieldName.equals("Remark") ) {
			return 11;
		}
		if( strFieldName.equals("MngCom") ) {
			return 12;
		}
		if( strFieldName.equals("AreaCode") ) {
			return 13;
		}
		if( strFieldName.equals("Operator") ) {
			return 14;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 15;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 18;
		}
		if( strFieldName.equals("CostLevel") ) {
			return 19;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "TreatmentCode";
				break;
			case 2:
				strFieldName = "TreatmentName";
				break;
			case 3:
				strFieldName = "TreatmentDetail";
				break;
			case 4:
				strFieldName = "TreatmentExplain";
				break;
			case 5:
				strFieldName = "TreatmentCategory";
				break;
			case 6:
				strFieldName = "Phonetic";
				break;
			case 7:
				strFieldName = "PackagingCode";
				break;
			case 8:
				strFieldName = "ComDescription";
				break;
			case 9:
				strFieldName = "ChargeUnit";
				break;
			case 10:
				strFieldName = "UnitPrice";
				break;
			case 11:
				strFieldName = "Remark";
				break;
			case 12:
				strFieldName = "MngCom";
				break;
			case 13:
				strFieldName = "AreaCode";
				break;
			case 14:
				strFieldName = "Operator";
				break;
			case 15:
				strFieldName = "MakeDate";
				break;
			case 16:
				strFieldName = "MakeTime";
				break;
			case 17:
				strFieldName = "ModifyDate";
				break;
			case 18:
				strFieldName = "ModifyTime";
				break;
			case 19:
				strFieldName = "CostLevel";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TreatmentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TreatmentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TreatmentDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TreatmentExplain") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TreatmentCategory") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phonetic") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PackagingCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComDescription") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChargeUnit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnitPrice") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AreaCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostLevel") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
