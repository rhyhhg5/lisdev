/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LLCaseBackReasonDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLCaseBackReasonSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 案件抽检
 * @CreateDate：2005-04-25
 */
public class LLCaseBackReasonSchema implements Schema, Cloneable
{
    // @Field
    /** 回退序号 */
    private String CaseBackNo;
    /** 分案号(个人理赔号) */
    private String CaseNo;
    /** 回退原因 */
    private String Reason;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLCaseBackReasonSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "CaseBackNo";
        pk[1] = "CaseNo";
        pk[2] = "Reason";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLCaseBackReasonSchema cloned = (LLCaseBackReasonSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCaseBackNo()
    {
        if (SysConst.CHANGECHARSET && CaseBackNo != null &&
            !CaseBackNo.equals(""))
        {
            CaseBackNo = StrTool.unicodeToGBK(CaseBackNo);
        }
        return CaseBackNo;
    }

    public void setCaseBackNo(String aCaseBackNo)
    {
        CaseBackNo = aCaseBackNo;
    }

    public String getCaseNo()
    {
        if (SysConst.CHANGECHARSET && CaseNo != null && !CaseNo.equals(""))
        {
            CaseNo = StrTool.unicodeToGBK(CaseNo);
        }
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo)
    {
        CaseNo = aCaseNo;
    }

    public String getReason()
    {
        if (SysConst.CHANGECHARSET && Reason != null && !Reason.equals(""))
        {
            Reason = StrTool.unicodeToGBK(Reason);
        }
        return Reason;
    }

    public void setReason(String aReason)
    {
        Reason = aReason;
    }

    /**
     * 使用另外一个 LLCaseBackReasonSchema 对象给 Schema 赋值
     * @param: aLLCaseBackReasonSchema LLCaseBackReasonSchema
     **/
    public void setSchema(LLCaseBackReasonSchema aLLCaseBackReasonSchema)
    {
        this.CaseBackNo = aLLCaseBackReasonSchema.getCaseBackNo();
        this.CaseNo = aLLCaseBackReasonSchema.getCaseNo();
        this.Reason = aLLCaseBackReasonSchema.getReason();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CaseBackNo") == null)
            {
                this.CaseBackNo = null;
            }
            else
            {
                this.CaseBackNo = rs.getString("CaseBackNo").trim();
            }

            if (rs.getString("CaseNo") == null)
            {
                this.CaseNo = null;
            }
            else
            {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("Reason") == null)
            {
                this.Reason = null;
            }
            else
            {
                this.Reason = rs.getString("Reason").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseBackReasonSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLCaseBackReasonSchema getSchema()
    {
        LLCaseBackReasonSchema aLLCaseBackReasonSchema = new
                LLCaseBackReasonSchema();
        aLLCaseBackReasonSchema.setSchema(this);
        return aLLCaseBackReasonSchema;
    }

    public LLCaseBackReasonDB getDB()
    {
        LLCaseBackReasonDB aDBOper = new LLCaseBackReasonDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseBackReason描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CaseBackNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CaseNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Reason)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseBackReason>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CaseBackNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseBackReasonSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CaseBackNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseBackNo));
        }
        if (FCode.equals("CaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("Reason"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Reason));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CaseBackNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Reason);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CaseBackNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseBackNo = FValue.trim();
            }
            else
            {
                CaseBackNo = null;
            }
        }
        if (FCode.equals("CaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
            {
                CaseNo = null;
            }
        }
        if (FCode.equals("Reason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Reason = FValue.trim();
            }
            else
            {
                Reason = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLCaseBackReasonSchema other = (LLCaseBackReasonSchema) otherObject;
        return
                CaseBackNo.equals(other.getCaseBackNo())
                && CaseNo.equals(other.getCaseNo())
                && Reason.equals(other.getReason());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CaseBackNo"))
        {
            return 0;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return 1;
        }
        if (strFieldName.equals("Reason"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CaseBackNo";
                break;
            case 1:
                strFieldName = "CaseNo";
                break;
            case 2:
                strFieldName = "Reason";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CaseBackNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Reason"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
