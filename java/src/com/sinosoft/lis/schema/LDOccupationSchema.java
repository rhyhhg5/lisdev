/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDOccupationDB;

/*
 * <p>ClassName: LDOccupationSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-08-24
 */
public class LDOccupationSchema implements Schema, Cloneable
{
	// @Field
	/** 工种代码 */
	private String OccupationCode;
	/** 工种名称 */
	private String OccupationName;
	/** 职业类别 */
	private String OccupationType;
	/** 行业代码 */
	private String WorkType;
	/** 行业名称 */
	private String WorkName;
	/** 医疗险加费比例 */
	private int MedRate;
	/** 意外风险 */
	private String SuddRisk;
	/** 重疾风险 */
	private String DiseaRisk;
	/** 住院风险 */
	private String HosipRisk;
	/** 备注 */
	private String Remark;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 操作员 */
	private String Operator;

	public static final int FIELDNUM = 15;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDOccupationSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "OccupationCode";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LDOccupationSchema cloned = (LDOccupationSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getOccupationCode()
	{
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
            OccupationCode = aOccupationCode;
	}
	public String getOccupationName()
	{
		return OccupationName;
	}
	public void setOccupationName(String aOccupationName)
	{
            OccupationName = aOccupationName;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
            OccupationType = aOccupationType;
	}
	public String getWorkType()
	{
		return WorkType;
	}
	public void setWorkType(String aWorkType)
	{
            WorkType = aWorkType;
	}
	public String getWorkName()
	{
		return WorkName;
	}
	public void setWorkName(String aWorkName)
	{
            WorkName = aWorkName;
	}
	public int getMedRate()
	{
		return MedRate;
	}
	public void setMedRate(int aMedRate)
	{
            MedRate = aMedRate;
	}
	public void setMedRate(String aMedRate)
	{
		if (aMedRate != null && !aMedRate.equals(""))
		{
			Integer tInteger = new Integer(aMedRate);
			int i = tInteger.intValue();
			MedRate = i;
		}
	}

	public String getSuddRisk()
	{
		return SuddRisk;
	}
	public void setSuddRisk(String aSuddRisk)
	{
            SuddRisk = aSuddRisk;
	}
	public String getDiseaRisk()
	{
		return DiseaRisk;
	}
	public void setDiseaRisk(String aDiseaRisk)
	{
            DiseaRisk = aDiseaRisk;
	}
	public String getHosipRisk()
	{
		return HosipRisk;
	}
	public void setHosipRisk(String aHosipRisk)
	{
            HosipRisk = aHosipRisk;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
            Remark = aRemark;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}

	/**
	* 使用另外一个 LDOccupationSchema 对象给 Schema 赋值
	* @param: aLDOccupationSchema LDOccupationSchema
	**/
	public void setSchema(LDOccupationSchema aLDOccupationSchema)
	{
		this.OccupationCode = aLDOccupationSchema.getOccupationCode();
		this.OccupationName = aLDOccupationSchema.getOccupationName();
		this.OccupationType = aLDOccupationSchema.getOccupationType();
		this.WorkType = aLDOccupationSchema.getWorkType();
		this.WorkName = aLDOccupationSchema.getWorkName();
		this.MedRate = aLDOccupationSchema.getMedRate();
		this.SuddRisk = aLDOccupationSchema.getSuddRisk();
		this.DiseaRisk = aLDOccupationSchema.getDiseaRisk();
		this.HosipRisk = aLDOccupationSchema.getHosipRisk();
		this.Remark = aLDOccupationSchema.getRemark();
		this.MakeDate = fDate.getDate( aLDOccupationSchema.getMakeDate());
		this.MakeTime = aLDOccupationSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLDOccupationSchema.getModifyDate());
		this.ModifyTime = aLDOccupationSchema.getModifyTime();
		this.Operator = aLDOccupationSchema.getOperator();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("OccupationCode") == null )
				this.OccupationCode = null;
			else
				this.OccupationCode = rs.getString("OccupationCode").trim();

			if( rs.getString("OccupationName") == null )
				this.OccupationName = null;
			else
				this.OccupationName = rs.getString("OccupationName").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			if( rs.getString("WorkType") == null )
				this.WorkType = null;
			else
				this.WorkType = rs.getString("WorkType").trim();

			if( rs.getString("WorkName") == null )
				this.WorkName = null;
			else
				this.WorkName = rs.getString("WorkName").trim();

			this.MedRate = rs.getInt("MedRate");
			if( rs.getString("SuddRisk") == null )
				this.SuddRisk = null;
			else
				this.SuddRisk = rs.getString("SuddRisk").trim();

			if( rs.getString("DiseaRisk") == null )
				this.DiseaRisk = null;
			else
				this.DiseaRisk = rs.getString("DiseaRisk").trim();

			if( rs.getString("HosipRisk") == null )
				this.HosipRisk = null;
			else
				this.HosipRisk = rs.getString("HosipRisk").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDOccupation表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDOccupationSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDOccupationSchema getSchema()
	{
		LDOccupationSchema aLDOccupationSchema = new LDOccupationSchema();
		aLDOccupationSchema.setSchema(this);
		return aLDOccupationSchema;
	}

	public LDOccupationDB getDB()
	{
		LDOccupationDB aDBOper = new LDOccupationDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDOccupation描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OccupationName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(WorkType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(WorkName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MedRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SuddRisk)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DiseaRisk)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(HosipRisk)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDOccupation>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			OccupationName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			WorkType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			WorkName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			MedRate= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
			SuddRisk = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			DiseaRisk = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			HosipRisk = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDOccupationSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("OccupationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
		}
		if (FCode.equals("OccupationName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationName));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("WorkType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WorkType));
		}
		if (FCode.equals("WorkName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WorkName));
		}
		if (FCode.equals("MedRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MedRate));
		}
		if (FCode.equals("SuddRisk"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SuddRisk));
		}
		if (FCode.equals("DiseaRisk"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiseaRisk));
		}
		if (FCode.equals("HosipRisk"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HosipRisk));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(OccupationCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(OccupationName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(WorkType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(WorkName);
				break;
			case 5:
				strFieldValue = String.valueOf(MedRate);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(SuddRisk);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(DiseaRisk);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(HosipRisk);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("OccupationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationCode = FValue.trim();
			}
			else
				OccupationCode = null;
		}
		if (FCode.equalsIgnoreCase("OccupationName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationName = FValue.trim();
			}
			else
				OccupationName = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("WorkType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WorkType = FValue.trim();
			}
			else
				WorkType = null;
		}
		if (FCode.equalsIgnoreCase("WorkName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WorkName = FValue.trim();
			}
			else
				WorkName = null;
		}
		if (FCode.equalsIgnoreCase("MedRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MedRate = i;
			}
		}
		if (FCode.equalsIgnoreCase("SuddRisk"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SuddRisk = FValue.trim();
			}
			else
				SuddRisk = null;
		}
		if (FCode.equalsIgnoreCase("DiseaRisk"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiseaRisk = FValue.trim();
			}
			else
				DiseaRisk = null;
		}
		if (FCode.equalsIgnoreCase("HosipRisk"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HosipRisk = FValue.trim();
			}
			else
				HosipRisk = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDOccupationSchema other = (LDOccupationSchema)otherObject;
		return
			OccupationCode.equals(other.getOccupationCode())
			&& OccupationName.equals(other.getOccupationName())
			&& OccupationType.equals(other.getOccupationType())
			&& WorkType.equals(other.getWorkType())
			&& WorkName.equals(other.getWorkName())
			&& MedRate == other.getMedRate()
			&& SuddRisk.equals(other.getSuddRisk())
			&& DiseaRisk.equals(other.getDiseaRisk())
			&& HosipRisk.equals(other.getHosipRisk())
			&& Remark.equals(other.getRemark())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& Operator.equals(other.getOperator());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("OccupationCode") ) {
			return 0;
		}
		if( strFieldName.equals("OccupationName") ) {
			return 1;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 2;
		}
		if( strFieldName.equals("WorkType") ) {
			return 3;
		}
		if( strFieldName.equals("WorkName") ) {
			return 4;
		}
		if( strFieldName.equals("MedRate") ) {
			return 5;
		}
		if( strFieldName.equals("SuddRisk") ) {
			return 6;
		}
		if( strFieldName.equals("DiseaRisk") ) {
			return 7;
		}
		if( strFieldName.equals("HosipRisk") ) {
			return 8;
		}
		if( strFieldName.equals("Remark") ) {
			return 9;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 13;
		}
		if( strFieldName.equals("Operator") ) {
			return 14;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "OccupationCode";
				break;
			case 1:
				strFieldName = "OccupationName";
				break;
			case 2:
				strFieldName = "OccupationType";
				break;
			case 3:
				strFieldName = "WorkType";
				break;
			case 4:
				strFieldName = "WorkName";
				break;
			case 5:
				strFieldName = "MedRate";
				break;
			case 6:
				strFieldName = "SuddRisk";
				break;
			case 7:
				strFieldName = "DiseaRisk";
				break;
			case 8:
				strFieldName = "HosipRisk";
				break;
			case 9:
				strFieldName = "Remark";
				break;
			case 10:
				strFieldName = "MakeDate";
				break;
			case 11:
				strFieldName = "MakeTime";
				break;
			case 12:
				strFieldName = "ModifyDate";
				break;
			case 13:
				strFieldName = "ModifyTime";
				break;
			case 14:
				strFieldName = "Operator";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("OccupationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WorkType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WorkName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MedRate") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SuddRisk") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiseaRisk") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HosipRisk") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_INT;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
