/*
 * <p>ClassName: LDTaskRunLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 后台任务服务
 * @CreateDate：2004-12-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDTaskRunLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDTaskRunLogSchema implements Schema
{
    // @Field
    /** 序号 */
    private double SerialNo;
    /** 任务代码 */
    private String TaskCode;
    /** 任务计划编码 */
    private String TaskPlanCode;
    /** 执行日期 */
    private Date ExecuteDate;
    /** 执行时间 */
    private String ExecuteTime;
    /** 结束日期 */
    private Date FinishDate;
    /** 结束时间 */
    private String FinishTime;
    /** 执行次数 */
    private double ExecuteFrequence;
    /** 执行状态 */
    private String ExecuteState;
    /** 执行结果 */
    private String ExecuteResult;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDTaskRunLogSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public double getSerialNo()
    {
        return SerialNo;
    }

    public void setSerialNo(double aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        if (aSerialNo != null && !aSerialNo.equals(""))
        {
            Double tDouble = new Double(aSerialNo);
            double d = tDouble.doubleValue();
            SerialNo = d;
        }
    }

    public String getTaskCode()
    {
        if (TaskCode != null && !TaskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            TaskCode = StrTool.unicodeToGBK(TaskCode);
        }
        return TaskCode;
    }

    public void setTaskCode(String aTaskCode)
    {
        TaskCode = aTaskCode;
    }

    public String getTaskPlanCode()
    {
        if (TaskPlanCode != null && !TaskPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TaskPlanCode = StrTool.unicodeToGBK(TaskPlanCode);
        }
        return TaskPlanCode;
    }

    public void setTaskPlanCode(String aTaskPlanCode)
    {
        TaskPlanCode = aTaskPlanCode;
    }

    public String getExecuteDate()
    {
        if (ExecuteDate != null)
        {
            return fDate.getString(ExecuteDate);
        }
        else
        {
            return null;
        }
    }

    public void setExecuteDate(Date aExecuteDate)
    {
        ExecuteDate = aExecuteDate;
    }

    public void setExecuteDate(String aExecuteDate)
    {
        if (aExecuteDate != null && !aExecuteDate.equals(""))
        {
            ExecuteDate = fDate.getDate(aExecuteDate);
        }
        else
        {
            ExecuteDate = null;
        }
    }

    public String getExecuteTime()
    {
        if (ExecuteTime != null && !ExecuteTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ExecuteTime = StrTool.unicodeToGBK(ExecuteTime);
        }
        return ExecuteTime;
    }

    public void setExecuteTime(String aExecuteTime)
    {
        ExecuteTime = aExecuteTime;
    }

    public String getFinishDate()
    {
        if (FinishDate != null)
        {
            return fDate.getString(FinishDate);
        }
        else
        {
            return null;
        }
    }

    public void setFinishDate(Date aFinishDate)
    {
        FinishDate = aFinishDate;
    }

    public void setFinishDate(String aFinishDate)
    {
        if (aFinishDate != null && !aFinishDate.equals(""))
        {
            FinishDate = fDate.getDate(aFinishDate);
        }
        else
        {
            FinishDate = null;
        }
    }

    public String getFinishTime()
    {
        if (FinishTime != null && !FinishTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FinishTime = StrTool.unicodeToGBK(FinishTime);
        }
        return FinishTime;
    }

    public void setFinishTime(String aFinishTime)
    {
        FinishTime = aFinishTime;
    }

    public double getExecuteFrequence()
    {
        return ExecuteFrequence;
    }

    public void setExecuteFrequence(double aExecuteFrequence)
    {
        ExecuteFrequence = aExecuteFrequence;
    }

    public void setExecuteFrequence(String aExecuteFrequence)
    {
        if (aExecuteFrequence != null && !aExecuteFrequence.equals(""))
        {
            Double tDouble = new Double(aExecuteFrequence);
            double d = tDouble.doubleValue();
            ExecuteFrequence = d;
        }
    }

    public String getExecuteState()
    {
        if (ExecuteState != null && !ExecuteState.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ExecuteState = StrTool.unicodeToGBK(ExecuteState);
        }
        return ExecuteState;
    }

    public void setExecuteState(String aExecuteState)
    {
        ExecuteState = aExecuteState;
    }

    public String getExecuteResult()
    {
        if (ExecuteResult != null && !ExecuteResult.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ExecuteResult = StrTool.unicodeToGBK(ExecuteResult);
        }
        return ExecuteResult;
    }

    public void setExecuteResult(String aExecuteResult)
    {
        ExecuteResult = aExecuteResult;
    }

    /**
     * 使用另外一个 LDTaskRunLogSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDTaskRunLogSchema aLDTaskRunLogSchema)
    {
        this.SerialNo = aLDTaskRunLogSchema.getSerialNo();
        this.TaskCode = aLDTaskRunLogSchema.getTaskCode();
        this.TaskPlanCode = aLDTaskRunLogSchema.getTaskPlanCode();
        this.ExecuteDate = fDate.getDate(aLDTaskRunLogSchema.getExecuteDate());
        this.ExecuteTime = aLDTaskRunLogSchema.getExecuteTime();
        this.FinishDate = fDate.getDate(aLDTaskRunLogSchema.getFinishDate());
        this.FinishTime = aLDTaskRunLogSchema.getFinishTime();
        this.ExecuteFrequence = aLDTaskRunLogSchema.getExecuteFrequence();
        this.ExecuteState = aLDTaskRunLogSchema.getExecuteState();
        this.ExecuteResult = aLDTaskRunLogSchema.getExecuteResult();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            this.SerialNo = rs.getDouble("SerialNo");
            if (rs.getString("TaskCode") == null)
            {
                this.TaskCode = null;
            }
            else
            {
                this.TaskCode = rs.getString("TaskCode").trim();
            }

            if (rs.getString("TaskPlanCode") == null)
            {
                this.TaskPlanCode = null;
            }
            else
            {
                this.TaskPlanCode = rs.getString("TaskPlanCode").trim();
            }

            this.ExecuteDate = rs.getDate("ExecuteDate");
            if (rs.getString("ExecuteTime") == null)
            {
                this.ExecuteTime = null;
            }
            else
            {
                this.ExecuteTime = rs.getString("ExecuteTime").trim();
            }

            this.FinishDate = rs.getDate("FinishDate");
            if (rs.getString("FinishTime") == null)
            {
                this.FinishTime = null;
            }
            else
            {
                this.FinishTime = rs.getString("FinishTime").trim();
            }

            this.ExecuteFrequence = rs.getDouble("ExecuteFrequence");
            if (rs.getString("ExecuteState") == null)
            {
                this.ExecuteState = null;
            }
            else
            {
                this.ExecuteState = rs.getString("ExecuteState").trim();
            }

            if (rs.getString("ExecuteResult") == null)
            {
                this.ExecuteResult = null;
            }
            else
            {
                this.ExecuteResult = rs.getString("ExecuteResult").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTaskRunLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDTaskRunLogSchema getSchema()
    {
        LDTaskRunLogSchema aLDTaskRunLogSchema = new LDTaskRunLogSchema();
        aLDTaskRunLogSchema.setSchema(this);
        return aLDTaskRunLogSchema;
    }

    public LDTaskRunLogDB getDB()
    {
        LDTaskRunLogDB aDBOper = new LDTaskRunLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTaskRunLog描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = ChgData.chgData(SerialNo) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TaskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TaskPlanCode)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ExecuteDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ExecuteTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            FinishDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FinishTime)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ExecuteFrequence) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ExecuteState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ExecuteResult));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTaskRunLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SerialNo = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 1, SysConst.PACKAGESPILTER))).doubleValue();
            TaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            TaskPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            ExecuteDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            ExecuteTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            FinishDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            FinishTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            ExecuteFrequence = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            ExecuteState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            ExecuteResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                           SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTaskRunLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("TaskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TaskCode));
        }
        if (FCode.equals("TaskPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TaskPlanCode));
        }
        if (FCode.equals("ExecuteDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getExecuteDate()));
        }
        if (FCode.equals("ExecuteTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ExecuteTime));
        }
        if (FCode.equals("FinishDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getFinishDate()));
        }
        if (FCode.equals("FinishTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FinishTime));
        }
        if (FCode.equals("ExecuteFrequence"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ExecuteFrequence));
        }
        if (FCode.equals("ExecuteState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ExecuteState));
        }
        if (FCode.equals("ExecuteResult"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ExecuteResult));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TaskCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TaskPlanCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getExecuteDate()));
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ExecuteTime);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getFinishDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(FinishTime);
                break;
            case 7:
                strFieldValue = String.valueOf(ExecuteFrequence);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ExecuteState);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ExecuteResult);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SerialNo = d;
            }
        }
        if (FCode.equals("TaskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TaskCode = FValue.trim();
            }
            else
            {
                TaskCode = null;
            }
        }
        if (FCode.equals("TaskPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TaskPlanCode = FValue.trim();
            }
            else
            {
                TaskPlanCode = null;
            }
        }
        if (FCode.equals("ExecuteDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ExecuteDate = fDate.getDate(FValue);
            }
            else
            {
                ExecuteDate = null;
            }
        }
        if (FCode.equals("ExecuteTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ExecuteTime = FValue.trim();
            }
            else
            {
                ExecuteTime = null;
            }
        }
        if (FCode.equals("FinishDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FinishDate = fDate.getDate(FValue);
            }
            else
            {
                FinishDate = null;
            }
        }
        if (FCode.equals("FinishTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FinishTime = FValue.trim();
            }
            else
            {
                FinishTime = null;
            }
        }
        if (FCode.equals("ExecuteFrequence"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ExecuteFrequence = d;
            }
        }
        if (FCode.equals("ExecuteState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ExecuteState = FValue.trim();
            }
            else
            {
                ExecuteState = null;
            }
        }
        if (FCode.equals("ExecuteResult"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ExecuteResult = FValue.trim();
            }
            else
            {
                ExecuteResult = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDTaskRunLogSchema other = (LDTaskRunLogSchema) otherObject;
        return
                SerialNo == other.getSerialNo()
                && TaskCode.equals(other.getTaskCode())
                && TaskPlanCode.equals(other.getTaskPlanCode())
                && fDate.getString(ExecuteDate).equals(other.getExecuteDate())
                && ExecuteTime.equals(other.getExecuteTime())
                && fDate.getString(FinishDate).equals(other.getFinishDate())
                && FinishTime.equals(other.getFinishTime())
                && ExecuteFrequence == other.getExecuteFrequence()
                && ExecuteState.equals(other.getExecuteState())
                && ExecuteResult.equals(other.getExecuteResult());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("TaskCode"))
        {
            return 1;
        }
        if (strFieldName.equals("TaskPlanCode"))
        {
            return 2;
        }
        if (strFieldName.equals("ExecuteDate"))
        {
            return 3;
        }
        if (strFieldName.equals("ExecuteTime"))
        {
            return 4;
        }
        if (strFieldName.equals("FinishDate"))
        {
            return 5;
        }
        if (strFieldName.equals("FinishTime"))
        {
            return 6;
        }
        if (strFieldName.equals("ExecuteFrequence"))
        {
            return 7;
        }
        if (strFieldName.equals("ExecuteState"))
        {
            return 8;
        }
        if (strFieldName.equals("ExecuteResult"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "TaskCode";
                break;
            case 2:
                strFieldName = "TaskPlanCode";
                break;
            case 3:
                strFieldName = "ExecuteDate";
                break;
            case 4:
                strFieldName = "ExecuteTime";
                break;
            case 5:
                strFieldName = "FinishDate";
                break;
            case 6:
                strFieldName = "FinishTime";
                break;
            case 7:
                strFieldName = "ExecuteFrequence";
                break;
            case 8:
                strFieldName = "ExecuteState";
                break;
            case 9:
                strFieldName = "ExecuteResult";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("TaskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TaskPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExecuteDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ExecuteTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FinishDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FinishTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExecuteFrequence"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ExecuteState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExecuteResult"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
