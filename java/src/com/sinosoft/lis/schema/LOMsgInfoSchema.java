/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LOMsgInfoDB;

/*
 * <p>ClassName: LOMsgInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_4
 * @CreateDate：2006-01-13
 */
public class LOMsgInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 信息号码 */
	private String MsgNo;
	/** 发送部门 */
	private String Department;
	/** 信息类型 */
	private String MsgType;
	/** 发件人地址 */
	private String SendEmail;
	/** 发送密码 */
	private String PassWord;
	/** 信息主题 */
	private String MsgTopic;
	/** 信息内容 */
	private String MsgContent;
	/** 预期发送日期 */
	private Date PreSendDate;
	/** 预期发送时间 */
	private String PreSendTime;
	/** 定时发送类型 */
	private String SendType;
	/** 是否已发送 */
	private String MsgSend;
	/** 发送日期 */
	private Date SendDate;
	/** 发送时间 */
	private String SendTime;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 信息内容类型标记 */
	private String ContentFlag;

	public static final int FIELDNUM = 20;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LOMsgInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "MsgNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LOMsgInfoSchema cloned = (LOMsgInfoSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMsgNo()
	{
		return MsgNo;
	}
	public void setMsgNo(String aMsgNo)
	{
            MsgNo = aMsgNo;
	}
	public String getDepartment()
	{
		return Department;
	}
	public void setDepartment(String aDepartment)
	{
            Department = aDepartment;
	}
	public String getMsgType()
	{
		return MsgType;
	}
	public void setMsgType(String aMsgType)
	{
            MsgType = aMsgType;
	}
	public String getSendEmail()
	{
		return SendEmail;
	}
	public void setSendEmail(String aSendEmail)
	{
            SendEmail = aSendEmail;
	}
	public String getPassWord()
	{
		return PassWord;
	}
	public void setPassWord(String aPassWord)
	{
            PassWord = aPassWord;
	}
	public String getMsgTopic()
	{
		return MsgTopic;
	}
	public void setMsgTopic(String aMsgTopic)
	{
            MsgTopic = aMsgTopic;
	}
	public String getMsgContent()
	{
		return MsgContent;
	}
	public void setMsgContent(String aMsgContent)
	{
            MsgContent = aMsgContent;
	}
	public String getPreSendDate()
	{
		if( PreSendDate != null )
			return fDate.getString(PreSendDate);
		else
			return null;
	}
	public void setPreSendDate(Date aPreSendDate)
	{
            PreSendDate = aPreSendDate;
	}
	public void setPreSendDate(String aPreSendDate)
	{
		if (aPreSendDate != null && !aPreSendDate.equals("") )
		{
			PreSendDate = fDate.getDate( aPreSendDate );
		}
		else
			PreSendDate = null;
	}

	public String getPreSendTime()
	{
		return PreSendTime;
	}
	public void setPreSendTime(String aPreSendTime)
	{
            PreSendTime = aPreSendTime;
	}
	public String getSendType()
	{
		return SendType;
	}
	public void setSendType(String aSendType)
	{
            SendType = aSendType;
	}
	public String getMsgSend()
	{
		return MsgSend;
	}
	public void setMsgSend(String aMsgSend)
	{
            MsgSend = aMsgSend;
	}
	public String getSendDate()
	{
		if( SendDate != null )
			return fDate.getString(SendDate);
		else
			return null;
	}
	public void setSendDate(Date aSendDate)
	{
            SendDate = aSendDate;
	}
	public void setSendDate(String aSendDate)
	{
		if (aSendDate != null && !aSendDate.equals("") )
		{
			SendDate = fDate.getDate( aSendDate );
		}
		else
			SendDate = null;
	}

	public String getSendTime()
	{
		return SendTime;
	}
	public void setSendTime(String aSendTime)
	{
            SendTime = aSendTime;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getContentFlag()
	{
		return ContentFlag;
	}
	public void setContentFlag(String aContentFlag)
	{
            ContentFlag = aContentFlag;
	}

	/**
	* 使用另外一个 LOMsgInfoSchema 对象给 Schema 赋值
	* @param: aLOMsgInfoSchema LOMsgInfoSchema
	**/
	public void setSchema(LOMsgInfoSchema aLOMsgInfoSchema)
	{
		this.MsgNo = aLOMsgInfoSchema.getMsgNo();
		this.Department = aLOMsgInfoSchema.getDepartment();
		this.MsgType = aLOMsgInfoSchema.getMsgType();
		this.SendEmail = aLOMsgInfoSchema.getSendEmail();
		this.PassWord = aLOMsgInfoSchema.getPassWord();
		this.MsgTopic = aLOMsgInfoSchema.getMsgTopic();
		this.MsgContent = aLOMsgInfoSchema.getMsgContent();
		this.PreSendDate = fDate.getDate( aLOMsgInfoSchema.getPreSendDate());
		this.PreSendTime = aLOMsgInfoSchema.getPreSendTime();
		this.SendType = aLOMsgInfoSchema.getSendType();
		this.MsgSend = aLOMsgInfoSchema.getMsgSend();
		this.SendDate = fDate.getDate( aLOMsgInfoSchema.getSendDate());
		this.SendTime = aLOMsgInfoSchema.getSendTime();
		this.ManageCom = aLOMsgInfoSchema.getManageCom();
		this.Operator = aLOMsgInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aLOMsgInfoSchema.getMakeDate());
		this.MakeTime = aLOMsgInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLOMsgInfoSchema.getModifyDate());
		this.ModifyTime = aLOMsgInfoSchema.getModifyTime();
		this.ContentFlag = aLOMsgInfoSchema.getContentFlag();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MsgNo") == null )
				this.MsgNo = null;
			else
				this.MsgNo = rs.getString("MsgNo").trim();

			if( rs.getString("Department") == null )
				this.Department = null;
			else
				this.Department = rs.getString("Department").trim();

			if( rs.getString("MsgType") == null )
				this.MsgType = null;
			else
				this.MsgType = rs.getString("MsgType").trim();

			if( rs.getString("SendEmail") == null )
				this.SendEmail = null;
			else
				this.SendEmail = rs.getString("SendEmail").trim();

			if( rs.getString("PassWord") == null )
				this.PassWord = null;
			else
				this.PassWord = rs.getString("PassWord").trim();

			if( rs.getString("MsgTopic") == null )
				this.MsgTopic = null;
			else
				this.MsgTopic = rs.getString("MsgTopic").trim();

			if( rs.getString("MsgContent") == null )
				this.MsgContent = null;
			else
				this.MsgContent = rs.getString("MsgContent").trim();

			this.PreSendDate = rs.getDate("PreSendDate");
			if( rs.getString("PreSendTime") == null )
				this.PreSendTime = null;
			else
				this.PreSendTime = rs.getString("PreSendTime").trim();

			if( rs.getString("SendType") == null )
				this.SendType = null;
			else
				this.SendType = rs.getString("SendType").trim();

			if( rs.getString("MsgSend") == null )
				this.MsgSend = null;
			else
				this.MsgSend = rs.getString("MsgSend").trim();

			this.SendDate = rs.getDate("SendDate");
			if( rs.getString("SendTime") == null )
				this.SendTime = null;
			else
				this.SendTime = rs.getString("SendTime").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("ContentFlag") == null )
				this.ContentFlag = null;
			else
				this.ContentFlag = rs.getString("ContentFlag").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LOMsgInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMsgInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LOMsgInfoSchema getSchema()
	{
		LOMsgInfoSchema aLOMsgInfoSchema = new LOMsgInfoSchema();
		aLOMsgInfoSchema.setSchema(this);
		return aLOMsgInfoSchema;
	}

	public LOMsgInfoDB getDB()
	{
		LOMsgInfoDB aDBOper = new LOMsgInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMsgInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(MsgNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Department)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MsgType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SendEmail)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PassWord)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MsgTopic)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MsgContent)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( PreSendDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PreSendTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SendType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MsgSend)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( SendDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SendTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContentFlag));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMsgInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MsgNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Department = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			MsgType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SendEmail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PassWord = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			MsgTopic = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			MsgContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			PreSendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			PreSendTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			SendType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MsgSend = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			SendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			SendTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ContentFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMsgInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MsgNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgNo));
		}
		if (FCode.equals("Department"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Department));
		}
		if (FCode.equals("MsgType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgType));
		}
		if (FCode.equals("SendEmail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendEmail));
		}
		if (FCode.equals("PassWord"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PassWord));
		}
		if (FCode.equals("MsgTopic"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgTopic));
		}
		if (FCode.equals("MsgContent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgContent));
		}
		if (FCode.equals("PreSendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPreSendDate()));
		}
		if (FCode.equals("PreSendTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PreSendTime));
		}
		if (FCode.equals("SendType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendType));
		}
		if (FCode.equals("MsgSend"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgSend));
		}
		if (FCode.equals("SendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
		}
		if (FCode.equals("SendTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendTime));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ContentFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContentFlag));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MsgNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Department);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(MsgType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SendEmail);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(PassWord);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(MsgTopic);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(MsgContent);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPreSendDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(PreSendTime);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(SendType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(MsgSend);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(SendTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ContentFlag);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MsgNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgNo = FValue.trim();
			}
			else
				MsgNo = null;
		}
		if (FCode.equalsIgnoreCase("Department"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Department = FValue.trim();
			}
			else
				Department = null;
		}
		if (FCode.equalsIgnoreCase("MsgType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgType = FValue.trim();
			}
			else
				MsgType = null;
		}
		if (FCode.equalsIgnoreCase("SendEmail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendEmail = FValue.trim();
			}
			else
				SendEmail = null;
		}
		if (FCode.equalsIgnoreCase("PassWord"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PassWord = FValue.trim();
			}
			else
				PassWord = null;
		}
		if (FCode.equalsIgnoreCase("MsgTopic"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgTopic = FValue.trim();
			}
			else
				MsgTopic = null;
		}
		if (FCode.equalsIgnoreCase("MsgContent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgContent = FValue.trim();
			}
			else
				MsgContent = null;
		}
		if (FCode.equalsIgnoreCase("PreSendDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PreSendDate = fDate.getDate( FValue );
			}
			else
				PreSendDate = null;
		}
		if (FCode.equalsIgnoreCase("PreSendTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PreSendTime = FValue.trim();
			}
			else
				PreSendTime = null;
		}
		if (FCode.equalsIgnoreCase("SendType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendType = FValue.trim();
			}
			else
				SendType = null;
		}
		if (FCode.equalsIgnoreCase("MsgSend"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgSend = FValue.trim();
			}
			else
				MsgSend = null;
		}
		if (FCode.equalsIgnoreCase("SendDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SendDate = fDate.getDate( FValue );
			}
			else
				SendDate = null;
		}
		if (FCode.equalsIgnoreCase("SendTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendTime = FValue.trim();
			}
			else
				SendTime = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ContentFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContentFlag = FValue.trim();
			}
			else
				ContentFlag = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LOMsgInfoSchema other = (LOMsgInfoSchema)otherObject;
		return
			MsgNo.equals(other.getMsgNo())
			&& Department.equals(other.getDepartment())
			&& MsgType.equals(other.getMsgType())
			&& SendEmail.equals(other.getSendEmail())
			&& PassWord.equals(other.getPassWord())
			&& MsgTopic.equals(other.getMsgTopic())
			&& MsgContent.equals(other.getMsgContent())
			&& fDate.getString(PreSendDate).equals(other.getPreSendDate())
			&& PreSendTime.equals(other.getPreSendTime())
			&& SendType.equals(other.getSendType())
			&& MsgSend.equals(other.getMsgSend())
			&& fDate.getString(SendDate).equals(other.getSendDate())
			&& SendTime.equals(other.getSendTime())
			&& ManageCom.equals(other.getManageCom())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& ContentFlag.equals(other.getContentFlag());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MsgNo") ) {
			return 0;
		}
		if( strFieldName.equals("Department") ) {
			return 1;
		}
		if( strFieldName.equals("MsgType") ) {
			return 2;
		}
		if( strFieldName.equals("SendEmail") ) {
			return 3;
		}
		if( strFieldName.equals("PassWord") ) {
			return 4;
		}
		if( strFieldName.equals("MsgTopic") ) {
			return 5;
		}
		if( strFieldName.equals("MsgContent") ) {
			return 6;
		}
		if( strFieldName.equals("PreSendDate") ) {
			return 7;
		}
		if( strFieldName.equals("PreSendTime") ) {
			return 8;
		}
		if( strFieldName.equals("SendType") ) {
			return 9;
		}
		if( strFieldName.equals("MsgSend") ) {
			return 10;
		}
		if( strFieldName.equals("SendDate") ) {
			return 11;
		}
		if( strFieldName.equals("SendTime") ) {
			return 12;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 13;
		}
		if( strFieldName.equals("Operator") ) {
			return 14;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 15;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 18;
		}
		if( strFieldName.equals("ContentFlag") ) {
			return 19;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MsgNo";
				break;
			case 1:
				strFieldName = "Department";
				break;
			case 2:
				strFieldName = "MsgType";
				break;
			case 3:
				strFieldName = "SendEmail";
				break;
			case 4:
				strFieldName = "PassWord";
				break;
			case 5:
				strFieldName = "MsgTopic";
				break;
			case 6:
				strFieldName = "MsgContent";
				break;
			case 7:
				strFieldName = "PreSendDate";
				break;
			case 8:
				strFieldName = "PreSendTime";
				break;
			case 9:
				strFieldName = "SendType";
				break;
			case 10:
				strFieldName = "MsgSend";
				break;
			case 11:
				strFieldName = "SendDate";
				break;
			case 12:
				strFieldName = "SendTime";
				break;
			case 13:
				strFieldName = "ManageCom";
				break;
			case 14:
				strFieldName = "Operator";
				break;
			case 15:
				strFieldName = "MakeDate";
				break;
			case 16:
				strFieldName = "MakeTime";
				break;
			case 17:
				strFieldName = "ModifyDate";
				break;
			case 18:
				strFieldName = "ModifyTime";
				break;
			case 19:
				strFieldName = "ContentFlag";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MsgNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Department") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MsgType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendEmail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PassWord") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MsgTopic") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MsgContent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PreSendDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PreSendTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MsgSend") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SendTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContentFlag") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
