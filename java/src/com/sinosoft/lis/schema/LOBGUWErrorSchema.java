/*
 * <p>ClassName: LOBGUWErrorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 核心表变更
 * @CreateDate：2004-12-06
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBGUWErrorDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LOBGUWErrorSchema implements Schema
{
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 总单投保单号码 */
    private String ProposalGrpContNo;
    /** 集体保单险种号码 */
    private String GrpPolNo;
    /** 集体投保单险种号码 */
    private String GrpProposalNo;
    /** 流水号 */
    private String SerialNo;
    /** 核保次数 */
    private int UWNo;
    /** 单位编码 */
    private String GrpNo;
    /** 单位名称 */
    private String Name;
    /** 管理机构 */
    private String ManageCom;
    /** 核保规则编码 */
    private String UWRuleCode;
    /** 核保出错信息 */
    private String UWError;
    /** 当前值 */
    private String CurrValue;
    /** 核保可通过标记（分保） */
    private String UWPassFlag;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 核保级别 */
    private String UWGrade;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBGUWErrorSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "GrpProposalNo";
        pk[1] = "SerialNo";
        pk[2] = "UWNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getProposalGrpContNo()
    {
        if (ProposalGrpContNo != null && !ProposalGrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProposalGrpContNo = StrTool.unicodeToGBK(ProposalGrpContNo);
        }
        return ProposalGrpContNo;
    }

    public void setProposalGrpContNo(String aProposalGrpContNo)
    {
        ProposalGrpContNo = aProposalGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getGrpProposalNo()
    {
        if (GrpProposalNo != null && !GrpProposalNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpProposalNo = StrTool.unicodeToGBK(GrpProposalNo);
        }
        return GrpProposalNo;
    }

    public void setGrpProposalNo(String aGrpProposalNo)
    {
        GrpProposalNo = aGrpProposalNo;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public int getUWNo()
    {
        return UWNo;
    }

    public void setUWNo(int aUWNo)
    {
        UWNo = aUWNo;
    }

    public void setUWNo(String aUWNo)
    {
        if (aUWNo != null && !aUWNo.equals(""))
        {
            Integer tInteger = new Integer(aUWNo);
            int i = tInteger.intValue();
            UWNo = i;
        }
    }

    public String getGrpNo()
    {
        if (GrpNo != null && !GrpNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpNo = StrTool.unicodeToGBK(GrpNo);
        }
        return GrpNo;
    }

    public void setGrpNo(String aGrpNo)
    {
        GrpNo = aGrpNo;
    }

    public String getName()
    {
        if (Name != null && !Name.equals("") && SysConst.CHANGECHARSET == true)
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getUWRuleCode()
    {
        if (UWRuleCode != null && !UWRuleCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UWRuleCode = StrTool.unicodeToGBK(UWRuleCode);
        }
        return UWRuleCode;
    }

    public void setUWRuleCode(String aUWRuleCode)
    {
        UWRuleCode = aUWRuleCode;
    }

    public String getUWError()
    {
        if (UWError != null && !UWError.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWError = StrTool.unicodeToGBK(UWError);
        }
        return UWError;
    }

    public void setUWError(String aUWError)
    {
        UWError = aUWError;
    }

    public String getCurrValue()
    {
        if (CurrValue != null && !CurrValue.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CurrValue = StrTool.unicodeToGBK(CurrValue);
        }
        return CurrValue;
    }

    public void setCurrValue(String aCurrValue)
    {
        CurrValue = aCurrValue;
    }

    public String getUWPassFlag()
    {
        if (UWPassFlag != null && !UWPassFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UWPassFlag = StrTool.unicodeToGBK(UWPassFlag);
        }
        return UWPassFlag;
    }

    public void setUWPassFlag(String aUWPassFlag)
    {
        UWPassFlag = aUWPassFlag;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getUWGrade()
    {
        if (UWGrade != null && !UWGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWGrade = StrTool.unicodeToGBK(UWGrade);
        }
        return UWGrade;
    }

    public void setUWGrade(String aUWGrade)
    {
        UWGrade = aUWGrade;
    }

    /**
     * 使用另外一个 LOBGUWErrorSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LOBGUWErrorSchema aLOBGUWErrorSchema)
    {
        this.GrpContNo = aLOBGUWErrorSchema.getGrpContNo();
        this.ProposalGrpContNo = aLOBGUWErrorSchema.getProposalGrpContNo();
        this.GrpPolNo = aLOBGUWErrorSchema.getGrpPolNo();
        this.GrpProposalNo = aLOBGUWErrorSchema.getGrpProposalNo();
        this.SerialNo = aLOBGUWErrorSchema.getSerialNo();
        this.UWNo = aLOBGUWErrorSchema.getUWNo();
        this.GrpNo = aLOBGUWErrorSchema.getGrpNo();
        this.Name = aLOBGUWErrorSchema.getName();
        this.ManageCom = aLOBGUWErrorSchema.getManageCom();
        this.UWRuleCode = aLOBGUWErrorSchema.getUWRuleCode();
        this.UWError = aLOBGUWErrorSchema.getUWError();
        this.CurrValue = aLOBGUWErrorSchema.getCurrValue();
        this.UWPassFlag = aLOBGUWErrorSchema.getUWPassFlag();
        this.ModifyDate = fDate.getDate(aLOBGUWErrorSchema.getModifyDate());
        this.ModifyTime = aLOBGUWErrorSchema.getModifyTime();
        this.UWGrade = aLOBGUWErrorSchema.getUWGrade();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ProposalGrpContNo") == null)
            {
                this.ProposalGrpContNo = null;
            }
            else
            {
                this.ProposalGrpContNo = rs.getString("ProposalGrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("GrpProposalNo") == null)
            {
                this.GrpProposalNo = null;
            }
            else
            {
                this.GrpProposalNo = rs.getString("GrpProposalNo").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            this.UWNo = rs.getInt("UWNo");
            if (rs.getString("GrpNo") == null)
            {
                this.GrpNo = null;
            }
            else
            {
                this.GrpNo = rs.getString("GrpNo").trim();
            }

            if (rs.getString("Name") == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString("Name").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("UWRuleCode") == null)
            {
                this.UWRuleCode = null;
            }
            else
            {
                this.UWRuleCode = rs.getString("UWRuleCode").trim();
            }

            if (rs.getString("UWError") == null)
            {
                this.UWError = null;
            }
            else
            {
                this.UWError = rs.getString("UWError").trim();
            }

            if (rs.getString("CurrValue") == null)
            {
                this.CurrValue = null;
            }
            else
            {
                this.CurrValue = rs.getString("CurrValue").trim();
            }

            if (rs.getString("UWPassFlag") == null)
            {
                this.UWPassFlag = null;
            }
            else
            {
                this.UWPassFlag = rs.getString("UWPassFlag").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("UWGrade") == null)
            {
                this.UWGrade = null;
            }
            else
            {
                this.UWGrade = rs.getString("UWGrade").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBGUWErrorSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOBGUWErrorSchema getSchema()
    {
        LOBGUWErrorSchema aLOBGUWErrorSchema = new LOBGUWErrorSchema();
        aLOBGUWErrorSchema.setSchema(this);
        return aLOBGUWErrorSchema;
    }

    public LOBGUWErrorDB getDB()
    {
        LOBGUWErrorDB aDBOper = new LOBGUWErrorDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBGUWError描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProposalGrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpProposalNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(UWNo) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWRuleCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWError)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CurrValue)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWPassFlag)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWGrade));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBGUWError>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ProposalGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               2, SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            GrpProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            UWNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    6, SysConst.PACKAGESPILTER))).intValue();
            GrpNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                  SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            UWRuleCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            UWError = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            CurrValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            UWPassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            UWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBGUWErrorSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("ProposalGrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    ProposalGrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("GrpProposalNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpProposalNo));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("UWNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWNo));
        }
        if (FCode.equals("GrpNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpNo));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Name));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("UWRuleCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWRuleCode));
        }
        if (FCode.equals("UWError"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWError));
        }
        if (FCode.equals("CurrValue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CurrValue));
        }
        if (FCode.equals("UWPassFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWPassFlag));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("UWGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWGrade));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ProposalGrpContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpProposalNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 5:
                strFieldValue = String.valueOf(UWNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(GrpNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(UWRuleCode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(UWError);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(CurrValue);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(UWPassFlag);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(UWGrade);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ProposalGrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalGrpContNo = FValue.trim();
            }
            else
            {
                ProposalGrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("GrpProposalNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpProposalNo = FValue.trim();
            }
            else
            {
                GrpProposalNo = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("UWNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                UWNo = i;
            }
        }
        if (FCode.equals("GrpNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpNo = FValue.trim();
            }
            else
            {
                GrpNo = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("UWRuleCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWRuleCode = FValue.trim();
            }
            else
            {
                UWRuleCode = null;
            }
        }
        if (FCode.equals("UWError"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWError = FValue.trim();
            }
            else
            {
                UWError = null;
            }
        }
        if (FCode.equals("CurrValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CurrValue = FValue.trim();
            }
            else
            {
                CurrValue = null;
            }
        }
        if (FCode.equals("UWPassFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWPassFlag = FValue.trim();
            }
            else
            {
                UWPassFlag = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("UWGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
            {
                UWGrade = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOBGUWErrorSchema other = (LOBGUWErrorSchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && ProposalGrpContNo.equals(other.getProposalGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && GrpProposalNo.equals(other.getGrpProposalNo())
                && SerialNo.equals(other.getSerialNo())
                && UWNo == other.getUWNo()
                && GrpNo.equals(other.getGrpNo())
                && Name.equals(other.getName())
                && ManageCom.equals(other.getManageCom())
                && UWRuleCode.equals(other.getUWRuleCode())
                && UWError.equals(other.getUWError())
                && CurrValue.equals(other.getCurrValue())
                && UWPassFlag.equals(other.getUWPassFlag())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && UWGrade.equals(other.getUWGrade());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ProposalGrpContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 2;
        }
        if (strFieldName.equals("GrpProposalNo"))
        {
            return 3;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 4;
        }
        if (strFieldName.equals("UWNo"))
        {
            return 5;
        }
        if (strFieldName.equals("GrpNo"))
        {
            return 6;
        }
        if (strFieldName.equals("Name"))
        {
            return 7;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 8;
        }
        if (strFieldName.equals("UWRuleCode"))
        {
            return 9;
        }
        if (strFieldName.equals("UWError"))
        {
            return 10;
        }
        if (strFieldName.equals("CurrValue"))
        {
            return 11;
        }
        if (strFieldName.equals("UWPassFlag"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 14;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GrpContNo";
                break;
            case 1:
                strFieldName = "ProposalGrpContNo";
                break;
            case 2:
                strFieldName = "GrpPolNo";
                break;
            case 3:
                strFieldName = "GrpProposalNo";
                break;
            case 4:
                strFieldName = "SerialNo";
                break;
            case 5:
                strFieldName = "UWNo";
                break;
            case 6:
                strFieldName = "GrpNo";
                break;
            case 7:
                strFieldName = "Name";
                break;
            case 8:
                strFieldName = "ManageCom";
                break;
            case 9:
                strFieldName = "UWRuleCode";
                break;
            case 10:
                strFieldName = "UWError";
                break;
            case 11:
                strFieldName = "CurrValue";
                break;
            case 12:
                strFieldName = "UWPassFlag";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            case 15:
                strFieldName = "UWGrade";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalGrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpProposalNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWNo"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GrpNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWRuleCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWError"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CurrValue"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWPassFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWGrade"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
