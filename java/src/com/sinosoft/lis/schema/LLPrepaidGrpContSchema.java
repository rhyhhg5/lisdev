/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLPrepaidGrpContDB;

/*
 * <p>ClassName: LLPrepaidGrpContSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新系统模型
 * @CreateDate：2010-12-08
 */
public class LLPrepaidGrpContSchema implements Schema, Cloneable
{
	// @Field
	/** 团体保单号 */
	private String GrpContNo;
	/** 账户创建日期 */
	private Date FoundDate;
	/** 账户创建时间 */
	private String FoundTime;
	/** 结算日期 */
	private Date BalaDate;
	/** 结算时间 */
	private String BalaTime;
	/** 预付赔款余额 */
	private double PrepaidBala;
	/** 预付赔款申请金额 */
	private double ApplyAmount;
	/** 预付赔款回收金额 */
	private double RegainAmount;
	/** 预付赔款回销金额 */
	private double SumPay;
	/** 申请人 */
	private String Register;
	/** 状态 */
	private String State;
	/** 撤销人 */
	private String Canceler;
	/** 撤销日期 */
	private Date CancelDate;
	/** 撤销时间 */
	private String CancelTime;
	/** 管理机构 */
	private String ManageCom;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 20;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLPrepaidGrpContSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "GrpContNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLPrepaidGrpContSchema cloned = (LLPrepaidGrpContSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getFoundDate()
	{
		if( FoundDate != null )
			return fDate.getString(FoundDate);
		else
			return null;
	}
	public void setFoundDate(Date aFoundDate)
	{
		FoundDate = aFoundDate;
	}
	public void setFoundDate(String aFoundDate)
	{
		if (aFoundDate != null && !aFoundDate.equals("") )
		{
			FoundDate = fDate.getDate( aFoundDate );
		}
		else
			FoundDate = null;
	}

	public String getFoundTime()
	{
		return FoundTime;
	}
	public void setFoundTime(String aFoundTime)
	{
		FoundTime = aFoundTime;
	}
	public String getBalaDate()
	{
		if( BalaDate != null )
			return fDate.getString(BalaDate);
		else
			return null;
	}
	public void setBalaDate(Date aBalaDate)
	{
		BalaDate = aBalaDate;
	}
	public void setBalaDate(String aBalaDate)
	{
		if (aBalaDate != null && !aBalaDate.equals("") )
		{
			BalaDate = fDate.getDate( aBalaDate );
		}
		else
			BalaDate = null;
	}

	public String getBalaTime()
	{
		return BalaTime;
	}
	public void setBalaTime(String aBalaTime)
	{
		BalaTime = aBalaTime;
	}
	public double getPrepaidBala()
	{
		return PrepaidBala;
	}
	public void setPrepaidBala(double aPrepaidBala)
	{
		PrepaidBala = Arith.round(aPrepaidBala,2);
	}
	public void setPrepaidBala(String aPrepaidBala)
	{
		if (aPrepaidBala != null && !aPrepaidBala.equals(""))
		{
			Double tDouble = new Double(aPrepaidBala);
			double d = tDouble.doubleValue();
                PrepaidBala = Arith.round(d,2);
		}
	}

	public double getApplyAmount()
	{
		return ApplyAmount;
	}
	public void setApplyAmount(double aApplyAmount)
	{
		ApplyAmount = Arith.round(aApplyAmount,2);
	}
	public void setApplyAmount(String aApplyAmount)
	{
		if (aApplyAmount != null && !aApplyAmount.equals(""))
		{
			Double tDouble = new Double(aApplyAmount);
			double d = tDouble.doubleValue();
                ApplyAmount = Arith.round(d,2);
		}
	}

	public double getRegainAmount()
	{
		return RegainAmount;
	}
	public void setRegainAmount(double aRegainAmount)
	{
		RegainAmount = Arith.round(aRegainAmount,2);
	}
	public void setRegainAmount(String aRegainAmount)
	{
		if (aRegainAmount != null && !aRegainAmount.equals(""))
		{
			Double tDouble = new Double(aRegainAmount);
			double d = tDouble.doubleValue();
                RegainAmount = Arith.round(d,2);
		}
	}

	public double getSumPay()
	{
		return SumPay;
	}
	public void setSumPay(double aSumPay)
	{
		SumPay = Arith.round(aSumPay,2);
	}
	public void setSumPay(String aSumPay)
	{
		if (aSumPay != null && !aSumPay.equals(""))
		{
			Double tDouble = new Double(aSumPay);
			double d = tDouble.doubleValue();
                SumPay = Arith.round(d,2);
		}
	}

	public String getRegister()
	{
		return Register;
	}
	public void setRegister(String aRegister)
	{
		Register = aRegister;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getCanceler()
	{
		return Canceler;
	}
	public void setCanceler(String aCanceler)
	{
		Canceler = aCanceler;
	}
	public String getCancelDate()
	{
		if( CancelDate != null )
			return fDate.getString(CancelDate);
		else
			return null;
	}
	public void setCancelDate(Date aCancelDate)
	{
		CancelDate = aCancelDate;
	}
	public void setCancelDate(String aCancelDate)
	{
		if (aCancelDate != null && !aCancelDate.equals("") )
		{
			CancelDate = fDate.getDate( aCancelDate );
		}
		else
			CancelDate = null;
	}

	public String getCancelTime()
	{
		return CancelTime;
	}
	public void setCancelTime(String aCancelTime)
	{
		CancelTime = aCancelTime;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLPrepaidGrpContSchema 对象给 Schema 赋值
	* @param: aLLPrepaidGrpContSchema LLPrepaidGrpContSchema
	**/
	public void setSchema(LLPrepaidGrpContSchema aLLPrepaidGrpContSchema)
	{
		this.GrpContNo = aLLPrepaidGrpContSchema.getGrpContNo();
		this.FoundDate = fDate.getDate( aLLPrepaidGrpContSchema.getFoundDate());
		this.FoundTime = aLLPrepaidGrpContSchema.getFoundTime();
		this.BalaDate = fDate.getDate( aLLPrepaidGrpContSchema.getBalaDate());
		this.BalaTime = aLLPrepaidGrpContSchema.getBalaTime();
		this.PrepaidBala = aLLPrepaidGrpContSchema.getPrepaidBala();
		this.ApplyAmount = aLLPrepaidGrpContSchema.getApplyAmount();
		this.RegainAmount = aLLPrepaidGrpContSchema.getRegainAmount();
		this.SumPay = aLLPrepaidGrpContSchema.getSumPay();
		this.Register = aLLPrepaidGrpContSchema.getRegister();
		this.State = aLLPrepaidGrpContSchema.getState();
		this.Canceler = aLLPrepaidGrpContSchema.getCanceler();
		this.CancelDate = fDate.getDate( aLLPrepaidGrpContSchema.getCancelDate());
		this.CancelTime = aLLPrepaidGrpContSchema.getCancelTime();
		this.ManageCom = aLLPrepaidGrpContSchema.getManageCom();
		this.Operator = aLLPrepaidGrpContSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLPrepaidGrpContSchema.getMakeDate());
		this.MakeTime = aLLPrepaidGrpContSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLPrepaidGrpContSchema.getModifyDate());
		this.ModifyTime = aLLPrepaidGrpContSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			this.FoundDate = rs.getDate("FoundDate");
			if( rs.getString("FoundTime") == null )
				this.FoundTime = null;
			else
				this.FoundTime = rs.getString("FoundTime").trim();

			this.BalaDate = rs.getDate("BalaDate");
			if( rs.getString("BalaTime") == null )
				this.BalaTime = null;
			else
				this.BalaTime = rs.getString("BalaTime").trim();

			this.PrepaidBala = rs.getDouble("PrepaidBala");
			this.ApplyAmount = rs.getDouble("ApplyAmount");
			this.RegainAmount = rs.getDouble("RegainAmount");
			this.SumPay = rs.getDouble("SumPay");
			if( rs.getString("Register") == null )
				this.Register = null;
			else
				this.Register = rs.getString("Register").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Canceler") == null )
				this.Canceler = null;
			else
				this.Canceler = rs.getString("Canceler").trim();

			this.CancelDate = rs.getDate("CancelDate");
			if( rs.getString("CancelTime") == null )
				this.CancelTime = null;
			else
				this.CancelTime = rs.getString("CancelTime").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLPrepaidGrpCont表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLPrepaidGrpContSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLPrepaidGrpContSchema getSchema()
	{
		LLPrepaidGrpContSchema aLLPrepaidGrpContSchema = new LLPrepaidGrpContSchema();
		aLLPrepaidGrpContSchema.setSchema(this);
		return aLLPrepaidGrpContSchema;
	}

	public LLPrepaidGrpContDB getDB()
	{
		LLPrepaidGrpContDB aDBOper = new LLPrepaidGrpContDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLPrepaidGrpCont描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FoundDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FoundTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( BalaDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BalaTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PrepaidBala));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ApplyAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RegainAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Register)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Canceler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CancelDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CancelTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLPrepaidGrpCont>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			FoundDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,SysConst.PACKAGESPILTER));
			FoundTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BalaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			BalaTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			PrepaidBala = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			ApplyAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			RegainAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			SumPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			Register = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Canceler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			CancelDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			CancelTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLPrepaidGrpContSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("FoundDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFoundDate()));
		}
		if (FCode.equals("FoundTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FoundTime));
		}
		if (FCode.equals("BalaDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBalaDate()));
		}
		if (FCode.equals("BalaTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalaTime));
		}
		if (FCode.equals("PrepaidBala"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidBala));
		}
		if (FCode.equals("ApplyAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyAmount));
		}
		if (FCode.equals("RegainAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RegainAmount));
		}
		if (FCode.equals("SumPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
		}
		if (FCode.equals("Register"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Register));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Canceler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Canceler));
		}
		if (FCode.equals("CancelDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCancelDate()));
		}
		if (FCode.equals("CancelTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CancelTime));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFoundDate()));
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(FoundTime);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBalaDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BalaTime);
				break;
			case 5:
				strFieldValue = String.valueOf(PrepaidBala);
				break;
			case 6:
				strFieldValue = String.valueOf(ApplyAmount);
				break;
			case 7:
				strFieldValue = String.valueOf(RegainAmount);
				break;
			case 8:
				strFieldValue = String.valueOf(SumPay);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Register);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Canceler);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCancelDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(CancelTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("FoundDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FoundDate = fDate.getDate( FValue );
			}
			else
				FoundDate = null;
		}
		if (FCode.equalsIgnoreCase("FoundTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FoundTime = FValue.trim();
			}
			else
				FoundTime = null;
		}
		if (FCode.equalsIgnoreCase("BalaDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BalaDate = fDate.getDate( FValue );
			}
			else
				BalaDate = null;
		}
		if (FCode.equalsIgnoreCase("BalaTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BalaTime = FValue.trim();
			}
			else
				BalaTime = null;
		}
		if (FCode.equalsIgnoreCase("PrepaidBala"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PrepaidBala = d;
			}
		}
		if (FCode.equalsIgnoreCase("ApplyAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ApplyAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("RegainAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RegainAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("Register"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Register = FValue.trim();
			}
			else
				Register = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Canceler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Canceler = FValue.trim();
			}
			else
				Canceler = null;
		}
		if (FCode.equalsIgnoreCase("CancelDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CancelDate = fDate.getDate( FValue );
			}
			else
				CancelDate = null;
		}
		if (FCode.equalsIgnoreCase("CancelTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CancelTime = FValue.trim();
			}
			else
				CancelTime = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLPrepaidGrpContSchema other = (LLPrepaidGrpContSchema)otherObject;
		return
			(GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (FoundDate == null ? other.getFoundDate() == null : fDate.getString(FoundDate).equals(other.getFoundDate()))
			&& (FoundTime == null ? other.getFoundTime() == null : FoundTime.equals(other.getFoundTime()))
			&& (BalaDate == null ? other.getBalaDate() == null : fDate.getString(BalaDate).equals(other.getBalaDate()))
			&& (BalaTime == null ? other.getBalaTime() == null : BalaTime.equals(other.getBalaTime()))
			&& PrepaidBala == other.getPrepaidBala()
			&& ApplyAmount == other.getApplyAmount()
			&& RegainAmount == other.getRegainAmount()
			&& SumPay == other.getSumPay()
			&& (Register == null ? other.getRegister() == null : Register.equals(other.getRegister()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Canceler == null ? other.getCanceler() == null : Canceler.equals(other.getCanceler()))
			&& (CancelDate == null ? other.getCancelDate() == null : fDate.getString(CancelDate).equals(other.getCancelDate()))
			&& (CancelTime == null ? other.getCancelTime() == null : CancelTime.equals(other.getCancelTime()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("FoundDate") ) {
			return 1;
		}
		if( strFieldName.equals("FoundTime") ) {
			return 2;
		}
		if( strFieldName.equals("BalaDate") ) {
			return 3;
		}
		if( strFieldName.equals("BalaTime") ) {
			return 4;
		}
		if( strFieldName.equals("PrepaidBala") ) {
			return 5;
		}
		if( strFieldName.equals("ApplyAmount") ) {
			return 6;
		}
		if( strFieldName.equals("RegainAmount") ) {
			return 7;
		}
		if( strFieldName.equals("SumPay") ) {
			return 8;
		}
		if( strFieldName.equals("Register") ) {
			return 9;
		}
		if( strFieldName.equals("State") ) {
			return 10;
		}
		if( strFieldName.equals("Canceler") ) {
			return 11;
		}
		if( strFieldName.equals("CancelDate") ) {
			return 12;
		}
		if( strFieldName.equals("CancelTime") ) {
			return 13;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 14;
		}
		if( strFieldName.equals("Operator") ) {
			return 15;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 16;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 19;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "FoundDate";
				break;
			case 2:
				strFieldName = "FoundTime";
				break;
			case 3:
				strFieldName = "BalaDate";
				break;
			case 4:
				strFieldName = "BalaTime";
				break;
			case 5:
				strFieldName = "PrepaidBala";
				break;
			case 6:
				strFieldName = "ApplyAmount";
				break;
			case 7:
				strFieldName = "RegainAmount";
				break;
			case 8:
				strFieldName = "SumPay";
				break;
			case 9:
				strFieldName = "Register";
				break;
			case 10:
				strFieldName = "State";
				break;
			case 11:
				strFieldName = "Canceler";
				break;
			case 12:
				strFieldName = "CancelDate";
				break;
			case 13:
				strFieldName = "CancelTime";
				break;
			case 14:
				strFieldName = "ManageCom";
				break;
			case 15:
				strFieldName = "Operator";
				break;
			case 16:
				strFieldName = "MakeDate";
				break;
			case 17:
				strFieldName = "MakeTime";
				break;
			case 18:
				strFieldName = "ModifyDate";
				break;
			case 19:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FoundDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("FoundTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalaDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BalaTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrepaidBala") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ApplyAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RegainAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Register") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Canceler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CancelDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CancelTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
