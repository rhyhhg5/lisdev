/*
 * <p>ClassName: LJSGetTempFeeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 更新表
 * @CreateDate：2004-12-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LJSGetTempFeeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LJSGetTempFeeSchema implements Schema
{
    // @Field
    /** 给付通知书号码 */
    private String GetNoticeNo;
    /** 暂交费收据号码 */
    private String TempFeeNo;
    /** 险种编码 */
    private String RiskCode;
    /** 暂交费收据号类型 */
    private String TempFeeType;
    /** 交费方式 */
    private String PayMode;
    /** 退费金额 */
    private double GetMoney;
    /** 退费日期 */
    private Date GetDate;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 投保人名称 */
    private String APPntName;
    /** 代理人组别 */
    private String AgentGroup;
    /** 代理人编码 */
    private String AgentCode;
    /** 补/退费业务类型 */
    private String FeeOperationType;
    /** 补/退费财务类型 */
    private String FeeFinaType;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机时间 */
    private String MakeTime;
    /** 入机日期 */
    private Date MakeDate;
    /** 退费原因编码 */
    private String GetReasonCode;
    /** 状态 */
    private String State;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 23; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LJSGetTempFeeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "GetNoticeNo";
        pk[1] = "TempFeeNo";
        pk[2] = "RiskCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGetNoticeNo()
    {
        if (GetNoticeNo != null && !GetNoticeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetNoticeNo = StrTool.unicodeToGBK(GetNoticeNo);
        }
        return GetNoticeNo;
    }

    public void setGetNoticeNo(String aGetNoticeNo)
    {
        GetNoticeNo = aGetNoticeNo;
    }

    public String getTempFeeNo()
    {
        if (TempFeeNo != null && !TempFeeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TempFeeNo = StrTool.unicodeToGBK(TempFeeNo);
        }
        return TempFeeNo;
    }

    public void setTempFeeNo(String aTempFeeNo)
    {
        TempFeeNo = aTempFeeNo;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getTempFeeType()
    {
        if (TempFeeType != null && !TempFeeType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TempFeeType = StrTool.unicodeToGBK(TempFeeType);
        }
        return TempFeeType;
    }

    public void setTempFeeType(String aTempFeeType)
    {
        TempFeeType = aTempFeeType;
    }

    public String getPayMode()
    {
        if (PayMode != null && !PayMode.equals("") && SysConst.CHANGECHARSET == true)
        {
            PayMode = StrTool.unicodeToGBK(PayMode);
        }
        return PayMode;
    }

    public void setPayMode(String aPayMode)
    {
        PayMode = aPayMode;
    }

    public double getGetMoney()
    {
        return GetMoney;
    }

    public void setGetMoney(double aGetMoney)
    {
        GetMoney = aGetMoney;
    }

    public void setGetMoney(String aGetMoney)
    {
        if (aGetMoney != null && !aGetMoney.equals(""))
        {
            Double tDouble = new Double(aGetMoney);
            double d = tDouble.doubleValue();
            GetMoney = d;
        }
    }

    public String getGetDate()
    {
        if (GetDate != null)
        {
            return fDate.getString(GetDate);
        }
        else
        {
            return null;
        }
    }

    public void setGetDate(Date aGetDate)
    {
        GetDate = aGetDate;
    }

    public void setGetDate(String aGetDate)
    {
        if (aGetDate != null && !aGetDate.equals(""))
        {
            GetDate = fDate.getDate(aGetDate);
        }
        else
        {
            GetDate = null;
        }
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAgentCom()
    {
        if (AgentCom != null && !AgentCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            AgentCom = StrTool.unicodeToGBK(AgentCom);
        }
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom)
    {
        AgentCom = aAgentCom;
    }

    public String getAgentType()
    {
        if (AgentType != null && !AgentType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentType = StrTool.unicodeToGBK(AgentType);
        }
        return AgentType;
    }

    public void setAgentType(String aAgentType)
    {
        AgentType = aAgentType;
    }

    public String getAPPntName()
    {
        if (APPntName != null && !APPntName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            APPntName = StrTool.unicodeToGBK(APPntName);
        }
        return APPntName;
    }

    public void setAPPntName(String aAPPntName)
    {
        APPntName = aAPPntName;
    }

    public String getAgentGroup()
    {
        if (AgentGroup != null && !AgentGroup.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getFeeOperationType()
    {
        if (FeeOperationType != null && !FeeOperationType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FeeOperationType = StrTool.unicodeToGBK(FeeOperationType);
        }
        return FeeOperationType;
    }

    public void setFeeOperationType(String aFeeOperationType)
    {
        FeeOperationType = aFeeOperationType;
    }

    public String getFeeFinaType()
    {
        if (FeeFinaType != null && !FeeFinaType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FeeFinaType = StrTool.unicodeToGBK(FeeFinaType);
        }
        return FeeFinaType;
    }

    public void setFeeFinaType(String aFeeFinaType)
    {
        FeeFinaType = aFeeFinaType;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getGetReasonCode()
    {
        if (GetReasonCode != null && !GetReasonCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetReasonCode = StrTool.unicodeToGBK(GetReasonCode);
        }
        return GetReasonCode;
    }

    public void setGetReasonCode(String aGetReasonCode)
    {
        GetReasonCode = aGetReasonCode;
    }

    public String getState()
    {
        if (State != null && !State.equals("") && SysConst.CHANGECHARSET == true)
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LJSGetTempFeeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LJSGetTempFeeSchema aLJSGetTempFeeSchema)
    {
        this.GetNoticeNo = aLJSGetTempFeeSchema.getGetNoticeNo();
        this.TempFeeNo = aLJSGetTempFeeSchema.getTempFeeNo();
        this.RiskCode = aLJSGetTempFeeSchema.getRiskCode();
        this.TempFeeType = aLJSGetTempFeeSchema.getTempFeeType();
        this.PayMode = aLJSGetTempFeeSchema.getPayMode();
        this.GetMoney = aLJSGetTempFeeSchema.getGetMoney();
        this.GetDate = fDate.getDate(aLJSGetTempFeeSchema.getGetDate());
        this.ManageCom = aLJSGetTempFeeSchema.getManageCom();
        this.AgentCom = aLJSGetTempFeeSchema.getAgentCom();
        this.AgentType = aLJSGetTempFeeSchema.getAgentType();
        this.APPntName = aLJSGetTempFeeSchema.getAPPntName();
        this.AgentGroup = aLJSGetTempFeeSchema.getAgentGroup();
        this.AgentCode = aLJSGetTempFeeSchema.getAgentCode();
        this.FeeOperationType = aLJSGetTempFeeSchema.getFeeOperationType();
        this.FeeFinaType = aLJSGetTempFeeSchema.getFeeFinaType();
        this.SerialNo = aLJSGetTempFeeSchema.getSerialNo();
        this.Operator = aLJSGetTempFeeSchema.getOperator();
        this.MakeTime = aLJSGetTempFeeSchema.getMakeTime();
        this.MakeDate = fDate.getDate(aLJSGetTempFeeSchema.getMakeDate());
        this.GetReasonCode = aLJSGetTempFeeSchema.getGetReasonCode();
        this.State = aLJSGetTempFeeSchema.getState();
        this.ModifyDate = fDate.getDate(aLJSGetTempFeeSchema.getModifyDate());
        this.ModifyTime = aLJSGetTempFeeSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GetNoticeNo") == null)
            {
                this.GetNoticeNo = null;
            }
            else
            {
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();
            }

            if (rs.getString("TempFeeNo") == null)
            {
                this.TempFeeNo = null;
            }
            else
            {
                this.TempFeeNo = rs.getString("TempFeeNo").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("TempFeeType") == null)
            {
                this.TempFeeType = null;
            }
            else
            {
                this.TempFeeType = rs.getString("TempFeeType").trim();
            }

            if (rs.getString("PayMode") == null)
            {
                this.PayMode = null;
            }
            else
            {
                this.PayMode = rs.getString("PayMode").trim();
            }

            this.GetMoney = rs.getDouble("GetMoney");
            this.GetDate = rs.getDate("GetDate");
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentCom") == null)
            {
                this.AgentCom = null;
            }
            else
            {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("AgentType") == null)
            {
                this.AgentType = null;
            }
            else
            {
                this.AgentType = rs.getString("AgentType").trim();
            }

            if (rs.getString("APPntName") == null)
            {
                this.APPntName = null;
            }
            else
            {
                this.APPntName = rs.getString("APPntName").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("FeeOperationType") == null)
            {
                this.FeeOperationType = null;
            }
            else
            {
                this.FeeOperationType = rs.getString("FeeOperationType").trim();
            }

            if (rs.getString("FeeFinaType") == null)
            {
                this.FeeFinaType = null;
            }
            else
            {
                this.FeeFinaType = rs.getString("FeeFinaType").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("GetReasonCode") == null)
            {
                this.GetReasonCode = null;
            }
            else
            {
                this.GetReasonCode = rs.getString("GetReasonCode").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSGetTempFeeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LJSGetTempFeeSchema getSchema()
    {
        LJSGetTempFeeSchema aLJSGetTempFeeSchema = new LJSGetTempFeeSchema();
        aLJSGetTempFeeSchema.setSchema(this);
        return aLJSGetTempFeeSchema;
    }

    public LJSGetTempFeeDB getDB()
    {
        LJSGetTempFeeDB aDBOper = new LJSGetTempFeeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJSGetTempFee描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GetNoticeNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TempFeeNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TempFeeType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayMode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetMoney) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(GetDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(APPntName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FeeOperationType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FeeFinaType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetReasonCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLJSGetTempFee>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            TempFeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            GetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            GetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            APPntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                       SysConst.PACKAGESPILTER);
            FeeOperationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              14, SysConst.PACKAGESPILTER);
            FeeFinaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                         SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            GetReasonCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                           SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                   SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LJSGetTempFeeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GetNoticeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetNoticeNo));
        }
        if (FCode.equals("TempFeeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TempFeeNo));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("TempFeeType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TempFeeType));
        }
        if (FCode.equals("PayMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayMode));
        }
        if (FCode.equals("GetMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetMoney));
        }
        if (FCode.equals("GetDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getGetDate()));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("AgentCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCom));
        }
        if (FCode.equals("AgentType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentType));
        }
        if (FCode.equals("APPntName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(APPntName));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentGroup));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("FeeOperationType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FeeOperationType));
        }
        if (FCode.equals("FeeFinaType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FeeFinaType));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("GetReasonCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetReasonCode));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(State));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(TempFeeType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 5:
                strFieldValue = String.valueOf(GetMoney);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getGetDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AgentCom);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AgentType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(APPntName);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(FeeOperationType);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(FeeFinaType);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(GetReasonCode);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GetNoticeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetNoticeNo = FValue.trim();
            }
            else
            {
                GetNoticeNo = null;
            }
        }
        if (FCode.equals("TempFeeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TempFeeNo = FValue.trim();
            }
            else
            {
                TempFeeNo = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("TempFeeType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TempFeeType = FValue.trim();
            }
            else
            {
                TempFeeType = null;
            }
        }
        if (FCode.equals("PayMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
            {
                PayMode = null;
            }
        }
        if (FCode.equals("GetMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetMoney = d;
            }
        }
        if (FCode.equals("GetDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDate = fDate.getDate(FValue);
            }
            else
            {
                GetDate = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AgentCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCom = FValue.trim();
            }
            else
            {
                AgentCom = null;
            }
        }
        if (FCode.equals("AgentType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentType = FValue.trim();
            }
            else
            {
                AgentType = null;
            }
        }
        if (FCode.equals("APPntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                APPntName = FValue.trim();
            }
            else
            {
                APPntName = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("FeeOperationType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FeeOperationType = FValue.trim();
            }
            else
            {
                FeeOperationType = null;
            }
        }
        if (FCode.equals("FeeFinaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FeeFinaType = FValue.trim();
            }
            else
            {
                FeeFinaType = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("GetReasonCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetReasonCode = FValue.trim();
            }
            else
            {
                GetReasonCode = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LJSGetTempFeeSchema other = (LJSGetTempFeeSchema) otherObject;
        return
                GetNoticeNo.equals(other.getGetNoticeNo())
                && TempFeeNo.equals(other.getTempFeeNo())
                && RiskCode.equals(other.getRiskCode())
                && TempFeeType.equals(other.getTempFeeType())
                && PayMode.equals(other.getPayMode())
                && GetMoney == other.getGetMoney()
                && fDate.getString(GetDate).equals(other.getGetDate())
                && ManageCom.equals(other.getManageCom())
                && AgentCom.equals(other.getAgentCom())
                && AgentType.equals(other.getAgentType())
                && APPntName.equals(other.getAPPntName())
                && AgentGroup.equals(other.getAgentGroup())
                && AgentCode.equals(other.getAgentCode())
                && FeeOperationType.equals(other.getFeeOperationType())
                && FeeFinaType.equals(other.getFeeFinaType())
                && SerialNo.equals(other.getSerialNo())
                && Operator.equals(other.getOperator())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && GetReasonCode.equals(other.getGetReasonCode())
                && State.equals(other.getState())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GetNoticeNo"))
        {
            return 0;
        }
        if (strFieldName.equals("TempFeeNo"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 2;
        }
        if (strFieldName.equals("TempFeeType"))
        {
            return 3;
        }
        if (strFieldName.equals("PayMode"))
        {
            return 4;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return 5;
        }
        if (strFieldName.equals("GetDate"))
        {
            return 6;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 7;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return 8;
        }
        if (strFieldName.equals("AgentType"))
        {
            return 9;
        }
        if (strFieldName.equals("APPntName"))
        {
            return 10;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 11;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 12;
        }
        if (strFieldName.equals("FeeOperationType"))
        {
            return 13;
        }
        if (strFieldName.equals("FeeFinaType"))
        {
            return 14;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 15;
        }
        if (strFieldName.equals("Operator"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 18;
        }
        if (strFieldName.equals("GetReasonCode"))
        {
            return 19;
        }
        if (strFieldName.equals("State"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 21;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 22;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GetNoticeNo";
                break;
            case 1:
                strFieldName = "TempFeeNo";
                break;
            case 2:
                strFieldName = "RiskCode";
                break;
            case 3:
                strFieldName = "TempFeeType";
                break;
            case 4:
                strFieldName = "PayMode";
                break;
            case 5:
                strFieldName = "GetMoney";
                break;
            case 6:
                strFieldName = "GetDate";
                break;
            case 7:
                strFieldName = "ManageCom";
                break;
            case 8:
                strFieldName = "AgentCom";
                break;
            case 9:
                strFieldName = "AgentType";
                break;
            case 10:
                strFieldName = "APPntName";
                break;
            case 11:
                strFieldName = "AgentGroup";
                break;
            case 12:
                strFieldName = "AgentCode";
                break;
            case 13:
                strFieldName = "FeeOperationType";
                break;
            case 14:
                strFieldName = "FeeFinaType";
                break;
            case 15:
                strFieldName = "SerialNo";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "MakeDate";
                break;
            case 19:
                strFieldName = "GetReasonCode";
                break;
            case 20:
                strFieldName = "State";
                break;
            case 21:
                strFieldName = "ModifyDate";
                break;
            case 22:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GetNoticeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TempFeeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TempFeeType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("APPntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeOperationType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeFinaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("GetReasonCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
