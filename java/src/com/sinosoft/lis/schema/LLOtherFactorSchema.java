/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LLOtherFactorDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;


/*
 * <p>ClassName: LLOtherFactorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-04-07
 */
public class LLOtherFactorSchema implements Schema
{
    // @Field
    /** 录入要素类型代码 */
    private String FactorType;

    /** 要素编号 */
    private String FactorCode;

    /** 分案号(个人理赔号) */
    private String CaseNo;

    /** 受理事故号 */
    private String CaseRelaNo;

    /** 事件号 */
    private String SubRptNo;

    /** 要素名字 */
    private String FactorName;

    /** 要素值 */
    private String Value;

    /** 备注 */
    private String Remark;

    public static final int FIELDNUM = 8; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息


    // @Constructor
    public LLOtherFactorSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "FactorType";
        pk[1] = "FactorCode";
        pk[2] = "CaseNo";
        pk[3] = "CaseRelaNo";

        PK = pk;
    }


    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getFactorType()
    {
        if (SysConst.CHANGECHARSET && FactorType != null &&
            !FactorType.equals(""))
        {
            FactorType = StrTool.unicodeToGBK(FactorType);
        }
        return FactorType;
    }

    public void setFactorType(String aFactorType)
    {
        FactorType = aFactorType;
    }

    public String getFactorCode()
    {
        if (SysConst.CHANGECHARSET && FactorCode != null &&
            !FactorCode.equals(""))
        {
            FactorCode = StrTool.unicodeToGBK(FactorCode);
        }
        return FactorCode;
    }

    public void setFactorCode(String aFactorCode)
    {
        FactorCode = aFactorCode;
    }

    public String getCaseNo()
    {
        if (SysConst.CHANGECHARSET && CaseNo != null && !CaseNo.equals(""))
        {
            CaseNo = StrTool.unicodeToGBK(CaseNo);
        }
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo)
    {
        CaseNo = aCaseNo;
    }

    public String getCaseRelaNo()
    {
        if (SysConst.CHANGECHARSET && CaseRelaNo != null &&
            !CaseRelaNo.equals(""))
        {
            CaseRelaNo = StrTool.unicodeToGBK(CaseRelaNo);
        }
        return CaseRelaNo;
    }

    public void setCaseRelaNo(String aCaseRelaNo)
    {
        CaseRelaNo = aCaseRelaNo;
    }

    public String getSubRptNo()
    {
        if (SysConst.CHANGECHARSET && SubRptNo != null && !SubRptNo.equals(""))
        {
            SubRptNo = StrTool.unicodeToGBK(SubRptNo);
        }
        return SubRptNo;
    }

    public void setSubRptNo(String aSubRptNo)
    {
        SubRptNo = aSubRptNo;
    }

    public String getFactorName()
    {
        if (SysConst.CHANGECHARSET && FactorName != null &&
            !FactorName.equals(""))
        {
            FactorName = StrTool.unicodeToGBK(FactorName);
        }
        return FactorName;
    }

    public void setFactorName(String aFactorName)
    {
        FactorName = aFactorName;
    }

    public String getValue()
    {
        if (SysConst.CHANGECHARSET && Value != null && !Value.equals(""))
        {
            Value = StrTool.unicodeToGBK(Value);
        }
        return Value;
    }

    public void setValue(String aValue)
    {
        Value = aValue;
    }

    public String getRemark()
    {
        if (SysConst.CHANGECHARSET && Remark != null && !Remark.equals(""))
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }


    /**
     * 使用另外一个 LLOtherFactorSchema 对象给 Schema 赋值
     * @param: aLLOtherFactorSchema LLOtherFactorSchema
     **/
    public void setSchema(LLOtherFactorSchema aLLOtherFactorSchema)
    {
        this.FactorType = aLLOtherFactorSchema.getFactorType();
        this.FactorCode = aLLOtherFactorSchema.getFactorCode();
        this.CaseNo = aLLOtherFactorSchema.getCaseNo();
        this.CaseRelaNo = aLLOtherFactorSchema.getCaseRelaNo();
        this.SubRptNo = aLLOtherFactorSchema.getSubRptNo();
        this.FactorName = aLLOtherFactorSchema.getFactorName();
        this.Value = aLLOtherFactorSchema.getValue();
        this.Remark = aLLOtherFactorSchema.getRemark();
    }


    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("FactorType") == null)
            {
                this.FactorType = null;
            }
            else
            {
                this.FactorType = rs.getString("FactorType").trim();
            }

            if (rs.getString("FactorCode") == null)
            {
                this.FactorCode = null;
            }
            else
            {
                this.FactorCode = rs.getString("FactorCode").trim();
            }

            if (rs.getString("CaseNo") == null)
            {
                this.CaseNo = null;
            }
            else
            {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("CaseRelaNo") == null)
            {
                this.CaseRelaNo = null;
            }
            else
            {
                this.CaseRelaNo = rs.getString("CaseRelaNo").trim();
            }

            if (rs.getString("SubRptNo") == null)
            {
                this.SubRptNo = null;
            }
            else
            {
                this.SubRptNo = rs.getString("SubRptNo").trim();
            }

            if (rs.getString("FactorName") == null)
            {
                this.FactorName = null;
            }
            else
            {
                this.FactorName = rs.getString("FactorName").trim();
            }

            if (rs.getString("Value") == null)
            {
                this.Value = null;
            }
            else
            {
                this.Value = rs.getString("Value").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLOtherFactorSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLOtherFactorSchema getSchema()
    {
        LLOtherFactorSchema aLLOtherFactorSchema = new LLOtherFactorSchema();
        aLLOtherFactorSchema.setSchema(this);
        return aLLOtherFactorSchema;
    }

    public LLOtherFactorDB getDB()
    {
        LLOtherFactorDB aDBOper = new LLOtherFactorDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLOtherFactor描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(FactorType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(FactorCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CaseNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CaseRelaNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(SubRptNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(FactorName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Value)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Remark)));
        return strReturn.toString();
    }


    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLOtherFactor>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            FactorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            FactorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            CaseRelaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            SubRptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            FactorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            Value = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLOtherFactorSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }


    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("FactorType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorType));
        }
        if (FCode.equals("FactorCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorCode));
        }
        if (FCode.equals("CaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("CaseRelaNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseRelaNo));
        }
        if (FCode.equals("SubRptNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubRptNo));
        }
        if (FCode.equals("FactorName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FactorName));
        }
        if (FCode.equals("Value"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Value));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(FactorType);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(FactorCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CaseRelaNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SubRptNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(FactorName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Value);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }


    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("FactorType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FactorType = FValue.trim();
            }
            else
            {
                FactorType = null;
            }
        }
        if (FCode.equals("FactorCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FactorCode = FValue.trim();
            }
            else
            {
                FactorCode = null;
            }
        }
        if (FCode.equals("CaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
            {
                CaseNo = null;
            }
        }
        if (FCode.equals("CaseRelaNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseRelaNo = FValue.trim();
            }
            else
            {
                CaseRelaNo = null;
            }
        }
        if (FCode.equals("SubRptNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubRptNo = FValue.trim();
            }
            else
            {
                SubRptNo = null;
            }
        }
        if (FCode.equals("FactorName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FactorName = FValue.trim();
            }
            else
            {
                FactorName = null;
            }
        }
        if (FCode.equals("Value"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Value = FValue.trim();
            }
            else
            {
                Value = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLOtherFactorSchema other = (LLOtherFactorSchema) otherObject;
        return
                FactorType.equals(other.getFactorType())
                && FactorCode.equals(other.getFactorCode())
                && CaseNo.equals(other.getCaseNo())
                && CaseRelaNo.equals(other.getCaseRelaNo())
                && SubRptNo.equals(other.getSubRptNo())
                && FactorName.equals(other.getFactorName())
                && Value.equals(other.getValue())
                && Remark.equals(other.getRemark());
    }


    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }


    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("FactorType"))
        {
            return 0;
        }
        if (strFieldName.equals("FactorCode"))
        {
            return 1;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return 2;
        }
        if (strFieldName.equals("CaseRelaNo"))
        {
            return 3;
        }
        if (strFieldName.equals("SubRptNo"))
        {
            return 4;
        }
        if (strFieldName.equals("FactorName"))
        {
            return 5;
        }
        if (strFieldName.equals("Value"))
        {
            return 6;
        }
        if (strFieldName.equals("Remark"))
        {
            return 7;
        }
        return -1;
    }


    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "FactorType";
                break;
            case 1:
                strFieldName = "FactorCode";
                break;
            case 2:
                strFieldName = "CaseNo";
                break;
            case 3:
                strFieldName = "CaseRelaNo";
                break;
            case 4:
                strFieldName = "SubRptNo";
                break;
            case 5:
                strFieldName = "FactorName";
                break;
            case 6:
                strFieldName = "Value";
                break;
            case 7:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }


    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("FactorType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactorCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseRelaNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubRptNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FactorName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Value"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }


    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
