/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LOMixComDB;

/*
 * <p>ClassName: LOMixComSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2009-12-10
 */
public class LOMixComSchema implements Schema, Cloneable
{
	// @Field
	/** 管理机构id */
	private String GrpAgentCom;
	/** 子公司代码 */
	private String Comp_Cod;
	/** 子公司名称 */
	private String Comp_Nam;
	/** 省级分公司代码 */
	private String Prov_Orgcod;
	/** 省级分公司名称 */
	private String Prov_Orgname;
	/** 地市级分公司代码 */
	private String City_Orgcod;
	/** 地市级分公司名称 */
	private String City_Orgname;
	/** 县支级分公司代码 */
	private String Town_Orgcod;
	/** 县支级分公司名称 */
	private String Town_Orgname;
	/** 四级分支机构代码 */
	private String Under_Orgcod;
	/** 四级分支机构名称 */
	private String Under_Orgname;
	/** 机构等级 */
	private String Org_Lvl;
	/** 起始日期 */
	private String Start_Dat;
	/** 终止日期 */
	private String End_Dat;
	/** 状态 */
	private String Status_Cod;
	/** 状态说明 */
	private String Status_Nam;
	/** 子公司报送时间 */
	private String Date_Send;
	/** 备用字段一 */
	private String StandByFlag1;
	/** 备用字段二 */
	private String StandByFlag2;
	/** 备用字段三 */
	private String StandByFlag3;
	/** 备用字段四 */
	private String StandByFlag4;
	/** 备用字段五 */
	private String StandByFlag5;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最近一次修改日期 */
	private Date ModifyDate;
	/** 最近一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 26;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LOMixComSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "GrpAgentCom";
		pk[1] = "Comp_Cod";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LOMixComSchema cloned = (LOMixComSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpAgentCom()
	{
		return GrpAgentCom;
	}
	public void setGrpAgentCom(String aGrpAgentCom)
	{
		GrpAgentCom = aGrpAgentCom;
	}
	public String getComp_Cod()
	{
		return Comp_Cod;
	}
	public void setComp_Cod(String aComp_Cod)
	{
		Comp_Cod = aComp_Cod;
	}
	public String getComp_Nam()
	{
		return Comp_Nam;
	}
	public void setComp_Nam(String aComp_Nam)
	{
		Comp_Nam = aComp_Nam;
	}
	public String getProv_Orgcod()
	{
		return Prov_Orgcod;
	}
	public void setProv_Orgcod(String aProv_Orgcod)
	{
		Prov_Orgcod = aProv_Orgcod;
	}
	public String getProv_Orgname()
	{
		return Prov_Orgname;
	}
	public void setProv_Orgname(String aProv_Orgname)
	{
		Prov_Orgname = aProv_Orgname;
	}
	public String getCity_Orgcod()
	{
		return City_Orgcod;
	}
	public void setCity_Orgcod(String aCity_Orgcod)
	{
		City_Orgcod = aCity_Orgcod;
	}
	public String getCity_Orgname()
	{
		return City_Orgname;
	}
	public void setCity_Orgname(String aCity_Orgname)
	{
		City_Orgname = aCity_Orgname;
	}
	public String getTown_Orgcod()
	{
		return Town_Orgcod;
	}
	public void setTown_Orgcod(String aTown_Orgcod)
	{
		Town_Orgcod = aTown_Orgcod;
	}
	public String getTown_Orgname()
	{
		return Town_Orgname;
	}
	public void setTown_Orgname(String aTown_Orgname)
	{
		Town_Orgname = aTown_Orgname;
	}
	public String getUnder_Orgcod()
	{
		return Under_Orgcod;
	}
	public void setUnder_Orgcod(String aUnder_Orgcod)
	{
		Under_Orgcod = aUnder_Orgcod;
	}
	public String getUnder_Orgname()
	{
		return Under_Orgname;
	}
	public void setUnder_Orgname(String aUnder_Orgname)
	{
		Under_Orgname = aUnder_Orgname;
	}
	public String getOrg_Lvl()
	{
		return Org_Lvl;
	}
	public void setOrg_Lvl(String aOrg_Lvl)
	{
		Org_Lvl = aOrg_Lvl;
	}
	public String getStart_Dat()
	{
		return Start_Dat;
	}
	public void setStart_Dat(String aStart_Dat)
	{
		Start_Dat = aStart_Dat;
	}
	public String getEnd_Dat()
	{
		return End_Dat;
	}
	public void setEnd_Dat(String aEnd_Dat)
	{
		End_Dat = aEnd_Dat;
	}
	public String getStatus_Cod()
	{
		return Status_Cod;
	}
	public void setStatus_Cod(String aStatus_Cod)
	{
		Status_Cod = aStatus_Cod;
	}
	public String getStatus_Nam()
	{
		return Status_Nam;
	}
	public void setStatus_Nam(String aStatus_Nam)
	{
		Status_Nam = aStatus_Nam;
	}
	public String getDate_Send()
	{
		return Date_Send;
	}
	public void setDate_Send(String aDate_Send)
	{
		Date_Send = aDate_Send;
	}
	public String getStandByFlag1()
	{
		return StandByFlag1;
	}
	public void setStandByFlag1(String aStandByFlag1)
	{
		StandByFlag1 = aStandByFlag1;
	}
	public String getStandByFlag2()
	{
		return StandByFlag2;
	}
	public void setStandByFlag2(String aStandByFlag2)
	{
		StandByFlag2 = aStandByFlag2;
	}
	public String getStandByFlag3()
	{
		return StandByFlag3;
	}
	public void setStandByFlag3(String aStandByFlag3)
	{
		StandByFlag3 = aStandByFlag3;
	}
	public String getStandByFlag4()
	{
		return StandByFlag4;
	}
	public void setStandByFlag4(String aStandByFlag4)
	{
		StandByFlag4 = aStandByFlag4;
	}
	public String getStandByFlag5()
	{
		return StandByFlag5;
	}
	public void setStandByFlag5(String aStandByFlag5)
	{
		StandByFlag5 = aStandByFlag5;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LOMixComSchema 对象给 Schema 赋值
	* @param: aLOMixComSchema LOMixComSchema
	**/
	public void setSchema(LOMixComSchema aLOMixComSchema)
	{
		this.GrpAgentCom = aLOMixComSchema.getGrpAgentCom();
		this.Comp_Cod = aLOMixComSchema.getComp_Cod();
		this.Comp_Nam = aLOMixComSchema.getComp_Nam();
		this.Prov_Orgcod = aLOMixComSchema.getProv_Orgcod();
		this.Prov_Orgname = aLOMixComSchema.getProv_Orgname();
		this.City_Orgcod = aLOMixComSchema.getCity_Orgcod();
		this.City_Orgname = aLOMixComSchema.getCity_Orgname();
		this.Town_Orgcod = aLOMixComSchema.getTown_Orgcod();
		this.Town_Orgname = aLOMixComSchema.getTown_Orgname();
		this.Under_Orgcod = aLOMixComSchema.getUnder_Orgcod();
		this.Under_Orgname = aLOMixComSchema.getUnder_Orgname();
		this.Org_Lvl = aLOMixComSchema.getOrg_Lvl();
		this.Start_Dat = aLOMixComSchema.getStart_Dat();
		this.End_Dat = aLOMixComSchema.getEnd_Dat();
		this.Status_Cod = aLOMixComSchema.getStatus_Cod();
		this.Status_Nam = aLOMixComSchema.getStatus_Nam();
		this.Date_Send = aLOMixComSchema.getDate_Send();
		this.StandByFlag1 = aLOMixComSchema.getStandByFlag1();
		this.StandByFlag2 = aLOMixComSchema.getStandByFlag2();
		this.StandByFlag3 = aLOMixComSchema.getStandByFlag3();
		this.StandByFlag4 = aLOMixComSchema.getStandByFlag4();
		this.StandByFlag5 = aLOMixComSchema.getStandByFlag5();
		this.MakeDate = fDate.getDate( aLOMixComSchema.getMakeDate());
		this.MakeTime = aLOMixComSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLOMixComSchema.getModifyDate());
		this.ModifyTime = aLOMixComSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpAgentCom") == null )
				this.GrpAgentCom = null;
			else
				this.GrpAgentCom = rs.getString("GrpAgentCom").trim();

			if( rs.getString("Comp_Cod") == null )
				this.Comp_Cod = null;
			else
				this.Comp_Cod = rs.getString("Comp_Cod").trim();

			if( rs.getString("Comp_Nam") == null )
				this.Comp_Nam = null;
			else
				this.Comp_Nam = rs.getString("Comp_Nam").trim();

			if( rs.getString("Prov_Orgcod") == null )
				this.Prov_Orgcod = null;
			else
				this.Prov_Orgcod = rs.getString("Prov_Orgcod").trim();

			if( rs.getString("Prov_Orgname") == null )
				this.Prov_Orgname = null;
			else
				this.Prov_Orgname = rs.getString("Prov_Orgname").trim();

			if( rs.getString("City_Orgcod") == null )
				this.City_Orgcod = null;
			else
				this.City_Orgcod = rs.getString("City_Orgcod").trim();

			if( rs.getString("City_Orgname") == null )
				this.City_Orgname = null;
			else
				this.City_Orgname = rs.getString("City_Orgname").trim();

			if( rs.getString("Town_Orgcod") == null )
				this.Town_Orgcod = null;
			else
				this.Town_Orgcod = rs.getString("Town_Orgcod").trim();

			if( rs.getString("Town_Orgname") == null )
				this.Town_Orgname = null;
			else
				this.Town_Orgname = rs.getString("Town_Orgname").trim();

			if( rs.getString("Under_Orgcod") == null )
				this.Under_Orgcod = null;
			else
				this.Under_Orgcod = rs.getString("Under_Orgcod").trim();

			if( rs.getString("Under_Orgname") == null )
				this.Under_Orgname = null;
			else
				this.Under_Orgname = rs.getString("Under_Orgname").trim();

			if( rs.getString("Org_Lvl") == null )
				this.Org_Lvl = null;
			else
				this.Org_Lvl = rs.getString("Org_Lvl").trim();

			if( rs.getString("Start_Dat") == null )
				this.Start_Dat = null;
			else
				this.Start_Dat = rs.getString("Start_Dat").trim();

			if( rs.getString("End_Dat") == null )
				this.End_Dat = null;
			else
				this.End_Dat = rs.getString("End_Dat").trim();

			if( rs.getString("Status_Cod") == null )
				this.Status_Cod = null;
			else
				this.Status_Cod = rs.getString("Status_Cod").trim();

			if( rs.getString("Status_Nam") == null )
				this.Status_Nam = null;
			else
				this.Status_Nam = rs.getString("Status_Nam").trim();

			if( rs.getString("Date_Send") == null )
				this.Date_Send = null;
			else
				this.Date_Send = rs.getString("Date_Send").trim();

			if( rs.getString("StandByFlag1") == null )
				this.StandByFlag1 = null;
			else
				this.StandByFlag1 = rs.getString("StandByFlag1").trim();

			if( rs.getString("StandByFlag2") == null )
				this.StandByFlag2 = null;
			else
				this.StandByFlag2 = rs.getString("StandByFlag2").trim();

			if( rs.getString("StandByFlag3") == null )
				this.StandByFlag3 = null;
			else
				this.StandByFlag3 = rs.getString("StandByFlag3").trim();

			if( rs.getString("StandByFlag4") == null )
				this.StandByFlag4 = null;
			else
				this.StandByFlag4 = rs.getString("StandByFlag4").trim();

			if( rs.getString("StandByFlag5") == null )
				this.StandByFlag5 = null;
			else
				this.StandByFlag5 = rs.getString("StandByFlag5").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LOMixCom表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMixComSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LOMixComSchema getSchema()
	{
		LOMixComSchema aLOMixComSchema = new LOMixComSchema();
		aLOMixComSchema.setSchema(this);
		return aLOMixComSchema;
	}

	public LOMixComDB getDB()
	{
		LOMixComDB aDBOper = new LOMixComDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMixCom描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpAgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Comp_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Comp_Nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Prov_Orgcod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Prov_Orgname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(City_Orgcod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(City_Orgname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Town_Orgcod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Town_Orgname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Under_Orgcod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Under_Orgname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Org_Lvl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Start_Dat)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(End_Dat)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Status_Cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Status_Nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Date_Send)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByFlag3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByFlag4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByFlag5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMixCom>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Comp_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Comp_Nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Prov_Orgcod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Prov_Orgname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			City_Orgcod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			City_Orgname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Town_Orgcod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Town_Orgname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Under_Orgcod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Under_Orgname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Org_Lvl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Start_Dat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			End_Dat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Status_Cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Status_Nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Date_Send = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			StandByFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			StandByFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			StandByFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			StandByFlag4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			StandByFlag5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMixComSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpAgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCom));
		}
		if (FCode.equals("Comp_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Comp_Cod));
		}
		if (FCode.equals("Comp_Nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Comp_Nam));
		}
		if (FCode.equals("Prov_Orgcod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prov_Orgcod));
		}
		if (FCode.equals("Prov_Orgname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Prov_Orgname));
		}
		if (FCode.equals("City_Orgcod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(City_Orgcod));
		}
		if (FCode.equals("City_Orgname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(City_Orgname));
		}
		if (FCode.equals("Town_Orgcod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Town_Orgcod));
		}
		if (FCode.equals("Town_Orgname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Town_Orgname));
		}
		if (FCode.equals("Under_Orgcod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Under_Orgcod));
		}
		if (FCode.equals("Under_Orgname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Under_Orgname));
		}
		if (FCode.equals("Org_Lvl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Org_Lvl));
		}
		if (FCode.equals("Start_Dat"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Start_Dat));
		}
		if (FCode.equals("End_Dat"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(End_Dat));
		}
		if (FCode.equals("Status_Cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Status_Cod));
		}
		if (FCode.equals("Status_Nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Status_Nam));
		}
		if (FCode.equals("Date_Send"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Date_Send));
		}
		if (FCode.equals("StandByFlag1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
		}
		if (FCode.equals("StandByFlag2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag2));
		}
		if (FCode.equals("StandByFlag3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag3));
		}
		if (FCode.equals("StandByFlag4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag4));
		}
		if (FCode.equals("StandByFlag5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag5));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCom);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Comp_Cod);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Comp_Nam);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Prov_Orgcod);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Prov_Orgname);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(City_Orgcod);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(City_Orgname);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Town_Orgcod);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Town_Orgname);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Under_Orgcod);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Under_Orgname);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Org_Lvl);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Start_Dat);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(End_Dat);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Status_Cod);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Status_Nam);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Date_Send);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(StandByFlag1);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(StandByFlag2);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(StandByFlag3);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(StandByFlag4);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(StandByFlag5);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpAgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCom = FValue.trim();
			}
			else
				GrpAgentCom = null;
		}
		if (FCode.equalsIgnoreCase("Comp_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Comp_Cod = FValue.trim();
			}
			else
				Comp_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Comp_Nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Comp_Nam = FValue.trim();
			}
			else
				Comp_Nam = null;
		}
		if (FCode.equalsIgnoreCase("Prov_Orgcod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Prov_Orgcod = FValue.trim();
			}
			else
				Prov_Orgcod = null;
		}
		if (FCode.equalsIgnoreCase("Prov_Orgname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Prov_Orgname = FValue.trim();
			}
			else
				Prov_Orgname = null;
		}
		if (FCode.equalsIgnoreCase("City_Orgcod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				City_Orgcod = FValue.trim();
			}
			else
				City_Orgcod = null;
		}
		if (FCode.equalsIgnoreCase("City_Orgname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				City_Orgname = FValue.trim();
			}
			else
				City_Orgname = null;
		}
		if (FCode.equalsIgnoreCase("Town_Orgcod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Town_Orgcod = FValue.trim();
			}
			else
				Town_Orgcod = null;
		}
		if (FCode.equalsIgnoreCase("Town_Orgname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Town_Orgname = FValue.trim();
			}
			else
				Town_Orgname = null;
		}
		if (FCode.equalsIgnoreCase("Under_Orgcod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Under_Orgcod = FValue.trim();
			}
			else
				Under_Orgcod = null;
		}
		if (FCode.equalsIgnoreCase("Under_Orgname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Under_Orgname = FValue.trim();
			}
			else
				Under_Orgname = null;
		}
		if (FCode.equalsIgnoreCase("Org_Lvl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Org_Lvl = FValue.trim();
			}
			else
				Org_Lvl = null;
		}
		if (FCode.equalsIgnoreCase("Start_Dat"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Start_Dat = FValue.trim();
			}
			else
				Start_Dat = null;
		}
		if (FCode.equalsIgnoreCase("End_Dat"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				End_Dat = FValue.trim();
			}
			else
				End_Dat = null;
		}
		if (FCode.equalsIgnoreCase("Status_Cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Status_Cod = FValue.trim();
			}
			else
				Status_Cod = null;
		}
		if (FCode.equalsIgnoreCase("Status_Nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Status_Nam = FValue.trim();
			}
			else
				Status_Nam = null;
		}
		if (FCode.equalsIgnoreCase("Date_Send"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Date_Send = FValue.trim();
			}
			else
				Date_Send = null;
		}
		if (FCode.equalsIgnoreCase("StandByFlag1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByFlag1 = FValue.trim();
			}
			else
				StandByFlag1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByFlag2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByFlag2 = FValue.trim();
			}
			else
				StandByFlag2 = null;
		}
		if (FCode.equalsIgnoreCase("StandByFlag3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByFlag3 = FValue.trim();
			}
			else
				StandByFlag3 = null;
		}
		if (FCode.equalsIgnoreCase("StandByFlag4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByFlag4 = FValue.trim();
			}
			else
				StandByFlag4 = null;
		}
		if (FCode.equalsIgnoreCase("StandByFlag5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByFlag5 = FValue.trim();
			}
			else
				StandByFlag5 = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LOMixComSchema other = (LOMixComSchema)otherObject;
		return
			(GrpAgentCom == null ? other.getGrpAgentCom() == null : GrpAgentCom.equals(other.getGrpAgentCom()))
			&& (Comp_Cod == null ? other.getComp_Cod() == null : Comp_Cod.equals(other.getComp_Cod()))
			&& (Comp_Nam == null ? other.getComp_Nam() == null : Comp_Nam.equals(other.getComp_Nam()))
			&& (Prov_Orgcod == null ? other.getProv_Orgcod() == null : Prov_Orgcod.equals(other.getProv_Orgcod()))
			&& (Prov_Orgname == null ? other.getProv_Orgname() == null : Prov_Orgname.equals(other.getProv_Orgname()))
			&& (City_Orgcod == null ? other.getCity_Orgcod() == null : City_Orgcod.equals(other.getCity_Orgcod()))
			&& (City_Orgname == null ? other.getCity_Orgname() == null : City_Orgname.equals(other.getCity_Orgname()))
			&& (Town_Orgcod == null ? other.getTown_Orgcod() == null : Town_Orgcod.equals(other.getTown_Orgcod()))
			&& (Town_Orgname == null ? other.getTown_Orgname() == null : Town_Orgname.equals(other.getTown_Orgname()))
			&& (Under_Orgcod == null ? other.getUnder_Orgcod() == null : Under_Orgcod.equals(other.getUnder_Orgcod()))
			&& (Under_Orgname == null ? other.getUnder_Orgname() == null : Under_Orgname.equals(other.getUnder_Orgname()))
			&& (Org_Lvl == null ? other.getOrg_Lvl() == null : Org_Lvl.equals(other.getOrg_Lvl()))
			&& (Start_Dat == null ? other.getStart_Dat() == null : Start_Dat.equals(other.getStart_Dat()))
			&& (End_Dat == null ? other.getEnd_Dat() == null : End_Dat.equals(other.getEnd_Dat()))
			&& (Status_Cod == null ? other.getStatus_Cod() == null : Status_Cod.equals(other.getStatus_Cod()))
			&& (Status_Nam == null ? other.getStatus_Nam() == null : Status_Nam.equals(other.getStatus_Nam()))
			&& (Date_Send == null ? other.getDate_Send() == null : Date_Send.equals(other.getDate_Send()))
			&& (StandByFlag1 == null ? other.getStandByFlag1() == null : StandByFlag1.equals(other.getStandByFlag1()))
			&& (StandByFlag2 == null ? other.getStandByFlag2() == null : StandByFlag2.equals(other.getStandByFlag2()))
			&& (StandByFlag3 == null ? other.getStandByFlag3() == null : StandByFlag3.equals(other.getStandByFlag3()))
			&& (StandByFlag4 == null ? other.getStandByFlag4() == null : StandByFlag4.equals(other.getStandByFlag4()))
			&& (StandByFlag5 == null ? other.getStandByFlag5() == null : StandByFlag5.equals(other.getStandByFlag5()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpAgentCom") ) {
			return 0;
		}
		if( strFieldName.equals("Comp_Cod") ) {
			return 1;
		}
		if( strFieldName.equals("Comp_Nam") ) {
			return 2;
		}
		if( strFieldName.equals("Prov_Orgcod") ) {
			return 3;
		}
		if( strFieldName.equals("Prov_Orgname") ) {
			return 4;
		}
		if( strFieldName.equals("City_Orgcod") ) {
			return 5;
		}
		if( strFieldName.equals("City_Orgname") ) {
			return 6;
		}
		if( strFieldName.equals("Town_Orgcod") ) {
			return 7;
		}
		if( strFieldName.equals("Town_Orgname") ) {
			return 8;
		}
		if( strFieldName.equals("Under_Orgcod") ) {
			return 9;
		}
		if( strFieldName.equals("Under_Orgname") ) {
			return 10;
		}
		if( strFieldName.equals("Org_Lvl") ) {
			return 11;
		}
		if( strFieldName.equals("Start_Dat") ) {
			return 12;
		}
		if( strFieldName.equals("End_Dat") ) {
			return 13;
		}
		if( strFieldName.equals("Status_Cod") ) {
			return 14;
		}
		if( strFieldName.equals("Status_Nam") ) {
			return 15;
		}
		if( strFieldName.equals("Date_Send") ) {
			return 16;
		}
		if( strFieldName.equals("StandByFlag1") ) {
			return 17;
		}
		if( strFieldName.equals("StandByFlag2") ) {
			return 18;
		}
		if( strFieldName.equals("StandByFlag3") ) {
			return 19;
		}
		if( strFieldName.equals("StandByFlag4") ) {
			return 20;
		}
		if( strFieldName.equals("StandByFlag5") ) {
			return 21;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 22;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 25;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpAgentCom";
				break;
			case 1:
				strFieldName = "Comp_Cod";
				break;
			case 2:
				strFieldName = "Comp_Nam";
				break;
			case 3:
				strFieldName = "Prov_Orgcod";
				break;
			case 4:
				strFieldName = "Prov_Orgname";
				break;
			case 5:
				strFieldName = "City_Orgcod";
				break;
			case 6:
				strFieldName = "City_Orgname";
				break;
			case 7:
				strFieldName = "Town_Orgcod";
				break;
			case 8:
				strFieldName = "Town_Orgname";
				break;
			case 9:
				strFieldName = "Under_Orgcod";
				break;
			case 10:
				strFieldName = "Under_Orgname";
				break;
			case 11:
				strFieldName = "Org_Lvl";
				break;
			case 12:
				strFieldName = "Start_Dat";
				break;
			case 13:
				strFieldName = "End_Dat";
				break;
			case 14:
				strFieldName = "Status_Cod";
				break;
			case 15:
				strFieldName = "Status_Nam";
				break;
			case 16:
				strFieldName = "Date_Send";
				break;
			case 17:
				strFieldName = "StandByFlag1";
				break;
			case 18:
				strFieldName = "StandByFlag2";
				break;
			case 19:
				strFieldName = "StandByFlag3";
				break;
			case 20:
				strFieldName = "StandByFlag4";
				break;
			case 21:
				strFieldName = "StandByFlag5";
				break;
			case 22:
				strFieldName = "MakeDate";
				break;
			case 23:
				strFieldName = "MakeTime";
				break;
			case 24:
				strFieldName = "ModifyDate";
				break;
			case 25:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpAgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Comp_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Comp_Nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Prov_Orgcod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Prov_Orgname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("City_Orgcod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("City_Orgname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Town_Orgcod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Town_Orgname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Under_Orgcod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Under_Orgname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Org_Lvl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Start_Dat") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("End_Dat") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Status_Cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Status_Nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Date_Send") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByFlag1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByFlag2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByFlag3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByFlag4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByFlag5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
