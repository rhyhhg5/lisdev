/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLNoticeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LLNoticeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LLNoticeSchema implements Schema
{
    // @Field
    /** 通知号 */
    private String NoticeNo;
    /** 登记号码 */
    private String LogNo;
    /** 客户号 */
    private String CustomerNo;
    /** 客户姓名 */
    private String CustomerName;
    /** 关联客户类型 */
    private String CustomerType;
    /** 通知日期 */
    private Date NoticeDate;
    /** 通知事件 */
    private String NoticeTime;
    /** 通知主题 */
    private String NSubject;
    /** 通知内容 */
    private String NContent;
    /** 疾病代码 */
    private String DiseaseCode;
    /** 意外代码 */
    private String AccCode;
    /** 疾病描述 */
    private String DiseaseDesc;
    /** 意外描述 */
    private String AccDesc;
    /** 医院代码 */
    private String HospitalCode;
    /** 医院名称 */
    private String HospitalName;
    /** 入院日期 */
    private Date InHospitalDate;
    /** 出院日期 */
    private Date OutHospitalDate;
    /** 客户现状 */
    private String CustStatus;
    /** 调查报告标志 */
    private String SurveyFlag;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 25; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLNoticeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "NoticeNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getNoticeNo()
    {
        if (NoticeNo != null && !NoticeNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            NoticeNo = StrTool.unicodeToGBK(NoticeNo);
        }
        return NoticeNo;
    }

    public void setNoticeNo(String aNoticeNo)
    {
        NoticeNo = aNoticeNo;
    }

    public String getLogNo()
    {
        if (LogNo != null && !LogNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            LogNo = StrTool.unicodeToGBK(LogNo);
        }
        return LogNo;
    }

    public void setLogNo(String aLogNo)
    {
        LogNo = aLogNo;
    }

    public String getCustomerNo()
    {
        if (CustomerNo != null && !CustomerNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustomerNo = StrTool.unicodeToGBK(CustomerNo);
        }
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo)
    {
        CustomerNo = aCustomerNo;
    }

    public String getCustomerName()
    {
        if (CustomerName != null && !CustomerName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustomerName = StrTool.unicodeToGBK(CustomerName);
        }
        return CustomerName;
    }

    public void setCustomerName(String aCustomerName)
    {
        CustomerName = aCustomerName;
    }

    public String getCustomerType()
    {
        if (CustomerType != null && !CustomerType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustomerType = StrTool.unicodeToGBK(CustomerType);
        }
        return CustomerType;
    }

    public void setCustomerType(String aCustomerType)
    {
        CustomerType = aCustomerType;
    }

    public String getNoticeDate()
    {
        if (NoticeDate != null)
        {
            return fDate.getString(NoticeDate);
        }
        else
        {
            return null;
        }
    }

    public void setNoticeDate(Date aNoticeDate)
    {
        NoticeDate = aNoticeDate;
    }

    public void setNoticeDate(String aNoticeDate)
    {
        if (aNoticeDate != null && !aNoticeDate.equals(""))
        {
            NoticeDate = fDate.getDate(aNoticeDate);
        }
        else
        {
            NoticeDate = null;
        }
    }

    public String getNoticeTime()
    {
        if (NoticeTime != null && !NoticeTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            NoticeTime = StrTool.unicodeToGBK(NoticeTime);
        }
        return NoticeTime;
    }

    public void setNoticeTime(String aNoticeTime)
    {
        NoticeTime = aNoticeTime;
    }

    public String getNSubject()
    {
        if (NSubject != null && !NSubject.equals("") && SysConst.CHANGECHARSET == true)
        {
            NSubject = StrTool.unicodeToGBK(NSubject);
        }
        return NSubject;
    }

    public void setNSubject(String aNSubject)
    {
        NSubject = aNSubject;
    }

    public String getNContent()
    {
        if (NContent != null && !NContent.equals("") && SysConst.CHANGECHARSET == true)
        {
            NContent = StrTool.unicodeToGBK(NContent);
        }
        return NContent;
    }

    public void setNContent(String aNContent)
    {
        NContent = aNContent;
    }

    public String getDiseaseCode()
    {
        if (DiseaseCode != null && !DiseaseCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DiseaseCode = StrTool.unicodeToGBK(DiseaseCode);
        }
        return DiseaseCode;
    }

    public void setDiseaseCode(String aDiseaseCode)
    {
        DiseaseCode = aDiseaseCode;
    }

    public String getAccCode()
    {
        if (AccCode != null && !AccCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccCode = StrTool.unicodeToGBK(AccCode);
        }
        return AccCode;
    }

    public void setAccCode(String aAccCode)
    {
        AccCode = aAccCode;
    }

    public String getDiseaseDesc()
    {
        if (DiseaseDesc != null && !DiseaseDesc.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DiseaseDesc = StrTool.unicodeToGBK(DiseaseDesc);
        }
        return DiseaseDesc;
    }

    public void setDiseaseDesc(String aDiseaseDesc)
    {
        DiseaseDesc = aDiseaseDesc;
    }

    public String getAccDesc()
    {
        if (AccDesc != null && !AccDesc.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccDesc = StrTool.unicodeToGBK(AccDesc);
        }
        return AccDesc;
    }

    public void setAccDesc(String aAccDesc)
    {
        AccDesc = aAccDesc;
    }

    public String getHospitalCode()
    {
        if (HospitalCode != null && !HospitalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HospitalCode = StrTool.unicodeToGBK(HospitalCode);
        }
        return HospitalCode;
    }

    public void setHospitalCode(String aHospitalCode)
    {
        HospitalCode = aHospitalCode;
    }

    public String getHospitalName()
    {
        if (HospitalName != null && !HospitalName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HospitalName = StrTool.unicodeToGBK(HospitalName);
        }
        return HospitalName;
    }

    public void setHospitalName(String aHospitalName)
    {
        HospitalName = aHospitalName;
    }

    public String getInHospitalDate()
    {
        if (InHospitalDate != null)
        {
            return fDate.getString(InHospitalDate);
        }
        else
        {
            return null;
        }
    }

    public void setInHospitalDate(Date aInHospitalDate)
    {
        InHospitalDate = aInHospitalDate;
    }

    public void setInHospitalDate(String aInHospitalDate)
    {
        if (aInHospitalDate != null && !aInHospitalDate.equals(""))
        {
            InHospitalDate = fDate.getDate(aInHospitalDate);
        }
        else
        {
            InHospitalDate = null;
        }
    }

    public String getOutHospitalDate()
    {
        if (OutHospitalDate != null)
        {
            return fDate.getString(OutHospitalDate);
        }
        else
        {
            return null;
        }
    }

    public void setOutHospitalDate(Date aOutHospitalDate)
    {
        OutHospitalDate = aOutHospitalDate;
    }

    public void setOutHospitalDate(String aOutHospitalDate)
    {
        if (aOutHospitalDate != null && !aOutHospitalDate.equals(""))
        {
            OutHospitalDate = fDate.getDate(aOutHospitalDate);
        }
        else
        {
            OutHospitalDate = null;
        }
    }

    public String getCustStatus()
    {
        if (CustStatus != null && !CustStatus.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustStatus = StrTool.unicodeToGBK(CustStatus);
        }
        return CustStatus;
    }

    public void setCustStatus(String aCustStatus)
    {
        CustStatus = aCustStatus;
    }

    public String getSurveyFlag()
    {
        if (SurveyFlag != null && !SurveyFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SurveyFlag = StrTool.unicodeToGBK(SurveyFlag);
        }
        return SurveyFlag;
    }

    public void setSurveyFlag(String aSurveyFlag)
    {
        SurveyFlag = aSurveyFlag;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLNoticeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLNoticeSchema aLLNoticeSchema)
    {
        this.NoticeNo = aLLNoticeSchema.getNoticeNo();
        this.LogNo = aLLNoticeSchema.getLogNo();
        this.CustomerNo = aLLNoticeSchema.getCustomerNo();
        this.CustomerName = aLLNoticeSchema.getCustomerName();
        this.CustomerType = aLLNoticeSchema.getCustomerType();
        this.NoticeDate = fDate.getDate(aLLNoticeSchema.getNoticeDate());
        this.NoticeTime = aLLNoticeSchema.getNoticeTime();
        this.NSubject = aLLNoticeSchema.getNSubject();
        this.NContent = aLLNoticeSchema.getNContent();
        this.DiseaseCode = aLLNoticeSchema.getDiseaseCode();
        this.AccCode = aLLNoticeSchema.getAccCode();
        this.DiseaseDesc = aLLNoticeSchema.getDiseaseDesc();
        this.AccDesc = aLLNoticeSchema.getAccDesc();
        this.HospitalCode = aLLNoticeSchema.getHospitalCode();
        this.HospitalName = aLLNoticeSchema.getHospitalName();
        this.InHospitalDate = fDate.getDate(aLLNoticeSchema.getInHospitalDate());
        this.OutHospitalDate = fDate.getDate(aLLNoticeSchema.getOutHospitalDate());
        this.CustStatus = aLLNoticeSchema.getCustStatus();
        this.SurveyFlag = aLLNoticeSchema.getSurveyFlag();
        this.Remark = aLLNoticeSchema.getRemark();
        this.Operator = aLLNoticeSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLNoticeSchema.getMakeDate());
        this.MakeTime = aLLNoticeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLNoticeSchema.getModifyDate());
        this.ModifyTime = aLLNoticeSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("NoticeNo") == null)
            {
                this.NoticeNo = null;
            }
            else
            {
                this.NoticeNo = rs.getString("NoticeNo").trim();
            }

            if (rs.getString("LogNo") == null)
            {
                this.LogNo = null;
            }
            else
            {
                this.LogNo = rs.getString("LogNo").trim();
            }

            if (rs.getString("CustomerNo") == null)
            {
                this.CustomerNo = null;
            }
            else
            {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("CustomerName") == null)
            {
                this.CustomerName = null;
            }
            else
            {
                this.CustomerName = rs.getString("CustomerName").trim();
            }

            if (rs.getString("CustomerType") == null)
            {
                this.CustomerType = null;
            }
            else
            {
                this.CustomerType = rs.getString("CustomerType").trim();
            }

            this.NoticeDate = rs.getDate("NoticeDate");
            if (rs.getString("NoticeTime") == null)
            {
                this.NoticeTime = null;
            }
            else
            {
                this.NoticeTime = rs.getString("NoticeTime").trim();
            }

            if (rs.getString("NSubject") == null)
            {
                this.NSubject = null;
            }
            else
            {
                this.NSubject = rs.getString("NSubject").trim();
            }

            if (rs.getString("NContent") == null)
            {
                this.NContent = null;
            }
            else
            {
                this.NContent = rs.getString("NContent").trim();
            }

            if (rs.getString("DiseaseCode") == null)
            {
                this.DiseaseCode = null;
            }
            else
            {
                this.DiseaseCode = rs.getString("DiseaseCode").trim();
            }

            if (rs.getString("AccCode") == null)
            {
                this.AccCode = null;
            }
            else
            {
                this.AccCode = rs.getString("AccCode").trim();
            }

            if (rs.getString("DiseaseDesc") == null)
            {
                this.DiseaseDesc = null;
            }
            else
            {
                this.DiseaseDesc = rs.getString("DiseaseDesc").trim();
            }

            if (rs.getString("AccDesc") == null)
            {
                this.AccDesc = null;
            }
            else
            {
                this.AccDesc = rs.getString("AccDesc").trim();
            }

            if (rs.getString("HospitalCode") == null)
            {
                this.HospitalCode = null;
            }
            else
            {
                this.HospitalCode = rs.getString("HospitalCode").trim();
            }

            if (rs.getString("HospitalName") == null)
            {
                this.HospitalName = null;
            }
            else
            {
                this.HospitalName = rs.getString("HospitalName").trim();
            }

            this.InHospitalDate = rs.getDate("InHospitalDate");
            this.OutHospitalDate = rs.getDate("OutHospitalDate");
            if (rs.getString("CustStatus") == null)
            {
                this.CustStatus = null;
            }
            else
            {
                this.CustStatus = rs.getString("CustStatus").trim();
            }

            if (rs.getString("SurveyFlag") == null)
            {
                this.SurveyFlag = null;
            }
            else
            {
                this.SurveyFlag = rs.getString("SurveyFlag").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLNoticeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLNoticeSchema getSchema()
    {
        LLNoticeSchema aLLNoticeSchema = new LLNoticeSchema();
        aLLNoticeSchema.setSchema(this);
        return aLLNoticeSchema;
    }

    public LLNoticeDB getDB()
    {
        LLNoticeDB aDBOper = new LLNoticeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLNotice描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(NoticeNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(LogNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustomerNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustomerName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustomerType)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            NoticeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NoticeTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NSubject)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NContent)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DiseaseCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DiseaseDesc)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccDesc)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HospitalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HospitalName)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            InHospitalDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            OutHospitalDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustStatus)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SurveyFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLNotice>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            NoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            LogNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            CustomerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            NoticeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            NoticeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            NSubject = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            NContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            DiseaseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            AccCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            DiseaseDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                         SysConst.PACKAGESPILTER);
            AccDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                          SysConst.PACKAGESPILTER);
            HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                          SysConst.PACKAGESPILTER);
            InHospitalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            OutHospitalDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            CustStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
            SurveyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 24, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLNoticeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("NoticeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NoticeNo));
        }
        if (FCode.equals("LogNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(LogNo));
        }
        if (FCode.equals("CustomerNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustomerNo));
        }
        if (FCode.equals("CustomerName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustomerName));
        }
        if (FCode.equals("CustomerType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustomerType));
        }
        if (FCode.equals("NoticeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getNoticeDate()));
        }
        if (FCode.equals("NoticeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NoticeTime));
        }
        if (FCode.equals("NSubject"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NSubject));
        }
        if (FCode.equals("NContent"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NContent));
        }
        if (FCode.equals("DiseaseCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DiseaseCode));
        }
        if (FCode.equals("AccCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccCode));
        }
        if (FCode.equals("DiseaseDesc"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DiseaseDesc));
        }
        if (FCode.equals("AccDesc"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccDesc));
        }
        if (FCode.equals("HospitalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HospitalCode));
        }
        if (FCode.equals("HospitalName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HospitalName));
        }
        if (FCode.equals("InHospitalDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getInHospitalDate()));
        }
        if (FCode.equals("OutHospitalDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getOutHospitalDate()));
        }
        if (FCode.equals("CustStatus"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustStatus));
        }
        if (FCode.equals("SurveyFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SurveyFlag));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(NoticeNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(LogNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CustomerName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CustomerType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getNoticeDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(NoticeTime);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(NSubject);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(NContent);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(DiseaseCode);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AccCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(DiseaseDesc);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(AccDesc);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(HospitalCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(HospitalName);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInHospitalDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getOutHospitalDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(CustStatus);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(SurveyFlag);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("NoticeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NoticeNo = FValue.trim();
            }
            else
            {
                NoticeNo = null;
            }
        }
        if (FCode.equals("LogNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LogNo = FValue.trim();
            }
            else
            {
                LogNo = null;
            }
        }
        if (FCode.equals("CustomerNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
            {
                CustomerNo = null;
            }
        }
        if (FCode.equals("CustomerName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerName = FValue.trim();
            }
            else
            {
                CustomerName = null;
            }
        }
        if (FCode.equals("CustomerType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerType = FValue.trim();
            }
            else
            {
                CustomerType = null;
            }
        }
        if (FCode.equals("NoticeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NoticeDate = fDate.getDate(FValue);
            }
            else
            {
                NoticeDate = null;
            }
        }
        if (FCode.equals("NoticeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NoticeTime = FValue.trim();
            }
            else
            {
                NoticeTime = null;
            }
        }
        if (FCode.equals("NSubject"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NSubject = FValue.trim();
            }
            else
            {
                NSubject = null;
            }
        }
        if (FCode.equals("NContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NContent = FValue.trim();
            }
            else
            {
                NContent = null;
            }
        }
        if (FCode.equals("DiseaseCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DiseaseCode = FValue.trim();
            }
            else
            {
                DiseaseCode = null;
            }
        }
        if (FCode.equals("AccCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccCode = FValue.trim();
            }
            else
            {
                AccCode = null;
            }
        }
        if (FCode.equals("DiseaseDesc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DiseaseDesc = FValue.trim();
            }
            else
            {
                DiseaseDesc = null;
            }
        }
        if (FCode.equals("AccDesc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccDesc = FValue.trim();
            }
            else
            {
                AccDesc = null;
            }
        }
        if (FCode.equals("HospitalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HospitalCode = FValue.trim();
            }
            else
            {
                HospitalCode = null;
            }
        }
        if (FCode.equals("HospitalName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HospitalName = FValue.trim();
            }
            else
            {
                HospitalName = null;
            }
        }
        if (FCode.equals("InHospitalDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InHospitalDate = fDate.getDate(FValue);
            }
            else
            {
                InHospitalDate = null;
            }
        }
        if (FCode.equals("OutHospitalDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OutHospitalDate = fDate.getDate(FValue);
            }
            else
            {
                OutHospitalDate = null;
            }
        }
        if (FCode.equals("CustStatus"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustStatus = FValue.trim();
            }
            else
            {
                CustStatus = null;
            }
        }
        if (FCode.equals("SurveyFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SurveyFlag = FValue.trim();
            }
            else
            {
                SurveyFlag = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLNoticeSchema other = (LLNoticeSchema) otherObject;
        return
                NoticeNo.equals(other.getNoticeNo())
                && LogNo.equals(other.getLogNo())
                && CustomerNo.equals(other.getCustomerNo())
                && CustomerName.equals(other.getCustomerName())
                && CustomerType.equals(other.getCustomerType())
                && fDate.getString(NoticeDate).equals(other.getNoticeDate())
                && NoticeTime.equals(other.getNoticeTime())
                && NSubject.equals(other.getNSubject())
                && NContent.equals(other.getNContent())
                && DiseaseCode.equals(other.getDiseaseCode())
                && AccCode.equals(other.getAccCode())
                && DiseaseDesc.equals(other.getDiseaseDesc())
                && AccDesc.equals(other.getAccDesc())
                && HospitalCode.equals(other.getHospitalCode())
                && HospitalName.equals(other.getHospitalName())
                &&
                fDate.getString(InHospitalDate).equals(other.getInHospitalDate())
                &&
                fDate.getString(OutHospitalDate).equals(other.
                getOutHospitalDate())
                && CustStatus.equals(other.getCustStatus())
                && SurveyFlag.equals(other.getSurveyFlag())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("NoticeNo"))
        {
            return 0;
        }
        if (strFieldName.equals("LogNo"))
        {
            return 1;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return 2;
        }
        if (strFieldName.equals("CustomerName"))
        {
            return 3;
        }
        if (strFieldName.equals("CustomerType"))
        {
            return 4;
        }
        if (strFieldName.equals("NoticeDate"))
        {
            return 5;
        }
        if (strFieldName.equals("NoticeTime"))
        {
            return 6;
        }
        if (strFieldName.equals("NSubject"))
        {
            return 7;
        }
        if (strFieldName.equals("NContent"))
        {
            return 8;
        }
        if (strFieldName.equals("DiseaseCode"))
        {
            return 9;
        }
        if (strFieldName.equals("AccCode"))
        {
            return 10;
        }
        if (strFieldName.equals("DiseaseDesc"))
        {
            return 11;
        }
        if (strFieldName.equals("AccDesc"))
        {
            return 12;
        }
        if (strFieldName.equals("HospitalCode"))
        {
            return 13;
        }
        if (strFieldName.equals("HospitalName"))
        {
            return 14;
        }
        if (strFieldName.equals("InHospitalDate"))
        {
            return 15;
        }
        if (strFieldName.equals("OutHospitalDate"))
        {
            return 16;
        }
        if (strFieldName.equals("CustStatus"))
        {
            return 17;
        }
        if (strFieldName.equals("SurveyFlag"))
        {
            return 18;
        }
        if (strFieldName.equals("Remark"))
        {
            return 19;
        }
        if (strFieldName.equals("Operator"))
        {
            return 20;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 21;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 22;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 23;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 24;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "NoticeNo";
                break;
            case 1:
                strFieldName = "LogNo";
                break;
            case 2:
                strFieldName = "CustomerNo";
                break;
            case 3:
                strFieldName = "CustomerName";
                break;
            case 4:
                strFieldName = "CustomerType";
                break;
            case 5:
                strFieldName = "NoticeDate";
                break;
            case 6:
                strFieldName = "NoticeTime";
                break;
            case 7:
                strFieldName = "NSubject";
                break;
            case 8:
                strFieldName = "NContent";
                break;
            case 9:
                strFieldName = "DiseaseCode";
                break;
            case 10:
                strFieldName = "AccCode";
                break;
            case 11:
                strFieldName = "DiseaseDesc";
                break;
            case 12:
                strFieldName = "AccDesc";
                break;
            case 13:
                strFieldName = "HospitalCode";
                break;
            case 14:
                strFieldName = "HospitalName";
                break;
            case 15:
                strFieldName = "InHospitalDate";
                break;
            case 16:
                strFieldName = "OutHospitalDate";
                break;
            case 17:
                strFieldName = "CustStatus";
                break;
            case 18:
                strFieldName = "SurveyFlag";
                break;
            case 19:
                strFieldName = "Remark";
                break;
            case 20:
                strFieldName = "Operator";
                break;
            case 21:
                strFieldName = "MakeDate";
                break;
            case 22:
                strFieldName = "MakeTime";
                break;
            case 23:
                strFieldName = "ModifyDate";
                break;
            case 24:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("NoticeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LogNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NoticeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("NoticeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NSubject"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DiseaseCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DiseaseDesc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccDesc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HospitalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HospitalName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InHospitalDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("OutHospitalDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CustStatus"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SurveyFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
