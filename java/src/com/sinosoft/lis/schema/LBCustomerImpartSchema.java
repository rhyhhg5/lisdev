/*
 * <p>ClassName: LBCustomerImpartSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 修改主键
 * @CreateDate：2005-01-12
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LBCustomerImpartDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LBCustomerImpartSchema implements Schema
{
    // @Field
    /** 批单号 */
    private String EdorNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 合同号码 */
    private String ContNo;
    /** 总单投保单号码 */
    private String ProposalContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 告知编码 */
    private String ImpartCode;
    /** 告知版别 */
    private String ImpartVer;
    /** 告知内容 */
    private String ImpartContent;
    /** 告知参数展现模版 */
    private String ImpartParamModle;
    /** 客户号码 */
    private String CustomerNo;
    /** 客户号码类型 */
    private String CustomerNoType;
    /** 是否参与核保核赔标志 */
    private String UWClaimFlg;
    /** 打印标志 */
    private String PrtFlag;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 18; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LBCustomerImpartSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "EdorNo";
        pk[1] = "ProposalContNo";
        pk[2] = "ImpartCode";
        pk[3] = "ImpartVer";
        pk[4] = "CustomerNo";
        pk[5] = "CustomerNoType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorNo()
    {
        if (EdorNo != null && !EdorNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getProposalContNo()
    {
        if (ProposalContNo != null && !ProposalContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ProposalContNo = StrTool.unicodeToGBK(ProposalContNo);
        }
        return ProposalContNo;
    }

    public void setProposalContNo(String aProposalContNo)
    {
        ProposalContNo = aProposalContNo;
    }

    public String getPrtNo()
    {
        if (PrtNo != null && !PrtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtNo = StrTool.unicodeToGBK(PrtNo);
        }
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo)
    {
        PrtNo = aPrtNo;
    }

    public String getImpartCode()
    {
        if (ImpartCode != null && !ImpartCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartCode = StrTool.unicodeToGBK(ImpartCode);
        }
        return ImpartCode;
    }

    public void setImpartCode(String aImpartCode)
    {
        ImpartCode = aImpartCode;
    }

    public String getImpartVer()
    {
        if (ImpartVer != null && !ImpartVer.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartVer = StrTool.unicodeToGBK(ImpartVer);
        }
        return ImpartVer;
    }

    public void setImpartVer(String aImpartVer)
    {
        ImpartVer = aImpartVer;
    }

    public String getImpartContent()
    {
        if (ImpartContent != null && !ImpartContent.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartContent = StrTool.unicodeToGBK(ImpartContent);
        }
        return ImpartContent;
    }

    public void setImpartContent(String aImpartContent)
    {
        ImpartContent = aImpartContent;
    }

    public String getImpartParamModle()
    {
        if (ImpartParamModle != null && !ImpartParamModle.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartParamModle = StrTool.unicodeToGBK(ImpartParamModle);
        }
        return ImpartParamModle;
    }

    public void setImpartParamModle(String aImpartParamModle)
    {
        ImpartParamModle = aImpartParamModle;
    }

    public String getCustomerNo()
    {
        if (CustomerNo != null && !CustomerNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustomerNo = StrTool.unicodeToGBK(CustomerNo);
        }
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo)
    {
        CustomerNo = aCustomerNo;
    }

    public String getCustomerNoType()
    {
        if (CustomerNoType != null && !CustomerNoType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustomerNoType = StrTool.unicodeToGBK(CustomerNoType);
        }
        return CustomerNoType;
    }

    public void setCustomerNoType(String aCustomerNoType)
    {
        CustomerNoType = aCustomerNoType;
    }

    public String getUWClaimFlg()
    {
        if (UWClaimFlg != null && !UWClaimFlg.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UWClaimFlg = StrTool.unicodeToGBK(UWClaimFlg);
        }
        return UWClaimFlg;
    }

    public void setUWClaimFlg(String aUWClaimFlg)
    {
        UWClaimFlg = aUWClaimFlg;
    }

    public String getPrtFlag()
    {
        if (PrtFlag != null && !PrtFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtFlag = StrTool.unicodeToGBK(PrtFlag);
        }
        return PrtFlag;
    }

    public void setPrtFlag(String aPrtFlag)
    {
        PrtFlag = aPrtFlag;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LBCustomerImpartSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LBCustomerImpartSchema aLBCustomerImpartSchema)
    {
        this.EdorNo = aLBCustomerImpartSchema.getEdorNo();
        this.GrpContNo = aLBCustomerImpartSchema.getGrpContNo();
        this.ContNo = aLBCustomerImpartSchema.getContNo();
        this.ProposalContNo = aLBCustomerImpartSchema.getProposalContNo();
        this.PrtNo = aLBCustomerImpartSchema.getPrtNo();
        this.ImpartCode = aLBCustomerImpartSchema.getImpartCode();
        this.ImpartVer = aLBCustomerImpartSchema.getImpartVer();
        this.ImpartContent = aLBCustomerImpartSchema.getImpartContent();
        this.ImpartParamModle = aLBCustomerImpartSchema.getImpartParamModle();
        this.CustomerNo = aLBCustomerImpartSchema.getCustomerNo();
        this.CustomerNoType = aLBCustomerImpartSchema.getCustomerNoType();
        this.UWClaimFlg = aLBCustomerImpartSchema.getUWClaimFlg();
        this.PrtFlag = aLBCustomerImpartSchema.getPrtFlag();
        this.Operator = aLBCustomerImpartSchema.getOperator();
        this.MakeDate = fDate.getDate(aLBCustomerImpartSchema.getMakeDate());
        this.MakeTime = aLBCustomerImpartSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLBCustomerImpartSchema.getModifyDate());
        this.ModifyTime = aLBCustomerImpartSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("ProposalContNo") == null)
            {
                this.ProposalContNo = null;
            }
            else
            {
                this.ProposalContNo = rs.getString("ProposalContNo").trim();
            }

            if (rs.getString("PrtNo") == null)
            {
                this.PrtNo = null;
            }
            else
            {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("ImpartCode") == null)
            {
                this.ImpartCode = null;
            }
            else
            {
                this.ImpartCode = rs.getString("ImpartCode").trim();
            }

            if (rs.getString("ImpartVer") == null)
            {
                this.ImpartVer = null;
            }
            else
            {
                this.ImpartVer = rs.getString("ImpartVer").trim();
            }

            if (rs.getString("ImpartContent") == null)
            {
                this.ImpartContent = null;
            }
            else
            {
                this.ImpartContent = rs.getString("ImpartContent").trim();
            }

            if (rs.getString("ImpartParamModle") == null)
            {
                this.ImpartParamModle = null;
            }
            else
            {
                this.ImpartParamModle = rs.getString("ImpartParamModle").trim();
            }

            if (rs.getString("CustomerNo") == null)
            {
                this.CustomerNo = null;
            }
            else
            {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("CustomerNoType") == null)
            {
                this.CustomerNoType = null;
            }
            else
            {
                this.CustomerNoType = rs.getString("CustomerNoType").trim();
            }

            if (rs.getString("UWClaimFlg") == null)
            {
                this.UWClaimFlg = null;
            }
            else
            {
                this.UWClaimFlg = rs.getString("UWClaimFlg").trim();
            }

            if (rs.getString("PrtFlag") == null)
            {
                this.PrtFlag = null;
            }
            else
            {
                this.PrtFlag = rs.getString("PrtFlag").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBCustomerImpartSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LBCustomerImpartSchema getSchema()
    {
        LBCustomerImpartSchema aLBCustomerImpartSchema = new
                LBCustomerImpartSchema();
        aLBCustomerImpartSchema.setSchema(this);
        return aLBCustomerImpartSchema;
    }

    public LBCustomerImpartDB getDB()
    {
        LBCustomerImpartDB aDBOper = new LBCustomerImpartDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBCustomerImpart描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProposalContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ImpartCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ImpartVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ImpartContent)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ImpartParamModle)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustomerNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustomerNoType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWClaimFlg)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBCustomerImpart>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            ProposalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                            SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
            ImpartCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            ImpartVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            ImpartContent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            ImpartParamModle = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              9, SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            CustomerNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            11, SysConst.PACKAGESPILTER);
            UWClaimFlg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            PrtFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBCustomerImpartSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorNo));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("ProposalContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProposalContNo));
        }
        if (FCode.equals("PrtNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtNo));
        }
        if (FCode.equals("ImpartCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ImpartCode));
        }
        if (FCode.equals("ImpartVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ImpartVer));
        }
        if (FCode.equals("ImpartContent"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ImpartContent));
        }
        if (FCode.equals("ImpartParamModle"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ImpartParamModle));
        }
        if (FCode.equals("CustomerNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustomerNo));
        }
        if (FCode.equals("CustomerNoType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustomerNoType));
        }
        if (FCode.equals("UWClaimFlg"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWClaimFlg));
        }
        if (FCode.equals("PrtFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtFlag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ProposalContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ImpartCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ImpartVer);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ImpartContent);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ImpartParamModle);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(CustomerNoType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(UWClaimFlg);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(PrtFlag);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("ProposalContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProposalContNo = FValue.trim();
            }
            else
            {
                ProposalContNo = null;
            }
        }
        if (FCode.equals("PrtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
            {
                PrtNo = null;
            }
        }
        if (FCode.equals("ImpartCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartCode = FValue.trim();
            }
            else
            {
                ImpartCode = null;
            }
        }
        if (FCode.equals("ImpartVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartVer = FValue.trim();
            }
            else
            {
                ImpartVer = null;
            }
        }
        if (FCode.equals("ImpartContent"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartContent = FValue.trim();
            }
            else
            {
                ImpartContent = null;
            }
        }
        if (FCode.equals("ImpartParamModle"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartParamModle = FValue.trim();
            }
            else
            {
                ImpartParamModle = null;
            }
        }
        if (FCode.equals("CustomerNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
            {
                CustomerNo = null;
            }
        }
        if (FCode.equals("CustomerNoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNoType = FValue.trim();
            }
            else
            {
                CustomerNoType = null;
            }
        }
        if (FCode.equals("UWClaimFlg"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWClaimFlg = FValue.trim();
            }
            else
            {
                UWClaimFlg = null;
            }
        }
        if (FCode.equals("PrtFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtFlag = FValue.trim();
            }
            else
            {
                PrtFlag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LBCustomerImpartSchema other = (LBCustomerImpartSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && GrpContNo.equals(other.getGrpContNo())
                && ContNo.equals(other.getContNo())
                && ProposalContNo.equals(other.getProposalContNo())
                && PrtNo.equals(other.getPrtNo())
                && ImpartCode.equals(other.getImpartCode())
                && ImpartVer.equals(other.getImpartVer())
                && ImpartContent.equals(other.getImpartContent())
                && ImpartParamModle.equals(other.getImpartParamModle())
                && CustomerNo.equals(other.getCustomerNo())
                && CustomerNoType.equals(other.getCustomerNoType())
                && UWClaimFlg.equals(other.getUWClaimFlg())
                && PrtFlag.equals(other.getPrtFlag())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 2;
        }
        if (strFieldName.equals("ProposalContNo"))
        {
            return 3;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return 4;
        }
        if (strFieldName.equals("ImpartCode"))
        {
            return 5;
        }
        if (strFieldName.equals("ImpartVer"))
        {
            return 6;
        }
        if (strFieldName.equals("ImpartContent"))
        {
            return 7;
        }
        if (strFieldName.equals("ImpartParamModle"))
        {
            return 8;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return 9;
        }
        if (strFieldName.equals("CustomerNoType"))
        {
            return 10;
        }
        if (strFieldName.equals("UWClaimFlg"))
        {
            return 11;
        }
        if (strFieldName.equals("PrtFlag"))
        {
            return 12;
        }
        if (strFieldName.equals("Operator"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 14;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 16;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 17;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "GrpContNo";
                break;
            case 2:
                strFieldName = "ContNo";
                break;
            case 3:
                strFieldName = "ProposalContNo";
                break;
            case 4:
                strFieldName = "PrtNo";
                break;
            case 5:
                strFieldName = "ImpartCode";
                break;
            case 6:
                strFieldName = "ImpartVer";
                break;
            case 7:
                strFieldName = "ImpartContent";
                break;
            case 8:
                strFieldName = "ImpartParamModle";
                break;
            case 9:
                strFieldName = "CustomerNo";
                break;
            case 10:
                strFieldName = "CustomerNoType";
                break;
            case 11:
                strFieldName = "UWClaimFlg";
                break;
            case 12:
                strFieldName = "PrtFlag";
                break;
            case 13:
                strFieldName = "Operator";
                break;
            case 14:
                strFieldName = "MakeDate";
                break;
            case 15:
                strFieldName = "MakeTime";
                break;
            case 16:
                strFieldName = "ModifyDate";
                break;
            case 17:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ImpartCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ImpartVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ImpartContent"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ImpartParamModle"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWClaimFlg"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
