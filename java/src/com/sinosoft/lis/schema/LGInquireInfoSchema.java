/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import com.sinosoft.lis.db.LGInquireInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;


/*
 * <p>ClassName: LGInquireInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 工单管理
 * @CreateDate：2005-04-04
 */
public class LGInquireInfoSchema implements Schema
{
  // @Field
  /** 咨询编号 */
  private String InquireNo;


  /** 机构 */
  private String ManageCom;


  /** 姓名 */
  private String Name;


  /** 性别 */
  private String Sex;


  /** 电子邮件 */
  private String Email;


  /** 联系电话 */
  private String Phone;


  /** 主题项目 */
  private String Title;


  /** 详细内容 */
  private String Content;


  /** 操作员代码 */
  private String Operator;


  /** 入机日期 */
  private Date MakeDate;


  /** 入机时间 */
  private String MakeTime;


  /** 最后一次修改日期 */
  private Date ModifyDate;


  /** 最后一次修改时间 */
  private String ModifyTime;

  public static final int FIELDNUM = 13; // 数据库表的字段个数

  private static String[] PK; // 主键

  private FDate fDate = new FDate(); // 处理日期

  public CErrors mErrors; // 错误信息


  // @Constructor
  public LGInquireInfoSchema()
  {
    mErrors = new CErrors();

    String[] pk = new String[1];
    pk[0] = "InquireNo";

    PK = pk;
  }


  // @Method
  public String[] getPK()
  {
    return PK;
  }

  public String getInquireNo()
  {
    if (SysConst.CHANGECHARSET && InquireNo != null && !InquireNo.equals(""))
    {
      InquireNo = StrTool.unicodeToGBK(InquireNo);
    }
    return InquireNo;
  }

  public void setInquireNo(String aInquireNo)
  {
    InquireNo = aInquireNo;
  }

  public String getManageCom()
  {
    if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
    {
      ManageCom = StrTool.unicodeToGBK(ManageCom);
    }
    return ManageCom;
  }

  public void setManageCom(String aManageCom)
  {
    ManageCom = aManageCom;
  }

  public String getName()
  {
    if (SysConst.CHANGECHARSET && Name != null && !Name.equals(""))
    {
      Name = StrTool.unicodeToGBK(Name);
    }
    return Name;
  }

  public void setName(String aName)
  {
    Name = aName;
  }

  public String getSex()
  {
    if (SysConst.CHANGECHARSET && Sex != null && !Sex.equals(""))
    {
      Sex = StrTool.unicodeToGBK(Sex);
    }
    return Sex;
  }

  public void setSex(String aSex)
  {
    Sex = aSex;
  }

  public String getEmail()
  {
    if (SysConst.CHANGECHARSET && Email != null && !Email.equals(""))
    {
      Email = StrTool.unicodeToGBK(Email);
    }
    return Email;
  }

  public void setEmail(String aEmail)
  {
    Email = aEmail;
  }

  public String getPhone()
  {
    if (SysConst.CHANGECHARSET && Phone != null && !Phone.equals(""))
    {
      Phone = StrTool.unicodeToGBK(Phone);
    }
    return Phone;
  }

  public void setPhone(String aPhone)
  {
    Phone = aPhone;
  }

  public String getTitle()
  {
    if (SysConst.CHANGECHARSET && Title != null && !Title.equals(""))
    {
      Title = StrTool.unicodeToGBK(Title);
    }
    return Title;
  }

  public void setTitle(String aTitle)
  {
    Title = aTitle;
  }

  public String getContent()
  {
    if (SysConst.CHANGECHARSET && Content != null && !Content.equals(""))
    {
      Content = StrTool.unicodeToGBK(Content);
    }
    return Content;
  }

  public void setContent(String aContent)
  {
    Content = aContent;
  }

  public String getOperator()
  {
    if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
    {
      Operator = StrTool.unicodeToGBK(Operator);
    }
    return Operator;
  }

  public void setOperator(String aOperator)
  {
    Operator = aOperator;
  }

  public String getMakeDate()
  {
    if (MakeDate != null)
    {
      return fDate.getString(MakeDate);
    }
    else
    {
      return null;
    }
  }

  public void setMakeDate(Date aMakeDate)
  {
    MakeDate = aMakeDate;
  }

  public void setMakeDate(String aMakeDate)
  {
    if (aMakeDate != null && !aMakeDate.equals(""))
    {
      MakeDate = fDate.getDate(aMakeDate);
    }
    else
    {
      MakeDate = null;
    }
  }

  public String getMakeTime()
  {
    if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
    {
      MakeTime = StrTool.unicodeToGBK(MakeTime);
    }
    return MakeTime;
  }

  public void setMakeTime(String aMakeTime)
  {
    MakeTime = aMakeTime;
  }

  public String getModifyDate()
  {
    if (ModifyDate != null)
    {
      return fDate.getString(ModifyDate);
    }
    else
    {
      return null;
    }
  }

  public void setModifyDate(Date aModifyDate)
  {
    ModifyDate = aModifyDate;
  }

  public void setModifyDate(String aModifyDate)
  {
    if (aModifyDate != null && !aModifyDate.equals(""))
    {
      ModifyDate = fDate.getDate(aModifyDate);
    }
    else
    {
      ModifyDate = null;
    }
  }

  public String getModifyTime()
  {
    if (SysConst.CHANGECHARSET && ModifyTime != null && !ModifyTime.equals(""))
    {
      ModifyTime = StrTool.unicodeToGBK(ModifyTime);
    }
    return ModifyTime;
  }

  public void setModifyTime(String aModifyTime)
  {
    ModifyTime = aModifyTime;
  }


  /**
   * 使用另外一个 LGInquireInfoSchema 对象给 Schema 赋值
   * @param: aLGInquireInfoSchema LGInquireInfoSchema
   **/
  public void setSchema(LGInquireInfoSchema aLGInquireInfoSchema)
  {
    this.InquireNo = aLGInquireInfoSchema.getInquireNo();
    this.ManageCom = aLGInquireInfoSchema.getManageCom();
    this.Name = aLGInquireInfoSchema.getName();
    this.Sex = aLGInquireInfoSchema.getSex();
    this.Email = aLGInquireInfoSchema.getEmail();
    this.Phone = aLGInquireInfoSchema.getPhone();
    this.Title = aLGInquireInfoSchema.getTitle();
    this.Content = aLGInquireInfoSchema.getContent();
    this.Operator = aLGInquireInfoSchema.getOperator();
    this.MakeDate = fDate.getDate(aLGInquireInfoSchema.getMakeDate());
    this.MakeTime = aLGInquireInfoSchema.getMakeTime();
    this.ModifyDate = fDate.getDate(aLGInquireInfoSchema.getModifyDate());
    this.ModifyTime = aLGInquireInfoSchema.getModifyTime();
  }


  /**
   * 使用 ResultSet 中的第 i 行给 Schema 赋值
   * @param: rs ResultSet
   * @param: i int
   * @return: boolean
   **/
  public boolean setSchema(ResultSet rs, int i)
  {
    try
    {
      //rs.absolute(i);		// 非滚动游标
      if (rs.getString("InquireNo") == null)
      {
        this.InquireNo = null;
      }
      else
      {
        this.InquireNo = rs.getString("InquireNo").trim();
      }

      if (rs.getString("ManageCom") == null)
      {
        this.ManageCom = null;
      }
      else
      {
        this.ManageCom = rs.getString("ManageCom").trim();
      }

      if (rs.getString("Name") == null)
      {
        this.Name = null;
      }
      else
      {
        this.Name = rs.getString("Name").trim();
      }

      if (rs.getString("Sex") == null)
      {
        this.Sex = null;
      }
      else
      {
        this.Sex = rs.getString("Sex").trim();
      }

      if (rs.getString("Email") == null)
      {
        this.Email = null;
      }
      else
      {
        this.Email = rs.getString("Email").trim();
      }

      if (rs.getString("Phone") == null)
      {
        this.Phone = null;
      }
      else
      {
        this.Phone = rs.getString("Phone").trim();
      }

      if (rs.getString("Title") == null)
      {
        this.Title = null;
      }
      else
      {
        this.Title = rs.getString("Title").trim();
      }

      if (rs.getString("Content") == null)
      {
        this.Content = null;
      }
      else
      {
        this.Content = rs.getString("Content").trim();
      }

      if (rs.getString("Operator") == null)
      {
        this.Operator = null;
      }
      else
      {
        this.Operator = rs.getString("Operator").trim();
      }

      this.MakeDate = rs.getDate("MakeDate");
      if (rs.getString("MakeTime") == null)
      {
        this.MakeTime = null;
      }
      else
      {
        this.MakeTime = rs.getString("MakeTime").trim();
      }

      this.ModifyDate = rs.getDate("ModifyDate");
      if (rs.getString("ModifyTime") == null)
      {
        this.ModifyTime = null;
      }
      else
      {
        this.ModifyTime = rs.getString("ModifyTime").trim();
      }

    }
    catch (SQLException sqle)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LGInquireInfoSchema";
      tError.functionName = "setSchema";
      tError.errorMessage = sqle.toString();
      this.mErrors.addOneError(tError);

      return false;
    }
    return true;
  }

  public LGInquireInfoSchema getSchema()
  {
    LGInquireInfoSchema aLGInquireInfoSchema = new LGInquireInfoSchema();
    aLGInquireInfoSchema.setSchema(this);
    return aLGInquireInfoSchema;
  }

  public LGInquireInfoDB getDB()
  {
    LGInquireInfoDB aDBOper = new LGInquireInfoDB();
    aDBOper.setSchema(this);
    return aDBOper;
  }


  /**
   * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGInquireInfo描述/A>表字段
   * @return: String 返回打包后字符串
   **/
  public String encode()
  {
    StringBuffer strReturn = new StringBuffer(256);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(InquireNo)));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Name)));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Sex)));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Email)));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Phone)));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Title)));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Content)));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
        MakeDate))));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
        ModifyDate))));
    strReturn.append(SysConst.PACKAGESPILTER);
    strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
    return strReturn.toString();
  }


  /**
   * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGInquireInfo>历史记账凭证主表信息</A>表字段
   * @param: strMessage String 包含一条纪录数据的字符串
   * @return: boolean
   **/
  public boolean decode(String strMessage)
  {
    try
    {
      InquireNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                 SysConst.PACKAGESPILTER);
      ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                 SysConst.PACKAGESPILTER);
      Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                            SysConst.PACKAGESPILTER);
      Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                           SysConst.PACKAGESPILTER);
      Email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                             SysConst.PACKAGESPILTER);
      Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                             SysConst.PACKAGESPILTER);
      Title = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                             SysConst.PACKAGESPILTER);
      Content = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                               SysConst.PACKAGESPILTER);
      Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                SysConst.PACKAGESPILTER);
      MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              10, SysConst.PACKAGESPILTER));
      MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                SysConst.PACKAGESPILTER);
      ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                12, SysConst.PACKAGESPILTER));
      ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                  SysConst.PACKAGESPILTER);
    }
    catch (NumberFormatException ex)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LGInquireInfoSchema";
      tError.functionName = "decode";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);

      return false;
    }
    return true;
  }


  /**
   * 取得对应传入参数的String形式的字段值
   * @param: FCode String 希望取得的字段名
   * @return: String
   * 如果没有对应的字段，返回""
   * 如果字段值为空，返回"null"
   **/
  public String getV(String FCode)
  {
    String strReturn = "";
    if (FCode.equals("InquireNo"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(InquireNo));
    }
    if (FCode.equals("ManageCom"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
    }
    if (FCode.equals("Name"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
    }
    if (FCode.equals("Sex"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
    }
    if (FCode.equals("Email"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
    }
    if (FCode.equals("Phone"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
    }
    if (FCode.equals("Title"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(Title));
    }
    if (FCode.equals("Content"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(Content));
    }
    if (FCode.equals("Operator"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
    }
    if (FCode.equals("MakeDate"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
    }
    if (FCode.equals("MakeTime"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
    }
    if (FCode.equals("ModifyDate"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
    }
    if (FCode.equals("ModifyTime"))
    {
      strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
    }
    if (strReturn.equals(""))
    {
      strReturn = "null";
    }

    return strReturn;
  }


  /**
   * 取得Schema中指定索引值所对应的字段值
   * @param: nFieldIndex int 指定的字段索引值
   * @return: String
   * 如果没有对应的字段，返回""
   * 如果字段值为空，返回"null"
   **/
  public String getV(int nFieldIndex)
  {
    String strFieldValue = "";
    switch (nFieldIndex)
    {
      case 0:
        strFieldValue = StrTool.GBKToUnicode(InquireNo);
        break;
      case 1:
        strFieldValue = StrTool.GBKToUnicode(ManageCom);
        break;
      case 2:
        strFieldValue = StrTool.GBKToUnicode(Name);
        break;
      case 3:
        strFieldValue = StrTool.GBKToUnicode(Sex);
        break;
      case 4:
        strFieldValue = StrTool.GBKToUnicode(Email);
        break;
      case 5:
        strFieldValue = StrTool.GBKToUnicode(Phone);
        break;
      case 6:
        strFieldValue = StrTool.GBKToUnicode(Title);
        break;
      case 7:
        strFieldValue = StrTool.GBKToUnicode(Content);
        break;
      case 8:
        strFieldValue = StrTool.GBKToUnicode(Operator);
        break;
      case 9:
        strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        break;
      case 10:
        strFieldValue = StrTool.GBKToUnicode(MakeTime);
        break;
      case 11:
        strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        break;
      case 12:
        strFieldValue = StrTool.GBKToUnicode(ModifyTime);
        break;
      default:
        strFieldValue = "";
    }
    ;
    if (strFieldValue.equals(""))
    {
      strFieldValue = "null";
    }
    return strFieldValue;
  }


  /**
   * 设置对应传入参数的String形式的字段值
   * @param: FCode String 需要赋值的对象
   * @param: FValue String 要赋的值
   * @return: boolean
   **/
  public boolean setV(String FCode, String FValue)
  {
    if (StrTool.cTrim(FCode).equals(""))
    {
      return false;
    }

    if (FCode.equals("InquireNo"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        InquireNo = FValue.trim();
      }
      else
      {
        InquireNo = null;
      }
    }
    if (FCode.equals("ManageCom"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        ManageCom = FValue.trim();
      }
      else
      {
        ManageCom = null;
      }
    }
    if (FCode.equals("Name"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        Name = FValue.trim();
      }
      else
      {
        Name = null;
      }
    }
    if (FCode.equals("Sex"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        Sex = FValue.trim();
      }
      else
      {
        Sex = null;
      }
    }
    if (FCode.equals("Email"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        Email = FValue.trim();
      }
      else
      {
        Email = null;
      }
    }
    if (FCode.equals("Phone"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        Phone = FValue.trim();
      }
      else
      {
        Phone = null;
      }
    }
    if (FCode.equals("Title"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        Title = FValue.trim();
      }
      else
      {
        Title = null;
      }
    }
    if (FCode.equals("Content"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        Content = FValue.trim();
      }
      else
      {
        Content = null;
      }
    }
    if (FCode.equals("Operator"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        Operator = FValue.trim();
      }
      else
      {
        Operator = null;
      }
    }
    if (FCode.equals("MakeDate"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        MakeDate = fDate.getDate(FValue);
      }
      else
      {
        MakeDate = null;
      }
    }
    if (FCode.equals("MakeTime"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        MakeTime = FValue.trim();
      }
      else
      {
        MakeTime = null;
      }
    }
    if (FCode.equals("ModifyDate"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        ModifyDate = fDate.getDate(FValue);
      }
      else
      {
        ModifyDate = null;
      }
    }
    if (FCode.equals("ModifyTime"))
    {
      if (FValue != null && !FValue.equals(""))
      {
        ModifyTime = FValue.trim();
      }
      else
      {
        ModifyTime = null;
      }
    }
    return true;
  }

  public boolean equals(Object otherObject)
  {
    if (this == otherObject)
    {
      return true;
    }
    if (otherObject == null)
    {
      return false;
    }
    if (getClass() != otherObject.getClass())
    {
      return false;
    }
    LGInquireInfoSchema other = (LGInquireInfoSchema) otherObject;
    return
        InquireNo.equals(other.getInquireNo())
        && ManageCom.equals(other.getManageCom())
        && Name.equals(other.getName())
        && Sex.equals(other.getSex())
        && Email.equals(other.getEmail())
        && Phone.equals(other.getPhone())
        && Title.equals(other.getTitle())
        && Content.equals(other.getContent())
        && Operator.equals(other.getOperator())
        && fDate.getString(MakeDate).equals(other.getMakeDate())
        && MakeTime.equals(other.getMakeTime())
        && fDate.getString(ModifyDate).equals(other.getModifyDate())
        && ModifyTime.equals(other.getModifyTime());
  }


  /**
   * 取得Schema拥有字段的数量
   * @return: int
   **/
  public int getFieldCount()
  {
    return FIELDNUM;
  }


  /**
   * 取得Schema中指定字段名所对应的索引值
   * 如果没有对应的字段，返回-1
   * @param: strFieldName String
   * @return: int
   **/
  public int getFieldIndex(String strFieldName)
  {
    if (strFieldName.equals("InquireNo"))
    {
      return 0;
    }
    if (strFieldName.equals("ManageCom"))
    {
      return 1;
    }
    if (strFieldName.equals("Name"))
    {
      return 2;
    }
    if (strFieldName.equals("Sex"))
    {
      return 3;
    }
    if (strFieldName.equals("Email"))
    {
      return 4;
    }
    if (strFieldName.equals("Phone"))
    {
      return 5;
    }
    if (strFieldName.equals("Title"))
    {
      return 6;
    }
    if (strFieldName.equals("Content"))
    {
      return 7;
    }
    if (strFieldName.equals("Operator"))
    {
      return 8;
    }
    if (strFieldName.equals("MakeDate"))
    {
      return 9;
    }
    if (strFieldName.equals("MakeTime"))
    {
      return 10;
    }
    if (strFieldName.equals("ModifyDate"))
    {
      return 11;
    }
    if (strFieldName.equals("ModifyTime"))
    {
      return 12;
    }
    return -1;
  }


  /**
   * 取得Schema中指定索引值所对应的字段名
   * 如果没有对应的字段，返回""
   * @param: nFieldIndex int
   * @return: String
   **/
  public String getFieldName(int nFieldIndex)
  {
    String strFieldName = "";
    switch (nFieldIndex)
    {
      case 0:
        strFieldName = "InquireNo";
        break;
      case 1:
        strFieldName = "ManageCom";
        break;
      case 2:
        strFieldName = "Name";
        break;
      case 3:
        strFieldName = "Sex";
        break;
      case 4:
        strFieldName = "Email";
        break;
      case 5:
        strFieldName = "Phone";
        break;
      case 6:
        strFieldName = "Title";
        break;
      case 7:
        strFieldName = "Content";
        break;
      case 8:
        strFieldName = "Operator";
        break;
      case 9:
        strFieldName = "MakeDate";
        break;
      case 10:
        strFieldName = "MakeTime";
        break;
      case 11:
        strFieldName = "ModifyDate";
        break;
      case 12:
        strFieldName = "ModifyTime";
        break;
      default:
        strFieldName = "";
    }
    ;
    return strFieldName;
  }


  /**
   * 取得Schema中指定字段名所对应的字段类型
   * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
   * @param: strFieldName String
   * @return: int
   **/
  public int getFieldType(String strFieldName)
  {
    if (strFieldName.equals("InquireNo"))
    {
      return Schema.TYPE_STRING;
    }
    if (strFieldName.equals("ManageCom"))
    {
      return Schema.TYPE_STRING;
    }
    if (strFieldName.equals("Name"))
    {
      return Schema.TYPE_STRING;
    }
    if (strFieldName.equals("Sex"))
    {
      return Schema.TYPE_STRING;
    }
    if (strFieldName.equals("Email"))
    {
      return Schema.TYPE_STRING;
    }
    if (strFieldName.equals("Phone"))
    {
      return Schema.TYPE_STRING;
    }
    if (strFieldName.equals("Title"))
    {
      return Schema.TYPE_STRING;
    }
    if (strFieldName.equals("Content"))
    {
      return Schema.TYPE_STRING;
    }
    if (strFieldName.equals("Operator"))
    {
      return Schema.TYPE_STRING;
    }
    if (strFieldName.equals("MakeDate"))
    {
      return Schema.TYPE_DATE;
    }
    if (strFieldName.equals("MakeTime"))
    {
      return Schema.TYPE_STRING;
    }
    if (strFieldName.equals("ModifyDate"))
    {
      return Schema.TYPE_DATE;
    }
    if (strFieldName.equals("ModifyTime"))
    {
      return Schema.TYPE_STRING;
    }
    return Schema.TYPE_NOFOUND;
  }


  /**
   * 取得Schema中指定索引值所对应的字段类型
   * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
   * @param: nFieldIndex int
   * @return: int
   **/
  public int getFieldType(int nFieldIndex)
  {
    int nFieldType = Schema.TYPE_NOFOUND;
    switch (nFieldIndex)
    {
      case 0:
        nFieldType = Schema.TYPE_STRING;
        break;
      case 1:
        nFieldType = Schema.TYPE_STRING;
        break;
      case 2:
        nFieldType = Schema.TYPE_STRING;
        break;
      case 3:
        nFieldType = Schema.TYPE_STRING;
        break;
      case 4:
        nFieldType = Schema.TYPE_STRING;
        break;
      case 5:
        nFieldType = Schema.TYPE_STRING;
        break;
      case 6:
        nFieldType = Schema.TYPE_STRING;
        break;
      case 7:
        nFieldType = Schema.TYPE_STRING;
        break;
      case 8:
        nFieldType = Schema.TYPE_STRING;
        break;
      case 9:
        nFieldType = Schema.TYPE_DATE;
        break;
      case 10:
        nFieldType = Schema.TYPE_STRING;
        break;
      case 11:
        nFieldType = Schema.TYPE_DATE;
        break;
      case 12:
        nFieldType = Schema.TYPE_STRING;
        break;
      default:
        nFieldType = Schema.TYPE_NOFOUND;
    }
    ;
    return nFieldType;
  }
}
