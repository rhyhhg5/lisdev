/*
 * <p>ClassName: LOReport2Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOReport2DB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LOReport2Schema implements Schema
{
    // @Field
    /** 管理机构 */
    private String ManageCom;
    /** 险种编码 */
    private String RiskCode;
    /** 销售渠道 */
    private String SaleChnl;
    /** 业务日期 */
    private Date OldMakeDate;
    /** 给付类型 */
    private String GetType;
    /** 人次累计 */
    private double PeoplesCount;
    /** 件数累计 */
    private double PolCount;
    /** 金额(本期) */
    private double Money1;
    /** 金额(累计) */
    private double Money2;
    /** 给付率 */
    private double GetRate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOReport2Schema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "ManageCom";
        pk[1] = "RiskCode";
        pk[2] = "SaleChnl";
        pk[3] = "OldMakeDate";
        pk[4] = "GetType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getSaleChnl()
    {
        if (SaleChnl != null && !SaleChnl.equals("") && SysConst.CHANGECHARSET == true)
        {
            SaleChnl = StrTool.unicodeToGBK(SaleChnl);
        }
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl)
    {
        SaleChnl = aSaleChnl;
    }

    public String getOldMakeDate()
    {
        if (OldMakeDate != null)
        {
            return fDate.getString(OldMakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setOldMakeDate(Date aOldMakeDate)
    {
        OldMakeDate = aOldMakeDate;
    }

    public void setOldMakeDate(String aOldMakeDate)
    {
        if (aOldMakeDate != null && !aOldMakeDate.equals(""))
        {
            OldMakeDate = fDate.getDate(aOldMakeDate);
        }
        else
        {
            OldMakeDate = null;
        }
    }

    public String getGetType()
    {
        if (GetType != null && !GetType.equals("") && SysConst.CHANGECHARSET == true)
        {
            GetType = StrTool.unicodeToGBK(GetType);
        }
        return GetType;
    }

    public void setGetType(String aGetType)
    {
        GetType = aGetType;
    }

    public double getPeoplesCount()
    {
        return PeoplesCount;
    }

    public void setPeoplesCount(double aPeoplesCount)
    {
        PeoplesCount = aPeoplesCount;
    }

    public void setPeoplesCount(String aPeoplesCount)
    {
        if (aPeoplesCount != null && !aPeoplesCount.equals(""))
        {
            Double tDouble = new Double(aPeoplesCount);
            double d = tDouble.doubleValue();
            PeoplesCount = d;
        }
    }

    public double getPolCount()
    {
        return PolCount;
    }

    public void setPolCount(double aPolCount)
    {
        PolCount = aPolCount;
    }

    public void setPolCount(String aPolCount)
    {
        if (aPolCount != null && !aPolCount.equals(""))
        {
            Double tDouble = new Double(aPolCount);
            double d = tDouble.doubleValue();
            PolCount = d;
        }
    }

    public double getMoney1()
    {
        return Money1;
    }

    public void setMoney1(double aMoney1)
    {
        Money1 = aMoney1;
    }

    public void setMoney1(String aMoney1)
    {
        if (aMoney1 != null && !aMoney1.equals(""))
        {
            Double tDouble = new Double(aMoney1);
            double d = tDouble.doubleValue();
            Money1 = d;
        }
    }

    public double getMoney2()
    {
        return Money2;
    }

    public void setMoney2(double aMoney2)
    {
        Money2 = aMoney2;
    }

    public void setMoney2(String aMoney2)
    {
        if (aMoney2 != null && !aMoney2.equals(""))
        {
            Double tDouble = new Double(aMoney2);
            double d = tDouble.doubleValue();
            Money2 = d;
        }
    }

    public double getGetRate()
    {
        return GetRate;
    }

    public void setGetRate(double aGetRate)
    {
        GetRate = aGetRate;
    }

    public void setGetRate(String aGetRate)
    {
        if (aGetRate != null && !aGetRate.equals(""))
        {
            Double tDouble = new Double(aGetRate);
            double d = tDouble.doubleValue();
            GetRate = d;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LOReport2Schema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LOReport2Schema aLOReport2Schema)
    {
        this.ManageCom = aLOReport2Schema.getManageCom();
        this.RiskCode = aLOReport2Schema.getRiskCode();
        this.SaleChnl = aLOReport2Schema.getSaleChnl();
        this.OldMakeDate = fDate.getDate(aLOReport2Schema.getOldMakeDate());
        this.GetType = aLOReport2Schema.getGetType();
        this.PeoplesCount = aLOReport2Schema.getPeoplesCount();
        this.PolCount = aLOReport2Schema.getPolCount();
        this.Money1 = aLOReport2Schema.getMoney1();
        this.Money2 = aLOReport2Schema.getMoney2();
        this.GetRate = aLOReport2Schema.getGetRate();
        this.Operator = aLOReport2Schema.getOperator();
        this.MakeDate = fDate.getDate(aLOReport2Schema.getMakeDate());
        this.MakeTime = aLOReport2Schema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOReport2Schema.getModifyDate());
        this.ModifyTime = aLOReport2Schema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("SaleChnl") == null)
            {
                this.SaleChnl = null;
            }
            else
            {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            this.OldMakeDate = rs.getDate("OldMakeDate");
            if (rs.getString("GetType") == null)
            {
                this.GetType = null;
            }
            else
            {
                this.GetType = rs.getString("GetType").trim();
            }

            this.PeoplesCount = rs.getDouble("PeoplesCount");
            this.PolCount = rs.getDouble("PolCount");
            this.Money1 = rs.getDouble("Money1");
            this.Money2 = rs.getDouble("Money2");
            this.GetRate = rs.getDouble("GetRate");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOReport2Schema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LOReport2Schema getSchema()
    {
        LOReport2Schema aLOReport2Schema = new LOReport2Schema();
        aLOReport2Schema.setSchema(this);
        return aLOReport2Schema;
    }

    public LOReport2DB getDB()
    {
        LOReport2DB aDBOper = new LOReport2DB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOReport2描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SaleChnl)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            OldMakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PeoplesCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(PolCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Money1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Money2) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOReport2>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            OldMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            GetType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            PeoplesCount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            PolCount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            Money1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    8, SysConst.PACKAGESPILTER))).doubleValue();
            Money2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    9, SysConst.PACKAGESPILTER))).doubleValue();
            GetRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOReport2Schema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("SaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SaleChnl));
        }
        if (FCode.equals("OldMakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getOldMakeDate()));
        }
        if (FCode.equals("GetType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetType));
        }
        if (FCode.equals("PeoplesCount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PeoplesCount));
        }
        if (FCode.equals("PolCount"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolCount));
        }
        if (FCode.equals("Money1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Money1));
        }
        if (FCode.equals("Money2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Money2));
        }
        if (FCode.equals("GetRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetRate));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getOldMakeDate()));
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GetType);
                break;
            case 5:
                strFieldValue = String.valueOf(PeoplesCount);
                break;
            case 6:
                strFieldValue = String.valueOf(PolCount);
                break;
            case 7:
                strFieldValue = String.valueOf(Money1);
                break;
            case 8:
                strFieldValue = String.valueOf(Money2);
                break;
            case 9:
                strFieldValue = String.valueOf(GetRate);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("SaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
            {
                SaleChnl = null;
            }
        }
        if (FCode.equals("OldMakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OldMakeDate = fDate.getDate(FValue);
            }
            else
            {
                OldMakeDate = null;
            }
        }
        if (FCode.equals("GetType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetType = FValue.trim();
            }
            else
            {
                GetType = null;
            }
        }
        if (FCode.equals("PeoplesCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PeoplesCount = d;
            }
        }
        if (FCode.equals("PolCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PolCount = d;
            }
        }
        if (FCode.equals("Money1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Money1 = d;
            }
        }
        if (FCode.equals("Money2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Money2 = d;
            }
        }
        if (FCode.equals("GetRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetRate = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LOReport2Schema other = (LOReport2Schema) otherObject;
        return
                ManageCom.equals(other.getManageCom())
                && RiskCode.equals(other.getRiskCode())
                && SaleChnl.equals(other.getSaleChnl())
                && fDate.getString(OldMakeDate).equals(other.getOldMakeDate())
                && GetType.equals(other.getGetType())
                && PeoplesCount == other.getPeoplesCount()
                && PolCount == other.getPolCount()
                && Money1 == other.getMoney1()
                && Money2 == other.getMoney2()
                && GetRate == other.getGetRate()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 1;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return 2;
        }
        if (strFieldName.equals("OldMakeDate"))
        {
            return 3;
        }
        if (strFieldName.equals("GetType"))
        {
            return 4;
        }
        if (strFieldName.equals("PeoplesCount"))
        {
            return 5;
        }
        if (strFieldName.equals("PolCount"))
        {
            return 6;
        }
        if (strFieldName.equals("Money1"))
        {
            return 7;
        }
        if (strFieldName.equals("Money2"))
        {
            return 8;
        }
        if (strFieldName.equals("GetRate"))
        {
            return 9;
        }
        if (strFieldName.equals("Operator"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "RiskCode";
                break;
            case 2:
                strFieldName = "SaleChnl";
                break;
            case 3:
                strFieldName = "OldMakeDate";
                break;
            case 4:
                strFieldName = "GetType";
                break;
            case 5:
                strFieldName = "PeoplesCount";
                break;
            case 6:
                strFieldName = "PolCount";
                break;
            case 7:
                strFieldName = "Money1";
                break;
            case 8:
                strFieldName = "Money2";
                break;
            case 9:
                strFieldName = "GetRate";
                break;
            case 10:
                strFieldName = "Operator";
                break;
            case 11:
                strFieldName = "MakeDate";
                break;
            case 12:
                strFieldName = "MakeTime";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OldMakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("GetType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PeoplesCount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PolCount"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Money1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Money2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
