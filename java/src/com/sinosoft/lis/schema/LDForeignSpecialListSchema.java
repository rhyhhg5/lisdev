/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDForeignSpecialListDB;

/*
 * <p>ClassName: LDForeignSpecialListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 国外特殊关系人员名单
 * @CreateDate：2018-06-26
 */
public class LDForeignSpecialListSchema implements Schema, Cloneable
{
	// @Field
	/** 特殊人员关系编号 */
	private String SpecialistCode;
	/** 类型 */
	private String Type;
	/** 姓名 */
	private String Name;
	/** 生日 */
	private Date Birthday;
	/** 证件号码类型 */
	private String IDNoType;
	/** 证件号码 */
	private String IDNo;
	/** 国籍 */
	private String Nationality;
	/** 曾用名1 */
	private String Name1;
	/** 曾用名2 */
	private String Name2;
	/** 曾用名3 */
	private String Name3;
	/** 曾用名4 */
	private String Name4;
	/** 曾用名5 */
	private String Name5;
	/** 曾用名6 */
	private String Name6;
	/** 曾用名7 */
	private String Name7;
	/** 曾用名8 */
	private String Name8;
	/** 曾用名9 */
	private String Name9;
	/** 曾用名10 */
	private String Name10;
	/** 文化程度 */
	private String EduLevel;
	/** 备注 */
	private String Remark;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 备注1 */
	private String Rem1;
	/** 备注2 */
	private String Rem2;

	public static final int FIELDNUM = 26;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDForeignSpecialListSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SpecialistCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDForeignSpecialListSchema cloned = (LDForeignSpecialListSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSpecialistCode()
	{
		return SpecialistCode;
	}
	public void setSpecialistCode(String aSpecialistCode)
	{
		SpecialistCode = aSpecialistCode;
	}
	public String getType()
	{
		return Type;
	}
	public void setType(String aType)
	{
		Type = aType;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
		Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getIDNoType()
	{
		return IDNoType;
	}
	public void setIDNoType(String aIDNoType)
	{
		IDNoType = aIDNoType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getNationality()
	{
		return Nationality;
	}
	public void setNationality(String aNationality)
	{
		Nationality = aNationality;
	}
	public String getName1()
	{
		return Name1;
	}
	public void setName1(String aName1)
	{
		Name1 = aName1;
	}
	public String getName2()
	{
		return Name2;
	}
	public void setName2(String aName2)
	{
		Name2 = aName2;
	}
	public String getName3()
	{
		return Name3;
	}
	public void setName3(String aName3)
	{
		Name3 = aName3;
	}
	public String getName4()
	{
		return Name4;
	}
	public void setName4(String aName4)
	{
		Name4 = aName4;
	}
	public String getName5()
	{
		return Name5;
	}
	public void setName5(String aName5)
	{
		Name5 = aName5;
	}
	public String getName6()
	{
		return Name6;
	}
	public void setName6(String aName6)
	{
		Name6 = aName6;
	}
	public String getName7()
	{
		return Name7;
	}
	public void setName7(String aName7)
	{
		Name7 = aName7;
	}
	public String getName8()
	{
		return Name8;
	}
	public void setName8(String aName8)
	{
		Name8 = aName8;
	}
	public String getName9()
	{
		return Name9;
	}
	public void setName9(String aName9)
	{
		Name9 = aName9;
	}
	public String getName10()
	{
		return Name10;
	}
	public void setName10(String aName10)
	{
		Name10 = aName10;
	}
	public String getEduLevel()
	{
		return EduLevel;
	}
	public void setEduLevel(String aEduLevel)
	{
		EduLevel = aEduLevel;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getRem1()
	{
		return Rem1;
	}
	public void setRem1(String aRem1)
	{
		Rem1 = aRem1;
	}
	public String getRem2()
	{
		return Rem2;
	}
	public void setRem2(String aRem2)
	{
		Rem2 = aRem2;
	}

	/**
	* 使用另外一个 LDForeignSpecialListSchema 对象给 Schema 赋值
	* @param: aLDForeignSpecialListSchema LDForeignSpecialListSchema
	**/
	public void setSchema(LDForeignSpecialListSchema aLDForeignSpecialListSchema)
	{
		this.SpecialistCode = aLDForeignSpecialListSchema.getSpecialistCode();
		this.Type = aLDForeignSpecialListSchema.getType();
		this.Name = aLDForeignSpecialListSchema.getName();
		this.Birthday = fDate.getDate( aLDForeignSpecialListSchema.getBirthday());
		this.IDNoType = aLDForeignSpecialListSchema.getIDNoType();
		this.IDNo = aLDForeignSpecialListSchema.getIDNo();
		this.Nationality = aLDForeignSpecialListSchema.getNationality();
		this.Name1 = aLDForeignSpecialListSchema.getName1();
		this.Name2 = aLDForeignSpecialListSchema.getName2();
		this.Name3 = aLDForeignSpecialListSchema.getName3();
		this.Name4 = aLDForeignSpecialListSchema.getName4();
		this.Name5 = aLDForeignSpecialListSchema.getName5();
		this.Name6 = aLDForeignSpecialListSchema.getName6();
		this.Name7 = aLDForeignSpecialListSchema.getName7();
		this.Name8 = aLDForeignSpecialListSchema.getName8();
		this.Name9 = aLDForeignSpecialListSchema.getName9();
		this.Name10 = aLDForeignSpecialListSchema.getName10();
		this.EduLevel = aLDForeignSpecialListSchema.getEduLevel();
		this.Remark = aLDForeignSpecialListSchema.getRemark();
		this.Operator = aLDForeignSpecialListSchema.getOperator();
		this.MakeDate = fDate.getDate( aLDForeignSpecialListSchema.getMakeDate());
		this.MakeTime = aLDForeignSpecialListSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLDForeignSpecialListSchema.getModifyDate());
		this.ModifyTime = aLDForeignSpecialListSchema.getModifyTime();
		this.Rem1 = aLDForeignSpecialListSchema.getRem1();
		this.Rem2 = aLDForeignSpecialListSchema.getRem2();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SpecialistCode") == null )
				this.SpecialistCode = null;
			else
				this.SpecialistCode = rs.getString("SpecialistCode").trim();

			if( rs.getString("Type") == null )
				this.Type = null;
			else
				this.Type = rs.getString("Type").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			this.Birthday = rs.getDate("Birthday");
			if( rs.getString("IDNoType") == null )
				this.IDNoType = null;
			else
				this.IDNoType = rs.getString("IDNoType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("Nationality") == null )
				this.Nationality = null;
			else
				this.Nationality = rs.getString("Nationality").trim();

			if( rs.getString("Name1") == null )
				this.Name1 = null;
			else
				this.Name1 = rs.getString("Name1").trim();

			if( rs.getString("Name2") == null )
				this.Name2 = null;
			else
				this.Name2 = rs.getString("Name2").trim();

			if( rs.getString("Name3") == null )
				this.Name3 = null;
			else
				this.Name3 = rs.getString("Name3").trim();

			if( rs.getString("Name4") == null )
				this.Name4 = null;
			else
				this.Name4 = rs.getString("Name4").trim();

			if( rs.getString("Name5") == null )
				this.Name5 = null;
			else
				this.Name5 = rs.getString("Name5").trim();

			if( rs.getString("Name6") == null )
				this.Name6 = null;
			else
				this.Name6 = rs.getString("Name6").trim();

			if( rs.getString("Name7") == null )
				this.Name7 = null;
			else
				this.Name7 = rs.getString("Name7").trim();

			if( rs.getString("Name8") == null )
				this.Name8 = null;
			else
				this.Name8 = rs.getString("Name8").trim();

			if( rs.getString("Name9") == null )
				this.Name9 = null;
			else
				this.Name9 = rs.getString("Name9").trim();

			if( rs.getString("Name10") == null )
				this.Name10 = null;
			else
				this.Name10 = rs.getString("Name10").trim();

			if( rs.getString("EduLevel") == null )
				this.EduLevel = null;
			else
				this.EduLevel = rs.getString("EduLevel").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Rem1") == null )
				this.Rem1 = null;
			else
				this.Rem1 = rs.getString("Rem1").trim();

			if( rs.getString("Rem2") == null )
				this.Rem2 = null;
			else
				this.Rem2 = rs.getString("Rem2").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDForeignSpecialList表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDForeignSpecialListSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDForeignSpecialListSchema getSchema()
	{
		LDForeignSpecialListSchema aLDForeignSpecialListSchema = new LDForeignSpecialListSchema();
		aLDForeignSpecialListSchema.setSchema(this);
		return aLDForeignSpecialListSchema;
	}

	public LDForeignSpecialListDB getDB()
	{
		LDForeignSpecialListDB aDBOper = new LDForeignSpecialListDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDForeignSpecialList描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SpecialistCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Type)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNoType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Nationality)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name7)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name8)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name10)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EduLevel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Rem1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Rem2));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDForeignSpecialList>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SpecialistCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			IDNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Name1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Name2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Name3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Name4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Name5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Name6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Name7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Name8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Name9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Name10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			EduLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Rem1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Rem2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDForeignSpecialListSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SpecialistCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialistCode));
		}
		if (FCode.equals("Type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Type));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("IDNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNoType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("Nationality"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
		}
		if (FCode.equals("Name1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name1));
		}
		if (FCode.equals("Name2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name2));
		}
		if (FCode.equals("Name3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name3));
		}
		if (FCode.equals("Name4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name4));
		}
		if (FCode.equals("Name5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name5));
		}
		if (FCode.equals("Name6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name6));
		}
		if (FCode.equals("Name7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name7));
		}
		if (FCode.equals("Name8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name8));
		}
		if (FCode.equals("Name9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name9));
		}
		if (FCode.equals("Name10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name10));
		}
		if (FCode.equals("EduLevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EduLevel));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Rem1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rem1));
		}
		if (FCode.equals("Rem2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rem2));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SpecialistCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Type);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(IDNoType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Nationality);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Name1);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Name2);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Name3);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Name4);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Name5);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Name6);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Name7);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Name8);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Name9);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Name10);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(EduLevel);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Rem1);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Rem2);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SpecialistCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SpecialistCode = FValue.trim();
			}
			else
				SpecialistCode = null;
		}
		if (FCode.equalsIgnoreCase("Type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Type = FValue.trim();
			}
			else
				Type = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("IDNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNoType = FValue.trim();
			}
			else
				IDNoType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("Nationality"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Nationality = FValue.trim();
			}
			else
				Nationality = null;
		}
		if (FCode.equalsIgnoreCase("Name1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name1 = FValue.trim();
			}
			else
				Name1 = null;
		}
		if (FCode.equalsIgnoreCase("Name2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name2 = FValue.trim();
			}
			else
				Name2 = null;
		}
		if (FCode.equalsIgnoreCase("Name3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name3 = FValue.trim();
			}
			else
				Name3 = null;
		}
		if (FCode.equalsIgnoreCase("Name4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name4 = FValue.trim();
			}
			else
				Name4 = null;
		}
		if (FCode.equalsIgnoreCase("Name5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name5 = FValue.trim();
			}
			else
				Name5 = null;
		}
		if (FCode.equalsIgnoreCase("Name6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name6 = FValue.trim();
			}
			else
				Name6 = null;
		}
		if (FCode.equalsIgnoreCase("Name7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name7 = FValue.trim();
			}
			else
				Name7 = null;
		}
		if (FCode.equalsIgnoreCase("Name8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name8 = FValue.trim();
			}
			else
				Name8 = null;
		}
		if (FCode.equalsIgnoreCase("Name9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name9 = FValue.trim();
			}
			else
				Name9 = null;
		}
		if (FCode.equalsIgnoreCase("Name10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name10 = FValue.trim();
			}
			else
				Name10 = null;
		}
		if (FCode.equalsIgnoreCase("EduLevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EduLevel = FValue.trim();
			}
			else
				EduLevel = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Rem1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Rem1 = FValue.trim();
			}
			else
				Rem1 = null;
		}
		if (FCode.equalsIgnoreCase("Rem2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Rem2 = FValue.trim();
			}
			else
				Rem2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDForeignSpecialListSchema other = (LDForeignSpecialListSchema)otherObject;
		return
			(SpecialistCode == null ? other.getSpecialistCode() == null : SpecialistCode.equals(other.getSpecialistCode()))
			&& (Type == null ? other.getType() == null : Type.equals(other.getType()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (Birthday == null ? other.getBirthday() == null : fDate.getString(Birthday).equals(other.getBirthday()))
			&& (IDNoType == null ? other.getIDNoType() == null : IDNoType.equals(other.getIDNoType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (Nationality == null ? other.getNationality() == null : Nationality.equals(other.getNationality()))
			&& (Name1 == null ? other.getName1() == null : Name1.equals(other.getName1()))
			&& (Name2 == null ? other.getName2() == null : Name2.equals(other.getName2()))
			&& (Name3 == null ? other.getName3() == null : Name3.equals(other.getName3()))
			&& (Name4 == null ? other.getName4() == null : Name4.equals(other.getName4()))
			&& (Name5 == null ? other.getName5() == null : Name5.equals(other.getName5()))
			&& (Name6 == null ? other.getName6() == null : Name6.equals(other.getName6()))
			&& (Name7 == null ? other.getName7() == null : Name7.equals(other.getName7()))
			&& (Name8 == null ? other.getName8() == null : Name8.equals(other.getName8()))
			&& (Name9 == null ? other.getName9() == null : Name9.equals(other.getName9()))
			&& (Name10 == null ? other.getName10() == null : Name10.equals(other.getName10()))
			&& (EduLevel == null ? other.getEduLevel() == null : EduLevel.equals(other.getEduLevel()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Rem1 == null ? other.getRem1() == null : Rem1.equals(other.getRem1()))
			&& (Rem2 == null ? other.getRem2() == null : Rem2.equals(other.getRem2()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SpecialistCode") ) {
			return 0;
		}
		if( strFieldName.equals("Type") ) {
			return 1;
		}
		if( strFieldName.equals("Name") ) {
			return 2;
		}
		if( strFieldName.equals("Birthday") ) {
			return 3;
		}
		if( strFieldName.equals("IDNoType") ) {
			return 4;
		}
		if( strFieldName.equals("IDNo") ) {
			return 5;
		}
		if( strFieldName.equals("Nationality") ) {
			return 6;
		}
		if( strFieldName.equals("Name1") ) {
			return 7;
		}
		if( strFieldName.equals("Name2") ) {
			return 8;
		}
		if( strFieldName.equals("Name3") ) {
			return 9;
		}
		if( strFieldName.equals("Name4") ) {
			return 10;
		}
		if( strFieldName.equals("Name5") ) {
			return 11;
		}
		if( strFieldName.equals("Name6") ) {
			return 12;
		}
		if( strFieldName.equals("Name7") ) {
			return 13;
		}
		if( strFieldName.equals("Name8") ) {
			return 14;
		}
		if( strFieldName.equals("Name9") ) {
			return 15;
		}
		if( strFieldName.equals("Name10") ) {
			return 16;
		}
		if( strFieldName.equals("EduLevel") ) {
			return 17;
		}
		if( strFieldName.equals("Remark") ) {
			return 18;
		}
		if( strFieldName.equals("Operator") ) {
			return 19;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 20;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 23;
		}
		if( strFieldName.equals("Rem1") ) {
			return 24;
		}
		if( strFieldName.equals("Rem2") ) {
			return 25;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SpecialistCode";
				break;
			case 1:
				strFieldName = "Type";
				break;
			case 2:
				strFieldName = "Name";
				break;
			case 3:
				strFieldName = "Birthday";
				break;
			case 4:
				strFieldName = "IDNoType";
				break;
			case 5:
				strFieldName = "IDNo";
				break;
			case 6:
				strFieldName = "Nationality";
				break;
			case 7:
				strFieldName = "Name1";
				break;
			case 8:
				strFieldName = "Name2";
				break;
			case 9:
				strFieldName = "Name3";
				break;
			case 10:
				strFieldName = "Name4";
				break;
			case 11:
				strFieldName = "Name5";
				break;
			case 12:
				strFieldName = "Name6";
				break;
			case 13:
				strFieldName = "Name7";
				break;
			case 14:
				strFieldName = "Name8";
				break;
			case 15:
				strFieldName = "Name9";
				break;
			case 16:
				strFieldName = "Name10";
				break;
			case 17:
				strFieldName = "EduLevel";
				break;
			case 18:
				strFieldName = "Remark";
				break;
			case 19:
				strFieldName = "Operator";
				break;
			case 20:
				strFieldName = "MakeDate";
				break;
			case 21:
				strFieldName = "MakeTime";
				break;
			case 22:
				strFieldName = "ModifyDate";
				break;
			case 23:
				strFieldName = "ModifyTime";
				break;
			case 24:
				strFieldName = "Rem1";
				break;
			case 25:
				strFieldName = "Rem2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SpecialistCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Nationality") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name8") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name10") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EduLevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Rem1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Rem2") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
