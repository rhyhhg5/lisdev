/*
 * <p>ClassName: LFReinsureSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LFReinsureDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFReinsureSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 管理机构 */
    private String ManageCom;
    /** 再保项目 */
    private String ReinsurItem;
    /** 再保地点 */
    private String ReinsurLocal;
    /** 协议类型 */
    private String ProtItem;
    /** 分出分入标志 */
    private String InOutFlag;
    /** 销售渠道 */
    private String SaleChnl;
    /** 团单个单标志 */
    private String PersonPolFlag;
    /** 生存金标志 */
    private String AnnulFlag;
    /** 经纪佣金标志 */
    private String AgentComFlag;
    /** 分保费用类型 */
    private String CessFeeType;
    /** 分保金额 */
    private double CessMoney;
    /** 报表日期 */
    private Date ReportDate;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFReinsureSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[12];
        pk[0] = "RiskCode";
        pk[1] = "ManageCom";
        pk[2] = "ReinsurItem";
        pk[3] = "ReinsurLocal";
        pk[4] = "ProtItem";
        pk[5] = "InOutFlag";
        pk[6] = "SaleChnl";
        pk[7] = "PersonPolFlag";
        pk[8] = "AnnulFlag";
        pk[9] = "AgentComFlag";
        pk[10] = "CessFeeType";
        pk[11] = "ReportDate";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getReinsurItem()
    {
        if (ReinsurItem != null && !ReinsurItem.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReinsurItem = StrTool.unicodeToGBK(ReinsurItem);
        }
        return ReinsurItem;
    }

    public void setReinsurItem(String aReinsurItem)
    {
        ReinsurItem = aReinsurItem;
    }

    public String getReinsurLocal()
    {
        if (ReinsurLocal != null && !ReinsurLocal.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReinsurLocal = StrTool.unicodeToGBK(ReinsurLocal);
        }
        return ReinsurLocal;
    }

    public void setReinsurLocal(String aReinsurLocal)
    {
        ReinsurLocal = aReinsurLocal;
    }

    public String getProtItem()
    {
        if (ProtItem != null && !ProtItem.equals("") && SysConst.CHANGECHARSET == true)
        {
            ProtItem = StrTool.unicodeToGBK(ProtItem);
        }
        return ProtItem;
    }

    public void setProtItem(String aProtItem)
    {
        ProtItem = aProtItem;
    }

    public String getInOutFlag()
    {
        if (InOutFlag != null && !InOutFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InOutFlag = StrTool.unicodeToGBK(InOutFlag);
        }
        return InOutFlag;
    }

    public void setInOutFlag(String aInOutFlag)
    {
        InOutFlag = aInOutFlag;
    }

    public String getSaleChnl()
    {
        if (SaleChnl != null && !SaleChnl.equals("") && SysConst.CHANGECHARSET == true)
        {
            SaleChnl = StrTool.unicodeToGBK(SaleChnl);
        }
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl)
    {
        SaleChnl = aSaleChnl;
    }

    public String getPersonPolFlag()
    {
        if (PersonPolFlag != null && !PersonPolFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PersonPolFlag = StrTool.unicodeToGBK(PersonPolFlag);
        }
        return PersonPolFlag;
    }

    public void setPersonPolFlag(String aPersonPolFlag)
    {
        PersonPolFlag = aPersonPolFlag;
    }

    public String getAnnulFlag()
    {
        if (AnnulFlag != null && !AnnulFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AnnulFlag = StrTool.unicodeToGBK(AnnulFlag);
        }
        return AnnulFlag;
    }

    public void setAnnulFlag(String aAnnulFlag)
    {
        AnnulFlag = aAnnulFlag;
    }

    public String getAgentComFlag()
    {
        if (AgentComFlag != null && !AgentComFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentComFlag = StrTool.unicodeToGBK(AgentComFlag);
        }
        return AgentComFlag;
    }

    public void setAgentComFlag(String aAgentComFlag)
    {
        AgentComFlag = aAgentComFlag;
    }

    public String getCessFeeType()
    {
        if (CessFeeType != null && !CessFeeType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CessFeeType = StrTool.unicodeToGBK(CessFeeType);
        }
        return CessFeeType;
    }

    public void setCessFeeType(String aCessFeeType)
    {
        CessFeeType = aCessFeeType;
    }

    public double getCessMoney()
    {
        return CessMoney;
    }

    public void setCessMoney(double aCessMoney)
    {
        CessMoney = aCessMoney;
    }

    public void setCessMoney(String aCessMoney)
    {
        if (aCessMoney != null && !aCessMoney.equals(""))
        {
            Double tDouble = new Double(aCessMoney);
            double d = tDouble.doubleValue();
            CessMoney = d;
        }
    }

    public String getReportDate()
    {
        if (ReportDate != null)
        {
            return fDate.getString(ReportDate);
        }
        else
        {
            return null;
        }
    }

    public void setReportDate(Date aReportDate)
    {
        ReportDate = aReportDate;
    }

    public void setReportDate(String aReportDate)
    {
        if (aReportDate != null && !aReportDate.equals(""))
        {
            ReportDate = fDate.getDate(aReportDate);
        }
        else
        {
            ReportDate = null;
        }
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LFReinsureSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFReinsureSchema aLFReinsureSchema)
    {
        this.RiskCode = aLFReinsureSchema.getRiskCode();
        this.ManageCom = aLFReinsureSchema.getManageCom();
        this.ReinsurItem = aLFReinsureSchema.getReinsurItem();
        this.ReinsurLocal = aLFReinsureSchema.getReinsurLocal();
        this.ProtItem = aLFReinsureSchema.getProtItem();
        this.InOutFlag = aLFReinsureSchema.getInOutFlag();
        this.SaleChnl = aLFReinsureSchema.getSaleChnl();
        this.PersonPolFlag = aLFReinsureSchema.getPersonPolFlag();
        this.AnnulFlag = aLFReinsureSchema.getAnnulFlag();
        this.AgentComFlag = aLFReinsureSchema.getAgentComFlag();
        this.CessFeeType = aLFReinsureSchema.getCessFeeType();
        this.CessMoney = aLFReinsureSchema.getCessMoney();
        this.ReportDate = fDate.getDate(aLFReinsureSchema.getReportDate());
        this.MakeDate = fDate.getDate(aLFReinsureSchema.getMakeDate());
        this.MakeTime = aLFReinsureSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("ReinsurItem") == null)
            {
                this.ReinsurItem = null;
            }
            else
            {
                this.ReinsurItem = rs.getString("ReinsurItem").trim();
            }

            if (rs.getString("ReinsurLocal") == null)
            {
                this.ReinsurLocal = null;
            }
            else
            {
                this.ReinsurLocal = rs.getString("ReinsurLocal").trim();
            }

            if (rs.getString("ProtItem") == null)
            {
                this.ProtItem = null;
            }
            else
            {
                this.ProtItem = rs.getString("ProtItem").trim();
            }

            if (rs.getString("InOutFlag") == null)
            {
                this.InOutFlag = null;
            }
            else
            {
                this.InOutFlag = rs.getString("InOutFlag").trim();
            }

            if (rs.getString("SaleChnl") == null)
            {
                this.SaleChnl = null;
            }
            else
            {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            if (rs.getString("PersonPolFlag") == null)
            {
                this.PersonPolFlag = null;
            }
            else
            {
                this.PersonPolFlag = rs.getString("PersonPolFlag").trim();
            }

            if (rs.getString("AnnulFlag") == null)
            {
                this.AnnulFlag = null;
            }
            else
            {
                this.AnnulFlag = rs.getString("AnnulFlag").trim();
            }

            if (rs.getString("AgentComFlag") == null)
            {
                this.AgentComFlag = null;
            }
            else
            {
                this.AgentComFlag = rs.getString("AgentComFlag").trim();
            }

            if (rs.getString("CessFeeType") == null)
            {
                this.CessFeeType = null;
            }
            else
            {
                this.CessFeeType = rs.getString("CessFeeType").trim();
            }

            this.CessMoney = rs.getDouble("CessMoney");
            this.ReportDate = rs.getDate("ReportDate");
            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFReinsureSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFReinsureSchema getSchema()
    {
        LFReinsureSchema aLFReinsureSchema = new LFReinsureSchema();
        aLFReinsureSchema.setSchema(this);
        return aLFReinsureSchema;
    }

    public LFReinsureDB getDB()
    {
        LFReinsureDB aDBOper = new LFReinsureDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFReinsure描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReinsurItem)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReinsurLocal)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProtItem)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InOutFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SaleChnl)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PersonPolFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AnnulFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentComFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CessFeeType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(CessMoney) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ReportDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFReinsure>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            ReinsurItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            ReinsurLocal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            ProtItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            InOutFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            PersonPolFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            AnnulFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            AgentComFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            CessFeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
            CessMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            ReportDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFReinsureSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("ReinsurItem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReinsurItem));
        }
        if (FCode.equals("ReinsurLocal"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReinsurLocal));
        }
        if (FCode.equals("ProtItem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProtItem));
        }
        if (FCode.equals("InOutFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InOutFlag));
        }
        if (FCode.equals("SaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SaleChnl));
        }
        if (FCode.equals("PersonPolFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PersonPolFlag));
        }
        if (FCode.equals("AnnulFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AnnulFlag));
        }
        if (FCode.equals("AgentComFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentComFlag));
        }
        if (FCode.equals("CessFeeType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CessFeeType));
        }
        if (FCode.equals("CessMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CessMoney));
        }
        if (FCode.equals("ReportDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getReportDate()));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ReinsurItem);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ReinsurLocal);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ProtItem);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(InOutFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PersonPolFlag);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AnnulFlag);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AgentComFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(CessFeeType);
                break;
            case 11:
                strFieldValue = String.valueOf(CessMoney);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getReportDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("ReinsurItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReinsurItem = FValue.trim();
            }
            else
            {
                ReinsurItem = null;
            }
        }
        if (FCode.equals("ReinsurLocal"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReinsurLocal = FValue.trim();
            }
            else
            {
                ReinsurLocal = null;
            }
        }
        if (FCode.equals("ProtItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProtItem = FValue.trim();
            }
            else
            {
                ProtItem = null;
            }
        }
        if (FCode.equals("InOutFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InOutFlag = FValue.trim();
            }
            else
            {
                InOutFlag = null;
            }
        }
        if (FCode.equals("SaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
            {
                SaleChnl = null;
            }
        }
        if (FCode.equals("PersonPolFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PersonPolFlag = FValue.trim();
            }
            else
            {
                PersonPolFlag = null;
            }
        }
        if (FCode.equals("AnnulFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AnnulFlag = FValue.trim();
            }
            else
            {
                AnnulFlag = null;
            }
        }
        if (FCode.equals("AgentComFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentComFlag = FValue.trim();
            }
            else
            {
                AgentComFlag = null;
            }
        }
        if (FCode.equals("CessFeeType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CessFeeType = FValue.trim();
            }
            else
            {
                CessFeeType = null;
            }
        }
        if (FCode.equals("CessMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CessMoney = d;
            }
        }
        if (FCode.equals("ReportDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReportDate = fDate.getDate(FValue);
            }
            else
            {
                ReportDate = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFReinsureSchema other = (LFReinsureSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && ManageCom.equals(other.getManageCom())
                && ReinsurItem.equals(other.getReinsurItem())
                && ReinsurLocal.equals(other.getReinsurLocal())
                && ProtItem.equals(other.getProtItem())
                && InOutFlag.equals(other.getInOutFlag())
                && SaleChnl.equals(other.getSaleChnl())
                && PersonPolFlag.equals(other.getPersonPolFlag())
                && AnnulFlag.equals(other.getAnnulFlag())
                && AgentComFlag.equals(other.getAgentComFlag())
                && CessFeeType.equals(other.getCessFeeType())
                && CessMoney == other.getCessMoney()
                && fDate.getString(ReportDate).equals(other.getReportDate())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 1;
        }
        if (strFieldName.equals("ReinsurItem"))
        {
            return 2;
        }
        if (strFieldName.equals("ReinsurLocal"))
        {
            return 3;
        }
        if (strFieldName.equals("ProtItem"))
        {
            return 4;
        }
        if (strFieldName.equals("InOutFlag"))
        {
            return 5;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return 6;
        }
        if (strFieldName.equals("PersonPolFlag"))
        {
            return 7;
        }
        if (strFieldName.equals("AnnulFlag"))
        {
            return 8;
        }
        if (strFieldName.equals("AgentComFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("CessFeeType"))
        {
            return 10;
        }
        if (strFieldName.equals("CessMoney"))
        {
            return 11;
        }
        if (strFieldName.equals("ReportDate"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "ManageCom";
                break;
            case 2:
                strFieldName = "ReinsurItem";
                break;
            case 3:
                strFieldName = "ReinsurLocal";
                break;
            case 4:
                strFieldName = "ProtItem";
                break;
            case 5:
                strFieldName = "InOutFlag";
                break;
            case 6:
                strFieldName = "SaleChnl";
                break;
            case 7:
                strFieldName = "PersonPolFlag";
                break;
            case 8:
                strFieldName = "AnnulFlag";
                break;
            case 9:
                strFieldName = "AgentComFlag";
                break;
            case 10:
                strFieldName = "CessFeeType";
                break;
            case 11:
                strFieldName = "CessMoney";
                break;
            case 12:
                strFieldName = "ReportDate";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReinsurItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReinsurLocal"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProtItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InOutFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PersonPolFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AnnulFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentComFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CessFeeType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CessMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ReportDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
