/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATrainCourseDB;

/*
 * <p>ClassName: LATrainCourseSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险培训
 * @CreateDate：2017-07-20
 */
public class LATrainCourseSchema implements Schema, Cloneable
{
	// @Field
	/** 培训班编号 */
	private String CourseNo;
	/** 省公司代码 */
	private String PManageCom;
	/** 支中心代码 */
	private String CManageCom;
	/** 营业部 */
	private String AgentGroup;
	/** 培训开始时间 */
	private Date StartDate;
	/** 培训结束时间 */
	private Date EndDate;
	/** 培训地点 */
	private String TrainPlace;
	/** 培训班名称 */
	private String CourseName;
	/** 课程表 */
	private String CourseList;
	/** 学员名单 */
	private String TraineeList;
	/** 培训照片 */
	private String TrainPicture;
	/** 评估总结 */
	private String TrainConclusion;
	/** 展业类型 */
	private String BranchType;
	/** 销售渠道 */
	private String BranchType2;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LATrainCourseSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CourseNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LATrainCourseSchema cloned = (LATrainCourseSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCourseNo()
	{
		return CourseNo;
	}
	public void setCourseNo(String aCourseNo)
	{
		CourseNo = aCourseNo;
	}
	public String getPManageCom()
	{
		return PManageCom;
	}
	public void setPManageCom(String aPManageCom)
	{
		PManageCom = aPManageCom;
	}
	public String getCManageCom()
	{
		return CManageCom;
	}
	public void setCManageCom(String aCManageCom)
	{
		CManageCom = aCManageCom;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
		StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getEndDate()
	{
		if( EndDate != null )
			return fDate.getString(EndDate);
		else
			return null;
	}
	public void setEndDate(Date aEndDate)
	{
		EndDate = aEndDate;
	}
	public void setEndDate(String aEndDate)
	{
		if (aEndDate != null && !aEndDate.equals("") )
		{
			EndDate = fDate.getDate( aEndDate );
		}
		else
			EndDate = null;
	}

	public String getTrainPlace()
	{
		return TrainPlace;
	}
	public void setTrainPlace(String aTrainPlace)
	{
		TrainPlace = aTrainPlace;
	}
	public String getCourseName()
	{
		return CourseName;
	}
	public void setCourseName(String aCourseName)
	{
		CourseName = aCourseName;
	}
	public String getCourseList()
	{
		return CourseList;
	}
	public void setCourseList(String aCourseList)
	{
		CourseList = aCourseList;
	}
	public String getTraineeList()
	{
		return TraineeList;
	}
	public void setTraineeList(String aTraineeList)
	{
		TraineeList = aTraineeList;
	}
	public String getTrainPicture()
	{
		return TrainPicture;
	}
	public void setTrainPicture(String aTrainPicture)
	{
		TrainPicture = aTrainPicture;
	}
	public String getTrainConclusion()
	{
		return TrainConclusion;
	}
	public void setTrainConclusion(String aTrainConclusion)
	{
		TrainConclusion = aTrainConclusion;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LATrainCourseSchema 对象给 Schema 赋值
	* @param: aLATrainCourseSchema LATrainCourseSchema
	**/
	public void setSchema(LATrainCourseSchema aLATrainCourseSchema)
	{
		this.CourseNo = aLATrainCourseSchema.getCourseNo();
		this.PManageCom = aLATrainCourseSchema.getPManageCom();
		this.CManageCom = aLATrainCourseSchema.getCManageCom();
		this.AgentGroup = aLATrainCourseSchema.getAgentGroup();
		this.StartDate = fDate.getDate( aLATrainCourseSchema.getStartDate());
		this.EndDate = fDate.getDate( aLATrainCourseSchema.getEndDate());
		this.TrainPlace = aLATrainCourseSchema.getTrainPlace();
		this.CourseName = aLATrainCourseSchema.getCourseName();
		this.CourseList = aLATrainCourseSchema.getCourseList();
		this.TraineeList = aLATrainCourseSchema.getTraineeList();
		this.TrainPicture = aLATrainCourseSchema.getTrainPicture();
		this.TrainConclusion = aLATrainCourseSchema.getTrainConclusion();
		this.BranchType = aLATrainCourseSchema.getBranchType();
		this.BranchType2 = aLATrainCourseSchema.getBranchType2();
		this.Operator = aLATrainCourseSchema.getOperator();
		this.MakeDate = fDate.getDate( aLATrainCourseSchema.getMakeDate());
		this.MakeTime = aLATrainCourseSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLATrainCourseSchema.getModifyDate());
		this.ModifyTime = aLATrainCourseSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CourseNo") == null )
				this.CourseNo = null;
			else
				this.CourseNo = rs.getString("CourseNo").trim();

			if( rs.getString("PManageCom") == null )
				this.PManageCom = null;
			else
				this.PManageCom = rs.getString("PManageCom").trim();

			if( rs.getString("CManageCom") == null )
				this.CManageCom = null;
			else
				this.CManageCom = rs.getString("CManageCom").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			this.StartDate = rs.getDate("StartDate");
			this.EndDate = rs.getDate("EndDate");
			if( rs.getString("TrainPlace") == null )
				this.TrainPlace = null;
			else
				this.TrainPlace = rs.getString("TrainPlace").trim();

			if( rs.getString("CourseName") == null )
				this.CourseName = null;
			else
				this.CourseName = rs.getString("CourseName").trim();

			if( rs.getString("CourseList") == null )
				this.CourseList = null;
			else
				this.CourseList = rs.getString("CourseList").trim();

			if( rs.getString("TraineeList") == null )
				this.TraineeList = null;
			else
				this.TraineeList = rs.getString("TraineeList").trim();

			if( rs.getString("TrainPicture") == null )
				this.TrainPicture = null;
			else
				this.TrainPicture = rs.getString("TrainPicture").trim();

			if( rs.getString("TrainConclusion") == null )
				this.TrainConclusion = null;
			else
				this.TrainConclusion = rs.getString("TrainConclusion").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LATrainCourse表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainCourseSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LATrainCourseSchema getSchema()
	{
		LATrainCourseSchema aLATrainCourseSchema = new LATrainCourseSchema();
		aLATrainCourseSchema.setSchema(this);
		return aLATrainCourseSchema;
	}

	public LATrainCourseDB getDB()
	{
		LATrainCourseDB aDBOper = new LATrainCourseDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainCourse描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CourseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainPlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CourseName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CourseList)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TraineeList)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainPicture)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainConclusion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainCourse>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CourseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			TrainPlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CourseName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CourseList = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			TraineeList = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			TrainPicture = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			TrainConclusion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainCourseSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CourseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CourseNo));
		}
		if (FCode.equals("PManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PManageCom));
		}
		if (FCode.equals("CManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CManageCom));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("EndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
		}
		if (FCode.equals("TrainPlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainPlace));
		}
		if (FCode.equals("CourseName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CourseName));
		}
		if (FCode.equals("CourseList"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CourseList));
		}
		if (FCode.equals("TraineeList"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TraineeList));
		}
		if (FCode.equals("TrainPicture"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainPicture));
		}
		if (FCode.equals("TrainConclusion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainConclusion));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CourseNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PManageCom);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CManageCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(TrainPlace);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CourseName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(CourseList);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(TraineeList);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(TrainPicture);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(TrainConclusion);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CourseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CourseNo = FValue.trim();
			}
			else
				CourseNo = null;
		}
		if (FCode.equalsIgnoreCase("PManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PManageCom = FValue.trim();
			}
			else
				PManageCom = null;
		}
		if (FCode.equalsIgnoreCase("CManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CManageCom = FValue.trim();
			}
			else
				CManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equalsIgnoreCase("EndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndDate = fDate.getDate( FValue );
			}
			else
				EndDate = null;
		}
		if (FCode.equalsIgnoreCase("TrainPlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainPlace = FValue.trim();
			}
			else
				TrainPlace = null;
		}
		if (FCode.equalsIgnoreCase("CourseName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CourseName = FValue.trim();
			}
			else
				CourseName = null;
		}
		if (FCode.equalsIgnoreCase("CourseList"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CourseList = FValue.trim();
			}
			else
				CourseList = null;
		}
		if (FCode.equalsIgnoreCase("TraineeList"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TraineeList = FValue.trim();
			}
			else
				TraineeList = null;
		}
		if (FCode.equalsIgnoreCase("TrainPicture"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainPicture = FValue.trim();
			}
			else
				TrainPicture = null;
		}
		if (FCode.equalsIgnoreCase("TrainConclusion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainConclusion = FValue.trim();
			}
			else
				TrainConclusion = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LATrainCourseSchema other = (LATrainCourseSchema)otherObject;
		return
			(CourseNo == null ? other.getCourseNo() == null : CourseNo.equals(other.getCourseNo()))
			&& (PManageCom == null ? other.getPManageCom() == null : PManageCom.equals(other.getPManageCom()))
			&& (CManageCom == null ? other.getCManageCom() == null : CManageCom.equals(other.getCManageCom()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (StartDate == null ? other.getStartDate() == null : fDate.getString(StartDate).equals(other.getStartDate()))
			&& (EndDate == null ? other.getEndDate() == null : fDate.getString(EndDate).equals(other.getEndDate()))
			&& (TrainPlace == null ? other.getTrainPlace() == null : TrainPlace.equals(other.getTrainPlace()))
			&& (CourseName == null ? other.getCourseName() == null : CourseName.equals(other.getCourseName()))
			&& (CourseList == null ? other.getCourseList() == null : CourseList.equals(other.getCourseList()))
			&& (TraineeList == null ? other.getTraineeList() == null : TraineeList.equals(other.getTraineeList()))
			&& (TrainPicture == null ? other.getTrainPicture() == null : TrainPicture.equals(other.getTrainPicture()))
			&& (TrainConclusion == null ? other.getTrainConclusion() == null : TrainConclusion.equals(other.getTrainConclusion()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CourseNo") ) {
			return 0;
		}
		if( strFieldName.equals("PManageCom") ) {
			return 1;
		}
		if( strFieldName.equals("CManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 3;
		}
		if( strFieldName.equals("StartDate") ) {
			return 4;
		}
		if( strFieldName.equals("EndDate") ) {
			return 5;
		}
		if( strFieldName.equals("TrainPlace") ) {
			return 6;
		}
		if( strFieldName.equals("CourseName") ) {
			return 7;
		}
		if( strFieldName.equals("CourseList") ) {
			return 8;
		}
		if( strFieldName.equals("TraineeList") ) {
			return 9;
		}
		if( strFieldName.equals("TrainPicture") ) {
			return 10;
		}
		if( strFieldName.equals("TrainConclusion") ) {
			return 11;
		}
		if( strFieldName.equals("BranchType") ) {
			return 12;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 13;
		}
		if( strFieldName.equals("Operator") ) {
			return 14;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 15;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CourseNo";
				break;
			case 1:
				strFieldName = "PManageCom";
				break;
			case 2:
				strFieldName = "CManageCom";
				break;
			case 3:
				strFieldName = "AgentGroup";
				break;
			case 4:
				strFieldName = "StartDate";
				break;
			case 5:
				strFieldName = "EndDate";
				break;
			case 6:
				strFieldName = "TrainPlace";
				break;
			case 7:
				strFieldName = "CourseName";
				break;
			case 8:
				strFieldName = "CourseList";
				break;
			case 9:
				strFieldName = "TraineeList";
				break;
			case 10:
				strFieldName = "TrainPicture";
				break;
			case 11:
				strFieldName = "TrainConclusion";
				break;
			case 12:
				strFieldName = "BranchType";
				break;
			case 13:
				strFieldName = "BranchType2";
				break;
			case 14:
				strFieldName = "Operator";
				break;
			case 15:
				strFieldName = "MakeDate";
				break;
			case 16:
				strFieldName = "MakeTime";
				break;
			case 17:
				strFieldName = "ModifyDate";
				break;
			case 18:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CourseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TrainPlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CourseName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CourseList") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TraineeList") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainPicture") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainConclusion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
