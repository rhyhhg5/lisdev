/*
 * <p>ClassName: LPComSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LPComDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LPComSchema implements Schema
{
    // @Field
    /** 上报申请号 */
    private String AppID;
    /** 上报项目 */
    private String AppItem;
    /** 机构编码 */
    private String ComCode;
    /** 对外公布的机构代码 */
    private String OutComCode;
    /** 机构名称 */
    private String Name;
    /** 短名称 */
    private String ShortName;
    /** 机构地址 */
    private String Address;
    /** 机构邮编 */
    private String ZipCode;
    /** 机构电话 */
    private String Phone;
    /** 机构传真 */
    private String Fax;
    /** Email */
    private String EMail;
    /** 网址 */
    private String WebAddress;
    /** 主管人姓名 */
    private String SatrapName;
    /** 标志 */
    private String Sign;
    /** 对应保监办代码 */
    private String InsuMonitorCode;
    /** 保险公司id */
    private String InsureID;
    /** 标识码 */
    private String SignID;
    /** 行政区划代码 */
    private String RegionalismCode;
    /** 公司性质 */
    private String ComNature;
    /** 校验码 */
    private String ValidCode;

    public static final int FIELDNUM = 20; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPComSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "AppID";
        pk[1] = "AppItem";
        pk[2] = "ComCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAppID()
    {
        if (AppID != null && !AppID.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppID = StrTool.unicodeToGBK(AppID);
        }
        return AppID;
    }

    public void setAppID(String aAppID)
    {
        AppID = aAppID;
    }

    public String getAppItem()
    {
        if (AppItem != null && !AppItem.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppItem = StrTool.unicodeToGBK(AppItem);
        }
        return AppItem;
    }

    public void setAppItem(String aAppItem)
    {
        AppItem = aAppItem;
    }

    public String getComCode()
    {
        if (ComCode != null && !ComCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ComCode = StrTool.unicodeToGBK(ComCode);
        }
        return ComCode;
    }

    public void setComCode(String aComCode)
    {
        ComCode = aComCode;
    }

    public String getOutComCode()
    {
        if (OutComCode != null && !OutComCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OutComCode = StrTool.unicodeToGBK(OutComCode);
        }
        return OutComCode;
    }

    public void setOutComCode(String aOutComCode)
    {
        OutComCode = aOutComCode;
    }

    public String getName()
    {
        if (Name != null && !Name.equals("") && SysConst.CHANGECHARSET == true)
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getShortName()
    {
        if (ShortName != null && !ShortName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ShortName = StrTool.unicodeToGBK(ShortName);
        }
        return ShortName;
    }

    public void setShortName(String aShortName)
    {
        ShortName = aShortName;
    }

    public String getAddress()
    {
        if (Address != null && !Address.equals("") && SysConst.CHANGECHARSET == true)
        {
            Address = StrTool.unicodeToGBK(Address);
        }
        return Address;
    }

    public void setAddress(String aAddress)
    {
        Address = aAddress;
    }

    public String getZipCode()
    {
        if (ZipCode != null && !ZipCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ZipCode = StrTool.unicodeToGBK(ZipCode);
        }
        return ZipCode;
    }

    public void setZipCode(String aZipCode)
    {
        ZipCode = aZipCode;
    }

    public String getPhone()
    {
        if (Phone != null && !Phone.equals("") && SysConst.CHANGECHARSET == true)
        {
            Phone = StrTool.unicodeToGBK(Phone);
        }
        return Phone;
    }

    public void setPhone(String aPhone)
    {
        Phone = aPhone;
    }

    public String getFax()
    {
        if (Fax != null && !Fax.equals("") && SysConst.CHANGECHARSET == true)
        {
            Fax = StrTool.unicodeToGBK(Fax);
        }
        return Fax;
    }

    public void setFax(String aFax)
    {
        Fax = aFax;
    }

    public String getEMail()
    {
        if (EMail != null && !EMail.equals("") && SysConst.CHANGECHARSET == true)
        {
            EMail = StrTool.unicodeToGBK(EMail);
        }
        return EMail;
    }

    public void setEMail(String aEMail)
    {
        EMail = aEMail;
    }

    public String getWebAddress()
    {
        if (WebAddress != null && !WebAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            WebAddress = StrTool.unicodeToGBK(WebAddress);
        }
        return WebAddress;
    }

    public void setWebAddress(String aWebAddress)
    {
        WebAddress = aWebAddress;
    }

    public String getSatrapName()
    {
        if (SatrapName != null && !SatrapName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SatrapName = StrTool.unicodeToGBK(SatrapName);
        }
        return SatrapName;
    }

    public void setSatrapName(String aSatrapName)
    {
        SatrapName = aSatrapName;
    }

    public String getSign()
    {
        if (Sign != null && !Sign.equals("") && SysConst.CHANGECHARSET == true)
        {
            Sign = StrTool.unicodeToGBK(Sign);
        }
        return Sign;
    }

    public void setSign(String aSign)
    {
        Sign = aSign;
    }

    public String getInsuMonitorCode()
    {
        if (InsuMonitorCode != null && !InsuMonitorCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuMonitorCode = StrTool.unicodeToGBK(InsuMonitorCode);
        }
        return InsuMonitorCode;
    }

    public void setInsuMonitorCode(String aInsuMonitorCode)
    {
        InsuMonitorCode = aInsuMonitorCode;
    }

    public String getInsureID()
    {
        if (InsureID != null && !InsureID.equals("") && SysConst.CHANGECHARSET == true)
        {
            InsureID = StrTool.unicodeToGBK(InsureID);
        }
        return InsureID;
    }

    public void setInsureID(String aInsureID)
    {
        InsureID = aInsureID;
    }

    public String getSignID()
    {
        if (SignID != null && !SignID.equals("") && SysConst.CHANGECHARSET == true)
        {
            SignID = StrTool.unicodeToGBK(SignID);
        }
        return SignID;
    }

    public void setSignID(String aSignID)
    {
        SignID = aSignID;
    }

    public String getRegionalismCode()
    {
        if (RegionalismCode != null && !RegionalismCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RegionalismCode = StrTool.unicodeToGBK(RegionalismCode);
        }
        return RegionalismCode;
    }

    public void setRegionalismCode(String aRegionalismCode)
    {
        RegionalismCode = aRegionalismCode;
    }

    public String getComNature()
    {
        if (ComNature != null && !ComNature.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ComNature = StrTool.unicodeToGBK(ComNature);
        }
        return ComNature;
    }

    public void setComNature(String aComNature)
    {
        ComNature = aComNature;
    }

    public String getValidCode()
    {
        if (ValidCode != null && !ValidCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ValidCode = StrTool.unicodeToGBK(ValidCode);
        }
        return ValidCode;
    }

    public void setValidCode(String aValidCode)
    {
        ValidCode = aValidCode;
    }

    /**
     * 使用另外一个 LPComSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LPComSchema aLPComSchema)
    {
        this.AppID = aLPComSchema.getAppID();
        this.AppItem = aLPComSchema.getAppItem();
        this.ComCode = aLPComSchema.getComCode();
        this.OutComCode = aLPComSchema.getOutComCode();
        this.Name = aLPComSchema.getName();
        this.ShortName = aLPComSchema.getShortName();
        this.Address = aLPComSchema.getAddress();
        this.ZipCode = aLPComSchema.getZipCode();
        this.Phone = aLPComSchema.getPhone();
        this.Fax = aLPComSchema.getFax();
        this.EMail = aLPComSchema.getEMail();
        this.WebAddress = aLPComSchema.getWebAddress();
        this.SatrapName = aLPComSchema.getSatrapName();
        this.Sign = aLPComSchema.getSign();
        this.InsuMonitorCode = aLPComSchema.getInsuMonitorCode();
        this.InsureID = aLPComSchema.getInsureID();
        this.SignID = aLPComSchema.getSignID();
        this.RegionalismCode = aLPComSchema.getRegionalismCode();
        this.ComNature = aLPComSchema.getComNature();
        this.ValidCode = aLPComSchema.getValidCode();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AppID") == null)
            {
                this.AppID = null;
            }
            else
            {
                this.AppID = rs.getString("AppID").trim();
            }

            if (rs.getString("AppItem") == null)
            {
                this.AppItem = null;
            }
            else
            {
                this.AppItem = rs.getString("AppItem").trim();
            }

            if (rs.getString("ComCode") == null)
            {
                this.ComCode = null;
            }
            else
            {
                this.ComCode = rs.getString("ComCode").trim();
            }

            if (rs.getString("OutComCode") == null)
            {
                this.OutComCode = null;
            }
            else
            {
                this.OutComCode = rs.getString("OutComCode").trim();
            }

            if (rs.getString("Name") == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString("Name").trim();
            }

            if (rs.getString("ShortName") == null)
            {
                this.ShortName = null;
            }
            else
            {
                this.ShortName = rs.getString("ShortName").trim();
            }

            if (rs.getString("Address") == null)
            {
                this.Address = null;
            }
            else
            {
                this.Address = rs.getString("Address").trim();
            }

            if (rs.getString("ZipCode") == null)
            {
                this.ZipCode = null;
            }
            else
            {
                this.ZipCode = rs.getString("ZipCode").trim();
            }

            if (rs.getString("Phone") == null)
            {
                this.Phone = null;
            }
            else
            {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("Fax") == null)
            {
                this.Fax = null;
            }
            else
            {
                this.Fax = rs.getString("Fax").trim();
            }

            if (rs.getString("EMail") == null)
            {
                this.EMail = null;
            }
            else
            {
                this.EMail = rs.getString("EMail").trim();
            }

            if (rs.getString("WebAddress") == null)
            {
                this.WebAddress = null;
            }
            else
            {
                this.WebAddress = rs.getString("WebAddress").trim();
            }

            if (rs.getString("SatrapName") == null)
            {
                this.SatrapName = null;
            }
            else
            {
                this.SatrapName = rs.getString("SatrapName").trim();
            }

            if (rs.getString("Sign") == null)
            {
                this.Sign = null;
            }
            else
            {
                this.Sign = rs.getString("Sign").trim();
            }

            if (rs.getString("InsuMonitorCode") == null)
            {
                this.InsuMonitorCode = null;
            }
            else
            {
                this.InsuMonitorCode = rs.getString("InsuMonitorCode").trim();
            }

            if (rs.getString("InsureID") == null)
            {
                this.InsureID = null;
            }
            else
            {
                this.InsureID = rs.getString("InsureID").trim();
            }

            if (rs.getString("SignID") == null)
            {
                this.SignID = null;
            }
            else
            {
                this.SignID = rs.getString("SignID").trim();
            }

            if (rs.getString("RegionalismCode") == null)
            {
                this.RegionalismCode = null;
            }
            else
            {
                this.RegionalismCode = rs.getString("RegionalismCode").trim();
            }

            if (rs.getString("ComNature") == null)
            {
                this.ComNature = null;
            }
            else
            {
                this.ComNature = rs.getString("ComNature").trim();
            }

            if (rs.getString("ValidCode") == null)
            {
                this.ValidCode = null;
            }
            else
            {
                this.ValidCode = rs.getString("ValidCode").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPComSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LPComSchema getSchema()
    {
        LPComSchema aLPComSchema = new LPComSchema();
        aLPComSchema.setSchema(this);
        return aLPComSchema;
    }

    public LPComDB getDB()
    {
        LPComDB aDBOper = new LPComDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPCom描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AppID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppItem)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ComCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OutComCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ShortName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Address)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ZipCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Phone)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Fax)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EMail)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(WebAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SatrapName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Sign)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuMonitorCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsureID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SignID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RegionalismCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ComNature)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ValidCode));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPCom>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AppID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            AppItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            OutComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                  SysConst.PACKAGESPILTER);
            ShortName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                 SysConst.PACKAGESPILTER);
            EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                   SysConst.PACKAGESPILTER);
            WebAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            SatrapName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            Sign = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                  SysConst.PACKAGESPILTER);
            InsuMonitorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             15, SysConst.PACKAGESPILTER);
            InsureID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            SignID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                    SysConst.PACKAGESPILTER);
            RegionalismCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             18, SysConst.PACKAGESPILTER);
            ComNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                       SysConst.PACKAGESPILTER);
            ValidCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                       SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPComSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AppID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppID));
        }
        if (FCode.equals("AppItem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppItem));
        }
        if (FCode.equals("ComCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComCode));
        }
        if (FCode.equals("OutComCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OutComCode));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Name));
        }
        if (FCode.equals("ShortName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ShortName));
        }
        if (FCode.equals("Address"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Address));
        }
        if (FCode.equals("ZipCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ZipCode));
        }
        if (FCode.equals("Phone"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Phone));
        }
        if (FCode.equals("Fax"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Fax));
        }
        if (FCode.equals("EMail"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EMail));
        }
        if (FCode.equals("WebAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WebAddress));
        }
        if (FCode.equals("SatrapName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SatrapName));
        }
        if (FCode.equals("Sign"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Sign));
        }
        if (FCode.equals("InsuMonitorCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuMonitorCode));
        }
        if (FCode.equals("InsureID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsureID));
        }
        if (FCode.equals("SignID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SignID));
        }
        if (FCode.equals("RegionalismCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RegionalismCode));
        }
        if (FCode.equals("ComNature"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComNature));
        }
        if (FCode.equals("ValidCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ValidCode));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AppID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AppItem);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OutComCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ShortName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Address);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Fax);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(EMail);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(WebAddress);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(SatrapName);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(Sign);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(InsuMonitorCode);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(InsureID);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(SignID);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(RegionalismCode);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(ComNature);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ValidCode);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AppID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppID = FValue.trim();
            }
            else
            {
                AppID = null;
            }
        }
        if (FCode.equals("AppItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppItem = FValue.trim();
            }
            else
            {
                AppItem = null;
            }
        }
        if (FCode.equals("ComCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
            {
                ComCode = null;
            }
        }
        if (FCode.equals("OutComCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OutComCode = FValue.trim();
            }
            else
            {
                OutComCode = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("ShortName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ShortName = FValue.trim();
            }
            else
            {
                ShortName = null;
            }
        }
        if (FCode.equals("Address"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Address = FValue.trim();
            }
            else
            {
                Address = null;
            }
        }
        if (FCode.equals("ZipCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
            {
                ZipCode = null;
            }
        }
        if (FCode.equals("Phone"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
            {
                Phone = null;
            }
        }
        if (FCode.equals("Fax"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Fax = FValue.trim();
            }
            else
            {
                Fax = null;
            }
        }
        if (FCode.equals("EMail"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EMail = FValue.trim();
            }
            else
            {
                EMail = null;
            }
        }
        if (FCode.equals("WebAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WebAddress = FValue.trim();
            }
            else
            {
                WebAddress = null;
            }
        }
        if (FCode.equals("SatrapName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SatrapName = FValue.trim();
            }
            else
            {
                SatrapName = null;
            }
        }
        if (FCode.equals("Sign"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Sign = FValue.trim();
            }
            else
            {
                Sign = null;
            }
        }
        if (FCode.equals("InsuMonitorCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuMonitorCode = FValue.trim();
            }
            else
            {
                InsuMonitorCode = null;
            }
        }
        if (FCode.equals("InsureID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsureID = FValue.trim();
            }
            else
            {
                InsureID = null;
            }
        }
        if (FCode.equals("SignID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SignID = FValue.trim();
            }
            else
            {
                SignID = null;
            }
        }
        if (FCode.equals("RegionalismCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RegionalismCode = FValue.trim();
            }
            else
            {
                RegionalismCode = null;
            }
        }
        if (FCode.equals("ComNature"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComNature = FValue.trim();
            }
            else
            {
                ComNature = null;
            }
        }
        if (FCode.equals("ValidCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ValidCode = FValue.trim();
            }
            else
            {
                ValidCode = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LPComSchema other = (LPComSchema) otherObject;
        return
                AppID.equals(other.getAppID())
                && AppItem.equals(other.getAppItem())
                && ComCode.equals(other.getComCode())
                && OutComCode.equals(other.getOutComCode())
                && Name.equals(other.getName())
                && ShortName.equals(other.getShortName())
                && Address.equals(other.getAddress())
                && ZipCode.equals(other.getZipCode())
                && Phone.equals(other.getPhone())
                && Fax.equals(other.getFax())
                && EMail.equals(other.getEMail())
                && WebAddress.equals(other.getWebAddress())
                && SatrapName.equals(other.getSatrapName())
                && Sign.equals(other.getSign())
                && InsuMonitorCode.equals(other.getInsuMonitorCode())
                && InsureID.equals(other.getInsureID())
                && SignID.equals(other.getSignID())
                && RegionalismCode.equals(other.getRegionalismCode())
                && ComNature.equals(other.getComNature())
                && ValidCode.equals(other.getValidCode());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return 0;
        }
        if (strFieldName.equals("AppItem"))
        {
            return 1;
        }
        if (strFieldName.equals("ComCode"))
        {
            return 2;
        }
        if (strFieldName.equals("OutComCode"))
        {
            return 3;
        }
        if (strFieldName.equals("Name"))
        {
            return 4;
        }
        if (strFieldName.equals("ShortName"))
        {
            return 5;
        }
        if (strFieldName.equals("Address"))
        {
            return 6;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return 7;
        }
        if (strFieldName.equals("Phone"))
        {
            return 8;
        }
        if (strFieldName.equals("Fax"))
        {
            return 9;
        }
        if (strFieldName.equals("EMail"))
        {
            return 10;
        }
        if (strFieldName.equals("WebAddress"))
        {
            return 11;
        }
        if (strFieldName.equals("SatrapName"))
        {
            return 12;
        }
        if (strFieldName.equals("Sign"))
        {
            return 13;
        }
        if (strFieldName.equals("InsuMonitorCode"))
        {
            return 14;
        }
        if (strFieldName.equals("InsureID"))
        {
            return 15;
        }
        if (strFieldName.equals("SignID"))
        {
            return 16;
        }
        if (strFieldName.equals("RegionalismCode"))
        {
            return 17;
        }
        if (strFieldName.equals("ComNature"))
        {
            return 18;
        }
        if (strFieldName.equals("ValidCode"))
        {
            return 19;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AppID";
                break;
            case 1:
                strFieldName = "AppItem";
                break;
            case 2:
                strFieldName = "ComCode";
                break;
            case 3:
                strFieldName = "OutComCode";
                break;
            case 4:
                strFieldName = "Name";
                break;
            case 5:
                strFieldName = "ShortName";
                break;
            case 6:
                strFieldName = "Address";
                break;
            case 7:
                strFieldName = "ZipCode";
                break;
            case 8:
                strFieldName = "Phone";
                break;
            case 9:
                strFieldName = "Fax";
                break;
            case 10:
                strFieldName = "EMail";
                break;
            case 11:
                strFieldName = "WebAddress";
                break;
            case 12:
                strFieldName = "SatrapName";
                break;
            case 13:
                strFieldName = "Sign";
                break;
            case 14:
                strFieldName = "InsuMonitorCode";
                break;
            case 15:
                strFieldName = "InsureID";
                break;
            case 16:
                strFieldName = "SignID";
                break;
            case 17:
                strFieldName = "RegionalismCode";
                break;
            case 18:
                strFieldName = "ComNature";
                break;
            case 19:
                strFieldName = "ValidCode";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OutComCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ShortName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Address"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Fax"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EMail"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WebAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SatrapName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Sign"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuMonitorCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsureID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SignID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RegionalismCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComNature"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ValidCode"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
