/*
 * <p>ClassName: LWFieldMapSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 工作流模型
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LWFieldMapDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LWFieldMapSchema implements Schema
{
    // @Field
    /** 活动id */
    private String ActivityID;
    /** 顺序号 */
    private int FieldOrder;
    /** 源表名 */
    private String SourTableName;
    /** 源字段 */
    private String SourFieldName;
    /** 源字段中文名 */
    private String SourFieldCName;
    /** 目标表名 */
    private String DestTableName;
    /** 目标字段 */
    private String DestFieldName;
    /** 目标字段中文名 */
    private String DestFieldCName;
    /** 从源到目标的取数规则 */
    private String GetValue;
    /** 从源到目标的取数规则类型 */
    private String GetValueType;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LWFieldMapSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ActivityID";
        pk[1] = "FieldOrder";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getActivityID()
    {
        if (ActivityID != null && !ActivityID.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ActivityID = StrTool.unicodeToGBK(ActivityID);
        }
        return ActivityID;
    }

    public void setActivityID(String aActivityID)
    {
        ActivityID = aActivityID;
    }

    public int getFieldOrder()
    {
        return FieldOrder;
    }

    public void setFieldOrder(int aFieldOrder)
    {
        FieldOrder = aFieldOrder;
    }

    public void setFieldOrder(String aFieldOrder)
    {
        if (aFieldOrder != null && !aFieldOrder.equals(""))
        {
            Integer tInteger = new Integer(aFieldOrder);
            int i = tInteger.intValue();
            FieldOrder = i;
        }
    }

    public String getSourTableName()
    {
        if (SourTableName != null && !SourTableName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SourTableName = StrTool.unicodeToGBK(SourTableName);
        }
        return SourTableName;
    }

    public void setSourTableName(String aSourTableName)
    {
        SourTableName = aSourTableName;
    }

    public String getSourFieldName()
    {
        if (SourFieldName != null && !SourFieldName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SourFieldName = StrTool.unicodeToGBK(SourFieldName);
        }
        return SourFieldName;
    }

    public void setSourFieldName(String aSourFieldName)
    {
        SourFieldName = aSourFieldName;
    }

    public String getSourFieldCName()
    {
        if (SourFieldCName != null && !SourFieldCName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SourFieldCName = StrTool.unicodeToGBK(SourFieldCName);
        }
        return SourFieldCName;
    }

    public void setSourFieldCName(String aSourFieldCName)
    {
        SourFieldCName = aSourFieldCName;
    }

    public String getDestTableName()
    {
        if (DestTableName != null && !DestTableName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DestTableName = StrTool.unicodeToGBK(DestTableName);
        }
        return DestTableName;
    }

    public void setDestTableName(String aDestTableName)
    {
        DestTableName = aDestTableName;
    }

    public String getDestFieldName()
    {
        if (DestFieldName != null && !DestFieldName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DestFieldName = StrTool.unicodeToGBK(DestFieldName);
        }
        return DestFieldName;
    }

    public void setDestFieldName(String aDestFieldName)
    {
        DestFieldName = aDestFieldName;
    }

    public String getDestFieldCName()
    {
        if (DestFieldCName != null && !DestFieldCName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DestFieldCName = StrTool.unicodeToGBK(DestFieldCName);
        }
        return DestFieldCName;
    }

    public void setDestFieldCName(String aDestFieldCName)
    {
        DestFieldCName = aDestFieldCName;
    }

    public String getGetValue()
    {
        if (GetValue != null && !GetValue.equals("") && SysConst.CHANGECHARSET == true)
        {
            GetValue = StrTool.unicodeToGBK(GetValue);
        }
        return GetValue;
    }

    public void setGetValue(String aGetValue)
    {
        GetValue = aGetValue;
    }

    public String getGetValueType()
    {
        if (GetValueType != null && !GetValueType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetValueType = StrTool.unicodeToGBK(GetValueType);
        }
        return GetValueType;
    }

    public void setGetValueType(String aGetValueType)
    {
        GetValueType = aGetValueType;
    }

    /**
     * 使用另外一个 LWFieldMapSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LWFieldMapSchema aLWFieldMapSchema)
    {
        this.ActivityID = aLWFieldMapSchema.getActivityID();
        this.FieldOrder = aLWFieldMapSchema.getFieldOrder();
        this.SourTableName = aLWFieldMapSchema.getSourTableName();
        this.SourFieldName = aLWFieldMapSchema.getSourFieldName();
        this.SourFieldCName = aLWFieldMapSchema.getSourFieldCName();
        this.DestTableName = aLWFieldMapSchema.getDestTableName();
        this.DestFieldName = aLWFieldMapSchema.getDestFieldName();
        this.DestFieldCName = aLWFieldMapSchema.getDestFieldCName();
        this.GetValue = aLWFieldMapSchema.getGetValue();
        this.GetValueType = aLWFieldMapSchema.getGetValueType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ActivityID") == null)
            {
                this.ActivityID = null;
            }
            else
            {
                this.ActivityID = rs.getString("ActivityID").trim();
            }

            this.FieldOrder = rs.getInt("FieldOrder");
            if (rs.getString("SourTableName") == null)
            {
                this.SourTableName = null;
            }
            else
            {
                this.SourTableName = rs.getString("SourTableName").trim();
            }

            if (rs.getString("SourFieldName") == null)
            {
                this.SourFieldName = null;
            }
            else
            {
                this.SourFieldName = rs.getString("SourFieldName").trim();
            }

            if (rs.getString("SourFieldCName") == null)
            {
                this.SourFieldCName = null;
            }
            else
            {
                this.SourFieldCName = rs.getString("SourFieldCName").trim();
            }

            if (rs.getString("DestTableName") == null)
            {
                this.DestTableName = null;
            }
            else
            {
                this.DestTableName = rs.getString("DestTableName").trim();
            }

            if (rs.getString("DestFieldName") == null)
            {
                this.DestFieldName = null;
            }
            else
            {
                this.DestFieldName = rs.getString("DestFieldName").trim();
            }

            if (rs.getString("DestFieldCName") == null)
            {
                this.DestFieldCName = null;
            }
            else
            {
                this.DestFieldCName = rs.getString("DestFieldCName").trim();
            }

            if (rs.getString("GetValue") == null)
            {
                this.GetValue = null;
            }
            else
            {
                this.GetValue = rs.getString("GetValue").trim();
            }

            if (rs.getString("GetValueType") == null)
            {
                this.GetValueType = null;
            }
            else
            {
                this.GetValueType = rs.getString("GetValueType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWFieldMapSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LWFieldMapSchema getSchema()
    {
        LWFieldMapSchema aLWFieldMapSchema = new LWFieldMapSchema();
        aLWFieldMapSchema.setSchema(this);
        return aLWFieldMapSchema;
    }

    public LWFieldMapDB getDB()
    {
        LWFieldMapDB aDBOper = new LWFieldMapDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWFieldMap描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ActivityID)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(FieldOrder) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SourTableName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SourFieldName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SourFieldCName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DestTableName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DestFieldName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DestFieldCName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetValue)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetValueType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWFieldMap>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ActivityID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            FieldOrder = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).intValue();
            SourTableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            SourFieldName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            SourFieldCName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                            SysConst.PACKAGESPILTER);
            DestTableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                           SysConst.PACKAGESPILTER);
            DestFieldName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            DestFieldCName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                            SysConst.PACKAGESPILTER);
            GetValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            GetValueType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWFieldMapSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ActivityID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ActivityID));
        }
        if (FCode.equals("FieldOrder"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FieldOrder));
        }
        if (FCode.equals("SourTableName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SourTableName));
        }
        if (FCode.equals("SourFieldName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SourFieldName));
        }
        if (FCode.equals("SourFieldCName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SourFieldCName));
        }
        if (FCode.equals("DestTableName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DestTableName));
        }
        if (FCode.equals("DestFieldName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DestFieldName));
        }
        if (FCode.equals("DestFieldCName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DestFieldCName));
        }
        if (FCode.equals("GetValue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetValue));
        }
        if (FCode.equals("GetValueType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetValueType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ActivityID);
                break;
            case 1:
                strFieldValue = String.valueOf(FieldOrder);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SourTableName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(SourFieldName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(SourFieldCName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DestTableName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(DestFieldName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(DestFieldCName);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(GetValue);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(GetValueType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ActivityID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ActivityID = FValue.trim();
            }
            else
            {
                ActivityID = null;
            }
        }
        if (FCode.equals("FieldOrder"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                FieldOrder = i;
            }
        }
        if (FCode.equals("SourTableName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SourTableName = FValue.trim();
            }
            else
            {
                SourTableName = null;
            }
        }
        if (FCode.equals("SourFieldName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SourFieldName = FValue.trim();
            }
            else
            {
                SourFieldName = null;
            }
        }
        if (FCode.equals("SourFieldCName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SourFieldCName = FValue.trim();
            }
            else
            {
                SourFieldCName = null;
            }
        }
        if (FCode.equals("DestTableName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestTableName = FValue.trim();
            }
            else
            {
                DestTableName = null;
            }
        }
        if (FCode.equals("DestFieldName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestFieldName = FValue.trim();
            }
            else
            {
                DestFieldName = null;
            }
        }
        if (FCode.equals("DestFieldCName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestFieldCName = FValue.trim();
            }
            else
            {
                DestFieldCName = null;
            }
        }
        if (FCode.equals("GetValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetValue = FValue.trim();
            }
            else
            {
                GetValue = null;
            }
        }
        if (FCode.equals("GetValueType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetValueType = FValue.trim();
            }
            else
            {
                GetValueType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LWFieldMapSchema other = (LWFieldMapSchema) otherObject;
        return
                ActivityID.equals(other.getActivityID())
                && FieldOrder == other.getFieldOrder()
                && SourTableName.equals(other.getSourTableName())
                && SourFieldName.equals(other.getSourFieldName())
                && SourFieldCName.equals(other.getSourFieldCName())
                && DestTableName.equals(other.getDestTableName())
                && DestFieldName.equals(other.getDestFieldName())
                && DestFieldCName.equals(other.getDestFieldCName())
                && GetValue.equals(other.getGetValue())
                && GetValueType.equals(other.getGetValueType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ActivityID"))
        {
            return 0;
        }
        if (strFieldName.equals("FieldOrder"))
        {
            return 1;
        }
        if (strFieldName.equals("SourTableName"))
        {
            return 2;
        }
        if (strFieldName.equals("SourFieldName"))
        {
            return 3;
        }
        if (strFieldName.equals("SourFieldCName"))
        {
            return 4;
        }
        if (strFieldName.equals("DestTableName"))
        {
            return 5;
        }
        if (strFieldName.equals("DestFieldName"))
        {
            return 6;
        }
        if (strFieldName.equals("DestFieldCName"))
        {
            return 7;
        }
        if (strFieldName.equals("GetValue"))
        {
            return 8;
        }
        if (strFieldName.equals("GetValueType"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ActivityID";
                break;
            case 1:
                strFieldName = "FieldOrder";
                break;
            case 2:
                strFieldName = "SourTableName";
                break;
            case 3:
                strFieldName = "SourFieldName";
                break;
            case 4:
                strFieldName = "SourFieldCName";
                break;
            case 5:
                strFieldName = "DestTableName";
                break;
            case 6:
                strFieldName = "DestFieldName";
                break;
            case 7:
                strFieldName = "DestFieldCName";
                break;
            case 8:
                strFieldName = "GetValue";
                break;
            case 9:
                strFieldName = "GetValueType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ActivityID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FieldOrder"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("SourTableName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SourFieldName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SourFieldCName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestTableName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestFieldName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestFieldCName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetValue"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetValueType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
