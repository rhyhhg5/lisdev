/*
 * <p>ClassName: LBTempFeeClassSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LBTempFeeClassDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LBTempFeeClassSchema implements Schema
{
    // @Field
    /** 暂交费收据号码 */
    private String TempFeeNo;
    /** 交费方式 */
    private String PayMode;
    /** 票据号 */
    private String ChequeNo;
    /** 交费金额 */
    private double PayMoney;
    /** 投保人名称 */
    private String APPntName;
    /** 交费日期 */
    private Date PayDate;
    /** 确认日期 */
    private Date ConfDate;
    /** 复核日期 */
    private Date ApproveDate;
    /** 到帐日期 */
    private Date EnterAccDate;
    /** 是否核销标志 */
    private String ConfFlag;
    /** 流水号 */
    private String SerialNo;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;
    /** 银行编码 */
    private String BankCode;
    /** 银行帐号 */
    private String BankAccNo;
    /** 银行帐户名 */
    private String AccName;
    /** 财务确认操作日期 */
    private Date ConfMakeDate;
    /** 备份流水号 */
    private String BackUpSerialNo;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LBTempFeeClassSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "TempFeeNo";
        pk[1] = "PayMode";
        pk[2] = "BackUpSerialNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getTempFeeNo()
    {
        if (TempFeeNo != null && !TempFeeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TempFeeNo = StrTool.unicodeToGBK(TempFeeNo);
        }
        return TempFeeNo;
    }

    public void setTempFeeNo(String aTempFeeNo)
    {
        TempFeeNo = aTempFeeNo;
    }

    public String getPayMode()
    {
        if (PayMode != null && !PayMode.equals("") && SysConst.CHANGECHARSET == true)
        {
            PayMode = StrTool.unicodeToGBK(PayMode);
        }
        return PayMode;
    }

    public void setPayMode(String aPayMode)
    {
        PayMode = aPayMode;
    }

    public String getChequeNo()
    {
        if (ChequeNo != null && !ChequeNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ChequeNo = StrTool.unicodeToGBK(ChequeNo);
        }
        return ChequeNo;
    }

    public void setChequeNo(String aChequeNo)
    {
        ChequeNo = aChequeNo;
    }

    public double getPayMoney()
    {
        return PayMoney;
    }

    public void setPayMoney(double aPayMoney)
    {
        PayMoney = aPayMoney;
    }

    public void setPayMoney(String aPayMoney)
    {
        if (aPayMoney != null && !aPayMoney.equals(""))
        {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = d;
        }
    }

    public String getAPPntName()
    {
        if (APPntName != null && !APPntName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            APPntName = StrTool.unicodeToGBK(APPntName);
        }
        return APPntName;
    }

    public void setAPPntName(String aAPPntName)
    {
        APPntName = aAPPntName;
    }

    public String getPayDate()
    {
        if (PayDate != null)
        {
            return fDate.getString(PayDate);
        }
        else
        {
            return null;
        }
    }

    public void setPayDate(Date aPayDate)
    {
        PayDate = aPayDate;
    }

    public void setPayDate(String aPayDate)
    {
        if (aPayDate != null && !aPayDate.equals(""))
        {
            PayDate = fDate.getDate(aPayDate);
        }
        else
        {
            PayDate = null;
        }
    }

    public String getConfDate()
    {
        if (ConfDate != null)
        {
            return fDate.getString(ConfDate);
        }
        else
        {
            return null;
        }
    }

    public void setConfDate(Date aConfDate)
    {
        ConfDate = aConfDate;
    }

    public void setConfDate(String aConfDate)
    {
        if (aConfDate != null && !aConfDate.equals(""))
        {
            ConfDate = fDate.getDate(aConfDate);
        }
        else
        {
            ConfDate = null;
        }
    }

    public String getApproveDate()
    {
        if (ApproveDate != null)
        {
            return fDate.getString(ApproveDate);
        }
        else
        {
            return null;
        }
    }

    public void setApproveDate(Date aApproveDate)
    {
        ApproveDate = aApproveDate;
    }

    public void setApproveDate(String aApproveDate)
    {
        if (aApproveDate != null && !aApproveDate.equals(""))
        {
            ApproveDate = fDate.getDate(aApproveDate);
        }
        else
        {
            ApproveDate = null;
        }
    }

    public String getEnterAccDate()
    {
        if (EnterAccDate != null)
        {
            return fDate.getString(EnterAccDate);
        }
        else
        {
            return null;
        }
    }

    public void setEnterAccDate(Date aEnterAccDate)
    {
        EnterAccDate = aEnterAccDate;
    }

    public void setEnterAccDate(String aEnterAccDate)
    {
        if (aEnterAccDate != null && !aEnterAccDate.equals(""))
        {
            EnterAccDate = fDate.getDate(aEnterAccDate);
        }
        else
        {
            EnterAccDate = null;
        }
    }

    public String getConfFlag()
    {
        if (ConfFlag != null && !ConfFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            ConfFlag = StrTool.unicodeToGBK(ConfFlag);
        }
        return ConfFlag;
    }

    public void setConfFlag(String aConfFlag)
    {
        ConfFlag = aConfFlag;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getBankCode()
    {
        if (BankCode != null && !BankCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            BankCode = StrTool.unicodeToGBK(BankCode);
        }
        return BankCode;
    }

    public void setBankCode(String aBankCode)
    {
        BankCode = aBankCode;
    }

    public String getBankAccNo()
    {
        if (BankAccNo != null && !BankAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BankAccNo = StrTool.unicodeToGBK(BankAccNo);
        }
        return BankAccNo;
    }

    public void setBankAccNo(String aBankAccNo)
    {
        BankAccNo = aBankAccNo;
    }

    public String getAccName()
    {
        if (AccName != null && !AccName.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccName = StrTool.unicodeToGBK(AccName);
        }
        return AccName;
    }

    public void setAccName(String aAccName)
    {
        AccName = aAccName;
    }

    public String getConfMakeDate()
    {
        if (ConfMakeDate != null)
        {
            return fDate.getString(ConfMakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setConfMakeDate(Date aConfMakeDate)
    {
        ConfMakeDate = aConfMakeDate;
    }

    public void setConfMakeDate(String aConfMakeDate)
    {
        if (aConfMakeDate != null && !aConfMakeDate.equals(""))
        {
            ConfMakeDate = fDate.getDate(aConfMakeDate);
        }
        else
        {
            ConfMakeDate = null;
        }
    }

    public String getBackUpSerialNo()
    {
        if (BackUpSerialNo != null && !BackUpSerialNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BackUpSerialNo = StrTool.unicodeToGBK(BackUpSerialNo);
        }
        return BackUpSerialNo;
    }

    public void setBackUpSerialNo(String aBackUpSerialNo)
    {
        BackUpSerialNo = aBackUpSerialNo;
    }

    /**
     * 使用另外一个 LBTempFeeClassSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LBTempFeeClassSchema aLBTempFeeClassSchema)
    {
        this.TempFeeNo = aLBTempFeeClassSchema.getTempFeeNo();
        this.PayMode = aLBTempFeeClassSchema.getPayMode();
        this.ChequeNo = aLBTempFeeClassSchema.getChequeNo();
        this.PayMoney = aLBTempFeeClassSchema.getPayMoney();
        this.APPntName = aLBTempFeeClassSchema.getAPPntName();
        this.PayDate = fDate.getDate(aLBTempFeeClassSchema.getPayDate());
        this.ConfDate = fDate.getDate(aLBTempFeeClassSchema.getConfDate());
        this.ApproveDate = fDate.getDate(aLBTempFeeClassSchema.getApproveDate());
        this.EnterAccDate = fDate.getDate(aLBTempFeeClassSchema.getEnterAccDate());
        this.ConfFlag = aLBTempFeeClassSchema.getConfFlag();
        this.SerialNo = aLBTempFeeClassSchema.getSerialNo();
        this.Operator = aLBTempFeeClassSchema.getOperator();
        this.MakeDate = fDate.getDate(aLBTempFeeClassSchema.getMakeDate());
        this.MakeTime = aLBTempFeeClassSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLBTempFeeClassSchema.getModifyDate());
        this.ModifyTime = aLBTempFeeClassSchema.getModifyTime();
        this.ManageCom = aLBTempFeeClassSchema.getManageCom();
        this.BankCode = aLBTempFeeClassSchema.getBankCode();
        this.BankAccNo = aLBTempFeeClassSchema.getBankAccNo();
        this.AccName = aLBTempFeeClassSchema.getAccName();
        this.ConfMakeDate = fDate.getDate(aLBTempFeeClassSchema.getConfMakeDate());
        this.BackUpSerialNo = aLBTempFeeClassSchema.getBackUpSerialNo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("TempFeeNo") == null)
            {
                this.TempFeeNo = null;
            }
            else
            {
                this.TempFeeNo = rs.getString("TempFeeNo").trim();
            }

            if (rs.getString("PayMode") == null)
            {
                this.PayMode = null;
            }
            else
            {
                this.PayMode = rs.getString("PayMode").trim();
            }

            if (rs.getString("ChequeNo") == null)
            {
                this.ChequeNo = null;
            }
            else
            {
                this.ChequeNo = rs.getString("ChequeNo").trim();
            }

            this.PayMoney = rs.getDouble("PayMoney");
            if (rs.getString("APPntName") == null)
            {
                this.APPntName = null;
            }
            else
            {
                this.APPntName = rs.getString("APPntName").trim();
            }

            this.PayDate = rs.getDate("PayDate");
            this.ConfDate = rs.getDate("ConfDate");
            this.ApproveDate = rs.getDate("ApproveDate");
            this.EnterAccDate = rs.getDate("EnterAccDate");
            if (rs.getString("ConfFlag") == null)
            {
                this.ConfFlag = null;
            }
            else
            {
                this.ConfFlag = rs.getString("ConfFlag").trim();
            }

            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("BankCode") == null)
            {
                this.BankCode = null;
            }
            else
            {
                this.BankCode = rs.getString("BankCode").trim();
            }

            if (rs.getString("BankAccNo") == null)
            {
                this.BankAccNo = null;
            }
            else
            {
                this.BankAccNo = rs.getString("BankAccNo").trim();
            }

            if (rs.getString("AccName") == null)
            {
                this.AccName = null;
            }
            else
            {
                this.AccName = rs.getString("AccName").trim();
            }

            this.ConfMakeDate = rs.getDate("ConfMakeDate");
            if (rs.getString("BackUpSerialNo") == null)
            {
                this.BackUpSerialNo = null;
            }
            else
            {
                this.BackUpSerialNo = rs.getString("BackUpSerialNo").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBTempFeeClassSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LBTempFeeClassSchema getSchema()
    {
        LBTempFeeClassSchema aLBTempFeeClassSchema = new LBTempFeeClassSchema();
        aLBTempFeeClassSchema.setSchema(this);
        return aLBTempFeeClassSchema;
    }

    public LBTempFeeClassDB getDB()
    {
        LBTempFeeClassDB aDBOper = new LBTempFeeClassDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBTempFeeClass描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(TempFeeNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayMode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ChequeNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayMoney) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(APPntName)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(PayDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ConfDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ApproveDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EnterAccDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ConfFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankAccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccName)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ConfMakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BackUpSerialNo));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBTempFeeClass>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            ChequeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            PayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            APPntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            PayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            EnterAccDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ConfFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                       SysConst.PACKAGESPILTER);
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                       SysConst.PACKAGESPILTER);
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                     SysConst.PACKAGESPILTER);
            ConfMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            BackUpSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            22, SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBTempFeeClassSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("TempFeeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TempFeeNo));
        }
        if (FCode.equals("PayMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayMode));
        }
        if (FCode.equals("ChequeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ChequeNo));
        }
        if (FCode.equals("PayMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayMoney));
        }
        if (FCode.equals("APPntName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(APPntName));
        }
        if (FCode.equals("PayDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getPayDate()));
        }
        if (FCode.equals("ConfDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getConfDate()));
        }
        if (FCode.equals("ApproveDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getApproveDate()));
        }
        if (FCode.equals("EnterAccDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getEnterAccDate()));
        }
        if (FCode.equals("ConfFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ConfFlag));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("BankCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankCode));
        }
        if (FCode.equals("BankAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankAccNo));
        }
        if (FCode.equals("AccName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccName));
        }
        if (FCode.equals("ConfMakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getConfMakeDate()));
        }
        if (FCode.equals("BackUpSerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BackUpSerialNo));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ChequeNo);
                break;
            case 3:
                strFieldValue = String.valueOf(PayMoney);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(APPntName);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPayDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getApproveDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEnterAccDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ConfFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(BankAccNo);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getConfMakeDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(BackUpSerialNo);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("TempFeeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TempFeeNo = FValue.trim();
            }
            else
            {
                TempFeeNo = null;
            }
        }
        if (FCode.equals("PayMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
            {
                PayMode = null;
            }
        }
        if (FCode.equals("ChequeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ChequeNo = FValue.trim();
            }
            else
            {
                ChequeNo = null;
            }
        }
        if (FCode.equals("PayMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        if (FCode.equals("APPntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                APPntName = FValue.trim();
            }
            else
            {
                APPntName = null;
            }
        }
        if (FCode.equals("PayDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayDate = fDate.getDate(FValue);
            }
            else
            {
                PayDate = null;
            }
        }
        if (FCode.equals("ConfDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfDate = fDate.getDate(FValue);
            }
            else
            {
                ConfDate = null;
            }
        }
        if (FCode.equals("ApproveDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ApproveDate = fDate.getDate(FValue);
            }
            else
            {
                ApproveDate = null;
            }
        }
        if (FCode.equals("EnterAccDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EnterAccDate = fDate.getDate(FValue);
            }
            else
            {
                EnterAccDate = null;
            }
        }
        if (FCode.equals("ConfFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfFlag = FValue.trim();
            }
            else
            {
                ConfFlag = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("BankCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
            {
                BankCode = null;
            }
        }
        if (FCode.equals("BankAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankAccNo = FValue.trim();
            }
            else
            {
                BankAccNo = null;
            }
        }
        if (FCode.equals("AccName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
            {
                AccName = null;
            }
        }
        if (FCode.equals("ConfMakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ConfMakeDate = fDate.getDate(FValue);
            }
            else
            {
                ConfMakeDate = null;
            }
        }
        if (FCode.equals("BackUpSerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BackUpSerialNo = FValue.trim();
            }
            else
            {
                BackUpSerialNo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LBTempFeeClassSchema other = (LBTempFeeClassSchema) otherObject;
        return
                TempFeeNo.equals(other.getTempFeeNo())
                && PayMode.equals(other.getPayMode())
                && ChequeNo.equals(other.getChequeNo())
                && PayMoney == other.getPayMoney()
                && APPntName.equals(other.getAPPntName())
                && fDate.getString(PayDate).equals(other.getPayDate())
                && fDate.getString(ConfDate).equals(other.getConfDate())
                && fDate.getString(ApproveDate).equals(other.getApproveDate())
                && fDate.getString(EnterAccDate).equals(other.getEnterAccDate())
                && ConfFlag.equals(other.getConfFlag())
                && SerialNo.equals(other.getSerialNo())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ManageCom.equals(other.getManageCom())
                && BankCode.equals(other.getBankCode())
                && BankAccNo.equals(other.getBankAccNo())
                && AccName.equals(other.getAccName())
                && fDate.getString(ConfMakeDate).equals(other.getConfMakeDate())
                && BackUpSerialNo.equals(other.getBackUpSerialNo());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("TempFeeNo"))
        {
            return 0;
        }
        if (strFieldName.equals("PayMode"))
        {
            return 1;
        }
        if (strFieldName.equals("ChequeNo"))
        {
            return 2;
        }
        if (strFieldName.equals("PayMoney"))
        {
            return 3;
        }
        if (strFieldName.equals("APPntName"))
        {
            return 4;
        }
        if (strFieldName.equals("PayDate"))
        {
            return 5;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return 6;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return 7;
        }
        if (strFieldName.equals("EnterAccDate"))
        {
            return 8;
        }
        if (strFieldName.equals("ConfFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 10;
        }
        if (strFieldName.equals("Operator"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 15;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 16;
        }
        if (strFieldName.equals("BankCode"))
        {
            return 17;
        }
        if (strFieldName.equals("BankAccNo"))
        {
            return 18;
        }
        if (strFieldName.equals("AccName"))
        {
            return 19;
        }
        if (strFieldName.equals("ConfMakeDate"))
        {
            return 20;
        }
        if (strFieldName.equals("BackUpSerialNo"))
        {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "TempFeeNo";
                break;
            case 1:
                strFieldName = "PayMode";
                break;
            case 2:
                strFieldName = "ChequeNo";
                break;
            case 3:
                strFieldName = "PayMoney";
                break;
            case 4:
                strFieldName = "APPntName";
                break;
            case 5:
                strFieldName = "PayDate";
                break;
            case 6:
                strFieldName = "ConfDate";
                break;
            case 7:
                strFieldName = "ApproveDate";
                break;
            case 8:
                strFieldName = "EnterAccDate";
                break;
            case 9:
                strFieldName = "ConfFlag";
                break;
            case 10:
                strFieldName = "SerialNo";
                break;
            case 11:
                strFieldName = "Operator";
                break;
            case 12:
                strFieldName = "MakeDate";
                break;
            case 13:
                strFieldName = "MakeTime";
                break;
            case 14:
                strFieldName = "ModifyDate";
                break;
            case 15:
                strFieldName = "ModifyTime";
                break;
            case 16:
                strFieldName = "ManageCom";
                break;
            case 17:
                strFieldName = "BankCode";
                break;
            case 18:
                strFieldName = "BankAccNo";
                break;
            case 19:
                strFieldName = "AccName";
                break;
            case 20:
                strFieldName = "ConfMakeDate";
                break;
            case 21:
                strFieldName = "BackUpSerialNo";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("TempFeeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChequeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("APPntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ConfDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ApproveDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EnterAccDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ConfFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ConfMakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BackUpSerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
