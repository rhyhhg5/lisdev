/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATreeDB;

/*
 * <p>ClassName: LATreeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 营业组组编码
 * @CreateDate：2015-04-18
 */
public class LATreeSchema implements Schema, Cloneable
{
	// @Field
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人展业机构代码 */
	private String AgentGroup;
	/** 管理机构 */
	private String ManageCom;
	/** 代理人系列 */
	private String AgentSeries;
	/** 代理人级别 */
	private String AgentGrade;
	/** 代理人上次系列 */
	private String AgentLastSeries;
	/** 代理人上次级别 */
	private String AgentLastGrade;
	/** 增员代理人 */
	private String IntroAgency;
	/** 上级代理人 */
	private String UpAgent;
	/** 其他上级代理人 */
	private String OthUpAgent;
	/** 增员链断裂标记 */
	private String IntroBreakFlag;
	/** 增员抽佣起期 */
	private Date IntroCommStart;
	/** 增员抽佣止期 */
	private Date IntroCommEnd;
	/** 育成主管代理人 */
	private String EduManager;
	/** 育成链断裂标记 */
	private String RearBreakFlag;
	/** 育成抽佣起期 */
	private Date RearCommStart;
	/** 育成抽佣止期 */
	private Date RearCommEnd;
	/** 归属顺序 */
	private String AscriptSeries;
	/** 前职级起聘日期 */
	private Date OldStartDate;
	/** 前职级解聘日期 */
	private Date OldEndDate;
	/** 现职级起聘日期 */
	private Date StartDate;
	/** 考核开始日期 */
	private Date AstartDate;
	/** 考核类型 */
	private String AssessType;
	/** 考核状态 */
	private String State;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 代理人组别 */
	private String BranchCode;
	/** 代理人类别 */
	private String AgentKind;
	/** 展业类型 */
	private String BranchType;
	/** 同业衔接人员标记 */
	private String isConnMan;
	/** 入司职级 */
	private String InitGrade;
	/** 代理人发展路线 */
	private String AgentLine;
	/** 内外勤标记 */
	private String InsideFlag;
	/** 特殊人员标记 */
	private String SpeciFlag;
	/** 渠道 */
	private String BranchType2;
	/** 业务员vip属性 */
	private String VIPProperty;
	/** 督导人编码 */
	private String RoleAgentCode;
	/** 督导人角色标识 */
	private String RoleFlag;
	/** 辅导人 */
	private String TutorShip;
	/** 转正标记 */
	private String InDueFormFlag;
	/** 转正时间 */
	private Date InDueFormDate;
	/** 辅助职级 */
	private String AgentGrade1;
	/** 辅助职级系列 */
	private String AgentSeries1;
	/** 辅助职级起期 */
	private Date StartDate1;
	/** 薪资版本 */
	private String WageVersion;
	/** 营业组编码 */
	private String AgentGroup2;

	public static final int FIELDNUM = 49;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LATreeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "AgentCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LATreeSchema cloned = (LATreeSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getAgentSeries()
	{
		return AgentSeries;
	}
	public void setAgentSeries(String aAgentSeries)
	{
		AgentSeries = aAgentSeries;
	}
	public String getAgentGrade()
	{
		return AgentGrade;
	}
	public void setAgentGrade(String aAgentGrade)
	{
		AgentGrade = aAgentGrade;
	}
	public String getAgentLastSeries()
	{
		return AgentLastSeries;
	}
	public void setAgentLastSeries(String aAgentLastSeries)
	{
		AgentLastSeries = aAgentLastSeries;
	}
	public String getAgentLastGrade()
	{
		return AgentLastGrade;
	}
	public void setAgentLastGrade(String aAgentLastGrade)
	{
		AgentLastGrade = aAgentLastGrade;
	}
	public String getIntroAgency()
	{
		return IntroAgency;
	}
	public void setIntroAgency(String aIntroAgency)
	{
		IntroAgency = aIntroAgency;
	}
	public String getUpAgent()
	{
		return UpAgent;
	}
	public void setUpAgent(String aUpAgent)
	{
		UpAgent = aUpAgent;
	}
	public String getOthUpAgent()
	{
		return OthUpAgent;
	}
	public void setOthUpAgent(String aOthUpAgent)
	{
		OthUpAgent = aOthUpAgent;
	}
	public String getIntroBreakFlag()
	{
		return IntroBreakFlag;
	}
	public void setIntroBreakFlag(String aIntroBreakFlag)
	{
		IntroBreakFlag = aIntroBreakFlag;
	}
	public String getIntroCommStart()
	{
		if( IntroCommStart != null )
			return fDate.getString(IntroCommStart);
		else
			return null;
	}
	public void setIntroCommStart(Date aIntroCommStart)
	{
		IntroCommStart = aIntroCommStart;
	}
	public void setIntroCommStart(String aIntroCommStart)
	{
		if (aIntroCommStart != null && !aIntroCommStart.equals("") )
		{
			IntroCommStart = fDate.getDate( aIntroCommStart );
		}
		else
			IntroCommStart = null;
	}

	public String getIntroCommEnd()
	{
		if( IntroCommEnd != null )
			return fDate.getString(IntroCommEnd);
		else
			return null;
	}
	public void setIntroCommEnd(Date aIntroCommEnd)
	{
		IntroCommEnd = aIntroCommEnd;
	}
	public void setIntroCommEnd(String aIntroCommEnd)
	{
		if (aIntroCommEnd != null && !aIntroCommEnd.equals("") )
		{
			IntroCommEnd = fDate.getDate( aIntroCommEnd );
		}
		else
			IntroCommEnd = null;
	}

	public String getEduManager()
	{
		return EduManager;
	}
	public void setEduManager(String aEduManager)
	{
		EduManager = aEduManager;
	}
	public String getRearBreakFlag()
	{
		return RearBreakFlag;
	}
	public void setRearBreakFlag(String aRearBreakFlag)
	{
		RearBreakFlag = aRearBreakFlag;
	}
	public String getRearCommStart()
	{
		if( RearCommStart != null )
			return fDate.getString(RearCommStart);
		else
			return null;
	}
	public void setRearCommStart(Date aRearCommStart)
	{
		RearCommStart = aRearCommStart;
	}
	public void setRearCommStart(String aRearCommStart)
	{
		if (aRearCommStart != null && !aRearCommStart.equals("") )
		{
			RearCommStart = fDate.getDate( aRearCommStart );
		}
		else
			RearCommStart = null;
	}

	public String getRearCommEnd()
	{
		if( RearCommEnd != null )
			return fDate.getString(RearCommEnd);
		else
			return null;
	}
	public void setRearCommEnd(Date aRearCommEnd)
	{
		RearCommEnd = aRearCommEnd;
	}
	public void setRearCommEnd(String aRearCommEnd)
	{
		if (aRearCommEnd != null && !aRearCommEnd.equals("") )
		{
			RearCommEnd = fDate.getDate( aRearCommEnd );
		}
		else
			RearCommEnd = null;
	}

	public String getAscriptSeries()
	{
		return AscriptSeries;
	}
	public void setAscriptSeries(String aAscriptSeries)
	{
		AscriptSeries = aAscriptSeries;
	}
	public String getOldStartDate()
	{
		if( OldStartDate != null )
			return fDate.getString(OldStartDate);
		else
			return null;
	}
	public void setOldStartDate(Date aOldStartDate)
	{
		OldStartDate = aOldStartDate;
	}
	public void setOldStartDate(String aOldStartDate)
	{
		if (aOldStartDate != null && !aOldStartDate.equals("") )
		{
			OldStartDate = fDate.getDate( aOldStartDate );
		}
		else
			OldStartDate = null;
	}

	public String getOldEndDate()
	{
		if( OldEndDate != null )
			return fDate.getString(OldEndDate);
		else
			return null;
	}
	public void setOldEndDate(Date aOldEndDate)
	{
		OldEndDate = aOldEndDate;
	}
	public void setOldEndDate(String aOldEndDate)
	{
		if (aOldEndDate != null && !aOldEndDate.equals("") )
		{
			OldEndDate = fDate.getDate( aOldEndDate );
		}
		else
			OldEndDate = null;
	}

	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
		StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getAstartDate()
	{
		if( AstartDate != null )
			return fDate.getString(AstartDate);
		else
			return null;
	}
	public void setAstartDate(Date aAstartDate)
	{
		AstartDate = aAstartDate;
	}
	public void setAstartDate(String aAstartDate)
	{
		if (aAstartDate != null && !aAstartDate.equals("") )
		{
			AstartDate = fDate.getDate( aAstartDate );
		}
		else
			AstartDate = null;
	}

	public String getAssessType()
	{
		return AssessType;
	}
	public void setAssessType(String aAssessType)
	{
		AssessType = aAssessType;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBranchCode()
	{
		return BranchCode;
	}
	public void setBranchCode(String aBranchCode)
	{
		BranchCode = aBranchCode;
	}
	public String getAgentKind()
	{
		return AgentKind;
	}
	public void setAgentKind(String aAgentKind)
	{
		AgentKind = aAgentKind;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getisConnMan()
	{
		return isConnMan;
	}
	public void setisConnMan(String aisConnMan)
	{
		isConnMan = aisConnMan;
	}
	public String getInitGrade()
	{
		return InitGrade;
	}
	public void setInitGrade(String aInitGrade)
	{
		InitGrade = aInitGrade;
	}
	public String getAgentLine()
	{
		return AgentLine;
	}
	public void setAgentLine(String aAgentLine)
	{
		AgentLine = aAgentLine;
	}
	public String getInsideFlag()
	{
		return InsideFlag;
	}
	public void setInsideFlag(String aInsideFlag)
	{
		InsideFlag = aInsideFlag;
	}
	public String getSpeciFlag()
	{
		return SpeciFlag;
	}
	public void setSpeciFlag(String aSpeciFlag)
	{
		SpeciFlag = aSpeciFlag;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getVIPProperty()
	{
		return VIPProperty;
	}
	public void setVIPProperty(String aVIPProperty)
	{
		VIPProperty = aVIPProperty;
	}
	public String getRoleAgentCode()
	{
		return RoleAgentCode;
	}
	public void setRoleAgentCode(String aRoleAgentCode)
	{
		RoleAgentCode = aRoleAgentCode;
	}
	public String getRoleFlag()
	{
		return RoleFlag;
	}
	public void setRoleFlag(String aRoleFlag)
	{
		RoleFlag = aRoleFlag;
	}
	public String getTutorShip()
	{
		return TutorShip;
	}
	public void setTutorShip(String aTutorShip)
	{
		TutorShip = aTutorShip;
	}
	public String getInDueFormFlag()
	{
		return InDueFormFlag;
	}
	public void setInDueFormFlag(String aInDueFormFlag)
	{
		InDueFormFlag = aInDueFormFlag;
	}
	public String getInDueFormDate()
	{
		if( InDueFormDate != null )
			return fDate.getString(InDueFormDate);
		else
			return null;
	}
	public void setInDueFormDate(Date aInDueFormDate)
	{
		InDueFormDate = aInDueFormDate;
	}
	public void setInDueFormDate(String aInDueFormDate)
	{
		if (aInDueFormDate != null && !aInDueFormDate.equals("") )
		{
			InDueFormDate = fDate.getDate( aInDueFormDate );
		}
		else
			InDueFormDate = null;
	}

	public String getAgentGrade1()
	{
		return AgentGrade1;
	}
	public void setAgentGrade1(String aAgentGrade1)
	{
		AgentGrade1 = aAgentGrade1;
	}
	public String getAgentSeries1()
	{
		return AgentSeries1;
	}
	public void setAgentSeries1(String aAgentSeries1)
	{
		AgentSeries1 = aAgentSeries1;
	}
	public String getStartDate1()
	{
		if( StartDate1 != null )
			return fDate.getString(StartDate1);
		else
			return null;
	}
	public void setStartDate1(Date aStartDate1)
	{
		StartDate1 = aStartDate1;
	}
	public void setStartDate1(String aStartDate1)
	{
		if (aStartDate1 != null && !aStartDate1.equals("") )
		{
			StartDate1 = fDate.getDate( aStartDate1 );
		}
		else
			StartDate1 = null;
	}

	public String getWageVersion()
	{
		return WageVersion;
	}
	public void setWageVersion(String aWageVersion)
	{
		WageVersion = aWageVersion;
	}
	public String getAgentGroup2()
	{
		return AgentGroup2;
	}
	public void setAgentGroup2(String aAgentGroup2)
	{
		AgentGroup2 = aAgentGroup2;
	}

	/**
	* 使用另外一个 LATreeSchema 对象给 Schema 赋值
	* @param: aLATreeSchema LATreeSchema
	**/
	public void setSchema(LATreeSchema aLATreeSchema)
	{
		this.AgentCode = aLATreeSchema.getAgentCode();
		this.AgentGroup = aLATreeSchema.getAgentGroup();
		this.ManageCom = aLATreeSchema.getManageCom();
		this.AgentSeries = aLATreeSchema.getAgentSeries();
		this.AgentGrade = aLATreeSchema.getAgentGrade();
		this.AgentLastSeries = aLATreeSchema.getAgentLastSeries();
		this.AgentLastGrade = aLATreeSchema.getAgentLastGrade();
		this.IntroAgency = aLATreeSchema.getIntroAgency();
		this.UpAgent = aLATreeSchema.getUpAgent();
		this.OthUpAgent = aLATreeSchema.getOthUpAgent();
		this.IntroBreakFlag = aLATreeSchema.getIntroBreakFlag();
		this.IntroCommStart = fDate.getDate( aLATreeSchema.getIntroCommStart());
		this.IntroCommEnd = fDate.getDate( aLATreeSchema.getIntroCommEnd());
		this.EduManager = aLATreeSchema.getEduManager();
		this.RearBreakFlag = aLATreeSchema.getRearBreakFlag();
		this.RearCommStart = fDate.getDate( aLATreeSchema.getRearCommStart());
		this.RearCommEnd = fDate.getDate( aLATreeSchema.getRearCommEnd());
		this.AscriptSeries = aLATreeSchema.getAscriptSeries();
		this.OldStartDate = fDate.getDate( aLATreeSchema.getOldStartDate());
		this.OldEndDate = fDate.getDate( aLATreeSchema.getOldEndDate());
		this.StartDate = fDate.getDate( aLATreeSchema.getStartDate());
		this.AstartDate = fDate.getDate( aLATreeSchema.getAstartDate());
		this.AssessType = aLATreeSchema.getAssessType();
		this.State = aLATreeSchema.getState();
		this.Operator = aLATreeSchema.getOperator();
		this.MakeDate = fDate.getDate( aLATreeSchema.getMakeDate());
		this.MakeTime = aLATreeSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLATreeSchema.getModifyDate());
		this.ModifyTime = aLATreeSchema.getModifyTime();
		this.BranchCode = aLATreeSchema.getBranchCode();
		this.AgentKind = aLATreeSchema.getAgentKind();
		this.BranchType = aLATreeSchema.getBranchType();
		this.isConnMan = aLATreeSchema.getisConnMan();
		this.InitGrade = aLATreeSchema.getInitGrade();
		this.AgentLine = aLATreeSchema.getAgentLine();
		this.InsideFlag = aLATreeSchema.getInsideFlag();
		this.SpeciFlag = aLATreeSchema.getSpeciFlag();
		this.BranchType2 = aLATreeSchema.getBranchType2();
		this.VIPProperty = aLATreeSchema.getVIPProperty();
		this.RoleAgentCode = aLATreeSchema.getRoleAgentCode();
		this.RoleFlag = aLATreeSchema.getRoleFlag();
		this.TutorShip = aLATreeSchema.getTutorShip();
		this.InDueFormFlag = aLATreeSchema.getInDueFormFlag();
		this.InDueFormDate = fDate.getDate( aLATreeSchema.getInDueFormDate());
		this.AgentGrade1 = aLATreeSchema.getAgentGrade1();
		this.AgentSeries1 = aLATreeSchema.getAgentSeries1();
		this.StartDate1 = fDate.getDate( aLATreeSchema.getStartDate1());
		this.WageVersion = aLATreeSchema.getWageVersion();
		this.AgentGroup2 = aLATreeSchema.getAgentGroup2();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("AgentSeries") == null )
				this.AgentSeries = null;
			else
				this.AgentSeries = rs.getString("AgentSeries").trim();

			if( rs.getString("AgentGrade") == null )
				this.AgentGrade = null;
			else
				this.AgentGrade = rs.getString("AgentGrade").trim();

			if( rs.getString("AgentLastSeries") == null )
				this.AgentLastSeries = null;
			else
				this.AgentLastSeries = rs.getString("AgentLastSeries").trim();

			if( rs.getString("AgentLastGrade") == null )
				this.AgentLastGrade = null;
			else
				this.AgentLastGrade = rs.getString("AgentLastGrade").trim();

			if( rs.getString("IntroAgency") == null )
				this.IntroAgency = null;
			else
				this.IntroAgency = rs.getString("IntroAgency").trim();

			if( rs.getString("UpAgent") == null )
				this.UpAgent = null;
			else
				this.UpAgent = rs.getString("UpAgent").trim();

			if( rs.getString("OthUpAgent") == null )
				this.OthUpAgent = null;
			else
				this.OthUpAgent = rs.getString("OthUpAgent").trim();

			if( rs.getString("IntroBreakFlag") == null )
				this.IntroBreakFlag = null;
			else
				this.IntroBreakFlag = rs.getString("IntroBreakFlag").trim();

			this.IntroCommStart = rs.getDate("IntroCommStart");
			this.IntroCommEnd = rs.getDate("IntroCommEnd");
			if( rs.getString("EduManager") == null )
				this.EduManager = null;
			else
				this.EduManager = rs.getString("EduManager").trim();

			if( rs.getString("RearBreakFlag") == null )
				this.RearBreakFlag = null;
			else
				this.RearBreakFlag = rs.getString("RearBreakFlag").trim();

			this.RearCommStart = rs.getDate("RearCommStart");
			this.RearCommEnd = rs.getDate("RearCommEnd");
			if( rs.getString("AscriptSeries") == null )
				this.AscriptSeries = null;
			else
				this.AscriptSeries = rs.getString("AscriptSeries").trim();

			this.OldStartDate = rs.getDate("OldStartDate");
			this.OldEndDate = rs.getDate("OldEndDate");
			this.StartDate = rs.getDate("StartDate");
			this.AstartDate = rs.getDate("AstartDate");
			if( rs.getString("AssessType") == null )
				this.AssessType = null;
			else
				this.AssessType = rs.getString("AssessType").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BranchCode") == null )
				this.BranchCode = null;
			else
				this.BranchCode = rs.getString("BranchCode").trim();

			if( rs.getString("AgentKind") == null )
				this.AgentKind = null;
			else
				this.AgentKind = rs.getString("AgentKind").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("isConnMan") == null )
				this.isConnMan = null;
			else
				this.isConnMan = rs.getString("isConnMan").trim();

			if( rs.getString("InitGrade") == null )
				this.InitGrade = null;
			else
				this.InitGrade = rs.getString("InitGrade").trim();

			if( rs.getString("AgentLine") == null )
				this.AgentLine = null;
			else
				this.AgentLine = rs.getString("AgentLine").trim();

			if( rs.getString("InsideFlag") == null )
				this.InsideFlag = null;
			else
				this.InsideFlag = rs.getString("InsideFlag").trim();

			if( rs.getString("SpeciFlag") == null )
				this.SpeciFlag = null;
			else
				this.SpeciFlag = rs.getString("SpeciFlag").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("VIPProperty") == null )
				this.VIPProperty = null;
			else
				this.VIPProperty = rs.getString("VIPProperty").trim();

			if( rs.getString("RoleAgentCode") == null )
				this.RoleAgentCode = null;
			else
				this.RoleAgentCode = rs.getString("RoleAgentCode").trim();

			if( rs.getString("RoleFlag") == null )
				this.RoleFlag = null;
			else
				this.RoleFlag = rs.getString("RoleFlag").trim();

			if( rs.getString("TutorShip") == null )
				this.TutorShip = null;
			else
				this.TutorShip = rs.getString("TutorShip").trim();

			if( rs.getString("InDueFormFlag") == null )
				this.InDueFormFlag = null;
			else
				this.InDueFormFlag = rs.getString("InDueFormFlag").trim();

			this.InDueFormDate = rs.getDate("InDueFormDate");
			if( rs.getString("AgentGrade1") == null )
				this.AgentGrade1 = null;
			else
				this.AgentGrade1 = rs.getString("AgentGrade1").trim();

			if( rs.getString("AgentSeries1") == null )
				this.AgentSeries1 = null;
			else
				this.AgentSeries1 = rs.getString("AgentSeries1").trim();

			this.StartDate1 = rs.getDate("StartDate1");
			if( rs.getString("WageVersion") == null )
				this.WageVersion = null;
			else
				this.WageVersion = rs.getString("WageVersion").trim();

			if( rs.getString("AgentGroup2") == null )
				this.AgentGroup2 = null;
			else
				this.AgentGroup2 = rs.getString("AgentGroup2").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LATree表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATreeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LATreeSchema getSchema()
	{
		LATreeSchema aLATreeSchema = new LATreeSchema();
		aLATreeSchema.setSchema(this);
		return aLATreeSchema;
	}

	public LATreeDB getDB()
	{
		LATreeDB aDBOper = new LATreeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATree描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentSeries)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentLastSeries)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentLastGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IntroAgency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UpAgent)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthUpAgent)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IntroBreakFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IntroCommStart ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IntroCommEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EduManager)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RearBreakFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RearCommStart ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RearCommEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AscriptSeries)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( OldStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( OldEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AstartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AssessType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentKind)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isConnMan)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InitGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentLine)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsideFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SpeciFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VIPProperty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RoleAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RoleFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TutorShip)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InDueFormFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InDueFormDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGrade1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentSeries1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartDate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageVersion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup2));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATree>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AgentSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentLastSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AgentLastGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			IntroAgency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			UpAgent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			OthUpAgent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			IntroBreakFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			IntroCommStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			IntroCommEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			EduManager = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			RearBreakFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			RearCommStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			RearCommEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			AscriptSeries = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			OldStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			OldEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			AstartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			AssessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			BranchCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			AgentKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			isConnMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			InitGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			AgentLine = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			InsideFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			SpeciFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			VIPProperty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			RoleAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			RoleFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			TutorShip = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			InDueFormFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			InDueFormDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,SysConst.PACKAGESPILTER));
			AgentGrade1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			AgentSeries1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			StartDate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47,SysConst.PACKAGESPILTER));
			WageVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			AgentGroup2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATreeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("AgentSeries"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSeries));
		}
		if (FCode.equals("AgentGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
		}
		if (FCode.equals("AgentLastSeries"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentLastSeries));
		}
		if (FCode.equals("AgentLastGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentLastGrade));
		}
		if (FCode.equals("IntroAgency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IntroAgency));
		}
		if (FCode.equals("UpAgent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UpAgent));
		}
		if (FCode.equals("OthUpAgent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthUpAgent));
		}
		if (FCode.equals("IntroBreakFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IntroBreakFlag));
		}
		if (FCode.equals("IntroCommStart"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIntroCommStart()));
		}
		if (FCode.equals("IntroCommEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIntroCommEnd()));
		}
		if (FCode.equals("EduManager"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EduManager));
		}
		if (FCode.equals("RearBreakFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RearBreakFlag));
		}
		if (FCode.equals("RearCommStart"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRearCommStart()));
		}
		if (FCode.equals("RearCommEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRearCommEnd()));
		}
		if (FCode.equals("AscriptSeries"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AscriptSeries));
		}
		if (FCode.equals("OldStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOldStartDate()));
		}
		if (FCode.equals("OldEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOldEndDate()));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("AstartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAstartDate()));
		}
		if (FCode.equals("AssessType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AssessType));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BranchCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchCode));
		}
		if (FCode.equals("AgentKind"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentKind));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("isConnMan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isConnMan));
		}
		if (FCode.equals("InitGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InitGrade));
		}
		if (FCode.equals("AgentLine"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentLine));
		}
		if (FCode.equals("InsideFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsideFlag));
		}
		if (FCode.equals("SpeciFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpeciFlag));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("VIPProperty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VIPProperty));
		}
		if (FCode.equals("RoleAgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RoleAgentCode));
		}
		if (FCode.equals("RoleFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RoleFlag));
		}
		if (FCode.equals("TutorShip"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TutorShip));
		}
		if (FCode.equals("InDueFormFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InDueFormFlag));
		}
		if (FCode.equals("InDueFormDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInDueFormDate()));
		}
		if (FCode.equals("AgentGrade1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade1));
		}
		if (FCode.equals("AgentSeries1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentSeries1));
		}
		if (FCode.equals("StartDate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate1()));
		}
		if (FCode.equals("WageVersion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageVersion));
		}
		if (FCode.equals("AgentGroup2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup2));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AgentSeries);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AgentGrade);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentLastSeries);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AgentLastGrade);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(IntroAgency);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(UpAgent);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(OthUpAgent);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(IntroBreakFlag);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIntroCommStart()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIntroCommEnd()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(EduManager);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(RearBreakFlag);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRearCommStart()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRearCommEnd()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(AscriptSeries);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOldStartDate()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOldEndDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAstartDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(AssessType);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(BranchCode);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(AgentKind);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(isConnMan);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(InitGrade);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(AgentLine);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(InsideFlag);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(SpeciFlag);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(VIPProperty);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(RoleAgentCode);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(RoleFlag);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(TutorShip);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(InDueFormFlag);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInDueFormDate()));
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(AgentGrade1);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(AgentSeries1);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate1()));
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(WageVersion);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup2);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentSeries"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentSeries = FValue.trim();
			}
			else
				AgentSeries = null;
		}
		if (FCode.equalsIgnoreCase("AgentGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGrade = FValue.trim();
			}
			else
				AgentGrade = null;
		}
		if (FCode.equalsIgnoreCase("AgentLastSeries"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentLastSeries = FValue.trim();
			}
			else
				AgentLastSeries = null;
		}
		if (FCode.equalsIgnoreCase("AgentLastGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentLastGrade = FValue.trim();
			}
			else
				AgentLastGrade = null;
		}
		if (FCode.equalsIgnoreCase("IntroAgency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IntroAgency = FValue.trim();
			}
			else
				IntroAgency = null;
		}
		if (FCode.equalsIgnoreCase("UpAgent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UpAgent = FValue.trim();
			}
			else
				UpAgent = null;
		}
		if (FCode.equalsIgnoreCase("OthUpAgent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthUpAgent = FValue.trim();
			}
			else
				OthUpAgent = null;
		}
		if (FCode.equalsIgnoreCase("IntroBreakFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IntroBreakFlag = FValue.trim();
			}
			else
				IntroBreakFlag = null;
		}
		if (FCode.equalsIgnoreCase("IntroCommStart"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IntroCommStart = fDate.getDate( FValue );
			}
			else
				IntroCommStart = null;
		}
		if (FCode.equalsIgnoreCase("IntroCommEnd"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IntroCommEnd = fDate.getDate( FValue );
			}
			else
				IntroCommEnd = null;
		}
		if (FCode.equalsIgnoreCase("EduManager"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EduManager = FValue.trim();
			}
			else
				EduManager = null;
		}
		if (FCode.equalsIgnoreCase("RearBreakFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RearBreakFlag = FValue.trim();
			}
			else
				RearBreakFlag = null;
		}
		if (FCode.equalsIgnoreCase("RearCommStart"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RearCommStart = fDate.getDate( FValue );
			}
			else
				RearCommStart = null;
		}
		if (FCode.equalsIgnoreCase("RearCommEnd"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RearCommEnd = fDate.getDate( FValue );
			}
			else
				RearCommEnd = null;
		}
		if (FCode.equalsIgnoreCase("AscriptSeries"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AscriptSeries = FValue.trim();
			}
			else
				AscriptSeries = null;
		}
		if (FCode.equalsIgnoreCase("OldStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OldStartDate = fDate.getDate( FValue );
			}
			else
				OldStartDate = null;
		}
		if (FCode.equalsIgnoreCase("OldEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OldEndDate = fDate.getDate( FValue );
			}
			else
				OldEndDate = null;
		}
		if (FCode.equalsIgnoreCase("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equalsIgnoreCase("AstartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AstartDate = fDate.getDate( FValue );
			}
			else
				AstartDate = null;
		}
		if (FCode.equalsIgnoreCase("AssessType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AssessType = FValue.trim();
			}
			else
				AssessType = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BranchCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchCode = FValue.trim();
			}
			else
				BranchCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentKind"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentKind = FValue.trim();
			}
			else
				AgentKind = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("isConnMan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isConnMan = FValue.trim();
			}
			else
				isConnMan = null;
		}
		if (FCode.equalsIgnoreCase("InitGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InitGrade = FValue.trim();
			}
			else
				InitGrade = null;
		}
		if (FCode.equalsIgnoreCase("AgentLine"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentLine = FValue.trim();
			}
			else
				AgentLine = null;
		}
		if (FCode.equalsIgnoreCase("InsideFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsideFlag = FValue.trim();
			}
			else
				InsideFlag = null;
		}
		if (FCode.equalsIgnoreCase("SpeciFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SpeciFlag = FValue.trim();
			}
			else
				SpeciFlag = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("VIPProperty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VIPProperty = FValue.trim();
			}
			else
				VIPProperty = null;
		}
		if (FCode.equalsIgnoreCase("RoleAgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RoleAgentCode = FValue.trim();
			}
			else
				RoleAgentCode = null;
		}
		if (FCode.equalsIgnoreCase("RoleFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RoleFlag = FValue.trim();
			}
			else
				RoleFlag = null;
		}
		if (FCode.equalsIgnoreCase("TutorShip"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TutorShip = FValue.trim();
			}
			else
				TutorShip = null;
		}
		if (FCode.equalsIgnoreCase("InDueFormFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InDueFormFlag = FValue.trim();
			}
			else
				InDueFormFlag = null;
		}
		if (FCode.equalsIgnoreCase("InDueFormDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InDueFormDate = fDate.getDate( FValue );
			}
			else
				InDueFormDate = null;
		}
		if (FCode.equalsIgnoreCase("AgentGrade1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGrade1 = FValue.trim();
			}
			else
				AgentGrade1 = null;
		}
		if (FCode.equalsIgnoreCase("AgentSeries1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentSeries1 = FValue.trim();
			}
			else
				AgentSeries1 = null;
		}
		if (FCode.equalsIgnoreCase("StartDate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate1 = fDate.getDate( FValue );
			}
			else
				StartDate1 = null;
		}
		if (FCode.equalsIgnoreCase("WageVersion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageVersion = FValue.trim();
			}
			else
				WageVersion = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup2 = FValue.trim();
			}
			else
				AgentGroup2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LATreeSchema other = (LATreeSchema)otherObject;
		return
			(AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (AgentSeries == null ? other.getAgentSeries() == null : AgentSeries.equals(other.getAgentSeries()))
			&& (AgentGrade == null ? other.getAgentGrade() == null : AgentGrade.equals(other.getAgentGrade()))
			&& (AgentLastSeries == null ? other.getAgentLastSeries() == null : AgentLastSeries.equals(other.getAgentLastSeries()))
			&& (AgentLastGrade == null ? other.getAgentLastGrade() == null : AgentLastGrade.equals(other.getAgentLastGrade()))
			&& (IntroAgency == null ? other.getIntroAgency() == null : IntroAgency.equals(other.getIntroAgency()))
			&& (UpAgent == null ? other.getUpAgent() == null : UpAgent.equals(other.getUpAgent()))
			&& (OthUpAgent == null ? other.getOthUpAgent() == null : OthUpAgent.equals(other.getOthUpAgent()))
			&& (IntroBreakFlag == null ? other.getIntroBreakFlag() == null : IntroBreakFlag.equals(other.getIntroBreakFlag()))
			&& (IntroCommStart == null ? other.getIntroCommStart() == null : fDate.getString(IntroCommStart).equals(other.getIntroCommStart()))
			&& (IntroCommEnd == null ? other.getIntroCommEnd() == null : fDate.getString(IntroCommEnd).equals(other.getIntroCommEnd()))
			&& (EduManager == null ? other.getEduManager() == null : EduManager.equals(other.getEduManager()))
			&& (RearBreakFlag == null ? other.getRearBreakFlag() == null : RearBreakFlag.equals(other.getRearBreakFlag()))
			&& (RearCommStart == null ? other.getRearCommStart() == null : fDate.getString(RearCommStart).equals(other.getRearCommStart()))
			&& (RearCommEnd == null ? other.getRearCommEnd() == null : fDate.getString(RearCommEnd).equals(other.getRearCommEnd()))
			&& (AscriptSeries == null ? other.getAscriptSeries() == null : AscriptSeries.equals(other.getAscriptSeries()))
			&& (OldStartDate == null ? other.getOldStartDate() == null : fDate.getString(OldStartDate).equals(other.getOldStartDate()))
			&& (OldEndDate == null ? other.getOldEndDate() == null : fDate.getString(OldEndDate).equals(other.getOldEndDate()))
			&& (StartDate == null ? other.getStartDate() == null : fDate.getString(StartDate).equals(other.getStartDate()))
			&& (AstartDate == null ? other.getAstartDate() == null : fDate.getString(AstartDate).equals(other.getAstartDate()))
			&& (AssessType == null ? other.getAssessType() == null : AssessType.equals(other.getAssessType()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BranchCode == null ? other.getBranchCode() == null : BranchCode.equals(other.getBranchCode()))
			&& (AgentKind == null ? other.getAgentKind() == null : AgentKind.equals(other.getAgentKind()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (isConnMan == null ? other.getisConnMan() == null : isConnMan.equals(other.getisConnMan()))
			&& (InitGrade == null ? other.getInitGrade() == null : InitGrade.equals(other.getInitGrade()))
			&& (AgentLine == null ? other.getAgentLine() == null : AgentLine.equals(other.getAgentLine()))
			&& (InsideFlag == null ? other.getInsideFlag() == null : InsideFlag.equals(other.getInsideFlag()))
			&& (SpeciFlag == null ? other.getSpeciFlag() == null : SpeciFlag.equals(other.getSpeciFlag()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (VIPProperty == null ? other.getVIPProperty() == null : VIPProperty.equals(other.getVIPProperty()))
			&& (RoleAgentCode == null ? other.getRoleAgentCode() == null : RoleAgentCode.equals(other.getRoleAgentCode()))
			&& (RoleFlag == null ? other.getRoleFlag() == null : RoleFlag.equals(other.getRoleFlag()))
			&& (TutorShip == null ? other.getTutorShip() == null : TutorShip.equals(other.getTutorShip()))
			&& (InDueFormFlag == null ? other.getInDueFormFlag() == null : InDueFormFlag.equals(other.getInDueFormFlag()))
			&& (InDueFormDate == null ? other.getInDueFormDate() == null : fDate.getString(InDueFormDate).equals(other.getInDueFormDate()))
			&& (AgentGrade1 == null ? other.getAgentGrade1() == null : AgentGrade1.equals(other.getAgentGrade1()))
			&& (AgentSeries1 == null ? other.getAgentSeries1() == null : AgentSeries1.equals(other.getAgentSeries1()))
			&& (StartDate1 == null ? other.getStartDate1() == null : fDate.getString(StartDate1).equals(other.getStartDate1()))
			&& (WageVersion == null ? other.getWageVersion() == null : WageVersion.equals(other.getWageVersion()))
			&& (AgentGroup2 == null ? other.getAgentGroup2() == null : AgentGroup2.equals(other.getAgentGroup2()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("AgentCode") ) {
			return 0;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 1;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 2;
		}
		if( strFieldName.equals("AgentSeries") ) {
			return 3;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return 4;
		}
		if( strFieldName.equals("AgentLastSeries") ) {
			return 5;
		}
		if( strFieldName.equals("AgentLastGrade") ) {
			return 6;
		}
		if( strFieldName.equals("IntroAgency") ) {
			return 7;
		}
		if( strFieldName.equals("UpAgent") ) {
			return 8;
		}
		if( strFieldName.equals("OthUpAgent") ) {
			return 9;
		}
		if( strFieldName.equals("IntroBreakFlag") ) {
			return 10;
		}
		if( strFieldName.equals("IntroCommStart") ) {
			return 11;
		}
		if( strFieldName.equals("IntroCommEnd") ) {
			return 12;
		}
		if( strFieldName.equals("EduManager") ) {
			return 13;
		}
		if( strFieldName.equals("RearBreakFlag") ) {
			return 14;
		}
		if( strFieldName.equals("RearCommStart") ) {
			return 15;
		}
		if( strFieldName.equals("RearCommEnd") ) {
			return 16;
		}
		if( strFieldName.equals("AscriptSeries") ) {
			return 17;
		}
		if( strFieldName.equals("OldStartDate") ) {
			return 18;
		}
		if( strFieldName.equals("OldEndDate") ) {
			return 19;
		}
		if( strFieldName.equals("StartDate") ) {
			return 20;
		}
		if( strFieldName.equals("AstartDate") ) {
			return 21;
		}
		if( strFieldName.equals("AssessType") ) {
			return 22;
		}
		if( strFieldName.equals("State") ) {
			return 23;
		}
		if( strFieldName.equals("Operator") ) {
			return 24;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 25;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 26;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 27;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 28;
		}
		if( strFieldName.equals("BranchCode") ) {
			return 29;
		}
		if( strFieldName.equals("AgentKind") ) {
			return 30;
		}
		if( strFieldName.equals("BranchType") ) {
			return 31;
		}
		if( strFieldName.equals("isConnMan") ) {
			return 32;
		}
		if( strFieldName.equals("InitGrade") ) {
			return 33;
		}
		if( strFieldName.equals("AgentLine") ) {
			return 34;
		}
		if( strFieldName.equals("InsideFlag") ) {
			return 35;
		}
		if( strFieldName.equals("SpeciFlag") ) {
			return 36;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 37;
		}
		if( strFieldName.equals("VIPProperty") ) {
			return 38;
		}
		if( strFieldName.equals("RoleAgentCode") ) {
			return 39;
		}
		if( strFieldName.equals("RoleFlag") ) {
			return 40;
		}
		if( strFieldName.equals("TutorShip") ) {
			return 41;
		}
		if( strFieldName.equals("InDueFormFlag") ) {
			return 42;
		}
		if( strFieldName.equals("InDueFormDate") ) {
			return 43;
		}
		if( strFieldName.equals("AgentGrade1") ) {
			return 44;
		}
		if( strFieldName.equals("AgentSeries1") ) {
			return 45;
		}
		if( strFieldName.equals("StartDate1") ) {
			return 46;
		}
		if( strFieldName.equals("WageVersion") ) {
			return 47;
		}
		if( strFieldName.equals("AgentGroup2") ) {
			return 48;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "AgentCode";
				break;
			case 1:
				strFieldName = "AgentGroup";
				break;
			case 2:
				strFieldName = "ManageCom";
				break;
			case 3:
				strFieldName = "AgentSeries";
				break;
			case 4:
				strFieldName = "AgentGrade";
				break;
			case 5:
				strFieldName = "AgentLastSeries";
				break;
			case 6:
				strFieldName = "AgentLastGrade";
				break;
			case 7:
				strFieldName = "IntroAgency";
				break;
			case 8:
				strFieldName = "UpAgent";
				break;
			case 9:
				strFieldName = "OthUpAgent";
				break;
			case 10:
				strFieldName = "IntroBreakFlag";
				break;
			case 11:
				strFieldName = "IntroCommStart";
				break;
			case 12:
				strFieldName = "IntroCommEnd";
				break;
			case 13:
				strFieldName = "EduManager";
				break;
			case 14:
				strFieldName = "RearBreakFlag";
				break;
			case 15:
				strFieldName = "RearCommStart";
				break;
			case 16:
				strFieldName = "RearCommEnd";
				break;
			case 17:
				strFieldName = "AscriptSeries";
				break;
			case 18:
				strFieldName = "OldStartDate";
				break;
			case 19:
				strFieldName = "OldEndDate";
				break;
			case 20:
				strFieldName = "StartDate";
				break;
			case 21:
				strFieldName = "AstartDate";
				break;
			case 22:
				strFieldName = "AssessType";
				break;
			case 23:
				strFieldName = "State";
				break;
			case 24:
				strFieldName = "Operator";
				break;
			case 25:
				strFieldName = "MakeDate";
				break;
			case 26:
				strFieldName = "MakeTime";
				break;
			case 27:
				strFieldName = "ModifyDate";
				break;
			case 28:
				strFieldName = "ModifyTime";
				break;
			case 29:
				strFieldName = "BranchCode";
				break;
			case 30:
				strFieldName = "AgentKind";
				break;
			case 31:
				strFieldName = "BranchType";
				break;
			case 32:
				strFieldName = "isConnMan";
				break;
			case 33:
				strFieldName = "InitGrade";
				break;
			case 34:
				strFieldName = "AgentLine";
				break;
			case 35:
				strFieldName = "InsideFlag";
				break;
			case 36:
				strFieldName = "SpeciFlag";
				break;
			case 37:
				strFieldName = "BranchType2";
				break;
			case 38:
				strFieldName = "VIPProperty";
				break;
			case 39:
				strFieldName = "RoleAgentCode";
				break;
			case 40:
				strFieldName = "RoleFlag";
				break;
			case 41:
				strFieldName = "TutorShip";
				break;
			case 42:
				strFieldName = "InDueFormFlag";
				break;
			case 43:
				strFieldName = "InDueFormDate";
				break;
			case 44:
				strFieldName = "AgentGrade1";
				break;
			case 45:
				strFieldName = "AgentSeries1";
				break;
			case 46:
				strFieldName = "StartDate1";
				break;
			case 47:
				strFieldName = "WageVersion";
				break;
			case 48:
				strFieldName = "AgentGroup2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentSeries") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentLastSeries") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentLastGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IntroAgency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UpAgent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthUpAgent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IntroBreakFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IntroCommStart") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IntroCommEnd") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EduManager") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RearBreakFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RearCommStart") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RearCommEnd") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AscriptSeries") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OldStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OldEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AstartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AssessType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentKind") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("isConnMan") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InitGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentLine") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsideFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SpeciFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VIPProperty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RoleAgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RoleFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TutorShip") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InDueFormFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InDueFormDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AgentGrade1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentSeries1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartDate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("WageVersion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup2") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
