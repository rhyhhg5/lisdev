/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAQualityItemDefDB;

/*
 * <p>ClassName: LAQualityItemDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2005-04-05
 */
public class LAQualityItemDefSchema implements Schema
{
	// @Field
	/** 项目编码 */
	private String ItemCode;
	/** 项目内容 */
	private String ItemContext;
	/** 处理类型 */
	private String ItemType;
	/** 处理下限 */
	private double DownLimit;
	/** 处理上限 */
	private double UpLimit;
	/** 备注 */
	private String Noti;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最近一次修改日期 */
	private Date ModifyDate;
	/** 最近一次修改时间 */
	private String ModifyTime;
	/** 操作员 */
	private String Operator;

	public static final int FIELDNUM = 11;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAQualityItemDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ItemCode";

		PK = pk;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getItemCode()
	{
		if (SysConst.CHANGECHARSET && ItemCode != null && !ItemCode.equals(""))
		{
			ItemCode = StrTool.unicodeToGBK(ItemCode);
		}
		return ItemCode;
	}
	public void setItemCode(String aItemCode)
	{
		ItemCode = aItemCode;
	}
	public String getItemContext()
	{
		if (SysConst.CHANGECHARSET && ItemContext != null && !ItemContext.equals(""))
		{
			ItemContext = StrTool.unicodeToGBK(ItemContext);
		}
		return ItemContext;
	}
	public void setItemContext(String aItemContext)
	{
		ItemContext = aItemContext;
	}
	public String getItemType()
	{
		if (SysConst.CHANGECHARSET && ItemType != null && !ItemType.equals(""))
		{
			ItemType = StrTool.unicodeToGBK(ItemType);
		}
		return ItemType;
	}
	public void setItemType(String aItemType)
	{
		ItemType = aItemType;
	}
	public double getDownLimit()
	{
		return DownLimit;
	}
	public void setDownLimit(double aDownLimit)
	{
		DownLimit = aDownLimit;
	}
	public void setDownLimit(String aDownLimit)
	{
		if (aDownLimit != null && !aDownLimit.equals(""))
		{
			Double tDouble = new Double(aDownLimit);
			double d = tDouble.doubleValue();
			DownLimit = d;
		}
	}

	public double getUpLimit()
	{
		return UpLimit;
	}
	public void setUpLimit(double aUpLimit)
	{
		UpLimit = aUpLimit;
	}
	public void setUpLimit(String aUpLimit)
	{
		if (aUpLimit != null && !aUpLimit.equals(""))
		{
			Double tDouble = new Double(aUpLimit);
			double d = tDouble.doubleValue();
			UpLimit = d;
		}
	}

	public String getNoti()
	{
		if (SysConst.CHANGECHARSET && Noti != null && !Noti.equals(""))
		{
			Noti = StrTool.unicodeToGBK(Noti);
		}
		return Noti;
	}
	public void setNoti(String aNoti)
	{
		Noti = aNoti;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
		{
			MakeTime = StrTool.unicodeToGBK(MakeTime);
		}
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		if (SysConst.CHANGECHARSET && ModifyTime != null && !ModifyTime.equals(""))
		{
			ModifyTime = StrTool.unicodeToGBK(ModifyTime);
		}
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOperator()
	{
		if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
		{
			Operator = StrTool.unicodeToGBK(Operator);
		}
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}

	/**
	* 使用另外一个 LAQualityItemDefSchema 对象给 Schema 赋值
	* @param: aLAQualityItemDefSchema LAQualityItemDefSchema
	**/
	public void setSchema(LAQualityItemDefSchema aLAQualityItemDefSchema)
	{
		this.ItemCode = aLAQualityItemDefSchema.getItemCode();
		this.ItemContext = aLAQualityItemDefSchema.getItemContext();
		this.ItemType = aLAQualityItemDefSchema.getItemType();
		this.DownLimit = aLAQualityItemDefSchema.getDownLimit();
		this.UpLimit = aLAQualityItemDefSchema.getUpLimit();
		this.Noti = aLAQualityItemDefSchema.getNoti();
		this.MakeDate = fDate.getDate( aLAQualityItemDefSchema.getMakeDate());
		this.MakeTime = aLAQualityItemDefSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAQualityItemDefSchema.getModifyDate());
		this.ModifyTime = aLAQualityItemDefSchema.getModifyTime();
		this.Operator = aLAQualityItemDefSchema.getOperator();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ItemCode") == null )
				this.ItemCode = null;
			else
				this.ItemCode = rs.getString("ItemCode").trim();

			if( rs.getString("ItemContext") == null )
				this.ItemContext = null;
			else
				this.ItemContext = rs.getString("ItemContext").trim();

			if( rs.getString("ItemType") == null )
				this.ItemType = null;
			else
				this.ItemType = rs.getString("ItemType").trim();

			this.DownLimit = rs.getDouble("DownLimit");
			this.UpLimit = rs.getDouble("UpLimit");
			if( rs.getString("Noti") == null )
				this.Noti = null;
			else
				this.Noti = rs.getString("Noti").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

		}
		catch(SQLException sqle)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAQualityItemDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	public LAQualityItemDefSchema getSchema()
	{
		LAQualityItemDefSchema aLAQualityItemDefSchema = new LAQualityItemDefSchema();
		aLAQualityItemDefSchema.setSchema(this);
		return aLAQualityItemDefSchema;
	}

	public LAQualityItemDefDB getDB()
	{
		LAQualityItemDefDB aDBOper = new LAQualityItemDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAQualityItemDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(ItemCode))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(ItemContext))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(ItemType))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DownLimit));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(UpLimit));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(Noti))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( MakeDate )))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(MakeTime))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(fDate.getString( ModifyDate )))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(ModifyTime))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(Operator)));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAQualityItemDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ItemContext = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DownLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).doubleValue();
			UpLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAQualityItemDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ItemCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemCode));
		}
		if (FCode.equals("ItemContext"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemContext));
		}
		if (FCode.equals("ItemType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ItemType));
		}
		if (FCode.equals("DownLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DownLimit));
		}
		if (FCode.equals("UpLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UpLimit));
		}
		if (FCode.equals("Noti"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ItemCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ItemContext);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ItemType);
				break;
			case 3:
				strFieldValue = String.valueOf(DownLimit);
				break;
			case 4:
				strFieldValue = String.valueOf(UpLimit);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Noti);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equals("ItemCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemCode = FValue.trim();
			}
			else
				ItemCode = null;
		}
		if (FCode.equals("ItemContext"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemContext = FValue.trim();
			}
			else
				ItemContext = null;
		}
		if (FCode.equals("ItemType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ItemType = FValue.trim();
			}
			else
				ItemType = null;
		}
		if (FCode.equals("DownLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DownLimit = d;
			}
		}
		if (FCode.equals("UpLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				UpLimit = d;
			}
		}
		if (FCode.equals("Noti"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Noti = FValue.trim();
			}
			else
				Noti = null;
		}
		if (FCode.equals("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equals("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equals("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equals("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equals("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAQualityItemDefSchema other = (LAQualityItemDefSchema)otherObject;
		return
			ItemCode.equals(other.getItemCode())
			&& ItemContext.equals(other.getItemContext())
			&& ItemType.equals(other.getItemType())
			&& DownLimit == other.getDownLimit()
			&& UpLimit == other.getUpLimit()
			&& Noti.equals(other.getNoti())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& Operator.equals(other.getOperator());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ItemCode") ) {
			return 0;
		}
		if( strFieldName.equals("ItemContext") ) {
			return 1;
		}
		if( strFieldName.equals("ItemType") ) {
			return 2;
		}
		if( strFieldName.equals("DownLimit") ) {
			return 3;
		}
		if( strFieldName.equals("UpLimit") ) {
			return 4;
		}
		if( strFieldName.equals("Noti") ) {
			return 5;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 6;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 7;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 8;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 9;
		}
		if( strFieldName.equals("Operator") ) {
			return 10;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ItemCode";
				break;
			case 1:
				strFieldName = "ItemContext";
				break;
			case 2:
				strFieldName = "ItemType";
				break;
			case 3:
				strFieldName = "DownLimit";
				break;
			case 4:
				strFieldName = "UpLimit";
				break;
			case 5:
				strFieldName = "Noti";
				break;
			case 6:
				strFieldName = "MakeDate";
				break;
			case 7:
				strFieldName = "MakeTime";
				break;
			case 8:
				strFieldName = "ModifyDate";
				break;
			case 9:
				strFieldName = "ModifyTime";
				break;
			case 10:
				strFieldName = "Operator";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ItemCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemContext") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ItemType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DownLimit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("UpLimit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Noti") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
