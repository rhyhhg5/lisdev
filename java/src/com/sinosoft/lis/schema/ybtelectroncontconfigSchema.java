/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.ybtelectroncontconfigDB;

/*
 * <p>ClassName: ybtelectroncontconfigSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2018-08-13
 */
public class ybtelectroncontconfigSchema implements Schema, Cloneable
{
	// @Field
	/** 银行代码 */
	private String bankcode;
	/** 产品代码 */
	private String productcode;
	/** 产品名称 */
	private String productname;
	/** 销售渠道 */
	private String salechnl;
	/** 备用1 */
	private String bak1;
	/** 备用2 */
	private String bak2;
	/** 备用3 */
	private String bak3;

	public static final int FIELDNUM = 7;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public ybtelectroncontconfigSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "bankcode";
		pk[1] = "productcode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		ybtelectroncontconfigSchema cloned = (ybtelectroncontconfigSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getbankcode()
	{
		return bankcode;
	}
	public void setbankcode(String abankcode)
	{
		bankcode = abankcode;
	}
	public String getproductcode()
	{
		return productcode;
	}
	public void setproductcode(String aproductcode)
	{
		productcode = aproductcode;
	}
	public String getproductname()
	{
		return productname;
	}
	public void setproductname(String aproductname)
	{
		productname = aproductname;
	}
	public String getsalechnl()
	{
		return salechnl;
	}
	public void setsalechnl(String asalechnl)
	{
		salechnl = asalechnl;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}

	/**
	* 使用另外一个 ybtelectroncontconfigSchema 对象给 Schema 赋值
	* @param: aybtelectroncontconfigSchema ybtelectroncontconfigSchema
	**/
	public void setSchema(ybtelectroncontconfigSchema aybtelectroncontconfigSchema)
	{
		this.bankcode = aybtelectroncontconfigSchema.getbankcode();
		this.productcode = aybtelectroncontconfigSchema.getproductcode();
		this.productname = aybtelectroncontconfigSchema.getproductname();
		this.salechnl = aybtelectroncontconfigSchema.getsalechnl();
		this.bak1 = aybtelectroncontconfigSchema.getbak1();
		this.bak2 = aybtelectroncontconfigSchema.getbak2();
		this.bak3 = aybtelectroncontconfigSchema.getbak3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("bankcode") == null )
				this.bankcode = null;
			else
				this.bankcode = rs.getString("bankcode").trim();

			if( rs.getString("productcode") == null )
				this.productcode = null;
			else
				this.productcode = rs.getString("productcode").trim();

			if( rs.getString("productname") == null )
				this.productname = null;
			else
				this.productname = rs.getString("productname").trim();

			if( rs.getString("salechnl") == null )
				this.salechnl = null;
			else
				this.salechnl = rs.getString("salechnl").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的ybtelectroncontconfig表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ybtelectroncontconfigSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public ybtelectroncontconfigSchema getSchema()
	{
		ybtelectroncontconfigSchema aybtelectroncontconfigSchema = new ybtelectroncontconfigSchema();
		aybtelectroncontconfigSchema.setSchema(this);
		return aybtelectroncontconfigSchema;
	}

	public ybtelectroncontconfigDB getDB()
	{
		ybtelectroncontconfigDB aDBOper = new ybtelectroncontconfigDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpybtelectroncontconfig描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(bankcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(productcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(productname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(salechnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpybtelectroncontconfig>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			bankcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			productcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			productname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			salechnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ybtelectroncontconfigSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("bankcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bankcode));
		}
		if (FCode.equals("productcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(productcode));
		}
		if (FCode.equals("productname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(productname));
		}
		if (FCode.equals("salechnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(salechnl));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(bankcode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(productcode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(productname);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(salechnl);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("bankcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bankcode = FValue.trim();
			}
			else
				bankcode = null;
		}
		if (FCode.equalsIgnoreCase("productcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				productcode = FValue.trim();
			}
			else
				productcode = null;
		}
		if (FCode.equalsIgnoreCase("productname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				productname = FValue.trim();
			}
			else
				productname = null;
		}
		if (FCode.equalsIgnoreCase("salechnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				salechnl = FValue.trim();
			}
			else
				salechnl = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		ybtelectroncontconfigSchema other = (ybtelectroncontconfigSchema)otherObject;
		return
			(bankcode == null ? other.getbankcode() == null : bankcode.equals(other.getbankcode()))
			&& (productcode == null ? other.getproductcode() == null : productcode.equals(other.getproductcode()))
			&& (productname == null ? other.getproductname() == null : productname.equals(other.getproductname()))
			&& (salechnl == null ? other.getsalechnl() == null : salechnl.equals(other.getsalechnl()))
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("bankcode") ) {
			return 0;
		}
		if( strFieldName.equals("productcode") ) {
			return 1;
		}
		if( strFieldName.equals("productname") ) {
			return 2;
		}
		if( strFieldName.equals("salechnl") ) {
			return 3;
		}
		if( strFieldName.equals("bak1") ) {
			return 4;
		}
		if( strFieldName.equals("bak2") ) {
			return 5;
		}
		if( strFieldName.equals("bak3") ) {
			return 6;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "bankcode";
				break;
			case 1:
				strFieldName = "productcode";
				break;
			case 2:
				strFieldName = "productname";
				break;
			case 3:
				strFieldName = "salechnl";
				break;
			case 4:
				strFieldName = "bak1";
				break;
			case 5:
				strFieldName = "bak2";
				break;
			case 6:
				strFieldName = "bak3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("bankcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("productcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("productname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("salechnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
