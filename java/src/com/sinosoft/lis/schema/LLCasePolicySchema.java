/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLCasePolicyDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LLCasePolicySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LLCasePolicySchema implements Schema
{
    // @Field
    /** 分案号 */
    private String CaseNo;
    /** 立案号 */
    private String RgtNo;
    /** 分案保单类型 */
    private String CasePolType;
    /** 集体合同号 */
    private String GrpContNo;
    /** 集体保单号 */
    private String GrpPolNo;
    /** 个单合同号 */
    private String ContNo;
    /** 险种保单号 */
    private String PolNo;
    /** 受理事故号 */
    private String CaseRelaNo;
    /** 险类代码 */
    private String KindCode;
    /** 险种版本号 */
    private String RiskVer;
    /** 险种代码 */
    private String RiskCode;
    /** 保单管理机构 */
    private String PolMngCom;
    /** 销售渠道 */
    private String SaleChnl;
    /** 代理人代码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 被保人客户号 */
    private String InsuredNo;
    /** 被保人名称 */
    private String InsuredName;
    /** 被保人性别 */
    private String InsuredSex;
    /** 被保人生日 */
    private Date InsuredBirthday;
    /** 投保人客户号 */
    private String AppntNo;
    /** 投保人名称 */
    private String AppntName;
    /** 保单生效日期 */
    private Date CValiDate;
    /** 保单状态 */
    private String PolState;
    /** 分案保单状态 */
    private String CasePolState;
    /** 赔付标志 */
    private String ClmFlag;
    /** 赔案号 */
    private String ClmNo;
    /** 结案日期 */
    private Date EndCaseDate;
    /** 保单性质标志 */
    private String PolType;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 34; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLCasePolicySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "CaseNo";
        pk[1] = "PolNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCaseNo()
    {
        if (CaseNo != null && !CaseNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            CaseNo = StrTool.unicodeToGBK(CaseNo);
        }
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo)
    {
        CaseNo = aCaseNo;
    }

    public String getRgtNo()
    {
        if (RgtNo != null && !RgtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            RgtNo = StrTool.unicodeToGBK(RgtNo);
        }
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo)
    {
        RgtNo = aRgtNo;
    }

    public String getCasePolType()
    {
        if (CasePolType != null && !CasePolType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CasePolType = StrTool.unicodeToGBK(CasePolType);
        }
        return CasePolType;
    }

    public void setCasePolType(String aCasePolType)
    {
        CasePolType = aCasePolType;
    }

    public String getGrpContNo()
    {
        if (GrpContNo != null && !GrpContNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GrpContNo = StrTool.unicodeToGBK(GrpContNo);
        }
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo)
    {
        GrpContNo = aGrpContNo;
    }

    public String getGrpPolNo()
    {
        if (GrpPolNo != null && !GrpPolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            GrpPolNo = StrTool.unicodeToGBK(GrpPolNo);
        }
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo)
    {
        GrpPolNo = aGrpPolNo;
    }

    public String getContNo()
    {
        if (ContNo != null && !ContNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ContNo = StrTool.unicodeToGBK(ContNo);
        }
        return ContNo;
    }

    public void setContNo(String aContNo)
    {
        ContNo = aContNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getCaseRelaNo()
    {
        if (CaseRelaNo != null && !CaseRelaNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CaseRelaNo = StrTool.unicodeToGBK(CaseRelaNo);
        }
        return CaseRelaNo;
    }

    public void setCaseRelaNo(String aCaseRelaNo)
    {
        CaseRelaNo = aCaseRelaNo;
    }

    public String getKindCode()
    {
        if (KindCode != null && !KindCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            KindCode = StrTool.unicodeToGBK(KindCode);
        }
        return KindCode;
    }

    public void setKindCode(String aKindCode)
    {
        KindCode = aKindCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getPolMngCom()
    {
        if (PolMngCom != null && !PolMngCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PolMngCom = StrTool.unicodeToGBK(PolMngCom);
        }
        return PolMngCom;
    }

    public void setPolMngCom(String aPolMngCom)
    {
        PolMngCom = aPolMngCom;
    }

    public String getSaleChnl()
    {
        if (SaleChnl != null && !SaleChnl.equals("") && SysConst.CHANGECHARSET == true)
        {
            SaleChnl = StrTool.unicodeToGBK(SaleChnl);
        }
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl)
    {
        SaleChnl = aSaleChnl;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup()
    {
        if (AgentGroup != null && !AgentGroup.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentGroup = StrTool.unicodeToGBK(AgentGroup);
        }
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup)
    {
        AgentGroup = aAgentGroup;
    }

    public String getInsuredNo()
    {
        if (InsuredNo != null && !InsuredNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredNo = StrTool.unicodeToGBK(InsuredNo);
        }
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo)
    {
        InsuredNo = aInsuredNo;
    }

    public String getInsuredName()
    {
        if (InsuredName != null && !InsuredName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredName = StrTool.unicodeToGBK(InsuredName);
        }
        return InsuredName;
    }

    public void setInsuredName(String aInsuredName)
    {
        InsuredName = aInsuredName;
    }

    public String getInsuredSex()
    {
        if (InsuredSex != null && !InsuredSex.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuredSex = StrTool.unicodeToGBK(InsuredSex);
        }
        return InsuredSex;
    }

    public void setInsuredSex(String aInsuredSex)
    {
        InsuredSex = aInsuredSex;
    }

    public String getInsuredBirthday()
    {
        if (InsuredBirthday != null)
        {
            return fDate.getString(InsuredBirthday);
        }
        else
        {
            return null;
        }
    }

    public void setInsuredBirthday(Date aInsuredBirthday)
    {
        InsuredBirthday = aInsuredBirthday;
    }

    public void setInsuredBirthday(String aInsuredBirthday)
    {
        if (aInsuredBirthday != null && !aInsuredBirthday.equals(""))
        {
            InsuredBirthday = fDate.getDate(aInsuredBirthday);
        }
        else
        {
            InsuredBirthday = null;
        }
    }

    public String getAppntNo()
    {
        if (AppntNo != null && !AppntNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppntNo = StrTool.unicodeToGBK(AppntNo);
        }
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo)
    {
        AppntNo = aAppntNo;
    }

    public String getAppntName()
    {
        if (AppntName != null && !AppntName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppntName = StrTool.unicodeToGBK(AppntName);
        }
        return AppntName;
    }

    public void setAppntName(String aAppntName)
    {
        AppntName = aAppntName;
    }

    public String getCValiDate()
    {
        if (CValiDate != null)
        {
            return fDate.getString(CValiDate);
        }
        else
        {
            return null;
        }
    }

    public void setCValiDate(Date aCValiDate)
    {
        CValiDate = aCValiDate;
    }

    public void setCValiDate(String aCValiDate)
    {
        if (aCValiDate != null && !aCValiDate.equals(""))
        {
            CValiDate = fDate.getDate(aCValiDate);
        }
        else
        {
            CValiDate = null;
        }
    }

    public String getPolState()
    {
        if (PolState != null && !PolState.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolState = StrTool.unicodeToGBK(PolState);
        }
        return PolState;
    }

    public void setPolState(String aPolState)
    {
        PolState = aPolState;
    }

    public String getCasePolState()
    {
        if (CasePolState != null && !CasePolState.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CasePolState = StrTool.unicodeToGBK(CasePolState);
        }
        return CasePolState;
    }

    public void setCasePolState(String aCasePolState)
    {
        CasePolState = aCasePolState;
    }

    public String getClmFlag()
    {
        if (ClmFlag != null && !ClmFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmFlag = StrTool.unicodeToGBK(ClmFlag);
        }
        return ClmFlag;
    }

    public void setClmFlag(String aClmFlag)
    {
        ClmFlag = aClmFlag;
    }

    public String getClmNo()
    {
        if (ClmNo != null && !ClmNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmNo = StrTool.unicodeToGBK(ClmNo);
        }
        return ClmNo;
    }

    public void setClmNo(String aClmNo)
    {
        ClmNo = aClmNo;
    }

    public String getEndCaseDate()
    {
        if (EndCaseDate != null)
        {
            return fDate.getString(EndCaseDate);
        }
        else
        {
            return null;
        }
    }

    public void setEndCaseDate(Date aEndCaseDate)
    {
        EndCaseDate = aEndCaseDate;
    }

    public void setEndCaseDate(String aEndCaseDate)
    {
        if (aEndCaseDate != null && !aEndCaseDate.equals(""))
        {
            EndCaseDate = fDate.getDate(aEndCaseDate);
        }
        else
        {
            EndCaseDate = null;
        }
    }

    public String getPolType()
    {
        if (PolType != null && !PolType.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolType = StrTool.unicodeToGBK(PolType);
        }
        return PolType;
    }

    public void setPolType(String aPolType)
    {
        PolType = aPolType;
    }

    public String getMngCom()
    {
        if (MngCom != null && !MngCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLCasePolicySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLCasePolicySchema aLLCasePolicySchema)
    {
        this.CaseNo = aLLCasePolicySchema.getCaseNo();
        this.RgtNo = aLLCasePolicySchema.getRgtNo();
        this.CasePolType = aLLCasePolicySchema.getCasePolType();
        this.GrpContNo = aLLCasePolicySchema.getGrpContNo();
        this.GrpPolNo = aLLCasePolicySchema.getGrpPolNo();
        this.ContNo = aLLCasePolicySchema.getContNo();
        this.PolNo = aLLCasePolicySchema.getPolNo();
        this.CaseRelaNo = aLLCasePolicySchema.getCaseRelaNo();
        this.KindCode = aLLCasePolicySchema.getKindCode();
        this.RiskVer = aLLCasePolicySchema.getRiskVer();
        this.RiskCode = aLLCasePolicySchema.getRiskCode();
        this.PolMngCom = aLLCasePolicySchema.getPolMngCom();
        this.SaleChnl = aLLCasePolicySchema.getSaleChnl();
        this.AgentCode = aLLCasePolicySchema.getAgentCode();
        this.AgentGroup = aLLCasePolicySchema.getAgentGroup();
        this.InsuredNo = aLLCasePolicySchema.getInsuredNo();
        this.InsuredName = aLLCasePolicySchema.getInsuredName();
        this.InsuredSex = aLLCasePolicySchema.getInsuredSex();
        this.InsuredBirthday = fDate.getDate(aLLCasePolicySchema.
                                             getInsuredBirthday());
        this.AppntNo = aLLCasePolicySchema.getAppntNo();
        this.AppntName = aLLCasePolicySchema.getAppntName();
        this.CValiDate = fDate.getDate(aLLCasePolicySchema.getCValiDate());
        this.PolState = aLLCasePolicySchema.getPolState();
        this.CasePolState = aLLCasePolicySchema.getCasePolState();
        this.ClmFlag = aLLCasePolicySchema.getClmFlag();
        this.ClmNo = aLLCasePolicySchema.getClmNo();
        this.EndCaseDate = fDate.getDate(aLLCasePolicySchema.getEndCaseDate());
        this.PolType = aLLCasePolicySchema.getPolType();
        this.MngCom = aLLCasePolicySchema.getMngCom();
        this.Operator = aLLCasePolicySchema.getOperator();
        this.MakeDate = fDate.getDate(aLLCasePolicySchema.getMakeDate());
        this.MakeTime = aLLCasePolicySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLCasePolicySchema.getModifyDate());
        this.ModifyTime = aLLCasePolicySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CaseNo") == null)
            {
                this.CaseNo = null;
            }
            else
            {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("RgtNo") == null)
            {
                this.RgtNo = null;
            }
            else
            {
                this.RgtNo = rs.getString("RgtNo").trim();
            }

            if (rs.getString("CasePolType") == null)
            {
                this.CasePolType = null;
            }
            else
            {
                this.CasePolType = rs.getString("CasePolType").trim();
            }

            if (rs.getString("GrpContNo") == null)
            {
                this.GrpContNo = null;
            }
            else
            {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null)
            {
                this.GrpPolNo = null;
            }
            else
            {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("ContNo") == null)
            {
                this.ContNo = null;
            }
            else
            {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("CaseRelaNo") == null)
            {
                this.CaseRelaNo = null;
            }
            else
            {
                this.CaseRelaNo = rs.getString("CaseRelaNo").trim();
            }

            if (rs.getString("KindCode") == null)
            {
                this.KindCode = null;
            }
            else
            {
                this.KindCode = rs.getString("KindCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("PolMngCom") == null)
            {
                this.PolMngCom = null;
            }
            else
            {
                this.PolMngCom = rs.getString("PolMngCom").trim();
            }

            if (rs.getString("SaleChnl") == null)
            {
                this.SaleChnl = null;
            }
            else
            {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null)
            {
                this.AgentGroup = null;
            }
            else
            {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("InsuredNo") == null)
            {
                this.InsuredNo = null;
            }
            else
            {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("InsuredName") == null)
            {
                this.InsuredName = null;
            }
            else
            {
                this.InsuredName = rs.getString("InsuredName").trim();
            }

            if (rs.getString("InsuredSex") == null)
            {
                this.InsuredSex = null;
            }
            else
            {
                this.InsuredSex = rs.getString("InsuredSex").trim();
            }

            this.InsuredBirthday = rs.getDate("InsuredBirthday");
            if (rs.getString("AppntNo") == null)
            {
                this.AppntNo = null;
            }
            else
            {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("AppntName") == null)
            {
                this.AppntName = null;
            }
            else
            {
                this.AppntName = rs.getString("AppntName").trim();
            }

            this.CValiDate = rs.getDate("CValiDate");
            if (rs.getString("PolState") == null)
            {
                this.PolState = null;
            }
            else
            {
                this.PolState = rs.getString("PolState").trim();
            }

            if (rs.getString("CasePolState") == null)
            {
                this.CasePolState = null;
            }
            else
            {
                this.CasePolState = rs.getString("CasePolState").trim();
            }

            if (rs.getString("ClmFlag") == null)
            {
                this.ClmFlag = null;
            }
            else
            {
                this.ClmFlag = rs.getString("ClmFlag").trim();
            }

            if (rs.getString("ClmNo") == null)
            {
                this.ClmNo = null;
            }
            else
            {
                this.ClmNo = rs.getString("ClmNo").trim();
            }

            this.EndCaseDate = rs.getDate("EndCaseDate");
            if (rs.getString("PolType") == null)
            {
                this.PolType = null;
            }
            else
            {
                this.PolType = rs.getString("PolType").trim();
            }

            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCasePolicySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLCasePolicySchema getSchema()
    {
        LLCasePolicySchema aLLCasePolicySchema = new LLCasePolicySchema();
        aLLCasePolicySchema.setSchema(this);
        return aLLCasePolicySchema;
    }

    public LLCasePolicyDB getDB()
    {
        LLCasePolicyDB aDBOper = new LLCasePolicyDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCasePolicy描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(CaseNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RgtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CasePolType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GrpPolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ContNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CaseRelaNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(KindCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolMngCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SaleChnl)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGroup)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuredSex)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            InsuredBirthday))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntName)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            CValiDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CasePolState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmNo)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EndCaseDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MngCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCasePolicy>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            CasePolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
            CaseRelaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            PolMngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                       SysConst.PACKAGESPILTER);
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                         SysConst.PACKAGESPILTER);
            InsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
            InsuredBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                     SysConst.PACKAGESPILTER);
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                       SysConst.PACKAGESPILTER);
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            PolState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                      SysConst.PACKAGESPILTER);
            CasePolState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                          SysConst.PACKAGESPILTER);
            ClmFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                     SysConst.PACKAGESPILTER);
            ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                   SysConst.PACKAGESPILTER);
            EndCaseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 27, SysConst.PACKAGESPILTER));
            PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                     SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 31, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 33, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCasePolicySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CaseNo));
        }
        if (FCode.equals("RgtNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RgtNo));
        }
        if (FCode.equals("CasePolType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CasePolType));
        }
        if (FCode.equals("GrpContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpPolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPolNo));
        }
        if (FCode.equals("ContNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ContNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("CaseRelaNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CaseRelaNo));
        }
        if (FCode.equals("KindCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(KindCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("PolMngCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolMngCom));
        }
        if (FCode.equals("SaleChnl"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SaleChnl));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentGroup));
        }
        if (FCode.equals("InsuredNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredNo));
        }
        if (FCode.equals("InsuredName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredName));
        }
        if (FCode.equals("InsuredSex"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuredSex));
        }
        if (FCode.equals("InsuredBirthday"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getInsuredBirthday()));
        }
        if (FCode.equals("AppntNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntNo));
        }
        if (FCode.equals("AppntName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntName));
        }
        if (FCode.equals("CValiDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getCValiDate()));
        }
        if (FCode.equals("PolState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolState));
        }
        if (FCode.equals("CasePolState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CasePolState));
        }
        if (FCode.equals("ClmFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmFlag));
        }
        if (FCode.equals("ClmNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmNo));
        }
        if (FCode.equals("EndCaseDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getEndCaseDate()));
        }
        if (FCode.equals("PolType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolType));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MngCom));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RgtNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CasePolType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(GrpContNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ContNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(CaseRelaNo);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(KindCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(PolMngCom);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(SaleChnl);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(AgentGroup);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(InsuredNo);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(InsuredName);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(InsuredSex);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInsuredBirthday()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(AppntNo);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(AppntName);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getCValiDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(PolState);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(CasePolState);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(ClmFlag);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(ClmNo);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEndCaseDate()));
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(PolType);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
            {
                CaseNo = null;
            }
        }
        if (FCode.equals("RgtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtNo = FValue.trim();
            }
            else
            {
                RgtNo = null;
            }
        }
        if (FCode.equals("CasePolType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CasePolType = FValue.trim();
            }
            else
            {
                CasePolType = null;
            }
        }
        if (FCode.equals("GrpContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpContNo = FValue.trim();
            }
            else
            {
                GrpContNo = null;
            }
        }
        if (FCode.equals("GrpPolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GrpPolNo = FValue.trim();
            }
            else
            {
                GrpPolNo = null;
            }
        }
        if (FCode.equals("ContNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ContNo = FValue.trim();
            }
            else
            {
                ContNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("CaseRelaNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseRelaNo = FValue.trim();
            }
            else
            {
                CaseRelaNo = null;
            }
        }
        if (FCode.equals("KindCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                KindCode = FValue.trim();
            }
            else
            {
                KindCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("PolMngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolMngCom = FValue.trim();
            }
            else
            {
                PolMngCom = null;
            }
        }
        if (FCode.equals("SaleChnl"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SaleChnl = FValue.trim();
            }
            else
            {
                SaleChnl = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("AgentGroup"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGroup = FValue.trim();
            }
            else
            {
                AgentGroup = null;
            }
        }
        if (FCode.equals("InsuredNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredNo = FValue.trim();
            }
            else
            {
                InsuredNo = null;
            }
        }
        if (FCode.equals("InsuredName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredName = FValue.trim();
            }
            else
            {
                InsuredName = null;
            }
        }
        if (FCode.equals("InsuredSex"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredSex = FValue.trim();
            }
            else
            {
                InsuredSex = null;
            }
        }
        if (FCode.equals("InsuredBirthday"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuredBirthday = fDate.getDate(FValue);
            }
            else
            {
                InsuredBirthday = null;
            }
        }
        if (FCode.equals("AppntNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntNo = FValue.trim();
            }
            else
            {
                AppntNo = null;
            }
        }
        if (FCode.equals("AppntName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntName = FValue.trim();
            }
            else
            {
                AppntName = null;
            }
        }
        if (FCode.equals("CValiDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CValiDate = fDate.getDate(FValue);
            }
            else
            {
                CValiDate = null;
            }
        }
        if (FCode.equals("PolState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolState = FValue.trim();
            }
            else
            {
                PolState = null;
            }
        }
        if (FCode.equals("CasePolState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CasePolState = FValue.trim();
            }
            else
            {
                CasePolState = null;
            }
        }
        if (FCode.equals("ClmFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmFlag = FValue.trim();
            }
            else
            {
                ClmFlag = null;
            }
        }
        if (FCode.equals("ClmNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmNo = FValue.trim();
            }
            else
            {
                ClmNo = null;
            }
        }
        if (FCode.equals("EndCaseDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndCaseDate = fDate.getDate(FValue);
            }
            else
            {
                EndCaseDate = null;
            }
        }
        if (FCode.equals("PolType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolType = FValue.trim();
            }
            else
            {
                PolType = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLCasePolicySchema other = (LLCasePolicySchema) otherObject;
        return
                CaseNo.equals(other.getCaseNo())
                && RgtNo.equals(other.getRgtNo())
                && CasePolType.equals(other.getCasePolType())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && ContNo.equals(other.getContNo())
                && PolNo.equals(other.getPolNo())
                && CaseRelaNo.equals(other.getCaseRelaNo())
                && KindCode.equals(other.getKindCode())
                && RiskVer.equals(other.getRiskVer())
                && RiskCode.equals(other.getRiskCode())
                && PolMngCom.equals(other.getPolMngCom())
                && SaleChnl.equals(other.getSaleChnl())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && InsuredNo.equals(other.getInsuredNo())
                && InsuredName.equals(other.getInsuredName())
                && InsuredSex.equals(other.getInsuredSex())
                &&
                fDate.getString(InsuredBirthday).equals(other.
                getInsuredBirthday())
                && AppntNo.equals(other.getAppntNo())
                && AppntName.equals(other.getAppntName())
                && fDate.getString(CValiDate).equals(other.getCValiDate())
                && PolState.equals(other.getPolState())
                && CasePolState.equals(other.getCasePolState())
                && ClmFlag.equals(other.getClmFlag())
                && ClmNo.equals(other.getClmNo())
                && fDate.getString(EndCaseDate).equals(other.getEndCaseDate())
                && PolType.equals(other.getPolType())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CaseNo"))
        {
            return 0;
        }
        if (strFieldName.equals("RgtNo"))
        {
            return 1;
        }
        if (strFieldName.equals("CasePolType"))
        {
            return 2;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return 3;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return 4;
        }
        if (strFieldName.equals("ContNo"))
        {
            return 5;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 6;
        }
        if (strFieldName.equals("CaseRelaNo"))
        {
            return 7;
        }
        if (strFieldName.equals("KindCode"))
        {
            return 8;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 9;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 10;
        }
        if (strFieldName.equals("PolMngCom"))
        {
            return 11;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return 12;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 13;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return 14;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return 15;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return 16;
        }
        if (strFieldName.equals("InsuredSex"))
        {
            return 17;
        }
        if (strFieldName.equals("InsuredBirthday"))
        {
            return 18;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return 19;
        }
        if (strFieldName.equals("AppntName"))
        {
            return 20;
        }
        if (strFieldName.equals("CValiDate"))
        {
            return 21;
        }
        if (strFieldName.equals("PolState"))
        {
            return 22;
        }
        if (strFieldName.equals("CasePolState"))
        {
            return 23;
        }
        if (strFieldName.equals("ClmFlag"))
        {
            return 24;
        }
        if (strFieldName.equals("ClmNo"))
        {
            return 25;
        }
        if (strFieldName.equals("EndCaseDate"))
        {
            return 26;
        }
        if (strFieldName.equals("PolType"))
        {
            return 27;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 28;
        }
        if (strFieldName.equals("Operator"))
        {
            return 29;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 30;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 31;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 32;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 33;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CaseNo";
                break;
            case 1:
                strFieldName = "RgtNo";
                break;
            case 2:
                strFieldName = "CasePolType";
                break;
            case 3:
                strFieldName = "GrpContNo";
                break;
            case 4:
                strFieldName = "GrpPolNo";
                break;
            case 5:
                strFieldName = "ContNo";
                break;
            case 6:
                strFieldName = "PolNo";
                break;
            case 7:
                strFieldName = "CaseRelaNo";
                break;
            case 8:
                strFieldName = "KindCode";
                break;
            case 9:
                strFieldName = "RiskVer";
                break;
            case 10:
                strFieldName = "RiskCode";
                break;
            case 11:
                strFieldName = "PolMngCom";
                break;
            case 12:
                strFieldName = "SaleChnl";
                break;
            case 13:
                strFieldName = "AgentCode";
                break;
            case 14:
                strFieldName = "AgentGroup";
                break;
            case 15:
                strFieldName = "InsuredNo";
                break;
            case 16:
                strFieldName = "InsuredName";
                break;
            case 17:
                strFieldName = "InsuredSex";
                break;
            case 18:
                strFieldName = "InsuredBirthday";
                break;
            case 19:
                strFieldName = "AppntNo";
                break;
            case 20:
                strFieldName = "AppntName";
                break;
            case 21:
                strFieldName = "CValiDate";
                break;
            case 22:
                strFieldName = "PolState";
                break;
            case 23:
                strFieldName = "CasePolState";
                break;
            case 24:
                strFieldName = "ClmFlag";
                break;
            case 25:
                strFieldName = "ClmNo";
                break;
            case 26:
                strFieldName = "EndCaseDate";
                break;
            case 27:
                strFieldName = "PolType";
                break;
            case 28:
                strFieldName = "MngCom";
                break;
            case 29:
                strFieldName = "Operator";
                break;
            case 30:
                strFieldName = "MakeDate";
                break;
            case 31:
                strFieldName = "MakeTime";
                break;
            case 32:
                strFieldName = "ModifyDate";
                break;
            case 33:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CasePolType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseRelaNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("KindCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolMngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredSex"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredBirthday"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AppntNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CValiDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PolState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CasePolState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndCaseDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PolType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 33:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
