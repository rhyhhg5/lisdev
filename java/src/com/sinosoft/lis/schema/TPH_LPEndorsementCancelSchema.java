/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.TPH_LPEndorsementCancelDB;

/*
 * <p>ClassName: TPH_LPEndorsementCancelSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 税优平台表机构变更
 * @CreateDate：2015-10-14
 */
public class TPH_LPEndorsementCancelSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String policyNo;
	/** 保全批单号 */
	private String endorsementNo;
	/** 处理日期 */
	private Date makeDate;
	/** 处理时间 */
	private String makeTime;

	public static final int FIELDNUM = 4;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public TPH_LPEndorsementCancelSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "policyNo";
		pk[1] = "endorsementNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		TPH_LPEndorsementCancelSchema cloned = (TPH_LPEndorsementCancelSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getpolicyNo()
	{
		return policyNo;
	}
	public void setpolicyNo(String apolicyNo)
	{
		policyNo = apolicyNo;
	}
	public String getendorsementNo()
	{
		return endorsementNo;
	}
	public void setendorsementNo(String aendorsementNo)
	{
		endorsementNo = aendorsementNo;
	}
	public String getmakeDate()
	{
		if( makeDate != null )
			return fDate.getString(makeDate);
		else
			return null;
	}
	public void setmakeDate(Date amakeDate)
	{
		makeDate = amakeDate;
	}
	public void setmakeDate(String amakeDate)
	{
		if (amakeDate != null && !amakeDate.equals("") )
		{
			makeDate = fDate.getDate( amakeDate );
		}
		else
			makeDate = null;
	}

	public String getmakeTime()
	{
		return makeTime;
	}
	public void setmakeTime(String amakeTime)
	{
		makeTime = amakeTime;
	}

	/**
	* 使用另外一个 TPH_LPEndorsementCancelSchema 对象给 Schema 赋值
	* @param: aTPH_LPEndorsementCancelSchema TPH_LPEndorsementCancelSchema
	**/
	public void setSchema(TPH_LPEndorsementCancelSchema aTPH_LPEndorsementCancelSchema)
	{
		this.policyNo = aTPH_LPEndorsementCancelSchema.getpolicyNo();
		this.endorsementNo = aTPH_LPEndorsementCancelSchema.getendorsementNo();
		this.makeDate = fDate.getDate( aTPH_LPEndorsementCancelSchema.getmakeDate());
		this.makeTime = aTPH_LPEndorsementCancelSchema.getmakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("policyNo") == null )
				this.policyNo = null;
			else
				this.policyNo = rs.getString("policyNo").trim();

			if( rs.getString("endorsementNo") == null )
				this.endorsementNo = null;
			else
				this.endorsementNo = rs.getString("endorsementNo").trim();

			this.makeDate = rs.getDate("makeDate");
			if( rs.getString("makeTime") == null )
				this.makeTime = null;
			else
				this.makeTime = rs.getString("makeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的TPH_LPEndorsementCancel表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LPEndorsementCancelSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public TPH_LPEndorsementCancelSchema getSchema()
	{
		TPH_LPEndorsementCancelSchema aTPH_LPEndorsementCancelSchema = new TPH_LPEndorsementCancelSchema();
		aTPH_LPEndorsementCancelSchema.setSchema(this);
		return aTPH_LPEndorsementCancelSchema;
	}

	public TPH_LPEndorsementCancelDB getDB()
	{
		TPH_LPEndorsementCancelDB aDBOper = new TPH_LPEndorsementCancelDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LPEndorsementCancel描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(policyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(endorsementNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( makeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(makeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LPEndorsementCancel>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			policyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			endorsementNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			makeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			makeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LPEndorsementCancelSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("policyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
		}
		if (FCode.equals("endorsementNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(endorsementNo));
		}
		if (FCode.equals("makeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
		}
		if (FCode.equals("makeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(makeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(policyNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(endorsementNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getmakeDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(makeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("policyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyNo = FValue.trim();
			}
			else
				policyNo = null;
		}
		if (FCode.equalsIgnoreCase("endorsementNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				endorsementNo = FValue.trim();
			}
			else
				endorsementNo = null;
		}
		if (FCode.equalsIgnoreCase("makeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				makeDate = fDate.getDate( FValue );
			}
			else
				makeDate = null;
		}
		if (FCode.equalsIgnoreCase("makeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				makeTime = FValue.trim();
			}
			else
				makeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		TPH_LPEndorsementCancelSchema other = (TPH_LPEndorsementCancelSchema)otherObject;
		return
			(policyNo == null ? other.getpolicyNo() == null : policyNo.equals(other.getpolicyNo()))
			&& (endorsementNo == null ? other.getendorsementNo() == null : endorsementNo.equals(other.getendorsementNo()))
			&& (makeDate == null ? other.getmakeDate() == null : fDate.getString(makeDate).equals(other.getmakeDate()))
			&& (makeTime == null ? other.getmakeTime() == null : makeTime.equals(other.getmakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return 0;
		}
		if( strFieldName.equals("endorsementNo") ) {
			return 1;
		}
		if( strFieldName.equals("makeDate") ) {
			return 2;
		}
		if( strFieldName.equals("makeTime") ) {
			return 3;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "policyNo";
				break;
			case 1:
				strFieldName = "endorsementNo";
				break;
			case 2:
				strFieldName = "makeDate";
				break;
			case 3:
				strFieldName = "makeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("endorsementNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("makeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("makeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
