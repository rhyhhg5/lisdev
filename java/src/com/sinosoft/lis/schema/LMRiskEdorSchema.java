/*
 * <p>ClassName: LMRiskEdorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMRiskEdorDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMRiskEdorSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVer;
    /** 险种名称 */
    private String RiskName;
    /** 批改重新计算标记 */
    private String EdorReCalFlag;
    /** 生存退保涉及净保费 */
    private String AliveSurrToPrem;
    /** 死亡退保涉及净保费 */
    private String DeadSurrToPremFlag;
    /** 转出涉及保费 */
    private String OutToPremFlag;
    /** 生存退保金计算单位 */
    private String AliveSurrCalUnit;
    /** 死亡退保金计算单位 */
    private String DeadSurrCalUnit;
    /** 转移计算单位 */
    private String MoveCalUnit;
    /** 生存退保金计算点 */
    private String AliveSurrCalPoint;
    /** 死亡退保金计算点 */
    private String DeadSurrCalPoint;
    /** 转移计算点 */
    private String MoveCalPoint;
    /** 利息计算方式 */
    private String CalInterestMode;
    /** 客户批改重算标记 */
    private String CustEdorReCalFlag;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMRiskEdorSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "RiskVer";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVer()
    {
        if (RiskVer != null && !RiskVer.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskVer = StrTool.unicodeToGBK(RiskVer);
        }
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer)
    {
        RiskVer = aRiskVer;
    }

    public String getRiskName()
    {
        if (RiskName != null && !RiskName.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskName = StrTool.unicodeToGBK(RiskName);
        }
        return RiskName;
    }

    public void setRiskName(String aRiskName)
    {
        RiskName = aRiskName;
    }

    public String getEdorReCalFlag()
    {
        if (EdorReCalFlag != null && !EdorReCalFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EdorReCalFlag = StrTool.unicodeToGBK(EdorReCalFlag);
        }
        return EdorReCalFlag;
    }

    public void setEdorReCalFlag(String aEdorReCalFlag)
    {
        EdorReCalFlag = aEdorReCalFlag;
    }

    public String getAliveSurrToPrem()
    {
        if (AliveSurrToPrem != null && !AliveSurrToPrem.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AliveSurrToPrem = StrTool.unicodeToGBK(AliveSurrToPrem);
        }
        return AliveSurrToPrem;
    }

    public void setAliveSurrToPrem(String aAliveSurrToPrem)
    {
        AliveSurrToPrem = aAliveSurrToPrem;
    }

    public String getDeadSurrToPremFlag()
    {
        if (DeadSurrToPremFlag != null && !DeadSurrToPremFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DeadSurrToPremFlag = StrTool.unicodeToGBK(DeadSurrToPremFlag);
        }
        return DeadSurrToPremFlag;
    }

    public void setDeadSurrToPremFlag(String aDeadSurrToPremFlag)
    {
        DeadSurrToPremFlag = aDeadSurrToPremFlag;
    }

    public String getOutToPremFlag()
    {
        if (OutToPremFlag != null && !OutToPremFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OutToPremFlag = StrTool.unicodeToGBK(OutToPremFlag);
        }
        return OutToPremFlag;
    }

    public void setOutToPremFlag(String aOutToPremFlag)
    {
        OutToPremFlag = aOutToPremFlag;
    }

    public String getAliveSurrCalUnit()
    {
        if (AliveSurrCalUnit != null && !AliveSurrCalUnit.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AliveSurrCalUnit = StrTool.unicodeToGBK(AliveSurrCalUnit);
        }
        return AliveSurrCalUnit;
    }

    public void setAliveSurrCalUnit(String aAliveSurrCalUnit)
    {
        AliveSurrCalUnit = aAliveSurrCalUnit;
    }

    public String getDeadSurrCalUnit()
    {
        if (DeadSurrCalUnit != null && !DeadSurrCalUnit.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DeadSurrCalUnit = StrTool.unicodeToGBK(DeadSurrCalUnit);
        }
        return DeadSurrCalUnit;
    }

    public void setDeadSurrCalUnit(String aDeadSurrCalUnit)
    {
        DeadSurrCalUnit = aDeadSurrCalUnit;
    }

    public String getMoveCalUnit()
    {
        if (MoveCalUnit != null && !MoveCalUnit.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            MoveCalUnit = StrTool.unicodeToGBK(MoveCalUnit);
        }
        return MoveCalUnit;
    }

    public void setMoveCalUnit(String aMoveCalUnit)
    {
        MoveCalUnit = aMoveCalUnit;
    }

    public String getAliveSurrCalPoint()
    {
        if (AliveSurrCalPoint != null && !AliveSurrCalPoint.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AliveSurrCalPoint = StrTool.unicodeToGBK(AliveSurrCalPoint);
        }
        return AliveSurrCalPoint;
    }

    public void setAliveSurrCalPoint(String aAliveSurrCalPoint)
    {
        AliveSurrCalPoint = aAliveSurrCalPoint;
    }

    public String getDeadSurrCalPoint()
    {
        if (DeadSurrCalPoint != null && !DeadSurrCalPoint.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DeadSurrCalPoint = StrTool.unicodeToGBK(DeadSurrCalPoint);
        }
        return DeadSurrCalPoint;
    }

    public void setDeadSurrCalPoint(String aDeadSurrCalPoint)
    {
        DeadSurrCalPoint = aDeadSurrCalPoint;
    }

    public String getMoveCalPoint()
    {
        if (MoveCalPoint != null && !MoveCalPoint.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            MoveCalPoint = StrTool.unicodeToGBK(MoveCalPoint);
        }
        return MoveCalPoint;
    }

    public void setMoveCalPoint(String aMoveCalPoint)
    {
        MoveCalPoint = aMoveCalPoint;
    }

    public String getCalInterestMode()
    {
        if (CalInterestMode != null && !CalInterestMode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalInterestMode = StrTool.unicodeToGBK(CalInterestMode);
        }
        return CalInterestMode;
    }

    public void setCalInterestMode(String aCalInterestMode)
    {
        CalInterestMode = aCalInterestMode;
    }

    public String getCustEdorReCalFlag()
    {
        if (CustEdorReCalFlag != null && !CustEdorReCalFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustEdorReCalFlag = StrTool.unicodeToGBK(CustEdorReCalFlag);
        }
        return CustEdorReCalFlag;
    }

    public void setCustEdorReCalFlag(String aCustEdorReCalFlag)
    {
        CustEdorReCalFlag = aCustEdorReCalFlag;
    }

    /**
     * 使用另外一个 LMRiskEdorSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMRiskEdorSchema aLMRiskEdorSchema)
    {
        this.RiskCode = aLMRiskEdorSchema.getRiskCode();
        this.RiskVer = aLMRiskEdorSchema.getRiskVer();
        this.RiskName = aLMRiskEdorSchema.getRiskName();
        this.EdorReCalFlag = aLMRiskEdorSchema.getEdorReCalFlag();
        this.AliveSurrToPrem = aLMRiskEdorSchema.getAliveSurrToPrem();
        this.DeadSurrToPremFlag = aLMRiskEdorSchema.getDeadSurrToPremFlag();
        this.OutToPremFlag = aLMRiskEdorSchema.getOutToPremFlag();
        this.AliveSurrCalUnit = aLMRiskEdorSchema.getAliveSurrCalUnit();
        this.DeadSurrCalUnit = aLMRiskEdorSchema.getDeadSurrCalUnit();
        this.MoveCalUnit = aLMRiskEdorSchema.getMoveCalUnit();
        this.AliveSurrCalPoint = aLMRiskEdorSchema.getAliveSurrCalPoint();
        this.DeadSurrCalPoint = aLMRiskEdorSchema.getDeadSurrCalPoint();
        this.MoveCalPoint = aLMRiskEdorSchema.getMoveCalPoint();
        this.CalInterestMode = aLMRiskEdorSchema.getCalInterestMode();
        this.CustEdorReCalFlag = aLMRiskEdorSchema.getCustEdorReCalFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null)
            {
                this.RiskVer = null;
            }
            else
            {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("RiskName") == null)
            {
                this.RiskName = null;
            }
            else
            {
                this.RiskName = rs.getString("RiskName").trim();
            }

            if (rs.getString("EdorReCalFlag") == null)
            {
                this.EdorReCalFlag = null;
            }
            else
            {
                this.EdorReCalFlag = rs.getString("EdorReCalFlag").trim();
            }

            if (rs.getString("AliveSurrToPrem") == null)
            {
                this.AliveSurrToPrem = null;
            }
            else
            {
                this.AliveSurrToPrem = rs.getString("AliveSurrToPrem").trim();
            }

            if (rs.getString("DeadSurrToPremFlag") == null)
            {
                this.DeadSurrToPremFlag = null;
            }
            else
            {
                this.DeadSurrToPremFlag = rs.getString("DeadSurrToPremFlag").
                                          trim();
            }

            if (rs.getString("OutToPremFlag") == null)
            {
                this.OutToPremFlag = null;
            }
            else
            {
                this.OutToPremFlag = rs.getString("OutToPremFlag").trim();
            }

            if (rs.getString("AliveSurrCalUnit") == null)
            {
                this.AliveSurrCalUnit = null;
            }
            else
            {
                this.AliveSurrCalUnit = rs.getString("AliveSurrCalUnit").trim();
            }

            if (rs.getString("DeadSurrCalUnit") == null)
            {
                this.DeadSurrCalUnit = null;
            }
            else
            {
                this.DeadSurrCalUnit = rs.getString("DeadSurrCalUnit").trim();
            }

            if (rs.getString("MoveCalUnit") == null)
            {
                this.MoveCalUnit = null;
            }
            else
            {
                this.MoveCalUnit = rs.getString("MoveCalUnit").trim();
            }

            if (rs.getString("AliveSurrCalPoint") == null)
            {
                this.AliveSurrCalPoint = null;
            }
            else
            {
                this.AliveSurrCalPoint = rs.getString("AliveSurrCalPoint").trim();
            }

            if (rs.getString("DeadSurrCalPoint") == null)
            {
                this.DeadSurrCalPoint = null;
            }
            else
            {
                this.DeadSurrCalPoint = rs.getString("DeadSurrCalPoint").trim();
            }

            if (rs.getString("MoveCalPoint") == null)
            {
                this.MoveCalPoint = null;
            }
            else
            {
                this.MoveCalPoint = rs.getString("MoveCalPoint").trim();
            }

            if (rs.getString("CalInterestMode") == null)
            {
                this.CalInterestMode = null;
            }
            else
            {
                this.CalInterestMode = rs.getString("CalInterestMode").trim();
            }

            if (rs.getString("CustEdorReCalFlag") == null)
            {
                this.CustEdorReCalFlag = null;
            }
            else
            {
                this.CustEdorReCalFlag = rs.getString("CustEdorReCalFlag").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskEdorSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMRiskEdorSchema getSchema()
    {
        LMRiskEdorSchema aLMRiskEdorSchema = new LMRiskEdorSchema();
        aLMRiskEdorSchema.setSchema(this);
        return aLMRiskEdorSchema;
    }

    public LMRiskEdorDB getDB()
    {
        LMRiskEdorDB aDBOper = new LMRiskEdorDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskEdor描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorReCalFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AliveSurrToPrem)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DeadSurrToPremFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OutToPremFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AliveSurrCalUnit)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DeadSurrCalUnit)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MoveCalUnit)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AliveSurrCalPoint)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DeadSurrCalPoint)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MoveCalPoint)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalInterestMode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustEdorReCalFlag));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMRiskEdor>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            RiskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            EdorReCalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            AliveSurrToPrem = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             5, SysConst.PACKAGESPILTER);
            DeadSurrToPremFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                6, SysConst.PACKAGESPILTER);
            OutToPremFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            AliveSurrCalUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              8, SysConst.PACKAGESPILTER);
            DeadSurrCalUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             9, SysConst.PACKAGESPILTER);
            MoveCalUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            AliveSurrCalPoint = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               11, SysConst.PACKAGESPILTER);
            DeadSurrCalPoint = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              12, SysConst.PACKAGESPILTER);
            MoveCalPoint = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                          SysConst.PACKAGESPILTER);
            CalInterestMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             14, SysConst.PACKAGESPILTER);
            CustEdorReCalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               15, SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMRiskEdorSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVer));
        }
        if (FCode.equals("RiskName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskName));
        }
        if (FCode.equals("EdorReCalFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorReCalFlag));
        }
        if (FCode.equals("AliveSurrToPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AliveSurrToPrem));
        }
        if (FCode.equals("DeadSurrToPremFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    DeadSurrToPremFlag));
        }
        if (FCode.equals("OutToPremFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OutToPremFlag));
        }
        if (FCode.equals("AliveSurrCalUnit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AliveSurrCalUnit));
        }
        if (FCode.equals("DeadSurrCalUnit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DeadSurrCalUnit));
        }
        if (FCode.equals("MoveCalUnit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MoveCalUnit));
        }
        if (FCode.equals("AliveSurrCalPoint"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    AliveSurrCalPoint));
        }
        if (FCode.equals("DeadSurrCalPoint"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DeadSurrCalPoint));
        }
        if (FCode.equals("MoveCalPoint"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MoveCalPoint));
        }
        if (FCode.equals("CalInterestMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalInterestMode));
        }
        if (FCode.equals("CustEdorReCalFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    CustEdorReCalFlag));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVer);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(EdorReCalFlag);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AliveSurrToPrem);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DeadSurrToPremFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(OutToPremFlag);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AliveSurrCalUnit);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(DeadSurrCalUnit);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MoveCalUnit);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AliveSurrCalPoint);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(DeadSurrCalPoint);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MoveCalPoint);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(CalInterestMode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(CustEdorReCalFlag);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVer = FValue.trim();
            }
            else
            {
                RiskVer = null;
            }
        }
        if (FCode.equals("RiskName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskName = FValue.trim();
            }
            else
            {
                RiskName = null;
            }
        }
        if (FCode.equals("EdorReCalFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorReCalFlag = FValue.trim();
            }
            else
            {
                EdorReCalFlag = null;
            }
        }
        if (FCode.equals("AliveSurrToPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AliveSurrToPrem = FValue.trim();
            }
            else
            {
                AliveSurrToPrem = null;
            }
        }
        if (FCode.equals("DeadSurrToPremFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeadSurrToPremFlag = FValue.trim();
            }
            else
            {
                DeadSurrToPremFlag = null;
            }
        }
        if (FCode.equals("OutToPremFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OutToPremFlag = FValue.trim();
            }
            else
            {
                OutToPremFlag = null;
            }
        }
        if (FCode.equals("AliveSurrCalUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AliveSurrCalUnit = FValue.trim();
            }
            else
            {
                AliveSurrCalUnit = null;
            }
        }
        if (FCode.equals("DeadSurrCalUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeadSurrCalUnit = FValue.trim();
            }
            else
            {
                DeadSurrCalUnit = null;
            }
        }
        if (FCode.equals("MoveCalUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MoveCalUnit = FValue.trim();
            }
            else
            {
                MoveCalUnit = null;
            }
        }
        if (FCode.equals("AliveSurrCalPoint"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AliveSurrCalPoint = FValue.trim();
            }
            else
            {
                AliveSurrCalPoint = null;
            }
        }
        if (FCode.equals("DeadSurrCalPoint"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeadSurrCalPoint = FValue.trim();
            }
            else
            {
                DeadSurrCalPoint = null;
            }
        }
        if (FCode.equals("MoveCalPoint"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MoveCalPoint = FValue.trim();
            }
            else
            {
                MoveCalPoint = null;
            }
        }
        if (FCode.equals("CalInterestMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalInterestMode = FValue.trim();
            }
            else
            {
                CalInterestMode = null;
            }
        }
        if (FCode.equals("CustEdorReCalFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustEdorReCalFlag = FValue.trim();
            }
            else
            {
                CustEdorReCalFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMRiskEdorSchema other = (LMRiskEdorSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && RiskName.equals(other.getRiskName())
                && EdorReCalFlag.equals(other.getEdorReCalFlag())
                && AliveSurrToPrem.equals(other.getAliveSurrToPrem())
                && DeadSurrToPremFlag.equals(other.getDeadSurrToPremFlag())
                && OutToPremFlag.equals(other.getOutToPremFlag())
                && AliveSurrCalUnit.equals(other.getAliveSurrCalUnit())
                && DeadSurrCalUnit.equals(other.getDeadSurrCalUnit())
                && MoveCalUnit.equals(other.getMoveCalUnit())
                && AliveSurrCalPoint.equals(other.getAliveSurrCalPoint())
                && DeadSurrCalPoint.equals(other.getDeadSurrCalPoint())
                && MoveCalPoint.equals(other.getMoveCalPoint())
                && CalInterestMode.equals(other.getCalInterestMode())
                && CustEdorReCalFlag.equals(other.getCustEdorReCalFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskName"))
        {
            return 2;
        }
        if (strFieldName.equals("EdorReCalFlag"))
        {
            return 3;
        }
        if (strFieldName.equals("AliveSurrToPrem"))
        {
            return 4;
        }
        if (strFieldName.equals("DeadSurrToPremFlag"))
        {
            return 5;
        }
        if (strFieldName.equals("OutToPremFlag"))
        {
            return 6;
        }
        if (strFieldName.equals("AliveSurrCalUnit"))
        {
            return 7;
        }
        if (strFieldName.equals("DeadSurrCalUnit"))
        {
            return 8;
        }
        if (strFieldName.equals("MoveCalUnit"))
        {
            return 9;
        }
        if (strFieldName.equals("AliveSurrCalPoint"))
        {
            return 10;
        }
        if (strFieldName.equals("DeadSurrCalPoint"))
        {
            return 11;
        }
        if (strFieldName.equals("MoveCalPoint"))
        {
            return 12;
        }
        if (strFieldName.equals("CalInterestMode"))
        {
            return 13;
        }
        if (strFieldName.equals("CustEdorReCalFlag"))
        {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVer";
                break;
            case 2:
                strFieldName = "RiskName";
                break;
            case 3:
                strFieldName = "EdorReCalFlag";
                break;
            case 4:
                strFieldName = "AliveSurrToPrem";
                break;
            case 5:
                strFieldName = "DeadSurrToPremFlag";
                break;
            case 6:
                strFieldName = "OutToPremFlag";
                break;
            case 7:
                strFieldName = "AliveSurrCalUnit";
                break;
            case 8:
                strFieldName = "DeadSurrCalUnit";
                break;
            case 9:
                strFieldName = "MoveCalUnit";
                break;
            case 10:
                strFieldName = "AliveSurrCalPoint";
                break;
            case 11:
                strFieldName = "DeadSurrCalPoint";
                break;
            case 12:
                strFieldName = "MoveCalPoint";
                break;
            case 13:
                strFieldName = "CalInterestMode";
                break;
            case 14:
                strFieldName = "CustEdorReCalFlag";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorReCalFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AliveSurrToPrem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeadSurrToPremFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OutToPremFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AliveSurrCalUnit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeadSurrCalUnit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MoveCalUnit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AliveSurrCalPoint"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeadSurrCalPoint"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MoveCalPoint"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalInterestMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustEdorReCalFlag"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
