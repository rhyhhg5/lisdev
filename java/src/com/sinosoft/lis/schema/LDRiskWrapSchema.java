/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDRiskWrapDB;

/*
 * <p>ClassName: LDRiskWrapSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-10-11
 */
public class LDRiskWrapSchema implements Schema, Cloneable {
    // @Field
    /** 套餐编码 */
    private String RiskWrapCode;
    /** 险种编码 */
    private String RiskCode;
    /** 套餐名称 */
    private String RiskWrapName;
    /** 套餐类型 */
    private String RiskWrapType;
    /** 开办日期 */
    private Date StartDate;
    /** 停办日期 */
    private Date EndDate;
    /** 保费 */
    private double Prem;
    /** 主险编码 */
    private String MainRiskCode;
    /** 最小投保人年龄 */
    private int MinAppntAge;
    /** 最大投保人年龄 */
    private int MaxAppntAge;
    /** 最大被保人年龄 */
    private int MaxInsuredAge;
    /** 最小被保人年龄 */
    private int MinInsuredAge;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDRiskWrapSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskWrapCode";
        pk[1] = "RiskCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDRiskWrapSchema cloned = (LDRiskWrapSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRiskWrapCode() {
        return RiskWrapCode;
    }

    public void setRiskWrapCode(String aRiskWrapCode) {
        RiskWrapCode = aRiskWrapCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getRiskWrapName() {
        return RiskWrapName;
    }

    public void setRiskWrapName(String aRiskWrapName) {
        RiskWrapName = aRiskWrapName;
    }

    public String getRiskWrapType() {
        return RiskWrapType;
    }

    public void setRiskWrapType(String aRiskWrapType) {
        RiskWrapType = aRiskWrapType;
    }

    public String getStartDate() {
        if (StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }

    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else {
            StartDate = null;
        }
    }

    public String getEndDate() {
        if (EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }

    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else {
            EndDate = null;
        }
    }

    public double getPrem() {
        return Prem;
    }

    public void setPrem(double aPrem) {
        Prem = Arith.round(aPrem, 2);
    }

    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = Arith.round(d, 2);
        }
    }

    public String getMainRiskCode() {
        return MainRiskCode;
    }

    public void setMainRiskCode(String aMainRiskCode) {
        MainRiskCode = aMainRiskCode;
    }

    public int getMinAppntAge() {
        return MinAppntAge;
    }

    public void setMinAppntAge(int aMinAppntAge) {
        MinAppntAge = aMinAppntAge;
    }

    public void setMinAppntAge(String aMinAppntAge) {
        if (aMinAppntAge != null && !aMinAppntAge.equals("")) {
            Integer tInteger = new Integer(aMinAppntAge);
            int i = tInteger.intValue();
            MinAppntAge = i;
        }
    }

    public int getMaxAppntAge() {
        return MaxAppntAge;
    }

    public void setMaxAppntAge(int aMaxAppntAge) {
        MaxAppntAge = aMaxAppntAge;
    }

    public void setMaxAppntAge(String aMaxAppntAge) {
        if (aMaxAppntAge != null && !aMaxAppntAge.equals("")) {
            Integer tInteger = new Integer(aMaxAppntAge);
            int i = tInteger.intValue();
            MaxAppntAge = i;
        }
    }

    public int getMaxInsuredAge() {
        return MaxInsuredAge;
    }

    public void setMaxInsuredAge(int aMaxInsuredAge) {
        MaxInsuredAge = aMaxInsuredAge;
    }

    public void setMaxInsuredAge(String aMaxInsuredAge) {
        if (aMaxInsuredAge != null && !aMaxInsuredAge.equals("")) {
            Integer tInteger = new Integer(aMaxInsuredAge);
            int i = tInteger.intValue();
            MaxInsuredAge = i;
        }
    }

    public int getMinInsuredAge() {
        return MinInsuredAge;
    }

    public void setMinInsuredAge(int aMinInsuredAge) {
        MinInsuredAge = aMinInsuredAge;
    }

    public void setMinInsuredAge(String aMinInsuredAge) {
        if (aMinInsuredAge != null && !aMinInsuredAge.equals("")) {
            Integer tInteger = new Integer(aMinInsuredAge);
            int i = tInteger.intValue();
            MinInsuredAge = i;
        }
    }


    /**
     * 使用另外一个 LDRiskWrapSchema 对象给 Schema 赋值
     * @param: aLDRiskWrapSchema LDRiskWrapSchema
     **/
    public void setSchema(LDRiskWrapSchema aLDRiskWrapSchema) {
        this.RiskWrapCode = aLDRiskWrapSchema.getRiskWrapCode();
        this.RiskCode = aLDRiskWrapSchema.getRiskCode();
        this.RiskWrapName = aLDRiskWrapSchema.getRiskWrapName();
        this.RiskWrapType = aLDRiskWrapSchema.getRiskWrapType();
        this.StartDate = fDate.getDate(aLDRiskWrapSchema.getStartDate());
        this.EndDate = fDate.getDate(aLDRiskWrapSchema.getEndDate());
        this.Prem = aLDRiskWrapSchema.getPrem();
        this.MainRiskCode = aLDRiskWrapSchema.getMainRiskCode();
        this.MinAppntAge = aLDRiskWrapSchema.getMinAppntAge();
        this.MaxAppntAge = aLDRiskWrapSchema.getMaxAppntAge();
        this.MaxInsuredAge = aLDRiskWrapSchema.getMaxInsuredAge();
        this.MinInsuredAge = aLDRiskWrapSchema.getMinInsuredAge();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskWrapCode") == null) {
                this.RiskWrapCode = null;
            } else {
                this.RiskWrapCode = rs.getString("RiskWrapCode").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskWrapName") == null) {
                this.RiskWrapName = null;
            } else {
                this.RiskWrapName = rs.getString("RiskWrapName").trim();
            }

            if (rs.getString("RiskWrapType") == null) {
                this.RiskWrapType = null;
            } else {
                this.RiskWrapType = rs.getString("RiskWrapType").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.EndDate = rs.getDate("EndDate");
            this.Prem = rs.getDouble("Prem");
            if (rs.getString("MainRiskCode") == null) {
                this.MainRiskCode = null;
            } else {
                this.MainRiskCode = rs.getString("MainRiskCode").trim();
            }

            this.MinAppntAge = rs.getInt("MinAppntAge");
            this.MaxAppntAge = rs.getInt("MaxAppntAge");
            this.MaxInsuredAge = rs.getInt("MaxInsuredAge");
            this.MinInsuredAge = rs.getInt("MinInsuredAge");
        } catch (SQLException sqle) {
            System.out.println("数据库中的LDRiskWrap表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskWrapSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDRiskWrapSchema getSchema() {
        LDRiskWrapSchema aLDRiskWrapSchema = new LDRiskWrapSchema();
        aLDRiskWrapSchema.setSchema(this);
        return aLDRiskWrapSchema;
    }

    public LDRiskWrapDB getDB() {
        LDRiskWrapDB aDBOper = new LDRiskWrapDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskWrap描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RiskWrapCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskWrapName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskWrapType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MainRiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MinAppntAge));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MaxAppntAge));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MaxInsuredAge));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MinInsuredAge));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDRiskWrap>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            RiskWrapCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            RiskWrapName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            RiskWrapType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    7, SysConst.PACKAGESPILTER))).doubleValue();
            MainRiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            MinAppntAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            MaxAppntAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            MaxInsuredAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).intValue();
            MinInsuredAge = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).intValue();
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDRiskWrapSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("RiskWrapCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskWrapCode));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("RiskWrapName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskWrapName));
        }
        if (FCode.equals("RiskWrapType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskWrapType));
        }
        if (FCode.equals("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equals("MainRiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainRiskCode));
        }
        if (FCode.equals("MinAppntAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinAppntAge));
        }
        if (FCode.equals("MaxAppntAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAppntAge));
        }
        if (FCode.equals("MaxInsuredAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxInsuredAge));
        }
        if (FCode.equals("MinInsuredAge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MinInsuredAge));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(RiskWrapCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(RiskWrapName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(RiskWrapType);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStartDate()));
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
            break;
        case 6:
            strFieldValue = String.valueOf(Prem);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(MainRiskCode);
            break;
        case 8:
            strFieldValue = String.valueOf(MinAppntAge);
            break;
        case 9:
            strFieldValue = String.valueOf(MaxAppntAge);
            break;
        case 10:
            strFieldValue = String.valueOf(MaxInsuredAge);
            break;
        case 11:
            strFieldValue = String.valueOf(MinInsuredAge);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("RiskWrapCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskWrapCode = FValue.trim();
            } else {
                RiskWrapCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskWrapName")) {
            if (FValue != null && !FValue.equals("")) {
                RiskWrapName = FValue.trim();
            } else {
                RiskWrapName = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskWrapType")) {
            if (FValue != null && !FValue.equals("")) {
                RiskWrapType = FValue.trim();
            } else {
                RiskWrapType = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if (FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate(FValue);
            } else {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if (FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate(FValue);
            } else {
                EndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("MainRiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                MainRiskCode = FValue.trim();
            } else {
                MainRiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("MinAppntAge")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MinAppntAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MaxAppntAge")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MaxAppntAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MaxInsuredAge")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MaxInsuredAge = i;
            }
        }
        if (FCode.equalsIgnoreCase("MinInsuredAge")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MinInsuredAge = i;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDRiskWrapSchema other = (LDRiskWrapSchema) otherObject;
        return
                RiskWrapCode.equals(other.getRiskWrapCode())
                && RiskCode.equals(other.getRiskCode())
                && RiskWrapName.equals(other.getRiskWrapName())
                && RiskWrapType.equals(other.getRiskWrapType())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && Prem == other.getPrem()
                && MainRiskCode.equals(other.getMainRiskCode())
                && MinAppntAge == other.getMinAppntAge()
                && MaxAppntAge == other.getMaxAppntAge()
                && MaxInsuredAge == other.getMaxInsuredAge()
                && MinInsuredAge == other.getMinInsuredAge();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("RiskWrapCode")) {
            return 0;
        }
        if (strFieldName.equals("RiskCode")) {
            return 1;
        }
        if (strFieldName.equals("RiskWrapName")) {
            return 2;
        }
        if (strFieldName.equals("RiskWrapType")) {
            return 3;
        }
        if (strFieldName.equals("StartDate")) {
            return 4;
        }
        if (strFieldName.equals("EndDate")) {
            return 5;
        }
        if (strFieldName.equals("Prem")) {
            return 6;
        }
        if (strFieldName.equals("MainRiskCode")) {
            return 7;
        }
        if (strFieldName.equals("MinAppntAge")) {
            return 8;
        }
        if (strFieldName.equals("MaxAppntAge")) {
            return 9;
        }
        if (strFieldName.equals("MaxInsuredAge")) {
            return 10;
        }
        if (strFieldName.equals("MinInsuredAge")) {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "RiskWrapCode";
            break;
        case 1:
            strFieldName = "RiskCode";
            break;
        case 2:
            strFieldName = "RiskWrapName";
            break;
        case 3:
            strFieldName = "RiskWrapType";
            break;
        case 4:
            strFieldName = "StartDate";
            break;
        case 5:
            strFieldName = "EndDate";
            break;
        case 6:
            strFieldName = "Prem";
            break;
        case 7:
            strFieldName = "MainRiskCode";
            break;
        case 8:
            strFieldName = "MinAppntAge";
            break;
        case 9:
            strFieldName = "MaxAppntAge";
            break;
        case 10:
            strFieldName = "MaxInsuredAge";
            break;
        case 11:
            strFieldName = "MinInsuredAge";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("RiskWrapCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskWrapName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskWrapType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("EndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Prem")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MainRiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MinAppntAge")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MaxAppntAge")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MaxInsuredAge")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MinInsuredAge")) {
            return Schema.TYPE_INT;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 5:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 6:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_INT;
            break;
        case 9:
            nFieldType = Schema.TYPE_INT;
            break;
        case 10:
            nFieldType = Schema.TYPE_INT;
            break;
        case 11:
            nFieldType = Schema.TYPE_INT;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
