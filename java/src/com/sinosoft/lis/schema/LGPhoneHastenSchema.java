/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LGPhoneHastenDB;

/*
 * <p>ClassName: LGPhoneHastenSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 电话催收记录
 * @CreateDate：2005-11-15
 */
public class LGPhoneHastenSchema implements Schema, Cloneable {
    // @Field
    /** 回访记录号 */
    private String WorkNo;
    /** 催缴次数编号 */
    private String TimesNo;
    /** 保全受理号 */
    private String EdorAcceptNo;
    /** 投保人客户号 */
    private String CustomerNo;
    /** 缴费原因 */
    private String PayReason;
    /** 未交费原因 */
    private String DelayReason;
    /** 原缴费日期 */
    private Date OldPayDate;
    /** 原缴费方式 */
    private String OldPayMode;
    /** 原转账银行 */
    private String OldBankCode;
    /** 原转帐帐号 */
    private String OldBackAccNo;
    /** 备注 */
    private String Remark;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 结案类型 */
    private String FinishType;
    /** 催缴类型 */
    private String HastenType;
    /** 催收通知书号 */
    private String GetNoticeNo;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LGPhoneHastenSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "WorkNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LGPhoneHastenSchema cloned = (LGPhoneHastenSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getWorkNo() {
        return WorkNo;
    }

    public void setWorkNo(String aWorkNo) {
        WorkNo = aWorkNo;
    }

    public String getTimesNo() {
        return TimesNo;
    }

    public void setTimesNo(String aTimesNo) {
        TimesNo = aTimesNo;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String aEdorAcceptNo) {
        EdorAcceptNo = aEdorAcceptNo;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getPayReason() {
        return PayReason;
    }

    public void setPayReason(String aPayReason) {
        PayReason = aPayReason;
    }

    public String getDelayReason() {
        return DelayReason;
    }

    public void setDelayReason(String aDelayReason) {
        DelayReason = aDelayReason;
    }

    public String getOldPayDate() {
        if (OldPayDate != null) {
            return fDate.getString(OldPayDate);
        } else {
            return null;
        }
    }

    public void setOldPayDate(Date aOldPayDate) {
        OldPayDate = aOldPayDate;
    }

    public void setOldPayDate(String aOldPayDate) {
        if (aOldPayDate != null && !aOldPayDate.equals("")) {
            OldPayDate = fDate.getDate(aOldPayDate);
        } else {
            OldPayDate = null;
        }
    }

    public String getOldPayMode() {
        return OldPayMode;
    }

    public void setOldPayMode(String aOldPayMode) {
        OldPayMode = aOldPayMode;
    }

    public String getOldBankCode() {
        return OldBankCode;
    }

    public void setOldBankCode(String aOldBankCode) {
        OldBankCode = aOldBankCode;
    }

    public String getOldBackAccNo() {
        return OldBackAccNo;
    }

    public void setOldBackAccNo(String aOldBackAccNo) {
        OldBackAccNo = aOldBackAccNo;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getFinishType() {
        return FinishType;
    }

    public void setFinishType(String aFinishType) {
        FinishType = aFinishType;
    }

    public String getHastenType() {
        return HastenType;
    }

    public void setHastenType(String aHastenType) {
        HastenType = aHastenType;
    }

    public String getGetNoticeNo() {
        return GetNoticeNo;
    }

    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }

    /**
     * 使用另外一个 LGPhoneHastenSchema 对象给 Schema 赋值
     * @param: aLGPhoneHastenSchema LGPhoneHastenSchema
     **/
    public void setSchema(LGPhoneHastenSchema aLGPhoneHastenSchema) {
        this.WorkNo = aLGPhoneHastenSchema.getWorkNo();
        this.TimesNo = aLGPhoneHastenSchema.getTimesNo();
        this.EdorAcceptNo = aLGPhoneHastenSchema.getEdorAcceptNo();
        this.CustomerNo = aLGPhoneHastenSchema.getCustomerNo();
        this.PayReason = aLGPhoneHastenSchema.getPayReason();
        this.DelayReason = aLGPhoneHastenSchema.getDelayReason();
        this.OldPayDate = fDate.getDate(aLGPhoneHastenSchema.getOldPayDate());
        this.OldPayMode = aLGPhoneHastenSchema.getOldPayMode();
        this.OldBankCode = aLGPhoneHastenSchema.getOldBankCode();
        this.OldBackAccNo = aLGPhoneHastenSchema.getOldBackAccNo();
        this.Remark = aLGPhoneHastenSchema.getRemark();
        this.Operator = aLGPhoneHastenSchema.getOperator();
        this.MakeDate = fDate.getDate(aLGPhoneHastenSchema.getMakeDate());
        this.MakeTime = aLGPhoneHastenSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLGPhoneHastenSchema.getModifyDate());
        this.ModifyTime = aLGPhoneHastenSchema.getModifyTime();
        this.FinishType = aLGPhoneHastenSchema.getFinishType();
        this.HastenType = aLGPhoneHastenSchema.getHastenType();
        this.GetNoticeNo = aLGPhoneHastenSchema.getGetNoticeNo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("WorkNo") == null) {
                this.WorkNo = null;
            } else {
                this.WorkNo = rs.getString("WorkNo").trim();
            }

            if (rs.getString("TimesNo") == null) {
                this.TimesNo = null;
            } else {
                this.TimesNo = rs.getString("TimesNo").trim();
            }

            if (rs.getString("EdorAcceptNo") == null) {
                this.EdorAcceptNo = null;
            } else {
                this.EdorAcceptNo = rs.getString("EdorAcceptNo").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("PayReason") == null) {
                this.PayReason = null;
            } else {
                this.PayReason = rs.getString("PayReason").trim();
            }

            if (rs.getString("DelayReason") == null) {
                this.DelayReason = null;
            } else {
                this.DelayReason = rs.getString("DelayReason").trim();
            }

            this.OldPayDate = rs.getDate("OldPayDate");
            if (rs.getString("OldPayMode") == null) {
                this.OldPayMode = null;
            } else {
                this.OldPayMode = rs.getString("OldPayMode").trim();
            }

            if (rs.getString("OldBankCode") == null) {
                this.OldBankCode = null;
            } else {
                this.OldBankCode = rs.getString("OldBankCode").trim();
            }

            if (rs.getString("OldBackAccNo") == null) {
                this.OldBackAccNo = null;
            } else {
                this.OldBackAccNo = rs.getString("OldBackAccNo").trim();
            }

            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("FinishType") == null) {
                this.FinishType = null;
            } else {
                this.FinishType = rs.getString("FinishType").trim();
            }

            if (rs.getString("HastenType") == null) {
                this.HastenType = null;
            } else {
                this.HastenType = rs.getString("HastenType").trim();
            }

            if (rs.getString("GetNoticeNo") == null) {
                this.GetNoticeNo = null;
            } else {
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LGPhoneHasten表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGPhoneHastenSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LGPhoneHastenSchema getSchema() {
        LGPhoneHastenSchema aLGPhoneHastenSchema = new LGPhoneHastenSchema();
        aLGPhoneHastenSchema.setSchema(this);
        return aLGPhoneHastenSchema;
    }

    public LGPhoneHastenDB getDB() {
        LGPhoneHastenDB aDBOper = new LGPhoneHastenDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGPhoneHasten描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(WorkNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TimesNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorAcceptNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayReason));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DelayReason));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(OldPayDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OldPayMode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OldBankCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OldBackAccNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FinishType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HastenType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetNoticeNo));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGPhoneHasten>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            WorkNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            TimesNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            PayReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            DelayReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            OldPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            OldPayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            OldBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            OldBackAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
            FinishType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
            HastenType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                         SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGPhoneHastenSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("WorkNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkNo));
        }
        if (FCode.equals("TimesNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TimesNo));
        }
        if (FCode.equals("EdorAcceptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("PayReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayReason));
        }
        if (FCode.equals("DelayReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DelayReason));
        }
        if (FCode.equals("OldPayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getOldPayDate()));
        }
        if (FCode.equals("OldPayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldPayMode));
        }
        if (FCode.equals("OldBankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldBankCode));
        }
        if (FCode.equals("OldBackAccNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OldBackAccNo));
        }
        if (FCode.equals("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("FinishType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FinishType));
        }
        if (FCode.equals("HastenType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HastenType));
        }
        if (FCode.equals("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(WorkNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(TimesNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(PayReason);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(DelayReason);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getOldPayDate()));
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(OldPayMode);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(OldBankCode);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(OldBackAccNo);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(FinishType);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(HastenType);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("WorkNo")) {
            if (FValue != null && !FValue.equals("")) {
                WorkNo = FValue.trim();
            } else {
                WorkNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("TimesNo")) {
            if (FValue != null && !FValue.equals("")) {
                TimesNo = FValue.trim();
            } else {
                TimesNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            if (FValue != null && !FValue.equals("")) {
                EdorAcceptNo = FValue.trim();
            } else {
                EdorAcceptNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PayReason")) {
            if (FValue != null && !FValue.equals("")) {
                PayReason = FValue.trim();
            } else {
                PayReason = null;
            }
        }
        if (FCode.equalsIgnoreCase("DelayReason")) {
            if (FValue != null && !FValue.equals("")) {
                DelayReason = FValue.trim();
            } else {
                DelayReason = null;
            }
        }
        if (FCode.equalsIgnoreCase("OldPayDate")) {
            if (FValue != null && !FValue.equals("")) {
                OldPayDate = fDate.getDate(FValue);
            } else {
                OldPayDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("OldPayMode")) {
            if (FValue != null && !FValue.equals("")) {
                OldPayMode = FValue.trim();
            } else {
                OldPayMode = null;
            }
        }
        if (FCode.equalsIgnoreCase("OldBankCode")) {
            if (FValue != null && !FValue.equals("")) {
                OldBankCode = FValue.trim();
            } else {
                OldBankCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("OldBackAccNo")) {
            if (FValue != null && !FValue.equals("")) {
                OldBackAccNo = FValue.trim();
            } else {
                OldBackAccNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("FinishType")) {
            if (FValue != null && !FValue.equals("")) {
                FinishType = FValue.trim();
            } else {
                FinishType = null;
            }
        }
        if (FCode.equalsIgnoreCase("HastenType")) {
            if (FValue != null && !FValue.equals("")) {
                HastenType = FValue.trim();
            } else {
                HastenType = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if (FValue != null && !FValue.equals("")) {
                GetNoticeNo = FValue.trim();
            } else {
                GetNoticeNo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LGPhoneHastenSchema other = (LGPhoneHastenSchema) otherObject;
        return
                WorkNo.equals(other.getWorkNo())
                && TimesNo.equals(other.getTimesNo())
                && EdorAcceptNo.equals(other.getEdorAcceptNo())
                && CustomerNo.equals(other.getCustomerNo())
                && PayReason.equals(other.getPayReason())
                && DelayReason.equals(other.getDelayReason())
                && fDate.getString(OldPayDate).equals(other.getOldPayDate())
                && OldPayMode.equals(other.getOldPayMode())
                && OldBankCode.equals(other.getOldBankCode())
                && OldBackAccNo.equals(other.getOldBackAccNo())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && FinishType.equals(other.getFinishType())
                && HastenType.equals(other.getHastenType())
                && GetNoticeNo.equals(other.getGetNoticeNo());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("WorkNo")) {
            return 0;
        }
        if (strFieldName.equals("TimesNo")) {
            return 1;
        }
        if (strFieldName.equals("EdorAcceptNo")) {
            return 2;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 3;
        }
        if (strFieldName.equals("PayReason")) {
            return 4;
        }
        if (strFieldName.equals("DelayReason")) {
            return 5;
        }
        if (strFieldName.equals("OldPayDate")) {
            return 6;
        }
        if (strFieldName.equals("OldPayMode")) {
            return 7;
        }
        if (strFieldName.equals("OldBankCode")) {
            return 8;
        }
        if (strFieldName.equals("OldBackAccNo")) {
            return 9;
        }
        if (strFieldName.equals("Remark")) {
            return 10;
        }
        if (strFieldName.equals("Operator")) {
            return 11;
        }
        if (strFieldName.equals("MakeDate")) {
            return 12;
        }
        if (strFieldName.equals("MakeTime")) {
            return 13;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 14;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 15;
        }
        if (strFieldName.equals("FinishType")) {
            return 16;
        }
        if (strFieldName.equals("HastenType")) {
            return 17;
        }
        if (strFieldName.equals("GetNoticeNo")) {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "WorkNo";
            break;
        case 1:
            strFieldName = "TimesNo";
            break;
        case 2:
            strFieldName = "EdorAcceptNo";
            break;
        case 3:
            strFieldName = "CustomerNo";
            break;
        case 4:
            strFieldName = "PayReason";
            break;
        case 5:
            strFieldName = "DelayReason";
            break;
        case 6:
            strFieldName = "OldPayDate";
            break;
        case 7:
            strFieldName = "OldPayMode";
            break;
        case 8:
            strFieldName = "OldBankCode";
            break;
        case 9:
            strFieldName = "OldBackAccNo";
            break;
        case 10:
            strFieldName = "Remark";
            break;
        case 11:
            strFieldName = "Operator";
            break;
        case 12:
            strFieldName = "MakeDate";
            break;
        case 13:
            strFieldName = "MakeTime";
            break;
        case 14:
            strFieldName = "ModifyDate";
            break;
        case 15:
            strFieldName = "ModifyTime";
            break;
        case 16:
            strFieldName = "FinishType";
            break;
        case 17:
            strFieldName = "HastenType";
            break;
        case 18:
            strFieldName = "GetNoticeNo";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("WorkNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TimesNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorAcceptNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayReason")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DelayReason")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OldPayDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("OldPayMode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OldBankCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OldBackAccNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FinishType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HastenType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetNoticeNo")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
