/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLOutsourcingTotalDB;

/*
 * <p>ClassName: LLOutsourcingTotalSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2013-12-25
 */
public class LLOutsourcingTotalSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String Batchno;
	/** 外包费用总额 */
	private double Ostotal;
	/** 险种编码 */
	private String Riskcode;
	/** 结算开始日期 */
	private Date Balancestartdate;
	/** 结算结束日期 */
	private Date Balanceenddate;
	/** 保单年度 */
	private String Payyear;
	/** 管理机构 */
	private String Managecom;
	/** 外包单位账户名 */
	private String Osaccount;
	/** 外包单位账户号码 */
	private String Osaccountno;
	/** 外包单位开户行 */
	private String Osbank;
	/** 转账方式 */
	private String Transfertype;
	/** 审核意见 */
	private String Auditopinion;
	/** 审核状态 */
	private String Auditstate;
	/** 保单个数 */
	private String Contcount;
	/** 备用字段f1 */
	private String F1;
	/** 备用字段f2 */
	private String F2;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date Makedate;
	/** 入机时间 */
	private String Maketime;
	/** 修改日期 */
	private Date Modifydate;
	/** 修改时间 */
	private String Modifytime;

	public static final int FIELDNUM = 21;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLOutsourcingTotalSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Batchno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLOutsourcingTotalSchema cloned = (LLOutsourcingTotalSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchno()
	{
		return Batchno;
	}
	public void setBatchno(String aBatchno)
	{
		Batchno = aBatchno;
	}
	public double getOstotal()
	{
		return Ostotal;
	}
	public void setOstotal(double aOstotal)
	{
		Ostotal = Arith.round(aOstotal,2);
	}
	public void setOstotal(String aOstotal)
	{
		if (aOstotal != null && !aOstotal.equals(""))
		{
			Double tDouble = new Double(aOstotal);
			double d = tDouble.doubleValue();
                Ostotal = Arith.round(d,2);
		}
	}

	public String getRiskcode()
	{
		return Riskcode;
	}
	public void setRiskcode(String aRiskcode)
	{
		Riskcode = aRiskcode;
	}
	public String getBalancestartdate()
	{
		if( Balancestartdate != null )
			return fDate.getString(Balancestartdate);
		else
			return null;
	}
	public void setBalancestartdate(Date aBalancestartdate)
	{
		Balancestartdate = aBalancestartdate;
	}
	public void setBalancestartdate(String aBalancestartdate)
	{
		if (aBalancestartdate != null && !aBalancestartdate.equals("") )
		{
			Balancestartdate = fDate.getDate( aBalancestartdate );
		}
		else
			Balancestartdate = null;
	}

	public String getBalanceenddate()
	{
		if( Balanceenddate != null )
			return fDate.getString(Balanceenddate);
		else
			return null;
	}
	public void setBalanceenddate(Date aBalanceenddate)
	{
		Balanceenddate = aBalanceenddate;
	}
	public void setBalanceenddate(String aBalanceenddate)
	{
		if (aBalanceenddate != null && !aBalanceenddate.equals("") )
		{
			Balanceenddate = fDate.getDate( aBalanceenddate );
		}
		else
			Balanceenddate = null;
	}

	public String getPayyear()
	{
		return Payyear;
	}
	public void setPayyear(String aPayyear)
	{
		Payyear = aPayyear;
	}
	public String getManagecom()
	{
		return Managecom;
	}
	public void setManagecom(String aManagecom)
	{
		Managecom = aManagecom;
	}
	public String getOsaccount()
	{
		return Osaccount;
	}
	public void setOsaccount(String aOsaccount)
	{
		Osaccount = aOsaccount;
	}
	public String getOsaccountno()
	{
		return Osaccountno;
	}
	public void setOsaccountno(String aOsaccountno)
	{
		Osaccountno = aOsaccountno;
	}
	public String getOsbank()
	{
		return Osbank;
	}
	public void setOsbank(String aOsbank)
	{
		Osbank = aOsbank;
	}
	public String getTransfertype()
	{
		return Transfertype;
	}
	public void setTransfertype(String aTransfertype)
	{
		Transfertype = aTransfertype;
	}
	public String getAuditopinion()
	{
		return Auditopinion;
	}
	public void setAuditopinion(String aAuditopinion)
	{
		Auditopinion = aAuditopinion;
	}
	public String getAuditstate()
	{
		return Auditstate;
	}
	public void setAuditstate(String aAuditstate)
	{
		Auditstate = aAuditstate;
	}
	public String getContcount()
	{
		return Contcount;
	}
	public void setContcount(String aContcount)
	{
		Contcount = aContcount;
	}
	public String getF1()
	{
		return F1;
	}
	public void setF1(String aF1)
	{
		F1 = aF1;
	}
	public String getF2()
	{
		return F2;
	}
	public void setF2(String aF2)
	{
		F2 = aF2;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakedate()
	{
		if( Makedate != null )
			return fDate.getString(Makedate);
		else
			return null;
	}
	public void setMakedate(Date aMakedate)
	{
		Makedate = aMakedate;
	}
	public void setMakedate(String aMakedate)
	{
		if (aMakedate != null && !aMakedate.equals("") )
		{
			Makedate = fDate.getDate( aMakedate );
		}
		else
			Makedate = null;
	}

	public String getMaketime()
	{
		return Maketime;
	}
	public void setMaketime(String aMaketime)
	{
		Maketime = aMaketime;
	}
	public String getModifydate()
	{
		if( Modifydate != null )
			return fDate.getString(Modifydate);
		else
			return null;
	}
	public void setModifydate(Date aModifydate)
	{
		Modifydate = aModifydate;
	}
	public void setModifydate(String aModifydate)
	{
		if (aModifydate != null && !aModifydate.equals("") )
		{
			Modifydate = fDate.getDate( aModifydate );
		}
		else
			Modifydate = null;
	}

	public String getModifytime()
	{
		return Modifytime;
	}
	public void setModifytime(String aModifytime)
	{
		Modifytime = aModifytime;
	}

	/**
	* 使用另外一个 LLOutsourcingTotalSchema 对象给 Schema 赋值
	* @param: aLLOutsourcingTotalSchema LLOutsourcingTotalSchema
	**/
	public void setSchema(LLOutsourcingTotalSchema aLLOutsourcingTotalSchema)
	{
		this.Batchno = aLLOutsourcingTotalSchema.getBatchno();
		this.Ostotal = aLLOutsourcingTotalSchema.getOstotal();
		this.Riskcode = aLLOutsourcingTotalSchema.getRiskcode();
		this.Balancestartdate = fDate.getDate( aLLOutsourcingTotalSchema.getBalancestartdate());
		this.Balanceenddate = fDate.getDate( aLLOutsourcingTotalSchema.getBalanceenddate());
		this.Payyear = aLLOutsourcingTotalSchema.getPayyear();
		this.Managecom = aLLOutsourcingTotalSchema.getManagecom();
		this.Osaccount = aLLOutsourcingTotalSchema.getOsaccount();
		this.Osaccountno = aLLOutsourcingTotalSchema.getOsaccountno();
		this.Osbank = aLLOutsourcingTotalSchema.getOsbank();
		this.Transfertype = aLLOutsourcingTotalSchema.getTransfertype();
		this.Auditopinion = aLLOutsourcingTotalSchema.getAuditopinion();
		this.Auditstate = aLLOutsourcingTotalSchema.getAuditstate();
		this.Contcount = aLLOutsourcingTotalSchema.getContcount();
		this.F1 = aLLOutsourcingTotalSchema.getF1();
		this.F2 = aLLOutsourcingTotalSchema.getF2();
		this.Operator = aLLOutsourcingTotalSchema.getOperator();
		this.Makedate = fDate.getDate( aLLOutsourcingTotalSchema.getMakedate());
		this.Maketime = aLLOutsourcingTotalSchema.getMaketime();
		this.Modifydate = fDate.getDate( aLLOutsourcingTotalSchema.getModifydate());
		this.Modifytime = aLLOutsourcingTotalSchema.getModifytime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Batchno") == null )
				this.Batchno = null;
			else
				this.Batchno = rs.getString("Batchno").trim();

			this.Ostotal = rs.getDouble("Ostotal");
			if( rs.getString("Riskcode") == null )
				this.Riskcode = null;
			else
				this.Riskcode = rs.getString("Riskcode").trim();

			this.Balancestartdate = rs.getDate("Balancestartdate");
			this.Balanceenddate = rs.getDate("Balanceenddate");
			if( rs.getString("Payyear") == null )
				this.Payyear = null;
			else
				this.Payyear = rs.getString("Payyear").trim();

			if( rs.getString("Managecom") == null )
				this.Managecom = null;
			else
				this.Managecom = rs.getString("Managecom").trim();

			if( rs.getString("Osaccount") == null )
				this.Osaccount = null;
			else
				this.Osaccount = rs.getString("Osaccount").trim();

			if( rs.getString("Osaccountno") == null )
				this.Osaccountno = null;
			else
				this.Osaccountno = rs.getString("Osaccountno").trim();

			if( rs.getString("Osbank") == null )
				this.Osbank = null;
			else
				this.Osbank = rs.getString("Osbank").trim();

			if( rs.getString("Transfertype") == null )
				this.Transfertype = null;
			else
				this.Transfertype = rs.getString("Transfertype").trim();

			if( rs.getString("Auditopinion") == null )
				this.Auditopinion = null;
			else
				this.Auditopinion = rs.getString("Auditopinion").trim();

			if( rs.getString("Auditstate") == null )
				this.Auditstate = null;
			else
				this.Auditstate = rs.getString("Auditstate").trim();

			if( rs.getString("Contcount") == null )
				this.Contcount = null;
			else
				this.Contcount = rs.getString("Contcount").trim();

			if( rs.getString("F1") == null )
				this.F1 = null;
			else
				this.F1 = rs.getString("F1").trim();

			if( rs.getString("F2") == null )
				this.F2 = null;
			else
				this.F2 = rs.getString("F2").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.Makedate = rs.getDate("Makedate");
			if( rs.getString("Maketime") == null )
				this.Maketime = null;
			else
				this.Maketime = rs.getString("Maketime").trim();

			this.Modifydate = rs.getDate("Modifydate");
			if( rs.getString("Modifytime") == null )
				this.Modifytime = null;
			else
				this.Modifytime = rs.getString("Modifytime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLOutsourcingTotal表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLOutsourcingTotalSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLOutsourcingTotalSchema getSchema()
	{
		LLOutsourcingTotalSchema aLLOutsourcingTotalSchema = new LLOutsourcingTotalSchema();
		aLLOutsourcingTotalSchema.setSchema(this);
		return aLLOutsourcingTotalSchema;
	}

	public LLOutsourcingTotalDB getDB()
	{
		LLOutsourcingTotalDB aDBOper = new LLOutsourcingTotalDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLOutsourcingTotal描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Batchno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Ostotal));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Riskcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Balancestartdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Balanceenddate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Payyear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Managecom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Osaccount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Osaccountno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Osbank)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Transfertype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Auditopinion)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Auditstate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Contcount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Maketime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Modifydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Modifytime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLOutsourcingTotal>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Batchno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Ostotal = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).doubleValue();
			Riskcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Balancestartdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			Balanceenddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			Payyear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Osaccount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Osaccountno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Osbank = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Transfertype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Auditopinion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Auditstate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Contcount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			F1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			F2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			Maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Modifydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			Modifytime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLOutsourcingTotalSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Batchno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Batchno));
		}
		if (FCode.equals("Ostotal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Ostotal));
		}
		if (FCode.equals("Riskcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Riskcode));
		}
		if (FCode.equals("Balancestartdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBalancestartdate()));
		}
		if (FCode.equals("Balanceenddate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBalanceenddate()));
		}
		if (FCode.equals("Payyear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Payyear));
		}
		if (FCode.equals("Managecom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
		}
		if (FCode.equals("Osaccount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Osaccount));
		}
		if (FCode.equals("Osaccountno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Osaccountno));
		}
		if (FCode.equals("Osbank"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Osbank));
		}
		if (FCode.equals("Transfertype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Transfertype));
		}
		if (FCode.equals("Auditopinion"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Auditopinion));
		}
		if (FCode.equals("Auditstate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Auditstate));
		}
		if (FCode.equals("Contcount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Contcount));
		}
		if (FCode.equals("F1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F1));
		}
		if (FCode.equals("F2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
		}
		if (FCode.equals("Maketime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Maketime));
		}
		if (FCode.equals("Modifydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifydate()));
		}
		if (FCode.equals("Modifytime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Modifytime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Batchno);
				break;
			case 1:
				strFieldValue = String.valueOf(Ostotal);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Riskcode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBalancestartdate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBalanceenddate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Payyear);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Managecom);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Osaccount);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Osaccountno);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Osbank);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Transfertype);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Auditopinion);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Auditstate);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Contcount);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(F1);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(F2);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(Maketime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifydate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Modifytime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Batchno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Batchno = FValue.trim();
			}
			else
				Batchno = null;
		}
		if (FCode.equalsIgnoreCase("Ostotal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Ostotal = d;
			}
		}
		if (FCode.equalsIgnoreCase("Riskcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Riskcode = FValue.trim();
			}
			else
				Riskcode = null;
		}
		if (FCode.equalsIgnoreCase("Balancestartdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Balancestartdate = fDate.getDate( FValue );
			}
			else
				Balancestartdate = null;
		}
		if (FCode.equalsIgnoreCase("Balanceenddate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Balanceenddate = fDate.getDate( FValue );
			}
			else
				Balanceenddate = null;
		}
		if (FCode.equalsIgnoreCase("Payyear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Payyear = FValue.trim();
			}
			else
				Payyear = null;
		}
		if (FCode.equalsIgnoreCase("Managecom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Managecom = FValue.trim();
			}
			else
				Managecom = null;
		}
		if (FCode.equalsIgnoreCase("Osaccount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Osaccount = FValue.trim();
			}
			else
				Osaccount = null;
		}
		if (FCode.equalsIgnoreCase("Osaccountno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Osaccountno = FValue.trim();
			}
			else
				Osaccountno = null;
		}
		if (FCode.equalsIgnoreCase("Osbank"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Osbank = FValue.trim();
			}
			else
				Osbank = null;
		}
		if (FCode.equalsIgnoreCase("Transfertype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Transfertype = FValue.trim();
			}
			else
				Transfertype = null;
		}
		if (FCode.equalsIgnoreCase("Auditopinion"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Auditopinion = FValue.trim();
			}
			else
				Auditopinion = null;
		}
		if (FCode.equalsIgnoreCase("Auditstate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Auditstate = FValue.trim();
			}
			else
				Auditstate = null;
		}
		if (FCode.equalsIgnoreCase("Contcount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Contcount = FValue.trim();
			}
			else
				Contcount = null;
		}
		if (FCode.equalsIgnoreCase("F1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F1 = FValue.trim();
			}
			else
				F1 = null;
		}
		if (FCode.equalsIgnoreCase("F2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F2 = FValue.trim();
			}
			else
				F2 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Makedate = fDate.getDate( FValue );
			}
			else
				Makedate = null;
		}
		if (FCode.equalsIgnoreCase("Maketime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Maketime = FValue.trim();
			}
			else
				Maketime = null;
		}
		if (FCode.equalsIgnoreCase("Modifydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Modifydate = fDate.getDate( FValue );
			}
			else
				Modifydate = null;
		}
		if (FCode.equalsIgnoreCase("Modifytime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Modifytime = FValue.trim();
			}
			else
				Modifytime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLOutsourcingTotalSchema other = (LLOutsourcingTotalSchema)otherObject;
		return
			(Batchno == null ? other.getBatchno() == null : Batchno.equals(other.getBatchno()))
			&& Ostotal == other.getOstotal()
			&& (Riskcode == null ? other.getRiskcode() == null : Riskcode.equals(other.getRiskcode()))
			&& (Balancestartdate == null ? other.getBalancestartdate() == null : fDate.getString(Balancestartdate).equals(other.getBalancestartdate()))
			&& (Balanceenddate == null ? other.getBalanceenddate() == null : fDate.getString(Balanceenddate).equals(other.getBalanceenddate()))
			&& (Payyear == null ? other.getPayyear() == null : Payyear.equals(other.getPayyear()))
			&& (Managecom == null ? other.getManagecom() == null : Managecom.equals(other.getManagecom()))
			&& (Osaccount == null ? other.getOsaccount() == null : Osaccount.equals(other.getOsaccount()))
			&& (Osaccountno == null ? other.getOsaccountno() == null : Osaccountno.equals(other.getOsaccountno()))
			&& (Osbank == null ? other.getOsbank() == null : Osbank.equals(other.getOsbank()))
			&& (Transfertype == null ? other.getTransfertype() == null : Transfertype.equals(other.getTransfertype()))
			&& (Auditopinion == null ? other.getAuditopinion() == null : Auditopinion.equals(other.getAuditopinion()))
			&& (Auditstate == null ? other.getAuditstate() == null : Auditstate.equals(other.getAuditstate()))
			&& (Contcount == null ? other.getContcount() == null : Contcount.equals(other.getContcount()))
			&& (F1 == null ? other.getF1() == null : F1.equals(other.getF1()))
			&& (F2 == null ? other.getF2() == null : F2.equals(other.getF2()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (Makedate == null ? other.getMakedate() == null : fDate.getString(Makedate).equals(other.getMakedate()))
			&& (Maketime == null ? other.getMaketime() == null : Maketime.equals(other.getMaketime()))
			&& (Modifydate == null ? other.getModifydate() == null : fDate.getString(Modifydate).equals(other.getModifydate()))
			&& (Modifytime == null ? other.getModifytime() == null : Modifytime.equals(other.getModifytime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Batchno") ) {
			return 0;
		}
		if( strFieldName.equals("Ostotal") ) {
			return 1;
		}
		if( strFieldName.equals("Riskcode") ) {
			return 2;
		}
		if( strFieldName.equals("Balancestartdate") ) {
			return 3;
		}
		if( strFieldName.equals("Balanceenddate") ) {
			return 4;
		}
		if( strFieldName.equals("Payyear") ) {
			return 5;
		}
		if( strFieldName.equals("Managecom") ) {
			return 6;
		}
		if( strFieldName.equals("Osaccount") ) {
			return 7;
		}
		if( strFieldName.equals("Osaccountno") ) {
			return 8;
		}
		if( strFieldName.equals("Osbank") ) {
			return 9;
		}
		if( strFieldName.equals("Transfertype") ) {
			return 10;
		}
		if( strFieldName.equals("Auditopinion") ) {
			return 11;
		}
		if( strFieldName.equals("Auditstate") ) {
			return 12;
		}
		if( strFieldName.equals("Contcount") ) {
			return 13;
		}
		if( strFieldName.equals("F1") ) {
			return 14;
		}
		if( strFieldName.equals("F2") ) {
			return 15;
		}
		if( strFieldName.equals("Operator") ) {
			return 16;
		}
		if( strFieldName.equals("Makedate") ) {
			return 17;
		}
		if( strFieldName.equals("Maketime") ) {
			return 18;
		}
		if( strFieldName.equals("Modifydate") ) {
			return 19;
		}
		if( strFieldName.equals("Modifytime") ) {
			return 20;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Batchno";
				break;
			case 1:
				strFieldName = "Ostotal";
				break;
			case 2:
				strFieldName = "Riskcode";
				break;
			case 3:
				strFieldName = "Balancestartdate";
				break;
			case 4:
				strFieldName = "Balanceenddate";
				break;
			case 5:
				strFieldName = "Payyear";
				break;
			case 6:
				strFieldName = "Managecom";
				break;
			case 7:
				strFieldName = "Osaccount";
				break;
			case 8:
				strFieldName = "Osaccountno";
				break;
			case 9:
				strFieldName = "Osbank";
				break;
			case 10:
				strFieldName = "Transfertype";
				break;
			case 11:
				strFieldName = "Auditopinion";
				break;
			case 12:
				strFieldName = "Auditstate";
				break;
			case 13:
				strFieldName = "Contcount";
				break;
			case 14:
				strFieldName = "F1";
				break;
			case 15:
				strFieldName = "F2";
				break;
			case 16:
				strFieldName = "Operator";
				break;
			case 17:
				strFieldName = "Makedate";
				break;
			case 18:
				strFieldName = "Maketime";
				break;
			case 19:
				strFieldName = "Modifydate";
				break;
			case 20:
				strFieldName = "Modifytime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Batchno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Ostotal") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Riskcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Balancestartdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Balanceenddate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Payyear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Managecom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Osaccount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Osaccountno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Osbank") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Transfertype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Auditopinion") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Auditstate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Contcount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Maketime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Modifydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Modifytime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
