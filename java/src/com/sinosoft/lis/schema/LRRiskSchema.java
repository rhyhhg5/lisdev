/*
 * <p>ClassName: LRRiskSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LRRiskDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LRRiskSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种分类 */
    private String RiskSort;
    /** 险种计算分类 */
    private String RiskCalSort;
    /** 再保险公司 */
    private String ReinsureCom;
    /** 再保项目 */
    private String ReinsurItem;
    /** 自留额上限 */
    private double RetainLine;
    /** 分出额 */
    private double CessionLimit;
    /** 分保方式 */
    private String CessionMode;
    /** 额外死亡率 */
    private double ExMorRate;
    /** 自留额计算比例 */
    private double RetentRate;
    /** 佣金率 */
    private double CommsionRate;
    /** 分保费率算法 */
    private String CessPremMode;
    /** 风险保额算法 */
    private String RiskAmntCalCode;
    /** 分保保费算法 */
    private String CessPremCalCode;
    /** 分保手续费算法 */
    private String CessCommCalCode2;
    /** 理赔摊回金额算法 */
    private String ReturnPayCalCode;
    /** 是否临分算法 */
    private String NegoCalCode;
    /** 团体内个人保额上限 */
    private double GrpPerAmtLimit;
    /** 团体内人均保额上限 */
    private double GrpAverAmntLimit;

    public static final int FIELDNUM = 19; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LRRiskSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "RiskCode";
        pk[1] = "RiskSort";
        pk[2] = "ReinsureCom";
        pk[3] = "ReinsurItem";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskSort()
    {
        if (RiskSort != null && !RiskSort.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskSort = StrTool.unicodeToGBK(RiskSort);
        }
        return RiskSort;
    }

    public void setRiskSort(String aRiskSort)
    {
        RiskSort = aRiskSort;
    }

    public String getRiskCalSort()
    {
        if (RiskCalSort != null && !RiskCalSort.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskCalSort = StrTool.unicodeToGBK(RiskCalSort);
        }
        return RiskCalSort;
    }

    public void setRiskCalSort(String aRiskCalSort)
    {
        RiskCalSort = aRiskCalSort;
    }

    public String getReinsureCom()
    {
        if (ReinsureCom != null && !ReinsureCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReinsureCom = StrTool.unicodeToGBK(ReinsureCom);
        }
        return ReinsureCom;
    }

    public void setReinsureCom(String aReinsureCom)
    {
        ReinsureCom = aReinsureCom;
    }

    public String getReinsurItem()
    {
        if (ReinsurItem != null && !ReinsurItem.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReinsurItem = StrTool.unicodeToGBK(ReinsurItem);
        }
        return ReinsurItem;
    }

    public void setReinsurItem(String aReinsurItem)
    {
        ReinsurItem = aReinsurItem;
    }

    public double getRetainLine()
    {
        return RetainLine;
    }

    public void setRetainLine(double aRetainLine)
    {
        RetainLine = aRetainLine;
    }

    public void setRetainLine(String aRetainLine)
    {
        if (aRetainLine != null && !aRetainLine.equals(""))
        {
            Double tDouble = new Double(aRetainLine);
            double d = tDouble.doubleValue();
            RetainLine = d;
        }
    }

    public double getCessionLimit()
    {
        return CessionLimit;
    }

    public void setCessionLimit(double aCessionLimit)
    {
        CessionLimit = aCessionLimit;
    }

    public void setCessionLimit(String aCessionLimit)
    {
        if (aCessionLimit != null && !aCessionLimit.equals(""))
        {
            Double tDouble = new Double(aCessionLimit);
            double d = tDouble.doubleValue();
            CessionLimit = d;
        }
    }

    public String getCessionMode()
    {
        if (CessionMode != null && !CessionMode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CessionMode = StrTool.unicodeToGBK(CessionMode);
        }
        return CessionMode;
    }

    public void setCessionMode(String aCessionMode)
    {
        CessionMode = aCessionMode;
    }

    public double getExMorRate()
    {
        return ExMorRate;
    }

    public void setExMorRate(double aExMorRate)
    {
        ExMorRate = aExMorRate;
    }

    public void setExMorRate(String aExMorRate)
    {
        if (aExMorRate != null && !aExMorRate.equals(""))
        {
            Double tDouble = new Double(aExMorRate);
            double d = tDouble.doubleValue();
            ExMorRate = d;
        }
    }

    public double getRetentRate()
    {
        return RetentRate;
    }

    public void setRetentRate(double aRetentRate)
    {
        RetentRate = aRetentRate;
    }

    public void setRetentRate(String aRetentRate)
    {
        if (aRetentRate != null && !aRetentRate.equals(""))
        {
            Double tDouble = new Double(aRetentRate);
            double d = tDouble.doubleValue();
            RetentRate = d;
        }
    }

    public double getCommsionRate()
    {
        return CommsionRate;
    }

    public void setCommsionRate(double aCommsionRate)
    {
        CommsionRate = aCommsionRate;
    }

    public void setCommsionRate(String aCommsionRate)
    {
        if (aCommsionRate != null && !aCommsionRate.equals(""))
        {
            Double tDouble = new Double(aCommsionRate);
            double d = tDouble.doubleValue();
            CommsionRate = d;
        }
    }

    public String getCessPremMode()
    {
        if (CessPremMode != null && !CessPremMode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CessPremMode = StrTool.unicodeToGBK(CessPremMode);
        }
        return CessPremMode;
    }

    public void setCessPremMode(String aCessPremMode)
    {
        CessPremMode = aCessPremMode;
    }

    public String getRiskAmntCalCode()
    {
        if (RiskAmntCalCode != null && !RiskAmntCalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskAmntCalCode = StrTool.unicodeToGBK(RiskAmntCalCode);
        }
        return RiskAmntCalCode;
    }

    public void setRiskAmntCalCode(String aRiskAmntCalCode)
    {
        RiskAmntCalCode = aRiskAmntCalCode;
    }

    public String getCessPremCalCode()
    {
        if (CessPremCalCode != null && !CessPremCalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CessPremCalCode = StrTool.unicodeToGBK(CessPremCalCode);
        }
        return CessPremCalCode;
    }

    public void setCessPremCalCode(String aCessPremCalCode)
    {
        CessPremCalCode = aCessPremCalCode;
    }

    public String getCessCommCalCode2()
    {
        if (CessCommCalCode2 != null && !CessCommCalCode2.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CessCommCalCode2 = StrTool.unicodeToGBK(CessCommCalCode2);
        }
        return CessCommCalCode2;
    }

    public void setCessCommCalCode2(String aCessCommCalCode2)
    {
        CessCommCalCode2 = aCessCommCalCode2;
    }

    public String getReturnPayCalCode()
    {
        if (ReturnPayCalCode != null && !ReturnPayCalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReturnPayCalCode = StrTool.unicodeToGBK(ReturnPayCalCode);
        }
        return ReturnPayCalCode;
    }

    public void setReturnPayCalCode(String aReturnPayCalCode)
    {
        ReturnPayCalCode = aReturnPayCalCode;
    }

    public String getNegoCalCode()
    {
        if (NegoCalCode != null && !NegoCalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            NegoCalCode = StrTool.unicodeToGBK(NegoCalCode);
        }
        return NegoCalCode;
    }

    public void setNegoCalCode(String aNegoCalCode)
    {
        NegoCalCode = aNegoCalCode;
    }

    public double getGrpPerAmtLimit()
    {
        return GrpPerAmtLimit;
    }

    public void setGrpPerAmtLimit(double aGrpPerAmtLimit)
    {
        GrpPerAmtLimit = aGrpPerAmtLimit;
    }

    public void setGrpPerAmtLimit(String aGrpPerAmtLimit)
    {
        if (aGrpPerAmtLimit != null && !aGrpPerAmtLimit.equals(""))
        {
            Double tDouble = new Double(aGrpPerAmtLimit);
            double d = tDouble.doubleValue();
            GrpPerAmtLimit = d;
        }
    }

    public double getGrpAverAmntLimit()
    {
        return GrpAverAmntLimit;
    }

    public void setGrpAverAmntLimit(double aGrpAverAmntLimit)
    {
        GrpAverAmntLimit = aGrpAverAmntLimit;
    }

    public void setGrpAverAmntLimit(String aGrpAverAmntLimit)
    {
        if (aGrpAverAmntLimit != null && !aGrpAverAmntLimit.equals(""))
        {
            Double tDouble = new Double(aGrpAverAmntLimit);
            double d = tDouble.doubleValue();
            GrpAverAmntLimit = d;
        }
    }


    /**
     * 使用另外一个 LRRiskSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LRRiskSchema aLRRiskSchema)
    {
        this.RiskCode = aLRRiskSchema.getRiskCode();
        this.RiskSort = aLRRiskSchema.getRiskSort();
        this.RiskCalSort = aLRRiskSchema.getRiskCalSort();
        this.ReinsureCom = aLRRiskSchema.getReinsureCom();
        this.ReinsurItem = aLRRiskSchema.getReinsurItem();
        this.RetainLine = aLRRiskSchema.getRetainLine();
        this.CessionLimit = aLRRiskSchema.getCessionLimit();
        this.CessionMode = aLRRiskSchema.getCessionMode();
        this.ExMorRate = aLRRiskSchema.getExMorRate();
        this.RetentRate = aLRRiskSchema.getRetentRate();
        this.CommsionRate = aLRRiskSchema.getCommsionRate();
        this.CessPremMode = aLRRiskSchema.getCessPremMode();
        this.RiskAmntCalCode = aLRRiskSchema.getRiskAmntCalCode();
        this.CessPremCalCode = aLRRiskSchema.getCessPremCalCode();
        this.CessCommCalCode2 = aLRRiskSchema.getCessCommCalCode2();
        this.ReturnPayCalCode = aLRRiskSchema.getReturnPayCalCode();
        this.NegoCalCode = aLRRiskSchema.getNegoCalCode();
        this.GrpPerAmtLimit = aLRRiskSchema.getGrpPerAmtLimit();
        this.GrpAverAmntLimit = aLRRiskSchema.getGrpAverAmntLimit();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskSort") == null)
            {
                this.RiskSort = null;
            }
            else
            {
                this.RiskSort = rs.getString("RiskSort").trim();
            }

            if (rs.getString("RiskCalSort") == null)
            {
                this.RiskCalSort = null;
            }
            else
            {
                this.RiskCalSort = rs.getString("RiskCalSort").trim();
            }

            if (rs.getString("ReinsureCom") == null)
            {
                this.ReinsureCom = null;
            }
            else
            {
                this.ReinsureCom = rs.getString("ReinsureCom").trim();
            }

            if (rs.getString("ReinsurItem") == null)
            {
                this.ReinsurItem = null;
            }
            else
            {
                this.ReinsurItem = rs.getString("ReinsurItem").trim();
            }

            this.RetainLine = rs.getDouble("RetainLine");
            this.CessionLimit = rs.getDouble("CessionLimit");
            if (rs.getString("CessionMode") == null)
            {
                this.CessionMode = null;
            }
            else
            {
                this.CessionMode = rs.getString("CessionMode").trim();
            }

            this.ExMorRate = rs.getDouble("ExMorRate");
            this.RetentRate = rs.getDouble("RetentRate");
            this.CommsionRate = rs.getDouble("CommsionRate");
            if (rs.getString("CessPremMode") == null)
            {
                this.CessPremMode = null;
            }
            else
            {
                this.CessPremMode = rs.getString("CessPremMode").trim();
            }

            if (rs.getString("RiskAmntCalCode") == null)
            {
                this.RiskAmntCalCode = null;
            }
            else
            {
                this.RiskAmntCalCode = rs.getString("RiskAmntCalCode").trim();
            }

            if (rs.getString("CessPremCalCode") == null)
            {
                this.CessPremCalCode = null;
            }
            else
            {
                this.CessPremCalCode = rs.getString("CessPremCalCode").trim();
            }

            if (rs.getString("CessCommCalCode2") == null)
            {
                this.CessCommCalCode2 = null;
            }
            else
            {
                this.CessCommCalCode2 = rs.getString("CessCommCalCode2").trim();
            }

            if (rs.getString("ReturnPayCalCode") == null)
            {
                this.ReturnPayCalCode = null;
            }
            else
            {
                this.ReturnPayCalCode = rs.getString("ReturnPayCalCode").trim();
            }

            if (rs.getString("NegoCalCode") == null)
            {
                this.NegoCalCode = null;
            }
            else
            {
                this.NegoCalCode = rs.getString("NegoCalCode").trim();
            }

            this.GrpPerAmtLimit = rs.getDouble("GrpPerAmtLimit");
            this.GrpAverAmntLimit = rs.getDouble("GrpAverAmntLimit");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRRiskSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LRRiskSchema getSchema()
    {
        LRRiskSchema aLRRiskSchema = new LRRiskSchema();
        aLRRiskSchema.setSchema(this);
        return aLRRiskSchema;
    }

    public LRRiskDB getDB()
    {
        LRRiskDB aDBOper = new LRRiskDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRRisk描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskSort)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCalSort)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReinsureCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReinsurItem)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(RetainLine) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CessionLimit) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CessionMode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ExMorRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(RetentRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CommsionRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CessPremMode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskAmntCalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CessPremCalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CessCommCalCode2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReturnPayCalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NegoCalCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GrpPerAmtLimit) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GrpAverAmntLimit);
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRRisk>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskSort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            RiskCalSort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            ReinsureCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            ReinsurItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            RetainLine = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            CessionLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).doubleValue();
            CessionMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
            ExMorRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            RetentRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            CommsionRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            CessPremMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            RiskAmntCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             13, SysConst.PACKAGESPILTER);
            CessPremCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             14, SysConst.PACKAGESPILTER);
            CessCommCalCode2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              15, SysConst.PACKAGESPILTER);
            ReturnPayCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              16, SysConst.PACKAGESPILTER);
            NegoCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                         SysConst.PACKAGESPILTER);
            GrpPerAmtLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            GrpAverAmntLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRRiskSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskSort"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskSort));
        }
        if (FCode.equals("RiskCalSort"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCalSort));
        }
        if (FCode.equals("ReinsureCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReinsureCom));
        }
        if (FCode.equals("ReinsurItem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReinsurItem));
        }
        if (FCode.equals("RetainLine"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RetainLine));
        }
        if (FCode.equals("CessionLimit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CessionLimit));
        }
        if (FCode.equals("CessionMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CessionMode));
        }
        if (FCode.equals("ExMorRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ExMorRate));
        }
        if (FCode.equals("RetentRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RetentRate));
        }
        if (FCode.equals("CommsionRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CommsionRate));
        }
        if (FCode.equals("CessPremMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CessPremMode));
        }
        if (FCode.equals("RiskAmntCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskAmntCalCode));
        }
        if (FCode.equals("CessPremCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CessPremCalCode));
        }
        if (FCode.equals("CessCommCalCode2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CessCommCalCode2));
        }
        if (FCode.equals("ReturnPayCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReturnPayCalCode));
        }
        if (FCode.equals("NegoCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NegoCalCode));
        }
        if (FCode.equals("GrpPerAmtLimit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpPerAmtLimit));
        }
        if (FCode.equals("GrpAverAmntLimit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GrpAverAmntLimit));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskSort);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskCalSort);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ReinsureCom);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ReinsurItem);
                break;
            case 5:
                strFieldValue = String.valueOf(RetainLine);
                break;
            case 6:
                strFieldValue = String.valueOf(CessionLimit);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(CessionMode);
                break;
            case 8:
                strFieldValue = String.valueOf(ExMorRate);
                break;
            case 9:
                strFieldValue = String.valueOf(RetentRate);
                break;
            case 10:
                strFieldValue = String.valueOf(CommsionRate);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(CessPremMode);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(RiskAmntCalCode);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(CessPremCalCode);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(CessCommCalCode2);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(ReturnPayCalCode);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(NegoCalCode);
                break;
            case 17:
                strFieldValue = String.valueOf(GrpPerAmtLimit);
                break;
            case 18:
                strFieldValue = String.valueOf(GrpAverAmntLimit);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskSort"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskSort = FValue.trim();
            }
            else
            {
                RiskSort = null;
            }
        }
        if (FCode.equals("RiskCalSort"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCalSort = FValue.trim();
            }
            else
            {
                RiskCalSort = null;
            }
        }
        if (FCode.equals("ReinsureCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReinsureCom = FValue.trim();
            }
            else
            {
                ReinsureCom = null;
            }
        }
        if (FCode.equals("ReinsurItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReinsurItem = FValue.trim();
            }
            else
            {
                ReinsurItem = null;
            }
        }
        if (FCode.equals("RetainLine"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RetainLine = d;
            }
        }
        if (FCode.equals("CessionLimit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CessionLimit = d;
            }
        }
        if (FCode.equals("CessionMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CessionMode = FValue.trim();
            }
            else
            {
                CessionMode = null;
            }
        }
        if (FCode.equals("ExMorRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ExMorRate = d;
            }
        }
        if (FCode.equals("RetentRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RetentRate = d;
            }
        }
        if (FCode.equals("CommsionRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CommsionRate = d;
            }
        }
        if (FCode.equals("CessPremMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CessPremMode = FValue.trim();
            }
            else
            {
                CessPremMode = null;
            }
        }
        if (FCode.equals("RiskAmntCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskAmntCalCode = FValue.trim();
            }
            else
            {
                RiskAmntCalCode = null;
            }
        }
        if (FCode.equals("CessPremCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CessPremCalCode = FValue.trim();
            }
            else
            {
                CessPremCalCode = null;
            }
        }
        if (FCode.equals("CessCommCalCode2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CessCommCalCode2 = FValue.trim();
            }
            else
            {
                CessCommCalCode2 = null;
            }
        }
        if (FCode.equals("ReturnPayCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReturnPayCalCode = FValue.trim();
            }
            else
            {
                ReturnPayCalCode = null;
            }
        }
        if (FCode.equals("NegoCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NegoCalCode = FValue.trim();
            }
            else
            {
                NegoCalCode = null;
            }
        }
        if (FCode.equals("GrpPerAmtLimit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GrpPerAmtLimit = d;
            }
        }
        if (FCode.equals("GrpAverAmntLimit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GrpAverAmntLimit = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LRRiskSchema other = (LRRiskSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskSort.equals(other.getRiskSort())
                && RiskCalSort.equals(other.getRiskCalSort())
                && ReinsureCom.equals(other.getReinsureCom())
                && ReinsurItem.equals(other.getReinsurItem())
                && RetainLine == other.getRetainLine()
                && CessionLimit == other.getCessionLimit()
                && CessionMode.equals(other.getCessionMode())
                && ExMorRate == other.getExMorRate()
                && RetentRate == other.getRetentRate()
                && CommsionRate == other.getCommsionRate()
                && CessPremMode.equals(other.getCessPremMode())
                && RiskAmntCalCode.equals(other.getRiskAmntCalCode())
                && CessPremCalCode.equals(other.getCessPremCalCode())
                && CessCommCalCode2.equals(other.getCessCommCalCode2())
                && ReturnPayCalCode.equals(other.getReturnPayCalCode())
                && NegoCalCode.equals(other.getNegoCalCode())
                && GrpPerAmtLimit == other.getGrpPerAmtLimit()
                && GrpAverAmntLimit == other.getGrpAverAmntLimit();
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskSort"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskCalSort"))
        {
            return 2;
        }
        if (strFieldName.equals("ReinsureCom"))
        {
            return 3;
        }
        if (strFieldName.equals("ReinsurItem"))
        {
            return 4;
        }
        if (strFieldName.equals("RetainLine"))
        {
            return 5;
        }
        if (strFieldName.equals("CessionLimit"))
        {
            return 6;
        }
        if (strFieldName.equals("CessionMode"))
        {
            return 7;
        }
        if (strFieldName.equals("ExMorRate"))
        {
            return 8;
        }
        if (strFieldName.equals("RetentRate"))
        {
            return 9;
        }
        if (strFieldName.equals("CommsionRate"))
        {
            return 10;
        }
        if (strFieldName.equals("CessPremMode"))
        {
            return 11;
        }
        if (strFieldName.equals("RiskAmntCalCode"))
        {
            return 12;
        }
        if (strFieldName.equals("CessPremCalCode"))
        {
            return 13;
        }
        if (strFieldName.equals("CessCommCalCode2"))
        {
            return 14;
        }
        if (strFieldName.equals("ReturnPayCalCode"))
        {
            return 15;
        }
        if (strFieldName.equals("NegoCalCode"))
        {
            return 16;
        }
        if (strFieldName.equals("GrpPerAmtLimit"))
        {
            return 17;
        }
        if (strFieldName.equals("GrpAverAmntLimit"))
        {
            return 18;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskSort";
                break;
            case 2:
                strFieldName = "RiskCalSort";
                break;
            case 3:
                strFieldName = "ReinsureCom";
                break;
            case 4:
                strFieldName = "ReinsurItem";
                break;
            case 5:
                strFieldName = "RetainLine";
                break;
            case 6:
                strFieldName = "CessionLimit";
                break;
            case 7:
                strFieldName = "CessionMode";
                break;
            case 8:
                strFieldName = "ExMorRate";
                break;
            case 9:
                strFieldName = "RetentRate";
                break;
            case 10:
                strFieldName = "CommsionRate";
                break;
            case 11:
                strFieldName = "CessPremMode";
                break;
            case 12:
                strFieldName = "RiskAmntCalCode";
                break;
            case 13:
                strFieldName = "CessPremCalCode";
                break;
            case 14:
                strFieldName = "CessCommCalCode2";
                break;
            case 15:
                strFieldName = "ReturnPayCalCode";
                break;
            case 16:
                strFieldName = "NegoCalCode";
                break;
            case 17:
                strFieldName = "GrpPerAmtLimit";
                break;
            case 18:
                strFieldName = "GrpAverAmntLimit";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskSort"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCalSort"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReinsureCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReinsurItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RetainLine"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CessionLimit"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CessionMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExMorRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RetentRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CommsionRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CessPremMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskAmntCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CessPremCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CessCommCalCode2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReturnPayCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NegoCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPerAmtLimit"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GrpAverAmntLimit"))
        {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
