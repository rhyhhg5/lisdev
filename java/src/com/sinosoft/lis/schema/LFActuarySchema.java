/*
 * <p>ClassName: LFActuarySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LFActuaryDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFActuarySchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 管理机构 */
    private String ManageCom;
    /** 准备金类型 */
    private String ReserveFlag;
    /** 未决赔款准备金类型 */
    private String ReportFlag;
    /** 团单个单标志 */
    private String PersonPolFlag;
    /** 准备金金额 */
    private double Reserve;
    /** 报表日期 */
    private Date ReportDate;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFActuarySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "RiskCode";
        pk[1] = "ManageCom";
        pk[2] = "ReserveFlag";
        pk[3] = "ReportFlag";
        pk[4] = "PersonPolFlag";
        pk[5] = "ReportDate";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getReserveFlag()
    {
        if (ReserveFlag != null && !ReserveFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReserveFlag = StrTool.unicodeToGBK(ReserveFlag);
        }
        return ReserveFlag;
    }

    public void setReserveFlag(String aReserveFlag)
    {
        ReserveFlag = aReserveFlag;
    }

    public String getReportFlag()
    {
        if (ReportFlag != null && !ReportFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ReportFlag = StrTool.unicodeToGBK(ReportFlag);
        }
        return ReportFlag;
    }

    public void setReportFlag(String aReportFlag)
    {
        ReportFlag = aReportFlag;
    }

    public String getPersonPolFlag()
    {
        if (PersonPolFlag != null && !PersonPolFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PersonPolFlag = StrTool.unicodeToGBK(PersonPolFlag);
        }
        return PersonPolFlag;
    }

    public void setPersonPolFlag(String aPersonPolFlag)
    {
        PersonPolFlag = aPersonPolFlag;
    }

    public double getReserve()
    {
        return Reserve;
    }

    public void setReserve(double aReserve)
    {
        Reserve = aReserve;
    }

    public void setReserve(String aReserve)
    {
        if (aReserve != null && !aReserve.equals(""))
        {
            Double tDouble = new Double(aReserve);
            double d = tDouble.doubleValue();
            Reserve = d;
        }
    }

    public String getReportDate()
    {
        if (ReportDate != null)
        {
            return fDate.getString(ReportDate);
        }
        else
        {
            return null;
        }
    }

    public void setReportDate(Date aReportDate)
    {
        ReportDate = aReportDate;
    }

    public void setReportDate(String aReportDate)
    {
        if (aReportDate != null && !aReportDate.equals(""))
        {
            ReportDate = fDate.getDate(aReportDate);
        }
        else
        {
            ReportDate = null;
        }
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LFActuarySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFActuarySchema aLFActuarySchema)
    {
        this.RiskCode = aLFActuarySchema.getRiskCode();
        this.ManageCom = aLFActuarySchema.getManageCom();
        this.ReserveFlag = aLFActuarySchema.getReserveFlag();
        this.ReportFlag = aLFActuarySchema.getReportFlag();
        this.PersonPolFlag = aLFActuarySchema.getPersonPolFlag();
        this.Reserve = aLFActuarySchema.getReserve();
        this.ReportDate = fDate.getDate(aLFActuarySchema.getReportDate());
        this.MakeDate = fDate.getDate(aLFActuarySchema.getMakeDate());
        this.MakeTime = aLFActuarySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLFActuarySchema.getModifyDate());
        this.ModifyTime = aLFActuarySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("ReserveFlag") == null)
            {
                this.ReserveFlag = null;
            }
            else
            {
                this.ReserveFlag = rs.getString("ReserveFlag").trim();
            }

            if (rs.getString("ReportFlag") == null)
            {
                this.ReportFlag = null;
            }
            else
            {
                this.ReportFlag = rs.getString("ReportFlag").trim();
            }

            if (rs.getString("PersonPolFlag") == null)
            {
                this.PersonPolFlag = null;
            }
            else
            {
                this.PersonPolFlag = rs.getString("PersonPolFlag").trim();
            }

            this.Reserve = rs.getDouble("Reserve");
            this.ReportDate = rs.getDate("ReportDate");
            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFActuarySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFActuarySchema getSchema()
    {
        LFActuarySchema aLFActuarySchema = new LFActuarySchema();
        aLFActuarySchema.setSchema(this);
        return aLFActuarySchema;
    }

    public LFActuaryDB getDB()
    {
        LFActuaryDB aDBOper = new LFActuaryDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFActuary描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReserveFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReportFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PersonPolFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Reserve) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ReportDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFActuary>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            ReserveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            ReportFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            PersonPolFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            Reserve = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            ReportDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFActuarySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("ReserveFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReserveFlag));
        }
        if (FCode.equals("ReportFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReportFlag));
        }
        if (FCode.equals("PersonPolFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PersonPolFlag));
        }
        if (FCode.equals("Reserve"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Reserve));
        }
        if (FCode.equals("ReportDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getReportDate()));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ReserveFlag);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ReportFlag);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PersonPolFlag);
                break;
            case 5:
                strFieldValue = String.valueOf(Reserve);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getReportDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("ReserveFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReserveFlag = FValue.trim();
            }
            else
            {
                ReserveFlag = null;
            }
        }
        if (FCode.equals("ReportFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReportFlag = FValue.trim();
            }
            else
            {
                ReportFlag = null;
            }
        }
        if (FCode.equals("PersonPolFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PersonPolFlag = FValue.trim();
            }
            else
            {
                PersonPolFlag = null;
            }
        }
        if (FCode.equals("Reserve"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Reserve = d;
            }
        }
        if (FCode.equals("ReportDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReportDate = fDate.getDate(FValue);
            }
            else
            {
                ReportDate = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFActuarySchema other = (LFActuarySchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && ManageCom.equals(other.getManageCom())
                && ReserveFlag.equals(other.getReserveFlag())
                && ReportFlag.equals(other.getReportFlag())
                && PersonPolFlag.equals(other.getPersonPolFlag())
                && Reserve == other.getReserve()
                && fDate.getString(ReportDate).equals(other.getReportDate())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 1;
        }
        if (strFieldName.equals("ReserveFlag"))
        {
            return 2;
        }
        if (strFieldName.equals("ReportFlag"))
        {
            return 3;
        }
        if (strFieldName.equals("PersonPolFlag"))
        {
            return 4;
        }
        if (strFieldName.equals("Reserve"))
        {
            return 5;
        }
        if (strFieldName.equals("ReportDate"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "ManageCom";
                break;
            case 2:
                strFieldName = "ReserveFlag";
                break;
            case 3:
                strFieldName = "ReportFlag";
                break;
            case 4:
                strFieldName = "PersonPolFlag";
                break;
            case 5:
                strFieldName = "Reserve";
                break;
            case 6:
                strFieldName = "ReportDate";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            case 9:
                strFieldName = "ModifyDate";
                break;
            case 10:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReserveFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReportFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PersonPolFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Reserve"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ReportDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
