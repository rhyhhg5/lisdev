/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDDrugstoreDB;

/*
 * <p>ClassName: LDDrugstoreSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 健康管理表修改_2005-12-06
 * @CreateDate：2005-12-06
 */
public class LDDrugstoreSchema implements Schema, Cloneable {
    // @Field
    /** 药店代码 */
    private String DrugStoreCode;
    /** 上级药店 */
    private String SuperDrugStoreCode;
    /** 药店名称 */
    private String DrugstoreName;
    /** 定点属性标识 */
    private String FixFlag;
    /** 社保定点机构标志 */
    private String CommunFixFlag;
    /** 所在地区代码 */
    private String AreaCode;
    /** 经济成分代码 */
    private String EconomElemenCode;
    /** 经营范围 */
    private String BusiScope;
    /** 建立日期 */
    private Date StartDate;
    /** 地址 */
    private String address;
    /** 邮编 */
    private String ZipCode;
    /** 联系电话 */
    private String Phone;
    /** 网址 */
    private String WebAddress;
    /** 传真 */
    private String Fax;
    /** 执业许可证号 */
    private String DrugstoreLicencNo;
    /** 开户银行 */
    private String bankCode;
    /** 户名 */
    private String AccName;
    /** 银行帐号 */
    private String bankAccNO;
    /** 负责人 */
    private String SatrapName;
    /** 联系人 */
    private String Linkman;
    /** 最近修改日期 */
    private Date LastModiDate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;

    public static final int FIELDNUM = 27; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDDrugstoreSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "DrugStoreCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDDrugstoreSchema cloned = (LDDrugstoreSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getDrugStoreCode() {
        return DrugStoreCode;
    }

    public void setDrugStoreCode(String aDrugStoreCode) {
        DrugStoreCode = aDrugStoreCode;
    }

    public String getSuperDrugStoreCode() {
        return SuperDrugStoreCode;
    }

    public void setSuperDrugStoreCode(String aSuperDrugStoreCode) {
        SuperDrugStoreCode = aSuperDrugStoreCode;
    }

    public String getDrugstoreName() {
        return DrugstoreName;
    }

    public void setDrugstoreName(String aDrugstoreName) {
        DrugstoreName = aDrugstoreName;
    }

    public String getFixFlag() {
        return FixFlag;
    }

    public void setFixFlag(String aFixFlag) {
        FixFlag = aFixFlag;
    }

    public String getCommunFixFlag() {
        return CommunFixFlag;
    }

    public void setCommunFixFlag(String aCommunFixFlag) {
        CommunFixFlag = aCommunFixFlag;
    }

    public String getAreaCode() {
        return AreaCode;
    }

    public void setAreaCode(String aAreaCode) {
        AreaCode = aAreaCode;
    }

    public String getEconomElemenCode() {
        return EconomElemenCode;
    }

    public void setEconomElemenCode(String aEconomElemenCode) {
        EconomElemenCode = aEconomElemenCode;
    }

    public String getBusiScope() {
        return BusiScope;
    }

    public void setBusiScope(String aBusiScope) {
        BusiScope = aBusiScope;
    }

    public String getStartDate() {
        if (StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }

    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else {
            StartDate = null;
        }
    }

    public String getaddress() {
        return address;
    }

    public void setaddress(String aaddress) {
        address = aaddress;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String aPhone) {
        Phone = aPhone;
    }

    public String getWebAddress() {
        return WebAddress;
    }

    public void setWebAddress(String aWebAddress) {
        WebAddress = aWebAddress;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String aFax) {
        Fax = aFax;
    }

    public String getDrugstoreLicencNo() {
        return DrugstoreLicencNo;
    }

    public void setDrugstoreLicencNo(String aDrugstoreLicencNo) {
        DrugstoreLicencNo = aDrugstoreLicencNo;
    }

    public String getbankCode() {
        return bankCode;
    }

    public void setbankCode(String abankCode) {
        bankCode = abankCode;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String aAccName) {
        AccName = aAccName;
    }

    public String getbankAccNO() {
        return bankAccNO;
    }

    public void setbankAccNO(String abankAccNO) {
        bankAccNO = abankAccNO;
    }

    public String getSatrapName() {
        return SatrapName;
    }

    public void setSatrapName(String aSatrapName) {
        SatrapName = aSatrapName;
    }

    public String getLinkman() {
        return Linkman;
    }

    public void setLinkman(String aLinkman) {
        Linkman = aLinkman;
    }

    public String getLastModiDate() {
        if (LastModiDate != null) {
            return fDate.getString(LastModiDate);
        } else {
            return null;
        }
    }

    public void setLastModiDate(Date aLastModiDate) {
        LastModiDate = aLastModiDate;
    }

    public void setLastModiDate(String aLastModiDate) {
        if (aLastModiDate != null && !aLastModiDate.equals("")) {
            LastModiDate = fDate.getDate(aLastModiDate);
        } else {
            LastModiDate = null;
        }
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    /**
     * 使用另外一个 LDDrugstoreSchema 对象给 Schema 赋值
     * @param: aLDDrugstoreSchema LDDrugstoreSchema
     **/
    public void setSchema(LDDrugstoreSchema aLDDrugstoreSchema) {
        this.DrugStoreCode = aLDDrugstoreSchema.getDrugStoreCode();
        this.SuperDrugStoreCode = aLDDrugstoreSchema.getSuperDrugStoreCode();
        this.DrugstoreName = aLDDrugstoreSchema.getDrugstoreName();
        this.FixFlag = aLDDrugstoreSchema.getFixFlag();
        this.CommunFixFlag = aLDDrugstoreSchema.getCommunFixFlag();
        this.AreaCode = aLDDrugstoreSchema.getAreaCode();
        this.EconomElemenCode = aLDDrugstoreSchema.getEconomElemenCode();
        this.BusiScope = aLDDrugstoreSchema.getBusiScope();
        this.StartDate = fDate.getDate(aLDDrugstoreSchema.getStartDate());
        this.address = aLDDrugstoreSchema.getaddress();
        this.ZipCode = aLDDrugstoreSchema.getZipCode();
        this.Phone = aLDDrugstoreSchema.getPhone();
        this.WebAddress = aLDDrugstoreSchema.getWebAddress();
        this.Fax = aLDDrugstoreSchema.getFax();
        this.DrugstoreLicencNo = aLDDrugstoreSchema.getDrugstoreLicencNo();
        this.bankCode = aLDDrugstoreSchema.getbankCode();
        this.AccName = aLDDrugstoreSchema.getAccName();
        this.bankAccNO = aLDDrugstoreSchema.getbankAccNO();
        this.SatrapName = aLDDrugstoreSchema.getSatrapName();
        this.Linkman = aLDDrugstoreSchema.getLinkman();
        this.LastModiDate = fDate.getDate(aLDDrugstoreSchema.getLastModiDate());
        this.Operator = aLDDrugstoreSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDDrugstoreSchema.getMakeDate());
        this.MakeTime = aLDDrugstoreSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDDrugstoreSchema.getModifyDate());
        this.ModifyTime = aLDDrugstoreSchema.getModifyTime();
        this.ManageCom = aLDDrugstoreSchema.getManageCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("DrugStoreCode") == null) {
                this.DrugStoreCode = null;
            } else {
                this.DrugStoreCode = rs.getString("DrugStoreCode").trim();
            }

            if (rs.getString("SuperDrugStoreCode") == null) {
                this.SuperDrugStoreCode = null;
            } else {
                this.SuperDrugStoreCode = rs.getString("SuperDrugStoreCode").
                                          trim();
            }

            if (rs.getString("DrugstoreName") == null) {
                this.DrugstoreName = null;
            } else {
                this.DrugstoreName = rs.getString("DrugstoreName").trim();
            }

            if (rs.getString("FixFlag") == null) {
                this.FixFlag = null;
            } else {
                this.FixFlag = rs.getString("FixFlag").trim();
            }

            if (rs.getString("CommunFixFlag") == null) {
                this.CommunFixFlag = null;
            } else {
                this.CommunFixFlag = rs.getString("CommunFixFlag").trim();
            }

            if (rs.getString("AreaCode") == null) {
                this.AreaCode = null;
            } else {
                this.AreaCode = rs.getString("AreaCode").trim();
            }

            if (rs.getString("EconomElemenCode") == null) {
                this.EconomElemenCode = null;
            } else {
                this.EconomElemenCode = rs.getString("EconomElemenCode").trim();
            }

            if (rs.getString("BusiScope") == null) {
                this.BusiScope = null;
            } else {
                this.BusiScope = rs.getString("BusiScope").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            if (rs.getString("address") == null) {
                this.address = null;
            } else {
                this.address = rs.getString("address").trim();
            }

            if (rs.getString("ZipCode") == null) {
                this.ZipCode = null;
            } else {
                this.ZipCode = rs.getString("ZipCode").trim();
            }

            if (rs.getString("Phone") == null) {
                this.Phone = null;
            } else {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("WebAddress") == null) {
                this.WebAddress = null;
            } else {
                this.WebAddress = rs.getString("WebAddress").trim();
            }

            if (rs.getString("Fax") == null) {
                this.Fax = null;
            } else {
                this.Fax = rs.getString("Fax").trim();
            }

            if (rs.getString("DrugstoreLicencNo") == null) {
                this.DrugstoreLicencNo = null;
            } else {
                this.DrugstoreLicencNo = rs.getString("DrugstoreLicencNo").trim();
            }

            if (rs.getString("bankCode") == null) {
                this.bankCode = null;
            } else {
                this.bankCode = rs.getString("bankCode").trim();
            }

            if (rs.getString("AccName") == null) {
                this.AccName = null;
            } else {
                this.AccName = rs.getString("AccName").trim();
            }

            if (rs.getString("bankAccNO") == null) {
                this.bankAccNO = null;
            } else {
                this.bankAccNO = rs.getString("bankAccNO").trim();
            }

            if (rs.getString("SatrapName") == null) {
                this.SatrapName = null;
            } else {
                this.SatrapName = rs.getString("SatrapName").trim();
            }

            if (rs.getString("Linkman") == null) {
                this.Linkman = null;
            } else {
                this.Linkman = rs.getString("Linkman").trim();
            }

            this.LastModiDate = rs.getDate("LastModiDate");
            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDDrugstore表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugstoreSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDDrugstoreSchema getSchema() {
        LDDrugstoreSchema aLDDrugstoreSchema = new LDDrugstoreSchema();
        aLDDrugstoreSchema.setSchema(this);
        return aLDDrugstoreSchema;
    }

    public LDDrugstoreDB getDB() {
        LDDrugstoreDB aDBOper = new LDDrugstoreDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDrugstore描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(DrugStoreCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SuperDrugStoreCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DrugstoreName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FixFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CommunFixFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AreaCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EconomElemenCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BusiScope));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(address));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZipCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WebAddress));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Fax));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DrugstoreLicencNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(bankCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(bankAccNO));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SatrapName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Linkman));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(LastModiDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDrugstore>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            DrugStoreCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            SuperDrugStoreCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                2, SysConst.PACKAGESPILTER);
            DrugstoreName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            FixFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            CommunFixFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            AreaCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            EconomElemenCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              7, SysConst.PACKAGESPILTER);
            BusiScope = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                   SysConst.PACKAGESPILTER);
            WebAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                 SysConst.PACKAGESPILTER);
            DrugstoreLicencNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               15, SysConst.PACKAGESPILTER);
            bankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                     SysConst.PACKAGESPILTER);
            bankAccNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                       SysConst.PACKAGESPILTER);
            SatrapName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
            Linkman = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                     SysConst.PACKAGESPILTER);
            LastModiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 25, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                       SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDrugstoreSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("DrugStoreCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugStoreCode));
        }
        if (FCode.equals("SuperDrugStoreCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SuperDrugStoreCode));
        }
        if (FCode.equals("DrugstoreName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugstoreName));
        }
        if (FCode.equals("FixFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FixFlag));
        }
        if (FCode.equals("CommunFixFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CommunFixFlag));
        }
        if (FCode.equals("AreaCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaCode));
        }
        if (FCode.equals("EconomElemenCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EconomElemenCode));
        }
        if (FCode.equals("BusiScope")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BusiScope));
        }
        if (FCode.equals("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("address")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(address));
        }
        if (FCode.equals("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equals("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equals("WebAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WebAddress));
        }
        if (FCode.equals("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equals("DrugstoreLicencNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrugstoreLicencNo));
        }
        if (FCode.equals("bankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bankCode));
        }
        if (FCode.equals("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equals("bankAccNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bankAccNO));
        }
        if (FCode.equals("SatrapName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SatrapName));
        }
        if (FCode.equals("Linkman")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Linkman));
        }
        if (FCode.equals("LastModiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getLastModiDate()));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(DrugStoreCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(SuperDrugStoreCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(DrugstoreName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(FixFlag);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(CommunFixFlag);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(AreaCode);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(EconomElemenCode);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(BusiScope);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStartDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(address);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ZipCode);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Phone);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(WebAddress);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(Fax);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(DrugstoreLicencNo);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(bankCode);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(AccName);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(bankAccNO);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(SatrapName);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(Linkman);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getLastModiDate()));
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 25:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 26:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("DrugStoreCode")) {
            if (FValue != null && !FValue.equals("")) {
                DrugStoreCode = FValue.trim();
            } else {
                DrugStoreCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("SuperDrugStoreCode")) {
            if (FValue != null && !FValue.equals("")) {
                SuperDrugStoreCode = FValue.trim();
            } else {
                SuperDrugStoreCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("DrugstoreName")) {
            if (FValue != null && !FValue.equals("")) {
                DrugstoreName = FValue.trim();
            } else {
                DrugstoreName = null;
            }
        }
        if (FCode.equalsIgnoreCase("FixFlag")) {
            if (FValue != null && !FValue.equals("")) {
                FixFlag = FValue.trim();
            } else {
                FixFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("CommunFixFlag")) {
            if (FValue != null && !FValue.equals("")) {
                CommunFixFlag = FValue.trim();
            } else {
                CommunFixFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("AreaCode")) {
            if (FValue != null && !FValue.equals("")) {
                AreaCode = FValue.trim();
            } else {
                AreaCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("EconomElemenCode")) {
            if (FValue != null && !FValue.equals("")) {
                EconomElemenCode = FValue.trim();
            } else {
                EconomElemenCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BusiScope")) {
            if (FValue != null && !FValue.equals("")) {
                BusiScope = FValue.trim();
            } else {
                BusiScope = null;
            }
        }
        if (FCode.equalsIgnoreCase("StartDate")) {
            if (FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate(FValue);
            } else {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("address")) {
            if (FValue != null && !FValue.equals("")) {
                address = FValue.trim();
            } else {
                address = null;
            }
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if (FValue != null && !FValue.equals("")) {
                ZipCode = FValue.trim();
            } else {
                ZipCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if (FValue != null && !FValue.equals("")) {
                Phone = FValue.trim();
            } else {
                Phone = null;
            }
        }
        if (FCode.equalsIgnoreCase("WebAddress")) {
            if (FValue != null && !FValue.equals("")) {
                WebAddress = FValue.trim();
            } else {
                WebAddress = null;
            }
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if (FValue != null && !FValue.equals("")) {
                Fax = FValue.trim();
            } else {
                Fax = null;
            }
        }
        if (FCode.equalsIgnoreCase("DrugstoreLicencNo")) {
            if (FValue != null && !FValue.equals("")) {
                DrugstoreLicencNo = FValue.trim();
            } else {
                DrugstoreLicencNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("bankCode")) {
            if (FValue != null && !FValue.equals("")) {
                bankCode = FValue.trim();
            } else {
                bankCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if (FValue != null && !FValue.equals("")) {
                AccName = FValue.trim();
            } else {
                AccName = null;
            }
        }
        if (FCode.equalsIgnoreCase("bankAccNO")) {
            if (FValue != null && !FValue.equals("")) {
                bankAccNO = FValue.trim();
            } else {
                bankAccNO = null;
            }
        }
        if (FCode.equalsIgnoreCase("SatrapName")) {
            if (FValue != null && !FValue.equals("")) {
                SatrapName = FValue.trim();
            } else {
                SatrapName = null;
            }
        }
        if (FCode.equalsIgnoreCase("Linkman")) {
            if (FValue != null && !FValue.equals("")) {
                Linkman = FValue.trim();
            } else {
                Linkman = null;
            }
        }
        if (FCode.equalsIgnoreCase("LastModiDate")) {
            if (FValue != null && !FValue.equals("")) {
                LastModiDate = fDate.getDate(FValue);
            } else {
                LastModiDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDDrugstoreSchema other = (LDDrugstoreSchema) otherObject;
        return
                DrugStoreCode.equals(other.getDrugStoreCode())
                && SuperDrugStoreCode.equals(other.getSuperDrugStoreCode())
                && DrugstoreName.equals(other.getDrugstoreName())
                && FixFlag.equals(other.getFixFlag())
                && CommunFixFlag.equals(other.getCommunFixFlag())
                && AreaCode.equals(other.getAreaCode())
                && EconomElemenCode.equals(other.getEconomElemenCode())
                && BusiScope.equals(other.getBusiScope())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && address.equals(other.getaddress())
                && ZipCode.equals(other.getZipCode())
                && Phone.equals(other.getPhone())
                && WebAddress.equals(other.getWebAddress())
                && Fax.equals(other.getFax())
                && DrugstoreLicencNo.equals(other.getDrugstoreLicencNo())
                && bankCode.equals(other.getbankCode())
                && AccName.equals(other.getAccName())
                && bankAccNO.equals(other.getbankAccNO())
                && SatrapName.equals(other.getSatrapName())
                && Linkman.equals(other.getLinkman())
                && fDate.getString(LastModiDate).equals(other.getLastModiDate())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ManageCom.equals(other.getManageCom());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("DrugStoreCode")) {
            return 0;
        }
        if (strFieldName.equals("SuperDrugStoreCode")) {
            return 1;
        }
        if (strFieldName.equals("DrugstoreName")) {
            return 2;
        }
        if (strFieldName.equals("FixFlag")) {
            return 3;
        }
        if (strFieldName.equals("CommunFixFlag")) {
            return 4;
        }
        if (strFieldName.equals("AreaCode")) {
            return 5;
        }
        if (strFieldName.equals("EconomElemenCode")) {
            return 6;
        }
        if (strFieldName.equals("BusiScope")) {
            return 7;
        }
        if (strFieldName.equals("StartDate")) {
            return 8;
        }
        if (strFieldName.equals("address")) {
            return 9;
        }
        if (strFieldName.equals("ZipCode")) {
            return 10;
        }
        if (strFieldName.equals("Phone")) {
            return 11;
        }
        if (strFieldName.equals("WebAddress")) {
            return 12;
        }
        if (strFieldName.equals("Fax")) {
            return 13;
        }
        if (strFieldName.equals("DrugstoreLicencNo")) {
            return 14;
        }
        if (strFieldName.equals("bankCode")) {
            return 15;
        }
        if (strFieldName.equals("AccName")) {
            return 16;
        }
        if (strFieldName.equals("bankAccNO")) {
            return 17;
        }
        if (strFieldName.equals("SatrapName")) {
            return 18;
        }
        if (strFieldName.equals("Linkman")) {
            return 19;
        }
        if (strFieldName.equals("LastModiDate")) {
            return 20;
        }
        if (strFieldName.equals("Operator")) {
            return 21;
        }
        if (strFieldName.equals("MakeDate")) {
            return 22;
        }
        if (strFieldName.equals("MakeTime")) {
            return 23;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 24;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 25;
        }
        if (strFieldName.equals("ManageCom")) {
            return 26;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "DrugStoreCode";
            break;
        case 1:
            strFieldName = "SuperDrugStoreCode";
            break;
        case 2:
            strFieldName = "DrugstoreName";
            break;
        case 3:
            strFieldName = "FixFlag";
            break;
        case 4:
            strFieldName = "CommunFixFlag";
            break;
        case 5:
            strFieldName = "AreaCode";
            break;
        case 6:
            strFieldName = "EconomElemenCode";
            break;
        case 7:
            strFieldName = "BusiScope";
            break;
        case 8:
            strFieldName = "StartDate";
            break;
        case 9:
            strFieldName = "address";
            break;
        case 10:
            strFieldName = "ZipCode";
            break;
        case 11:
            strFieldName = "Phone";
            break;
        case 12:
            strFieldName = "WebAddress";
            break;
        case 13:
            strFieldName = "Fax";
            break;
        case 14:
            strFieldName = "DrugstoreLicencNo";
            break;
        case 15:
            strFieldName = "bankCode";
            break;
        case 16:
            strFieldName = "AccName";
            break;
        case 17:
            strFieldName = "bankAccNO";
            break;
        case 18:
            strFieldName = "SatrapName";
            break;
        case 19:
            strFieldName = "Linkman";
            break;
        case 20:
            strFieldName = "LastModiDate";
            break;
        case 21:
            strFieldName = "Operator";
            break;
        case 22:
            strFieldName = "MakeDate";
            break;
        case 23:
            strFieldName = "MakeTime";
            break;
        case 24:
            strFieldName = "ModifyDate";
            break;
        case 25:
            strFieldName = "ModifyTime";
            break;
        case 26:
            strFieldName = "ManageCom";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("DrugStoreCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SuperDrugStoreCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrugstoreName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FixFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CommunFixFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EconomElemenCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BusiScope")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("address")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZipCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WebAddress")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Fax")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrugstoreLicencNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bankCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bankAccNO")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SatrapName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Linkman")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LastModiDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 21:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 22:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 23:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 24:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 25:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 26:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
