/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLInqFeeDB;

/*
 * <p>ClassName: LLInqFeeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-09-25
 */
public class LLInqFeeSchema implements Schema, Cloneable
{
	// @Field
	/** 对应号码 */
	private String OtherNo;
	/** 其他对应号码类型 */
	private String OtherNoType;
	/** 调查号 */
	private String SurveyNo;
	/** 调查机构 */
	private String InqDept;
	/** 调查次数 */
	private int ContSN;
	/** 费用项目 */
	private String FeeItem;
	/** 发生时间 */
	private Date FeeDate;
	/** 金额 */
	private double FeeSum;
	/** 备注 */
	private String Remark;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 业务审核人 */
	private String Confer;
	/** 财务审核人 */
	private String FinInspector;
	/** 审核状态 */
	private String UWState;

	public static final int FIELDNUM = 17;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLInqFeeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "SurveyNo";
		pk[1] = "InqDept";
		pk[2] = "ContSN";
		pk[3] = "FeeItem";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LLInqFeeSchema cloned = (LLInqFeeSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getOtherNo()
	{
		return OtherNo;
	}
	public void setOtherNo(String aOtherNo)
	{
            OtherNo = aOtherNo;
	}
	public String getOtherNoType()
	{
		return OtherNoType;
	}
	public void setOtherNoType(String aOtherNoType)
	{
            OtherNoType = aOtherNoType;
	}
	public String getSurveyNo()
	{
		return SurveyNo;
	}
	public void setSurveyNo(String aSurveyNo)
	{
            SurveyNo = aSurveyNo;
	}
	public String getInqDept()
	{
		return InqDept;
	}
	public void setInqDept(String aInqDept)
	{
            InqDept = aInqDept;
	}
	public int getContSN()
	{
		return ContSN;
	}
	public void setContSN(int aContSN)
	{
            ContSN = aContSN;
	}
	public void setContSN(String aContSN)
	{
		if (aContSN != null && !aContSN.equals(""))
		{
			Integer tInteger = new Integer(aContSN);
			int i = tInteger.intValue();
			ContSN = i;
		}
	}

	public String getFeeItem()
	{
		return FeeItem;
	}
	public void setFeeItem(String aFeeItem)
	{
            FeeItem = aFeeItem;
	}
	public String getFeeDate()
	{
		if( FeeDate != null )
			return fDate.getString(FeeDate);
		else
			return null;
	}
	public void setFeeDate(Date aFeeDate)
	{
            FeeDate = aFeeDate;
	}
	public void setFeeDate(String aFeeDate)
	{
		if (aFeeDate != null && !aFeeDate.equals("") )
		{
			FeeDate = fDate.getDate( aFeeDate );
		}
		else
			FeeDate = null;
	}

	public double getFeeSum()
	{
		return FeeSum;
	}
	public void setFeeSum(double aFeeSum)
	{
            FeeSum = Arith.round(aFeeSum,2);
	}
	public void setFeeSum(String aFeeSum)
	{
		if (aFeeSum != null && !aFeeSum.equals(""))
		{
			Double tDouble = new Double(aFeeSum);
			double d = tDouble.doubleValue();
                FeeSum = Arith.round(d,2);
		}
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
            Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getConfer()
	{
		return Confer;
	}
	public void setConfer(String aConfer)
	{
            Confer = aConfer;
	}
	public String getFinInspector()
	{
		return FinInspector;
	}
	public void setFinInspector(String aFinInspector)
	{
            FinInspector = aFinInspector;
	}
	public String getUWState()
	{
		return UWState;
	}
	public void setUWState(String aUWState)
	{
            UWState = aUWState;
	}

	/**
	* 使用另外一个 LLInqFeeSchema 对象给 Schema 赋值
	* @param: aLLInqFeeSchema LLInqFeeSchema
	**/
	public void setSchema(LLInqFeeSchema aLLInqFeeSchema)
	{
		this.OtherNo = aLLInqFeeSchema.getOtherNo();
		this.OtherNoType = aLLInqFeeSchema.getOtherNoType();
		this.SurveyNo = aLLInqFeeSchema.getSurveyNo();
		this.InqDept = aLLInqFeeSchema.getInqDept();
		this.ContSN = aLLInqFeeSchema.getContSN();
		this.FeeItem = aLLInqFeeSchema.getFeeItem();
		this.FeeDate = fDate.getDate( aLLInqFeeSchema.getFeeDate());
		this.FeeSum = aLLInqFeeSchema.getFeeSum();
		this.Remark = aLLInqFeeSchema.getRemark();
		this.Operator = aLLInqFeeSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLInqFeeSchema.getMakeDate());
		this.MakeTime = aLLInqFeeSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLInqFeeSchema.getModifyDate());
		this.ModifyTime = aLLInqFeeSchema.getModifyTime();
		this.Confer = aLLInqFeeSchema.getConfer();
		this.FinInspector = aLLInqFeeSchema.getFinInspector();
		this.UWState = aLLInqFeeSchema.getUWState();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("OtherNo") == null )
				this.OtherNo = null;
			else
				this.OtherNo = rs.getString("OtherNo").trim();

			if( rs.getString("OtherNoType") == null )
				this.OtherNoType = null;
			else
				this.OtherNoType = rs.getString("OtherNoType").trim();

			if( rs.getString("SurveyNo") == null )
				this.SurveyNo = null;
			else
				this.SurveyNo = rs.getString("SurveyNo").trim();

			if( rs.getString("InqDept") == null )
				this.InqDept = null;
			else
				this.InqDept = rs.getString("InqDept").trim();

			this.ContSN = rs.getInt("ContSN");
			if( rs.getString("FeeItem") == null )
				this.FeeItem = null;
			else
				this.FeeItem = rs.getString("FeeItem").trim();

			this.FeeDate = rs.getDate("FeeDate");
			this.FeeSum = rs.getDouble("FeeSum");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Confer") == null )
				this.Confer = null;
			else
				this.Confer = rs.getString("Confer").trim();

			if( rs.getString("FinInspector") == null )
				this.FinInspector = null;
			else
				this.FinInspector = rs.getString("FinInspector").trim();

			if( rs.getString("UWState") == null )
				this.UWState = null;
			else
				this.UWState = rs.getString("UWState").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLInqFee表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLInqFeeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLInqFeeSchema getSchema()
	{
		LLInqFeeSchema aLLInqFeeSchema = new LLInqFeeSchema();
		aLLInqFeeSchema.setSchema(this);
		return aLLInqFeeSchema;
	}

	public LLInqFeeDB getDB()
	{
		LLInqFeeDB aDBOper = new LLInqFeeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqFee描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OtherNoType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SurveyNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InqDept)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ContSN));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FeeItem)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( FeeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(FeeSum));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Confer)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FinInspector)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(UWState));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLInqFee>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			OtherNoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SurveyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			InqDept = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ContSN= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).intValue();
			FeeItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			FeeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			FeeSum = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Confer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			FinInspector = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			UWState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLInqFeeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("OtherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
		}
		if (FCode.equals("OtherNoType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNoType));
		}
		if (FCode.equals("SurveyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyNo));
		}
		if (FCode.equals("InqDept"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InqDept));
		}
		if (FCode.equals("ContSN"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContSN));
		}
		if (FCode.equals("FeeItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeItem));
		}
		if (FCode.equals("FeeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
		}
		if (FCode.equals("FeeSum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeSum));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Confer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Confer));
		}
		if (FCode.equals("FinInspector"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinInspector));
		}
		if (FCode.equals("UWState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWState));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(OtherNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(OtherNoType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SurveyNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(InqDept);
				break;
			case 4:
				strFieldValue = String.valueOf(ContSN);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(FeeItem);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
				break;
			case 7:
				strFieldValue = String.valueOf(FeeSum);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Confer);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(FinInspector);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(UWState);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("OtherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNo = FValue.trim();
			}
			else
				OtherNo = null;
		}
		if (FCode.equalsIgnoreCase("OtherNoType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNoType = FValue.trim();
			}
			else
				OtherNoType = null;
		}
		if (FCode.equalsIgnoreCase("SurveyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SurveyNo = FValue.trim();
			}
			else
				SurveyNo = null;
		}
		if (FCode.equalsIgnoreCase("InqDept"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InqDept = FValue.trim();
			}
			else
				InqDept = null;
		}
		if (FCode.equalsIgnoreCase("ContSN"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ContSN = i;
			}
		}
		if (FCode.equalsIgnoreCase("FeeItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeItem = FValue.trim();
			}
			else
				FeeItem = null;
		}
		if (FCode.equalsIgnoreCase("FeeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FeeDate = fDate.getDate( FValue );
			}
			else
				FeeDate = null;
		}
		if (FCode.equalsIgnoreCase("FeeSum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				FeeSum = d;
			}
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Confer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Confer = FValue.trim();
			}
			else
				Confer = null;
		}
		if (FCode.equalsIgnoreCase("FinInspector"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinInspector = FValue.trim();
			}
			else
				FinInspector = null;
		}
		if (FCode.equalsIgnoreCase("UWState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWState = FValue.trim();
			}
			else
				UWState = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLInqFeeSchema other = (LLInqFeeSchema)otherObject;
		return
			OtherNo.equals(other.getOtherNo())
			&& OtherNoType.equals(other.getOtherNoType())
			&& SurveyNo.equals(other.getSurveyNo())
			&& InqDept.equals(other.getInqDept())
			&& ContSN == other.getContSN()
			&& FeeItem.equals(other.getFeeItem())
			&& fDate.getString(FeeDate).equals(other.getFeeDate())
			&& FeeSum == other.getFeeSum()
			&& Remark.equals(other.getRemark())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& Confer.equals(other.getConfer())
			&& FinInspector.equals(other.getFinInspector())
			&& UWState.equals(other.getUWState());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("OtherNo") ) {
			return 0;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return 1;
		}
		if( strFieldName.equals("SurveyNo") ) {
			return 2;
		}
		if( strFieldName.equals("InqDept") ) {
			return 3;
		}
		if( strFieldName.equals("ContSN") ) {
			return 4;
		}
		if( strFieldName.equals("FeeItem") ) {
			return 5;
		}
		if( strFieldName.equals("FeeDate") ) {
			return 6;
		}
		if( strFieldName.equals("FeeSum") ) {
			return 7;
		}
		if( strFieldName.equals("Remark") ) {
			return 8;
		}
		if( strFieldName.equals("Operator") ) {
			return 9;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 13;
		}
		if( strFieldName.equals("Confer") ) {
			return 14;
		}
		if( strFieldName.equals("FinInspector") ) {
			return 15;
		}
		if( strFieldName.equals("UWState") ) {
			return 16;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "OtherNo";
				break;
			case 1:
				strFieldName = "OtherNoType";
				break;
			case 2:
				strFieldName = "SurveyNo";
				break;
			case 3:
				strFieldName = "InqDept";
				break;
			case 4:
				strFieldName = "ContSN";
				break;
			case 5:
				strFieldName = "FeeItem";
				break;
			case 6:
				strFieldName = "FeeDate";
				break;
			case 7:
				strFieldName = "FeeSum";
				break;
			case 8:
				strFieldName = "Remark";
				break;
			case 9:
				strFieldName = "Operator";
				break;
			case 10:
				strFieldName = "MakeDate";
				break;
			case 11:
				strFieldName = "MakeTime";
				break;
			case 12:
				strFieldName = "ModifyDate";
				break;
			case 13:
				strFieldName = "ModifyTime";
				break;
			case 14:
				strFieldName = "Confer";
				break;
			case 15:
				strFieldName = "FinInspector";
				break;
			case 16:
				strFieldName = "UWState";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("OtherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNoType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SurveyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InqDept") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContSN") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("FeeItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("FeeSum") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Confer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinInspector") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWState") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_INT;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
