/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIPlaceRentInfoBDB;

/*
 * <p>ClassName: LIPlaceRentInfoBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 职场管理
 * @CreateDate：2012-08-22
 */
public class LIPlaceRentInfoBSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String Serialno;
	/** 职场编码 */
	private String Placeno;
	/** 操作类型 */
	private String Dealtype;
	/** 操作状态 */
	private String Dealstate;
	/** 关联职场编码 */
	private String Placenorefer;
	/** 公司代码 */
	private String Managecom;
	/** 准租批文号 */
	private String Appno;
	/** 状态 */
	private String State;
	/** 锁定状态 */
	private String Islock;
	/** 公司级别 */
	private String Comlevel;
	/** 机构名称 */
	private String Comname;
	/** 租期起期 */
	private Date Begindate;
	/** 租期止期 */
	private Date Enddate;
	/** 实际租期止期 */
	private Date Actualenddate;
	/** 地址 */
	private String Address;
	/** 出租方 */
	private String Lessor;
	/** 房屋所有权证号 */
	private String Houseno;
	/** 所属省份 */
	private String Province;
	/** 所属地市 */
	private String City;
	/** 是否办理租赁登记 */
	private String Isrentregister;
	/** 出租方是否为关联方 */
	private String Islessorassociate;
	/** 出让方是否具有出租房屋的权利 */
	private String Islessorright;
	/** 租赁标的所占用的土地性质是否出让 */
	private String Ishouserent;
	/** 租赁是否用于自用办公 */
	private String Isplaceoffice;
	/** 房屋所占用的土地是否为集体用地 */
	private String Ishousegroup;
	/** 房屋所占用的土地是否为农用地 */
	private String Ishousefarm;
	/** 个险渠道面积 */
	private double Sigarea;
	/** 团险渠道面积 */
	private double Grparea;
	/** 银保渠道面积 */
	private double Bankarea;
	/** 健康管理面积 */
	private double Healtharea;
	/** 运营客服面积 */
	private double Servicearea;
	/** 联合办公面积 */
	private double Jointarea;
	/** 总经理室 */
	private double Manageroffice;
	/** 人事行政部 */
	private double Departpersno;
	/** 计划财务部 */
	private double Planfinance;
	/** 公共区域 */
	private double Publicarea;
	/** 其他面积 */
	private double Otherarea;
	/** 说明 */
	private String Explanation;
	/** 保证金金额 */
	private double Marginfee;
	/** 保证金支付日期 */
	private Date Marginpaydate;
	/** 保证金退还日期 */
	private Date Marginbackdate;
	/** 合同约定违约金 */
	private double Penaltyfee;
	/** 备注 */
	private String Remake;
	/** 文件名称 */
	private String Filename;
	/** 文件路径 */
	private String Filepath;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date Makedate;
	/** 入机时间 */
	private String Maketime;
	/** 修改日期 */
	private Date Modifydate;
	/** 修改时间 */
	private String Modifytime;
	/** 备用字符串属性1 */
	private String Standbystring1;
	/** 备用字符串属性2 */
	private String Standbystring2;
	/** 备用字符串属性3 */
	private String Standbystring3;
	/** 备用数字属性1 */
	private double Standbynum1;
	/** 备用数字属性2 */
	private double Standbynum2;
	/** 备用数字属性3 */
	private double Standbynum3;
	/** 备用日期属性1 */
	private Date Standbydate1;
	/** 备用日期属性2 */
	private Date Standbydate2;

	public static final int FIELDNUM = 58;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIPlaceRentInfoBSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Serialno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LIPlaceRentInfoBSchema cloned = (LIPlaceRentInfoBSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialno()
	{
		return Serialno;
	}
	public void setSerialno(String aSerialno)
	{
		Serialno = aSerialno;
	}
	public String getPlaceno()
	{
		return Placeno;
	}
	public void setPlaceno(String aPlaceno)
	{
		Placeno = aPlaceno;
	}
	public String getDealtype()
	{
		return Dealtype;
	}
	public void setDealtype(String aDealtype)
	{
		Dealtype = aDealtype;
	}
	public String getDealstate()
	{
		return Dealstate;
	}
	public void setDealstate(String aDealstate)
	{
		Dealstate = aDealstate;
	}
	public String getPlacenorefer()
	{
		return Placenorefer;
	}
	public void setPlacenorefer(String aPlacenorefer)
	{
		Placenorefer = aPlacenorefer;
	}
	public String getManagecom()
	{
		return Managecom;
	}
	public void setManagecom(String aManagecom)
	{
		Managecom = aManagecom;
	}
	public String getAppno()
	{
		return Appno;
	}
	public void setAppno(String aAppno)
	{
		Appno = aAppno;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getIslock()
	{
		return Islock;
	}
	public void setIslock(String aIslock)
	{
		Islock = aIslock;
	}
	public String getComlevel()
	{
		return Comlevel;
	}
	public void setComlevel(String aComlevel)
	{
		Comlevel = aComlevel;
	}
	public String getComname()
	{
		return Comname;
	}
	public void setComname(String aComname)
	{
		Comname = aComname;
	}
	public String getBegindate()
	{
		if( Begindate != null )
			return fDate.getString(Begindate);
		else
			return null;
	}
	public void setBegindate(Date aBegindate)
	{
		Begindate = aBegindate;
	}
	public void setBegindate(String aBegindate)
	{
		if (aBegindate != null && !aBegindate.equals("") )
		{
			Begindate = fDate.getDate( aBegindate );
		}
		else
			Begindate = null;
	}

	public String getEnddate()
	{
		if( Enddate != null )
			return fDate.getString(Enddate);
		else
			return null;
	}
	public void setEnddate(Date aEnddate)
	{
		Enddate = aEnddate;
	}
	public void setEnddate(String aEnddate)
	{
		if (aEnddate != null && !aEnddate.equals("") )
		{
			Enddate = fDate.getDate( aEnddate );
		}
		else
			Enddate = null;
	}

	public String getActualenddate()
	{
		if( Actualenddate != null )
			return fDate.getString(Actualenddate);
		else
			return null;
	}
	public void setActualenddate(Date aActualenddate)
	{
		Actualenddate = aActualenddate;
	}
	public void setActualenddate(String aActualenddate)
	{
		if (aActualenddate != null && !aActualenddate.equals("") )
		{
			Actualenddate = fDate.getDate( aActualenddate );
		}
		else
			Actualenddate = null;
	}

	public String getAddress()
	{
		return Address;
	}
	public void setAddress(String aAddress)
	{
		Address = aAddress;
	}
	public String getLessor()
	{
		return Lessor;
	}
	public void setLessor(String aLessor)
	{
		Lessor = aLessor;
	}
	public String getHouseno()
	{
		return Houseno;
	}
	public void setHouseno(String aHouseno)
	{
		Houseno = aHouseno;
	}
	public String getProvince()
	{
		return Province;
	}
	public void setProvince(String aProvince)
	{
		Province = aProvince;
	}
	public String getCity()
	{
		return City;
	}
	public void setCity(String aCity)
	{
		City = aCity;
	}
	public String getIsrentregister()
	{
		return Isrentregister;
	}
	public void setIsrentregister(String aIsrentregister)
	{
		Isrentregister = aIsrentregister;
	}
	public String getIslessorassociate()
	{
		return Islessorassociate;
	}
	public void setIslessorassociate(String aIslessorassociate)
	{
		Islessorassociate = aIslessorassociate;
	}
	public String getIslessorright()
	{
		return Islessorright;
	}
	public void setIslessorright(String aIslessorright)
	{
		Islessorright = aIslessorright;
	}
	public String getIshouserent()
	{
		return Ishouserent;
	}
	public void setIshouserent(String aIshouserent)
	{
		Ishouserent = aIshouserent;
	}
	public String getIsplaceoffice()
	{
		return Isplaceoffice;
	}
	public void setIsplaceoffice(String aIsplaceoffice)
	{
		Isplaceoffice = aIsplaceoffice;
	}
	public String getIshousegroup()
	{
		return Ishousegroup;
	}
	public void setIshousegroup(String aIshousegroup)
	{
		Ishousegroup = aIshousegroup;
	}
	public String getIshousefarm()
	{
		return Ishousefarm;
	}
	public void setIshousefarm(String aIshousefarm)
	{
		Ishousefarm = aIshousefarm;
	}
	public double getSigarea()
	{
		return Sigarea;
	}
	public void setSigarea(double aSigarea)
	{
		Sigarea = Arith.round(aSigarea,2);
	}
	public void setSigarea(String aSigarea)
	{
		if (aSigarea != null && !aSigarea.equals(""))
		{
			Double tDouble = new Double(aSigarea);
			double d = tDouble.doubleValue();
                Sigarea = Arith.round(d,2);
		}
	}

	public double getGrparea()
	{
		return Grparea;
	}
	public void setGrparea(double aGrparea)
	{
		Grparea = Arith.round(aGrparea,2);
	}
	public void setGrparea(String aGrparea)
	{
		if (aGrparea != null && !aGrparea.equals(""))
		{
			Double tDouble = new Double(aGrparea);
			double d = tDouble.doubleValue();
                Grparea = Arith.round(d,2);
		}
	}

	public double getBankarea()
	{
		return Bankarea;
	}
	public void setBankarea(double aBankarea)
	{
		Bankarea = Arith.round(aBankarea,2);
	}
	public void setBankarea(String aBankarea)
	{
		if (aBankarea != null && !aBankarea.equals(""))
		{
			Double tDouble = new Double(aBankarea);
			double d = tDouble.doubleValue();
                Bankarea = Arith.round(d,2);
		}
	}

	public double getHealtharea()
	{
		return Healtharea;
	}
	public void setHealtharea(double aHealtharea)
	{
		Healtharea = Arith.round(aHealtharea,2);
	}
	public void setHealtharea(String aHealtharea)
	{
		if (aHealtharea != null && !aHealtharea.equals(""))
		{
			Double tDouble = new Double(aHealtharea);
			double d = tDouble.doubleValue();
                Healtharea = Arith.round(d,2);
		}
	}

	public double getServicearea()
	{
		return Servicearea;
	}
	public void setServicearea(double aServicearea)
	{
		Servicearea = Arith.round(aServicearea,2);
	}
	public void setServicearea(String aServicearea)
	{
		if (aServicearea != null && !aServicearea.equals(""))
		{
			Double tDouble = new Double(aServicearea);
			double d = tDouble.doubleValue();
                Servicearea = Arith.round(d,2);
		}
	}

	public double getJointarea()
	{
		return Jointarea;
	}
	public void setJointarea(double aJointarea)
	{
		Jointarea = Arith.round(aJointarea,2);
	}
	public void setJointarea(String aJointarea)
	{
		if (aJointarea != null && !aJointarea.equals(""))
		{
			Double tDouble = new Double(aJointarea);
			double d = tDouble.doubleValue();
                Jointarea = Arith.round(d,2);
		}
	}

	public double getManageroffice()
	{
		return Manageroffice;
	}
	public void setManageroffice(double aManageroffice)
	{
		Manageroffice = Arith.round(aManageroffice,2);
	}
	public void setManageroffice(String aManageroffice)
	{
		if (aManageroffice != null && !aManageroffice.equals(""))
		{
			Double tDouble = new Double(aManageroffice);
			double d = tDouble.doubleValue();
                Manageroffice = Arith.round(d,2);
		}
	}

	public double getDepartpersno()
	{
		return Departpersno;
	}
	public void setDepartpersno(double aDepartpersno)
	{
		Departpersno = Arith.round(aDepartpersno,2);
	}
	public void setDepartpersno(String aDepartpersno)
	{
		if (aDepartpersno != null && !aDepartpersno.equals(""))
		{
			Double tDouble = new Double(aDepartpersno);
			double d = tDouble.doubleValue();
                Departpersno = Arith.round(d,2);
		}
	}

	public double getPlanfinance()
	{
		return Planfinance;
	}
	public void setPlanfinance(double aPlanfinance)
	{
		Planfinance = Arith.round(aPlanfinance,2);
	}
	public void setPlanfinance(String aPlanfinance)
	{
		if (aPlanfinance != null && !aPlanfinance.equals(""))
		{
			Double tDouble = new Double(aPlanfinance);
			double d = tDouble.doubleValue();
                Planfinance = Arith.round(d,2);
		}
	}

	public double getPublicarea()
	{
		return Publicarea;
	}
	public void setPublicarea(double aPublicarea)
	{
		Publicarea = Arith.round(aPublicarea,2);
	}
	public void setPublicarea(String aPublicarea)
	{
		if (aPublicarea != null && !aPublicarea.equals(""))
		{
			Double tDouble = new Double(aPublicarea);
			double d = tDouble.doubleValue();
                Publicarea = Arith.round(d,2);
		}
	}

	public double getOtherarea()
	{
		return Otherarea;
	}
	public void setOtherarea(double aOtherarea)
	{
		Otherarea = Arith.round(aOtherarea,2);
	}
	public void setOtherarea(String aOtherarea)
	{
		if (aOtherarea != null && !aOtherarea.equals(""))
		{
			Double tDouble = new Double(aOtherarea);
			double d = tDouble.doubleValue();
                Otherarea = Arith.round(d,2);
		}
	}

	public String getExplanation()
	{
		return Explanation;
	}
	public void setExplanation(String aExplanation)
	{
		Explanation = aExplanation;
	}
	public double getMarginfee()
	{
		return Marginfee;
	}
	public void setMarginfee(double aMarginfee)
	{
		Marginfee = Arith.round(aMarginfee,2);
	}
	public void setMarginfee(String aMarginfee)
	{
		if (aMarginfee != null && !aMarginfee.equals(""))
		{
			Double tDouble = new Double(aMarginfee);
			double d = tDouble.doubleValue();
                Marginfee = Arith.round(d,2);
		}
	}

	public String getMarginpaydate()
	{
		if( Marginpaydate != null )
			return fDate.getString(Marginpaydate);
		else
			return null;
	}
	public void setMarginpaydate(Date aMarginpaydate)
	{
		Marginpaydate = aMarginpaydate;
	}
	public void setMarginpaydate(String aMarginpaydate)
	{
		if (aMarginpaydate != null && !aMarginpaydate.equals("") )
		{
			Marginpaydate = fDate.getDate( aMarginpaydate );
		}
		else
			Marginpaydate = null;
	}

	public String getMarginbackdate()
	{
		if( Marginbackdate != null )
			return fDate.getString(Marginbackdate);
		else
			return null;
	}
	public void setMarginbackdate(Date aMarginbackdate)
	{
		Marginbackdate = aMarginbackdate;
	}
	public void setMarginbackdate(String aMarginbackdate)
	{
		if (aMarginbackdate != null && !aMarginbackdate.equals("") )
		{
			Marginbackdate = fDate.getDate( aMarginbackdate );
		}
		else
			Marginbackdate = null;
	}

	public double getPenaltyfee()
	{
		return Penaltyfee;
	}
	public void setPenaltyfee(double aPenaltyfee)
	{
		Penaltyfee = Arith.round(aPenaltyfee,2);
	}
	public void setPenaltyfee(String aPenaltyfee)
	{
		if (aPenaltyfee != null && !aPenaltyfee.equals(""))
		{
			Double tDouble = new Double(aPenaltyfee);
			double d = tDouble.doubleValue();
                Penaltyfee = Arith.round(d,2);
		}
	}

	public String getRemake()
	{
		return Remake;
	}
	public void setRemake(String aRemake)
	{
		Remake = aRemake;
	}
	public String getFilename()
	{
		return Filename;
	}
	public void setFilename(String aFilename)
	{
		Filename = aFilename;
	}
	public String getFilepath()
	{
		return Filepath;
	}
	public void setFilepath(String aFilepath)
	{
		Filepath = aFilepath;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakedate()
	{
		if( Makedate != null )
			return fDate.getString(Makedate);
		else
			return null;
	}
	public void setMakedate(Date aMakedate)
	{
		Makedate = aMakedate;
	}
	public void setMakedate(String aMakedate)
	{
		if (aMakedate != null && !aMakedate.equals("") )
		{
			Makedate = fDate.getDate( aMakedate );
		}
		else
			Makedate = null;
	}

	public String getMaketime()
	{
		return Maketime;
	}
	public void setMaketime(String aMaketime)
	{
		Maketime = aMaketime;
	}
	public String getModifydate()
	{
		if( Modifydate != null )
			return fDate.getString(Modifydate);
		else
			return null;
	}
	public void setModifydate(Date aModifydate)
	{
		Modifydate = aModifydate;
	}
	public void setModifydate(String aModifydate)
	{
		if (aModifydate != null && !aModifydate.equals("") )
		{
			Modifydate = fDate.getDate( aModifydate );
		}
		else
			Modifydate = null;
	}

	public String getModifytime()
	{
		return Modifytime;
	}
	public void setModifytime(String aModifytime)
	{
		Modifytime = aModifytime;
	}
	public String getStandbystring1()
	{
		return Standbystring1;
	}
	public void setStandbystring1(String aStandbystring1)
	{
		Standbystring1 = aStandbystring1;
	}
	public String getStandbystring2()
	{
		return Standbystring2;
	}
	public void setStandbystring2(String aStandbystring2)
	{
		Standbystring2 = aStandbystring2;
	}
	public String getStandbystring3()
	{
		return Standbystring3;
	}
	public void setStandbystring3(String aStandbystring3)
	{
		Standbystring3 = aStandbystring3;
	}
	public double getStandbynum1()
	{
		return Standbynum1;
	}
	public void setStandbynum1(double aStandbynum1)
	{
		Standbynum1 = Arith.round(aStandbynum1,2);
	}
	public void setStandbynum1(String aStandbynum1)
	{
		if (aStandbynum1 != null && !aStandbynum1.equals(""))
		{
			Double tDouble = new Double(aStandbynum1);
			double d = tDouble.doubleValue();
                Standbynum1 = Arith.round(d,2);
		}
	}

	public double getStandbynum2()
	{
		return Standbynum2;
	}
	public void setStandbynum2(double aStandbynum2)
	{
		Standbynum2 = Arith.round(aStandbynum2,2);
	}
	public void setStandbynum2(String aStandbynum2)
	{
		if (aStandbynum2 != null && !aStandbynum2.equals(""))
		{
			Double tDouble = new Double(aStandbynum2);
			double d = tDouble.doubleValue();
                Standbynum2 = Arith.round(d,2);
		}
	}

	public double getStandbynum3()
	{
		return Standbynum3;
	}
	public void setStandbynum3(double aStandbynum3)
	{
		Standbynum3 = Arith.round(aStandbynum3,2);
	}
	public void setStandbynum3(String aStandbynum3)
	{
		if (aStandbynum3 != null && !aStandbynum3.equals(""))
		{
			Double tDouble = new Double(aStandbynum3);
			double d = tDouble.doubleValue();
                Standbynum3 = Arith.round(d,2);
		}
	}

	public String getStandbydate1()
	{
		if( Standbydate1 != null )
			return fDate.getString(Standbydate1);
		else
			return null;
	}
	public void setStandbydate1(Date aStandbydate1)
	{
		Standbydate1 = aStandbydate1;
	}
	public void setStandbydate1(String aStandbydate1)
	{
		if (aStandbydate1 != null && !aStandbydate1.equals("") )
		{
			Standbydate1 = fDate.getDate( aStandbydate1 );
		}
		else
			Standbydate1 = null;
	}

	public String getStandbydate2()
	{
		if( Standbydate2 != null )
			return fDate.getString(Standbydate2);
		else
			return null;
	}
	public void setStandbydate2(Date aStandbydate2)
	{
		Standbydate2 = aStandbydate2;
	}
	public void setStandbydate2(String aStandbydate2)
	{
		if (aStandbydate2 != null && !aStandbydate2.equals("") )
		{
			Standbydate2 = fDate.getDate( aStandbydate2 );
		}
		else
			Standbydate2 = null;
	}


	/**
	* 使用另外一个 LIPlaceRentInfoBSchema 对象给 Schema 赋值
	* @param: aLIPlaceRentInfoBSchema LIPlaceRentInfoBSchema
	**/
	public void setSchema(LIPlaceRentInfoBSchema aLIPlaceRentInfoBSchema)
	{
		this.Serialno = aLIPlaceRentInfoBSchema.getSerialno();
		this.Placeno = aLIPlaceRentInfoBSchema.getPlaceno();
		this.Dealtype = aLIPlaceRentInfoBSchema.getDealtype();
		this.Dealstate = aLIPlaceRentInfoBSchema.getDealstate();
		this.Placenorefer = aLIPlaceRentInfoBSchema.getPlacenorefer();
		this.Managecom = aLIPlaceRentInfoBSchema.getManagecom();
		this.Appno = aLIPlaceRentInfoBSchema.getAppno();
		this.State = aLIPlaceRentInfoBSchema.getState();
		this.Islock = aLIPlaceRentInfoBSchema.getIslock();
		this.Comlevel = aLIPlaceRentInfoBSchema.getComlevel();
		this.Comname = aLIPlaceRentInfoBSchema.getComname();
		this.Begindate = fDate.getDate( aLIPlaceRentInfoBSchema.getBegindate());
		this.Enddate = fDate.getDate( aLIPlaceRentInfoBSchema.getEnddate());
		this.Actualenddate = fDate.getDate( aLIPlaceRentInfoBSchema.getActualenddate());
		this.Address = aLIPlaceRentInfoBSchema.getAddress();
		this.Lessor = aLIPlaceRentInfoBSchema.getLessor();
		this.Houseno = aLIPlaceRentInfoBSchema.getHouseno();
		this.Province = aLIPlaceRentInfoBSchema.getProvince();
		this.City = aLIPlaceRentInfoBSchema.getCity();
		this.Isrentregister = aLIPlaceRentInfoBSchema.getIsrentregister();
		this.Islessorassociate = aLIPlaceRentInfoBSchema.getIslessorassociate();
		this.Islessorright = aLIPlaceRentInfoBSchema.getIslessorright();
		this.Ishouserent = aLIPlaceRentInfoBSchema.getIshouserent();
		this.Isplaceoffice = aLIPlaceRentInfoBSchema.getIsplaceoffice();
		this.Ishousegroup = aLIPlaceRentInfoBSchema.getIshousegroup();
		this.Ishousefarm = aLIPlaceRentInfoBSchema.getIshousefarm();
		this.Sigarea = aLIPlaceRentInfoBSchema.getSigarea();
		this.Grparea = aLIPlaceRentInfoBSchema.getGrparea();
		this.Bankarea = aLIPlaceRentInfoBSchema.getBankarea();
		this.Healtharea = aLIPlaceRentInfoBSchema.getHealtharea();
		this.Servicearea = aLIPlaceRentInfoBSchema.getServicearea();
		this.Jointarea = aLIPlaceRentInfoBSchema.getJointarea();
		this.Manageroffice = aLIPlaceRentInfoBSchema.getManageroffice();
		this.Departpersno = aLIPlaceRentInfoBSchema.getDepartpersno();
		this.Planfinance = aLIPlaceRentInfoBSchema.getPlanfinance();
		this.Publicarea = aLIPlaceRentInfoBSchema.getPublicarea();
		this.Otherarea = aLIPlaceRentInfoBSchema.getOtherarea();
		this.Explanation = aLIPlaceRentInfoBSchema.getExplanation();
		this.Marginfee = aLIPlaceRentInfoBSchema.getMarginfee();
		this.Marginpaydate = fDate.getDate( aLIPlaceRentInfoBSchema.getMarginpaydate());
		this.Marginbackdate = fDate.getDate( aLIPlaceRentInfoBSchema.getMarginbackdate());
		this.Penaltyfee = aLIPlaceRentInfoBSchema.getPenaltyfee();
		this.Remake = aLIPlaceRentInfoBSchema.getRemake();
		this.Filename = aLIPlaceRentInfoBSchema.getFilename();
		this.Filepath = aLIPlaceRentInfoBSchema.getFilepath();
		this.Operator = aLIPlaceRentInfoBSchema.getOperator();
		this.Makedate = fDate.getDate( aLIPlaceRentInfoBSchema.getMakedate());
		this.Maketime = aLIPlaceRentInfoBSchema.getMaketime();
		this.Modifydate = fDate.getDate( aLIPlaceRentInfoBSchema.getModifydate());
		this.Modifytime = aLIPlaceRentInfoBSchema.getModifytime();
		this.Standbystring1 = aLIPlaceRentInfoBSchema.getStandbystring1();
		this.Standbystring2 = aLIPlaceRentInfoBSchema.getStandbystring2();
		this.Standbystring3 = aLIPlaceRentInfoBSchema.getStandbystring3();
		this.Standbynum1 = aLIPlaceRentInfoBSchema.getStandbynum1();
		this.Standbynum2 = aLIPlaceRentInfoBSchema.getStandbynum2();
		this.Standbynum3 = aLIPlaceRentInfoBSchema.getStandbynum3();
		this.Standbydate1 = fDate.getDate( aLIPlaceRentInfoBSchema.getStandbydate1());
		this.Standbydate2 = fDate.getDate( aLIPlaceRentInfoBSchema.getStandbydate2());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Serialno") == null )
				this.Serialno = null;
			else
				this.Serialno = rs.getString("Serialno").trim();

			if( rs.getString("Placeno") == null )
				this.Placeno = null;
			else
				this.Placeno = rs.getString("Placeno").trim();

			if( rs.getString("Dealtype") == null )
				this.Dealtype = null;
			else
				this.Dealtype = rs.getString("Dealtype").trim();

			if( rs.getString("Dealstate") == null )
				this.Dealstate = null;
			else
				this.Dealstate = rs.getString("Dealstate").trim();

			if( rs.getString("Placenorefer") == null )
				this.Placenorefer = null;
			else
				this.Placenorefer = rs.getString("Placenorefer").trim();

			if( rs.getString("Managecom") == null )
				this.Managecom = null;
			else
				this.Managecom = rs.getString("Managecom").trim();

			if( rs.getString("Appno") == null )
				this.Appno = null;
			else
				this.Appno = rs.getString("Appno").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Islock") == null )
				this.Islock = null;
			else
				this.Islock = rs.getString("Islock").trim();

			if( rs.getString("Comlevel") == null )
				this.Comlevel = null;
			else
				this.Comlevel = rs.getString("Comlevel").trim();

			if( rs.getString("Comname") == null )
				this.Comname = null;
			else
				this.Comname = rs.getString("Comname").trim();

			this.Begindate = rs.getDate("Begindate");
			this.Enddate = rs.getDate("Enddate");
			this.Actualenddate = rs.getDate("Actualenddate");
			if( rs.getString("Address") == null )
				this.Address = null;
			else
				this.Address = rs.getString("Address").trim();

			if( rs.getString("Lessor") == null )
				this.Lessor = null;
			else
				this.Lessor = rs.getString("Lessor").trim();

			if( rs.getString("Houseno") == null )
				this.Houseno = null;
			else
				this.Houseno = rs.getString("Houseno").trim();

			if( rs.getString("Province") == null )
				this.Province = null;
			else
				this.Province = rs.getString("Province").trim();

			if( rs.getString("City") == null )
				this.City = null;
			else
				this.City = rs.getString("City").trim();

			if( rs.getString("Isrentregister") == null )
				this.Isrentregister = null;
			else
				this.Isrentregister = rs.getString("Isrentregister").trim();

			if( rs.getString("Islessorassociate") == null )
				this.Islessorassociate = null;
			else
				this.Islessorassociate = rs.getString("Islessorassociate").trim();

			if( rs.getString("Islessorright") == null )
				this.Islessorright = null;
			else
				this.Islessorright = rs.getString("Islessorright").trim();

			if( rs.getString("Ishouserent") == null )
				this.Ishouserent = null;
			else
				this.Ishouserent = rs.getString("Ishouserent").trim();

			if( rs.getString("Isplaceoffice") == null )
				this.Isplaceoffice = null;
			else
				this.Isplaceoffice = rs.getString("Isplaceoffice").trim();

			if( rs.getString("Ishousegroup") == null )
				this.Ishousegroup = null;
			else
				this.Ishousegroup = rs.getString("Ishousegroup").trim();

			if( rs.getString("Ishousefarm") == null )
				this.Ishousefarm = null;
			else
				this.Ishousefarm = rs.getString("Ishousefarm").trim();

			this.Sigarea = rs.getDouble("Sigarea");
			this.Grparea = rs.getDouble("Grparea");
			this.Bankarea = rs.getDouble("Bankarea");
			this.Healtharea = rs.getDouble("Healtharea");
			this.Servicearea = rs.getDouble("Servicearea");
			this.Jointarea = rs.getDouble("Jointarea");
			this.Manageroffice = rs.getDouble("Manageroffice");
			this.Departpersno = rs.getDouble("Departpersno");
			this.Planfinance = rs.getDouble("Planfinance");
			this.Publicarea = rs.getDouble("Publicarea");
			this.Otherarea = rs.getDouble("Otherarea");
			if( rs.getString("Explanation") == null )
				this.Explanation = null;
			else
				this.Explanation = rs.getString("Explanation").trim();

			this.Marginfee = rs.getDouble("Marginfee");
			this.Marginpaydate = rs.getDate("Marginpaydate");
			this.Marginbackdate = rs.getDate("Marginbackdate");
			this.Penaltyfee = rs.getDouble("Penaltyfee");
			if( rs.getString("Remake") == null )
				this.Remake = null;
			else
				this.Remake = rs.getString("Remake").trim();

			if( rs.getString("Filename") == null )
				this.Filename = null;
			else
				this.Filename = rs.getString("Filename").trim();

			if( rs.getString("Filepath") == null )
				this.Filepath = null;
			else
				this.Filepath = rs.getString("Filepath").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.Makedate = rs.getDate("Makedate");
			if( rs.getString("Maketime") == null )
				this.Maketime = null;
			else
				this.Maketime = rs.getString("Maketime").trim();

			this.Modifydate = rs.getDate("Modifydate");
			if( rs.getString("Modifytime") == null )
				this.Modifytime = null;
			else
				this.Modifytime = rs.getString("Modifytime").trim();

			if( rs.getString("Standbystring1") == null )
				this.Standbystring1 = null;
			else
				this.Standbystring1 = rs.getString("Standbystring1").trim();

			if( rs.getString("Standbystring2") == null )
				this.Standbystring2 = null;
			else
				this.Standbystring2 = rs.getString("Standbystring2").trim();

			if( rs.getString("Standbystring3") == null )
				this.Standbystring3 = null;
			else
				this.Standbystring3 = rs.getString("Standbystring3").trim();

			this.Standbynum1 = rs.getDouble("Standbynum1");
			this.Standbynum2 = rs.getDouble("Standbynum2");
			this.Standbynum3 = rs.getDouble("Standbynum3");
			this.Standbydate1 = rs.getDate("Standbydate1");
			this.Standbydate2 = rs.getDate("Standbydate2");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIPlaceRentInfoB表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIPlaceRentInfoBSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIPlaceRentInfoBSchema getSchema()
	{
		LIPlaceRentInfoBSchema aLIPlaceRentInfoBSchema = new LIPlaceRentInfoBSchema();
		aLIPlaceRentInfoBSchema.setSchema(this);
		return aLIPlaceRentInfoBSchema;
	}

	public LIPlaceRentInfoBDB getDB()
	{
		LIPlaceRentInfoBDB aDBOper = new LIPlaceRentInfoBDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIPlaceRentInfoB描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Serialno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Placeno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Dealtype)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Dealstate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Placenorefer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Managecom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Appno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Islock)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Comlevel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Comname)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Begindate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Enddate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Actualenddate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Address)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Lessor)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Houseno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Province)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(City)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Isrentregister)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Islessorassociate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Islessorright)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Ishouserent)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Isplaceoffice)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Ishousegroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Ishousefarm)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Sigarea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Grparea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Bankarea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Healtharea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Servicearea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Jointarea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Manageroffice));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Departpersno));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Planfinance));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Publicarea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Otherarea));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Explanation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Marginfee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Marginpaydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Marginbackdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Penaltyfee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remake)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Filename)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Filepath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Maketime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Modifydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Modifytime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Standbydate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Standbydate2 )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIPlaceRentInfoB>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Serialno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Placeno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Dealtype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Dealstate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Placenorefer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Appno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Islock = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Comlevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Comname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Begindate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			Enddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			Actualenddate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Lessor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Houseno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Province = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			City = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Isrentregister = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			Islessorassociate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Islessorright = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Ishouserent = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			Isplaceoffice = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Ishousegroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Ishousefarm = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Sigarea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			Grparea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			Bankarea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			Healtharea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			Servicearea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			Jointarea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			Manageroffice = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).doubleValue();
			Departpersno = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).doubleValue();
			Planfinance = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).doubleValue();
			Publicarea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
			Otherarea = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).doubleValue();
			Explanation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			Marginfee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,39,SysConst.PACKAGESPILTER))).doubleValue();
			Marginpaydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,SysConst.PACKAGESPILTER));
			Marginbackdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
			Penaltyfee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,42,SysConst.PACKAGESPILTER))).doubleValue();
			Remake = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			Filename = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			Filepath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			Makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47,SysConst.PACKAGESPILTER));
			Maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			Modifydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49,SysConst.PACKAGESPILTER));
			Modifytime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			Standbystring1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			Standbystring2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			Standbystring3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			Standbynum1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,54,SysConst.PACKAGESPILTER))).doubleValue();
			Standbynum2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,55,SysConst.PACKAGESPILTER))).doubleValue();
			Standbynum3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,56,SysConst.PACKAGESPILTER))).doubleValue();
			Standbydate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57,SysConst.PACKAGESPILTER));
			Standbydate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIPlaceRentInfoBSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Serialno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Serialno));
		}
		if (FCode.equals("Placeno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Placeno));
		}
		if (FCode.equals("Dealtype"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dealtype));
		}
		if (FCode.equals("Dealstate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dealstate));
		}
		if (FCode.equals("Placenorefer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Placenorefer));
		}
		if (FCode.equals("Managecom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
		}
		if (FCode.equals("Appno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Appno));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Islock"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Islock));
		}
		if (FCode.equals("Comlevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Comlevel));
		}
		if (FCode.equals("Comname"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Comname));
		}
		if (FCode.equals("Begindate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBegindate()));
		}
		if (FCode.equals("Enddate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEnddate()));
		}
		if (FCode.equals("Actualenddate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getActualenddate()));
		}
		if (FCode.equals("Address"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
		}
		if (FCode.equals("Lessor"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Lessor));
		}
		if (FCode.equals("Houseno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Houseno));
		}
		if (FCode.equals("Province"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Province));
		}
		if (FCode.equals("City"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(City));
		}
		if (FCode.equals("Isrentregister"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Isrentregister));
		}
		if (FCode.equals("Islessorassociate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Islessorassociate));
		}
		if (FCode.equals("Islessorright"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Islessorright));
		}
		if (FCode.equals("Ishouserent"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Ishouserent));
		}
		if (FCode.equals("Isplaceoffice"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Isplaceoffice));
		}
		if (FCode.equals("Ishousegroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Ishousegroup));
		}
		if (FCode.equals("Ishousefarm"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Ishousefarm));
		}
		if (FCode.equals("Sigarea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sigarea));
		}
		if (FCode.equals("Grparea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Grparea));
		}
		if (FCode.equals("Bankarea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bankarea));
		}
		if (FCode.equals("Healtharea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Healtharea));
		}
		if (FCode.equals("Servicearea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Servicearea));
		}
		if (FCode.equals("Jointarea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Jointarea));
		}
		if (FCode.equals("Manageroffice"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Manageroffice));
		}
		if (FCode.equals("Departpersno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Departpersno));
		}
		if (FCode.equals("Planfinance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Planfinance));
		}
		if (FCode.equals("Publicarea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Publicarea));
		}
		if (FCode.equals("Otherarea"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Otherarea));
		}
		if (FCode.equals("Explanation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Explanation));
		}
		if (FCode.equals("Marginfee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Marginfee));
		}
		if (FCode.equals("Marginpaydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMarginpaydate()));
		}
		if (FCode.equals("Marginbackdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMarginbackdate()));
		}
		if (FCode.equals("Penaltyfee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Penaltyfee));
		}
		if (FCode.equals("Remake"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remake));
		}
		if (FCode.equals("Filename"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Filename));
		}
		if (FCode.equals("Filepath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Filepath));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
		}
		if (FCode.equals("Maketime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Maketime));
		}
		if (FCode.equals("Modifydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifydate()));
		}
		if (FCode.equals("Modifytime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Modifytime));
		}
		if (FCode.equals("Standbystring1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring1));
		}
		if (FCode.equals("Standbystring2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring2));
		}
		if (FCode.equals("Standbystring3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring3));
		}
		if (FCode.equals("Standbynum1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum1));
		}
		if (FCode.equals("Standbynum2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum2));
		}
		if (FCode.equals("Standbynum3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum3));
		}
		if (FCode.equals("Standbydate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate1()));
		}
		if (FCode.equals("Standbydate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate2()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Serialno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Placeno);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Dealtype);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Dealstate);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Placenorefer);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Managecom);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Appno);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Islock);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Comlevel);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Comname);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBegindate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEnddate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getActualenddate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Address);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Lessor);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Houseno);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Province);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(City);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Isrentregister);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Islessorassociate);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Islessorright);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Ishouserent);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(Isplaceoffice);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Ishousegroup);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Ishousefarm);
				break;
			case 26:
				strFieldValue = String.valueOf(Sigarea);
				break;
			case 27:
				strFieldValue = String.valueOf(Grparea);
				break;
			case 28:
				strFieldValue = String.valueOf(Bankarea);
				break;
			case 29:
				strFieldValue = String.valueOf(Healtharea);
				break;
			case 30:
				strFieldValue = String.valueOf(Servicearea);
				break;
			case 31:
				strFieldValue = String.valueOf(Jointarea);
				break;
			case 32:
				strFieldValue = String.valueOf(Manageroffice);
				break;
			case 33:
				strFieldValue = String.valueOf(Departpersno);
				break;
			case 34:
				strFieldValue = String.valueOf(Planfinance);
				break;
			case 35:
				strFieldValue = String.valueOf(Publicarea);
				break;
			case 36:
				strFieldValue = String.valueOf(Otherarea);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(Explanation);
				break;
			case 38:
				strFieldValue = String.valueOf(Marginfee);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMarginpaydate()));
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMarginbackdate()));
				break;
			case 41:
				strFieldValue = String.valueOf(Penaltyfee);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(Remake);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(Filename);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(Filepath);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(Maketime);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifydate()));
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(Modifytime);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(Standbystring1);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(Standbystring2);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(Standbystring3);
				break;
			case 53:
				strFieldValue = String.valueOf(Standbynum1);
				break;
			case 54:
				strFieldValue = String.valueOf(Standbynum2);
				break;
			case 55:
				strFieldValue = String.valueOf(Standbynum3);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate1()));
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate2()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Serialno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Serialno = FValue.trim();
			}
			else
				Serialno = null;
		}
		if (FCode.equalsIgnoreCase("Placeno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Placeno = FValue.trim();
			}
			else
				Placeno = null;
		}
		if (FCode.equalsIgnoreCase("Dealtype"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dealtype = FValue.trim();
			}
			else
				Dealtype = null;
		}
		if (FCode.equalsIgnoreCase("Dealstate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dealstate = FValue.trim();
			}
			else
				Dealstate = null;
		}
		if (FCode.equalsIgnoreCase("Placenorefer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Placenorefer = FValue.trim();
			}
			else
				Placenorefer = null;
		}
		if (FCode.equalsIgnoreCase("Managecom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Managecom = FValue.trim();
			}
			else
				Managecom = null;
		}
		if (FCode.equalsIgnoreCase("Appno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Appno = FValue.trim();
			}
			else
				Appno = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Islock"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Islock = FValue.trim();
			}
			else
				Islock = null;
		}
		if (FCode.equalsIgnoreCase("Comlevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Comlevel = FValue.trim();
			}
			else
				Comlevel = null;
		}
		if (FCode.equalsIgnoreCase("Comname"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Comname = FValue.trim();
			}
			else
				Comname = null;
		}
		if (FCode.equalsIgnoreCase("Begindate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Begindate = fDate.getDate( FValue );
			}
			else
				Begindate = null;
		}
		if (FCode.equalsIgnoreCase("Enddate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Enddate = fDate.getDate( FValue );
			}
			else
				Enddate = null;
		}
		if (FCode.equalsIgnoreCase("Actualenddate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Actualenddate = fDate.getDate( FValue );
			}
			else
				Actualenddate = null;
		}
		if (FCode.equalsIgnoreCase("Address"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Address = FValue.trim();
			}
			else
				Address = null;
		}
		if (FCode.equalsIgnoreCase("Lessor"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Lessor = FValue.trim();
			}
			else
				Lessor = null;
		}
		if (FCode.equalsIgnoreCase("Houseno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Houseno = FValue.trim();
			}
			else
				Houseno = null;
		}
		if (FCode.equalsIgnoreCase("Province"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Province = FValue.trim();
			}
			else
				Province = null;
		}
		if (FCode.equalsIgnoreCase("City"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				City = FValue.trim();
			}
			else
				City = null;
		}
		if (FCode.equalsIgnoreCase("Isrentregister"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Isrentregister = FValue.trim();
			}
			else
				Isrentregister = null;
		}
		if (FCode.equalsIgnoreCase("Islessorassociate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Islessorassociate = FValue.trim();
			}
			else
				Islessorassociate = null;
		}
		if (FCode.equalsIgnoreCase("Islessorright"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Islessorright = FValue.trim();
			}
			else
				Islessorright = null;
		}
		if (FCode.equalsIgnoreCase("Ishouserent"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Ishouserent = FValue.trim();
			}
			else
				Ishouserent = null;
		}
		if (FCode.equalsIgnoreCase("Isplaceoffice"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Isplaceoffice = FValue.trim();
			}
			else
				Isplaceoffice = null;
		}
		if (FCode.equalsIgnoreCase("Ishousegroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Ishousegroup = FValue.trim();
			}
			else
				Ishousegroup = null;
		}
		if (FCode.equalsIgnoreCase("Ishousefarm"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Ishousefarm = FValue.trim();
			}
			else
				Ishousefarm = null;
		}
		if (FCode.equalsIgnoreCase("Sigarea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Sigarea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Grparea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Grparea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Bankarea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Bankarea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Healtharea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Healtharea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Servicearea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Servicearea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Jointarea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Jointarea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Manageroffice"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Manageroffice = d;
			}
		}
		if (FCode.equalsIgnoreCase("Departpersno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Departpersno = d;
			}
		}
		if (FCode.equalsIgnoreCase("Planfinance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Planfinance = d;
			}
		}
		if (FCode.equalsIgnoreCase("Publicarea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Publicarea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Otherarea"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Otherarea = d;
			}
		}
		if (FCode.equalsIgnoreCase("Explanation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Explanation = FValue.trim();
			}
			else
				Explanation = null;
		}
		if (FCode.equalsIgnoreCase("Marginfee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Marginfee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Marginpaydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Marginpaydate = fDate.getDate( FValue );
			}
			else
				Marginpaydate = null;
		}
		if (FCode.equalsIgnoreCase("Marginbackdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Marginbackdate = fDate.getDate( FValue );
			}
			else
				Marginbackdate = null;
		}
		if (FCode.equalsIgnoreCase("Penaltyfee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Penaltyfee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Remake"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remake = FValue.trim();
			}
			else
				Remake = null;
		}
		if (FCode.equalsIgnoreCase("Filename"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Filename = FValue.trim();
			}
			else
				Filename = null;
		}
		if (FCode.equalsIgnoreCase("Filepath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Filepath = FValue.trim();
			}
			else
				Filepath = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Makedate = fDate.getDate( FValue );
			}
			else
				Makedate = null;
		}
		if (FCode.equalsIgnoreCase("Maketime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Maketime = FValue.trim();
			}
			else
				Maketime = null;
		}
		if (FCode.equalsIgnoreCase("Modifydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Modifydate = fDate.getDate( FValue );
			}
			else
				Modifydate = null;
		}
		if (FCode.equalsIgnoreCase("Modifytime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Modifytime = FValue.trim();
			}
			else
				Modifytime = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring1 = FValue.trim();
			}
			else
				Standbystring1 = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring2 = FValue.trim();
			}
			else
				Standbystring2 = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring3 = FValue.trim();
			}
			else
				Standbystring3 = null;
		}
		if (FCode.equalsIgnoreCase("Standbynum1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbynum2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbynum3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbydate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Standbydate1 = fDate.getDate( FValue );
			}
			else
				Standbydate1 = null;
		}
		if (FCode.equalsIgnoreCase("Standbydate2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Standbydate2 = fDate.getDate( FValue );
			}
			else
				Standbydate2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIPlaceRentInfoBSchema other = (LIPlaceRentInfoBSchema)otherObject;
		return
			(Serialno == null ? other.getSerialno() == null : Serialno.equals(other.getSerialno()))
			&& (Placeno == null ? other.getPlaceno() == null : Placeno.equals(other.getPlaceno()))
			&& (Dealtype == null ? other.getDealtype() == null : Dealtype.equals(other.getDealtype()))
			&& (Dealstate == null ? other.getDealstate() == null : Dealstate.equals(other.getDealstate()))
			&& (Placenorefer == null ? other.getPlacenorefer() == null : Placenorefer.equals(other.getPlacenorefer()))
			&& (Managecom == null ? other.getManagecom() == null : Managecom.equals(other.getManagecom()))
			&& (Appno == null ? other.getAppno() == null : Appno.equals(other.getAppno()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Islock == null ? other.getIslock() == null : Islock.equals(other.getIslock()))
			&& (Comlevel == null ? other.getComlevel() == null : Comlevel.equals(other.getComlevel()))
			&& (Comname == null ? other.getComname() == null : Comname.equals(other.getComname()))
			&& (Begindate == null ? other.getBegindate() == null : fDate.getString(Begindate).equals(other.getBegindate()))
			&& (Enddate == null ? other.getEnddate() == null : fDate.getString(Enddate).equals(other.getEnddate()))
			&& (Actualenddate == null ? other.getActualenddate() == null : fDate.getString(Actualenddate).equals(other.getActualenddate()))
			&& (Address == null ? other.getAddress() == null : Address.equals(other.getAddress()))
			&& (Lessor == null ? other.getLessor() == null : Lessor.equals(other.getLessor()))
			&& (Houseno == null ? other.getHouseno() == null : Houseno.equals(other.getHouseno()))
			&& (Province == null ? other.getProvince() == null : Province.equals(other.getProvince()))
			&& (City == null ? other.getCity() == null : City.equals(other.getCity()))
			&& (Isrentregister == null ? other.getIsrentregister() == null : Isrentregister.equals(other.getIsrentregister()))
			&& (Islessorassociate == null ? other.getIslessorassociate() == null : Islessorassociate.equals(other.getIslessorassociate()))
			&& (Islessorright == null ? other.getIslessorright() == null : Islessorright.equals(other.getIslessorright()))
			&& (Ishouserent == null ? other.getIshouserent() == null : Ishouserent.equals(other.getIshouserent()))
			&& (Isplaceoffice == null ? other.getIsplaceoffice() == null : Isplaceoffice.equals(other.getIsplaceoffice()))
			&& (Ishousegroup == null ? other.getIshousegroup() == null : Ishousegroup.equals(other.getIshousegroup()))
			&& (Ishousefarm == null ? other.getIshousefarm() == null : Ishousefarm.equals(other.getIshousefarm()))
			&& Sigarea == other.getSigarea()
			&& Grparea == other.getGrparea()
			&& Bankarea == other.getBankarea()
			&& Healtharea == other.getHealtharea()
			&& Servicearea == other.getServicearea()
			&& Jointarea == other.getJointarea()
			&& Manageroffice == other.getManageroffice()
			&& Departpersno == other.getDepartpersno()
			&& Planfinance == other.getPlanfinance()
			&& Publicarea == other.getPublicarea()
			&& Otherarea == other.getOtherarea()
			&& (Explanation == null ? other.getExplanation() == null : Explanation.equals(other.getExplanation()))
			&& Marginfee == other.getMarginfee()
			&& (Marginpaydate == null ? other.getMarginpaydate() == null : fDate.getString(Marginpaydate).equals(other.getMarginpaydate()))
			&& (Marginbackdate == null ? other.getMarginbackdate() == null : fDate.getString(Marginbackdate).equals(other.getMarginbackdate()))
			&& Penaltyfee == other.getPenaltyfee()
			&& (Remake == null ? other.getRemake() == null : Remake.equals(other.getRemake()))
			&& (Filename == null ? other.getFilename() == null : Filename.equals(other.getFilename()))
			&& (Filepath == null ? other.getFilepath() == null : Filepath.equals(other.getFilepath()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (Makedate == null ? other.getMakedate() == null : fDate.getString(Makedate).equals(other.getMakedate()))
			&& (Maketime == null ? other.getMaketime() == null : Maketime.equals(other.getMaketime()))
			&& (Modifydate == null ? other.getModifydate() == null : fDate.getString(Modifydate).equals(other.getModifydate()))
			&& (Modifytime == null ? other.getModifytime() == null : Modifytime.equals(other.getModifytime()))
			&& (Standbystring1 == null ? other.getStandbystring1() == null : Standbystring1.equals(other.getStandbystring1()))
			&& (Standbystring2 == null ? other.getStandbystring2() == null : Standbystring2.equals(other.getStandbystring2()))
			&& (Standbystring3 == null ? other.getStandbystring3() == null : Standbystring3.equals(other.getStandbystring3()))
			&& Standbynum1 == other.getStandbynum1()
			&& Standbynum2 == other.getStandbynum2()
			&& Standbynum3 == other.getStandbynum3()
			&& (Standbydate1 == null ? other.getStandbydate1() == null : fDate.getString(Standbydate1).equals(other.getStandbydate1()))
			&& (Standbydate2 == null ? other.getStandbydate2() == null : fDate.getString(Standbydate2).equals(other.getStandbydate2()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Serialno") ) {
			return 0;
		}
		if( strFieldName.equals("Placeno") ) {
			return 1;
		}
		if( strFieldName.equals("Dealtype") ) {
			return 2;
		}
		if( strFieldName.equals("Dealstate") ) {
			return 3;
		}
		if( strFieldName.equals("Placenorefer") ) {
			return 4;
		}
		if( strFieldName.equals("Managecom") ) {
			return 5;
		}
		if( strFieldName.equals("Appno") ) {
			return 6;
		}
		if( strFieldName.equals("State") ) {
			return 7;
		}
		if( strFieldName.equals("Islock") ) {
			return 8;
		}
		if( strFieldName.equals("Comlevel") ) {
			return 9;
		}
		if( strFieldName.equals("Comname") ) {
			return 10;
		}
		if( strFieldName.equals("Begindate") ) {
			return 11;
		}
		if( strFieldName.equals("Enddate") ) {
			return 12;
		}
		if( strFieldName.equals("Actualenddate") ) {
			return 13;
		}
		if( strFieldName.equals("Address") ) {
			return 14;
		}
		if( strFieldName.equals("Lessor") ) {
			return 15;
		}
		if( strFieldName.equals("Houseno") ) {
			return 16;
		}
		if( strFieldName.equals("Province") ) {
			return 17;
		}
		if( strFieldName.equals("City") ) {
			return 18;
		}
		if( strFieldName.equals("Isrentregister") ) {
			return 19;
		}
		if( strFieldName.equals("Islessorassociate") ) {
			return 20;
		}
		if( strFieldName.equals("Islessorright") ) {
			return 21;
		}
		if( strFieldName.equals("Ishouserent") ) {
			return 22;
		}
		if( strFieldName.equals("Isplaceoffice") ) {
			return 23;
		}
		if( strFieldName.equals("Ishousegroup") ) {
			return 24;
		}
		if( strFieldName.equals("Ishousefarm") ) {
			return 25;
		}
		if( strFieldName.equals("Sigarea") ) {
			return 26;
		}
		if( strFieldName.equals("Grparea") ) {
			return 27;
		}
		if( strFieldName.equals("Bankarea") ) {
			return 28;
		}
		if( strFieldName.equals("Healtharea") ) {
			return 29;
		}
		if( strFieldName.equals("Servicearea") ) {
			return 30;
		}
		if( strFieldName.equals("Jointarea") ) {
			return 31;
		}
		if( strFieldName.equals("Manageroffice") ) {
			return 32;
		}
		if( strFieldName.equals("Departpersno") ) {
			return 33;
		}
		if( strFieldName.equals("Planfinance") ) {
			return 34;
		}
		if( strFieldName.equals("Publicarea") ) {
			return 35;
		}
		if( strFieldName.equals("Otherarea") ) {
			return 36;
		}
		if( strFieldName.equals("Explanation") ) {
			return 37;
		}
		if( strFieldName.equals("Marginfee") ) {
			return 38;
		}
		if( strFieldName.equals("Marginpaydate") ) {
			return 39;
		}
		if( strFieldName.equals("Marginbackdate") ) {
			return 40;
		}
		if( strFieldName.equals("Penaltyfee") ) {
			return 41;
		}
		if( strFieldName.equals("Remake") ) {
			return 42;
		}
		if( strFieldName.equals("Filename") ) {
			return 43;
		}
		if( strFieldName.equals("Filepath") ) {
			return 44;
		}
		if( strFieldName.equals("Operator") ) {
			return 45;
		}
		if( strFieldName.equals("Makedate") ) {
			return 46;
		}
		if( strFieldName.equals("Maketime") ) {
			return 47;
		}
		if( strFieldName.equals("Modifydate") ) {
			return 48;
		}
		if( strFieldName.equals("Modifytime") ) {
			return 49;
		}
		if( strFieldName.equals("Standbystring1") ) {
			return 50;
		}
		if( strFieldName.equals("Standbystring2") ) {
			return 51;
		}
		if( strFieldName.equals("Standbystring3") ) {
			return 52;
		}
		if( strFieldName.equals("Standbynum1") ) {
			return 53;
		}
		if( strFieldName.equals("Standbynum2") ) {
			return 54;
		}
		if( strFieldName.equals("Standbynum3") ) {
			return 55;
		}
		if( strFieldName.equals("Standbydate1") ) {
			return 56;
		}
		if( strFieldName.equals("Standbydate2") ) {
			return 57;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Serialno";
				break;
			case 1:
				strFieldName = "Placeno";
				break;
			case 2:
				strFieldName = "Dealtype";
				break;
			case 3:
				strFieldName = "Dealstate";
				break;
			case 4:
				strFieldName = "Placenorefer";
				break;
			case 5:
				strFieldName = "Managecom";
				break;
			case 6:
				strFieldName = "Appno";
				break;
			case 7:
				strFieldName = "State";
				break;
			case 8:
				strFieldName = "Islock";
				break;
			case 9:
				strFieldName = "Comlevel";
				break;
			case 10:
				strFieldName = "Comname";
				break;
			case 11:
				strFieldName = "Begindate";
				break;
			case 12:
				strFieldName = "Enddate";
				break;
			case 13:
				strFieldName = "Actualenddate";
				break;
			case 14:
				strFieldName = "Address";
				break;
			case 15:
				strFieldName = "Lessor";
				break;
			case 16:
				strFieldName = "Houseno";
				break;
			case 17:
				strFieldName = "Province";
				break;
			case 18:
				strFieldName = "City";
				break;
			case 19:
				strFieldName = "Isrentregister";
				break;
			case 20:
				strFieldName = "Islessorassociate";
				break;
			case 21:
				strFieldName = "Islessorright";
				break;
			case 22:
				strFieldName = "Ishouserent";
				break;
			case 23:
				strFieldName = "Isplaceoffice";
				break;
			case 24:
				strFieldName = "Ishousegroup";
				break;
			case 25:
				strFieldName = "Ishousefarm";
				break;
			case 26:
				strFieldName = "Sigarea";
				break;
			case 27:
				strFieldName = "Grparea";
				break;
			case 28:
				strFieldName = "Bankarea";
				break;
			case 29:
				strFieldName = "Healtharea";
				break;
			case 30:
				strFieldName = "Servicearea";
				break;
			case 31:
				strFieldName = "Jointarea";
				break;
			case 32:
				strFieldName = "Manageroffice";
				break;
			case 33:
				strFieldName = "Departpersno";
				break;
			case 34:
				strFieldName = "Planfinance";
				break;
			case 35:
				strFieldName = "Publicarea";
				break;
			case 36:
				strFieldName = "Otherarea";
				break;
			case 37:
				strFieldName = "Explanation";
				break;
			case 38:
				strFieldName = "Marginfee";
				break;
			case 39:
				strFieldName = "Marginpaydate";
				break;
			case 40:
				strFieldName = "Marginbackdate";
				break;
			case 41:
				strFieldName = "Penaltyfee";
				break;
			case 42:
				strFieldName = "Remake";
				break;
			case 43:
				strFieldName = "Filename";
				break;
			case 44:
				strFieldName = "Filepath";
				break;
			case 45:
				strFieldName = "Operator";
				break;
			case 46:
				strFieldName = "Makedate";
				break;
			case 47:
				strFieldName = "Maketime";
				break;
			case 48:
				strFieldName = "Modifydate";
				break;
			case 49:
				strFieldName = "Modifytime";
				break;
			case 50:
				strFieldName = "Standbystring1";
				break;
			case 51:
				strFieldName = "Standbystring2";
				break;
			case 52:
				strFieldName = "Standbystring3";
				break;
			case 53:
				strFieldName = "Standbynum1";
				break;
			case 54:
				strFieldName = "Standbynum2";
				break;
			case 55:
				strFieldName = "Standbynum3";
				break;
			case 56:
				strFieldName = "Standbydate1";
				break;
			case 57:
				strFieldName = "Standbydate2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Serialno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Placeno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Dealtype") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Dealstate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Placenorefer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Managecom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Appno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Islock") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Comlevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Comname") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Begindate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Enddate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Actualenddate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Address") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Lessor") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Houseno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Province") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("City") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Isrentregister") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Islessorassociate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Islessorright") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Ishouserent") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Isplaceoffice") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Ishousegroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Ishousefarm") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sigarea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Grparea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Bankarea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Healtharea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Servicearea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Jointarea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Manageroffice") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Departpersno") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Planfinance") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Publicarea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Otherarea") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Explanation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Marginfee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Marginpaydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Marginbackdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Penaltyfee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Remake") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Filename") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Filepath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Maketime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Modifydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Modifytime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbynum1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbynum2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbynum3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbydate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Standbydate2") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 33:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 34:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 36:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 39:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 41:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 54:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 55:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 56:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 57:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
