/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.CustomerQueryDB;

/*
 * <p>ClassName: CustomerQuerySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2015-10-21
 */
public class CustomerQuerySchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerNo;
	/** 平台客户号 */
	private String CustomerNo;
	/** 国籍 */
	private String Nationality;
	/** 保险公司代码 */
	private String CompanyCode;
	/** 保单类别 */
	private String PolicyType;
	/** 保单号码 */
	private String PolicyNo;
	/** 分单号 */
	private String SequenceNo;
	/** 保单投保来源 */
	private String PolicySource;
	/** 保单状态 */
	private String PolicyStatus;
	/** 保单生效日期 */
	private Date EffectiveDate;
	/** 保单满期日期 */
	private Date ExpireDate;
	/** 保单终止日期 */
	private Date TerminationDate;
	/** 保单终止原因 */
	private String TerminationReason;
	/** 是否有有效的税优识别码 */
	private String TaxFavorIndi;
	/** 当前保单年度内赔付金额 */
	private double PeriodPayment;
	/** 保单累计赔付金额 */
	private double PolicytotalPayment;
	/** 响应状态 */
	private String ResponseCode;
	/** 结果状态 */
	private String ResultStatus;
	/** 结果描述 */
	private String ResultInfoDesc;
	/** Ciitc结果状态 */
	private String CiitcResultCode;
	/** Ciitc结果信息 */
	private String CiitcResultMessage;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public CustomerQuerySchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		CustomerQuerySchema cloned = (CustomerQuerySchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerNo()
	{
		return SerNo;
	}
	public void setSerNo(String aSerNo)
	{
		SerNo = aSerNo;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getNationality()
	{
		return Nationality;
	}
	public void setNationality(String aNationality)
	{
		Nationality = aNationality;
	}
	public String getCompanyCode()
	{
		return CompanyCode;
	}
	public void setCompanyCode(String aCompanyCode)
	{
		CompanyCode = aCompanyCode;
	}
	public String getPolicyType()
	{
		return PolicyType;
	}
	public void setPolicyType(String aPolicyType)
	{
		PolicyType = aPolicyType;
	}
	public String getPolicyNo()
	{
		return PolicyNo;
	}
	public void setPolicyNo(String aPolicyNo)
	{
		PolicyNo = aPolicyNo;
	}
	public String getSequenceNo()
	{
		return SequenceNo;
	}
	public void setSequenceNo(String aSequenceNo)
	{
		SequenceNo = aSequenceNo;
	}
	public String getPolicySource()
	{
		return PolicySource;
	}
	public void setPolicySource(String aPolicySource)
	{
		PolicySource = aPolicySource;
	}
	public String getPolicyStatus()
	{
		return PolicyStatus;
	}
	public void setPolicyStatus(String aPolicyStatus)
	{
		PolicyStatus = aPolicyStatus;
	}
	public String getEffectiveDate()
	{
		if( EffectiveDate != null )
			return fDate.getString(EffectiveDate);
		else
			return null;
	}
	public void setEffectiveDate(Date aEffectiveDate)
	{
		EffectiveDate = aEffectiveDate;
	}
	public void setEffectiveDate(String aEffectiveDate)
	{
		if (aEffectiveDate != null && !aEffectiveDate.equals("") )
		{
			EffectiveDate = fDate.getDate( aEffectiveDate );
		}
		else
			EffectiveDate = null;
	}

	public String getExpireDate()
	{
		if( ExpireDate != null )
			return fDate.getString(ExpireDate);
		else
			return null;
	}
	public void setExpireDate(Date aExpireDate)
	{
		ExpireDate = aExpireDate;
	}
	public void setExpireDate(String aExpireDate)
	{
		if (aExpireDate != null && !aExpireDate.equals("") )
		{
			ExpireDate = fDate.getDate( aExpireDate );
		}
		else
			ExpireDate = null;
	}

	public String getTerminationDate()
	{
		if( TerminationDate != null )
			return fDate.getString(TerminationDate);
		else
			return null;
	}
	public void setTerminationDate(Date aTerminationDate)
	{
		TerminationDate = aTerminationDate;
	}
	public void setTerminationDate(String aTerminationDate)
	{
		if (aTerminationDate != null && !aTerminationDate.equals("") )
		{
			TerminationDate = fDate.getDate( aTerminationDate );
		}
		else
			TerminationDate = null;
	}

	public String getTerminationReason()
	{
		return TerminationReason;
	}
	public void setTerminationReason(String aTerminationReason)
	{
		TerminationReason = aTerminationReason;
	}
	public String getTaxFavorIndi()
	{
		return TaxFavorIndi;
	}
	public void setTaxFavorIndi(String aTaxFavorIndi)
	{
		TaxFavorIndi = aTaxFavorIndi;
	}
	public double getPeriodPayment()
	{
		return PeriodPayment;
	}
	public void setPeriodPayment(double aPeriodPayment)
	{
		PeriodPayment = Arith.round(aPeriodPayment, 2);
	}
	public void setPeriodPayment(String aPeriodPayment)
	{
		if (aPeriodPayment != null && !aPeriodPayment.equals(""))
		{
			Double tDouble = new Double(aPeriodPayment);
			double d = tDouble.doubleValue();
                PeriodPayment = Arith.round(d, 2);
		}
	}

	public double getPolicytotalPayment()
	{
		return PolicytotalPayment;
	}
	public void setPolicytotalPayment(double aPolicytotalPayment)
	{
		PolicytotalPayment = Arith.round(aPolicytotalPayment, 2);
	}
	public void setPolicytotalPayment(String aPolicytotalPayment)
	{
		if (aPolicytotalPayment != null && !aPolicytotalPayment.equals(""))
		{
			Double tDouble = new Double(aPolicytotalPayment);
			double d = tDouble.doubleValue();
                PolicytotalPayment = Arith.round(d, 2);
		}
	}

	public String getResponseCode()
	{
		return ResponseCode;
	}
	public void setResponseCode(String aResponseCode)
	{
		ResponseCode = aResponseCode;
	}
	public String getResultStatus()
	{
		return ResultStatus;
	}
	public void setResultStatus(String aResultStatus)
	{
		ResultStatus = aResultStatus;
	}
	public String getResultInfoDesc()
	{
		return ResultInfoDesc;
	}
	public void setResultInfoDesc(String aResultInfoDesc)
	{
		ResultInfoDesc = aResultInfoDesc;
	}
	public String getCiitcResultCode()
	{
		return CiitcResultCode;
	}
	public void setCiitcResultCode(String aCiitcResultCode)
	{
		CiitcResultCode = aCiitcResultCode;
	}
	public String getCiitcResultMessage()
	{
		return CiitcResultMessage;
	}
	public void setCiitcResultMessage(String aCiitcResultMessage)
	{
		CiitcResultMessage = aCiitcResultMessage;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 CustomerQuerySchema 对象给 Schema 赋值
	* @param: aCustomerQuerySchema CustomerQuerySchema
	**/
	public void setSchema(CustomerQuerySchema aCustomerQuerySchema)
	{
		this.SerNo = aCustomerQuerySchema.getSerNo();
		this.CustomerNo = aCustomerQuerySchema.getCustomerNo();
		this.Nationality = aCustomerQuerySchema.getNationality();
		this.CompanyCode = aCustomerQuerySchema.getCompanyCode();
		this.PolicyType = aCustomerQuerySchema.getPolicyType();
		this.PolicyNo = aCustomerQuerySchema.getPolicyNo();
		this.SequenceNo = aCustomerQuerySchema.getSequenceNo();
		this.PolicySource = aCustomerQuerySchema.getPolicySource();
		this.PolicyStatus = aCustomerQuerySchema.getPolicyStatus();
		this.EffectiveDate = fDate.getDate( aCustomerQuerySchema.getEffectiveDate());
		this.ExpireDate = fDate.getDate( aCustomerQuerySchema.getExpireDate());
		this.TerminationDate = fDate.getDate( aCustomerQuerySchema.getTerminationDate());
		this.TerminationReason = aCustomerQuerySchema.getTerminationReason();
		this.TaxFavorIndi = aCustomerQuerySchema.getTaxFavorIndi();
		this.PeriodPayment = aCustomerQuerySchema.getPeriodPayment();
		this.PolicytotalPayment = aCustomerQuerySchema.getPolicytotalPayment();
		this.ResponseCode = aCustomerQuerySchema.getResponseCode();
		this.ResultStatus = aCustomerQuerySchema.getResultStatus();
		this.ResultInfoDesc = aCustomerQuerySchema.getResultInfoDesc();
		this.CiitcResultCode = aCustomerQuerySchema.getCiitcResultCode();
		this.CiitcResultMessage = aCustomerQuerySchema.getCiitcResultMessage();
		this.MakeDate = fDate.getDate( aCustomerQuerySchema.getMakeDate());
		this.MakeTime = aCustomerQuerySchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aCustomerQuerySchema.getModifyDate());
		this.ModifyTime = aCustomerQuerySchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerNo") == null )
				this.SerNo = null;
			else
				this.SerNo = rs.getString("SerNo").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("Nationality") == null )
				this.Nationality = null;
			else
				this.Nationality = rs.getString("Nationality").trim();

			if( rs.getString("CompanyCode") == null )
				this.CompanyCode = null;
			else
				this.CompanyCode = rs.getString("CompanyCode").trim();

			if( rs.getString("PolicyType") == null )
				this.PolicyType = null;
			else
				this.PolicyType = rs.getString("PolicyType").trim();

			if( rs.getString("PolicyNo") == null )
				this.PolicyNo = null;
			else
				this.PolicyNo = rs.getString("PolicyNo").trim();

			if( rs.getString("SequenceNo") == null )
				this.SequenceNo = null;
			else
				this.SequenceNo = rs.getString("SequenceNo").trim();

			if( rs.getString("PolicySource") == null )
				this.PolicySource = null;
			else
				this.PolicySource = rs.getString("PolicySource").trim();

			if( rs.getString("PolicyStatus") == null )
				this.PolicyStatus = null;
			else
				this.PolicyStatus = rs.getString("PolicyStatus").trim();

			this.EffectiveDate = rs.getDate("EffectiveDate");
			this.ExpireDate = rs.getDate("ExpireDate");
			this.TerminationDate = rs.getDate("TerminationDate");
			if( rs.getString("TerminationReason") == null )
				this.TerminationReason = null;
			else
				this.TerminationReason = rs.getString("TerminationReason").trim();

			if( rs.getString("TaxFavorIndi") == null )
				this.TaxFavorIndi = null;
			else
				this.TaxFavorIndi = rs.getString("TaxFavorIndi").trim();

			this.PeriodPayment = rs.getDouble("PeriodPayment");
			this.PolicytotalPayment = rs.getDouble("PolicytotalPayment");
			if( rs.getString("ResponseCode") == null )
				this.ResponseCode = null;
			else
				this.ResponseCode = rs.getString("ResponseCode").trim();

			if( rs.getString("ResultStatus") == null )
				this.ResultStatus = null;
			else
				this.ResultStatus = rs.getString("ResultStatus").trim();

			if( rs.getString("ResultInfoDesc") == null )
				this.ResultInfoDesc = null;
			else
				this.ResultInfoDesc = rs.getString("ResultInfoDesc").trim();

			if( rs.getString("CiitcResultCode") == null )
				this.CiitcResultCode = null;
			else
				this.CiitcResultCode = rs.getString("CiitcResultCode").trim();

			if( rs.getString("CiitcResultMessage") == null )
				this.CiitcResultMessage = null;
			else
				this.CiitcResultMessage = rs.getString("CiitcResultMessage").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的CustomerQuery表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CustomerQuerySchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public CustomerQuerySchema getSchema()
	{
		CustomerQuerySchema aCustomerQuerySchema = new CustomerQuerySchema();
		aCustomerQuerySchema.setSchema(this);
		return aCustomerQuerySchema;
	}

	public CustomerQueryDB getDB()
	{
		CustomerQueryDB aDBOper = new CustomerQueryDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCustomerQuery描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Nationality)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicyType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicySource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicyStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EffectiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ExpireDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TerminationDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TerminationReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxFavorIndi)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PeriodPayment));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolicytotalPayment));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResponseCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResultStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResultInfoDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CiitcResultCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CiitcResultMessage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCustomerQuery>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CompanyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PolicyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			PolicyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			SequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			PolicySource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			PolicyStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			EffectiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			ExpireDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			TerminationDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			TerminationReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			TaxFavorIndi = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			PeriodPayment = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			PolicytotalPayment = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			ResponseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ResultStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ResultInfoDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			CiitcResultCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			CiitcResultMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CustomerQuerySchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerNo));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("Nationality"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
		}
		if (FCode.equals("CompanyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyCode));
		}
		if (FCode.equals("PolicyType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyType));
		}
		if (FCode.equals("PolicyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyNo));
		}
		if (FCode.equals("SequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SequenceNo));
		}
		if (FCode.equals("PolicySource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicySource));
		}
		if (FCode.equals("PolicyStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyStatus));
		}
		if (FCode.equals("EffectiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEffectiveDate()));
		}
		if (FCode.equals("ExpireDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getExpireDate()));
		}
		if (FCode.equals("TerminationDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTerminationDate()));
		}
		if (FCode.equals("TerminationReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TerminationReason));
		}
		if (FCode.equals("TaxFavorIndi"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxFavorIndi));
		}
		if (FCode.equals("PeriodPayment"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PeriodPayment));
		}
		if (FCode.equals("PolicytotalPayment"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicytotalPayment));
		}
		if (FCode.equals("ResponseCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResponseCode));
		}
		if (FCode.equals("ResultStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResultStatus));
		}
		if (FCode.equals("ResultInfoDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResultInfoDesc));
		}
		if (FCode.equals("CiitcResultCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CiitcResultCode));
		}
		if (FCode.equals("CiitcResultMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CiitcResultMessage));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Nationality);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CompanyCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(PolicyType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(PolicyNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(SequenceNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(PolicySource);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(PolicyStatus);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEffectiveDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getExpireDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTerminationDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(TerminationReason);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(TaxFavorIndi);
				break;
			case 14:
				strFieldValue = String.valueOf(PeriodPayment);
				break;
			case 15:
				strFieldValue = String.valueOf(PolicytotalPayment);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ResponseCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ResultStatus);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ResultInfoDesc);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(CiitcResultCode);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(CiitcResultMessage);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerNo = FValue.trim();
			}
			else
				SerNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("Nationality"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Nationality = FValue.trim();
			}
			else
				Nationality = null;
		}
		if (FCode.equalsIgnoreCase("CompanyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyCode = FValue.trim();
			}
			else
				CompanyCode = null;
		}
		if (FCode.equalsIgnoreCase("PolicyType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicyType = FValue.trim();
			}
			else
				PolicyType = null;
		}
		if (FCode.equalsIgnoreCase("PolicyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicyNo = FValue.trim();
			}
			else
				PolicyNo = null;
		}
		if (FCode.equalsIgnoreCase("SequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SequenceNo = FValue.trim();
			}
			else
				SequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("PolicySource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicySource = FValue.trim();
			}
			else
				PolicySource = null;
		}
		if (FCode.equalsIgnoreCase("PolicyStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicyStatus = FValue.trim();
			}
			else
				PolicyStatus = null;
		}
		if (FCode.equalsIgnoreCase("EffectiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EffectiveDate = fDate.getDate( FValue );
			}
			else
				EffectiveDate = null;
		}
		if (FCode.equalsIgnoreCase("ExpireDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ExpireDate = fDate.getDate( FValue );
			}
			else
				ExpireDate = null;
		}
		if (FCode.equalsIgnoreCase("TerminationDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TerminationDate = fDate.getDate( FValue );
			}
			else
				TerminationDate = null;
		}
		if (FCode.equalsIgnoreCase("TerminationReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TerminationReason = FValue.trim();
			}
			else
				TerminationReason = null;
		}
		if (FCode.equalsIgnoreCase("TaxFavorIndi"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxFavorIndi = FValue.trim();
			}
			else
				TaxFavorIndi = null;
		}
		if (FCode.equalsIgnoreCase("PeriodPayment"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PeriodPayment = d;
			}
		}
		if (FCode.equalsIgnoreCase("PolicytotalPayment"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PolicytotalPayment = d;
			}
		}
		if (FCode.equalsIgnoreCase("ResponseCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResponseCode = FValue.trim();
			}
			else
				ResponseCode = null;
		}
		if (FCode.equalsIgnoreCase("ResultStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResultStatus = FValue.trim();
			}
			else
				ResultStatus = null;
		}
		if (FCode.equalsIgnoreCase("ResultInfoDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResultInfoDesc = FValue.trim();
			}
			else
				ResultInfoDesc = null;
		}
		if (FCode.equalsIgnoreCase("CiitcResultCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CiitcResultCode = FValue.trim();
			}
			else
				CiitcResultCode = null;
		}
		if (FCode.equalsIgnoreCase("CiitcResultMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CiitcResultMessage = FValue.trim();
			}
			else
				CiitcResultMessage = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		CustomerQuerySchema other = (CustomerQuerySchema)otherObject;
		return
			(SerNo == null ? other.getSerNo() == null : SerNo.equals(other.getSerNo()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (Nationality == null ? other.getNationality() == null : Nationality.equals(other.getNationality()))
			&& (CompanyCode == null ? other.getCompanyCode() == null : CompanyCode.equals(other.getCompanyCode()))
			&& (PolicyType == null ? other.getPolicyType() == null : PolicyType.equals(other.getPolicyType()))
			&& (PolicyNo == null ? other.getPolicyNo() == null : PolicyNo.equals(other.getPolicyNo()))
			&& (SequenceNo == null ? other.getSequenceNo() == null : SequenceNo.equals(other.getSequenceNo()))
			&& (PolicySource == null ? other.getPolicySource() == null : PolicySource.equals(other.getPolicySource()))
			&& (PolicyStatus == null ? other.getPolicyStatus() == null : PolicyStatus.equals(other.getPolicyStatus()))
			&& (EffectiveDate == null ? other.getEffectiveDate() == null : fDate.getString(EffectiveDate).equals(other.getEffectiveDate()))
			&& (ExpireDate == null ? other.getExpireDate() == null : fDate.getString(ExpireDate).equals(other.getExpireDate()))
			&& (TerminationDate == null ? other.getTerminationDate() == null : fDate.getString(TerminationDate).equals(other.getTerminationDate()))
			&& (TerminationReason == null ? other.getTerminationReason() == null : TerminationReason.equals(other.getTerminationReason()))
			&& (TaxFavorIndi == null ? other.getTaxFavorIndi() == null : TaxFavorIndi.equals(other.getTaxFavorIndi()))
			&& PeriodPayment == other.getPeriodPayment()
			&& PolicytotalPayment == other.getPolicytotalPayment()
			&& (ResponseCode == null ? other.getResponseCode() == null : ResponseCode.equals(other.getResponseCode()))
			&& (ResultStatus == null ? other.getResultStatus() == null : ResultStatus.equals(other.getResultStatus()))
			&& (ResultInfoDesc == null ? other.getResultInfoDesc() == null : ResultInfoDesc.equals(other.getResultInfoDesc()))
			&& (CiitcResultCode == null ? other.getCiitcResultCode() == null : CiitcResultCode.equals(other.getCiitcResultCode()))
			&& (CiitcResultMessage == null ? other.getCiitcResultMessage() == null : CiitcResultMessage.equals(other.getCiitcResultMessage()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return 0;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 1;
		}
		if( strFieldName.equals("Nationality") ) {
			return 2;
		}
		if( strFieldName.equals("CompanyCode") ) {
			return 3;
		}
		if( strFieldName.equals("PolicyType") ) {
			return 4;
		}
		if( strFieldName.equals("PolicyNo") ) {
			return 5;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return 6;
		}
		if( strFieldName.equals("PolicySource") ) {
			return 7;
		}
		if( strFieldName.equals("PolicyStatus") ) {
			return 8;
		}
		if( strFieldName.equals("EffectiveDate") ) {
			return 9;
		}
		if( strFieldName.equals("ExpireDate") ) {
			return 10;
		}
		if( strFieldName.equals("TerminationDate") ) {
			return 11;
		}
		if( strFieldName.equals("TerminationReason") ) {
			return 12;
		}
		if( strFieldName.equals("TaxFavorIndi") ) {
			return 13;
		}
		if( strFieldName.equals("PeriodPayment") ) {
			return 14;
		}
		if( strFieldName.equals("PolicytotalPayment") ) {
			return 15;
		}
		if( strFieldName.equals("ResponseCode") ) {
			return 16;
		}
		if( strFieldName.equals("ResultStatus") ) {
			return 17;
		}
		if( strFieldName.equals("ResultInfoDesc") ) {
			return 18;
		}
		if( strFieldName.equals("CiitcResultCode") ) {
			return 19;
		}
		if( strFieldName.equals("CiitcResultMessage") ) {
			return 20;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 21;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerNo";
				break;
			case 1:
				strFieldName = "CustomerNo";
				break;
			case 2:
				strFieldName = "Nationality";
				break;
			case 3:
				strFieldName = "CompanyCode";
				break;
			case 4:
				strFieldName = "PolicyType";
				break;
			case 5:
				strFieldName = "PolicyNo";
				break;
			case 6:
				strFieldName = "SequenceNo";
				break;
			case 7:
				strFieldName = "PolicySource";
				break;
			case 8:
				strFieldName = "PolicyStatus";
				break;
			case 9:
				strFieldName = "EffectiveDate";
				break;
			case 10:
				strFieldName = "ExpireDate";
				break;
			case 11:
				strFieldName = "TerminationDate";
				break;
			case 12:
				strFieldName = "TerminationReason";
				break;
			case 13:
				strFieldName = "TaxFavorIndi";
				break;
			case 14:
				strFieldName = "PeriodPayment";
				break;
			case 15:
				strFieldName = "PolicytotalPayment";
				break;
			case 16:
				strFieldName = "ResponseCode";
				break;
			case 17:
				strFieldName = "ResultStatus";
				break;
			case 18:
				strFieldName = "ResultInfoDesc";
				break;
			case 19:
				strFieldName = "CiitcResultCode";
				break;
			case 20:
				strFieldName = "CiitcResultMessage";
				break;
			case 21:
				strFieldName = "MakeDate";
				break;
			case 22:
				strFieldName = "MakeTime";
				break;
			case 23:
				strFieldName = "ModifyDate";
				break;
			case 24:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Nationality") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicyType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicySource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicyStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EffectiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ExpireDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TerminationDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TerminationReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxFavorIndi") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PeriodPayment") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PolicytotalPayment") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ResponseCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResultStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResultInfoDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CiitcResultCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CiitcResultMessage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
