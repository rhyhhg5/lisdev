/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATrainerIndexDB;

/*
 * <p>ClassName: LATrainerIndexSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险培训组训信息表
 * @CreateDate：2018-05-03
 */
public class LATrainerIndexSchema implements Schema, Cloneable
{
	// @Field
	/** 序号 */
	private String Idx;
	/** 配置代码 */
	private String WageCode;
	/** 配置名称 */
	private String WageName;
	/** 组训人员工号 */
	private String TrainerCode;
	/** 管理机构 */
	private String PManageCom;
	/** 营业部 */
	private String AgentGroup;
	/** 组训姓名 */
	private String TrainerName;
	/** 组训职级 */
	private String TrainerGrade;
	/** 基本工资标准 */
	private double TransMoney;
	/** 基本工资标准系数 */
	private double TransMoneyRate;
	/** 绩效比例 */
	private double CommisionRate;
	/** 薪资年月 */
	private String WageNo;
	/** 岗位津贴 */
	private double JobSubsidies;
	/** 最低月新单期缴标准保费 */
	private double TransMoneyMin;
	/** 最高月新单期缴标准保费 */
	private double TransMoneyMax;
	/** 提奖比例 */
	private double Rate;
	/** 最低继续率 */
	private double PersistencyMin;
	/** 最高继续率 */
	private double PersistencyMax;
	/** 继续率系数 */
	private double PersistencyCoefficient;
	/** 最低举绩率 */
	private double LPRMin;
	/** 最高举绩率 */
	private double LPRMax;
	/** 举绩率系数 */
	private double LPRCoefficient;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 备用1 */
	private double A1;
	/** 备用2 */
	private double A2;
	/** 备用3 */
	private double A3;
	/** 备用4 */
	private double A4;
	/** 备用5 */
	private double A5;
	/** 备用6 */
	private double A6;
	/** 备用7 */
	private double A7;
	/** 备用8 */
	private double A8;
	/** 备用9 */
	private String A9;
	/** 备用10 */
	private String A10;
	/** 备用11 */
	private String A11;
	/** 备用12 */
	private String A12;
	/** 加扣款标识 */
	private String ADFlag;
	/** 加扣款类型 */
	private String ADState;
	/** 加扣款原因 */
	private String ADReason;
	/** 操作员 */
	private String Operator;

	public static final int FIELDNUM = 42;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LATrainerIndexSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Idx";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LATrainerIndexSchema cloned = (LATrainerIndexSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getIdx()
	{
		return Idx;
	}
	public void setIdx(String aIdx)
	{
		Idx = aIdx;
	}
	public String getWageCode()
	{
		return WageCode;
	}
	public void setWageCode(String aWageCode)
	{
		WageCode = aWageCode;
	}
	public String getWageName()
	{
		return WageName;
	}
	public void setWageName(String aWageName)
	{
		WageName = aWageName;
	}
	public String getTrainerCode()
	{
		return TrainerCode;
	}
	public void setTrainerCode(String aTrainerCode)
	{
		TrainerCode = aTrainerCode;
	}
	public String getPManageCom()
	{
		return PManageCom;
	}
	public void setPManageCom(String aPManageCom)
	{
		PManageCom = aPManageCom;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getTrainerName()
	{
		return TrainerName;
	}
	public void setTrainerName(String aTrainerName)
	{
		TrainerName = aTrainerName;
	}
	public String getTrainerGrade()
	{
		return TrainerGrade;
	}
	public void setTrainerGrade(String aTrainerGrade)
	{
		TrainerGrade = aTrainerGrade;
	}
	public double getTransMoney()
	{
		return TransMoney;
	}
	public void setTransMoney(double aTransMoney)
	{
		TransMoney = Arith.round(aTransMoney,2);
	}
	public void setTransMoney(String aTransMoney)
	{
		if (aTransMoney != null && !aTransMoney.equals(""))
		{
			Double tDouble = new Double(aTransMoney);
			double d = tDouble.doubleValue();
                TransMoney = Arith.round(d,2);
		}
	}

	public double getTransMoneyRate()
	{
		return TransMoneyRate;
	}
	public void setTransMoneyRate(double aTransMoneyRate)
	{
		TransMoneyRate = Arith.round(aTransMoneyRate,2);
	}
	public void setTransMoneyRate(String aTransMoneyRate)
	{
		if (aTransMoneyRate != null && !aTransMoneyRate.equals(""))
		{
			Double tDouble = new Double(aTransMoneyRate);
			double d = tDouble.doubleValue();
                TransMoneyRate = Arith.round(d,2);
		}
	}

	public double getCommisionRate()
	{
		return CommisionRate;
	}
	public void setCommisionRate(double aCommisionRate)
	{
		CommisionRate = Arith.round(aCommisionRate,2);
	}
	public void setCommisionRate(String aCommisionRate)
	{
		if (aCommisionRate != null && !aCommisionRate.equals(""))
		{
			Double tDouble = new Double(aCommisionRate);
			double d = tDouble.doubleValue();
                CommisionRate = Arith.round(d,2);
		}
	}

	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public double getJobSubsidies()
	{
		return JobSubsidies;
	}
	public void setJobSubsidies(double aJobSubsidies)
	{
		JobSubsidies = Arith.round(aJobSubsidies,2);
	}
	public void setJobSubsidies(String aJobSubsidies)
	{
		if (aJobSubsidies != null && !aJobSubsidies.equals(""))
		{
			Double tDouble = new Double(aJobSubsidies);
			double d = tDouble.doubleValue();
                JobSubsidies = Arith.round(d,2);
		}
	}

	public double getTransMoneyMin()
	{
		return TransMoneyMin;
	}
	public void setTransMoneyMin(double aTransMoneyMin)
	{
		TransMoneyMin = Arith.round(aTransMoneyMin,2);
	}
	public void setTransMoneyMin(String aTransMoneyMin)
	{
		if (aTransMoneyMin != null && !aTransMoneyMin.equals(""))
		{
			Double tDouble = new Double(aTransMoneyMin);
			double d = tDouble.doubleValue();
                TransMoneyMin = Arith.round(d,2);
		}
	}

	public double getTransMoneyMax()
	{
		return TransMoneyMax;
	}
	public void setTransMoneyMax(double aTransMoneyMax)
	{
		TransMoneyMax = Arith.round(aTransMoneyMax,2);
	}
	public void setTransMoneyMax(String aTransMoneyMax)
	{
		if (aTransMoneyMax != null && !aTransMoneyMax.equals(""))
		{
			Double tDouble = new Double(aTransMoneyMax);
			double d = tDouble.doubleValue();
                TransMoneyMax = Arith.round(d,2);
		}
	}

	public double getRate()
	{
		return Rate;
	}
	public void setRate(double aRate)
	{
		Rate = Arith.round(aRate,2);
	}
	public void setRate(String aRate)
	{
		if (aRate != null && !aRate.equals(""))
		{
			Double tDouble = new Double(aRate);
			double d = tDouble.doubleValue();
                Rate = Arith.round(d,2);
		}
	}

	public double getPersistencyMin()
	{
		return PersistencyMin;
	}
	public void setPersistencyMin(double aPersistencyMin)
	{
		PersistencyMin = Arith.round(aPersistencyMin,2);
	}
	public void setPersistencyMin(String aPersistencyMin)
	{
		if (aPersistencyMin != null && !aPersistencyMin.equals(""))
		{
			Double tDouble = new Double(aPersistencyMin);
			double d = tDouble.doubleValue();
                PersistencyMin = Arith.round(d,2);
		}
	}

	public double getPersistencyMax()
	{
		return PersistencyMax;
	}
	public void setPersistencyMax(double aPersistencyMax)
	{
		PersistencyMax = Arith.round(aPersistencyMax,2);
	}
	public void setPersistencyMax(String aPersistencyMax)
	{
		if (aPersistencyMax != null && !aPersistencyMax.equals(""))
		{
			Double tDouble = new Double(aPersistencyMax);
			double d = tDouble.doubleValue();
                PersistencyMax = Arith.round(d,2);
		}
	}

	public double getPersistencyCoefficient()
	{
		return PersistencyCoefficient;
	}
	public void setPersistencyCoefficient(double aPersistencyCoefficient)
	{
		PersistencyCoefficient = Arith.round(aPersistencyCoefficient,2);
	}
	public void setPersistencyCoefficient(String aPersistencyCoefficient)
	{
		if (aPersistencyCoefficient != null && !aPersistencyCoefficient.equals(""))
		{
			Double tDouble = new Double(aPersistencyCoefficient);
			double d = tDouble.doubleValue();
                PersistencyCoefficient = Arith.round(d,2);
		}
	}

	public double getLPRMin()
	{
		return LPRMin;
	}
	public void setLPRMin(double aLPRMin)
	{
		LPRMin = Arith.round(aLPRMin,2);
	}
	public void setLPRMin(String aLPRMin)
	{
		if (aLPRMin != null && !aLPRMin.equals(""))
		{
			Double tDouble = new Double(aLPRMin);
			double d = tDouble.doubleValue();
                LPRMin = Arith.round(d,2);
		}
	}

	public double getLPRMax()
	{
		return LPRMax;
	}
	public void setLPRMax(double aLPRMax)
	{
		LPRMax = Arith.round(aLPRMax,2);
	}
	public void setLPRMax(String aLPRMax)
	{
		if (aLPRMax != null && !aLPRMax.equals(""))
		{
			Double tDouble = new Double(aLPRMax);
			double d = tDouble.doubleValue();
                LPRMax = Arith.round(d,2);
		}
	}

	public double getLPRCoefficient()
	{
		return LPRCoefficient;
	}
	public void setLPRCoefficient(double aLPRCoefficient)
	{
		LPRCoefficient = Arith.round(aLPRCoefficient,2);
	}
	public void setLPRCoefficient(String aLPRCoefficient)
	{
		if (aLPRCoefficient != null && !aLPRCoefficient.equals(""))
		{
			Double tDouble = new Double(aLPRCoefficient);
			double d = tDouble.doubleValue();
                LPRCoefficient = Arith.round(d,2);
		}
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public double getA1()
	{
		return A1;
	}
	public void setA1(double aA1)
	{
		A1 = Arith.round(aA1,2);
	}
	public void setA1(String aA1)
	{
		if (aA1 != null && !aA1.equals(""))
		{
			Double tDouble = new Double(aA1);
			double d = tDouble.doubleValue();
                A1 = Arith.round(d,2);
		}
	}

	public double getA2()
	{
		return A2;
	}
	public void setA2(double aA2)
	{
		A2 = Arith.round(aA2,2);
	}
	public void setA2(String aA2)
	{
		if (aA2 != null && !aA2.equals(""))
		{
			Double tDouble = new Double(aA2);
			double d = tDouble.doubleValue();
                A2 = Arith.round(d,2);
		}
	}

	public double getA3()
	{
		return A3;
	}
	public void setA3(double aA3)
	{
		A3 = Arith.round(aA3,2);
	}
	public void setA3(String aA3)
	{
		if (aA3 != null && !aA3.equals(""))
		{
			Double tDouble = new Double(aA3);
			double d = tDouble.doubleValue();
                A3 = Arith.round(d,2);
		}
	}

	public double getA4()
	{
		return A4;
	}
	public void setA4(double aA4)
	{
		A4 = Arith.round(aA4,2);
	}
	public void setA4(String aA4)
	{
		if (aA4 != null && !aA4.equals(""))
		{
			Double tDouble = new Double(aA4);
			double d = tDouble.doubleValue();
                A4 = Arith.round(d,2);
		}
	}

	public double getA5()
	{
		return A5;
	}
	public void setA5(double aA5)
	{
		A5 = Arith.round(aA5,2);
	}
	public void setA5(String aA5)
	{
		if (aA5 != null && !aA5.equals(""))
		{
			Double tDouble = new Double(aA5);
			double d = tDouble.doubleValue();
                A5 = Arith.round(d,2);
		}
	}

	public double getA6()
	{
		return A6;
	}
	public void setA6(double aA6)
	{
		A6 = Arith.round(aA6,2);
	}
	public void setA6(String aA6)
	{
		if (aA6 != null && !aA6.equals(""))
		{
			Double tDouble = new Double(aA6);
			double d = tDouble.doubleValue();
                A6 = Arith.round(d,2);
		}
	}

	public double getA7()
	{
		return A7;
	}
	public void setA7(double aA7)
	{
		A7 = Arith.round(aA7,2);
	}
	public void setA7(String aA7)
	{
		if (aA7 != null && !aA7.equals(""))
		{
			Double tDouble = new Double(aA7);
			double d = tDouble.doubleValue();
                A7 = Arith.round(d,2);
		}
	}

	public double getA8()
	{
		return A8;
	}
	public void setA8(double aA8)
	{
		A8 = Arith.round(aA8,2);
	}
	public void setA8(String aA8)
	{
		if (aA8 != null && !aA8.equals(""))
		{
			Double tDouble = new Double(aA8);
			double d = tDouble.doubleValue();
                A8 = Arith.round(d,2);
		}
	}

	public String getA9()
	{
		return A9;
	}
	public void setA9(String aA9)
	{
		A9 = aA9;
	}
	public String getA10()
	{
		return A10;
	}
	public void setA10(String aA10)
	{
		A10 = aA10;
	}
	public String getA11()
	{
		return A11;
	}
	public void setA11(String aA11)
	{
		A11 = aA11;
	}
	public String getA12()
	{
		return A12;
	}
	public void setA12(String aA12)
	{
		A12 = aA12;
	}
	public String getADFlag()
	{
		return ADFlag;
	}
	public void setADFlag(String aADFlag)
	{
		ADFlag = aADFlag;
	}
	public String getADState()
	{
		return ADState;
	}
	public void setADState(String aADState)
	{
		ADState = aADState;
	}
	public String getADReason()
	{
		return ADReason;
	}
	public void setADReason(String aADReason)
	{
		ADReason = aADReason;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}

	/**
	* 使用另外一个 LATrainerIndexSchema 对象给 Schema 赋值
	* @param: aLATrainerIndexSchema LATrainerIndexSchema
	**/
	public void setSchema(LATrainerIndexSchema aLATrainerIndexSchema)
	{
		this.Idx = aLATrainerIndexSchema.getIdx();
		this.WageCode = aLATrainerIndexSchema.getWageCode();
		this.WageName = aLATrainerIndexSchema.getWageName();
		this.TrainerCode = aLATrainerIndexSchema.getTrainerCode();
		this.PManageCom = aLATrainerIndexSchema.getPManageCom();
		this.AgentGroup = aLATrainerIndexSchema.getAgentGroup();
		this.TrainerName = aLATrainerIndexSchema.getTrainerName();
		this.TrainerGrade = aLATrainerIndexSchema.getTrainerGrade();
		this.TransMoney = aLATrainerIndexSchema.getTransMoney();
		this.TransMoneyRate = aLATrainerIndexSchema.getTransMoneyRate();
		this.CommisionRate = aLATrainerIndexSchema.getCommisionRate();
		this.WageNo = aLATrainerIndexSchema.getWageNo();
		this.JobSubsidies = aLATrainerIndexSchema.getJobSubsidies();
		this.TransMoneyMin = aLATrainerIndexSchema.getTransMoneyMin();
		this.TransMoneyMax = aLATrainerIndexSchema.getTransMoneyMax();
		this.Rate = aLATrainerIndexSchema.getRate();
		this.PersistencyMin = aLATrainerIndexSchema.getPersistencyMin();
		this.PersistencyMax = aLATrainerIndexSchema.getPersistencyMax();
		this.PersistencyCoefficient = aLATrainerIndexSchema.getPersistencyCoefficient();
		this.LPRMin = aLATrainerIndexSchema.getLPRMin();
		this.LPRMax = aLATrainerIndexSchema.getLPRMax();
		this.LPRCoefficient = aLATrainerIndexSchema.getLPRCoefficient();
		this.MakeDate = fDate.getDate( aLATrainerIndexSchema.getMakeDate());
		this.MakeTime = aLATrainerIndexSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLATrainerIndexSchema.getModifyDate());
		this.ModifyTime = aLATrainerIndexSchema.getModifyTime();
		this.A1 = aLATrainerIndexSchema.getA1();
		this.A2 = aLATrainerIndexSchema.getA2();
		this.A3 = aLATrainerIndexSchema.getA3();
		this.A4 = aLATrainerIndexSchema.getA4();
		this.A5 = aLATrainerIndexSchema.getA5();
		this.A6 = aLATrainerIndexSchema.getA6();
		this.A7 = aLATrainerIndexSchema.getA7();
		this.A8 = aLATrainerIndexSchema.getA8();
		this.A9 = aLATrainerIndexSchema.getA9();
		this.A10 = aLATrainerIndexSchema.getA10();
		this.A11 = aLATrainerIndexSchema.getA11();
		this.A12 = aLATrainerIndexSchema.getA12();
		this.ADFlag = aLATrainerIndexSchema.getADFlag();
		this.ADState = aLATrainerIndexSchema.getADState();
		this.ADReason = aLATrainerIndexSchema.getADReason();
		this.Operator = aLATrainerIndexSchema.getOperator();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Idx") == null )
				this.Idx = null;
			else
				this.Idx = rs.getString("Idx").trim();

			if( rs.getString("WageCode") == null )
				this.WageCode = null;
			else
				this.WageCode = rs.getString("WageCode").trim();

			if( rs.getString("WageName") == null )
				this.WageName = null;
			else
				this.WageName = rs.getString("WageName").trim();

			if( rs.getString("TrainerCode") == null )
				this.TrainerCode = null;
			else
				this.TrainerCode = rs.getString("TrainerCode").trim();

			if( rs.getString("PManageCom") == null )
				this.PManageCom = null;
			else
				this.PManageCom = rs.getString("PManageCom").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("TrainerName") == null )
				this.TrainerName = null;
			else
				this.TrainerName = rs.getString("TrainerName").trim();

			if( rs.getString("TrainerGrade") == null )
				this.TrainerGrade = null;
			else
				this.TrainerGrade = rs.getString("TrainerGrade").trim();

			this.TransMoney = rs.getDouble("TransMoney");
			this.TransMoneyRate = rs.getDouble("TransMoneyRate");
			this.CommisionRate = rs.getDouble("CommisionRate");
			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			this.JobSubsidies = rs.getDouble("JobSubsidies");
			this.TransMoneyMin = rs.getDouble("TransMoneyMin");
			this.TransMoneyMax = rs.getDouble("TransMoneyMax");
			this.Rate = rs.getDouble("Rate");
			this.PersistencyMin = rs.getDouble("PersistencyMin");
			this.PersistencyMax = rs.getDouble("PersistencyMax");
			this.PersistencyCoefficient = rs.getDouble("PersistencyCoefficient");
			this.LPRMin = rs.getDouble("LPRMin");
			this.LPRMax = rs.getDouble("LPRMax");
			this.LPRCoefficient = rs.getDouble("LPRCoefficient");
			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.A1 = rs.getDouble("A1");
			this.A2 = rs.getDouble("A2");
			this.A3 = rs.getDouble("A3");
			this.A4 = rs.getDouble("A4");
			this.A5 = rs.getDouble("A5");
			this.A6 = rs.getDouble("A6");
			this.A7 = rs.getDouble("A7");
			this.A8 = rs.getDouble("A8");
			if( rs.getString("A9") == null )
				this.A9 = null;
			else
				this.A9 = rs.getString("A9").trim();

			if( rs.getString("A10") == null )
				this.A10 = null;
			else
				this.A10 = rs.getString("A10").trim();

			if( rs.getString("A11") == null )
				this.A11 = null;
			else
				this.A11 = rs.getString("A11").trim();

			if( rs.getString("A12") == null )
				this.A12 = null;
			else
				this.A12 = rs.getString("A12").trim();

			if( rs.getString("ADFlag") == null )
				this.ADFlag = null;
			else
				this.ADFlag = rs.getString("ADFlag").trim();

			if( rs.getString("ADState") == null )
				this.ADState = null;
			else
				this.ADState = rs.getString("ADState").trim();

			if( rs.getString("ADReason") == null )
				this.ADReason = null;
			else
				this.ADReason = rs.getString("ADReason").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LATrainerIndex表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainerIndexSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LATrainerIndexSchema getSchema()
	{
		LATrainerIndexSchema aLATrainerIndexSchema = new LATrainerIndexSchema();
		aLATrainerIndexSchema.setSchema(this);
		return aLATrainerIndexSchema;
	}

	public LATrainerIndexDB getDB()
	{
		LATrainerIndexDB aDBOper = new LATrainerIndexDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainerIndex描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Idx)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainerCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainerGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransMoneyRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CommisionRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(JobSubsidies));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransMoneyMin));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransMoneyMax));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Rate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PersistencyMin));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PersistencyMax));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PersistencyCoefficient));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LPRMin));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LPRMax));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LPRCoefficient));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(A1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(A2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(A3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(A4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(A5));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(A6));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(A7));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(A8));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(A9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(A10)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(A11)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(A12)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ADFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ADState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ADReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainerIndex>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Idx = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			WageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			WageName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TrainerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			TrainerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			TrainerGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			TransMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			TransMoneyRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			CommisionRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			JobSubsidies = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			TransMoneyMin = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			TransMoneyMax = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			PersistencyMin = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			PersistencyMax = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			PersistencyCoefficient = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			LPRMin = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			LPRMax = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			LPRCoefficient = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			A1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			A2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			A3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			A4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			A5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			A6 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			A7 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).doubleValue();
			A8 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).doubleValue();
			A9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			A10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			A11 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			A12 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			ADFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			ADState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			ADReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainerIndexSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Idx"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
		}
		if (FCode.equals("WageCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageCode));
		}
		if (FCode.equals("WageName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageName));
		}
		if (FCode.equals("TrainerCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerCode));
		}
		if (FCode.equals("PManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PManageCom));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("TrainerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerName));
		}
		if (FCode.equals("TrainerGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerGrade));
		}
		if (FCode.equals("TransMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMoney));
		}
		if (FCode.equals("TransMoneyRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMoneyRate));
		}
		if (FCode.equals("CommisionRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommisionRate));
		}
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("JobSubsidies"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(JobSubsidies));
		}
		if (FCode.equals("TransMoneyMin"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMoneyMin));
		}
		if (FCode.equals("TransMoneyMax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMoneyMax));
		}
		if (FCode.equals("Rate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rate));
		}
		if (FCode.equals("PersistencyMin"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersistencyMin));
		}
		if (FCode.equals("PersistencyMax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersistencyMax));
		}
		if (FCode.equals("PersistencyCoefficient"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersistencyCoefficient));
		}
		if (FCode.equals("LPRMin"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LPRMin));
		}
		if (FCode.equals("LPRMax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LPRMax));
		}
		if (FCode.equals("LPRCoefficient"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LPRCoefficient));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("A1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A1));
		}
		if (FCode.equals("A2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A2));
		}
		if (FCode.equals("A3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A3));
		}
		if (FCode.equals("A4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A4));
		}
		if (FCode.equals("A5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A5));
		}
		if (FCode.equals("A6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A6));
		}
		if (FCode.equals("A7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A7));
		}
		if (FCode.equals("A8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A8));
		}
		if (FCode.equals("A9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A9));
		}
		if (FCode.equals("A10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A10));
		}
		if (FCode.equals("A11"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A11));
		}
		if (FCode.equals("A12"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(A12));
		}
		if (FCode.equals("ADFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ADFlag));
		}
		if (FCode.equals("ADState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ADState));
		}
		if (FCode.equals("ADReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ADReason));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Idx);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(WageCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(WageName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TrainerCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(PManageCom);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(TrainerName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(TrainerGrade);
				break;
			case 8:
				strFieldValue = String.valueOf(TransMoney);
				break;
			case 9:
				strFieldValue = String.valueOf(TransMoneyRate);
				break;
			case 10:
				strFieldValue = String.valueOf(CommisionRate);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 12:
				strFieldValue = String.valueOf(JobSubsidies);
				break;
			case 13:
				strFieldValue = String.valueOf(TransMoneyMin);
				break;
			case 14:
				strFieldValue = String.valueOf(TransMoneyMax);
				break;
			case 15:
				strFieldValue = String.valueOf(Rate);
				break;
			case 16:
				strFieldValue = String.valueOf(PersistencyMin);
				break;
			case 17:
				strFieldValue = String.valueOf(PersistencyMax);
				break;
			case 18:
				strFieldValue = String.valueOf(PersistencyCoefficient);
				break;
			case 19:
				strFieldValue = String.valueOf(LPRMin);
				break;
			case 20:
				strFieldValue = String.valueOf(LPRMax);
				break;
			case 21:
				strFieldValue = String.valueOf(LPRCoefficient);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 26:
				strFieldValue = String.valueOf(A1);
				break;
			case 27:
				strFieldValue = String.valueOf(A2);
				break;
			case 28:
				strFieldValue = String.valueOf(A3);
				break;
			case 29:
				strFieldValue = String.valueOf(A4);
				break;
			case 30:
				strFieldValue = String.valueOf(A5);
				break;
			case 31:
				strFieldValue = String.valueOf(A6);
				break;
			case 32:
				strFieldValue = String.valueOf(A7);
				break;
			case 33:
				strFieldValue = String.valueOf(A8);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(A9);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(A10);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(A11);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(A12);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(ADFlag);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(ADState);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(ADReason);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Idx"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Idx = FValue.trim();
			}
			else
				Idx = null;
		}
		if (FCode.equalsIgnoreCase("WageCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageCode = FValue.trim();
			}
			else
				WageCode = null;
		}
		if (FCode.equalsIgnoreCase("WageName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageName = FValue.trim();
			}
			else
				WageName = null;
		}
		if (FCode.equalsIgnoreCase("TrainerCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerCode = FValue.trim();
			}
			else
				TrainerCode = null;
		}
		if (FCode.equalsIgnoreCase("PManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PManageCom = FValue.trim();
			}
			else
				PManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("TrainerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerName = FValue.trim();
			}
			else
				TrainerName = null;
		}
		if (FCode.equalsIgnoreCase("TrainerGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerGrade = FValue.trim();
			}
			else
				TrainerGrade = null;
		}
		if (FCode.equalsIgnoreCase("TransMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("TransMoneyRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransMoneyRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("CommisionRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CommisionRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("JobSubsidies"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				JobSubsidies = d;
			}
		}
		if (FCode.equalsIgnoreCase("TransMoneyMin"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransMoneyMin = d;
			}
		}
		if (FCode.equalsIgnoreCase("TransMoneyMax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransMoneyMax = d;
			}
		}
		if (FCode.equalsIgnoreCase("Rate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Rate = d;
			}
		}
		if (FCode.equalsIgnoreCase("PersistencyMin"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PersistencyMin = d;
			}
		}
		if (FCode.equalsIgnoreCase("PersistencyMax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PersistencyMax = d;
			}
		}
		if (FCode.equalsIgnoreCase("PersistencyCoefficient"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PersistencyCoefficient = d;
			}
		}
		if (FCode.equalsIgnoreCase("LPRMin"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LPRMin = d;
			}
		}
		if (FCode.equalsIgnoreCase("LPRMax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LPRMax = d;
			}
		}
		if (FCode.equalsIgnoreCase("LPRCoefficient"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LPRCoefficient = d;
			}
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("A1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				A1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("A2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				A2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("A3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				A3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("A4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				A4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("A5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				A5 = d;
			}
		}
		if (FCode.equalsIgnoreCase("A6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				A6 = d;
			}
		}
		if (FCode.equalsIgnoreCase("A7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				A7 = d;
			}
		}
		if (FCode.equalsIgnoreCase("A8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				A8 = d;
			}
		}
		if (FCode.equalsIgnoreCase("A9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				A9 = FValue.trim();
			}
			else
				A9 = null;
		}
		if (FCode.equalsIgnoreCase("A10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				A10 = FValue.trim();
			}
			else
				A10 = null;
		}
		if (FCode.equalsIgnoreCase("A11"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				A11 = FValue.trim();
			}
			else
				A11 = null;
		}
		if (FCode.equalsIgnoreCase("A12"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				A12 = FValue.trim();
			}
			else
				A12 = null;
		}
		if (FCode.equalsIgnoreCase("ADFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ADFlag = FValue.trim();
			}
			else
				ADFlag = null;
		}
		if (FCode.equalsIgnoreCase("ADState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ADState = FValue.trim();
			}
			else
				ADState = null;
		}
		if (FCode.equalsIgnoreCase("ADReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ADReason = FValue.trim();
			}
			else
				ADReason = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LATrainerIndexSchema other = (LATrainerIndexSchema)otherObject;
		return
			(Idx == null ? other.getIdx() == null : Idx.equals(other.getIdx()))
			&& (WageCode == null ? other.getWageCode() == null : WageCode.equals(other.getWageCode()))
			&& (WageName == null ? other.getWageName() == null : WageName.equals(other.getWageName()))
			&& (TrainerCode == null ? other.getTrainerCode() == null : TrainerCode.equals(other.getTrainerCode()))
			&& (PManageCom == null ? other.getPManageCom() == null : PManageCom.equals(other.getPManageCom()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (TrainerName == null ? other.getTrainerName() == null : TrainerName.equals(other.getTrainerName()))
			&& (TrainerGrade == null ? other.getTrainerGrade() == null : TrainerGrade.equals(other.getTrainerGrade()))
			&& TransMoney == other.getTransMoney()
			&& TransMoneyRate == other.getTransMoneyRate()
			&& CommisionRate == other.getCommisionRate()
			&& (WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& JobSubsidies == other.getJobSubsidies()
			&& TransMoneyMin == other.getTransMoneyMin()
			&& TransMoneyMax == other.getTransMoneyMax()
			&& Rate == other.getRate()
			&& PersistencyMin == other.getPersistencyMin()
			&& PersistencyMax == other.getPersistencyMax()
			&& PersistencyCoefficient == other.getPersistencyCoefficient()
			&& LPRMin == other.getLPRMin()
			&& LPRMax == other.getLPRMax()
			&& LPRCoefficient == other.getLPRCoefficient()
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& A1 == other.getA1()
			&& A2 == other.getA2()
			&& A3 == other.getA3()
			&& A4 == other.getA4()
			&& A5 == other.getA5()
			&& A6 == other.getA6()
			&& A7 == other.getA7()
			&& A8 == other.getA8()
			&& (A9 == null ? other.getA9() == null : A9.equals(other.getA9()))
			&& (A10 == null ? other.getA10() == null : A10.equals(other.getA10()))
			&& (A11 == null ? other.getA11() == null : A11.equals(other.getA11()))
			&& (A12 == null ? other.getA12() == null : A12.equals(other.getA12()))
			&& (ADFlag == null ? other.getADFlag() == null : ADFlag.equals(other.getADFlag()))
			&& (ADState == null ? other.getADState() == null : ADState.equals(other.getADState()))
			&& (ADReason == null ? other.getADReason() == null : ADReason.equals(other.getADReason()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Idx") ) {
			return 0;
		}
		if( strFieldName.equals("WageCode") ) {
			return 1;
		}
		if( strFieldName.equals("WageName") ) {
			return 2;
		}
		if( strFieldName.equals("TrainerCode") ) {
			return 3;
		}
		if( strFieldName.equals("PManageCom") ) {
			return 4;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 5;
		}
		if( strFieldName.equals("TrainerName") ) {
			return 6;
		}
		if( strFieldName.equals("TrainerGrade") ) {
			return 7;
		}
		if( strFieldName.equals("TransMoney") ) {
			return 8;
		}
		if( strFieldName.equals("TransMoneyRate") ) {
			return 9;
		}
		if( strFieldName.equals("CommisionRate") ) {
			return 10;
		}
		if( strFieldName.equals("WageNo") ) {
			return 11;
		}
		if( strFieldName.equals("JobSubsidies") ) {
			return 12;
		}
		if( strFieldName.equals("TransMoneyMin") ) {
			return 13;
		}
		if( strFieldName.equals("TransMoneyMax") ) {
			return 14;
		}
		if( strFieldName.equals("Rate") ) {
			return 15;
		}
		if( strFieldName.equals("PersistencyMin") ) {
			return 16;
		}
		if( strFieldName.equals("PersistencyMax") ) {
			return 17;
		}
		if( strFieldName.equals("PersistencyCoefficient") ) {
			return 18;
		}
		if( strFieldName.equals("LPRMin") ) {
			return 19;
		}
		if( strFieldName.equals("LPRMax") ) {
			return 20;
		}
		if( strFieldName.equals("LPRCoefficient") ) {
			return 21;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 22;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 23;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 25;
		}
		if( strFieldName.equals("A1") ) {
			return 26;
		}
		if( strFieldName.equals("A2") ) {
			return 27;
		}
		if( strFieldName.equals("A3") ) {
			return 28;
		}
		if( strFieldName.equals("A4") ) {
			return 29;
		}
		if( strFieldName.equals("A5") ) {
			return 30;
		}
		if( strFieldName.equals("A6") ) {
			return 31;
		}
		if( strFieldName.equals("A7") ) {
			return 32;
		}
		if( strFieldName.equals("A8") ) {
			return 33;
		}
		if( strFieldName.equals("A9") ) {
			return 34;
		}
		if( strFieldName.equals("A10") ) {
			return 35;
		}
		if( strFieldName.equals("A11") ) {
			return 36;
		}
		if( strFieldName.equals("A12") ) {
			return 37;
		}
		if( strFieldName.equals("ADFlag") ) {
			return 38;
		}
		if( strFieldName.equals("ADState") ) {
			return 39;
		}
		if( strFieldName.equals("ADReason") ) {
			return 40;
		}
		if( strFieldName.equals("Operator") ) {
			return 41;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Idx";
				break;
			case 1:
				strFieldName = "WageCode";
				break;
			case 2:
				strFieldName = "WageName";
				break;
			case 3:
				strFieldName = "TrainerCode";
				break;
			case 4:
				strFieldName = "PManageCom";
				break;
			case 5:
				strFieldName = "AgentGroup";
				break;
			case 6:
				strFieldName = "TrainerName";
				break;
			case 7:
				strFieldName = "TrainerGrade";
				break;
			case 8:
				strFieldName = "TransMoney";
				break;
			case 9:
				strFieldName = "TransMoneyRate";
				break;
			case 10:
				strFieldName = "CommisionRate";
				break;
			case 11:
				strFieldName = "WageNo";
				break;
			case 12:
				strFieldName = "JobSubsidies";
				break;
			case 13:
				strFieldName = "TransMoneyMin";
				break;
			case 14:
				strFieldName = "TransMoneyMax";
				break;
			case 15:
				strFieldName = "Rate";
				break;
			case 16:
				strFieldName = "PersistencyMin";
				break;
			case 17:
				strFieldName = "PersistencyMax";
				break;
			case 18:
				strFieldName = "PersistencyCoefficient";
				break;
			case 19:
				strFieldName = "LPRMin";
				break;
			case 20:
				strFieldName = "LPRMax";
				break;
			case 21:
				strFieldName = "LPRCoefficient";
				break;
			case 22:
				strFieldName = "MakeDate";
				break;
			case 23:
				strFieldName = "MakeTime";
				break;
			case 24:
				strFieldName = "ModifyDate";
				break;
			case 25:
				strFieldName = "ModifyTime";
				break;
			case 26:
				strFieldName = "A1";
				break;
			case 27:
				strFieldName = "A2";
				break;
			case 28:
				strFieldName = "A3";
				break;
			case 29:
				strFieldName = "A4";
				break;
			case 30:
				strFieldName = "A5";
				break;
			case 31:
				strFieldName = "A6";
				break;
			case 32:
				strFieldName = "A7";
				break;
			case 33:
				strFieldName = "A8";
				break;
			case 34:
				strFieldName = "A9";
				break;
			case 35:
				strFieldName = "A10";
				break;
			case 36:
				strFieldName = "A11";
				break;
			case 37:
				strFieldName = "A12";
				break;
			case 38:
				strFieldName = "ADFlag";
				break;
			case 39:
				strFieldName = "ADState";
				break;
			case 40:
				strFieldName = "ADReason";
				break;
			case 41:
				strFieldName = "Operator";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Idx") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TransMoneyRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CommisionRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JobSubsidies") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TransMoneyMin") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TransMoneyMax") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Rate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PersistencyMin") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PersistencyMax") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PersistencyCoefficient") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("LPRMin") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("LPRMax") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("LPRCoefficient") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("A1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("A2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("A3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("A4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("A5") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("A6") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("A7") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("A8") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("A9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("A10") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("A11") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("A12") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ADFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ADState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ADReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 33:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
