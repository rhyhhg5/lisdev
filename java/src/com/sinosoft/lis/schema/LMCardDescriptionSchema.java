/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMCardDescriptionDB;

/*
 * <p>ClassName: LMCardDescriptionSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2013-07-18
 */
public class LMCardDescriptionSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 单证编码 */
	private String CertifyCode;
	/** 单证类型码 */
	private String SubCode;
	/** 单证名称 */
	private String CertifyName;
	/** 是否是有价单证 */
	private String HavePrice;
	/** 是否是有号单证 */
	private String HaveNumber;
	/** 单价 */
	private double Price;
	/** 单证所属类型 */
	private String CertifyType;
	/** 管理机构 */
	private String ComCode;
	/** 印刷厂编码 */
	private String PrintCode;
	/** 预警线 */
	private int WarningNo;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 单证类型 */
	private String CertifyClass;
	/** 单证号码长度 */
	private int CertifyLength;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LMCardDescriptionSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LMCardDescriptionSchema cloned = (LMCardDescriptionSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getCertifyCode()
	{
		return CertifyCode;
	}
	public void setCertifyCode(String aCertifyCode)
	{
		CertifyCode = aCertifyCode;
	}
	public String getSubCode()
	{
		return SubCode;
	}
	public void setSubCode(String aSubCode)
	{
		SubCode = aSubCode;
	}
	public String getCertifyName()
	{
		return CertifyName;
	}
	public void setCertifyName(String aCertifyName)
	{
		CertifyName = aCertifyName;
	}
	public String getHavePrice()
	{
		return HavePrice;
	}
	public void setHavePrice(String aHavePrice)
	{
		HavePrice = aHavePrice;
	}
	public String getHaveNumber()
	{
		return HaveNumber;
	}
	public void setHaveNumber(String aHaveNumber)
	{
		HaveNumber = aHaveNumber;
	}
	public double getPrice()
	{
		return Price;
	}
	public void setPrice(double aPrice)
	{
		Price = Arith.round(aPrice,2);
	}
	public void setPrice(String aPrice)
	{
		if (aPrice != null && !aPrice.equals(""))
		{
			Double tDouble = new Double(aPrice);
			double d = tDouble.doubleValue();
                Price = Arith.round(d,2);
		}
	}

	public String getCertifyType()
	{
		return CertifyType;
	}
	public void setCertifyType(String aCertifyType)
	{
		CertifyType = aCertifyType;
	}
	public String getComCode()
	{
		return ComCode;
	}
	public void setComCode(String aComCode)
	{
		ComCode = aComCode;
	}
	public String getPrintCode()
	{
		return PrintCode;
	}
	public void setPrintCode(String aPrintCode)
	{
		PrintCode = aPrintCode;
	}
	public int getWarningNo()
	{
		return WarningNo;
	}
	public void setWarningNo(int aWarningNo)
	{
		WarningNo = aWarningNo;
	}
	public void setWarningNo(String aWarningNo)
	{
		if (aWarningNo != null && !aWarningNo.equals(""))
		{
			Integer tInteger = new Integer(aWarningNo);
			int i = tInteger.intValue();
			WarningNo = i;
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getCertifyClass()
	{
		return CertifyClass;
	}
	public void setCertifyClass(String aCertifyClass)
	{
		CertifyClass = aCertifyClass;
	}
	public int getCertifyLength()
	{
		return CertifyLength;
	}
	public void setCertifyLength(int aCertifyLength)
	{
		CertifyLength = aCertifyLength;
	}
	public void setCertifyLength(String aCertifyLength)
	{
		if (aCertifyLength != null && !aCertifyLength.equals(""))
		{
			Integer tInteger = new Integer(aCertifyLength);
			int i = tInteger.intValue();
			CertifyLength = i;
		}
	}


	/**
	* 使用另外一个 LMCardDescriptionSchema 对象给 Schema 赋值
	* @param: aLMCardDescriptionSchema LMCardDescriptionSchema
	**/
	public void setSchema(LMCardDescriptionSchema aLMCardDescriptionSchema)
	{
		this.SerialNo = aLMCardDescriptionSchema.getSerialNo();
		this.CertifyCode = aLMCardDescriptionSchema.getCertifyCode();
		this.SubCode = aLMCardDescriptionSchema.getSubCode();
		this.CertifyName = aLMCardDescriptionSchema.getCertifyName();
		this.HavePrice = aLMCardDescriptionSchema.getHavePrice();
		this.HaveNumber = aLMCardDescriptionSchema.getHaveNumber();
		this.Price = aLMCardDescriptionSchema.getPrice();
		this.CertifyType = aLMCardDescriptionSchema.getCertifyType();
		this.ComCode = aLMCardDescriptionSchema.getComCode();
		this.PrintCode = aLMCardDescriptionSchema.getPrintCode();
		this.WarningNo = aLMCardDescriptionSchema.getWarningNo();
		this.Operator = aLMCardDescriptionSchema.getOperator();
		this.MakeDate = fDate.getDate( aLMCardDescriptionSchema.getMakeDate());
		this.MakeTime = aLMCardDescriptionSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLMCardDescriptionSchema.getModifyDate());
		this.ModifyTime = aLMCardDescriptionSchema.getModifyTime();
		this.CertifyClass = aLMCardDescriptionSchema.getCertifyClass();
		this.CertifyLength = aLMCardDescriptionSchema.getCertifyLength();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("CertifyCode") == null )
				this.CertifyCode = null;
			else
				this.CertifyCode = rs.getString("CertifyCode").trim();

			if( rs.getString("SubCode") == null )
				this.SubCode = null;
			else
				this.SubCode = rs.getString("SubCode").trim();

			if( rs.getString("CertifyName") == null )
				this.CertifyName = null;
			else
				this.CertifyName = rs.getString("CertifyName").trim();

			if( rs.getString("HavePrice") == null )
				this.HavePrice = null;
			else
				this.HavePrice = rs.getString("HavePrice").trim();

			if( rs.getString("HaveNumber") == null )
				this.HaveNumber = null;
			else
				this.HaveNumber = rs.getString("HaveNumber").trim();

			this.Price = rs.getDouble("Price");
			if( rs.getString("CertifyType") == null )
				this.CertifyType = null;
			else
				this.CertifyType = rs.getString("CertifyType").trim();

			if( rs.getString("ComCode") == null )
				this.ComCode = null;
			else
				this.ComCode = rs.getString("ComCode").trim();

			if( rs.getString("PrintCode") == null )
				this.PrintCode = null;
			else
				this.PrintCode = rs.getString("PrintCode").trim();

			this.WarningNo = rs.getInt("WarningNo");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("CertifyClass") == null )
				this.CertifyClass = null;
			else
				this.CertifyClass = rs.getString("CertifyClass").trim();

			this.CertifyLength = rs.getInt("CertifyLength");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LMCardDescription表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMCardDescriptionSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LMCardDescriptionSchema getSchema()
	{
		LMCardDescriptionSchema aLMCardDescriptionSchema = new LMCardDescriptionSchema();
		aLMCardDescriptionSchema.setSchema(this);
		return aLMCardDescriptionSchema;
	}

	public LMCardDescriptionDB getDB()
	{
		LMCardDescriptionDB aDBOper = new LMCardDescriptionDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMCardDescription描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SubCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HavePrice)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HaveNumber)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Price));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrintCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(WarningNo));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifyClass)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CertifyLength));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMCardDescription>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SubCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CertifyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			HavePrice = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			HaveNumber = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Price = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			CertifyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			PrintCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			WarningNo= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).intValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CertifyClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			CertifyLength= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).intValue();
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMCardDescriptionSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("CertifyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
		}
		if (FCode.equals("SubCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SubCode));
		}
		if (FCode.equals("CertifyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyName));
		}
		if (FCode.equals("HavePrice"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HavePrice));
		}
		if (FCode.equals("HaveNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HaveNumber));
		}
		if (FCode.equals("Price"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Price));
		}
		if (FCode.equals("CertifyType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyType));
		}
		if (FCode.equals("ComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
		}
		if (FCode.equals("PrintCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCode));
		}
		if (FCode.equals("WarningNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WarningNo));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("CertifyClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyClass));
		}
		if (FCode.equals("CertifyLength"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyLength));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CertifyCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SubCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CertifyName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(HavePrice);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(HaveNumber);
				break;
			case 6:
				strFieldValue = String.valueOf(Price);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CertifyType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ComCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(PrintCode);
				break;
			case 10:
				strFieldValue = String.valueOf(WarningNo);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(CertifyClass);
				break;
			case 17:
				strFieldValue = String.valueOf(CertifyLength);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("CertifyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyCode = FValue.trim();
			}
			else
				CertifyCode = null;
		}
		if (FCode.equalsIgnoreCase("SubCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SubCode = FValue.trim();
			}
			else
				SubCode = null;
		}
		if (FCode.equalsIgnoreCase("CertifyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyName = FValue.trim();
			}
			else
				CertifyName = null;
		}
		if (FCode.equalsIgnoreCase("HavePrice"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HavePrice = FValue.trim();
			}
			else
				HavePrice = null;
		}
		if (FCode.equalsIgnoreCase("HaveNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HaveNumber = FValue.trim();
			}
			else
				HaveNumber = null;
		}
		if (FCode.equalsIgnoreCase("Price"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Price = d;
			}
		}
		if (FCode.equalsIgnoreCase("CertifyType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyType = FValue.trim();
			}
			else
				CertifyType = null;
		}
		if (FCode.equalsIgnoreCase("ComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComCode = FValue.trim();
			}
			else
				ComCode = null;
		}
		if (FCode.equalsIgnoreCase("PrintCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrintCode = FValue.trim();
			}
			else
				PrintCode = null;
		}
		if (FCode.equalsIgnoreCase("WarningNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				WarningNo = i;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("CertifyClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifyClass = FValue.trim();
			}
			else
				CertifyClass = null;
		}
		if (FCode.equalsIgnoreCase("CertifyLength"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				CertifyLength = i;
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LMCardDescriptionSchema other = (LMCardDescriptionSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (CertifyCode == null ? other.getCertifyCode() == null : CertifyCode.equals(other.getCertifyCode()))
			&& (SubCode == null ? other.getSubCode() == null : SubCode.equals(other.getSubCode()))
			&& (CertifyName == null ? other.getCertifyName() == null : CertifyName.equals(other.getCertifyName()))
			&& (HavePrice == null ? other.getHavePrice() == null : HavePrice.equals(other.getHavePrice()))
			&& (HaveNumber == null ? other.getHaveNumber() == null : HaveNumber.equals(other.getHaveNumber()))
			&& Price == other.getPrice()
			&& (CertifyType == null ? other.getCertifyType() == null : CertifyType.equals(other.getCertifyType()))
			&& (ComCode == null ? other.getComCode() == null : ComCode.equals(other.getComCode()))
			&& (PrintCode == null ? other.getPrintCode() == null : PrintCode.equals(other.getPrintCode()))
			&& WarningNo == other.getWarningNo()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (CertifyClass == null ? other.getCertifyClass() == null : CertifyClass.equals(other.getCertifyClass()))
			&& CertifyLength == other.getCertifyLength();
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return 1;
		}
		if( strFieldName.equals("SubCode") ) {
			return 2;
		}
		if( strFieldName.equals("CertifyName") ) {
			return 3;
		}
		if( strFieldName.equals("HavePrice") ) {
			return 4;
		}
		if( strFieldName.equals("HaveNumber") ) {
			return 5;
		}
		if( strFieldName.equals("Price") ) {
			return 6;
		}
		if( strFieldName.equals("CertifyType") ) {
			return 7;
		}
		if( strFieldName.equals("ComCode") ) {
			return 8;
		}
		if( strFieldName.equals("PrintCode") ) {
			return 9;
		}
		if( strFieldName.equals("WarningNo") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		if( strFieldName.equals("CertifyClass") ) {
			return 16;
		}
		if( strFieldName.equals("CertifyLength") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "CertifyCode";
				break;
			case 2:
				strFieldName = "SubCode";
				break;
			case 3:
				strFieldName = "CertifyName";
				break;
			case 4:
				strFieldName = "HavePrice";
				break;
			case 5:
				strFieldName = "HaveNumber";
				break;
			case 6:
				strFieldName = "Price";
				break;
			case 7:
				strFieldName = "CertifyType";
				break;
			case 8:
				strFieldName = "ComCode";
				break;
			case 9:
				strFieldName = "PrintCode";
				break;
			case 10:
				strFieldName = "WarningNo";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			case 16:
				strFieldName = "CertifyClass";
				break;
			case 17:
				strFieldName = "CertifyLength";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SubCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HavePrice") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HaveNumber") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Price") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CertifyType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrintCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WarningNo") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifyLength") ) {
			return Schema.TYPE_INT;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_INT;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_INT;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
