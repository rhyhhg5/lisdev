/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAAgentPromRadixDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAgentPromRadixSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAAgentPromRadixSchema implements Schema
{
    // @Field
    /** 代理人职级 */
    private String AgentGrade;
    /** 展业类型 */
    private String BranchType;
    /** 版本号 */
    private String AreaType;
    /** 目标代理人职级 */
    private String DestAgentGrade;
    /** 考核类型 */
    private String AssessCode;
    /** 现任职级要求的达到考核期限 */
    private int YearCode;
    /** 考核期限 */
    private String LimitPeriod;
    /** 降级/晋升起期 */
    private String StartDate;
    /** 通过品质考核标记 */
    private String QuaPassFlag;
    /** 通过培训并考试合格标记 */
    private String ExamPassFlag;
    /** 通过工作考核及素质考评标记 */
    private String WorkPassFlag;
    /** 月平均fyc */
    private double MonAvgFYC;
    /** 个人累计fyc */
    private double IndFYCSum;
    /** 本人及所增人员累计fyc */
    private double IndAddFYCSum;
    /** 本组累计fyc */
    private double TeamFYCSum;
    /** 直辖组累计fyc */
    private double DirTeamFYCSum;
    /** 直辖及育成组累计fyc */
    private double DRFYCSum;
    /** 育成组fyc前三月回算比例 */
    private double FormerRate;
    /** 育成组fyc后三月回算比例 */
    private double LatterRate;
    /** 营业部累计fyc */
    private double DepFYCSum;
    /** 直辖部月均fyc */
    private double DirDepMonAvgFYC;
    /** Fyc 挂零月数 */
    private int FYCZeroMon;
    /** 个人继续率 */
    private double IndRate;
    /** 组平均继续率 */
    private double TeamAvgRate;
    /** 直辖及育成组平均继续率 */
    private double DRTeamAvgRate;
    /** 部门平均继续率 */
    private double DepAvgRate;
    /** 直辖部及育成部平均继续率 */
    private double DRDepAvgRate;
    /** 全区平均继续率 */
    private double AreaRate;
    /** 个人累计新客户人数 */
    private int IndNewCustSum;
    /** 个人累计客户人数 */
    private int IndCustSum;
    /** 月平均客户人数 */
    private int MonAvgCust;
    /** 直接增员人数 */
    private int DirAddCount;
    /** 增员人数 */
    private int AddCount;
    /** 所增人员需达到的维持标准的职级 */
    private String ReachGrade;
    /** 需达到的维持标准的所增人员人数 */
    private int ReachGradeCount;
    /** 所辖人数 */
    private int MngAgentCount;
    /** 所辖人员中理财主任人数 */
    private int MngFinaCount;
    /** 所辖人员中直接推荐人数 */
    private int DirRmdCount;
    /** 直辖人数 */
    private int DirMngCount;
    /** 直辖人员中理财主任人数 */
    private int DirMngFinaCount;
    /** 营业组数 */
    private int DepCount;
    /** 直接育成营业组数 */
    private int DRTeamCount;
    /** 合计育成组数 */
    private int RearTeamSum;
    /** 直接育成营业部数 */
    private int RearDepCount;
    /** 累计育成营业部个数 */
    private int RearDepSum;
    /** 所辖营业部个数 */
    private int MngDepCount;
    /** 所辖营业部理财主任人数 */
    private int DepFinaCount;
    /** 限期内要求育成营业部个数 */
    private int ReqDepCount;
    /** 育成营业部期限 */
    private int ReqPeriod;
    /** 所辖营业区理财主任人数 */
    private int MngAreaFinaCount;
    /** 直接育成业务总监人数 */
    private int DirRearAdmCount;
    /** 育成业务总监人数 */
    private int RearAdmCount;
    /** 月均有效人力fyc标准 */
    private double LaborFYCStand;
    /** 月均有效人力人数 */
    private int MonAvgLabor;
    /** 直辖及育成组月均有效人力人数 */
    private int DRMonLabor;
    /** 直辖部及育成部的有效人力人数 */
    private int DRDepLabor;
    /** 区域总监推荐标记 */
    private String AreaAdmRmdFlag;
    /** 试用打折比率 */
    private double RebateRate;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 59; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAgentPromRadixSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "AgentGrade";
        pk[1] = "AreaType";
        pk[2] = "DestAgentGrade";
        pk[3] = "AssessCode";
        pk[4] = "LimitPeriod";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getAreaType()
    {
        if (SysConst.CHANGECHARSET && AreaType != null && !AreaType.equals(""))
        {
            AreaType = StrTool.unicodeToGBK(AreaType);
        }
        return AreaType;
    }

    public void setAreaType(String aAreaType)
    {
        AreaType = aAreaType;
    }

    public String getDestAgentGrade()
    {
        if (SysConst.CHANGECHARSET && DestAgentGrade != null &&
            !DestAgentGrade.equals(""))
        {
            DestAgentGrade = StrTool.unicodeToGBK(DestAgentGrade);
        }
        return DestAgentGrade;
    }

    public void setDestAgentGrade(String aDestAgentGrade)
    {
        DestAgentGrade = aDestAgentGrade;
    }

    public String getAssessCode()
    {
        if (SysConst.CHANGECHARSET && AssessCode != null &&
            !AssessCode.equals(""))
        {
            AssessCode = StrTool.unicodeToGBK(AssessCode);
        }
        return AssessCode;
    }

    public void setAssessCode(String aAssessCode)
    {
        AssessCode = aAssessCode;
    }

    public int getYearCode()
    {
        return YearCode;
    }

    public void setYearCode(int aYearCode)
    {
        YearCode = aYearCode;
    }

    public void setYearCode(String aYearCode)
    {
        if (aYearCode != null && !aYearCode.equals(""))
        {
            Integer tInteger = new Integer(aYearCode);
            int i = tInteger.intValue();
            YearCode = i;
        }
    }

    public String getLimitPeriod()
    {
        if (SysConst.CHANGECHARSET && LimitPeriod != null &&
            !LimitPeriod.equals(""))
        {
            LimitPeriod = StrTool.unicodeToGBK(LimitPeriod);
        }
        return LimitPeriod;
    }

    public void setLimitPeriod(String aLimitPeriod)
    {
        LimitPeriod = aLimitPeriod;
    }

    public String getStartDate()
    {
        if (SysConst.CHANGECHARSET && StartDate != null && !StartDate.equals(""))
        {
            StartDate = StrTool.unicodeToGBK(StartDate);
        }
        return StartDate;
    }

    public void setStartDate(String aStartDate)
    {
        StartDate = aStartDate;
    }

    public String getQuaPassFlag()
    {
        if (SysConst.CHANGECHARSET && QuaPassFlag != null &&
            !QuaPassFlag.equals(""))
        {
            QuaPassFlag = StrTool.unicodeToGBK(QuaPassFlag);
        }
        return QuaPassFlag;
    }

    public void setQuaPassFlag(String aQuaPassFlag)
    {
        QuaPassFlag = aQuaPassFlag;
    }

    public String getExamPassFlag()
    {
        if (SysConst.CHANGECHARSET && ExamPassFlag != null &&
            !ExamPassFlag.equals(""))
        {
            ExamPassFlag = StrTool.unicodeToGBK(ExamPassFlag);
        }
        return ExamPassFlag;
    }

    public void setExamPassFlag(String aExamPassFlag)
    {
        ExamPassFlag = aExamPassFlag;
    }

    public String getWorkPassFlag()
    {
        if (SysConst.CHANGECHARSET && WorkPassFlag != null &&
            !WorkPassFlag.equals(""))
        {
            WorkPassFlag = StrTool.unicodeToGBK(WorkPassFlag);
        }
        return WorkPassFlag;
    }

    public void setWorkPassFlag(String aWorkPassFlag)
    {
        WorkPassFlag = aWorkPassFlag;
    }

    public double getMonAvgFYC()
    {
        return MonAvgFYC;
    }

    public void setMonAvgFYC(double aMonAvgFYC)
    {
        MonAvgFYC = aMonAvgFYC;
    }

    public void setMonAvgFYC(String aMonAvgFYC)
    {
        if (aMonAvgFYC != null && !aMonAvgFYC.equals(""))
        {
            Double tDouble = new Double(aMonAvgFYC);
            double d = tDouble.doubleValue();
            MonAvgFYC = d;
        }
    }

    public double getIndFYCSum()
    {
        return IndFYCSum;
    }

    public void setIndFYCSum(double aIndFYCSum)
    {
        IndFYCSum = aIndFYCSum;
    }

    public void setIndFYCSum(String aIndFYCSum)
    {
        if (aIndFYCSum != null && !aIndFYCSum.equals(""))
        {
            Double tDouble = new Double(aIndFYCSum);
            double d = tDouble.doubleValue();
            IndFYCSum = d;
        }
    }

    public double getIndAddFYCSum()
    {
        return IndAddFYCSum;
    }

    public void setIndAddFYCSum(double aIndAddFYCSum)
    {
        IndAddFYCSum = aIndAddFYCSum;
    }

    public void setIndAddFYCSum(String aIndAddFYCSum)
    {
        if (aIndAddFYCSum != null && !aIndAddFYCSum.equals(""))
        {
            Double tDouble = new Double(aIndAddFYCSum);
            double d = tDouble.doubleValue();
            IndAddFYCSum = d;
        }
    }

    public double getTeamFYCSum()
    {
        return TeamFYCSum;
    }

    public void setTeamFYCSum(double aTeamFYCSum)
    {
        TeamFYCSum = aTeamFYCSum;
    }

    public void setTeamFYCSum(String aTeamFYCSum)
    {
        if (aTeamFYCSum != null && !aTeamFYCSum.equals(""))
        {
            Double tDouble = new Double(aTeamFYCSum);
            double d = tDouble.doubleValue();
            TeamFYCSum = d;
        }
    }

    public double getDirTeamFYCSum()
    {
        return DirTeamFYCSum;
    }

    public void setDirTeamFYCSum(double aDirTeamFYCSum)
    {
        DirTeamFYCSum = aDirTeamFYCSum;
    }

    public void setDirTeamFYCSum(String aDirTeamFYCSum)
    {
        if (aDirTeamFYCSum != null && !aDirTeamFYCSum.equals(""))
        {
            Double tDouble = new Double(aDirTeamFYCSum);
            double d = tDouble.doubleValue();
            DirTeamFYCSum = d;
        }
    }

    public double getDRFYCSum()
    {
        return DRFYCSum;
    }

    public void setDRFYCSum(double aDRFYCSum)
    {
        DRFYCSum = aDRFYCSum;
    }

    public void setDRFYCSum(String aDRFYCSum)
    {
        if (aDRFYCSum != null && !aDRFYCSum.equals(""))
        {
            Double tDouble = new Double(aDRFYCSum);
            double d = tDouble.doubleValue();
            DRFYCSum = d;
        }
    }

    public double getFormerRate()
    {
        return FormerRate;
    }

    public void setFormerRate(double aFormerRate)
    {
        FormerRate = aFormerRate;
    }

    public void setFormerRate(String aFormerRate)
    {
        if (aFormerRate != null && !aFormerRate.equals(""))
        {
            Double tDouble = new Double(aFormerRate);
            double d = tDouble.doubleValue();
            FormerRate = d;
        }
    }

    public double getLatterRate()
    {
        return LatterRate;
    }

    public void setLatterRate(double aLatterRate)
    {
        LatterRate = aLatterRate;
    }

    public void setLatterRate(String aLatterRate)
    {
        if (aLatterRate != null && !aLatterRate.equals(""))
        {
            Double tDouble = new Double(aLatterRate);
            double d = tDouble.doubleValue();
            LatterRate = d;
        }
    }

    public double getDepFYCSum()
    {
        return DepFYCSum;
    }

    public void setDepFYCSum(double aDepFYCSum)
    {
        DepFYCSum = aDepFYCSum;
    }

    public void setDepFYCSum(String aDepFYCSum)
    {
        if (aDepFYCSum != null && !aDepFYCSum.equals(""))
        {
            Double tDouble = new Double(aDepFYCSum);
            double d = tDouble.doubleValue();
            DepFYCSum = d;
        }
    }

    public double getDirDepMonAvgFYC()
    {
        return DirDepMonAvgFYC;
    }

    public void setDirDepMonAvgFYC(double aDirDepMonAvgFYC)
    {
        DirDepMonAvgFYC = aDirDepMonAvgFYC;
    }

    public void setDirDepMonAvgFYC(String aDirDepMonAvgFYC)
    {
        if (aDirDepMonAvgFYC != null && !aDirDepMonAvgFYC.equals(""))
        {
            Double tDouble = new Double(aDirDepMonAvgFYC);
            double d = tDouble.doubleValue();
            DirDepMonAvgFYC = d;
        }
    }

    public int getFYCZeroMon()
    {
        return FYCZeroMon;
    }

    public void setFYCZeroMon(int aFYCZeroMon)
    {
        FYCZeroMon = aFYCZeroMon;
    }

    public void setFYCZeroMon(String aFYCZeroMon)
    {
        if (aFYCZeroMon != null && !aFYCZeroMon.equals(""))
        {
            Integer tInteger = new Integer(aFYCZeroMon);
            int i = tInteger.intValue();
            FYCZeroMon = i;
        }
    }

    public double getIndRate()
    {
        return IndRate;
    }

    public void setIndRate(double aIndRate)
    {
        IndRate = aIndRate;
    }

    public void setIndRate(String aIndRate)
    {
        if (aIndRate != null && !aIndRate.equals(""))
        {
            Double tDouble = new Double(aIndRate);
            double d = tDouble.doubleValue();
            IndRate = d;
        }
    }

    public double getTeamAvgRate()
    {
        return TeamAvgRate;
    }

    public void setTeamAvgRate(double aTeamAvgRate)
    {
        TeamAvgRate = aTeamAvgRate;
    }

    public void setTeamAvgRate(String aTeamAvgRate)
    {
        if (aTeamAvgRate != null && !aTeamAvgRate.equals(""))
        {
            Double tDouble = new Double(aTeamAvgRate);
            double d = tDouble.doubleValue();
            TeamAvgRate = d;
        }
    }

    public double getDRTeamAvgRate()
    {
        return DRTeamAvgRate;
    }

    public void setDRTeamAvgRate(double aDRTeamAvgRate)
    {
        DRTeamAvgRate = aDRTeamAvgRate;
    }

    public void setDRTeamAvgRate(String aDRTeamAvgRate)
    {
        if (aDRTeamAvgRate != null && !aDRTeamAvgRate.equals(""))
        {
            Double tDouble = new Double(aDRTeamAvgRate);
            double d = tDouble.doubleValue();
            DRTeamAvgRate = d;
        }
    }

    public double getDepAvgRate()
    {
        return DepAvgRate;
    }

    public void setDepAvgRate(double aDepAvgRate)
    {
        DepAvgRate = aDepAvgRate;
    }

    public void setDepAvgRate(String aDepAvgRate)
    {
        if (aDepAvgRate != null && !aDepAvgRate.equals(""))
        {
            Double tDouble = new Double(aDepAvgRate);
            double d = tDouble.doubleValue();
            DepAvgRate = d;
        }
    }

    public double getDRDepAvgRate()
    {
        return DRDepAvgRate;
    }

    public void setDRDepAvgRate(double aDRDepAvgRate)
    {
        DRDepAvgRate = aDRDepAvgRate;
    }

    public void setDRDepAvgRate(String aDRDepAvgRate)
    {
        if (aDRDepAvgRate != null && !aDRDepAvgRate.equals(""))
        {
            Double tDouble = new Double(aDRDepAvgRate);
            double d = tDouble.doubleValue();
            DRDepAvgRate = d;
        }
    }

    public double getAreaRate()
    {
        return AreaRate;
    }

    public void setAreaRate(double aAreaRate)
    {
        AreaRate = aAreaRate;
    }

    public void setAreaRate(String aAreaRate)
    {
        if (aAreaRate != null && !aAreaRate.equals(""))
        {
            Double tDouble = new Double(aAreaRate);
            double d = tDouble.doubleValue();
            AreaRate = d;
        }
    }

    public int getIndNewCustSum()
    {
        return IndNewCustSum;
    }

    public void setIndNewCustSum(int aIndNewCustSum)
    {
        IndNewCustSum = aIndNewCustSum;
    }

    public void setIndNewCustSum(String aIndNewCustSum)
    {
        if (aIndNewCustSum != null && !aIndNewCustSum.equals(""))
        {
            Integer tInteger = new Integer(aIndNewCustSum);
            int i = tInteger.intValue();
            IndNewCustSum = i;
        }
    }

    public int getIndCustSum()
    {
        return IndCustSum;
    }

    public void setIndCustSum(int aIndCustSum)
    {
        IndCustSum = aIndCustSum;
    }

    public void setIndCustSum(String aIndCustSum)
    {
        if (aIndCustSum != null && !aIndCustSum.equals(""))
        {
            Integer tInteger = new Integer(aIndCustSum);
            int i = tInteger.intValue();
            IndCustSum = i;
        }
    }

    public int getMonAvgCust()
    {
        return MonAvgCust;
    }

    public void setMonAvgCust(int aMonAvgCust)
    {
        MonAvgCust = aMonAvgCust;
    }

    public void setMonAvgCust(String aMonAvgCust)
    {
        if (aMonAvgCust != null && !aMonAvgCust.equals(""))
        {
            Integer tInteger = new Integer(aMonAvgCust);
            int i = tInteger.intValue();
            MonAvgCust = i;
        }
    }

    public int getDirAddCount()
    {
        return DirAddCount;
    }

    public void setDirAddCount(int aDirAddCount)
    {
        DirAddCount = aDirAddCount;
    }

    public void setDirAddCount(String aDirAddCount)
    {
        if (aDirAddCount != null && !aDirAddCount.equals(""))
        {
            Integer tInteger = new Integer(aDirAddCount);
            int i = tInteger.intValue();
            DirAddCount = i;
        }
    }

    public int getAddCount()
    {
        return AddCount;
    }

    public void setAddCount(int aAddCount)
    {
        AddCount = aAddCount;
    }

    public void setAddCount(String aAddCount)
    {
        if (aAddCount != null && !aAddCount.equals(""))
        {
            Integer tInteger = new Integer(aAddCount);
            int i = tInteger.intValue();
            AddCount = i;
        }
    }

    public String getReachGrade()
    {
        if (SysConst.CHANGECHARSET && ReachGrade != null &&
            !ReachGrade.equals(""))
        {
            ReachGrade = StrTool.unicodeToGBK(ReachGrade);
        }
        return ReachGrade;
    }

    public void setReachGrade(String aReachGrade)
    {
        ReachGrade = aReachGrade;
    }

    public int getReachGradeCount()
    {
        return ReachGradeCount;
    }

    public void setReachGradeCount(int aReachGradeCount)
    {
        ReachGradeCount = aReachGradeCount;
    }

    public void setReachGradeCount(String aReachGradeCount)
    {
        if (aReachGradeCount != null && !aReachGradeCount.equals(""))
        {
            Integer tInteger = new Integer(aReachGradeCount);
            int i = tInteger.intValue();
            ReachGradeCount = i;
        }
    }

    public int getMngAgentCount()
    {
        return MngAgentCount;
    }

    public void setMngAgentCount(int aMngAgentCount)
    {
        MngAgentCount = aMngAgentCount;
    }

    public void setMngAgentCount(String aMngAgentCount)
    {
        if (aMngAgentCount != null && !aMngAgentCount.equals(""))
        {
            Integer tInteger = new Integer(aMngAgentCount);
            int i = tInteger.intValue();
            MngAgentCount = i;
        }
    }

    public int getMngFinaCount()
    {
        return MngFinaCount;
    }

    public void setMngFinaCount(int aMngFinaCount)
    {
        MngFinaCount = aMngFinaCount;
    }

    public void setMngFinaCount(String aMngFinaCount)
    {
        if (aMngFinaCount != null && !aMngFinaCount.equals(""))
        {
            Integer tInteger = new Integer(aMngFinaCount);
            int i = tInteger.intValue();
            MngFinaCount = i;
        }
    }

    public int getDirRmdCount()
    {
        return DirRmdCount;
    }

    public void setDirRmdCount(int aDirRmdCount)
    {
        DirRmdCount = aDirRmdCount;
    }

    public void setDirRmdCount(String aDirRmdCount)
    {
        if (aDirRmdCount != null && !aDirRmdCount.equals(""))
        {
            Integer tInteger = new Integer(aDirRmdCount);
            int i = tInteger.intValue();
            DirRmdCount = i;
        }
    }

    public int getDirMngCount()
    {
        return DirMngCount;
    }

    public void setDirMngCount(int aDirMngCount)
    {
        DirMngCount = aDirMngCount;
    }

    public void setDirMngCount(String aDirMngCount)
    {
        if (aDirMngCount != null && !aDirMngCount.equals(""))
        {
            Integer tInteger = new Integer(aDirMngCount);
            int i = tInteger.intValue();
            DirMngCount = i;
        }
    }

    public int getDirMngFinaCount()
    {
        return DirMngFinaCount;
    }

    public void setDirMngFinaCount(int aDirMngFinaCount)
    {
        DirMngFinaCount = aDirMngFinaCount;
    }

    public void setDirMngFinaCount(String aDirMngFinaCount)
    {
        if (aDirMngFinaCount != null && !aDirMngFinaCount.equals(""))
        {
            Integer tInteger = new Integer(aDirMngFinaCount);
            int i = tInteger.intValue();
            DirMngFinaCount = i;
        }
    }

    public int getDepCount()
    {
        return DepCount;
    }

    public void setDepCount(int aDepCount)
    {
        DepCount = aDepCount;
    }

    public void setDepCount(String aDepCount)
    {
        if (aDepCount != null && !aDepCount.equals(""))
        {
            Integer tInteger = new Integer(aDepCount);
            int i = tInteger.intValue();
            DepCount = i;
        }
    }

    public int getDRTeamCount()
    {
        return DRTeamCount;
    }

    public void setDRTeamCount(int aDRTeamCount)
    {
        DRTeamCount = aDRTeamCount;
    }

    public void setDRTeamCount(String aDRTeamCount)
    {
        if (aDRTeamCount != null && !aDRTeamCount.equals(""))
        {
            Integer tInteger = new Integer(aDRTeamCount);
            int i = tInteger.intValue();
            DRTeamCount = i;
        }
    }

    public int getRearTeamSum()
    {
        return RearTeamSum;
    }

    public void setRearTeamSum(int aRearTeamSum)
    {
        RearTeamSum = aRearTeamSum;
    }

    public void setRearTeamSum(String aRearTeamSum)
    {
        if (aRearTeamSum != null && !aRearTeamSum.equals(""))
        {
            Integer tInteger = new Integer(aRearTeamSum);
            int i = tInteger.intValue();
            RearTeamSum = i;
        }
    }

    public int getRearDepCount()
    {
        return RearDepCount;
    }

    public void setRearDepCount(int aRearDepCount)
    {
        RearDepCount = aRearDepCount;
    }

    public void setRearDepCount(String aRearDepCount)
    {
        if (aRearDepCount != null && !aRearDepCount.equals(""))
        {
            Integer tInteger = new Integer(aRearDepCount);
            int i = tInteger.intValue();
            RearDepCount = i;
        }
    }

    public int getRearDepSum()
    {
        return RearDepSum;
    }

    public void setRearDepSum(int aRearDepSum)
    {
        RearDepSum = aRearDepSum;
    }

    public void setRearDepSum(String aRearDepSum)
    {
        if (aRearDepSum != null && !aRearDepSum.equals(""))
        {
            Integer tInteger = new Integer(aRearDepSum);
            int i = tInteger.intValue();
            RearDepSum = i;
        }
    }

    public int getMngDepCount()
    {
        return MngDepCount;
    }

    public void setMngDepCount(int aMngDepCount)
    {
        MngDepCount = aMngDepCount;
    }

    public void setMngDepCount(String aMngDepCount)
    {
        if (aMngDepCount != null && !aMngDepCount.equals(""))
        {
            Integer tInteger = new Integer(aMngDepCount);
            int i = tInteger.intValue();
            MngDepCount = i;
        }
    }

    public int getDepFinaCount()
    {
        return DepFinaCount;
    }

    public void setDepFinaCount(int aDepFinaCount)
    {
        DepFinaCount = aDepFinaCount;
    }

    public void setDepFinaCount(String aDepFinaCount)
    {
        if (aDepFinaCount != null && !aDepFinaCount.equals(""))
        {
            Integer tInteger = new Integer(aDepFinaCount);
            int i = tInteger.intValue();
            DepFinaCount = i;
        }
    }

    public int getReqDepCount()
    {
        return ReqDepCount;
    }

    public void setReqDepCount(int aReqDepCount)
    {
        ReqDepCount = aReqDepCount;
    }

    public void setReqDepCount(String aReqDepCount)
    {
        if (aReqDepCount != null && !aReqDepCount.equals(""))
        {
            Integer tInteger = new Integer(aReqDepCount);
            int i = tInteger.intValue();
            ReqDepCount = i;
        }
    }

    public int getReqPeriod()
    {
        return ReqPeriod;
    }

    public void setReqPeriod(int aReqPeriod)
    {
        ReqPeriod = aReqPeriod;
    }

    public void setReqPeriod(String aReqPeriod)
    {
        if (aReqPeriod != null && !aReqPeriod.equals(""))
        {
            Integer tInteger = new Integer(aReqPeriod);
            int i = tInteger.intValue();
            ReqPeriod = i;
        }
    }

    public int getMngAreaFinaCount()
    {
        return MngAreaFinaCount;
    }

    public void setMngAreaFinaCount(int aMngAreaFinaCount)
    {
        MngAreaFinaCount = aMngAreaFinaCount;
    }

    public void setMngAreaFinaCount(String aMngAreaFinaCount)
    {
        if (aMngAreaFinaCount != null && !aMngAreaFinaCount.equals(""))
        {
            Integer tInteger = new Integer(aMngAreaFinaCount);
            int i = tInteger.intValue();
            MngAreaFinaCount = i;
        }
    }

    public int getDirRearAdmCount()
    {
        return DirRearAdmCount;
    }

    public void setDirRearAdmCount(int aDirRearAdmCount)
    {
        DirRearAdmCount = aDirRearAdmCount;
    }

    public void setDirRearAdmCount(String aDirRearAdmCount)
    {
        if (aDirRearAdmCount != null && !aDirRearAdmCount.equals(""))
        {
            Integer tInteger = new Integer(aDirRearAdmCount);
            int i = tInteger.intValue();
            DirRearAdmCount = i;
        }
    }

    public int getRearAdmCount()
    {
        return RearAdmCount;
    }

    public void setRearAdmCount(int aRearAdmCount)
    {
        RearAdmCount = aRearAdmCount;
    }

    public void setRearAdmCount(String aRearAdmCount)
    {
        if (aRearAdmCount != null && !aRearAdmCount.equals(""))
        {
            Integer tInteger = new Integer(aRearAdmCount);
            int i = tInteger.intValue();
            RearAdmCount = i;
        }
    }

    public double getLaborFYCStand()
    {
        return LaborFYCStand;
    }

    public void setLaborFYCStand(double aLaborFYCStand)
    {
        LaborFYCStand = aLaborFYCStand;
    }

    public void setLaborFYCStand(String aLaborFYCStand)
    {
        if (aLaborFYCStand != null && !aLaborFYCStand.equals(""))
        {
            Double tDouble = new Double(aLaborFYCStand);
            double d = tDouble.doubleValue();
            LaborFYCStand = d;
        }
    }

    public int getMonAvgLabor()
    {
        return MonAvgLabor;
    }

    public void setMonAvgLabor(int aMonAvgLabor)
    {
        MonAvgLabor = aMonAvgLabor;
    }

    public void setMonAvgLabor(String aMonAvgLabor)
    {
        if (aMonAvgLabor != null && !aMonAvgLabor.equals(""))
        {
            Integer tInteger = new Integer(aMonAvgLabor);
            int i = tInteger.intValue();
            MonAvgLabor = i;
        }
    }

    public int getDRMonLabor()
    {
        return DRMonLabor;
    }

    public void setDRMonLabor(int aDRMonLabor)
    {
        DRMonLabor = aDRMonLabor;
    }

    public void setDRMonLabor(String aDRMonLabor)
    {
        if (aDRMonLabor != null && !aDRMonLabor.equals(""))
        {
            Integer tInteger = new Integer(aDRMonLabor);
            int i = tInteger.intValue();
            DRMonLabor = i;
        }
    }

    public int getDRDepLabor()
    {
        return DRDepLabor;
    }

    public void setDRDepLabor(int aDRDepLabor)
    {
        DRDepLabor = aDRDepLabor;
    }

    public void setDRDepLabor(String aDRDepLabor)
    {
        if (aDRDepLabor != null && !aDRDepLabor.equals(""))
        {
            Integer tInteger = new Integer(aDRDepLabor);
            int i = tInteger.intValue();
            DRDepLabor = i;
        }
    }

    public String getAreaAdmRmdFlag()
    {
        if (SysConst.CHANGECHARSET && AreaAdmRmdFlag != null &&
            !AreaAdmRmdFlag.equals(""))
        {
            AreaAdmRmdFlag = StrTool.unicodeToGBK(AreaAdmRmdFlag);
        }
        return AreaAdmRmdFlag;
    }

    public void setAreaAdmRmdFlag(String aAreaAdmRmdFlag)
    {
        AreaAdmRmdFlag = aAreaAdmRmdFlag;
    }

    public double getRebateRate()
    {
        return RebateRate;
    }

    public void setRebateRate(double aRebateRate)
    {
        RebateRate = aRebateRate;
    }

    public void setRebateRate(String aRebateRate)
    {
        if (aRebateRate != null && !aRebateRate.equals(""))
        {
            Double tDouble = new Double(aRebateRate);
            double d = tDouble.doubleValue();
            RebateRate = d;
        }
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAAgentPromRadixSchema 对象给 Schema 赋值
     * @param: aLAAgentPromRadixSchema LAAgentPromRadixSchema
     **/
    public void setSchema(LAAgentPromRadixSchema aLAAgentPromRadixSchema)
    {
        this.AgentGrade = aLAAgentPromRadixSchema.getAgentGrade();
        this.BranchType = aLAAgentPromRadixSchema.getBranchType();
        this.AreaType = aLAAgentPromRadixSchema.getAreaType();
        this.DestAgentGrade = aLAAgentPromRadixSchema.getDestAgentGrade();
        this.AssessCode = aLAAgentPromRadixSchema.getAssessCode();
        this.YearCode = aLAAgentPromRadixSchema.getYearCode();
        this.LimitPeriod = aLAAgentPromRadixSchema.getLimitPeriod();
        this.StartDate = aLAAgentPromRadixSchema.getStartDate();
        this.QuaPassFlag = aLAAgentPromRadixSchema.getQuaPassFlag();
        this.ExamPassFlag = aLAAgentPromRadixSchema.getExamPassFlag();
        this.WorkPassFlag = aLAAgentPromRadixSchema.getWorkPassFlag();
        this.MonAvgFYC = aLAAgentPromRadixSchema.getMonAvgFYC();
        this.IndFYCSum = aLAAgentPromRadixSchema.getIndFYCSum();
        this.IndAddFYCSum = aLAAgentPromRadixSchema.getIndAddFYCSum();
        this.TeamFYCSum = aLAAgentPromRadixSchema.getTeamFYCSum();
        this.DirTeamFYCSum = aLAAgentPromRadixSchema.getDirTeamFYCSum();
        this.DRFYCSum = aLAAgentPromRadixSchema.getDRFYCSum();
        this.FormerRate = aLAAgentPromRadixSchema.getFormerRate();
        this.LatterRate = aLAAgentPromRadixSchema.getLatterRate();
        this.DepFYCSum = aLAAgentPromRadixSchema.getDepFYCSum();
        this.DirDepMonAvgFYC = aLAAgentPromRadixSchema.getDirDepMonAvgFYC();
        this.FYCZeroMon = aLAAgentPromRadixSchema.getFYCZeroMon();
        this.IndRate = aLAAgentPromRadixSchema.getIndRate();
        this.TeamAvgRate = aLAAgentPromRadixSchema.getTeamAvgRate();
        this.DRTeamAvgRate = aLAAgentPromRadixSchema.getDRTeamAvgRate();
        this.DepAvgRate = aLAAgentPromRadixSchema.getDepAvgRate();
        this.DRDepAvgRate = aLAAgentPromRadixSchema.getDRDepAvgRate();
        this.AreaRate = aLAAgentPromRadixSchema.getAreaRate();
        this.IndNewCustSum = aLAAgentPromRadixSchema.getIndNewCustSum();
        this.IndCustSum = aLAAgentPromRadixSchema.getIndCustSum();
        this.MonAvgCust = aLAAgentPromRadixSchema.getMonAvgCust();
        this.DirAddCount = aLAAgentPromRadixSchema.getDirAddCount();
        this.AddCount = aLAAgentPromRadixSchema.getAddCount();
        this.ReachGrade = aLAAgentPromRadixSchema.getReachGrade();
        this.ReachGradeCount = aLAAgentPromRadixSchema.getReachGradeCount();
        this.MngAgentCount = aLAAgentPromRadixSchema.getMngAgentCount();
        this.MngFinaCount = aLAAgentPromRadixSchema.getMngFinaCount();
        this.DirRmdCount = aLAAgentPromRadixSchema.getDirRmdCount();
        this.DirMngCount = aLAAgentPromRadixSchema.getDirMngCount();
        this.DirMngFinaCount = aLAAgentPromRadixSchema.getDirMngFinaCount();
        this.DepCount = aLAAgentPromRadixSchema.getDepCount();
        this.DRTeamCount = aLAAgentPromRadixSchema.getDRTeamCount();
        this.RearTeamSum = aLAAgentPromRadixSchema.getRearTeamSum();
        this.RearDepCount = aLAAgentPromRadixSchema.getRearDepCount();
        this.RearDepSum = aLAAgentPromRadixSchema.getRearDepSum();
        this.MngDepCount = aLAAgentPromRadixSchema.getMngDepCount();
        this.DepFinaCount = aLAAgentPromRadixSchema.getDepFinaCount();
        this.ReqDepCount = aLAAgentPromRadixSchema.getReqDepCount();
        this.ReqPeriod = aLAAgentPromRadixSchema.getReqPeriod();
        this.MngAreaFinaCount = aLAAgentPromRadixSchema.getMngAreaFinaCount();
        this.DirRearAdmCount = aLAAgentPromRadixSchema.getDirRearAdmCount();
        this.RearAdmCount = aLAAgentPromRadixSchema.getRearAdmCount();
        this.LaborFYCStand = aLAAgentPromRadixSchema.getLaborFYCStand();
        this.MonAvgLabor = aLAAgentPromRadixSchema.getMonAvgLabor();
        this.DRMonLabor = aLAAgentPromRadixSchema.getDRMonLabor();
        this.DRDepLabor = aLAAgentPromRadixSchema.getDRDepLabor();
        this.AreaAdmRmdFlag = aLAAgentPromRadixSchema.getAreaAdmRmdFlag();
        this.RebateRate = aLAAgentPromRadixSchema.getRebateRate();
        this.BranchType2 = aLAAgentPromRadixSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("AreaType") == null)
            {
                this.AreaType = null;
            }
            else
            {
                this.AreaType = rs.getString("AreaType").trim();
            }

            if (rs.getString("DestAgentGrade") == null)
            {
                this.DestAgentGrade = null;
            }
            else
            {
                this.DestAgentGrade = rs.getString("DestAgentGrade").trim();
            }

            if (rs.getString("AssessCode") == null)
            {
                this.AssessCode = null;
            }
            else
            {
                this.AssessCode = rs.getString("AssessCode").trim();
            }

            this.YearCode = rs.getInt("YearCode");
            if (rs.getString("LimitPeriod") == null)
            {
                this.LimitPeriod = null;
            }
            else
            {
                this.LimitPeriod = rs.getString("LimitPeriod").trim();
            }

            if (rs.getString("StartDate") == null)
            {
                this.StartDate = null;
            }
            else
            {
                this.StartDate = rs.getString("StartDate").trim();
            }

            if (rs.getString("QuaPassFlag") == null)
            {
                this.QuaPassFlag = null;
            }
            else
            {
                this.QuaPassFlag = rs.getString("QuaPassFlag").trim();
            }

            if (rs.getString("ExamPassFlag") == null)
            {
                this.ExamPassFlag = null;
            }
            else
            {
                this.ExamPassFlag = rs.getString("ExamPassFlag").trim();
            }

            if (rs.getString("WorkPassFlag") == null)
            {
                this.WorkPassFlag = null;
            }
            else
            {
                this.WorkPassFlag = rs.getString("WorkPassFlag").trim();
            }

            this.MonAvgFYC = rs.getDouble("MonAvgFYC");
            this.IndFYCSum = rs.getDouble("IndFYCSum");
            this.IndAddFYCSum = rs.getDouble("IndAddFYCSum");
            this.TeamFYCSum = rs.getDouble("TeamFYCSum");
            this.DirTeamFYCSum = rs.getDouble("DirTeamFYCSum");
            this.DRFYCSum = rs.getDouble("DRFYCSum");
            this.FormerRate = rs.getDouble("FormerRate");
            this.LatterRate = rs.getDouble("LatterRate");
            this.DepFYCSum = rs.getDouble("DepFYCSum");
            this.DirDepMonAvgFYC = rs.getDouble("DirDepMonAvgFYC");
            this.FYCZeroMon = rs.getInt("FYCZeroMon");
            this.IndRate = rs.getDouble("IndRate");
            this.TeamAvgRate = rs.getDouble("TeamAvgRate");
            this.DRTeamAvgRate = rs.getDouble("DRTeamAvgRate");
            this.DepAvgRate = rs.getDouble("DepAvgRate");
            this.DRDepAvgRate = rs.getDouble("DRDepAvgRate");
            this.AreaRate = rs.getDouble("AreaRate");
            this.IndNewCustSum = rs.getInt("IndNewCustSum");
            this.IndCustSum = rs.getInt("IndCustSum");
            this.MonAvgCust = rs.getInt("MonAvgCust");
            this.DirAddCount = rs.getInt("DirAddCount");
            this.AddCount = rs.getInt("AddCount");
            if (rs.getString("ReachGrade") == null)
            {
                this.ReachGrade = null;
            }
            else
            {
                this.ReachGrade = rs.getString("ReachGrade").trim();
            }

            this.ReachGradeCount = rs.getInt("ReachGradeCount");
            this.MngAgentCount = rs.getInt("MngAgentCount");
            this.MngFinaCount = rs.getInt("MngFinaCount");
            this.DirRmdCount = rs.getInt("DirRmdCount");
            this.DirMngCount = rs.getInt("DirMngCount");
            this.DirMngFinaCount = rs.getInt("DirMngFinaCount");
            this.DepCount = rs.getInt("DepCount");
            this.DRTeamCount = rs.getInt("DRTeamCount");
            this.RearTeamSum = rs.getInt("RearTeamSum");
            this.RearDepCount = rs.getInt("RearDepCount");
            this.RearDepSum = rs.getInt("RearDepSum");
            this.MngDepCount = rs.getInt("MngDepCount");
            this.DepFinaCount = rs.getInt("DepFinaCount");
            this.ReqDepCount = rs.getInt("ReqDepCount");
            this.ReqPeriod = rs.getInt("ReqPeriod");
            this.MngAreaFinaCount = rs.getInt("MngAreaFinaCount");
            this.DirRearAdmCount = rs.getInt("DirRearAdmCount");
            this.RearAdmCount = rs.getInt("RearAdmCount");
            this.LaborFYCStand = rs.getDouble("LaborFYCStand");
            this.MonAvgLabor = rs.getInt("MonAvgLabor");
            this.DRMonLabor = rs.getInt("DRMonLabor");
            this.DRDepLabor = rs.getInt("DRDepLabor");
            if (rs.getString("AreaAdmRmdFlag") == null)
            {
                this.AreaAdmRmdFlag = null;
            }
            else
            {
                this.AreaAdmRmdFlag = rs.getString("AreaAdmRmdFlag").trim();
            }

            this.RebateRate = rs.getDouble("RebateRate");
            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentPromRadixSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAAgentPromRadixSchema getSchema()
    {
        LAAgentPromRadixSchema aLAAgentPromRadixSchema = new
                LAAgentPromRadixSchema();
        aLAAgentPromRadixSchema.setSchema(this);
        return aLAAgentPromRadixSchema;
    }

    public LAAgentPromRadixDB getDB()
    {
        LAAgentPromRadixDB aDBOper = new LAAgentPromRadixDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentPromRadix描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AreaType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DestAgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AssessCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(YearCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(LimitPeriod)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(QuaPassFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ExamPassFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(WorkPassFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MonAvgFYC));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(IndFYCSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(IndAddFYCSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(TeamFYCSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DirTeamFYCSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DRFYCSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FormerRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LatterRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DepFYCSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DirDepMonAvgFYC));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FYCZeroMon));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(IndRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(TeamAvgRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DRTeamAvgRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DepAvgRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DRDepAvgRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AreaRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(IndNewCustSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(IndCustSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MonAvgCust));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DirAddCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AddCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ReachGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ReachGradeCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MngAgentCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MngFinaCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DirRmdCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DirMngCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DirMngFinaCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DepCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DRTeamCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RearTeamSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RearDepCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RearDepSum));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MngDepCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DepFinaCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ReqDepCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ReqPeriod));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MngAreaFinaCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DirRearAdmCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RearAdmCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LaborFYCStand));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MonAvgLabor));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DRMonLabor));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DRDepLabor));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AreaAdmRmdFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RebateRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentPromRadix>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            DestAgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                            SysConst.PACKAGESPILTER);
            AssessCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            YearCode = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).intValue();
            LimitPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            StartDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            QuaPassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            ExamPassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            WorkPassFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                          SysConst.PACKAGESPILTER);
            MonAvgFYC = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            IndFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            IndAddFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            TeamFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 15, SysConst.PACKAGESPILTER))).doubleValue();
            DirTeamFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 16, SysConst.PACKAGESPILTER))).doubleValue();
            DRFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).doubleValue();
            FormerRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            LatterRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            DepFYCSum = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            DirDepMonAvgFYC = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 21, SysConst.PACKAGESPILTER))).doubleValue();
            FYCZeroMon = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 22, SysConst.PACKAGESPILTER))).intValue();
            IndRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 23, SysConst.PACKAGESPILTER))).doubleValue();
            TeamAvgRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 24, SysConst.PACKAGESPILTER))).doubleValue();
            DRTeamAvgRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 25, SysConst.PACKAGESPILTER))).doubleValue();
            DepAvgRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 26, SysConst.PACKAGESPILTER))).doubleValue();
            DRDepAvgRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 27, SysConst.PACKAGESPILTER))).doubleValue();
            AreaRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 28, SysConst.PACKAGESPILTER))).doubleValue();
            IndNewCustSum = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 29, SysConst.PACKAGESPILTER))).intValue();
            IndCustSum = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 30, SysConst.PACKAGESPILTER))).intValue();
            MonAvgCust = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 31, SysConst.PACKAGESPILTER))).intValue();
            DirAddCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 32, SysConst.PACKAGESPILTER))).intValue();
            AddCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 33, SysConst.PACKAGESPILTER))).intValue();
            ReachGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                        SysConst.PACKAGESPILTER);
            ReachGradeCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 35, SysConst.PACKAGESPILTER))).intValue();
            MngAgentCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 36, SysConst.PACKAGESPILTER))).intValue();
            MngFinaCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 37, SysConst.PACKAGESPILTER))).intValue();
            DirRmdCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 38, SysConst.PACKAGESPILTER))).intValue();
            DirMngCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 39, SysConst.PACKAGESPILTER))).intValue();
            DirMngFinaCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 40, SysConst.PACKAGESPILTER))).intValue();
            DepCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 41, SysConst.PACKAGESPILTER))).intValue();
            DRTeamCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 42, SysConst.PACKAGESPILTER))).intValue();
            RearTeamSum = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 43, SysConst.PACKAGESPILTER))).intValue();
            RearDepCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 44, SysConst.PACKAGESPILTER))).intValue();
            RearDepSum = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 45, SysConst.PACKAGESPILTER))).intValue();
            MngDepCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 46, SysConst.PACKAGESPILTER))).intValue();
            DepFinaCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 47, SysConst.PACKAGESPILTER))).intValue();
            ReqDepCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 48, SysConst.PACKAGESPILTER))).intValue();
            ReqPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 49, SysConst.PACKAGESPILTER))).intValue();
            MngAreaFinaCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 50, SysConst.PACKAGESPILTER))).intValue();
            DirRearAdmCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 51, SysConst.PACKAGESPILTER))).intValue();
            RearAdmCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 52, SysConst.PACKAGESPILTER))).intValue();
            LaborFYCStand = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 53, SysConst.PACKAGESPILTER))).doubleValue();
            MonAvgLabor = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 54, SysConst.PACKAGESPILTER))).intValue();
            DRMonLabor = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 55, SysConst.PACKAGESPILTER))).intValue();
            DRDepLabor = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 56, SysConst.PACKAGESPILTER))).intValue();
            AreaAdmRmdFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            57, SysConst.PACKAGESPILTER);
            RebateRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 58, SysConst.PACKAGESPILTER))).doubleValue();
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentPromRadixSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("AreaType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
        }
        if (FCode.equals("DestAgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DestAgentGrade));
        }
        if (FCode.equals("AssessCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessCode));
        }
        if (FCode.equals("YearCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(YearCode));
        }
        if (FCode.equals("LimitPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LimitPeriod));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StartDate));
        }
        if (FCode.equals("QuaPassFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(QuaPassFlag));
        }
        if (FCode.equals("ExamPassFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExamPassFlag));
        }
        if (FCode.equals("WorkPassFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WorkPassFlag));
        }
        if (FCode.equals("MonAvgFYC"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonAvgFYC));
        }
        if (FCode.equals("IndFYCSum"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndFYCSum));
        }
        if (FCode.equals("IndAddFYCSum"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndAddFYCSum));
        }
        if (FCode.equals("TeamFYCSum"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TeamFYCSum));
        }
        if (FCode.equals("DirTeamFYCSum"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirTeamFYCSum));
        }
        if (FCode.equals("DRFYCSum"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DRFYCSum));
        }
        if (FCode.equals("FormerRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FormerRate));
        }
        if (FCode.equals("LatterRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LatterRate));
        }
        if (FCode.equals("DepFYCSum"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DepFYCSum));
        }
        if (FCode.equals("DirDepMonAvgFYC"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirDepMonAvgFYC));
        }
        if (FCode.equals("FYCZeroMon"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FYCZeroMon));
        }
        if (FCode.equals("IndRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndRate));
        }
        if (FCode.equals("TeamAvgRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TeamAvgRate));
        }
        if (FCode.equals("DRTeamAvgRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DRTeamAvgRate));
        }
        if (FCode.equals("DepAvgRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DepAvgRate));
        }
        if (FCode.equals("DRDepAvgRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DRDepAvgRate));
        }
        if (FCode.equals("AreaRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaRate));
        }
        if (FCode.equals("IndNewCustSum"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndNewCustSum));
        }
        if (FCode.equals("IndCustSum"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndCustSum));
        }
        if (FCode.equals("MonAvgCust"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonAvgCust));
        }
        if (FCode.equals("DirAddCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirAddCount));
        }
        if (FCode.equals("AddCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddCount));
        }
        if (FCode.equals("ReachGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReachGrade));
        }
        if (FCode.equals("ReachGradeCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReachGradeCount));
        }
        if (FCode.equals("MngAgentCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngAgentCount));
        }
        if (FCode.equals("MngFinaCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngFinaCount));
        }
        if (FCode.equals("DirRmdCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirRmdCount));
        }
        if (FCode.equals("DirMngCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirMngCount));
        }
        if (FCode.equals("DirMngFinaCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirMngFinaCount));
        }
        if (FCode.equals("DepCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DepCount));
        }
        if (FCode.equals("DRTeamCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DRTeamCount));
        }
        if (FCode.equals("RearTeamSum"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearTeamSum));
        }
        if (FCode.equals("RearDepCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearDepCount));
        }
        if (FCode.equals("RearDepSum"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearDepSum));
        }
        if (FCode.equals("MngDepCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngDepCount));
        }
        if (FCode.equals("DepFinaCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DepFinaCount));
        }
        if (FCode.equals("ReqDepCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReqDepCount));
        }
        if (FCode.equals("ReqPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReqPeriod));
        }
        if (FCode.equals("MngAreaFinaCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngAreaFinaCount));
        }
        if (FCode.equals("DirRearAdmCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DirRearAdmCount));
        }
        if (FCode.equals("RearAdmCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearAdmCount));
        }
        if (FCode.equals("LaborFYCStand"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LaborFYCStand));
        }
        if (FCode.equals("MonAvgLabor"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonAvgLabor));
        }
        if (FCode.equals("DRMonLabor"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DRMonLabor));
        }
        if (FCode.equals("DRDepLabor"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DRDepLabor));
        }
        if (FCode.equals("AreaAdmRmdFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaAdmRmdFlag));
        }
        if (FCode.equals("RebateRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RebateRate));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AreaType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(DestAgentGrade);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AssessCode);
                break;
            case 5:
                strFieldValue = String.valueOf(YearCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(LimitPeriod);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(StartDate);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(QuaPassFlag);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ExamPassFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(WorkPassFlag);
                break;
            case 11:
                strFieldValue = String.valueOf(MonAvgFYC);
                break;
            case 12:
                strFieldValue = String.valueOf(IndFYCSum);
                break;
            case 13:
                strFieldValue = String.valueOf(IndAddFYCSum);
                break;
            case 14:
                strFieldValue = String.valueOf(TeamFYCSum);
                break;
            case 15:
                strFieldValue = String.valueOf(DirTeamFYCSum);
                break;
            case 16:
                strFieldValue = String.valueOf(DRFYCSum);
                break;
            case 17:
                strFieldValue = String.valueOf(FormerRate);
                break;
            case 18:
                strFieldValue = String.valueOf(LatterRate);
                break;
            case 19:
                strFieldValue = String.valueOf(DepFYCSum);
                break;
            case 20:
                strFieldValue = String.valueOf(DirDepMonAvgFYC);
                break;
            case 21:
                strFieldValue = String.valueOf(FYCZeroMon);
                break;
            case 22:
                strFieldValue = String.valueOf(IndRate);
                break;
            case 23:
                strFieldValue = String.valueOf(TeamAvgRate);
                break;
            case 24:
                strFieldValue = String.valueOf(DRTeamAvgRate);
                break;
            case 25:
                strFieldValue = String.valueOf(DepAvgRate);
                break;
            case 26:
                strFieldValue = String.valueOf(DRDepAvgRate);
                break;
            case 27:
                strFieldValue = String.valueOf(AreaRate);
                break;
            case 28:
                strFieldValue = String.valueOf(IndNewCustSum);
                break;
            case 29:
                strFieldValue = String.valueOf(IndCustSum);
                break;
            case 30:
                strFieldValue = String.valueOf(MonAvgCust);
                break;
            case 31:
                strFieldValue = String.valueOf(DirAddCount);
                break;
            case 32:
                strFieldValue = String.valueOf(AddCount);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(ReachGrade);
                break;
            case 34:
                strFieldValue = String.valueOf(ReachGradeCount);
                break;
            case 35:
                strFieldValue = String.valueOf(MngAgentCount);
                break;
            case 36:
                strFieldValue = String.valueOf(MngFinaCount);
                break;
            case 37:
                strFieldValue = String.valueOf(DirRmdCount);
                break;
            case 38:
                strFieldValue = String.valueOf(DirMngCount);
                break;
            case 39:
                strFieldValue = String.valueOf(DirMngFinaCount);
                break;
            case 40:
                strFieldValue = String.valueOf(DepCount);
                break;
            case 41:
                strFieldValue = String.valueOf(DRTeamCount);
                break;
            case 42:
                strFieldValue = String.valueOf(RearTeamSum);
                break;
            case 43:
                strFieldValue = String.valueOf(RearDepCount);
                break;
            case 44:
                strFieldValue = String.valueOf(RearDepSum);
                break;
            case 45:
                strFieldValue = String.valueOf(MngDepCount);
                break;
            case 46:
                strFieldValue = String.valueOf(DepFinaCount);
                break;
            case 47:
                strFieldValue = String.valueOf(ReqDepCount);
                break;
            case 48:
                strFieldValue = String.valueOf(ReqPeriod);
                break;
            case 49:
                strFieldValue = String.valueOf(MngAreaFinaCount);
                break;
            case 50:
                strFieldValue = String.valueOf(DirRearAdmCount);
                break;
            case 51:
                strFieldValue = String.valueOf(RearAdmCount);
                break;
            case 52:
                strFieldValue = String.valueOf(LaborFYCStand);
                break;
            case 53:
                strFieldValue = String.valueOf(MonAvgLabor);
                break;
            case 54:
                strFieldValue = String.valueOf(DRMonLabor);
                break;
            case 55:
                strFieldValue = String.valueOf(DRDepLabor);
                break;
            case 56:
                strFieldValue = StrTool.GBKToUnicode(AreaAdmRmdFlag);
                break;
            case 57:
                strFieldValue = String.valueOf(RebateRate);
                break;
            case 58:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("AreaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AreaType = FValue.trim();
            }
            else
            {
                AreaType = null;
            }
        }
        if (FCode.equals("DestAgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestAgentGrade = FValue.trim();
            }
            else
            {
                DestAgentGrade = null;
            }
        }
        if (FCode.equals("AssessCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AssessCode = FValue.trim();
            }
            else
            {
                AssessCode = null;
            }
        }
        if (FCode.equals("YearCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                YearCode = i;
            }
        }
        if (FCode.equals("LimitPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LimitPeriod = FValue.trim();
            }
            else
            {
                LimitPeriod = null;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = FValue.trim();
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equals("QuaPassFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                QuaPassFlag = FValue.trim();
            }
            else
            {
                QuaPassFlag = null;
            }
        }
        if (FCode.equals("ExamPassFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ExamPassFlag = FValue.trim();
            }
            else
            {
                ExamPassFlag = null;
            }
        }
        if (FCode.equals("WorkPassFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WorkPassFlag = FValue.trim();
            }
            else
            {
                WorkPassFlag = null;
            }
        }
        if (FCode.equals("MonAvgFYC"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MonAvgFYC = d;
            }
        }
        if (FCode.equals("IndFYCSum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                IndFYCSum = d;
            }
        }
        if (FCode.equals("IndAddFYCSum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                IndAddFYCSum = d;
            }
        }
        if (FCode.equals("TeamFYCSum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TeamFYCSum = d;
            }
        }
        if (FCode.equals("DirTeamFYCSum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DirTeamFYCSum = d;
            }
        }
        if (FCode.equals("DRFYCSum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DRFYCSum = d;
            }
        }
        if (FCode.equals("FormerRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FormerRate = d;
            }
        }
        if (FCode.equals("LatterRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LatterRate = d;
            }
        }
        if (FCode.equals("DepFYCSum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DepFYCSum = d;
            }
        }
        if (FCode.equals("DirDepMonAvgFYC"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DirDepMonAvgFYC = d;
            }
        }
        if (FCode.equals("FYCZeroMon"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                FYCZeroMon = i;
            }
        }
        if (FCode.equals("IndRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                IndRate = d;
            }
        }
        if (FCode.equals("TeamAvgRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TeamAvgRate = d;
            }
        }
        if (FCode.equals("DRTeamAvgRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DRTeamAvgRate = d;
            }
        }
        if (FCode.equals("DepAvgRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DepAvgRate = d;
            }
        }
        if (FCode.equals("DRDepAvgRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DRDepAvgRate = d;
            }
        }
        if (FCode.equals("AreaRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AreaRate = d;
            }
        }
        if (FCode.equals("IndNewCustSum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                IndNewCustSum = i;
            }
        }
        if (FCode.equals("IndCustSum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                IndCustSum = i;
            }
        }
        if (FCode.equals("MonAvgCust"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MonAvgCust = i;
            }
        }
        if (FCode.equals("DirAddCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DirAddCount = i;
            }
        }
        if (FCode.equals("AddCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                AddCount = i;
            }
        }
        if (FCode.equals("ReachGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReachGrade = FValue.trim();
            }
            else
            {
                ReachGrade = null;
            }
        }
        if (FCode.equals("ReachGradeCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ReachGradeCount = i;
            }
        }
        if (FCode.equals("MngAgentCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MngAgentCount = i;
            }
        }
        if (FCode.equals("MngFinaCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MngFinaCount = i;
            }
        }
        if (FCode.equals("DirRmdCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DirRmdCount = i;
            }
        }
        if (FCode.equals("DirMngCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DirMngCount = i;
            }
        }
        if (FCode.equals("DirMngFinaCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DirMngFinaCount = i;
            }
        }
        if (FCode.equals("DepCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DepCount = i;
            }
        }
        if (FCode.equals("DRTeamCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DRTeamCount = i;
            }
        }
        if (FCode.equals("RearTeamSum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RearTeamSum = i;
            }
        }
        if (FCode.equals("RearDepCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RearDepCount = i;
            }
        }
        if (FCode.equals("RearDepSum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RearDepSum = i;
            }
        }
        if (FCode.equals("MngDepCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MngDepCount = i;
            }
        }
        if (FCode.equals("DepFinaCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DepFinaCount = i;
            }
        }
        if (FCode.equals("ReqDepCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ReqDepCount = i;
            }
        }
        if (FCode.equals("ReqPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ReqPeriod = i;
            }
        }
        if (FCode.equals("MngAreaFinaCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MngAreaFinaCount = i;
            }
        }
        if (FCode.equals("DirRearAdmCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DirRearAdmCount = i;
            }
        }
        if (FCode.equals("RearAdmCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RearAdmCount = i;
            }
        }
        if (FCode.equals("LaborFYCStand"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LaborFYCStand = d;
            }
        }
        if (FCode.equals("MonAvgLabor"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MonAvgLabor = i;
            }
        }
        if (FCode.equals("DRMonLabor"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DRMonLabor = i;
            }
        }
        if (FCode.equals("DRDepLabor"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                DRDepLabor = i;
            }
        }
        if (FCode.equals("AreaAdmRmdFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AreaAdmRmdFlag = FValue.trim();
            }
            else
            {
                AreaAdmRmdFlag = null;
            }
        }
        if (FCode.equals("RebateRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RebateRate = d;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAAgentPromRadixSchema other = (LAAgentPromRadixSchema) otherObject;
        return
                AgentGrade.equals(other.getAgentGrade())
                && BranchType.equals(other.getBranchType())
                && AreaType.equals(other.getAreaType())
                && DestAgentGrade.equals(other.getDestAgentGrade())
                && AssessCode.equals(other.getAssessCode())
                && YearCode == other.getYearCode()
                && LimitPeriod.equals(other.getLimitPeriod())
                && StartDate.equals(other.getStartDate())
                && QuaPassFlag.equals(other.getQuaPassFlag())
                && ExamPassFlag.equals(other.getExamPassFlag())
                && WorkPassFlag.equals(other.getWorkPassFlag())
                && MonAvgFYC == other.getMonAvgFYC()
                && IndFYCSum == other.getIndFYCSum()
                && IndAddFYCSum == other.getIndAddFYCSum()
                && TeamFYCSum == other.getTeamFYCSum()
                && DirTeamFYCSum == other.getDirTeamFYCSum()
                && DRFYCSum == other.getDRFYCSum()
                && FormerRate == other.getFormerRate()
                && LatterRate == other.getLatterRate()
                && DepFYCSum == other.getDepFYCSum()
                && DirDepMonAvgFYC == other.getDirDepMonAvgFYC()
                && FYCZeroMon == other.getFYCZeroMon()
                && IndRate == other.getIndRate()
                && TeamAvgRate == other.getTeamAvgRate()
                && DRTeamAvgRate == other.getDRTeamAvgRate()
                && DepAvgRate == other.getDepAvgRate()
                && DRDepAvgRate == other.getDRDepAvgRate()
                && AreaRate == other.getAreaRate()
                && IndNewCustSum == other.getIndNewCustSum()
                && IndCustSum == other.getIndCustSum()
                && MonAvgCust == other.getMonAvgCust()
                && DirAddCount == other.getDirAddCount()
                && AddCount == other.getAddCount()
                && ReachGrade.equals(other.getReachGrade())
                && ReachGradeCount == other.getReachGradeCount()
                && MngAgentCount == other.getMngAgentCount()
                && MngFinaCount == other.getMngFinaCount()
                && DirRmdCount == other.getDirRmdCount()
                && DirMngCount == other.getDirMngCount()
                && DirMngFinaCount == other.getDirMngFinaCount()
                && DepCount == other.getDepCount()
                && DRTeamCount == other.getDRTeamCount()
                && RearTeamSum == other.getRearTeamSum()
                && RearDepCount == other.getRearDepCount()
                && RearDepSum == other.getRearDepSum()
                && MngDepCount == other.getMngDepCount()
                && DepFinaCount == other.getDepFinaCount()
                && ReqDepCount == other.getReqDepCount()
                && ReqPeriod == other.getReqPeriod()
                && MngAreaFinaCount == other.getMngAreaFinaCount()
                && DirRearAdmCount == other.getDirRearAdmCount()
                && RearAdmCount == other.getRearAdmCount()
                && LaborFYCStand == other.getLaborFYCStand()
                && MonAvgLabor == other.getMonAvgLabor()
                && DRMonLabor == other.getDRMonLabor()
                && DRDepLabor == other.getDRDepLabor()
                && AreaAdmRmdFlag.equals(other.getAreaAdmRmdFlag())
                && RebateRate == other.getRebateRate()
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return 0;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 1;
        }
        if (strFieldName.equals("AreaType"))
        {
            return 2;
        }
        if (strFieldName.equals("DestAgentGrade"))
        {
            return 3;
        }
        if (strFieldName.equals("AssessCode"))
        {
            return 4;
        }
        if (strFieldName.equals("YearCode"))
        {
            return 5;
        }
        if (strFieldName.equals("LimitPeriod"))
        {
            return 6;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 7;
        }
        if (strFieldName.equals("QuaPassFlag"))
        {
            return 8;
        }
        if (strFieldName.equals("ExamPassFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("WorkPassFlag"))
        {
            return 10;
        }
        if (strFieldName.equals("MonAvgFYC"))
        {
            return 11;
        }
        if (strFieldName.equals("IndFYCSum"))
        {
            return 12;
        }
        if (strFieldName.equals("IndAddFYCSum"))
        {
            return 13;
        }
        if (strFieldName.equals("TeamFYCSum"))
        {
            return 14;
        }
        if (strFieldName.equals("DirTeamFYCSum"))
        {
            return 15;
        }
        if (strFieldName.equals("DRFYCSum"))
        {
            return 16;
        }
        if (strFieldName.equals("FormerRate"))
        {
            return 17;
        }
        if (strFieldName.equals("LatterRate"))
        {
            return 18;
        }
        if (strFieldName.equals("DepFYCSum"))
        {
            return 19;
        }
        if (strFieldName.equals("DirDepMonAvgFYC"))
        {
            return 20;
        }
        if (strFieldName.equals("FYCZeroMon"))
        {
            return 21;
        }
        if (strFieldName.equals("IndRate"))
        {
            return 22;
        }
        if (strFieldName.equals("TeamAvgRate"))
        {
            return 23;
        }
        if (strFieldName.equals("DRTeamAvgRate"))
        {
            return 24;
        }
        if (strFieldName.equals("DepAvgRate"))
        {
            return 25;
        }
        if (strFieldName.equals("DRDepAvgRate"))
        {
            return 26;
        }
        if (strFieldName.equals("AreaRate"))
        {
            return 27;
        }
        if (strFieldName.equals("IndNewCustSum"))
        {
            return 28;
        }
        if (strFieldName.equals("IndCustSum"))
        {
            return 29;
        }
        if (strFieldName.equals("MonAvgCust"))
        {
            return 30;
        }
        if (strFieldName.equals("DirAddCount"))
        {
            return 31;
        }
        if (strFieldName.equals("AddCount"))
        {
            return 32;
        }
        if (strFieldName.equals("ReachGrade"))
        {
            return 33;
        }
        if (strFieldName.equals("ReachGradeCount"))
        {
            return 34;
        }
        if (strFieldName.equals("MngAgentCount"))
        {
            return 35;
        }
        if (strFieldName.equals("MngFinaCount"))
        {
            return 36;
        }
        if (strFieldName.equals("DirRmdCount"))
        {
            return 37;
        }
        if (strFieldName.equals("DirMngCount"))
        {
            return 38;
        }
        if (strFieldName.equals("DirMngFinaCount"))
        {
            return 39;
        }
        if (strFieldName.equals("DepCount"))
        {
            return 40;
        }
        if (strFieldName.equals("DRTeamCount"))
        {
            return 41;
        }
        if (strFieldName.equals("RearTeamSum"))
        {
            return 42;
        }
        if (strFieldName.equals("RearDepCount"))
        {
            return 43;
        }
        if (strFieldName.equals("RearDepSum"))
        {
            return 44;
        }
        if (strFieldName.equals("MngDepCount"))
        {
            return 45;
        }
        if (strFieldName.equals("DepFinaCount"))
        {
            return 46;
        }
        if (strFieldName.equals("ReqDepCount"))
        {
            return 47;
        }
        if (strFieldName.equals("ReqPeriod"))
        {
            return 48;
        }
        if (strFieldName.equals("MngAreaFinaCount"))
        {
            return 49;
        }
        if (strFieldName.equals("DirRearAdmCount"))
        {
            return 50;
        }
        if (strFieldName.equals("RearAdmCount"))
        {
            return 51;
        }
        if (strFieldName.equals("LaborFYCStand"))
        {
            return 52;
        }
        if (strFieldName.equals("MonAvgLabor"))
        {
            return 53;
        }
        if (strFieldName.equals("DRMonLabor"))
        {
            return 54;
        }
        if (strFieldName.equals("DRDepLabor"))
        {
            return 55;
        }
        if (strFieldName.equals("AreaAdmRmdFlag"))
        {
            return 56;
        }
        if (strFieldName.equals("RebateRate"))
        {
            return 57;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 58;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentGrade";
                break;
            case 1:
                strFieldName = "BranchType";
                break;
            case 2:
                strFieldName = "AreaType";
                break;
            case 3:
                strFieldName = "DestAgentGrade";
                break;
            case 4:
                strFieldName = "AssessCode";
                break;
            case 5:
                strFieldName = "YearCode";
                break;
            case 6:
                strFieldName = "LimitPeriod";
                break;
            case 7:
                strFieldName = "StartDate";
                break;
            case 8:
                strFieldName = "QuaPassFlag";
                break;
            case 9:
                strFieldName = "ExamPassFlag";
                break;
            case 10:
                strFieldName = "WorkPassFlag";
                break;
            case 11:
                strFieldName = "MonAvgFYC";
                break;
            case 12:
                strFieldName = "IndFYCSum";
                break;
            case 13:
                strFieldName = "IndAddFYCSum";
                break;
            case 14:
                strFieldName = "TeamFYCSum";
                break;
            case 15:
                strFieldName = "DirTeamFYCSum";
                break;
            case 16:
                strFieldName = "DRFYCSum";
                break;
            case 17:
                strFieldName = "FormerRate";
                break;
            case 18:
                strFieldName = "LatterRate";
                break;
            case 19:
                strFieldName = "DepFYCSum";
                break;
            case 20:
                strFieldName = "DirDepMonAvgFYC";
                break;
            case 21:
                strFieldName = "FYCZeroMon";
                break;
            case 22:
                strFieldName = "IndRate";
                break;
            case 23:
                strFieldName = "TeamAvgRate";
                break;
            case 24:
                strFieldName = "DRTeamAvgRate";
                break;
            case 25:
                strFieldName = "DepAvgRate";
                break;
            case 26:
                strFieldName = "DRDepAvgRate";
                break;
            case 27:
                strFieldName = "AreaRate";
                break;
            case 28:
                strFieldName = "IndNewCustSum";
                break;
            case 29:
                strFieldName = "IndCustSum";
                break;
            case 30:
                strFieldName = "MonAvgCust";
                break;
            case 31:
                strFieldName = "DirAddCount";
                break;
            case 32:
                strFieldName = "AddCount";
                break;
            case 33:
                strFieldName = "ReachGrade";
                break;
            case 34:
                strFieldName = "ReachGradeCount";
                break;
            case 35:
                strFieldName = "MngAgentCount";
                break;
            case 36:
                strFieldName = "MngFinaCount";
                break;
            case 37:
                strFieldName = "DirRmdCount";
                break;
            case 38:
                strFieldName = "DirMngCount";
                break;
            case 39:
                strFieldName = "DirMngFinaCount";
                break;
            case 40:
                strFieldName = "DepCount";
                break;
            case 41:
                strFieldName = "DRTeamCount";
                break;
            case 42:
                strFieldName = "RearTeamSum";
                break;
            case 43:
                strFieldName = "RearDepCount";
                break;
            case 44:
                strFieldName = "RearDepSum";
                break;
            case 45:
                strFieldName = "MngDepCount";
                break;
            case 46:
                strFieldName = "DepFinaCount";
                break;
            case 47:
                strFieldName = "ReqDepCount";
                break;
            case 48:
                strFieldName = "ReqPeriod";
                break;
            case 49:
                strFieldName = "MngAreaFinaCount";
                break;
            case 50:
                strFieldName = "DirRearAdmCount";
                break;
            case 51:
                strFieldName = "RearAdmCount";
                break;
            case 52:
                strFieldName = "LaborFYCStand";
                break;
            case 53:
                strFieldName = "MonAvgLabor";
                break;
            case 54:
                strFieldName = "DRMonLabor";
                break;
            case 55:
                strFieldName = "DRDepLabor";
                break;
            case 56:
                strFieldName = "AreaAdmRmdFlag";
                break;
            case 57:
                strFieldName = "RebateRate";
                break;
            case 58:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestAgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AssessCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("YearCode"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("LimitPeriod"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("QuaPassFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExamPassFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WorkPassFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MonAvgFYC"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("IndFYCSum"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("IndAddFYCSum"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("TeamFYCSum"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DirTeamFYCSum"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DRFYCSum"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FormerRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("LatterRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DepFYCSum"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DirDepMonAvgFYC"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FYCZeroMon"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("IndRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("TeamAvgRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DRTeamAvgRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DepAvgRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DRDepAvgRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AreaRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("IndNewCustSum"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("IndCustSum"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MonAvgCust"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DirAddCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AddCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ReachGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReachGradeCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MngAgentCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MngFinaCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DirRmdCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DirMngCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DirMngFinaCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DepCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DRTeamCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RearTeamSum"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RearDepCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RearDepSum"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MngDepCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DepFinaCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ReqDepCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ReqPeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MngAreaFinaCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DirRearAdmCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RearAdmCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("LaborFYCStand"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MonAvgLabor"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DRMonLabor"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DRDepLabor"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AreaAdmRmdFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RebateRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 15:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 16:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 21:
                nFieldType = Schema.TYPE_INT;
                break;
            case 22:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 23:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 24:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 25:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 26:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 27:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 28:
                nFieldType = Schema.TYPE_INT;
                break;
            case 29:
                nFieldType = Schema.TYPE_INT;
                break;
            case 30:
                nFieldType = Schema.TYPE_INT;
                break;
            case 31:
                nFieldType = Schema.TYPE_INT;
                break;
            case 32:
                nFieldType = Schema.TYPE_INT;
                break;
            case 33:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 34:
                nFieldType = Schema.TYPE_INT;
                break;
            case 35:
                nFieldType = Schema.TYPE_INT;
                break;
            case 36:
                nFieldType = Schema.TYPE_INT;
                break;
            case 37:
                nFieldType = Schema.TYPE_INT;
                break;
            case 38:
                nFieldType = Schema.TYPE_INT;
                break;
            case 39:
                nFieldType = Schema.TYPE_INT;
                break;
            case 40:
                nFieldType = Schema.TYPE_INT;
                break;
            case 41:
                nFieldType = Schema.TYPE_INT;
                break;
            case 42:
                nFieldType = Schema.TYPE_INT;
                break;
            case 43:
                nFieldType = Schema.TYPE_INT;
                break;
            case 44:
                nFieldType = Schema.TYPE_INT;
                break;
            case 45:
                nFieldType = Schema.TYPE_INT;
                break;
            case 46:
                nFieldType = Schema.TYPE_INT;
                break;
            case 47:
                nFieldType = Schema.TYPE_INT;
                break;
            case 48:
                nFieldType = Schema.TYPE_INT;
                break;
            case 49:
                nFieldType = Schema.TYPE_INT;
                break;
            case 50:
                nFieldType = Schema.TYPE_INT;
                break;
            case 51:
                nFieldType = Schema.TYPE_INT;
                break;
            case 52:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 53:
                nFieldType = Schema.TYPE_INT;
                break;
            case 54:
                nFieldType = Schema.TYPE_INT;
                break;
            case 55:
                nFieldType = Schema.TYPE_INT;
                break;
            case 56:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 57:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 58:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
