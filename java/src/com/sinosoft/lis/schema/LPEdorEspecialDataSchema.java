/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LPEdorEspecialDataDB;

/*
 * <p>ClassName: LPEdorEspecialDataSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-17
 */
public class LPEdorEspecialDataSchema implements Schema, Cloneable {
    // @Field
    /** 受理号 */
    private String EdorAcceptNo;
    /** 批单号 */
    private String EdorNo;
    /** 批改类型 */
    private String EdorType;
    /** 具体类型 */
    private String DetailType;
    /** 值 */
    private String EdorValue;
    /** 状态 */
    private String State;
    /** 险种号 */
    private String PolNo;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPEdorEspecialDataSchema() {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "EdorAcceptNo";
        pk[1] = "EdorNo";
        pk[2] = "EdorType";
        pk[3] = "DetailType";
        pk[4] = "PolNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LPEdorEspecialDataSchema cloned = (LPEdorEspecialDataSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getEdorAcceptNo() {
        return EdorAcceptNo;
    }

    public void setEdorAcceptNo(String aEdorAcceptNo) {
        EdorAcceptNo = aEdorAcceptNo;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String aEdorType) {
        EdorType = aEdorType;
    }

    public String getDetailType() {
        return DetailType;
    }

    public void setDetailType(String aDetailType) {
        DetailType = aDetailType;
    }

    public String getEdorValue() {
        return EdorValue;
    }

    public void setEdorValue(String aEdorValue) {
        EdorValue = aEdorValue;
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }

    /**
     * 使用另外一个 LPEdorEspecialDataSchema 对象给 Schema 赋值
     * @param: aLPEdorEspecialDataSchema LPEdorEspecialDataSchema
     **/
    public void setSchema(LPEdorEspecialDataSchema aLPEdorEspecialDataSchema) {
        this.EdorAcceptNo = aLPEdorEspecialDataSchema.getEdorAcceptNo();
        this.EdorNo = aLPEdorEspecialDataSchema.getEdorNo();
        this.EdorType = aLPEdorEspecialDataSchema.getEdorType();
        this.DetailType = aLPEdorEspecialDataSchema.getDetailType();
        this.EdorValue = aLPEdorEspecialDataSchema.getEdorValue();
        this.State = aLPEdorEspecialDataSchema.getState();
        this.PolNo = aLPEdorEspecialDataSchema.getPolNo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorAcceptNo") == null) {
                this.EdorAcceptNo = null;
            } else {
                this.EdorAcceptNo = rs.getString("EdorAcceptNo").trim();
            }

            if (rs.getString("EdorNo") == null) {
                this.EdorNo = null;
            } else {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("EdorType") == null) {
                this.EdorType = null;
            } else {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("DetailType") == null) {
                this.DetailType = null;
            } else {
                this.DetailType = rs.getString("DetailType").trim();
            }

            if (rs.getString("EdorValue") == null) {
                this.EdorValue = null;
            } else {
                this.EdorValue = rs.getString("EdorValue").trim();
            }

            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("PolNo") == null) {
                this.PolNo = null;
            } else {
                this.PolNo = rs.getString("PolNo").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LPEdorEspecialData表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPEdorEspecialDataSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LPEdorEspecialDataSchema getSchema() {
        LPEdorEspecialDataSchema aLPEdorEspecialDataSchema = new
                LPEdorEspecialDataSchema();
        aLPEdorEspecialDataSchema.setSchema(this);
        return aLPEdorEspecialDataSchema;
    }

    public LPEdorEspecialDataDB getDB() {
        LPEdorEspecialDataDB aDBOper = new LPEdorEspecialDataDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPEdorEspecialData描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(EdorAcceptNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DetailType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorValue));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPEdorEspecialData>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            EdorAcceptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            DetailType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            EdorValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                   SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                   SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPEdorEspecialDataSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("EdorAcceptNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorAcceptNo));
        }
        if (FCode.equals("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("EdorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equals("DetailType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DetailType));
        }
        if (FCode.equals("EdorValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorValue));
        }
        if (FCode.equals("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(EdorAcceptNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(EdorNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(EdorType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(DetailType);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(EdorValue);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(PolNo);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("EdorAcceptNo")) {
            if (FValue != null && !FValue.equals("")) {
                EdorAcceptNo = FValue.trim();
            } else {
                EdorAcceptNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("EdorNo")) {
            if (FValue != null && !FValue.equals("")) {
                EdorNo = FValue.trim();
            } else {
                EdorNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("EdorType")) {
            if (FValue != null && !FValue.equals("")) {
                EdorType = FValue.trim();
            } else {
                EdorType = null;
            }
        }
        if (FCode.equalsIgnoreCase("DetailType")) {
            if (FValue != null && !FValue.equals("")) {
                DetailType = FValue.trim();
            } else {
                DetailType = null;
            }
        }
        if (FCode.equalsIgnoreCase("EdorValue")) {
            if (FValue != null && !FValue.equals("")) {
                EdorValue = FValue.trim();
            } else {
                EdorValue = null;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if (FValue != null && !FValue.equals("")) {
                PolNo = FValue.trim();
            } else {
                PolNo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LPEdorEspecialDataSchema other = (LPEdorEspecialDataSchema) otherObject;
        return
                EdorAcceptNo.equals(other.getEdorAcceptNo())
                && EdorNo.equals(other.getEdorNo())
                && EdorType.equals(other.getEdorType())
                && DetailType.equals(other.getDetailType())
                && EdorValue.equals(other.getEdorValue())
                && State.equals(other.getState())
                && PolNo.equals(other.getPolNo());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("EdorAcceptNo")) {
            return 0;
        }
        if (strFieldName.equals("EdorNo")) {
            return 1;
        }
        if (strFieldName.equals("EdorType")) {
            return 2;
        }
        if (strFieldName.equals("DetailType")) {
            return 3;
        }
        if (strFieldName.equals("EdorValue")) {
            return 4;
        }
        if (strFieldName.equals("State")) {
            return 5;
        }
        if (strFieldName.equals("PolNo")) {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "EdorAcceptNo";
            break;
        case 1:
            strFieldName = "EdorNo";
            break;
        case 2:
            strFieldName = "EdorType";
            break;
        case 3:
            strFieldName = "DetailType";
            break;
        case 4:
            strFieldName = "EdorValue";
            break;
        case 5:
            strFieldName = "State";
            break;
        case 6:
            strFieldName = "PolNo";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("EdorAcceptNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DetailType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorValue")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
