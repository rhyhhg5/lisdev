/*
 * <p>ClassName: LCAppntTableSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LCAppntTableDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LCAppntTableSchema implements Schema
{
    // @Field
    /** 保单号码 */
    private String PolNo;
    /** 客户号码 */
    private String CustomerNo;
    /** 投保人类型 */
    private String AppntType;
    /** 投保人级别 */
    private String AppntGrade;
    /** 被保人与投保人关系 */
    private String RelationToInsured;
    /** 密码 */
    private String Password;
    /** 客户姓名 */
    private String Name;
    /** 通讯地址 */
    private String PostalAddress;
    /** 邮政编码 */
    private String ZipCode;
    /** 电话 */
    private String Phone;
    /** 状态 */
    private String State;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCAppntTableSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getCustomerNo()
    {
        if (CustomerNo != null && !CustomerNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CustomerNo = StrTool.unicodeToGBK(CustomerNo);
        }
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo)
    {
        CustomerNo = aCustomerNo;
    }

    public String getAppntType()
    {
        if (AppntType != null && !AppntType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppntType = StrTool.unicodeToGBK(AppntType);
        }
        return AppntType;
    }

    public void setAppntType(String aAppntType)
    {
        AppntType = aAppntType;
    }

    public String getAppntGrade()
    {
        if (AppntGrade != null && !AppntGrade.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppntGrade = StrTool.unicodeToGBK(AppntGrade);
        }
        return AppntGrade;
    }

    public void setAppntGrade(String aAppntGrade)
    {
        AppntGrade = aAppntGrade;
    }

    public String getRelationToInsured()
    {
        if (RelationToInsured != null && !RelationToInsured.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RelationToInsured = StrTool.unicodeToGBK(RelationToInsured);
        }
        return RelationToInsured;
    }

    public void setRelationToInsured(String aRelationToInsured)
    {
        RelationToInsured = aRelationToInsured;
    }

    public String getPassword()
    {
        if (Password != null && !Password.equals("") && SysConst.CHANGECHARSET == true)
        {
            Password = StrTool.unicodeToGBK(Password);
        }
        return Password;
    }

    public void setPassword(String aPassword)
    {
        Password = aPassword;
    }

    public String getName()
    {
        if (Name != null && !Name.equals("") && SysConst.CHANGECHARSET == true)
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getPostalAddress()
    {
        if (PostalAddress != null && !PostalAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PostalAddress = StrTool.unicodeToGBK(PostalAddress);
        }
        return PostalAddress;
    }

    public void setPostalAddress(String aPostalAddress)
    {
        PostalAddress = aPostalAddress;
    }

    public String getZipCode()
    {
        if (ZipCode != null && !ZipCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ZipCode = StrTool.unicodeToGBK(ZipCode);
        }
        return ZipCode;
    }

    public void setZipCode(String aZipCode)
    {
        ZipCode = aZipCode;
    }

    public String getPhone()
    {
        if (Phone != null && !Phone.equals("") && SysConst.CHANGECHARSET == true)
        {
            Phone = StrTool.unicodeToGBK(Phone);
        }
        return Phone;
    }

    public void setPhone(String aPhone)
    {
        Phone = aPhone;
    }

    public String getState()
    {
        if (State != null && !State.equals("") && SysConst.CHANGECHARSET == true)
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    /**
     * 使用另外一个 LCAppntTableSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LCAppntTableSchema aLCAppntTableSchema)
    {
        this.PolNo = aLCAppntTableSchema.getPolNo();
        this.CustomerNo = aLCAppntTableSchema.getCustomerNo();
        this.AppntType = aLCAppntTableSchema.getAppntType();
        this.AppntGrade = aLCAppntTableSchema.getAppntGrade();
        this.RelationToInsured = aLCAppntTableSchema.getRelationToInsured();
        this.Password = aLCAppntTableSchema.getPassword();
        this.Name = aLCAppntTableSchema.getName();
        this.PostalAddress = aLCAppntTableSchema.getPostalAddress();
        this.ZipCode = aLCAppntTableSchema.getZipCode();
        this.Phone = aLCAppntTableSchema.getPhone();
        this.State = aLCAppntTableSchema.getState();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("CustomerNo") == null)
            {
                this.CustomerNo = null;
            }
            else
            {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("AppntType") == null)
            {
                this.AppntType = null;
            }
            else
            {
                this.AppntType = rs.getString("AppntType").trim();
            }

            if (rs.getString("AppntGrade") == null)
            {
                this.AppntGrade = null;
            }
            else
            {
                this.AppntGrade = rs.getString("AppntGrade").trim();
            }

            if (rs.getString("RelationToInsured") == null)
            {
                this.RelationToInsured = null;
            }
            else
            {
                this.RelationToInsured = rs.getString("RelationToInsured").trim();
            }

            if (rs.getString("Password") == null)
            {
                this.Password = null;
            }
            else
            {
                this.Password = rs.getString("Password").trim();
            }

            if (rs.getString("Name") == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString("Name").trim();
            }

            if (rs.getString("PostalAddress") == null)
            {
                this.PostalAddress = null;
            }
            else
            {
                this.PostalAddress = rs.getString("PostalAddress").trim();
            }

            if (rs.getString("ZipCode") == null)
            {
                this.ZipCode = null;
            }
            else
            {
                this.ZipCode = rs.getString("ZipCode").trim();
            }

            if (rs.getString("Phone") == null)
            {
                this.Phone = null;
            }
            else
            {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntTableSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCAppntTableSchema getSchema()
    {
        LCAppntTableSchema aLCAppntTableSchema = new LCAppntTableSchema();
        aLCAppntTableSchema.setSchema(this);
        return aLCAppntTableSchema;
    }

    public LCAppntTableDB getDB()
    {
        LCAppntTableDB aDBOper = new LCAppntTableDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAppntTable描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CustomerNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppntGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RelationToInsured)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Password)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PostalAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ZipCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Phone)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCAppntTable>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            AppntType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            AppntGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            RelationToInsured = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               5, SysConst.PACKAGESPILTER);
            Password = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                  SysConst.PACKAGESPILTER);
            PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                   SysConst.PACKAGESPILTER);
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                   SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCAppntTableSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("CustomerNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CustomerNo));
        }
        if (FCode.equals("AppntType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntType));
        }
        if (FCode.equals("AppntGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppntGrade));
        }
        if (FCode.equals("RelationToInsured"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    RelationToInsured));
        }
        if (FCode.equals("Password"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Password));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Name));
        }
        if (FCode.equals("PostalAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PostalAddress));
        }
        if (FCode.equals("ZipCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ZipCode));
        }
        if (FCode.equals("Phone"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Phone));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(State));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AppntType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AppntGrade);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RelationToInsured);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Password);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(PostalAddress);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(ZipCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Phone);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("CustomerNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
            {
                CustomerNo = null;
            }
        }
        if (FCode.equals("AppntType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntType = FValue.trim();
            }
            else
            {
                AppntType = null;
            }
        }
        if (FCode.equals("AppntGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppntGrade = FValue.trim();
            }
            else
            {
                AppntGrade = null;
            }
        }
        if (FCode.equals("RelationToInsured"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelationToInsured = FValue.trim();
            }
            else
            {
                RelationToInsured = null;
            }
        }
        if (FCode.equals("Password"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Password = FValue.trim();
            }
            else
            {
                Password = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("PostalAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PostalAddress = FValue.trim();
            }
            else
            {
                PostalAddress = null;
            }
        }
        if (FCode.equals("ZipCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ZipCode = FValue.trim();
            }
            else
            {
                ZipCode = null;
            }
        }
        if (FCode.equals("Phone"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Phone = FValue.trim();
            }
            else
            {
                Phone = null;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCAppntTableSchema other = (LCAppntTableSchema) otherObject;
        return
                PolNo.equals(other.getPolNo())
                && CustomerNo.equals(other.getCustomerNo())
                && AppntType.equals(other.getAppntType())
                && AppntGrade.equals(other.getAppntGrade())
                && RelationToInsured.equals(other.getRelationToInsured())
                && Password.equals(other.getPassword())
                && Name.equals(other.getName())
                && PostalAddress.equals(other.getPostalAddress())
                && ZipCode.equals(other.getZipCode())
                && Phone.equals(other.getPhone())
                && State.equals(other.getState());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("PolNo"))
        {
            return 0;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return 1;
        }
        if (strFieldName.equals("AppntType"))
        {
            return 2;
        }
        if (strFieldName.equals("AppntGrade"))
        {
            return 3;
        }
        if (strFieldName.equals("RelationToInsured"))
        {
            return 4;
        }
        if (strFieldName.equals("Password"))
        {
            return 5;
        }
        if (strFieldName.equals("Name"))
        {
            return 6;
        }
        if (strFieldName.equals("PostalAddress"))
        {
            return 7;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return 8;
        }
        if (strFieldName.equals("Phone"))
        {
            return 9;
        }
        if (strFieldName.equals("State"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "PolNo";
                break;
            case 1:
                strFieldName = "CustomerNo";
                break;
            case 2:
                strFieldName = "AppntType";
                break;
            case 3:
                strFieldName = "AppntGrade";
                break;
            case 4:
                strFieldName = "RelationToInsured";
                break;
            case 5:
                strFieldName = "Password";
                break;
            case 6:
                strFieldName = "Name";
                break;
            case 7:
                strFieldName = "PostalAddress";
                break;
            case 8:
                strFieldName = "ZipCode";
                break;
            case 9:
                strFieldName = "Phone";
                break;
            case 10:
                strFieldName = "State";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelationToInsured"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Password"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PostalAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZipCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
