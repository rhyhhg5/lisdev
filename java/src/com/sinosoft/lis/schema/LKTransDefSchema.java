/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKTransDefDB;

/*
 * <p>ClassName: LKTransDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-06-16
 */
public class LKTransDefSchema implements Schema, Cloneable
{
	// @Field
	/** Transcode */
	private String TransCode;
	/** Transdesc */
	private String TransDesc;
	/** Transtype */
	private String TransType;
	/** Transid */
	private String TransID;
	/** Requestcode */
	private String RequestCode;
	/** Reponsecode */
	private String ReponseCode;
	/** Remark */
	private String Remark;

	public static final int FIELDNUM = 7;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKTransDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "TransCode";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LKTransDefSchema cloned = (LKTransDefSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTransCode()
	{
		if (SysConst.CHANGECHARSET && TransCode != null && !TransCode.equals(""))
		{
			TransCode = StrTool.unicodeToGBK(TransCode);
		}
		return TransCode;
	}
	public void setTransCode(String aTransCode)
	{
		TransCode = aTransCode;
	}
	public String getTransDesc()
	{
		if (SysConst.CHANGECHARSET && TransDesc != null && !TransDesc.equals(""))
		{
			TransDesc = StrTool.unicodeToGBK(TransDesc);
		}
		return TransDesc;
	}
	public void setTransDesc(String aTransDesc)
	{
		TransDesc = aTransDesc;
	}
	public String getTransType()
	{
		if (SysConst.CHANGECHARSET && TransType != null && !TransType.equals(""))
		{
			TransType = StrTool.unicodeToGBK(TransType);
		}
		return TransType;
	}
	public void setTransType(String aTransType)
	{
		TransType = aTransType;
	}
	public String getTransID()
	{
		if (SysConst.CHANGECHARSET && TransID != null && !TransID.equals(""))
		{
			TransID = StrTool.unicodeToGBK(TransID);
		}
		return TransID;
	}
	public void setTransID(String aTransID)
	{
		TransID = aTransID;
	}
	public String getRequestCode()
	{
		if (SysConst.CHANGECHARSET && RequestCode != null && !RequestCode.equals(""))
		{
			RequestCode = StrTool.unicodeToGBK(RequestCode);
		}
		return RequestCode;
	}
	public void setRequestCode(String aRequestCode)
	{
		RequestCode = aRequestCode;
	}
	public String getReponseCode()
	{
		if (SysConst.CHANGECHARSET && ReponseCode != null && !ReponseCode.equals(""))
		{
			ReponseCode = StrTool.unicodeToGBK(ReponseCode);
		}
		return ReponseCode;
	}
	public void setReponseCode(String aReponseCode)
	{
		ReponseCode = aReponseCode;
	}
	public String getRemark()
	{
		if (SysConst.CHANGECHARSET && Remark != null && !Remark.equals(""))
		{
			Remark = StrTool.unicodeToGBK(Remark);
		}
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}

	/**
	* 使用另外一个 LKTransDefSchema 对象给 Schema 赋值
	* @param: aLKTransDefSchema LKTransDefSchema
	**/
	public void setSchema(LKTransDefSchema aLKTransDefSchema)
	{
		this.TransCode = aLKTransDefSchema.getTransCode();
		this.TransDesc = aLKTransDefSchema.getTransDesc();
		this.TransType = aLKTransDefSchema.getTransType();
		this.TransID = aLKTransDefSchema.getTransID();
		this.RequestCode = aLKTransDefSchema.getRequestCode();
		this.ReponseCode = aLKTransDefSchema.getReponseCode();
		this.Remark = aLKTransDefSchema.getRemark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TransCode") == null )
				this.TransCode = null;
			else
				this.TransCode = rs.getString("TransCode").trim();

			if( rs.getString("TransDesc") == null )
				this.TransDesc = null;
			else
				this.TransDesc = rs.getString("TransDesc").trim();

			if( rs.getString("TransType") == null )
				this.TransType = null;
			else
				this.TransType = rs.getString("TransType").trim();

			if( rs.getString("TransID") == null )
				this.TransID = null;
			else
				this.TransID = rs.getString("TransID").trim();

			if( rs.getString("RequestCode") == null )
				this.RequestCode = null;
			else
				this.RequestCode = rs.getString("RequestCode").trim();

			if( rs.getString("ReponseCode") == null )
				this.ReponseCode = null;
			else
				this.ReponseCode = rs.getString("ReponseCode").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

		}
		catch(SQLException sqle)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKTransDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	public LKTransDefSchema getSchema()
	{
		LKTransDefSchema aLKTransDefSchema = new LKTransDefSchema();
		aLKTransDefSchema.setSchema(this);
		return aLKTransDefSchema;
	}

	public LKTransDefDB getDB()
	{
		LKTransDefDB aDBOper = new LKTransDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(TransCode))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(TransDesc))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(TransType))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(TransID))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(RequestCode))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(ReponseCode))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim( StrTool.unicodeToGBK(Remark)));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TransDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			TransType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TransID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RequestCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ReponseCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKTransDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TransCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransCode));
		}
		if (FCode.equals("TransDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransDesc));
		}
		if (FCode.equals("TransType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransType));
		}
		if (FCode.equals("TransID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransID));
		}
		if (FCode.equals("RequestCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RequestCode));
		}
		if (FCode.equals("ReponseCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReponseCode));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TransCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TransDesc);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(TransType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TransID);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(RequestCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ReponseCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equals("TransCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransCode = FValue.trim();
			}
			else
				TransCode = null;
		}
		if (FCode.equals("TransDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransDesc = FValue.trim();
			}
			else
				TransDesc = null;
		}
		if (FCode.equals("TransType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransType = FValue.trim();
			}
			else
				TransType = null;
		}
		if (FCode.equals("TransID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransID = FValue.trim();
			}
			else
				TransID = null;
		}
		if (FCode.equals("RequestCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RequestCode = FValue.trim();
			}
			else
				RequestCode = null;
		}
		if (FCode.equals("ReponseCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReponseCode = FValue.trim();
			}
			else
				ReponseCode = null;
		}
		if (FCode.equals("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKTransDefSchema other = (LKTransDefSchema)otherObject;
		return
			TransCode.equals(other.getTransCode())
			&& TransDesc.equals(other.getTransDesc())
			&& TransType.equals(other.getTransType())
			&& TransID.equals(other.getTransID())
			&& RequestCode.equals(other.getRequestCode())
			&& ReponseCode.equals(other.getReponseCode())
			&& Remark.equals(other.getRemark());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TransCode") ) {
			return 0;
		}
		if( strFieldName.equals("TransDesc") ) {
			return 1;
		}
		if( strFieldName.equals("TransType") ) {
			return 2;
		}
		if( strFieldName.equals("TransID") ) {
			return 3;
		}
		if( strFieldName.equals("RequestCode") ) {
			return 4;
		}
		if( strFieldName.equals("ReponseCode") ) {
			return 5;
		}
		if( strFieldName.equals("Remark") ) {
			return 6;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TransCode";
				break;
			case 1:
				strFieldName = "TransDesc";
				break;
			case 2:
				strFieldName = "TransType";
				break;
			case 3:
				strFieldName = "TransID";
				break;
			case 4:
				strFieldName = "RequestCode";
				break;
			case 5:
				strFieldName = "ReponseCode";
				break;
			case 6:
				strFieldName = "Remark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TransCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RequestCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReponseCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
