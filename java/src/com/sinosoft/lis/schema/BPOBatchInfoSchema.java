/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.BPOBatchInfoDB;

/*
 * <p>ClassName: BPOBatchInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 外包录入
 * @CreateDate：2007-09-24
 */
public class BPOBatchInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 外包商标识 */
	private String BPOID;
	/** 核心系统扫描件备份相对路径 */
	private String BackupBasePath;
	/** 扫描件份数 */
	private int DocNumber;
	/** 扫描页数 */
	private int PageNumber;
	/** 回送扫描件份数 */
	private int DocNumberBack;
	/** 发送日期 */
	private Date SendOutDate;
	/** 发送时间 */
	private String SendOutTime;
	/** 接收日期 */
	private Date ReceiveDate;
	/** 接收时间 */
	private String ReceiveTime;
	/** 状态 */
	private String State;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public BPOBatchInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BatchNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                BPOBatchInfoSchema cloned = (BPOBatchInfoSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
            BatchNo = aBatchNo;
	}
	public String getBPOID()
	{
		return BPOID;
	}
	public void setBPOID(String aBPOID)
	{
            BPOID = aBPOID;
	}
	public String getBackupBasePath()
	{
		return BackupBasePath;
	}
	public void setBackupBasePath(String aBackupBasePath)
	{
            BackupBasePath = aBackupBasePath;
	}
	public int getDocNumber()
	{
		return DocNumber;
	}
	public void setDocNumber(int aDocNumber)
	{
            DocNumber = aDocNumber;
	}
	public void setDocNumber(String aDocNumber)
	{
		if (aDocNumber != null && !aDocNumber.equals(""))
		{
			Integer tInteger = new Integer(aDocNumber);
			int i = tInteger.intValue();
			DocNumber = i;
		}
	}

	public int getPageNumber()
	{
		return PageNumber;
	}
	public void setPageNumber(int aPageNumber)
	{
            PageNumber = aPageNumber;
	}
	public void setPageNumber(String aPageNumber)
	{
		if (aPageNumber != null && !aPageNumber.equals(""))
		{
			Integer tInteger = new Integer(aPageNumber);
			int i = tInteger.intValue();
			PageNumber = i;
		}
	}

	public int getDocNumberBack()
	{
		return DocNumberBack;
	}
	public void setDocNumberBack(int aDocNumberBack)
	{
            DocNumberBack = aDocNumberBack;
	}
	public void setDocNumberBack(String aDocNumberBack)
	{
		if (aDocNumberBack != null && !aDocNumberBack.equals(""))
		{
			Integer tInteger = new Integer(aDocNumberBack);
			int i = tInteger.intValue();
			DocNumberBack = i;
		}
	}

	public String getSendOutDate()
	{
		if( SendOutDate != null )
			return fDate.getString(SendOutDate);
		else
			return null;
	}
	public void setSendOutDate(Date aSendOutDate)
	{
            SendOutDate = aSendOutDate;
	}
	public void setSendOutDate(String aSendOutDate)
	{
		if (aSendOutDate != null && !aSendOutDate.equals("") )
		{
			SendOutDate = fDate.getDate( aSendOutDate );
		}
		else
			SendOutDate = null;
	}

	public String getSendOutTime()
	{
		return SendOutTime;
	}
	public void setSendOutTime(String aSendOutTime)
	{
            SendOutTime = aSendOutTime;
	}
	public String getReceiveDate()
	{
		if( ReceiveDate != null )
			return fDate.getString(ReceiveDate);
		else
			return null;
	}
	public void setReceiveDate(Date aReceiveDate)
	{
            ReceiveDate = aReceiveDate;
	}
	public void setReceiveDate(String aReceiveDate)
	{
		if (aReceiveDate != null && !aReceiveDate.equals("") )
		{
			ReceiveDate = fDate.getDate( aReceiveDate );
		}
		else
			ReceiveDate = null;
	}

	public String getReceiveTime()
	{
		return ReceiveTime;
	}
	public void setReceiveTime(String aReceiveTime)
	{
            ReceiveTime = aReceiveTime;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
            State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 BPOBatchInfoSchema 对象给 Schema 赋值
	* @param: aBPOBatchInfoSchema BPOBatchInfoSchema
	**/
	public void setSchema(BPOBatchInfoSchema aBPOBatchInfoSchema)
	{
		this.BatchNo = aBPOBatchInfoSchema.getBatchNo();
		this.BPOID = aBPOBatchInfoSchema.getBPOID();
		this.BackupBasePath = aBPOBatchInfoSchema.getBackupBasePath();
		this.DocNumber = aBPOBatchInfoSchema.getDocNumber();
		this.PageNumber = aBPOBatchInfoSchema.getPageNumber();
		this.DocNumberBack = aBPOBatchInfoSchema.getDocNumberBack();
		this.SendOutDate = fDate.getDate( aBPOBatchInfoSchema.getSendOutDate());
		this.SendOutTime = aBPOBatchInfoSchema.getSendOutTime();
		this.ReceiveDate = fDate.getDate( aBPOBatchInfoSchema.getReceiveDate());
		this.ReceiveTime = aBPOBatchInfoSchema.getReceiveTime();
		this.State = aBPOBatchInfoSchema.getState();
		this.Operator = aBPOBatchInfoSchema.getOperator();
		this.MakeDate = fDate.getDate( aBPOBatchInfoSchema.getMakeDate());
		this.MakeTime = aBPOBatchInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aBPOBatchInfoSchema.getModifyDate());
		this.ModifyTime = aBPOBatchInfoSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("BPOID") == null )
				this.BPOID = null;
			else
				this.BPOID = rs.getString("BPOID").trim();

			if( rs.getString("BackupBasePath") == null )
				this.BackupBasePath = null;
			else
				this.BackupBasePath = rs.getString("BackupBasePath").trim();

			this.DocNumber = rs.getInt("DocNumber");
			this.PageNumber = rs.getInt("PageNumber");
			this.DocNumberBack = rs.getInt("DocNumberBack");
			this.SendOutDate = rs.getDate("SendOutDate");
			if( rs.getString("SendOutTime") == null )
				this.SendOutTime = null;
			else
				this.SendOutTime = rs.getString("SendOutTime").trim();

			this.ReceiveDate = rs.getDate("ReceiveDate");
			if( rs.getString("ReceiveTime") == null )
				this.ReceiveTime = null;
			else
				this.ReceiveTime = rs.getString("ReceiveTime").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的BPOBatchInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOBatchInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public BPOBatchInfoSchema getSchema()
	{
		BPOBatchInfoSchema aBPOBatchInfoSchema = new BPOBatchInfoSchema();
		aBPOBatchInfoSchema.setSchema(this);
		return aBPOBatchInfoSchema;
	}

	public BPOBatchInfoDB getDB()
	{
		BPOBatchInfoDB aDBOper = new BPOBatchInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOBatchInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BPOID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BackupBasePath)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DocNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PageNumber));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(DocNumberBack));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( SendOutDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SendOutTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ReceiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ReceiveTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBPOBatchInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BPOID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BackupBasePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DocNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			PageNumber= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).intValue();
			DocNumberBack= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
			SendOutDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			SendOutTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			ReceiveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BPOBatchInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("BPOID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BPOID));
		}
		if (FCode.equals("BackupBasePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackupBasePath));
		}
		if (FCode.equals("DocNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DocNumber));
		}
		if (FCode.equals("PageNumber"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PageNumber));
		}
		if (FCode.equals("DocNumberBack"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DocNumberBack));
		}
		if (FCode.equals("SendOutDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSendOutDate()));
		}
		if (FCode.equals("SendOutTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendOutTime));
		}
		if (FCode.equals("ReceiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
		}
		if (FCode.equals("ReceiveTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BPOID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BackupBasePath);
				break;
			case 3:
				strFieldValue = String.valueOf(DocNumber);
				break;
			case 4:
				strFieldValue = String.valueOf(PageNumber);
				break;
			case 5:
				strFieldValue = String.valueOf(DocNumberBack);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSendOutDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(SendOutTime);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReceiveDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ReceiveTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("BPOID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BPOID = FValue.trim();
			}
			else
				BPOID = null;
		}
		if (FCode.equalsIgnoreCase("BackupBasePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackupBasePath = FValue.trim();
			}
			else
				BackupBasePath = null;
		}
		if (FCode.equalsIgnoreCase("DocNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DocNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("PageNumber"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PageNumber = i;
			}
		}
		if (FCode.equalsIgnoreCase("DocNumberBack"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DocNumberBack = i;
			}
		}
		if (FCode.equalsIgnoreCase("SendOutDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SendOutDate = fDate.getDate( FValue );
			}
			else
				SendOutDate = null;
		}
		if (FCode.equalsIgnoreCase("SendOutTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendOutTime = FValue.trim();
			}
			else
				SendOutTime = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ReceiveDate = fDate.getDate( FValue );
			}
			else
				ReceiveDate = null;
		}
		if (FCode.equalsIgnoreCase("ReceiveTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiveTime = FValue.trim();
			}
			else
				ReceiveTime = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		BPOBatchInfoSchema other = (BPOBatchInfoSchema)otherObject;
		return
			BatchNo.equals(other.getBatchNo())
			&& BPOID.equals(other.getBPOID())
			&& BackupBasePath.equals(other.getBackupBasePath())
			&& DocNumber == other.getDocNumber()
			&& PageNumber == other.getPageNumber()
			&& DocNumberBack == other.getDocNumberBack()
			&& fDate.getString(SendOutDate).equals(other.getSendOutDate())
			&& SendOutTime.equals(other.getSendOutTime())
			&& fDate.getString(ReceiveDate).equals(other.getReceiveDate())
			&& ReceiveTime.equals(other.getReceiveTime())
			&& State.equals(other.getState())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("BPOID") ) {
			return 1;
		}
		if( strFieldName.equals("BackupBasePath") ) {
			return 2;
		}
		if( strFieldName.equals("DocNumber") ) {
			return 3;
		}
		if( strFieldName.equals("PageNumber") ) {
			return 4;
		}
		if( strFieldName.equals("DocNumberBack") ) {
			return 5;
		}
		if( strFieldName.equals("SendOutDate") ) {
			return 6;
		}
		if( strFieldName.equals("SendOutTime") ) {
			return 7;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return 8;
		}
		if( strFieldName.equals("ReceiveTime") ) {
			return 9;
		}
		if( strFieldName.equals("State") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "BPOID";
				break;
			case 2:
				strFieldName = "BackupBasePath";
				break;
			case 3:
				strFieldName = "DocNumber";
				break;
			case 4:
				strFieldName = "PageNumber";
				break;
			case 5:
				strFieldName = "DocNumberBack";
				break;
			case 6:
				strFieldName = "SendOutDate";
				break;
			case 7:
				strFieldName = "SendOutTime";
				break;
			case 8:
				strFieldName = "ReceiveDate";
				break;
			case 9:
				strFieldName = "ReceiveTime";
				break;
			case 10:
				strFieldName = "State";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BPOID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackupBasePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DocNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PageNumber") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DocNumberBack") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SendOutDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SendOutTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReceiveTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_INT;
				break;
			case 5:
				nFieldType = Schema.TYPE_INT;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
