/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.HMCustExamInfoDB;

/*
 * <p>ClassName: HMCustExamInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-03-17
 */
public class HMCustExamInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 客户号码 */
	private String CustomerNo;
	/** 检查序号 */
	private String TestNo;
	/** 医疗服务项目代码 */
	private String MedicaItemCode;
	/** 结果单位数值 */
	private String ResultUnitNum;
	/** 检查结果 */
	private String TestResult;
	/** 检查日期 */
	private Date TestDate;
	/** 医师代码 */
	private String DoctNo;
	/** 检查费用 */
	private double TestFeeAmount;
	/** 是否异常 */
	private String IsNormal;
	/** 操作员代码 */
	private String Operator;
	/** 管理机构 */
	private String ManageCom;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 标准计量单位 */
	private String StandardMeasureUnit;
	/** 正常值 */
	private String NormalValue;
	/** 备用字段1 */
	private String StandbyField1;
	/** 备用字段2 */
	private String StandbyField2;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public HMCustExamInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "CustomerNo";
		pk[1] = "TestNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		HMCustExamInfoSchema cloned = (HMCustExamInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getTestNo()
	{
		return TestNo;
	}
	public void setTestNo(String aTestNo)
	{
		TestNo = aTestNo;
	}
	public String getMedicaItemCode()
	{
		return MedicaItemCode;
	}
	public void setMedicaItemCode(String aMedicaItemCode)
	{
		MedicaItemCode = aMedicaItemCode;
	}
	public String getResultUnitNum()
	{
		return ResultUnitNum;
	}
	public void setResultUnitNum(String aResultUnitNum)
	{
		ResultUnitNum = aResultUnitNum;
	}
	public String getTestResult()
	{
		return TestResult;
	}
	public void setTestResult(String aTestResult)
	{
		TestResult = aTestResult;
	}
	public String getTestDate()
	{
		if( TestDate != null )
			return fDate.getString(TestDate);
		else
			return null;
	}
	public void setTestDate(Date aTestDate)
	{
		TestDate = aTestDate;
	}
	public void setTestDate(String aTestDate)
	{
		if (aTestDate != null && !aTestDate.equals("") )
		{
			TestDate = fDate.getDate( aTestDate );
		}
		else
			TestDate = null;
	}

	public String getDoctNo()
	{
		return DoctNo;
	}
	public void setDoctNo(String aDoctNo)
	{
		DoctNo = aDoctNo;
	}
	public double getTestFeeAmount()
	{
		return TestFeeAmount;
	}
	public void setTestFeeAmount(double aTestFeeAmount)
	{
		TestFeeAmount = Arith.round(aTestFeeAmount,2);
	}
	public void setTestFeeAmount(String aTestFeeAmount)
	{
		if (aTestFeeAmount != null && !aTestFeeAmount.equals(""))
		{
			Double tDouble = new Double(aTestFeeAmount);
			double d = tDouble.doubleValue();
                TestFeeAmount = Arith.round(d,2);
		}
	}

	public String getIsNormal()
	{
		return IsNormal;
	}
	public void setIsNormal(String aIsNormal)
	{
		IsNormal = aIsNormal;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getStandardMeasureUnit()
	{
		return StandardMeasureUnit;
	}
	public void setStandardMeasureUnit(String aStandardMeasureUnit)
	{
		StandardMeasureUnit = aStandardMeasureUnit;
	}
	public String getNormalValue()
	{
		return NormalValue;
	}
	public void setNormalValue(String aNormalValue)
	{
		NormalValue = aNormalValue;
	}
	public String getStandbyField1()
	{
		return StandbyField1;
	}
	public void setStandbyField1(String aStandbyField1)
	{
		StandbyField1 = aStandbyField1;
	}
	public String getStandbyField2()
	{
		return StandbyField2;
	}
	public void setStandbyField2(String aStandbyField2)
	{
		StandbyField2 = aStandbyField2;
	}

	/**
	* 使用另外一个 HMCustExamInfoSchema 对象给 Schema 赋值
	* @param: aHMCustExamInfoSchema HMCustExamInfoSchema
	**/
	public void setSchema(HMCustExamInfoSchema aHMCustExamInfoSchema)
	{
		this.CustomerNo = aHMCustExamInfoSchema.getCustomerNo();
		this.TestNo = aHMCustExamInfoSchema.getTestNo();
		this.MedicaItemCode = aHMCustExamInfoSchema.getMedicaItemCode();
		this.ResultUnitNum = aHMCustExamInfoSchema.getResultUnitNum();
		this.TestResult = aHMCustExamInfoSchema.getTestResult();
		this.TestDate = fDate.getDate( aHMCustExamInfoSchema.getTestDate());
		this.DoctNo = aHMCustExamInfoSchema.getDoctNo();
		this.TestFeeAmount = aHMCustExamInfoSchema.getTestFeeAmount();
		this.IsNormal = aHMCustExamInfoSchema.getIsNormal();
		this.Operator = aHMCustExamInfoSchema.getOperator();
		this.ManageCom = aHMCustExamInfoSchema.getManageCom();
		this.MakeDate = fDate.getDate( aHMCustExamInfoSchema.getMakeDate());
		this.MakeTime = aHMCustExamInfoSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aHMCustExamInfoSchema.getModifyDate());
		this.ModifyTime = aHMCustExamInfoSchema.getModifyTime();
		this.StandardMeasureUnit = aHMCustExamInfoSchema.getStandardMeasureUnit();
		this.NormalValue = aHMCustExamInfoSchema.getNormalValue();
		this.StandbyField1 = aHMCustExamInfoSchema.getStandbyField1();
		this.StandbyField2 = aHMCustExamInfoSchema.getStandbyField2();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("TestNo") == null )
				this.TestNo = null;
			else
				this.TestNo = rs.getString("TestNo").trim();

			if( rs.getString("MedicaItemCode") == null )
				this.MedicaItemCode = null;
			else
				this.MedicaItemCode = rs.getString("MedicaItemCode").trim();

			if( rs.getString("ResultUnitNum") == null )
				this.ResultUnitNum = null;
			else
				this.ResultUnitNum = rs.getString("ResultUnitNum").trim();

			if( rs.getString("TestResult") == null )
				this.TestResult = null;
			else
				this.TestResult = rs.getString("TestResult").trim();

			this.TestDate = rs.getDate("TestDate");
			if( rs.getString("DoctNo") == null )
				this.DoctNo = null;
			else
				this.DoctNo = rs.getString("DoctNo").trim();

			this.TestFeeAmount = rs.getDouble("TestFeeAmount");
			if( rs.getString("IsNormal") == null )
				this.IsNormal = null;
			else
				this.IsNormal = rs.getString("IsNormal").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("StandardMeasureUnit") == null )
				this.StandardMeasureUnit = null;
			else
				this.StandardMeasureUnit = rs.getString("StandardMeasureUnit").trim();

			if( rs.getString("NormalValue") == null )
				this.NormalValue = null;
			else
				this.NormalValue = rs.getString("NormalValue").trim();

			if( rs.getString("StandbyField1") == null )
				this.StandbyField1 = null;
			else
				this.StandbyField1 = rs.getString("StandbyField1").trim();

			if( rs.getString("StandbyField2") == null )
				this.StandbyField2 = null;
			else
				this.StandbyField2 = rs.getString("StandbyField2").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的HMCustExamInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMCustExamInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public HMCustExamInfoSchema getSchema()
	{
		HMCustExamInfoSchema aHMCustExamInfoSchema = new HMCustExamInfoSchema();
		aHMCustExamInfoSchema.setSchema(this);
		return aHMCustExamInfoSchema;
	}

	public HMCustExamInfoDB getDB()
	{
		HMCustExamInfoDB aDBOper = new HMCustExamInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHMCustExamInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TestNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MedicaItemCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResultUnitNum)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TestResult)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TestDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DoctNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TestFeeAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IsNormal)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandardMeasureUnit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NormalValue)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyField1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyField2));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpHMCustExamInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TestNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			MedicaItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ResultUnitNum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			TestResult = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			TestDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			DoctNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			TestFeeAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			IsNormal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			StandardMeasureUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			NormalValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			StandbyField1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			StandbyField2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "HMCustExamInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("TestNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TestNo));
		}
		if (FCode.equals("MedicaItemCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaItemCode));
		}
		if (FCode.equals("ResultUnitNum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResultUnitNum));
		}
		if (FCode.equals("TestResult"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TestResult));
		}
		if (FCode.equals("TestDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTestDate()));
		}
		if (FCode.equals("DoctNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctNo));
		}
		if (FCode.equals("TestFeeAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TestFeeAmount));
		}
		if (FCode.equals("IsNormal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IsNormal));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("StandardMeasureUnit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandardMeasureUnit));
		}
		if (FCode.equals("NormalValue"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NormalValue));
		}
		if (FCode.equals("StandbyField1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyField1));
		}
		if (FCode.equals("StandbyField2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyField2));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TestNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(MedicaItemCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ResultUnitNum);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(TestResult);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTestDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DoctNo);
				break;
			case 7:
				strFieldValue = String.valueOf(TestFeeAmount);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(IsNormal);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(StandardMeasureUnit);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(NormalValue);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(StandbyField1);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(StandbyField2);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("TestNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TestNo = FValue.trim();
			}
			else
				TestNo = null;
		}
		if (FCode.equalsIgnoreCase("MedicaItemCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MedicaItemCode = FValue.trim();
			}
			else
				MedicaItemCode = null;
		}
		if (FCode.equalsIgnoreCase("ResultUnitNum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResultUnitNum = FValue.trim();
			}
			else
				ResultUnitNum = null;
		}
		if (FCode.equalsIgnoreCase("TestResult"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TestResult = FValue.trim();
			}
			else
				TestResult = null;
		}
		if (FCode.equalsIgnoreCase("TestDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TestDate = fDate.getDate( FValue );
			}
			else
				TestDate = null;
		}
		if (FCode.equalsIgnoreCase("DoctNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctNo = FValue.trim();
			}
			else
				DoctNo = null;
		}
		if (FCode.equalsIgnoreCase("TestFeeAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TestFeeAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("IsNormal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IsNormal = FValue.trim();
			}
			else
				IsNormal = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("StandardMeasureUnit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandardMeasureUnit = FValue.trim();
			}
			else
				StandardMeasureUnit = null;
		}
		if (FCode.equalsIgnoreCase("NormalValue"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NormalValue = FValue.trim();
			}
			else
				NormalValue = null;
		}
		if (FCode.equalsIgnoreCase("StandbyField1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyField1 = FValue.trim();
			}
			else
				StandbyField1 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyField2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyField2 = FValue.trim();
			}
			else
				StandbyField2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		HMCustExamInfoSchema other = (HMCustExamInfoSchema)otherObject;
		return
			(CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (TestNo == null ? other.getTestNo() == null : TestNo.equals(other.getTestNo()))
			&& (MedicaItemCode == null ? other.getMedicaItemCode() == null : MedicaItemCode.equals(other.getMedicaItemCode()))
			&& (ResultUnitNum == null ? other.getResultUnitNum() == null : ResultUnitNum.equals(other.getResultUnitNum()))
			&& (TestResult == null ? other.getTestResult() == null : TestResult.equals(other.getTestResult()))
			&& (TestDate == null ? other.getTestDate() == null : fDate.getString(TestDate).equals(other.getTestDate()))
			&& (DoctNo == null ? other.getDoctNo() == null : DoctNo.equals(other.getDoctNo()))
			&& TestFeeAmount == other.getTestFeeAmount()
			&& (IsNormal == null ? other.getIsNormal() == null : IsNormal.equals(other.getIsNormal()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (StandardMeasureUnit == null ? other.getStandardMeasureUnit() == null : StandardMeasureUnit.equals(other.getStandardMeasureUnit()))
			&& (NormalValue == null ? other.getNormalValue() == null : NormalValue.equals(other.getNormalValue()))
			&& (StandbyField1 == null ? other.getStandbyField1() == null : StandbyField1.equals(other.getStandbyField1()))
			&& (StandbyField2 == null ? other.getStandbyField2() == null : StandbyField2.equals(other.getStandbyField2()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CustomerNo") ) {
			return 0;
		}
		if( strFieldName.equals("TestNo") ) {
			return 1;
		}
		if( strFieldName.equals("MedicaItemCode") ) {
			return 2;
		}
		if( strFieldName.equals("ResultUnitNum") ) {
			return 3;
		}
		if( strFieldName.equals("TestResult") ) {
			return 4;
		}
		if( strFieldName.equals("TestDate") ) {
			return 5;
		}
		if( strFieldName.equals("DoctNo") ) {
			return 6;
		}
		if( strFieldName.equals("TestFeeAmount") ) {
			return 7;
		}
		if( strFieldName.equals("IsNormal") ) {
			return 8;
		}
		if( strFieldName.equals("Operator") ) {
			return 9;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 10;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 14;
		}
		if( strFieldName.equals("StandardMeasureUnit") ) {
			return 15;
		}
		if( strFieldName.equals("NormalValue") ) {
			return 16;
		}
		if( strFieldName.equals("StandbyField1") ) {
			return 17;
		}
		if( strFieldName.equals("StandbyField2") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CustomerNo";
				break;
			case 1:
				strFieldName = "TestNo";
				break;
			case 2:
				strFieldName = "MedicaItemCode";
				break;
			case 3:
				strFieldName = "ResultUnitNum";
				break;
			case 4:
				strFieldName = "TestResult";
				break;
			case 5:
				strFieldName = "TestDate";
				break;
			case 6:
				strFieldName = "DoctNo";
				break;
			case 7:
				strFieldName = "TestFeeAmount";
				break;
			case 8:
				strFieldName = "IsNormal";
				break;
			case 9:
				strFieldName = "Operator";
				break;
			case 10:
				strFieldName = "ManageCom";
				break;
			case 11:
				strFieldName = "MakeDate";
				break;
			case 12:
				strFieldName = "MakeTime";
				break;
			case 13:
				strFieldName = "ModifyDate";
				break;
			case 14:
				strFieldName = "ModifyTime";
				break;
			case 15:
				strFieldName = "StandardMeasureUnit";
				break;
			case 16:
				strFieldName = "NormalValue";
				break;
			case 17:
				strFieldName = "StandbyField1";
				break;
			case 18:
				strFieldName = "StandbyField2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TestNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MedicaItemCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResultUnitNum") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TestResult") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TestDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DoctNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TestFeeAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("IsNormal") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandardMeasureUnit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NormalValue") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyField1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyField2") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
