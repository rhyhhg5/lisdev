/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLClaimDeclineDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LLClaimDeclineSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LLClaimDeclineSchema implements Schema
{
    // @Field
    /** 拒赔号 */
    private String DeclineNo;
    /** 赔案号 */
    private String ClmNo;
    /** 保单号 */
    private String PolNo;
    /** 立案号 */
    private String RgtNo;
    /** 分案号 */
    private String CaseNo;
    /** 拒赔类型 */
    private String DeclineType;
    /** 关联案件号 */
    private String RelationNo;
    /** 拒赔原因 */
    private String Reason;
    /** 拒赔日期 */
    private Date DeclineDate;
    /** 存档编号 */
    private String ArchiveNo;
    /** 经办人 */
    private String Handler;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLClaimDeclineSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "DeclineNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getDeclineNo()
    {
        if (DeclineNo != null && !DeclineNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DeclineNo = StrTool.unicodeToGBK(DeclineNo);
        }
        return DeclineNo;
    }

    public void setDeclineNo(String aDeclineNo)
    {
        DeclineNo = aDeclineNo;
    }

    public String getClmNo()
    {
        if (ClmNo != null && !ClmNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmNo = StrTool.unicodeToGBK(ClmNo);
        }
        return ClmNo;
    }

    public void setClmNo(String aClmNo)
    {
        ClmNo = aClmNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getRgtNo()
    {
        if (RgtNo != null && !RgtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            RgtNo = StrTool.unicodeToGBK(RgtNo);
        }
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo)
    {
        RgtNo = aRgtNo;
    }

    public String getCaseNo()
    {
        if (CaseNo != null && !CaseNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            CaseNo = StrTool.unicodeToGBK(CaseNo);
        }
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo)
    {
        CaseNo = aCaseNo;
    }

    public String getDeclineType()
    {
        if (DeclineType != null && !DeclineType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DeclineType = StrTool.unicodeToGBK(DeclineType);
        }
        return DeclineType;
    }

    public void setDeclineType(String aDeclineType)
    {
        DeclineType = aDeclineType;
    }

    public String getRelationNo()
    {
        if (RelationNo != null && !RelationNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RelationNo = StrTool.unicodeToGBK(RelationNo);
        }
        return RelationNo;
    }

    public void setRelationNo(String aRelationNo)
    {
        RelationNo = aRelationNo;
    }

    public String getReason()
    {
        if (Reason != null && !Reason.equals("") && SysConst.CHANGECHARSET == true)
        {
            Reason = StrTool.unicodeToGBK(Reason);
        }
        return Reason;
    }

    public void setReason(String aReason)
    {
        Reason = aReason;
    }

    public String getDeclineDate()
    {
        if (DeclineDate != null)
        {
            return fDate.getString(DeclineDate);
        }
        else
        {
            return null;
        }
    }

    public void setDeclineDate(Date aDeclineDate)
    {
        DeclineDate = aDeclineDate;
    }

    public void setDeclineDate(String aDeclineDate)
    {
        if (aDeclineDate != null && !aDeclineDate.equals(""))
        {
            DeclineDate = fDate.getDate(aDeclineDate);
        }
        else
        {
            DeclineDate = null;
        }
    }

    public String getArchiveNo()
    {
        if (ArchiveNo != null && !ArchiveNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ArchiveNo = StrTool.unicodeToGBK(ArchiveNo);
        }
        return ArchiveNo;
    }

    public void setArchiveNo(String aArchiveNo)
    {
        ArchiveNo = aArchiveNo;
    }

    public String getHandler()
    {
        if (Handler != null && !Handler.equals("") && SysConst.CHANGECHARSET == true)
        {
            Handler = StrTool.unicodeToGBK(Handler);
        }
        return Handler;
    }

    public void setHandler(String aHandler)
    {
        Handler = aHandler;
    }

    public String getMngCom()
    {
        if (MngCom != null && !MngCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLClaimDeclineSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLClaimDeclineSchema aLLClaimDeclineSchema)
    {
        this.DeclineNo = aLLClaimDeclineSchema.getDeclineNo();
        this.ClmNo = aLLClaimDeclineSchema.getClmNo();
        this.PolNo = aLLClaimDeclineSchema.getPolNo();
        this.RgtNo = aLLClaimDeclineSchema.getRgtNo();
        this.CaseNo = aLLClaimDeclineSchema.getCaseNo();
        this.DeclineType = aLLClaimDeclineSchema.getDeclineType();
        this.RelationNo = aLLClaimDeclineSchema.getRelationNo();
        this.Reason = aLLClaimDeclineSchema.getReason();
        this.DeclineDate = fDate.getDate(aLLClaimDeclineSchema.getDeclineDate());
        this.ArchiveNo = aLLClaimDeclineSchema.getArchiveNo();
        this.Handler = aLLClaimDeclineSchema.getHandler();
        this.MngCom = aLLClaimDeclineSchema.getMngCom();
        this.Operator = aLLClaimDeclineSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLClaimDeclineSchema.getMakeDate());
        this.MakeTime = aLLClaimDeclineSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLClaimDeclineSchema.getModifyDate());
        this.ModifyTime = aLLClaimDeclineSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("DeclineNo") == null)
            {
                this.DeclineNo = null;
            }
            else
            {
                this.DeclineNo = rs.getString("DeclineNo").trim();
            }

            if (rs.getString("ClmNo") == null)
            {
                this.ClmNo = null;
            }
            else
            {
                this.ClmNo = rs.getString("ClmNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("RgtNo") == null)
            {
                this.RgtNo = null;
            }
            else
            {
                this.RgtNo = rs.getString("RgtNo").trim();
            }

            if (rs.getString("CaseNo") == null)
            {
                this.CaseNo = null;
            }
            else
            {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("DeclineType") == null)
            {
                this.DeclineType = null;
            }
            else
            {
                this.DeclineType = rs.getString("DeclineType").trim();
            }

            if (rs.getString("RelationNo") == null)
            {
                this.RelationNo = null;
            }
            else
            {
                this.RelationNo = rs.getString("RelationNo").trim();
            }

            if (rs.getString("Reason") == null)
            {
                this.Reason = null;
            }
            else
            {
                this.Reason = rs.getString("Reason").trim();
            }

            this.DeclineDate = rs.getDate("DeclineDate");
            if (rs.getString("ArchiveNo") == null)
            {
                this.ArchiveNo = null;
            }
            else
            {
                this.ArchiveNo = rs.getString("ArchiveNo").trim();
            }

            if (rs.getString("Handler") == null)
            {
                this.Handler = null;
            }
            else
            {
                this.Handler = rs.getString("Handler").trim();
            }

            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimDeclineSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLClaimDeclineSchema getSchema()
    {
        LLClaimDeclineSchema aLLClaimDeclineSchema = new LLClaimDeclineSchema();
        aLLClaimDeclineSchema.setSchema(this);
        return aLLClaimDeclineSchema;
    }

    public LLClaimDeclineDB getDB()
    {
        LLClaimDeclineDB aDBOper = new LLClaimDeclineDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimDecline描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(DeclineNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RgtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CaseNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DeclineType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RelationNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Reason)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            DeclineDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ArchiveNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Handler)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MngCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimDecline>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            DeclineNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
            DeclineType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            RelationNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            DeclineDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ArchiveNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimDeclineSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("DeclineNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DeclineNo));
        }
        if (FCode.equals("ClmNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("RgtNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RgtNo));
        }
        if (FCode.equals("CaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CaseNo));
        }
        if (FCode.equals("DeclineType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DeclineType));
        }
        if (FCode.equals("RelationNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RelationNo));
        }
        if (FCode.equals("Reason"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Reason));
        }
        if (FCode.equals("DeclineDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getDeclineDate()));
        }
        if (FCode.equals("ArchiveNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ArchiveNo));
        }
        if (FCode.equals("Handler"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Handler));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MngCom));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(DeclineNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ClmNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(RgtNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DeclineType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(RelationNo);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Reason);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getDeclineDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ArchiveNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Handler);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("DeclineNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeclineNo = FValue.trim();
            }
            else
            {
                DeclineNo = null;
            }
        }
        if (FCode.equals("ClmNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmNo = FValue.trim();
            }
            else
            {
                ClmNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("RgtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtNo = FValue.trim();
            }
            else
            {
                RgtNo = null;
            }
        }
        if (FCode.equals("CaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
            {
                CaseNo = null;
            }
        }
        if (FCode.equals("DeclineType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeclineType = FValue.trim();
            }
            else
            {
                DeclineType = null;
            }
        }
        if (FCode.equals("RelationNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RelationNo = FValue.trim();
            }
            else
            {
                RelationNo = null;
            }
        }
        if (FCode.equals("Reason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Reason = FValue.trim();
            }
            else
            {
                Reason = null;
            }
        }
        if (FCode.equals("DeclineDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeclineDate = fDate.getDate(FValue);
            }
            else
            {
                DeclineDate = null;
            }
        }
        if (FCode.equals("ArchiveNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ArchiveNo = FValue.trim();
            }
            else
            {
                ArchiveNo = null;
            }
        }
        if (FCode.equals("Handler"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Handler = FValue.trim();
            }
            else
            {
                Handler = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLClaimDeclineSchema other = (LLClaimDeclineSchema) otherObject;
        return
                DeclineNo.equals(other.getDeclineNo())
                && ClmNo.equals(other.getClmNo())
                && PolNo.equals(other.getPolNo())
                && RgtNo.equals(other.getRgtNo())
                && CaseNo.equals(other.getCaseNo())
                && DeclineType.equals(other.getDeclineType())
                && RelationNo.equals(other.getRelationNo())
                && Reason.equals(other.getReason())
                && fDate.getString(DeclineDate).equals(other.getDeclineDate())
                && ArchiveNo.equals(other.getArchiveNo())
                && Handler.equals(other.getHandler())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("DeclineNo"))
        {
            return 0;
        }
        if (strFieldName.equals("ClmNo"))
        {
            return 1;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 2;
        }
        if (strFieldName.equals("RgtNo"))
        {
            return 3;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return 4;
        }
        if (strFieldName.equals("DeclineType"))
        {
            return 5;
        }
        if (strFieldName.equals("RelationNo"))
        {
            return 6;
        }
        if (strFieldName.equals("Reason"))
        {
            return 7;
        }
        if (strFieldName.equals("DeclineDate"))
        {
            return 8;
        }
        if (strFieldName.equals("ArchiveNo"))
        {
            return 9;
        }
        if (strFieldName.equals("Handler"))
        {
            return 10;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 11;
        }
        if (strFieldName.equals("Operator"))
        {
            return 12;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 13;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 14;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 15;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "DeclineNo";
                break;
            case 1:
                strFieldName = "ClmNo";
                break;
            case 2:
                strFieldName = "PolNo";
                break;
            case 3:
                strFieldName = "RgtNo";
                break;
            case 4:
                strFieldName = "CaseNo";
                break;
            case 5:
                strFieldName = "DeclineType";
                break;
            case 6:
                strFieldName = "RelationNo";
                break;
            case 7:
                strFieldName = "Reason";
                break;
            case 8:
                strFieldName = "DeclineDate";
                break;
            case 9:
                strFieldName = "ArchiveNo";
                break;
            case 10:
                strFieldName = "Handler";
                break;
            case 11:
                strFieldName = "MngCom";
                break;
            case 12:
                strFieldName = "Operator";
                break;
            case 13:
                strFieldName = "MakeDate";
                break;
            case 14:
                strFieldName = "MakeTime";
                break;
            case 15:
                strFieldName = "ModifyDate";
                break;
            case 16:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("DeclineNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeclineType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RelationNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Reason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeclineDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ArchiveNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Handler"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
