/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDGrpTestDB;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDGrpTestSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 体检预约套餐表
 * @CreateDate：2005-07-27
 */
public class LDGrpTestSchema implements Schema, Cloneable
{
    // @Field
    /** 体检套餐名称 */
    private String TestGrpName;
    /** 体检套餐代码 */
    private String TestGrpCode;
    /** 检查项目代码 */
    private String TestItemCode;
    /** 检查项目俗称 */
    private String TestItemCommonName;
    /** 是否空腹 */
    private String IfEmpty;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDGrpTestSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "TestGrpCode";
        pk[1] = "TestItemCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LDGrpTestSchema cloned = (LDGrpTestSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getTestGrpName()
    {
        return TestGrpName;
    }

    public void setTestGrpName(String aTestGrpName)
    {
        TestGrpName = aTestGrpName;
    }

    public String getTestGrpCode()
    {
        return TestGrpCode;
    }

    public void setTestGrpCode(String aTestGrpCode)
    {
        TestGrpCode = aTestGrpCode;
    }

    public String getTestItemCode()
    {
        return TestItemCode;
    }

    public void setTestItemCode(String aTestItemCode)
    {
        TestItemCode = aTestItemCode;
    }

    public String getTestItemCommonName()
    {
        return TestItemCommonName;
    }

    public void setTestItemCommonName(String aTestItemCommonName)
    {
        TestItemCommonName = aTestItemCommonName;
    }

    public String getIfEmpty()
    {
        return IfEmpty;
    }

    public void setIfEmpty(String aIfEmpty)
    {
        IfEmpty = aIfEmpty;
    }

    /**
     * 使用另外一个 LDGrpTestSchema 对象给 Schema 赋值
     * @param: aLDGrpTestSchema LDGrpTestSchema
     **/
    public void setSchema(LDGrpTestSchema aLDGrpTestSchema)
    {
        this.TestGrpName = aLDGrpTestSchema.getTestGrpName();
        this.TestGrpCode = aLDGrpTestSchema.getTestGrpCode();
        this.TestItemCode = aLDGrpTestSchema.getTestItemCode();
        this.TestItemCommonName = aLDGrpTestSchema.getTestItemCommonName();
        this.IfEmpty = aLDGrpTestSchema.getIfEmpty();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("TestGrpName") == null)
            {
                this.TestGrpName = null;
            }
            else
            {
                this.TestGrpName = rs.getString("TestGrpName").trim();
            }

            if (rs.getString("TestGrpCode") == null)
            {
                this.TestGrpCode = null;
            }
            else
            {
                this.TestGrpCode = rs.getString("TestGrpCode").trim();
            }

            if (rs.getString("TestItemCode") == null)
            {
                this.TestItemCode = null;
            }
            else
            {
                this.TestItemCode = rs.getString("TestItemCode").trim();
            }

            if (rs.getString("TestItemCommonName") == null)
            {
                this.TestItemCommonName = null;
            }
            else
            {
                this.TestItemCommonName = rs.getString("TestItemCommonName").
                                          trim();
            }

            if (rs.getString("IfEmpty") == null)
            {
                this.IfEmpty = null;
            }
            else
            {
                this.IfEmpty = rs.getString("IfEmpty").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LDGrpTest表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpTestSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDGrpTestSchema getSchema()
    {
        LDGrpTestSchema aLDGrpTestSchema = new LDGrpTestSchema();
        aLDGrpTestSchema.setSchema(this);
        return aLDGrpTestSchema;
    }

    public LDGrpTestDB getDB()
    {
        LDGrpTestDB aDBOper = new LDGrpTestDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDGrpTest描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(TestGrpName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestGrpCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TestItemCommonName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IfEmpty));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDGrpTest>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            TestGrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            TestGrpCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            TestItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            TestItemCommonName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                4, SysConst.PACKAGESPILTER);
            IfEmpty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDGrpTestSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("TestGrpName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestGrpName));
        }
        if (FCode.equals("TestGrpCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestGrpCode));
        }
        if (FCode.equals("TestItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestItemCode));
        }
        if (FCode.equals("TestItemCommonName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TestItemCommonName));
        }
        if (FCode.equals("IfEmpty"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IfEmpty));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(TestGrpName);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TestGrpCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(TestItemCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(TestItemCommonName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(IfEmpty);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equalsIgnoreCase("TestGrpName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TestGrpName = FValue.trim();
            }
            else
            {
                TestGrpName = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestGrpCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TestGrpCode = FValue.trim();
            }
            else
            {
                TestGrpCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TestItemCode = FValue.trim();
            }
            else
            {
                TestItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("TestItemCommonName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TestItemCommonName = FValue.trim();
            }
            else
            {
                TestItemCommonName = null;
            }
        }
        if (FCode.equalsIgnoreCase("IfEmpty"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IfEmpty = FValue.trim();
            }
            else
            {
                IfEmpty = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDGrpTestSchema other = (LDGrpTestSchema) otherObject;
        return
                TestGrpName.equals(other.getTestGrpName())
                && TestGrpCode.equals(other.getTestGrpCode())
                && TestItemCode.equals(other.getTestItemCode())
                && TestItemCommonName.equals(other.getTestItemCommonName())
                && IfEmpty.equals(other.getIfEmpty());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("TestGrpName"))
        {
            return 0;
        }
        if (strFieldName.equals("TestGrpCode"))
        {
            return 1;
        }
        if (strFieldName.equals("TestItemCode"))
        {
            return 2;
        }
        if (strFieldName.equals("TestItemCommonName"))
        {
            return 3;
        }
        if (strFieldName.equals("IfEmpty"))
        {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "TestGrpName";
                break;
            case 1:
                strFieldName = "TestGrpCode";
                break;
            case 2:
                strFieldName = "TestItemCode";
                break;
            case 3:
                strFieldName = "TestItemCommonName";
                break;
            case 4:
                strFieldName = "IfEmpty";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("TestGrpName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestGrpCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TestItemCommonName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IfEmpty"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
