/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIInfoFinItemAssociatedDB;

/*
 * <p>ClassName: LIInfoFinItemAssociatedSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2008-05-20
 */
public class LIInfoFinItemAssociatedSchema implements Schema, Cloneable
{
	// @Field
	/** 科目类型编码 */
	private String FinItemID;
	/** 数据源名称 */
	private String FountainID;
	/** 数据目标名称 */
	private String TargetID;
	/** 转换标志 */
	private String TransFlag;
	/** 转化类型编码 */
	private String TransCode;
	/** 科目类型描述 */
	private String ReMark;
	/** 转化类型处理类 */
	private String TransClass;

	public static final int FIELDNUM = 7;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIInfoFinItemAssociatedSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "FinItemID";
		pk[1] = "FountainID";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LIInfoFinItemAssociatedSchema cloned = (LIInfoFinItemAssociatedSchema)super.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getFinItemID()
	{
		return FinItemID;
	}
	public void setFinItemID(String aFinItemID)
	{
            FinItemID = aFinItemID;
	}
	public String getFountainID()
	{
		return FountainID;
	}
	public void setFountainID(String aFountainID)
	{
            FountainID = aFountainID;
	}
	public String getTargetID()
	{
		return TargetID;
	}
	public void setTargetID(String aTargetID)
	{
            TargetID = aTargetID;
	}
	public String getTransFlag()
	{
		return TransFlag;
	}
	public void setTransFlag(String aTransFlag)
	{
            TransFlag = aTransFlag;
	}
	public String getTransCode()
	{
		return TransCode;
	}
	public void setTransCode(String aTransCode)
	{
            TransCode = aTransCode;
	}
	public String getReMark()
	{
		return ReMark;
	}
	public void setReMark(String aReMark)
	{
            ReMark = aReMark;
	}
	public String getTransClass()
	{
		return TransClass;
	}
	public void setTransClass(String aTransClass)
	{
            TransClass = aTransClass;
	}

	/**
	* 使用另外一个 LIInfoFinItemAssociatedSchema 对象给 Schema 赋值
	* @param: aLIInfoFinItemAssociatedSchema LIInfoFinItemAssociatedSchema
	**/
	public void setSchema(LIInfoFinItemAssociatedSchema aLIInfoFinItemAssociatedSchema)
	{
		this.FinItemID = aLIInfoFinItemAssociatedSchema.getFinItemID();
		this.FountainID = aLIInfoFinItemAssociatedSchema.getFountainID();
		this.TargetID = aLIInfoFinItemAssociatedSchema.getTargetID();
		this.TransFlag = aLIInfoFinItemAssociatedSchema.getTransFlag();
		this.TransCode = aLIInfoFinItemAssociatedSchema.getTransCode();
		this.ReMark = aLIInfoFinItemAssociatedSchema.getReMark();
		this.TransClass = aLIInfoFinItemAssociatedSchema.getTransClass();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("FinItemID") == null )
				this.FinItemID = null;
			else
				this.FinItemID = rs.getString("FinItemID").trim();

			if( rs.getString("FountainID") == null )
				this.FountainID = null;
			else
				this.FountainID = rs.getString("FountainID").trim();

			if( rs.getString("TargetID") == null )
				this.TargetID = null;
			else
				this.TargetID = rs.getString("TargetID").trim();

			if( rs.getString("TransFlag") == null )
				this.TransFlag = null;
			else
				this.TransFlag = rs.getString("TransFlag").trim();

			if( rs.getString("TransCode") == null )
				this.TransCode = null;
			else
				this.TransCode = rs.getString("TransCode").trim();

			if( rs.getString("ReMark") == null )
				this.ReMark = null;
			else
				this.ReMark = rs.getString("ReMark").trim();

			if( rs.getString("TransClass") == null )
				this.TransClass = null;
			else
				this.TransClass = rs.getString("TransClass").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIInfoFinItemAssociated表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIInfoFinItemAssociatedSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIInfoFinItemAssociatedSchema getSchema()
	{
		LIInfoFinItemAssociatedSchema aLIInfoFinItemAssociatedSchema = new LIInfoFinItemAssociatedSchema();
		aLIInfoFinItemAssociatedSchema.setSchema(this);
		return aLIInfoFinItemAssociatedSchema;
	}

	public LIInfoFinItemAssociatedDB getDB()
	{
		LIInfoFinItemAssociatedDB aDBOper = new LIInfoFinItemAssociatedDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIInfoFinItemAssociated描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(FinItemID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FountainID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TargetID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TransFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TransCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ReMark)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TransClass));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIInfoFinItemAssociated>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			FinItemID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			FountainID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			TargetID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TransFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			TransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ReMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			TransClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIInfoFinItemAssociatedSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("FinItemID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinItemID));
		}
		if (FCode.equals("FountainID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FountainID));
		}
		if (FCode.equals("TargetID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TargetID));
		}
		if (FCode.equals("TransFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransFlag));
		}
		if (FCode.equals("TransCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransCode));
		}
		if (FCode.equals("ReMark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReMark));
		}
		if (FCode.equals("TransClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransClass));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(FinItemID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(FountainID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(TargetID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TransFlag);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(TransCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ReMark);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(TransClass);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("FinItemID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinItemID = FValue.trim();
			}
			else
				FinItemID = null;
		}
		if (FCode.equalsIgnoreCase("FountainID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FountainID = FValue.trim();
			}
			else
				FountainID = null;
		}
		if (FCode.equalsIgnoreCase("TargetID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TargetID = FValue.trim();
			}
			else
				TargetID = null;
		}
		if (FCode.equalsIgnoreCase("TransFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransFlag = FValue.trim();
			}
			else
				TransFlag = null;
		}
		if (FCode.equalsIgnoreCase("TransCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransCode = FValue.trim();
			}
			else
				TransCode = null;
		}
		if (FCode.equalsIgnoreCase("ReMark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReMark = FValue.trim();
			}
			else
				ReMark = null;
		}
		if (FCode.equalsIgnoreCase("TransClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransClass = FValue.trim();
			}
			else
				TransClass = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIInfoFinItemAssociatedSchema other = (LIInfoFinItemAssociatedSchema)otherObject;
		return
			FinItemID.equals(other.getFinItemID())
			&& FountainID.equals(other.getFountainID())
			&& TargetID.equals(other.getTargetID())
			&& TransFlag.equals(other.getTransFlag())
			&& TransCode.equals(other.getTransCode())
			&& ReMark.equals(other.getReMark())
			&& TransClass.equals(other.getTransClass());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("FinItemID") ) {
			return 0;
		}
		if( strFieldName.equals("FountainID") ) {
			return 1;
		}
		if( strFieldName.equals("TargetID") ) {
			return 2;
		}
		if( strFieldName.equals("TransFlag") ) {
			return 3;
		}
		if( strFieldName.equals("TransCode") ) {
			return 4;
		}
		if( strFieldName.equals("ReMark") ) {
			return 5;
		}
		if( strFieldName.equals("TransClass") ) {
			return 6;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "FinItemID";
				break;
			case 1:
				strFieldName = "FountainID";
				break;
			case 2:
				strFieldName = "TargetID";
				break;
			case 3:
				strFieldName = "TransFlag";
				break;
			case 4:
				strFieldName = "TransCode";
				break;
			case 5:
				strFieldName = "ReMark";
				break;
			case 6:
				strFieldName = "TransClass";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("FinItemID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FountainID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TargetID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReMark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransClass") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
