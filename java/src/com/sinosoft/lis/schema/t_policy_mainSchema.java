/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.t_policy_mainDB;

/*
 * <p>ClassName: t_policy_mainSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-07-28
 */
public class t_policy_mainSchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String pol_no;
	/** 原保单号 */
	private String pol_no_orig;
	/** 子公司代码 */
	private String comp_cod;
	/** 子公司名称 */
	private String comp_nam;
	/** 签单日期 */
	private String date_sign;
	/** 生效日期 */
	private String date_begin;
	/** 终保日期 */
	private String date_end;
	/** 首次缴费日期 */
	private String date_fee;
	/** 缴费年期 */
	private int pay_yrs;
	/** 缴费方式代码 */
	private String pay_mod_cod;
	/** 缴费方式名称 */
	private String pay_mod_nam;
	/** 团单/个单标志 */
	private String grp_psn_flg;
	/** 渠道类型代码 */
	private String chl_typ_cod;
	/** 渠道类型名称 */
	private String chl_typ_nam;
	/** 渠道代码 */
	private String chl_cod;
	/** 渠道名称 */
	private String chl_nam;
	/** 中介机构id */
	private String brok_cod;
	/** 中介机构名称 */
	private String brok_nam;
	/** 交叉销售类型id */
	private String crs_typ_cod;
	/** 交叉销售类型名称 */
	private String crs_typ_nam;
	/** 业务员id */
	private String sales_cod;
	/** 业务员姓名 */
	private String sales_nam;
	/** 销售专员id */
	private String crs_sales_cod;
	/** 销售专员名称 */
	private String crs_sales_nam;
	/** 出单机构 */
	private String org_sales_cod;
	/** 出单机构名称 */
	private String org_sales_nam;
	/** 归属机构 */
	private String org_belg_cod;
	/** 归属机构名称 */
	private String org_belg_nam;
	/** 销售机构代码 */
	private String org_crs_cod;
	/** 销售机构名称 */
	private String org_crs_nam;
	/** 客户号 */
	private String cst_cod;
	/** 客户名称 */
	private String cst_nam;
	/** 客户类型 */
	private String cst_typ;
	/** 客户地址 */
	private String cst_addr;
	/** 邮政编码 */
	private String cst_post;
	/** 证件类型代码 */
	private String cst_idtyp_cod;
	/** 证件类型名称 */
	private String cst_idtyp_nam;
	/** 证件号码 */
	private String cst_idno;
	/** 客户联系人姓名 */
	private String cst_linker_nam;
	/** 客户联系人性别 */
	private String cst_linker_sex;
	/** 客户电话 */
	private String cst_tel;
	/** 客户手机 */
	private String cst_mob;
	/** 客户电子邮件 */
	private String cst_mail;
	/** 被保人简明信息 */
	private String insd_info;
	/** 受益人简明信息 */
	private String beft_info;
	/** 承保标的 */
	private String insr_targ;
	/** 承保标的描述 */
	private String insr_targ_info;
	/** 保费收入 */
	private double prem;
	/** 签单保费 */
	private double prem_sign;
	/** 保单保费 */
	private double prem_pol;
	/** 保额 */
	private double amount;
	/** 折算比例 */
	private double percnt;
	/** 备用字段1 */
	private String bak1;
	/** 备用字段2 */
	private String bak2;
	/** 备用字段3 */
	private String bak3;
	/** 备用字段4 */
	private String bak4;
	/** 备用字段5 */
	private String bak5;
	/** 备用字段6 */
	private String bak6;
	/** 备用字段7 */
	private String bak7;
	/** 备用字段8 */
	private String bak8;
	/** 数据变更类型 */
	private String data_upd_typ;
	/** 子公司报送时间 */
	private String date_send;
	/** 时间戳 */
	private String date_update;
	/** 录单时间 */
	private String input_date;
	/** 投保日期 */
	private String hold_date;
	/** 投保单号 */
	private String proposal_no;

	public static final int FIELDNUM = 66;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public t_policy_mainSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "pol_no";
		pk[1] = "comp_cod";
		pk[2] = "date_sign";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		t_policy_mainSchema cloned = (t_policy_mainSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getpol_no()
	{
		return pol_no;
	}
	public void setpol_no(String apol_no)
	{
		pol_no = apol_no;
	}
	public String getpol_no_orig()
	{
		return pol_no_orig;
	}
	public void setpol_no_orig(String apol_no_orig)
	{
		pol_no_orig = apol_no_orig;
	}
	public String getcomp_cod()
	{
		return comp_cod;
	}
	public void setcomp_cod(String acomp_cod)
	{
		comp_cod = acomp_cod;
	}
	public String getcomp_nam()
	{
		return comp_nam;
	}
	public void setcomp_nam(String acomp_nam)
	{
		comp_nam = acomp_nam;
	}
	public String getdate_sign()
	{
		return date_sign;
	}
	public void setdate_sign(String adate_sign)
	{
		date_sign = adate_sign;
	}
	public String getdate_begin()
	{
		return date_begin;
	}
	public void setdate_begin(String adate_begin)
	{
		date_begin = adate_begin;
	}
	public String getdate_end()
	{
		return date_end;
	}
	public void setdate_end(String adate_end)
	{
		date_end = adate_end;
	}
	public String getdate_fee()
	{
		return date_fee;
	}
	public void setdate_fee(String adate_fee)
	{
		date_fee = adate_fee;
	}
	public int getpay_yrs()
	{
		return pay_yrs;
	}
	public void setpay_yrs(int apay_yrs)
	{
		pay_yrs = apay_yrs;
	}
	public void setpay_yrs(String apay_yrs)
	{
		if (apay_yrs != null && !apay_yrs.equals(""))
		{
			Integer tInteger = new Integer(apay_yrs);
			int i = tInteger.intValue();
			pay_yrs = i;
		}
	}

	public String getpay_mod_cod()
	{
		return pay_mod_cod;
	}
	public void setpay_mod_cod(String apay_mod_cod)
	{
		pay_mod_cod = apay_mod_cod;
	}
	public String getpay_mod_nam()
	{
		return pay_mod_nam;
	}
	public void setpay_mod_nam(String apay_mod_nam)
	{
		pay_mod_nam = apay_mod_nam;
	}
	public String getgrp_psn_flg()
	{
		return grp_psn_flg;
	}
	public void setgrp_psn_flg(String agrp_psn_flg)
	{
		grp_psn_flg = agrp_psn_flg;
	}
	public String getchl_typ_cod()
	{
		return chl_typ_cod;
	}
	public void setchl_typ_cod(String achl_typ_cod)
	{
		chl_typ_cod = achl_typ_cod;
	}
	public String getchl_typ_nam()
	{
		return chl_typ_nam;
	}
	public void setchl_typ_nam(String achl_typ_nam)
	{
		chl_typ_nam = achl_typ_nam;
	}
	public String getchl_cod()
	{
		return chl_cod;
	}
	public void setchl_cod(String achl_cod)
	{
		chl_cod = achl_cod;
	}
	public String getchl_nam()
	{
		return chl_nam;
	}
	public void setchl_nam(String achl_nam)
	{
		chl_nam = achl_nam;
	}
	public String getbrok_cod()
	{
		return brok_cod;
	}
	public void setbrok_cod(String abrok_cod)
	{
		brok_cod = abrok_cod;
	}
	public String getbrok_nam()
	{
		return brok_nam;
	}
	public void setbrok_nam(String abrok_nam)
	{
		brok_nam = abrok_nam;
	}
	public String getcrs_typ_cod()
	{
		return crs_typ_cod;
	}
	public void setcrs_typ_cod(String acrs_typ_cod)
	{
		crs_typ_cod = acrs_typ_cod;
	}
	public String getcrs_typ_nam()
	{
		return crs_typ_nam;
	}
	public void setcrs_typ_nam(String acrs_typ_nam)
	{
		crs_typ_nam = acrs_typ_nam;
	}
	public String getsales_cod()
	{
		return sales_cod;
	}
	public void setsales_cod(String asales_cod)
	{
		sales_cod = asales_cod;
	}
	public String getsales_nam()
	{
		return sales_nam;
	}
	public void setsales_nam(String asales_nam)
	{
		sales_nam = asales_nam;
	}
	public String getcrs_sales_cod()
	{
		return crs_sales_cod;
	}
	public void setcrs_sales_cod(String acrs_sales_cod)
	{
		crs_sales_cod = acrs_sales_cod;
	}
	public String getcrs_sales_nam()
	{
		return crs_sales_nam;
	}
	public void setcrs_sales_nam(String acrs_sales_nam)
	{
		crs_sales_nam = acrs_sales_nam;
	}
	public String getorg_sales_cod()
	{
		return org_sales_cod;
	}
	public void setorg_sales_cod(String aorg_sales_cod)
	{
		org_sales_cod = aorg_sales_cod;
	}
	public String getorg_sales_nam()
	{
		return org_sales_nam;
	}
	public void setorg_sales_nam(String aorg_sales_nam)
	{
		org_sales_nam = aorg_sales_nam;
	}
	public String getorg_belg_cod()
	{
		return org_belg_cod;
	}
	public void setorg_belg_cod(String aorg_belg_cod)
	{
		org_belg_cod = aorg_belg_cod;
	}
	public String getorg_belg_nam()
	{
		return org_belg_nam;
	}
	public void setorg_belg_nam(String aorg_belg_nam)
	{
		org_belg_nam = aorg_belg_nam;
	}
	public String getorg_crs_cod()
	{
		return org_crs_cod;
	}
	public void setorg_crs_cod(String aorg_crs_cod)
	{
		org_crs_cod = aorg_crs_cod;
	}
	public String getorg_crs_nam()
	{
		return org_crs_nam;
	}
	public void setorg_crs_nam(String aorg_crs_nam)
	{
		org_crs_nam = aorg_crs_nam;
	}
	public String getcst_cod()
	{
		return cst_cod;
	}
	public void setcst_cod(String acst_cod)
	{
		cst_cod = acst_cod;
	}
	public String getcst_nam()
	{
		return cst_nam;
	}
	public void setcst_nam(String acst_nam)
	{
		cst_nam = acst_nam;
	}
	public String getcst_typ()
	{
		return cst_typ;
	}
	public void setcst_typ(String acst_typ)
	{
		cst_typ = acst_typ;
	}
	public String getcst_addr()
	{
		return cst_addr;
	}
	public void setcst_addr(String acst_addr)
	{
		cst_addr = acst_addr;
	}
	public String getcst_post()
	{
		return cst_post;
	}
	public void setcst_post(String acst_post)
	{
		cst_post = acst_post;
	}
	public String getcst_idtyp_cod()
	{
		return cst_idtyp_cod;
	}
	public void setcst_idtyp_cod(String acst_idtyp_cod)
	{
		cst_idtyp_cod = acst_idtyp_cod;
	}
	public String getcst_idtyp_nam()
	{
		return cst_idtyp_nam;
	}
	public void setcst_idtyp_nam(String acst_idtyp_nam)
	{
		cst_idtyp_nam = acst_idtyp_nam;
	}
	public String getcst_idno()
	{
		return cst_idno;
	}
	public void setcst_idno(String acst_idno)
	{
		cst_idno = acst_idno;
	}
	public String getcst_linker_nam()
	{
		return cst_linker_nam;
	}
	public void setcst_linker_nam(String acst_linker_nam)
	{
		cst_linker_nam = acst_linker_nam;
	}
	public String getcst_linker_sex()
	{
		return cst_linker_sex;
	}
	public void setcst_linker_sex(String acst_linker_sex)
	{
		cst_linker_sex = acst_linker_sex;
	}
	public String getcst_tel()
	{
		return cst_tel;
	}
	public void setcst_tel(String acst_tel)
	{
		cst_tel = acst_tel;
	}
	public String getcst_mob()
	{
		return cst_mob;
	}
	public void setcst_mob(String acst_mob)
	{
		cst_mob = acst_mob;
	}
	public String getcst_mail()
	{
		return cst_mail;
	}
	public void setcst_mail(String acst_mail)
	{
		cst_mail = acst_mail;
	}
	public String getinsd_info()
	{
		return insd_info;
	}
	public void setinsd_info(String ainsd_info)
	{
		insd_info = ainsd_info;
	}
	public String getbeft_info()
	{
		return beft_info;
	}
	public void setbeft_info(String abeft_info)
	{
		beft_info = abeft_info;
	}
	public String getinsr_targ()
	{
		return insr_targ;
	}
	public void setinsr_targ(String ainsr_targ)
	{
		insr_targ = ainsr_targ;
	}
	public String getinsr_targ_info()
	{
		return insr_targ_info;
	}
	public void setinsr_targ_info(String ainsr_targ_info)
	{
		insr_targ_info = ainsr_targ_info;
	}
	public double getprem()
	{
		return prem;
	}
	public void setprem(double aprem)
	{
		prem = Arith.round(aprem,2);
	}
	public void setprem(String aprem)
	{
		if (aprem != null && !aprem.equals(""))
		{
			Double tDouble = new Double(aprem);
			double d = tDouble.doubleValue();
                prem = Arith.round(d,2);
		}
	}

	public double getprem_sign()
	{
		return prem_sign;
	}
	public void setprem_sign(double aprem_sign)
	{
		prem_sign = Arith.round(aprem_sign,2);
	}
	public void setprem_sign(String aprem_sign)
	{
		if (aprem_sign != null && !aprem_sign.equals(""))
		{
			Double tDouble = new Double(aprem_sign);
			double d = tDouble.doubleValue();
                prem_sign = Arith.round(d,2);
		}
	}

	public double getprem_pol()
	{
		return prem_pol;
	}
	public void setprem_pol(double aprem_pol)
	{
		prem_pol = Arith.round(aprem_pol,2);
	}
	public void setprem_pol(String aprem_pol)
	{
		if (aprem_pol != null && !aprem_pol.equals(""))
		{
			Double tDouble = new Double(aprem_pol);
			double d = tDouble.doubleValue();
                prem_pol = Arith.round(d,2);
		}
	}

	public double getamount()
	{
		return amount;
	}
	public void setamount(double aamount)
	{
		amount = Arith.round(aamount,2);
	}
	public void setamount(String aamount)
	{
		if (aamount != null && !aamount.equals(""))
		{
			Double tDouble = new Double(aamount);
			double d = tDouble.doubleValue();
                amount = Arith.round(d,2);
		}
	}

	public double getpercnt()
	{
		return percnt;
	}
	public void setpercnt(double apercnt)
	{
		percnt = Arith.round(apercnt,2);
	}
	public void setpercnt(String apercnt)
	{
		if (apercnt != null && !apercnt.equals(""))
		{
			Double tDouble = new Double(apercnt);
			double d = tDouble.doubleValue();
                percnt = Arith.round(d,2);
		}
	}

	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}
	public String getbak4()
	{
		return bak4;
	}
	public void setbak4(String abak4)
	{
		bak4 = abak4;
	}
	public String getbak5()
	{
		return bak5;
	}
	public void setbak5(String abak5)
	{
		bak5 = abak5;
	}
	public String getbak6()
	{
		return bak6;
	}
	public void setbak6(String abak6)
	{
		bak6 = abak6;
	}
	public String getbak7()
	{
		return bak7;
	}
	public void setbak7(String abak7)
	{
		bak7 = abak7;
	}
	public String getbak8()
	{
		return bak8;
	}
	public void setbak8(String abak8)
	{
		bak8 = abak8;
	}
	public String getdata_upd_typ()
	{
		return data_upd_typ;
	}
	public void setdata_upd_typ(String adata_upd_typ)
	{
		data_upd_typ = adata_upd_typ;
	}
	public String getdate_send()
	{
		return date_send;
	}
	public void setdate_send(String adate_send)
	{
		date_send = adate_send;
	}
	public String getdate_update()
	{
		return date_update;
	}
	public void setdate_update(String adate_update)
	{
		date_update = adate_update;
	}
	public String getinput_date()
	{
		return input_date;
	}
	public void setinput_date(String ainput_date)
	{
		input_date = ainput_date;
	}
	public String gethold_date()
	{
		return hold_date;
	}
	public void sethold_date(String ahold_date)
	{
		hold_date = ahold_date;
	}
	public String getproposal_no()
	{
		return proposal_no;
	}
	public void setproposal_no(String aproposal_no)
	{
		proposal_no = aproposal_no;
	}

	/**
	* 使用另外一个 t_policy_mainSchema 对象给 Schema 赋值
	* @param: at_policy_mainSchema t_policy_mainSchema
	**/
	public void setSchema(t_policy_mainSchema at_policy_mainSchema)
	{
		this.pol_no = at_policy_mainSchema.getpol_no();
		this.pol_no_orig = at_policy_mainSchema.getpol_no_orig();
		this.comp_cod = at_policy_mainSchema.getcomp_cod();
		this.comp_nam = at_policy_mainSchema.getcomp_nam();
		this.date_sign = at_policy_mainSchema.getdate_sign();
		this.date_begin = at_policy_mainSchema.getdate_begin();
		this.date_end = at_policy_mainSchema.getdate_end();
		this.date_fee = at_policy_mainSchema.getdate_fee();
		this.pay_yrs = at_policy_mainSchema.getpay_yrs();
		this.pay_mod_cod = at_policy_mainSchema.getpay_mod_cod();
		this.pay_mod_nam = at_policy_mainSchema.getpay_mod_nam();
		this.grp_psn_flg = at_policy_mainSchema.getgrp_psn_flg();
		this.chl_typ_cod = at_policy_mainSchema.getchl_typ_cod();
		this.chl_typ_nam = at_policy_mainSchema.getchl_typ_nam();
		this.chl_cod = at_policy_mainSchema.getchl_cod();
		this.chl_nam = at_policy_mainSchema.getchl_nam();
		this.brok_cod = at_policy_mainSchema.getbrok_cod();
		this.brok_nam = at_policy_mainSchema.getbrok_nam();
		this.crs_typ_cod = at_policy_mainSchema.getcrs_typ_cod();
		this.crs_typ_nam = at_policy_mainSchema.getcrs_typ_nam();
		this.sales_cod = at_policy_mainSchema.getsales_cod();
		this.sales_nam = at_policy_mainSchema.getsales_nam();
		this.crs_sales_cod = at_policy_mainSchema.getcrs_sales_cod();
		this.crs_sales_nam = at_policy_mainSchema.getcrs_sales_nam();
		this.org_sales_cod = at_policy_mainSchema.getorg_sales_cod();
		this.org_sales_nam = at_policy_mainSchema.getorg_sales_nam();
		this.org_belg_cod = at_policy_mainSchema.getorg_belg_cod();
		this.org_belg_nam = at_policy_mainSchema.getorg_belg_nam();
		this.org_crs_cod = at_policy_mainSchema.getorg_crs_cod();
		this.org_crs_nam = at_policy_mainSchema.getorg_crs_nam();
		this.cst_cod = at_policy_mainSchema.getcst_cod();
		this.cst_nam = at_policy_mainSchema.getcst_nam();
		this.cst_typ = at_policy_mainSchema.getcst_typ();
		this.cst_addr = at_policy_mainSchema.getcst_addr();
		this.cst_post = at_policy_mainSchema.getcst_post();
		this.cst_idtyp_cod = at_policy_mainSchema.getcst_idtyp_cod();
		this.cst_idtyp_nam = at_policy_mainSchema.getcst_idtyp_nam();
		this.cst_idno = at_policy_mainSchema.getcst_idno();
		this.cst_linker_nam = at_policy_mainSchema.getcst_linker_nam();
		this.cst_linker_sex = at_policy_mainSchema.getcst_linker_sex();
		this.cst_tel = at_policy_mainSchema.getcst_tel();
		this.cst_mob = at_policy_mainSchema.getcst_mob();
		this.cst_mail = at_policy_mainSchema.getcst_mail();
		this.insd_info = at_policy_mainSchema.getinsd_info();
		this.beft_info = at_policy_mainSchema.getbeft_info();
		this.insr_targ = at_policy_mainSchema.getinsr_targ();
		this.insr_targ_info = at_policy_mainSchema.getinsr_targ_info();
		this.prem = at_policy_mainSchema.getprem();
		this.prem_sign = at_policy_mainSchema.getprem_sign();
		this.prem_pol = at_policy_mainSchema.getprem_pol();
		this.amount = at_policy_mainSchema.getamount();
		this.percnt = at_policy_mainSchema.getpercnt();
		this.bak1 = at_policy_mainSchema.getbak1();
		this.bak2 = at_policy_mainSchema.getbak2();
		this.bak3 = at_policy_mainSchema.getbak3();
		this.bak4 = at_policy_mainSchema.getbak4();
		this.bak5 = at_policy_mainSchema.getbak5();
		this.bak6 = at_policy_mainSchema.getbak6();
		this.bak7 = at_policy_mainSchema.getbak7();
		this.bak8 = at_policy_mainSchema.getbak8();
		this.data_upd_typ = at_policy_mainSchema.getdata_upd_typ();
		this.date_send = at_policy_mainSchema.getdate_send();
		this.date_update = at_policy_mainSchema.getdate_update();
		this.input_date = at_policy_mainSchema.getinput_date();
		this.hold_date = at_policy_mainSchema.gethold_date();
		this.proposal_no = at_policy_mainSchema.getproposal_no();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("pol_no") == null )
				this.pol_no = null;
			else
				this.pol_no = rs.getString("pol_no").trim();

			if( rs.getString("pol_no_orig") == null )
				this.pol_no_orig = null;
			else
				this.pol_no_orig = rs.getString("pol_no_orig").trim();

			if( rs.getString("comp_cod") == null )
				this.comp_cod = null;
			else
				this.comp_cod = rs.getString("comp_cod").trim();

			if( rs.getString("comp_nam") == null )
				this.comp_nam = null;
			else
				this.comp_nam = rs.getString("comp_nam").trim();

			if( rs.getString("date_sign") == null )
				this.date_sign = null;
			else
				this.date_sign = rs.getString("date_sign").trim();

			if( rs.getString("date_begin") == null )
				this.date_begin = null;
			else
				this.date_begin = rs.getString("date_begin").trim();

			if( rs.getString("date_end") == null )
				this.date_end = null;
			else
				this.date_end = rs.getString("date_end").trim();

			if( rs.getString("date_fee") == null )
				this.date_fee = null;
			else
				this.date_fee = rs.getString("date_fee").trim();

			this.pay_yrs = rs.getInt("pay_yrs");
			if( rs.getString("pay_mod_cod") == null )
				this.pay_mod_cod = null;
			else
				this.pay_mod_cod = rs.getString("pay_mod_cod").trim();

			if( rs.getString("pay_mod_nam") == null )
				this.pay_mod_nam = null;
			else
				this.pay_mod_nam = rs.getString("pay_mod_nam").trim();

			if( rs.getString("grp_psn_flg") == null )
				this.grp_psn_flg = null;
			else
				this.grp_psn_flg = rs.getString("grp_psn_flg").trim();

			if( rs.getString("chl_typ_cod") == null )
				this.chl_typ_cod = null;
			else
				this.chl_typ_cod = rs.getString("chl_typ_cod").trim();

			if( rs.getString("chl_typ_nam") == null )
				this.chl_typ_nam = null;
			else
				this.chl_typ_nam = rs.getString("chl_typ_nam").trim();

			if( rs.getString("chl_cod") == null )
				this.chl_cod = null;
			else
				this.chl_cod = rs.getString("chl_cod").trim();

			if( rs.getString("chl_nam") == null )
				this.chl_nam = null;
			else
				this.chl_nam = rs.getString("chl_nam").trim();

			if( rs.getString("brok_cod") == null )
				this.brok_cod = null;
			else
				this.brok_cod = rs.getString("brok_cod").trim();

			if( rs.getString("brok_nam") == null )
				this.brok_nam = null;
			else
				this.brok_nam = rs.getString("brok_nam").trim();

			if( rs.getString("crs_typ_cod") == null )
				this.crs_typ_cod = null;
			else
				this.crs_typ_cod = rs.getString("crs_typ_cod").trim();

			if( rs.getString("crs_typ_nam") == null )
				this.crs_typ_nam = null;
			else
				this.crs_typ_nam = rs.getString("crs_typ_nam").trim();

			if( rs.getString("sales_cod") == null )
				this.sales_cod = null;
			else
				this.sales_cod = rs.getString("sales_cod").trim();

			if( rs.getString("sales_nam") == null )
				this.sales_nam = null;
			else
				this.sales_nam = rs.getString("sales_nam").trim();

			if( rs.getString("crs_sales_cod") == null )
				this.crs_sales_cod = null;
			else
				this.crs_sales_cod = rs.getString("crs_sales_cod").trim();

			if( rs.getString("crs_sales_nam") == null )
				this.crs_sales_nam = null;
			else
				this.crs_sales_nam = rs.getString("crs_sales_nam").trim();

			if( rs.getString("org_sales_cod") == null )
				this.org_sales_cod = null;
			else
				this.org_sales_cod = rs.getString("org_sales_cod").trim();

			if( rs.getString("org_sales_nam") == null )
				this.org_sales_nam = null;
			else
				this.org_sales_nam = rs.getString("org_sales_nam").trim();

			if( rs.getString("org_belg_cod") == null )
				this.org_belg_cod = null;
			else
				this.org_belg_cod = rs.getString("org_belg_cod").trim();

			if( rs.getString("org_belg_nam") == null )
				this.org_belg_nam = null;
			else
				this.org_belg_nam = rs.getString("org_belg_nam").trim();

			if( rs.getString("org_crs_cod") == null )
				this.org_crs_cod = null;
			else
				this.org_crs_cod = rs.getString("org_crs_cod").trim();

			if( rs.getString("org_crs_nam") == null )
				this.org_crs_nam = null;
			else
				this.org_crs_nam = rs.getString("org_crs_nam").trim();

			if( rs.getString("cst_cod") == null )
				this.cst_cod = null;
			else
				this.cst_cod = rs.getString("cst_cod").trim();

			if( rs.getString("cst_nam") == null )
				this.cst_nam = null;
			else
				this.cst_nam = rs.getString("cst_nam").trim();

			if( rs.getString("cst_typ") == null )
				this.cst_typ = null;
			else
				this.cst_typ = rs.getString("cst_typ").trim();

			if( rs.getString("cst_addr") == null )
				this.cst_addr = null;
			else
				this.cst_addr = rs.getString("cst_addr").trim();

			if( rs.getString("cst_post") == null )
				this.cst_post = null;
			else
				this.cst_post = rs.getString("cst_post").trim();

			if( rs.getString("cst_idtyp_cod") == null )
				this.cst_idtyp_cod = null;
			else
				this.cst_idtyp_cod = rs.getString("cst_idtyp_cod").trim();

			if( rs.getString("cst_idtyp_nam") == null )
				this.cst_idtyp_nam = null;
			else
				this.cst_idtyp_nam = rs.getString("cst_idtyp_nam").trim();

			if( rs.getString("cst_idno") == null )
				this.cst_idno = null;
			else
				this.cst_idno = rs.getString("cst_idno").trim();

			if( rs.getString("cst_linker_nam") == null )
				this.cst_linker_nam = null;
			else
				this.cst_linker_nam = rs.getString("cst_linker_nam").trim();

			if( rs.getString("cst_linker_sex") == null )
				this.cst_linker_sex = null;
			else
				this.cst_linker_sex = rs.getString("cst_linker_sex").trim();

			if( rs.getString("cst_tel") == null )
				this.cst_tel = null;
			else
				this.cst_tel = rs.getString("cst_tel").trim();

			if( rs.getString("cst_mob") == null )
				this.cst_mob = null;
			else
				this.cst_mob = rs.getString("cst_mob").trim();

			if( rs.getString("cst_mail") == null )
				this.cst_mail = null;
			else
				this.cst_mail = rs.getString("cst_mail").trim();

			if( rs.getString("insd_info") == null )
				this.insd_info = null;
			else
				this.insd_info = rs.getString("insd_info").trim();

			if( rs.getString("beft_info") == null )
				this.beft_info = null;
			else
				this.beft_info = rs.getString("beft_info").trim();

			if( rs.getString("insr_targ") == null )
				this.insr_targ = null;
			else
				this.insr_targ = rs.getString("insr_targ").trim();

			if( rs.getString("insr_targ_info") == null )
				this.insr_targ_info = null;
			else
				this.insr_targ_info = rs.getString("insr_targ_info").trim();

			this.prem = rs.getDouble("prem");
			this.prem_sign = rs.getDouble("prem_sign");
			this.prem_pol = rs.getDouble("prem_pol");
			this.amount = rs.getDouble("amount");
			this.percnt = rs.getDouble("percnt");
			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

			if( rs.getString("bak4") == null )
				this.bak4 = null;
			else
				this.bak4 = rs.getString("bak4").trim();

			if( rs.getString("bak5") == null )
				this.bak5 = null;
			else
				this.bak5 = rs.getString("bak5").trim();

			if( rs.getString("bak6") == null )
				this.bak6 = null;
			else
				this.bak6 = rs.getString("bak6").trim();

			if( rs.getString("bak7") == null )
				this.bak7 = null;
			else
				this.bak7 = rs.getString("bak7").trim();

			if( rs.getString("bak8") == null )
				this.bak8 = null;
			else
				this.bak8 = rs.getString("bak8").trim();

			if( rs.getString("data_upd_typ") == null )
				this.data_upd_typ = null;
			else
				this.data_upd_typ = rs.getString("data_upd_typ").trim();

			if( rs.getString("date_send") == null )
				this.date_send = null;
			else
				this.date_send = rs.getString("date_send").trim();

			if( rs.getString("date_update") == null )
				this.date_update = null;
			else
				this.date_update = rs.getString("date_update").trim();

			if( rs.getString("input_date") == null )
				this.input_date = null;
			else
				this.input_date = rs.getString("input_date").trim();

			if( rs.getString("hold_date") == null )
				this.hold_date = null;
			else
				this.hold_date = rs.getString("hold_date").trim();

			if( rs.getString("proposal_no") == null )
				this.proposal_no = null;
			else
				this.proposal_no = rs.getString("proposal_no").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的t_policy_main表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "t_policy_mainSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public t_policy_mainSchema getSchema()
	{
		t_policy_mainSchema at_policy_mainSchema = new t_policy_mainSchema();
		at_policy_mainSchema.setSchema(this);
		return at_policy_mainSchema;
	}

	public t_policy_mainDB getDB()
	{
		t_policy_mainDB aDBOper = new t_policy_mainDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpt_policy_main描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(pol_no)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(pol_no_orig)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comp_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comp_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(date_sign)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(date_begin)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(date_end)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(date_fee)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(pay_yrs));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(pay_mod_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(pay_mod_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(grp_psn_flg)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(chl_typ_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(chl_typ_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(chl_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(chl_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(brok_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(brok_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(crs_typ_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(crs_typ_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sales_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sales_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(crs_sales_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(crs_sales_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(org_sales_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(org_sales_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(org_belg_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(org_belg_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(org_crs_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(org_crs_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_typ)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_addr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_post)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_idtyp_cod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_idtyp_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_idno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_linker_nam)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_linker_sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_tel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_mob)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(cst_mail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insd_info)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(beft_info)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insr_targ)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insr_targ_info)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(prem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(prem_sign));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(prem_pol));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(amount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(percnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak7)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak8)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(data_upd_typ)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(date_send)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(date_update)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(input_date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(hold_date)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(proposal_no));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpt_policy_main>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			pol_no = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			pol_no_orig = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			comp_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			comp_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			date_sign = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			date_begin = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			date_end = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			date_fee = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			pay_yrs= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			pay_mod_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			pay_mod_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			grp_psn_flg = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			chl_typ_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			chl_typ_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			chl_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			chl_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			brok_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			brok_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			crs_typ_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			crs_typ_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			sales_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			sales_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			crs_sales_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			crs_sales_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			org_sales_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			org_sales_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			org_belg_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			org_belg_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			org_crs_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			org_crs_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			cst_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			cst_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			cst_typ = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			cst_addr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			cst_post = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			cst_idtyp_cod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			cst_idtyp_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			cst_idno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			cst_linker_nam = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			cst_linker_sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			cst_tel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			cst_mob = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			cst_mail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			insd_info = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			beft_info = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			insr_targ = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			insr_targ_info = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,48,SysConst.PACKAGESPILTER))).doubleValue();
			prem_sign = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,49,SysConst.PACKAGESPILTER))).doubleValue();
			prem_pol = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,50,SysConst.PACKAGESPILTER))).doubleValue();
			amount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,51,SysConst.PACKAGESPILTER))).doubleValue();
			percnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,52,SysConst.PACKAGESPILTER))).doubleValue();
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			bak5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			bak6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			bak7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			bak8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			data_upd_typ = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			date_send = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			date_update = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
			input_date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			hold_date = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			proposal_no = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "t_policy_mainSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("pol_no"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(pol_no));
		}
		if (FCode.equals("pol_no_orig"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(pol_no_orig));
		}
		if (FCode.equals("comp_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comp_cod));
		}
		if (FCode.equals("comp_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comp_nam));
		}
		if (FCode.equals("date_sign"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(date_sign));
		}
		if (FCode.equals("date_begin"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(date_begin));
		}
		if (FCode.equals("date_end"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(date_end));
		}
		if (FCode.equals("date_fee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(date_fee));
		}
		if (FCode.equals("pay_yrs"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(pay_yrs));
		}
		if (FCode.equals("pay_mod_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(pay_mod_cod));
		}
		if (FCode.equals("pay_mod_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(pay_mod_nam));
		}
		if (FCode.equals("grp_psn_flg"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(grp_psn_flg));
		}
		if (FCode.equals("chl_typ_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(chl_typ_cod));
		}
		if (FCode.equals("chl_typ_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(chl_typ_nam));
		}
		if (FCode.equals("chl_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(chl_cod));
		}
		if (FCode.equals("chl_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(chl_nam));
		}
		if (FCode.equals("brok_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(brok_cod));
		}
		if (FCode.equals("brok_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(brok_nam));
		}
		if (FCode.equals("crs_typ_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(crs_typ_cod));
		}
		if (FCode.equals("crs_typ_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(crs_typ_nam));
		}
		if (FCode.equals("sales_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sales_cod));
		}
		if (FCode.equals("sales_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sales_nam));
		}
		if (FCode.equals("crs_sales_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(crs_sales_cod));
		}
		if (FCode.equals("crs_sales_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(crs_sales_nam));
		}
		if (FCode.equals("org_sales_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(org_sales_cod));
		}
		if (FCode.equals("org_sales_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(org_sales_nam));
		}
		if (FCode.equals("org_belg_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(org_belg_cod));
		}
		if (FCode.equals("org_belg_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(org_belg_nam));
		}
		if (FCode.equals("org_crs_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(org_crs_cod));
		}
		if (FCode.equals("org_crs_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(org_crs_nam));
		}
		if (FCode.equals("cst_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_cod));
		}
		if (FCode.equals("cst_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_nam));
		}
		if (FCode.equals("cst_typ"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_typ));
		}
		if (FCode.equals("cst_addr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_addr));
		}
		if (FCode.equals("cst_post"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_post));
		}
		if (FCode.equals("cst_idtyp_cod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_idtyp_cod));
		}
		if (FCode.equals("cst_idtyp_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_idtyp_nam));
		}
		if (FCode.equals("cst_idno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_idno));
		}
		if (FCode.equals("cst_linker_nam"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_linker_nam));
		}
		if (FCode.equals("cst_linker_sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_linker_sex));
		}
		if (FCode.equals("cst_tel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_tel));
		}
		if (FCode.equals("cst_mob"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_mob));
		}
		if (FCode.equals("cst_mail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(cst_mail));
		}
		if (FCode.equals("insd_info"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insd_info));
		}
		if (FCode.equals("beft_info"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(beft_info));
		}
		if (FCode.equals("insr_targ"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insr_targ));
		}
		if (FCode.equals("insr_targ_info"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insr_targ_info));
		}
		if (FCode.equals("prem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prem));
		}
		if (FCode.equals("prem_sign"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prem_sign));
		}
		if (FCode.equals("prem_pol"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(prem_pol));
		}
		if (FCode.equals("amount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(amount));
		}
		if (FCode.equals("percnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(percnt));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (FCode.equals("bak4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak4));
		}
		if (FCode.equals("bak5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak5));
		}
		if (FCode.equals("bak6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak6));
		}
		if (FCode.equals("bak7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak7));
		}
		if (FCode.equals("bak8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak8));
		}
		if (FCode.equals("data_upd_typ"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(data_upd_typ));
		}
		if (FCode.equals("date_send"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(date_send));
		}
		if (FCode.equals("date_update"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(date_update));
		}
		if (FCode.equals("input_date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(input_date));
		}
		if (FCode.equals("hold_date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(hold_date));
		}
		if (FCode.equals("proposal_no"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(proposal_no));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(pol_no);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(pol_no_orig);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(comp_cod);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(comp_nam);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(date_sign);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(date_begin);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(date_end);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(date_fee);
				break;
			case 8:
				strFieldValue = String.valueOf(pay_yrs);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(pay_mod_cod);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(pay_mod_nam);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(grp_psn_flg);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(chl_typ_cod);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(chl_typ_nam);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(chl_cod);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(chl_nam);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(brok_cod);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(brok_nam);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(crs_typ_cod);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(crs_typ_nam);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(sales_cod);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(sales_nam);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(crs_sales_cod);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(crs_sales_nam);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(org_sales_cod);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(org_sales_nam);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(org_belg_cod);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(org_belg_nam);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(org_crs_cod);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(org_crs_nam);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(cst_cod);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(cst_nam);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(cst_typ);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(cst_addr);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(cst_post);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(cst_idtyp_cod);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(cst_idtyp_nam);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(cst_idno);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(cst_linker_nam);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(cst_linker_sex);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(cst_tel);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(cst_mob);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(cst_mail);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(insd_info);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(beft_info);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(insr_targ);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(insr_targ_info);
				break;
			case 47:
				strFieldValue = String.valueOf(prem);
				break;
			case 48:
				strFieldValue = String.valueOf(prem_sign);
				break;
			case 49:
				strFieldValue = String.valueOf(prem_pol);
				break;
			case 50:
				strFieldValue = String.valueOf(amount);
				break;
			case 51:
				strFieldValue = String.valueOf(percnt);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(bak4);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(bak5);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(bak6);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(bak7);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(bak8);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(data_upd_typ);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(date_send);
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(date_update);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(input_date);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(hold_date);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(proposal_no);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("pol_no"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				pol_no = FValue.trim();
			}
			else
				pol_no = null;
		}
		if (FCode.equalsIgnoreCase("pol_no_orig"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				pol_no_orig = FValue.trim();
			}
			else
				pol_no_orig = null;
		}
		if (FCode.equalsIgnoreCase("comp_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comp_cod = FValue.trim();
			}
			else
				comp_cod = null;
		}
		if (FCode.equalsIgnoreCase("comp_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comp_nam = FValue.trim();
			}
			else
				comp_nam = null;
		}
		if (FCode.equalsIgnoreCase("date_sign"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				date_sign = FValue.trim();
			}
			else
				date_sign = null;
		}
		if (FCode.equalsIgnoreCase("date_begin"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				date_begin = FValue.trim();
			}
			else
				date_begin = null;
		}
		if (FCode.equalsIgnoreCase("date_end"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				date_end = FValue.trim();
			}
			else
				date_end = null;
		}
		if (FCode.equalsIgnoreCase("date_fee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				date_fee = FValue.trim();
			}
			else
				date_fee = null;
		}
		if (FCode.equalsIgnoreCase("pay_yrs"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				pay_yrs = i;
			}
		}
		if (FCode.equalsIgnoreCase("pay_mod_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				pay_mod_cod = FValue.trim();
			}
			else
				pay_mod_cod = null;
		}
		if (FCode.equalsIgnoreCase("pay_mod_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				pay_mod_nam = FValue.trim();
			}
			else
				pay_mod_nam = null;
		}
		if (FCode.equalsIgnoreCase("grp_psn_flg"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				grp_psn_flg = FValue.trim();
			}
			else
				grp_psn_flg = null;
		}
		if (FCode.equalsIgnoreCase("chl_typ_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				chl_typ_cod = FValue.trim();
			}
			else
				chl_typ_cod = null;
		}
		if (FCode.equalsIgnoreCase("chl_typ_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				chl_typ_nam = FValue.trim();
			}
			else
				chl_typ_nam = null;
		}
		if (FCode.equalsIgnoreCase("chl_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				chl_cod = FValue.trim();
			}
			else
				chl_cod = null;
		}
		if (FCode.equalsIgnoreCase("chl_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				chl_nam = FValue.trim();
			}
			else
				chl_nam = null;
		}
		if (FCode.equalsIgnoreCase("brok_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				brok_cod = FValue.trim();
			}
			else
				brok_cod = null;
		}
		if (FCode.equalsIgnoreCase("brok_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				brok_nam = FValue.trim();
			}
			else
				brok_nam = null;
		}
		if (FCode.equalsIgnoreCase("crs_typ_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				crs_typ_cod = FValue.trim();
			}
			else
				crs_typ_cod = null;
		}
		if (FCode.equalsIgnoreCase("crs_typ_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				crs_typ_nam = FValue.trim();
			}
			else
				crs_typ_nam = null;
		}
		if (FCode.equalsIgnoreCase("sales_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sales_cod = FValue.trim();
			}
			else
				sales_cod = null;
		}
		if (FCode.equalsIgnoreCase("sales_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sales_nam = FValue.trim();
			}
			else
				sales_nam = null;
		}
		if (FCode.equalsIgnoreCase("crs_sales_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				crs_sales_cod = FValue.trim();
			}
			else
				crs_sales_cod = null;
		}
		if (FCode.equalsIgnoreCase("crs_sales_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				crs_sales_nam = FValue.trim();
			}
			else
				crs_sales_nam = null;
		}
		if (FCode.equalsIgnoreCase("org_sales_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				org_sales_cod = FValue.trim();
			}
			else
				org_sales_cod = null;
		}
		if (FCode.equalsIgnoreCase("org_sales_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				org_sales_nam = FValue.trim();
			}
			else
				org_sales_nam = null;
		}
		if (FCode.equalsIgnoreCase("org_belg_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				org_belg_cod = FValue.trim();
			}
			else
				org_belg_cod = null;
		}
		if (FCode.equalsIgnoreCase("org_belg_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				org_belg_nam = FValue.trim();
			}
			else
				org_belg_nam = null;
		}
		if (FCode.equalsIgnoreCase("org_crs_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				org_crs_cod = FValue.trim();
			}
			else
				org_crs_cod = null;
		}
		if (FCode.equalsIgnoreCase("org_crs_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				org_crs_nam = FValue.trim();
			}
			else
				org_crs_nam = null;
		}
		if (FCode.equalsIgnoreCase("cst_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_cod = FValue.trim();
			}
			else
				cst_cod = null;
		}
		if (FCode.equalsIgnoreCase("cst_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_nam = FValue.trim();
			}
			else
				cst_nam = null;
		}
		if (FCode.equalsIgnoreCase("cst_typ"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_typ = FValue.trim();
			}
			else
				cst_typ = null;
		}
		if (FCode.equalsIgnoreCase("cst_addr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_addr = FValue.trim();
			}
			else
				cst_addr = null;
		}
		if (FCode.equalsIgnoreCase("cst_post"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_post = FValue.trim();
			}
			else
				cst_post = null;
		}
		if (FCode.equalsIgnoreCase("cst_idtyp_cod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_idtyp_cod = FValue.trim();
			}
			else
				cst_idtyp_cod = null;
		}
		if (FCode.equalsIgnoreCase("cst_idtyp_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_idtyp_nam = FValue.trim();
			}
			else
				cst_idtyp_nam = null;
		}
		if (FCode.equalsIgnoreCase("cst_idno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_idno = FValue.trim();
			}
			else
				cst_idno = null;
		}
		if (FCode.equalsIgnoreCase("cst_linker_nam"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_linker_nam = FValue.trim();
			}
			else
				cst_linker_nam = null;
		}
		if (FCode.equalsIgnoreCase("cst_linker_sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_linker_sex = FValue.trim();
			}
			else
				cst_linker_sex = null;
		}
		if (FCode.equalsIgnoreCase("cst_tel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_tel = FValue.trim();
			}
			else
				cst_tel = null;
		}
		if (FCode.equalsIgnoreCase("cst_mob"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_mob = FValue.trim();
			}
			else
				cst_mob = null;
		}
		if (FCode.equalsIgnoreCase("cst_mail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				cst_mail = FValue.trim();
			}
			else
				cst_mail = null;
		}
		if (FCode.equalsIgnoreCase("insd_info"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insd_info = FValue.trim();
			}
			else
				insd_info = null;
		}
		if (FCode.equalsIgnoreCase("beft_info"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				beft_info = FValue.trim();
			}
			else
				beft_info = null;
		}
		if (FCode.equalsIgnoreCase("insr_targ"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insr_targ = FValue.trim();
			}
			else
				insr_targ = null;
		}
		if (FCode.equalsIgnoreCase("insr_targ_info"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insr_targ_info = FValue.trim();
			}
			else
				insr_targ_info = null;
		}
		if (FCode.equalsIgnoreCase("prem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				prem = d;
			}
		}
		if (FCode.equalsIgnoreCase("prem_sign"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				prem_sign = d;
			}
		}
		if (FCode.equalsIgnoreCase("prem_pol"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				prem_pol = d;
			}
		}
		if (FCode.equalsIgnoreCase("amount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				amount = d;
			}
		}
		if (FCode.equalsIgnoreCase("percnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				percnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		if (FCode.equalsIgnoreCase("bak4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak4 = FValue.trim();
			}
			else
				bak4 = null;
		}
		if (FCode.equalsIgnoreCase("bak5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak5 = FValue.trim();
			}
			else
				bak5 = null;
		}
		if (FCode.equalsIgnoreCase("bak6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak6 = FValue.trim();
			}
			else
				bak6 = null;
		}
		if (FCode.equalsIgnoreCase("bak7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak7 = FValue.trim();
			}
			else
				bak7 = null;
		}
		if (FCode.equalsIgnoreCase("bak8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak8 = FValue.trim();
			}
			else
				bak8 = null;
		}
		if (FCode.equalsIgnoreCase("data_upd_typ"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				data_upd_typ = FValue.trim();
			}
			else
				data_upd_typ = null;
		}
		if (FCode.equalsIgnoreCase("date_send"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				date_send = FValue.trim();
			}
			else
				date_send = null;
		}
		if (FCode.equalsIgnoreCase("date_update"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				date_update = FValue.trim();
			}
			else
				date_update = null;
		}
		if (FCode.equalsIgnoreCase("input_date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				input_date = FValue.trim();
			}
			else
				input_date = null;
		}
		if (FCode.equalsIgnoreCase("hold_date"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				hold_date = FValue.trim();
			}
			else
				hold_date = null;
		}
		if (FCode.equalsIgnoreCase("proposal_no"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				proposal_no = FValue.trim();
			}
			else
				proposal_no = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		t_policy_mainSchema other = (t_policy_mainSchema)otherObject;
		return
			(pol_no == null ? other.getpol_no() == null : pol_no.equals(other.getpol_no()))
			&& (pol_no_orig == null ? other.getpol_no_orig() == null : pol_no_orig.equals(other.getpol_no_orig()))
			&& (comp_cod == null ? other.getcomp_cod() == null : comp_cod.equals(other.getcomp_cod()))
			&& (comp_nam == null ? other.getcomp_nam() == null : comp_nam.equals(other.getcomp_nam()))
			&& (date_sign == null ? other.getdate_sign() == null : date_sign.equals(other.getdate_sign()))
			&& (date_begin == null ? other.getdate_begin() == null : date_begin.equals(other.getdate_begin()))
			&& (date_end == null ? other.getdate_end() == null : date_end.equals(other.getdate_end()))
			&& (date_fee == null ? other.getdate_fee() == null : date_fee.equals(other.getdate_fee()))
			&& pay_yrs == other.getpay_yrs()
			&& (pay_mod_cod == null ? other.getpay_mod_cod() == null : pay_mod_cod.equals(other.getpay_mod_cod()))
			&& (pay_mod_nam == null ? other.getpay_mod_nam() == null : pay_mod_nam.equals(other.getpay_mod_nam()))
			&& (grp_psn_flg == null ? other.getgrp_psn_flg() == null : grp_psn_flg.equals(other.getgrp_psn_flg()))
			&& (chl_typ_cod == null ? other.getchl_typ_cod() == null : chl_typ_cod.equals(other.getchl_typ_cod()))
			&& (chl_typ_nam == null ? other.getchl_typ_nam() == null : chl_typ_nam.equals(other.getchl_typ_nam()))
			&& (chl_cod == null ? other.getchl_cod() == null : chl_cod.equals(other.getchl_cod()))
			&& (chl_nam == null ? other.getchl_nam() == null : chl_nam.equals(other.getchl_nam()))
			&& (brok_cod == null ? other.getbrok_cod() == null : brok_cod.equals(other.getbrok_cod()))
			&& (brok_nam == null ? other.getbrok_nam() == null : brok_nam.equals(other.getbrok_nam()))
			&& (crs_typ_cod == null ? other.getcrs_typ_cod() == null : crs_typ_cod.equals(other.getcrs_typ_cod()))
			&& (crs_typ_nam == null ? other.getcrs_typ_nam() == null : crs_typ_nam.equals(other.getcrs_typ_nam()))
			&& (sales_cod == null ? other.getsales_cod() == null : sales_cod.equals(other.getsales_cod()))
			&& (sales_nam == null ? other.getsales_nam() == null : sales_nam.equals(other.getsales_nam()))
			&& (crs_sales_cod == null ? other.getcrs_sales_cod() == null : crs_sales_cod.equals(other.getcrs_sales_cod()))
			&& (crs_sales_nam == null ? other.getcrs_sales_nam() == null : crs_sales_nam.equals(other.getcrs_sales_nam()))
			&& (org_sales_cod == null ? other.getorg_sales_cod() == null : org_sales_cod.equals(other.getorg_sales_cod()))
			&& (org_sales_nam == null ? other.getorg_sales_nam() == null : org_sales_nam.equals(other.getorg_sales_nam()))
			&& (org_belg_cod == null ? other.getorg_belg_cod() == null : org_belg_cod.equals(other.getorg_belg_cod()))
			&& (org_belg_nam == null ? other.getorg_belg_nam() == null : org_belg_nam.equals(other.getorg_belg_nam()))
			&& (org_crs_cod == null ? other.getorg_crs_cod() == null : org_crs_cod.equals(other.getorg_crs_cod()))
			&& (org_crs_nam == null ? other.getorg_crs_nam() == null : org_crs_nam.equals(other.getorg_crs_nam()))
			&& (cst_cod == null ? other.getcst_cod() == null : cst_cod.equals(other.getcst_cod()))
			&& (cst_nam == null ? other.getcst_nam() == null : cst_nam.equals(other.getcst_nam()))
			&& (cst_typ == null ? other.getcst_typ() == null : cst_typ.equals(other.getcst_typ()))
			&& (cst_addr == null ? other.getcst_addr() == null : cst_addr.equals(other.getcst_addr()))
			&& (cst_post == null ? other.getcst_post() == null : cst_post.equals(other.getcst_post()))
			&& (cst_idtyp_cod == null ? other.getcst_idtyp_cod() == null : cst_idtyp_cod.equals(other.getcst_idtyp_cod()))
			&& (cst_idtyp_nam == null ? other.getcst_idtyp_nam() == null : cst_idtyp_nam.equals(other.getcst_idtyp_nam()))
			&& (cst_idno == null ? other.getcst_idno() == null : cst_idno.equals(other.getcst_idno()))
			&& (cst_linker_nam == null ? other.getcst_linker_nam() == null : cst_linker_nam.equals(other.getcst_linker_nam()))
			&& (cst_linker_sex == null ? other.getcst_linker_sex() == null : cst_linker_sex.equals(other.getcst_linker_sex()))
			&& (cst_tel == null ? other.getcst_tel() == null : cst_tel.equals(other.getcst_tel()))
			&& (cst_mob == null ? other.getcst_mob() == null : cst_mob.equals(other.getcst_mob()))
			&& (cst_mail == null ? other.getcst_mail() == null : cst_mail.equals(other.getcst_mail()))
			&& (insd_info == null ? other.getinsd_info() == null : insd_info.equals(other.getinsd_info()))
			&& (beft_info == null ? other.getbeft_info() == null : beft_info.equals(other.getbeft_info()))
			&& (insr_targ == null ? other.getinsr_targ() == null : insr_targ.equals(other.getinsr_targ()))
			&& (insr_targ_info == null ? other.getinsr_targ_info() == null : insr_targ_info.equals(other.getinsr_targ_info()))
			&& prem == other.getprem()
			&& prem_sign == other.getprem_sign()
			&& prem_pol == other.getprem_pol()
			&& amount == other.getamount()
			&& percnt == other.getpercnt()
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()))
			&& (bak4 == null ? other.getbak4() == null : bak4.equals(other.getbak4()))
			&& (bak5 == null ? other.getbak5() == null : bak5.equals(other.getbak5()))
			&& (bak6 == null ? other.getbak6() == null : bak6.equals(other.getbak6()))
			&& (bak7 == null ? other.getbak7() == null : bak7.equals(other.getbak7()))
			&& (bak8 == null ? other.getbak8() == null : bak8.equals(other.getbak8()))
			&& (data_upd_typ == null ? other.getdata_upd_typ() == null : data_upd_typ.equals(other.getdata_upd_typ()))
			&& (date_send == null ? other.getdate_send() == null : date_send.equals(other.getdate_send()))
			&& (date_update == null ? other.getdate_update() == null : date_update.equals(other.getdate_update()))
			&& (input_date == null ? other.getinput_date() == null : input_date.equals(other.getinput_date()))
			&& (hold_date == null ? other.gethold_date() == null : hold_date.equals(other.gethold_date()))
			&& (proposal_no == null ? other.getproposal_no() == null : proposal_no.equals(other.getproposal_no()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("pol_no") ) {
			return 0;
		}
		if( strFieldName.equals("pol_no_orig") ) {
			return 1;
		}
		if( strFieldName.equals("comp_cod") ) {
			return 2;
		}
		if( strFieldName.equals("comp_nam") ) {
			return 3;
		}
		if( strFieldName.equals("date_sign") ) {
			return 4;
		}
		if( strFieldName.equals("date_begin") ) {
			return 5;
		}
		if( strFieldName.equals("date_end") ) {
			return 6;
		}
		if( strFieldName.equals("date_fee") ) {
			return 7;
		}
		if( strFieldName.equals("pay_yrs") ) {
			return 8;
		}
		if( strFieldName.equals("pay_mod_cod") ) {
			return 9;
		}
		if( strFieldName.equals("pay_mod_nam") ) {
			return 10;
		}
		if( strFieldName.equals("grp_psn_flg") ) {
			return 11;
		}
		if( strFieldName.equals("chl_typ_cod") ) {
			return 12;
		}
		if( strFieldName.equals("chl_typ_nam") ) {
			return 13;
		}
		if( strFieldName.equals("chl_cod") ) {
			return 14;
		}
		if( strFieldName.equals("chl_nam") ) {
			return 15;
		}
		if( strFieldName.equals("brok_cod") ) {
			return 16;
		}
		if( strFieldName.equals("brok_nam") ) {
			return 17;
		}
		if( strFieldName.equals("crs_typ_cod") ) {
			return 18;
		}
		if( strFieldName.equals("crs_typ_nam") ) {
			return 19;
		}
		if( strFieldName.equals("sales_cod") ) {
			return 20;
		}
		if( strFieldName.equals("sales_nam") ) {
			return 21;
		}
		if( strFieldName.equals("crs_sales_cod") ) {
			return 22;
		}
		if( strFieldName.equals("crs_sales_nam") ) {
			return 23;
		}
		if( strFieldName.equals("org_sales_cod") ) {
			return 24;
		}
		if( strFieldName.equals("org_sales_nam") ) {
			return 25;
		}
		if( strFieldName.equals("org_belg_cod") ) {
			return 26;
		}
		if( strFieldName.equals("org_belg_nam") ) {
			return 27;
		}
		if( strFieldName.equals("org_crs_cod") ) {
			return 28;
		}
		if( strFieldName.equals("org_crs_nam") ) {
			return 29;
		}
		if( strFieldName.equals("cst_cod") ) {
			return 30;
		}
		if( strFieldName.equals("cst_nam") ) {
			return 31;
		}
		if( strFieldName.equals("cst_typ") ) {
			return 32;
		}
		if( strFieldName.equals("cst_addr") ) {
			return 33;
		}
		if( strFieldName.equals("cst_post") ) {
			return 34;
		}
		if( strFieldName.equals("cst_idtyp_cod") ) {
			return 35;
		}
		if( strFieldName.equals("cst_idtyp_nam") ) {
			return 36;
		}
		if( strFieldName.equals("cst_idno") ) {
			return 37;
		}
		if( strFieldName.equals("cst_linker_nam") ) {
			return 38;
		}
		if( strFieldName.equals("cst_linker_sex") ) {
			return 39;
		}
		if( strFieldName.equals("cst_tel") ) {
			return 40;
		}
		if( strFieldName.equals("cst_mob") ) {
			return 41;
		}
		if( strFieldName.equals("cst_mail") ) {
			return 42;
		}
		if( strFieldName.equals("insd_info") ) {
			return 43;
		}
		if( strFieldName.equals("beft_info") ) {
			return 44;
		}
		if( strFieldName.equals("insr_targ") ) {
			return 45;
		}
		if( strFieldName.equals("insr_targ_info") ) {
			return 46;
		}
		if( strFieldName.equals("prem") ) {
			return 47;
		}
		if( strFieldName.equals("prem_sign") ) {
			return 48;
		}
		if( strFieldName.equals("prem_pol") ) {
			return 49;
		}
		if( strFieldName.equals("amount") ) {
			return 50;
		}
		if( strFieldName.equals("percnt") ) {
			return 51;
		}
		if( strFieldName.equals("bak1") ) {
			return 52;
		}
		if( strFieldName.equals("bak2") ) {
			return 53;
		}
		if( strFieldName.equals("bak3") ) {
			return 54;
		}
		if( strFieldName.equals("bak4") ) {
			return 55;
		}
		if( strFieldName.equals("bak5") ) {
			return 56;
		}
		if( strFieldName.equals("bak6") ) {
			return 57;
		}
		if( strFieldName.equals("bak7") ) {
			return 58;
		}
		if( strFieldName.equals("bak8") ) {
			return 59;
		}
		if( strFieldName.equals("data_upd_typ") ) {
			return 60;
		}
		if( strFieldName.equals("date_send") ) {
			return 61;
		}
		if( strFieldName.equals("date_update") ) {
			return 62;
		}
		if( strFieldName.equals("input_date") ) {
			return 63;
		}
		if( strFieldName.equals("hold_date") ) {
			return 64;
		}
		if( strFieldName.equals("proposal_no") ) {
			return 65;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "pol_no";
				break;
			case 1:
				strFieldName = "pol_no_orig";
				break;
			case 2:
				strFieldName = "comp_cod";
				break;
			case 3:
				strFieldName = "comp_nam";
				break;
			case 4:
				strFieldName = "date_sign";
				break;
			case 5:
				strFieldName = "date_begin";
				break;
			case 6:
				strFieldName = "date_end";
				break;
			case 7:
				strFieldName = "date_fee";
				break;
			case 8:
				strFieldName = "pay_yrs";
				break;
			case 9:
				strFieldName = "pay_mod_cod";
				break;
			case 10:
				strFieldName = "pay_mod_nam";
				break;
			case 11:
				strFieldName = "grp_psn_flg";
				break;
			case 12:
				strFieldName = "chl_typ_cod";
				break;
			case 13:
				strFieldName = "chl_typ_nam";
				break;
			case 14:
				strFieldName = "chl_cod";
				break;
			case 15:
				strFieldName = "chl_nam";
				break;
			case 16:
				strFieldName = "brok_cod";
				break;
			case 17:
				strFieldName = "brok_nam";
				break;
			case 18:
				strFieldName = "crs_typ_cod";
				break;
			case 19:
				strFieldName = "crs_typ_nam";
				break;
			case 20:
				strFieldName = "sales_cod";
				break;
			case 21:
				strFieldName = "sales_nam";
				break;
			case 22:
				strFieldName = "crs_sales_cod";
				break;
			case 23:
				strFieldName = "crs_sales_nam";
				break;
			case 24:
				strFieldName = "org_sales_cod";
				break;
			case 25:
				strFieldName = "org_sales_nam";
				break;
			case 26:
				strFieldName = "org_belg_cod";
				break;
			case 27:
				strFieldName = "org_belg_nam";
				break;
			case 28:
				strFieldName = "org_crs_cod";
				break;
			case 29:
				strFieldName = "org_crs_nam";
				break;
			case 30:
				strFieldName = "cst_cod";
				break;
			case 31:
				strFieldName = "cst_nam";
				break;
			case 32:
				strFieldName = "cst_typ";
				break;
			case 33:
				strFieldName = "cst_addr";
				break;
			case 34:
				strFieldName = "cst_post";
				break;
			case 35:
				strFieldName = "cst_idtyp_cod";
				break;
			case 36:
				strFieldName = "cst_idtyp_nam";
				break;
			case 37:
				strFieldName = "cst_idno";
				break;
			case 38:
				strFieldName = "cst_linker_nam";
				break;
			case 39:
				strFieldName = "cst_linker_sex";
				break;
			case 40:
				strFieldName = "cst_tel";
				break;
			case 41:
				strFieldName = "cst_mob";
				break;
			case 42:
				strFieldName = "cst_mail";
				break;
			case 43:
				strFieldName = "insd_info";
				break;
			case 44:
				strFieldName = "beft_info";
				break;
			case 45:
				strFieldName = "insr_targ";
				break;
			case 46:
				strFieldName = "insr_targ_info";
				break;
			case 47:
				strFieldName = "prem";
				break;
			case 48:
				strFieldName = "prem_sign";
				break;
			case 49:
				strFieldName = "prem_pol";
				break;
			case 50:
				strFieldName = "amount";
				break;
			case 51:
				strFieldName = "percnt";
				break;
			case 52:
				strFieldName = "bak1";
				break;
			case 53:
				strFieldName = "bak2";
				break;
			case 54:
				strFieldName = "bak3";
				break;
			case 55:
				strFieldName = "bak4";
				break;
			case 56:
				strFieldName = "bak5";
				break;
			case 57:
				strFieldName = "bak6";
				break;
			case 58:
				strFieldName = "bak7";
				break;
			case 59:
				strFieldName = "bak8";
				break;
			case 60:
				strFieldName = "data_upd_typ";
				break;
			case 61:
				strFieldName = "date_send";
				break;
			case 62:
				strFieldName = "date_update";
				break;
			case 63:
				strFieldName = "input_date";
				break;
			case 64:
				strFieldName = "hold_date";
				break;
			case 65:
				strFieldName = "proposal_no";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("pol_no") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("pol_no_orig") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comp_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comp_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("date_sign") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("date_begin") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("date_end") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("date_fee") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("pay_yrs") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("pay_mod_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("pay_mod_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("grp_psn_flg") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("chl_typ_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("chl_typ_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("chl_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("chl_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("brok_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("brok_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("crs_typ_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("crs_typ_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sales_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sales_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("crs_sales_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("crs_sales_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("org_sales_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("org_sales_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("org_belg_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("org_belg_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("org_crs_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("org_crs_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_typ") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_addr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_post") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_idtyp_cod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_idtyp_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_idno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_linker_nam") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_linker_sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_tel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_mob") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("cst_mail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insd_info") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("beft_info") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insr_targ") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insr_targ_info") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("prem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("prem_sign") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("prem_pol") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("amount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("percnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak8") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("data_upd_typ") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("date_send") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("date_update") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("input_date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("hold_date") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("proposal_no") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 48:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 49:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 50:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 51:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
