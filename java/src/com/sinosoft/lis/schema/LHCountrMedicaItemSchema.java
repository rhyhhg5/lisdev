/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LHCountrMedicaItemDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LHCountrMedicaItemSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-刘莹-表结构修改-医疗服务项目信息-200606014*
 * @CreateDate：2006-06-14
 */
public class LHCountrMedicaItemSchema implements Schema, Cloneable {
    // @Field
    /** 医疗服务项目代码 */
    private String MedicaItemCode;
    /** 医疗服务项目名称 */
    private String MedicaItemName;
    /** 医疗服务项目别名 */
    private String MedicaItemOtheName;
    /** 项目内涵 */
    private String MedicaMeanin;
    /** 除外内容 */
    private String ExcConten;
    /** 计价单位 */
    private String PriceUnit;
    /** 收费单位 */
    private String ChargeUnit;
    /** 说明 */
    private String Explai;
    /** 使用频率级别 */
    private int FrequencyFlag;
    /** Icd代码 */
    private String ICDCode;
    /** 手术等级 */
    private String OpsGrag;
    /** 地区属性 */
    private String AreaTpye;
    /** 标准计量单位 */
    private String StandardMeasureUnit;
    /** 基础收费标准 */
    private double BasicCharge;
    /** 卫生材料费用 */
    private double SanitationCost;
    /** 一级医院收费标准 */
    private double FirstChargeStd;
    /** 二级医院收费标准 */
    private double SecondChargeStd;
    /** 三级医院收费标准 */
    private double ThirdChargeStd;
    /** 医疗服务项目类别代码 */
    private String ClassCode;
    /** 是否空腹 */
    private String InBelly;
    /** 正常值 */
    private String NomalValue;

    public static final int FIELDNUM = 21; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHCountrMedicaItemSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "MedicaItemCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHCountrMedicaItemSchema cloned = (LHCountrMedicaItemSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getMedicaItemCode() {
        return MedicaItemCode;
    }

    public void setMedicaItemCode(String aMedicaItemCode) {
        MedicaItemCode = aMedicaItemCode;
    }

    public String getMedicaItemName() {
        return MedicaItemName;
    }

    public void setMedicaItemName(String aMedicaItemName) {
        MedicaItemName = aMedicaItemName;
    }

    public String getMedicaItemOtheName() {
        return MedicaItemOtheName;
    }

    public void setMedicaItemOtheName(String aMedicaItemOtheName) {
        MedicaItemOtheName = aMedicaItemOtheName;
    }

    public String getMedicaMeanin() {
        return MedicaMeanin;
    }

    public void setMedicaMeanin(String aMedicaMeanin) {
        MedicaMeanin = aMedicaMeanin;
    }

    public String getExcConten() {
        return ExcConten;
    }

    public void setExcConten(String aExcConten) {
        ExcConten = aExcConten;
    }

    public String getPriceUnit() {
        return PriceUnit;
    }

    public void setPriceUnit(String aPriceUnit) {
        PriceUnit = aPriceUnit;
    }

    public String getChargeUnit() {
        return ChargeUnit;
    }

    public void setChargeUnit(String aChargeUnit) {
        ChargeUnit = aChargeUnit;
    }

    public String getExplai() {
        return Explai;
    }

    public void setExplai(String aExplai) {
        Explai = aExplai;
    }

    public int getFrequencyFlag() {
        return FrequencyFlag;
    }

    public void setFrequencyFlag(int aFrequencyFlag) {
        FrequencyFlag = aFrequencyFlag;
    }

    public void setFrequencyFlag(String aFrequencyFlag) {
        if (aFrequencyFlag != null && !aFrequencyFlag.equals("")) {
            Integer tInteger = new Integer(aFrequencyFlag);
            int i = tInteger.intValue();
            FrequencyFlag = i;
        }
    }

    public String getICDCode() {
        return ICDCode;
    }

    public void setICDCode(String aICDCode) {
        ICDCode = aICDCode;
    }

    public String getOpsGrag() {
        return OpsGrag;
    }

    public void setOpsGrag(String aOpsGrag) {
        OpsGrag = aOpsGrag;
    }

    public String getAreaTpye() {
        return AreaTpye;
    }

    public void setAreaTpye(String aAreaTpye) {
        AreaTpye = aAreaTpye;
    }

    public String getStandardMeasureUnit() {
        return StandardMeasureUnit;
    }

    public void setStandardMeasureUnit(String aStandardMeasureUnit) {
        StandardMeasureUnit = aStandardMeasureUnit;
    }

    public double getBasicCharge() {
        return BasicCharge;
    }

    public void setBasicCharge(double aBasicCharge) {
        BasicCharge = Arith.round(aBasicCharge, 0);
    }

    public void setBasicCharge(String aBasicCharge) {
        if (aBasicCharge != null && !aBasicCharge.equals("")) {
            Double tDouble = new Double(aBasicCharge);
            double d = tDouble.doubleValue();
            BasicCharge = Arith.round(d, 0);
        }
    }

    public double getSanitationCost() {
        return SanitationCost;
    }

    public void setSanitationCost(double aSanitationCost) {
        SanitationCost = Arith.round(aSanitationCost, 0);
    }

    public void setSanitationCost(String aSanitationCost) {
        if (aSanitationCost != null && !aSanitationCost.equals("")) {
            Double tDouble = new Double(aSanitationCost);
            double d = tDouble.doubleValue();
            SanitationCost = Arith.round(d, 0);
        }
    }

    public double getFirstChargeStd() {
        return FirstChargeStd;
    }

    public void setFirstChargeStd(double aFirstChargeStd) {
        FirstChargeStd = Arith.round(aFirstChargeStd, 0);
    }

    public void setFirstChargeStd(String aFirstChargeStd) {
        if (aFirstChargeStd != null && !aFirstChargeStd.equals("")) {
            Double tDouble = new Double(aFirstChargeStd);
            double d = tDouble.doubleValue();
            FirstChargeStd = Arith.round(d, 0);
        }
    }

    public double getSecondChargeStd() {
        return SecondChargeStd;
    }

    public void setSecondChargeStd(double aSecondChargeStd) {
        SecondChargeStd = Arith.round(aSecondChargeStd, 0);
    }

    public void setSecondChargeStd(String aSecondChargeStd) {
        if (aSecondChargeStd != null && !aSecondChargeStd.equals("")) {
            Double tDouble = new Double(aSecondChargeStd);
            double d = tDouble.doubleValue();
            SecondChargeStd = Arith.round(d, 0);
        }
    }

    public double getThirdChargeStd() {
        return ThirdChargeStd;
    }

    public void setThirdChargeStd(double aThirdChargeStd) {
        ThirdChargeStd = Arith.round(aThirdChargeStd, 0);
    }

    public void setThirdChargeStd(String aThirdChargeStd) {
        if (aThirdChargeStd != null && !aThirdChargeStd.equals("")) {
            Double tDouble = new Double(aThirdChargeStd);
            double d = tDouble.doubleValue();
            ThirdChargeStd = Arith.round(d, 0);
        }
    }

    public String getClassCode() {
        return ClassCode;
    }

    public void setClassCode(String aClassCode) {
        ClassCode = aClassCode;
    }

    public String getInBelly() {
        return InBelly;
    }

    public void setInBelly(String aInBelly) {
        InBelly = aInBelly;
    }

    public String getNomalValue() {
        return NomalValue;
    }

    public void setNomalValue(String aNomalValue) {
        NomalValue = aNomalValue;
    }

    /**
     * 使用另外一个 LHCountrMedicaItemSchema 对象给 Schema 赋值
     * @param: aLHCountrMedicaItemSchema LHCountrMedicaItemSchema
     **/
    public void setSchema(LHCountrMedicaItemSchema aLHCountrMedicaItemSchema) {
        this.MedicaItemCode = aLHCountrMedicaItemSchema.getMedicaItemCode();
        this.MedicaItemName = aLHCountrMedicaItemSchema.getMedicaItemName();
        this.MedicaItemOtheName = aLHCountrMedicaItemSchema.
                                  getMedicaItemOtheName();
        this.MedicaMeanin = aLHCountrMedicaItemSchema.getMedicaMeanin();
        this.ExcConten = aLHCountrMedicaItemSchema.getExcConten();
        this.PriceUnit = aLHCountrMedicaItemSchema.getPriceUnit();
        this.ChargeUnit = aLHCountrMedicaItemSchema.getChargeUnit();
        this.Explai = aLHCountrMedicaItemSchema.getExplai();
        this.FrequencyFlag = aLHCountrMedicaItemSchema.getFrequencyFlag();
        this.ICDCode = aLHCountrMedicaItemSchema.getICDCode();
        this.OpsGrag = aLHCountrMedicaItemSchema.getOpsGrag();
        this.AreaTpye = aLHCountrMedicaItemSchema.getAreaTpye();
        this.StandardMeasureUnit = aLHCountrMedicaItemSchema.
                                   getStandardMeasureUnit();
        this.BasicCharge = aLHCountrMedicaItemSchema.getBasicCharge();
        this.SanitationCost = aLHCountrMedicaItemSchema.getSanitationCost();
        this.FirstChargeStd = aLHCountrMedicaItemSchema.getFirstChargeStd();
        this.SecondChargeStd = aLHCountrMedicaItemSchema.getSecondChargeStd();
        this.ThirdChargeStd = aLHCountrMedicaItemSchema.getThirdChargeStd();
        this.ClassCode = aLHCountrMedicaItemSchema.getClassCode();
        this.InBelly = aLHCountrMedicaItemSchema.getInBelly();
        this.NomalValue = aLHCountrMedicaItemSchema.getNomalValue();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("MedicaItemCode") == null) {
                this.MedicaItemCode = null;
            } else {
                this.MedicaItemCode = rs.getString("MedicaItemCode").trim();
            }

            if (rs.getString("MedicaItemName") == null) {
                this.MedicaItemName = null;
            } else {
                this.MedicaItemName = rs.getString("MedicaItemName").trim();
            }

            if (rs.getString("MedicaItemOtheName") == null) {
                this.MedicaItemOtheName = null;
            } else {
                this.MedicaItemOtheName = rs.getString("MedicaItemOtheName").
                                          trim();
            }

            if (rs.getString("MedicaMeanin") == null) {
                this.MedicaMeanin = null;
            } else {
                this.MedicaMeanin = rs.getString("MedicaMeanin").trim();
            }

            if (rs.getString("ExcConten") == null) {
                this.ExcConten = null;
            } else {
                this.ExcConten = rs.getString("ExcConten").trim();
            }

            if (rs.getString("PriceUnit") == null) {
                this.PriceUnit = null;
            } else {
                this.PriceUnit = rs.getString("PriceUnit").trim();
            }

            if (rs.getString("ChargeUnit") == null) {
                this.ChargeUnit = null;
            } else {
                this.ChargeUnit = rs.getString("ChargeUnit").trim();
            }

            if (rs.getString("Explai") == null) {
                this.Explai = null;
            } else {
                this.Explai = rs.getString("Explai").trim();
            }

            this.FrequencyFlag = rs.getInt("FrequencyFlag");
            if (rs.getString("ICDCode") == null) {
                this.ICDCode = null;
            } else {
                this.ICDCode = rs.getString("ICDCode").trim();
            }

            if (rs.getString("OpsGrag") == null) {
                this.OpsGrag = null;
            } else {
                this.OpsGrag = rs.getString("OpsGrag").trim();
            }

            if (rs.getString("AreaTpye") == null) {
                this.AreaTpye = null;
            } else {
                this.AreaTpye = rs.getString("AreaTpye").trim();
            }

            if (rs.getString("StandardMeasureUnit") == null) {
                this.StandardMeasureUnit = null;
            } else {
                this.StandardMeasureUnit = rs.getString("StandardMeasureUnit").
                                           trim();
            }

            this.BasicCharge = rs.getDouble("BasicCharge");
            this.SanitationCost = rs.getDouble("SanitationCost");
            this.FirstChargeStd = rs.getDouble("FirstChargeStd");
            this.SecondChargeStd = rs.getDouble("SecondChargeStd");
            this.ThirdChargeStd = rs.getDouble("ThirdChargeStd");
            if (rs.getString("ClassCode") == null) {
                this.ClassCode = null;
            } else {
                this.ClassCode = rs.getString("ClassCode").trim();
            }

            if (rs.getString("InBelly") == null) {
                this.InBelly = null;
            } else {
                this.InBelly = rs.getString("InBelly").trim();
            }

            if (rs.getString("NomalValue") == null) {
                this.NomalValue = null;
            } else {
                this.NomalValue = rs.getString("NomalValue").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHCountrMedicaItem表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCountrMedicaItemSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHCountrMedicaItemSchema getSchema() {
        LHCountrMedicaItemSchema aLHCountrMedicaItemSchema = new
                LHCountrMedicaItemSchema();
        aLHCountrMedicaItemSchema.setSchema(this);
        return aLHCountrMedicaItemSchema;
    }

    public LHCountrMedicaItemDB getDB() {
        LHCountrMedicaItemDB aDBOper = new LHCountrMedicaItemDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHCountrMedicaItem描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(MedicaItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MedicaItemName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MedicaItemOtheName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MedicaMeanin));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExcConten));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PriceUnit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ChargeUnit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Explai));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FrequencyFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ICDCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OpsGrag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AreaTpye));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandardMeasureUnit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BasicCharge));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SanitationCost));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FirstChargeStd));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SecondChargeStd));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ThirdChargeStd));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ClassCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InBelly));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NomalValue));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHCountrMedicaItem>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            MedicaItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                            SysConst.PACKAGESPILTER);
            MedicaItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                            SysConst.PACKAGESPILTER);
            MedicaItemOtheName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                3, SysConst.PACKAGESPILTER);
            MedicaMeanin = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            ExcConten = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            PriceUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            ChargeUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            Explai = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            FrequencyFlag = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            ICDCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            OpsGrag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                     SysConst.PACKAGESPILTER);
            AreaTpye = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            StandardMeasureUnit = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER);
            BasicCharge = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            SanitationCost = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 15, SysConst.PACKAGESPILTER))).doubleValue();
            FirstChargeStd = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 16, SysConst.PACKAGESPILTER))).doubleValue();
            SecondChargeStd = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).doubleValue();
            ThirdChargeStd = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            ClassCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                       SysConst.PACKAGESPILTER);
            InBelly = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                     SysConst.PACKAGESPILTER);
            NomalValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCountrMedicaItemSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("MedicaItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaItemCode));
        }
        if (FCode.equals("MedicaItemName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaItemName));
        }
        if (FCode.equals("MedicaItemOtheName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaItemOtheName));
        }
        if (FCode.equals("MedicaMeanin")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaMeanin));
        }
        if (FCode.equals("ExcConten")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExcConten));
        }
        if (FCode.equals("PriceUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PriceUnit));
        }
        if (FCode.equals("ChargeUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeUnit));
        }
        if (FCode.equals("Explai")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Explai));
        }
        if (FCode.equals("FrequencyFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FrequencyFlag));
        }
        if (FCode.equals("ICDCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ICDCode));
        }
        if (FCode.equals("OpsGrag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OpsGrag));
        }
        if (FCode.equals("AreaTpye")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaTpye));
        }
        if (FCode.equals("StandardMeasureUnit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandardMeasureUnit));
        }
        if (FCode.equals("BasicCharge")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BasicCharge));
        }
        if (FCode.equals("SanitationCost")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SanitationCost));
        }
        if (FCode.equals("FirstChargeStd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstChargeStd));
        }
        if (FCode.equals("SecondChargeStd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SecondChargeStd));
        }
        if (FCode.equals("ThirdChargeStd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ThirdChargeStd));
        }
        if (FCode.equals("ClassCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClassCode));
        }
        if (FCode.equals("InBelly")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InBelly));
        }
        if (FCode.equals("NomalValue")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NomalValue));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(MedicaItemCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(MedicaItemName);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(MedicaItemOtheName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(MedicaMeanin);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ExcConten);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(PriceUnit);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ChargeUnit);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Explai);
            break;
        case 8:
            strFieldValue = String.valueOf(FrequencyFlag);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ICDCode);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(OpsGrag);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(AreaTpye);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(StandardMeasureUnit);
            break;
        case 13:
            strFieldValue = String.valueOf(BasicCharge);
            break;
        case 14:
            strFieldValue = String.valueOf(SanitationCost);
            break;
        case 15:
            strFieldValue = String.valueOf(FirstChargeStd);
            break;
        case 16:
            strFieldValue = String.valueOf(SecondChargeStd);
            break;
        case 17:
            strFieldValue = String.valueOf(ThirdChargeStd);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(ClassCode);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(InBelly);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(NomalValue);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("MedicaItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                MedicaItemCode = FValue.trim();
            } else {
                MedicaItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("MedicaItemName")) {
            if (FValue != null && !FValue.equals("")) {
                MedicaItemName = FValue.trim();
            } else {
                MedicaItemName = null;
            }
        }
        if (FCode.equalsIgnoreCase("MedicaItemOtheName")) {
            if (FValue != null && !FValue.equals("")) {
                MedicaItemOtheName = FValue.trim();
            } else {
                MedicaItemOtheName = null;
            }
        }
        if (FCode.equalsIgnoreCase("MedicaMeanin")) {
            if (FValue != null && !FValue.equals("")) {
                MedicaMeanin = FValue.trim();
            } else {
                MedicaMeanin = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExcConten")) {
            if (FValue != null && !FValue.equals("")) {
                ExcConten = FValue.trim();
            } else {
                ExcConten = null;
            }
        }
        if (FCode.equalsIgnoreCase("PriceUnit")) {
            if (FValue != null && !FValue.equals("")) {
                PriceUnit = FValue.trim();
            } else {
                PriceUnit = null;
            }
        }
        if (FCode.equalsIgnoreCase("ChargeUnit")) {
            if (FValue != null && !FValue.equals("")) {
                ChargeUnit = FValue.trim();
            } else {
                ChargeUnit = null;
            }
        }
        if (FCode.equalsIgnoreCase("Explai")) {
            if (FValue != null && !FValue.equals("")) {
                Explai = FValue.trim();
            } else {
                Explai = null;
            }
        }
        if (FCode.equalsIgnoreCase("FrequencyFlag")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                FrequencyFlag = i;
            }
        }
        if (FCode.equalsIgnoreCase("ICDCode")) {
            if (FValue != null && !FValue.equals("")) {
                ICDCode = FValue.trim();
            } else {
                ICDCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("OpsGrag")) {
            if (FValue != null && !FValue.equals("")) {
                OpsGrag = FValue.trim();
            } else {
                OpsGrag = null;
            }
        }
        if (FCode.equalsIgnoreCase("AreaTpye")) {
            if (FValue != null && !FValue.equals("")) {
                AreaTpye = FValue.trim();
            } else {
                AreaTpye = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandardMeasureUnit")) {
            if (FValue != null && !FValue.equals("")) {
                StandardMeasureUnit = FValue.trim();
            } else {
                StandardMeasureUnit = null;
            }
        }
        if (FCode.equalsIgnoreCase("BasicCharge")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BasicCharge = d;
            }
        }
        if (FCode.equalsIgnoreCase("SanitationCost")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SanitationCost = d;
            }
        }
        if (FCode.equalsIgnoreCase("FirstChargeStd")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FirstChargeStd = d;
            }
        }
        if (FCode.equalsIgnoreCase("SecondChargeStd")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SecondChargeStd = d;
            }
        }
        if (FCode.equalsIgnoreCase("ThirdChargeStd")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ThirdChargeStd = d;
            }
        }
        if (FCode.equalsIgnoreCase("ClassCode")) {
            if (FValue != null && !FValue.equals("")) {
                ClassCode = FValue.trim();
            } else {
                ClassCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("InBelly")) {
            if (FValue != null && !FValue.equals("")) {
                InBelly = FValue.trim();
            } else {
                InBelly = null;
            }
        }
        if (FCode.equalsIgnoreCase("NomalValue")) {
            if (FValue != null && !FValue.equals("")) {
                NomalValue = FValue.trim();
            } else {
                NomalValue = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHCountrMedicaItemSchema other = (LHCountrMedicaItemSchema) otherObject;
        return
                MedicaItemCode.equals(other.getMedicaItemCode())
                && MedicaItemName.equals(other.getMedicaItemName())
                && MedicaItemOtheName.equals(other.getMedicaItemOtheName())
                && MedicaMeanin.equals(other.getMedicaMeanin())
                && ExcConten.equals(other.getExcConten())
                && PriceUnit.equals(other.getPriceUnit())
                && ChargeUnit.equals(other.getChargeUnit())
                && Explai.equals(other.getExplai())
                && FrequencyFlag == other.getFrequencyFlag()
                && ICDCode.equals(other.getICDCode())
                && OpsGrag.equals(other.getOpsGrag())
                && AreaTpye.equals(other.getAreaTpye())
                && StandardMeasureUnit.equals(other.getStandardMeasureUnit())
                && BasicCharge == other.getBasicCharge()
                && SanitationCost == other.getSanitationCost()
                && FirstChargeStd == other.getFirstChargeStd()
                && SecondChargeStd == other.getSecondChargeStd()
                && ThirdChargeStd == other.getThirdChargeStd()
                && ClassCode.equals(other.getClassCode())
                && InBelly.equals(other.getInBelly())
                && NomalValue.equals(other.getNomalValue());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("MedicaItemCode")) {
            return 0;
        }
        if (strFieldName.equals("MedicaItemName")) {
            return 1;
        }
        if (strFieldName.equals("MedicaItemOtheName")) {
            return 2;
        }
        if (strFieldName.equals("MedicaMeanin")) {
            return 3;
        }
        if (strFieldName.equals("ExcConten")) {
            return 4;
        }
        if (strFieldName.equals("PriceUnit")) {
            return 5;
        }
        if (strFieldName.equals("ChargeUnit")) {
            return 6;
        }
        if (strFieldName.equals("Explai")) {
            return 7;
        }
        if (strFieldName.equals("FrequencyFlag")) {
            return 8;
        }
        if (strFieldName.equals("ICDCode")) {
            return 9;
        }
        if (strFieldName.equals("OpsGrag")) {
            return 10;
        }
        if (strFieldName.equals("AreaTpye")) {
            return 11;
        }
        if (strFieldName.equals("StandardMeasureUnit")) {
            return 12;
        }
        if (strFieldName.equals("BasicCharge")) {
            return 13;
        }
        if (strFieldName.equals("SanitationCost")) {
            return 14;
        }
        if (strFieldName.equals("FirstChargeStd")) {
            return 15;
        }
        if (strFieldName.equals("SecondChargeStd")) {
            return 16;
        }
        if (strFieldName.equals("ThirdChargeStd")) {
            return 17;
        }
        if (strFieldName.equals("ClassCode")) {
            return 18;
        }
        if (strFieldName.equals("InBelly")) {
            return 19;
        }
        if (strFieldName.equals("NomalValue")) {
            return 20;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "MedicaItemCode";
            break;
        case 1:
            strFieldName = "MedicaItemName";
            break;
        case 2:
            strFieldName = "MedicaItemOtheName";
            break;
        case 3:
            strFieldName = "MedicaMeanin";
            break;
        case 4:
            strFieldName = "ExcConten";
            break;
        case 5:
            strFieldName = "PriceUnit";
            break;
        case 6:
            strFieldName = "ChargeUnit";
            break;
        case 7:
            strFieldName = "Explai";
            break;
        case 8:
            strFieldName = "FrequencyFlag";
            break;
        case 9:
            strFieldName = "ICDCode";
            break;
        case 10:
            strFieldName = "OpsGrag";
            break;
        case 11:
            strFieldName = "AreaTpye";
            break;
        case 12:
            strFieldName = "StandardMeasureUnit";
            break;
        case 13:
            strFieldName = "BasicCharge";
            break;
        case 14:
            strFieldName = "SanitationCost";
            break;
        case 15:
            strFieldName = "FirstChargeStd";
            break;
        case 16:
            strFieldName = "SecondChargeStd";
            break;
        case 17:
            strFieldName = "ThirdChargeStd";
            break;
        case 18:
            strFieldName = "ClassCode";
            break;
        case 19:
            strFieldName = "InBelly";
            break;
        case 20:
            strFieldName = "NomalValue";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("MedicaItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MedicaItemName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MedicaItemOtheName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MedicaMeanin")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExcConten")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PriceUnit")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ChargeUnit")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Explai")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FrequencyFlag")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ICDCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OpsGrag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaTpye")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandardMeasureUnit")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BasicCharge")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SanitationCost")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FirstChargeStd")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SecondChargeStd")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ThirdChargeStd")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ClassCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InBelly")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NomalValue")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_INT;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 14:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 15:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 16:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 17:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
