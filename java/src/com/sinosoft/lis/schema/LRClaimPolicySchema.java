/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRClaimPolicyDB;

/*
 * <p>ClassName: LRClaimPolicySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-01-23
 */
public class LRClaimPolicySchema implements Schema, Cloneable {
    // @Field
    /** 赔案号 */
    private String ClmNo;
    /** 保单号 */
    private String PolNo;
    /** 再保险公司 */
    private String ReComCode;
    /** 再保项目 */
    private String ReinsureItem;
    /** 险种计算分类 */
    private String RiskCalSort;
    /** 立案号 */
    private String RgtNo;
    /** 分案号 */
    private String CaseNo;
    /** 总单/合同号 */
    private String ContNo;
    /** 团体合同号 */
    private String GrpContNo;
    /** 集体保单号 */
    private String GrpPolNo;
    /** 给付责任类型 */
    private String GetDutyKind;
    /** 险类代码 */
    private String KindCode;
    /** 险种代码 */
    private String RiskCode;
    /** 险种版本号 */
    private String RiskVer;
    /** 保单管理机构 */
    private String PolMngCom;
    /** 销售渠道 */
    private String SaleChnl;
    /** 代理人代码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 被保人客户号 */
    private String InsuredNo;
    /** 被保人名称 */
    private String InsuredName;
    /** 投保人客户号 */
    private String AppntNo;
    /** 投保人名称 */
    private String AppntName;
    /** 保单生效日期 */
    private Date CValiDate;
    /** 出险日期 */
    private Date AccidentDate;
    /** 保单状态 */
    private String PolState;
    /** 赔案状态 */
    private String ClmState;
    /** 核算赔付金额 */
    private double StandPay;
    /** 核赔赔付金额 */
    private double RealPay;
    /** 理赔员 */
    private String ClmUWer;
    /** 结案日期 */
    private Date EndCaseDate;
    /** 分保开始日期 */
    private Date CessStart;
    /** 分保结束日期 */
    private Date CessEnd;
    /** 分保比例 */
    private double CessionRate;
    /** 分保额 */
    private double CessionAmount;
    /** 摊回赔付金额 */
    private double ReturnPay;
    /** 保单性质标志 */
    private String PolType;
    /** 保单失效日期 */
    private Date EndDate;
    /** 帐目期间 */
    private String AccountPeriod;
    /** 索赔申请日期 */
    private Date RgtDate;
    /** 赔付日期 */
    private Date ConfDate;
    /** 索赔额 */
    private double ClaimMoney;
    /** 核保决定 */
    private String UWFlag;
    /** 出险描述 */
    private String AccDesc;
    /** 自留额 */
    private double RetainAmnt;
    /** 再保险额 */
    private double CessAmnt;
    /** 分出人意见 */
    private String CessPersonSug;
    /** 出院日期 */
    private Date LeaveHospDate;
    /** 诊断 */
    private String Diagnoses;
    /** 理赔分保比例 */
    private double ClaimCessRate;
    /** 理赔摊回 */
    private double ClaimBackFee;
    /** 备注 */
    private String Remark;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String MngCom;

    public static final int FIELDNUM = 57; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LRClaimPolicySchema() {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "ClmNo";
        pk[1] = "PolNo";
        pk[2] = "ReComCode";
        pk[3] = "ReinsureItem";
        pk[4] = "RgtNo";
        pk[5] = "CaseNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LRClaimPolicySchema cloned = (LRClaimPolicySchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getClmNo() {
        return ClmNo;
    }

    public void setClmNo(String aClmNo) {
        ClmNo = aClmNo;
    }

    public String getPolNo() {
        return PolNo;
    }

    public void setPolNo(String aPolNo) {
        PolNo = aPolNo;
    }

    public String getReComCode() {
        return ReComCode;
    }

    public void setReComCode(String aReComCode) {
        ReComCode = aReComCode;
    }

    public String getReinsureItem() {
        return ReinsureItem;
    }

    public void setReinsureItem(String aReinsureItem) {
        ReinsureItem = aReinsureItem;
    }

    public String getRiskCalSort() {
        return RiskCalSort;
    }

    public void setRiskCalSort(String aRiskCalSort) {
        RiskCalSort = aRiskCalSort;
    }

    public String getRgtNo() {
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo) {
        RgtNo = aRgtNo;
    }

    public String getCaseNo() {
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo) {
        CaseNo = aCaseNo;
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getGrpPolNo() {
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }

    public String getGetDutyKind() {
        return GetDutyKind;
    }

    public void setGetDutyKind(String aGetDutyKind) {
        GetDutyKind = aGetDutyKind;
    }

    public String getKindCode() {
        return KindCode;
    }

    public void setKindCode(String aKindCode) {
        KindCode = aKindCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getRiskVer() {
        return RiskVer;
    }

    public void setRiskVer(String aRiskVer) {
        RiskVer = aRiskVer;
    }

    public String getPolMngCom() {
        return PolMngCom;
    }

    public void setPolMngCom(String aPolMngCom) {
        PolMngCom = aPolMngCom;
    }

    public String getSaleChnl() {
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup() {
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }

    public String getInsuredNo() {
        return InsuredNo;
    }

    public void setInsuredNo(String aInsuredNo) {
        InsuredNo = aInsuredNo;
    }

    public String getInsuredName() {
        return InsuredName;
    }

    public void setInsuredName(String aInsuredName) {
        InsuredName = aInsuredName;
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }

    public String getCValiDate() {
        if (CValiDate != null) {
            return fDate.getString(CValiDate);
        } else {
            return null;
        }
    }

    public void setCValiDate(Date aCValiDate) {
        CValiDate = aCValiDate;
    }

    public void setCValiDate(String aCValiDate) {
        if (aCValiDate != null && !aCValiDate.equals("")) {
            CValiDate = fDate.getDate(aCValiDate);
        } else {
            CValiDate = null;
        }
    }

    public String getAccidentDate() {
        if (AccidentDate != null) {
            return fDate.getString(AccidentDate);
        } else {
            return null;
        }
    }

    public void setAccidentDate(Date aAccidentDate) {
        AccidentDate = aAccidentDate;
    }

    public void setAccidentDate(String aAccidentDate) {
        if (aAccidentDate != null && !aAccidentDate.equals("")) {
            AccidentDate = fDate.getDate(aAccidentDate);
        } else {
            AccidentDate = null;
        }
    }

    public String getPolState() {
        return PolState;
    }

    public void setPolState(String aPolState) {
        PolState = aPolState;
    }

    public String getClmState() {
        return ClmState;
    }

    public void setClmState(String aClmState) {
        ClmState = aClmState;
    }

    public double getStandPay() {
        return StandPay;
    }

    public void setStandPay(double aStandPay) {
        StandPay = Arith.round(aStandPay, 2);
    }

    public void setStandPay(String aStandPay) {
        if (aStandPay != null && !aStandPay.equals("")) {
            Double tDouble = new Double(aStandPay);
            double d = tDouble.doubleValue();
            StandPay = Arith.round(d, 2);
        }
    }

    public double getRealPay() {
        return RealPay;
    }

    public void setRealPay(double aRealPay) {
        RealPay = Arith.round(aRealPay, 2);
    }

    public void setRealPay(String aRealPay) {
        if (aRealPay != null && !aRealPay.equals("")) {
            Double tDouble = new Double(aRealPay);
            double d = tDouble.doubleValue();
            RealPay = Arith.round(d, 2);
        }
    }

    public String getClmUWer() {
        return ClmUWer;
    }

    public void setClmUWer(String aClmUWer) {
        ClmUWer = aClmUWer;
    }

    public String getEndCaseDate() {
        if (EndCaseDate != null) {
            return fDate.getString(EndCaseDate);
        } else {
            return null;
        }
    }

    public void setEndCaseDate(Date aEndCaseDate) {
        EndCaseDate = aEndCaseDate;
    }

    public void setEndCaseDate(String aEndCaseDate) {
        if (aEndCaseDate != null && !aEndCaseDate.equals("")) {
            EndCaseDate = fDate.getDate(aEndCaseDate);
        } else {
            EndCaseDate = null;
        }
    }

    public String getCessStart() {
        if (CessStart != null) {
            return fDate.getString(CessStart);
        } else {
            return null;
        }
    }

    public void setCessStart(Date aCessStart) {
        CessStart = aCessStart;
    }

    public void setCessStart(String aCessStart) {
        if (aCessStart != null && !aCessStart.equals("")) {
            CessStart = fDate.getDate(aCessStart);
        } else {
            CessStart = null;
        }
    }

    public String getCessEnd() {
        if (CessEnd != null) {
            return fDate.getString(CessEnd);
        } else {
            return null;
        }
    }

    public void setCessEnd(Date aCessEnd) {
        CessEnd = aCessEnd;
    }

    public void setCessEnd(String aCessEnd) {
        if (aCessEnd != null && !aCessEnd.equals("")) {
            CessEnd = fDate.getDate(aCessEnd);
        } else {
            CessEnd = null;
        }
    }

    public double getCessionRate() {
        return CessionRate;
    }

    public void setCessionRate(double aCessionRate) {
        CessionRate = Arith.round(aCessionRate, 0);
    }

    public void setCessionRate(String aCessionRate) {
        if (aCessionRate != null && !aCessionRate.equals("")) {
            Double tDouble = new Double(aCessionRate);
            double d = tDouble.doubleValue();
            CessionRate = Arith.round(d, 0);
        }
    }

    public double getCessionAmount() {
        return CessionAmount;
    }

    public void setCessionAmount(double aCessionAmount) {
        CessionAmount = Arith.round(aCessionAmount, 2);
    }

    public void setCessionAmount(String aCessionAmount) {
        if (aCessionAmount != null && !aCessionAmount.equals("")) {
            Double tDouble = new Double(aCessionAmount);
            double d = tDouble.doubleValue();
            CessionAmount = Arith.round(d, 2);
        }
    }

    public double getReturnPay() {
        return ReturnPay;
    }

    public void setReturnPay(double aReturnPay) {
        ReturnPay = Arith.round(aReturnPay, 2);
    }

    public void setReturnPay(String aReturnPay) {
        if (aReturnPay != null && !aReturnPay.equals("")) {
            Double tDouble = new Double(aReturnPay);
            double d = tDouble.doubleValue();
            ReturnPay = Arith.round(d, 2);
        }
    }

    public String getPolType() {
        return PolType;
    }

    public void setPolType(String aPolType) {
        PolType = aPolType;
    }

    public String getEndDate() {
        if (EndDate != null) {
            return fDate.getString(EndDate);
        } else {
            return null;
        }
    }

    public void setEndDate(Date aEndDate) {
        EndDate = aEndDate;
    }

    public void setEndDate(String aEndDate) {
        if (aEndDate != null && !aEndDate.equals("")) {
            EndDate = fDate.getDate(aEndDate);
        } else {
            EndDate = null;
        }
    }

    public String getAccountPeriod() {
        return AccountPeriod;
    }

    public void setAccountPeriod(String aAccountPeriod) {
        AccountPeriod = aAccountPeriod;
    }

    public String getRgtDate() {
        if (RgtDate != null) {
            return fDate.getString(RgtDate);
        } else {
            return null;
        }
    }

    public void setRgtDate(Date aRgtDate) {
        RgtDate = aRgtDate;
    }

    public void setRgtDate(String aRgtDate) {
        if (aRgtDate != null && !aRgtDate.equals("")) {
            RgtDate = fDate.getDate(aRgtDate);
        } else {
            RgtDate = null;
        }
    }

    public String getConfDate() {
        if (ConfDate != null) {
            return fDate.getString(ConfDate);
        } else {
            return null;
        }
    }

    public void setConfDate(Date aConfDate) {
        ConfDate = aConfDate;
    }

    public void setConfDate(String aConfDate) {
        if (aConfDate != null && !aConfDate.equals("")) {
            ConfDate = fDate.getDate(aConfDate);
        } else {
            ConfDate = null;
        }
    }

    public double getClaimMoney() {
        return ClaimMoney;
    }

    public void setClaimMoney(double aClaimMoney) {
        ClaimMoney = Arith.round(aClaimMoney, 2);
    }

    public void setClaimMoney(String aClaimMoney) {
        if (aClaimMoney != null && !aClaimMoney.equals("")) {
            Double tDouble = new Double(aClaimMoney);
            double d = tDouble.doubleValue();
            ClaimMoney = Arith.round(d, 2);
        }
    }

    public String getUWFlag() {
        return UWFlag;
    }

    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }

    public String getAccDesc() {
        return AccDesc;
    }

    public void setAccDesc(String aAccDesc) {
        AccDesc = aAccDesc;
    }

    public double getRetainAmnt() {
        return RetainAmnt;
    }

    public void setRetainAmnt(double aRetainAmnt) {
        RetainAmnt = Arith.round(aRetainAmnt, 2);
    }

    public void setRetainAmnt(String aRetainAmnt) {
        if (aRetainAmnt != null && !aRetainAmnt.equals("")) {
            Double tDouble = new Double(aRetainAmnt);
            double d = tDouble.doubleValue();
            RetainAmnt = Arith.round(d, 2);
        }
    }

    public double getCessAmnt() {
        return CessAmnt;
    }

    public void setCessAmnt(double aCessAmnt) {
        CessAmnt = Arith.round(aCessAmnt, 2);
    }

    public void setCessAmnt(String aCessAmnt) {
        if (aCessAmnt != null && !aCessAmnt.equals("")) {
            Double tDouble = new Double(aCessAmnt);
            double d = tDouble.doubleValue();
            CessAmnt = Arith.round(d, 2);
        }
    }

    public String getCessPersonSug() {
        return CessPersonSug;
    }

    public void setCessPersonSug(String aCessPersonSug) {
        CessPersonSug = aCessPersonSug;
    }

    public String getLeaveHospDate() {
        if (LeaveHospDate != null) {
            return fDate.getString(LeaveHospDate);
        } else {
            return null;
        }
    }

    public void setLeaveHospDate(Date aLeaveHospDate) {
        LeaveHospDate = aLeaveHospDate;
    }

    public void setLeaveHospDate(String aLeaveHospDate) {
        if (aLeaveHospDate != null && !aLeaveHospDate.equals("")) {
            LeaveHospDate = fDate.getDate(aLeaveHospDate);
        } else {
            LeaveHospDate = null;
        }
    }

    public String getDiagnoses() {
        return Diagnoses;
    }

    public void setDiagnoses(String aDiagnoses) {
        Diagnoses = aDiagnoses;
    }

    public double getClaimCessRate() {
        return ClaimCessRate;
    }

    public void setClaimCessRate(double aClaimCessRate) {
        ClaimCessRate = Arith.round(aClaimCessRate, 2);
    }

    public void setClaimCessRate(String aClaimCessRate) {
        if (aClaimCessRate != null && !aClaimCessRate.equals("")) {
            Double tDouble = new Double(aClaimCessRate);
            double d = tDouble.doubleValue();
            ClaimCessRate = Arith.round(d, 2);
        }
    }

    public double getClaimBackFee() {
        return ClaimBackFee;
    }

    public void setClaimBackFee(double aClaimBackFee) {
        ClaimBackFee = Arith.round(aClaimBackFee, 2);
    }

    public void setClaimBackFee(String aClaimBackFee) {
        if (aClaimBackFee != null && !aClaimBackFee.equals("")) {
            Double tDouble = new Double(aClaimBackFee);
            double d = tDouble.doubleValue();
            ClaimBackFee = Arith.round(d, 2);
        }
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMngCom() {
        return MngCom;
    }

    public void setMngCom(String aMngCom) {
        MngCom = aMngCom;
    }

    /**
     * 使用另外一个 LRClaimPolicySchema 对象给 Schema 赋值
     * @param: aLRClaimPolicySchema LRClaimPolicySchema
     **/
    public void setSchema(LRClaimPolicySchema aLRClaimPolicySchema) {
        this.ClmNo = aLRClaimPolicySchema.getClmNo();
        this.PolNo = aLRClaimPolicySchema.getPolNo();
        this.ReComCode = aLRClaimPolicySchema.getReComCode();
        this.ReinsureItem = aLRClaimPolicySchema.getReinsureItem();
        this.RiskCalSort = aLRClaimPolicySchema.getRiskCalSort();
        this.RgtNo = aLRClaimPolicySchema.getRgtNo();
        this.CaseNo = aLRClaimPolicySchema.getCaseNo();
        this.ContNo = aLRClaimPolicySchema.getContNo();
        this.GrpContNo = aLRClaimPolicySchema.getGrpContNo();
        this.GrpPolNo = aLRClaimPolicySchema.getGrpPolNo();
        this.GetDutyKind = aLRClaimPolicySchema.getGetDutyKind();
        this.KindCode = aLRClaimPolicySchema.getKindCode();
        this.RiskCode = aLRClaimPolicySchema.getRiskCode();
        this.RiskVer = aLRClaimPolicySchema.getRiskVer();
        this.PolMngCom = aLRClaimPolicySchema.getPolMngCom();
        this.SaleChnl = aLRClaimPolicySchema.getSaleChnl();
        this.AgentCode = aLRClaimPolicySchema.getAgentCode();
        this.AgentGroup = aLRClaimPolicySchema.getAgentGroup();
        this.InsuredNo = aLRClaimPolicySchema.getInsuredNo();
        this.InsuredName = aLRClaimPolicySchema.getInsuredName();
        this.AppntNo = aLRClaimPolicySchema.getAppntNo();
        this.AppntName = aLRClaimPolicySchema.getAppntName();
        this.CValiDate = fDate.getDate(aLRClaimPolicySchema.getCValiDate());
        this.AccidentDate = fDate.getDate(aLRClaimPolicySchema.getAccidentDate());
        this.PolState = aLRClaimPolicySchema.getPolState();
        this.ClmState = aLRClaimPolicySchema.getClmState();
        this.StandPay = aLRClaimPolicySchema.getStandPay();
        this.RealPay = aLRClaimPolicySchema.getRealPay();
        this.ClmUWer = aLRClaimPolicySchema.getClmUWer();
        this.EndCaseDate = fDate.getDate(aLRClaimPolicySchema.getEndCaseDate());
        this.CessStart = fDate.getDate(aLRClaimPolicySchema.getCessStart());
        this.CessEnd = fDate.getDate(aLRClaimPolicySchema.getCessEnd());
        this.CessionRate = aLRClaimPolicySchema.getCessionRate();
        this.CessionAmount = aLRClaimPolicySchema.getCessionAmount();
        this.ReturnPay = aLRClaimPolicySchema.getReturnPay();
        this.PolType = aLRClaimPolicySchema.getPolType();
        this.EndDate = fDate.getDate(aLRClaimPolicySchema.getEndDate());
        this.AccountPeriod = aLRClaimPolicySchema.getAccountPeriod();
        this.RgtDate = fDate.getDate(aLRClaimPolicySchema.getRgtDate());
        this.ConfDate = fDate.getDate(aLRClaimPolicySchema.getConfDate());
        this.ClaimMoney = aLRClaimPolicySchema.getClaimMoney();
        this.UWFlag = aLRClaimPolicySchema.getUWFlag();
        this.AccDesc = aLRClaimPolicySchema.getAccDesc();
        this.RetainAmnt = aLRClaimPolicySchema.getRetainAmnt();
        this.CessAmnt = aLRClaimPolicySchema.getCessAmnt();
        this.CessPersonSug = aLRClaimPolicySchema.getCessPersonSug();
        this.LeaveHospDate = fDate.getDate(aLRClaimPolicySchema.
                                           getLeaveHospDate());
        this.Diagnoses = aLRClaimPolicySchema.getDiagnoses();
        this.ClaimCessRate = aLRClaimPolicySchema.getClaimCessRate();
        this.ClaimBackFee = aLRClaimPolicySchema.getClaimBackFee();
        this.Remark = aLRClaimPolicySchema.getRemark();
        this.MakeDate = fDate.getDate(aLRClaimPolicySchema.getMakeDate());
        this.MakeTime = aLRClaimPolicySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLRClaimPolicySchema.getModifyDate());
        this.ModifyTime = aLRClaimPolicySchema.getModifyTime();
        this.Operator = aLRClaimPolicySchema.getOperator();
        this.MngCom = aLRClaimPolicySchema.getMngCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ClmNo") == null) {
                this.ClmNo = null;
            } else {
                this.ClmNo = rs.getString("ClmNo").trim();
            }

            if (rs.getString("PolNo") == null) {
                this.PolNo = null;
            } else {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("ReComCode") == null) {
                this.ReComCode = null;
            } else {
                this.ReComCode = rs.getString("ReComCode").trim();
            }

            if (rs.getString("ReinsureItem") == null) {
                this.ReinsureItem = null;
            } else {
                this.ReinsureItem = rs.getString("ReinsureItem").trim();
            }

            if (rs.getString("RiskCalSort") == null) {
                this.RiskCalSort = null;
            } else {
                this.RiskCalSort = rs.getString("RiskCalSort").trim();
            }

            if (rs.getString("RgtNo") == null) {
                this.RgtNo = null;
            } else {
                this.RgtNo = rs.getString("RgtNo").trim();
            }

            if (rs.getString("CaseNo") == null) {
                this.CaseNo = null;
            } else {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpPolNo") == null) {
                this.GrpPolNo = null;
            } else {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("GetDutyKind") == null) {
                this.GetDutyKind = null;
            } else {
                this.GetDutyKind = rs.getString("GetDutyKind").trim();
            }

            if (rs.getString("KindCode") == null) {
                this.KindCode = null;
            } else {
                this.KindCode = rs.getString("KindCode").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVer") == null) {
                this.RiskVer = null;
            } else {
                this.RiskVer = rs.getString("RiskVer").trim();
            }

            if (rs.getString("PolMngCom") == null) {
                this.PolMngCom = null;
            } else {
                this.PolMngCom = rs.getString("PolMngCom").trim();
            }

            if (rs.getString("SaleChnl") == null) {
                this.SaleChnl = null;
            } else {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null) {
                this.AgentGroup = null;
            } else {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("InsuredNo") == null) {
                this.InsuredNo = null;
            } else {
                this.InsuredNo = rs.getString("InsuredNo").trim();
            }

            if (rs.getString("InsuredName") == null) {
                this.InsuredName = null;
            } else {
                this.InsuredName = rs.getString("InsuredName").trim();
            }

            if (rs.getString("AppntNo") == null) {
                this.AppntNo = null;
            } else {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("AppntName") == null) {
                this.AppntName = null;
            } else {
                this.AppntName = rs.getString("AppntName").trim();
            }

            this.CValiDate = rs.getDate("CValiDate");
            this.AccidentDate = rs.getDate("AccidentDate");
            if (rs.getString("PolState") == null) {
                this.PolState = null;
            } else {
                this.PolState = rs.getString("PolState").trim();
            }

            if (rs.getString("ClmState") == null) {
                this.ClmState = null;
            } else {
                this.ClmState = rs.getString("ClmState").trim();
            }

            this.StandPay = rs.getDouble("StandPay");
            this.RealPay = rs.getDouble("RealPay");
            if (rs.getString("ClmUWer") == null) {
                this.ClmUWer = null;
            } else {
                this.ClmUWer = rs.getString("ClmUWer").trim();
            }

            this.EndCaseDate = rs.getDate("EndCaseDate");
            this.CessStart = rs.getDate("CessStart");
            this.CessEnd = rs.getDate("CessEnd");
            this.CessionRate = rs.getDouble("CessionRate");
            this.CessionAmount = rs.getDouble("CessionAmount");
            this.ReturnPay = rs.getDouble("ReturnPay");
            if (rs.getString("PolType") == null) {
                this.PolType = null;
            } else {
                this.PolType = rs.getString("PolType").trim();
            }

            this.EndDate = rs.getDate("EndDate");
            if (rs.getString("AccountPeriod") == null) {
                this.AccountPeriod = null;
            } else {
                this.AccountPeriod = rs.getString("AccountPeriod").trim();
            }

            this.RgtDate = rs.getDate("RgtDate");
            this.ConfDate = rs.getDate("ConfDate");
            this.ClaimMoney = rs.getDouble("ClaimMoney");
            if (rs.getString("UWFlag") == null) {
                this.UWFlag = null;
            } else {
                this.UWFlag = rs.getString("UWFlag").trim();
            }

            if (rs.getString("AccDesc") == null) {
                this.AccDesc = null;
            } else {
                this.AccDesc = rs.getString("AccDesc").trim();
            }

            this.RetainAmnt = rs.getDouble("RetainAmnt");
            this.CessAmnt = rs.getDouble("CessAmnt");
            if (rs.getString("CessPersonSug") == null) {
                this.CessPersonSug = null;
            } else {
                this.CessPersonSug = rs.getString("CessPersonSug").trim();
            }

            this.LeaveHospDate = rs.getDate("LeaveHospDate");
            if (rs.getString("Diagnoses") == null) {
                this.Diagnoses = null;
            } else {
                this.Diagnoses = rs.getString("Diagnoses").trim();
            }

            this.ClaimCessRate = rs.getDouble("ClaimCessRate");
            this.ClaimBackFee = rs.getDouble("ClaimBackFee");
            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("MngCom") == null) {
                this.MngCom = null;
            } else {
                this.MngCom = rs.getString("MngCom").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LRClaimPolicy表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRClaimPolicySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LRClaimPolicySchema getSchema() {
        LRClaimPolicySchema aLRClaimPolicySchema = new LRClaimPolicySchema();
        aLRClaimPolicySchema.setSchema(this);
        return aLRClaimPolicySchema;
    }

    public LRClaimPolicyDB getDB() {
        LRClaimPolicyDB aDBOper = new LRClaimPolicyDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRClaimPolicy描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ClmNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReComCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReinsureItem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCalSort));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RgtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CaseNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpPolNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetDutyKind));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(KindCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskVer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolMngCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InsuredName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(CValiDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(AccidentDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ClmState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(StandPay));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RealPay));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ClmUWer));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndCaseDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(CessStart)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(CessEnd)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CessionRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CessionAmount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ReturnPay));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PolType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(EndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccountPeriod));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(RgtDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ConfDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ClaimMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccDesc));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RetainAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(CessAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CessPersonSug));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(LeaveHospDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Diagnoses));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ClaimCessRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ClaimBackFee));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MngCom));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRClaimPolicy>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            ReComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            ReinsureItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            RiskCalSort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                         SysConst.PACKAGESPILTER);
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                   SysConst.PACKAGESPILTER);
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                    SysConst.PACKAGESPILTER);
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            GetDutyKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            RiskVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                     SysConst.PACKAGESPILTER);
            PolMngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                       SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                        SysConst.PACKAGESPILTER);
            InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                       SysConst.PACKAGESPILTER);
            InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                         SysConst.PACKAGESPILTER);
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                     SysConst.PACKAGESPILTER);
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                       SysConst.PACKAGESPILTER);
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            AccidentDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 24, SysConst.PACKAGESPILTER));
            PolState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                      SysConst.PACKAGESPILTER);
            ClmState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                      SysConst.PACKAGESPILTER);
            StandPay = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 27, SysConst.PACKAGESPILTER))).doubleValue();
            RealPay = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 28, SysConst.PACKAGESPILTER))).doubleValue();
            ClmUWer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                     SysConst.PACKAGESPILTER);
            EndCaseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 30, SysConst.PACKAGESPILTER));
            CessStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 31, SysConst.PACKAGESPILTER));
            CessEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 32, SysConst.PACKAGESPILTER));
            CessionRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 33, SysConst.PACKAGESPILTER))).doubleValue();
            CessionAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 34, SysConst.PACKAGESPILTER))).doubleValue();
            ReturnPay = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 35, SysConst.PACKAGESPILTER))).doubleValue();
            PolType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,
                                     SysConst.PACKAGESPILTER);
            EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 37, SysConst.PACKAGESPILTER));
            AccountPeriod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,
                                           SysConst.PACKAGESPILTER);
            RgtDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 39, SysConst.PACKAGESPILTER));
            ConfDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 40, SysConst.PACKAGESPILTER));
            ClaimMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 41, SysConst.PACKAGESPILTER))).doubleValue();
            UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,
                                    SysConst.PACKAGESPILTER);
            AccDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,
                                     SysConst.PACKAGESPILTER);
            RetainAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 44, SysConst.PACKAGESPILTER))).doubleValue();
            CessAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 45, SysConst.PACKAGESPILTER))).doubleValue();
            CessPersonSug = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46,
                                           SysConst.PACKAGESPILTER);
            LeaveHospDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 47, SysConst.PACKAGESPILTER));
            Diagnoses = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48,
                                       SysConst.PACKAGESPILTER);
            ClaimCessRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 49, SysConst.PACKAGESPILTER))).doubleValue();
            ClaimBackFee = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 50, SysConst.PACKAGESPILTER))).doubleValue();
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 52, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 54, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55,
                                        SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56,
                                      SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57,
                                    SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRClaimPolicySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ClmNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmNo));
        }
        if (FCode.equals("PolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
        }
        if (FCode.equals("ReComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReComCode));
        }
        if (FCode.equals("ReinsureItem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReinsureItem));
        }
        if (FCode.equals("RiskCalSort")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCalSort));
        }
        if (FCode.equals("RgtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
        }
        if (FCode.equals("CaseNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equals("GetDutyKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyKind));
        }
        if (FCode.equals("KindCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVer));
        }
        if (FCode.equals("PolMngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolMngCom));
        }
        if (FCode.equals("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("InsuredNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
        }
        if (FCode.equals("InsuredName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
        }
        if (FCode.equals("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equals("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equals("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCValiDate()));
        }
        if (FCode.equals("AccidentDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getAccidentDate()));
        }
        if (FCode.equals("PolState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolState));
        }
        if (FCode.equals("ClmState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmState));
        }
        if (FCode.equals("StandPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandPay));
        }
        if (FCode.equals("RealPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RealPay));
        }
        if (FCode.equals("ClmUWer")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmUWer));
        }
        if (FCode.equals("EndCaseDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndCaseDate()));
        }
        if (FCode.equals("CessStart")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCessStart()));
        }
        if (FCode.equals("CessEnd")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCessEnd()));
        }
        if (FCode.equals("CessionRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CessionRate));
        }
        if (FCode.equals("CessionAmount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CessionAmount));
        }
        if (FCode.equals("ReturnPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnPay));
        }
        if (FCode.equals("PolType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolType));
        }
        if (FCode.equals("EndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
        }
        if (FCode.equals("AccountPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccountPeriod));
        }
        if (FCode.equals("RgtDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getRgtDate()));
        }
        if (FCode.equals("ConfDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getConfDate()));
        }
        if (FCode.equals("ClaimMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimMoney));
        }
        if (FCode.equals("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equals("AccDesc")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccDesc));
        }
        if (FCode.equals("RetainAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RetainAmnt));
        }
        if (FCode.equals("CessAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CessAmnt));
        }
        if (FCode.equals("CessPersonSug")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CessPersonSug));
        }
        if (FCode.equals("LeaveHospDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getLeaveHospDate()));
        }
        if (FCode.equals("Diagnoses")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Diagnoses));
        }
        if (FCode.equals("ClaimCessRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimCessRate));
        }
        if (FCode.equals("ClaimBackFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimBackFee));
        }
        if (FCode.equals("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MngCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ClmNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(PolNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ReComCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ReinsureItem);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(RiskCalSort);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(RgtNo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(CaseNo);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(GetDutyKind);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(KindCode);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(RiskVer);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(PolMngCom);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(SaleChnl);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(AgentGroup);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(InsuredNo);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(InsuredName);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(AppntNo);
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(AppntName);
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getCValiDate()));
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getAccidentDate()));
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(PolState);
            break;
        case 25:
            strFieldValue = StrTool.GBKToUnicode(ClmState);
            break;
        case 26:
            strFieldValue = String.valueOf(StandPay);
            break;
        case 27:
            strFieldValue = String.valueOf(RealPay);
            break;
        case 28:
            strFieldValue = StrTool.GBKToUnicode(ClmUWer);
            break;
        case 29:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getEndCaseDate()));
            break;
        case 30:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getCessStart()));
            break;
        case 31:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getCessEnd()));
            break;
        case 32:
            strFieldValue = String.valueOf(CessionRate);
            break;
        case 33:
            strFieldValue = String.valueOf(CessionAmount);
            break;
        case 34:
            strFieldValue = String.valueOf(ReturnPay);
            break;
        case 35:
            strFieldValue = StrTool.GBKToUnicode(PolType);
            break;
        case 36:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getEndDate()));
            break;
        case 37:
            strFieldValue = StrTool.GBKToUnicode(AccountPeriod);
            break;
        case 38:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getRgtDate()));
            break;
        case 39:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getConfDate()));
            break;
        case 40:
            strFieldValue = String.valueOf(ClaimMoney);
            break;
        case 41:
            strFieldValue = StrTool.GBKToUnicode(UWFlag);
            break;
        case 42:
            strFieldValue = StrTool.GBKToUnicode(AccDesc);
            break;
        case 43:
            strFieldValue = String.valueOf(RetainAmnt);
            break;
        case 44:
            strFieldValue = String.valueOf(CessAmnt);
            break;
        case 45:
            strFieldValue = StrTool.GBKToUnicode(CessPersonSug);
            break;
        case 46:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getLeaveHospDate()));
            break;
        case 47:
            strFieldValue = StrTool.GBKToUnicode(Diagnoses);
            break;
        case 48:
            strFieldValue = String.valueOf(ClaimCessRate);
            break;
        case 49:
            strFieldValue = String.valueOf(ClaimBackFee);
            break;
        case 50:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        case 51:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 52:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 53:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 54:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 55:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 56:
            strFieldValue = StrTool.GBKToUnicode(MngCom);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ClmNo")) {
            if (FValue != null && !FValue.equals("")) {
                ClmNo = FValue.trim();
            } else {
                ClmNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PolNo")) {
            if (FValue != null && !FValue.equals("")) {
                PolNo = FValue.trim();
            } else {
                PolNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReComCode")) {
            if (FValue != null && !FValue.equals("")) {
                ReComCode = FValue.trim();
            } else {
                ReComCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReinsureItem")) {
            if (FValue != null && !FValue.equals("")) {
                ReinsureItem = FValue.trim();
            } else {
                ReinsureItem = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCalSort")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCalSort = FValue.trim();
            } else {
                RiskCalSort = null;
            }
        }
        if (FCode.equalsIgnoreCase("RgtNo")) {
            if (FValue != null && !FValue.equals("")) {
                RgtNo = FValue.trim();
            } else {
                RgtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CaseNo")) {
            if (FValue != null && !FValue.equals("")) {
                CaseNo = FValue.trim();
            } else {
                CaseNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpPolNo = FValue.trim();
            } else {
                GrpPolNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetDutyKind")) {
            if (FValue != null && !FValue.equals("")) {
                GetDutyKind = FValue.trim();
            } else {
                GetDutyKind = null;
            }
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            if (FValue != null && !FValue.equals("")) {
                KindCode = FValue.trim();
            } else {
                KindCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskVer")) {
            if (FValue != null && !FValue.equals("")) {
                RiskVer = FValue.trim();
            } else {
                RiskVer = null;
            }
        }
        if (FCode.equalsIgnoreCase("PolMngCom")) {
            if (FValue != null && !FValue.equals("")) {
                PolMngCom = FValue.trim();
            } else {
                PolMngCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if (FValue != null && !FValue.equals("")) {
                SaleChnl = FValue.trim();
            } else {
                SaleChnl = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if (FValue != null && !FValue.equals("")) {
                AgentGroup = FValue.trim();
            } else {
                AgentGroup = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredNo")) {
            if (FValue != null && !FValue.equals("")) {
                InsuredNo = FValue.trim();
            } else {
                InsuredNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("InsuredName")) {
            if (FValue != null && !FValue.equals("")) {
                InsuredName = FValue.trim();
            } else {
                InsuredName = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if (FValue != null && !FValue.equals("")) {
                AppntNo = FValue.trim();
            } else {
                AppntNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if (FValue != null && !FValue.equals("")) {
                AppntName = FValue.trim();
            } else {
                AppntName = null;
            }
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if (FValue != null && !FValue.equals("")) {
                CValiDate = fDate.getDate(FValue);
            } else {
                CValiDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("AccidentDate")) {
            if (FValue != null && !FValue.equals("")) {
                AccidentDate = fDate.getDate(FValue);
            } else {
                AccidentDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("PolState")) {
            if (FValue != null && !FValue.equals("")) {
                PolState = FValue.trim();
            } else {
                PolState = null;
            }
        }
        if (FCode.equalsIgnoreCase("ClmState")) {
            if (FValue != null && !FValue.equals("")) {
                ClmState = FValue.trim();
            } else {
                ClmState = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandPay")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StandPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("RealPay")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RealPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("ClmUWer")) {
            if (FValue != null && !FValue.equals("")) {
                ClmUWer = FValue.trim();
            } else {
                ClmUWer = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndCaseDate")) {
            if (FValue != null && !FValue.equals("")) {
                EndCaseDate = fDate.getDate(FValue);
            } else {
                EndCaseDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("CessStart")) {
            if (FValue != null && !FValue.equals("")) {
                CessStart = fDate.getDate(FValue);
            } else {
                CessStart = null;
            }
        }
        if (FCode.equalsIgnoreCase("CessEnd")) {
            if (FValue != null && !FValue.equals("")) {
                CessEnd = fDate.getDate(FValue);
            } else {
                CessEnd = null;
            }
        }
        if (FCode.equalsIgnoreCase("CessionRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CessionRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("CessionAmount")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CessionAmount = d;
            }
        }
        if (FCode.equalsIgnoreCase("ReturnPay")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ReturnPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("PolType")) {
            if (FValue != null && !FValue.equals("")) {
                PolType = FValue.trim();
            } else {
                PolType = null;
            }
        }
        if (FCode.equalsIgnoreCase("EndDate")) {
            if (FValue != null && !FValue.equals("")) {
                EndDate = fDate.getDate(FValue);
            } else {
                EndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("AccountPeriod")) {
            if (FValue != null && !FValue.equals("")) {
                AccountPeriod = FValue.trim();
            } else {
                AccountPeriod = null;
            }
        }
        if (FCode.equalsIgnoreCase("RgtDate")) {
            if (FValue != null && !FValue.equals("")) {
                RgtDate = fDate.getDate(FValue);
            } else {
                RgtDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ConfDate")) {
            if (FValue != null && !FValue.equals("")) {
                ConfDate = fDate.getDate(FValue);
            } else {
                ConfDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ClaimMoney")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ClaimMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if (FValue != null && !FValue.equals("")) {
                UWFlag = FValue.trim();
            } else {
                UWFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("AccDesc")) {
            if (FValue != null && !FValue.equals("")) {
                AccDesc = FValue.trim();
            } else {
                AccDesc = null;
            }
        }
        if (FCode.equalsIgnoreCase("RetainAmnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RetainAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("CessAmnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                CessAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("CessPersonSug")) {
            if (FValue != null && !FValue.equals("")) {
                CessPersonSug = FValue.trim();
            } else {
                CessPersonSug = null;
            }
        }
        if (FCode.equalsIgnoreCase("LeaveHospDate")) {
            if (FValue != null && !FValue.equals("")) {
                LeaveHospDate = fDate.getDate(FValue);
            } else {
                LeaveHospDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Diagnoses")) {
            if (FValue != null && !FValue.equals("")) {
                Diagnoses = FValue.trim();
            } else {
                Diagnoses = null;
            }
        }
        if (FCode.equalsIgnoreCase("ClaimCessRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ClaimCessRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("ClaimBackFee")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ClaimBackFee = d;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MngCom")) {
            if (FValue != null && !FValue.equals("")) {
                MngCom = FValue.trim();
            } else {
                MngCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LRClaimPolicySchema other = (LRClaimPolicySchema) otherObject;
        return
                ClmNo.equals(other.getClmNo())
                && PolNo.equals(other.getPolNo())
                && ReComCode.equals(other.getReComCode())
                && ReinsureItem.equals(other.getReinsureItem())
                && RiskCalSort.equals(other.getRiskCalSort())
                && RgtNo.equals(other.getRgtNo())
                && CaseNo.equals(other.getCaseNo())
                && ContNo.equals(other.getContNo())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpPolNo.equals(other.getGrpPolNo())
                && GetDutyKind.equals(other.getGetDutyKind())
                && KindCode.equals(other.getKindCode())
                && RiskCode.equals(other.getRiskCode())
                && RiskVer.equals(other.getRiskVer())
                && PolMngCom.equals(other.getPolMngCom())
                && SaleChnl.equals(other.getSaleChnl())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && InsuredNo.equals(other.getInsuredNo())
                && InsuredName.equals(other.getInsuredName())
                && AppntNo.equals(other.getAppntNo())
                && AppntName.equals(other.getAppntName())
                && fDate.getString(CValiDate).equals(other.getCValiDate())
                && fDate.getString(AccidentDate).equals(other.getAccidentDate())
                && PolState.equals(other.getPolState())
                && ClmState.equals(other.getClmState())
                && StandPay == other.getStandPay()
                && RealPay == other.getRealPay()
                && ClmUWer.equals(other.getClmUWer())
                && fDate.getString(EndCaseDate).equals(other.getEndCaseDate())
                && fDate.getString(CessStart).equals(other.getCessStart())
                && fDate.getString(CessEnd).equals(other.getCessEnd())
                && CessionRate == other.getCessionRate()
                && CessionAmount == other.getCessionAmount()
                && ReturnPay == other.getReturnPay()
                && PolType.equals(other.getPolType())
                && fDate.getString(EndDate).equals(other.getEndDate())
                && AccountPeriod.equals(other.getAccountPeriod())
                && fDate.getString(RgtDate).equals(other.getRgtDate())
                && fDate.getString(ConfDate).equals(other.getConfDate())
                && ClaimMoney == other.getClaimMoney()
                && UWFlag.equals(other.getUWFlag())
                && AccDesc.equals(other.getAccDesc())
                && RetainAmnt == other.getRetainAmnt()
                && CessAmnt == other.getCessAmnt()
                && CessPersonSug.equals(other.getCessPersonSug())
                && fDate.getString(LeaveHospDate).equals(other.getLeaveHospDate())
                && Diagnoses.equals(other.getDiagnoses())
                && ClaimCessRate == other.getClaimCessRate()
                && ClaimBackFee == other.getClaimBackFee()
                && Remark.equals(other.getRemark())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && Operator.equals(other.getOperator())
                && MngCom.equals(other.getMngCom());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ClmNo")) {
            return 0;
        }
        if (strFieldName.equals("PolNo")) {
            return 1;
        }
        if (strFieldName.equals("ReComCode")) {
            return 2;
        }
        if (strFieldName.equals("ReinsureItem")) {
            return 3;
        }
        if (strFieldName.equals("RiskCalSort")) {
            return 4;
        }
        if (strFieldName.equals("RgtNo")) {
            return 5;
        }
        if (strFieldName.equals("CaseNo")) {
            return 6;
        }
        if (strFieldName.equals("ContNo")) {
            return 7;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 8;
        }
        if (strFieldName.equals("GrpPolNo")) {
            return 9;
        }
        if (strFieldName.equals("GetDutyKind")) {
            return 10;
        }
        if (strFieldName.equals("KindCode")) {
            return 11;
        }
        if (strFieldName.equals("RiskCode")) {
            return 12;
        }
        if (strFieldName.equals("RiskVer")) {
            return 13;
        }
        if (strFieldName.equals("PolMngCom")) {
            return 14;
        }
        if (strFieldName.equals("SaleChnl")) {
            return 15;
        }
        if (strFieldName.equals("AgentCode")) {
            return 16;
        }
        if (strFieldName.equals("AgentGroup")) {
            return 17;
        }
        if (strFieldName.equals("InsuredNo")) {
            return 18;
        }
        if (strFieldName.equals("InsuredName")) {
            return 19;
        }
        if (strFieldName.equals("AppntNo")) {
            return 20;
        }
        if (strFieldName.equals("AppntName")) {
            return 21;
        }
        if (strFieldName.equals("CValiDate")) {
            return 22;
        }
        if (strFieldName.equals("AccidentDate")) {
            return 23;
        }
        if (strFieldName.equals("PolState")) {
            return 24;
        }
        if (strFieldName.equals("ClmState")) {
            return 25;
        }
        if (strFieldName.equals("StandPay")) {
            return 26;
        }
        if (strFieldName.equals("RealPay")) {
            return 27;
        }
        if (strFieldName.equals("ClmUWer")) {
            return 28;
        }
        if (strFieldName.equals("EndCaseDate")) {
            return 29;
        }
        if (strFieldName.equals("CessStart")) {
            return 30;
        }
        if (strFieldName.equals("CessEnd")) {
            return 31;
        }
        if (strFieldName.equals("CessionRate")) {
            return 32;
        }
        if (strFieldName.equals("CessionAmount")) {
            return 33;
        }
        if (strFieldName.equals("ReturnPay")) {
            return 34;
        }
        if (strFieldName.equals("PolType")) {
            return 35;
        }
        if (strFieldName.equals("EndDate")) {
            return 36;
        }
        if (strFieldName.equals("AccountPeriod")) {
            return 37;
        }
        if (strFieldName.equals("RgtDate")) {
            return 38;
        }
        if (strFieldName.equals("ConfDate")) {
            return 39;
        }
        if (strFieldName.equals("ClaimMoney")) {
            return 40;
        }
        if (strFieldName.equals("UWFlag")) {
            return 41;
        }
        if (strFieldName.equals("AccDesc")) {
            return 42;
        }
        if (strFieldName.equals("RetainAmnt")) {
            return 43;
        }
        if (strFieldName.equals("CessAmnt")) {
            return 44;
        }
        if (strFieldName.equals("CessPersonSug")) {
            return 45;
        }
        if (strFieldName.equals("LeaveHospDate")) {
            return 46;
        }
        if (strFieldName.equals("Diagnoses")) {
            return 47;
        }
        if (strFieldName.equals("ClaimCessRate")) {
            return 48;
        }
        if (strFieldName.equals("ClaimBackFee")) {
            return 49;
        }
        if (strFieldName.equals("Remark")) {
            return 50;
        }
        if (strFieldName.equals("MakeDate")) {
            return 51;
        }
        if (strFieldName.equals("MakeTime")) {
            return 52;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 53;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 54;
        }
        if (strFieldName.equals("Operator")) {
            return 55;
        }
        if (strFieldName.equals("MngCom")) {
            return 56;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ClmNo";
            break;
        case 1:
            strFieldName = "PolNo";
            break;
        case 2:
            strFieldName = "ReComCode";
            break;
        case 3:
            strFieldName = "ReinsureItem";
            break;
        case 4:
            strFieldName = "RiskCalSort";
            break;
        case 5:
            strFieldName = "RgtNo";
            break;
        case 6:
            strFieldName = "CaseNo";
            break;
        case 7:
            strFieldName = "ContNo";
            break;
        case 8:
            strFieldName = "GrpContNo";
            break;
        case 9:
            strFieldName = "GrpPolNo";
            break;
        case 10:
            strFieldName = "GetDutyKind";
            break;
        case 11:
            strFieldName = "KindCode";
            break;
        case 12:
            strFieldName = "RiskCode";
            break;
        case 13:
            strFieldName = "RiskVer";
            break;
        case 14:
            strFieldName = "PolMngCom";
            break;
        case 15:
            strFieldName = "SaleChnl";
            break;
        case 16:
            strFieldName = "AgentCode";
            break;
        case 17:
            strFieldName = "AgentGroup";
            break;
        case 18:
            strFieldName = "InsuredNo";
            break;
        case 19:
            strFieldName = "InsuredName";
            break;
        case 20:
            strFieldName = "AppntNo";
            break;
        case 21:
            strFieldName = "AppntName";
            break;
        case 22:
            strFieldName = "CValiDate";
            break;
        case 23:
            strFieldName = "AccidentDate";
            break;
        case 24:
            strFieldName = "PolState";
            break;
        case 25:
            strFieldName = "ClmState";
            break;
        case 26:
            strFieldName = "StandPay";
            break;
        case 27:
            strFieldName = "RealPay";
            break;
        case 28:
            strFieldName = "ClmUWer";
            break;
        case 29:
            strFieldName = "EndCaseDate";
            break;
        case 30:
            strFieldName = "CessStart";
            break;
        case 31:
            strFieldName = "CessEnd";
            break;
        case 32:
            strFieldName = "CessionRate";
            break;
        case 33:
            strFieldName = "CessionAmount";
            break;
        case 34:
            strFieldName = "ReturnPay";
            break;
        case 35:
            strFieldName = "PolType";
            break;
        case 36:
            strFieldName = "EndDate";
            break;
        case 37:
            strFieldName = "AccountPeriod";
            break;
        case 38:
            strFieldName = "RgtDate";
            break;
        case 39:
            strFieldName = "ConfDate";
            break;
        case 40:
            strFieldName = "ClaimMoney";
            break;
        case 41:
            strFieldName = "UWFlag";
            break;
        case 42:
            strFieldName = "AccDesc";
            break;
        case 43:
            strFieldName = "RetainAmnt";
            break;
        case 44:
            strFieldName = "CessAmnt";
            break;
        case 45:
            strFieldName = "CessPersonSug";
            break;
        case 46:
            strFieldName = "LeaveHospDate";
            break;
        case 47:
            strFieldName = "Diagnoses";
            break;
        case 48:
            strFieldName = "ClaimCessRate";
            break;
        case 49:
            strFieldName = "ClaimBackFee";
            break;
        case 50:
            strFieldName = "Remark";
            break;
        case 51:
            strFieldName = "MakeDate";
            break;
        case 52:
            strFieldName = "MakeTime";
            break;
        case 53:
            strFieldName = "ModifyDate";
            break;
        case 54:
            strFieldName = "ModifyTime";
            break;
        case 55:
            strFieldName = "Operator";
            break;
        case 56:
            strFieldName = "MngCom";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ClmNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReComCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReinsureItem")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCalSort")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpPolNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyKind")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("KindCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVer")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolMngCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuredName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CValiDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AccidentDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PolState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandPay")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RealPay")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ClmUWer")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndCaseDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CessStart")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CessEnd")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("CessionRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CessionAmount")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ReturnPay")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PolType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AccountPeriod")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ConfDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ClaimMoney")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("UWFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccDesc")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RetainAmnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CessAmnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CessPersonSug")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LeaveHospDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Diagnoses")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClaimCessRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ClaimBackFee")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 21:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 22:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 23:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 24:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 25:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 26:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 27:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 28:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 29:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 30:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 31:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 32:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 33:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 34:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 35:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 36:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 37:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 38:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 39:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 40:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 41:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 42:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 43:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 44:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 45:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 46:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 47:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 48:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 49:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 50:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 51:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 52:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 53:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 54:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 55:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 56:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
