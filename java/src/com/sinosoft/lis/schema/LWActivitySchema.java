/*
 * <p>ClassName: LWActivitySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 工作流模型
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LWActivityDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LWActivitySchema implements Schema
{
    // @Field
    /** 活动id */
    private String ActivityID;
    /** 活动名 */
    private String ActivityName;
    /** 活动类型 */
    private String ActivityType;
    /** 活动说明 */
    private String ActivityDesc;
    /** 活动进入前动作 */
    private String BeforeInit;
    /** 活动进入前动作类型 */
    private String BeforeInitType;
    /** 活动进入后动作 */
    private String AfterInit;
    /** 活动进入后动作类型 */
    private String AfterInitType;
    /** 活动结束前动作 */
    private String BeforeEnd;
    /** 活动结束前动作类型 */
    private String BeforeEndType;
    /** 活动结束后动作 */
    private String AfterEnd;
    /** 活动结束后动作类型 */
    private String AfterEndType;
    /** 平均等待时间 */
    private double WatingTime;
    /** 平均执行时间 */
    private double WorkingTime;
    /** 超时时间 */
    private double TimeOut;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 20; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LWActivitySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ActivityID";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getActivityID()
    {
        if (ActivityID != null && !ActivityID.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ActivityID = StrTool.unicodeToGBK(ActivityID);
        }
        return ActivityID;
    }

    public void setActivityID(String aActivityID)
    {
        ActivityID = aActivityID;
    }

    public String getActivityName()
    {
        if (ActivityName != null && !ActivityName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ActivityName = StrTool.unicodeToGBK(ActivityName);
        }
        return ActivityName;
    }

    public void setActivityName(String aActivityName)
    {
        ActivityName = aActivityName;
    }

    public String getActivityType()
    {
        if (ActivityType != null && !ActivityType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ActivityType = StrTool.unicodeToGBK(ActivityType);
        }
        return ActivityType;
    }

    public void setActivityType(String aActivityType)
    {
        ActivityType = aActivityType;
    }

    public String getActivityDesc()
    {
        if (ActivityDesc != null && !ActivityDesc.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ActivityDesc = StrTool.unicodeToGBK(ActivityDesc);
        }
        return ActivityDesc;
    }

    public void setActivityDesc(String aActivityDesc)
    {
        ActivityDesc = aActivityDesc;
    }

    public String getBeforeInit()
    {
        if (BeforeInit != null && !BeforeInit.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BeforeInit = StrTool.unicodeToGBK(BeforeInit);
        }
        return BeforeInit;
    }

    public void setBeforeInit(String aBeforeInit)
    {
        BeforeInit = aBeforeInit;
    }

    public String getBeforeInitType()
    {
        if (BeforeInitType != null && !BeforeInitType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BeforeInitType = StrTool.unicodeToGBK(BeforeInitType);
        }
        return BeforeInitType;
    }

    public void setBeforeInitType(String aBeforeInitType)
    {
        BeforeInitType = aBeforeInitType;
    }

    public String getAfterInit()
    {
        if (AfterInit != null && !AfterInit.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AfterInit = StrTool.unicodeToGBK(AfterInit);
        }
        return AfterInit;
    }

    public void setAfterInit(String aAfterInit)
    {
        AfterInit = aAfterInit;
    }

    public String getAfterInitType()
    {
        if (AfterInitType != null && !AfterInitType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AfterInitType = StrTool.unicodeToGBK(AfterInitType);
        }
        return AfterInitType;
    }

    public void setAfterInitType(String aAfterInitType)
    {
        AfterInitType = aAfterInitType;
    }

    public String getBeforeEnd()
    {
        if (BeforeEnd != null && !BeforeEnd.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BeforeEnd = StrTool.unicodeToGBK(BeforeEnd);
        }
        return BeforeEnd;
    }

    public void setBeforeEnd(String aBeforeEnd)
    {
        BeforeEnd = aBeforeEnd;
    }

    public String getBeforeEndType()
    {
        if (BeforeEndType != null && !BeforeEndType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BeforeEndType = StrTool.unicodeToGBK(BeforeEndType);
        }
        return BeforeEndType;
    }

    public void setBeforeEndType(String aBeforeEndType)
    {
        BeforeEndType = aBeforeEndType;
    }

    public String getAfterEnd()
    {
        if (AfterEnd != null && !AfterEnd.equals("") && SysConst.CHANGECHARSET == true)
        {
            AfterEnd = StrTool.unicodeToGBK(AfterEnd);
        }
        return AfterEnd;
    }

    public void setAfterEnd(String aAfterEnd)
    {
        AfterEnd = aAfterEnd;
    }

    public String getAfterEndType()
    {
        if (AfterEndType != null && !AfterEndType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AfterEndType = StrTool.unicodeToGBK(AfterEndType);
        }
        return AfterEndType;
    }

    public void setAfterEndType(String aAfterEndType)
    {
        AfterEndType = aAfterEndType;
    }

    public double getWatingTime()
    {
        return WatingTime;
    }

    public void setWatingTime(double aWatingTime)
    {
        WatingTime = aWatingTime;
    }

    public void setWatingTime(String aWatingTime)
    {
        if (aWatingTime != null && !aWatingTime.equals(""))
        {
            Double tDouble = new Double(aWatingTime);
            double d = tDouble.doubleValue();
            WatingTime = d;
        }
    }

    public double getWorkingTime()
    {
        return WorkingTime;
    }

    public void setWorkingTime(double aWorkingTime)
    {
        WorkingTime = aWorkingTime;
    }

    public void setWorkingTime(String aWorkingTime)
    {
        if (aWorkingTime != null && !aWorkingTime.equals(""))
        {
            Double tDouble = new Double(aWorkingTime);
            double d = tDouble.doubleValue();
            WorkingTime = d;
        }
    }

    public double getTimeOut()
    {
        return TimeOut;
    }

    public void setTimeOut(double aTimeOut)
    {
        TimeOut = aTimeOut;
    }

    public void setTimeOut(String aTimeOut)
    {
        if (aTimeOut != null && !aTimeOut.equals(""))
        {
            Double tDouble = new Double(aTimeOut);
            double d = tDouble.doubleValue();
            TimeOut = d;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LWActivitySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LWActivitySchema aLWActivitySchema)
    {
        this.ActivityID = aLWActivitySchema.getActivityID();
        this.ActivityName = aLWActivitySchema.getActivityName();
        this.ActivityType = aLWActivitySchema.getActivityType();
        this.ActivityDesc = aLWActivitySchema.getActivityDesc();
        this.BeforeInit = aLWActivitySchema.getBeforeInit();
        this.BeforeInitType = aLWActivitySchema.getBeforeInitType();
        this.AfterInit = aLWActivitySchema.getAfterInit();
        this.AfterInitType = aLWActivitySchema.getAfterInitType();
        this.BeforeEnd = aLWActivitySchema.getBeforeEnd();
        this.BeforeEndType = aLWActivitySchema.getBeforeEndType();
        this.AfterEnd = aLWActivitySchema.getAfterEnd();
        this.AfterEndType = aLWActivitySchema.getAfterEndType();
        this.WatingTime = aLWActivitySchema.getWatingTime();
        this.WorkingTime = aLWActivitySchema.getWorkingTime();
        this.TimeOut = aLWActivitySchema.getTimeOut();
        this.Operator = aLWActivitySchema.getOperator();
        this.MakeDate = fDate.getDate(aLWActivitySchema.getMakeDate());
        this.MakeTime = aLWActivitySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLWActivitySchema.getModifyDate());
        this.ModifyTime = aLWActivitySchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ActivityID") == null)
            {
                this.ActivityID = null;
            }
            else
            {
                this.ActivityID = rs.getString("ActivityID").trim();
            }

            if (rs.getString("ActivityName") == null)
            {
                this.ActivityName = null;
            }
            else
            {
                this.ActivityName = rs.getString("ActivityName").trim();
            }

            if (rs.getString("ActivityType") == null)
            {
                this.ActivityType = null;
            }
            else
            {
                this.ActivityType = rs.getString("ActivityType").trim();
            }

            if (rs.getString("ActivityDesc") == null)
            {
                this.ActivityDesc = null;
            }
            else
            {
                this.ActivityDesc = rs.getString("ActivityDesc").trim();
            }

            if (rs.getString("BeforeInit") == null)
            {
                this.BeforeInit = null;
            }
            else
            {
                this.BeforeInit = rs.getString("BeforeInit").trim();
            }

            if (rs.getString("BeforeInitType") == null)
            {
                this.BeforeInitType = null;
            }
            else
            {
                this.BeforeInitType = rs.getString("BeforeInitType").trim();
            }

            if (rs.getString("AfterInit") == null)
            {
                this.AfterInit = null;
            }
            else
            {
                this.AfterInit = rs.getString("AfterInit").trim();
            }

            if (rs.getString("AfterInitType") == null)
            {
                this.AfterInitType = null;
            }
            else
            {
                this.AfterInitType = rs.getString("AfterInitType").trim();
            }

            if (rs.getString("BeforeEnd") == null)
            {
                this.BeforeEnd = null;
            }
            else
            {
                this.BeforeEnd = rs.getString("BeforeEnd").trim();
            }

            if (rs.getString("BeforeEndType") == null)
            {
                this.BeforeEndType = null;
            }
            else
            {
                this.BeforeEndType = rs.getString("BeforeEndType").trim();
            }

            if (rs.getString("AfterEnd") == null)
            {
                this.AfterEnd = null;
            }
            else
            {
                this.AfterEnd = rs.getString("AfterEnd").trim();
            }

            if (rs.getString("AfterEndType") == null)
            {
                this.AfterEndType = null;
            }
            else
            {
                this.AfterEndType = rs.getString("AfterEndType").trim();
            }

            this.WatingTime = rs.getDouble("WatingTime");
            this.WorkingTime = rs.getDouble("WorkingTime");
            this.TimeOut = rs.getDouble("TimeOut");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWActivitySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LWActivitySchema getSchema()
    {
        LWActivitySchema aLWActivitySchema = new LWActivitySchema();
        aLWActivitySchema.setSchema(this);
        return aLWActivitySchema;
    }

    public LWActivityDB getDB()
    {
        LWActivityDB aDBOper = new LWActivityDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWActivity描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ActivityID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ActivityName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ActivityType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ActivityDesc)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BeforeInit)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BeforeInitType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AfterInit)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AfterInitType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BeforeEnd)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BeforeEndType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AfterEnd)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AfterEndType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(WatingTime) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(WorkingTime) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(TimeOut) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWActivity>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ActivityID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            ActivityName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ActivityType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            ActivityDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            BeforeInit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            BeforeInitType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                            SysConst.PACKAGESPILTER);
            AfterInit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            AfterInitType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                           SysConst.PACKAGESPILTER);
            BeforeEnd = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            BeforeEndType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                           SysConst.PACKAGESPILTER);
            AfterEnd = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            AfterEndType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            WatingTime = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            WorkingTime = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            TimeOut = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 15, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWActivitySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ActivityID"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ActivityID));
        }
        if (FCode.equals("ActivityName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ActivityName));
        }
        if (FCode.equals("ActivityType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ActivityType));
        }
        if (FCode.equals("ActivityDesc"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ActivityDesc));
        }
        if (FCode.equals("BeforeInit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BeforeInit));
        }
        if (FCode.equals("BeforeInitType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BeforeInitType));
        }
        if (FCode.equals("AfterInit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AfterInit));
        }
        if (FCode.equals("AfterInitType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AfterInitType));
        }
        if (FCode.equals("BeforeEnd"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BeforeEnd));
        }
        if (FCode.equals("BeforeEndType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BeforeEndType));
        }
        if (FCode.equals("AfterEnd"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AfterEnd));
        }
        if (FCode.equals("AfterEndType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AfterEndType));
        }
        if (FCode.equals("WatingTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WatingTime));
        }
        if (FCode.equals("WorkingTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WorkingTime));
        }
        if (FCode.equals("TimeOut"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TimeOut));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ActivityID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ActivityName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ActivityType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ActivityDesc);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(BeforeInit);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(BeforeInitType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AfterInit);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AfterInitType);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(BeforeEnd);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(BeforeEndType);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AfterEnd);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AfterEndType);
                break;
            case 12:
                strFieldValue = String.valueOf(WatingTime);
                break;
            case 13:
                strFieldValue = String.valueOf(WorkingTime);
                break;
            case 14:
                strFieldValue = String.valueOf(TimeOut);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ActivityID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ActivityID = FValue.trim();
            }
            else
            {
                ActivityID = null;
            }
        }
        if (FCode.equals("ActivityName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ActivityName = FValue.trim();
            }
            else
            {
                ActivityName = null;
            }
        }
        if (FCode.equals("ActivityType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ActivityType = FValue.trim();
            }
            else
            {
                ActivityType = null;
            }
        }
        if (FCode.equals("ActivityDesc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ActivityDesc = FValue.trim();
            }
            else
            {
                ActivityDesc = null;
            }
        }
        if (FCode.equals("BeforeInit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BeforeInit = FValue.trim();
            }
            else
            {
                BeforeInit = null;
            }
        }
        if (FCode.equals("BeforeInitType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BeforeInitType = FValue.trim();
            }
            else
            {
                BeforeInitType = null;
            }
        }
        if (FCode.equals("AfterInit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AfterInit = FValue.trim();
            }
            else
            {
                AfterInit = null;
            }
        }
        if (FCode.equals("AfterInitType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AfterInitType = FValue.trim();
            }
            else
            {
                AfterInitType = null;
            }
        }
        if (FCode.equals("BeforeEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BeforeEnd = FValue.trim();
            }
            else
            {
                BeforeEnd = null;
            }
        }
        if (FCode.equals("BeforeEndType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BeforeEndType = FValue.trim();
            }
            else
            {
                BeforeEndType = null;
            }
        }
        if (FCode.equals("AfterEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AfterEnd = FValue.trim();
            }
            else
            {
                AfterEnd = null;
            }
        }
        if (FCode.equals("AfterEndType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AfterEndType = FValue.trim();
            }
            else
            {
                AfterEndType = null;
            }
        }
        if (FCode.equals("WatingTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                WatingTime = d;
            }
        }
        if (FCode.equals("WorkingTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                WorkingTime = d;
            }
        }
        if (FCode.equals("TimeOut"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                TimeOut = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LWActivitySchema other = (LWActivitySchema) otherObject;
        return
                ActivityID.equals(other.getActivityID())
                && ActivityName.equals(other.getActivityName())
                && ActivityType.equals(other.getActivityType())
                && ActivityDesc.equals(other.getActivityDesc())
                && BeforeInit.equals(other.getBeforeInit())
                && BeforeInitType.equals(other.getBeforeInitType())
                && AfterInit.equals(other.getAfterInit())
                && AfterInitType.equals(other.getAfterInitType())
                && BeforeEnd.equals(other.getBeforeEnd())
                && BeforeEndType.equals(other.getBeforeEndType())
                && AfterEnd.equals(other.getAfterEnd())
                && AfterEndType.equals(other.getAfterEndType())
                && WatingTime == other.getWatingTime()
                && WorkingTime == other.getWorkingTime()
                && TimeOut == other.getTimeOut()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ActivityID"))
        {
            return 0;
        }
        if (strFieldName.equals("ActivityName"))
        {
            return 1;
        }
        if (strFieldName.equals("ActivityType"))
        {
            return 2;
        }
        if (strFieldName.equals("ActivityDesc"))
        {
            return 3;
        }
        if (strFieldName.equals("BeforeInit"))
        {
            return 4;
        }
        if (strFieldName.equals("BeforeInitType"))
        {
            return 5;
        }
        if (strFieldName.equals("AfterInit"))
        {
            return 6;
        }
        if (strFieldName.equals("AfterInitType"))
        {
            return 7;
        }
        if (strFieldName.equals("BeforeEnd"))
        {
            return 8;
        }
        if (strFieldName.equals("BeforeEndType"))
        {
            return 9;
        }
        if (strFieldName.equals("AfterEnd"))
        {
            return 10;
        }
        if (strFieldName.equals("AfterEndType"))
        {
            return 11;
        }
        if (strFieldName.equals("WatingTime"))
        {
            return 12;
        }
        if (strFieldName.equals("WorkingTime"))
        {
            return 13;
        }
        if (strFieldName.equals("TimeOut"))
        {
            return 14;
        }
        if (strFieldName.equals("Operator"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 17;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 19;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ActivityID";
                break;
            case 1:
                strFieldName = "ActivityName";
                break;
            case 2:
                strFieldName = "ActivityType";
                break;
            case 3:
                strFieldName = "ActivityDesc";
                break;
            case 4:
                strFieldName = "BeforeInit";
                break;
            case 5:
                strFieldName = "BeforeInitType";
                break;
            case 6:
                strFieldName = "AfterInit";
                break;
            case 7:
                strFieldName = "AfterInitType";
                break;
            case 8:
                strFieldName = "BeforeEnd";
                break;
            case 9:
                strFieldName = "BeforeEndType";
                break;
            case 10:
                strFieldName = "AfterEnd";
                break;
            case 11:
                strFieldName = "AfterEndType";
                break;
            case 12:
                strFieldName = "WatingTime";
                break;
            case 13:
                strFieldName = "WorkingTime";
                break;
            case 14:
                strFieldName = "TimeOut";
                break;
            case 15:
                strFieldName = "Operator";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ActivityID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ActivityName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ActivityType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ActivityDesc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BeforeInit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BeforeInitType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AfterInit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AfterInitType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BeforeEnd"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BeforeEndType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AfterEnd"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AfterEndType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WatingTime"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("WorkingTime"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("TimeOut"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
