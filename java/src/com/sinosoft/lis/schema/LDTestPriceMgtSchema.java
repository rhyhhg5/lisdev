/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDTestPriceMgtDB;

/*
 * <p>ClassName: LDTestPriceMgtSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭丽颖-表结构修改提交单-20060323
 * @CreateDate：2006-03-23
 */
public class LDTestPriceMgtSchema implements Schema, Cloneable
{
	// @Field
	/** 体检机构代码 */
	private String HospitCode;
	/** 流水号 */
	private String SerialNo;
	/** 记录时间 */
	private Date RecordDate;
	/** 检查项目代码 */
	private String MedicaItemCode;
	/** 检查项目单价 */
	private String MedicaItemPrice;
	/** 补充说明 */
	private String Explain;
	/** 价格类别 */
	private String PriceClass;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 管理机构 */
	private String ManageCom;
	/** 合同编号 */
	private String ContraNo;
	/** 合同责任项目编号 */
	private String ContraItemNo;

	public static final int FIELDNUM = 15;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDTestPriceMgtSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "HospitCode";
		pk[1] = "SerialNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LDTestPriceMgtSchema cloned = (LDTestPriceMgtSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getHospitCode()
	{
		return HospitCode;
	}
	public void setHospitCode(String aHospitCode)
	{
            HospitCode = aHospitCode;
	}
	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
            SerialNo = aSerialNo;
	}
	public String getRecordDate()
	{
		if( RecordDate != null )
			return fDate.getString(RecordDate);
		else
			return null;
	}
	public void setRecordDate(Date aRecordDate)
	{
            RecordDate = aRecordDate;
	}
	public void setRecordDate(String aRecordDate)
	{
		if (aRecordDate != null && !aRecordDate.equals("") )
		{
			RecordDate = fDate.getDate( aRecordDate );
		}
		else
			RecordDate = null;
	}

	public String getMedicaItemCode()
	{
		return MedicaItemCode;
	}
	public void setMedicaItemCode(String aMedicaItemCode)
	{
            MedicaItemCode = aMedicaItemCode;
	}
	public String getMedicaItemPrice()
	{
		return MedicaItemPrice;
	}
	public void setMedicaItemPrice(String aMedicaItemPrice)
	{
            MedicaItemPrice = aMedicaItemPrice;
	}
	public String getExplain()
	{
		return Explain;
	}
	public void setExplain(String aExplain)
	{
            Explain = aExplain;
	}
	public String getPriceClass()
	{
		return PriceClass;
	}
	public void setPriceClass(String aPriceClass)
	{
            PriceClass = aPriceClass;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getContraNo()
	{
		return ContraNo;
	}
	public void setContraNo(String aContraNo)
	{
            ContraNo = aContraNo;
	}
	public String getContraItemNo()
	{
		return ContraItemNo;
	}
	public void setContraItemNo(String aContraItemNo)
	{
            ContraItemNo = aContraItemNo;
	}

	/**
	* 使用另外一个 LDTestPriceMgtSchema 对象给 Schema 赋值
	* @param: aLDTestPriceMgtSchema LDTestPriceMgtSchema
	**/
	public void setSchema(LDTestPriceMgtSchema aLDTestPriceMgtSchema)
	{
		this.HospitCode = aLDTestPriceMgtSchema.getHospitCode();
		this.SerialNo = aLDTestPriceMgtSchema.getSerialNo();
		this.RecordDate = fDate.getDate( aLDTestPriceMgtSchema.getRecordDate());
		this.MedicaItemCode = aLDTestPriceMgtSchema.getMedicaItemCode();
		this.MedicaItemPrice = aLDTestPriceMgtSchema.getMedicaItemPrice();
		this.Explain = aLDTestPriceMgtSchema.getExplain();
		this.PriceClass = aLDTestPriceMgtSchema.getPriceClass();
		this.Operator = aLDTestPriceMgtSchema.getOperator();
		this.MakeDate = fDate.getDate( aLDTestPriceMgtSchema.getMakeDate());
		this.MakeTime = aLDTestPriceMgtSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLDTestPriceMgtSchema.getModifyDate());
		this.ModifyTime = aLDTestPriceMgtSchema.getModifyTime();
		this.ManageCom = aLDTestPriceMgtSchema.getManageCom();
		this.ContraNo = aLDTestPriceMgtSchema.getContraNo();
		this.ContraItemNo = aLDTestPriceMgtSchema.getContraItemNo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("HospitCode") == null )
				this.HospitCode = null;
			else
				this.HospitCode = rs.getString("HospitCode").trim();

			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			this.RecordDate = rs.getDate("RecordDate");
			if( rs.getString("MedicaItemCode") == null )
				this.MedicaItemCode = null;
			else
				this.MedicaItemCode = rs.getString("MedicaItemCode").trim();

			if( rs.getString("MedicaItemPrice") == null )
				this.MedicaItemPrice = null;
			else
				this.MedicaItemPrice = rs.getString("MedicaItemPrice").trim();

			if( rs.getString("Explain") == null )
				this.Explain = null;
			else
				this.Explain = rs.getString("Explain").trim();

			if( rs.getString("PriceClass") == null )
				this.PriceClass = null;
			else
				this.PriceClass = rs.getString("PriceClass").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ContraNo") == null )
				this.ContraNo = null;
			else
				this.ContraNo = rs.getString("ContraNo").trim();

			if( rs.getString("ContraItemNo") == null )
				this.ContraItemNo = null;
			else
				this.ContraItemNo = rs.getString("ContraItemNo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDTestPriceMgt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDTestPriceMgtSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDTestPriceMgtSchema getSchema()
	{
		LDTestPriceMgtSchema aLDTestPriceMgtSchema = new LDTestPriceMgtSchema();
		aLDTestPriceMgtSchema.setSchema(this);
		return aLDTestPriceMgtSchema;
	}

	public LDTestPriceMgtDB getDB()
	{
		LDTestPriceMgtDB aDBOper = new LDTestPriceMgtDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTestPriceMgt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(HospitCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( RecordDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MedicaItemCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MedicaItemPrice)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Explain)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PriceClass)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContraNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContraItemNo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTestPriceMgt>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			HospitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RecordDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			MedicaItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			MedicaItemPrice = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Explain = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			PriceClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ContraNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ContraItemNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDTestPriceMgtSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("HospitCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitCode));
		}
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("RecordDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRecordDate()));
		}
		if (FCode.equals("MedicaItemCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaItemCode));
		}
		if (FCode.equals("MedicaItemPrice"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MedicaItemPrice));
		}
		if (FCode.equals("Explain"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Explain));
		}
		if (FCode.equals("PriceClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PriceClass));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ContraNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraNo));
		}
		if (FCode.equals("ContraItemNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraItemNo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(HospitCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRecordDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(MedicaItemCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(MedicaItemPrice);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Explain);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(PriceClass);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ContraNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ContraItemNo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("HospitCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitCode = FValue.trim();
			}
			else
				HospitCode = null;
		}
		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("RecordDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RecordDate = fDate.getDate( FValue );
			}
			else
				RecordDate = null;
		}
		if (FCode.equalsIgnoreCase("MedicaItemCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MedicaItemCode = FValue.trim();
			}
			else
				MedicaItemCode = null;
		}
		if (FCode.equalsIgnoreCase("MedicaItemPrice"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MedicaItemPrice = FValue.trim();
			}
			else
				MedicaItemPrice = null;
		}
		if (FCode.equalsIgnoreCase("Explain"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Explain = FValue.trim();
			}
			else
				Explain = null;
		}
		if (FCode.equalsIgnoreCase("PriceClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PriceClass = FValue.trim();
			}
			else
				PriceClass = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ContraNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraNo = FValue.trim();
			}
			else
				ContraNo = null;
		}
		if (FCode.equalsIgnoreCase("ContraItemNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraItemNo = FValue.trim();
			}
			else
				ContraItemNo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDTestPriceMgtSchema other = (LDTestPriceMgtSchema)otherObject;
		return
			HospitCode.equals(other.getHospitCode())
			&& SerialNo.equals(other.getSerialNo())
			&& fDate.getString(RecordDate).equals(other.getRecordDate())
			&& MedicaItemCode.equals(other.getMedicaItemCode())
			&& MedicaItemPrice.equals(other.getMedicaItemPrice())
			&& Explain.equals(other.getExplain())
			&& PriceClass.equals(other.getPriceClass())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& ManageCom.equals(other.getManageCom())
			&& ContraNo.equals(other.getContraNo())
			&& ContraItemNo.equals(other.getContraItemNo());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("HospitCode") ) {
			return 0;
		}
		if( strFieldName.equals("SerialNo") ) {
			return 1;
		}
		if( strFieldName.equals("RecordDate") ) {
			return 2;
		}
		if( strFieldName.equals("MedicaItemCode") ) {
			return 3;
		}
		if( strFieldName.equals("MedicaItemPrice") ) {
			return 4;
		}
		if( strFieldName.equals("Explain") ) {
			return 5;
		}
		if( strFieldName.equals("PriceClass") ) {
			return 6;
		}
		if( strFieldName.equals("Operator") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 12;
		}
		if( strFieldName.equals("ContraNo") ) {
			return 13;
		}
		if( strFieldName.equals("ContraItemNo") ) {
			return 14;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "HospitCode";
				break;
			case 1:
				strFieldName = "SerialNo";
				break;
			case 2:
				strFieldName = "RecordDate";
				break;
			case 3:
				strFieldName = "MedicaItemCode";
				break;
			case 4:
				strFieldName = "MedicaItemPrice";
				break;
			case 5:
				strFieldName = "Explain";
				break;
			case 6:
				strFieldName = "PriceClass";
				break;
			case 7:
				strFieldName = "Operator";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			case 12:
				strFieldName = "ManageCom";
				break;
			case 13:
				strFieldName = "ContraNo";
				break;
			case 14:
				strFieldName = "ContraItemNo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("HospitCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RecordDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MedicaItemCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MedicaItemPrice") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Explain") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PriceClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContraNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContraItemNo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
