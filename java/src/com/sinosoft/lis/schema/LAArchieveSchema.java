/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAArchieveDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAArchieveSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-24
 */
public class LAArchieveSchema implements Schema
{
    // @Field
    /** 业务员编码 */
    private String AgentCode;
    /** 档案编码 */
    private String ArchieveNo;
    /** 档案项目流水号 */
    private int ItemIdx;
    /** 档案日期 */
    private Date ArchieveDate;
    /** 归档日期 */
    private Date PigeOnHoleDate;
    /** 档案类型 */
    private String ArchType;
    /** 档案项目 */
    private String ArchItem;
    /** 销毁日期 */
    private Date DestoryDate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 上一次修改日期 */
    private Date ModifyDate;
    /** 上一次修改时间 */
    private String ModifyTime;
    /** 档案项目编码 */
    private String ItemCode;

    public static final int FIELDNUM = 14; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAArchieveSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "AgentCode";
        pk[1] = "ArchieveNo";
        pk[2] = "ItemIdx";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getArchieveNo()
    {
        if (ArchieveNo != null && !ArchieveNo.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ArchieveNo = StrTool.unicodeToGBK(ArchieveNo);
        }
        return ArchieveNo;
    }

    public void setArchieveNo(String aArchieveNo)
    {
        ArchieveNo = aArchieveNo;
    }

    public int getItemIdx()
    {
        return ItemIdx;
    }

    public void setItemIdx(int aItemIdx)
    {
        ItemIdx = aItemIdx;
    }

    public void setItemIdx(String aItemIdx)
    {
        if (aItemIdx != null && !aItemIdx.equals(""))
        {
            Integer tInteger = new Integer(aItemIdx);
            int i = tInteger.intValue();
            ItemIdx = i;
        }
    }

    public String getArchieveDate()
    {
        if (ArchieveDate != null)
        {
            return fDate.getString(ArchieveDate);
        }
        else
        {
            return null;
        }
    }

    public void setArchieveDate(Date aArchieveDate)
    {
        ArchieveDate = aArchieveDate;
    }

    public void setArchieveDate(String aArchieveDate)
    {
        if (aArchieveDate != null && !aArchieveDate.equals(""))
        {
            ArchieveDate = fDate.getDate(aArchieveDate);
        }
        else
        {
            ArchieveDate = null;
        }
    }

    public String getPigeOnHoleDate()
    {
        if (PigeOnHoleDate != null)
        {
            return fDate.getString(PigeOnHoleDate);
        }
        else
        {
            return null;
        }
    }

    public void setPigeOnHoleDate(Date aPigeOnHoleDate)
    {
        PigeOnHoleDate = aPigeOnHoleDate;
    }

    public void setPigeOnHoleDate(String aPigeOnHoleDate)
    {
        if (aPigeOnHoleDate != null && !aPigeOnHoleDate.equals(""))
        {
            PigeOnHoleDate = fDate.getDate(aPigeOnHoleDate);
        }
        else
        {
            PigeOnHoleDate = null;
        }
    }

    public String getArchType()
    {
        if (ArchType != null && !ArchType.equals("") && SysConst.CHANGECHARSET)
        {
            ArchType = StrTool.unicodeToGBK(ArchType);
        }
        return ArchType;
    }

    public void setArchType(String aArchType)
    {
        ArchType = aArchType;
    }

    public String getArchItem()
    {
        if (ArchItem != null && !ArchItem.equals("") && SysConst.CHANGECHARSET)
        {
            ArchItem = StrTool.unicodeToGBK(ArchItem);
        }
        return ArchItem;
    }

    public void setArchItem(String aArchItem)
    {
        ArchItem = aArchItem;
    }

    public String getDestoryDate()
    {
        if (DestoryDate != null)
        {
            return fDate.getString(DestoryDate);
        }
        else
        {
            return null;
        }
    }

    public void setDestoryDate(Date aDestoryDate)
    {
        DestoryDate = aDestoryDate;
    }

    public void setDestoryDate(String aDestoryDate)
    {
        if (aDestoryDate != null && !aDestoryDate.equals(""))
        {
            DestoryDate = fDate.getDate(aDestoryDate);
        }
        else
        {
            DestoryDate = null;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getItemCode()
    {
        if (ItemCode != null && !ItemCode.equals("") && SysConst.CHANGECHARSET)
        {
            ItemCode = StrTool.unicodeToGBK(ItemCode);
        }
        return ItemCode;
    }

    public void setItemCode(String aItemCode)
    {
        ItemCode = aItemCode;
    }

    /**
     * 使用另外一个 LAArchieveSchema 对象给 Schema 赋值
     * @param: aLAArchieveSchema LAArchieveSchema
     **/
    public void setSchema(LAArchieveSchema aLAArchieveSchema)
    {
        this.AgentCode = aLAArchieveSchema.getAgentCode();
        this.ArchieveNo = aLAArchieveSchema.getArchieveNo();
        this.ItemIdx = aLAArchieveSchema.getItemIdx();
        this.ArchieveDate = fDate.getDate(aLAArchieveSchema.getArchieveDate());
        this.PigeOnHoleDate = fDate.getDate(aLAArchieveSchema.getPigeOnHoleDate());
        this.ArchType = aLAArchieveSchema.getArchType();
        this.ArchItem = aLAArchieveSchema.getArchItem();
        this.DestoryDate = fDate.getDate(aLAArchieveSchema.getDestoryDate());
        this.Operator = aLAArchieveSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAArchieveSchema.getMakeDate());
        this.MakeTime = aLAArchieveSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAArchieveSchema.getModifyDate());
        this.ModifyTime = aLAArchieveSchema.getModifyTime();
        this.ItemCode = aLAArchieveSchema.getItemCode();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("ArchieveNo") == null)
            {
                this.ArchieveNo = null;
            }
            else
            {
                this.ArchieveNo = rs.getString("ArchieveNo").trim();
            }

            this.ItemIdx = rs.getInt("ItemIdx");
            this.ArchieveDate = rs.getDate("ArchieveDate");
            this.PigeOnHoleDate = rs.getDate("PigeOnHoleDate");
            if (rs.getString("ArchType") == null)
            {
                this.ArchType = null;
            }
            else
            {
                this.ArchType = rs.getString("ArchType").trim();
            }

            if (rs.getString("ArchItem") == null)
            {
                this.ArchItem = null;
            }
            else
            {
                this.ArchItem = rs.getString("ArchItem").trim();
            }

            this.DestoryDate = rs.getDate("DestoryDate");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ItemCode") == null)
            {
                this.ItemCode = null;
            }
            else
            {
                this.ItemCode = rs.getString("ItemCode").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAArchieveSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAArchieveSchema getSchema()
    {
        LAArchieveSchema aLAArchieveSchema = new LAArchieveSchema();
        aLAArchieveSchema.setSchema(this);
        return aLAArchieveSchema;
    }

    public LAArchieveDB getDB()
    {
        LAArchieveDB aDBOper = new LAArchieveDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAArchieve描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ArchieveNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ItemIdx) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ArchieveDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(PigeOnHoleDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ArchType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ArchItem)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(DestoryDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ItemCode));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAArchieve>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ArchieveNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ItemIdx = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).intValue();
            ArchieveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            PigeOnHoleDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            ArchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            ArchItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            DestoryDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAArchieveSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("ArchieveNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArchieveNo));
        }
        if (FCode.equals("ItemIdx"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ItemIdx));
        }
        if (FCode.equals("ArchieveDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getArchieveDate()));
        }
        if (FCode.equals("PigeOnHoleDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getPigeOnHoleDate()));
        }
        if (FCode.equals("ArchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArchType));
        }
        if (FCode.equals("ArchItem"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArchItem));
        }
        if (FCode.equals("DestoryDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getDestoryDate()));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("ItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ItemCode));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ArchieveNo);
                break;
            case 2:
                strFieldValue = String.valueOf(ItemIdx);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getArchieveDate()));
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getPigeOnHoleDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ArchType);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ArchItem);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getDestoryDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ItemCode);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("ArchieveNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ArchieveNo = FValue.trim();
            }
            else
            {
                ArchieveNo = null;
            }
        }
        if (FCode.equals("ItemIdx"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ItemIdx = i;
            }
        }
        if (FCode.equals("ArchieveDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ArchieveDate = fDate.getDate(FValue);
            }
            else
            {
                ArchieveDate = null;
            }
        }
        if (FCode.equals("PigeOnHoleDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PigeOnHoleDate = fDate.getDate(FValue);
            }
            else
            {
                PigeOnHoleDate = null;
            }
        }
        if (FCode.equals("ArchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ArchType = FValue.trim();
            }
            else
            {
                ArchType = null;
            }
        }
        if (FCode.equals("ArchItem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ArchItem = FValue.trim();
            }
            else
            {
                ArchItem = null;
            }
        }
        if (FCode.equals("DestoryDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestoryDate = fDate.getDate(FValue);
            }
            else
            {
                DestoryDate = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("ItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemCode = FValue.trim();
            }
            else
            {
                ItemCode = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAArchieveSchema other = (LAArchieveSchema) otherObject;
        return
                AgentCode.equals(other.getAgentCode())
                && ArchieveNo.equals(other.getArchieveNo())
                && ItemIdx == other.getItemIdx()
                && fDate.getString(ArchieveDate).equals(other.getArchieveDate())
                &&
                fDate.getString(PigeOnHoleDate).equals(other.getPigeOnHoleDate())
                && ArchType.equals(other.getArchType())
                && ArchItem.equals(other.getArchItem())
                && fDate.getString(DestoryDate).equals(other.getDestoryDate())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ItemCode.equals(other.getItemCode());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ArchieveNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ItemIdx"))
        {
            return 2;
        }
        if (strFieldName.equals("ArchieveDate"))
        {
            return 3;
        }
        if (strFieldName.equals("PigeOnHoleDate"))
        {
            return 4;
        }
        if (strFieldName.equals("ArchType"))
        {
            return 5;
        }
        if (strFieldName.equals("ArchItem"))
        {
            return 6;
        }
        if (strFieldName.equals("DestoryDate"))
        {
            return 7;
        }
        if (strFieldName.equals("Operator"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 9;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 10;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 11;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 12;
        }
        if (strFieldName.equals("ItemCode"))
        {
            return 13;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "ArchieveNo";
                break;
            case 2:
                strFieldName = "ItemIdx";
                break;
            case 3:
                strFieldName = "ArchieveDate";
                break;
            case 4:
                strFieldName = "PigeOnHoleDate";
                break;
            case 5:
                strFieldName = "ArchType";
                break;
            case 6:
                strFieldName = "ArchItem";
                break;
            case 7:
                strFieldName = "DestoryDate";
                break;
            case 8:
                strFieldName = "Operator";
                break;
            case 9:
                strFieldName = "MakeDate";
                break;
            case 10:
                strFieldName = "MakeTime";
                break;
            case 11:
                strFieldName = "ModifyDate";
                break;
            case 12:
                strFieldName = "ModifyTime";
                break;
            case 13:
                strFieldName = "ItemCode";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ArchieveNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemIdx"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ArchieveDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PigeOnHoleDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ArchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ArchItem"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestoryDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_INT;
                break;
            case 3:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
