/*
 * <p>ClassName: LMDutyGetSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 险种定义
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMDutyGetDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMDutyGetSchema implements Schema
{
    // @Field
    /** 给付代码 */
    private String GetDutyCode;
    /** 给付名称 */
    private String GetDutyName;
    /** 给付类型 */
    private String Type;
    /** 给付间隔 */
    private int GetIntv;
    /** 默认值 */
    private double DefaultVal;
    /** 算法 */
    private String CalCode;
    /** 反算算法 */
    private String CnterCalCode;
    /** 其他算法 */
    private String OthCalCode;
    /** 起领期间 */
    private int GetYear;
    /** 起领期间单位 */
    private String GetYearFlag;
    /** 起领日期计算参照 */
    private String StartDateCalRef;
    /** 起领日期计算方式 */
    private String StartDateCalMode;
    /** 起领期间上限 */
    private int MinGetStartPeriod;
    /** 止领期间 */
    private int GetEndPeriod;
    /** 止领期间单位 */
    private String GetEndUnit;
    /** 止领日期计算参照 */
    private String EndDateCalRef;
    /** 止领日期计算方式 */
    private String EndDateCalMode;
    /** 止领期间下限 */
    private int MaxGetEndPeriod;
    /** 递增标记 */
    private String AddFlag;
    /** 性别关联标记 */
    private String SexRelaFlag;
    /** 单位投保关联标记 */
    private String UnitAppRelaFlag;
    /** 算入保额标记 */
    private String AddAmntFlag;
    /** 现金领取标记 */
    private String DiscntFlag;
    /** 利率标记 */
    private String InterestFlag;
    /** 多被保人分享标记 */
    private String ShareFlag;
    /** 录入标记 */
    private String InpFlag;
    /** 受益人标记 */
    private String BnfFlag;
    /** 催付标记 */
    private String UrgeGetFlag;
    /** 被保人死亡后有效标记 */
    private String DeadValiFlag;
    /** 给付初始化标记 */
    private String GetInitFlag;
    /** 起付限 */
    private double GetLimit;
    /** 赔付限额 */
    private double MaxGet;
    /** 赔付比例 */
    private double GetRate;
    /** 是否和账户相关 */
    private String NeedAcc;
    /** 默认申请标志 */
    private String CanGet;
    /** 是否是账户结清后才能申请 */
    private String NeedCancelAcc;
    /** 给付分类1 */
    private String GetType1;
    /** 是否允许零值标记 */
    private String ZeroFlag;
    /** 给付分类2 */
    private String GetType2;

    public static final int FIELDNUM = 39; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMDutyGetSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "GetDutyCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGetDutyCode()
    {
        if (GetDutyCode != null && !GetDutyCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyCode = StrTool.unicodeToGBK(GetDutyCode);
        }
        return GetDutyCode;
    }

    public void setGetDutyCode(String aGetDutyCode)
    {
        GetDutyCode = aGetDutyCode;
    }

    public String getGetDutyName()
    {
        if (GetDutyName != null && !GetDutyName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetDutyName = StrTool.unicodeToGBK(GetDutyName);
        }
        return GetDutyName;
    }

    public void setGetDutyName(String aGetDutyName)
    {
        GetDutyName = aGetDutyName;
    }

    public String getType()
    {
        if (Type != null && !Type.equals("") && SysConst.CHANGECHARSET == true)
        {
            Type = StrTool.unicodeToGBK(Type);
        }
        return Type;
    }

    public void setType(String aType)
    {
        Type = aType;
    }

    public int getGetIntv()
    {
        return GetIntv;
    }

    public void setGetIntv(int aGetIntv)
    {
        GetIntv = aGetIntv;
    }

    public void setGetIntv(String aGetIntv)
    {
        if (aGetIntv != null && !aGetIntv.equals(""))
        {
            Integer tInteger = new Integer(aGetIntv);
            int i = tInteger.intValue();
            GetIntv = i;
        }
    }

    public double getDefaultVal()
    {
        return DefaultVal;
    }

    public void setDefaultVal(double aDefaultVal)
    {
        DefaultVal = aDefaultVal;
    }

    public void setDefaultVal(String aDefaultVal)
    {
        if (aDefaultVal != null && !aDefaultVal.equals(""))
        {
            Double tDouble = new Double(aDefaultVal);
            double d = tDouble.doubleValue();
            DefaultVal = d;
        }
    }

    public String getCalCode()
    {
        if (CalCode != null && !CalCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public String getCnterCalCode()
    {
        if (CnterCalCode != null && !CnterCalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CnterCalCode = StrTool.unicodeToGBK(CnterCalCode);
        }
        return CnterCalCode;
    }

    public void setCnterCalCode(String aCnterCalCode)
    {
        CnterCalCode = aCnterCalCode;
    }

    public String getOthCalCode()
    {
        if (OthCalCode != null && !OthCalCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OthCalCode = StrTool.unicodeToGBK(OthCalCode);
        }
        return OthCalCode;
    }

    public void setOthCalCode(String aOthCalCode)
    {
        OthCalCode = aOthCalCode;
    }

    public int getGetYear()
    {
        return GetYear;
    }

    public void setGetYear(int aGetYear)
    {
        GetYear = aGetYear;
    }

    public void setGetYear(String aGetYear)
    {
        if (aGetYear != null && !aGetYear.equals(""))
        {
            Integer tInteger = new Integer(aGetYear);
            int i = tInteger.intValue();
            GetYear = i;
        }
    }

    public String getGetYearFlag()
    {
        if (GetYearFlag != null && !GetYearFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetYearFlag = StrTool.unicodeToGBK(GetYearFlag);
        }
        return GetYearFlag;
    }

    public void setGetYearFlag(String aGetYearFlag)
    {
        GetYearFlag = aGetYearFlag;
    }

    public String getStartDateCalRef()
    {
        if (StartDateCalRef != null && !StartDateCalRef.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            StartDateCalRef = StrTool.unicodeToGBK(StartDateCalRef);
        }
        return StartDateCalRef;
    }

    public void setStartDateCalRef(String aStartDateCalRef)
    {
        StartDateCalRef = aStartDateCalRef;
    }

    public String getStartDateCalMode()
    {
        if (StartDateCalMode != null && !StartDateCalMode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            StartDateCalMode = StrTool.unicodeToGBK(StartDateCalMode);
        }
        return StartDateCalMode;
    }

    public void setStartDateCalMode(String aStartDateCalMode)
    {
        StartDateCalMode = aStartDateCalMode;
    }

    public int getMinGetStartPeriod()
    {
        return MinGetStartPeriod;
    }

    public void setMinGetStartPeriod(int aMinGetStartPeriod)
    {
        MinGetStartPeriod = aMinGetStartPeriod;
    }

    public void setMinGetStartPeriod(String aMinGetStartPeriod)
    {
        if (aMinGetStartPeriod != null && !aMinGetStartPeriod.equals(""))
        {
            Integer tInteger = new Integer(aMinGetStartPeriod);
            int i = tInteger.intValue();
            MinGetStartPeriod = i;
        }
    }

    public int getGetEndPeriod()
    {
        return GetEndPeriod;
    }

    public void setGetEndPeriod(int aGetEndPeriod)
    {
        GetEndPeriod = aGetEndPeriod;
    }

    public void setGetEndPeriod(String aGetEndPeriod)
    {
        if (aGetEndPeriod != null && !aGetEndPeriod.equals(""))
        {
            Integer tInteger = new Integer(aGetEndPeriod);
            int i = tInteger.intValue();
            GetEndPeriod = i;
        }
    }

    public String getGetEndUnit()
    {
        if (GetEndUnit != null && !GetEndUnit.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetEndUnit = StrTool.unicodeToGBK(GetEndUnit);
        }
        return GetEndUnit;
    }

    public void setGetEndUnit(String aGetEndUnit)
    {
        GetEndUnit = aGetEndUnit;
    }

    public String getEndDateCalRef()
    {
        if (EndDateCalRef != null && !EndDateCalRef.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EndDateCalRef = StrTool.unicodeToGBK(EndDateCalRef);
        }
        return EndDateCalRef;
    }

    public void setEndDateCalRef(String aEndDateCalRef)
    {
        EndDateCalRef = aEndDateCalRef;
    }

    public String getEndDateCalMode()
    {
        if (EndDateCalMode != null && !EndDateCalMode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            EndDateCalMode = StrTool.unicodeToGBK(EndDateCalMode);
        }
        return EndDateCalMode;
    }

    public void setEndDateCalMode(String aEndDateCalMode)
    {
        EndDateCalMode = aEndDateCalMode;
    }

    public int getMaxGetEndPeriod()
    {
        return MaxGetEndPeriod;
    }

    public void setMaxGetEndPeriod(int aMaxGetEndPeriod)
    {
        MaxGetEndPeriod = aMaxGetEndPeriod;
    }

    public void setMaxGetEndPeriod(String aMaxGetEndPeriod)
    {
        if (aMaxGetEndPeriod != null && !aMaxGetEndPeriod.equals(""))
        {
            Integer tInteger = new Integer(aMaxGetEndPeriod);
            int i = tInteger.intValue();
            MaxGetEndPeriod = i;
        }
    }

    public String getAddFlag()
    {
        if (AddFlag != null && !AddFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            AddFlag = StrTool.unicodeToGBK(AddFlag);
        }
        return AddFlag;
    }

    public void setAddFlag(String aAddFlag)
    {
        AddFlag = aAddFlag;
    }

    public String getSexRelaFlag()
    {
        if (SexRelaFlag != null && !SexRelaFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SexRelaFlag = StrTool.unicodeToGBK(SexRelaFlag);
        }
        return SexRelaFlag;
    }

    public void setSexRelaFlag(String aSexRelaFlag)
    {
        SexRelaFlag = aSexRelaFlag;
    }

    public String getUnitAppRelaFlag()
    {
        if (UnitAppRelaFlag != null && !UnitAppRelaFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UnitAppRelaFlag = StrTool.unicodeToGBK(UnitAppRelaFlag);
        }
        return UnitAppRelaFlag;
    }

    public void setUnitAppRelaFlag(String aUnitAppRelaFlag)
    {
        UnitAppRelaFlag = aUnitAppRelaFlag;
    }

    public String getAddAmntFlag()
    {
        if (AddAmntFlag != null && !AddAmntFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AddAmntFlag = StrTool.unicodeToGBK(AddAmntFlag);
        }
        return AddAmntFlag;
    }

    public void setAddAmntFlag(String aAddAmntFlag)
    {
        AddAmntFlag = aAddAmntFlag;
    }

    public String getDiscntFlag()
    {
        if (DiscntFlag != null && !DiscntFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DiscntFlag = StrTool.unicodeToGBK(DiscntFlag);
        }
        return DiscntFlag;
    }

    public void setDiscntFlag(String aDiscntFlag)
    {
        DiscntFlag = aDiscntFlag;
    }

    public String getInterestFlag()
    {
        if (InterestFlag != null && !InterestFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InterestFlag = StrTool.unicodeToGBK(InterestFlag);
        }
        return InterestFlag;
    }

    public void setInterestFlag(String aInterestFlag)
    {
        InterestFlag = aInterestFlag;
    }

    public String getShareFlag()
    {
        if (ShareFlag != null && !ShareFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ShareFlag = StrTool.unicodeToGBK(ShareFlag);
        }
        return ShareFlag;
    }

    public void setShareFlag(String aShareFlag)
    {
        ShareFlag = aShareFlag;
    }

    public String getInpFlag()
    {
        if (InpFlag != null && !InpFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            InpFlag = StrTool.unicodeToGBK(InpFlag);
        }
        return InpFlag;
    }

    public void setInpFlag(String aInpFlag)
    {
        InpFlag = aInpFlag;
    }

    public String getBnfFlag()
    {
        if (BnfFlag != null && !BnfFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            BnfFlag = StrTool.unicodeToGBK(BnfFlag);
        }
        return BnfFlag;
    }

    public void setBnfFlag(String aBnfFlag)
    {
        BnfFlag = aBnfFlag;
    }

    public String getUrgeGetFlag()
    {
        if (UrgeGetFlag != null && !UrgeGetFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UrgeGetFlag = StrTool.unicodeToGBK(UrgeGetFlag);
        }
        return UrgeGetFlag;
    }

    public void setUrgeGetFlag(String aUrgeGetFlag)
    {
        UrgeGetFlag = aUrgeGetFlag;
    }

    public String getDeadValiFlag()
    {
        if (DeadValiFlag != null && !DeadValiFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DeadValiFlag = StrTool.unicodeToGBK(DeadValiFlag);
        }
        return DeadValiFlag;
    }

    public void setDeadValiFlag(String aDeadValiFlag)
    {
        DeadValiFlag = aDeadValiFlag;
    }

    public String getGetInitFlag()
    {
        if (GetInitFlag != null && !GetInitFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            GetInitFlag = StrTool.unicodeToGBK(GetInitFlag);
        }
        return GetInitFlag;
    }

    public void setGetInitFlag(String aGetInitFlag)
    {
        GetInitFlag = aGetInitFlag;
    }

    public double getGetLimit()
    {
        return GetLimit;
    }

    public void setGetLimit(double aGetLimit)
    {
        GetLimit = aGetLimit;
    }

    public void setGetLimit(String aGetLimit)
    {
        if (aGetLimit != null && !aGetLimit.equals(""))
        {
            Double tDouble = new Double(aGetLimit);
            double d = tDouble.doubleValue();
            GetLimit = d;
        }
    }

    public double getMaxGet()
    {
        return MaxGet;
    }

    public void setMaxGet(double aMaxGet)
    {
        MaxGet = aMaxGet;
    }

    public void setMaxGet(String aMaxGet)
    {
        if (aMaxGet != null && !aMaxGet.equals(""))
        {
            Double tDouble = new Double(aMaxGet);
            double d = tDouble.doubleValue();
            MaxGet = d;
        }
    }

    public double getGetRate()
    {
        return GetRate;
    }

    public void setGetRate(double aGetRate)
    {
        GetRate = aGetRate;
    }

    public void setGetRate(String aGetRate)
    {
        if (aGetRate != null && !aGetRate.equals(""))
        {
            Double tDouble = new Double(aGetRate);
            double d = tDouble.doubleValue();
            GetRate = d;
        }
    }

    public String getNeedAcc()
    {
        if (NeedAcc != null && !NeedAcc.equals("") && SysConst.CHANGECHARSET == true)
        {
            NeedAcc = StrTool.unicodeToGBK(NeedAcc);
        }
        return NeedAcc;
    }

    public void setNeedAcc(String aNeedAcc)
    {
        NeedAcc = aNeedAcc;
    }

    public String getCanGet()
    {
        if (CanGet != null && !CanGet.equals("") && SysConst.CHANGECHARSET == true)
        {
            CanGet = StrTool.unicodeToGBK(CanGet);
        }
        return CanGet;
    }

    public void setCanGet(String aCanGet)
    {
        CanGet = aCanGet;
    }

    public String getNeedCancelAcc()
    {
        if (NeedCancelAcc != null && !NeedCancelAcc.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            NeedCancelAcc = StrTool.unicodeToGBK(NeedCancelAcc);
        }
        return NeedCancelAcc;
    }

    public void setNeedCancelAcc(String aNeedCancelAcc)
    {
        NeedCancelAcc = aNeedCancelAcc;
    }

    public String getGetType1()
    {
        if (GetType1 != null && !GetType1.equals("") && SysConst.CHANGECHARSET == true)
        {
            GetType1 = StrTool.unicodeToGBK(GetType1);
        }
        return GetType1;
    }

    public void setGetType1(String aGetType1)
    {
        GetType1 = aGetType1;
    }

    public String getZeroFlag()
    {
        if (ZeroFlag != null && !ZeroFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            ZeroFlag = StrTool.unicodeToGBK(ZeroFlag);
        }
        return ZeroFlag;
    }

    public void setZeroFlag(String aZeroFlag)
    {
        ZeroFlag = aZeroFlag;
    }

    public String getGetType2()
    {
        if (GetType2 != null && !GetType2.equals("") && SysConst.CHANGECHARSET == true)
        {
            GetType2 = StrTool.unicodeToGBK(GetType2);
        }
        return GetType2;
    }

    public void setGetType2(String aGetType2)
    {
        GetType2 = aGetType2;
    }

    /**
     * 使用另外一个 LMDutyGetSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMDutyGetSchema aLMDutyGetSchema)
    {
        this.GetDutyCode = aLMDutyGetSchema.getGetDutyCode();
        this.GetDutyName = aLMDutyGetSchema.getGetDutyName();
        this.Type = aLMDutyGetSchema.getType();
        this.GetIntv = aLMDutyGetSchema.getGetIntv();
        this.DefaultVal = aLMDutyGetSchema.getDefaultVal();
        this.CalCode = aLMDutyGetSchema.getCalCode();
        this.CnterCalCode = aLMDutyGetSchema.getCnterCalCode();
        this.OthCalCode = aLMDutyGetSchema.getOthCalCode();
        this.GetYear = aLMDutyGetSchema.getGetYear();
        this.GetYearFlag = aLMDutyGetSchema.getGetYearFlag();
        this.StartDateCalRef = aLMDutyGetSchema.getStartDateCalRef();
        this.StartDateCalMode = aLMDutyGetSchema.getStartDateCalMode();
        this.MinGetStartPeriod = aLMDutyGetSchema.getMinGetStartPeriod();
        this.GetEndPeriod = aLMDutyGetSchema.getGetEndPeriod();
        this.GetEndUnit = aLMDutyGetSchema.getGetEndUnit();
        this.EndDateCalRef = aLMDutyGetSchema.getEndDateCalRef();
        this.EndDateCalMode = aLMDutyGetSchema.getEndDateCalMode();
        this.MaxGetEndPeriod = aLMDutyGetSchema.getMaxGetEndPeriod();
        this.AddFlag = aLMDutyGetSchema.getAddFlag();
        this.SexRelaFlag = aLMDutyGetSchema.getSexRelaFlag();
        this.UnitAppRelaFlag = aLMDutyGetSchema.getUnitAppRelaFlag();
        this.AddAmntFlag = aLMDutyGetSchema.getAddAmntFlag();
        this.DiscntFlag = aLMDutyGetSchema.getDiscntFlag();
        this.InterestFlag = aLMDutyGetSchema.getInterestFlag();
        this.ShareFlag = aLMDutyGetSchema.getShareFlag();
        this.InpFlag = aLMDutyGetSchema.getInpFlag();
        this.BnfFlag = aLMDutyGetSchema.getBnfFlag();
        this.UrgeGetFlag = aLMDutyGetSchema.getUrgeGetFlag();
        this.DeadValiFlag = aLMDutyGetSchema.getDeadValiFlag();
        this.GetInitFlag = aLMDutyGetSchema.getGetInitFlag();
        this.GetLimit = aLMDutyGetSchema.getGetLimit();
        this.MaxGet = aLMDutyGetSchema.getMaxGet();
        this.GetRate = aLMDutyGetSchema.getGetRate();
        this.NeedAcc = aLMDutyGetSchema.getNeedAcc();
        this.CanGet = aLMDutyGetSchema.getCanGet();
        this.NeedCancelAcc = aLMDutyGetSchema.getNeedCancelAcc();
        this.GetType1 = aLMDutyGetSchema.getGetType1();
        this.ZeroFlag = aLMDutyGetSchema.getZeroFlag();
        this.GetType2 = aLMDutyGetSchema.getGetType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GetDutyCode") == null)
            {
                this.GetDutyCode = null;
            }
            else
            {
                this.GetDutyCode = rs.getString("GetDutyCode").trim();
            }

            if (rs.getString("GetDutyName") == null)
            {
                this.GetDutyName = null;
            }
            else
            {
                this.GetDutyName = rs.getString("GetDutyName").trim();
            }

            if (rs.getString("Type") == null)
            {
                this.Type = null;
            }
            else
            {
                this.Type = rs.getString("Type").trim();
            }

            this.GetIntv = rs.getInt("GetIntv");
            this.DefaultVal = rs.getDouble("DefaultVal");
            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("CnterCalCode") == null)
            {
                this.CnterCalCode = null;
            }
            else
            {
                this.CnterCalCode = rs.getString("CnterCalCode").trim();
            }

            if (rs.getString("OthCalCode") == null)
            {
                this.OthCalCode = null;
            }
            else
            {
                this.OthCalCode = rs.getString("OthCalCode").trim();
            }

            this.GetYear = rs.getInt("GetYear");
            if (rs.getString("GetYearFlag") == null)
            {
                this.GetYearFlag = null;
            }
            else
            {
                this.GetYearFlag = rs.getString("GetYearFlag").trim();
            }

            if (rs.getString("StartDateCalRef") == null)
            {
                this.StartDateCalRef = null;
            }
            else
            {
                this.StartDateCalRef = rs.getString("StartDateCalRef").trim();
            }

            if (rs.getString("StartDateCalMode") == null)
            {
                this.StartDateCalMode = null;
            }
            else
            {
                this.StartDateCalMode = rs.getString("StartDateCalMode").trim();
            }

            this.MinGetStartPeriod = rs.getInt("MinGetStartPeriod");
            this.GetEndPeriod = rs.getInt("GetEndPeriod");
            if (rs.getString("GetEndUnit") == null)
            {
                this.GetEndUnit = null;
            }
            else
            {
                this.GetEndUnit = rs.getString("GetEndUnit").trim();
            }

            if (rs.getString("EndDateCalRef") == null)
            {
                this.EndDateCalRef = null;
            }
            else
            {
                this.EndDateCalRef = rs.getString("EndDateCalRef").trim();
            }

            if (rs.getString("EndDateCalMode") == null)
            {
                this.EndDateCalMode = null;
            }
            else
            {
                this.EndDateCalMode = rs.getString("EndDateCalMode").trim();
            }

            this.MaxGetEndPeriod = rs.getInt("MaxGetEndPeriod");
            if (rs.getString("AddFlag") == null)
            {
                this.AddFlag = null;
            }
            else
            {
                this.AddFlag = rs.getString("AddFlag").trim();
            }

            if (rs.getString("SexRelaFlag") == null)
            {
                this.SexRelaFlag = null;
            }
            else
            {
                this.SexRelaFlag = rs.getString("SexRelaFlag").trim();
            }

            if (rs.getString("UnitAppRelaFlag") == null)
            {
                this.UnitAppRelaFlag = null;
            }
            else
            {
                this.UnitAppRelaFlag = rs.getString("UnitAppRelaFlag").trim();
            }

            if (rs.getString("AddAmntFlag") == null)
            {
                this.AddAmntFlag = null;
            }
            else
            {
                this.AddAmntFlag = rs.getString("AddAmntFlag").trim();
            }

            if (rs.getString("DiscntFlag") == null)
            {
                this.DiscntFlag = null;
            }
            else
            {
                this.DiscntFlag = rs.getString("DiscntFlag").trim();
            }

            if (rs.getString("InterestFlag") == null)
            {
                this.InterestFlag = null;
            }
            else
            {
                this.InterestFlag = rs.getString("InterestFlag").trim();
            }

            if (rs.getString("ShareFlag") == null)
            {
                this.ShareFlag = null;
            }
            else
            {
                this.ShareFlag = rs.getString("ShareFlag").trim();
            }

            if (rs.getString("InpFlag") == null)
            {
                this.InpFlag = null;
            }
            else
            {
                this.InpFlag = rs.getString("InpFlag").trim();
            }

            if (rs.getString("BnfFlag") == null)
            {
                this.BnfFlag = null;
            }
            else
            {
                this.BnfFlag = rs.getString("BnfFlag").trim();
            }

            if (rs.getString("UrgeGetFlag") == null)
            {
                this.UrgeGetFlag = null;
            }
            else
            {
                this.UrgeGetFlag = rs.getString("UrgeGetFlag").trim();
            }

            if (rs.getString("DeadValiFlag") == null)
            {
                this.DeadValiFlag = null;
            }
            else
            {
                this.DeadValiFlag = rs.getString("DeadValiFlag").trim();
            }

            if (rs.getString("GetInitFlag") == null)
            {
                this.GetInitFlag = null;
            }
            else
            {
                this.GetInitFlag = rs.getString("GetInitFlag").trim();
            }

            this.GetLimit = rs.getDouble("GetLimit");
            this.MaxGet = rs.getDouble("MaxGet");
            this.GetRate = rs.getDouble("GetRate");
            if (rs.getString("NeedAcc") == null)
            {
                this.NeedAcc = null;
            }
            else
            {
                this.NeedAcc = rs.getString("NeedAcc").trim();
            }

            if (rs.getString("CanGet") == null)
            {
                this.CanGet = null;
            }
            else
            {
                this.CanGet = rs.getString("CanGet").trim();
            }

            if (rs.getString("NeedCancelAcc") == null)
            {
                this.NeedCancelAcc = null;
            }
            else
            {
                this.NeedCancelAcc = rs.getString("NeedCancelAcc").trim();
            }

            if (rs.getString("GetType1") == null)
            {
                this.GetType1 = null;
            }
            else
            {
                this.GetType1 = rs.getString("GetType1").trim();
            }

            if (rs.getString("ZeroFlag") == null)
            {
                this.ZeroFlag = null;
            }
            else
            {
                this.ZeroFlag = rs.getString("ZeroFlag").trim();
            }

            if (rs.getString("GetType2") == null)
            {
                this.GetType2 = null;
            }
            else
            {
                this.GetType2 = rs.getString("GetType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMDutyGetSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMDutyGetSchema getSchema()
    {
        LMDutyGetSchema aLMDutyGetSchema = new LMDutyGetSchema();
        aLMDutyGetSchema.setSchema(this);
        return aLMDutyGetSchema;
    }

    public LMDutyGetDB getDB()
    {
        LMDutyGetDB aDBOper = new LMDutyGetDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMDutyGet描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(GetDutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetDutyName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Type)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetIntv) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(DefaultVal) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CnterCalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OthCalCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetYear) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetYearFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StartDateCalRef)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StartDateCalMode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(MinGetStartPeriod) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetEndPeriod) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetEndUnit)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EndDateCalRef)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EndDateCalMode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(MaxGetEndPeriod) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AddFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SexRelaFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UnitAppRelaFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AddAmntFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DiscntFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InterestFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ShareFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InpFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BnfFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UrgeGetFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DeadValiFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetInitFlag)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetLimit) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(MaxGet) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(GetRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NeedAcc)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CanGet)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NeedCancelAcc)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetType1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ZeroFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetType2));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMDutyGet>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            GetDutyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            Type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                  SysConst.PACKAGESPILTER);
            GetIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            DefaultVal = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            CnterCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            OthCalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            GetYear = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            GetYearFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                         SysConst.PACKAGESPILTER);
            StartDateCalRef = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             11, SysConst.PACKAGESPILTER);
            StartDateCalMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              12, SysConst.PACKAGESPILTER);
            MinGetStartPeriod = new Integer(ChgData.chgNumericStr(StrTool.
                    getStr(strMessage, 13, SysConst.PACKAGESPILTER))).intValue();
            GetEndPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).intValue();
            GetEndUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            EndDateCalRef = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                           SysConst.PACKAGESPILTER);
            EndDateCalMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            17, SysConst.PACKAGESPILTER);
            MaxGetEndPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).intValue();
            AddFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                     SysConst.PACKAGESPILTER);
            SexRelaFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                         SysConst.PACKAGESPILTER);
            UnitAppRelaFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             21, SysConst.PACKAGESPILTER);
            AddAmntFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                         SysConst.PACKAGESPILTER);
            DiscntFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                        SysConst.PACKAGESPILTER);
            InterestFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                          SysConst.PACKAGESPILTER);
            ShareFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                       SysConst.PACKAGESPILTER);
            InpFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                     SysConst.PACKAGESPILTER);
            BnfFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                     SysConst.PACKAGESPILTER);
            UrgeGetFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                         SysConst.PACKAGESPILTER);
            DeadValiFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                          SysConst.PACKAGESPILTER);
            GetInitFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                         SysConst.PACKAGESPILTER);
            GetLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 31, SysConst.PACKAGESPILTER))).doubleValue();
            MaxGet = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    32, SysConst.PACKAGESPILTER))).doubleValue();
            GetRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 33, SysConst.PACKAGESPILTER))).doubleValue();
            NeedAcc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                     SysConst.PACKAGESPILTER);
            CanGet = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,
                                    SysConst.PACKAGESPILTER);
            NeedCancelAcc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,
                                           SysConst.PACKAGESPILTER);
            GetType1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,
                                      SysConst.PACKAGESPILTER);
            ZeroFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,
                                      SysConst.PACKAGESPILTER);
            GetType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMDutyGetSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GetDutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyCode));
        }
        if (FCode.equals("GetDutyName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetDutyName));
        }
        if (FCode.equals("Type"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Type));
        }
        if (FCode.equals("GetIntv"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetIntv));
        }
        if (FCode.equals("DefaultVal"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DefaultVal));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCode));
        }
        if (FCode.equals("CnterCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CnterCalCode));
        }
        if (FCode.equals("OthCalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OthCalCode));
        }
        if (FCode.equals("GetYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetYear));
        }
        if (FCode.equals("GetYearFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetYearFlag));
        }
        if (FCode.equals("StartDateCalRef"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StartDateCalRef));
        }
        if (FCode.equals("StartDateCalMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StartDateCalMode));
        }
        if (FCode.equals("MinGetStartPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(
                    MinGetStartPeriod));
        }
        if (FCode.equals("GetEndPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetEndPeriod));
        }
        if (FCode.equals("GetEndUnit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetEndUnit));
        }
        if (FCode.equals("EndDateCalRef"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndDateCalRef));
        }
        if (FCode.equals("EndDateCalMode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndDateCalMode));
        }
        if (FCode.equals("MaxGetEndPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MaxGetEndPeriod));
        }
        if (FCode.equals("AddFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AddFlag));
        }
        if (FCode.equals("SexRelaFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SexRelaFlag));
        }
        if (FCode.equals("UnitAppRelaFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UnitAppRelaFlag));
        }
        if (FCode.equals("AddAmntFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AddAmntFlag));
        }
        if (FCode.equals("DiscntFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DiscntFlag));
        }
        if (FCode.equals("InterestFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InterestFlag));
        }
        if (FCode.equals("ShareFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ShareFlag));
        }
        if (FCode.equals("InpFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InpFlag));
        }
        if (FCode.equals("BnfFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BnfFlag));
        }
        if (FCode.equals("UrgeGetFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UrgeGetFlag));
        }
        if (FCode.equals("DeadValiFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DeadValiFlag));
        }
        if (FCode.equals("GetInitFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetInitFlag));
        }
        if (FCode.equals("GetLimit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetLimit));
        }
        if (FCode.equals("MaxGet"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MaxGet));
        }
        if (FCode.equals("GetRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetRate));
        }
        if (FCode.equals("NeedAcc"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NeedAcc));
        }
        if (FCode.equals("CanGet"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CanGet));
        }
        if (FCode.equals("NeedCancelAcc"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NeedCancelAcc));
        }
        if (FCode.equals("GetType1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetType1));
        }
        if (FCode.equals("ZeroFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ZeroFlag));
        }
        if (FCode.equals("GetType2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(GetDutyName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Type);
                break;
            case 3:
                strFieldValue = String.valueOf(GetIntv);
                break;
            case 4:
                strFieldValue = String.valueOf(DefaultVal);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CnterCalCode);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(OthCalCode);
                break;
            case 8:
                strFieldValue = String.valueOf(GetYear);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(GetYearFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(StartDateCalRef);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(StartDateCalMode);
                break;
            case 12:
                strFieldValue = String.valueOf(MinGetStartPeriod);
                break;
            case 13:
                strFieldValue = String.valueOf(GetEndPeriod);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(GetEndUnit);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(EndDateCalRef);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(EndDateCalMode);
                break;
            case 17:
                strFieldValue = String.valueOf(MaxGetEndPeriod);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(AddFlag);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(SexRelaFlag);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(UnitAppRelaFlag);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(AddAmntFlag);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(DiscntFlag);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(InterestFlag);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(ShareFlag);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(InpFlag);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(BnfFlag);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(UrgeGetFlag);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(DeadValiFlag);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(GetInitFlag);
                break;
            case 30:
                strFieldValue = String.valueOf(GetLimit);
                break;
            case 31:
                strFieldValue = String.valueOf(MaxGet);
                break;
            case 32:
                strFieldValue = String.valueOf(GetRate);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(NeedAcc);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(CanGet);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(NeedCancelAcc);
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(GetType1);
                break;
            case 37:
                strFieldValue = StrTool.GBKToUnicode(ZeroFlag);
                break;
            case 38:
                strFieldValue = StrTool.GBKToUnicode(GetType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GetDutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyCode = FValue.trim();
            }
            else
            {
                GetDutyCode = null;
            }
        }
        if (FCode.equals("GetDutyName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDutyName = FValue.trim();
            }
            else
            {
                GetDutyName = null;
            }
        }
        if (FCode.equals("Type"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Type = FValue.trim();
            }
            else
            {
                Type = null;
            }
        }
        if (FCode.equals("GetIntv"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                GetIntv = i;
            }
        }
        if (FCode.equals("DefaultVal"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DefaultVal = d;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("CnterCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CnterCalCode = FValue.trim();
            }
            else
            {
                CnterCalCode = null;
            }
        }
        if (FCode.equals("OthCalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OthCalCode = FValue.trim();
            }
            else
            {
                OthCalCode = null;
            }
        }
        if (FCode.equals("GetYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                GetYear = i;
            }
        }
        if (FCode.equals("GetYearFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetYearFlag = FValue.trim();
            }
            else
            {
                GetYearFlag = null;
            }
        }
        if (FCode.equals("StartDateCalRef"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDateCalRef = FValue.trim();
            }
            else
            {
                StartDateCalRef = null;
            }
        }
        if (FCode.equals("StartDateCalMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDateCalMode = FValue.trim();
            }
            else
            {
                StartDateCalMode = null;
            }
        }
        if (FCode.equals("MinGetStartPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MinGetStartPeriod = i;
            }
        }
        if (FCode.equals("GetEndPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                GetEndPeriod = i;
            }
        }
        if (FCode.equals("GetEndUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetEndUnit = FValue.trim();
            }
            else
            {
                GetEndUnit = null;
            }
        }
        if (FCode.equals("EndDateCalRef"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDateCalRef = FValue.trim();
            }
            else
            {
                EndDateCalRef = null;
            }
        }
        if (FCode.equals("EndDateCalMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndDateCalMode = FValue.trim();
            }
            else
            {
                EndDateCalMode = null;
            }
        }
        if (FCode.equals("MaxGetEndPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MaxGetEndPeriod = i;
            }
        }
        if (FCode.equals("AddFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AddFlag = FValue.trim();
            }
            else
            {
                AddFlag = null;
            }
        }
        if (FCode.equals("SexRelaFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SexRelaFlag = FValue.trim();
            }
            else
            {
                SexRelaFlag = null;
            }
        }
        if (FCode.equals("UnitAppRelaFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UnitAppRelaFlag = FValue.trim();
            }
            else
            {
                UnitAppRelaFlag = null;
            }
        }
        if (FCode.equals("AddAmntFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AddAmntFlag = FValue.trim();
            }
            else
            {
                AddAmntFlag = null;
            }
        }
        if (FCode.equals("DiscntFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DiscntFlag = FValue.trim();
            }
            else
            {
                DiscntFlag = null;
            }
        }
        if (FCode.equals("InterestFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InterestFlag = FValue.trim();
            }
            else
            {
                InterestFlag = null;
            }
        }
        if (FCode.equals("ShareFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ShareFlag = FValue.trim();
            }
            else
            {
                ShareFlag = null;
            }
        }
        if (FCode.equals("InpFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InpFlag = FValue.trim();
            }
            else
            {
                InpFlag = null;
            }
        }
        if (FCode.equals("BnfFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BnfFlag = FValue.trim();
            }
            else
            {
                BnfFlag = null;
            }
        }
        if (FCode.equals("UrgeGetFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UrgeGetFlag = FValue.trim();
            }
            else
            {
                UrgeGetFlag = null;
            }
        }
        if (FCode.equals("DeadValiFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeadValiFlag = FValue.trim();
            }
            else
            {
                DeadValiFlag = null;
            }
        }
        if (FCode.equals("GetInitFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetInitFlag = FValue.trim();
            }
            else
            {
                GetInitFlag = null;
            }
        }
        if (FCode.equals("GetLimit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetLimit = d;
            }
        }
        if (FCode.equals("MaxGet"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MaxGet = d;
            }
        }
        if (FCode.equals("GetRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetRate = d;
            }
        }
        if (FCode.equals("NeedAcc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NeedAcc = FValue.trim();
            }
            else
            {
                NeedAcc = null;
            }
        }
        if (FCode.equals("CanGet"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CanGet = FValue.trim();
            }
            else
            {
                CanGet = null;
            }
        }
        if (FCode.equals("NeedCancelAcc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NeedCancelAcc = FValue.trim();
            }
            else
            {
                NeedCancelAcc = null;
            }
        }
        if (FCode.equals("GetType1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetType1 = FValue.trim();
            }
            else
            {
                GetType1 = null;
            }
        }
        if (FCode.equals("ZeroFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ZeroFlag = FValue.trim();
            }
            else
            {
                ZeroFlag = null;
            }
        }
        if (FCode.equals("GetType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetType2 = FValue.trim();
            }
            else
            {
                GetType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMDutyGetSchema other = (LMDutyGetSchema) otherObject;
        return
                GetDutyCode.equals(other.getGetDutyCode())
                && GetDutyName.equals(other.getGetDutyName())
                && Type.equals(other.getType())
                && GetIntv == other.getGetIntv()
                && DefaultVal == other.getDefaultVal()
                && CalCode.equals(other.getCalCode())
                && CnterCalCode.equals(other.getCnterCalCode())
                && OthCalCode.equals(other.getOthCalCode())
                && GetYear == other.getGetYear()
                && GetYearFlag.equals(other.getGetYearFlag())
                && StartDateCalRef.equals(other.getStartDateCalRef())
                && StartDateCalMode.equals(other.getStartDateCalMode())
                && MinGetStartPeriod == other.getMinGetStartPeriod()
                && GetEndPeriod == other.getGetEndPeriod()
                && GetEndUnit.equals(other.getGetEndUnit())
                && EndDateCalRef.equals(other.getEndDateCalRef())
                && EndDateCalMode.equals(other.getEndDateCalMode())
                && MaxGetEndPeriod == other.getMaxGetEndPeriod()
                && AddFlag.equals(other.getAddFlag())
                && SexRelaFlag.equals(other.getSexRelaFlag())
                && UnitAppRelaFlag.equals(other.getUnitAppRelaFlag())
                && AddAmntFlag.equals(other.getAddAmntFlag())
                && DiscntFlag.equals(other.getDiscntFlag())
                && InterestFlag.equals(other.getInterestFlag())
                && ShareFlag.equals(other.getShareFlag())
                && InpFlag.equals(other.getInpFlag())
                && BnfFlag.equals(other.getBnfFlag())
                && UrgeGetFlag.equals(other.getUrgeGetFlag())
                && DeadValiFlag.equals(other.getDeadValiFlag())
                && GetInitFlag.equals(other.getGetInitFlag())
                && GetLimit == other.getGetLimit()
                && MaxGet == other.getMaxGet()
                && GetRate == other.getGetRate()
                && NeedAcc.equals(other.getNeedAcc())
                && CanGet.equals(other.getCanGet())
                && NeedCancelAcc.equals(other.getNeedCancelAcc())
                && GetType1.equals(other.getGetType1())
                && ZeroFlag.equals(other.getZeroFlag())
                && GetType2.equals(other.getGetType2());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GetDutyCode"))
        {
            return 0;
        }
        if (strFieldName.equals("GetDutyName"))
        {
            return 1;
        }
        if (strFieldName.equals("Type"))
        {
            return 2;
        }
        if (strFieldName.equals("GetIntv"))
        {
            return 3;
        }
        if (strFieldName.equals("DefaultVal"))
        {
            return 4;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 5;
        }
        if (strFieldName.equals("CnterCalCode"))
        {
            return 6;
        }
        if (strFieldName.equals("OthCalCode"))
        {
            return 7;
        }
        if (strFieldName.equals("GetYear"))
        {
            return 8;
        }
        if (strFieldName.equals("GetYearFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("StartDateCalRef"))
        {
            return 10;
        }
        if (strFieldName.equals("StartDateCalMode"))
        {
            return 11;
        }
        if (strFieldName.equals("MinGetStartPeriod"))
        {
            return 12;
        }
        if (strFieldName.equals("GetEndPeriod"))
        {
            return 13;
        }
        if (strFieldName.equals("GetEndUnit"))
        {
            return 14;
        }
        if (strFieldName.equals("EndDateCalRef"))
        {
            return 15;
        }
        if (strFieldName.equals("EndDateCalMode"))
        {
            return 16;
        }
        if (strFieldName.equals("MaxGetEndPeriod"))
        {
            return 17;
        }
        if (strFieldName.equals("AddFlag"))
        {
            return 18;
        }
        if (strFieldName.equals("SexRelaFlag"))
        {
            return 19;
        }
        if (strFieldName.equals("UnitAppRelaFlag"))
        {
            return 20;
        }
        if (strFieldName.equals("AddAmntFlag"))
        {
            return 21;
        }
        if (strFieldName.equals("DiscntFlag"))
        {
            return 22;
        }
        if (strFieldName.equals("InterestFlag"))
        {
            return 23;
        }
        if (strFieldName.equals("ShareFlag"))
        {
            return 24;
        }
        if (strFieldName.equals("InpFlag"))
        {
            return 25;
        }
        if (strFieldName.equals("BnfFlag"))
        {
            return 26;
        }
        if (strFieldName.equals("UrgeGetFlag"))
        {
            return 27;
        }
        if (strFieldName.equals("DeadValiFlag"))
        {
            return 28;
        }
        if (strFieldName.equals("GetInitFlag"))
        {
            return 29;
        }
        if (strFieldName.equals("GetLimit"))
        {
            return 30;
        }
        if (strFieldName.equals("MaxGet"))
        {
            return 31;
        }
        if (strFieldName.equals("GetRate"))
        {
            return 32;
        }
        if (strFieldName.equals("NeedAcc"))
        {
            return 33;
        }
        if (strFieldName.equals("CanGet"))
        {
            return 34;
        }
        if (strFieldName.equals("NeedCancelAcc"))
        {
            return 35;
        }
        if (strFieldName.equals("GetType1"))
        {
            return 36;
        }
        if (strFieldName.equals("ZeroFlag"))
        {
            return 37;
        }
        if (strFieldName.equals("GetType2"))
        {
            return 38;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GetDutyCode";
                break;
            case 1:
                strFieldName = "GetDutyName";
                break;
            case 2:
                strFieldName = "Type";
                break;
            case 3:
                strFieldName = "GetIntv";
                break;
            case 4:
                strFieldName = "DefaultVal";
                break;
            case 5:
                strFieldName = "CalCode";
                break;
            case 6:
                strFieldName = "CnterCalCode";
                break;
            case 7:
                strFieldName = "OthCalCode";
                break;
            case 8:
                strFieldName = "GetYear";
                break;
            case 9:
                strFieldName = "GetYearFlag";
                break;
            case 10:
                strFieldName = "StartDateCalRef";
                break;
            case 11:
                strFieldName = "StartDateCalMode";
                break;
            case 12:
                strFieldName = "MinGetStartPeriod";
                break;
            case 13:
                strFieldName = "GetEndPeriod";
                break;
            case 14:
                strFieldName = "GetEndUnit";
                break;
            case 15:
                strFieldName = "EndDateCalRef";
                break;
            case 16:
                strFieldName = "EndDateCalMode";
                break;
            case 17:
                strFieldName = "MaxGetEndPeriod";
                break;
            case 18:
                strFieldName = "AddFlag";
                break;
            case 19:
                strFieldName = "SexRelaFlag";
                break;
            case 20:
                strFieldName = "UnitAppRelaFlag";
                break;
            case 21:
                strFieldName = "AddAmntFlag";
                break;
            case 22:
                strFieldName = "DiscntFlag";
                break;
            case 23:
                strFieldName = "InterestFlag";
                break;
            case 24:
                strFieldName = "ShareFlag";
                break;
            case 25:
                strFieldName = "InpFlag";
                break;
            case 26:
                strFieldName = "BnfFlag";
                break;
            case 27:
                strFieldName = "UrgeGetFlag";
                break;
            case 28:
                strFieldName = "DeadValiFlag";
                break;
            case 29:
                strFieldName = "GetInitFlag";
                break;
            case 30:
                strFieldName = "GetLimit";
                break;
            case 31:
                strFieldName = "MaxGet";
                break;
            case 32:
                strFieldName = "GetRate";
                break;
            case 33:
                strFieldName = "NeedAcc";
                break;
            case 34:
                strFieldName = "CanGet";
                break;
            case 35:
                strFieldName = "NeedCancelAcc";
                break;
            case 36:
                strFieldName = "GetType1";
                break;
            case 37:
                strFieldName = "ZeroFlag";
                break;
            case 38:
                strFieldName = "GetType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GetDutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDutyName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Type"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetIntv"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("DefaultVal"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CnterCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OthCalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetYear"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GetYearFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDateCalRef"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDateCalMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MinGetStartPeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GetEndPeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GetEndUnit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndDateCalRef"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndDateCalMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MaxGetEndPeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AddFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SexRelaFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UnitAppRelaFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AddAmntFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DiscntFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterestFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ShareFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InpFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BnfFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UrgeGetFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeadValiFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetInitFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetLimit"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MaxGet"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("NeedAcc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CanGet"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NeedCancelAcc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetType1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZeroFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_INT;
                break;
            case 13:
                nFieldType = Schema.TYPE_INT;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_INT;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 31:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 32:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 33:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 34:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 35:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 36:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 37:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 38:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
