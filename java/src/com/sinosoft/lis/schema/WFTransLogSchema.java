/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.WFTransLogDB;

/*
 * <p>ClassName: WFTransLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2010-06-02
 */
public class WFTransLogSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 批次交易类型 */
	private String MessageType;
	/** 交易日期 */
	private Date SendDate;
	/** 交易时间 */
	private String SendTime;
	/** 批次交易状态 */
	private String BatchFlag;
	/** 操作员 */
	private String opertator;
	/** 错误编码 */
	private String ErrCode;
	/** 错误原因 */
	private String ErrInfo;

	public static final int FIELDNUM = 8;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public WFTransLogSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BatchNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		WFTransLogSchema cloned = (WFTransLogSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getMessageType()
	{
		return MessageType;
	}
	public void setMessageType(String aMessageType)
	{
		MessageType = aMessageType;
	}
	public String getSendDate()
	{
		if( SendDate != null )
			return fDate.getString(SendDate);
		else
			return null;
	}
	public void setSendDate(Date aSendDate)
	{
		SendDate = aSendDate;
	}
	public void setSendDate(String aSendDate)
	{
		if (aSendDate != null && !aSendDate.equals("") )
		{
			SendDate = fDate.getDate( aSendDate );
		}
		else
			SendDate = null;
	}

	public String getSendTime()
	{
		return SendTime;
	}
	public void setSendTime(String aSendTime)
	{
		SendTime = aSendTime;
	}
	public String getBatchFlag()
	{
		return BatchFlag;
	}
	public void setBatchFlag(String aBatchFlag)
	{
		BatchFlag = aBatchFlag;
	}
	public String getopertator()
	{
		return opertator;
	}
	public void setopertator(String aopertator)
	{
		opertator = aopertator;
	}
	public String getErrCode()
	{
		return ErrCode;
	}
	public void setErrCode(String aErrCode)
	{
		ErrCode = aErrCode;
	}
	public String getErrInfo()
	{
		return ErrInfo;
	}
	public void setErrInfo(String aErrInfo)
	{
		ErrInfo = aErrInfo;
	}

	/**
	* 使用另外一个 WFTransLogSchema 对象给 Schema 赋值
	* @param: aWFTransLogSchema WFTransLogSchema
	**/
	public void setSchema(WFTransLogSchema aWFTransLogSchema)
	{
		this.BatchNo = aWFTransLogSchema.getBatchNo();
		this.MessageType = aWFTransLogSchema.getMessageType();
		this.SendDate = fDate.getDate( aWFTransLogSchema.getSendDate());
		this.SendTime = aWFTransLogSchema.getSendTime();
		this.BatchFlag = aWFTransLogSchema.getBatchFlag();
		this.opertator = aWFTransLogSchema.getopertator();
		this.ErrCode = aWFTransLogSchema.getErrCode();
		this.ErrInfo = aWFTransLogSchema.getErrInfo();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("MessageType") == null )
				this.MessageType = null;
			else
				this.MessageType = rs.getString("MessageType").trim();

			this.SendDate = rs.getDate("SendDate");
			if( rs.getString("SendTime") == null )
				this.SendTime = null;
			else
				this.SendTime = rs.getString("SendTime").trim();

			if( rs.getString("BatchFlag") == null )
				this.BatchFlag = null;
			else
				this.BatchFlag = rs.getString("BatchFlag").trim();

			if( rs.getString("opertator") == null )
				this.opertator = null;
			else
				this.opertator = rs.getString("opertator").trim();

			if( rs.getString("ErrCode") == null )
				this.ErrCode = null;
			else
				this.ErrCode = rs.getString("ErrCode").trim();

			if( rs.getString("ErrInfo") == null )
				this.ErrInfo = null;
			else
				this.ErrInfo = rs.getString("ErrInfo").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的WFTransLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "WFTransLogSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public WFTransLogSchema getSchema()
	{
		WFTransLogSchema aWFTransLogSchema = new WFTransLogSchema();
		aWFTransLogSchema.setSchema(this);
		return aWFTransLogSchema;
	}

	public WFTransLogDB getDB()
	{
		WFTransLogDB aDBOper = new WFTransLogDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpWFTransLog描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MessageType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SendDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(opertator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrInfo));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpWFTransLog>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			MessageType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			SendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			SendTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BatchFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			opertator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ErrCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ErrInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "WFTransLogSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("MessageType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MessageType));
		}
		if (FCode.equals("SendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
		}
		if (FCode.equals("SendTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendTime));
		}
		if (FCode.equals("BatchFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchFlag));
		}
		if (FCode.equals("opertator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(opertator));
		}
		if (FCode.equals("ErrCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrCode));
		}
		if (FCode.equals("ErrInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrInfo));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(MessageType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SendTime);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BatchFlag);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(opertator);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ErrCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ErrInfo);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("MessageType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MessageType = FValue.trim();
			}
			else
				MessageType = null;
		}
		if (FCode.equalsIgnoreCase("SendDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SendDate = fDate.getDate( FValue );
			}
			else
				SendDate = null;
		}
		if (FCode.equalsIgnoreCase("SendTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendTime = FValue.trim();
			}
			else
				SendTime = null;
		}
		if (FCode.equalsIgnoreCase("BatchFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchFlag = FValue.trim();
			}
			else
				BatchFlag = null;
		}
		if (FCode.equalsIgnoreCase("opertator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				opertator = FValue.trim();
			}
			else
				opertator = null;
		}
		if (FCode.equalsIgnoreCase("ErrCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrCode = FValue.trim();
			}
			else
				ErrCode = null;
		}
		if (FCode.equalsIgnoreCase("ErrInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrInfo = FValue.trim();
			}
			else
				ErrInfo = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		WFTransLogSchema other = (WFTransLogSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (MessageType == null ? other.getMessageType() == null : MessageType.equals(other.getMessageType()))
			&& (SendDate == null ? other.getSendDate() == null : fDate.getString(SendDate).equals(other.getSendDate()))
			&& (SendTime == null ? other.getSendTime() == null : SendTime.equals(other.getSendTime()))
			&& (BatchFlag == null ? other.getBatchFlag() == null : BatchFlag.equals(other.getBatchFlag()))
			&& (opertator == null ? other.getopertator() == null : opertator.equals(other.getopertator()))
			&& (ErrCode == null ? other.getErrCode() == null : ErrCode.equals(other.getErrCode()))
			&& (ErrInfo == null ? other.getErrInfo() == null : ErrInfo.equals(other.getErrInfo()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("MessageType") ) {
			return 1;
		}
		if( strFieldName.equals("SendDate") ) {
			return 2;
		}
		if( strFieldName.equals("SendTime") ) {
			return 3;
		}
		if( strFieldName.equals("BatchFlag") ) {
			return 4;
		}
		if( strFieldName.equals("opertator") ) {
			return 5;
		}
		if( strFieldName.equals("ErrCode") ) {
			return 6;
		}
		if( strFieldName.equals("ErrInfo") ) {
			return 7;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "MessageType";
				break;
			case 2:
				strFieldName = "SendDate";
				break;
			case 3:
				strFieldName = "SendTime";
				break;
			case 4:
				strFieldName = "BatchFlag";
				break;
			case 5:
				strFieldName = "opertator";
				break;
			case 6:
				strFieldName = "ErrCode";
				break;
			case 7:
				strFieldName = "ErrInfo";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MessageType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SendTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("opertator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrInfo") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
