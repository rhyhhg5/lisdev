package com.sinosoft.lis.llcase;

import com.sinosoft.lis.db.LLOpsDiseaseDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LLOpsDiseaseSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LLOpsDiseaseBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors=new CErrors();
	private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	private MMap map = new MMap();
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */
	private String mOperate;
	/** 业务处理相关变量 */
	private LLOpsDiseaseSchema mLLOpsDiseaseSchema=new LLOpsDiseaseSchema();
	public LLOpsDiseaseBL() {
	}
	public static void main(String[] args) {
	}

	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
	 public boolean submitData(VData cInputData,String cOperate)
	 {
	    //将操作数据拷贝到本类中
	    this.mOperate =cOperate;
	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData(cInputData))
	         return false;
	    //进行业务处理
	    if (!dealData())
	    {
	          // @@错误处理
	          CError tError = new CError();
	          tError.moduleName = "LLOpsDiseaseBL";
	          tError.functionName = "submitData";
	          tError.errorMessage = "数据处理失败LLOpsDiseaseBL-->dealData!";
	          this.mErrors .addOneError(tError) ;
	          return false;
	    }
	    //准备往后台的数据
	    if (!prepareOutputData())
	        return false;
	    if (this.mOperate.equals("QUERY||MAIN"))
	    {
	        this.submitquery();
	    }
	    else
	    {
	        PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(mInputData, mOperate))
	        {
	            // @@错误处理
	            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "LLOpsDiseaseBL";
	            tError.functionName = "submitData";
	            tError.errorMessage = "数据提交失败!";

	            this.mErrors.addOneError(tError);
	            return false;
	        }
	    }
	    mInputData = null;
	    return true;
	}
	 /**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean dealData()
	{
//        String SerialNo = mLLOpsDiseaseSchema.getSerialNo();
//        LLOpsDiseaseDB tLDMenuDB = new LLOpsDiseaseDB();
//        tLDMenuDB.setSerialNo(SerialNo);
//        if (!tLDMenuDB.getInfo()) {
//            CError.buildErr(this, "获取参考结点失败");
//            return false;
//        }

	    if (mOperate.equals("INSERT||MAIN"))
	    {
	    	mLLOpsDiseaseSchema.setSerialNo(
                PubFun1.CreateMaxNo("SerialNo", 5));
	        mLLOpsDiseaseSchema.setMakeDate(mCurrentDate);
	        mLLOpsDiseaseSchema.setMakeTime(mCurrentTime);
	        mLLOpsDiseaseSchema.setModifyDate(mCurrentDate);
	        mLLOpsDiseaseSchema.setModifyTime(mCurrentTime);
	        mLLOpsDiseaseSchema.setOperator(mGlobalInput.Operator);
	        mLLOpsDiseaseSchema.setMngCom(mGlobalInput.ManageCom);
	        map.put(mLLOpsDiseaseSchema, "INSERT"); //插入
	        System.out.println(mLLOpsDiseaseSchema.getSerialNo()+"=======================");
	        System.out.println("===ICDOPSCode====="+mLLOpsDiseaseSchema.getICDOPSCode());
	        System.out.println("===ICDOPSName====="+mLLOpsDiseaseSchema.getICDOPSName());
	        System.out.println("===ICDCode====="+mLLOpsDiseaseSchema.getICDCode());
	        System.out.println("===ICDCode====="+mLLOpsDiseaseSchema.getICDName());
	        System.out.println("===MakeDate====="+mLLOpsDiseaseSchema.getMakeDate());
	        System.out.println("===MakeTime====="+mLLOpsDiseaseSchema.getMakeTime());
	        System.out.println("===ModifyDate====="+mLLOpsDiseaseSchema.getModifyDate());
	        System.out.println("===ModifyTime====="+mLLOpsDiseaseSchema.getModifyTime());
	        System.out.println("===Operator====="+mLLOpsDiseaseSchema.getOperator());

	    }
	    if (mOperate.equals("UPDATE||MAIN"))
	    {
	        String sql = "Update LLOpsDisease set " +
	                     "ICDOPSCode = '" + mLLOpsDiseaseSchema.getICDOPSCode() + "', " +
	                     "ICDOPSName = '" + mLLOpsDiseaseSchema.getICDOPSName() + "', " +
	                     "ICDCode = '" + mLLOpsDiseaseSchema.getICDCode() + "', " +
	                     "ICDName = '" + mLLOpsDiseaseSchema.getICDName() + "', " +
	                     "Remark = '" + mLLOpsDiseaseSchema.getRemark() + "', " +
	                     "MngCom = '" + mGlobalInput.ManageCom + "', " +
	                     "Operator = '" + mGlobalInput.Operator + "', " +
	                     "ModifyDate = '" + mCurrentDate + "', " +
	                     "ModifyTime = '" + mCurrentTime + "' " +
	                     "Where  SerialNo = '" + mLLOpsDiseaseSchema.getSerialNo() + "' ";
	        map.put(sql, "UPDATE"); //修改
	    }
	    if (this.mOperate.equals("DELETE||MAIN"))
	    {
	    	String sql="delete from LLOpsDisease where SerialNo= '" + mLLOpsDiseaseSchema.getSerialNo() + "' ";
	    			
	        map.put(sql, "DELETE"); //删除
	    }

	    return true;
	}
	/**
	* 根据前面的输入数据，进行BL逻辑处理
	* 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean updateData()
	{
	   return true;
	}
	/**
	* 根据前面的输入数据，进行BL逻辑处理
	* 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean deleteData()
	{
	  return true;
	}
	 /**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
	     this.mLLOpsDiseaseSchema.setSchema((LLOpsDiseaseSchema)cInputData.getObjectByObjectName("LLOpsDiseaseSchema",0));
	     this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
	        if ((mLLOpsDiseaseSchema == null)) {
	            CError tError = new CError();
	            tError.moduleName = "LLOpsDiseaseBL";
	            tError.functionName = "getInputData";
	            tError.errorMessage = "没有得到足够的数据，请您确认!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        return true;

	}
	/**
	* 准备往后层输出所需要的数据
	* 输出：如果准备数据时发生错误则返回false,否则返回true
	*/
	 private boolean submitquery()
	{
	    this.mResult.clear();
	    LLOpsDiseaseDB tLLOpsDiseaseDB=new LLOpsDiseaseDB();
	    tLLOpsDiseaseDB.setSchema(this.mLLOpsDiseaseSchema);
			//如果有需要处理的错误，则返回
			if (tLLOpsDiseaseDB.mErrors.needDealError())
	 		{
			  // @@错误处理
	 			this.mErrors.copyAllErrors(tLLOpsDiseaseDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "LLOpsDiseaseBL";
				tError.functionName = "submitData";
				tError.errorMessage = "数据提交失败!";
				this.mErrors .addOneError(tError) ;
	 			return false;
	    }
	    mInputData=null;
	    return true;
	}
	 private boolean prepareOutputData()
	 {
	   try
		{
	            this.mInputData.clear();
	            this.mInputData.add(this.mLLOpsDiseaseSchema);
	            mInputData.add(this.map);
	            mResult.clear();
	            mResult.add(this.mLLOpsDiseaseSchema);
	        }
		catch(Exception ex)
		{
	 		// @@错误处理
			CError tError =new CError();
	 		tError.moduleName="LLOpsDiseaseBL";
	 		tError.functionName="prepareData";
	 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
	 		this.mErrors .addOneError(tError) ;
			return false;
		}
		return true;
		}
		public VData getResult()
		{
	  	return this.mResult;
		}
	}
