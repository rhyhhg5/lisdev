/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LOBGrpPolDB;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LOBGrpPolSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-01-09
 */
public class LOBGrpPolSchema implements Schema, Cloneable {
    // @Field
    /** 集体保单险种号码 */
    private String GrpPolNo;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体投保单险种号码 */
    private String GrpProposalNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 险类编码 */
    private String KindCode;
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 销售渠道 */
    private String SaleChnl;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;
    /** 代理机构内部分类 */
    private String AgentType;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人组别 */
    private String AgentGroup;
    /** 联合代理人代码 */
    private String AgentCode1;
    /** 客户号码 */
    private String CustomerNo;
    /** 地址号码 */
    private String AddressNo;
    /** 单位名称 */
    private String GrpName;
    /** 首期交费日期 */
    private Date FirstPayDate;
    /** 终交日期 */
    private Date PayEndDate;
    /** 交至日期 */
    private Date PaytoDate;
    /** 最后一次催收日期 */
    private Date RegetDate;
    /** 最后一次保全日期 */
    private Date LastEdorDate;
    /** 社保标记 */
    private String SSFlag;
    /** 封顶线 */
    private double PeakLine;
    /** 起付限 */
    private double GetLimit;
    /** 赔付比例 */
    private double GetRate;
    /** 分红比率 */
    private double BonusRate;
    /** 医疗费用限额 */
    private double MaxMedFee;
    /** 溢交处理方式 */
    private String OutPayFlag;
    /** 雇员自付比例 */
    private double EmployeeRate;
    /** 家属自付比例 */
    private double FamilyRate;
    /** 团体特殊业务标志 */
    private String SpecFlag;
    /** 预计人数 */
    private int ExpPeoples;
    /** 预计保费 */
    private double ExpPremium;
    /** 预计保额 */
    private double ExpAmnt;
    /** 交费方式 */
    private String PayMode;
    /** 管理费比例 */
    private double ManageFeeRate;
    /** 交费间隔 */
    private int PayIntv;
    /** 险种生效日期 */
    private Date CValiDate;
    /** 投保总人数 */
    private int Peoples2;
    /** 档次 */
    private double Mult;
    /** 保费 */
    private double Prem;
    /** 保额 */
    private double Amnt;
    /** 累计保费 */
    private double SumPrem;
    /** 累计交费 */
    private double SumPay;
    /** 差额 */
    private double Dif;
    /** 状态 */
    private String State;
    /** 复核状态 */
    private String ApproveFlag;
    /** 复核人编码 */
    private String ApproveCode;
    /** 复核日期 */
    private Date ApproveDate;
    /** 复核时间 */
    private String ApproveTime;
    /** 核保状态 */
    private String UWFlag;
    /** 核保人 */
    private String UWOperator;
    /** 核保完成日期 */
    private Date UWDate;
    /** 核保完成时间 */
    private String UWTime;
    /** 投保单/保单标志 */
    private String AppFlag;
    /** 备注 */
    private String Remark;
    /** 备用属性字段1 */
    private String StandbyFlag1;
    /** 备用属性字段2 */
    private String StandbyFlag2;
    /** 备用属性字段3 */
    private String StandbyFlag3;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 在职投保人数 */
    private int OnWorkPeoples;
    /** 退休投保人数 */
    private int OffWorkPeoples;
    /** 其它投保人数 */
    private int OtherPeoples;
    /** 连带投保人数 */
    private int RelaPeoples;
    /** 连带配偶投保人数 */
    private int RelaMatePeoples;
    /** 连带子女投保人数 */
    private int RelaYoungPeoples;
    /** 连带其它投保人数 */
    private int RelaOtherPeoples;
    /** 等待期 */
    private int WaitPeriod;
    /** 分红标志 */
    private String BonusFlag;
    /** 销售渠道明细 */
    private String SaleChnlDetail;
    /** 险种序号 */
    private String RiskSeqNo;
    /** 份数 */
    private double Copys;
    /** 资金规模 */
    private double PremScope;
    /** 分公司费用率 */
    private double BranchFeeRate;
    /** 套餐险种标志 */
    private String RiskWrapFlag;
    /** 保单状态 */
    private String StateFlag;

    public static final int FIELDNUM = 81; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LOBGrpPolSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "GrpPolNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LOBGrpPolSchema cloned = (LOBGrpPolSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getGrpPolNo() {
        return GrpPolNo;
    }

    public void setGrpPolNo(String aGrpPolNo) {
        GrpPolNo = aGrpPolNo;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getGrpProposalNo() {
        return GrpProposalNo;
    }

    public void setGrpProposalNo(String aGrpProposalNo) {
        GrpProposalNo = aGrpProposalNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }

    public String getKindCode() {
        return KindCode;
    }

    public void setKindCode(String aKindCode) {
        KindCode = aKindCode;
    }

    public String getRiskCode() {
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode) {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion() {
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion) {
        RiskVersion = aRiskVersion;
    }

    public String getSaleChnl() {
        return SaleChnl;
    }

    public void setSaleChnl(String aSaleChnl) {
        SaleChnl = aSaleChnl;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getAgentCom() {
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }

    public String getAgentType() {
        return AgentType;
    }

    public void setAgentType(String aAgentType) {
        AgentType = aAgentType;
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getAgentGroup() {
        return AgentGroup;
    }

    public void setAgentGroup(String aAgentGroup) {
        AgentGroup = aAgentGroup;
    }

    public String getAgentCode1() {
        return AgentCode1;
    }

    public void setAgentCode1(String aAgentCode1) {
        AgentCode1 = aAgentCode1;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getAddressNo() {
        return AddressNo;
    }

    public void setAddressNo(String aAddressNo) {
        AddressNo = aAddressNo;
    }

    public String getGrpName() {
        return GrpName;
    }

    public void setGrpName(String aGrpName) {
        GrpName = aGrpName;
    }

    public String getFirstPayDate() {
        if (FirstPayDate != null) {
            return fDate.getString(FirstPayDate);
        } else {
            return null;
        }
    }

    public void setFirstPayDate(Date aFirstPayDate) {
        FirstPayDate = aFirstPayDate;
    }

    public void setFirstPayDate(String aFirstPayDate) {
        if (aFirstPayDate != null && !aFirstPayDate.equals("")) {
            FirstPayDate = fDate.getDate(aFirstPayDate);
        } else {
            FirstPayDate = null;
        }
    }

    public String getPayEndDate() {
        if (PayEndDate != null) {
            return fDate.getString(PayEndDate);
        } else {
            return null;
        }
    }

    public void setPayEndDate(Date aPayEndDate) {
        PayEndDate = aPayEndDate;
    }

    public void setPayEndDate(String aPayEndDate) {
        if (aPayEndDate != null && !aPayEndDate.equals("")) {
            PayEndDate = fDate.getDate(aPayEndDate);
        } else {
            PayEndDate = null;
        }
    }

    public String getPaytoDate() {
        if (PaytoDate != null) {
            return fDate.getString(PaytoDate);
        } else {
            return null;
        }
    }

    public void setPaytoDate(Date aPaytoDate) {
        PaytoDate = aPaytoDate;
    }

    public void setPaytoDate(String aPaytoDate) {
        if (aPaytoDate != null && !aPaytoDate.equals("")) {
            PaytoDate = fDate.getDate(aPaytoDate);
        } else {
            PaytoDate = null;
        }
    }

    public String getRegetDate() {
        if (RegetDate != null) {
            return fDate.getString(RegetDate);
        } else {
            return null;
        }
    }

    public void setRegetDate(Date aRegetDate) {
        RegetDate = aRegetDate;
    }

    public void setRegetDate(String aRegetDate) {
        if (aRegetDate != null && !aRegetDate.equals("")) {
            RegetDate = fDate.getDate(aRegetDate);
        } else {
            RegetDate = null;
        }
    }

    public String getLastEdorDate() {
        if (LastEdorDate != null) {
            return fDate.getString(LastEdorDate);
        } else {
            return null;
        }
    }

    public void setLastEdorDate(Date aLastEdorDate) {
        LastEdorDate = aLastEdorDate;
    }

    public void setLastEdorDate(String aLastEdorDate) {
        if (aLastEdorDate != null && !aLastEdorDate.equals("")) {
            LastEdorDate = fDate.getDate(aLastEdorDate);
        } else {
            LastEdorDate = null;
        }
    }

    public String getSSFlag() {
        return SSFlag;
    }

    public void setSSFlag(String aSSFlag) {
        SSFlag = aSSFlag;
    }

    public double getPeakLine() {
        return PeakLine;
    }

    public void setPeakLine(double aPeakLine) {
        PeakLine = Arith.round(aPeakLine, 2);
    }

    public void setPeakLine(String aPeakLine) {
        if (aPeakLine != null && !aPeakLine.equals("")) {
            Double tDouble = new Double(aPeakLine);
            double d = tDouble.doubleValue();
            PeakLine = Arith.round(d, 2);
        }
    }

    public double getGetLimit() {
        return GetLimit;
    }

    public void setGetLimit(double aGetLimit) {
        GetLimit = Arith.round(aGetLimit, 2);
    }

    public void setGetLimit(String aGetLimit) {
        if (aGetLimit != null && !aGetLimit.equals("")) {
            Double tDouble = new Double(aGetLimit);
            double d = tDouble.doubleValue();
            GetLimit = Arith.round(d, 2);
        }
    }

    public double getGetRate() {
        return GetRate;
    }

    public void setGetRate(double aGetRate) {
        GetRate = Arith.round(aGetRate, 6);
    }

    public void setGetRate(String aGetRate) {
        if (aGetRate != null && !aGetRate.equals("")) {
            Double tDouble = new Double(aGetRate);
            double d = tDouble.doubleValue();
            GetRate = Arith.round(d, 6);
        }
    }

    public double getBonusRate() {
        return BonusRate;
    }

    public void setBonusRate(double aBonusRate) {
        BonusRate = Arith.round(aBonusRate, 4);
    }

    public void setBonusRate(String aBonusRate) {
        if (aBonusRate != null && !aBonusRate.equals("")) {
            Double tDouble = new Double(aBonusRate);
            double d = tDouble.doubleValue();
            BonusRate = Arith.round(d, 4);
        }
    }

    public double getMaxMedFee() {
        return MaxMedFee;
    }

    public void setMaxMedFee(double aMaxMedFee) {
        MaxMedFee = Arith.round(aMaxMedFee, 2);
    }

    public void setMaxMedFee(String aMaxMedFee) {
        if (aMaxMedFee != null && !aMaxMedFee.equals("")) {
            Double tDouble = new Double(aMaxMedFee);
            double d = tDouble.doubleValue();
            MaxMedFee = Arith.round(d, 2);
        }
    }

    public String getOutPayFlag() {
        return OutPayFlag;
    }

    public void setOutPayFlag(String aOutPayFlag) {
        OutPayFlag = aOutPayFlag;
    }

    public double getEmployeeRate() {
        return EmployeeRate;
    }

    public void setEmployeeRate(double aEmployeeRate) {
        EmployeeRate = Arith.round(aEmployeeRate, 2);
    }

    public void setEmployeeRate(String aEmployeeRate) {
        if (aEmployeeRate != null && !aEmployeeRate.equals("")) {
            Double tDouble = new Double(aEmployeeRate);
            double d = tDouble.doubleValue();
            EmployeeRate = Arith.round(d, 2);
        }
    }

    public double getFamilyRate() {
        return FamilyRate;
    }

    public void setFamilyRate(double aFamilyRate) {
        FamilyRate = Arith.round(aFamilyRate, 2);
    }

    public void setFamilyRate(String aFamilyRate) {
        if (aFamilyRate != null && !aFamilyRate.equals("")) {
            Double tDouble = new Double(aFamilyRate);
            double d = tDouble.doubleValue();
            FamilyRate = Arith.round(d, 2);
        }
    }

    public String getSpecFlag() {
        return SpecFlag;
    }

    public void setSpecFlag(String aSpecFlag) {
        SpecFlag = aSpecFlag;
    }

    public int getExpPeoples() {
        return ExpPeoples;
    }

    public void setExpPeoples(int aExpPeoples) {
        ExpPeoples = aExpPeoples;
    }

    public void setExpPeoples(String aExpPeoples) {
        if (aExpPeoples != null && !aExpPeoples.equals("")) {
            Integer tInteger = new Integer(aExpPeoples);
            int i = tInteger.intValue();
            ExpPeoples = i;
        }
    }

    public double getExpPremium() {
        return ExpPremium;
    }

    public void setExpPremium(double aExpPremium) {
        ExpPremium = Arith.round(aExpPremium, 2);
    }

    public void setExpPremium(String aExpPremium) {
        if (aExpPremium != null && !aExpPremium.equals("")) {
            Double tDouble = new Double(aExpPremium);
            double d = tDouble.doubleValue();
            ExpPremium = Arith.round(d, 2);
        }
    }

    public double getExpAmnt() {
        return ExpAmnt;
    }

    public void setExpAmnt(double aExpAmnt) {
        ExpAmnt = Arith.round(aExpAmnt, 2);
    }

    public void setExpAmnt(String aExpAmnt) {
        if (aExpAmnt != null && !aExpAmnt.equals("")) {
            Double tDouble = new Double(aExpAmnt);
            double d = tDouble.doubleValue();
            ExpAmnt = Arith.round(d, 2);
        }
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String aPayMode) {
        PayMode = aPayMode;
    }

    public double getManageFeeRate() {
        return ManageFeeRate;
    }

    public void setManageFeeRate(double aManageFeeRate) {
        ManageFeeRate = Arith.round(aManageFeeRate, 0);
    }

    public void setManageFeeRate(String aManageFeeRate) {
        if (aManageFeeRate != null && !aManageFeeRate.equals("")) {
            Double tDouble = new Double(aManageFeeRate);
            double d = tDouble.doubleValue();
            ManageFeeRate = Arith.round(d, 0);
        }
    }

    public int getPayIntv() {
        return PayIntv;
    }

    public void setPayIntv(int aPayIntv) {
        PayIntv = aPayIntv;
    }

    public void setPayIntv(String aPayIntv) {
        if (aPayIntv != null && !aPayIntv.equals("")) {
            Integer tInteger = new Integer(aPayIntv);
            int i = tInteger.intValue();
            PayIntv = i;
        }
    }

    public String getCValiDate() {
        if (CValiDate != null) {
            return fDate.getString(CValiDate);
        } else {
            return null;
        }
    }

    public void setCValiDate(Date aCValiDate) {
        CValiDate = aCValiDate;
    }

    public void setCValiDate(String aCValiDate) {
        if (aCValiDate != null && !aCValiDate.equals("")) {
            CValiDate = fDate.getDate(aCValiDate);
        } else {
            CValiDate = null;
        }
    }

    public int getPeoples2() {
        return Peoples2;
    }

    public void setPeoples2(int aPeoples2) {
        Peoples2 = aPeoples2;
    }

    public void setPeoples2(String aPeoples2) {
        if (aPeoples2 != null && !aPeoples2.equals("")) {
            Integer tInteger = new Integer(aPeoples2);
            int i = tInteger.intValue();
            Peoples2 = i;
        }
    }

    public double getMult() {
        return Mult;
    }

    public void setMult(double aMult) {
        Mult = Arith.round(aMult, 5);
    }

    public void setMult(String aMult) {
        if (aMult != null && !aMult.equals("")) {
            Double tDouble = new Double(aMult);
            double d = tDouble.doubleValue();
            Mult = Arith.round(d, 5);
        }
    }

    public double getPrem() {
        return Prem;
    }

    public void setPrem(double aPrem) {
        Prem = Arith.round(aPrem, 2);
    }

    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = Arith.round(d, 2);
        }
    }

    public double getAmnt() {
        return Amnt;
    }

    public void setAmnt(double aAmnt) {
        Amnt = Arith.round(aAmnt, 2);
    }

    public void setAmnt(String aAmnt) {
        if (aAmnt != null && !aAmnt.equals("")) {
            Double tDouble = new Double(aAmnt);
            double d = tDouble.doubleValue();
            Amnt = Arith.round(d, 2);
        }
    }

    public double getSumPrem() {
        return SumPrem;
    }

    public void setSumPrem(double aSumPrem) {
        SumPrem = Arith.round(aSumPrem, 2);
    }

    public void setSumPrem(String aSumPrem) {
        if (aSumPrem != null && !aSumPrem.equals("")) {
            Double tDouble = new Double(aSumPrem);
            double d = tDouble.doubleValue();
            SumPrem = Arith.round(d, 2);
        }
    }

    public double getSumPay() {
        return SumPay;
    }

    public void setSumPay(double aSumPay) {
        SumPay = Arith.round(aSumPay, 2);
    }

    public void setSumPay(String aSumPay) {
        if (aSumPay != null && !aSumPay.equals("")) {
            Double tDouble = new Double(aSumPay);
            double d = tDouble.doubleValue();
            SumPay = Arith.round(d, 2);
        }
    }

    public double getDif() {
        return Dif;
    }

    public void setDif(double aDif) {
        Dif = Arith.round(aDif, 2);
    }

    public void setDif(String aDif) {
        if (aDif != null && !aDif.equals("")) {
            Double tDouble = new Double(aDif);
            double d = tDouble.doubleValue();
            Dif = Arith.round(d, 2);
        }
    }

    public String getState() {
        return State;
    }

    public void setState(String aState) {
        State = aState;
    }

    public String getApproveFlag() {
        return ApproveFlag;
    }

    public void setApproveFlag(String aApproveFlag) {
        ApproveFlag = aApproveFlag;
    }

    public String getApproveCode() {
        return ApproveCode;
    }

    public void setApproveCode(String aApproveCode) {
        ApproveCode = aApproveCode;
    }

    public String getApproveDate() {
        if (ApproveDate != null) {
            return fDate.getString(ApproveDate);
        } else {
            return null;
        }
    }

    public void setApproveDate(Date aApproveDate) {
        ApproveDate = aApproveDate;
    }

    public void setApproveDate(String aApproveDate) {
        if (aApproveDate != null && !aApproveDate.equals("")) {
            ApproveDate = fDate.getDate(aApproveDate);
        } else {
            ApproveDate = null;
        }
    }

    public String getApproveTime() {
        return ApproveTime;
    }

    public void setApproveTime(String aApproveTime) {
        ApproveTime = aApproveTime;
    }

    public String getUWFlag() {
        return UWFlag;
    }

    public void setUWFlag(String aUWFlag) {
        UWFlag = aUWFlag;
    }

    public String getUWOperator() {
        return UWOperator;
    }

    public void setUWOperator(String aUWOperator) {
        UWOperator = aUWOperator;
    }

    public String getUWDate() {
        if (UWDate != null) {
            return fDate.getString(UWDate);
        } else {
            return null;
        }
    }

    public void setUWDate(Date aUWDate) {
        UWDate = aUWDate;
    }

    public void setUWDate(String aUWDate) {
        if (aUWDate != null && !aUWDate.equals("")) {
            UWDate = fDate.getDate(aUWDate);
        } else {
            UWDate = null;
        }
    }

    public String getUWTime() {
        return UWTime;
    }

    public void setUWTime(String aUWTime) {
        UWTime = aUWTime;
    }

    public String getAppFlag() {
        return AppFlag;
    }

    public void setAppFlag(String aAppFlag) {
        AppFlag = aAppFlag;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    public String getStandbyFlag1() {
        return StandbyFlag1;
    }

    public void setStandbyFlag1(String aStandbyFlag1) {
        StandbyFlag1 = aStandbyFlag1;
    }

    public String getStandbyFlag2() {
        return StandbyFlag2;
    }

    public void setStandbyFlag2(String aStandbyFlag2) {
        StandbyFlag2 = aStandbyFlag2;
    }

    public String getStandbyFlag3() {
        return StandbyFlag3;
    }

    public void setStandbyFlag3(String aStandbyFlag3) {
        StandbyFlag3 = aStandbyFlag3;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public int getOnWorkPeoples() {
        return OnWorkPeoples;
    }

    public void setOnWorkPeoples(int aOnWorkPeoples) {
        OnWorkPeoples = aOnWorkPeoples;
    }

    public void setOnWorkPeoples(String aOnWorkPeoples) {
        if (aOnWorkPeoples != null && !aOnWorkPeoples.equals("")) {
            Integer tInteger = new Integer(aOnWorkPeoples);
            int i = tInteger.intValue();
            OnWorkPeoples = i;
        }
    }

    public int getOffWorkPeoples() {
        return OffWorkPeoples;
    }

    public void setOffWorkPeoples(int aOffWorkPeoples) {
        OffWorkPeoples = aOffWorkPeoples;
    }

    public void setOffWorkPeoples(String aOffWorkPeoples) {
        if (aOffWorkPeoples != null && !aOffWorkPeoples.equals("")) {
            Integer tInteger = new Integer(aOffWorkPeoples);
            int i = tInteger.intValue();
            OffWorkPeoples = i;
        }
    }

    public int getOtherPeoples() {
        return OtherPeoples;
    }

    public void setOtherPeoples(int aOtherPeoples) {
        OtherPeoples = aOtherPeoples;
    }

    public void setOtherPeoples(String aOtherPeoples) {
        if (aOtherPeoples != null && !aOtherPeoples.equals("")) {
            Integer tInteger = new Integer(aOtherPeoples);
            int i = tInteger.intValue();
            OtherPeoples = i;
        }
    }

    public int getRelaPeoples() {
        return RelaPeoples;
    }

    public void setRelaPeoples(int aRelaPeoples) {
        RelaPeoples = aRelaPeoples;
    }

    public void setRelaPeoples(String aRelaPeoples) {
        if (aRelaPeoples != null && !aRelaPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaPeoples);
            int i = tInteger.intValue();
            RelaPeoples = i;
        }
    }

    public int getRelaMatePeoples() {
        return RelaMatePeoples;
    }

    public void setRelaMatePeoples(int aRelaMatePeoples) {
        RelaMatePeoples = aRelaMatePeoples;
    }

    public void setRelaMatePeoples(String aRelaMatePeoples) {
        if (aRelaMatePeoples != null && !aRelaMatePeoples.equals("")) {
            Integer tInteger = new Integer(aRelaMatePeoples);
            int i = tInteger.intValue();
            RelaMatePeoples = i;
        }
    }

    public int getRelaYoungPeoples() {
        return RelaYoungPeoples;
    }

    public void setRelaYoungPeoples(int aRelaYoungPeoples) {
        RelaYoungPeoples = aRelaYoungPeoples;
    }

    public void setRelaYoungPeoples(String aRelaYoungPeoples) {
        if (aRelaYoungPeoples != null && !aRelaYoungPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaYoungPeoples);
            int i = tInteger.intValue();
            RelaYoungPeoples = i;
        }
    }

    public int getRelaOtherPeoples() {
        return RelaOtherPeoples;
    }

    public void setRelaOtherPeoples(int aRelaOtherPeoples) {
        RelaOtherPeoples = aRelaOtherPeoples;
    }

    public void setRelaOtherPeoples(String aRelaOtherPeoples) {
        if (aRelaOtherPeoples != null && !aRelaOtherPeoples.equals("")) {
            Integer tInteger = new Integer(aRelaOtherPeoples);
            int i = tInteger.intValue();
            RelaOtherPeoples = i;
        }
    }

    public int getWaitPeriod() {
        return WaitPeriod;
    }

    public void setWaitPeriod(int aWaitPeriod) {
        WaitPeriod = aWaitPeriod;
    }

    public void setWaitPeriod(String aWaitPeriod) {
        if (aWaitPeriod != null && !aWaitPeriod.equals("")) {
            Integer tInteger = new Integer(aWaitPeriod);
            int i = tInteger.intValue();
            WaitPeriod = i;
        }
    }

    public String getBonusFlag() {
        return BonusFlag;
    }

    public void setBonusFlag(String aBonusFlag) {
        BonusFlag = aBonusFlag;
    }

    public String getSaleChnlDetail() {
        return SaleChnlDetail;
    }

    public void setSaleChnlDetail(String aSaleChnlDetail) {
        SaleChnlDetail = aSaleChnlDetail;
    }

    public String getRiskSeqNo() {
        return RiskSeqNo;
    }

    public void setRiskSeqNo(String aRiskSeqNo) {
        RiskSeqNo = aRiskSeqNo;
    }

    public double getCopys() {
        return Copys;
    }

    public void setCopys(double aCopys) {
        Copys = Arith.round(aCopys, 5);
    }

    public void setCopys(String aCopys) {
        if (aCopys != null && !aCopys.equals("")) {
            Double tDouble = new Double(aCopys);
            double d = tDouble.doubleValue();
            Copys = Arith.round(d, 5);
        }
    }

    public double getPremScope() {
        return PremScope;
    }

    public void setPremScope(double aPremScope) {
        PremScope = Arith.round(aPremScope, 2);
    }

    public void setPremScope(String aPremScope) {
        if (aPremScope != null && !aPremScope.equals("")) {
            Double tDouble = new Double(aPremScope);
            double d = tDouble.doubleValue();
            PremScope = Arith.round(d, 2);
        }
    }

    public double getBranchFeeRate() {
        return BranchFeeRate;
    }

    public void setBranchFeeRate(double aBranchFeeRate) {
        BranchFeeRate = Arith.round(aBranchFeeRate, 2);
    }

    public void setBranchFeeRate(String aBranchFeeRate) {
        if (aBranchFeeRate != null && !aBranchFeeRate.equals("")) {
            Double tDouble = new Double(aBranchFeeRate);
            double d = tDouble.doubleValue();
            BranchFeeRate = Arith.round(d, 2);
        }
    }

    public String getRiskWrapFlag() {
        return RiskWrapFlag;
    }

    public void setRiskWrapFlag(String aRiskWrapFlag) {
        RiskWrapFlag = aRiskWrapFlag;
    }

    public String getStateFlag() {
        return StateFlag;
    }

    public void setStateFlag(String aStateFlag) {
        StateFlag = aStateFlag;
    }

    /**
     * 使用另外一个 LOBGrpPolSchema 对象给 Schema 赋值
     * @param: aLOBGrpPolSchema LOBGrpPolSchema
     **/
    public void setSchema(LOBGrpPolSchema aLOBGrpPolSchema) {
        this.GrpPolNo = aLOBGrpPolSchema.getGrpPolNo();
        this.GrpContNo = aLOBGrpPolSchema.getGrpContNo();
        this.GrpProposalNo = aLOBGrpPolSchema.getGrpProposalNo();
        this.PrtNo = aLOBGrpPolSchema.getPrtNo();
        this.KindCode = aLOBGrpPolSchema.getKindCode();
        this.RiskCode = aLOBGrpPolSchema.getRiskCode();
        this.RiskVersion = aLOBGrpPolSchema.getRiskVersion();
        this.SaleChnl = aLOBGrpPolSchema.getSaleChnl();
        this.ManageCom = aLOBGrpPolSchema.getManageCom();
        this.AgentCom = aLOBGrpPolSchema.getAgentCom();
        this.AgentType = aLOBGrpPolSchema.getAgentType();
        this.AgentCode = aLOBGrpPolSchema.getAgentCode();
        this.AgentGroup = aLOBGrpPolSchema.getAgentGroup();
        this.AgentCode1 = aLOBGrpPolSchema.getAgentCode1();
        this.CustomerNo = aLOBGrpPolSchema.getCustomerNo();
        this.AddressNo = aLOBGrpPolSchema.getAddressNo();
        this.GrpName = aLOBGrpPolSchema.getGrpName();
        this.FirstPayDate = fDate.getDate(aLOBGrpPolSchema.getFirstPayDate());
        this.PayEndDate = fDate.getDate(aLOBGrpPolSchema.getPayEndDate());
        this.PaytoDate = fDate.getDate(aLOBGrpPolSchema.getPaytoDate());
        this.RegetDate = fDate.getDate(aLOBGrpPolSchema.getRegetDate());
        this.LastEdorDate = fDate.getDate(aLOBGrpPolSchema.getLastEdorDate());
        this.SSFlag = aLOBGrpPolSchema.getSSFlag();
        this.PeakLine = aLOBGrpPolSchema.getPeakLine();
        this.GetLimit = aLOBGrpPolSchema.getGetLimit();
        this.GetRate = aLOBGrpPolSchema.getGetRate();
        this.BonusRate = aLOBGrpPolSchema.getBonusRate();
        this.MaxMedFee = aLOBGrpPolSchema.getMaxMedFee();
        this.OutPayFlag = aLOBGrpPolSchema.getOutPayFlag();
        this.EmployeeRate = aLOBGrpPolSchema.getEmployeeRate();
        this.FamilyRate = aLOBGrpPolSchema.getFamilyRate();
        this.SpecFlag = aLOBGrpPolSchema.getSpecFlag();
        this.ExpPeoples = aLOBGrpPolSchema.getExpPeoples();
        this.ExpPremium = aLOBGrpPolSchema.getExpPremium();
        this.ExpAmnt = aLOBGrpPolSchema.getExpAmnt();
        this.PayMode = aLOBGrpPolSchema.getPayMode();
        this.ManageFeeRate = aLOBGrpPolSchema.getManageFeeRate();
        this.PayIntv = aLOBGrpPolSchema.getPayIntv();
        this.CValiDate = fDate.getDate(aLOBGrpPolSchema.getCValiDate());
        this.Peoples2 = aLOBGrpPolSchema.getPeoples2();
        this.Mult = aLOBGrpPolSchema.getMult();
        this.Prem = aLOBGrpPolSchema.getPrem();
        this.Amnt = aLOBGrpPolSchema.getAmnt();
        this.SumPrem = aLOBGrpPolSchema.getSumPrem();
        this.SumPay = aLOBGrpPolSchema.getSumPay();
        this.Dif = aLOBGrpPolSchema.getDif();
        this.State = aLOBGrpPolSchema.getState();
        this.ApproveFlag = aLOBGrpPolSchema.getApproveFlag();
        this.ApproveCode = aLOBGrpPolSchema.getApproveCode();
        this.ApproveDate = fDate.getDate(aLOBGrpPolSchema.getApproveDate());
        this.ApproveTime = aLOBGrpPolSchema.getApproveTime();
        this.UWFlag = aLOBGrpPolSchema.getUWFlag();
        this.UWOperator = aLOBGrpPolSchema.getUWOperator();
        this.UWDate = fDate.getDate(aLOBGrpPolSchema.getUWDate());
        this.UWTime = aLOBGrpPolSchema.getUWTime();
        this.AppFlag = aLOBGrpPolSchema.getAppFlag();
        this.Remark = aLOBGrpPolSchema.getRemark();
        this.StandbyFlag1 = aLOBGrpPolSchema.getStandbyFlag1();
        this.StandbyFlag2 = aLOBGrpPolSchema.getStandbyFlag2();
        this.StandbyFlag3 = aLOBGrpPolSchema.getStandbyFlag3();
        this.Operator = aLOBGrpPolSchema.getOperator();
        this.MakeDate = fDate.getDate(aLOBGrpPolSchema.getMakeDate());
        this.MakeTime = aLOBGrpPolSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLOBGrpPolSchema.getModifyDate());
        this.ModifyTime = aLOBGrpPolSchema.getModifyTime();
        this.OnWorkPeoples = aLOBGrpPolSchema.getOnWorkPeoples();
        this.OffWorkPeoples = aLOBGrpPolSchema.getOffWorkPeoples();
        this.OtherPeoples = aLOBGrpPolSchema.getOtherPeoples();
        this.RelaPeoples = aLOBGrpPolSchema.getRelaPeoples();
        this.RelaMatePeoples = aLOBGrpPolSchema.getRelaMatePeoples();
        this.RelaYoungPeoples = aLOBGrpPolSchema.getRelaYoungPeoples();
        this.RelaOtherPeoples = aLOBGrpPolSchema.getRelaOtherPeoples();
        this.WaitPeriod = aLOBGrpPolSchema.getWaitPeriod();
        this.BonusFlag = aLOBGrpPolSchema.getBonusFlag();
        this.SaleChnlDetail = aLOBGrpPolSchema.getSaleChnlDetail();
        this.RiskSeqNo = aLOBGrpPolSchema.getRiskSeqNo();
        this.Copys = aLOBGrpPolSchema.getCopys();
        this.PremScope = aLOBGrpPolSchema.getPremScope();
        this.BranchFeeRate = aLOBGrpPolSchema.getBranchFeeRate();
        this.RiskWrapFlag = aLOBGrpPolSchema.getRiskWrapFlag();
        this.StateFlag = aLOBGrpPolSchema.getStateFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpPolNo") == null) {
                this.GrpPolNo = null;
            } else {
                this.GrpPolNo = rs.getString("GrpPolNo").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("GrpProposalNo") == null) {
                this.GrpProposalNo = null;
            } else {
                this.GrpProposalNo = rs.getString("GrpProposalNo").trim();
            }

            if (rs.getString("PrtNo") == null) {
                this.PrtNo = null;
            } else {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("KindCode") == null) {
                this.KindCode = null;
            } else {
                this.KindCode = rs.getString("KindCode").trim();
            }

            if (rs.getString("RiskCode") == null) {
                this.RiskCode = null;
            } else {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null) {
                this.RiskVersion = null;
            } else {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("SaleChnl") == null) {
                this.SaleChnl = null;
            } else {
                this.SaleChnl = rs.getString("SaleChnl").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentCom") == null) {
                this.AgentCom = null;
            } else {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

            if (rs.getString("AgentType") == null) {
                this.AgentType = null;
            } else {
                this.AgentType = rs.getString("AgentType").trim();
            }

            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentGroup") == null) {
                this.AgentGroup = null;
            } else {
                this.AgentGroup = rs.getString("AgentGroup").trim();
            }

            if (rs.getString("AgentCode1") == null) {
                this.AgentCode1 = null;
            } else {
                this.AgentCode1 = rs.getString("AgentCode1").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("AddressNo") == null) {
                this.AddressNo = null;
            } else {
                this.AddressNo = rs.getString("AddressNo").trim();
            }

            if (rs.getString("GrpName") == null) {
                this.GrpName = null;
            } else {
                this.GrpName = rs.getString("GrpName").trim();
            }

            this.FirstPayDate = rs.getDate("FirstPayDate");
            this.PayEndDate = rs.getDate("PayEndDate");
            this.PaytoDate = rs.getDate("PaytoDate");
            this.RegetDate = rs.getDate("RegetDate");
            this.LastEdorDate = rs.getDate("LastEdorDate");
            if (rs.getString("SSFlag") == null) {
                this.SSFlag = null;
            } else {
                this.SSFlag = rs.getString("SSFlag").trim();
            }

            this.PeakLine = rs.getDouble("PeakLine");
            this.GetLimit = rs.getDouble("GetLimit");
            this.GetRate = rs.getDouble("GetRate");
            this.BonusRate = rs.getDouble("BonusRate");
            this.MaxMedFee = rs.getDouble("MaxMedFee");
            if (rs.getString("OutPayFlag") == null) {
                this.OutPayFlag = null;
            } else {
                this.OutPayFlag = rs.getString("OutPayFlag").trim();
            }

            this.EmployeeRate = rs.getDouble("EmployeeRate");
            this.FamilyRate = rs.getDouble("FamilyRate");
            if (rs.getString("SpecFlag") == null) {
                this.SpecFlag = null;
            } else {
                this.SpecFlag = rs.getString("SpecFlag").trim();
            }

            this.ExpPeoples = rs.getInt("ExpPeoples");
            this.ExpPremium = rs.getDouble("ExpPremium");
            this.ExpAmnt = rs.getDouble("ExpAmnt");
            if (rs.getString("PayMode") == null) {
                this.PayMode = null;
            } else {
                this.PayMode = rs.getString("PayMode").trim();
            }

            this.ManageFeeRate = rs.getDouble("ManageFeeRate");
            this.PayIntv = rs.getInt("PayIntv");
            this.CValiDate = rs.getDate("CValiDate");
            this.Peoples2 = rs.getInt("Peoples2");
            this.Mult = rs.getDouble("Mult");
            this.Prem = rs.getDouble("Prem");
            this.Amnt = rs.getDouble("Amnt");
            this.SumPrem = rs.getDouble("SumPrem");
            this.SumPay = rs.getDouble("SumPay");
            this.Dif = rs.getDouble("Dif");
            if (rs.getString("State") == null) {
                this.State = null;
            } else {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("ApproveFlag") == null) {
                this.ApproveFlag = null;
            } else {
                this.ApproveFlag = rs.getString("ApproveFlag").trim();
            }

            if (rs.getString("ApproveCode") == null) {
                this.ApproveCode = null;
            } else {
                this.ApproveCode = rs.getString("ApproveCode").trim();
            }

            this.ApproveDate = rs.getDate("ApproveDate");
            if (rs.getString("ApproveTime") == null) {
                this.ApproveTime = null;
            } else {
                this.ApproveTime = rs.getString("ApproveTime").trim();
            }

            if (rs.getString("UWFlag") == null) {
                this.UWFlag = null;
            } else {
                this.UWFlag = rs.getString("UWFlag").trim();
            }

            if (rs.getString("UWOperator") == null) {
                this.UWOperator = null;
            } else {
                this.UWOperator = rs.getString("UWOperator").trim();
            }

            this.UWDate = rs.getDate("UWDate");
            if (rs.getString("UWTime") == null) {
                this.UWTime = null;
            } else {
                this.UWTime = rs.getString("UWTime").trim();
            }

            if (rs.getString("AppFlag") == null) {
                this.AppFlag = null;
            } else {
                this.AppFlag = rs.getString("AppFlag").trim();
            }

            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("StandbyFlag1") == null) {
                this.StandbyFlag1 = null;
            } else {
                this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();
            }

            if (rs.getString("StandbyFlag2") == null) {
                this.StandbyFlag2 = null;
            } else {
                this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();
            }

            if (rs.getString("StandbyFlag3") == null) {
                this.StandbyFlag3 = null;
            } else {
                this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.OnWorkPeoples = rs.getInt("OnWorkPeoples");
            this.OffWorkPeoples = rs.getInt("OffWorkPeoples");
            this.OtherPeoples = rs.getInt("OtherPeoples");
            this.RelaPeoples = rs.getInt("RelaPeoples");
            this.RelaMatePeoples = rs.getInt("RelaMatePeoples");
            this.RelaYoungPeoples = rs.getInt("RelaYoungPeoples");
            this.RelaOtherPeoples = rs.getInt("RelaOtherPeoples");
            this.WaitPeriod = rs.getInt("WaitPeriod");
            if (rs.getString("BonusFlag") == null) {
                this.BonusFlag = null;
            } else {
                this.BonusFlag = rs.getString("BonusFlag").trim();
            }

            if (rs.getString("SaleChnlDetail") == null) {
                this.SaleChnlDetail = null;
            } else {
                this.SaleChnlDetail = rs.getString("SaleChnlDetail").trim();
            }

            if (rs.getString("RiskSeqNo") == null) {
                this.RiskSeqNo = null;
            } else {
                this.RiskSeqNo = rs.getString("RiskSeqNo").trim();
            }

            this.Copys = rs.getDouble("Copys");
            this.PremScope = rs.getDouble("PremScope");
            this.BranchFeeRate = rs.getDouble("BranchFeeRate");
            if (rs.getString("RiskWrapFlag") == null) {
                this.RiskWrapFlag = null;
            } else {
                this.RiskWrapFlag = rs.getString("RiskWrapFlag").trim();
            }

            if (rs.getString("StateFlag") == null) {
                this.StateFlag = null;
            } else {
                this.StateFlag = rs.getString("StateFlag").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LOBGrpPol表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBGrpPolSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LOBGrpPolSchema getSchema() {
        LOBGrpPolSchema aLOBGrpPolSchema = new LOBGrpPolSchema();
        aLOBGrpPolSchema.setSchema(this);
        return aLOBGrpPolSchema;
    }

    public LOBGrpPolDB getDB() {
        LOBGrpPolDB aDBOper = new LOBGrpPolDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBGrpPol描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpPolNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpProposalNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(KindCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskVersion));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnl));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentGroup));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AddressNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(FirstPayDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PayEndDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PaytoDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(RegetDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(LastEdorDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SSFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PeakLine));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetLimit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GetRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BonusRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(MaxMedFee));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OutPayFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(EmployeeRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FamilyRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SpecFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ExpPeoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ExpPremium));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ExpAmnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PayMode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ManageFeeRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PayIntv));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(CValiDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Mult));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Amnt));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPrem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SumPay));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Dif));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(State));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ApproveDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApproveTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(UWDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UWTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag1));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StandbyFlag3));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OnWorkPeoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OffWorkPeoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OtherPeoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RelaPeoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RelaMatePeoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RelaYoungPeoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RelaOtherPeoples));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(WaitPeriod));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BonusFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SaleChnlDetail));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskSeqNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Copys));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PremScope));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(BranchFeeRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RiskWrapFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StateFlag));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOBGrpPol>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            GrpProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            KindCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
            AgentCode1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
            AddressNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                       SysConst.PACKAGESPILTER);
            GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                     SysConst.PACKAGESPILTER);
            FirstPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            PayEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            PaytoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            RegetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            LastEdorDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            SSFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                    SysConst.PACKAGESPILTER);
            PeakLine = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 24, SysConst.PACKAGESPILTER))).doubleValue();
            GetLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 25, SysConst.PACKAGESPILTER))).doubleValue();
            GetRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 26, SysConst.PACKAGESPILTER))).doubleValue();
            BonusRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 27, SysConst.PACKAGESPILTER))).doubleValue();
            MaxMedFee = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 28, SysConst.PACKAGESPILTER))).doubleValue();
            OutPayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                        SysConst.PACKAGESPILTER);
            EmployeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 30, SysConst.PACKAGESPILTER))).doubleValue();
            FamilyRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 31, SysConst.PACKAGESPILTER))).doubleValue();
            SpecFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                      SysConst.PACKAGESPILTER);
            ExpPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 33, SysConst.PACKAGESPILTER))).intValue();
            ExpPremium = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 34, SysConst.PACKAGESPILTER))).doubleValue();
            ExpAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 35, SysConst.PACKAGESPILTER))).doubleValue();
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,
                                     SysConst.PACKAGESPILTER);
            ManageFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 37, SysConst.PACKAGESPILTER))).doubleValue();
            PayIntv = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 38, SysConst.PACKAGESPILTER))).intValue();
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 39, SysConst.PACKAGESPILTER));
            Peoples2 = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 40, SysConst.PACKAGESPILTER))).intValue();
            Mult = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    41, SysConst.PACKAGESPILTER))).doubleValue();
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    42, SysConst.PACKAGESPILTER))).doubleValue();
            Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    43, SysConst.PACKAGESPILTER))).doubleValue();
            SumPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 44, SysConst.PACKAGESPILTER))).doubleValue();
            SumPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    45, SysConst.PACKAGESPILTER))).doubleValue();
            Dif = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    46, SysConst.PACKAGESPILTER))).doubleValue();
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47,
                                   SysConst.PACKAGESPILTER);
            ApproveFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48,
                                         SysConst.PACKAGESPILTER);
            ApproveCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49,
                                         SysConst.PACKAGESPILTER);
            ApproveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 50, SysConst.PACKAGESPILTER));
            ApproveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51,
                                         SysConst.PACKAGESPILTER);
            UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52,
                                    SysConst.PACKAGESPILTER);
            UWOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53,
                                        SysConst.PACKAGESPILTER);
            UWDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 54, SysConst.PACKAGESPILTER));
            UWTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55,
                                    SysConst.PACKAGESPILTER);
            AppFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56,
                                     SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57,
                                    SysConst.PACKAGESPILTER);
            StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58,
                                          SysConst.PACKAGESPILTER);
            StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59,
                                          SysConst.PACKAGESPILTER);
            StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60,
                                          SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 62, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 64, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65,
                                        SysConst.PACKAGESPILTER);
            OnWorkPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 66, SysConst.PACKAGESPILTER))).intValue();
            OffWorkPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 67, SysConst.PACKAGESPILTER))).intValue();
            OtherPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 68, SysConst.PACKAGESPILTER))).intValue();
            RelaPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 69, SysConst.PACKAGESPILTER))).intValue();
            RelaMatePeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 70, SysConst.PACKAGESPILTER))).intValue();
            RelaYoungPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 71, SysConst.PACKAGESPILTER))).intValue();
            RelaOtherPeoples = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 72, SysConst.PACKAGESPILTER))).intValue();
            WaitPeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 73, SysConst.PACKAGESPILTER))).intValue();
            BonusFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 74,
                                       SysConst.PACKAGESPILTER);
            SaleChnlDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            75, SysConst.PACKAGESPILTER);
            RiskSeqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 76,
                                       SysConst.PACKAGESPILTER);
            Copys = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    77, SysConst.PACKAGESPILTER))).doubleValue();
            PremScope = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 78, SysConst.PACKAGESPILTER))).doubleValue();
            BranchFeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 79, SysConst.PACKAGESPILTER))).doubleValue();
            RiskWrapFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 80,
                                          SysConst.PACKAGESPILTER);
            StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 81,
                                       SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LOBGrpPolSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("GrpPolNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("GrpProposalNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpProposalNo));
        }
        if (FCode.equals("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("KindCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(KindCode));
        }
        if (FCode.equals("RiskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskVersion));
        }
        if (FCode.equals("SaleChnl")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (FCode.equals("AgentType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
        }
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentGroup")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
        }
        if (FCode.equals("AgentCode1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode1));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("AddressNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AddressNo));
        }
        if (FCode.equals("GrpName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
        }
        if (FCode.equals("FirstPayDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getFirstPayDate()));
        }
        if (FCode.equals("PayEndDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPayEndDate()));
        }
        if (FCode.equals("PaytoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPaytoDate()));
        }
        if (FCode.equals("RegetDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getRegetDate()));
        }
        if (FCode.equals("LastEdorDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getLastEdorDate()));
        }
        if (FCode.equals("SSFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SSFlag));
        }
        if (FCode.equals("PeakLine")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PeakLine));
        }
        if (FCode.equals("GetLimit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetLimit));
        }
        if (FCode.equals("GetRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetRate));
        }
        if (FCode.equals("BonusRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusRate));
        }
        if (FCode.equals("MaxMedFee")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MaxMedFee));
        }
        if (FCode.equals("OutPayFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutPayFlag));
        }
        if (FCode.equals("EmployeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EmployeeRate));
        }
        if (FCode.equals("FamilyRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FamilyRate));
        }
        if (FCode.equals("SpecFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SpecFlag));
        }
        if (FCode.equals("ExpPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpPeoples));
        }
        if (FCode.equals("ExpPremium")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpPremium));
        }
        if (FCode.equals("ExpAmnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExpAmnt));
        }
        if (FCode.equals("PayMode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equals("ManageFeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFeeRate));
        }
        if (FCode.equals("PayIntv")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
        }
        if (FCode.equals("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCValiDate()));
        }
        if (FCode.equals("Peoples2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
        }
        if (FCode.equals("Mult")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mult));
        }
        if (FCode.equals("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equals("Amnt")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
        }
        if (FCode.equals("SumPrem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPrem));
        }
        if (FCode.equals("SumPay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SumPay));
        }
        if (FCode.equals("Dif")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Dif));
        }
        if (FCode.equals("State")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("ApproveFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveFlag));
        }
        if (FCode.equals("ApproveCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveCode));
        }
        if (FCode.equals("ApproveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getApproveDate()));
        }
        if (FCode.equals("ApproveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApproveTime));
        }
        if (FCode.equals("UWFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
        }
        if (FCode.equals("UWOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWOperator));
        }
        if (FCode.equals("UWDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getUWDate()));
        }
        if (FCode.equals("UWTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UWTime));
        }
        if (FCode.equals("AppFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppFlag));
        }
        if (FCode.equals("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("StandbyFlag1")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
        }
        if (FCode.equals("StandbyFlag2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
        }
        if (FCode.equals("StandbyFlag3")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("OnWorkPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OnWorkPeoples));
        }
        if (FCode.equals("OffWorkPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OffWorkPeoples));
        }
        if (FCode.equals("OtherPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPeoples));
        }
        if (FCode.equals("RelaPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaPeoples));
        }
        if (FCode.equals("RelaMatePeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaMatePeoples));
        }
        if (FCode.equals("RelaYoungPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaYoungPeoples));
        }
        if (FCode.equals("RelaOtherPeoples")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaOtherPeoples));
        }
        if (FCode.equals("WaitPeriod")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WaitPeriod));
        }
        if (FCode.equals("BonusFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BonusFlag));
        }
        if (FCode.equals("SaleChnlDetail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnlDetail));
        }
        if (FCode.equals("RiskSeqNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskSeqNo));
        }
        if (FCode.equals("Copys")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Copys));
        }
        if (FCode.equals("PremScope")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremScope));
        }
        if (FCode.equals("BranchFeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchFeeRate));
        }
        if (FCode.equals("RiskWrapFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskWrapFlag));
        }
        if (FCode.equals("StateFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(GrpProposalNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(PrtNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(KindCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(RiskCode);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(RiskVersion);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(SaleChnl);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(AgentCom);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(AgentType);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(AgentGroup);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(AgentCode1);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(AddressNo);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(GrpName);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getFirstPayDate()));
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getPayEndDate()));
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getPaytoDate()));
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getRegetDate()));
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getLastEdorDate()));
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(SSFlag);
            break;
        case 23:
            strFieldValue = String.valueOf(PeakLine);
            break;
        case 24:
            strFieldValue = String.valueOf(GetLimit);
            break;
        case 25:
            strFieldValue = String.valueOf(GetRate);
            break;
        case 26:
            strFieldValue = String.valueOf(BonusRate);
            break;
        case 27:
            strFieldValue = String.valueOf(MaxMedFee);
            break;
        case 28:
            strFieldValue = StrTool.GBKToUnicode(OutPayFlag);
            break;
        case 29:
            strFieldValue = String.valueOf(EmployeeRate);
            break;
        case 30:
            strFieldValue = String.valueOf(FamilyRate);
            break;
        case 31:
            strFieldValue = StrTool.GBKToUnicode(SpecFlag);
            break;
        case 32:
            strFieldValue = String.valueOf(ExpPeoples);
            break;
        case 33:
            strFieldValue = String.valueOf(ExpPremium);
            break;
        case 34:
            strFieldValue = String.valueOf(ExpAmnt);
            break;
        case 35:
            strFieldValue = StrTool.GBKToUnicode(PayMode);
            break;
        case 36:
            strFieldValue = String.valueOf(ManageFeeRate);
            break;
        case 37:
            strFieldValue = String.valueOf(PayIntv);
            break;
        case 38:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getCValiDate()));
            break;
        case 39:
            strFieldValue = String.valueOf(Peoples2);
            break;
        case 40:
            strFieldValue = String.valueOf(Mult);
            break;
        case 41:
            strFieldValue = String.valueOf(Prem);
            break;
        case 42:
            strFieldValue = String.valueOf(Amnt);
            break;
        case 43:
            strFieldValue = String.valueOf(SumPrem);
            break;
        case 44:
            strFieldValue = String.valueOf(SumPay);
            break;
        case 45:
            strFieldValue = String.valueOf(Dif);
            break;
        case 46:
            strFieldValue = StrTool.GBKToUnicode(State);
            break;
        case 47:
            strFieldValue = StrTool.GBKToUnicode(ApproveFlag);
            break;
        case 48:
            strFieldValue = StrTool.GBKToUnicode(ApproveCode);
            break;
        case 49:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getApproveDate()));
            break;
        case 50:
            strFieldValue = StrTool.GBKToUnicode(ApproveTime);
            break;
        case 51:
            strFieldValue = StrTool.GBKToUnicode(UWFlag);
            break;
        case 52:
            strFieldValue = StrTool.GBKToUnicode(UWOperator);
            break;
        case 53:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.getUWDate()));
            break;
        case 54:
            strFieldValue = StrTool.GBKToUnicode(UWTime);
            break;
        case 55:
            strFieldValue = StrTool.GBKToUnicode(AppFlag);
            break;
        case 56:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        case 57:
            strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
            break;
        case 58:
            strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
            break;
        case 59:
            strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
            break;
        case 60:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 61:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 62:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 63:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 64:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 65:
            strFieldValue = String.valueOf(OnWorkPeoples);
            break;
        case 66:
            strFieldValue = String.valueOf(OffWorkPeoples);
            break;
        case 67:
            strFieldValue = String.valueOf(OtherPeoples);
            break;
        case 68:
            strFieldValue = String.valueOf(RelaPeoples);
            break;
        case 69:
            strFieldValue = String.valueOf(RelaMatePeoples);
            break;
        case 70:
            strFieldValue = String.valueOf(RelaYoungPeoples);
            break;
        case 71:
            strFieldValue = String.valueOf(RelaOtherPeoples);
            break;
        case 72:
            strFieldValue = String.valueOf(WaitPeriod);
            break;
        case 73:
            strFieldValue = StrTool.GBKToUnicode(BonusFlag);
            break;
        case 74:
            strFieldValue = StrTool.GBKToUnicode(SaleChnlDetail);
            break;
        case 75:
            strFieldValue = StrTool.GBKToUnicode(RiskSeqNo);
            break;
        case 76:
            strFieldValue = String.valueOf(Copys);
            break;
        case 77:
            strFieldValue = String.valueOf(PremScope);
            break;
        case 78:
            strFieldValue = String.valueOf(BranchFeeRate);
            break;
        case 79:
            strFieldValue = StrTool.GBKToUnicode(RiskWrapFlag);
            break;
        case 80:
            strFieldValue = StrTool.GBKToUnicode(StateFlag);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("GrpPolNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpPolNo = FValue.trim();
            } else {
                GrpPolNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpProposalNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpProposalNo = FValue.trim();
            } else {
                GrpProposalNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if (FValue != null && !FValue.equals("")) {
                PrtNo = FValue.trim();
            } else {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("KindCode")) {
            if (FValue != null && !FValue.equals("")) {
                KindCode = FValue.trim();
            } else {
                KindCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskCode")) {
            if (FValue != null && !FValue.equals("")) {
                RiskCode = FValue.trim();
            } else {
                RiskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskVersion")) {
            if (FValue != null && !FValue.equals("")) {
                RiskVersion = FValue.trim();
            } else {
                RiskVersion = null;
            }
        }
        if (FCode.equalsIgnoreCase("SaleChnl")) {
            if (FValue != null && !FValue.equals("")) {
                SaleChnl = FValue.trim();
            } else {
                SaleChnl = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCom = FValue.trim();
            } else {
                AgentCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentType")) {
            if (FValue != null && !FValue.equals("")) {
                AgentType = FValue.trim();
            } else {
                AgentType = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentGroup")) {
            if (FValue != null && !FValue.equals("")) {
                AgentGroup = FValue.trim();
            } else {
                AgentGroup = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode1")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode1 = FValue.trim();
            } else {
                AgentCode1 = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("AddressNo")) {
            if (FValue != null && !FValue.equals("")) {
                AddressNo = FValue.trim();
            } else {
                AddressNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpName")) {
            if (FValue != null && !FValue.equals("")) {
                GrpName = FValue.trim();
            } else {
                GrpName = null;
            }
        }
        if (FCode.equalsIgnoreCase("FirstPayDate")) {
            if (FValue != null && !FValue.equals("")) {
                FirstPayDate = fDate.getDate(FValue);
            } else {
                FirstPayDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("PayEndDate")) {
            if (FValue != null && !FValue.equals("")) {
                PayEndDate = fDate.getDate(FValue);
            } else {
                PayEndDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("PaytoDate")) {
            if (FValue != null && !FValue.equals("")) {
                PaytoDate = fDate.getDate(FValue);
            } else {
                PaytoDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("RegetDate")) {
            if (FValue != null && !FValue.equals("")) {
                RegetDate = fDate.getDate(FValue);
            } else {
                RegetDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("LastEdorDate")) {
            if (FValue != null && !FValue.equals("")) {
                LastEdorDate = fDate.getDate(FValue);
            } else {
                LastEdorDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("SSFlag")) {
            if (FValue != null && !FValue.equals("")) {
                SSFlag = FValue.trim();
            } else {
                SSFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("PeakLine")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PeakLine = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetLimit")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetLimit = d;
            }
        }
        if (FCode.equalsIgnoreCase("GetRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                GetRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("BonusRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BonusRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("MaxMedFee")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MaxMedFee = d;
            }
        }
        if (FCode.equalsIgnoreCase("OutPayFlag")) {
            if (FValue != null && !FValue.equals("")) {
                OutPayFlag = FValue.trim();
            } else {
                OutPayFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("EmployeeRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                EmployeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("FamilyRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FamilyRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("SpecFlag")) {
            if (FValue != null && !FValue.equals("")) {
                SpecFlag = FValue.trim();
            } else {
                SpecFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExpPeoples")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ExpPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("ExpPremium")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ExpPremium = d;
            }
        }
        if (FCode.equalsIgnoreCase("ExpAmnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ExpAmnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayMode")) {
            if (FValue != null && !FValue.equals("")) {
                PayMode = FValue.trim();
            } else {
                PayMode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageFeeRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ManageFeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("PayIntv")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayIntv = i;
            }
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if (FValue != null && !FValue.equals("")) {
                CValiDate = fDate.getDate(FValue);
            } else {
                CValiDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Peoples2 = i;
            }
        }
        if (FCode.equalsIgnoreCase("Mult")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Mult = d;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("Amnt")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Amnt = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPrem")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumPrem = d;
            }
        }
        if (FCode.equalsIgnoreCase("SumPay")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                SumPay = d;
            }
        }
        if (FCode.equalsIgnoreCase("Dif")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Dif = d;
            }
        }
        if (FCode.equalsIgnoreCase("State")) {
            if (FValue != null && !FValue.equals("")) {
                State = FValue.trim();
            } else {
                State = null;
            }
        }
        if (FCode.equalsIgnoreCase("ApproveFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ApproveFlag = FValue.trim();
            } else {
                ApproveFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ApproveCode")) {
            if (FValue != null && !FValue.equals("")) {
                ApproveCode = FValue.trim();
            } else {
                ApproveCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ApproveDate")) {
            if (FValue != null && !FValue.equals("")) {
                ApproveDate = fDate.getDate(FValue);
            } else {
                ApproveDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ApproveTime")) {
            if (FValue != null && !FValue.equals("")) {
                ApproveTime = FValue.trim();
            } else {
                ApproveTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("UWFlag")) {
            if (FValue != null && !FValue.equals("")) {
                UWFlag = FValue.trim();
            } else {
                UWFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("UWOperator")) {
            if (FValue != null && !FValue.equals("")) {
                UWOperator = FValue.trim();
            } else {
                UWOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("UWDate")) {
            if (FValue != null && !FValue.equals("")) {
                UWDate = fDate.getDate(FValue);
            } else {
                UWDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("UWTime")) {
            if (FValue != null && !FValue.equals("")) {
                UWTime = FValue.trim();
            } else {
                UWTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppFlag")) {
            if (FValue != null && !FValue.equals("")) {
                AppFlag = FValue.trim();
            } else {
                AppFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag1")) {
            if (FValue != null && !FValue.equals("")) {
                StandbyFlag1 = FValue.trim();
            } else {
                StandbyFlag1 = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag2")) {
            if (FValue != null && !FValue.equals("")) {
                StandbyFlag2 = FValue.trim();
            } else {
                StandbyFlag2 = null;
            }
        }
        if (FCode.equalsIgnoreCase("StandbyFlag3")) {
            if (FValue != null && !FValue.equals("")) {
                StandbyFlag3 = FValue.trim();
            } else {
                StandbyFlag3 = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("OnWorkPeoples")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                OnWorkPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OffWorkPeoples")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                OffWorkPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("OtherPeoples")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                OtherPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaPeoples")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RelaPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaMatePeoples")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RelaMatePeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaYoungPeoples")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RelaYoungPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaOtherPeoples")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RelaOtherPeoples = i;
            }
        }
        if (FCode.equalsIgnoreCase("WaitPeriod")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                WaitPeriod = i;
            }
        }
        if (FCode.equalsIgnoreCase("BonusFlag")) {
            if (FValue != null && !FValue.equals("")) {
                BonusFlag = FValue.trim();
            } else {
                BonusFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("SaleChnlDetail")) {
            if (FValue != null && !FValue.equals("")) {
                SaleChnlDetail = FValue.trim();
            } else {
                SaleChnlDetail = null;
            }
        }
        if (FCode.equalsIgnoreCase("RiskSeqNo")) {
            if (FValue != null && !FValue.equals("")) {
                RiskSeqNo = FValue.trim();
            } else {
                RiskSeqNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("Copys")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Copys = d;
            }
        }
        if (FCode.equalsIgnoreCase("PremScope")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PremScope = d;
            }
        }
        if (FCode.equalsIgnoreCase("BranchFeeRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BranchFeeRate = d;
            }
        }
        if (FCode.equalsIgnoreCase("RiskWrapFlag")) {
            if (FValue != null && !FValue.equals("")) {
                RiskWrapFlag = FValue.trim();
            } else {
                RiskWrapFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("StateFlag")) {
            if (FValue != null && !FValue.equals("")) {
                StateFlag = FValue.trim();
            } else {
                StateFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LOBGrpPolSchema other = (LOBGrpPolSchema) otherObject;
        return
                GrpPolNo.equals(other.getGrpPolNo())
                && GrpContNo.equals(other.getGrpContNo())
                && GrpProposalNo.equals(other.getGrpProposalNo())
                && PrtNo.equals(other.getPrtNo())
                && KindCode.equals(other.getKindCode())
                && RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && SaleChnl.equals(other.getSaleChnl())
                && ManageCom.equals(other.getManageCom())
                && AgentCom.equals(other.getAgentCom())
                && AgentType.equals(other.getAgentType())
                && AgentCode.equals(other.getAgentCode())
                && AgentGroup.equals(other.getAgentGroup())
                && AgentCode1.equals(other.getAgentCode1())
                && CustomerNo.equals(other.getCustomerNo())
                && AddressNo.equals(other.getAddressNo())
                && GrpName.equals(other.getGrpName())
                && fDate.getString(FirstPayDate).equals(other.getFirstPayDate())
                && fDate.getString(PayEndDate).equals(other.getPayEndDate())
                && fDate.getString(PaytoDate).equals(other.getPaytoDate())
                && fDate.getString(RegetDate).equals(other.getRegetDate())
                && fDate.getString(LastEdorDate).equals(other.getLastEdorDate())
                && SSFlag.equals(other.getSSFlag())
                && PeakLine == other.getPeakLine()
                && GetLimit == other.getGetLimit()
                && GetRate == other.getGetRate()
                && BonusRate == other.getBonusRate()
                && MaxMedFee == other.getMaxMedFee()
                && OutPayFlag.equals(other.getOutPayFlag())
                && EmployeeRate == other.getEmployeeRate()
                && FamilyRate == other.getFamilyRate()
                && SpecFlag.equals(other.getSpecFlag())
                && ExpPeoples == other.getExpPeoples()
                && ExpPremium == other.getExpPremium()
                && ExpAmnt == other.getExpAmnt()
                && PayMode.equals(other.getPayMode())
                && ManageFeeRate == other.getManageFeeRate()
                && PayIntv == other.getPayIntv()
                && fDate.getString(CValiDate).equals(other.getCValiDate())
                && Peoples2 == other.getPeoples2()
                && Mult == other.getMult()
                && Prem == other.getPrem()
                && Amnt == other.getAmnt()
                && SumPrem == other.getSumPrem()
                && SumPay == other.getSumPay()
                && Dif == other.getDif()
                && State.equals(other.getState())
                && ApproveFlag.equals(other.getApproveFlag())
                && ApproveCode.equals(other.getApproveCode())
                && fDate.getString(ApproveDate).equals(other.getApproveDate())
                && ApproveTime.equals(other.getApproveTime())
                && UWFlag.equals(other.getUWFlag())
                && UWOperator.equals(other.getUWOperator())
                && fDate.getString(UWDate).equals(other.getUWDate())
                && UWTime.equals(other.getUWTime())
                && AppFlag.equals(other.getAppFlag())
                && Remark.equals(other.getRemark())
                && StandbyFlag1.equals(other.getStandbyFlag1())
                && StandbyFlag2.equals(other.getStandbyFlag2())
                && StandbyFlag3.equals(other.getStandbyFlag3())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && OnWorkPeoples == other.getOnWorkPeoples()
                && OffWorkPeoples == other.getOffWorkPeoples()
                && OtherPeoples == other.getOtherPeoples()
                && RelaPeoples == other.getRelaPeoples()
                && RelaMatePeoples == other.getRelaMatePeoples()
                && RelaYoungPeoples == other.getRelaYoungPeoples()
                && RelaOtherPeoples == other.getRelaOtherPeoples()
                && WaitPeriod == other.getWaitPeriod()
                && BonusFlag.equals(other.getBonusFlag())
                && SaleChnlDetail.equals(other.getSaleChnlDetail())
                && RiskSeqNo.equals(other.getRiskSeqNo())
                && Copys == other.getCopys()
                && PremScope == other.getPremScope()
                && BranchFeeRate == other.getBranchFeeRate()
                && RiskWrapFlag.equals(other.getRiskWrapFlag())
                && StateFlag.equals(other.getStateFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("GrpPolNo")) {
            return 0;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 1;
        }
        if (strFieldName.equals("GrpProposalNo")) {
            return 2;
        }
        if (strFieldName.equals("PrtNo")) {
            return 3;
        }
        if (strFieldName.equals("KindCode")) {
            return 4;
        }
        if (strFieldName.equals("RiskCode")) {
            return 5;
        }
        if (strFieldName.equals("RiskVersion")) {
            return 6;
        }
        if (strFieldName.equals("SaleChnl")) {
            return 7;
        }
        if (strFieldName.equals("ManageCom")) {
            return 8;
        }
        if (strFieldName.equals("AgentCom")) {
            return 9;
        }
        if (strFieldName.equals("AgentType")) {
            return 10;
        }
        if (strFieldName.equals("AgentCode")) {
            return 11;
        }
        if (strFieldName.equals("AgentGroup")) {
            return 12;
        }
        if (strFieldName.equals("AgentCode1")) {
            return 13;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 14;
        }
        if (strFieldName.equals("AddressNo")) {
            return 15;
        }
        if (strFieldName.equals("GrpName")) {
            return 16;
        }
        if (strFieldName.equals("FirstPayDate")) {
            return 17;
        }
        if (strFieldName.equals("PayEndDate")) {
            return 18;
        }
        if (strFieldName.equals("PaytoDate")) {
            return 19;
        }
        if (strFieldName.equals("RegetDate")) {
            return 20;
        }
        if (strFieldName.equals("LastEdorDate")) {
            return 21;
        }
        if (strFieldName.equals("SSFlag")) {
            return 22;
        }
        if (strFieldName.equals("PeakLine")) {
            return 23;
        }
        if (strFieldName.equals("GetLimit")) {
            return 24;
        }
        if (strFieldName.equals("GetRate")) {
            return 25;
        }
        if (strFieldName.equals("BonusRate")) {
            return 26;
        }
        if (strFieldName.equals("MaxMedFee")) {
            return 27;
        }
        if (strFieldName.equals("OutPayFlag")) {
            return 28;
        }
        if (strFieldName.equals("EmployeeRate")) {
            return 29;
        }
        if (strFieldName.equals("FamilyRate")) {
            return 30;
        }
        if (strFieldName.equals("SpecFlag")) {
            return 31;
        }
        if (strFieldName.equals("ExpPeoples")) {
            return 32;
        }
        if (strFieldName.equals("ExpPremium")) {
            return 33;
        }
        if (strFieldName.equals("ExpAmnt")) {
            return 34;
        }
        if (strFieldName.equals("PayMode")) {
            return 35;
        }
        if (strFieldName.equals("ManageFeeRate")) {
            return 36;
        }
        if (strFieldName.equals("PayIntv")) {
            return 37;
        }
        if (strFieldName.equals("CValiDate")) {
            return 38;
        }
        if (strFieldName.equals("Peoples2")) {
            return 39;
        }
        if (strFieldName.equals("Mult")) {
            return 40;
        }
        if (strFieldName.equals("Prem")) {
            return 41;
        }
        if (strFieldName.equals("Amnt")) {
            return 42;
        }
        if (strFieldName.equals("SumPrem")) {
            return 43;
        }
        if (strFieldName.equals("SumPay")) {
            return 44;
        }
        if (strFieldName.equals("Dif")) {
            return 45;
        }
        if (strFieldName.equals("State")) {
            return 46;
        }
        if (strFieldName.equals("ApproveFlag")) {
            return 47;
        }
        if (strFieldName.equals("ApproveCode")) {
            return 48;
        }
        if (strFieldName.equals("ApproveDate")) {
            return 49;
        }
        if (strFieldName.equals("ApproveTime")) {
            return 50;
        }
        if (strFieldName.equals("UWFlag")) {
            return 51;
        }
        if (strFieldName.equals("UWOperator")) {
            return 52;
        }
        if (strFieldName.equals("UWDate")) {
            return 53;
        }
        if (strFieldName.equals("UWTime")) {
            return 54;
        }
        if (strFieldName.equals("AppFlag")) {
            return 55;
        }
        if (strFieldName.equals("Remark")) {
            return 56;
        }
        if (strFieldName.equals("StandbyFlag1")) {
            return 57;
        }
        if (strFieldName.equals("StandbyFlag2")) {
            return 58;
        }
        if (strFieldName.equals("StandbyFlag3")) {
            return 59;
        }
        if (strFieldName.equals("Operator")) {
            return 60;
        }
        if (strFieldName.equals("MakeDate")) {
            return 61;
        }
        if (strFieldName.equals("MakeTime")) {
            return 62;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 63;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 64;
        }
        if (strFieldName.equals("OnWorkPeoples")) {
            return 65;
        }
        if (strFieldName.equals("OffWorkPeoples")) {
            return 66;
        }
        if (strFieldName.equals("OtherPeoples")) {
            return 67;
        }
        if (strFieldName.equals("RelaPeoples")) {
            return 68;
        }
        if (strFieldName.equals("RelaMatePeoples")) {
            return 69;
        }
        if (strFieldName.equals("RelaYoungPeoples")) {
            return 70;
        }
        if (strFieldName.equals("RelaOtherPeoples")) {
            return 71;
        }
        if (strFieldName.equals("WaitPeriod")) {
            return 72;
        }
        if (strFieldName.equals("BonusFlag")) {
            return 73;
        }
        if (strFieldName.equals("SaleChnlDetail")) {
            return 74;
        }
        if (strFieldName.equals("RiskSeqNo")) {
            return 75;
        }
        if (strFieldName.equals("Copys")) {
            return 76;
        }
        if (strFieldName.equals("PremScope")) {
            return 77;
        }
        if (strFieldName.equals("BranchFeeRate")) {
            return 78;
        }
        if (strFieldName.equals("RiskWrapFlag")) {
            return 79;
        }
        if (strFieldName.equals("StateFlag")) {
            return 80;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "GrpPolNo";
            break;
        case 1:
            strFieldName = "GrpContNo";
            break;
        case 2:
            strFieldName = "GrpProposalNo";
            break;
        case 3:
            strFieldName = "PrtNo";
            break;
        case 4:
            strFieldName = "KindCode";
            break;
        case 5:
            strFieldName = "RiskCode";
            break;
        case 6:
            strFieldName = "RiskVersion";
            break;
        case 7:
            strFieldName = "SaleChnl";
            break;
        case 8:
            strFieldName = "ManageCom";
            break;
        case 9:
            strFieldName = "AgentCom";
            break;
        case 10:
            strFieldName = "AgentType";
            break;
        case 11:
            strFieldName = "AgentCode";
            break;
        case 12:
            strFieldName = "AgentGroup";
            break;
        case 13:
            strFieldName = "AgentCode1";
            break;
        case 14:
            strFieldName = "CustomerNo";
            break;
        case 15:
            strFieldName = "AddressNo";
            break;
        case 16:
            strFieldName = "GrpName";
            break;
        case 17:
            strFieldName = "FirstPayDate";
            break;
        case 18:
            strFieldName = "PayEndDate";
            break;
        case 19:
            strFieldName = "PaytoDate";
            break;
        case 20:
            strFieldName = "RegetDate";
            break;
        case 21:
            strFieldName = "LastEdorDate";
            break;
        case 22:
            strFieldName = "SSFlag";
            break;
        case 23:
            strFieldName = "PeakLine";
            break;
        case 24:
            strFieldName = "GetLimit";
            break;
        case 25:
            strFieldName = "GetRate";
            break;
        case 26:
            strFieldName = "BonusRate";
            break;
        case 27:
            strFieldName = "MaxMedFee";
            break;
        case 28:
            strFieldName = "OutPayFlag";
            break;
        case 29:
            strFieldName = "EmployeeRate";
            break;
        case 30:
            strFieldName = "FamilyRate";
            break;
        case 31:
            strFieldName = "SpecFlag";
            break;
        case 32:
            strFieldName = "ExpPeoples";
            break;
        case 33:
            strFieldName = "ExpPremium";
            break;
        case 34:
            strFieldName = "ExpAmnt";
            break;
        case 35:
            strFieldName = "PayMode";
            break;
        case 36:
            strFieldName = "ManageFeeRate";
            break;
        case 37:
            strFieldName = "PayIntv";
            break;
        case 38:
            strFieldName = "CValiDate";
            break;
        case 39:
            strFieldName = "Peoples2";
            break;
        case 40:
            strFieldName = "Mult";
            break;
        case 41:
            strFieldName = "Prem";
            break;
        case 42:
            strFieldName = "Amnt";
            break;
        case 43:
            strFieldName = "SumPrem";
            break;
        case 44:
            strFieldName = "SumPay";
            break;
        case 45:
            strFieldName = "Dif";
            break;
        case 46:
            strFieldName = "State";
            break;
        case 47:
            strFieldName = "ApproveFlag";
            break;
        case 48:
            strFieldName = "ApproveCode";
            break;
        case 49:
            strFieldName = "ApproveDate";
            break;
        case 50:
            strFieldName = "ApproveTime";
            break;
        case 51:
            strFieldName = "UWFlag";
            break;
        case 52:
            strFieldName = "UWOperator";
            break;
        case 53:
            strFieldName = "UWDate";
            break;
        case 54:
            strFieldName = "UWTime";
            break;
        case 55:
            strFieldName = "AppFlag";
            break;
        case 56:
            strFieldName = "Remark";
            break;
        case 57:
            strFieldName = "StandbyFlag1";
            break;
        case 58:
            strFieldName = "StandbyFlag2";
            break;
        case 59:
            strFieldName = "StandbyFlag3";
            break;
        case 60:
            strFieldName = "Operator";
            break;
        case 61:
            strFieldName = "MakeDate";
            break;
        case 62:
            strFieldName = "MakeTime";
            break;
        case 63:
            strFieldName = "ModifyDate";
            break;
        case 64:
            strFieldName = "ModifyTime";
            break;
        case 65:
            strFieldName = "OnWorkPeoples";
            break;
        case 66:
            strFieldName = "OffWorkPeoples";
            break;
        case 67:
            strFieldName = "OtherPeoples";
            break;
        case 68:
            strFieldName = "RelaPeoples";
            break;
        case 69:
            strFieldName = "RelaMatePeoples";
            break;
        case 70:
            strFieldName = "RelaYoungPeoples";
            break;
        case 71:
            strFieldName = "RelaOtherPeoples";
            break;
        case 72:
            strFieldName = "WaitPeriod";
            break;
        case 73:
            strFieldName = "BonusFlag";
            break;
        case 74:
            strFieldName = "SaleChnlDetail";
            break;
        case 75:
            strFieldName = "RiskSeqNo";
            break;
        case 76:
            strFieldName = "Copys";
            break;
        case 77:
            strFieldName = "PremScope";
            break;
        case 78:
            strFieldName = "BranchFeeRate";
            break;
        case 79:
            strFieldName = "RiskWrapFlag";
            break;
        case 80:
            strFieldName = "StateFlag";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("GrpPolNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpProposalNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("KindCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnl")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGroup")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode1")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AddressNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FirstPayDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PayEndDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PaytoDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("RegetDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("LastEdorDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("SSFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PeakLine")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetLimit")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("GetRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BonusRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MaxMedFee")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("OutPayFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EmployeeRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FamilyRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SpecFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExpPeoples")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ExpPremium")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ExpAmnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PayMode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageFeeRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PayIntv")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("CValiDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Peoples2")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Mult")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Prem")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Amnt")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SumPrem")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SumPay")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Dif")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("State")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApproveDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ApproveTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("UWTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandbyFlag1")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandbyFlag2")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandbyFlag3")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OnWorkPeoples")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("OffWorkPeoples")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("OtherPeoples")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RelaPeoples")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RelaMatePeoples")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RelaYoungPeoples")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RelaOtherPeoples")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("WaitPeriod")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("BonusFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SaleChnlDetail")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskSeqNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Copys")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PremScope")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BranchFeeRate")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RiskWrapFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StateFlag")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 18:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 19:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 20:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 21:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 22:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 23:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 24:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 25:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 26:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 27:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 28:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 29:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 30:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 31:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 32:
            nFieldType = Schema.TYPE_INT;
            break;
        case 33:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 34:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 35:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 36:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 37:
            nFieldType = Schema.TYPE_INT;
            break;
        case 38:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 39:
            nFieldType = Schema.TYPE_INT;
            break;
        case 40:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 41:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 42:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 43:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 44:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 45:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 46:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 47:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 48:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 49:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 50:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 51:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 52:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 53:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 54:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 55:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 56:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 57:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 58:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 59:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 60:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 61:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 62:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 63:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 64:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 65:
            nFieldType = Schema.TYPE_INT;
            break;
        case 66:
            nFieldType = Schema.TYPE_INT;
            break;
        case 67:
            nFieldType = Schema.TYPE_INT;
            break;
        case 68:
            nFieldType = Schema.TYPE_INT;
            break;
        case 69:
            nFieldType = Schema.TYPE_INT;
            break;
        case 70:
            nFieldType = Schema.TYPE_INT;
            break;
        case 71:
            nFieldType = Schema.TYPE_INT;
            break;
        case 72:
            nFieldType = Schema.TYPE_INT;
            break;
        case 73:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 74:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 75:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 76:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 77:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 78:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 79:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 80:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
