/*
 * <p>ClassName: LYDupPaySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LYDupPayDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LYDupPaySchema implements Schema
{
    // @Field
    /** 批次号 */
    private String SerialNo;
    /** 数据类型 */
    private String DataType;
    /** 交退费编码 */
    private String PayCode;
    /** 机构编码 */
    private String ComCode;
    /** 姓名 */
    private String Name;
    /** 银行代码 */
    private String BankCode;
    /** 帐户类型 */
    private String AccType;
    /** 帐户名 */
    private String AccName;
    /** 帐号 */
    private String AccNo;
    /** 保单号码 */
    private String PolNo;
    /** 号码类型 */
    private String NoType;
    /** 代理人编码 */
    private String AgentCode;
    /** 缴费退费金额 */
    private double PayMoney;
    /** 发送日期 */
    private Date SendDate;
    /** 银行扣款日期 */
    private Date BankDealDate;
    /** 银行扣款时间 */
    private Date BankDealTime;
    /** 银行成功标记 */
    private String BankSuccFlag;
    /** 退款编码 */
    private String GetCode;
    /** 退款日期 */
    private Date GetDate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 险种编码 */
    private String RiskCode;

    public static final int FIELDNUM = 25; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LYDupPaySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "SerialNo";
        pk[1] = "DataType";
        pk[2] = "PayCode";
        pk[3] = "PolNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSerialNo()
    {
        if (SerialNo != null && !SerialNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getDataType()
    {
        if (DataType != null && !DataType.equals("") && SysConst.CHANGECHARSET == true)
        {
            DataType = StrTool.unicodeToGBK(DataType);
        }
        return DataType;
    }

    public void setDataType(String aDataType)
    {
        DataType = aDataType;
    }

    public String getPayCode()
    {
        if (PayCode != null && !PayCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            PayCode = StrTool.unicodeToGBK(PayCode);
        }
        return PayCode;
    }

    public void setPayCode(String aPayCode)
    {
        PayCode = aPayCode;
    }

    public String getComCode()
    {
        if (ComCode != null && !ComCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ComCode = StrTool.unicodeToGBK(ComCode);
        }
        return ComCode;
    }

    public void setComCode(String aComCode)
    {
        ComCode = aComCode;
    }

    public String getName()
    {
        if (Name != null && !Name.equals("") && SysConst.CHANGECHARSET == true)
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getBankCode()
    {
        if (BankCode != null && !BankCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            BankCode = StrTool.unicodeToGBK(BankCode);
        }
        return BankCode;
    }

    public void setBankCode(String aBankCode)
    {
        BankCode = aBankCode;
    }

    public String getAccType()
    {
        if (AccType != null && !AccType.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccType = StrTool.unicodeToGBK(AccType);
        }
        return AccType;
    }

    public void setAccType(String aAccType)
    {
        AccType = aAccType;
    }

    public String getAccName()
    {
        if (AccName != null && !AccName.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccName = StrTool.unicodeToGBK(AccName);
        }
        return AccName;
    }

    public void setAccName(String aAccName)
    {
        AccName = aAccName;
    }

    public String getAccNo()
    {
        if (AccNo != null && !AccNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            AccNo = StrTool.unicodeToGBK(AccNo);
        }
        return AccNo;
    }

    public void setAccNo(String aAccNo)
    {
        AccNo = aAccNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getNoType()
    {
        if (NoType != null && !NoType.equals("") && SysConst.CHANGECHARSET == true)
        {
            NoType = StrTool.unicodeToGBK(NoType);
        }
        return NoType;
    }

    public void setNoType(String aNoType)
    {
        NoType = aNoType;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public double getPayMoney()
    {
        return PayMoney;
    }

    public void setPayMoney(double aPayMoney)
    {
        PayMoney = aPayMoney;
    }

    public void setPayMoney(String aPayMoney)
    {
        if (aPayMoney != null && !aPayMoney.equals(""))
        {
            Double tDouble = new Double(aPayMoney);
            double d = tDouble.doubleValue();
            PayMoney = d;
        }
    }

    public String getSendDate()
    {
        if (SendDate != null)
        {
            return fDate.getString(SendDate);
        }
        else
        {
            return null;
        }
    }

    public void setSendDate(Date aSendDate)
    {
        SendDate = aSendDate;
    }

    public void setSendDate(String aSendDate)
    {
        if (aSendDate != null && !aSendDate.equals(""))
        {
            SendDate = fDate.getDate(aSendDate);
        }
        else
        {
            SendDate = null;
        }
    }

    public String getBankDealDate()
    {
        if (BankDealDate != null)
        {
            return fDate.getString(BankDealDate);
        }
        else
        {
            return null;
        }
    }

    public void setBankDealDate(Date aBankDealDate)
    {
        BankDealDate = aBankDealDate;
    }

    public void setBankDealDate(String aBankDealDate)
    {
        if (aBankDealDate != null && !aBankDealDate.equals(""))
        {
            BankDealDate = fDate.getDate(aBankDealDate);
        }
        else
        {
            BankDealDate = null;
        }
    }

    public String getBankDealTime()
    {
        if (BankDealTime != null)
        {
            return fDate.getString(BankDealTime);
        }
        else
        {
            return null;
        }
    }

    public void setBankDealTime(Date aBankDealTime)
    {
        BankDealTime = aBankDealTime;
    }

    public void setBankDealTime(String aBankDealTime)
    {
        if (aBankDealTime != null && !aBankDealTime.equals(""))
        {
            BankDealTime = fDate.getDate(aBankDealTime);
        }
        else
        {
            BankDealTime = null;
        }
    }

    public String getBankSuccFlag()
    {
        if (BankSuccFlag != null && !BankSuccFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BankSuccFlag = StrTool.unicodeToGBK(BankSuccFlag);
        }
        return BankSuccFlag;
    }

    public void setBankSuccFlag(String aBankSuccFlag)
    {
        BankSuccFlag = aBankSuccFlag;
    }

    public String getGetCode()
    {
        if (GetCode != null && !GetCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            GetCode = StrTool.unicodeToGBK(GetCode);
        }
        return GetCode;
    }

    public void setGetCode(String aGetCode)
    {
        GetCode = aGetCode;
    }

    public String getGetDate()
    {
        if (GetDate != null)
        {
            return fDate.getString(GetDate);
        }
        else
        {
            return null;
        }
    }

    public void setGetDate(Date aGetDate)
    {
        GetDate = aGetDate;
    }

    public void setGetDate(String aGetDate)
    {
        if (aGetDate != null && !aGetDate.equals(""))
        {
            GetDate = fDate.getDate(aGetDate);
        }
        else
        {
            GetDate = null;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    /**
     * 使用另外一个 LYDupPaySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LYDupPaySchema aLYDupPaySchema)
    {
        this.SerialNo = aLYDupPaySchema.getSerialNo();
        this.DataType = aLYDupPaySchema.getDataType();
        this.PayCode = aLYDupPaySchema.getPayCode();
        this.ComCode = aLYDupPaySchema.getComCode();
        this.Name = aLYDupPaySchema.getName();
        this.BankCode = aLYDupPaySchema.getBankCode();
        this.AccType = aLYDupPaySchema.getAccType();
        this.AccName = aLYDupPaySchema.getAccName();
        this.AccNo = aLYDupPaySchema.getAccNo();
        this.PolNo = aLYDupPaySchema.getPolNo();
        this.NoType = aLYDupPaySchema.getNoType();
        this.AgentCode = aLYDupPaySchema.getAgentCode();
        this.PayMoney = aLYDupPaySchema.getPayMoney();
        this.SendDate = fDate.getDate(aLYDupPaySchema.getSendDate());
        this.BankDealDate = fDate.getDate(aLYDupPaySchema.getBankDealDate());
        this.BankDealTime = fDate.getDate(aLYDupPaySchema.getBankDealTime());
        this.BankSuccFlag = aLYDupPaySchema.getBankSuccFlag();
        this.GetCode = aLYDupPaySchema.getGetCode();
        this.GetDate = fDate.getDate(aLYDupPaySchema.getGetDate());
        this.Operator = aLYDupPaySchema.getOperator();
        this.MakeDate = fDate.getDate(aLYDupPaySchema.getMakeDate());
        this.MakeTime = aLYDupPaySchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLYDupPaySchema.getModifyDate());
        this.ModifyTime = aLYDupPaySchema.getModifyTime();
        this.RiskCode = aLYDupPaySchema.getRiskCode();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("DataType") == null)
            {
                this.DataType = null;
            }
            else
            {
                this.DataType = rs.getString("DataType").trim();
            }

            if (rs.getString("PayCode") == null)
            {
                this.PayCode = null;
            }
            else
            {
                this.PayCode = rs.getString("PayCode").trim();
            }

            if (rs.getString("ComCode") == null)
            {
                this.ComCode = null;
            }
            else
            {
                this.ComCode = rs.getString("ComCode").trim();
            }

            if (rs.getString("Name") == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString("Name").trim();
            }

            if (rs.getString("BankCode") == null)
            {
                this.BankCode = null;
            }
            else
            {
                this.BankCode = rs.getString("BankCode").trim();
            }

            if (rs.getString("AccType") == null)
            {
                this.AccType = null;
            }
            else
            {
                this.AccType = rs.getString("AccType").trim();
            }

            if (rs.getString("AccName") == null)
            {
                this.AccName = null;
            }
            else
            {
                this.AccName = rs.getString("AccName").trim();
            }

            if (rs.getString("AccNo") == null)
            {
                this.AccNo = null;
            }
            else
            {
                this.AccNo = rs.getString("AccNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("NoType") == null)
            {
                this.NoType = null;
            }
            else
            {
                this.NoType = rs.getString("NoType").trim();
            }

            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            this.PayMoney = rs.getDouble("PayMoney");
            this.SendDate = rs.getDate("SendDate");
            this.BankDealDate = rs.getDate("BankDealDate");
            this.BankDealTime = rs.getDate("BankDealTime");
            if (rs.getString("BankSuccFlag") == null)
            {
                this.BankSuccFlag = null;
            }
            else
            {
                this.BankSuccFlag = rs.getString("BankSuccFlag").trim();
            }

            if (rs.getString("GetCode") == null)
            {
                this.GetCode = null;
            }
            else
            {
                this.GetCode = rs.getString("GetCode").trim();
            }

            this.GetDate = rs.getDate("GetDate");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYDupPaySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LYDupPaySchema getSchema()
    {
        LYDupPaySchema aLYDupPaySchema = new LYDupPaySchema();
        aLYDupPaySchema.setSchema(this);
        return aLYDupPaySchema;
    }

    public LYDupPayDB getDB()
    {
        LYDupPayDB aDBOper = new LYDupPayDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLYDupPay描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DataType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ComCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Name)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AccNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NoType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayMoney) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(SendDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            BankDealDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            BankDealTime))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BankSuccFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(GetCode)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(GetDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLYDupPay>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            DataType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            PayCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                  SysConst.PACKAGESPILTER);
            BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            AccType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            AccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                   SysConst.PACKAGESPILTER);
            NoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                    SysConst.PACKAGESPILTER);
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            PayMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            SendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            BankDealDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            BankDealTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            BankSuccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                          SysConst.PACKAGESPILTER);
            GetCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                     SysConst.PACKAGESPILTER);
            GetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 23, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                        SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LYDupPaySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SerialNo));
        }
        if (FCode.equals("DataType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DataType));
        }
        if (FCode.equals("PayCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayCode));
        }
        if (FCode.equals("ComCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComCode));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Name));
        }
        if (FCode.equals("BankCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankCode));
        }
        if (FCode.equals("AccType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccType));
        }
        if (FCode.equals("AccName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccName));
        }
        if (FCode.equals("AccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AccNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("NoType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NoType));
        }
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AgentCode));
        }
        if (FCode.equals("PayMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayMoney));
        }
        if (FCode.equals("SendDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getSendDate()));
        }
        if (FCode.equals("BankDealDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getBankDealDate()));
        }
        if (FCode.equals("BankDealTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getBankDealTime()));
        }
        if (FCode.equals("BankSuccFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BankSuccFlag));
        }
        if (FCode.equals("GetCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(GetCode));
        }
        if (FCode.equals("GetDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getGetDate()));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(DataType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(PayCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(BankCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AccType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AccName);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AccNo);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(NoType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 12:
                strFieldValue = String.valueOf(PayMoney);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSendDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBankDealDate()));
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBankDealTime()));
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(BankSuccFlag);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(GetCode);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getGetDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("DataType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DataType = FValue.trim();
            }
            else
            {
                DataType = null;
            }
        }
        if (FCode.equals("PayCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayCode = FValue.trim();
            }
            else
            {
                PayCode = null;
            }
        }
        if (FCode.equals("ComCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
            {
                ComCode = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("BankCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankCode = FValue.trim();
            }
            else
            {
                BankCode = null;
            }
        }
        if (FCode.equals("AccType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccType = FValue.trim();
            }
            else
            {
                AccType = null;
            }
        }
        if (FCode.equals("AccName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccName = FValue.trim();
            }
            else
            {
                AccName = null;
            }
        }
        if (FCode.equals("AccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AccNo = FValue.trim();
            }
            else
            {
                AccNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("NoType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NoType = FValue.trim();
            }
            else
            {
                NoType = null;
            }
        }
        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("PayMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PayMoney = d;
            }
        }
        if (FCode.equals("SendDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SendDate = fDate.getDate(FValue);
            }
            else
            {
                SendDate = null;
            }
        }
        if (FCode.equals("BankDealDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankDealDate = fDate.getDate(FValue);
            }
            else
            {
                BankDealDate = null;
            }
        }
        if (FCode.equals("BankDealTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankDealTime = fDate.getDate(FValue);
            }
            else
            {
                BankDealTime = null;
            }
        }
        if (FCode.equals("BankSuccFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BankSuccFlag = FValue.trim();
            }
            else
            {
                BankSuccFlag = null;
            }
        }
        if (FCode.equals("GetCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetCode = FValue.trim();
            }
            else
            {
                GetCode = null;
            }
        }
        if (FCode.equals("GetDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GetDate = fDate.getDate(FValue);
            }
            else
            {
                GetDate = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LYDupPaySchema other = (LYDupPaySchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && DataType.equals(other.getDataType())
                && PayCode.equals(other.getPayCode())
                && ComCode.equals(other.getComCode())
                && Name.equals(other.getName())
                && BankCode.equals(other.getBankCode())
                && AccType.equals(other.getAccType())
                && AccName.equals(other.getAccName())
                && AccNo.equals(other.getAccNo())
                && PolNo.equals(other.getPolNo())
                && NoType.equals(other.getNoType())
                && AgentCode.equals(other.getAgentCode())
                && PayMoney == other.getPayMoney()
                && fDate.getString(SendDate).equals(other.getSendDate())
                && fDate.getString(BankDealDate).equals(other.getBankDealDate())
                && fDate.getString(BankDealTime).equals(other.getBankDealTime())
                && BankSuccFlag.equals(other.getBankSuccFlag())
                && GetCode.equals(other.getGetCode())
                && fDate.getString(GetDate).equals(other.getGetDate())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && RiskCode.equals(other.getRiskCode());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return 0;
        }
        if (strFieldName.equals("DataType"))
        {
            return 1;
        }
        if (strFieldName.equals("PayCode"))
        {
            return 2;
        }
        if (strFieldName.equals("ComCode"))
        {
            return 3;
        }
        if (strFieldName.equals("Name"))
        {
            return 4;
        }
        if (strFieldName.equals("BankCode"))
        {
            return 5;
        }
        if (strFieldName.equals("AccType"))
        {
            return 6;
        }
        if (strFieldName.equals("AccName"))
        {
            return 7;
        }
        if (strFieldName.equals("AccNo"))
        {
            return 8;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 9;
        }
        if (strFieldName.equals("NoType"))
        {
            return 10;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return 11;
        }
        if (strFieldName.equals("PayMoney"))
        {
            return 12;
        }
        if (strFieldName.equals("SendDate"))
        {
            return 13;
        }
        if (strFieldName.equals("BankDealDate"))
        {
            return 14;
        }
        if (strFieldName.equals("BankDealTime"))
        {
            return 15;
        }
        if (strFieldName.equals("BankSuccFlag"))
        {
            return 16;
        }
        if (strFieldName.equals("GetCode"))
        {
            return 17;
        }
        if (strFieldName.equals("GetDate"))
        {
            return 18;
        }
        if (strFieldName.equals("Operator"))
        {
            return 19;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 20;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 21;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 22;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 23;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 24;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SerialNo";
                break;
            case 1:
                strFieldName = "DataType";
                break;
            case 2:
                strFieldName = "PayCode";
                break;
            case 3:
                strFieldName = "ComCode";
                break;
            case 4:
                strFieldName = "Name";
                break;
            case 5:
                strFieldName = "BankCode";
                break;
            case 6:
                strFieldName = "AccType";
                break;
            case 7:
                strFieldName = "AccName";
                break;
            case 8:
                strFieldName = "AccNo";
                break;
            case 9:
                strFieldName = "PolNo";
                break;
            case 10:
                strFieldName = "NoType";
                break;
            case 11:
                strFieldName = "AgentCode";
                break;
            case 12:
                strFieldName = "PayMoney";
                break;
            case 13:
                strFieldName = "SendDate";
                break;
            case 14:
                strFieldName = "BankDealDate";
                break;
            case 15:
                strFieldName = "BankDealTime";
                break;
            case 16:
                strFieldName = "BankSuccFlag";
                break;
            case 17:
                strFieldName = "GetCode";
                break;
            case 18:
                strFieldName = "GetDate";
                break;
            case 19:
                strFieldName = "Operator";
                break;
            case 20:
                strFieldName = "MakeDate";
                break;
            case 21:
                strFieldName = "MakeTime";
                break;
            case 22:
                strFieldName = "ModifyDate";
                break;
            case 23:
                strFieldName = "ModifyTime";
                break;
            case 24:
                strFieldName = "RiskCode";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DataType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BankCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NoType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SendDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BankDealDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BankDealTime"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BankSuccFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 15:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
