/*
 * <p>ClassName: LITranErrSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LITranErrDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LITranErrSchema implements Schema
{
    // @Field
    /** 批次号 */
    private String BatchNo;
    /** 单证号码 */
    private String BillId;
    /** 业务号1(收据号) */
    private String BussNo;
    /** 业务号2(保单号) */
    private String PolicyNo;
    /** 业务日期 */
    private Date BussDate;
    /** 数据表名 */
    private String TableName;
    /** 错误代码 */
    private String ErrFlag;
    /** 管理机构 */
    private String ManageCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 操作员 */
    private String Operator;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LITranErrSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "BatchNo";
        pk[1] = "BillId";
        pk[2] = "BussNo";
        pk[3] = "PolicyNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getBatchNo()
    {
        if (BatchNo != null && !BatchNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            BatchNo = StrTool.unicodeToGBK(BatchNo);
        }
        return BatchNo;
    }

    public void setBatchNo(String aBatchNo)
    {
        BatchNo = aBatchNo;
    }

    public String getBillId()
    {
        if (BillId != null && !BillId.equals("") && SysConst.CHANGECHARSET == true)
        {
            BillId = StrTool.unicodeToGBK(BillId);
        }
        return BillId;
    }

    public void setBillId(String aBillId)
    {
        BillId = aBillId;
    }

    public String getBussNo()
    {
        if (BussNo != null && !BussNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            BussNo = StrTool.unicodeToGBK(BussNo);
        }
        return BussNo;
    }

    public void setBussNo(String aBussNo)
    {
        BussNo = aBussNo;
    }

    public String getPolicyNo()
    {
        if (PolicyNo != null && !PolicyNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolicyNo = StrTool.unicodeToGBK(PolicyNo);
        }
        return PolicyNo;
    }

    public void setPolicyNo(String aPolicyNo)
    {
        PolicyNo = aPolicyNo;
    }

    public String getBussDate()
    {
        if (BussDate != null)
        {
            return fDate.getString(BussDate);
        }
        else
        {
            return null;
        }
    }

    public void setBussDate(Date aBussDate)
    {
        BussDate = aBussDate;
    }

    public void setBussDate(String aBussDate)
    {
        if (aBussDate != null && !aBussDate.equals(""))
        {
            BussDate = fDate.getDate(aBussDate);
        }
        else
        {
            BussDate = null;
        }
    }

    public String getTableName()
    {
        if (TableName != null && !TableName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TableName = StrTool.unicodeToGBK(TableName);
        }
        return TableName;
    }

    public void setTableName(String aTableName)
    {
        TableName = aTableName;
    }

    public String getErrFlag()
    {
        if (ErrFlag != null && !ErrFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            ErrFlag = StrTool.unicodeToGBK(ErrFlag);
        }
        return ErrFlag;
    }

    public void setErrFlag(String aErrFlag)
    {
        ErrFlag = aErrFlag;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    /**
     * 使用另外一个 LITranErrSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LITranErrSchema aLITranErrSchema)
    {
        this.BatchNo = aLITranErrSchema.getBatchNo();
        this.BillId = aLITranErrSchema.getBillId();
        this.BussNo = aLITranErrSchema.getBussNo();
        this.PolicyNo = aLITranErrSchema.getPolicyNo();
        this.BussDate = fDate.getDate(aLITranErrSchema.getBussDate());
        this.TableName = aLITranErrSchema.getTableName();
        this.ErrFlag = aLITranErrSchema.getErrFlag();
        this.ManageCom = aLITranErrSchema.getManageCom();
        this.MakeDate = fDate.getDate(aLITranErrSchema.getMakeDate());
        this.MakeTime = aLITranErrSchema.getMakeTime();
        this.Operator = aLITranErrSchema.getOperator();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("BatchNo") == null)
            {
                this.BatchNo = null;
            }
            else
            {
                this.BatchNo = rs.getString("BatchNo").trim();
            }

            if (rs.getString("BillId") == null)
            {
                this.BillId = null;
            }
            else
            {
                this.BillId = rs.getString("BillId").trim();
            }

            if (rs.getString("BussNo") == null)
            {
                this.BussNo = null;
            }
            else
            {
                this.BussNo = rs.getString("BussNo").trim();
            }

            if (rs.getString("PolicyNo") == null)
            {
                this.PolicyNo = null;
            }
            else
            {
                this.PolicyNo = rs.getString("PolicyNo").trim();
            }

            this.BussDate = rs.getDate("BussDate");
            if (rs.getString("TableName") == null)
            {
                this.TableName = null;
            }
            else
            {
                this.TableName = rs.getString("TableName").trim();
            }

            if (rs.getString("ErrFlag") == null)
            {
                this.ErrFlag = null;
            }
            else
            {
                this.ErrFlag = rs.getString("ErrFlag").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LITranErrSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LITranErrSchema getSchema()
    {
        LITranErrSchema aLITranErrSchema = new LITranErrSchema();
        aLITranErrSchema.setSchema(this);
        return aLITranErrSchema;
    }

    public LITranErrDB getDB()
    {
        LITranErrDB aDBOper = new LITranErrDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLITranErr描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(BatchNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BillId)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BussNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolicyNo)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(BussDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TableName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ErrFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLITranErr>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            BillId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            BussNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            PolicyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            BussDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            TableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            ErrFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LITranErrSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("BatchNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BatchNo));
        }
        if (FCode.equals("BillId"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BillId));
        }
        if (FCode.equals("BussNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BussNo));
        }
        if (FCode.equals("PolicyNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolicyNo));
        }
        if (FCode.equals("BussDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getBussDate()));
        }
        if (FCode.equals("TableName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TableName));
        }
        if (FCode.equals("ErrFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrFlag));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(BatchNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(BillId);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(BussNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PolicyNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBussDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(TableName);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ErrFlag);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("BatchNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BatchNo = FValue.trim();
            }
            else
            {
                BatchNo = null;
            }
        }
        if (FCode.equals("BillId"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BillId = FValue.trim();
            }
            else
            {
                BillId = null;
            }
        }
        if (FCode.equals("BussNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BussNo = FValue.trim();
            }
            else
            {
                BussNo = null;
            }
        }
        if (FCode.equals("PolicyNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolicyNo = FValue.trim();
            }
            else
            {
                PolicyNo = null;
            }
        }
        if (FCode.equals("BussDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BussDate = fDate.getDate(FValue);
            }
            else
            {
                BussDate = null;
            }
        }
        if (FCode.equals("TableName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TableName = FValue.trim();
            }
            else
            {
                TableName = null;
            }
        }
        if (FCode.equals("ErrFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrFlag = FValue.trim();
            }
            else
            {
                ErrFlag = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LITranErrSchema other = (LITranErrSchema) otherObject;
        return
                BatchNo.equals(other.getBatchNo())
                && BillId.equals(other.getBillId())
                && BussNo.equals(other.getBussNo())
                && PolicyNo.equals(other.getPolicyNo())
                && fDate.getString(BussDate).equals(other.getBussDate())
                && TableName.equals(other.getTableName())
                && ErrFlag.equals(other.getErrFlag())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && Operator.equals(other.getOperator());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("BatchNo"))
        {
            return 0;
        }
        if (strFieldName.equals("BillId"))
        {
            return 1;
        }
        if (strFieldName.equals("BussNo"))
        {
            return 2;
        }
        if (strFieldName.equals("PolicyNo"))
        {
            return 3;
        }
        if (strFieldName.equals("BussDate"))
        {
            return 4;
        }
        if (strFieldName.equals("TableName"))
        {
            return 5;
        }
        if (strFieldName.equals("ErrFlag"))
        {
            return 6;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 8;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 9;
        }
        if (strFieldName.equals("Operator"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "BatchNo";
                break;
            case 1:
                strFieldName = "BillId";
                break;
            case 2:
                strFieldName = "BussNo";
                break;
            case 3:
                strFieldName = "PolicyNo";
                break;
            case 4:
                strFieldName = "BussDate";
                break;
            case 5:
                strFieldName = "TableName";
                break;
            case 6:
                strFieldName = "ErrFlag";
                break;
            case 7:
                strFieldName = "ManageCom";
                break;
            case 8:
                strFieldName = "MakeDate";
                break;
            case 9:
                strFieldName = "MakeTime";
                break;
            case 10:
                strFieldName = "Operator";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("BatchNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BillId"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BussNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolicyNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BussDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("TableName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
