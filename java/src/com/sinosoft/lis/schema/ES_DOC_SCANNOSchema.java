/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.ES_DOC_SCANNODB;

/*
 * <p>ClassName: ES_DOC_SCANNOSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2018-06-25
 */
public class ES_DOC_SCANNOSchema implements Schema, Cloneable
{
	// @Field
	/** Scan_no */
	private String scan_no;
	/** Manage_com */
	private String manage_com;
	/** Del_flag */
	private String del_flag;
	/** Create_user */
	private String create_user;
	/** Create_date */
	private Date create_date;
	/** Update_user */
	private String update_user;
	/** Update_date */
	private Date update_date;
	/** P1 */
	private String p1;
	/** P2 */
	private String p2;
	/** P3 */
	private String p3;
	/** P4 */
	private String p4;
	/** P5 */
	private String p5;
	/** P6 */
	private Date p6;
	/** P7 */
	private Date p7;
	/** P8 */
	private double p8;
	/** P9 */
	private double p9;
	/** Buss_type */
	private String buss_type;
	/** Isfull */
	private String isfull;

	public static final int FIELDNUM = 18;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public ES_DOC_SCANNOSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "scan_no";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		ES_DOC_SCANNOSchema cloned = (ES_DOC_SCANNOSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getscan_no()
	{
		return scan_no;
	}
	public void setscan_no(String ascan_no)
	{
		scan_no = ascan_no;
	}
	public String getmanage_com()
	{
		return manage_com;
	}
	public void setmanage_com(String amanage_com)
	{
		manage_com = amanage_com;
	}
	public String getdel_flag()
	{
		return del_flag;
	}
	public void setdel_flag(String adel_flag)
	{
		del_flag = adel_flag;
	}
	public String getcreate_user()
	{
		return create_user;
	}
	public void setcreate_user(String acreate_user)
	{
		create_user = acreate_user;
	}
	public String getcreate_date()
	{
		if( create_date != null )
			return fDate.getString(create_date);
		else
			return null;
	}
	public void setcreate_date(Date acreate_date)
	{
		create_date = acreate_date;
	}
	public void setcreate_date(String acreate_date)
	{
		if (acreate_date != null && !acreate_date.equals("") )
		{
			create_date = fDate.getDate( acreate_date );
		}
		else
			create_date = null;
	}

	public String getupdate_user()
	{
		return update_user;
	}
	public void setupdate_user(String aupdate_user)
	{
		update_user = aupdate_user;
	}
	public String getupdate_date()
	{
		if( update_date != null )
			return fDate.getString(update_date);
		else
			return null;
	}
	public void setupdate_date(Date aupdate_date)
	{
		update_date = aupdate_date;
	}
	public void setupdate_date(String aupdate_date)
	{
		if (aupdate_date != null && !aupdate_date.equals("") )
		{
			update_date = fDate.getDate( aupdate_date );
		}
		else
			update_date = null;
	}

	public String getp1()
	{
		return p1;
	}
	public void setp1(String ap1)
	{
		p1 = ap1;
	}
	public String getp2()
	{
		return p2;
	}
	public void setp2(String ap2)
	{
		p2 = ap2;
	}
	public String getp3()
	{
		return p3;
	}
	public void setp3(String ap3)
	{
		p3 = ap3;
	}
	public String getp4()
	{
		return p4;
	}
	public void setp4(String ap4)
	{
		p4 = ap4;
	}
	public String getp5()
	{
		return p5;
	}
	public void setp5(String ap5)
	{
		p5 = ap5;
	}
	public String getp6()
	{
		if( p6 != null )
			return fDate.getString(p6);
		else
			return null;
	}
	public void setp6(Date ap6)
	{
		p6 = ap6;
	}
	public void setp6(String ap6)
	{
		if (ap6 != null && !ap6.equals("") )
		{
			p6 = fDate.getDate( ap6 );
		}
		else
			p6 = null;
	}

	public String getp7()
	{
		if( p7 != null )
			return fDate.getString(p7);
		else
			return null;
	}
	public void setp7(Date ap7)
	{
		p7 = ap7;
	}
	public void setp7(String ap7)
	{
		if (ap7 != null && !ap7.equals("") )
		{
			p7 = fDate.getDate( ap7 );
		}
		else
			p7 = null;
	}

	public double getp8()
	{
		return p8;
	}
	public void setp8(double ap8)
	{
		p8 = Arith.round(ap8,0);
	}
	public void setp8(String ap8)
	{
		if (ap8 != null && !ap8.equals(""))
		{
			Double tDouble = new Double(ap8);
			double d = tDouble.doubleValue();
                p8 = Arith.round(d,0);
		}
	}

	public double getp9()
	{
		return p9;
	}
	public void setp9(double ap9)
	{
		p9 = Arith.round(ap9,0);
	}
	public void setp9(String ap9)
	{
		if (ap9 != null && !ap9.equals(""))
		{
			Double tDouble = new Double(ap9);
			double d = tDouble.doubleValue();
                p9 = Arith.round(d,0);
		}
	}

	public String getbuss_type()
	{
		return buss_type;
	}
	public void setbuss_type(String abuss_type)
	{
		buss_type = abuss_type;
	}
	public String getisfull()
	{
		return isfull;
	}
	public void setisfull(String aisfull)
	{
		isfull = aisfull;
	}

	/**
	* 使用另外一个 ES_DOC_SCANNOSchema 对象给 Schema 赋值
	* @param: aES_DOC_SCANNOSchema ES_DOC_SCANNOSchema
	**/
	public void setSchema(ES_DOC_SCANNOSchema aES_DOC_SCANNOSchema)
	{
		this.scan_no = aES_DOC_SCANNOSchema.getscan_no();
		this.manage_com = aES_DOC_SCANNOSchema.getmanage_com();
		this.del_flag = aES_DOC_SCANNOSchema.getdel_flag();
		this.create_user = aES_DOC_SCANNOSchema.getcreate_user();
		this.create_date = fDate.getDate( aES_DOC_SCANNOSchema.getcreate_date());
		this.update_user = aES_DOC_SCANNOSchema.getupdate_user();
		this.update_date = fDate.getDate( aES_DOC_SCANNOSchema.getupdate_date());
		this.p1 = aES_DOC_SCANNOSchema.getp1();
		this.p2 = aES_DOC_SCANNOSchema.getp2();
		this.p3 = aES_DOC_SCANNOSchema.getp3();
		this.p4 = aES_DOC_SCANNOSchema.getp4();
		this.p5 = aES_DOC_SCANNOSchema.getp5();
		this.p6 = fDate.getDate( aES_DOC_SCANNOSchema.getp6());
		this.p7 = fDate.getDate( aES_DOC_SCANNOSchema.getp7());
		this.p8 = aES_DOC_SCANNOSchema.getp8();
		this.p9 = aES_DOC_SCANNOSchema.getp9();
		this.buss_type = aES_DOC_SCANNOSchema.getbuss_type();
		this.isfull = aES_DOC_SCANNOSchema.getisfull();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("scan_no") == null )
				this.scan_no = null;
			else
				this.scan_no = rs.getString("scan_no").trim();

			if( rs.getString("manage_com") == null )
				this.manage_com = null;
			else
				this.manage_com = rs.getString("manage_com").trim();

			if( rs.getString("del_flag") == null )
				this.del_flag = null;
			else
				this.del_flag = rs.getString("del_flag").trim();

			if( rs.getString("create_user") == null )
				this.create_user = null;
			else
				this.create_user = rs.getString("create_user").trim();

			this.create_date = rs.getDate("create_date");
			if( rs.getString("update_user") == null )
				this.update_user = null;
			else
				this.update_user = rs.getString("update_user").trim();

			this.update_date = rs.getDate("update_date");
			if( rs.getString("p1") == null )
				this.p1 = null;
			else
				this.p1 = rs.getString("p1").trim();

			if( rs.getString("p2") == null )
				this.p2 = null;
			else
				this.p2 = rs.getString("p2").trim();

			if( rs.getString("p3") == null )
				this.p3 = null;
			else
				this.p3 = rs.getString("p3").trim();

			if( rs.getString("p4") == null )
				this.p4 = null;
			else
				this.p4 = rs.getString("p4").trim();

			if( rs.getString("p5") == null )
				this.p5 = null;
			else
				this.p5 = rs.getString("p5").trim();

			this.p6 = rs.getDate("p6");
			this.p7 = rs.getDate("p7");
			this.p8 = rs.getDouble("p8");
			this.p9 = rs.getDouble("p9");
			if( rs.getString("buss_type") == null )
				this.buss_type = null;
			else
				this.buss_type = rs.getString("buss_type").trim();

			if( rs.getString("isfull") == null )
				this.isfull = null;
			else
				this.isfull = rs.getString("isfull").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的ES_DOC_SCANNO表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ES_DOC_SCANNOSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public ES_DOC_SCANNOSchema getSchema()
	{
		ES_DOC_SCANNOSchema aES_DOC_SCANNOSchema = new ES_DOC_SCANNOSchema();
		aES_DOC_SCANNOSchema.setSchema(this);
		return aES_DOC_SCANNOSchema;
	}

	public ES_DOC_SCANNODB getDB()
	{
		ES_DOC_SCANNODB aDBOper = new ES_DOC_SCANNODB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpES_DOC_SCANNO描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(scan_no)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(manage_com)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(del_flag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(create_user)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( create_date ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(update_user)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( update_date ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(p1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(p2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(p3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(p4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(p5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( p6 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( p7 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(p8));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(p9));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(buss_type)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(isfull));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpES_DOC_SCANNO>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			scan_no = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			manage_com = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			del_flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			create_user = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			create_date = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			update_user = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			update_date = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			p1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			p2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			p3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			p4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			p5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			p6 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			p7 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			p8 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			p9 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			buss_type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			isfull = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ES_DOC_SCANNOSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("scan_no"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(scan_no));
		}
		if (FCode.equals("manage_com"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(manage_com));
		}
		if (FCode.equals("del_flag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(del_flag));
		}
		if (FCode.equals("create_user"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(create_user));
		}
		if (FCode.equals("create_date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getcreate_date()));
		}
		if (FCode.equals("update_user"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(update_user));
		}
		if (FCode.equals("update_date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getupdate_date()));
		}
		if (FCode.equals("p1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(p1));
		}
		if (FCode.equals("p2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(p2));
		}
		if (FCode.equals("p3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(p3));
		}
		if (FCode.equals("p4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(p4));
		}
		if (FCode.equals("p5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(p5));
		}
		if (FCode.equals("p6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getp6()));
		}
		if (FCode.equals("p7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getp7()));
		}
		if (FCode.equals("p8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(p8));
		}
		if (FCode.equals("p9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(p9));
		}
		if (FCode.equals("buss_type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(buss_type));
		}
		if (FCode.equals("isfull"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(isfull));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(scan_no);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(manage_com);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(del_flag);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(create_user);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getcreate_date()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(update_user);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getupdate_date()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(p1);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(p2);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(p3);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(p4);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(p5);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getp6()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getp7()));
				break;
			case 14:
				strFieldValue = String.valueOf(p8);
				break;
			case 15:
				strFieldValue = String.valueOf(p9);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(buss_type);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(isfull);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("scan_no"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				scan_no = FValue.trim();
			}
			else
				scan_no = null;
		}
		if (FCode.equalsIgnoreCase("manage_com"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				manage_com = FValue.trim();
			}
			else
				manage_com = null;
		}
		if (FCode.equalsIgnoreCase("del_flag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				del_flag = FValue.trim();
			}
			else
				del_flag = null;
		}
		if (FCode.equalsIgnoreCase("create_user"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				create_user = FValue.trim();
			}
			else
				create_user = null;
		}
		if (FCode.equalsIgnoreCase("create_date"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				create_date = fDate.getDate( FValue );
			}
			else
				create_date = null;
		}
		if (FCode.equalsIgnoreCase("update_user"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				update_user = FValue.trim();
			}
			else
				update_user = null;
		}
		if (FCode.equalsIgnoreCase("update_date"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				update_date = fDate.getDate( FValue );
			}
			else
				update_date = null;
		}
		if (FCode.equalsIgnoreCase("p1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				p1 = FValue.trim();
			}
			else
				p1 = null;
		}
		if (FCode.equalsIgnoreCase("p2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				p2 = FValue.trim();
			}
			else
				p2 = null;
		}
		if (FCode.equalsIgnoreCase("p3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				p3 = FValue.trim();
			}
			else
				p3 = null;
		}
		if (FCode.equalsIgnoreCase("p4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				p4 = FValue.trim();
			}
			else
				p4 = null;
		}
		if (FCode.equalsIgnoreCase("p5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				p5 = FValue.trim();
			}
			else
				p5 = null;
		}
		if (FCode.equalsIgnoreCase("p6"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				p6 = fDate.getDate( FValue );
			}
			else
				p6 = null;
		}
		if (FCode.equalsIgnoreCase("p7"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				p7 = fDate.getDate( FValue );
			}
			else
				p7 = null;
		}
		if (FCode.equalsIgnoreCase("p8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				p8 = d;
			}
		}
		if (FCode.equalsIgnoreCase("p9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				p9 = d;
			}
		}
		if (FCode.equalsIgnoreCase("buss_type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				buss_type = FValue.trim();
			}
			else
				buss_type = null;
		}
		if (FCode.equalsIgnoreCase("isfull"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				isfull = FValue.trim();
			}
			else
				isfull = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		ES_DOC_SCANNOSchema other = (ES_DOC_SCANNOSchema)otherObject;
		return
			(scan_no == null ? other.getscan_no() == null : scan_no.equals(other.getscan_no()))
			&& (manage_com == null ? other.getmanage_com() == null : manage_com.equals(other.getmanage_com()))
			&& (del_flag == null ? other.getdel_flag() == null : del_flag.equals(other.getdel_flag()))
			&& (create_user == null ? other.getcreate_user() == null : create_user.equals(other.getcreate_user()))
			&& (create_date == null ? other.getcreate_date() == null : fDate.getString(create_date).equals(other.getcreate_date()))
			&& (update_user == null ? other.getupdate_user() == null : update_user.equals(other.getupdate_user()))
			&& (update_date == null ? other.getupdate_date() == null : fDate.getString(update_date).equals(other.getupdate_date()))
			&& (p1 == null ? other.getp1() == null : p1.equals(other.getp1()))
			&& (p2 == null ? other.getp2() == null : p2.equals(other.getp2()))
			&& (p3 == null ? other.getp3() == null : p3.equals(other.getp3()))
			&& (p4 == null ? other.getp4() == null : p4.equals(other.getp4()))
			&& (p5 == null ? other.getp5() == null : p5.equals(other.getp5()))
			&& (p6 == null ? other.getp6() == null : fDate.getString(p6).equals(other.getp6()))
			&& (p7 == null ? other.getp7() == null : fDate.getString(p7).equals(other.getp7()))
			&& p8 == other.getp8()
			&& p9 == other.getp9()
			&& (buss_type == null ? other.getbuss_type() == null : buss_type.equals(other.getbuss_type()))
			&& (isfull == null ? other.getisfull() == null : isfull.equals(other.getisfull()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("scan_no") ) {
			return 0;
		}
		if( strFieldName.equals("manage_com") ) {
			return 1;
		}
		if( strFieldName.equals("del_flag") ) {
			return 2;
		}
		if( strFieldName.equals("create_user") ) {
			return 3;
		}
		if( strFieldName.equals("create_date") ) {
			return 4;
		}
		if( strFieldName.equals("update_user") ) {
			return 5;
		}
		if( strFieldName.equals("update_date") ) {
			return 6;
		}
		if( strFieldName.equals("p1") ) {
			return 7;
		}
		if( strFieldName.equals("p2") ) {
			return 8;
		}
		if( strFieldName.equals("p3") ) {
			return 9;
		}
		if( strFieldName.equals("p4") ) {
			return 10;
		}
		if( strFieldName.equals("p5") ) {
			return 11;
		}
		if( strFieldName.equals("p6") ) {
			return 12;
		}
		if( strFieldName.equals("p7") ) {
			return 13;
		}
		if( strFieldName.equals("p8") ) {
			return 14;
		}
		if( strFieldName.equals("p9") ) {
			return 15;
		}
		if( strFieldName.equals("buss_type") ) {
			return 16;
		}
		if( strFieldName.equals("isfull") ) {
			return 17;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "scan_no";
				break;
			case 1:
				strFieldName = "manage_com";
				break;
			case 2:
				strFieldName = "del_flag";
				break;
			case 3:
				strFieldName = "create_user";
				break;
			case 4:
				strFieldName = "create_date";
				break;
			case 5:
				strFieldName = "update_user";
				break;
			case 6:
				strFieldName = "update_date";
				break;
			case 7:
				strFieldName = "p1";
				break;
			case 8:
				strFieldName = "p2";
				break;
			case 9:
				strFieldName = "p3";
				break;
			case 10:
				strFieldName = "p4";
				break;
			case 11:
				strFieldName = "p5";
				break;
			case 12:
				strFieldName = "p6";
				break;
			case 13:
				strFieldName = "p7";
				break;
			case 14:
				strFieldName = "p8";
				break;
			case 15:
				strFieldName = "p9";
				break;
			case 16:
				strFieldName = "buss_type";
				break;
			case 17:
				strFieldName = "isfull";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("scan_no") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("manage_com") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("del_flag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("create_user") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("create_date") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("update_user") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("update_date") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("p1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("p2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("p3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("p4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("p5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("p6") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("p7") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("p8") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("p9") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("buss_type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("isfull") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
