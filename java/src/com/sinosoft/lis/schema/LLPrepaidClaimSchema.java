/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLPrepaidClaimDB;

/*
 * <p>ClassName: LLPrepaidClaimSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新系统模型
 * @CreateDate：2010-12-08
 */
public class LLPrepaidClaimSchema implements Schema, Cloneable
{
	// @Field
	/** 预付赔款号 */
	private String PrepaidNo;
	/** 预付赔款类型 */
	private String RgtType;
	/** 预付赔款状态 */
	private String RgtState;
	/** 团体保单号 */
	private String GrpContNo;
	/** 投保单位名称 */
	private String GrpName;
	/** 申请日期 */
	private Date RgtDate;
	/** 申请人 */
	private String Register;
	/** 撤销人 */
	private String Canceler;
	/** 撤销日期 */
	private Date CancelDate;
	/** 审批人 */
	private String Handler;
	/** 审批日期 */
	private Date HandleDate;
	/** 审定人 */
	private String Dealer;
	/** 审定完成日期 */
	private Date EndDate;
	/** 通知人 */
	private String Noticer;
	/** 通知日期 */
	private Date NoticeDate;
	/** 收付方式 */
	private String PayMode;
	/** 银行编码 */
	private String BankCode;
	/** 银行账号 */
	private String BankAccNo;
	/** 账户名 */
	private String AccName;
	/** 预付总金额 */
	private double SumMoney;
	/** 备注 */
	private String Remark;
	/** 管理机构 */
	private String ManageCom;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 27;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLPrepaidClaimSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "PrepaidNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLPrepaidClaimSchema cloned = (LLPrepaidClaimSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPrepaidNo()
	{
		return PrepaidNo;
	}
	public void setPrepaidNo(String aPrepaidNo)
	{
		PrepaidNo = aPrepaidNo;
	}
	public String getRgtType()
	{
		return RgtType;
	}
	public void setRgtType(String aRgtType)
	{
		RgtType = aRgtType;
	}
	public String getRgtState()
	{
		return RgtState;
	}
	public void setRgtState(String aRgtState)
	{
		RgtState = aRgtState;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getRgtDate()
	{
		if( RgtDate != null )
			return fDate.getString(RgtDate);
		else
			return null;
	}
	public void setRgtDate(Date aRgtDate)
	{
		RgtDate = aRgtDate;
	}
	public void setRgtDate(String aRgtDate)
	{
		if (aRgtDate != null && !aRgtDate.equals("") )
		{
			RgtDate = fDate.getDate( aRgtDate );
		}
		else
			RgtDate = null;
	}

	public String getRegister()
	{
		return Register;
	}
	public void setRegister(String aRegister)
	{
		Register = aRegister;
	}
	public String getCanceler()
	{
		return Canceler;
	}
	public void setCanceler(String aCanceler)
	{
		Canceler = aCanceler;
	}
	public String getCancelDate()
	{
		if( CancelDate != null )
			return fDate.getString(CancelDate);
		else
			return null;
	}
	public void setCancelDate(Date aCancelDate)
	{
		CancelDate = aCancelDate;
	}
	public void setCancelDate(String aCancelDate)
	{
		if (aCancelDate != null && !aCancelDate.equals("") )
		{
			CancelDate = fDate.getDate( aCancelDate );
		}
		else
			CancelDate = null;
	}

	public String getHandler()
	{
		return Handler;
	}
	public void setHandler(String aHandler)
	{
		Handler = aHandler;
	}
	public String getHandleDate()
	{
		if( HandleDate != null )
			return fDate.getString(HandleDate);
		else
			return null;
	}
	public void setHandleDate(Date aHandleDate)
	{
		HandleDate = aHandleDate;
	}
	public void setHandleDate(String aHandleDate)
	{
		if (aHandleDate != null && !aHandleDate.equals("") )
		{
			HandleDate = fDate.getDate( aHandleDate );
		}
		else
			HandleDate = null;
	}

	public String getDealer()
	{
		return Dealer;
	}
	public void setDealer(String aDealer)
	{
		Dealer = aDealer;
	}
	public String getEndDate()
	{
		if( EndDate != null )
			return fDate.getString(EndDate);
		else
			return null;
	}
	public void setEndDate(Date aEndDate)
	{
		EndDate = aEndDate;
	}
	public void setEndDate(String aEndDate)
	{
		if (aEndDate != null && !aEndDate.equals("") )
		{
			EndDate = fDate.getDate( aEndDate );
		}
		else
			EndDate = null;
	}

	public String getNoticer()
	{
		return Noticer;
	}
	public void setNoticer(String aNoticer)
	{
		Noticer = aNoticer;
	}
	public String getNoticeDate()
	{
		if( NoticeDate != null )
			return fDate.getString(NoticeDate);
		else
			return null;
	}
	public void setNoticeDate(Date aNoticeDate)
	{
		NoticeDate = aNoticeDate;
	}
	public void setNoticeDate(String aNoticeDate)
	{
		if (aNoticeDate != null && !aNoticeDate.equals("") )
		{
			NoticeDate = fDate.getDate( aNoticeDate );
		}
		else
			NoticeDate = null;
	}

	public String getPayMode()
	{
		return PayMode;
	}
	public void setPayMode(String aPayMode)
	{
		PayMode = aPayMode;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public double getSumMoney()
	{
		return SumMoney;
	}
	public void setSumMoney(double aSumMoney)
	{
		SumMoney = Arith.round(aSumMoney,2);
	}
	public void setSumMoney(String aSumMoney)
	{
		if (aSumMoney != null && !aSumMoney.equals(""))
		{
			Double tDouble = new Double(aSumMoney);
			double d = tDouble.doubleValue();
                SumMoney = Arith.round(d,2);
		}
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLPrepaidClaimSchema 对象给 Schema 赋值
	* @param: aLLPrepaidClaimSchema LLPrepaidClaimSchema
	**/
	public void setSchema(LLPrepaidClaimSchema aLLPrepaidClaimSchema)
	{
		this.PrepaidNo = aLLPrepaidClaimSchema.getPrepaidNo();
		this.RgtType = aLLPrepaidClaimSchema.getRgtType();
		this.RgtState = aLLPrepaidClaimSchema.getRgtState();
		this.GrpContNo = aLLPrepaidClaimSchema.getGrpContNo();
		this.GrpName = aLLPrepaidClaimSchema.getGrpName();
		this.RgtDate = fDate.getDate( aLLPrepaidClaimSchema.getRgtDate());
		this.Register = aLLPrepaidClaimSchema.getRegister();
		this.Canceler = aLLPrepaidClaimSchema.getCanceler();
		this.CancelDate = fDate.getDate( aLLPrepaidClaimSchema.getCancelDate());
		this.Handler = aLLPrepaidClaimSchema.getHandler();
		this.HandleDate = fDate.getDate( aLLPrepaidClaimSchema.getHandleDate());
		this.Dealer = aLLPrepaidClaimSchema.getDealer();
		this.EndDate = fDate.getDate( aLLPrepaidClaimSchema.getEndDate());
		this.Noticer = aLLPrepaidClaimSchema.getNoticer();
		this.NoticeDate = fDate.getDate( aLLPrepaidClaimSchema.getNoticeDate());
		this.PayMode = aLLPrepaidClaimSchema.getPayMode();
		this.BankCode = aLLPrepaidClaimSchema.getBankCode();
		this.BankAccNo = aLLPrepaidClaimSchema.getBankAccNo();
		this.AccName = aLLPrepaidClaimSchema.getAccName();
		this.SumMoney = aLLPrepaidClaimSchema.getSumMoney();
		this.Remark = aLLPrepaidClaimSchema.getRemark();
		this.ManageCom = aLLPrepaidClaimSchema.getManageCom();
		this.Operator = aLLPrepaidClaimSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLPrepaidClaimSchema.getMakeDate());
		this.MakeTime = aLLPrepaidClaimSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLPrepaidClaimSchema.getModifyDate());
		this.ModifyTime = aLLPrepaidClaimSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PrepaidNo") == null )
				this.PrepaidNo = null;
			else
				this.PrepaidNo = rs.getString("PrepaidNo").trim();

			if( rs.getString("RgtType") == null )
				this.RgtType = null;
			else
				this.RgtType = rs.getString("RgtType").trim();

			if( rs.getString("RgtState") == null )
				this.RgtState = null;
			else
				this.RgtState = rs.getString("RgtState").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			this.RgtDate = rs.getDate("RgtDate");
			if( rs.getString("Register") == null )
				this.Register = null;
			else
				this.Register = rs.getString("Register").trim();

			if( rs.getString("Canceler") == null )
				this.Canceler = null;
			else
				this.Canceler = rs.getString("Canceler").trim();

			this.CancelDate = rs.getDate("CancelDate");
			if( rs.getString("Handler") == null )
				this.Handler = null;
			else
				this.Handler = rs.getString("Handler").trim();

			this.HandleDate = rs.getDate("HandleDate");
			if( rs.getString("Dealer") == null )
				this.Dealer = null;
			else
				this.Dealer = rs.getString("Dealer").trim();

			this.EndDate = rs.getDate("EndDate");
			if( rs.getString("Noticer") == null )
				this.Noticer = null;
			else
				this.Noticer = rs.getString("Noticer").trim();

			this.NoticeDate = rs.getDate("NoticeDate");
			if( rs.getString("PayMode") == null )
				this.PayMode = null;
			else
				this.PayMode = rs.getString("PayMode").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			this.SumMoney = rs.getDouble("SumMoney");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLPrepaidClaim表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLPrepaidClaimSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLPrepaidClaimSchema getSchema()
	{
		LLPrepaidClaimSchema aLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
		aLLPrepaidClaimSchema.setSchema(this);
		return aLLPrepaidClaimSchema;
	}

	public LLPrepaidClaimDB getDB()
	{
		LLPrepaidClaimDB aDBOper = new LLPrepaidClaimDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLPrepaidClaim描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PrepaidNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RgtDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Register)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Canceler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CancelDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HandleDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Dealer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Noticer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( NoticeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLPrepaidClaim>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PrepaidNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RgtType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RgtState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RgtDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			Register = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Canceler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			CancelDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			HandleDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			Dealer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			Noticer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			NoticeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			SumMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLPrepaidClaimSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PrepaidNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidNo));
		}
		if (FCode.equals("RgtType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtType));
		}
		if (FCode.equals("RgtState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtState));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("RgtDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRgtDate()));
		}
		if (FCode.equals("Register"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Register));
		}
		if (FCode.equals("Canceler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Canceler));
		}
		if (FCode.equals("CancelDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCancelDate()));
		}
		if (FCode.equals("Handler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
		}
		if (FCode.equals("HandleDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHandleDate()));
		}
		if (FCode.equals("Dealer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dealer));
		}
		if (FCode.equals("EndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
		}
		if (FCode.equals("Noticer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Noticer));
		}
		if (FCode.equals("NoticeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getNoticeDate()));
		}
		if (FCode.equals("PayMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("SumMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumMoney));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PrepaidNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RgtType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RgtState);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRgtDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Register);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Canceler);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCancelDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Handler);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHandleDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Dealer);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Noticer);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getNoticeDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(PayMode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 19:
				strFieldValue = String.valueOf(SumMoney);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PrepaidNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrepaidNo = FValue.trim();
			}
			else
				PrepaidNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtType = FValue.trim();
			}
			else
				RgtType = null;
		}
		if (FCode.equalsIgnoreCase("RgtState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtState = FValue.trim();
			}
			else
				RgtState = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("RgtDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RgtDate = fDate.getDate( FValue );
			}
			else
				RgtDate = null;
		}
		if (FCode.equalsIgnoreCase("Register"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Register = FValue.trim();
			}
			else
				Register = null;
		}
		if (FCode.equalsIgnoreCase("Canceler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Canceler = FValue.trim();
			}
			else
				Canceler = null;
		}
		if (FCode.equalsIgnoreCase("CancelDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CancelDate = fDate.getDate( FValue );
			}
			else
				CancelDate = null;
		}
		if (FCode.equalsIgnoreCase("Handler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler = FValue.trim();
			}
			else
				Handler = null;
		}
		if (FCode.equalsIgnoreCase("HandleDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HandleDate = fDate.getDate( FValue );
			}
			else
				HandleDate = null;
		}
		if (FCode.equalsIgnoreCase("Dealer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dealer = FValue.trim();
			}
			else
				Dealer = null;
		}
		if (FCode.equalsIgnoreCase("EndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndDate = fDate.getDate( FValue );
			}
			else
				EndDate = null;
		}
		if (FCode.equalsIgnoreCase("Noticer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Noticer = FValue.trim();
			}
			else
				Noticer = null;
		}
		if (FCode.equalsIgnoreCase("NoticeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				NoticeDate = fDate.getDate( FValue );
			}
			else
				NoticeDate = null;
		}
		if (FCode.equalsIgnoreCase("PayMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayMode = FValue.trim();
			}
			else
				PayMode = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("SumMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLPrepaidClaimSchema other = (LLPrepaidClaimSchema)otherObject;
		return
			(PrepaidNo == null ? other.getPrepaidNo() == null : PrepaidNo.equals(other.getPrepaidNo()))
			&& (RgtType == null ? other.getRgtType() == null : RgtType.equals(other.getRgtType()))
			&& (RgtState == null ? other.getRgtState() == null : RgtState.equals(other.getRgtState()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (RgtDate == null ? other.getRgtDate() == null : fDate.getString(RgtDate).equals(other.getRgtDate()))
			&& (Register == null ? other.getRegister() == null : Register.equals(other.getRegister()))
			&& (Canceler == null ? other.getCanceler() == null : Canceler.equals(other.getCanceler()))
			&& (CancelDate == null ? other.getCancelDate() == null : fDate.getString(CancelDate).equals(other.getCancelDate()))
			&& (Handler == null ? other.getHandler() == null : Handler.equals(other.getHandler()))
			&& (HandleDate == null ? other.getHandleDate() == null : fDate.getString(HandleDate).equals(other.getHandleDate()))
			&& (Dealer == null ? other.getDealer() == null : Dealer.equals(other.getDealer()))
			&& (EndDate == null ? other.getEndDate() == null : fDate.getString(EndDate).equals(other.getEndDate()))
			&& (Noticer == null ? other.getNoticer() == null : Noticer.equals(other.getNoticer()))
			&& (NoticeDate == null ? other.getNoticeDate() == null : fDate.getString(NoticeDate).equals(other.getNoticeDate()))
			&& (PayMode == null ? other.getPayMode() == null : PayMode.equals(other.getPayMode()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& SumMoney == other.getSumMoney()
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PrepaidNo") ) {
			return 0;
		}
		if( strFieldName.equals("RgtType") ) {
			return 1;
		}
		if( strFieldName.equals("RgtState") ) {
			return 2;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 3;
		}
		if( strFieldName.equals("GrpName") ) {
			return 4;
		}
		if( strFieldName.equals("RgtDate") ) {
			return 5;
		}
		if( strFieldName.equals("Register") ) {
			return 6;
		}
		if( strFieldName.equals("Canceler") ) {
			return 7;
		}
		if( strFieldName.equals("CancelDate") ) {
			return 8;
		}
		if( strFieldName.equals("Handler") ) {
			return 9;
		}
		if( strFieldName.equals("HandleDate") ) {
			return 10;
		}
		if( strFieldName.equals("Dealer") ) {
			return 11;
		}
		if( strFieldName.equals("EndDate") ) {
			return 12;
		}
		if( strFieldName.equals("Noticer") ) {
			return 13;
		}
		if( strFieldName.equals("NoticeDate") ) {
			return 14;
		}
		if( strFieldName.equals("PayMode") ) {
			return 15;
		}
		if( strFieldName.equals("BankCode") ) {
			return 16;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 17;
		}
		if( strFieldName.equals("AccName") ) {
			return 18;
		}
		if( strFieldName.equals("SumMoney") ) {
			return 19;
		}
		if( strFieldName.equals("Remark") ) {
			return 20;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 21;
		}
		if( strFieldName.equals("Operator") ) {
			return 22;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 23;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 25;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 26;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PrepaidNo";
				break;
			case 1:
				strFieldName = "RgtType";
				break;
			case 2:
				strFieldName = "RgtState";
				break;
			case 3:
				strFieldName = "GrpContNo";
				break;
			case 4:
				strFieldName = "GrpName";
				break;
			case 5:
				strFieldName = "RgtDate";
				break;
			case 6:
				strFieldName = "Register";
				break;
			case 7:
				strFieldName = "Canceler";
				break;
			case 8:
				strFieldName = "CancelDate";
				break;
			case 9:
				strFieldName = "Handler";
				break;
			case 10:
				strFieldName = "HandleDate";
				break;
			case 11:
				strFieldName = "Dealer";
				break;
			case 12:
				strFieldName = "EndDate";
				break;
			case 13:
				strFieldName = "Noticer";
				break;
			case 14:
				strFieldName = "NoticeDate";
				break;
			case 15:
				strFieldName = "PayMode";
				break;
			case 16:
				strFieldName = "BankCode";
				break;
			case 17:
				strFieldName = "BankAccNo";
				break;
			case 18:
				strFieldName = "AccName";
				break;
			case 19:
				strFieldName = "SumMoney";
				break;
			case 20:
				strFieldName = "Remark";
				break;
			case 21:
				strFieldName = "ManageCom";
				break;
			case 22:
				strFieldName = "Operator";
				break;
			case 23:
				strFieldName = "MakeDate";
				break;
			case 24:
				strFieldName = "MakeTime";
				break;
			case 25:
				strFieldName = "ModifyDate";
				break;
			case 26:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PrepaidNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Register") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Canceler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CancelDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Handler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandleDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Dealer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Noticer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NoticeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PayMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
