/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAWageRadixAllTraceDB;

/*
 * <p>ClassName: LAWageRadixAllTraceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 团体销售新建表
 * @CreateDate：2015-09-28
 */
public class LAWageRadixAllTraceSchema implements Schema, Cloneable
{
	// @Field
	/** 转储号码 */
	private String EdorNo;
	/** 转储类型 */
	private String EdorType;
	/** 系列号 */
	private String Idx;
	/** 轨迹标识 */
	private String TraceFlag;
	/** 修改标识 */
	private String ModifyFlag;
	/** 代理人职级 */
	private String AgentGrade;
	/** 管理机构 */
	private String ManageCom;
	/** 代理机构 */
	private String AgentCom;
	/** 薪资类型 */
	private String WageType;
	/** 奖金代码 */
	private String WageCode;
	/** 奖金名称 */
	private String WageName;
	/** 薪资月 */
	private String WageNo;
	/** 奖金金额 */
	private double RewardMoney;
	/** 奖金状态 */
	private String State;
	/** 奖金类型 */
	private String AClass;
	/** 地区类型 */
	private String AreaType;
	/** 代理人类型 */
	private String AgentType;
	/** 提取比例 */
	private double DrawRate;
	/** 其它提取比例 */
	private double DrawRateOth;
	/** 提取比例项1 */
	private double DrawRate1;
	/** 最低保费 */
	private double TransMoneyMin;
	/** 最高保费 */
	private double TransMoneyMax;
	/** 保费区间下限 */
	private double TransFeeMin;
	/** 保费区间上限 */
	private double TransFeeMax;
	/** 奖金项1 */
	private double RewardMoney1;
	/** 特殊标记 */
	private String SpeFlag;
	/** 日期标识 */
	private Date TDate;
	/** 时间标识 */
	private String TTime;
	/** 备用项1 */
	private double F1;
	/** 备用项2 */
	private double F2;
	/** 备用项3 */
	private double F3;
	/** 备用项4 */
	private double F4;
	/** 备用项5 */
	private String F5;
	/** 备用项6 */
	private String F6;
	/** 备用项7 */
	private int F7;
	/** 备用项8 */
	private int F8;
	/** 备用项9 */
	private Date SpeDate;
	/** 备用项10 */
	private String SpeTime;
	/** 备用项11 */
	private Date SpareDate;
	/** 备用项12 */
	private String SpareTime;
	/** 展业类型 */
	private String BranchType;
	/** 销售渠道 */
	private String BranchType2;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 47;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAWageRadixAllTraceSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "EdorNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAWageRadixAllTraceSchema cloned = (LAWageRadixAllTraceSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getEdorType()
	{
		return EdorType;
	}
	public void setEdorType(String aEdorType)
	{
		EdorType = aEdorType;
	}
	public String getIdx()
	{
		return Idx;
	}
	public void setIdx(String aIdx)
	{
		Idx = aIdx;
	}
	public String getTraceFlag()
	{
		return TraceFlag;
	}
	public void setTraceFlag(String aTraceFlag)
	{
		TraceFlag = aTraceFlag;
	}
	public String getModifyFlag()
	{
		return ModifyFlag;
	}
	public void setModifyFlag(String aModifyFlag)
	{
		ModifyFlag = aModifyFlag;
	}
	public String getAgentGrade()
	{
		return AgentGrade;
	}
	public void setAgentGrade(String aAgentGrade)
	{
		AgentGrade = aAgentGrade;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getWageType()
	{
		return WageType;
	}
	public void setWageType(String aWageType)
	{
		WageType = aWageType;
	}
	public String getWageCode()
	{
		return WageCode;
	}
	public void setWageCode(String aWageCode)
	{
		WageCode = aWageCode;
	}
	public String getWageName()
	{
		return WageName;
	}
	public void setWageName(String aWageName)
	{
		WageName = aWageName;
	}
	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public double getRewardMoney()
	{
		return RewardMoney;
	}
	public void setRewardMoney(double aRewardMoney)
	{
		RewardMoney = Arith.round(aRewardMoney,2);
	}
	public void setRewardMoney(String aRewardMoney)
	{
		if (aRewardMoney != null && !aRewardMoney.equals(""))
		{
			Double tDouble = new Double(aRewardMoney);
			double d = tDouble.doubleValue();
                RewardMoney = Arith.round(d,2);
		}
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getAClass()
	{
		return AClass;
	}
	public void setAClass(String aAClass)
	{
		AClass = aAClass;
	}
	public String getAreaType()
	{
		return AreaType;
	}
	public void setAreaType(String aAreaType)
	{
		AreaType = aAreaType;
	}
	public String getAgentType()
	{
		return AgentType;
	}
	public void setAgentType(String aAgentType)
	{
		AgentType = aAgentType;
	}
	public double getDrawRate()
	{
		return DrawRate;
	}
	public void setDrawRate(double aDrawRate)
	{
		DrawRate = Arith.round(aDrawRate,6);
	}
	public void setDrawRate(String aDrawRate)
	{
		if (aDrawRate != null && !aDrawRate.equals(""))
		{
			Double tDouble = new Double(aDrawRate);
			double d = tDouble.doubleValue();
                DrawRate = Arith.round(d,6);
		}
	}

	public double getDrawRateOth()
	{
		return DrawRateOth;
	}
	public void setDrawRateOth(double aDrawRateOth)
	{
		DrawRateOth = Arith.round(aDrawRateOth,6);
	}
	public void setDrawRateOth(String aDrawRateOth)
	{
		if (aDrawRateOth != null && !aDrawRateOth.equals(""))
		{
			Double tDouble = new Double(aDrawRateOth);
			double d = tDouble.doubleValue();
                DrawRateOth = Arith.round(d,6);
		}
	}

	public double getDrawRate1()
	{
		return DrawRate1;
	}
	public void setDrawRate1(double aDrawRate1)
	{
		DrawRate1 = Arith.round(aDrawRate1,6);
	}
	public void setDrawRate1(String aDrawRate1)
	{
		if (aDrawRate1 != null && !aDrawRate1.equals(""))
		{
			Double tDouble = new Double(aDrawRate1);
			double d = tDouble.doubleValue();
                DrawRate1 = Arith.round(d,6);
		}
	}

	public double getTransMoneyMin()
	{
		return TransMoneyMin;
	}
	public void setTransMoneyMin(double aTransMoneyMin)
	{
		TransMoneyMin = Arith.round(aTransMoneyMin,2);
	}
	public void setTransMoneyMin(String aTransMoneyMin)
	{
		if (aTransMoneyMin != null && !aTransMoneyMin.equals(""))
		{
			Double tDouble = new Double(aTransMoneyMin);
			double d = tDouble.doubleValue();
                TransMoneyMin = Arith.round(d,2);
		}
	}

	public double getTransMoneyMax()
	{
		return TransMoneyMax;
	}
	public void setTransMoneyMax(double aTransMoneyMax)
	{
		TransMoneyMax = Arith.round(aTransMoneyMax,2);
	}
	public void setTransMoneyMax(String aTransMoneyMax)
	{
		if (aTransMoneyMax != null && !aTransMoneyMax.equals(""))
		{
			Double tDouble = new Double(aTransMoneyMax);
			double d = tDouble.doubleValue();
                TransMoneyMax = Arith.round(d,2);
		}
	}

	public double getTransFeeMin()
	{
		return TransFeeMin;
	}
	public void setTransFeeMin(double aTransFeeMin)
	{
		TransFeeMin = Arith.round(aTransFeeMin,2);
	}
	public void setTransFeeMin(String aTransFeeMin)
	{
		if (aTransFeeMin != null && !aTransFeeMin.equals(""))
		{
			Double tDouble = new Double(aTransFeeMin);
			double d = tDouble.doubleValue();
                TransFeeMin = Arith.round(d,2);
		}
	}

	public double getTransFeeMax()
	{
		return TransFeeMax;
	}
	public void setTransFeeMax(double aTransFeeMax)
	{
		TransFeeMax = Arith.round(aTransFeeMax,2);
	}
	public void setTransFeeMax(String aTransFeeMax)
	{
		if (aTransFeeMax != null && !aTransFeeMax.equals(""))
		{
			Double tDouble = new Double(aTransFeeMax);
			double d = tDouble.doubleValue();
                TransFeeMax = Arith.round(d,2);
		}
	}

	public double getRewardMoney1()
	{
		return RewardMoney1;
	}
	public void setRewardMoney1(double aRewardMoney1)
	{
		RewardMoney1 = Arith.round(aRewardMoney1,2);
	}
	public void setRewardMoney1(String aRewardMoney1)
	{
		if (aRewardMoney1 != null && !aRewardMoney1.equals(""))
		{
			Double tDouble = new Double(aRewardMoney1);
			double d = tDouble.doubleValue();
                RewardMoney1 = Arith.round(d,2);
		}
	}

	public String getSpeFlag()
	{
		return SpeFlag;
	}
	public void setSpeFlag(String aSpeFlag)
	{
		SpeFlag = aSpeFlag;
	}
	public String getTDate()
	{
		if( TDate != null )
			return fDate.getString(TDate);
		else
			return null;
	}
	public void setTDate(Date aTDate)
	{
		TDate = aTDate;
	}
	public void setTDate(String aTDate)
	{
		if (aTDate != null && !aTDate.equals("") )
		{
			TDate = fDate.getDate( aTDate );
		}
		else
			TDate = null;
	}

	public String getTTime()
	{
		return TTime;
	}
	public void setTTime(String aTTime)
	{
		TTime = aTTime;
	}
	public double getF1()
	{
		return F1;
	}
	public void setF1(double aF1)
	{
		F1 = Arith.round(aF1,0);
	}
	public void setF1(String aF1)
	{
		if (aF1 != null && !aF1.equals(""))
		{
			Double tDouble = new Double(aF1);
			double d = tDouble.doubleValue();
                F1 = Arith.round(d,0);
		}
	}

	public double getF2()
	{
		return F2;
	}
	public void setF2(double aF2)
	{
		F2 = Arith.round(aF2,0);
	}
	public void setF2(String aF2)
	{
		if (aF2 != null && !aF2.equals(""))
		{
			Double tDouble = new Double(aF2);
			double d = tDouble.doubleValue();
                F2 = Arith.round(d,0);
		}
	}

	public double getF3()
	{
		return F3;
	}
	public void setF3(double aF3)
	{
		F3 = Arith.round(aF3,2);
	}
	public void setF3(String aF3)
	{
		if (aF3 != null && !aF3.equals(""))
		{
			Double tDouble = new Double(aF3);
			double d = tDouble.doubleValue();
                F3 = Arith.round(d,2);
		}
	}

	public double getF4()
	{
		return F4;
	}
	public void setF4(double aF4)
	{
		F4 = Arith.round(aF4,2);
	}
	public void setF4(String aF4)
	{
		if (aF4 != null && !aF4.equals(""))
		{
			Double tDouble = new Double(aF4);
			double d = tDouble.doubleValue();
                F4 = Arith.round(d,2);
		}
	}

	public String getF5()
	{
		return F5;
	}
	public void setF5(String aF5)
	{
		F5 = aF5;
	}
	public String getF6()
	{
		return F6;
	}
	public void setF6(String aF6)
	{
		F6 = aF6;
	}
	public int getF7()
	{
		return F7;
	}
	public void setF7(int aF7)
	{
		F7 = aF7;
	}
	public void setF7(String aF7)
	{
		if (aF7 != null && !aF7.equals(""))
		{
			Integer tInteger = new Integer(aF7);
			int i = tInteger.intValue();
			F7 = i;
		}
	}

	public int getF8()
	{
		return F8;
	}
	public void setF8(int aF8)
	{
		F8 = aF8;
	}
	public void setF8(String aF8)
	{
		if (aF8 != null && !aF8.equals(""))
		{
			Integer tInteger = new Integer(aF8);
			int i = tInteger.intValue();
			F8 = i;
		}
	}

	public String getSpeDate()
	{
		if( SpeDate != null )
			return fDate.getString(SpeDate);
		else
			return null;
	}
	public void setSpeDate(Date aSpeDate)
	{
		SpeDate = aSpeDate;
	}
	public void setSpeDate(String aSpeDate)
	{
		if (aSpeDate != null && !aSpeDate.equals("") )
		{
			SpeDate = fDate.getDate( aSpeDate );
		}
		else
			SpeDate = null;
	}

	public String getSpeTime()
	{
		return SpeTime;
	}
	public void setSpeTime(String aSpeTime)
	{
		SpeTime = aSpeTime;
	}
	public String getSpareDate()
	{
		if( SpareDate != null )
			return fDate.getString(SpareDate);
		else
			return null;
	}
	public void setSpareDate(Date aSpareDate)
	{
		SpareDate = aSpareDate;
	}
	public void setSpareDate(String aSpareDate)
	{
		if (aSpareDate != null && !aSpareDate.equals("") )
		{
			SpareDate = fDate.getDate( aSpareDate );
		}
		else
			SpareDate = null;
	}

	public String getSpareTime()
	{
		return SpareTime;
	}
	public void setSpareTime(String aSpareTime)
	{
		SpareTime = aSpareTime;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LAWageRadixAllTraceSchema 对象给 Schema 赋值
	* @param: aLAWageRadixAllTraceSchema LAWageRadixAllTraceSchema
	**/
	public void setSchema(LAWageRadixAllTraceSchema aLAWageRadixAllTraceSchema)
	{
		this.EdorNo = aLAWageRadixAllTraceSchema.getEdorNo();
		this.EdorType = aLAWageRadixAllTraceSchema.getEdorType();
		this.Idx = aLAWageRadixAllTraceSchema.getIdx();
		this.TraceFlag = aLAWageRadixAllTraceSchema.getTraceFlag();
		this.ModifyFlag = aLAWageRadixAllTraceSchema.getModifyFlag();
		this.AgentGrade = aLAWageRadixAllTraceSchema.getAgentGrade();
		this.ManageCom = aLAWageRadixAllTraceSchema.getManageCom();
		this.AgentCom = aLAWageRadixAllTraceSchema.getAgentCom();
		this.WageType = aLAWageRadixAllTraceSchema.getWageType();
		this.WageCode = aLAWageRadixAllTraceSchema.getWageCode();
		this.WageName = aLAWageRadixAllTraceSchema.getWageName();
		this.WageNo = aLAWageRadixAllTraceSchema.getWageNo();
		this.RewardMoney = aLAWageRadixAllTraceSchema.getRewardMoney();
		this.State = aLAWageRadixAllTraceSchema.getState();
		this.AClass = aLAWageRadixAllTraceSchema.getAClass();
		this.AreaType = aLAWageRadixAllTraceSchema.getAreaType();
		this.AgentType = aLAWageRadixAllTraceSchema.getAgentType();
		this.DrawRate = aLAWageRadixAllTraceSchema.getDrawRate();
		this.DrawRateOth = aLAWageRadixAllTraceSchema.getDrawRateOth();
		this.DrawRate1 = aLAWageRadixAllTraceSchema.getDrawRate1();
		this.TransMoneyMin = aLAWageRadixAllTraceSchema.getTransMoneyMin();
		this.TransMoneyMax = aLAWageRadixAllTraceSchema.getTransMoneyMax();
		this.TransFeeMin = aLAWageRadixAllTraceSchema.getTransFeeMin();
		this.TransFeeMax = aLAWageRadixAllTraceSchema.getTransFeeMax();
		this.RewardMoney1 = aLAWageRadixAllTraceSchema.getRewardMoney1();
		this.SpeFlag = aLAWageRadixAllTraceSchema.getSpeFlag();
		this.TDate = fDate.getDate( aLAWageRadixAllTraceSchema.getTDate());
		this.TTime = aLAWageRadixAllTraceSchema.getTTime();
		this.F1 = aLAWageRadixAllTraceSchema.getF1();
		this.F2 = aLAWageRadixAllTraceSchema.getF2();
		this.F3 = aLAWageRadixAllTraceSchema.getF3();
		this.F4 = aLAWageRadixAllTraceSchema.getF4();
		this.F5 = aLAWageRadixAllTraceSchema.getF5();
		this.F6 = aLAWageRadixAllTraceSchema.getF6();
		this.F7 = aLAWageRadixAllTraceSchema.getF7();
		this.F8 = aLAWageRadixAllTraceSchema.getF8();
		this.SpeDate = fDate.getDate( aLAWageRadixAllTraceSchema.getSpeDate());
		this.SpeTime = aLAWageRadixAllTraceSchema.getSpeTime();
		this.SpareDate = fDate.getDate( aLAWageRadixAllTraceSchema.getSpareDate());
		this.SpareTime = aLAWageRadixAllTraceSchema.getSpareTime();
		this.BranchType = aLAWageRadixAllTraceSchema.getBranchType();
		this.BranchType2 = aLAWageRadixAllTraceSchema.getBranchType2();
		this.Operator = aLAWageRadixAllTraceSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAWageRadixAllTraceSchema.getMakeDate());
		this.MakeTime = aLAWageRadixAllTraceSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAWageRadixAllTraceSchema.getModifyDate());
		this.ModifyTime = aLAWageRadixAllTraceSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("EdorType") == null )
				this.EdorType = null;
			else
				this.EdorType = rs.getString("EdorType").trim();

			if( rs.getString("Idx") == null )
				this.Idx = null;
			else
				this.Idx = rs.getString("Idx").trim();

			if( rs.getString("TraceFlag") == null )
				this.TraceFlag = null;
			else
				this.TraceFlag = rs.getString("TraceFlag").trim();

			if( rs.getString("ModifyFlag") == null )
				this.ModifyFlag = null;
			else
				this.ModifyFlag = rs.getString("ModifyFlag").trim();

			if( rs.getString("AgentGrade") == null )
				this.AgentGrade = null;
			else
				this.AgentGrade = rs.getString("AgentGrade").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("WageType") == null )
				this.WageType = null;
			else
				this.WageType = rs.getString("WageType").trim();

			if( rs.getString("WageCode") == null )
				this.WageCode = null;
			else
				this.WageCode = rs.getString("WageCode").trim();

			if( rs.getString("WageName") == null )
				this.WageName = null;
			else
				this.WageName = rs.getString("WageName").trim();

			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			this.RewardMoney = rs.getDouble("RewardMoney");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("AClass") == null )
				this.AClass = null;
			else
				this.AClass = rs.getString("AClass").trim();

			if( rs.getString("AreaType") == null )
				this.AreaType = null;
			else
				this.AreaType = rs.getString("AreaType").trim();

			if( rs.getString("AgentType") == null )
				this.AgentType = null;
			else
				this.AgentType = rs.getString("AgentType").trim();

			this.DrawRate = rs.getDouble("DrawRate");
			this.DrawRateOth = rs.getDouble("DrawRateOth");
			this.DrawRate1 = rs.getDouble("DrawRate1");
			this.TransMoneyMin = rs.getDouble("TransMoneyMin");
			this.TransMoneyMax = rs.getDouble("TransMoneyMax");
			this.TransFeeMin = rs.getDouble("TransFeeMin");
			this.TransFeeMax = rs.getDouble("TransFeeMax");
			this.RewardMoney1 = rs.getDouble("RewardMoney1");
			if( rs.getString("SpeFlag") == null )
				this.SpeFlag = null;
			else
				this.SpeFlag = rs.getString("SpeFlag").trim();

			this.TDate = rs.getDate("TDate");
			if( rs.getString("TTime") == null )
				this.TTime = null;
			else
				this.TTime = rs.getString("TTime").trim();

			this.F1 = rs.getDouble("F1");
			this.F2 = rs.getDouble("F2");
			this.F3 = rs.getDouble("F3");
			this.F4 = rs.getDouble("F4");
			if( rs.getString("F5") == null )
				this.F5 = null;
			else
				this.F5 = rs.getString("F5").trim();

			if( rs.getString("F6") == null )
				this.F6 = null;
			else
				this.F6 = rs.getString("F6").trim();

			this.F7 = rs.getInt("F7");
			this.F8 = rs.getInt("F8");
			this.SpeDate = rs.getDate("SpeDate");
			if( rs.getString("SpeTime") == null )
				this.SpeTime = null;
			else
				this.SpeTime = rs.getString("SpeTime").trim();

			this.SpareDate = rs.getDate("SpareDate");
			if( rs.getString("SpareTime") == null )
				this.SpareTime = null;
			else
				this.SpareTime = rs.getString("SpareTime").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAWageRadixAllTrace表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAWageRadixAllTraceSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAWageRadixAllTraceSchema getSchema()
	{
		LAWageRadixAllTraceSchema aLAWageRadixAllTraceSchema = new LAWageRadixAllTraceSchema();
		aLAWageRadixAllTraceSchema.setSchema(this);
		return aLAWageRadixAllTraceSchema;
	}

	public LAWageRadixAllTraceDB getDB()
	{
		LAWageRadixAllTraceDB aDBOper = new LAWageRadixAllTraceDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageRadixAllTrace描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Idx)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TraceFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RewardMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AClass)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AreaType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DrawRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DrawRateOth));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DrawRate1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransMoneyMin));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransMoneyMax));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransFeeMin));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransFeeMax));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RewardMoney1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SpeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F4));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(F6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F7));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F8));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SpeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SpeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SpareDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SpareTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageRadixAllTrace>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Idx = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			TraceFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ModifyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			WageType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			WageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			WageName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			RewardMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			AClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			AgentType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			DrawRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			DrawRateOth = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			DrawRate1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			TransMoneyMin = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			TransMoneyMax = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			TransFeeMin = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			TransFeeMax = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			RewardMoney1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			SpeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			TDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			TTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			F1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			F2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			F3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			F4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			F5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			F6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			F7= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).intValue();
			F8= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).intValue();
			SpeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,SysConst.PACKAGESPILTER));
			SpeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			SpareDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,SysConst.PACKAGESPILTER));
			SpareTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAWageRadixAllTraceSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("EdorType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
		}
		if (FCode.equals("Idx"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
		}
		if (FCode.equals("TraceFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TraceFlag));
		}
		if (FCode.equals("ModifyFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyFlag));
		}
		if (FCode.equals("AgentGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("WageType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageType));
		}
		if (FCode.equals("WageCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageCode));
		}
		if (FCode.equals("WageName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageName));
		}
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("RewardMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RewardMoney));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("AClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AClass));
		}
		if (FCode.equals("AreaType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
		}
		if (FCode.equals("AgentType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentType));
		}
		if (FCode.equals("DrawRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawRate));
		}
		if (FCode.equals("DrawRateOth"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawRateOth));
		}
		if (FCode.equals("DrawRate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawRate1));
		}
		if (FCode.equals("TransMoneyMin"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMoneyMin));
		}
		if (FCode.equals("TransMoneyMax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransMoneyMax));
		}
		if (FCode.equals("TransFeeMin"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransFeeMin));
		}
		if (FCode.equals("TransFeeMax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransFeeMax));
		}
		if (FCode.equals("RewardMoney1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RewardMoney1));
		}
		if (FCode.equals("SpeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpeFlag));
		}
		if (FCode.equals("TDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTDate()));
		}
		if (FCode.equals("TTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TTime));
		}
		if (FCode.equals("F1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F1));
		}
		if (FCode.equals("F2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F2));
		}
		if (FCode.equals("F3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F3));
		}
		if (FCode.equals("F4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F4));
		}
		if (FCode.equals("F5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F5));
		}
		if (FCode.equals("F6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F6));
		}
		if (FCode.equals("F7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F7));
		}
		if (FCode.equals("F8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F8));
		}
		if (FCode.equals("SpeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSpeDate()));
		}
		if (FCode.equals("SpeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpeTime));
		}
		if (FCode.equals("SpareDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSpareDate()));
		}
		if (FCode.equals("SpareTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpareTime));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(EdorType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Idx);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(TraceFlag);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ModifyFlag);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentGrade);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(WageType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(WageCode);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(WageName);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 12:
				strFieldValue = String.valueOf(RewardMoney);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(AClass);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AreaType);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(AgentType);
				break;
			case 17:
				strFieldValue = String.valueOf(DrawRate);
				break;
			case 18:
				strFieldValue = String.valueOf(DrawRateOth);
				break;
			case 19:
				strFieldValue = String.valueOf(DrawRate1);
				break;
			case 20:
				strFieldValue = String.valueOf(TransMoneyMin);
				break;
			case 21:
				strFieldValue = String.valueOf(TransMoneyMax);
				break;
			case 22:
				strFieldValue = String.valueOf(TransFeeMin);
				break;
			case 23:
				strFieldValue = String.valueOf(TransFeeMax);
				break;
			case 24:
				strFieldValue = String.valueOf(RewardMoney1);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(SpeFlag);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTDate()));
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(TTime);
				break;
			case 28:
				strFieldValue = String.valueOf(F1);
				break;
			case 29:
				strFieldValue = String.valueOf(F2);
				break;
			case 30:
				strFieldValue = String.valueOf(F3);
				break;
			case 31:
				strFieldValue = String.valueOf(F4);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(F5);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(F6);
				break;
			case 34:
				strFieldValue = String.valueOf(F7);
				break;
			case 35:
				strFieldValue = String.valueOf(F8);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSpeDate()));
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(SpeTime);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSpareDate()));
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(SpareTime);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorType = FValue.trim();
			}
			else
				EdorType = null;
		}
		if (FCode.equalsIgnoreCase("Idx"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Idx = FValue.trim();
			}
			else
				Idx = null;
		}
		if (FCode.equalsIgnoreCase("TraceFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TraceFlag = FValue.trim();
			}
			else
				TraceFlag = null;
		}
		if (FCode.equalsIgnoreCase("ModifyFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyFlag = FValue.trim();
			}
			else
				ModifyFlag = null;
		}
		if (FCode.equalsIgnoreCase("AgentGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGrade = FValue.trim();
			}
			else
				AgentGrade = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("WageType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageType = FValue.trim();
			}
			else
				WageType = null;
		}
		if (FCode.equalsIgnoreCase("WageCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageCode = FValue.trim();
			}
			else
				WageCode = null;
		}
		if (FCode.equalsIgnoreCase("WageName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageName = FValue.trim();
			}
			else
				WageName = null;
		}
		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("RewardMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RewardMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("AClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AClass = FValue.trim();
			}
			else
				AClass = null;
		}
		if (FCode.equalsIgnoreCase("AreaType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaType = FValue.trim();
			}
			else
				AreaType = null;
		}
		if (FCode.equalsIgnoreCase("AgentType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentType = FValue.trim();
			}
			else
				AgentType = null;
		}
		if (FCode.equalsIgnoreCase("DrawRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DrawRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("DrawRateOth"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DrawRateOth = d;
			}
		}
		if (FCode.equalsIgnoreCase("DrawRate1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DrawRate1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("TransMoneyMin"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransMoneyMin = d;
			}
		}
		if (FCode.equalsIgnoreCase("TransMoneyMax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransMoneyMax = d;
			}
		}
		if (FCode.equalsIgnoreCase("TransFeeMin"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransFeeMin = d;
			}
		}
		if (FCode.equalsIgnoreCase("TransFeeMax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransFeeMax = d;
			}
		}
		if (FCode.equalsIgnoreCase("RewardMoney1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RewardMoney1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("SpeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SpeFlag = FValue.trim();
			}
			else
				SpeFlag = null;
		}
		if (FCode.equalsIgnoreCase("TDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TDate = fDate.getDate( FValue );
			}
			else
				TDate = null;
		}
		if (FCode.equalsIgnoreCase("TTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TTime = FValue.trim();
			}
			else
				TTime = null;
		}
		if (FCode.equalsIgnoreCase("F1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F4 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F5 = FValue.trim();
			}
			else
				F5 = null;
		}
		if (FCode.equalsIgnoreCase("F6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				F6 = FValue.trim();
			}
			else
				F6 = null;
		}
		if (FCode.equalsIgnoreCase("F7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				F7 = i;
			}
		}
		if (FCode.equalsIgnoreCase("F8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				F8 = i;
			}
		}
		if (FCode.equalsIgnoreCase("SpeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SpeDate = fDate.getDate( FValue );
			}
			else
				SpeDate = null;
		}
		if (FCode.equalsIgnoreCase("SpeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SpeTime = FValue.trim();
			}
			else
				SpeTime = null;
		}
		if (FCode.equalsIgnoreCase("SpareDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SpareDate = fDate.getDate( FValue );
			}
			else
				SpareDate = null;
		}
		if (FCode.equalsIgnoreCase("SpareTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SpareTime = FValue.trim();
			}
			else
				SpareTime = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAWageRadixAllTraceSchema other = (LAWageRadixAllTraceSchema)otherObject;
		return
			(EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (EdorType == null ? other.getEdorType() == null : EdorType.equals(other.getEdorType()))
			&& (Idx == null ? other.getIdx() == null : Idx.equals(other.getIdx()))
			&& (TraceFlag == null ? other.getTraceFlag() == null : TraceFlag.equals(other.getTraceFlag()))
			&& (ModifyFlag == null ? other.getModifyFlag() == null : ModifyFlag.equals(other.getModifyFlag()))
			&& (AgentGrade == null ? other.getAgentGrade() == null : AgentGrade.equals(other.getAgentGrade()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (WageType == null ? other.getWageType() == null : WageType.equals(other.getWageType()))
			&& (WageCode == null ? other.getWageCode() == null : WageCode.equals(other.getWageCode()))
			&& (WageName == null ? other.getWageName() == null : WageName.equals(other.getWageName()))
			&& (WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& RewardMoney == other.getRewardMoney()
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (AClass == null ? other.getAClass() == null : AClass.equals(other.getAClass()))
			&& (AreaType == null ? other.getAreaType() == null : AreaType.equals(other.getAreaType()))
			&& (AgentType == null ? other.getAgentType() == null : AgentType.equals(other.getAgentType()))
			&& DrawRate == other.getDrawRate()
			&& DrawRateOth == other.getDrawRateOth()
			&& DrawRate1 == other.getDrawRate1()
			&& TransMoneyMin == other.getTransMoneyMin()
			&& TransMoneyMax == other.getTransMoneyMax()
			&& TransFeeMin == other.getTransFeeMin()
			&& TransFeeMax == other.getTransFeeMax()
			&& RewardMoney1 == other.getRewardMoney1()
			&& (SpeFlag == null ? other.getSpeFlag() == null : SpeFlag.equals(other.getSpeFlag()))
			&& (TDate == null ? other.getTDate() == null : fDate.getString(TDate).equals(other.getTDate()))
			&& (TTime == null ? other.getTTime() == null : TTime.equals(other.getTTime()))
			&& F1 == other.getF1()
			&& F2 == other.getF2()
			&& F3 == other.getF3()
			&& F4 == other.getF4()
			&& (F5 == null ? other.getF5() == null : F5.equals(other.getF5()))
			&& (F6 == null ? other.getF6() == null : F6.equals(other.getF6()))
			&& F7 == other.getF7()
			&& F8 == other.getF8()
			&& (SpeDate == null ? other.getSpeDate() == null : fDate.getString(SpeDate).equals(other.getSpeDate()))
			&& (SpeTime == null ? other.getSpeTime() == null : SpeTime.equals(other.getSpeTime()))
			&& (SpareDate == null ? other.getSpareDate() == null : fDate.getString(SpareDate).equals(other.getSpareDate()))
			&& (SpareTime == null ? other.getSpareTime() == null : SpareTime.equals(other.getSpareTime()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("EdorType") ) {
			return 1;
		}
		if( strFieldName.equals("Idx") ) {
			return 2;
		}
		if( strFieldName.equals("TraceFlag") ) {
			return 3;
		}
		if( strFieldName.equals("ModifyFlag") ) {
			return 4;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return 5;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 6;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 7;
		}
		if( strFieldName.equals("WageType") ) {
			return 8;
		}
		if( strFieldName.equals("WageCode") ) {
			return 9;
		}
		if( strFieldName.equals("WageName") ) {
			return 10;
		}
		if( strFieldName.equals("WageNo") ) {
			return 11;
		}
		if( strFieldName.equals("RewardMoney") ) {
			return 12;
		}
		if( strFieldName.equals("State") ) {
			return 13;
		}
		if( strFieldName.equals("AClass") ) {
			return 14;
		}
		if( strFieldName.equals("AreaType") ) {
			return 15;
		}
		if( strFieldName.equals("AgentType") ) {
			return 16;
		}
		if( strFieldName.equals("DrawRate") ) {
			return 17;
		}
		if( strFieldName.equals("DrawRateOth") ) {
			return 18;
		}
		if( strFieldName.equals("DrawRate1") ) {
			return 19;
		}
		if( strFieldName.equals("TransMoneyMin") ) {
			return 20;
		}
		if( strFieldName.equals("TransMoneyMax") ) {
			return 21;
		}
		if( strFieldName.equals("TransFeeMin") ) {
			return 22;
		}
		if( strFieldName.equals("TransFeeMax") ) {
			return 23;
		}
		if( strFieldName.equals("RewardMoney1") ) {
			return 24;
		}
		if( strFieldName.equals("SpeFlag") ) {
			return 25;
		}
		if( strFieldName.equals("TDate") ) {
			return 26;
		}
		if( strFieldName.equals("TTime") ) {
			return 27;
		}
		if( strFieldName.equals("F1") ) {
			return 28;
		}
		if( strFieldName.equals("F2") ) {
			return 29;
		}
		if( strFieldName.equals("F3") ) {
			return 30;
		}
		if( strFieldName.equals("F4") ) {
			return 31;
		}
		if( strFieldName.equals("F5") ) {
			return 32;
		}
		if( strFieldName.equals("F6") ) {
			return 33;
		}
		if( strFieldName.equals("F7") ) {
			return 34;
		}
		if( strFieldName.equals("F8") ) {
			return 35;
		}
		if( strFieldName.equals("SpeDate") ) {
			return 36;
		}
		if( strFieldName.equals("SpeTime") ) {
			return 37;
		}
		if( strFieldName.equals("SpareDate") ) {
			return 38;
		}
		if( strFieldName.equals("SpareTime") ) {
			return 39;
		}
		if( strFieldName.equals("BranchType") ) {
			return 40;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 41;
		}
		if( strFieldName.equals("Operator") ) {
			return 42;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 43;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 44;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 45;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 46;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "EdorType";
				break;
			case 2:
				strFieldName = "Idx";
				break;
			case 3:
				strFieldName = "TraceFlag";
				break;
			case 4:
				strFieldName = "ModifyFlag";
				break;
			case 5:
				strFieldName = "AgentGrade";
				break;
			case 6:
				strFieldName = "ManageCom";
				break;
			case 7:
				strFieldName = "AgentCom";
				break;
			case 8:
				strFieldName = "WageType";
				break;
			case 9:
				strFieldName = "WageCode";
				break;
			case 10:
				strFieldName = "WageName";
				break;
			case 11:
				strFieldName = "WageNo";
				break;
			case 12:
				strFieldName = "RewardMoney";
				break;
			case 13:
				strFieldName = "State";
				break;
			case 14:
				strFieldName = "AClass";
				break;
			case 15:
				strFieldName = "AreaType";
				break;
			case 16:
				strFieldName = "AgentType";
				break;
			case 17:
				strFieldName = "DrawRate";
				break;
			case 18:
				strFieldName = "DrawRateOth";
				break;
			case 19:
				strFieldName = "DrawRate1";
				break;
			case 20:
				strFieldName = "TransMoneyMin";
				break;
			case 21:
				strFieldName = "TransMoneyMax";
				break;
			case 22:
				strFieldName = "TransFeeMin";
				break;
			case 23:
				strFieldName = "TransFeeMax";
				break;
			case 24:
				strFieldName = "RewardMoney1";
				break;
			case 25:
				strFieldName = "SpeFlag";
				break;
			case 26:
				strFieldName = "TDate";
				break;
			case 27:
				strFieldName = "TTime";
				break;
			case 28:
				strFieldName = "F1";
				break;
			case 29:
				strFieldName = "F2";
				break;
			case 30:
				strFieldName = "F3";
				break;
			case 31:
				strFieldName = "F4";
				break;
			case 32:
				strFieldName = "F5";
				break;
			case 33:
				strFieldName = "F6";
				break;
			case 34:
				strFieldName = "F7";
				break;
			case 35:
				strFieldName = "F8";
				break;
			case 36:
				strFieldName = "SpeDate";
				break;
			case 37:
				strFieldName = "SpeTime";
				break;
			case 38:
				strFieldName = "SpareDate";
				break;
			case 39:
				strFieldName = "SpareTime";
				break;
			case 40:
				strFieldName = "BranchType";
				break;
			case 41:
				strFieldName = "BranchType2";
				break;
			case 42:
				strFieldName = "Operator";
				break;
			case 43:
				strFieldName = "MakeDate";
				break;
			case 44:
				strFieldName = "MakeTime";
				break;
			case 45:
				strFieldName = "ModifyDate";
				break;
			case 46:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Idx") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TraceFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RewardMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AreaType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DrawRateOth") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DrawRate1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TransMoneyMin") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TransMoneyMax") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TransFeeMin") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TransFeeMax") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RewardMoney1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SpeFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F4") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F7") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("F8") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SpeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SpeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SpareDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SpareTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_INT;
				break;
			case 35:
				nFieldType = Schema.TYPE_INT;
				break;
			case 36:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
