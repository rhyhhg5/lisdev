/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLAppealAcceptDB;

/*
 * <p>ClassName: LLAppealAcceptSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-01-18
 */
public class LLAppealAcceptSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String BatchNo;
	/** 发送日期 */
	private Date SendDate;
	/** 发送时间 */
	private String SendTime;
	/** 交易编码 */
	private String BranchCode;
	/** 交易人员 */
	private String SendOperator;
	/** 固定值 */
	private String MsgType;
	/** 给付人数 */
	private String Rownum;
	/** 登录机构 */
	private String ManageCom;
	/** 理赔批次号 */
	private String rgtno;
	/** 申诉错误类型 */
	private String AppealType;
	/** 申诉错误原因 */
	private String AppeanRCode;
	/** 备注 */
	private String AppealRDesc;
	/** 客户姓名 */
	private String CustomerName;
	/** 客户号码 */
	private String CustomerNo;
	/** 证件号码 */
	private String tIDNo;
	/** 证件类型 */
	private String tIDType;
	/** 出生日期 */
	private Date CBirthday;
	/** 社保号码 */
	private String OtherIDNo;
	/** 手机号码 */
	private String MobilePhone;
	/** 申请人与被保人关系 */
	private String Relation;
	/** 受理方式 */
	private String RgtType;
	/** 回执发送方式 */
	private String ReturnMode;
	/** 申请人姓名 */
	private String RgtantName;
	/** 申请人证件号码 */
	private String IDNo;
	/** 申请人证件类型 */
	private String IDType;
	/** 申请人电话 */
	private String RgtantPhone;
	/** 申请人手机 */
	private String Mobile;
	/** 申请人电子邮箱 */
	private String Email;
	/** 申请人地址 */
	private String RgtantAddress;
	/** 邮政编码 */
	private String PostCode;
	/** 受益金领取方式 */
	private String paymode;
	/** 银行编码 */
	private String BankCode;
	/** 签约银行 */
	private String SendFlag;
	/** 银行账号 */
	private String BankAccNo;
	/** 银行账户名 */
	private String AccName;
	/** 事故者现状 */
	private String CustStatus;
	/** 死亡日期 */
	private Date DeathDate;
	/** 申请原因 */
	private String appReasonCode;
	/** 参保人员类别 */
	private String InsuredStat;
	/** 客户社保号 */
	private String SecurityNo;
	/** 单位社保登记号 */
	private String CompSecuNo;
	/** 医院名称 */
	private String HospitalName;
	/** 医院代码 */
	private String HospitalCode;
	/** 账单属性 */
	private String FeeAtti;
	/** 账单种类 */
	private String FeeType;
	/** 账单类型 */
	private String FeeAffixType;
	/** 收据类型 */
	private String ReceiptType;
	/** 账单号码 */
	private String ReceiptNo;
	/** 住院号 */
	private String inpatientNo;
	/** 结算日期 */
	private Date FeeDate;
	/** 入院日期 */
	private Date HospStartDate;
	/** 出院日期 */
	private Date HospEndDate;
	/** 分割单出具单位 */
	private String IssueUnit;
	/** 机构处理 */
	private String SimpleCase;
	/** 简易案件 */
	private String EasyCase;
	/** 上报总公司 */
	private String HeadCase;
	/** 上报分公司 */
	private String FenCase;
	/** 客户特约信息 */
	private String ContRemark;
	/** 回销预付赔款 */
	private String PrePaidFlag;
	/** 审定结论 */
	private String checkDecision2;
	/** 审批意见 */
	private String Remark1;
	/** 审定意见 */
	private String Remark2;
	/** 给付账户名 */
	private String GAccName;
	/** 给付账号 */
	private String GBankAccNo;
	/** 给付银行编码 */
	private String GBankCode;
	/** 给付领取方式 */
	private String Gpaymode;
	/** 给付回销预付赔款 */
	private String GPrePaidFlag;
	/** 领取人 */
	private String Drawer;
	/** 领取人身份证号 */
	private String DrawerID;
	/** 更改账户原因 */
	private String ModifyReason;

	public static final int FIELDNUM = 70;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLAppealAcceptSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLAppealAcceptSchema cloned = (LLAppealAcceptSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getSendDate()
	{
		if( SendDate != null )
			return fDate.getString(SendDate);
		else
			return null;
	}
	public void setSendDate(Date aSendDate)
	{
		SendDate = aSendDate;
	}
	public void setSendDate(String aSendDate)
	{
		if (aSendDate != null && !aSendDate.equals("") )
		{
			SendDate = fDate.getDate( aSendDate );
		}
		else
			SendDate = null;
	}

	public String getSendTime()
	{
		return SendTime;
	}
	public void setSendTime(String aSendTime)
	{
		SendTime = aSendTime;
	}
	public String getBranchCode()
	{
		return BranchCode;
	}
	public void setBranchCode(String aBranchCode)
	{
		BranchCode = aBranchCode;
	}
	public String getSendOperator()
	{
		return SendOperator;
	}
	public void setSendOperator(String aSendOperator)
	{
		SendOperator = aSendOperator;
	}
	public String getMsgType()
	{
		return MsgType;
	}
	public void setMsgType(String aMsgType)
	{
		MsgType = aMsgType;
	}
	public String getRownum()
	{
		return Rownum;
	}
	public void setRownum(String aRownum)
	{
		Rownum = aRownum;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getrgtno()
	{
		return rgtno;
	}
	public void setrgtno(String argtno)
	{
		rgtno = argtno;
	}
	public String getAppealType()
	{
		return AppealType;
	}
	public void setAppealType(String aAppealType)
	{
		AppealType = aAppealType;
	}
	public String getAppeanRCode()
	{
		return AppeanRCode;
	}
	public void setAppeanRCode(String aAppeanRCode)
	{
		AppeanRCode = aAppeanRCode;
	}
	public String getAppealRDesc()
	{
		return AppealRDesc;
	}
	public void setAppealRDesc(String aAppealRDesc)
	{
		AppealRDesc = aAppealRDesc;
	}
	public String getCustomerName()
	{
		return CustomerName;
	}
	public void setCustomerName(String aCustomerName)
	{
		CustomerName = aCustomerName;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String gettIDNo()
	{
		return tIDNo;
	}
	public void settIDNo(String atIDNo)
	{
		tIDNo = atIDNo;
	}
	public String gettIDType()
	{
		return tIDType;
	}
	public void settIDType(String atIDType)
	{
		tIDType = atIDType;
	}
	public String getCBirthday()
	{
		if( CBirthday != null )
			return fDate.getString(CBirthday);
		else
			return null;
	}
	public void setCBirthday(Date aCBirthday)
	{
		CBirthday = aCBirthday;
	}
	public void setCBirthday(String aCBirthday)
	{
		if (aCBirthday != null && !aCBirthday.equals("") )
		{
			CBirthday = fDate.getDate( aCBirthday );
		}
		else
			CBirthday = null;
	}

	public String getOtherIDNo()
	{
		return OtherIDNo;
	}
	public void setOtherIDNo(String aOtherIDNo)
	{
		OtherIDNo = aOtherIDNo;
	}
	public String getMobilePhone()
	{
		return MobilePhone;
	}
	public void setMobilePhone(String aMobilePhone)
	{
		MobilePhone = aMobilePhone;
	}
	public String getRelation()
	{
		return Relation;
	}
	public void setRelation(String aRelation)
	{
		Relation = aRelation;
	}
	public String getRgtType()
	{
		return RgtType;
	}
	public void setRgtType(String aRgtType)
	{
		RgtType = aRgtType;
	}
	public String getReturnMode()
	{
		return ReturnMode;
	}
	public void setReturnMode(String aReturnMode)
	{
		ReturnMode = aReturnMode;
	}
	public String getRgtantName()
	{
		return RgtantName;
	}
	public void setRgtantName(String aRgtantName)
	{
		RgtantName = aRgtantName;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getRgtantPhone()
	{
		return RgtantPhone;
	}
	public void setRgtantPhone(String aRgtantPhone)
	{
		RgtantPhone = aRgtantPhone;
	}
	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getEmail()
	{
		return Email;
	}
	public void setEmail(String aEmail)
	{
		Email = aEmail;
	}
	public String getRgtantAddress()
	{
		return RgtantAddress;
	}
	public void setRgtantAddress(String aRgtantAddress)
	{
		RgtantAddress = aRgtantAddress;
	}
	public String getPostCode()
	{
		return PostCode;
	}
	public void setPostCode(String aPostCode)
	{
		PostCode = aPostCode;
	}
	public String getpaymode()
	{
		return paymode;
	}
	public void setpaymode(String apaymode)
	{
		paymode = apaymode;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getSendFlag()
	{
		return SendFlag;
	}
	public void setSendFlag(String aSendFlag)
	{
		SendFlag = aSendFlag;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getCustStatus()
	{
		return CustStatus;
	}
	public void setCustStatus(String aCustStatus)
	{
		CustStatus = aCustStatus;
	}
	public String getDeathDate()
	{
		if( DeathDate != null )
			return fDate.getString(DeathDate);
		else
			return null;
	}
	public void setDeathDate(Date aDeathDate)
	{
		DeathDate = aDeathDate;
	}
	public void setDeathDate(String aDeathDate)
	{
		if (aDeathDate != null && !aDeathDate.equals("") )
		{
			DeathDate = fDate.getDate( aDeathDate );
		}
		else
			DeathDate = null;
	}

	public String getappReasonCode()
	{
		return appReasonCode;
	}
	public void setappReasonCode(String aappReasonCode)
	{
		appReasonCode = aappReasonCode;
	}
	public String getInsuredStat()
	{
		return InsuredStat;
	}
	public void setInsuredStat(String aInsuredStat)
	{
		InsuredStat = aInsuredStat;
	}
	public String getSecurityNo()
	{
		return SecurityNo;
	}
	public void setSecurityNo(String aSecurityNo)
	{
		SecurityNo = aSecurityNo;
	}
	public String getCompSecuNo()
	{
		return CompSecuNo;
	}
	public void setCompSecuNo(String aCompSecuNo)
	{
		CompSecuNo = aCompSecuNo;
	}
	public String getHospitalName()
	{
		return HospitalName;
	}
	public void setHospitalName(String aHospitalName)
	{
		HospitalName = aHospitalName;
	}
	public String getHospitalCode()
	{
		return HospitalCode;
	}
	public void setHospitalCode(String aHospitalCode)
	{
		HospitalCode = aHospitalCode;
	}
	public String getFeeAtti()
	{
		return FeeAtti;
	}
	public void setFeeAtti(String aFeeAtti)
	{
		FeeAtti = aFeeAtti;
	}
	public String getFeeType()
	{
		return FeeType;
	}
	public void setFeeType(String aFeeType)
	{
		FeeType = aFeeType;
	}
	public String getFeeAffixType()
	{
		return FeeAffixType;
	}
	public void setFeeAffixType(String aFeeAffixType)
	{
		FeeAffixType = aFeeAffixType;
	}
	public String getReceiptType()
	{
		return ReceiptType;
	}
	public void setReceiptType(String aReceiptType)
	{
		ReceiptType = aReceiptType;
	}
	public String getReceiptNo()
	{
		return ReceiptNo;
	}
	public void setReceiptNo(String aReceiptNo)
	{
		ReceiptNo = aReceiptNo;
	}
	public String getinpatientNo()
	{
		return inpatientNo;
	}
	public void setinpatientNo(String ainpatientNo)
	{
		inpatientNo = ainpatientNo;
	}
	public String getFeeDate()
	{
		if( FeeDate != null )
			return fDate.getString(FeeDate);
		else
			return null;
	}
	public void setFeeDate(Date aFeeDate)
	{
		FeeDate = aFeeDate;
	}
	public void setFeeDate(String aFeeDate)
	{
		if (aFeeDate != null && !aFeeDate.equals("") )
		{
			FeeDate = fDate.getDate( aFeeDate );
		}
		else
			FeeDate = null;
	}

	public String getHospStartDate()
	{
		if( HospStartDate != null )
			return fDate.getString(HospStartDate);
		else
			return null;
	}
	public void setHospStartDate(Date aHospStartDate)
	{
		HospStartDate = aHospStartDate;
	}
	public void setHospStartDate(String aHospStartDate)
	{
		if (aHospStartDate != null && !aHospStartDate.equals("") )
		{
			HospStartDate = fDate.getDate( aHospStartDate );
		}
		else
			HospStartDate = null;
	}

	public String getHospEndDate()
	{
		if( HospEndDate != null )
			return fDate.getString(HospEndDate);
		else
			return null;
	}
	public void setHospEndDate(Date aHospEndDate)
	{
		HospEndDate = aHospEndDate;
	}
	public void setHospEndDate(String aHospEndDate)
	{
		if (aHospEndDate != null && !aHospEndDate.equals("") )
		{
			HospEndDate = fDate.getDate( aHospEndDate );
		}
		else
			HospEndDate = null;
	}

	public String getIssueUnit()
	{
		return IssueUnit;
	}
	public void setIssueUnit(String aIssueUnit)
	{
		IssueUnit = aIssueUnit;
	}
	public String getSimpleCase()
	{
		return SimpleCase;
	}
	public void setSimpleCase(String aSimpleCase)
	{
		SimpleCase = aSimpleCase;
	}
	public String getEasyCase()
	{
		return EasyCase;
	}
	public void setEasyCase(String aEasyCase)
	{
		EasyCase = aEasyCase;
	}
	public String getHeadCase()
	{
		return HeadCase;
	}
	public void setHeadCase(String aHeadCase)
	{
		HeadCase = aHeadCase;
	}
	public String getFenCase()
	{
		return FenCase;
	}
	public void setFenCase(String aFenCase)
	{
		FenCase = aFenCase;
	}
	public String getContRemark()
	{
		return ContRemark;
	}
	public void setContRemark(String aContRemark)
	{
		ContRemark = aContRemark;
	}
	public String getPrePaidFlag()
	{
		return PrePaidFlag;
	}
	public void setPrePaidFlag(String aPrePaidFlag)
	{
		PrePaidFlag = aPrePaidFlag;
	}
	public String getcheckDecision2()
	{
		return checkDecision2;
	}
	public void setcheckDecision2(String acheckDecision2)
	{
		checkDecision2 = acheckDecision2;
	}
	public String getRemark1()
	{
		return Remark1;
	}
	public void setRemark1(String aRemark1)
	{
		Remark1 = aRemark1;
	}
	public String getRemark2()
	{
		return Remark2;
	}
	public void setRemark2(String aRemark2)
	{
		Remark2 = aRemark2;
	}
	public String getGAccName()
	{
		return GAccName;
	}
	public void setGAccName(String aGAccName)
	{
		GAccName = aGAccName;
	}
	public String getGBankAccNo()
	{
		return GBankAccNo;
	}
	public void setGBankAccNo(String aGBankAccNo)
	{
		GBankAccNo = aGBankAccNo;
	}
	public String getGBankCode()
	{
		return GBankCode;
	}
	public void setGBankCode(String aGBankCode)
	{
		GBankCode = aGBankCode;
	}
	public String getGpaymode()
	{
		return Gpaymode;
	}
	public void setGpaymode(String aGpaymode)
	{
		Gpaymode = aGpaymode;
	}
	public String getGPrePaidFlag()
	{
		return GPrePaidFlag;
	}
	public void setGPrePaidFlag(String aGPrePaidFlag)
	{
		GPrePaidFlag = aGPrePaidFlag;
	}
	public String getDrawer()
	{
		return Drawer;
	}
	public void setDrawer(String aDrawer)
	{
		Drawer = aDrawer;
	}
	public String getDrawerID()
	{
		return DrawerID;
	}
	public void setDrawerID(String aDrawerID)
	{
		DrawerID = aDrawerID;
	}
	public String getModifyReason()
	{
		return ModifyReason;
	}
	public void setModifyReason(String aModifyReason)
	{
		ModifyReason = aModifyReason;
	}

	/**
	* 使用另外一个 LLAppealAcceptSchema 对象给 Schema 赋值
	* @param: aLLAppealAcceptSchema LLAppealAcceptSchema
	**/
	public void setSchema(LLAppealAcceptSchema aLLAppealAcceptSchema)
	{
		this.BatchNo = aLLAppealAcceptSchema.getBatchNo();
		this.SendDate = fDate.getDate( aLLAppealAcceptSchema.getSendDate());
		this.SendTime = aLLAppealAcceptSchema.getSendTime();
		this.BranchCode = aLLAppealAcceptSchema.getBranchCode();
		this.SendOperator = aLLAppealAcceptSchema.getSendOperator();
		this.MsgType = aLLAppealAcceptSchema.getMsgType();
		this.Rownum = aLLAppealAcceptSchema.getRownum();
		this.ManageCom = aLLAppealAcceptSchema.getManageCom();
		this.rgtno = aLLAppealAcceptSchema.getrgtno();
		this.AppealType = aLLAppealAcceptSchema.getAppealType();
		this.AppeanRCode = aLLAppealAcceptSchema.getAppeanRCode();
		this.AppealRDesc = aLLAppealAcceptSchema.getAppealRDesc();
		this.CustomerName = aLLAppealAcceptSchema.getCustomerName();
		this.CustomerNo = aLLAppealAcceptSchema.getCustomerNo();
		this.tIDNo = aLLAppealAcceptSchema.gettIDNo();
		this.tIDType = aLLAppealAcceptSchema.gettIDType();
		this.CBirthday = fDate.getDate( aLLAppealAcceptSchema.getCBirthday());
		this.OtherIDNo = aLLAppealAcceptSchema.getOtherIDNo();
		this.MobilePhone = aLLAppealAcceptSchema.getMobilePhone();
		this.Relation = aLLAppealAcceptSchema.getRelation();
		this.RgtType = aLLAppealAcceptSchema.getRgtType();
		this.ReturnMode = aLLAppealAcceptSchema.getReturnMode();
		this.RgtantName = aLLAppealAcceptSchema.getRgtantName();
		this.IDNo = aLLAppealAcceptSchema.getIDNo();
		this.IDType = aLLAppealAcceptSchema.getIDType();
		this.RgtantPhone = aLLAppealAcceptSchema.getRgtantPhone();
		this.Mobile = aLLAppealAcceptSchema.getMobile();
		this.Email = aLLAppealAcceptSchema.getEmail();
		this.RgtantAddress = aLLAppealAcceptSchema.getRgtantAddress();
		this.PostCode = aLLAppealAcceptSchema.getPostCode();
		this.paymode = aLLAppealAcceptSchema.getpaymode();
		this.BankCode = aLLAppealAcceptSchema.getBankCode();
		this.SendFlag = aLLAppealAcceptSchema.getSendFlag();
		this.BankAccNo = aLLAppealAcceptSchema.getBankAccNo();
		this.AccName = aLLAppealAcceptSchema.getAccName();
		this.CustStatus = aLLAppealAcceptSchema.getCustStatus();
		this.DeathDate = fDate.getDate( aLLAppealAcceptSchema.getDeathDate());
		this.appReasonCode = aLLAppealAcceptSchema.getappReasonCode();
		this.InsuredStat = aLLAppealAcceptSchema.getInsuredStat();
		this.SecurityNo = aLLAppealAcceptSchema.getSecurityNo();
		this.CompSecuNo = aLLAppealAcceptSchema.getCompSecuNo();
		this.HospitalName = aLLAppealAcceptSchema.getHospitalName();
		this.HospitalCode = aLLAppealAcceptSchema.getHospitalCode();
		this.FeeAtti = aLLAppealAcceptSchema.getFeeAtti();
		this.FeeType = aLLAppealAcceptSchema.getFeeType();
		this.FeeAffixType = aLLAppealAcceptSchema.getFeeAffixType();
		this.ReceiptType = aLLAppealAcceptSchema.getReceiptType();
		this.ReceiptNo = aLLAppealAcceptSchema.getReceiptNo();
		this.inpatientNo = aLLAppealAcceptSchema.getinpatientNo();
		this.FeeDate = fDate.getDate( aLLAppealAcceptSchema.getFeeDate());
		this.HospStartDate = fDate.getDate( aLLAppealAcceptSchema.getHospStartDate());
		this.HospEndDate = fDate.getDate( aLLAppealAcceptSchema.getHospEndDate());
		this.IssueUnit = aLLAppealAcceptSchema.getIssueUnit();
		this.SimpleCase = aLLAppealAcceptSchema.getSimpleCase();
		this.EasyCase = aLLAppealAcceptSchema.getEasyCase();
		this.HeadCase = aLLAppealAcceptSchema.getHeadCase();
		this.FenCase = aLLAppealAcceptSchema.getFenCase();
		this.ContRemark = aLLAppealAcceptSchema.getContRemark();
		this.PrePaidFlag = aLLAppealAcceptSchema.getPrePaidFlag();
		this.checkDecision2 = aLLAppealAcceptSchema.getcheckDecision2();
		this.Remark1 = aLLAppealAcceptSchema.getRemark1();
		this.Remark2 = aLLAppealAcceptSchema.getRemark2();
		this.GAccName = aLLAppealAcceptSchema.getGAccName();
		this.GBankAccNo = aLLAppealAcceptSchema.getGBankAccNo();
		this.GBankCode = aLLAppealAcceptSchema.getGBankCode();
		this.Gpaymode = aLLAppealAcceptSchema.getGpaymode();
		this.GPrePaidFlag = aLLAppealAcceptSchema.getGPrePaidFlag();
		this.Drawer = aLLAppealAcceptSchema.getDrawer();
		this.DrawerID = aLLAppealAcceptSchema.getDrawerID();
		this.ModifyReason = aLLAppealAcceptSchema.getModifyReason();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			this.SendDate = rs.getDate("SendDate");
			if( rs.getString("SendTime") == null )
				this.SendTime = null;
			else
				this.SendTime = rs.getString("SendTime").trim();

			if( rs.getString("BranchCode") == null )
				this.BranchCode = null;
			else
				this.BranchCode = rs.getString("BranchCode").trim();

			if( rs.getString("SendOperator") == null )
				this.SendOperator = null;
			else
				this.SendOperator = rs.getString("SendOperator").trim();

			if( rs.getString("MsgType") == null )
				this.MsgType = null;
			else
				this.MsgType = rs.getString("MsgType").trim();

			if( rs.getString("Rownum") == null )
				this.Rownum = null;
			else
				this.Rownum = rs.getString("Rownum").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("rgtno") == null )
				this.rgtno = null;
			else
				this.rgtno = rs.getString("rgtno").trim();

			if( rs.getString("AppealType") == null )
				this.AppealType = null;
			else
				this.AppealType = rs.getString("AppealType").trim();

			if( rs.getString("AppeanRCode") == null )
				this.AppeanRCode = null;
			else
				this.AppeanRCode = rs.getString("AppeanRCode").trim();

			if( rs.getString("AppealRDesc") == null )
				this.AppealRDesc = null;
			else
				this.AppealRDesc = rs.getString("AppealRDesc").trim();

			if( rs.getString("CustomerName") == null )
				this.CustomerName = null;
			else
				this.CustomerName = rs.getString("CustomerName").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("tIDNo") == null )
				this.tIDNo = null;
			else
				this.tIDNo = rs.getString("tIDNo").trim();

			if( rs.getString("tIDType") == null )
				this.tIDType = null;
			else
				this.tIDType = rs.getString("tIDType").trim();

			this.CBirthday = rs.getDate("CBirthday");
			if( rs.getString("OtherIDNo") == null )
				this.OtherIDNo = null;
			else
				this.OtherIDNo = rs.getString("OtherIDNo").trim();

			if( rs.getString("MobilePhone") == null )
				this.MobilePhone = null;
			else
				this.MobilePhone = rs.getString("MobilePhone").trim();

			if( rs.getString("Relation") == null )
				this.Relation = null;
			else
				this.Relation = rs.getString("Relation").trim();

			if( rs.getString("RgtType") == null )
				this.RgtType = null;
			else
				this.RgtType = rs.getString("RgtType").trim();

			if( rs.getString("ReturnMode") == null )
				this.ReturnMode = null;
			else
				this.ReturnMode = rs.getString("ReturnMode").trim();

			if( rs.getString("RgtantName") == null )
				this.RgtantName = null;
			else
				this.RgtantName = rs.getString("RgtantName").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("RgtantPhone") == null )
				this.RgtantPhone = null;
			else
				this.RgtantPhone = rs.getString("RgtantPhone").trim();

			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("Email") == null )
				this.Email = null;
			else
				this.Email = rs.getString("Email").trim();

			if( rs.getString("RgtantAddress") == null )
				this.RgtantAddress = null;
			else
				this.RgtantAddress = rs.getString("RgtantAddress").trim();

			if( rs.getString("PostCode") == null )
				this.PostCode = null;
			else
				this.PostCode = rs.getString("PostCode").trim();

			if( rs.getString("paymode") == null )
				this.paymode = null;
			else
				this.paymode = rs.getString("paymode").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("SendFlag") == null )
				this.SendFlag = null;
			else
				this.SendFlag = rs.getString("SendFlag").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("CustStatus") == null )
				this.CustStatus = null;
			else
				this.CustStatus = rs.getString("CustStatus").trim();

			this.DeathDate = rs.getDate("DeathDate");
			if( rs.getString("appReasonCode") == null )
				this.appReasonCode = null;
			else
				this.appReasonCode = rs.getString("appReasonCode").trim();

			if( rs.getString("InsuredStat") == null )
				this.InsuredStat = null;
			else
				this.InsuredStat = rs.getString("InsuredStat").trim();

			if( rs.getString("SecurityNo") == null )
				this.SecurityNo = null;
			else
				this.SecurityNo = rs.getString("SecurityNo").trim();

			if( rs.getString("CompSecuNo") == null )
				this.CompSecuNo = null;
			else
				this.CompSecuNo = rs.getString("CompSecuNo").trim();

			if( rs.getString("HospitalName") == null )
				this.HospitalName = null;
			else
				this.HospitalName = rs.getString("HospitalName").trim();

			if( rs.getString("HospitalCode") == null )
				this.HospitalCode = null;
			else
				this.HospitalCode = rs.getString("HospitalCode").trim();

			if( rs.getString("FeeAtti") == null )
				this.FeeAtti = null;
			else
				this.FeeAtti = rs.getString("FeeAtti").trim();

			if( rs.getString("FeeType") == null )
				this.FeeType = null;
			else
				this.FeeType = rs.getString("FeeType").trim();

			if( rs.getString("FeeAffixType") == null )
				this.FeeAffixType = null;
			else
				this.FeeAffixType = rs.getString("FeeAffixType").trim();

			if( rs.getString("ReceiptType") == null )
				this.ReceiptType = null;
			else
				this.ReceiptType = rs.getString("ReceiptType").trim();

			if( rs.getString("ReceiptNo") == null )
				this.ReceiptNo = null;
			else
				this.ReceiptNo = rs.getString("ReceiptNo").trim();

			if( rs.getString("inpatientNo") == null )
				this.inpatientNo = null;
			else
				this.inpatientNo = rs.getString("inpatientNo").trim();

			this.FeeDate = rs.getDate("FeeDate");
			this.HospStartDate = rs.getDate("HospStartDate");
			this.HospEndDate = rs.getDate("HospEndDate");
			if( rs.getString("IssueUnit") == null )
				this.IssueUnit = null;
			else
				this.IssueUnit = rs.getString("IssueUnit").trim();

			if( rs.getString("SimpleCase") == null )
				this.SimpleCase = null;
			else
				this.SimpleCase = rs.getString("SimpleCase").trim();

			if( rs.getString("EasyCase") == null )
				this.EasyCase = null;
			else
				this.EasyCase = rs.getString("EasyCase").trim();

			if( rs.getString("HeadCase") == null )
				this.HeadCase = null;
			else
				this.HeadCase = rs.getString("HeadCase").trim();

			if( rs.getString("FenCase") == null )
				this.FenCase = null;
			else
				this.FenCase = rs.getString("FenCase").trim();

			if( rs.getString("ContRemark") == null )
				this.ContRemark = null;
			else
				this.ContRemark = rs.getString("ContRemark").trim();

			if( rs.getString("PrePaidFlag") == null )
				this.PrePaidFlag = null;
			else
				this.PrePaidFlag = rs.getString("PrePaidFlag").trim();

			if( rs.getString("checkDecision2") == null )
				this.checkDecision2 = null;
			else
				this.checkDecision2 = rs.getString("checkDecision2").trim();

			if( rs.getString("Remark1") == null )
				this.Remark1 = null;
			else
				this.Remark1 = rs.getString("Remark1").trim();

			if( rs.getString("Remark2") == null )
				this.Remark2 = null;
			else
				this.Remark2 = rs.getString("Remark2").trim();

			if( rs.getString("GAccName") == null )
				this.GAccName = null;
			else
				this.GAccName = rs.getString("GAccName").trim();

			if( rs.getString("GBankAccNo") == null )
				this.GBankAccNo = null;
			else
				this.GBankAccNo = rs.getString("GBankAccNo").trim();

			if( rs.getString("GBankCode") == null )
				this.GBankCode = null;
			else
				this.GBankCode = rs.getString("GBankCode").trim();

			if( rs.getString("Gpaymode") == null )
				this.Gpaymode = null;
			else
				this.Gpaymode = rs.getString("Gpaymode").trim();

			if( rs.getString("GPrePaidFlag") == null )
				this.GPrePaidFlag = null;
			else
				this.GPrePaidFlag = rs.getString("GPrePaidFlag").trim();

			if( rs.getString("Drawer") == null )
				this.Drawer = null;
			else
				this.Drawer = rs.getString("Drawer").trim();

			if( rs.getString("DrawerID") == null )
				this.DrawerID = null;
			else
				this.DrawerID = rs.getString("DrawerID").trim();

			if( rs.getString("ModifyReason") == null )
				this.ModifyReason = null;
			else
				this.ModifyReason = rs.getString("ModifyReason").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLAppealAccept表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLAppealAcceptSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLAppealAcceptSchema getSchema()
	{
		LLAppealAcceptSchema aLLAppealAcceptSchema = new LLAppealAcceptSchema();
		aLLAppealAcceptSchema.setSchema(this);
		return aLLAppealAcceptSchema;
	}

	public LLAppealAcceptDB getDB()
	{
		LLAppealAcceptDB aDBOper = new LLAppealAcceptDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLAppealAccept描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SendDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MsgType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Rownum)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(rgtno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppealType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppeanRCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppealRDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(tIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(tIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MobilePhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Relation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReturnMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtantName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtantPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Email)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtantAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(paymode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SendFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DeathDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(appReasonCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredStat)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SecurityNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompSecuNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HospitalCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeAtti)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeAffixType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReceiptNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(inpatientNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FeeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HospStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( HospEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueUnit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SimpleCase)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EasyCase)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HeadCase)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FenCase)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContRemark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrePaidFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(checkDecision2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GAccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GBankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Gpaymode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GPrePaidFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Drawer)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DrawerID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyReason));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLAppealAccept>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			SendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,SysConst.PACKAGESPILTER));
			SendTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BranchCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SendOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			MsgType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Rownum = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			rgtno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AppealType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AppeanRCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AppealRDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			tIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			tIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			OtherIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			MobilePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Relation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			RgtType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ReturnMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			RgtantName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			RgtantPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			RgtantAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			PostCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			paymode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			SendFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			CustStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			DeathDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,SysConst.PACKAGESPILTER));
			appReasonCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			InsuredStat = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			SecurityNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			CompSecuNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			FeeAtti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			FeeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			FeeAffixType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			ReceiptType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			ReceiptNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			inpatientNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			FeeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50,SysConst.PACKAGESPILTER));
			HospStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51,SysConst.PACKAGESPILTER));
			HospEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52,SysConst.PACKAGESPILTER));
			IssueUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			SimpleCase = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			EasyCase = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			HeadCase = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			FenCase = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			ContRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			PrePaidFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			checkDecision2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			Remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			GAccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
			GBankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64, SysConst.PACKAGESPILTER );
			GBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 65, SysConst.PACKAGESPILTER );
			Gpaymode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 66, SysConst.PACKAGESPILTER );
			GPrePaidFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 67, SysConst.PACKAGESPILTER );
			Drawer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 68, SysConst.PACKAGESPILTER );
			DrawerID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 69, SysConst.PACKAGESPILTER );
			ModifyReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 70, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLAppealAcceptSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("SendDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
		}
		if (FCode.equals("SendTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendTime));
		}
		if (FCode.equals("BranchCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchCode));
		}
		if (FCode.equals("SendOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendOperator));
		}
		if (FCode.equals("MsgType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MsgType));
		}
		if (FCode.equals("Rownum"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rownum));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("rgtno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(rgtno));
		}
		if (FCode.equals("AppealType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppealType));
		}
		if (FCode.equals("AppeanRCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppeanRCode));
		}
		if (FCode.equals("AppealRDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppealRDesc));
		}
		if (FCode.equals("CustomerName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("tIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(tIDNo));
		}
		if (FCode.equals("tIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(tIDType));
		}
		if (FCode.equals("CBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCBirthday()));
		}
		if (FCode.equals("OtherIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherIDNo));
		}
		if (FCode.equals("MobilePhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MobilePhone));
		}
		if (FCode.equals("Relation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
		}
		if (FCode.equals("RgtType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtType));
		}
		if (FCode.equals("ReturnMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnMode));
		}
		if (FCode.equals("RgtantName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantName));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("RgtantPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantPhone));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("Email"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
		}
		if (FCode.equals("RgtantAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantAddress));
		}
		if (FCode.equals("PostCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostCode));
		}
		if (FCode.equals("paymode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(paymode));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("SendFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SendFlag));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("CustStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustStatus));
		}
		if (FCode.equals("DeathDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDeathDate()));
		}
		if (FCode.equals("appReasonCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(appReasonCode));
		}
		if (FCode.equals("InsuredStat"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredStat));
		}
		if (FCode.equals("SecurityNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecurityNo));
		}
		if (FCode.equals("CompSecuNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompSecuNo));
		}
		if (FCode.equals("HospitalName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
		}
		if (FCode.equals("HospitalCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
		}
		if (FCode.equals("FeeAtti"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeAtti));
		}
		if (FCode.equals("FeeType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeType));
		}
		if (FCode.equals("FeeAffixType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeAffixType));
		}
		if (FCode.equals("ReceiptType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptType));
		}
		if (FCode.equals("ReceiptNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiptNo));
		}
		if (FCode.equals("inpatientNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(inpatientNo));
		}
		if (FCode.equals("FeeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
		}
		if (FCode.equals("HospStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHospStartDate()));
		}
		if (FCode.equals("HospEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getHospEndDate()));
		}
		if (FCode.equals("IssueUnit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueUnit));
		}
		if (FCode.equals("SimpleCase"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SimpleCase));
		}
		if (FCode.equals("EasyCase"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EasyCase));
		}
		if (FCode.equals("HeadCase"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HeadCase));
		}
		if (FCode.equals("FenCase"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FenCase));
		}
		if (FCode.equals("ContRemark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContRemark));
		}
		if (FCode.equals("PrePaidFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrePaidFlag));
		}
		if (FCode.equals("checkDecision2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(checkDecision2));
		}
		if (FCode.equals("Remark1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
		}
		if (FCode.equals("Remark2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
		}
		if (FCode.equals("GAccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GAccName));
		}
		if (FCode.equals("GBankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GBankAccNo));
		}
		if (FCode.equals("GBankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GBankCode));
		}
		if (FCode.equals("Gpaymode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Gpaymode));
		}
		if (FCode.equals("GPrePaidFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GPrePaidFlag));
		}
		if (FCode.equals("Drawer"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Drawer));
		}
		if (FCode.equals("DrawerID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawerID));
		}
		if (FCode.equals("ModifyReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyReason));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSendDate()));
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(SendTime);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BranchCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SendOperator);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(MsgType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Rownum);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(rgtno);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AppealType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AppeanRCode);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(AppealRDesc);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(CustomerName);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(tIDNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(tIDType);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCBirthday()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(OtherIDNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MobilePhone);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Relation);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(RgtType);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ReturnMode);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(RgtantName);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(RgtantPhone);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Email);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(RgtantAddress);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(PostCode);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(paymode);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(SendFlag);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(CustStatus);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDeathDate()));
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(appReasonCode);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(InsuredStat);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(SecurityNo);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(CompSecuNo);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(HospitalName);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(HospitalCode);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(FeeAtti);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(FeeType);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(FeeAffixType);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(ReceiptType);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(ReceiptNo);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(inpatientNo);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFeeDate()));
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHospStartDate()));
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getHospEndDate()));
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(IssueUnit);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(SimpleCase);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(EasyCase);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(HeadCase);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(FenCase);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(ContRemark);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(PrePaidFlag);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(checkDecision2);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(Remark1);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(Remark2);
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(GAccName);
				break;
			case 63:
				strFieldValue = StrTool.GBKToUnicode(GBankAccNo);
				break;
			case 64:
				strFieldValue = StrTool.GBKToUnicode(GBankCode);
				break;
			case 65:
				strFieldValue = StrTool.GBKToUnicode(Gpaymode);
				break;
			case 66:
				strFieldValue = StrTool.GBKToUnicode(GPrePaidFlag);
				break;
			case 67:
				strFieldValue = StrTool.GBKToUnicode(Drawer);
				break;
			case 68:
				strFieldValue = StrTool.GBKToUnicode(DrawerID);
				break;
			case 69:
				strFieldValue = StrTool.GBKToUnicode(ModifyReason);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("SendDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SendDate = fDate.getDate( FValue );
			}
			else
				SendDate = null;
		}
		if (FCode.equalsIgnoreCase("SendTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendTime = FValue.trim();
			}
			else
				SendTime = null;
		}
		if (FCode.equalsIgnoreCase("BranchCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchCode = FValue.trim();
			}
			else
				BranchCode = null;
		}
		if (FCode.equalsIgnoreCase("SendOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendOperator = FValue.trim();
			}
			else
				SendOperator = null;
		}
		if (FCode.equalsIgnoreCase("MsgType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MsgType = FValue.trim();
			}
			else
				MsgType = null;
		}
		if (FCode.equalsIgnoreCase("Rownum"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Rownum = FValue.trim();
			}
			else
				Rownum = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("rgtno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				rgtno = FValue.trim();
			}
			else
				rgtno = null;
		}
		if (FCode.equalsIgnoreCase("AppealType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppealType = FValue.trim();
			}
			else
				AppealType = null;
		}
		if (FCode.equalsIgnoreCase("AppeanRCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppeanRCode = FValue.trim();
			}
			else
				AppeanRCode = null;
		}
		if (FCode.equalsIgnoreCase("AppealRDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppealRDesc = FValue.trim();
			}
			else
				AppealRDesc = null;
		}
		if (FCode.equalsIgnoreCase("CustomerName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerName = FValue.trim();
			}
			else
				CustomerName = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("tIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				tIDNo = FValue.trim();
			}
			else
				tIDNo = null;
		}
		if (FCode.equalsIgnoreCase("tIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				tIDType = FValue.trim();
			}
			else
				tIDType = null;
		}
		if (FCode.equalsIgnoreCase("CBirthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CBirthday = fDate.getDate( FValue );
			}
			else
				CBirthday = null;
		}
		if (FCode.equalsIgnoreCase("OtherIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherIDNo = FValue.trim();
			}
			else
				OtherIDNo = null;
		}
		if (FCode.equalsIgnoreCase("MobilePhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MobilePhone = FValue.trim();
			}
			else
				MobilePhone = null;
		}
		if (FCode.equalsIgnoreCase("Relation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Relation = FValue.trim();
			}
			else
				Relation = null;
		}
		if (FCode.equalsIgnoreCase("RgtType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtType = FValue.trim();
			}
			else
				RgtType = null;
		}
		if (FCode.equalsIgnoreCase("ReturnMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReturnMode = FValue.trim();
			}
			else
				ReturnMode = null;
		}
		if (FCode.equalsIgnoreCase("RgtantName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtantName = FValue.trim();
			}
			else
				RgtantName = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("RgtantPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtantPhone = FValue.trim();
			}
			else
				RgtantPhone = null;
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("Email"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Email = FValue.trim();
			}
			else
				Email = null;
		}
		if (FCode.equalsIgnoreCase("RgtantAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtantAddress = FValue.trim();
			}
			else
				RgtantAddress = null;
		}
		if (FCode.equalsIgnoreCase("PostCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostCode = FValue.trim();
			}
			else
				PostCode = null;
		}
		if (FCode.equalsIgnoreCase("paymode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				paymode = FValue.trim();
			}
			else
				paymode = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("SendFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SendFlag = FValue.trim();
			}
			else
				SendFlag = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("CustStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustStatus = FValue.trim();
			}
			else
				CustStatus = null;
		}
		if (FCode.equalsIgnoreCase("DeathDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DeathDate = fDate.getDate( FValue );
			}
			else
				DeathDate = null;
		}
		if (FCode.equalsIgnoreCase("appReasonCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				appReasonCode = FValue.trim();
			}
			else
				appReasonCode = null;
		}
		if (FCode.equalsIgnoreCase("InsuredStat"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredStat = FValue.trim();
			}
			else
				InsuredStat = null;
		}
		if (FCode.equalsIgnoreCase("SecurityNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecurityNo = FValue.trim();
			}
			else
				SecurityNo = null;
		}
		if (FCode.equalsIgnoreCase("CompSecuNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompSecuNo = FValue.trim();
			}
			else
				CompSecuNo = null;
		}
		if (FCode.equalsIgnoreCase("HospitalName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalName = FValue.trim();
			}
			else
				HospitalName = null;
		}
		if (FCode.equalsIgnoreCase("HospitalCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitalCode = FValue.trim();
			}
			else
				HospitalCode = null;
		}
		if (FCode.equalsIgnoreCase("FeeAtti"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeAtti = FValue.trim();
			}
			else
				FeeAtti = null;
		}
		if (FCode.equalsIgnoreCase("FeeType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeType = FValue.trim();
			}
			else
				FeeType = null;
		}
		if (FCode.equalsIgnoreCase("FeeAffixType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeAffixType = FValue.trim();
			}
			else
				FeeAffixType = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptType = FValue.trim();
			}
			else
				ReceiptType = null;
		}
		if (FCode.equalsIgnoreCase("ReceiptNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReceiptNo = FValue.trim();
			}
			else
				ReceiptNo = null;
		}
		if (FCode.equalsIgnoreCase("inpatientNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				inpatientNo = FValue.trim();
			}
			else
				inpatientNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FeeDate = fDate.getDate( FValue );
			}
			else
				FeeDate = null;
		}
		if (FCode.equalsIgnoreCase("HospStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HospStartDate = fDate.getDate( FValue );
			}
			else
				HospStartDate = null;
		}
		if (FCode.equalsIgnoreCase("HospEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				HospEndDate = fDate.getDate( FValue );
			}
			else
				HospEndDate = null;
		}
		if (FCode.equalsIgnoreCase("IssueUnit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueUnit = FValue.trim();
			}
			else
				IssueUnit = null;
		}
		if (FCode.equalsIgnoreCase("SimpleCase"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SimpleCase = FValue.trim();
			}
			else
				SimpleCase = null;
		}
		if (FCode.equalsIgnoreCase("EasyCase"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EasyCase = FValue.trim();
			}
			else
				EasyCase = null;
		}
		if (FCode.equalsIgnoreCase("HeadCase"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HeadCase = FValue.trim();
			}
			else
				HeadCase = null;
		}
		if (FCode.equalsIgnoreCase("FenCase"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FenCase = FValue.trim();
			}
			else
				FenCase = null;
		}
		if (FCode.equalsIgnoreCase("ContRemark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContRemark = FValue.trim();
			}
			else
				ContRemark = null;
		}
		if (FCode.equalsIgnoreCase("PrePaidFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrePaidFlag = FValue.trim();
			}
			else
				PrePaidFlag = null;
		}
		if (FCode.equalsIgnoreCase("checkDecision2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				checkDecision2 = FValue.trim();
			}
			else
				checkDecision2 = null;
		}
		if (FCode.equalsIgnoreCase("Remark1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark1 = FValue.trim();
			}
			else
				Remark1 = null;
		}
		if (FCode.equalsIgnoreCase("Remark2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark2 = FValue.trim();
			}
			else
				Remark2 = null;
		}
		if (FCode.equalsIgnoreCase("GAccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GAccName = FValue.trim();
			}
			else
				GAccName = null;
		}
		if (FCode.equalsIgnoreCase("GBankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GBankAccNo = FValue.trim();
			}
			else
				GBankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("GBankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GBankCode = FValue.trim();
			}
			else
				GBankCode = null;
		}
		if (FCode.equalsIgnoreCase("Gpaymode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Gpaymode = FValue.trim();
			}
			else
				Gpaymode = null;
		}
		if (FCode.equalsIgnoreCase("GPrePaidFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GPrePaidFlag = FValue.trim();
			}
			else
				GPrePaidFlag = null;
		}
		if (FCode.equalsIgnoreCase("Drawer"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Drawer = FValue.trim();
			}
			else
				Drawer = null;
		}
		if (FCode.equalsIgnoreCase("DrawerID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawerID = FValue.trim();
			}
			else
				DrawerID = null;
		}
		if (FCode.equalsIgnoreCase("ModifyReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyReason = FValue.trim();
			}
			else
				ModifyReason = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLAppealAcceptSchema other = (LLAppealAcceptSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (SendDate == null ? other.getSendDate() == null : fDate.getString(SendDate).equals(other.getSendDate()))
			&& (SendTime == null ? other.getSendTime() == null : SendTime.equals(other.getSendTime()))
			&& (BranchCode == null ? other.getBranchCode() == null : BranchCode.equals(other.getBranchCode()))
			&& (SendOperator == null ? other.getSendOperator() == null : SendOperator.equals(other.getSendOperator()))
			&& (MsgType == null ? other.getMsgType() == null : MsgType.equals(other.getMsgType()))
			&& (Rownum == null ? other.getRownum() == null : Rownum.equals(other.getRownum()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (rgtno == null ? other.getrgtno() == null : rgtno.equals(other.getrgtno()))
			&& (AppealType == null ? other.getAppealType() == null : AppealType.equals(other.getAppealType()))
			&& (AppeanRCode == null ? other.getAppeanRCode() == null : AppeanRCode.equals(other.getAppeanRCode()))
			&& (AppealRDesc == null ? other.getAppealRDesc() == null : AppealRDesc.equals(other.getAppealRDesc()))
			&& (CustomerName == null ? other.getCustomerName() == null : CustomerName.equals(other.getCustomerName()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (tIDNo == null ? other.gettIDNo() == null : tIDNo.equals(other.gettIDNo()))
			&& (tIDType == null ? other.gettIDType() == null : tIDType.equals(other.gettIDType()))
			&& (CBirthday == null ? other.getCBirthday() == null : fDate.getString(CBirthday).equals(other.getCBirthday()))
			&& (OtherIDNo == null ? other.getOtherIDNo() == null : OtherIDNo.equals(other.getOtherIDNo()))
			&& (MobilePhone == null ? other.getMobilePhone() == null : MobilePhone.equals(other.getMobilePhone()))
			&& (Relation == null ? other.getRelation() == null : Relation.equals(other.getRelation()))
			&& (RgtType == null ? other.getRgtType() == null : RgtType.equals(other.getRgtType()))
			&& (ReturnMode == null ? other.getReturnMode() == null : ReturnMode.equals(other.getReturnMode()))
			&& (RgtantName == null ? other.getRgtantName() == null : RgtantName.equals(other.getRgtantName()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (RgtantPhone == null ? other.getRgtantPhone() == null : RgtantPhone.equals(other.getRgtantPhone()))
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (Email == null ? other.getEmail() == null : Email.equals(other.getEmail()))
			&& (RgtantAddress == null ? other.getRgtantAddress() == null : RgtantAddress.equals(other.getRgtantAddress()))
			&& (PostCode == null ? other.getPostCode() == null : PostCode.equals(other.getPostCode()))
			&& (paymode == null ? other.getpaymode() == null : paymode.equals(other.getpaymode()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (SendFlag == null ? other.getSendFlag() == null : SendFlag.equals(other.getSendFlag()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (CustStatus == null ? other.getCustStatus() == null : CustStatus.equals(other.getCustStatus()))
			&& (DeathDate == null ? other.getDeathDate() == null : fDate.getString(DeathDate).equals(other.getDeathDate()))
			&& (appReasonCode == null ? other.getappReasonCode() == null : appReasonCode.equals(other.getappReasonCode()))
			&& (InsuredStat == null ? other.getInsuredStat() == null : InsuredStat.equals(other.getInsuredStat()))
			&& (SecurityNo == null ? other.getSecurityNo() == null : SecurityNo.equals(other.getSecurityNo()))
			&& (CompSecuNo == null ? other.getCompSecuNo() == null : CompSecuNo.equals(other.getCompSecuNo()))
			&& (HospitalName == null ? other.getHospitalName() == null : HospitalName.equals(other.getHospitalName()))
			&& (HospitalCode == null ? other.getHospitalCode() == null : HospitalCode.equals(other.getHospitalCode()))
			&& (FeeAtti == null ? other.getFeeAtti() == null : FeeAtti.equals(other.getFeeAtti()))
			&& (FeeType == null ? other.getFeeType() == null : FeeType.equals(other.getFeeType()))
			&& (FeeAffixType == null ? other.getFeeAffixType() == null : FeeAffixType.equals(other.getFeeAffixType()))
			&& (ReceiptType == null ? other.getReceiptType() == null : ReceiptType.equals(other.getReceiptType()))
			&& (ReceiptNo == null ? other.getReceiptNo() == null : ReceiptNo.equals(other.getReceiptNo()))
			&& (inpatientNo == null ? other.getinpatientNo() == null : inpatientNo.equals(other.getinpatientNo()))
			&& (FeeDate == null ? other.getFeeDate() == null : fDate.getString(FeeDate).equals(other.getFeeDate()))
			&& (HospStartDate == null ? other.getHospStartDate() == null : fDate.getString(HospStartDate).equals(other.getHospStartDate()))
			&& (HospEndDate == null ? other.getHospEndDate() == null : fDate.getString(HospEndDate).equals(other.getHospEndDate()))
			&& (IssueUnit == null ? other.getIssueUnit() == null : IssueUnit.equals(other.getIssueUnit()))
			&& (SimpleCase == null ? other.getSimpleCase() == null : SimpleCase.equals(other.getSimpleCase()))
			&& (EasyCase == null ? other.getEasyCase() == null : EasyCase.equals(other.getEasyCase()))
			&& (HeadCase == null ? other.getHeadCase() == null : HeadCase.equals(other.getHeadCase()))
			&& (FenCase == null ? other.getFenCase() == null : FenCase.equals(other.getFenCase()))
			&& (ContRemark == null ? other.getContRemark() == null : ContRemark.equals(other.getContRemark()))
			&& (PrePaidFlag == null ? other.getPrePaidFlag() == null : PrePaidFlag.equals(other.getPrePaidFlag()))
			&& (checkDecision2 == null ? other.getcheckDecision2() == null : checkDecision2.equals(other.getcheckDecision2()))
			&& (Remark1 == null ? other.getRemark1() == null : Remark1.equals(other.getRemark1()))
			&& (Remark2 == null ? other.getRemark2() == null : Remark2.equals(other.getRemark2()))
			&& (GAccName == null ? other.getGAccName() == null : GAccName.equals(other.getGAccName()))
			&& (GBankAccNo == null ? other.getGBankAccNo() == null : GBankAccNo.equals(other.getGBankAccNo()))
			&& (GBankCode == null ? other.getGBankCode() == null : GBankCode.equals(other.getGBankCode()))
			&& (Gpaymode == null ? other.getGpaymode() == null : Gpaymode.equals(other.getGpaymode()))
			&& (GPrePaidFlag == null ? other.getGPrePaidFlag() == null : GPrePaidFlag.equals(other.getGPrePaidFlag()))
			&& (Drawer == null ? other.getDrawer() == null : Drawer.equals(other.getDrawer()))
			&& (DrawerID == null ? other.getDrawerID() == null : DrawerID.equals(other.getDrawerID()))
			&& (ModifyReason == null ? other.getModifyReason() == null : ModifyReason.equals(other.getModifyReason()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("SendDate") ) {
			return 1;
		}
		if( strFieldName.equals("SendTime") ) {
			return 2;
		}
		if( strFieldName.equals("BranchCode") ) {
			return 3;
		}
		if( strFieldName.equals("SendOperator") ) {
			return 4;
		}
		if( strFieldName.equals("MsgType") ) {
			return 5;
		}
		if( strFieldName.equals("Rownum") ) {
			return 6;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 7;
		}
		if( strFieldName.equals("rgtno") ) {
			return 8;
		}
		if( strFieldName.equals("AppealType") ) {
			return 9;
		}
		if( strFieldName.equals("AppeanRCode") ) {
			return 10;
		}
		if( strFieldName.equals("AppealRDesc") ) {
			return 11;
		}
		if( strFieldName.equals("CustomerName") ) {
			return 12;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 13;
		}
		if( strFieldName.equals("tIDNo") ) {
			return 14;
		}
		if( strFieldName.equals("tIDType") ) {
			return 15;
		}
		if( strFieldName.equals("CBirthday") ) {
			return 16;
		}
		if( strFieldName.equals("OtherIDNo") ) {
			return 17;
		}
		if( strFieldName.equals("MobilePhone") ) {
			return 18;
		}
		if( strFieldName.equals("Relation") ) {
			return 19;
		}
		if( strFieldName.equals("RgtType") ) {
			return 20;
		}
		if( strFieldName.equals("ReturnMode") ) {
			return 21;
		}
		if( strFieldName.equals("RgtantName") ) {
			return 22;
		}
		if( strFieldName.equals("IDNo") ) {
			return 23;
		}
		if( strFieldName.equals("IDType") ) {
			return 24;
		}
		if( strFieldName.equals("RgtantPhone") ) {
			return 25;
		}
		if( strFieldName.equals("Mobile") ) {
			return 26;
		}
		if( strFieldName.equals("Email") ) {
			return 27;
		}
		if( strFieldName.equals("RgtantAddress") ) {
			return 28;
		}
		if( strFieldName.equals("PostCode") ) {
			return 29;
		}
		if( strFieldName.equals("paymode") ) {
			return 30;
		}
		if( strFieldName.equals("BankCode") ) {
			return 31;
		}
		if( strFieldName.equals("SendFlag") ) {
			return 32;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 33;
		}
		if( strFieldName.equals("AccName") ) {
			return 34;
		}
		if( strFieldName.equals("CustStatus") ) {
			return 35;
		}
		if( strFieldName.equals("DeathDate") ) {
			return 36;
		}
		if( strFieldName.equals("appReasonCode") ) {
			return 37;
		}
		if( strFieldName.equals("InsuredStat") ) {
			return 38;
		}
		if( strFieldName.equals("SecurityNo") ) {
			return 39;
		}
		if( strFieldName.equals("CompSecuNo") ) {
			return 40;
		}
		if( strFieldName.equals("HospitalName") ) {
			return 41;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return 42;
		}
		if( strFieldName.equals("FeeAtti") ) {
			return 43;
		}
		if( strFieldName.equals("FeeType") ) {
			return 44;
		}
		if( strFieldName.equals("FeeAffixType") ) {
			return 45;
		}
		if( strFieldName.equals("ReceiptType") ) {
			return 46;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return 47;
		}
		if( strFieldName.equals("inpatientNo") ) {
			return 48;
		}
		if( strFieldName.equals("FeeDate") ) {
			return 49;
		}
		if( strFieldName.equals("HospStartDate") ) {
			return 50;
		}
		if( strFieldName.equals("HospEndDate") ) {
			return 51;
		}
		if( strFieldName.equals("IssueUnit") ) {
			return 52;
		}
		if( strFieldName.equals("SimpleCase") ) {
			return 53;
		}
		if( strFieldName.equals("EasyCase") ) {
			return 54;
		}
		if( strFieldName.equals("HeadCase") ) {
			return 55;
		}
		if( strFieldName.equals("FenCase") ) {
			return 56;
		}
		if( strFieldName.equals("ContRemark") ) {
			return 57;
		}
		if( strFieldName.equals("PrePaidFlag") ) {
			return 58;
		}
		if( strFieldName.equals("checkDecision2") ) {
			return 59;
		}
		if( strFieldName.equals("Remark1") ) {
			return 60;
		}
		if( strFieldName.equals("Remark2") ) {
			return 61;
		}
		if( strFieldName.equals("GAccName") ) {
			return 62;
		}
		if( strFieldName.equals("GBankAccNo") ) {
			return 63;
		}
		if( strFieldName.equals("GBankCode") ) {
			return 64;
		}
		if( strFieldName.equals("Gpaymode") ) {
			return 65;
		}
		if( strFieldName.equals("GPrePaidFlag") ) {
			return 66;
		}
		if( strFieldName.equals("Drawer") ) {
			return 67;
		}
		if( strFieldName.equals("DrawerID") ) {
			return 68;
		}
		if( strFieldName.equals("ModifyReason") ) {
			return 69;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "SendDate";
				break;
			case 2:
				strFieldName = "SendTime";
				break;
			case 3:
				strFieldName = "BranchCode";
				break;
			case 4:
				strFieldName = "SendOperator";
				break;
			case 5:
				strFieldName = "MsgType";
				break;
			case 6:
				strFieldName = "Rownum";
				break;
			case 7:
				strFieldName = "ManageCom";
				break;
			case 8:
				strFieldName = "rgtno";
				break;
			case 9:
				strFieldName = "AppealType";
				break;
			case 10:
				strFieldName = "AppeanRCode";
				break;
			case 11:
				strFieldName = "AppealRDesc";
				break;
			case 12:
				strFieldName = "CustomerName";
				break;
			case 13:
				strFieldName = "CustomerNo";
				break;
			case 14:
				strFieldName = "tIDNo";
				break;
			case 15:
				strFieldName = "tIDType";
				break;
			case 16:
				strFieldName = "CBirthday";
				break;
			case 17:
				strFieldName = "OtherIDNo";
				break;
			case 18:
				strFieldName = "MobilePhone";
				break;
			case 19:
				strFieldName = "Relation";
				break;
			case 20:
				strFieldName = "RgtType";
				break;
			case 21:
				strFieldName = "ReturnMode";
				break;
			case 22:
				strFieldName = "RgtantName";
				break;
			case 23:
				strFieldName = "IDNo";
				break;
			case 24:
				strFieldName = "IDType";
				break;
			case 25:
				strFieldName = "RgtantPhone";
				break;
			case 26:
				strFieldName = "Mobile";
				break;
			case 27:
				strFieldName = "Email";
				break;
			case 28:
				strFieldName = "RgtantAddress";
				break;
			case 29:
				strFieldName = "PostCode";
				break;
			case 30:
				strFieldName = "paymode";
				break;
			case 31:
				strFieldName = "BankCode";
				break;
			case 32:
				strFieldName = "SendFlag";
				break;
			case 33:
				strFieldName = "BankAccNo";
				break;
			case 34:
				strFieldName = "AccName";
				break;
			case 35:
				strFieldName = "CustStatus";
				break;
			case 36:
				strFieldName = "DeathDate";
				break;
			case 37:
				strFieldName = "appReasonCode";
				break;
			case 38:
				strFieldName = "InsuredStat";
				break;
			case 39:
				strFieldName = "SecurityNo";
				break;
			case 40:
				strFieldName = "CompSecuNo";
				break;
			case 41:
				strFieldName = "HospitalName";
				break;
			case 42:
				strFieldName = "HospitalCode";
				break;
			case 43:
				strFieldName = "FeeAtti";
				break;
			case 44:
				strFieldName = "FeeType";
				break;
			case 45:
				strFieldName = "FeeAffixType";
				break;
			case 46:
				strFieldName = "ReceiptType";
				break;
			case 47:
				strFieldName = "ReceiptNo";
				break;
			case 48:
				strFieldName = "inpatientNo";
				break;
			case 49:
				strFieldName = "FeeDate";
				break;
			case 50:
				strFieldName = "HospStartDate";
				break;
			case 51:
				strFieldName = "HospEndDate";
				break;
			case 52:
				strFieldName = "IssueUnit";
				break;
			case 53:
				strFieldName = "SimpleCase";
				break;
			case 54:
				strFieldName = "EasyCase";
				break;
			case 55:
				strFieldName = "HeadCase";
				break;
			case 56:
				strFieldName = "FenCase";
				break;
			case 57:
				strFieldName = "ContRemark";
				break;
			case 58:
				strFieldName = "PrePaidFlag";
				break;
			case 59:
				strFieldName = "checkDecision2";
				break;
			case 60:
				strFieldName = "Remark1";
				break;
			case 61:
				strFieldName = "Remark2";
				break;
			case 62:
				strFieldName = "GAccName";
				break;
			case 63:
				strFieldName = "GBankAccNo";
				break;
			case 64:
				strFieldName = "GBankCode";
				break;
			case 65:
				strFieldName = "Gpaymode";
				break;
			case 66:
				strFieldName = "GPrePaidFlag";
				break;
			case 67:
				strFieldName = "Drawer";
				break;
			case 68:
				strFieldName = "DrawerID";
				break;
			case 69:
				strFieldName = "ModifyReason";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SendTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MsgType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Rownum") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("rgtno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppealType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppeanRCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppealRDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("tIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("tIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CBirthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OtherIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MobilePhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Relation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReturnMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtantName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtantPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Email") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtantAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("paymode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SendFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeathDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("appReasonCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredStat") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecurityNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompSecuNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitalCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeAtti") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeAffixType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReceiptNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("inpatientNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HospStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HospEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IssueUnit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SimpleCase") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EasyCase") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HeadCase") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FenCase") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContRemark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrePaidFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("checkDecision2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GAccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GBankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GBankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Gpaymode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GPrePaidFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Drawer") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DrawerID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyReason") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 50:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 51:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 63:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 64:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 65:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 66:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 67:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 68:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 69:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
