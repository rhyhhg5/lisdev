/*
 * <p>ClassName: LAEmployeeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAEmployeeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LAEmployeeSchema implements Schema
{
    // @Field
    /** 人员编码 */
    private String EmployeeCode;
    /** 管理机构 */
    private String ManageCom;
    /** 姓名 */
    private String Name;
    /** 出生日期 */
    private Date Birthday;
    /** 身份证号码 */
    private String IDNo;
    /** 政治面貌 */
    private String PolityVisage;
    /** 民族 */
    private String Nationality;
    /** 性别 */
    private String Sex;
    /** 户口所在地 */
    private String RgtAddress;
    /** 学历 */
    private String Degree;
    /** 专业技术职称 */
    private String PostTitle;
    /** 入司日期 */
    private Date EmployeeDate;
    /** 离司日期 */
    private Date OutWorkDate;
    /** 高管标志 */
    private String ManageFlag;
    /** 人员状态 */
    private String EmployeeCodeState;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 20; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAEmployeeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "EmployeeCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEmployeeCode()
    {
        if (EmployeeCode != null && !EmployeeCode.equals("") &&
            SysConst.CHANGECHARSET)
        {
            EmployeeCode = StrTool.unicodeToGBK(EmployeeCode);
        }
        return EmployeeCode;
    }

    public void setEmployeeCode(String aEmployeeCode)
    {
        EmployeeCode = aEmployeeCode;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getName()
    {
        if (Name != null && !Name.equals("") && SysConst.CHANGECHARSET)
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getBirthday()
    {
        if (Birthday != null)
        {
            return fDate.getString(Birthday);
        }
        else
        {
            return null;
        }
    }

    public void setBirthday(Date aBirthday)
    {
        Birthday = aBirthday;
    }

    public void setBirthday(String aBirthday)
    {
        if (aBirthday != null && !aBirthday.equals(""))
        {
            Birthday = fDate.getDate(aBirthday);
        }
        else
        {
            Birthday = null;
        }
    }

    public String getIDNo()
    {
        if (IDNo != null && !IDNo.equals("") && SysConst.CHANGECHARSET)
        {
            IDNo = StrTool.unicodeToGBK(IDNo);
        }
        return IDNo;
    }

    public void setIDNo(String aIDNo)
    {
        IDNo = aIDNo;
    }

    public String getPolityVisage()
    {
        if (PolityVisage != null && !PolityVisage.equals("") &&
            SysConst.CHANGECHARSET)
        {
            PolityVisage = StrTool.unicodeToGBK(PolityVisage);
        }
        return PolityVisage;
    }

    public void setPolityVisage(String aPolityVisage)
    {
        PolityVisage = aPolityVisage;
    }

    public String getNationality()
    {
        if (Nationality != null && !Nationality.equals("") &&
            SysConst.CHANGECHARSET)
        {
            Nationality = StrTool.unicodeToGBK(Nationality);
        }
        return Nationality;
    }

    public void setNationality(String aNationality)
    {
        Nationality = aNationality;
    }

    public String getSex()
    {
        if (Sex != null && !Sex.equals("") && SysConst.CHANGECHARSET)
        {
            Sex = StrTool.unicodeToGBK(Sex);
        }
        return Sex;
    }

    public void setSex(String aSex)
    {
        Sex = aSex;
    }

    public String getRgtAddress()
    {
        if (RgtAddress != null && !RgtAddress.equals("") &&
            SysConst.CHANGECHARSET)
        {
            RgtAddress = StrTool.unicodeToGBK(RgtAddress);
        }
        return RgtAddress;
    }

    public void setRgtAddress(String aRgtAddress)
    {
        RgtAddress = aRgtAddress;
    }

    public String getDegree()
    {
        if (Degree != null && !Degree.equals("") && SysConst.CHANGECHARSET)
        {
            Degree = StrTool.unicodeToGBK(Degree);
        }
        return Degree;
    }

    public void setDegree(String aDegree)
    {
        Degree = aDegree;
    }

    public String getPostTitle()
    {
        if (PostTitle != null && !PostTitle.equals("") &&
            SysConst.CHANGECHARSET)
        {
            PostTitle = StrTool.unicodeToGBK(PostTitle);
        }
        return PostTitle;
    }

    public void setPostTitle(String aPostTitle)
    {
        PostTitle = aPostTitle;
    }

    public String getEmployeeDate()
    {
        if (EmployeeDate != null)
        {
            return fDate.getString(EmployeeDate);
        }
        else
        {
            return null;
        }
    }

    public void setEmployeeDate(Date aEmployeeDate)
    {
        EmployeeDate = aEmployeeDate;
    }

    public void setEmployeeDate(String aEmployeeDate)
    {
        if (aEmployeeDate != null && !aEmployeeDate.equals(""))
        {
            EmployeeDate = fDate.getDate(aEmployeeDate);
        }
        else
        {
            EmployeeDate = null;
        }
    }

    public String getOutWorkDate()
    {
        if (OutWorkDate != null)
        {
            return fDate.getString(OutWorkDate);
        }
        else
        {
            return null;
        }
    }

    public void setOutWorkDate(Date aOutWorkDate)
    {
        OutWorkDate = aOutWorkDate;
    }

    public void setOutWorkDate(String aOutWorkDate)
    {
        if (aOutWorkDate != null && !aOutWorkDate.equals(""))
        {
            OutWorkDate = fDate.getDate(aOutWorkDate);
        }
        else
        {
            OutWorkDate = null;
        }
    }

    public String getManageFlag()
    {
        if (ManageFlag != null && !ManageFlag.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ManageFlag = StrTool.unicodeToGBK(ManageFlag);
        }
        return ManageFlag;
    }

    public void setManageFlag(String aManageFlag)
    {
        ManageFlag = aManageFlag;
    }

    public String getEmployeeCodeState()
    {
        if (EmployeeCodeState != null && !EmployeeCodeState.equals("") &&
            SysConst.CHANGECHARSET)
        {
            EmployeeCodeState = StrTool.unicodeToGBK(EmployeeCodeState);
        }
        return EmployeeCodeState;
    }

    public void setEmployeeCodeState(String aEmployeeCodeState)
    {
        EmployeeCodeState = aEmployeeCodeState;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LAEmployeeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LAEmployeeSchema aLAEmployeeSchema)
    {
        this.EmployeeCode = aLAEmployeeSchema.getEmployeeCode();
        this.ManageCom = aLAEmployeeSchema.getManageCom();
        this.Name = aLAEmployeeSchema.getName();
        this.Birthday = fDate.getDate(aLAEmployeeSchema.getBirthday());
        this.IDNo = aLAEmployeeSchema.getIDNo();
        this.PolityVisage = aLAEmployeeSchema.getPolityVisage();
        this.Nationality = aLAEmployeeSchema.getNationality();
        this.Sex = aLAEmployeeSchema.getSex();
        this.RgtAddress = aLAEmployeeSchema.getRgtAddress();
        this.Degree = aLAEmployeeSchema.getDegree();
        this.PostTitle = aLAEmployeeSchema.getPostTitle();
        this.EmployeeDate = fDate.getDate(aLAEmployeeSchema.getEmployeeDate());
        this.OutWorkDate = fDate.getDate(aLAEmployeeSchema.getOutWorkDate());
        this.ManageFlag = aLAEmployeeSchema.getManageFlag();
        this.EmployeeCodeState = aLAEmployeeSchema.getEmployeeCodeState();
        this.Operator = aLAEmployeeSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAEmployeeSchema.getMakeDate());
        this.MakeTime = aLAEmployeeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAEmployeeSchema.getModifyDate());
        this.ModifyTime = aLAEmployeeSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EmployeeCode") == null)
            {
                this.EmployeeCode = null;
            }
            else
            {
                this.EmployeeCode = rs.getString("EmployeeCode").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Name") == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString("Name").trim();
            }

            this.Birthday = rs.getDate("Birthday");
            if (rs.getString("IDNo") == null)
            {
                this.IDNo = null;
            }
            else
            {
                this.IDNo = rs.getString("IDNo").trim();
            }

            if (rs.getString("PolityVisage") == null)
            {
                this.PolityVisage = null;
            }
            else
            {
                this.PolityVisage = rs.getString("PolityVisage").trim();
            }

            if (rs.getString("Nationality") == null)
            {
                this.Nationality = null;
            }
            else
            {
                this.Nationality = rs.getString("Nationality").trim();
            }

            if (rs.getString("Sex") == null)
            {
                this.Sex = null;
            }
            else
            {
                this.Sex = rs.getString("Sex").trim();
            }

            if (rs.getString("RgtAddress") == null)
            {
                this.RgtAddress = null;
            }
            else
            {
                this.RgtAddress = rs.getString("RgtAddress").trim();
            }

            if (rs.getString("Degree") == null)
            {
                this.Degree = null;
            }
            else
            {
                this.Degree = rs.getString("Degree").trim();
            }

            if (rs.getString("PostTitle") == null)
            {
                this.PostTitle = null;
            }
            else
            {
                this.PostTitle = rs.getString("PostTitle").trim();
            }

            this.EmployeeDate = rs.getDate("EmployeeDate");
            this.OutWorkDate = rs.getDate("OutWorkDate");
            if (rs.getString("ManageFlag") == null)
            {
                this.ManageFlag = null;
            }
            else
            {
                this.ManageFlag = rs.getString("ManageFlag").trim();
            }

            if (rs.getString("EmployeeCodeState") == null)
            {
                this.EmployeeCodeState = null;
            }
            else
            {
                this.EmployeeCodeState = rs.getString("EmployeeCodeState").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAEmployeeSchema getSchema()
    {
        LAEmployeeSchema aLAEmployeeSchema = new LAEmployeeSchema();
        aLAEmployeeSchema.setSchema(this);
        return aLAEmployeeSchema;
    }

    public LAEmployeeDB getDB()
    {
        LAEmployeeDB aDBOper = new LAEmployeeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAEmployee描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(EmployeeCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Name)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(Birthday))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IDNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolityVisage)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Nationality)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Sex)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RgtAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Degree)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PostTitle)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            EmployeeDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            OutWorkDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EmployeeCodeState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAEmployee>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EmployeeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                  SysConst.PACKAGESPILTER);
            Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                  SysConst.PACKAGESPILTER);
            PolityVisage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            Nationality = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                 SysConst.PACKAGESPILTER);
            RgtAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            Degree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
            PostTitle = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            EmployeeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            OutWorkDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ManageFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            EmployeeCodeState = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               15, SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAEmployeeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EmployeeCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EmployeeCode));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equals("Birthday"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getBirthday()));
        }
        if (FCode.equals("IDNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
        }
        if (FCode.equals("PolityVisage"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PolityVisage));
        }
        if (FCode.equals("Nationality"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Nationality));
        }
        if (FCode.equals("Sex"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equals("RgtAddress"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtAddress));
        }
        if (FCode.equals("Degree"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Degree));
        }
        if (FCode.equals("PostTitle"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PostTitle));
        }
        if (FCode.equals("EmployeeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getEmployeeDate()));
        }
        if (FCode.equals("OutWorkDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getOutWorkDate()));
        }
        if (FCode.equals("ManageFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageFlag));
        }
        if (FCode.equals("EmployeeCodeState"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(
                    EmployeeCodeState));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EmployeeCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBirthday()));
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(IDNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PolityVisage);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Nationality);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Sex);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(RgtAddress);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Degree);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(PostTitle);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getEmployeeDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getOutWorkDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ManageFlag);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(EmployeeCodeState);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EmployeeCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EmployeeCode = FValue.trim();
            }
            else
            {
                EmployeeCode = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("Birthday"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Birthday = fDate.getDate(FValue);
            }
            else
            {
                Birthday = null;
            }
        }
        if (FCode.equals("IDNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IDNo = FValue.trim();
            }
            else
            {
                IDNo = null;
            }
        }
        if (FCode.equals("PolityVisage"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolityVisage = FValue.trim();
            }
            else
            {
                PolityVisage = null;
            }
        }
        if (FCode.equals("Nationality"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Nationality = FValue.trim();
            }
            else
            {
                Nationality = null;
            }
        }
        if (FCode.equals("Sex"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Sex = FValue.trim();
            }
            else
            {
                Sex = null;
            }
        }
        if (FCode.equals("RgtAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtAddress = FValue.trim();
            }
            else
            {
                RgtAddress = null;
            }
        }
        if (FCode.equals("Degree"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Degree = FValue.trim();
            }
            else
            {
                Degree = null;
            }
        }
        if (FCode.equals("PostTitle"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PostTitle = FValue.trim();
            }
            else
            {
                PostTitle = null;
            }
        }
        if (FCode.equals("EmployeeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EmployeeDate = fDate.getDate(FValue);
            }
            else
            {
                EmployeeDate = null;
            }
        }
        if (FCode.equals("OutWorkDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OutWorkDate = fDate.getDate(FValue);
            }
            else
            {
                OutWorkDate = null;
            }
        }
        if (FCode.equals("ManageFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageFlag = FValue.trim();
            }
            else
            {
                ManageFlag = null;
            }
        }
        if (FCode.equals("EmployeeCodeState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EmployeeCodeState = FValue.trim();
            }
            else
            {
                EmployeeCodeState = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAEmployeeSchema other = (LAEmployeeSchema) otherObject;
        return
                EmployeeCode.equals(other.getEmployeeCode())
                && ManageCom.equals(other.getManageCom())
                && Name.equals(other.getName())
                && fDate.getString(Birthday).equals(other.getBirthday())
                && IDNo.equals(other.getIDNo())
                && PolityVisage.equals(other.getPolityVisage())
                && Nationality.equals(other.getNationality())
                && Sex.equals(other.getSex())
                && RgtAddress.equals(other.getRgtAddress())
                && Degree.equals(other.getDegree())
                && PostTitle.equals(other.getPostTitle())
                && fDate.getString(EmployeeDate).equals(other.getEmployeeDate())
                && fDate.getString(OutWorkDate).equals(other.getOutWorkDate())
                && ManageFlag.equals(other.getManageFlag())
                && EmployeeCodeState.equals(other.getEmployeeCodeState())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EmployeeCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 1;
        }
        if (strFieldName.equals("Name"))
        {
            return 2;
        }
        if (strFieldName.equals("Birthday"))
        {
            return 3;
        }
        if (strFieldName.equals("IDNo"))
        {
            return 4;
        }
        if (strFieldName.equals("PolityVisage"))
        {
            return 5;
        }
        if (strFieldName.equals("Nationality"))
        {
            return 6;
        }
        if (strFieldName.equals("Sex"))
        {
            return 7;
        }
        if (strFieldName.equals("RgtAddress"))
        {
            return 8;
        }
        if (strFieldName.equals("Degree"))
        {
            return 9;
        }
        if (strFieldName.equals("PostTitle"))
        {
            return 10;
        }
        if (strFieldName.equals("EmployeeDate"))
        {
            return 11;
        }
        if (strFieldName.equals("OutWorkDate"))
        {
            return 12;
        }
        if (strFieldName.equals("ManageFlag"))
        {
            return 13;
        }
        if (strFieldName.equals("EmployeeCodeState"))
        {
            return 14;
        }
        if (strFieldName.equals("Operator"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 17;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 19;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EmployeeCode";
                break;
            case 1:
                strFieldName = "ManageCom";
                break;
            case 2:
                strFieldName = "Name";
                break;
            case 3:
                strFieldName = "Birthday";
                break;
            case 4:
                strFieldName = "IDNo";
                break;
            case 5:
                strFieldName = "PolityVisage";
                break;
            case 6:
                strFieldName = "Nationality";
                break;
            case 7:
                strFieldName = "Sex";
                break;
            case 8:
                strFieldName = "RgtAddress";
                break;
            case 9:
                strFieldName = "Degree";
                break;
            case 10:
                strFieldName = "PostTitle";
                break;
            case 11:
                strFieldName = "EmployeeDate";
                break;
            case 12:
                strFieldName = "OutWorkDate";
                break;
            case 13:
                strFieldName = "ManageFlag";
                break;
            case 14:
                strFieldName = "EmployeeCodeState";
                break;
            case 15:
                strFieldName = "Operator";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EmployeeCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Birthday"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("IDNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolityVisage"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Nationality"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Sex"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Degree"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PostTitle"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EmployeeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("OutWorkDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ManageFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EmployeeCodeState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
