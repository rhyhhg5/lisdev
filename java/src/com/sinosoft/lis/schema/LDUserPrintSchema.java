/*
 * <p>ClassName: LDUserPrintSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2004-11-27
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDUserPrintDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDUserPrintSchema implements Schema
{
    // @Field
    /** 用户编码 */
    private String UserCode;
    /** 分公司代码 */
    private String ManageCom;
    /** 机构代码 */
    private String ComCode;
    /** 打印类型 */
    private String PrintType;
    /** 备注 */
    private String ReMark;

    public static final int FIELDNUM = 5; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDUserPrintSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "UserCode";
        pk[1] = "ManageCom";
        pk[2] = "ComCode";
        pk[3] = "PrintType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getUserCode()
    {
        if (UserCode != null && !UserCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            UserCode = StrTool.unicodeToGBK(UserCode);
        }
        return UserCode;
    }

    public void setUserCode(String aUserCode)
    {
        UserCode = aUserCode;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getComCode()
    {
        if (ComCode != null && !ComCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ComCode = StrTool.unicodeToGBK(ComCode);
        }
        return ComCode;
    }

    public void setComCode(String aComCode)
    {
        ComCode = aComCode;
    }

    public String getPrintType()
    {
        if (PrintType != null && !PrintType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PrintType = StrTool.unicodeToGBK(PrintType);
        }
        return PrintType;
    }

    public void setPrintType(String aPrintType)
    {
        PrintType = aPrintType;
    }

    public String getReMark()
    {
        if (ReMark != null && !ReMark.equals("") && SysConst.CHANGECHARSET == true)
        {
            ReMark = StrTool.unicodeToGBK(ReMark);
        }
        return ReMark;
    }

    public void setReMark(String aReMark)
    {
        ReMark = aReMark;
    }

    /**
     * 使用另外一个 LDUserPrintSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDUserPrintSchema aLDUserPrintSchema)
    {
        this.UserCode = aLDUserPrintSchema.getUserCode();
        this.ManageCom = aLDUserPrintSchema.getManageCom();
        this.ComCode = aLDUserPrintSchema.getComCode();
        this.PrintType = aLDUserPrintSchema.getPrintType();
        this.ReMark = aLDUserPrintSchema.getReMark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("UserCode") == null)
            {
                this.UserCode = null;
            }
            else
            {
                this.UserCode = rs.getString("UserCode").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("ComCode") == null)
            {
                this.ComCode = null;
            }
            else
            {
                this.ComCode = rs.getString("ComCode").trim();
            }

            if (rs.getString("PrintType") == null)
            {
                this.PrintType = null;
            }
            else
            {
                this.PrintType = rs.getString("PrintType").trim();
            }

            if (rs.getString("ReMark") == null)
            {
                this.ReMark = null;
            }
            else
            {
                this.ReMark = rs.getString("ReMark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDUserPrintSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDUserPrintSchema getSchema()
    {
        LDUserPrintSchema aLDUserPrintSchema = new LDUserPrintSchema();
        aLDUserPrintSchema.setSchema(this);
        return aLDUserPrintSchema;
    }

    public LDUserPrintDB getDB()
    {
        LDUserPrintDB aDBOper = new LDUserPrintDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDUserPrint描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(UserCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ComCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrintType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ReMark));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDUserPrint>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            UserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            PrintType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            ReMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDUserPrintSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("UserCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UserCode));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("ComCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComCode));
        }
        if (FCode.equals("PrintType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrintType));
        }
        if (FCode.equals("ReMark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ReMark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(UserCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ComCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PrintType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ReMark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("UserCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UserCode = FValue.trim();
            }
            else
            {
                UserCode = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("ComCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCode = FValue.trim();
            }
            else
            {
                ComCode = null;
            }
        }
        if (FCode.equals("PrintType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrintType = FValue.trim();
            }
            else
            {
                PrintType = null;
            }
        }
        if (FCode.equals("ReMark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ReMark = FValue.trim();
            }
            else
            {
                ReMark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDUserPrintSchema other = (LDUserPrintSchema) otherObject;
        return
                UserCode.equals(other.getUserCode())
                && ManageCom.equals(other.getManageCom())
                && ComCode.equals(other.getComCode())
                && PrintType.equals(other.getPrintType())
                && ReMark.equals(other.getReMark());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("UserCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 1;
        }
        if (strFieldName.equals("ComCode"))
        {
            return 2;
        }
        if (strFieldName.equals("PrintType"))
        {
            return 3;
        }
        if (strFieldName.equals("ReMark"))
        {
            return 4;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "UserCode";
                break;
            case 1:
                strFieldName = "ManageCom";
                break;
            case 2:
                strFieldName = "ComCode";
                break;
            case 3:
                strFieldName = "PrintType";
                break;
            case 4:
                strFieldName = "ReMark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("UserCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrintType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReMark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
