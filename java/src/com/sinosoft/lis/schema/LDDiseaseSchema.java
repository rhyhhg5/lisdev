/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDDiseaseDB;

/*
 * <p>ClassName: LDDiseaseSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-06-14
 */
public class LDDiseaseSchema implements Schema, Cloneable
{
	// @Field
	/** Icd疾病代码 */
	private String ICDCode;
	/** 疾病名称 */
	private String ICDName;
	/** 使用频率级别 */
	private int FrequencyFlag;
	/** 慢性病标志 */
	private String ChronicMark;
	/** 风险等级 */
	private String RiskLevel;
	/** 风险存续期 */
	private String RiskContinueFlag;
	/** 肿瘤标志 */
	private String TUMFLAG;

	public static final int FIELDNUM = 7;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDDiseaseSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ICDCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDDiseaseSchema cloned = (LDDiseaseSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getICDCode()
	{
		return ICDCode;
	}
	public void setICDCode(String aICDCode)
	{
		ICDCode = aICDCode;
	}
	public String getICDName()
	{
		return ICDName;
	}
	public void setICDName(String aICDName)
	{
		ICDName = aICDName;
	}
	public int getFrequencyFlag()
	{
		return FrequencyFlag;
	}
	public void setFrequencyFlag(int aFrequencyFlag)
	{
		FrequencyFlag = aFrequencyFlag;
	}
	public void setFrequencyFlag(String aFrequencyFlag)
	{
		if (aFrequencyFlag != null && !aFrequencyFlag.equals(""))
		{
			Integer tInteger = new Integer(aFrequencyFlag);
			int i = tInteger.intValue();
			FrequencyFlag = i;
		}
	}

	public String getChronicMark()
	{
		return ChronicMark;
	}
	public void setChronicMark(String aChronicMark)
	{
		ChronicMark = aChronicMark;
	}
	public String getRiskLevel()
	{
		return RiskLevel;
	}
	public void setRiskLevel(String aRiskLevel)
	{
		RiskLevel = aRiskLevel;
	}
	public String getRiskContinueFlag()
	{
		return RiskContinueFlag;
	}
	public void setRiskContinueFlag(String aRiskContinueFlag)
	{
		RiskContinueFlag = aRiskContinueFlag;
	}
	public String getTUMFLAG()
	{
		return TUMFLAG;
	}
	public void setTUMFLAG(String aTUMFLAG)
	{
		TUMFLAG = aTUMFLAG;
	}

	/**
	* 使用另外一个 LDDiseaseSchema 对象给 Schema 赋值
	* @param: aLDDiseaseSchema LDDiseaseSchema
	**/
	public void setSchema(LDDiseaseSchema aLDDiseaseSchema)
	{
		this.ICDCode = aLDDiseaseSchema.getICDCode();
		this.ICDName = aLDDiseaseSchema.getICDName();
		this.FrequencyFlag = aLDDiseaseSchema.getFrequencyFlag();
		this.ChronicMark = aLDDiseaseSchema.getChronicMark();
		this.RiskLevel = aLDDiseaseSchema.getRiskLevel();
		this.RiskContinueFlag = aLDDiseaseSchema.getRiskContinueFlag();
		this.TUMFLAG = aLDDiseaseSchema.getTUMFLAG();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ICDCode") == null )
				this.ICDCode = null;
			else
				this.ICDCode = rs.getString("ICDCode").trim();

			if( rs.getString("ICDName") == null )
				this.ICDName = null;
			else
				this.ICDName = rs.getString("ICDName").trim();

			this.FrequencyFlag = rs.getInt("FrequencyFlag");
			if( rs.getString("ChronicMark") == null )
				this.ChronicMark = null;
			else
				this.ChronicMark = rs.getString("ChronicMark").trim();

			if( rs.getString("RiskLevel") == null )
				this.RiskLevel = null;
			else
				this.RiskLevel = rs.getString("RiskLevel").trim();

			if( rs.getString("RiskContinueFlag") == null )
				this.RiskContinueFlag = null;
			else
				this.RiskContinueFlag = rs.getString("RiskContinueFlag").trim();

			if( rs.getString("TUMFLAG") == null )
				this.TUMFLAG = null;
			else
				this.TUMFLAG = rs.getString("TUMFLAG").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDDisease表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDDiseaseSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDDiseaseSchema getSchema()
	{
		LDDiseaseSchema aLDDiseaseSchema = new LDDiseaseSchema();
		aLDDiseaseSchema.setSchema(this);
		return aLDDiseaseSchema;
	}

	public LDDiseaseDB getDB()
	{
		LDDiseaseDB aDBOper = new LDDiseaseDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDisease描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ICDCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ICDName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(FrequencyFlag));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChronicMark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskLevel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskContinueFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TUMFLAG));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDisease>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ICDCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ICDName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			FrequencyFlag= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).intValue();
			ChronicMark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RiskLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RiskContinueFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			TUMFLAG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDDiseaseSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ICDCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ICDCode));
		}
		if (FCode.equals("ICDName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ICDName));
		}
		if (FCode.equals("FrequencyFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FrequencyFlag));
		}
		if (FCode.equals("ChronicMark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChronicMark));
		}
		if (FCode.equals("RiskLevel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskLevel));
		}
		if (FCode.equals("RiskContinueFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskContinueFlag));
		}
		if (FCode.equals("TUMFLAG"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TUMFLAG));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ICDCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ICDName);
				break;
			case 2:
				strFieldValue = String.valueOf(FrequencyFlag);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ChronicMark);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(RiskLevel);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RiskContinueFlag);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(TUMFLAG);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ICDCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ICDCode = FValue.trim();
			}
			else
				ICDCode = null;
		}
		if (FCode.equalsIgnoreCase("ICDName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ICDName = FValue.trim();
			}
			else
				ICDName = null;
		}
		if (FCode.equalsIgnoreCase("FrequencyFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				FrequencyFlag = i;
			}
		}
		if (FCode.equalsIgnoreCase("ChronicMark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChronicMark = FValue.trim();
			}
			else
				ChronicMark = null;
		}
		if (FCode.equalsIgnoreCase("RiskLevel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskLevel = FValue.trim();
			}
			else
				RiskLevel = null;
		}
		if (FCode.equalsIgnoreCase("RiskContinueFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskContinueFlag = FValue.trim();
			}
			else
				RiskContinueFlag = null;
		}
		if (FCode.equalsIgnoreCase("TUMFLAG"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TUMFLAG = FValue.trim();
			}
			else
				TUMFLAG = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDDiseaseSchema other = (LDDiseaseSchema)otherObject;
		return
			(ICDCode == null ? other.getICDCode() == null : ICDCode.equals(other.getICDCode()))
			&& (ICDName == null ? other.getICDName() == null : ICDName.equals(other.getICDName()))
			&& FrequencyFlag == other.getFrequencyFlag()
			&& (ChronicMark == null ? other.getChronicMark() == null : ChronicMark.equals(other.getChronicMark()))
			&& (RiskLevel == null ? other.getRiskLevel() == null : RiskLevel.equals(other.getRiskLevel()))
			&& (RiskContinueFlag == null ? other.getRiskContinueFlag() == null : RiskContinueFlag.equals(other.getRiskContinueFlag()))
			&& (TUMFLAG == null ? other.getTUMFLAG() == null : TUMFLAG.equals(other.getTUMFLAG()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ICDCode") ) {
			return 0;
		}
		if( strFieldName.equals("ICDName") ) {
			return 1;
		}
		if( strFieldName.equals("FrequencyFlag") ) {
			return 2;
		}
		if( strFieldName.equals("ChronicMark") ) {
			return 3;
		}
		if( strFieldName.equals("RiskLevel") ) {
			return 4;
		}
		if( strFieldName.equals("RiskContinueFlag") ) {
			return 5;
		}
		if( strFieldName.equals("TUMFLAG") ) {
			return 6;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ICDCode";
				break;
			case 1:
				strFieldName = "ICDName";
				break;
			case 2:
				strFieldName = "FrequencyFlag";
				break;
			case 3:
				strFieldName = "ChronicMark";
				break;
			case 4:
				strFieldName = "RiskLevel";
				break;
			case 5:
				strFieldName = "RiskContinueFlag";
				break;
			case 6:
				strFieldName = "TUMFLAG";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ICDCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ICDName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FrequencyFlag") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ChronicMark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskLevel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskContinueFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TUMFLAG") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_INT;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
