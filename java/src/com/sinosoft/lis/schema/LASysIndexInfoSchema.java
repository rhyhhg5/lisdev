/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LASysIndexInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LASysIndexInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-29
 */
public class LASysIndexInfoSchema implements Schema
{
    // @Field
    /** 指标计算编码 */
    private String IndexCalNo;
    /** 指标类型 */
    private String IndexType;
    /** 系统指标编码 */
    private String SysIndexCode;
    /** 管理机构 */
    private String ManageCom;
    /** 统计起期 */
    private Date StartDate;
    /** 统计止期 */
    private Date StartEnd;
    /** 展业机构编码 */
    private String BranchAttr;
    /** 代理人级别 */
    private String AgentGrade;
    /** 其他1 */
    private double T1;
    /** 其他2 */
    private double T2;
    /** 其他3 */
    private double T3;
    /** 其他4 */
    private double T4;
    /** 其他5 */
    private double T5;
    /** 其他6 */
    private double T6;
    /** 其他7 */
    private double T7;
    /** 其他8 */
    private double T8;
    /** 其他9 */
    private double T9;
    /** 其他10 */
    private double T10;
    /** 其他11 */
    private double T11;
    /** 其他12 */
    private double T12;
    /** 其他13 */
    private double T13;
    /** 其他14 */
    private double T14;
    /** 其他15 */
    private double T15;
    /** 其他16 */
    private double T16;
    /** 其他17 */
    private double T17;
    /** 其他18 */
    private double T18;
    /** 其他19 */
    private double T19;
    /** 其他20 */
    private double T20;
    /** 其他21 */
    private double T21;
    /** 其他22 */
    private double T22;
    /** 其他23 */
    private double T23;
    /** 其他24 */
    private double T24;
    /** 其他25 */
    private double T25;
    /** 其他26 */
    private double T26;
    /** 其他27 */
    private double T27;
    /** 其他28 */
    private double T28;
    /** 其他29 */
    private double T29;
    /** 其他30 */
    private double T30;
    /** 其他31 */
    private double T31;
    /** 其他32 */
    private double T32;
    /** 其他33 */
    private double T33;
    /** 其他34 */
    private double T34;
    /** 其他35 */
    private double T35;
    /** 其他36 */
    private double T36;
    /** 其他37 */
    private double T37;
    /** 其他38 */
    private double T38;
    /** 其他39 */
    private double T39;
    /** 其他40 */
    private double T40;
    /** 其他41 */
    private double T41;
    /** 其他42 */
    private double T42;
    /** 其他43 */
    private double T43;
    /** 其他44 */
    private double T44;
    /** 其他45 */
    private double T45;
    /** 其他46 */
    private double T46;
    /** 其他47 */
    private double T47;
    /** 其他48 */
    private double T48;
    /** 其他49 */
    private double T49;
    /** 其他50 */
    private double T50;
    /** 状态 */
    private String State;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 64; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LASysIndexInfoSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "IndexCalNo";
        pk[1] = "SysIndexCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getIndexCalNo()
    {
        if (SysConst.CHANGECHARSET && IndexCalNo != null &&
            !IndexCalNo.equals(""))
        {
            IndexCalNo = StrTool.unicodeToGBK(IndexCalNo);
        }
        return IndexCalNo;
    }

    public void setIndexCalNo(String aIndexCalNo)
    {
        IndexCalNo = aIndexCalNo;
    }

    public String getIndexType()
    {
        if (SysConst.CHANGECHARSET && IndexType != null && !IndexType.equals(""))
        {
            IndexType = StrTool.unicodeToGBK(IndexType);
        }
        return IndexType;
    }

    public void setIndexType(String aIndexType)
    {
        IndexType = aIndexType;
    }

    public String getSysIndexCode()
    {
        if (SysConst.CHANGECHARSET && SysIndexCode != null &&
            !SysIndexCode.equals(""))
        {
            SysIndexCode = StrTool.unicodeToGBK(SysIndexCode);
        }
        return SysIndexCode;
    }

    public void setSysIndexCode(String aSysIndexCode)
    {
        SysIndexCode = aSysIndexCode;
    }

    public String getManageCom()
    {
        if (SysConst.CHANGECHARSET && ManageCom != null && !ManageCom.equals(""))
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getStartDate()
    {
        if (StartDate != null)
        {
            return fDate.getString(StartDate);
        }
        else
        {
            return null;
        }
    }

    public void setStartDate(Date aStartDate)
    {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate)
    {
        if (aStartDate != null && !aStartDate.equals(""))
        {
            StartDate = fDate.getDate(aStartDate);
        }
        else
        {
            StartDate = null;
        }
    }

    public String getStartEnd()
    {
        if (StartEnd != null)
        {
            return fDate.getString(StartEnd);
        }
        else
        {
            return null;
        }
    }

    public void setStartEnd(Date aStartEnd)
    {
        StartEnd = aStartEnd;
    }

    public void setStartEnd(String aStartEnd)
    {
        if (aStartEnd != null && !aStartEnd.equals(""))
        {
            StartEnd = fDate.getDate(aStartEnd);
        }
        else
        {
            StartEnd = null;
        }
    }

    public String getBranchAttr()
    {
        if (SysConst.CHANGECHARSET && BranchAttr != null &&
            !BranchAttr.equals(""))
        {
            BranchAttr = StrTool.unicodeToGBK(BranchAttr);
        }
        return BranchAttr;
    }

    public void setBranchAttr(String aBranchAttr)
    {
        BranchAttr = aBranchAttr;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public double getT1()
    {
        return T1;
    }

    public void setT1(double aT1)
    {
        T1 = aT1;
    }

    public void setT1(String aT1)
    {
        if (aT1 != null && !aT1.equals(""))
        {
            Double tDouble = new Double(aT1);
            double d = tDouble.doubleValue();
            T1 = d;
        }
    }

    public double getT2()
    {
        return T2;
    }

    public void setT2(double aT2)
    {
        T2 = aT2;
    }

    public void setT2(String aT2)
    {
        if (aT2 != null && !aT2.equals(""))
        {
            Double tDouble = new Double(aT2);
            double d = tDouble.doubleValue();
            T2 = d;
        }
    }

    public double getT3()
    {
        return T3;
    }

    public void setT3(double aT3)
    {
        T3 = aT3;
    }

    public void setT3(String aT3)
    {
        if (aT3 != null && !aT3.equals(""))
        {
            Double tDouble = new Double(aT3);
            double d = tDouble.doubleValue();
            T3 = d;
        }
    }

    public double getT4()
    {
        return T4;
    }

    public void setT4(double aT4)
    {
        T4 = aT4;
    }

    public void setT4(String aT4)
    {
        if (aT4 != null && !aT4.equals(""))
        {
            Double tDouble = new Double(aT4);
            double d = tDouble.doubleValue();
            T4 = d;
        }
    }

    public double getT5()
    {
        return T5;
    }

    public void setT5(double aT5)
    {
        T5 = aT5;
    }

    public void setT5(String aT5)
    {
        if (aT5 != null && !aT5.equals(""))
        {
            Double tDouble = new Double(aT5);
            double d = tDouble.doubleValue();
            T5 = d;
        }
    }

    public double getT6()
    {
        return T6;
    }

    public void setT6(double aT6)
    {
        T6 = aT6;
    }

    public void setT6(String aT6)
    {
        if (aT6 != null && !aT6.equals(""))
        {
            Double tDouble = new Double(aT6);
            double d = tDouble.doubleValue();
            T6 = d;
        }
    }

    public double getT7()
    {
        return T7;
    }

    public void setT7(double aT7)
    {
        T7 = aT7;
    }

    public void setT7(String aT7)
    {
        if (aT7 != null && !aT7.equals(""))
        {
            Double tDouble = new Double(aT7);
            double d = tDouble.doubleValue();
            T7 = d;
        }
    }

    public double getT8()
    {
        return T8;
    }

    public void setT8(double aT8)
    {
        T8 = aT8;
    }

    public void setT8(String aT8)
    {
        if (aT8 != null && !aT8.equals(""))
        {
            Double tDouble = new Double(aT8);
            double d = tDouble.doubleValue();
            T8 = d;
        }
    }

    public double getT9()
    {
        return T9;
    }

    public void setT9(double aT9)
    {
        T9 = aT9;
    }

    public void setT9(String aT9)
    {
        if (aT9 != null && !aT9.equals(""))
        {
            Double tDouble = new Double(aT9);
            double d = tDouble.doubleValue();
            T9 = d;
        }
    }

    public double getT10()
    {
        return T10;
    }

    public void setT10(double aT10)
    {
        T10 = aT10;
    }

    public void setT10(String aT10)
    {
        if (aT10 != null && !aT10.equals(""))
        {
            Double tDouble = new Double(aT10);
            double d = tDouble.doubleValue();
            T10 = d;
        }
    }

    public double getT11()
    {
        return T11;
    }

    public void setT11(double aT11)
    {
        T11 = aT11;
    }

    public void setT11(String aT11)
    {
        if (aT11 != null && !aT11.equals(""))
        {
            Double tDouble = new Double(aT11);
            double d = tDouble.doubleValue();
            T11 = d;
        }
    }

    public double getT12()
    {
        return T12;
    }

    public void setT12(double aT12)
    {
        T12 = aT12;
    }

    public void setT12(String aT12)
    {
        if (aT12 != null && !aT12.equals(""))
        {
            Double tDouble = new Double(aT12);
            double d = tDouble.doubleValue();
            T12 = d;
        }
    }

    public double getT13()
    {
        return T13;
    }

    public void setT13(double aT13)
    {
        T13 = aT13;
    }

    public void setT13(String aT13)
    {
        if (aT13 != null && !aT13.equals(""))
        {
            Double tDouble = new Double(aT13);
            double d = tDouble.doubleValue();
            T13 = d;
        }
    }

    public double getT14()
    {
        return T14;
    }

    public void setT14(double aT14)
    {
        T14 = aT14;
    }

    public void setT14(String aT14)
    {
        if (aT14 != null && !aT14.equals(""))
        {
            Double tDouble = new Double(aT14);
            double d = tDouble.doubleValue();
            T14 = d;
        }
    }

    public double getT15()
    {
        return T15;
    }

    public void setT15(double aT15)
    {
        T15 = aT15;
    }

    public void setT15(String aT15)
    {
        if (aT15 != null && !aT15.equals(""))
        {
            Double tDouble = new Double(aT15);
            double d = tDouble.doubleValue();
            T15 = d;
        }
    }

    public double getT16()
    {
        return T16;
    }

    public void setT16(double aT16)
    {
        T16 = aT16;
    }

    public void setT16(String aT16)
    {
        if (aT16 != null && !aT16.equals(""))
        {
            Double tDouble = new Double(aT16);
            double d = tDouble.doubleValue();
            T16 = d;
        }
    }

    public double getT17()
    {
        return T17;
    }

    public void setT17(double aT17)
    {
        T17 = aT17;
    }

    public void setT17(String aT17)
    {
        if (aT17 != null && !aT17.equals(""))
        {
            Double tDouble = new Double(aT17);
            double d = tDouble.doubleValue();
            T17 = d;
        }
    }

    public double getT18()
    {
        return T18;
    }

    public void setT18(double aT18)
    {
        T18 = aT18;
    }

    public void setT18(String aT18)
    {
        if (aT18 != null && !aT18.equals(""))
        {
            Double tDouble = new Double(aT18);
            double d = tDouble.doubleValue();
            T18 = d;
        }
    }

    public double getT19()
    {
        return T19;
    }

    public void setT19(double aT19)
    {
        T19 = aT19;
    }

    public void setT19(String aT19)
    {
        if (aT19 != null && !aT19.equals(""))
        {
            Double tDouble = new Double(aT19);
            double d = tDouble.doubleValue();
            T19 = d;
        }
    }

    public double getT20()
    {
        return T20;
    }

    public void setT20(double aT20)
    {
        T20 = aT20;
    }

    public void setT20(String aT20)
    {
        if (aT20 != null && !aT20.equals(""))
        {
            Double tDouble = new Double(aT20);
            double d = tDouble.doubleValue();
            T20 = d;
        }
    }

    public double getT21()
    {
        return T21;
    }

    public void setT21(double aT21)
    {
        T21 = aT21;
    }

    public void setT21(String aT21)
    {
        if (aT21 != null && !aT21.equals(""))
        {
            Double tDouble = new Double(aT21);
            double d = tDouble.doubleValue();
            T21 = d;
        }
    }

    public double getT22()
    {
        return T22;
    }

    public void setT22(double aT22)
    {
        T22 = aT22;
    }

    public void setT22(String aT22)
    {
        if (aT22 != null && !aT22.equals(""))
        {
            Double tDouble = new Double(aT22);
            double d = tDouble.doubleValue();
            T22 = d;
        }
    }

    public double getT23()
    {
        return T23;
    }

    public void setT23(double aT23)
    {
        T23 = aT23;
    }

    public void setT23(String aT23)
    {
        if (aT23 != null && !aT23.equals(""))
        {
            Double tDouble = new Double(aT23);
            double d = tDouble.doubleValue();
            T23 = d;
        }
    }

    public double getT24()
    {
        return T24;
    }

    public void setT24(double aT24)
    {
        T24 = aT24;
    }

    public void setT24(String aT24)
    {
        if (aT24 != null && !aT24.equals(""))
        {
            Double tDouble = new Double(aT24);
            double d = tDouble.doubleValue();
            T24 = d;
        }
    }

    public double getT25()
    {
        return T25;
    }

    public void setT25(double aT25)
    {
        T25 = aT25;
    }

    public void setT25(String aT25)
    {
        if (aT25 != null && !aT25.equals(""))
        {
            Double tDouble = new Double(aT25);
            double d = tDouble.doubleValue();
            T25 = d;
        }
    }

    public double getT26()
    {
        return T26;
    }

    public void setT26(double aT26)
    {
        T26 = aT26;
    }

    public void setT26(String aT26)
    {
        if (aT26 != null && !aT26.equals(""))
        {
            Double tDouble = new Double(aT26);
            double d = tDouble.doubleValue();
            T26 = d;
        }
    }

    public double getT27()
    {
        return T27;
    }

    public void setT27(double aT27)
    {
        T27 = aT27;
    }

    public void setT27(String aT27)
    {
        if (aT27 != null && !aT27.equals(""))
        {
            Double tDouble = new Double(aT27);
            double d = tDouble.doubleValue();
            T27 = d;
        }
    }

    public double getT28()
    {
        return T28;
    }

    public void setT28(double aT28)
    {
        T28 = aT28;
    }

    public void setT28(String aT28)
    {
        if (aT28 != null && !aT28.equals(""))
        {
            Double tDouble = new Double(aT28);
            double d = tDouble.doubleValue();
            T28 = d;
        }
    }

    public double getT29()
    {
        return T29;
    }

    public void setT29(double aT29)
    {
        T29 = aT29;
    }

    public void setT29(String aT29)
    {
        if (aT29 != null && !aT29.equals(""))
        {
            Double tDouble = new Double(aT29);
            double d = tDouble.doubleValue();
            T29 = d;
        }
    }

    public double getT30()
    {
        return T30;
    }

    public void setT30(double aT30)
    {
        T30 = aT30;
    }

    public void setT30(String aT30)
    {
        if (aT30 != null && !aT30.equals(""))
        {
            Double tDouble = new Double(aT30);
            double d = tDouble.doubleValue();
            T30 = d;
        }
    }

    public double getT31()
    {
        return T31;
    }

    public void setT31(double aT31)
    {
        T31 = aT31;
    }

    public void setT31(String aT31)
    {
        if (aT31 != null && !aT31.equals(""))
        {
            Double tDouble = new Double(aT31);
            double d = tDouble.doubleValue();
            T31 = d;
        }
    }

    public double getT32()
    {
        return T32;
    }

    public void setT32(double aT32)
    {
        T32 = aT32;
    }

    public void setT32(String aT32)
    {
        if (aT32 != null && !aT32.equals(""))
        {
            Double tDouble = new Double(aT32);
            double d = tDouble.doubleValue();
            T32 = d;
        }
    }

    public double getT33()
    {
        return T33;
    }

    public void setT33(double aT33)
    {
        T33 = aT33;
    }

    public void setT33(String aT33)
    {
        if (aT33 != null && !aT33.equals(""))
        {
            Double tDouble = new Double(aT33);
            double d = tDouble.doubleValue();
            T33 = d;
        }
    }

    public double getT34()
    {
        return T34;
    }

    public void setT34(double aT34)
    {
        T34 = aT34;
    }

    public void setT34(String aT34)
    {
        if (aT34 != null && !aT34.equals(""))
        {
            Double tDouble = new Double(aT34);
            double d = tDouble.doubleValue();
            T34 = d;
        }
    }

    public double getT35()
    {
        return T35;
    }

    public void setT35(double aT35)
    {
        T35 = aT35;
    }

    public void setT35(String aT35)
    {
        if (aT35 != null && !aT35.equals(""))
        {
            Double tDouble = new Double(aT35);
            double d = tDouble.doubleValue();
            T35 = d;
        }
    }

    public double getT36()
    {
        return T36;
    }

    public void setT36(double aT36)
    {
        T36 = aT36;
    }

    public void setT36(String aT36)
    {
        if (aT36 != null && !aT36.equals(""))
        {
            Double tDouble = new Double(aT36);
            double d = tDouble.doubleValue();
            T36 = d;
        }
    }

    public double getT37()
    {
        return T37;
    }

    public void setT37(double aT37)
    {
        T37 = aT37;
    }

    public void setT37(String aT37)
    {
        if (aT37 != null && !aT37.equals(""))
        {
            Double tDouble = new Double(aT37);
            double d = tDouble.doubleValue();
            T37 = d;
        }
    }

    public double getT38()
    {
        return T38;
    }

    public void setT38(double aT38)
    {
        T38 = aT38;
    }

    public void setT38(String aT38)
    {
        if (aT38 != null && !aT38.equals(""))
        {
            Double tDouble = new Double(aT38);
            double d = tDouble.doubleValue();
            T38 = d;
        }
    }

    public double getT39()
    {
        return T39;
    }

    public void setT39(double aT39)
    {
        T39 = aT39;
    }

    public void setT39(String aT39)
    {
        if (aT39 != null && !aT39.equals(""))
        {
            Double tDouble = new Double(aT39);
            double d = tDouble.doubleValue();
            T39 = d;
        }
    }

    public double getT40()
    {
        return T40;
    }

    public void setT40(double aT40)
    {
        T40 = aT40;
    }

    public void setT40(String aT40)
    {
        if (aT40 != null && !aT40.equals(""))
        {
            Double tDouble = new Double(aT40);
            double d = tDouble.doubleValue();
            T40 = d;
        }
    }

    public double getT41()
    {
        return T41;
    }

    public void setT41(double aT41)
    {
        T41 = aT41;
    }

    public void setT41(String aT41)
    {
        if (aT41 != null && !aT41.equals(""))
        {
            Double tDouble = new Double(aT41);
            double d = tDouble.doubleValue();
            T41 = d;
        }
    }

    public double getT42()
    {
        return T42;
    }

    public void setT42(double aT42)
    {
        T42 = aT42;
    }

    public void setT42(String aT42)
    {
        if (aT42 != null && !aT42.equals(""))
        {
            Double tDouble = new Double(aT42);
            double d = tDouble.doubleValue();
            T42 = d;
        }
    }

    public double getT43()
    {
        return T43;
    }

    public void setT43(double aT43)
    {
        T43 = aT43;
    }

    public void setT43(String aT43)
    {
        if (aT43 != null && !aT43.equals(""))
        {
            Double tDouble = new Double(aT43);
            double d = tDouble.doubleValue();
            T43 = d;
        }
    }

    public double getT44()
    {
        return T44;
    }

    public void setT44(double aT44)
    {
        T44 = aT44;
    }

    public void setT44(String aT44)
    {
        if (aT44 != null && !aT44.equals(""))
        {
            Double tDouble = new Double(aT44);
            double d = tDouble.doubleValue();
            T44 = d;
        }
    }

    public double getT45()
    {
        return T45;
    }

    public void setT45(double aT45)
    {
        T45 = aT45;
    }

    public void setT45(String aT45)
    {
        if (aT45 != null && !aT45.equals(""))
        {
            Double tDouble = new Double(aT45);
            double d = tDouble.doubleValue();
            T45 = d;
        }
    }

    public double getT46()
    {
        return T46;
    }

    public void setT46(double aT46)
    {
        T46 = aT46;
    }

    public void setT46(String aT46)
    {
        if (aT46 != null && !aT46.equals(""))
        {
            Double tDouble = new Double(aT46);
            double d = tDouble.doubleValue();
            T46 = d;
        }
    }

    public double getT47()
    {
        return T47;
    }

    public void setT47(double aT47)
    {
        T47 = aT47;
    }

    public void setT47(String aT47)
    {
        if (aT47 != null && !aT47.equals(""))
        {
            Double tDouble = new Double(aT47);
            double d = tDouble.doubleValue();
            T47 = d;
        }
    }

    public double getT48()
    {
        return T48;
    }

    public void setT48(double aT48)
    {
        T48 = aT48;
    }

    public void setT48(String aT48)
    {
        if (aT48 != null && !aT48.equals(""))
        {
            Double tDouble = new Double(aT48);
            double d = tDouble.doubleValue();
            T48 = d;
        }
    }

    public double getT49()
    {
        return T49;
    }

    public void setT49(double aT49)
    {
        T49 = aT49;
    }

    public void setT49(String aT49)
    {
        if (aT49 != null && !aT49.equals(""))
        {
            Double tDouble = new Double(aT49);
            double d = tDouble.doubleValue();
            T49 = d;
        }
    }

    public double getT50()
    {
        return T50;
    }

    public void setT50(double aT50)
    {
        T50 = aT50;
    }

    public void setT50(String aT50)
    {
        if (aT50 != null && !aT50.equals(""))
        {
            Double tDouble = new Double(aT50);
            double d = tDouble.doubleValue();
            T50 = d;
        }
    }

    public String getState()
    {
        if (SysConst.CHANGECHARSET && State != null && !State.equals(""))
        {
            State = StrTool.unicodeToGBK(State);
        }
        return State;
    }

    public void setState(String aState)
    {
        State = aState;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LASysIndexInfoSchema 对象给 Schema 赋值
     * @param: aLASysIndexInfoSchema LASysIndexInfoSchema
     **/
    public void setSchema(LASysIndexInfoSchema aLASysIndexInfoSchema)
    {
        this.IndexCalNo = aLASysIndexInfoSchema.getIndexCalNo();
        this.IndexType = aLASysIndexInfoSchema.getIndexType();
        this.SysIndexCode = aLASysIndexInfoSchema.getSysIndexCode();
        this.ManageCom = aLASysIndexInfoSchema.getManageCom();
        this.StartDate = fDate.getDate(aLASysIndexInfoSchema.getStartDate());
        this.StartEnd = fDate.getDate(aLASysIndexInfoSchema.getStartEnd());
        this.BranchAttr = aLASysIndexInfoSchema.getBranchAttr();
        this.AgentGrade = aLASysIndexInfoSchema.getAgentGrade();
        this.T1 = aLASysIndexInfoSchema.getT1();
        this.T2 = aLASysIndexInfoSchema.getT2();
        this.T3 = aLASysIndexInfoSchema.getT3();
        this.T4 = aLASysIndexInfoSchema.getT4();
        this.T5 = aLASysIndexInfoSchema.getT5();
        this.T6 = aLASysIndexInfoSchema.getT6();
        this.T7 = aLASysIndexInfoSchema.getT7();
        this.T8 = aLASysIndexInfoSchema.getT8();
        this.T9 = aLASysIndexInfoSchema.getT9();
        this.T10 = aLASysIndexInfoSchema.getT10();
        this.T11 = aLASysIndexInfoSchema.getT11();
        this.T12 = aLASysIndexInfoSchema.getT12();
        this.T13 = aLASysIndexInfoSchema.getT13();
        this.T14 = aLASysIndexInfoSchema.getT14();
        this.T15 = aLASysIndexInfoSchema.getT15();
        this.T16 = aLASysIndexInfoSchema.getT16();
        this.T17 = aLASysIndexInfoSchema.getT17();
        this.T18 = aLASysIndexInfoSchema.getT18();
        this.T19 = aLASysIndexInfoSchema.getT19();
        this.T20 = aLASysIndexInfoSchema.getT20();
        this.T21 = aLASysIndexInfoSchema.getT21();
        this.T22 = aLASysIndexInfoSchema.getT22();
        this.T23 = aLASysIndexInfoSchema.getT23();
        this.T24 = aLASysIndexInfoSchema.getT24();
        this.T25 = aLASysIndexInfoSchema.getT25();
        this.T26 = aLASysIndexInfoSchema.getT26();
        this.T27 = aLASysIndexInfoSchema.getT27();
        this.T28 = aLASysIndexInfoSchema.getT28();
        this.T29 = aLASysIndexInfoSchema.getT29();
        this.T30 = aLASysIndexInfoSchema.getT30();
        this.T31 = aLASysIndexInfoSchema.getT31();
        this.T32 = aLASysIndexInfoSchema.getT32();
        this.T33 = aLASysIndexInfoSchema.getT33();
        this.T34 = aLASysIndexInfoSchema.getT34();
        this.T35 = aLASysIndexInfoSchema.getT35();
        this.T36 = aLASysIndexInfoSchema.getT36();
        this.T37 = aLASysIndexInfoSchema.getT37();
        this.T38 = aLASysIndexInfoSchema.getT38();
        this.T39 = aLASysIndexInfoSchema.getT39();
        this.T40 = aLASysIndexInfoSchema.getT40();
        this.T41 = aLASysIndexInfoSchema.getT41();
        this.T42 = aLASysIndexInfoSchema.getT42();
        this.T43 = aLASysIndexInfoSchema.getT43();
        this.T44 = aLASysIndexInfoSchema.getT44();
        this.T45 = aLASysIndexInfoSchema.getT45();
        this.T46 = aLASysIndexInfoSchema.getT46();
        this.T47 = aLASysIndexInfoSchema.getT47();
        this.T48 = aLASysIndexInfoSchema.getT48();
        this.T49 = aLASysIndexInfoSchema.getT49();
        this.T50 = aLASysIndexInfoSchema.getT50();
        this.State = aLASysIndexInfoSchema.getState();
        this.Operator = aLASysIndexInfoSchema.getOperator();
        this.MakeDate = fDate.getDate(aLASysIndexInfoSchema.getMakeDate());
        this.MakeTime = aLASysIndexInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLASysIndexInfoSchema.getModifyDate());
        this.ModifyTime = aLASysIndexInfoSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("IndexCalNo") == null)
            {
                this.IndexCalNo = null;
            }
            else
            {
                this.IndexCalNo = rs.getString("IndexCalNo").trim();
            }

            if (rs.getString("IndexType") == null)
            {
                this.IndexType = null;
            }
            else
            {
                this.IndexType = rs.getString("IndexType").trim();
            }

            if (rs.getString("SysIndexCode") == null)
            {
                this.SysIndexCode = null;
            }
            else
            {
                this.SysIndexCode = rs.getString("SysIndexCode").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.StartDate = rs.getDate("StartDate");
            this.StartEnd = rs.getDate("StartEnd");
            if (rs.getString("BranchAttr") == null)
            {
                this.BranchAttr = null;
            }
            else
            {
                this.BranchAttr = rs.getString("BranchAttr").trim();
            }

            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            this.T1 = rs.getDouble("T1");
            this.T2 = rs.getDouble("T2");
            this.T3 = rs.getDouble("T3");
            this.T4 = rs.getDouble("T4");
            this.T5 = rs.getDouble("T5");
            this.T6 = rs.getDouble("T6");
            this.T7 = rs.getDouble("T7");
            this.T8 = rs.getDouble("T8");
            this.T9 = rs.getDouble("T9");
            this.T10 = rs.getDouble("T10");
            this.T11 = rs.getDouble("T11");
            this.T12 = rs.getDouble("T12");
            this.T13 = rs.getDouble("T13");
            this.T14 = rs.getDouble("T14");
            this.T15 = rs.getDouble("T15");
            this.T16 = rs.getDouble("T16");
            this.T17 = rs.getDouble("T17");
            this.T18 = rs.getDouble("T18");
            this.T19 = rs.getDouble("T19");
            this.T20 = rs.getDouble("T20");
            this.T21 = rs.getDouble("T21");
            this.T22 = rs.getDouble("T22");
            this.T23 = rs.getDouble("T23");
            this.T24 = rs.getDouble("T24");
            this.T25 = rs.getDouble("T25");
            this.T26 = rs.getDouble("T26");
            this.T27 = rs.getDouble("T27");
            this.T28 = rs.getDouble("T28");
            this.T29 = rs.getDouble("T29");
            this.T30 = rs.getDouble("T30");
            this.T31 = rs.getDouble("T31");
            this.T32 = rs.getDouble("T32");
            this.T33 = rs.getDouble("T33");
            this.T34 = rs.getDouble("T34");
            this.T35 = rs.getDouble("T35");
            this.T36 = rs.getDouble("T36");
            this.T37 = rs.getDouble("T37");
            this.T38 = rs.getDouble("T38");
            this.T39 = rs.getDouble("T39");
            this.T40 = rs.getDouble("T40");
            this.T41 = rs.getDouble("T41");
            this.T42 = rs.getDouble("T42");
            this.T43 = rs.getDouble("T43");
            this.T44 = rs.getDouble("T44");
            this.T45 = rs.getDouble("T45");
            this.T46 = rs.getDouble("T46");
            this.T47 = rs.getDouble("T47");
            this.T48 = rs.getDouble("T48");
            this.T49 = rs.getDouble("T49");
            this.T50 = rs.getDouble("T50");
            if (rs.getString("State") == null)
            {
                this.State = null;
            }
            else
            {
                this.State = rs.getString("State").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASysIndexInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LASysIndexInfoSchema getSchema()
    {
        LASysIndexInfoSchema aLASysIndexInfoSchema = new LASysIndexInfoSchema();
        aLASysIndexInfoSchema.setSchema(this);
        return aLASysIndexInfoSchema;
    }

    public LASysIndexInfoDB getDB()
    {
        LASysIndexInfoDB aDBOper = new LASysIndexInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLASysIndexInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(IndexCalNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IndexType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SysIndexCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(StartDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(StartEnd))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BranchAttr)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(T1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T2) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T3) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T4) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T5) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T6) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T7) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T8) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T9) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T10) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T11) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T12) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T13) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T14) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T15) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T16) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T17) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T18) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T19) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T20) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T21) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T22) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T23) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T24) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T25) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T26) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T27) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T28) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T29) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T30) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T31) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T32) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T33) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T34) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T35) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T36) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T37) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T38) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T39) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T40) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T41) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T42) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T43) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T44) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T45) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T46) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T47) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T48) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T49) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(T50) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(State)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLASysIndexInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            IndexCalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            IndexType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            SysIndexCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            StartEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            T1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage, 9,
                    SysConst.PACKAGESPILTER))).doubleValue();
            T2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage, 10,
                    SysConst.PACKAGESPILTER))).doubleValue();
            T3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage, 11,
                    SysConst.PACKAGESPILTER))).doubleValue();
            T4 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage, 12,
                    SysConst.PACKAGESPILTER))).doubleValue();
            T5 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage, 13,
                    SysConst.PACKAGESPILTER))).doubleValue();
            T6 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage, 14,
                    SysConst.PACKAGESPILTER))).doubleValue();
            T7 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage, 15,
                    SysConst.PACKAGESPILTER))).doubleValue();
            T8 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage, 16,
                    SysConst.PACKAGESPILTER))).doubleValue();
            T9 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage, 17,
                    SysConst.PACKAGESPILTER))).doubleValue();
            T10 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    18, SysConst.PACKAGESPILTER))).doubleValue();
            T11 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    19, SysConst.PACKAGESPILTER))).doubleValue();
            T12 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    20, SysConst.PACKAGESPILTER))).doubleValue();
            T13 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    21, SysConst.PACKAGESPILTER))).doubleValue();
            T14 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    22, SysConst.PACKAGESPILTER))).doubleValue();
            T15 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    23, SysConst.PACKAGESPILTER))).doubleValue();
            T16 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    24, SysConst.PACKAGESPILTER))).doubleValue();
            T17 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    25, SysConst.PACKAGESPILTER))).doubleValue();
            T18 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    26, SysConst.PACKAGESPILTER))).doubleValue();
            T19 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    27, SysConst.PACKAGESPILTER))).doubleValue();
            T20 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    28, SysConst.PACKAGESPILTER))).doubleValue();
            T21 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    29, SysConst.PACKAGESPILTER))).doubleValue();
            T22 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    30, SysConst.PACKAGESPILTER))).doubleValue();
            T23 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    31, SysConst.PACKAGESPILTER))).doubleValue();
            T24 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    32, SysConst.PACKAGESPILTER))).doubleValue();
            T25 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    33, SysConst.PACKAGESPILTER))).doubleValue();
            T26 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    34, SysConst.PACKAGESPILTER))).doubleValue();
            T27 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    35, SysConst.PACKAGESPILTER))).doubleValue();
            T28 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    36, SysConst.PACKAGESPILTER))).doubleValue();
            T29 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    37, SysConst.PACKAGESPILTER))).doubleValue();
            T30 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    38, SysConst.PACKAGESPILTER))).doubleValue();
            T31 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    39, SysConst.PACKAGESPILTER))).doubleValue();
            T32 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    40, SysConst.PACKAGESPILTER))).doubleValue();
            T33 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    41, SysConst.PACKAGESPILTER))).doubleValue();
            T34 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    42, SysConst.PACKAGESPILTER))).doubleValue();
            T35 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    43, SysConst.PACKAGESPILTER))).doubleValue();
            T36 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    44, SysConst.PACKAGESPILTER))).doubleValue();
            T37 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    45, SysConst.PACKAGESPILTER))).doubleValue();
            T38 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    46, SysConst.PACKAGESPILTER))).doubleValue();
            T39 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    47, SysConst.PACKAGESPILTER))).doubleValue();
            T40 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    48, SysConst.PACKAGESPILTER))).doubleValue();
            T41 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    49, SysConst.PACKAGESPILTER))).doubleValue();
            T42 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    50, SysConst.PACKAGESPILTER))).doubleValue();
            T43 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    51, SysConst.PACKAGESPILTER))).doubleValue();
            T44 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    52, SysConst.PACKAGESPILTER))).doubleValue();
            T45 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    53, SysConst.PACKAGESPILTER))).doubleValue();
            T46 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    54, SysConst.PACKAGESPILTER))).doubleValue();
            T47 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    55, SysConst.PACKAGESPILTER))).doubleValue();
            T48 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    56, SysConst.PACKAGESPILTER))).doubleValue();
            T49 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    57, SysConst.PACKAGESPILTER))).doubleValue();
            T50 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    58, SysConst.PACKAGESPILTER))).doubleValue();
            State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59,
                                   SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 61, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 63, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 64,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASysIndexInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("IndexCalNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCalNo));
        }
        if (FCode.equals("IndexType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexType));
        }
        if (FCode.equals("SysIndexCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SysIndexCode));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("StartDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("StartEnd"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartEnd()));
        }
        if (FCode.equals("BranchAttr"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("T1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T1));
        }
        if (FCode.equals("T2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T2));
        }
        if (FCode.equals("T3"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T3));
        }
        if (FCode.equals("T4"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T4));
        }
        if (FCode.equals("T5"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T5));
        }
        if (FCode.equals("T6"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T6));
        }
        if (FCode.equals("T7"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T7));
        }
        if (FCode.equals("T8"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T8));
        }
        if (FCode.equals("T9"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T9));
        }
        if (FCode.equals("T10"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T10));
        }
        if (FCode.equals("T11"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T11));
        }
        if (FCode.equals("T12"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T12));
        }
        if (FCode.equals("T13"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T13));
        }
        if (FCode.equals("T14"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T14));
        }
        if (FCode.equals("T15"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T15));
        }
        if (FCode.equals("T16"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T16));
        }
        if (FCode.equals("T17"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T17));
        }
        if (FCode.equals("T18"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T18));
        }
        if (FCode.equals("T19"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T19));
        }
        if (FCode.equals("T20"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T20));
        }
        if (FCode.equals("T21"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T21));
        }
        if (FCode.equals("T22"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T22));
        }
        if (FCode.equals("T23"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T23));
        }
        if (FCode.equals("T24"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T24));
        }
        if (FCode.equals("T25"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T25));
        }
        if (FCode.equals("T26"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T26));
        }
        if (FCode.equals("T27"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T27));
        }
        if (FCode.equals("T28"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T28));
        }
        if (FCode.equals("T29"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T29));
        }
        if (FCode.equals("T30"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T30));
        }
        if (FCode.equals("T31"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T31));
        }
        if (FCode.equals("T32"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T32));
        }
        if (FCode.equals("T33"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T33));
        }
        if (FCode.equals("T34"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T34));
        }
        if (FCode.equals("T35"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T35));
        }
        if (FCode.equals("T36"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T36));
        }
        if (FCode.equals("T37"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T37));
        }
        if (FCode.equals("T38"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T38));
        }
        if (FCode.equals("T39"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T39));
        }
        if (FCode.equals("T40"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T40));
        }
        if (FCode.equals("T41"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T41));
        }
        if (FCode.equals("T42"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T42));
        }
        if (FCode.equals("T43"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T43));
        }
        if (FCode.equals("T44"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T44));
        }
        if (FCode.equals("T45"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T45));
        }
        if (FCode.equals("T46"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T46));
        }
        if (FCode.equals("T47"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T47));
        }
        if (FCode.equals("T48"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T48));
        }
        if (FCode.equals("T49"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T49));
        }
        if (FCode.equals("T50"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(T50));
        }
        if (FCode.equals("State"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(State));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(IndexCalNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(IndexType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SysIndexCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getStartEnd()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BranchAttr);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 8:
                strFieldValue = String.valueOf(T1);
                break;
            case 9:
                strFieldValue = String.valueOf(T2);
                break;
            case 10:
                strFieldValue = String.valueOf(T3);
                break;
            case 11:
                strFieldValue = String.valueOf(T4);
                break;
            case 12:
                strFieldValue = String.valueOf(T5);
                break;
            case 13:
                strFieldValue = String.valueOf(T6);
                break;
            case 14:
                strFieldValue = String.valueOf(T7);
                break;
            case 15:
                strFieldValue = String.valueOf(T8);
                break;
            case 16:
                strFieldValue = String.valueOf(T9);
                break;
            case 17:
                strFieldValue = String.valueOf(T10);
                break;
            case 18:
                strFieldValue = String.valueOf(T11);
                break;
            case 19:
                strFieldValue = String.valueOf(T12);
                break;
            case 20:
                strFieldValue = String.valueOf(T13);
                break;
            case 21:
                strFieldValue = String.valueOf(T14);
                break;
            case 22:
                strFieldValue = String.valueOf(T15);
                break;
            case 23:
                strFieldValue = String.valueOf(T16);
                break;
            case 24:
                strFieldValue = String.valueOf(T17);
                break;
            case 25:
                strFieldValue = String.valueOf(T18);
                break;
            case 26:
                strFieldValue = String.valueOf(T19);
                break;
            case 27:
                strFieldValue = String.valueOf(T20);
                break;
            case 28:
                strFieldValue = String.valueOf(T21);
                break;
            case 29:
                strFieldValue = String.valueOf(T22);
                break;
            case 30:
                strFieldValue = String.valueOf(T23);
                break;
            case 31:
                strFieldValue = String.valueOf(T24);
                break;
            case 32:
                strFieldValue = String.valueOf(T25);
                break;
            case 33:
                strFieldValue = String.valueOf(T26);
                break;
            case 34:
                strFieldValue = String.valueOf(T27);
                break;
            case 35:
                strFieldValue = String.valueOf(T28);
                break;
            case 36:
                strFieldValue = String.valueOf(T29);
                break;
            case 37:
                strFieldValue = String.valueOf(T30);
                break;
            case 38:
                strFieldValue = String.valueOf(T31);
                break;
            case 39:
                strFieldValue = String.valueOf(T32);
                break;
            case 40:
                strFieldValue = String.valueOf(T33);
                break;
            case 41:
                strFieldValue = String.valueOf(T34);
                break;
            case 42:
                strFieldValue = String.valueOf(T35);
                break;
            case 43:
                strFieldValue = String.valueOf(T36);
                break;
            case 44:
                strFieldValue = String.valueOf(T37);
                break;
            case 45:
                strFieldValue = String.valueOf(T38);
                break;
            case 46:
                strFieldValue = String.valueOf(T39);
                break;
            case 47:
                strFieldValue = String.valueOf(T40);
                break;
            case 48:
                strFieldValue = String.valueOf(T41);
                break;
            case 49:
                strFieldValue = String.valueOf(T42);
                break;
            case 50:
                strFieldValue = String.valueOf(T43);
                break;
            case 51:
                strFieldValue = String.valueOf(T44);
                break;
            case 52:
                strFieldValue = String.valueOf(T45);
                break;
            case 53:
                strFieldValue = String.valueOf(T46);
                break;
            case 54:
                strFieldValue = String.valueOf(T47);
                break;
            case 55:
                strFieldValue = String.valueOf(T48);
                break;
            case 56:
                strFieldValue = String.valueOf(T49);
                break;
            case 57:
                strFieldValue = String.valueOf(T50);
                break;
            case 58:
                strFieldValue = StrTool.GBKToUnicode(State);
                break;
            case 59:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 60:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 61:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 62:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 63:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("IndexCalNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexCalNo = FValue.trim();
            }
            else
            {
                IndexCalNo = null;
            }
        }
        if (FCode.equals("IndexType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexType = FValue.trim();
            }
            else
            {
                IndexType = null;
            }
        }
        if (FCode.equals("SysIndexCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SysIndexCode = FValue.trim();
            }
            else
            {
                SysIndexCode = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("StartDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartDate = fDate.getDate(FValue);
            }
            else
            {
                StartDate = null;
            }
        }
        if (FCode.equals("StartEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartEnd = fDate.getDate(FValue);
            }
            else
            {
                StartEnd = null;
            }
        }
        if (FCode.equals("BranchAttr"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchAttr = FValue.trim();
            }
            else
            {
                BranchAttr = null;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("T1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T1 = d;
            }
        }
        if (FCode.equals("T2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T2 = d;
            }
        }
        if (FCode.equals("T3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T3 = d;
            }
        }
        if (FCode.equals("T4"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T4 = d;
            }
        }
        if (FCode.equals("T5"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T5 = d;
            }
        }
        if (FCode.equals("T6"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T6 = d;
            }
        }
        if (FCode.equals("T7"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T7 = d;
            }
        }
        if (FCode.equals("T8"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T8 = d;
            }
        }
        if (FCode.equals("T9"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T9 = d;
            }
        }
        if (FCode.equals("T10"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T10 = d;
            }
        }
        if (FCode.equals("T11"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T11 = d;
            }
        }
        if (FCode.equals("T12"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T12 = d;
            }
        }
        if (FCode.equals("T13"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T13 = d;
            }
        }
        if (FCode.equals("T14"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T14 = d;
            }
        }
        if (FCode.equals("T15"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T15 = d;
            }
        }
        if (FCode.equals("T16"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T16 = d;
            }
        }
        if (FCode.equals("T17"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T17 = d;
            }
        }
        if (FCode.equals("T18"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T18 = d;
            }
        }
        if (FCode.equals("T19"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T19 = d;
            }
        }
        if (FCode.equals("T20"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T20 = d;
            }
        }
        if (FCode.equals("T21"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T21 = d;
            }
        }
        if (FCode.equals("T22"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T22 = d;
            }
        }
        if (FCode.equals("T23"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T23 = d;
            }
        }
        if (FCode.equals("T24"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T24 = d;
            }
        }
        if (FCode.equals("T25"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T25 = d;
            }
        }
        if (FCode.equals("T26"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T26 = d;
            }
        }
        if (FCode.equals("T27"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T27 = d;
            }
        }
        if (FCode.equals("T28"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T28 = d;
            }
        }
        if (FCode.equals("T29"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T29 = d;
            }
        }
        if (FCode.equals("T30"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T30 = d;
            }
        }
        if (FCode.equals("T31"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T31 = d;
            }
        }
        if (FCode.equals("T32"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T32 = d;
            }
        }
        if (FCode.equals("T33"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T33 = d;
            }
        }
        if (FCode.equals("T34"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T34 = d;
            }
        }
        if (FCode.equals("T35"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T35 = d;
            }
        }
        if (FCode.equals("T36"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T36 = d;
            }
        }
        if (FCode.equals("T37"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T37 = d;
            }
        }
        if (FCode.equals("T38"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T38 = d;
            }
        }
        if (FCode.equals("T39"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T39 = d;
            }
        }
        if (FCode.equals("T40"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T40 = d;
            }
        }
        if (FCode.equals("T41"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T41 = d;
            }
        }
        if (FCode.equals("T42"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T42 = d;
            }
        }
        if (FCode.equals("T43"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T43 = d;
            }
        }
        if (FCode.equals("T44"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T44 = d;
            }
        }
        if (FCode.equals("T45"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T45 = d;
            }
        }
        if (FCode.equals("T46"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T46 = d;
            }
        }
        if (FCode.equals("T47"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T47 = d;
            }
        }
        if (FCode.equals("T48"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T48 = d;
            }
        }
        if (FCode.equals("T49"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T49 = d;
            }
        }
        if (FCode.equals("T50"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                T50 = d;
            }
        }
        if (FCode.equals("State"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                State = FValue.trim();
            }
            else
            {
                State = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LASysIndexInfoSchema other = (LASysIndexInfoSchema) otherObject;
        return
                IndexCalNo.equals(other.getIndexCalNo())
                && IndexType.equals(other.getIndexType())
                && SysIndexCode.equals(other.getSysIndexCode())
                && ManageCom.equals(other.getManageCom())
                && fDate.getString(StartDate).equals(other.getStartDate())
                && fDate.getString(StartEnd).equals(other.getStartEnd())
                && BranchAttr.equals(other.getBranchAttr())
                && AgentGrade.equals(other.getAgentGrade())
                && T1 == other.getT1()
                && T2 == other.getT2()
                && T3 == other.getT3()
                && T4 == other.getT4()
                && T5 == other.getT5()
                && T6 == other.getT6()
                && T7 == other.getT7()
                && T8 == other.getT8()
                && T9 == other.getT9()
                && T10 == other.getT10()
                && T11 == other.getT11()
                && T12 == other.getT12()
                && T13 == other.getT13()
                && T14 == other.getT14()
                && T15 == other.getT15()
                && T16 == other.getT16()
                && T17 == other.getT17()
                && T18 == other.getT18()
                && T19 == other.getT19()
                && T20 == other.getT20()
                && T21 == other.getT21()
                && T22 == other.getT22()
                && T23 == other.getT23()
                && T24 == other.getT24()
                && T25 == other.getT25()
                && T26 == other.getT26()
                && T27 == other.getT27()
                && T28 == other.getT28()
                && T29 == other.getT29()
                && T30 == other.getT30()
                && T31 == other.getT31()
                && T32 == other.getT32()
                && T33 == other.getT33()
                && T34 == other.getT34()
                && T35 == other.getT35()
                && T36 == other.getT36()
                && T37 == other.getT37()
                && T38 == other.getT38()
                && T39 == other.getT39()
                && T40 == other.getT40()
                && T41 == other.getT41()
                && T42 == other.getT42()
                && T43 == other.getT43()
                && T44 == other.getT44()
                && T45 == other.getT45()
                && T46 == other.getT46()
                && T47 == other.getT47()
                && T48 == other.getT48()
                && T49 == other.getT49()
                && T50 == other.getT50()
                && State.equals(other.getState())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("IndexCalNo"))
        {
            return 0;
        }
        if (strFieldName.equals("IndexType"))
        {
            return 1;
        }
        if (strFieldName.equals("SysIndexCode"))
        {
            return 2;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 3;
        }
        if (strFieldName.equals("StartDate"))
        {
            return 4;
        }
        if (strFieldName.equals("StartEnd"))
        {
            return 5;
        }
        if (strFieldName.equals("BranchAttr"))
        {
            return 6;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 7;
        }
        if (strFieldName.equals("T1"))
        {
            return 8;
        }
        if (strFieldName.equals("T2"))
        {
            return 9;
        }
        if (strFieldName.equals("T3"))
        {
            return 10;
        }
        if (strFieldName.equals("T4"))
        {
            return 11;
        }
        if (strFieldName.equals("T5"))
        {
            return 12;
        }
        if (strFieldName.equals("T6"))
        {
            return 13;
        }
        if (strFieldName.equals("T7"))
        {
            return 14;
        }
        if (strFieldName.equals("T8"))
        {
            return 15;
        }
        if (strFieldName.equals("T9"))
        {
            return 16;
        }
        if (strFieldName.equals("T10"))
        {
            return 17;
        }
        if (strFieldName.equals("T11"))
        {
            return 18;
        }
        if (strFieldName.equals("T12"))
        {
            return 19;
        }
        if (strFieldName.equals("T13"))
        {
            return 20;
        }
        if (strFieldName.equals("T14"))
        {
            return 21;
        }
        if (strFieldName.equals("T15"))
        {
            return 22;
        }
        if (strFieldName.equals("T16"))
        {
            return 23;
        }
        if (strFieldName.equals("T17"))
        {
            return 24;
        }
        if (strFieldName.equals("T18"))
        {
            return 25;
        }
        if (strFieldName.equals("T19"))
        {
            return 26;
        }
        if (strFieldName.equals("T20"))
        {
            return 27;
        }
        if (strFieldName.equals("T21"))
        {
            return 28;
        }
        if (strFieldName.equals("T22"))
        {
            return 29;
        }
        if (strFieldName.equals("T23"))
        {
            return 30;
        }
        if (strFieldName.equals("T24"))
        {
            return 31;
        }
        if (strFieldName.equals("T25"))
        {
            return 32;
        }
        if (strFieldName.equals("T26"))
        {
            return 33;
        }
        if (strFieldName.equals("T27"))
        {
            return 34;
        }
        if (strFieldName.equals("T28"))
        {
            return 35;
        }
        if (strFieldName.equals("T29"))
        {
            return 36;
        }
        if (strFieldName.equals("T30"))
        {
            return 37;
        }
        if (strFieldName.equals("T31"))
        {
            return 38;
        }
        if (strFieldName.equals("T32"))
        {
            return 39;
        }
        if (strFieldName.equals("T33"))
        {
            return 40;
        }
        if (strFieldName.equals("T34"))
        {
            return 41;
        }
        if (strFieldName.equals("T35"))
        {
            return 42;
        }
        if (strFieldName.equals("T36"))
        {
            return 43;
        }
        if (strFieldName.equals("T37"))
        {
            return 44;
        }
        if (strFieldName.equals("T38"))
        {
            return 45;
        }
        if (strFieldName.equals("T39"))
        {
            return 46;
        }
        if (strFieldName.equals("T40"))
        {
            return 47;
        }
        if (strFieldName.equals("T41"))
        {
            return 48;
        }
        if (strFieldName.equals("T42"))
        {
            return 49;
        }
        if (strFieldName.equals("T43"))
        {
            return 50;
        }
        if (strFieldName.equals("T44"))
        {
            return 51;
        }
        if (strFieldName.equals("T45"))
        {
            return 52;
        }
        if (strFieldName.equals("T46"))
        {
            return 53;
        }
        if (strFieldName.equals("T47"))
        {
            return 54;
        }
        if (strFieldName.equals("T48"))
        {
            return 55;
        }
        if (strFieldName.equals("T49"))
        {
            return 56;
        }
        if (strFieldName.equals("T50"))
        {
            return 57;
        }
        if (strFieldName.equals("State"))
        {
            return 58;
        }
        if (strFieldName.equals("Operator"))
        {
            return 59;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 60;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 61;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 62;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 63;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "IndexCalNo";
                break;
            case 1:
                strFieldName = "IndexType";
                break;
            case 2:
                strFieldName = "SysIndexCode";
                break;
            case 3:
                strFieldName = "ManageCom";
                break;
            case 4:
                strFieldName = "StartDate";
                break;
            case 5:
                strFieldName = "StartEnd";
                break;
            case 6:
                strFieldName = "BranchAttr";
                break;
            case 7:
                strFieldName = "AgentGrade";
                break;
            case 8:
                strFieldName = "T1";
                break;
            case 9:
                strFieldName = "T2";
                break;
            case 10:
                strFieldName = "T3";
                break;
            case 11:
                strFieldName = "T4";
                break;
            case 12:
                strFieldName = "T5";
                break;
            case 13:
                strFieldName = "T6";
                break;
            case 14:
                strFieldName = "T7";
                break;
            case 15:
                strFieldName = "T8";
                break;
            case 16:
                strFieldName = "T9";
                break;
            case 17:
                strFieldName = "T10";
                break;
            case 18:
                strFieldName = "T11";
                break;
            case 19:
                strFieldName = "T12";
                break;
            case 20:
                strFieldName = "T13";
                break;
            case 21:
                strFieldName = "T14";
                break;
            case 22:
                strFieldName = "T15";
                break;
            case 23:
                strFieldName = "T16";
                break;
            case 24:
                strFieldName = "T17";
                break;
            case 25:
                strFieldName = "T18";
                break;
            case 26:
                strFieldName = "T19";
                break;
            case 27:
                strFieldName = "T20";
                break;
            case 28:
                strFieldName = "T21";
                break;
            case 29:
                strFieldName = "T22";
                break;
            case 30:
                strFieldName = "T23";
                break;
            case 31:
                strFieldName = "T24";
                break;
            case 32:
                strFieldName = "T25";
                break;
            case 33:
                strFieldName = "T26";
                break;
            case 34:
                strFieldName = "T27";
                break;
            case 35:
                strFieldName = "T28";
                break;
            case 36:
                strFieldName = "T29";
                break;
            case 37:
                strFieldName = "T30";
                break;
            case 38:
                strFieldName = "T31";
                break;
            case 39:
                strFieldName = "T32";
                break;
            case 40:
                strFieldName = "T33";
                break;
            case 41:
                strFieldName = "T34";
                break;
            case 42:
                strFieldName = "T35";
                break;
            case 43:
                strFieldName = "T36";
                break;
            case 44:
                strFieldName = "T37";
                break;
            case 45:
                strFieldName = "T38";
                break;
            case 46:
                strFieldName = "T39";
                break;
            case 47:
                strFieldName = "T40";
                break;
            case 48:
                strFieldName = "T41";
                break;
            case 49:
                strFieldName = "T42";
                break;
            case 50:
                strFieldName = "T43";
                break;
            case 51:
                strFieldName = "T44";
                break;
            case 52:
                strFieldName = "T45";
                break;
            case 53:
                strFieldName = "T46";
                break;
            case 54:
                strFieldName = "T47";
                break;
            case 55:
                strFieldName = "T48";
                break;
            case 56:
                strFieldName = "T49";
                break;
            case 57:
                strFieldName = "T50";
                break;
            case 58:
                strFieldName = "State";
                break;
            case 59:
                strFieldName = "Operator";
                break;
            case 60:
                strFieldName = "MakeDate";
                break;
            case 61:
                strFieldName = "MakeTime";
                break;
            case 62:
                strFieldName = "ModifyDate";
                break;
            case 63:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("IndexCalNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IndexType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SysIndexCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("StartEnd"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BranchAttr"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("T1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T2"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T3"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T4"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T5"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T6"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T7"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T8"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T9"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T10"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T11"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T12"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T13"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T14"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T15"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T16"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T17"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T18"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T19"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T20"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T21"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T22"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T23"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T24"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T25"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T26"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T27"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T28"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T29"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T30"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T31"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T32"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T33"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T34"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T35"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T36"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T37"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T38"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T39"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T40"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T41"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T42"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T43"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T44"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T45"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T46"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T47"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T48"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T49"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("T50"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("State"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 15:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 16:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 21:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 22:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 23:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 24:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 25:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 26:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 27:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 28:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 29:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 30:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 31:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 32:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 33:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 34:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 35:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 36:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 37:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 38:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 39:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 40:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 41:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 42:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 43:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 44:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 45:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 46:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 47:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 48:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 49:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 50:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 51:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 52:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 53:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 54:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 55:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 56:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 57:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 58:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 59:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 60:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 61:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 62:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 63:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
