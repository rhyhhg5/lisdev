package com.sinosoft.lis.schema;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PAD_AdjustmentUI {
	public CErrors mErrors = new CErrors();
	private PAD_AdjustmentBL padBL;
	public PAD_AdjustmentUI(){
		this.padBL = new PAD_AdjustmentBL();
	}
	public boolean submitData(VData cInputData,String transact) {
		if(!padBL.submitData(cInputData,transact))
        {
            mErrors.copyAllErrors(padBL.mErrors);
            return false;
        }
        return true;
	}

}
