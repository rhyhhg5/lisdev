/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAChargeBatchLogDB;

/*
 * <p>ClassName: LAChargeBatchLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-05-11
 */
public class LAChargeBatchLogSchema implements Schema, Cloneable
{
	// @Field
	/** 序列号 */
	private String SerialNo;
	/** 批次号 */
	private String BatchNo;
	/** 校验接口类型 */
	private String ZXRequestType;
	/** 校验错误代码 */
	private String ZXErrorCode;
	/** 校验错误描述 */
	private String ZXErrorMessage;
	/** 结算接口类型 */
	private String HXRequestType;
	/** 结算错误代码 */
	private String HXErrorCode;
	/** 结算错误描述 */
	private String HXErrorMessage;
	/** 凭证登记码 */
	private String VoucherRegNo;
	/** 支付查询接口类型 */
	private String PayRequestType;
	/** 支付错误代码 */
	private String PayErrorCode;
	/** 支付错误描述 */
	private String PayErrorMessage;
	/** 结算单号 */
	private String StatementsNo;
	/** 支付状态 */
	private String PayStatus;
	/** 支付失败原因 */
	private String PayFailReasons;
	/** 其它原因描述 */
	private String OtherReasonsDes;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 21;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAChargeBatchLogSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAChargeBatchLogSchema cloned = (LAChargeBatchLogSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getZXRequestType()
	{
		return ZXRequestType;
	}
	public void setZXRequestType(String aZXRequestType)
	{
		ZXRequestType = aZXRequestType;
	}
	public String getZXErrorCode()
	{
		return ZXErrorCode;
	}
	public void setZXErrorCode(String aZXErrorCode)
	{
		ZXErrorCode = aZXErrorCode;
	}
	public String getZXErrorMessage()
	{
		return ZXErrorMessage;
	}
	public void setZXErrorMessage(String aZXErrorMessage)
	{
		ZXErrorMessage = aZXErrorMessage;
	}
	public String getHXRequestType()
	{
		return HXRequestType;
	}
	public void setHXRequestType(String aHXRequestType)
	{
		HXRequestType = aHXRequestType;
	}
	public String getHXErrorCode()
	{
		return HXErrorCode;
	}
	public void setHXErrorCode(String aHXErrorCode)
	{
		HXErrorCode = aHXErrorCode;
	}
	public String getHXErrorMessage()
	{
		return HXErrorMessage;
	}
	public void setHXErrorMessage(String aHXErrorMessage)
	{
		HXErrorMessage = aHXErrorMessage;
	}
	public String getVoucherRegNo()
	{
		return VoucherRegNo;
	}
	public void setVoucherRegNo(String aVoucherRegNo)
	{
		VoucherRegNo = aVoucherRegNo;
	}
	public String getPayRequestType()
	{
		return PayRequestType;
	}
	public void setPayRequestType(String aPayRequestType)
	{
		PayRequestType = aPayRequestType;
	}
	public String getPayErrorCode()
	{
		return PayErrorCode;
	}
	public void setPayErrorCode(String aPayErrorCode)
	{
		PayErrorCode = aPayErrorCode;
	}
	public String getPayErrorMessage()
	{
		return PayErrorMessage;
	}
	public void setPayErrorMessage(String aPayErrorMessage)
	{
		PayErrorMessage = aPayErrorMessage;
	}
	public String getStatementsNo()
	{
		return StatementsNo;
	}
	public void setStatementsNo(String aStatementsNo)
	{
		StatementsNo = aStatementsNo;
	}
	public String getPayStatus()
	{
		return PayStatus;
	}
	public void setPayStatus(String aPayStatus)
	{
		PayStatus = aPayStatus;
	}
	public String getPayFailReasons()
	{
		return PayFailReasons;
	}
	public void setPayFailReasons(String aPayFailReasons)
	{
		PayFailReasons = aPayFailReasons;
	}
	public String getOtherReasonsDes()
	{
		return OtherReasonsDes;
	}
	public void setOtherReasonsDes(String aOtherReasonsDes)
	{
		OtherReasonsDes = aOtherReasonsDes;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LAChargeBatchLogSchema 对象给 Schema 赋值
	* @param: aLAChargeBatchLogSchema LAChargeBatchLogSchema
	**/
	public void setSchema(LAChargeBatchLogSchema aLAChargeBatchLogSchema)
	{
		this.SerialNo = aLAChargeBatchLogSchema.getSerialNo();
		this.BatchNo = aLAChargeBatchLogSchema.getBatchNo();
		this.ZXRequestType = aLAChargeBatchLogSchema.getZXRequestType();
		this.ZXErrorCode = aLAChargeBatchLogSchema.getZXErrorCode();
		this.ZXErrorMessage = aLAChargeBatchLogSchema.getZXErrorMessage();
		this.HXRequestType = aLAChargeBatchLogSchema.getHXRequestType();
		this.HXErrorCode = aLAChargeBatchLogSchema.getHXErrorCode();
		this.HXErrorMessage = aLAChargeBatchLogSchema.getHXErrorMessage();
		this.VoucherRegNo = aLAChargeBatchLogSchema.getVoucherRegNo();
		this.PayRequestType = aLAChargeBatchLogSchema.getPayRequestType();
		this.PayErrorCode = aLAChargeBatchLogSchema.getPayErrorCode();
		this.PayErrorMessage = aLAChargeBatchLogSchema.getPayErrorMessage();
		this.StatementsNo = aLAChargeBatchLogSchema.getStatementsNo();
		this.PayStatus = aLAChargeBatchLogSchema.getPayStatus();
		this.PayFailReasons = aLAChargeBatchLogSchema.getPayFailReasons();
		this.OtherReasonsDes = aLAChargeBatchLogSchema.getOtherReasonsDes();
		this.Operator = aLAChargeBatchLogSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAChargeBatchLogSchema.getMakeDate());
		this.MakeTime = aLAChargeBatchLogSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAChargeBatchLogSchema.getModifyDate());
		this.ModifyTime = aLAChargeBatchLogSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("ZXRequestType") == null )
				this.ZXRequestType = null;
			else
				this.ZXRequestType = rs.getString("ZXRequestType").trim();

			if( rs.getString("ZXErrorCode") == null )
				this.ZXErrorCode = null;
			else
				this.ZXErrorCode = rs.getString("ZXErrorCode").trim();

			if( rs.getString("ZXErrorMessage") == null )
				this.ZXErrorMessage = null;
			else
				this.ZXErrorMessage = rs.getString("ZXErrorMessage").trim();

			if( rs.getString("HXRequestType") == null )
				this.HXRequestType = null;
			else
				this.HXRequestType = rs.getString("HXRequestType").trim();

			if( rs.getString("HXErrorCode") == null )
				this.HXErrorCode = null;
			else
				this.HXErrorCode = rs.getString("HXErrorCode").trim();

			if( rs.getString("HXErrorMessage") == null )
				this.HXErrorMessage = null;
			else
				this.HXErrorMessage = rs.getString("HXErrorMessage").trim();

			if( rs.getString("VoucherRegNo") == null )
				this.VoucherRegNo = null;
			else
				this.VoucherRegNo = rs.getString("VoucherRegNo").trim();

			if( rs.getString("PayRequestType") == null )
				this.PayRequestType = null;
			else
				this.PayRequestType = rs.getString("PayRequestType").trim();

			if( rs.getString("PayErrorCode") == null )
				this.PayErrorCode = null;
			else
				this.PayErrorCode = rs.getString("PayErrorCode").trim();

			if( rs.getString("PayErrorMessage") == null )
				this.PayErrorMessage = null;
			else
				this.PayErrorMessage = rs.getString("PayErrorMessage").trim();

			if( rs.getString("StatementsNo") == null )
				this.StatementsNo = null;
			else
				this.StatementsNo = rs.getString("StatementsNo").trim();

			if( rs.getString("PayStatus") == null )
				this.PayStatus = null;
			else
				this.PayStatus = rs.getString("PayStatus").trim();

			if( rs.getString("PayFailReasons") == null )
				this.PayFailReasons = null;
			else
				this.PayFailReasons = rs.getString("PayFailReasons").trim();

			if( rs.getString("OtherReasonsDes") == null )
				this.OtherReasonsDes = null;
			else
				this.OtherReasonsDes = rs.getString("OtherReasonsDes").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAChargeBatchLog表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAChargeBatchLogSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAChargeBatchLogSchema getSchema()
	{
		LAChargeBatchLogSchema aLAChargeBatchLogSchema = new LAChargeBatchLogSchema();
		aLAChargeBatchLogSchema.setSchema(this);
		return aLAChargeBatchLogSchema;
	}

	public LAChargeBatchLogDB getDB()
	{
		LAChargeBatchLogDB aDBOper = new LAChargeBatchLogDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAChargeBatchLog描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZXRequestType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZXErrorCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZXErrorMessage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HXRequestType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HXErrorCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HXErrorMessage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherRegNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayRequestType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayErrorCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayErrorMessage)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StatementsNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayFailReasons)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OtherReasonsDes)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAChargeBatchLog>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ZXRequestType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ZXErrorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ZXErrorMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			HXRequestType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			HXErrorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			HXErrorMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			VoucherRegNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			PayRequestType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			PayErrorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			PayErrorMessage = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			StatementsNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			PayStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			PayFailReasons = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			OtherReasonsDes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAChargeBatchLogSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("ZXRequestType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZXRequestType));
		}
		if (FCode.equals("ZXErrorCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZXErrorCode));
		}
		if (FCode.equals("ZXErrorMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZXErrorMessage));
		}
		if (FCode.equals("HXRequestType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HXRequestType));
		}
		if (FCode.equals("HXErrorCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HXErrorCode));
		}
		if (FCode.equals("HXErrorMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HXErrorMessage));
		}
		if (FCode.equals("VoucherRegNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherRegNo));
		}
		if (FCode.equals("PayRequestType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayRequestType));
		}
		if (FCode.equals("PayErrorCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayErrorCode));
		}
		if (FCode.equals("PayErrorMessage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayErrorMessage));
		}
		if (FCode.equals("StatementsNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StatementsNo));
		}
		if (FCode.equals("PayStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayStatus));
		}
		if (FCode.equals("PayFailReasons"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayFailReasons));
		}
		if (FCode.equals("OtherReasonsDes"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherReasonsDes));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ZXRequestType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ZXErrorCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ZXErrorMessage);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(HXRequestType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(HXErrorCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(HXErrorMessage);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(VoucherRegNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(PayRequestType);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(PayErrorCode);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(PayErrorMessage);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(StatementsNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(PayStatus);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(PayFailReasons);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(OtherReasonsDes);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("ZXRequestType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZXRequestType = FValue.trim();
			}
			else
				ZXRequestType = null;
		}
		if (FCode.equalsIgnoreCase("ZXErrorCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZXErrorCode = FValue.trim();
			}
			else
				ZXErrorCode = null;
		}
		if (FCode.equalsIgnoreCase("ZXErrorMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZXErrorMessage = FValue.trim();
			}
			else
				ZXErrorMessage = null;
		}
		if (FCode.equalsIgnoreCase("HXRequestType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HXRequestType = FValue.trim();
			}
			else
				HXRequestType = null;
		}
		if (FCode.equalsIgnoreCase("HXErrorCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HXErrorCode = FValue.trim();
			}
			else
				HXErrorCode = null;
		}
		if (FCode.equalsIgnoreCase("HXErrorMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HXErrorMessage = FValue.trim();
			}
			else
				HXErrorMessage = null;
		}
		if (FCode.equalsIgnoreCase("VoucherRegNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherRegNo = FValue.trim();
			}
			else
				VoucherRegNo = null;
		}
		if (FCode.equalsIgnoreCase("PayRequestType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayRequestType = FValue.trim();
			}
			else
				PayRequestType = null;
		}
		if (FCode.equalsIgnoreCase("PayErrorCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayErrorCode = FValue.trim();
			}
			else
				PayErrorCode = null;
		}
		if (FCode.equalsIgnoreCase("PayErrorMessage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayErrorMessage = FValue.trim();
			}
			else
				PayErrorMessage = null;
		}
		if (FCode.equalsIgnoreCase("StatementsNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StatementsNo = FValue.trim();
			}
			else
				StatementsNo = null;
		}
		if (FCode.equalsIgnoreCase("PayStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayStatus = FValue.trim();
			}
			else
				PayStatus = null;
		}
		if (FCode.equalsIgnoreCase("PayFailReasons"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayFailReasons = FValue.trim();
			}
			else
				PayFailReasons = null;
		}
		if (FCode.equalsIgnoreCase("OtherReasonsDes"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherReasonsDes = FValue.trim();
			}
			else
				OtherReasonsDes = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAChargeBatchLogSchema other = (LAChargeBatchLogSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (ZXRequestType == null ? other.getZXRequestType() == null : ZXRequestType.equals(other.getZXRequestType()))
			&& (ZXErrorCode == null ? other.getZXErrorCode() == null : ZXErrorCode.equals(other.getZXErrorCode()))
			&& (ZXErrorMessage == null ? other.getZXErrorMessage() == null : ZXErrorMessage.equals(other.getZXErrorMessage()))
			&& (HXRequestType == null ? other.getHXRequestType() == null : HXRequestType.equals(other.getHXRequestType()))
			&& (HXErrorCode == null ? other.getHXErrorCode() == null : HXErrorCode.equals(other.getHXErrorCode()))
			&& (HXErrorMessage == null ? other.getHXErrorMessage() == null : HXErrorMessage.equals(other.getHXErrorMessage()))
			&& (VoucherRegNo == null ? other.getVoucherRegNo() == null : VoucherRegNo.equals(other.getVoucherRegNo()))
			&& (PayRequestType == null ? other.getPayRequestType() == null : PayRequestType.equals(other.getPayRequestType()))
			&& (PayErrorCode == null ? other.getPayErrorCode() == null : PayErrorCode.equals(other.getPayErrorCode()))
			&& (PayErrorMessage == null ? other.getPayErrorMessage() == null : PayErrorMessage.equals(other.getPayErrorMessage()))
			&& (StatementsNo == null ? other.getStatementsNo() == null : StatementsNo.equals(other.getStatementsNo()))
			&& (PayStatus == null ? other.getPayStatus() == null : PayStatus.equals(other.getPayStatus()))
			&& (PayFailReasons == null ? other.getPayFailReasons() == null : PayFailReasons.equals(other.getPayFailReasons()))
			&& (OtherReasonsDes == null ? other.getOtherReasonsDes() == null : OtherReasonsDes.equals(other.getOtherReasonsDes()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 1;
		}
		if( strFieldName.equals("ZXRequestType") ) {
			return 2;
		}
		if( strFieldName.equals("ZXErrorCode") ) {
			return 3;
		}
		if( strFieldName.equals("ZXErrorMessage") ) {
			return 4;
		}
		if( strFieldName.equals("HXRequestType") ) {
			return 5;
		}
		if( strFieldName.equals("HXErrorCode") ) {
			return 6;
		}
		if( strFieldName.equals("HXErrorMessage") ) {
			return 7;
		}
		if( strFieldName.equals("VoucherRegNo") ) {
			return 8;
		}
		if( strFieldName.equals("PayRequestType") ) {
			return 9;
		}
		if( strFieldName.equals("PayErrorCode") ) {
			return 10;
		}
		if( strFieldName.equals("PayErrorMessage") ) {
			return 11;
		}
		if( strFieldName.equals("StatementsNo") ) {
			return 12;
		}
		if( strFieldName.equals("PayStatus") ) {
			return 13;
		}
		if( strFieldName.equals("PayFailReasons") ) {
			return 14;
		}
		if( strFieldName.equals("OtherReasonsDes") ) {
			return 15;
		}
		if( strFieldName.equals("Operator") ) {
			return 16;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 17;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 20;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "BatchNo";
				break;
			case 2:
				strFieldName = "ZXRequestType";
				break;
			case 3:
				strFieldName = "ZXErrorCode";
				break;
			case 4:
				strFieldName = "ZXErrorMessage";
				break;
			case 5:
				strFieldName = "HXRequestType";
				break;
			case 6:
				strFieldName = "HXErrorCode";
				break;
			case 7:
				strFieldName = "HXErrorMessage";
				break;
			case 8:
				strFieldName = "VoucherRegNo";
				break;
			case 9:
				strFieldName = "PayRequestType";
				break;
			case 10:
				strFieldName = "PayErrorCode";
				break;
			case 11:
				strFieldName = "PayErrorMessage";
				break;
			case 12:
				strFieldName = "StatementsNo";
				break;
			case 13:
				strFieldName = "PayStatus";
				break;
			case 14:
				strFieldName = "PayFailReasons";
				break;
			case 15:
				strFieldName = "OtherReasonsDes";
				break;
			case 16:
				strFieldName = "Operator";
				break;
			case 17:
				strFieldName = "MakeDate";
				break;
			case 18:
				strFieldName = "MakeTime";
				break;
			case 19:
				strFieldName = "ModifyDate";
				break;
			case 20:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZXRequestType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZXErrorCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZXErrorMessage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HXRequestType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HXErrorCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HXErrorMessage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherRegNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayRequestType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayErrorCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayErrorMessage") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StatementsNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayFailReasons") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherReasonsDes") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
