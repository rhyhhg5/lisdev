/*
 * <p>ClassName: LDUWGradePersonSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 更新表
 * @CreateDate：2005-01-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDUWGradePersonDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDUWGradePersonSchema implements Schema
{
    // @Field
    /** 核保级别 */
    private String UWGrade;
    /** 险类 */
    private String UWKind;
    /** 机构权限 */
    private String UWCom;
    /** 是否标准体 */
    private String StandFlag;
    /** 是否体检件 */
    private String HealthFlag;
    /** 是否延期拒保 */
    private String DelayFlag;
    /** 业务规模 */
    private String BusiRange;
    /** 保额上限 */
    private double MaxAmnt;
    /** 疾病保额上限 */
    private double MaxIllAmnt;
    /** 死亡保额上限 */
    private double MaxDieAmnt;
    /** 其它保额上限 */
    private double OthMaxAmnt;
    /** 保费上限 */
    private double MaxPrem;
    /** 费率 */
    private double Rate;
    /** 其它费率 */
    private double OthRate;
    /** 是否可调管理费 */
    private String Flag;
    /** 是否可调整责任 */
    private String Flag1;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDUWGradePersonSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[7];
        pk[0] = "UWGrade";
        pk[1] = "UWKind";
        pk[2] = "UWCom";
        pk[3] = "StandFlag";
        pk[4] = "HealthFlag";
        pk[5] = "DelayFlag";
        pk[6] = "BusiRange";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getUWGrade()
    {
        if (UWGrade != null && !UWGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWGrade = StrTool.unicodeToGBK(UWGrade);
        }
        return UWGrade;
    }

    public void setUWGrade(String aUWGrade)
    {
        UWGrade = aUWGrade;
    }

    public String getUWKind()
    {
        if (UWKind != null && !UWKind.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWKind = StrTool.unicodeToGBK(UWKind);
        }
        return UWKind;
    }

    public void setUWKind(String aUWKind)
    {
        UWKind = aUWKind;
    }

    public String getUWCom()
    {
        if (UWCom != null && !UWCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            UWCom = StrTool.unicodeToGBK(UWCom);
        }
        return UWCom;
    }

    public void setUWCom(String aUWCom)
    {
        UWCom = aUWCom;
    }

    public String getStandFlag()
    {
        if (StandFlag != null && !StandFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            StandFlag = StrTool.unicodeToGBK(StandFlag);
        }
        return StandFlag;
    }

    public void setStandFlag(String aStandFlag)
    {
        StandFlag = aStandFlag;
    }

    public String getHealthFlag()
    {
        if (HealthFlag != null && !HealthFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HealthFlag = StrTool.unicodeToGBK(HealthFlag);
        }
        return HealthFlag;
    }

    public void setHealthFlag(String aHealthFlag)
    {
        HealthFlag = aHealthFlag;
    }

    public String getDelayFlag()
    {
        if (DelayFlag != null && !DelayFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DelayFlag = StrTool.unicodeToGBK(DelayFlag);
        }
        return DelayFlag;
    }

    public void setDelayFlag(String aDelayFlag)
    {
        DelayFlag = aDelayFlag;
    }

    public String getBusiRange()
    {
        if (BusiRange != null && !BusiRange.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            BusiRange = StrTool.unicodeToGBK(BusiRange);
        }
        return BusiRange;
    }

    public void setBusiRange(String aBusiRange)
    {
        BusiRange = aBusiRange;
    }

    public double getMaxAmnt()
    {
        return MaxAmnt;
    }

    public void setMaxAmnt(double aMaxAmnt)
    {
        MaxAmnt = aMaxAmnt;
    }

    public void setMaxAmnt(String aMaxAmnt)
    {
        if (aMaxAmnt != null && !aMaxAmnt.equals(""))
        {
            Double tDouble = new Double(aMaxAmnt);
            double d = tDouble.doubleValue();
            MaxAmnt = d;
        }
    }

    public double getMaxIllAmnt()
    {
        return MaxIllAmnt;
    }

    public void setMaxIllAmnt(double aMaxIllAmnt)
    {
        MaxIllAmnt = aMaxIllAmnt;
    }

    public void setMaxIllAmnt(String aMaxIllAmnt)
    {
        if (aMaxIllAmnt != null && !aMaxIllAmnt.equals(""))
        {
            Double tDouble = new Double(aMaxIllAmnt);
            double d = tDouble.doubleValue();
            MaxIllAmnt = d;
        }
    }

    public double getMaxDieAmnt()
    {
        return MaxDieAmnt;
    }

    public void setMaxDieAmnt(double aMaxDieAmnt)
    {
        MaxDieAmnt = aMaxDieAmnt;
    }

    public void setMaxDieAmnt(String aMaxDieAmnt)
    {
        if (aMaxDieAmnt != null && !aMaxDieAmnt.equals(""))
        {
            Double tDouble = new Double(aMaxDieAmnt);
            double d = tDouble.doubleValue();
            MaxDieAmnt = d;
        }
    }

    public double getOthMaxAmnt()
    {
        return OthMaxAmnt;
    }

    public void setOthMaxAmnt(double aOthMaxAmnt)
    {
        OthMaxAmnt = aOthMaxAmnt;
    }

    public void setOthMaxAmnt(String aOthMaxAmnt)
    {
        if (aOthMaxAmnt != null && !aOthMaxAmnt.equals(""))
        {
            Double tDouble = new Double(aOthMaxAmnt);
            double d = tDouble.doubleValue();
            OthMaxAmnt = d;
        }
    }

    public double getMaxPrem()
    {
        return MaxPrem;
    }

    public void setMaxPrem(double aMaxPrem)
    {
        MaxPrem = aMaxPrem;
    }

    public void setMaxPrem(String aMaxPrem)
    {
        if (aMaxPrem != null && !aMaxPrem.equals(""))
        {
            Double tDouble = new Double(aMaxPrem);
            double d = tDouble.doubleValue();
            MaxPrem = d;
        }
    }

    public double getRate()
    {
        return Rate;
    }

    public void setRate(double aRate)
    {
        Rate = aRate;
    }

    public void setRate(String aRate)
    {
        if (aRate != null && !aRate.equals(""))
        {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = d;
        }
    }

    public double getOthRate()
    {
        return OthRate;
    }

    public void setOthRate(double aOthRate)
    {
        OthRate = aOthRate;
    }

    public void setOthRate(String aOthRate)
    {
        if (aOthRate != null && !aOthRate.equals(""))
        {
            Double tDouble = new Double(aOthRate);
            double d = tDouble.doubleValue();
            OthRate = d;
        }
    }

    public String getFlag()
    {
        if (Flag != null && !Flag.equals("") && SysConst.CHANGECHARSET == true)
        {
            Flag = StrTool.unicodeToGBK(Flag);
        }
        return Flag;
    }

    public void setFlag(String aFlag)
    {
        Flag = aFlag;
    }

    public String getFlag1()
    {
        if (Flag1 != null && !Flag1.equals("") && SysConst.CHANGECHARSET == true)
        {
            Flag1 = StrTool.unicodeToGBK(Flag1);
        }
        return Flag1;
    }

    public void setFlag1(String aFlag1)
    {
        Flag1 = aFlag1;
    }

    /**
     * 使用另外一个 LDUWGradePersonSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDUWGradePersonSchema aLDUWGradePersonSchema)
    {
        this.UWGrade = aLDUWGradePersonSchema.getUWGrade();
        this.UWKind = aLDUWGradePersonSchema.getUWKind();
        this.UWCom = aLDUWGradePersonSchema.getUWCom();
        this.StandFlag = aLDUWGradePersonSchema.getStandFlag();
        this.HealthFlag = aLDUWGradePersonSchema.getHealthFlag();
        this.DelayFlag = aLDUWGradePersonSchema.getDelayFlag();
        this.BusiRange = aLDUWGradePersonSchema.getBusiRange();
        this.MaxAmnt = aLDUWGradePersonSchema.getMaxAmnt();
        this.MaxIllAmnt = aLDUWGradePersonSchema.getMaxIllAmnt();
        this.MaxDieAmnt = aLDUWGradePersonSchema.getMaxDieAmnt();
        this.OthMaxAmnt = aLDUWGradePersonSchema.getOthMaxAmnt();
        this.MaxPrem = aLDUWGradePersonSchema.getMaxPrem();
        this.Rate = aLDUWGradePersonSchema.getRate();
        this.OthRate = aLDUWGradePersonSchema.getOthRate();
        this.Flag = aLDUWGradePersonSchema.getFlag();
        this.Flag1 = aLDUWGradePersonSchema.getFlag1();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("UWGrade") == null)
            {
                this.UWGrade = null;
            }
            else
            {
                this.UWGrade = rs.getString("UWGrade").trim();
            }

            if (rs.getString("UWKind") == null)
            {
                this.UWKind = null;
            }
            else
            {
                this.UWKind = rs.getString("UWKind").trim();
            }

            if (rs.getString("UWCom") == null)
            {
                this.UWCom = null;
            }
            else
            {
                this.UWCom = rs.getString("UWCom").trim();
            }

            if (rs.getString("StandFlag") == null)
            {
                this.StandFlag = null;
            }
            else
            {
                this.StandFlag = rs.getString("StandFlag").trim();
            }

            if (rs.getString("HealthFlag") == null)
            {
                this.HealthFlag = null;
            }
            else
            {
                this.HealthFlag = rs.getString("HealthFlag").trim();
            }

            if (rs.getString("DelayFlag") == null)
            {
                this.DelayFlag = null;
            }
            else
            {
                this.DelayFlag = rs.getString("DelayFlag").trim();
            }

            if (rs.getString("BusiRange") == null)
            {
                this.BusiRange = null;
            }
            else
            {
                this.BusiRange = rs.getString("BusiRange").trim();
            }

            this.MaxAmnt = rs.getDouble("MaxAmnt");
            this.MaxIllAmnt = rs.getDouble("MaxIllAmnt");
            this.MaxDieAmnt = rs.getDouble("MaxDieAmnt");
            this.OthMaxAmnt = rs.getDouble("OthMaxAmnt");
            this.MaxPrem = rs.getDouble("MaxPrem");
            this.Rate = rs.getDouble("Rate");
            this.OthRate = rs.getDouble("OthRate");
            if (rs.getString("Flag") == null)
            {
                this.Flag = null;
            }
            else
            {
                this.Flag = rs.getString("Flag").trim();
            }

            if (rs.getString("Flag1") == null)
            {
                this.Flag1 = null;
            }
            else
            {
                this.Flag1 = rs.getString("Flag1").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDUWGradePersonSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDUWGradePersonSchema getSchema()
    {
        LDUWGradePersonSchema aLDUWGradePersonSchema = new
                LDUWGradePersonSchema();
        aLDUWGradePersonSchema.setSchema(this);
        return aLDUWGradePersonSchema;
    }

    public LDUWGradePersonDB getDB()
    {
        LDUWGradePersonDB aDBOper = new LDUWGradePersonDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDUWGradePerson描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(UWGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWKind)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UWCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StandFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HealthFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DelayFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(BusiRange)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(MaxAmnt) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(MaxIllAmnt) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(MaxDieAmnt) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(OthMaxAmnt) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(MaxPrem) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Rate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(OthRate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Flag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Flag1));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDUWGradePerson>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            UWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            UWKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            UWCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            StandFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            HealthFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            DelayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            BusiRange = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            MaxAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            MaxIllAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            MaxDieAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            OthMaxAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            MaxPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    13, SysConst.PACKAGESPILTER))).doubleValue();
            OthRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            Flag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                  SysConst.PACKAGESPILTER);
            Flag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                   SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDUWGradePersonSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("UWGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWGrade));
        }
        if (FCode.equals("UWKind"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWKind));
        }
        if (FCode.equals("UWCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UWCom));
        }
        if (FCode.equals("StandFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StandFlag));
        }
        if (FCode.equals("HealthFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HealthFlag));
        }
        if (FCode.equals("DelayFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DelayFlag));
        }
        if (FCode.equals("BusiRange"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(BusiRange));
        }
        if (FCode.equals("MaxAmnt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MaxAmnt));
        }
        if (FCode.equals("MaxIllAmnt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MaxIllAmnt));
        }
        if (FCode.equals("MaxDieAmnt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MaxDieAmnt));
        }
        if (FCode.equals("OthMaxAmnt"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OthMaxAmnt));
        }
        if (FCode.equals("MaxPrem"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MaxPrem));
        }
        if (FCode.equals("Rate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Rate));
        }
        if (FCode.equals("OthRate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OthRate));
        }
        if (FCode.equals("Flag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Flag));
        }
        if (FCode.equals("Flag1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Flag1));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(UWGrade);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(UWKind);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(UWCom);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(StandFlag);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(HealthFlag);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DelayFlag);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(BusiRange);
                break;
            case 7:
                strFieldValue = String.valueOf(MaxAmnt);
                break;
            case 8:
                strFieldValue = String.valueOf(MaxIllAmnt);
                break;
            case 9:
                strFieldValue = String.valueOf(MaxDieAmnt);
                break;
            case 10:
                strFieldValue = String.valueOf(OthMaxAmnt);
                break;
            case 11:
                strFieldValue = String.valueOf(MaxPrem);
                break;
            case 12:
                strFieldValue = String.valueOf(Rate);
                break;
            case 13:
                strFieldValue = String.valueOf(OthRate);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Flag);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Flag1);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("UWGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWGrade = FValue.trim();
            }
            else
            {
                UWGrade = null;
            }
        }
        if (FCode.equals("UWKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWKind = FValue.trim();
            }
            else
            {
                UWKind = null;
            }
        }
        if (FCode.equals("UWCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UWCom = FValue.trim();
            }
            else
            {
                UWCom = null;
            }
        }
        if (FCode.equals("StandFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StandFlag = FValue.trim();
            }
            else
            {
                StandFlag = null;
            }
        }
        if (FCode.equals("HealthFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HealthFlag = FValue.trim();
            }
            else
            {
                HealthFlag = null;
            }
        }
        if (FCode.equals("DelayFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DelayFlag = FValue.trim();
            }
            else
            {
                DelayFlag = null;
            }
        }
        if (FCode.equals("BusiRange"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BusiRange = FValue.trim();
            }
            else
            {
                BusiRange = null;
            }
        }
        if (FCode.equals("MaxAmnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MaxAmnt = d;
            }
        }
        if (FCode.equals("MaxIllAmnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MaxIllAmnt = d;
            }
        }
        if (FCode.equals("MaxDieAmnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MaxDieAmnt = d;
            }
        }
        if (FCode.equals("OthMaxAmnt"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                OthMaxAmnt = d;
            }
        }
        if (FCode.equals("MaxPrem"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                MaxPrem = d;
            }
        }
        if (FCode.equals("Rate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        if (FCode.equals("OthRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                OthRate = d;
            }
        }
        if (FCode.equals("Flag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Flag = FValue.trim();
            }
            else
            {
                Flag = null;
            }
        }
        if (FCode.equals("Flag1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Flag1 = FValue.trim();
            }
            else
            {
                Flag1 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDUWGradePersonSchema other = (LDUWGradePersonSchema) otherObject;
        return
                UWGrade.equals(other.getUWGrade())
                && UWKind.equals(other.getUWKind())
                && UWCom.equals(other.getUWCom())
                && StandFlag.equals(other.getStandFlag())
                && HealthFlag.equals(other.getHealthFlag())
                && DelayFlag.equals(other.getDelayFlag())
                && BusiRange.equals(other.getBusiRange())
                && MaxAmnt == other.getMaxAmnt()
                && MaxIllAmnt == other.getMaxIllAmnt()
                && MaxDieAmnt == other.getMaxDieAmnt()
                && OthMaxAmnt == other.getOthMaxAmnt()
                && MaxPrem == other.getMaxPrem()
                && Rate == other.getRate()
                && OthRate == other.getOthRate()
                && Flag.equals(other.getFlag())
                && Flag1.equals(other.getFlag1());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("UWGrade"))
        {
            return 0;
        }
        if (strFieldName.equals("UWKind"))
        {
            return 1;
        }
        if (strFieldName.equals("UWCom"))
        {
            return 2;
        }
        if (strFieldName.equals("StandFlag"))
        {
            return 3;
        }
        if (strFieldName.equals("HealthFlag"))
        {
            return 4;
        }
        if (strFieldName.equals("DelayFlag"))
        {
            return 5;
        }
        if (strFieldName.equals("BusiRange"))
        {
            return 6;
        }
        if (strFieldName.equals("MaxAmnt"))
        {
            return 7;
        }
        if (strFieldName.equals("MaxIllAmnt"))
        {
            return 8;
        }
        if (strFieldName.equals("MaxDieAmnt"))
        {
            return 9;
        }
        if (strFieldName.equals("OthMaxAmnt"))
        {
            return 10;
        }
        if (strFieldName.equals("MaxPrem"))
        {
            return 11;
        }
        if (strFieldName.equals("Rate"))
        {
            return 12;
        }
        if (strFieldName.equals("OthRate"))
        {
            return 13;
        }
        if (strFieldName.equals("Flag"))
        {
            return 14;
        }
        if (strFieldName.equals("Flag1"))
        {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "UWGrade";
                break;
            case 1:
                strFieldName = "UWKind";
                break;
            case 2:
                strFieldName = "UWCom";
                break;
            case 3:
                strFieldName = "StandFlag";
                break;
            case 4:
                strFieldName = "HealthFlag";
                break;
            case 5:
                strFieldName = "DelayFlag";
                break;
            case 6:
                strFieldName = "BusiRange";
                break;
            case 7:
                strFieldName = "MaxAmnt";
                break;
            case 8:
                strFieldName = "MaxIllAmnt";
                break;
            case 9:
                strFieldName = "MaxDieAmnt";
                break;
            case 10:
                strFieldName = "OthMaxAmnt";
                break;
            case 11:
                strFieldName = "MaxPrem";
                break;
            case 12:
                strFieldName = "Rate";
                break;
            case 13:
                strFieldName = "OthRate";
                break;
            case 14:
                strFieldName = "Flag";
                break;
            case 15:
                strFieldName = "Flag1";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("UWGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UWCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StandFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HealthFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DelayFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BusiRange"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MaxAmnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MaxIllAmnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MaxDieAmnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("OthMaxAmnt"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("MaxPrem"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Rate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("OthRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Flag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Flag1"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
