/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.Branch_Info_tempDB;

/*
 * <p>ClassName: Branch_Info_tempSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2012-04-19
 */
public class Branch_Info_tempSchema implements Schema, Cloneable
{
	// @Field
	/** 分支机构代码 */
	private String Branch_Code;
	/** 分支机构名称 */
	private String Branch_Name;
	/** 经营保险业务许可证号 */
	private String Branch_busi_num;
	/** 许可证发放日期 */
	private Date StartDate;
	/** 许可证注销日期 */
	private Date EndDate;
	/** 负责人代码 */
	private String Leader_Code;
	/** 分支机构编码 */
	private String Branch_ID;
	/** 经营区域代码 */
	private String BRANCH_AREA_CODE;

	public static final int FIELDNUM = 8;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public Branch_Info_tempSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Branch_Code";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		Branch_Info_tempSchema cloned = (Branch_Info_tempSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBranch_Code()
	{
		return Branch_Code;
	}
	public void setBranch_Code(String aBranch_Code)
	{
		Branch_Code = aBranch_Code;
	}
	public String getBranch_Name()
	{
		return Branch_Name;
	}
	public void setBranch_Name(String aBranch_Name)
	{
		Branch_Name = aBranch_Name;
	}
	public String getBranch_busi_num()
	{
		return Branch_busi_num;
	}
	public void setBranch_busi_num(String aBranch_busi_num)
	{
		Branch_busi_num = aBranch_busi_num;
	}
	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
		StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getEndDate()
	{
		if( EndDate != null )
			return fDate.getString(EndDate);
		else
			return null;
	}
	public void setEndDate(Date aEndDate)
	{
		EndDate = aEndDate;
	}
	public void setEndDate(String aEndDate)
	{
		if (aEndDate != null && !aEndDate.equals("") )
		{
			EndDate = fDate.getDate( aEndDate );
		}
		else
			EndDate = null;
	}

	public String getLeader_Code()
	{
		return Leader_Code;
	}
	public void setLeader_Code(String aLeader_Code)
	{
		Leader_Code = aLeader_Code;
	}
	public String getBranch_ID()
	{
		return Branch_ID;
	}
	public void setBranch_ID(String aBranch_ID)
	{
		Branch_ID = aBranch_ID;
	}
	public String getBRANCH_AREA_CODE()
	{
		return BRANCH_AREA_CODE;
	}
	public void setBRANCH_AREA_CODE(String aBRANCH_AREA_CODE)
	{
		BRANCH_AREA_CODE = aBRANCH_AREA_CODE;
	}

	/**
	* 使用另外一个 Branch_Info_tempSchema 对象给 Schema 赋值
	* @param: aBranch_Info_tempSchema Branch_Info_tempSchema
	**/
	public void setSchema(Branch_Info_tempSchema aBranch_Info_tempSchema)
	{
		this.Branch_Code = aBranch_Info_tempSchema.getBranch_Code();
		this.Branch_Name = aBranch_Info_tempSchema.getBranch_Name();
		this.Branch_busi_num = aBranch_Info_tempSchema.getBranch_busi_num();
		this.StartDate = fDate.getDate( aBranch_Info_tempSchema.getStartDate());
		this.EndDate = fDate.getDate( aBranch_Info_tempSchema.getEndDate());
		this.Leader_Code = aBranch_Info_tempSchema.getLeader_Code();
		this.Branch_ID = aBranch_Info_tempSchema.getBranch_ID();
		this.BRANCH_AREA_CODE = aBranch_Info_tempSchema.getBRANCH_AREA_CODE();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Branch_Code") == null )
				this.Branch_Code = null;
			else
				this.Branch_Code = rs.getString("Branch_Code").trim();

			if( rs.getString("Branch_Name") == null )
				this.Branch_Name = null;
			else
				this.Branch_Name = rs.getString("Branch_Name").trim();

			if( rs.getString("Branch_busi_num") == null )
				this.Branch_busi_num = null;
			else
				this.Branch_busi_num = rs.getString("Branch_busi_num").trim();

			this.StartDate = rs.getDate("StartDate");
			this.EndDate = rs.getDate("EndDate");
			if( rs.getString("Leader_Code") == null )
				this.Leader_Code = null;
			else
				this.Leader_Code = rs.getString("Leader_Code").trim();

			if( rs.getString("Branch_ID") == null )
				this.Branch_ID = null;
			else
				this.Branch_ID = rs.getString("Branch_ID").trim();

			if( rs.getString("BRANCH_AREA_CODE") == null )
				this.BRANCH_AREA_CODE = null;
			else
				this.BRANCH_AREA_CODE = rs.getString("BRANCH_AREA_CODE").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的Branch_Info_temp表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Branch_Info_tempSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public Branch_Info_tempSchema getSchema()
	{
		Branch_Info_tempSchema aBranch_Info_tempSchema = new Branch_Info_tempSchema();
		aBranch_Info_tempSchema.setSchema(this);
		return aBranch_Info_tempSchema;
	}

	public Branch_Info_tempDB getDB()
	{
		Branch_Info_tempDB aDBOper = new Branch_Info_tempDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBranch_Info_temp描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Branch_Code)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Branch_Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Branch_busi_num)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Leader_Code)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Branch_ID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BRANCH_AREA_CODE));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpBranch_Info_temp>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Branch_Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Branch_Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Branch_busi_num = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			Leader_Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Branch_ID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BRANCH_AREA_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Branch_Info_tempSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Branch_Code"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Branch_Code));
		}
		if (FCode.equals("Branch_Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Branch_Name));
		}
		if (FCode.equals("Branch_busi_num"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Branch_busi_num));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("EndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
		}
		if (FCode.equals("Leader_Code"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Leader_Code));
		}
		if (FCode.equals("Branch_ID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Branch_ID));
		}
		if (FCode.equals("BRANCH_AREA_CODE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BRANCH_AREA_CODE));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Branch_Code);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Branch_Name);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Branch_busi_num);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Leader_Code);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Branch_ID);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(BRANCH_AREA_CODE);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Branch_Code"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Branch_Code = FValue.trim();
			}
			else
				Branch_Code = null;
		}
		if (FCode.equalsIgnoreCase("Branch_Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Branch_Name = FValue.trim();
			}
			else
				Branch_Name = null;
		}
		if (FCode.equalsIgnoreCase("Branch_busi_num"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Branch_busi_num = FValue.trim();
			}
			else
				Branch_busi_num = null;
		}
		if (FCode.equalsIgnoreCase("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equalsIgnoreCase("EndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndDate = fDate.getDate( FValue );
			}
			else
				EndDate = null;
		}
		if (FCode.equalsIgnoreCase("Leader_Code"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Leader_Code = FValue.trim();
			}
			else
				Leader_Code = null;
		}
		if (FCode.equalsIgnoreCase("Branch_ID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Branch_ID = FValue.trim();
			}
			else
				Branch_ID = null;
		}
		if (FCode.equalsIgnoreCase("BRANCH_AREA_CODE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BRANCH_AREA_CODE = FValue.trim();
			}
			else
				BRANCH_AREA_CODE = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		Branch_Info_tempSchema other = (Branch_Info_tempSchema)otherObject;
		return
			(Branch_Code == null ? other.getBranch_Code() == null : Branch_Code.equals(other.getBranch_Code()))
			&& (Branch_Name == null ? other.getBranch_Name() == null : Branch_Name.equals(other.getBranch_Name()))
			&& (Branch_busi_num == null ? other.getBranch_busi_num() == null : Branch_busi_num.equals(other.getBranch_busi_num()))
			&& (StartDate == null ? other.getStartDate() == null : fDate.getString(StartDate).equals(other.getStartDate()))
			&& (EndDate == null ? other.getEndDate() == null : fDate.getString(EndDate).equals(other.getEndDate()))
			&& (Leader_Code == null ? other.getLeader_Code() == null : Leader_Code.equals(other.getLeader_Code()))
			&& (Branch_ID == null ? other.getBranch_ID() == null : Branch_ID.equals(other.getBranch_ID()))
			&& (BRANCH_AREA_CODE == null ? other.getBRANCH_AREA_CODE() == null : BRANCH_AREA_CODE.equals(other.getBRANCH_AREA_CODE()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Branch_Code") ) {
			return 0;
		}
		if( strFieldName.equals("Branch_Name") ) {
			return 1;
		}
		if( strFieldName.equals("Branch_busi_num") ) {
			return 2;
		}
		if( strFieldName.equals("StartDate") ) {
			return 3;
		}
		if( strFieldName.equals("EndDate") ) {
			return 4;
		}
		if( strFieldName.equals("Leader_Code") ) {
			return 5;
		}
		if( strFieldName.equals("Branch_ID") ) {
			return 6;
		}
		if( strFieldName.equals("BRANCH_AREA_CODE") ) {
			return 7;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Branch_Code";
				break;
			case 1:
				strFieldName = "Branch_Name";
				break;
			case 2:
				strFieldName = "Branch_busi_num";
				break;
			case 3:
				strFieldName = "StartDate";
				break;
			case 4:
				strFieldName = "EndDate";
				break;
			case 5:
				strFieldName = "Leader_Code";
				break;
			case 6:
				strFieldName = "Branch_ID";
				break;
			case 7:
				strFieldName = "BRANCH_AREA_CODE";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Branch_Code") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Branch_Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Branch_busi_num") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Leader_Code") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Branch_ID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BRANCH_AREA_CODE") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}

