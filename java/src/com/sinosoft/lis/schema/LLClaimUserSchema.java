/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLClaimUserDB;

/*
 * <p>ClassName: LLClaimUserSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2018-09-13
 */
public class LLClaimUserSchema implements Schema, Cloneable
{
	// @Field
	/** 用户编码 */
	private String UserCode;
	/** 用户姓名 */
	private String UserName;
	/** 机构编码 */
	private String ComCode;
	/** 上级用户代码 */
	private String UpUserCode;
	/** 理赔处理权限 */
	private String ClaimDeal;
	/** 理赔查询权限 */
	private String ClaimQuery;
	/** 报案权限 */
	private String ReportFlag;
	/** 立案权限 */
	private String RegisterFlag;
	/** 理赔计算权限 */
	private String ClaimFlag;
	/** 核赔权限 */
	private String ClaimPopedom;
	/** 审核权限 */
	private String CheckFlag;
	/** 签批权限 */
	private String UWFlag;
	/** 调查权限 */
	private String SurveyFlag;
	/** 给付权限 */
	private String PayFlag;
	/** 有效标识 */
	private String StateFlag;
	/** 入机时间 */
	private Date MakeDate;
	/** 入机日期 */
	private Date MakeTime;
	/** 修改时间 */
	private Date ModifyDate;
	/** 修改日期 */
	private Date ModiftTime;
	/** 操作人员 */
	private String Operator;
	/** 管理机构 */
	private String ManageCom;
	/** 原因 */
	private String Reason;
	/** 备注 */
	private String Remark;
	/** 理赔标记 */
	private String RgtFlag;
	/** 是否参加案件分配 */
	private String HandleFlag;
	/** 是否参加账单录入分配 */
	private String InputFlag;
	/** 案件分配比例 */
	private int DispatchRate;
	/** 预付赔款审批权限 */
	private String PrepaidFlag;
	/** 预付赔款审批上级 */
	private String PrepaidUpUserCode;
	/** 预付赔款审批额度 */
	private double PrepaidLimit;
	/** 特需业务审批权限 */
	private String SpecialNeedFlag;
	/** 特需业务审批额度 */
	private double SpecialNeedLimit;
	/** 特需业务审批上级 */
	private String SpecialNeedUpUserCode;
	/** 电商业务审批权限 */
	private String DSNeedFlag;
	/** 电商业务审批额度权限 */
	private String DSNeedLimitPopedom;
	/** 电商业务审批上级用户 */
	private String DSNeedUpUserCode;
	/** 电商业务审批备注 */
	private String DSRemark;

	public static final int FIELDNUM = 37;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLClaimUserSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "UserCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLClaimUserSchema cloned = (LLClaimUserSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getUserCode()
	{
		return UserCode;
	}
	public void setUserCode(String aUserCode)
	{
		UserCode = aUserCode;
	}
	public String getUserName()
	{
		return UserName;
	}
	public void setUserName(String aUserName)
	{
		UserName = aUserName;
	}
	public String getComCode()
	{
		return ComCode;
	}
	public void setComCode(String aComCode)
	{
		ComCode = aComCode;
	}
	public String getUpUserCode()
	{
		return UpUserCode;
	}
	public void setUpUserCode(String aUpUserCode)
	{
		UpUserCode = aUpUserCode;
	}
	public String getClaimDeal()
	{
		return ClaimDeal;
	}
	public void setClaimDeal(String aClaimDeal)
	{
		ClaimDeal = aClaimDeal;
	}
	public String getClaimQuery()
	{
		return ClaimQuery;
	}
	public void setClaimQuery(String aClaimQuery)
	{
		ClaimQuery = aClaimQuery;
	}
	public String getReportFlag()
	{
		return ReportFlag;
	}
	public void setReportFlag(String aReportFlag)
	{
		ReportFlag = aReportFlag;
	}
	public String getRegisterFlag()
	{
		return RegisterFlag;
	}
	public void setRegisterFlag(String aRegisterFlag)
	{
		RegisterFlag = aRegisterFlag;
	}
	public String getClaimFlag()
	{
		return ClaimFlag;
	}
	public void setClaimFlag(String aClaimFlag)
	{
		ClaimFlag = aClaimFlag;
	}
	public String getClaimPopedom()
	{
		return ClaimPopedom;
	}
	public void setClaimPopedom(String aClaimPopedom)
	{
		ClaimPopedom = aClaimPopedom;
	}
	public String getCheckFlag()
	{
		return CheckFlag;
	}
	public void setCheckFlag(String aCheckFlag)
	{
		CheckFlag = aCheckFlag;
	}
	public String getUWFlag()
	{
		return UWFlag;
	}
	public void setUWFlag(String aUWFlag)
	{
		UWFlag = aUWFlag;
	}
	public String getSurveyFlag()
	{
		return SurveyFlag;
	}
	public void setSurveyFlag(String aSurveyFlag)
	{
		SurveyFlag = aSurveyFlag;
	}
	public String getPayFlag()
	{
		return PayFlag;
	}
	public void setPayFlag(String aPayFlag)
	{
		PayFlag = aPayFlag;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		if( MakeTime != null )
			return fDate.getString(MakeTime);
		else
			return null;
	}
	public void setMakeTime(Date aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		if (aMakeTime != null && !aMakeTime.equals("") )
		{
			MakeTime = fDate.getDate( aMakeTime );
		}
		else
			MakeTime = null;
	}

	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModiftTime()
	{
		if( ModiftTime != null )
			return fDate.getString(ModiftTime);
		else
			return null;
	}
	public void setModiftTime(Date aModiftTime)
	{
		ModiftTime = aModiftTime;
	}
	public void setModiftTime(String aModiftTime)
	{
		if (aModiftTime != null && !aModiftTime.equals("") )
		{
			ModiftTime = fDate.getDate( aModiftTime );
		}
		else
			ModiftTime = null;
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getReason()
	{
		return Reason;
	}
	public void setReason(String aReason)
	{
		Reason = aReason;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getRgtFlag()
	{
		return RgtFlag;
	}
	public void setRgtFlag(String aRgtFlag)
	{
		RgtFlag = aRgtFlag;
	}
	public String getHandleFlag()
	{
		return HandleFlag;
	}
	public void setHandleFlag(String aHandleFlag)
	{
		HandleFlag = aHandleFlag;
	}
	public String getInputFlag()
	{
		return InputFlag;
	}
	public void setInputFlag(String aInputFlag)
	{
		InputFlag = aInputFlag;
	}
	public int getDispatchRate()
	{
		return DispatchRate;
	}
	public void setDispatchRate(int aDispatchRate)
	{
		DispatchRate = aDispatchRate;
	}
	public void setDispatchRate(String aDispatchRate)
	{
		if (aDispatchRate != null && !aDispatchRate.equals(""))
		{
			Integer tInteger = new Integer(aDispatchRate);
			int i = tInteger.intValue();
			DispatchRate = i;
		}
	}

	public String getPrepaidFlag()
	{
		return PrepaidFlag;
	}
	public void setPrepaidFlag(String aPrepaidFlag)
	{
		PrepaidFlag = aPrepaidFlag;
	}
	public String getPrepaidUpUserCode()
	{
		return PrepaidUpUserCode;
	}
	public void setPrepaidUpUserCode(String aPrepaidUpUserCode)
	{
		PrepaidUpUserCode = aPrepaidUpUserCode;
	}
	public double getPrepaidLimit()
	{
		return PrepaidLimit;
	}
	public void setPrepaidLimit(double aPrepaidLimit)
	{
		PrepaidLimit = Arith.round(aPrepaidLimit,2);
	}
	public void setPrepaidLimit(String aPrepaidLimit)
	{
		if (aPrepaidLimit != null && !aPrepaidLimit.equals(""))
		{
			Double tDouble = new Double(aPrepaidLimit);
			double d = tDouble.doubleValue();
                PrepaidLimit = Arith.round(d,2);
		}
	}

	public String getSpecialNeedFlag()
	{
		return SpecialNeedFlag;
	}
	public void setSpecialNeedFlag(String aSpecialNeedFlag)
	{
		SpecialNeedFlag = aSpecialNeedFlag;
	}
	public double getSpecialNeedLimit()
	{
		return SpecialNeedLimit;
	}
	public void setSpecialNeedLimit(double aSpecialNeedLimit)
	{
		SpecialNeedLimit = Arith.round(aSpecialNeedLimit,2);
	}
	public void setSpecialNeedLimit(String aSpecialNeedLimit)
	{
		if (aSpecialNeedLimit != null && !aSpecialNeedLimit.equals(""))
		{
			Double tDouble = new Double(aSpecialNeedLimit);
			double d = tDouble.doubleValue();
                SpecialNeedLimit = Arith.round(d,2);
		}
	}

	public String getSpecialNeedUpUserCode()
	{
		return SpecialNeedUpUserCode;
	}
	public void setSpecialNeedUpUserCode(String aSpecialNeedUpUserCode)
	{
		SpecialNeedUpUserCode = aSpecialNeedUpUserCode;
	}
	public String getDSNeedFlag()
	{
		return DSNeedFlag;
	}
	public void setDSNeedFlag(String aDSNeedFlag)
	{
		DSNeedFlag = aDSNeedFlag;
	}
	public String getDSNeedLimitPopedom()
	{
		return DSNeedLimitPopedom;
	}
	public void setDSNeedLimitPopedom(String aDSNeedLimitPopedom)
	{
		DSNeedLimitPopedom = aDSNeedLimitPopedom;
	}
	public String getDSNeedUpUserCode()
	{
		return DSNeedUpUserCode;
	}
	public void setDSNeedUpUserCode(String aDSNeedUpUserCode)
	{
		DSNeedUpUserCode = aDSNeedUpUserCode;
	}
	public String getDSRemark()
	{
		return DSRemark;
	}
	public void setDSRemark(String aDSRemark)
	{
		DSRemark = aDSRemark;
	}

	/**
	* 使用另外一个 LLClaimUserSchema 对象给 Schema 赋值
	* @param: aLLClaimUserSchema LLClaimUserSchema
	**/
	public void setSchema(LLClaimUserSchema aLLClaimUserSchema)
	{
		this.UserCode = aLLClaimUserSchema.getUserCode();
		this.UserName = aLLClaimUserSchema.getUserName();
		this.ComCode = aLLClaimUserSchema.getComCode();
		this.UpUserCode = aLLClaimUserSchema.getUpUserCode();
		this.ClaimDeal = aLLClaimUserSchema.getClaimDeal();
		this.ClaimQuery = aLLClaimUserSchema.getClaimQuery();
		this.ReportFlag = aLLClaimUserSchema.getReportFlag();
		this.RegisterFlag = aLLClaimUserSchema.getRegisterFlag();
		this.ClaimFlag = aLLClaimUserSchema.getClaimFlag();
		this.ClaimPopedom = aLLClaimUserSchema.getClaimPopedom();
		this.CheckFlag = aLLClaimUserSchema.getCheckFlag();
		this.UWFlag = aLLClaimUserSchema.getUWFlag();
		this.SurveyFlag = aLLClaimUserSchema.getSurveyFlag();
		this.PayFlag = aLLClaimUserSchema.getPayFlag();
		this.StateFlag = aLLClaimUserSchema.getStateFlag();
		this.MakeDate = fDate.getDate( aLLClaimUserSchema.getMakeDate());
		this.MakeTime = fDate.getDate( aLLClaimUserSchema.getMakeTime());
		this.ModifyDate = fDate.getDate( aLLClaimUserSchema.getModifyDate());
		this.ModiftTime = fDate.getDate( aLLClaimUserSchema.getModiftTime());
		this.Operator = aLLClaimUserSchema.getOperator();
		this.ManageCom = aLLClaimUserSchema.getManageCom();
		this.Reason = aLLClaimUserSchema.getReason();
		this.Remark = aLLClaimUserSchema.getRemark();
		this.RgtFlag = aLLClaimUserSchema.getRgtFlag();
		this.HandleFlag = aLLClaimUserSchema.getHandleFlag();
		this.InputFlag = aLLClaimUserSchema.getInputFlag();
		this.DispatchRate = aLLClaimUserSchema.getDispatchRate();
		this.PrepaidFlag = aLLClaimUserSchema.getPrepaidFlag();
		this.PrepaidUpUserCode = aLLClaimUserSchema.getPrepaidUpUserCode();
		this.PrepaidLimit = aLLClaimUserSchema.getPrepaidLimit();
		this.SpecialNeedFlag = aLLClaimUserSchema.getSpecialNeedFlag();
		this.SpecialNeedLimit = aLLClaimUserSchema.getSpecialNeedLimit();
		this.SpecialNeedUpUserCode = aLLClaimUserSchema.getSpecialNeedUpUserCode();
		this.DSNeedFlag = aLLClaimUserSchema.getDSNeedFlag();
		this.DSNeedLimitPopedom = aLLClaimUserSchema.getDSNeedLimitPopedom();
		this.DSNeedUpUserCode = aLLClaimUserSchema.getDSNeedUpUserCode();
		this.DSRemark = aLLClaimUserSchema.getDSRemark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("UserCode") == null )
				this.UserCode = null;
			else
				this.UserCode = rs.getString("UserCode").trim();

			if( rs.getString("UserName") == null )
				this.UserName = null;
			else
				this.UserName = rs.getString("UserName").trim();

			if( rs.getString("ComCode") == null )
				this.ComCode = null;
			else
				this.ComCode = rs.getString("ComCode").trim();

			if( rs.getString("UpUserCode") == null )
				this.UpUserCode = null;
			else
				this.UpUserCode = rs.getString("UpUserCode").trim();

			if( rs.getString("ClaimDeal") == null )
				this.ClaimDeal = null;
			else
				this.ClaimDeal = rs.getString("ClaimDeal").trim();

			if( rs.getString("ClaimQuery") == null )
				this.ClaimQuery = null;
			else
				this.ClaimQuery = rs.getString("ClaimQuery").trim();

			if( rs.getString("ReportFlag") == null )
				this.ReportFlag = null;
			else
				this.ReportFlag = rs.getString("ReportFlag").trim();

			if( rs.getString("RegisterFlag") == null )
				this.RegisterFlag = null;
			else
				this.RegisterFlag = rs.getString("RegisterFlag").trim();

			if( rs.getString("ClaimFlag") == null )
				this.ClaimFlag = null;
			else
				this.ClaimFlag = rs.getString("ClaimFlag").trim();

			if( rs.getString("ClaimPopedom") == null )
				this.ClaimPopedom = null;
			else
				this.ClaimPopedom = rs.getString("ClaimPopedom").trim();

			if( rs.getString("CheckFlag") == null )
				this.CheckFlag = null;
			else
				this.CheckFlag = rs.getString("CheckFlag").trim();

			if( rs.getString("UWFlag") == null )
				this.UWFlag = null;
			else
				this.UWFlag = rs.getString("UWFlag").trim();

			if( rs.getString("SurveyFlag") == null )
				this.SurveyFlag = null;
			else
				this.SurveyFlag = rs.getString("SurveyFlag").trim();

			if( rs.getString("PayFlag") == null )
				this.PayFlag = null;
			else
				this.PayFlag = rs.getString("PayFlag").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

			this.MakeDate = rs.getDate("MakeDate");
			this.MakeTime = rs.getDate("MakeTime");
			this.ModifyDate = rs.getDate("ModifyDate");
			this.ModiftTime = rs.getDate("ModiftTime");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Reason") == null )
				this.Reason = null;
			else
				this.Reason = rs.getString("Reason").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("RgtFlag") == null )
				this.RgtFlag = null;
			else
				this.RgtFlag = rs.getString("RgtFlag").trim();

			if( rs.getString("HandleFlag") == null )
				this.HandleFlag = null;
			else
				this.HandleFlag = rs.getString("HandleFlag").trim();

			if( rs.getString("InputFlag") == null )
				this.InputFlag = null;
			else
				this.InputFlag = rs.getString("InputFlag").trim();

			this.DispatchRate = rs.getInt("DispatchRate");
			if( rs.getString("PrepaidFlag") == null )
				this.PrepaidFlag = null;
			else
				this.PrepaidFlag = rs.getString("PrepaidFlag").trim();

			if( rs.getString("PrepaidUpUserCode") == null )
				this.PrepaidUpUserCode = null;
			else
				this.PrepaidUpUserCode = rs.getString("PrepaidUpUserCode").trim();

			this.PrepaidLimit = rs.getDouble("PrepaidLimit");
			if( rs.getString("SpecialNeedFlag") == null )
				this.SpecialNeedFlag = null;
			else
				this.SpecialNeedFlag = rs.getString("SpecialNeedFlag").trim();

			this.SpecialNeedLimit = rs.getDouble("SpecialNeedLimit");
			if( rs.getString("SpecialNeedUpUserCode") == null )
				this.SpecialNeedUpUserCode = null;
			else
				this.SpecialNeedUpUserCode = rs.getString("SpecialNeedUpUserCode").trim();

			if( rs.getString("DSNeedFlag") == null )
				this.DSNeedFlag = null;
			else
				this.DSNeedFlag = rs.getString("DSNeedFlag").trim();

			if( rs.getString("DSNeedLimitPopedom") == null )
				this.DSNeedLimitPopedom = null;
			else
				this.DSNeedLimitPopedom = rs.getString("DSNeedLimitPopedom").trim();

			if( rs.getString("DSNeedUpUserCode") == null )
				this.DSNeedUpUserCode = null;
			else
				this.DSNeedUpUserCode = rs.getString("DSNeedUpUserCode").trim();

			if( rs.getString("DSRemark") == null )
				this.DSRemark = null;
			else
				this.DSRemark = rs.getString("DSRemark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLClaimUser表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLClaimUserSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLClaimUserSchema getSchema()
	{
		LLClaimUserSchema aLLClaimUserSchema = new LLClaimUserSchema();
		aLLClaimUserSchema.setSchema(this);
		return aLLClaimUserSchema;
	}

	public LLClaimUserDB getDB()
	{
		LLClaimUserDB aDBOper = new LLClaimUserDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimUser描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(UserCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UserName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UpUserCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimDeal)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimQuery)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReportFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RegisterFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimPopedom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SurveyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeTime ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModiftTime ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Reason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HandleFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InputFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DispatchRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrepaidFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrepaidUpUserCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PrepaidLimit));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SpecialNeedFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SpecialNeedLimit));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SpecialNeedUpUserCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DSNeedFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DSNeedLimitPopedom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DSNeedUpUserCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DSRemark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimUser>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			UserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			UserName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			UpUserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ClaimDeal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ClaimQuery = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ReportFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			RegisterFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ClaimFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ClaimPopedom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			SurveyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			PayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			MakeTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			ModiftTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Reason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			RgtFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			HandleFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			InputFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			DispatchRate= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).intValue();
			PrepaidFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			PrepaidUpUserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			PrepaidLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			SpecialNeedFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			SpecialNeedLimit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			SpecialNeedUpUserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			DSNeedFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			DSNeedLimitPopedom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			DSNeedUpUserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			DSRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLClaimUserSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("UserCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UserCode));
		}
		if (FCode.equals("UserName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UserName));
		}
		if (FCode.equals("ComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
		}
		if (FCode.equals("UpUserCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UpUserCode));
		}
		if (FCode.equals("ClaimDeal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimDeal));
		}
		if (FCode.equals("ClaimQuery"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimQuery));
		}
		if (FCode.equals("ReportFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReportFlag));
		}
		if (FCode.equals("RegisterFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RegisterFlag));
		}
		if (FCode.equals("ClaimFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimFlag));
		}
		if (FCode.equals("ClaimPopedom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimPopedom));
		}
		if (FCode.equals("CheckFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
		}
		if (FCode.equals("UWFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
		}
		if (FCode.equals("SurveyFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SurveyFlag));
		}
		if (FCode.equals("PayFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayFlag));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeTime()));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModiftTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModiftTime()));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Reason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Reason));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("RgtFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtFlag));
		}
		if (FCode.equals("HandleFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HandleFlag));
		}
		if (FCode.equals("InputFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputFlag));
		}
		if (FCode.equals("DispatchRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DispatchRate));
		}
		if (FCode.equals("PrepaidFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidFlag));
		}
		if (FCode.equals("PrepaidUpUserCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidUpUserCode));
		}
		if (FCode.equals("PrepaidLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidLimit));
		}
		if (FCode.equals("SpecialNeedFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialNeedFlag));
		}
		if (FCode.equals("SpecialNeedLimit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialNeedLimit));
		}
		if (FCode.equals("SpecialNeedUpUserCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialNeedUpUserCode));
		}
		if (FCode.equals("DSNeedFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DSNeedFlag));
		}
		if (FCode.equals("DSNeedLimitPopedom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DSNeedLimitPopedom));
		}
		if (FCode.equals("DSNeedUpUserCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DSNeedUpUserCode));
		}
		if (FCode.equals("DSRemark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DSRemark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(UserCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(UserName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ComCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(UpUserCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ClaimDeal);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ClaimQuery);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ReportFlag);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(RegisterFlag);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ClaimFlag);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ClaimPopedom);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(CheckFlag);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(UWFlag);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(SurveyFlag);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(PayFlag);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeTime()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModiftTime()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Reason);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(RgtFlag);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(HandleFlag);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(InputFlag);
				break;
			case 26:
				strFieldValue = String.valueOf(DispatchRate);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(PrepaidFlag);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(PrepaidUpUserCode);
				break;
			case 29:
				strFieldValue = String.valueOf(PrepaidLimit);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(SpecialNeedFlag);
				break;
			case 31:
				strFieldValue = String.valueOf(SpecialNeedLimit);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(SpecialNeedUpUserCode);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(DSNeedFlag);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(DSNeedLimitPopedom);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(DSNeedUpUserCode);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(DSRemark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("UserCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UserCode = FValue.trim();
			}
			else
				UserCode = null;
		}
		if (FCode.equalsIgnoreCase("UserName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UserName = FValue.trim();
			}
			else
				UserName = null;
		}
		if (FCode.equalsIgnoreCase("ComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComCode = FValue.trim();
			}
			else
				ComCode = null;
		}
		if (FCode.equalsIgnoreCase("UpUserCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UpUserCode = FValue.trim();
			}
			else
				UpUserCode = null;
		}
		if (FCode.equalsIgnoreCase("ClaimDeal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimDeal = FValue.trim();
			}
			else
				ClaimDeal = null;
		}
		if (FCode.equalsIgnoreCase("ClaimQuery"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimQuery = FValue.trim();
			}
			else
				ClaimQuery = null;
		}
		if (FCode.equalsIgnoreCase("ReportFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReportFlag = FValue.trim();
			}
			else
				ReportFlag = null;
		}
		if (FCode.equalsIgnoreCase("RegisterFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RegisterFlag = FValue.trim();
			}
			else
				RegisterFlag = null;
		}
		if (FCode.equalsIgnoreCase("ClaimFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimFlag = FValue.trim();
			}
			else
				ClaimFlag = null;
		}
		if (FCode.equalsIgnoreCase("ClaimPopedom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimPopedom = FValue.trim();
			}
			else
				ClaimPopedom = null;
		}
		if (FCode.equalsIgnoreCase("CheckFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckFlag = FValue.trim();
			}
			else
				CheckFlag = null;
		}
		if (FCode.equalsIgnoreCase("UWFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWFlag = FValue.trim();
			}
			else
				UWFlag = null;
		}
		if (FCode.equalsIgnoreCase("SurveyFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SurveyFlag = FValue.trim();
			}
			else
				SurveyFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayFlag = FValue.trim();
			}
			else
				PayFlag = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeTime = fDate.getDate( FValue );
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModiftTime"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModiftTime = fDate.getDate( FValue );
			}
			else
				ModiftTime = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Reason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Reason = FValue.trim();
			}
			else
				Reason = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("RgtFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtFlag = FValue.trim();
			}
			else
				RgtFlag = null;
		}
		if (FCode.equalsIgnoreCase("HandleFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HandleFlag = FValue.trim();
			}
			else
				HandleFlag = null;
		}
		if (FCode.equalsIgnoreCase("InputFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InputFlag = FValue.trim();
			}
			else
				InputFlag = null;
		}
		if (FCode.equalsIgnoreCase("DispatchRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				DispatchRate = i;
			}
		}
		if (FCode.equalsIgnoreCase("PrepaidFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrepaidFlag = FValue.trim();
			}
			else
				PrepaidFlag = null;
		}
		if (FCode.equalsIgnoreCase("PrepaidUpUserCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrepaidUpUserCode = FValue.trim();
			}
			else
				PrepaidUpUserCode = null;
		}
		if (FCode.equalsIgnoreCase("PrepaidLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PrepaidLimit = d;
			}
		}
		if (FCode.equalsIgnoreCase("SpecialNeedFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SpecialNeedFlag = FValue.trim();
			}
			else
				SpecialNeedFlag = null;
		}
		if (FCode.equalsIgnoreCase("SpecialNeedLimit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SpecialNeedLimit = d;
			}
		}
		if (FCode.equalsIgnoreCase("SpecialNeedUpUserCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SpecialNeedUpUserCode = FValue.trim();
			}
			else
				SpecialNeedUpUserCode = null;
		}
		if (FCode.equalsIgnoreCase("DSNeedFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DSNeedFlag = FValue.trim();
			}
			else
				DSNeedFlag = null;
		}
		if (FCode.equalsIgnoreCase("DSNeedLimitPopedom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DSNeedLimitPopedom = FValue.trim();
			}
			else
				DSNeedLimitPopedom = null;
		}
		if (FCode.equalsIgnoreCase("DSNeedUpUserCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DSNeedUpUserCode = FValue.trim();
			}
			else
				DSNeedUpUserCode = null;
		}
		if (FCode.equalsIgnoreCase("DSRemark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DSRemark = FValue.trim();
			}
			else
				DSRemark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLClaimUserSchema other = (LLClaimUserSchema)otherObject;
		return
			(UserCode == null ? other.getUserCode() == null : UserCode.equals(other.getUserCode()))
			&& (UserName == null ? other.getUserName() == null : UserName.equals(other.getUserName()))
			&& (ComCode == null ? other.getComCode() == null : ComCode.equals(other.getComCode()))
			&& (UpUserCode == null ? other.getUpUserCode() == null : UpUserCode.equals(other.getUpUserCode()))
			&& (ClaimDeal == null ? other.getClaimDeal() == null : ClaimDeal.equals(other.getClaimDeal()))
			&& (ClaimQuery == null ? other.getClaimQuery() == null : ClaimQuery.equals(other.getClaimQuery()))
			&& (ReportFlag == null ? other.getReportFlag() == null : ReportFlag.equals(other.getReportFlag()))
			&& (RegisterFlag == null ? other.getRegisterFlag() == null : RegisterFlag.equals(other.getRegisterFlag()))
			&& (ClaimFlag == null ? other.getClaimFlag() == null : ClaimFlag.equals(other.getClaimFlag()))
			&& (ClaimPopedom == null ? other.getClaimPopedom() == null : ClaimPopedom.equals(other.getClaimPopedom()))
			&& (CheckFlag == null ? other.getCheckFlag() == null : CheckFlag.equals(other.getCheckFlag()))
			&& (UWFlag == null ? other.getUWFlag() == null : UWFlag.equals(other.getUWFlag()))
			&& (SurveyFlag == null ? other.getSurveyFlag() == null : SurveyFlag.equals(other.getSurveyFlag()))
			&& (PayFlag == null ? other.getPayFlag() == null : PayFlag.equals(other.getPayFlag()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : fDate.getString(MakeTime).equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModiftTime == null ? other.getModiftTime() == null : fDate.getString(ModiftTime).equals(other.getModiftTime()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Reason == null ? other.getReason() == null : Reason.equals(other.getReason()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (RgtFlag == null ? other.getRgtFlag() == null : RgtFlag.equals(other.getRgtFlag()))
			&& (HandleFlag == null ? other.getHandleFlag() == null : HandleFlag.equals(other.getHandleFlag()))
			&& (InputFlag == null ? other.getInputFlag() == null : InputFlag.equals(other.getInputFlag()))
			&& DispatchRate == other.getDispatchRate()
			&& (PrepaidFlag == null ? other.getPrepaidFlag() == null : PrepaidFlag.equals(other.getPrepaidFlag()))
			&& (PrepaidUpUserCode == null ? other.getPrepaidUpUserCode() == null : PrepaidUpUserCode.equals(other.getPrepaidUpUserCode()))
			&& PrepaidLimit == other.getPrepaidLimit()
			&& (SpecialNeedFlag == null ? other.getSpecialNeedFlag() == null : SpecialNeedFlag.equals(other.getSpecialNeedFlag()))
			&& SpecialNeedLimit == other.getSpecialNeedLimit()
			&& (SpecialNeedUpUserCode == null ? other.getSpecialNeedUpUserCode() == null : SpecialNeedUpUserCode.equals(other.getSpecialNeedUpUserCode()))
			&& (DSNeedFlag == null ? other.getDSNeedFlag() == null : DSNeedFlag.equals(other.getDSNeedFlag()))
			&& (DSNeedLimitPopedom == null ? other.getDSNeedLimitPopedom() == null : DSNeedLimitPopedom.equals(other.getDSNeedLimitPopedom()))
			&& (DSNeedUpUserCode == null ? other.getDSNeedUpUserCode() == null : DSNeedUpUserCode.equals(other.getDSNeedUpUserCode()))
			&& (DSRemark == null ? other.getDSRemark() == null : DSRemark.equals(other.getDSRemark()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("UserCode") ) {
			return 0;
		}
		if( strFieldName.equals("UserName") ) {
			return 1;
		}
		if( strFieldName.equals("ComCode") ) {
			return 2;
		}
		if( strFieldName.equals("UpUserCode") ) {
			return 3;
		}
		if( strFieldName.equals("ClaimDeal") ) {
			return 4;
		}
		if( strFieldName.equals("ClaimQuery") ) {
			return 5;
		}
		if( strFieldName.equals("ReportFlag") ) {
			return 6;
		}
		if( strFieldName.equals("RegisterFlag") ) {
			return 7;
		}
		if( strFieldName.equals("ClaimFlag") ) {
			return 8;
		}
		if( strFieldName.equals("ClaimPopedom") ) {
			return 9;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return 10;
		}
		if( strFieldName.equals("UWFlag") ) {
			return 11;
		}
		if( strFieldName.equals("SurveyFlag") ) {
			return 12;
		}
		if( strFieldName.equals("PayFlag") ) {
			return 13;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 14;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 15;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 17;
		}
		if( strFieldName.equals("ModiftTime") ) {
			return 18;
		}
		if( strFieldName.equals("Operator") ) {
			return 19;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 20;
		}
		if( strFieldName.equals("Reason") ) {
			return 21;
		}
		if( strFieldName.equals("Remark") ) {
			return 22;
		}
		if( strFieldName.equals("RgtFlag") ) {
			return 23;
		}
		if( strFieldName.equals("HandleFlag") ) {
			return 24;
		}
		if( strFieldName.equals("InputFlag") ) {
			return 25;
		}
		if( strFieldName.equals("DispatchRate") ) {
			return 26;
		}
		if( strFieldName.equals("PrepaidFlag") ) {
			return 27;
		}
		if( strFieldName.equals("PrepaidUpUserCode") ) {
			return 28;
		}
		if( strFieldName.equals("PrepaidLimit") ) {
			return 29;
		}
		if( strFieldName.equals("SpecialNeedFlag") ) {
			return 30;
		}
		if( strFieldName.equals("SpecialNeedLimit") ) {
			return 31;
		}
		if( strFieldName.equals("SpecialNeedUpUserCode") ) {
			return 32;
		}
		if( strFieldName.equals("DSNeedFlag") ) {
			return 33;
		}
		if( strFieldName.equals("DSNeedLimitPopedom") ) {
			return 34;
		}
		if( strFieldName.equals("DSNeedUpUserCode") ) {
			return 35;
		}
		if( strFieldName.equals("DSRemark") ) {
			return 36;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "UserCode";
				break;
			case 1:
				strFieldName = "UserName";
				break;
			case 2:
				strFieldName = "ComCode";
				break;
			case 3:
				strFieldName = "UpUserCode";
				break;
			case 4:
				strFieldName = "ClaimDeal";
				break;
			case 5:
				strFieldName = "ClaimQuery";
				break;
			case 6:
				strFieldName = "ReportFlag";
				break;
			case 7:
				strFieldName = "RegisterFlag";
				break;
			case 8:
				strFieldName = "ClaimFlag";
				break;
			case 9:
				strFieldName = "ClaimPopedom";
				break;
			case 10:
				strFieldName = "CheckFlag";
				break;
			case 11:
				strFieldName = "UWFlag";
				break;
			case 12:
				strFieldName = "SurveyFlag";
				break;
			case 13:
				strFieldName = "PayFlag";
				break;
			case 14:
				strFieldName = "StateFlag";
				break;
			case 15:
				strFieldName = "MakeDate";
				break;
			case 16:
				strFieldName = "MakeTime";
				break;
			case 17:
				strFieldName = "ModifyDate";
				break;
			case 18:
				strFieldName = "ModiftTime";
				break;
			case 19:
				strFieldName = "Operator";
				break;
			case 20:
				strFieldName = "ManageCom";
				break;
			case 21:
				strFieldName = "Reason";
				break;
			case 22:
				strFieldName = "Remark";
				break;
			case 23:
				strFieldName = "RgtFlag";
				break;
			case 24:
				strFieldName = "HandleFlag";
				break;
			case 25:
				strFieldName = "InputFlag";
				break;
			case 26:
				strFieldName = "DispatchRate";
				break;
			case 27:
				strFieldName = "PrepaidFlag";
				break;
			case 28:
				strFieldName = "PrepaidUpUserCode";
				break;
			case 29:
				strFieldName = "PrepaidLimit";
				break;
			case 30:
				strFieldName = "SpecialNeedFlag";
				break;
			case 31:
				strFieldName = "SpecialNeedLimit";
				break;
			case 32:
				strFieldName = "SpecialNeedUpUserCode";
				break;
			case 33:
				strFieldName = "DSNeedFlag";
				break;
			case 34:
				strFieldName = "DSNeedLimitPopedom";
				break;
			case 35:
				strFieldName = "DSNeedUpUserCode";
				break;
			case 36:
				strFieldName = "DSRemark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("UserCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UserName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UpUserCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimDeal") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimQuery") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReportFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RegisterFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimPopedom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SurveyFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModiftTime") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Reason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HandleFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DispatchRate") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PrepaidFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrepaidUpUserCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrepaidLimit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SpecialNeedFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SpecialNeedLimit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SpecialNeedUpUserCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DSNeedFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DSNeedLimitPopedom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DSNeedUpUserCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DSRemark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_INT;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
