/*
 * <p>ClassName: LCFamilyInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 家庭关系
 * @CreateDate：2005-01-26
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCFamilyInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LCFamilyInfoSchema implements Schema
{
    // @Field
    /** 家庭号 */
    private String FamilyNo;
    /** 备注 */
    private String Remark;
    /** 家庭地址 */
    private String HomeAddress;
    /** 家庭邮编 */
    private String HomeZipCode;
    /** 家庭电话 */
    private String HomePhone;
    /** 家庭传真 */
    private String HomeFax;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCFamilyInfoSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "FamilyNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getFamilyNo()
    {
        if (FamilyNo != null && !FamilyNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            FamilyNo = StrTool.unicodeToGBK(FamilyNo);
        }
        return FamilyNo;
    }

    public void setFamilyNo(String aFamilyNo)
    {
        FamilyNo = aFamilyNo;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getHomeAddress()
    {
        if (HomeAddress != null && !HomeAddress.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HomeAddress = StrTool.unicodeToGBK(HomeAddress);
        }
        return HomeAddress;
    }

    public void setHomeAddress(String aHomeAddress)
    {
        HomeAddress = aHomeAddress;
    }

    public String getHomeZipCode()
    {
        if (HomeZipCode != null && !HomeZipCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HomeZipCode = StrTool.unicodeToGBK(HomeZipCode);
        }
        return HomeZipCode;
    }

    public void setHomeZipCode(String aHomeZipCode)
    {
        HomeZipCode = aHomeZipCode;
    }

    public String getHomePhone()
    {
        if (HomePhone != null && !HomePhone.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            HomePhone = StrTool.unicodeToGBK(HomePhone);
        }
        return HomePhone;
    }

    public void setHomePhone(String aHomePhone)
    {
        HomePhone = aHomePhone;
    }

    public String getHomeFax()
    {
        if (HomeFax != null && !HomeFax.equals("") && SysConst.CHANGECHARSET == true)
        {
            HomeFax = StrTool.unicodeToGBK(HomeFax);
        }
        return HomeFax;
    }

    public void setHomeFax(String aHomeFax)
    {
        HomeFax = aHomeFax;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCFamilyInfoSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LCFamilyInfoSchema aLCFamilyInfoSchema)
    {
        this.FamilyNo = aLCFamilyInfoSchema.getFamilyNo();
        this.Remark = aLCFamilyInfoSchema.getRemark();
        this.HomeAddress = aLCFamilyInfoSchema.getHomeAddress();
        this.HomeZipCode = aLCFamilyInfoSchema.getHomeZipCode();
        this.HomePhone = aLCFamilyInfoSchema.getHomePhone();
        this.HomeFax = aLCFamilyInfoSchema.getHomeFax();
        this.MakeDate = fDate.getDate(aLCFamilyInfoSchema.getMakeDate());
        this.MakeTime = aLCFamilyInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCFamilyInfoSchema.getModifyDate());
        this.ModifyTime = aLCFamilyInfoSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("FamilyNo") == null)
            {
                this.FamilyNo = null;
            }
            else
            {
                this.FamilyNo = rs.getString("FamilyNo").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("HomeAddress") == null)
            {
                this.HomeAddress = null;
            }
            else
            {
                this.HomeAddress = rs.getString("HomeAddress").trim();
            }

            if (rs.getString("HomeZipCode") == null)
            {
                this.HomeZipCode = null;
            }
            else
            {
                this.HomeZipCode = rs.getString("HomeZipCode").trim();
            }

            if (rs.getString("HomePhone") == null)
            {
                this.HomePhone = null;
            }
            else
            {
                this.HomePhone = rs.getString("HomePhone").trim();
            }

            if (rs.getString("HomeFax") == null)
            {
                this.HomeFax = null;
            }
            else
            {
                this.HomeFax = rs.getString("HomeFax").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCFamilyInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCFamilyInfoSchema getSchema()
    {
        LCFamilyInfoSchema aLCFamilyInfoSchema = new LCFamilyInfoSchema();
        aLCFamilyInfoSchema.setSchema(this);
        return aLCFamilyInfoSchema;
    }

    public LCFamilyInfoDB getDB()
    {
        LCFamilyInfoDB aDBOper = new LCFamilyInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }

    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCFamilyInfo描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(FamilyNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HomeAddress)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HomeZipCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HomePhone)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(HomeFax)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCFamilyInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            FamilyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            HomeAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            HomeZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            HomePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            HomeFax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage),
                    7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage),
                    9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCFamilyInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("FamilyNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FamilyNo));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("HomeAddress"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HomeAddress));
        }
        if (FCode.equals("HomeZipCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HomeZipCode));
        }
        if (FCode.equals("HomePhone"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HomePhone));
        }
        if (FCode.equals("HomeFax"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(HomeFax));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }

    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(FamilyNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(HomeAddress);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(HomeZipCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(HomePhone);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(HomeFax);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("FamilyNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FamilyNo = FValue.trim();
            }
            else
            {
                FamilyNo = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("HomeAddress"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HomeAddress = FValue.trim();
            }
            else
            {
                HomeAddress = null;
            }
        }
        if (FCode.equals("HomeZipCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HomeZipCode = FValue.trim();
            }
            else
            {
                HomeZipCode = null;
            }
        }
        if (FCode.equals("HomePhone"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HomePhone = FValue.trim();
            }
            else
            {
                HomePhone = null;
            }
        }
        if (FCode.equals("HomeFax"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HomeFax = FValue.trim();
            }
            else
            {
                HomeFax = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCFamilyInfoSchema other = (LCFamilyInfoSchema) otherObject;
        return
                FamilyNo.equals(other.getFamilyNo())
                && Remark.equals(other.getRemark())
                && HomeAddress.equals(other.getHomeAddress())
                && HomeZipCode.equals(other.getHomeZipCode())
                && HomePhone.equals(other.getHomePhone())
                && HomeFax.equals(other.getHomeFax())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("FamilyNo"))
        {
            return 0;
        }
        if (strFieldName.equals("Remark"))
        {
            return 1;
        }
        if (strFieldName.equals("HomeAddress"))
        {
            return 2;
        }
        if (strFieldName.equals("HomeZipCode"))
        {
            return 3;
        }
        if (strFieldName.equals("HomePhone"))
        {
            return 4;
        }
        if (strFieldName.equals("HomeFax"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 7;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "FamilyNo";
                break;
            case 1:
                strFieldName = "Remark";
                break;
            case 2:
                strFieldName = "HomeAddress";
                break;
            case 3:
                strFieldName = "HomeZipCode";
                break;
            case 4:
                strFieldName = "HomePhone";
                break;
            case 5:
                strFieldName = "HomeFax";
                break;
            case 6:
                strFieldName = "MakeDate";
                break;
            case 7:
                strFieldName = "MakeTime";
                break;
            case 8:
                strFieldName = "ModifyDate";
                break;
            case 9:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("FamilyNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomeAddress"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomeZipCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomePhone"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HomeFax"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
