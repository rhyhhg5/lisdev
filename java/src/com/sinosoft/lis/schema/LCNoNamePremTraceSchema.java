/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCNoNamePremTraceDB;

/*
 * <p>ClassName: LCNoNamePremTraceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-07-13
 */
public class LCNoNamePremTraceSchema implements Schema, Cloneable {
    // @Field
    /** 集体合同号码 */
    private String GrpContNo;
    /** 保险计划编码 */
    private String ContPlanCode;
    /** 通知书号码 */
    private String GetNoticeNo;
    /** 原交至日期 */
    private Date PayToDate;
    /** 期交保费 */
    private double Prem;
    /** 录入保费 */
    private double PremInput;
    /** 被保人数 */
    private int Peoples2;
    /** 交费人数 */
    private int Peoples2Input;
    /** 录入日期 */
    private Date InputDate;
    /** 录入时间 */
    private String InputTime;
    /** 录入人 */
    private String InputOperator;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCNoNamePremTraceSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "GrpContNo";
        pk[1] = "ContPlanCode";
        pk[2] = "GetNoticeNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCNoNamePremTraceSchema cloned = (LCNoNamePremTraceSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getContPlanCode() {
        return ContPlanCode;
    }

    public void setContPlanCode(String aContPlanCode) {
        ContPlanCode = aContPlanCode;
    }

    public String getGetNoticeNo() {
        return GetNoticeNo;
    }

    public void setGetNoticeNo(String aGetNoticeNo) {
        GetNoticeNo = aGetNoticeNo;
    }

    public String getPayToDate() {
        if (PayToDate != null) {
            return fDate.getString(PayToDate);
        } else {
            return null;
        }
    }

    public void setPayToDate(Date aPayToDate) {
        PayToDate = aPayToDate;
    }

    public void setPayToDate(String aPayToDate) {
        if (aPayToDate != null && !aPayToDate.equals("")) {
            PayToDate = fDate.getDate(aPayToDate);
        } else {
            PayToDate = null;
        }
    }

    public double getPrem() {
        return Prem;
    }

    public void setPrem(double aPrem) {
        Prem = Arith.round(aPrem, 2);
    }

    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = Arith.round(d, 2);
        }
    }

    public double getPremInput() {
        return PremInput;
    }

    public void setPremInput(double aPremInput) {
        PremInput = Arith.round(aPremInput, 2);
    }

    public void setPremInput(String aPremInput) {
        if (aPremInput != null && !aPremInput.equals("")) {
            Double tDouble = new Double(aPremInput);
            double d = tDouble.doubleValue();
            PremInput = Arith.round(d, 2);
        }
    }

    public int getPeoples2() {
        return Peoples2;
    }

    public void setPeoples2(int aPeoples2) {
        Peoples2 = aPeoples2;
    }

    public void setPeoples2(String aPeoples2) {
        if (aPeoples2 != null && !aPeoples2.equals("")) {
            Integer tInteger = new Integer(aPeoples2);
            int i = tInteger.intValue();
            Peoples2 = i;
        }
    }

    public int getPeoples2Input() {
        return Peoples2Input;
    }

    public void setPeoples2Input(int aPeoples2Input) {
        Peoples2Input = aPeoples2Input;
    }

    public void setPeoples2Input(String aPeoples2Input) {
        if (aPeoples2Input != null && !aPeoples2Input.equals("")) {
            Integer tInteger = new Integer(aPeoples2Input);
            int i = tInteger.intValue();
            Peoples2Input = i;
        }
    }

    public String getInputDate() {
        if (InputDate != null) {
            return fDate.getString(InputDate);
        } else {
            return null;
        }
    }

    public void setInputDate(Date aInputDate) {
        InputDate = aInputDate;
    }

    public void setInputDate(String aInputDate) {
        if (aInputDate != null && !aInputDate.equals("")) {
            InputDate = fDate.getDate(aInputDate);
        } else {
            InputDate = null;
        }
    }

    public String getInputTime() {
        return InputTime;
    }

    public void setInputTime(String aInputTime) {
        InputTime = aInputTime;
    }

    public String getInputOperator() {
        return InputOperator;
    }

    public void setInputOperator(String aInputOperator) {
        InputOperator = aInputOperator;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCNoNamePremTraceSchema 对象给 Schema 赋值
     * @param: aLCNoNamePremTraceSchema LCNoNamePremTraceSchema
     **/
    public void setSchema(LCNoNamePremTraceSchema aLCNoNamePremTraceSchema) {
        this.GrpContNo = aLCNoNamePremTraceSchema.getGrpContNo();
        this.ContPlanCode = aLCNoNamePremTraceSchema.getContPlanCode();
        this.GetNoticeNo = aLCNoNamePremTraceSchema.getGetNoticeNo();
        this.PayToDate = fDate.getDate(aLCNoNamePremTraceSchema.getPayToDate());
        this.Prem = aLCNoNamePremTraceSchema.getPrem();
        this.PremInput = aLCNoNamePremTraceSchema.getPremInput();
        this.Peoples2 = aLCNoNamePremTraceSchema.getPeoples2();
        this.Peoples2Input = aLCNoNamePremTraceSchema.getPeoples2Input();
        this.InputDate = fDate.getDate(aLCNoNamePremTraceSchema.getInputDate());
        this.InputTime = aLCNoNamePremTraceSchema.getInputTime();
        this.InputOperator = aLCNoNamePremTraceSchema.getInputOperator();
        this.Remark = aLCNoNamePremTraceSchema.getRemark();
        this.Operator = aLCNoNamePremTraceSchema.getOperator();
        this.MakeDate = fDate.getDate(aLCNoNamePremTraceSchema.getMakeDate());
        this.MakeTime = aLCNoNamePremTraceSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCNoNamePremTraceSchema.getModifyDate());
        this.ModifyTime = aLCNoNamePremTraceSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ContPlanCode") == null) {
                this.ContPlanCode = null;
            } else {
                this.ContPlanCode = rs.getString("ContPlanCode").trim();
            }

            if (rs.getString("GetNoticeNo") == null) {
                this.GetNoticeNo = null;
            } else {
                this.GetNoticeNo = rs.getString("GetNoticeNo").trim();
            }

            this.PayToDate = rs.getDate("PayToDate");
            this.Prem = rs.getDouble("Prem");
            this.PremInput = rs.getDouble("PremInput");
            this.Peoples2 = rs.getInt("Peoples2");
            this.Peoples2Input = rs.getInt("Peoples2Input");
            this.InputDate = rs.getDate("InputDate");
            if (rs.getString("InputTime") == null) {
                this.InputTime = null;
            } else {
                this.InputTime = rs.getString("InputTime").trim();
            }

            if (rs.getString("InputOperator") == null) {
                this.InputOperator = null;
            } else {
                this.InputOperator = rs.getString("InputOperator").trim();
            }

            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LCNoNamePremTrace表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCNoNamePremTraceSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LCNoNamePremTraceSchema getSchema() {
        LCNoNamePremTraceSchema aLCNoNamePremTraceSchema = new
                LCNoNamePremTraceSchema();
        aLCNoNamePremTraceSchema.setSchema(this);
        return aLCNoNamePremTraceSchema;
    }

    public LCNoNamePremTraceDB getDB() {
        LCNoNamePremTraceDB aDBOper = new LCNoNamePremTraceDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCNoNamePremTrace描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContPlanCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GetNoticeNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PayToDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PremInput));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples2));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Peoples2Input));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(InputDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InputTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InputOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCNoNamePremTrace>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            GetNoticeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            PayToDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    5, SysConst.PACKAGESPILTER))).doubleValue();
            PremInput = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            Peoples2 = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            Peoples2Input = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).intValue();
            InputDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            InputTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            InputOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                           SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCNoNamePremTraceSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("ContPlanCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
        }
        if (FCode.equals("GetNoticeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GetNoticeNo));
        }
        if (FCode.equals("PayToDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPayToDate()));
        }
        if (FCode.equals("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equals("PremInput")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremInput));
        }
        if (FCode.equals("Peoples2")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2));
        }
        if (FCode.equals("Peoples2Input")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples2Input));
        }
        if (FCode.equals("InputDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getInputDate()));
        }
        if (FCode.equals("InputTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputTime));
        }
        if (FCode.equals("InputOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InputOperator));
        }
        if (FCode.equals("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(GetNoticeNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getPayToDate()));
            break;
        case 4:
            strFieldValue = String.valueOf(Prem);
            break;
        case 5:
            strFieldValue = String.valueOf(PremInput);
            break;
        case 6:
            strFieldValue = String.valueOf(Peoples2);
            break;
        case 7:
            strFieldValue = String.valueOf(Peoples2Input);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getInputDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(InputTime);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(InputOperator);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContPlanCode")) {
            if (FValue != null && !FValue.equals("")) {
                ContPlanCode = FValue.trim();
            } else {
                ContPlanCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("GetNoticeNo")) {
            if (FValue != null && !FValue.equals("")) {
                GetNoticeNo = FValue.trim();
            } else {
                GetNoticeNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PayToDate")) {
            if (FValue != null && !FValue.equals("")) {
                PayToDate = fDate.getDate(FValue);
            } else {
                PayToDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("PremInput")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PremInput = d;
            }
        }
        if (FCode.equalsIgnoreCase("Peoples2")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Peoples2 = i;
            }
        }
        if (FCode.equalsIgnoreCase("Peoples2Input")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Peoples2Input = i;
            }
        }
        if (FCode.equalsIgnoreCase("InputDate")) {
            if (FValue != null && !FValue.equals("")) {
                InputDate = fDate.getDate(FValue);
            } else {
                InputDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("InputTime")) {
            if (FValue != null && !FValue.equals("")) {
                InputTime = FValue.trim();
            } else {
                InputTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("InputOperator")) {
            if (FValue != null && !FValue.equals("")) {
                InputOperator = FValue.trim();
            } else {
                InputOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LCNoNamePremTraceSchema other = (LCNoNamePremTraceSchema) otherObject;
        return
                GrpContNo.equals(other.getGrpContNo())
                && ContPlanCode.equals(other.getContPlanCode())
                && GetNoticeNo.equals(other.getGetNoticeNo())
                && fDate.getString(PayToDate).equals(other.getPayToDate())
                && Prem == other.getPrem()
                && PremInput == other.getPremInput()
                && Peoples2 == other.getPeoples2()
                && Peoples2Input == other.getPeoples2Input()
                && fDate.getString(InputDate).equals(other.getInputDate())
                && InputTime.equals(other.getInputTime())
                && InputOperator.equals(other.getInputOperator())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("GrpContNo")) {
            return 0;
        }
        if (strFieldName.equals("ContPlanCode")) {
            return 1;
        }
        if (strFieldName.equals("GetNoticeNo")) {
            return 2;
        }
        if (strFieldName.equals("PayToDate")) {
            return 3;
        }
        if (strFieldName.equals("Prem")) {
            return 4;
        }
        if (strFieldName.equals("PremInput")) {
            return 5;
        }
        if (strFieldName.equals("Peoples2")) {
            return 6;
        }
        if (strFieldName.equals("Peoples2Input")) {
            return 7;
        }
        if (strFieldName.equals("InputDate")) {
            return 8;
        }
        if (strFieldName.equals("InputTime")) {
            return 9;
        }
        if (strFieldName.equals("InputOperator")) {
            return 10;
        }
        if (strFieldName.equals("Remark")) {
            return 11;
        }
        if (strFieldName.equals("Operator")) {
            return 12;
        }
        if (strFieldName.equals("MakeDate")) {
            return 13;
        }
        if (strFieldName.equals("MakeTime")) {
            return 14;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 15;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "GrpContNo";
            break;
        case 1:
            strFieldName = "ContPlanCode";
            break;
        case 2:
            strFieldName = "GetNoticeNo";
            break;
        case 3:
            strFieldName = "PayToDate";
            break;
        case 4:
            strFieldName = "Prem";
            break;
        case 5:
            strFieldName = "PremInput";
            break;
        case 6:
            strFieldName = "Peoples2";
            break;
        case 7:
            strFieldName = "Peoples2Input";
            break;
        case 8:
            strFieldName = "InputDate";
            break;
        case 9:
            strFieldName = "InputTime";
            break;
        case 10:
            strFieldName = "InputOperator";
            break;
        case 11:
            strFieldName = "Remark";
            break;
        case 12:
            strFieldName = "Operator";
            break;
        case 13:
            strFieldName = "MakeDate";
            break;
        case 14:
            strFieldName = "MakeTime";
            break;
        case 15:
            strFieldName = "ModifyDate";
            break;
        case 16:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContPlanCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GetNoticeNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayToDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Prem")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PremInput")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Peoples2")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Peoples2Input")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("InputDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InputTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InputOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 4:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 5:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 6:
            nFieldType = Schema.TYPE_INT;
            break;
        case 7:
            nFieldType = Schema.TYPE_INT;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
