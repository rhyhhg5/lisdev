/*
 * <p>ClassName: LDTaskPlanSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 后台任务服务
 * @CreateDate：2004-12-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDTaskPlanDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDTaskPlanSchema implements Schema
{
    // @Field
    /** 任务计划编码 */
    private String TaskPlanCode;
    /** 启用标志 */
    private String RunFlag;
    /** 周期类型 */
    private String RecycleType;
    /** 起始时间 */
    private String StartTime;
    /** 终止时间 */
    private String EndTime;
    /** 循环间隔 */
    private double Interval;
    /** 循环次数 */
    private double Times;
    /** 任务状态 */
    private String RunState;
    /** 任务代码 */
    private String TaskCode;
    /** 操作员 */
    private String Operator;
    /** 录入日期 */
    private Date MakeDate;
    /** 录入时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 14; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDTaskPlanSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "TaskPlanCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getTaskPlanCode()
    {
        if (TaskPlanCode != null && !TaskPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            TaskPlanCode = StrTool.unicodeToGBK(TaskPlanCode);
        }
        return TaskPlanCode;
    }

    public void setTaskPlanCode(String aTaskPlanCode)
    {
        TaskPlanCode = aTaskPlanCode;
    }

    public String getRunFlag()
    {
        if (RunFlag != null && !RunFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            RunFlag = StrTool.unicodeToGBK(RunFlag);
        }
        return RunFlag;
    }

    public void setRunFlag(String aRunFlag)
    {
        RunFlag = aRunFlag;
    }

    public String getRecycleType()
    {
        if (RecycleType != null && !RecycleType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RecycleType = StrTool.unicodeToGBK(RecycleType);
        }
        return RecycleType;
    }

    public void setRecycleType(String aRecycleType)
    {
        RecycleType = aRecycleType;
    }

    public String getStartTime()
    {
        if (StartTime != null && !StartTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            StartTime = StrTool.unicodeToGBK(StartTime);
        }
        return StartTime;
    }

    public void setStartTime(String aStartTime)
    {
        StartTime = aStartTime;
    }

    public String getEndTime()
    {
        if (EndTime != null && !EndTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            EndTime = StrTool.unicodeToGBK(EndTime);
        }
        return EndTime;
    }

    public void setEndTime(String aEndTime)
    {
        EndTime = aEndTime;
    }

    public double getInterval()
    {
        return Interval;
    }

    public void setInterval(double aInterval)
    {
        Interval = aInterval;
    }

    public void setInterval(String aInterval)
    {
        if (aInterval != null && !aInterval.equals(""))
        {
            Double tDouble = new Double(aInterval);
            double d = tDouble.doubleValue();
            Interval = d;
        }
    }

    public double getTimes()
    {
        return Times;
    }

    public void setTimes(double aTimes)
    {
        Times = aTimes;
    }

    public void setTimes(String aTimes)
    {
        if (aTimes != null && !aTimes.equals(""))
        {
            Double tDouble = new Double(aTimes);
            double d = tDouble.doubleValue();
            Times = d;
        }
    }

    public String getRunState()
    {
        if (RunState != null && !RunState.equals("") && SysConst.CHANGECHARSET == true)
        {
            RunState = StrTool.unicodeToGBK(RunState);
        }
        return RunState;
    }

    public void setRunState(String aRunState)
    {
        RunState = aRunState;
    }

    public String getTaskCode()
    {
        if (TaskCode != null && !TaskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            TaskCode = StrTool.unicodeToGBK(TaskCode);
        }
        return TaskCode;
    }

    public void setTaskCode(String aTaskCode)
    {
        TaskCode = aTaskCode;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LDTaskPlanSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDTaskPlanSchema aLDTaskPlanSchema)
    {
        this.TaskPlanCode = aLDTaskPlanSchema.getTaskPlanCode();
        this.RunFlag = aLDTaskPlanSchema.getRunFlag();
        this.RecycleType = aLDTaskPlanSchema.getRecycleType();
        this.StartTime = aLDTaskPlanSchema.getStartTime();
        this.EndTime = aLDTaskPlanSchema.getEndTime();
        this.Interval = aLDTaskPlanSchema.getInterval();
        this.Times = aLDTaskPlanSchema.getTimes();
        this.RunState = aLDTaskPlanSchema.getRunState();
        this.TaskCode = aLDTaskPlanSchema.getTaskCode();
        this.Operator = aLDTaskPlanSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDTaskPlanSchema.getMakeDate());
        this.MakeTime = aLDTaskPlanSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDTaskPlanSchema.getModifyDate());
        this.ModifyTime = aLDTaskPlanSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("TaskPlanCode") == null)
            {
                this.TaskPlanCode = null;
            }
            else
            {
                this.TaskPlanCode = rs.getString("TaskPlanCode").trim();
            }

            if (rs.getString("RunFlag") == null)
            {
                this.RunFlag = null;
            }
            else
            {
                this.RunFlag = rs.getString("RunFlag").trim();
            }

            if (rs.getString("RecycleType") == null)
            {
                this.RecycleType = null;
            }
            else
            {
                this.RecycleType = rs.getString("RecycleType").trim();
            }

            if (rs.getString("StartTime") == null)
            {
                this.StartTime = null;
            }
            else
            {
                this.StartTime = rs.getString("StartTime").trim();
            }

            if (rs.getString("EndTime") == null)
            {
                this.EndTime = null;
            }
            else
            {
                this.EndTime = rs.getString("EndTime").trim();
            }

            this.Interval = rs.getDouble("Interval");
            this.Times = rs.getDouble("Times");
            if (rs.getString("RunState") == null)
            {
                this.RunState = null;
            }
            else
            {
                this.RunState = rs.getString("RunState").trim();
            }

            if (rs.getString("TaskCode") == null)
            {
                this.TaskCode = null;
            }
            else
            {
                this.TaskCode = rs.getString("TaskCode").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTaskPlanSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDTaskPlanSchema getSchema()
    {
        LDTaskPlanSchema aLDTaskPlanSchema = new LDTaskPlanSchema();
        aLDTaskPlanSchema.setSchema(this);
        return aLDTaskPlanSchema;
    }

    public LDTaskPlanDB getDB()
    {
        LDTaskPlanDB aDBOper = new LDTaskPlanDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTaskPlan描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(TaskPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RunFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RecycleType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(StartTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EndTime)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Interval) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Times) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RunState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(TaskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTaskPlan>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            TaskPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            RunFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            RecycleType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            StartTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                       SysConst.PACKAGESPILTER);
            EndTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            Interval = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            Times = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    7, SysConst.PACKAGESPILTER))).doubleValue();
            RunState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            TaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTaskPlanSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("TaskPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TaskPlanCode));
        }
        if (FCode.equals("RunFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RunFlag));
        }
        if (FCode.equals("RecycleType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RecycleType));
        }
        if (FCode.equals("StartTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StartTime));
        }
        if (FCode.equals("EndTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EndTime));
        }
        if (FCode.equals("Interval"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Interval));
        }
        if (FCode.equals("Times"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Times));
        }
        if (FCode.equals("RunState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RunState));
        }
        if (FCode.equals("TaskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(TaskCode));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(TaskPlanCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RunFlag);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RecycleType);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(StartTime);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(EndTime);
                break;
            case 5:
                strFieldValue = String.valueOf(Interval);
                break;
            case 6:
                strFieldValue = String.valueOf(Times);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(RunState);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(TaskCode);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("TaskPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TaskPlanCode = FValue.trim();
            }
            else
            {
                TaskPlanCode = null;
            }
        }
        if (FCode.equals("RunFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RunFlag = FValue.trim();
            }
            else
            {
                RunFlag = null;
            }
        }
        if (FCode.equals("RecycleType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RecycleType = FValue.trim();
            }
            else
            {
                RecycleType = null;
            }
        }
        if (FCode.equals("StartTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                StartTime = FValue.trim();
            }
            else
            {
                StartTime = null;
            }
        }
        if (FCode.equals("EndTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EndTime = FValue.trim();
            }
            else
            {
                EndTime = null;
            }
        }
        if (FCode.equals("Interval"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Interval = d;
            }
        }
        if (FCode.equals("Times"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Times = d;
            }
        }
        if (FCode.equals("RunState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RunState = FValue.trim();
            }
            else
            {
                RunState = null;
            }
        }
        if (FCode.equals("TaskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TaskCode = FValue.trim();
            }
            else
            {
                TaskCode = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDTaskPlanSchema other = (LDTaskPlanSchema) otherObject;
        return
                TaskPlanCode.equals(other.getTaskPlanCode())
                && RunFlag.equals(other.getRunFlag())
                && RecycleType.equals(other.getRecycleType())
                && StartTime.equals(other.getStartTime())
                && EndTime.equals(other.getEndTime())
                && Interval == other.getInterval()
                && Times == other.getTimes()
                && RunState.equals(other.getRunState())
                && TaskCode.equals(other.getTaskCode())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("TaskPlanCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RunFlag"))
        {
            return 1;
        }
        if (strFieldName.equals("RecycleType"))
        {
            return 2;
        }
        if (strFieldName.equals("StartTime"))
        {
            return 3;
        }
        if (strFieldName.equals("EndTime"))
        {
            return 4;
        }
        if (strFieldName.equals("Interval"))
        {
            return 5;
        }
        if (strFieldName.equals("Times"))
        {
            return 6;
        }
        if (strFieldName.equals("RunState"))
        {
            return 7;
        }
        if (strFieldName.equals("TaskCode"))
        {
            return 8;
        }
        if (strFieldName.equals("Operator"))
        {
            return 9;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 11;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 13;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "TaskPlanCode";
                break;
            case 1:
                strFieldName = "RunFlag";
                break;
            case 2:
                strFieldName = "RecycleType";
                break;
            case 3:
                strFieldName = "StartTime";
                break;
            case 4:
                strFieldName = "EndTime";
                break;
            case 5:
                strFieldName = "Interval";
                break;
            case 6:
                strFieldName = "Times";
                break;
            case 7:
                strFieldName = "RunState";
                break;
            case 8:
                strFieldName = "TaskCode";
                break;
            case 9:
                strFieldName = "Operator";
                break;
            case 10:
                strFieldName = "MakeDate";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            case 12:
                strFieldName = "ModifyDate";
                break;
            case 13:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("TaskPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RunFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RecycleType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StartTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EndTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Interval"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Times"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RunState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TaskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
