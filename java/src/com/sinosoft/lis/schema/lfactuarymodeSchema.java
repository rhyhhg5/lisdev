/*
 * <p>ClassName: lfactuarymodeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.lfactuarymodeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class lfactuarymodeSchema implements Schema
{
    // @Field
    /** 内部科目编码 */
    private String ItemCode;
    /** 内部科目名称 */
    private String ItemName;
    /** 关联类型 */
    private String itemrelatype;
    /** 关联科目编码 */
    private String relaitemcode;
    /** 关联科目名称 */
    private String relaitemname;
    /** 是否提供 */
    private String isprovideflag;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public lfactuarymodeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getItemCode()
    {
        if (ItemCode != null && !ItemCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ItemCode = StrTool.unicodeToGBK(ItemCode);
        }
        return ItemCode;
    }

    public void setItemCode(String aItemCode)
    {
        ItemCode = aItemCode;
    }

    public String getItemName()
    {
        if (ItemName != null && !ItemName.equals("") && SysConst.CHANGECHARSET == true)
        {
            ItemName = StrTool.unicodeToGBK(ItemName);
        }
        return ItemName;
    }

    public void setItemName(String aItemName)
    {
        ItemName = aItemName;
    }

    public String getitemrelatype()
    {
        if (itemrelatype != null && !itemrelatype.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            itemrelatype = StrTool.unicodeToGBK(itemrelatype);
        }
        return itemrelatype;
    }

    public void setitemrelatype(String aitemrelatype)
    {
        itemrelatype = aitemrelatype;
    }

    public String getrelaitemcode()
    {
        if (relaitemcode != null && !relaitemcode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            relaitemcode = StrTool.unicodeToGBK(relaitemcode);
        }
        return relaitemcode;
    }

    public void setrelaitemcode(String arelaitemcode)
    {
        relaitemcode = arelaitemcode;
    }

    public String getrelaitemname()
    {
        if (relaitemname != null && !relaitemname.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            relaitemname = StrTool.unicodeToGBK(relaitemname);
        }
        return relaitemname;
    }

    public void setrelaitemname(String arelaitemname)
    {
        relaitemname = arelaitemname;
    }

    public String getisprovideflag()
    {
        if (isprovideflag != null && !isprovideflag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            isprovideflag = StrTool.unicodeToGBK(isprovideflag);
        }
        return isprovideflag;
    }

    public void setisprovideflag(String aisprovideflag)
    {
        isprovideflag = aisprovideflag;
    }

    /**
     * 使用另外一个 lfactuarymodeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(lfactuarymodeSchema alfactuarymodeSchema)
    {
        this.ItemCode = alfactuarymodeSchema.getItemCode();
        this.ItemName = alfactuarymodeSchema.getItemName();
        this.itemrelatype = alfactuarymodeSchema.getitemrelatype();
        this.relaitemcode = alfactuarymodeSchema.getrelaitemcode();
        this.relaitemname = alfactuarymodeSchema.getrelaitemname();
        this.isprovideflag = alfactuarymodeSchema.getisprovideflag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ItemCode") == null)
            {
                this.ItemCode = null;
            }
            else
            {
                this.ItemCode = rs.getString("ItemCode").trim();
            }

            if (rs.getString("ItemName") == null)
            {
                this.ItemName = null;
            }
            else
            {
                this.ItemName = rs.getString("ItemName").trim();
            }

            if (rs.getString("itemrelatype") == null)
            {
                this.itemrelatype = null;
            }
            else
            {
                this.itemrelatype = rs.getString("itemrelatype").trim();
            }

            if (rs.getString("relaitemcode") == null)
            {
                this.relaitemcode = null;
            }
            else
            {
                this.relaitemcode = rs.getString("relaitemcode").trim();
            }

            if (rs.getString("relaitemname") == null)
            {
                this.relaitemname = null;
            }
            else
            {
                this.relaitemname = rs.getString("relaitemname").trim();
            }

            if (rs.getString("isprovideflag") == null)
            {
                this.isprovideflag = null;
            }
            else
            {
                this.isprovideflag = rs.getString("isprovideflag").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "lfactuarymodeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public lfactuarymodeSchema getSchema()
    {
        lfactuarymodeSchema alfactuarymodeSchema = new lfactuarymodeSchema();
        alfactuarymodeSchema.setSchema(this);
        return alfactuarymodeSchema;
    }

    public lfactuarymodeDB getDB()
    {
        lfactuarymodeDB aDBOper = new lfactuarymodeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prplfactuarymode描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ItemCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ItemName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(itemrelatype)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(relaitemcode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(relaitemname)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(isprovideflag));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prplfactuarymode>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            itemrelatype = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            relaitemcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            relaitemname = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            isprovideflag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                           SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "lfactuarymodeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ItemCode));
        }
        if (FCode.equals("ItemName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ItemName));
        }
        if (FCode.equals("itemrelatype"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(itemrelatype));
        }
        if (FCode.equals("relaitemcode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(relaitemcode));
        }
        if (FCode.equals("relaitemname"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(relaitemname));
        }
        if (FCode.equals("isprovideflag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(isprovideflag));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ItemCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ItemName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(itemrelatype);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(relaitemcode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(relaitemname);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(isprovideflag);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemCode = FValue.trim();
            }
            else
            {
                ItemCode = null;
            }
        }
        if (FCode.equals("ItemName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemName = FValue.trim();
            }
            else
            {
                ItemName = null;
            }
        }
        if (FCode.equals("itemrelatype"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                itemrelatype = FValue.trim();
            }
            else
            {
                itemrelatype = null;
            }
        }
        if (FCode.equals("relaitemcode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                relaitemcode = FValue.trim();
            }
            else
            {
                relaitemcode = null;
            }
        }
        if (FCode.equals("relaitemname"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                relaitemname = FValue.trim();
            }
            else
            {
                relaitemname = null;
            }
        }
        if (FCode.equals("isprovideflag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                isprovideflag = FValue.trim();
            }
            else
            {
                isprovideflag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        lfactuarymodeSchema other = (lfactuarymodeSchema) otherObject;
        return
                ItemCode.equals(other.getItemCode())
                && ItemName.equals(other.getItemName())
                && itemrelatype.equals(other.getitemrelatype())
                && relaitemcode.equals(other.getrelaitemcode())
                && relaitemname.equals(other.getrelaitemname())
                && isprovideflag.equals(other.getisprovideflag());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ItemCode"))
        {
            return 0;
        }
        if (strFieldName.equals("ItemName"))
        {
            return 1;
        }
        if (strFieldName.equals("itemrelatype"))
        {
            return 2;
        }
        if (strFieldName.equals("relaitemcode"))
        {
            return 3;
        }
        if (strFieldName.equals("relaitemname"))
        {
            return 4;
        }
        if (strFieldName.equals("isprovideflag"))
        {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ItemCode";
                break;
            case 1:
                strFieldName = "ItemName";
                break;
            case 2:
                strFieldName = "itemrelatype";
                break;
            case 3:
                strFieldName = "relaitemcode";
                break;
            case 4:
                strFieldName = "relaitemname";
                break;
            case 5:
                strFieldName = "isprovideflag";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("itemrelatype"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("relaitemcode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("relaitemname"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("isprovideflag"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
