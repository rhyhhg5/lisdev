/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCTempCustomerDetail2011DB;

/*
 * <p>ClassName: LCTempCustomerDetail2011Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 集团客户提数数据临时表
 * @CreateDate：2011-10-17
 */
public class LCTempCustomerDetail2011Schema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerNo;
	/** 日期 */
	private String CstDate;
	/** 公司编码 */
	private String CompCod;
	/** 客户编号 */
	private String CstNo;
	/** 证件号码 */
	private String CstID;
	/** 名称 */
	private String CstName;
	/** 客户类型 */
	private String CstType;
	/** 出生日期 */
	private String CstBirthday;
	/** 投保险类 */
	private String RiskType;
	/** 机构代码 */
	private String CstZone;
	/** 客户状态 */
	private String CstState;
	/** 个团标志 */
	private String CstFlag;
	/** 保单号码 */
	private String ContNo;
	/** 生效日期 */
	private Date CValiDate;
	/** 合同终止日期 */
	private Date CInValiDate;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCTempCustomerDetail2011Schema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "SerNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCTempCustomerDetail2011Schema cloned = (LCTempCustomerDetail2011Schema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerNo()
	{
		return SerNo;
	}
	public void setSerNo(String aSerNo)
	{
		SerNo = aSerNo;
	}
	public String getCstDate()
	{
		return CstDate;
	}
	public void setCstDate(String aCstDate)
	{
		CstDate = aCstDate;
	}
	public String getCompCod()
	{
		return CompCod;
	}
	public void setCompCod(String aCompCod)
	{
		CompCod = aCompCod;
	}
	public String getCstNo()
	{
		return CstNo;
	}
	public void setCstNo(String aCstNo)
	{
		CstNo = aCstNo;
	}
	public String getCstID()
	{
		return CstID;
	}
	public void setCstID(String aCstID)
	{
		CstID = aCstID;
	}
	public String getCstName()
	{
		return CstName;
	}
	public void setCstName(String aCstName)
	{
		CstName = aCstName;
	}
	public String getCstType()
	{
		return CstType;
	}
	public void setCstType(String aCstType)
	{
		CstType = aCstType;
	}
	public String getCstBirthday()
	{
		return CstBirthday;
	}
	public void setCstBirthday(String aCstBirthday)
	{
		CstBirthday = aCstBirthday;
	}
	public String getRiskType()
	{
		return RiskType;
	}
	public void setRiskType(String aRiskType)
	{
		RiskType = aRiskType;
	}
	public String getCstZone()
	{
		return CstZone;
	}
	public void setCstZone(String aCstZone)
	{
		CstZone = aCstZone;
	}
	public String getCstState()
	{
		return CstState;
	}
	public void setCstState(String aCstState)
	{
		CstState = aCstState;
	}
	public String getCstFlag()
	{
		return CstFlag;
	}
	public void setCstFlag(String aCstFlag)
	{
		CstFlag = aCstFlag;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public String getCInValiDate()
	{
		if( CInValiDate != null )
			return fDate.getString(CInValiDate);
		else
			return null;
	}
	public void setCInValiDate(Date aCInValiDate)
	{
		CInValiDate = aCInValiDate;
	}
	public void setCInValiDate(String aCInValiDate)
	{
		if (aCInValiDate != null && !aCInValiDate.equals("") )
		{
			CInValiDate = fDate.getDate( aCInValiDate );
		}
		else
			CInValiDate = null;
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LCTempCustomerDetail2011Schema 对象给 Schema 赋值
	* @param: aLCTempCustomerDetail2011Schema LCTempCustomerDetail2011Schema
	**/
	public void setSchema(LCTempCustomerDetail2011Schema aLCTempCustomerDetail2011Schema)
	{
		this.SerNo = aLCTempCustomerDetail2011Schema.getSerNo();
		this.CstDate = aLCTempCustomerDetail2011Schema.getCstDate();
		this.CompCod = aLCTempCustomerDetail2011Schema.getCompCod();
		this.CstNo = aLCTempCustomerDetail2011Schema.getCstNo();
		this.CstID = aLCTempCustomerDetail2011Schema.getCstID();
		this.CstName = aLCTempCustomerDetail2011Schema.getCstName();
		this.CstType = aLCTempCustomerDetail2011Schema.getCstType();
		this.CstBirthday = aLCTempCustomerDetail2011Schema.getCstBirthday();
		this.RiskType = aLCTempCustomerDetail2011Schema.getRiskType();
		this.CstZone = aLCTempCustomerDetail2011Schema.getCstZone();
		this.CstState = aLCTempCustomerDetail2011Schema.getCstState();
		this.CstFlag = aLCTempCustomerDetail2011Schema.getCstFlag();
		this.ContNo = aLCTempCustomerDetail2011Schema.getContNo();
		this.CValiDate = fDate.getDate( aLCTempCustomerDetail2011Schema.getCValiDate());
		this.CInValiDate = fDate.getDate( aLCTempCustomerDetail2011Schema.getCInValiDate());
		this.MakeDate = fDate.getDate( aLCTempCustomerDetail2011Schema.getMakeDate());
		this.MakeTime = aLCTempCustomerDetail2011Schema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCTempCustomerDetail2011Schema.getModifyDate());
		this.ModifyTime = aLCTempCustomerDetail2011Schema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerNo") == null )
				this.SerNo = null;
			else
				this.SerNo = rs.getString("SerNo").trim();

			if( rs.getString("CstDate") == null )
				this.CstDate = null;
			else
				this.CstDate = rs.getString("CstDate").trim();

			if( rs.getString("CompCod") == null )
				this.CompCod = null;
			else
				this.CompCod = rs.getString("CompCod").trim();

			if( rs.getString("CstNo") == null )
				this.CstNo = null;
			else
				this.CstNo = rs.getString("CstNo").trim();

			if( rs.getString("CstID") == null )
				this.CstID = null;
			else
				this.CstID = rs.getString("CstID").trim();

			if( rs.getString("CstName") == null )
				this.CstName = null;
			else
				this.CstName = rs.getString("CstName").trim();

			if( rs.getString("CstType") == null )
				this.CstType = null;
			else
				this.CstType = rs.getString("CstType").trim();

			if( rs.getString("CstBirthday") == null )
				this.CstBirthday = null;
			else
				this.CstBirthday = rs.getString("CstBirthday").trim();

			if( rs.getString("RiskType") == null )
				this.RiskType = null;
			else
				this.RiskType = rs.getString("RiskType").trim();

			if( rs.getString("CstZone") == null )
				this.CstZone = null;
			else
				this.CstZone = rs.getString("CstZone").trim();

			if( rs.getString("CstState") == null )
				this.CstState = null;
			else
				this.CstState = rs.getString("CstState").trim();

			if( rs.getString("CstFlag") == null )
				this.CstFlag = null;
			else
				this.CstFlag = rs.getString("CstFlag").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			this.CValiDate = rs.getDate("CValiDate");
			this.CInValiDate = rs.getDate("CInValiDate");
			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCTempCustomerDetail2011表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCTempCustomerDetail2011Schema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCTempCustomerDetail2011Schema getSchema()
	{
		LCTempCustomerDetail2011Schema aLCTempCustomerDetail2011Schema = new LCTempCustomerDetail2011Schema();
		aLCTempCustomerDetail2011Schema.setSchema(this);
		return aLCTempCustomerDetail2011Schema;
	}

	public LCTempCustomerDetail2011DB getDB()
	{
		LCTempCustomerDetail2011DB aDBOper = new LCTempCustomerDetail2011DB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCTempCustomerDetail2011描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CstDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompCod)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CstNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CstID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CstName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CstType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CstBirthday)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CstZone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CstState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CstFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CInValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCTempCustomerDetail2011>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CstDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CompCod = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CstNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CstID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CstName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CstType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			CstBirthday = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			CstZone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			CstState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			CstFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			CInValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCTempCustomerDetail2011Schema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerNo));
		}
		if (FCode.equals("CstDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CstDate));
		}
		if (FCode.equals("CompCod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompCod));
		}
		if (FCode.equals("CstNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CstNo));
		}
		if (FCode.equals("CstID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CstID));
		}
		if (FCode.equals("CstName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CstName));
		}
		if (FCode.equals("CstType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CstType));
		}
		if (FCode.equals("CstBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CstBirthday));
		}
		if (FCode.equals("RiskType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
		}
		if (FCode.equals("CstZone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CstZone));
		}
		if (FCode.equals("CstState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CstState));
		}
		if (FCode.equals("CstFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CstFlag));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("CInValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCInValiDate()));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CstDate);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CompCod);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CstNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CstID);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CstName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CstType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(CstBirthday);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(RiskType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(CstZone);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(CstState);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(CstFlag);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCInValiDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerNo = FValue.trim();
			}
			else
				SerNo = null;
		}
		if (FCode.equalsIgnoreCase("CstDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CstDate = FValue.trim();
			}
			else
				CstDate = null;
		}
		if (FCode.equalsIgnoreCase("CompCod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompCod = FValue.trim();
			}
			else
				CompCod = null;
		}
		if (FCode.equalsIgnoreCase("CstNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CstNo = FValue.trim();
			}
			else
				CstNo = null;
		}
		if (FCode.equalsIgnoreCase("CstID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CstID = FValue.trim();
			}
			else
				CstID = null;
		}
		if (FCode.equalsIgnoreCase("CstName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CstName = FValue.trim();
			}
			else
				CstName = null;
		}
		if (FCode.equalsIgnoreCase("CstType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CstType = FValue.trim();
			}
			else
				CstType = null;
		}
		if (FCode.equalsIgnoreCase("CstBirthday"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CstBirthday = FValue.trim();
			}
			else
				CstBirthday = null;
		}
		if (FCode.equalsIgnoreCase("RiskType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType = FValue.trim();
			}
			else
				RiskType = null;
		}
		if (FCode.equalsIgnoreCase("CstZone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CstZone = FValue.trim();
			}
			else
				CstZone = null;
		}
		if (FCode.equalsIgnoreCase("CstState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CstState = FValue.trim();
			}
			else
				CstState = null;
		}
		if (FCode.equalsIgnoreCase("CstFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CstFlag = FValue.trim();
			}
			else
				CstFlag = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("CInValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CInValiDate = fDate.getDate( FValue );
			}
			else
				CInValiDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCTempCustomerDetail2011Schema other = (LCTempCustomerDetail2011Schema)otherObject;
		return
			(SerNo == null ? other.getSerNo() == null : SerNo.equals(other.getSerNo()))
			&& (CstDate == null ? other.getCstDate() == null : CstDate.equals(other.getCstDate()))
			&& (CompCod == null ? other.getCompCod() == null : CompCod.equals(other.getCompCod()))
			&& (CstNo == null ? other.getCstNo() == null : CstNo.equals(other.getCstNo()))
			&& (CstID == null ? other.getCstID() == null : CstID.equals(other.getCstID()))
			&& (CstName == null ? other.getCstName() == null : CstName.equals(other.getCstName()))
			&& (CstType == null ? other.getCstType() == null : CstType.equals(other.getCstType()))
			&& (CstBirthday == null ? other.getCstBirthday() == null : CstBirthday.equals(other.getCstBirthday()))
			&& (RiskType == null ? other.getRiskType() == null : RiskType.equals(other.getRiskType()))
			&& (CstZone == null ? other.getCstZone() == null : CstZone.equals(other.getCstZone()))
			&& (CstState == null ? other.getCstState() == null : CstState.equals(other.getCstState()))
			&& (CstFlag == null ? other.getCstFlag() == null : CstFlag.equals(other.getCstFlag()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& (CInValiDate == null ? other.getCInValiDate() == null : fDate.getString(CInValiDate).equals(other.getCInValiDate()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return 0;
		}
		if( strFieldName.equals("CstDate") ) {
			return 1;
		}
		if( strFieldName.equals("CompCod") ) {
			return 2;
		}
		if( strFieldName.equals("CstNo") ) {
			return 3;
		}
		if( strFieldName.equals("CstID") ) {
			return 4;
		}
		if( strFieldName.equals("CstName") ) {
			return 5;
		}
		if( strFieldName.equals("CstType") ) {
			return 6;
		}
		if( strFieldName.equals("CstBirthday") ) {
			return 7;
		}
		if( strFieldName.equals("RiskType") ) {
			return 8;
		}
		if( strFieldName.equals("CstZone") ) {
			return 9;
		}
		if( strFieldName.equals("CstState") ) {
			return 10;
		}
		if( strFieldName.equals("CstFlag") ) {
			return 11;
		}
		if( strFieldName.equals("ContNo") ) {
			return 12;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 13;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return 14;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 15;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 16;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 17;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerNo";
				break;
			case 1:
				strFieldName = "CstDate";
				break;
			case 2:
				strFieldName = "CompCod";
				break;
			case 3:
				strFieldName = "CstNo";
				break;
			case 4:
				strFieldName = "CstID";
				break;
			case 5:
				strFieldName = "CstName";
				break;
			case 6:
				strFieldName = "CstType";
				break;
			case 7:
				strFieldName = "CstBirthday";
				break;
			case 8:
				strFieldName = "RiskType";
				break;
			case 9:
				strFieldName = "CstZone";
				break;
			case 10:
				strFieldName = "CstState";
				break;
			case 11:
				strFieldName = "CstFlag";
				break;
			case 12:
				strFieldName = "ContNo";
				break;
			case 13:
				strFieldName = "CValiDate";
				break;
			case 14:
				strFieldName = "CInValiDate";
				break;
			case 15:
				strFieldName = "MakeDate";
				break;
			case 16:
				strFieldName = "MakeTime";
				break;
			case 17:
				strFieldName = "ModifyDate";
				break;
			case 18:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CstDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompCod") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CstNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CstID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CstName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CstType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CstBirthday") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CstZone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CstState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CstFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CInValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
