/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LPDiskImportDB;

/*
 * <p>ClassName: LPDiskImportSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LPDiskImport001
 * @CreateDate：2019-04-12
 */
public class LPDiskImportSchema implements Schema, Cloneable
{
	// @Field
	/** 保全号 */
	private String EdorNo;
	/** 保全类型 */
	private String EdorType;
	/** 集体合同号码 */
	private String GrpContNo;
	/** 被保人序号 */
	private String SerialNo;
	/** 状态 */
	private String State;
	/** 无效原因 */
	private String ErrorReason;
	/** 险种序号 */
	private String RiskSeqNo;
	/** 被保人客户号 */
	private String InsuredNo;
	/** 在职/退休 */
	private String Retire;
	/** 员工姓名 */
	private String EmployeeName;
	/** 被保人姓名 */
	private String InsuredName;
	/** 与员工关系 */
	private String Relation;
	/** 性别 */
	private String Sex;
	/** 出生日期 */
	private Date Birthday;
	/** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 保险计划 */
	private String ContPlanCode;
	/** 职业类别 */
	private String OccupationType;
	/** 理赔金转帐银行 */
	private String BankCode;
	/** 帐号 */
	private String BankAccNo;
	/** 户名 */
	private String AccName;
	/** 导入金额 */
	private double Money;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 保全生效日期 */
	private Date EdorValiDate;
	/** 保全处理后金额 */
	private double GetMoney;
	/** 导入金额2 */
	private String Money2;
	/** 帐户金额 */
	private String InsuAccBala;
	/** 帐户利息 */
	private double InsuAccInterest;
	/** 其它证件类型 */
	private String OthIDType;
	/** 其它证件号码 */
	private String OthIDNo;
	/** 导入文件名 */
	private String ImportFileName;
	/** 投保人缴费 */
	private double AppntPrem;
	/** 个人协议缴费 */
	private double PersonPrem;
	/** 个人自愿缴费 */
	private double PersonOwnPrem;
	/** 联系电话 */
	private String Phone;
	/** 职业代码 */
	private String OccupationCode;
	/** 岗位 */
	private String Position;
	/** 国籍 */
	private String NativePlace;
	/** 国家/地区 */
	private String NativeCity;

	public static final int FIELDNUM = 43;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LPDiskImportSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "EdorNo";
		pk[1] = "EdorType";
		pk[2] = "GrpContNo";
		pk[3] = "SerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LPDiskImportSchema cloned = (LPDiskImportSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getEdorType()
	{
		return EdorType;
	}
	public void setEdorType(String aEdorType)
	{
		EdorType = aEdorType;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getErrorReason()
	{
		return ErrorReason;
	}
	public void setErrorReason(String aErrorReason)
	{
		ErrorReason = aErrorReason;
	}
	public String getRiskSeqNo()
	{
		return RiskSeqNo;
	}
	public void setRiskSeqNo(String aRiskSeqNo)
	{
		RiskSeqNo = aRiskSeqNo;
	}
	public String getInsuredNo()
	{
		return InsuredNo;
	}
	public void setInsuredNo(String aInsuredNo)
	{
		InsuredNo = aInsuredNo;
	}
	public String getRetire()
	{
		return Retire;
	}
	public void setRetire(String aRetire)
	{
		Retire = aRetire;
	}
	public String getEmployeeName()
	{
		return EmployeeName;
	}
	public void setEmployeeName(String aEmployeeName)
	{
		EmployeeName = aEmployeeName;
	}
	public String getInsuredName()
	{
		return InsuredName;
	}
	public void setInsuredName(String aInsuredName)
	{
		InsuredName = aInsuredName;
	}
	public String getRelation()
	{
		return Relation;
	}
	public void setRelation(String aRelation)
	{
		Relation = aRelation;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
		Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getContPlanCode()
	{
		return ContPlanCode;
	}
	public void setContPlanCode(String aContPlanCode)
	{
		ContPlanCode = aContPlanCode;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public double getMoney()
	{
		return Money;
	}
	public void setMoney(double aMoney)
	{
		Money = Arith.round(aMoney,2);
	}
	public void setMoney(String aMoney)
	{
		if (aMoney != null && !aMoney.equals(""))
		{
			Double tDouble = new Double(aMoney);
			double d = tDouble.doubleValue();
                Money = Arith.round(d,2);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getEdorValiDate()
	{
		if( EdorValiDate != null )
			return fDate.getString(EdorValiDate);
		else
			return null;
	}
	public void setEdorValiDate(Date aEdorValiDate)
	{
		EdorValiDate = aEdorValiDate;
	}
	public void setEdorValiDate(String aEdorValiDate)
	{
		if (aEdorValiDate != null && !aEdorValiDate.equals("") )
		{
			EdorValiDate = fDate.getDate( aEdorValiDate );
		}
		else
			EdorValiDate = null;
	}

	public double getGetMoney()
	{
		return GetMoney;
	}
	public void setGetMoney(double aGetMoney)
	{
		GetMoney = Arith.round(aGetMoney,2);
	}
	public void setGetMoney(String aGetMoney)
	{
		if (aGetMoney != null && !aGetMoney.equals(""))
		{
			Double tDouble = new Double(aGetMoney);
			double d = tDouble.doubleValue();
                GetMoney = Arith.round(d,2);
		}
	}

	public String getMoney2()
	{
		return Money2;
	}
	public void setMoney2(String aMoney2)
	{
		Money2 = aMoney2;
	}
	public String getInsuAccBala()
	{
		return InsuAccBala;
	}
	public void setInsuAccBala(String aInsuAccBala)
	{
		InsuAccBala = aInsuAccBala;
	}
	public double getInsuAccInterest()
	{
		return InsuAccInterest;
	}
	public void setInsuAccInterest(double aInsuAccInterest)
	{
		InsuAccInterest = Arith.round(aInsuAccInterest,2);
	}
	public void setInsuAccInterest(String aInsuAccInterest)
	{
		if (aInsuAccInterest != null && !aInsuAccInterest.equals(""))
		{
			Double tDouble = new Double(aInsuAccInterest);
			double d = tDouble.doubleValue();
                InsuAccInterest = Arith.round(d,2);
		}
	}

	public String getOthIDType()
	{
		return OthIDType;
	}
	public void setOthIDType(String aOthIDType)
	{
		OthIDType = aOthIDType;
	}
	public String getOthIDNo()
	{
		return OthIDNo;
	}
	public void setOthIDNo(String aOthIDNo)
	{
		OthIDNo = aOthIDNo;
	}
	public String getImportFileName()
	{
		return ImportFileName;
	}
	public void setImportFileName(String aImportFileName)
	{
		ImportFileName = aImportFileName;
	}
	public double getAppntPrem()
	{
		return AppntPrem;
	}
	public void setAppntPrem(double aAppntPrem)
	{
		AppntPrem = Arith.round(aAppntPrem,2);
	}
	public void setAppntPrem(String aAppntPrem)
	{
		if (aAppntPrem != null && !aAppntPrem.equals(""))
		{
			Double tDouble = new Double(aAppntPrem);
			double d = tDouble.doubleValue();
                AppntPrem = Arith.round(d,2);
		}
	}

	public double getPersonPrem()
	{
		return PersonPrem;
	}
	public void setPersonPrem(double aPersonPrem)
	{
		PersonPrem = Arith.round(aPersonPrem,2);
	}
	public void setPersonPrem(String aPersonPrem)
	{
		if (aPersonPrem != null && !aPersonPrem.equals(""))
		{
			Double tDouble = new Double(aPersonPrem);
			double d = tDouble.doubleValue();
                PersonPrem = Arith.round(d,2);
		}
	}

	public double getPersonOwnPrem()
	{
		return PersonOwnPrem;
	}
	public void setPersonOwnPrem(double aPersonOwnPrem)
	{
		PersonOwnPrem = Arith.round(aPersonOwnPrem,2);
	}
	public void setPersonOwnPrem(String aPersonOwnPrem)
	{
		if (aPersonOwnPrem != null && !aPersonOwnPrem.equals(""))
		{
			Double tDouble = new Double(aPersonOwnPrem);
			double d = tDouble.doubleValue();
                PersonOwnPrem = Arith.round(d,2);
		}
	}

	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getOccupationCode()
	{
		return OccupationCode;
	}
	public void setOccupationCode(String aOccupationCode)
	{
		OccupationCode = aOccupationCode;
	}
	public String getPosition()
	{
		return Position;
	}
	public void setPosition(String aPosition)
	{
		Position = aPosition;
	}
	public String getNativePlace()
	{
		return NativePlace;
	}
	public void setNativePlace(String aNativePlace)
	{
		NativePlace = aNativePlace;
	}
	public String getNativeCity()
	{
		return NativeCity;
	}
	public void setNativeCity(String aNativeCity)
	{
		NativeCity = aNativeCity;
	}

	/**
	* 使用另外一个 LPDiskImportSchema 对象给 Schema 赋值
	* @param: aLPDiskImportSchema LPDiskImportSchema
	**/
	public void setSchema(LPDiskImportSchema aLPDiskImportSchema)
	{
		this.EdorNo = aLPDiskImportSchema.getEdorNo();
		this.EdorType = aLPDiskImportSchema.getEdorType();
		this.GrpContNo = aLPDiskImportSchema.getGrpContNo();
		this.SerialNo = aLPDiskImportSchema.getSerialNo();
		this.State = aLPDiskImportSchema.getState();
		this.ErrorReason = aLPDiskImportSchema.getErrorReason();
		this.RiskSeqNo = aLPDiskImportSchema.getRiskSeqNo();
		this.InsuredNo = aLPDiskImportSchema.getInsuredNo();
		this.Retire = aLPDiskImportSchema.getRetire();
		this.EmployeeName = aLPDiskImportSchema.getEmployeeName();
		this.InsuredName = aLPDiskImportSchema.getInsuredName();
		this.Relation = aLPDiskImportSchema.getRelation();
		this.Sex = aLPDiskImportSchema.getSex();
		this.Birthday = fDate.getDate( aLPDiskImportSchema.getBirthday());
		this.IDType = aLPDiskImportSchema.getIDType();
		this.IDNo = aLPDiskImportSchema.getIDNo();
		this.ContPlanCode = aLPDiskImportSchema.getContPlanCode();
		this.OccupationType = aLPDiskImportSchema.getOccupationType();
		this.BankCode = aLPDiskImportSchema.getBankCode();
		this.BankAccNo = aLPDiskImportSchema.getBankAccNo();
		this.AccName = aLPDiskImportSchema.getAccName();
		this.Money = aLPDiskImportSchema.getMoney();
		this.Operator = aLPDiskImportSchema.getOperator();
		this.MakeDate = fDate.getDate( aLPDiskImportSchema.getMakeDate());
		this.MakeTime = aLPDiskImportSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLPDiskImportSchema.getModifyDate());
		this.ModifyTime = aLPDiskImportSchema.getModifyTime();
		this.EdorValiDate = fDate.getDate( aLPDiskImportSchema.getEdorValiDate());
		this.GetMoney = aLPDiskImportSchema.getGetMoney();
		this.Money2 = aLPDiskImportSchema.getMoney2();
		this.InsuAccBala = aLPDiskImportSchema.getInsuAccBala();
		this.InsuAccInterest = aLPDiskImportSchema.getInsuAccInterest();
		this.OthIDType = aLPDiskImportSchema.getOthIDType();
		this.OthIDNo = aLPDiskImportSchema.getOthIDNo();
		this.ImportFileName = aLPDiskImportSchema.getImportFileName();
		this.AppntPrem = aLPDiskImportSchema.getAppntPrem();
		this.PersonPrem = aLPDiskImportSchema.getPersonPrem();
		this.PersonOwnPrem = aLPDiskImportSchema.getPersonOwnPrem();
		this.Phone = aLPDiskImportSchema.getPhone();
		this.OccupationCode = aLPDiskImportSchema.getOccupationCode();
		this.Position = aLPDiskImportSchema.getPosition();
		this.NativePlace = aLPDiskImportSchema.getNativePlace();
		this.NativeCity = aLPDiskImportSchema.getNativeCity();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("EdorType") == null )
				this.EdorType = null;
			else
				this.EdorType = rs.getString("EdorType").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("ErrorReason") == null )
				this.ErrorReason = null;
			else
				this.ErrorReason = rs.getString("ErrorReason").trim();

			if( rs.getString("RiskSeqNo") == null )
				this.RiskSeqNo = null;
			else
				this.RiskSeqNo = rs.getString("RiskSeqNo").trim();

			if( rs.getString("InsuredNo") == null )
				this.InsuredNo = null;
			else
				this.InsuredNo = rs.getString("InsuredNo").trim();

			if( rs.getString("Retire") == null )
				this.Retire = null;
			else
				this.Retire = rs.getString("Retire").trim();

			if( rs.getString("EmployeeName") == null )
				this.EmployeeName = null;
			else
				this.EmployeeName = rs.getString("EmployeeName").trim();

			if( rs.getString("InsuredName") == null )
				this.InsuredName = null;
			else
				this.InsuredName = rs.getString("InsuredName").trim();

			if( rs.getString("Relation") == null )
				this.Relation = null;
			else
				this.Relation = rs.getString("Relation").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			this.Birthday = rs.getDate("Birthday");
			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("ContPlanCode") == null )
				this.ContPlanCode = null;
			else
				this.ContPlanCode = rs.getString("ContPlanCode").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			this.Money = rs.getDouble("Money");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.EdorValiDate = rs.getDate("EdorValiDate");
			this.GetMoney = rs.getDouble("GetMoney");
			if( rs.getString("Money2") == null )
				this.Money2 = null;
			else
				this.Money2 = rs.getString("Money2").trim();

			if( rs.getString("InsuAccBala") == null )
				this.InsuAccBala = null;
			else
				this.InsuAccBala = rs.getString("InsuAccBala").trim();

			this.InsuAccInterest = rs.getDouble("InsuAccInterest");
			if( rs.getString("OthIDType") == null )
				this.OthIDType = null;
			else
				this.OthIDType = rs.getString("OthIDType").trim();

			if( rs.getString("OthIDNo") == null )
				this.OthIDNo = null;
			else
				this.OthIDNo = rs.getString("OthIDNo").trim();

			if( rs.getString("ImportFileName") == null )
				this.ImportFileName = null;
			else
				this.ImportFileName = rs.getString("ImportFileName").trim();

			this.AppntPrem = rs.getDouble("AppntPrem");
			this.PersonPrem = rs.getDouble("PersonPrem");
			this.PersonOwnPrem = rs.getDouble("PersonOwnPrem");
			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("OccupationCode") == null )
				this.OccupationCode = null;
			else
				this.OccupationCode = rs.getString("OccupationCode").trim();

			if( rs.getString("Position") == null )
				this.Position = null;
			else
				this.Position = rs.getString("Position").trim();

			if( rs.getString("NativePlace") == null )
				this.NativePlace = null;
			else
				this.NativePlace = rs.getString("NativePlace").trim();

			if( rs.getString("NativeCity") == null )
				this.NativeCity = null;
			else
				this.NativeCity = rs.getString("NativeCity").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LPDiskImport表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LPDiskImportSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LPDiskImportSchema getSchema()
	{
		LPDiskImportSchema aLPDiskImportSchema = new LPDiskImportSchema();
		aLPDiskImportSchema.setSchema(this);
		return aLPDiskImportSchema;
	}

	public LPDiskImportDB getDB()
	{
		LPDiskImportDB aDBOper = new LPDiskImportDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPDiskImport描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ErrorReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskSeqNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Retire)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EmployeeName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Relation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContPlanCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Money));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EdorValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GetMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Money2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuAccBala)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InsuAccInterest));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OthIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ImportFileName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppntPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PersonPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PersonOwnPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Position)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativePlace)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NativeCity));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPDiskImport>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ErrorReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			RiskSeqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			InsuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Retire = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			EmployeeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Relation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ContPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Money = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			EdorValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			GetMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			Money2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			InsuAccBala = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			InsuAccInterest = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			OthIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			OthIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			ImportFileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			AppntPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).doubleValue();
			PersonPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).doubleValue();
			PersonOwnPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,38,SysConst.PACKAGESPILTER))).doubleValue();
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			OccupationCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			Position = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			NativePlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			NativeCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LPDiskImportSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("EdorType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("ErrorReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorReason));
		}
		if (FCode.equals("RiskSeqNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskSeqNo));
		}
		if (FCode.equals("InsuredNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredNo));
		}
		if (FCode.equals("Retire"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Retire));
		}
		if (FCode.equals("EmployeeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EmployeeName));
		}
		if (FCode.equals("InsuredName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
		}
		if (FCode.equals("Relation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("ContPlanCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContPlanCode));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("Money"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Money));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("EdorValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEdorValiDate()));
		}
		if (FCode.equals("GetMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetMoney));
		}
		if (FCode.equals("Money2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Money2));
		}
		if (FCode.equals("InsuAccBala"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccBala));
		}
		if (FCode.equals("InsuAccInterest"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccInterest));
		}
		if (FCode.equals("OthIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDType));
		}
		if (FCode.equals("OthIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OthIDNo));
		}
		if (FCode.equals("ImportFileName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ImportFileName));
		}
		if (FCode.equals("AppntPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntPrem));
		}
		if (FCode.equals("PersonPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersonPrem));
		}
		if (FCode.equals("PersonOwnPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersonOwnPrem));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("OccupationCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationCode));
		}
		if (FCode.equals("Position"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Position));
		}
		if (FCode.equals("NativePlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativePlace));
		}
		if (FCode.equals("NativeCity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NativeCity));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(EdorType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ErrorReason);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(RiskSeqNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(InsuredNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Retire);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(EmployeeName);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(InsuredName);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Relation);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ContPlanCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 21:
				strFieldValue = String.valueOf(Money);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEdorValiDate()));
				break;
			case 28:
				strFieldValue = String.valueOf(GetMoney);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(Money2);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(InsuAccBala);
				break;
			case 31:
				strFieldValue = String.valueOf(InsuAccInterest);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(OthIDType);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(OthIDNo);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(ImportFileName);
				break;
			case 35:
				strFieldValue = String.valueOf(AppntPrem);
				break;
			case 36:
				strFieldValue = String.valueOf(PersonPrem);
				break;
			case 37:
				strFieldValue = String.valueOf(PersonOwnPrem);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(OccupationCode);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(Position);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(NativePlace);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(NativeCity);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorType = FValue.trim();
			}
			else
				EdorType = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("ErrorReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorReason = FValue.trim();
			}
			else
				ErrorReason = null;
		}
		if (FCode.equalsIgnoreCase("RiskSeqNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskSeqNo = FValue.trim();
			}
			else
				RiskSeqNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredNo = FValue.trim();
			}
			else
				InsuredNo = null;
		}
		if (FCode.equalsIgnoreCase("Retire"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Retire = FValue.trim();
			}
			else
				Retire = null;
		}
		if (FCode.equalsIgnoreCase("EmployeeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EmployeeName = FValue.trim();
			}
			else
				EmployeeName = null;
		}
		if (FCode.equalsIgnoreCase("InsuredName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredName = FValue.trim();
			}
			else
				InsuredName = null;
		}
		if (FCode.equalsIgnoreCase("Relation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Relation = FValue.trim();
			}
			else
				Relation = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("ContPlanCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContPlanCode = FValue.trim();
			}
			else
				ContPlanCode = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("Money"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Money = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("EdorValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EdorValiDate = fDate.getDate( FValue );
			}
			else
				EdorValiDate = null;
		}
		if (FCode.equalsIgnoreCase("GetMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GetMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("Money2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Money2 = FValue.trim();
			}
			else
				Money2 = null;
		}
		if (FCode.equalsIgnoreCase("InsuAccBala"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuAccBala = FValue.trim();
			}
			else
				InsuAccBala = null;
		}
		if (FCode.equalsIgnoreCase("InsuAccInterest"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				InsuAccInterest = d;
			}
		}
		if (FCode.equalsIgnoreCase("OthIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDType = FValue.trim();
			}
			else
				OthIDType = null;
		}
		if (FCode.equalsIgnoreCase("OthIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OthIDNo = FValue.trim();
			}
			else
				OthIDNo = null;
		}
		if (FCode.equalsIgnoreCase("ImportFileName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ImportFileName = FValue.trim();
			}
			else
				ImportFileName = null;
		}
		if (FCode.equalsIgnoreCase("AppntPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AppntPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("PersonPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PersonPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("PersonOwnPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PersonOwnPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("OccupationCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationCode = FValue.trim();
			}
			else
				OccupationCode = null;
		}
		if (FCode.equalsIgnoreCase("Position"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Position = FValue.trim();
			}
			else
				Position = null;
		}
		if (FCode.equalsIgnoreCase("NativePlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativePlace = FValue.trim();
			}
			else
				NativePlace = null;
		}
		if (FCode.equalsIgnoreCase("NativeCity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NativeCity = FValue.trim();
			}
			else
				NativeCity = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LPDiskImportSchema other = (LPDiskImportSchema)otherObject;
		return
			(EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (EdorType == null ? other.getEdorType() == null : EdorType.equals(other.getEdorType()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (ErrorReason == null ? other.getErrorReason() == null : ErrorReason.equals(other.getErrorReason()))
			&& (RiskSeqNo == null ? other.getRiskSeqNo() == null : RiskSeqNo.equals(other.getRiskSeqNo()))
			&& (InsuredNo == null ? other.getInsuredNo() == null : InsuredNo.equals(other.getInsuredNo()))
			&& (Retire == null ? other.getRetire() == null : Retire.equals(other.getRetire()))
			&& (EmployeeName == null ? other.getEmployeeName() == null : EmployeeName.equals(other.getEmployeeName()))
			&& (InsuredName == null ? other.getInsuredName() == null : InsuredName.equals(other.getInsuredName()))
			&& (Relation == null ? other.getRelation() == null : Relation.equals(other.getRelation()))
			&& (Sex == null ? other.getSex() == null : Sex.equals(other.getSex()))
			&& (Birthday == null ? other.getBirthday() == null : fDate.getString(Birthday).equals(other.getBirthday()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (ContPlanCode == null ? other.getContPlanCode() == null : ContPlanCode.equals(other.getContPlanCode()))
			&& (OccupationType == null ? other.getOccupationType() == null : OccupationType.equals(other.getOccupationType()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& Money == other.getMoney()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (EdorValiDate == null ? other.getEdorValiDate() == null : fDate.getString(EdorValiDate).equals(other.getEdorValiDate()))
			&& GetMoney == other.getGetMoney()
			&& (Money2 == null ? other.getMoney2() == null : Money2.equals(other.getMoney2()))
			&& (InsuAccBala == null ? other.getInsuAccBala() == null : InsuAccBala.equals(other.getInsuAccBala()))
			&& InsuAccInterest == other.getInsuAccInterest()
			&& (OthIDType == null ? other.getOthIDType() == null : OthIDType.equals(other.getOthIDType()))
			&& (OthIDNo == null ? other.getOthIDNo() == null : OthIDNo.equals(other.getOthIDNo()))
			&& (ImportFileName == null ? other.getImportFileName() == null : ImportFileName.equals(other.getImportFileName()))
			&& AppntPrem == other.getAppntPrem()
			&& PersonPrem == other.getPersonPrem()
			&& PersonOwnPrem == other.getPersonOwnPrem()
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (OccupationCode == null ? other.getOccupationCode() == null : OccupationCode.equals(other.getOccupationCode()))
			&& (Position == null ? other.getPosition() == null : Position.equals(other.getPosition()))
			&& (NativePlace == null ? other.getNativePlace() == null : NativePlace.equals(other.getNativePlace()))
			&& (NativeCity == null ? other.getNativeCity() == null : NativeCity.equals(other.getNativeCity()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("EdorType") ) {
			return 1;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 2;
		}
		if( strFieldName.equals("SerialNo") ) {
			return 3;
		}
		if( strFieldName.equals("State") ) {
			return 4;
		}
		if( strFieldName.equals("ErrorReason") ) {
			return 5;
		}
		if( strFieldName.equals("RiskSeqNo") ) {
			return 6;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return 7;
		}
		if( strFieldName.equals("Retire") ) {
			return 8;
		}
		if( strFieldName.equals("EmployeeName") ) {
			return 9;
		}
		if( strFieldName.equals("InsuredName") ) {
			return 10;
		}
		if( strFieldName.equals("Relation") ) {
			return 11;
		}
		if( strFieldName.equals("Sex") ) {
			return 12;
		}
		if( strFieldName.equals("Birthday") ) {
			return 13;
		}
		if( strFieldName.equals("IDType") ) {
			return 14;
		}
		if( strFieldName.equals("IDNo") ) {
			return 15;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return 16;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 17;
		}
		if( strFieldName.equals("BankCode") ) {
			return 18;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 19;
		}
		if( strFieldName.equals("AccName") ) {
			return 20;
		}
		if( strFieldName.equals("Money") ) {
			return 21;
		}
		if( strFieldName.equals("Operator") ) {
			return 22;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 23;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 24;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 25;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 26;
		}
		if( strFieldName.equals("EdorValiDate") ) {
			return 27;
		}
		if( strFieldName.equals("GetMoney") ) {
			return 28;
		}
		if( strFieldName.equals("Money2") ) {
			return 29;
		}
		if( strFieldName.equals("InsuAccBala") ) {
			return 30;
		}
		if( strFieldName.equals("InsuAccInterest") ) {
			return 31;
		}
		if( strFieldName.equals("OthIDType") ) {
			return 32;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return 33;
		}
		if( strFieldName.equals("ImportFileName") ) {
			return 34;
		}
		if( strFieldName.equals("AppntPrem") ) {
			return 35;
		}
		if( strFieldName.equals("PersonPrem") ) {
			return 36;
		}
		if( strFieldName.equals("PersonOwnPrem") ) {
			return 37;
		}
		if( strFieldName.equals("Phone") ) {
			return 38;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return 39;
		}
		if( strFieldName.equals("Position") ) {
			return 40;
		}
		if( strFieldName.equals("NativePlace") ) {
			return 41;
		}
		if( strFieldName.equals("NativeCity") ) {
			return 42;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "EdorType";
				break;
			case 2:
				strFieldName = "GrpContNo";
				break;
			case 3:
				strFieldName = "SerialNo";
				break;
			case 4:
				strFieldName = "State";
				break;
			case 5:
				strFieldName = "ErrorReason";
				break;
			case 6:
				strFieldName = "RiskSeqNo";
				break;
			case 7:
				strFieldName = "InsuredNo";
				break;
			case 8:
				strFieldName = "Retire";
				break;
			case 9:
				strFieldName = "EmployeeName";
				break;
			case 10:
				strFieldName = "InsuredName";
				break;
			case 11:
				strFieldName = "Relation";
				break;
			case 12:
				strFieldName = "Sex";
				break;
			case 13:
				strFieldName = "Birthday";
				break;
			case 14:
				strFieldName = "IDType";
				break;
			case 15:
				strFieldName = "IDNo";
				break;
			case 16:
				strFieldName = "ContPlanCode";
				break;
			case 17:
				strFieldName = "OccupationType";
				break;
			case 18:
				strFieldName = "BankCode";
				break;
			case 19:
				strFieldName = "BankAccNo";
				break;
			case 20:
				strFieldName = "AccName";
				break;
			case 21:
				strFieldName = "Money";
				break;
			case 22:
				strFieldName = "Operator";
				break;
			case 23:
				strFieldName = "MakeDate";
				break;
			case 24:
				strFieldName = "MakeTime";
				break;
			case 25:
				strFieldName = "ModifyDate";
				break;
			case 26:
				strFieldName = "ModifyTime";
				break;
			case 27:
				strFieldName = "EdorValiDate";
				break;
			case 28:
				strFieldName = "GetMoney";
				break;
			case 29:
				strFieldName = "Money2";
				break;
			case 30:
				strFieldName = "InsuAccBala";
				break;
			case 31:
				strFieldName = "InsuAccInterest";
				break;
			case 32:
				strFieldName = "OthIDType";
				break;
			case 33:
				strFieldName = "OthIDNo";
				break;
			case 34:
				strFieldName = "ImportFileName";
				break;
			case 35:
				strFieldName = "AppntPrem";
				break;
			case 36:
				strFieldName = "PersonPrem";
				break;
			case 37:
				strFieldName = "PersonOwnPrem";
				break;
			case 38:
				strFieldName = "Phone";
				break;
			case 39:
				strFieldName = "OccupationCode";
				break;
			case 40:
				strFieldName = "Position";
				break;
			case 41:
				strFieldName = "NativePlace";
				break;
			case 42:
				strFieldName = "NativeCity";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskSeqNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Retire") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EmployeeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Relation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContPlanCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Money") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GetMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Money2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuAccBala") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuAccInterest") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("OthIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OthIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ImportFileName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PersonPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PersonOwnPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Position") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NativePlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NativeCity") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 36:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 37:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
