/*
 * <p>ClassName: LALinkWageSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LALinkWageDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

public class LALinkWageSchema implements Schema
{
    // @Field
    /** 地区编码 */
    private String ManageCom;
    /** 代理人职级 */
    private String AgentGrade;
    /** 资金类型 */
    private String WageCode;
    /** 发放期间类型 */
    private String PayPeriodType;
    /** 发放月度 */
    private String PayMonth;
    /** 发放方式 */
    private String PayMode;
    /** 发放间隔 */
    private int PayInterval;
    /** 发放时间限制月数 */
    private int MonthLimit;
    /** 标准比率 */
    private double StandRate;
    /** 标准金额 */
    private double StandMoney;
    /** 标准金额１ */
    private double StandMoney1;
    /** 基数 */
    private double BaseRate;
    /** 基数1 */
    private double BaseRate1;
    /** 预留 */
    private double P01;
    /** 预留1 */
    private double P02;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 版本号 */
    private String AreaType;

    public static final int FIELDNUM = 21; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LALinkWageSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[6];
        pk[0] = "ManageCom";
        pk[1] = "AgentGrade";
        pk[2] = "WageCode";
        pk[3] = "PayPeriodType";
        pk[4] = "PayMonth";
        pk[5] = "AreaType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAgentGrade()
    {
        if (AgentGrade != null && !AgentGrade.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getWageCode()
    {
        if (WageCode != null && !WageCode.equals("") && SysConst.CHANGECHARSET)
        {
            WageCode = StrTool.unicodeToGBK(WageCode);
        }
        return WageCode;
    }

    public void setWageCode(String aWageCode)
    {
        WageCode = aWageCode;
    }

    public String getPayPeriodType()
    {
        if (PayPeriodType != null && !PayPeriodType.equals("") &&
            SysConst.CHANGECHARSET)
        {
            PayPeriodType = StrTool.unicodeToGBK(PayPeriodType);
        }
        return PayPeriodType;
    }

    public void setPayPeriodType(String aPayPeriodType)
    {
        PayPeriodType = aPayPeriodType;
    }

    public String getPayMonth()
    {
        if (PayMonth != null && !PayMonth.equals("") && SysConst.CHANGECHARSET)
        {
            PayMonth = StrTool.unicodeToGBK(PayMonth);
        }
        return PayMonth;
    }

    public void setPayMonth(String aPayMonth)
    {
        PayMonth = aPayMonth;
    }

    public String getPayMode()
    {
        if (PayMode != null && !PayMode.equals("") && SysConst.CHANGECHARSET)
        {
            PayMode = StrTool.unicodeToGBK(PayMode);
        }
        return PayMode;
    }

    public void setPayMode(String aPayMode)
    {
        PayMode = aPayMode;
    }

    public int getPayInterval()
    {
        return PayInterval;
    }

    public void setPayInterval(int aPayInterval)
    {
        PayInterval = aPayInterval;
    }

    public void setPayInterval(String aPayInterval)
    {
        if (aPayInterval != null && !aPayInterval.equals(""))
        {
            Integer tInteger = new Integer(aPayInterval);
            int i = tInteger.intValue();
            PayInterval = i;
        }
    }

    public int getMonthLimit()
    {
        return MonthLimit;
    }

    public void setMonthLimit(int aMonthLimit)
    {
        MonthLimit = aMonthLimit;
    }

    public void setMonthLimit(String aMonthLimit)
    {
        if (aMonthLimit != null && !aMonthLimit.equals(""))
        {
            Integer tInteger = new Integer(aMonthLimit);
            int i = tInteger.intValue();
            MonthLimit = i;
        }
    }

    public double getStandRate()
    {
        return StandRate;
    }

    public void setStandRate(double aStandRate)
    {
        StandRate = aStandRate;
    }

    public void setStandRate(String aStandRate)
    {
        if (aStandRate != null && !aStandRate.equals(""))
        {
            Double tDouble = new Double(aStandRate);
            double d = tDouble.doubleValue();
            StandRate = d;
        }
    }

    public double getStandMoney()
    {
        return StandMoney;
    }

    public void setStandMoney(double aStandMoney)
    {
        StandMoney = aStandMoney;
    }

    public void setStandMoney(String aStandMoney)
    {
        if (aStandMoney != null && !aStandMoney.equals(""))
        {
            Double tDouble = new Double(aStandMoney);
            double d = tDouble.doubleValue();
            StandMoney = d;
        }
    }

    public double getStandMoney1()
    {
        return StandMoney1;
    }

    public void setStandMoney1(double aStandMoney1)
    {
        StandMoney1 = aStandMoney1;
    }

    public void setStandMoney1(String aStandMoney1)
    {
        if (aStandMoney1 != null && !aStandMoney1.equals(""))
        {
            Double tDouble = new Double(aStandMoney1);
            double d = tDouble.doubleValue();
            StandMoney1 = d;
        }
    }

    public double getBaseRate()
    {
        return BaseRate;
    }

    public void setBaseRate(double aBaseRate)
    {
        BaseRate = aBaseRate;
    }

    public void setBaseRate(String aBaseRate)
    {
        if (aBaseRate != null && !aBaseRate.equals(""))
        {
            Double tDouble = new Double(aBaseRate);
            double d = tDouble.doubleValue();
            BaseRate = d;
        }
    }

    public double getBaseRate1()
    {
        return BaseRate1;
    }

    public void setBaseRate1(double aBaseRate1)
    {
        BaseRate1 = aBaseRate1;
    }

    public void setBaseRate1(String aBaseRate1)
    {
        if (aBaseRate1 != null && !aBaseRate1.equals(""))
        {
            Double tDouble = new Double(aBaseRate1);
            double d = tDouble.doubleValue();
            BaseRate1 = d;
        }
    }

    public double getP01()
    {
        return P01;
    }

    public void setP01(double aP01)
    {
        P01 = aP01;
    }

    public void setP01(String aP01)
    {
        if (aP01 != null && !aP01.equals(""))
        {
            Double tDouble = new Double(aP01);
            double d = tDouble.doubleValue();
            P01 = d;
        }
    }

    public double getP02()
    {
        return P02;
    }

    public void setP02(double aP02)
    {
        P02 = aP02;
    }

    public void setP02(String aP02)
    {
        if (aP02 != null && !aP02.equals(""))
        {
            Double tDouble = new Double(aP02);
            double d = tDouble.doubleValue();
            P02 = d;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getAreaType()
    {
        if (AreaType != null && !AreaType.equals("") && SysConst.CHANGECHARSET)
        {
            AreaType = StrTool.unicodeToGBK(AreaType);
        }
        return AreaType;
    }

    public void setAreaType(String aAreaType)
    {
        AreaType = aAreaType;
    }

    /**
     * 使用另外一个 LALinkWageSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LALinkWageSchema aLALinkWageSchema)
    {
        this.ManageCom = aLALinkWageSchema.getManageCom();
        this.AgentGrade = aLALinkWageSchema.getAgentGrade();
        this.WageCode = aLALinkWageSchema.getWageCode();
        this.PayPeriodType = aLALinkWageSchema.getPayPeriodType();
        this.PayMonth = aLALinkWageSchema.getPayMonth();
        this.PayMode = aLALinkWageSchema.getPayMode();
        this.PayInterval = aLALinkWageSchema.getPayInterval();
        this.MonthLimit = aLALinkWageSchema.getMonthLimit();
        this.StandRate = aLALinkWageSchema.getStandRate();
        this.StandMoney = aLALinkWageSchema.getStandMoney();
        this.StandMoney1 = aLALinkWageSchema.getStandMoney1();
        this.BaseRate = aLALinkWageSchema.getBaseRate();
        this.BaseRate1 = aLALinkWageSchema.getBaseRate1();
        this.P01 = aLALinkWageSchema.getP01();
        this.P02 = aLALinkWageSchema.getP02();
        this.Operator = aLALinkWageSchema.getOperator();
        this.MakeDate = fDate.getDate(aLALinkWageSchema.getMakeDate());
        this.MakeTime = aLALinkWageSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLALinkWageSchema.getModifyDate());
        this.ModifyTime = aLALinkWageSchema.getModifyTime();
        this.AreaType = aLALinkWageSchema.getAreaType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("WageCode") == null)
            {
                this.WageCode = null;
            }
            else
            {
                this.WageCode = rs.getString("WageCode").trim();
            }

            if (rs.getString("PayPeriodType") == null)
            {
                this.PayPeriodType = null;
            }
            else
            {
                this.PayPeriodType = rs.getString("PayPeriodType").trim();
            }

            if (rs.getString("PayMonth") == null)
            {
                this.PayMonth = null;
            }
            else
            {
                this.PayMonth = rs.getString("PayMonth").trim();
            }

            if (rs.getString("PayMode") == null)
            {
                this.PayMode = null;
            }
            else
            {
                this.PayMode = rs.getString("PayMode").trim();
            }

            this.PayInterval = rs.getInt("PayInterval");
            this.MonthLimit = rs.getInt("MonthLimit");
            this.StandRate = rs.getDouble("StandRate");
            this.StandMoney = rs.getDouble("StandMoney");
            this.StandMoney1 = rs.getDouble("StandMoney1");
            this.BaseRate = rs.getDouble("BaseRate");
            this.BaseRate1 = rs.getDouble("BaseRate1");
            this.P01 = rs.getDouble("P01");
            this.P02 = rs.getDouble("P02");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("AreaType") == null)
            {
                this.AreaType = null;
            }
            else
            {
                this.AreaType = rs.getString("AreaType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LALinkWageSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LALinkWageSchema getSchema()
    {
        LALinkWageSchema aLALinkWageSchema = new LALinkWageSchema();
        aLALinkWageSchema.setSchema(this);
        return aLALinkWageSchema;
    }

    public LALinkWageDB getDB()
    {
        LALinkWageDB aDBOper = new LALinkWageDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLALinkWage描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(WageCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayPeriodType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayMonth)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayMode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(PayInterval) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(MonthLimit) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StandRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StandMoney) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StandMoney1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(BaseRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(BaseRate1) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(P01) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(P02) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AreaType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLALinkWage>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            WageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            PayPeriodType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            PayMonth = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            PayMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            PayInterval = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 7, SysConst.PACKAGESPILTER))).intValue();
            MonthLimit = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).intValue();
            StandRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            StandMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            StandMoney1 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            BaseRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            BaseRate1 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).doubleValue();
            P01 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    14, SysConst.PACKAGESPILTER))).doubleValue();
            P02 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    15, SysConst.PACKAGESPILTER))).doubleValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 17, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                        SysConst.PACKAGESPILTER);
            AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LALinkWageSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("WageCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WageCode));
        }
        if (FCode.equals("PayPeriodType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayPeriodType));
        }
        if (FCode.equals("PayMonth"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMonth));
        }
        if (FCode.equals("PayMode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayMode));
        }
        if (FCode.equals("PayInterval"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PayInterval));
        }
        if (FCode.equals("MonthLimit"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MonthLimit));
        }
        if (FCode.equals("StandRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandRate));
        }
        if (FCode.equals("StandMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandMoney));
        }
        if (FCode.equals("StandMoney1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StandMoney1));
        }
        if (FCode.equals("BaseRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BaseRate));
        }
        if (FCode.equals("BaseRate1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BaseRate1));
        }
        if (FCode.equals("P01"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P01));
        }
        if (FCode.equals("P02"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(P02));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("AreaType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(WageCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PayPeriodType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PayMonth);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(PayMode);
                break;
            case 6:
                strFieldValue = String.valueOf(PayInterval);
                break;
            case 7:
                strFieldValue = String.valueOf(MonthLimit);
                break;
            case 8:
                strFieldValue = String.valueOf(StandRate);
                break;
            case 9:
                strFieldValue = String.valueOf(StandMoney);
                break;
            case 10:
                strFieldValue = String.valueOf(StandMoney1);
                break;
            case 11:
                strFieldValue = String.valueOf(BaseRate);
                break;
            case 12:
                strFieldValue = String.valueOf(BaseRate1);
                break;
            case 13:
                strFieldValue = String.valueOf(P01);
                break;
            case 14:
                strFieldValue = String.valueOf(P02);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(AreaType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("WageCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WageCode = FValue.trim();
            }
            else
            {
                WageCode = null;
            }
        }
        if (FCode.equals("PayPeriodType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayPeriodType = FValue.trim();
            }
            else
            {
                PayPeriodType = null;
            }
        }
        if (FCode.equals("PayMonth"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayMonth = FValue.trim();
            }
            else
            {
                PayMonth = null;
            }
        }
        if (FCode.equals("PayMode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayMode = FValue.trim();
            }
            else
            {
                PayMode = null;
            }
        }
        if (FCode.equals("PayInterval"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PayInterval = i;
            }
        }
        if (FCode.equals("MonthLimit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                MonthLimit = i;
            }
        }
        if (FCode.equals("StandRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StandRate = d;
            }
        }
        if (FCode.equals("StandMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StandMoney = d;
            }
        }
        if (FCode.equals("StandMoney1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StandMoney1 = d;
            }
        }
        if (FCode.equals("BaseRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BaseRate = d;
            }
        }
        if (FCode.equals("BaseRate1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                BaseRate1 = d;
            }
        }
        if (FCode.equals("P01"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                P01 = d;
            }
        }
        if (FCode.equals("P02"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                P02 = d;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("AreaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AreaType = FValue.trim();
            }
            else
            {
                AreaType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LALinkWageSchema other = (LALinkWageSchema) otherObject;
        return
                ManageCom.equals(other.getManageCom())
                && AgentGrade.equals(other.getAgentGrade())
                && WageCode.equals(other.getWageCode())
                && PayPeriodType.equals(other.getPayPeriodType())
                && PayMonth.equals(other.getPayMonth())
                && PayMode.equals(other.getPayMode())
                && PayInterval == other.getPayInterval()
                && MonthLimit == other.getMonthLimit()
                && StandRate == other.getStandRate()
                && StandMoney == other.getStandMoney()
                && StandMoney1 == other.getStandMoney1()
                && BaseRate == other.getBaseRate()
                && BaseRate1 == other.getBaseRate1()
                && P01 == other.getP01()
                && P02 == other.getP02()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && AreaType.equals(other.getAreaType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return 0;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 1;
        }
        if (strFieldName.equals("WageCode"))
        {
            return 2;
        }
        if (strFieldName.equals("PayPeriodType"))
        {
            return 3;
        }
        if (strFieldName.equals("PayMonth"))
        {
            return 4;
        }
        if (strFieldName.equals("PayMode"))
        {
            return 5;
        }
        if (strFieldName.equals("PayInterval"))
        {
            return 6;
        }
        if (strFieldName.equals("MonthLimit"))
        {
            return 7;
        }
        if (strFieldName.equals("StandRate"))
        {
            return 8;
        }
        if (strFieldName.equals("StandMoney"))
        {
            return 9;
        }
        if (strFieldName.equals("StandMoney1"))
        {
            return 10;
        }
        if (strFieldName.equals("BaseRate"))
        {
            return 11;
        }
        if (strFieldName.equals("BaseRate1"))
        {
            return 12;
        }
        if (strFieldName.equals("P01"))
        {
            return 13;
        }
        if (strFieldName.equals("P02"))
        {
            return 14;
        }
        if (strFieldName.equals("Operator"))
        {
            return 15;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 16;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 17;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 18;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 19;
        }
        if (strFieldName.equals("AreaType"))
        {
            return 20;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "AgentGrade";
                break;
            case 2:
                strFieldName = "WageCode";
                break;
            case 3:
                strFieldName = "PayPeriodType";
                break;
            case 4:
                strFieldName = "PayMonth";
                break;
            case 5:
                strFieldName = "PayMode";
                break;
            case 6:
                strFieldName = "PayInterval";
                break;
            case 7:
                strFieldName = "MonthLimit";
                break;
            case 8:
                strFieldName = "StandRate";
                break;
            case 9:
                strFieldName = "StandMoney";
                break;
            case 10:
                strFieldName = "StandMoney1";
                break;
            case 11:
                strFieldName = "BaseRate";
                break;
            case 12:
                strFieldName = "BaseRate1";
                break;
            case 13:
                strFieldName = "P01";
                break;
            case 14:
                strFieldName = "P02";
                break;
            case 15:
                strFieldName = "Operator";
                break;
            case 16:
                strFieldName = "MakeDate";
                break;
            case 17:
                strFieldName = "MakeTime";
                break;
            case 18:
                strFieldName = "ModifyDate";
                break;
            case 19:
                strFieldName = "ModifyTime";
                break;
            case 20:
                strFieldName = "AreaType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WageCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayPeriodType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMonth"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayMode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayInterval"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("MonthLimit"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("StandRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StandMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StandMoney1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BaseRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BaseRate1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("P01"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("P02"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_INT;
                break;
            case 7:
                nFieldType = Schema.TYPE_INT;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
