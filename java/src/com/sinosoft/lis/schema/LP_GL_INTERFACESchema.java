/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LP_GL_INTERFACEDB;

/*
 * <p>ClassName: LP_GL_INTERFACESchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2008-05-20
 */
public class LP_GL_INTERFACESchema implements Schema, Cloneable
{
	// @Field
	/** 行id */
	private double ROW_ID;
	/** 币别 */
	private String CURRENCY_CODE;
	/** 凭证类别 */
	private String USER_JE_CATEGORY_NAME;
	/** 分公司代码 */
	private String SEGMENT1;
	/** 支公司代码 */
	private String SEGMENT2;
	/** 科目代码 */
	private String SEGMENT3;
	/** 科目明细代码 */
	private String SEGMENT4;
	/** 保险产品代码 */
	private String SEGMENT5;
	/** 渠道代码 */
	private String SEGMENT6;
	/** 备用段1 */
	private String SEGMENT7;
	/** 备用段2 */
	private String SEGMENT8;
	/** 事务日期 */
	private Date TRANSACTION_DATE;
	/** 业务系统产生会计信息时的关键标识 */
	private double POSTING_ID;
	/** 事务借计金额 */
	private double ENTERED_DR;
	/** 事务贷计金额 */
	private double ENTERED_CR;
	/** 日记帐摘要 */
	private String REFERENCE5;
	/** 日记帐行摘要 */
	private String REFERENCE10;
	/** 汇率日期 */
	private Date CURRENCY_CONVERSION_DATE;
	/** 汇率 */
	private double CURRENCY_CONVERSION_RATE;
	/** 记帐借计金额 */
	private double ACCOUNTED_DR;
	/** 记帐贷计金额 */
	private double ACCOUNTED_CR;
	/** 以备后用1 */
	private String ATTRIBUTE1;
	/** 保单号码 */
	private String ATTRIBUTE2;
	/** 以备后用3 */
	private String ATTRIBUTE3;
	/** 以备后用4 */
	private String ATTRIBUTE4;
	/** 以备后用5 */
	private String ATTRIBUTE5;
	/** 以备后用6 */
	private String ATTRIBUTE6;
	/** 以备后用7 */
	private String ATTRIBUTE7;
	/** 以备后用8 */
	private String ATTRIBUTE8;
	/** 以备后用9 */
	private String ATTRIBUTE9;
	/** 回写标志 */
	private String ATTRIBUTE10;

	public static final int FIELDNUM = 31;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LP_GL_INTERFACESchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ROW_ID";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LP_GL_INTERFACESchema cloned = (LP_GL_INTERFACESchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public double getROW_ID()
	{
		return ROW_ID;
	}
	public void setROW_ID(double aROW_ID)
	{
            ROW_ID = Arith.round(aROW_ID,0);
	}
	public void setROW_ID(String aROW_ID)
	{
		if (aROW_ID != null && !aROW_ID.equals(""))
		{
			Double tDouble = new Double(aROW_ID);
			double d = tDouble.doubleValue();
                ROW_ID = Arith.round(d,0);
		}
	}

	public String getCURRENCY_CODE()
	{
		return CURRENCY_CODE;
	}
	public void setCURRENCY_CODE(String aCURRENCY_CODE)
	{
            CURRENCY_CODE = aCURRENCY_CODE;
	}
	public String getUSER_JE_CATEGORY_NAME()
	{
		return USER_JE_CATEGORY_NAME;
	}
	public void setUSER_JE_CATEGORY_NAME(String aUSER_JE_CATEGORY_NAME)
	{
            USER_JE_CATEGORY_NAME = aUSER_JE_CATEGORY_NAME;
	}
	public String getSEGMENT1()
	{
		return SEGMENT1;
	}
	public void setSEGMENT1(String aSEGMENT1)
	{
            SEGMENT1 = aSEGMENT1;
	}
	public String getSEGMENT2()
	{
		return SEGMENT2;
	}
	public void setSEGMENT2(String aSEGMENT2)
	{
            SEGMENT2 = aSEGMENT2;
	}
	public String getSEGMENT3()
	{
		return SEGMENT3;
	}
	public void setSEGMENT3(String aSEGMENT3)
	{
            SEGMENT3 = aSEGMENT3;
	}
	public String getSEGMENT4()
	{
		return SEGMENT4;
	}
	public void setSEGMENT4(String aSEGMENT4)
	{
            SEGMENT4 = aSEGMENT4;
	}
	public String getSEGMENT5()
	{
		return SEGMENT5;
	}
	public void setSEGMENT5(String aSEGMENT5)
	{
            SEGMENT5 = aSEGMENT5;
	}
	public String getSEGMENT6()
	{
		return SEGMENT6;
	}
	public void setSEGMENT6(String aSEGMENT6)
	{
            SEGMENT6 = aSEGMENT6;
	}
	public String getSEGMENT7()
	{
		return SEGMENT7;
	}
	public void setSEGMENT7(String aSEGMENT7)
	{
            SEGMENT7 = aSEGMENT7;
	}
	public String getSEGMENT8()
	{
		return SEGMENT8;
	}
	public void setSEGMENT8(String aSEGMENT8)
	{
            SEGMENT8 = aSEGMENT8;
	}
	public String getTRANSACTION_DATE()
	{
		if( TRANSACTION_DATE != null )
			return fDate.getString(TRANSACTION_DATE);
		else
			return null;
	}
	public void setTRANSACTION_DATE(Date aTRANSACTION_DATE)
	{
            TRANSACTION_DATE = aTRANSACTION_DATE;
	}
	public void setTRANSACTION_DATE(String aTRANSACTION_DATE)
	{
		if (aTRANSACTION_DATE != null && !aTRANSACTION_DATE.equals("") )
		{
			TRANSACTION_DATE = fDate.getDate( aTRANSACTION_DATE );
		}
		else
			TRANSACTION_DATE = null;
	}

	public double getPOSTING_ID()
	{
		return POSTING_ID;
	}
	public void setPOSTING_ID(double aPOSTING_ID)
	{
            POSTING_ID = Arith.round(aPOSTING_ID,0);
	}
	public void setPOSTING_ID(String aPOSTING_ID)
	{
		if (aPOSTING_ID != null && !aPOSTING_ID.equals(""))
		{
			Double tDouble = new Double(aPOSTING_ID);
			double d = tDouble.doubleValue();
                POSTING_ID = Arith.round(d,0);
		}
	}

	public double getENTERED_DR()
	{
		return ENTERED_DR;
	}
	public void setENTERED_DR(double aENTERED_DR)
	{
            ENTERED_DR = Arith.round(aENTERED_DR,0);
	}
	public void setENTERED_DR(String aENTERED_DR)
	{
		if (aENTERED_DR != null && !aENTERED_DR.equals(""))
		{
			Double tDouble = new Double(aENTERED_DR);
			double d = tDouble.doubleValue();
                ENTERED_DR = Arith.round(d,0);
		}
	}

	public double getENTERED_CR()
	{
		return ENTERED_CR;
	}
	public void setENTERED_CR(double aENTERED_CR)
	{
            ENTERED_CR = Arith.round(aENTERED_CR,0);
	}
	public void setENTERED_CR(String aENTERED_CR)
	{
		if (aENTERED_CR != null && !aENTERED_CR.equals(""))
		{
			Double tDouble = new Double(aENTERED_CR);
			double d = tDouble.doubleValue();
                ENTERED_CR = Arith.round(d,0);
		}
	}

	public String getREFERENCE5()
	{
		return REFERENCE5;
	}
	public void setREFERENCE5(String aREFERENCE5)
	{
            REFERENCE5 = aREFERENCE5;
	}
	public String getREFERENCE10()
	{
		return REFERENCE10;
	}
	public void setREFERENCE10(String aREFERENCE10)
	{
            REFERENCE10 = aREFERENCE10;
	}
	public String getCURRENCY_CONVERSION_DATE()
	{
		if( CURRENCY_CONVERSION_DATE != null )
			return fDate.getString(CURRENCY_CONVERSION_DATE);
		else
			return null;
	}
	public void setCURRENCY_CONVERSION_DATE(Date aCURRENCY_CONVERSION_DATE)
	{
            CURRENCY_CONVERSION_DATE = aCURRENCY_CONVERSION_DATE;
	}
	public void setCURRENCY_CONVERSION_DATE(String aCURRENCY_CONVERSION_DATE)
	{
		if (aCURRENCY_CONVERSION_DATE != null && !aCURRENCY_CONVERSION_DATE.equals("") )
		{
			CURRENCY_CONVERSION_DATE = fDate.getDate( aCURRENCY_CONVERSION_DATE );
		}
		else
			CURRENCY_CONVERSION_DATE = null;
	}

	public double getCURRENCY_CONVERSION_RATE()
	{
		return CURRENCY_CONVERSION_RATE;
	}
	public void setCURRENCY_CONVERSION_RATE(double aCURRENCY_CONVERSION_RATE)
	{
            CURRENCY_CONVERSION_RATE = Arith.round(aCURRENCY_CONVERSION_RATE,0);
	}
	public void setCURRENCY_CONVERSION_RATE(String aCURRENCY_CONVERSION_RATE)
	{
		if (aCURRENCY_CONVERSION_RATE != null && !aCURRENCY_CONVERSION_RATE.equals(""))
		{
			Double tDouble = new Double(aCURRENCY_CONVERSION_RATE);
			double d = tDouble.doubleValue();
                CURRENCY_CONVERSION_RATE = Arith.round(d,0);
		}
	}

	public double getACCOUNTED_DR()
	{
		return ACCOUNTED_DR;
	}
	public void setACCOUNTED_DR(double aACCOUNTED_DR)
	{
            ACCOUNTED_DR = Arith.round(aACCOUNTED_DR,0);
	}
	public void setACCOUNTED_DR(String aACCOUNTED_DR)
	{
		if (aACCOUNTED_DR != null && !aACCOUNTED_DR.equals(""))
		{
			Double tDouble = new Double(aACCOUNTED_DR);
			double d = tDouble.doubleValue();
                ACCOUNTED_DR = Arith.round(d,0);
		}
	}

	public double getACCOUNTED_CR()
	{
		return ACCOUNTED_CR;
	}
	public void setACCOUNTED_CR(double aACCOUNTED_CR)
	{
            ACCOUNTED_CR = Arith.round(aACCOUNTED_CR,0);
	}
	public void setACCOUNTED_CR(String aACCOUNTED_CR)
	{
		if (aACCOUNTED_CR != null && !aACCOUNTED_CR.equals(""))
		{
			Double tDouble = new Double(aACCOUNTED_CR);
			double d = tDouble.doubleValue();
                ACCOUNTED_CR = Arith.round(d,0);
		}
	}

	public String getATTRIBUTE1()
	{
		return ATTRIBUTE1;
	}
	public void setATTRIBUTE1(String aATTRIBUTE1)
	{
            ATTRIBUTE1 = aATTRIBUTE1;
	}
	public String getATTRIBUTE2()
	{
		return ATTRIBUTE2;
	}
	public void setATTRIBUTE2(String aATTRIBUTE2)
	{
            ATTRIBUTE2 = aATTRIBUTE2;
	}
	public String getATTRIBUTE3()
	{
		return ATTRIBUTE3;
	}
	public void setATTRIBUTE3(String aATTRIBUTE3)
	{
            ATTRIBUTE3 = aATTRIBUTE3;
	}
	public String getATTRIBUTE4()
	{
		return ATTRIBUTE4;
	}
	public void setATTRIBUTE4(String aATTRIBUTE4)
	{
            ATTRIBUTE4 = aATTRIBUTE4;
	}
	public String getATTRIBUTE5()
	{
		return ATTRIBUTE5;
	}
	public void setATTRIBUTE5(String aATTRIBUTE5)
	{
            ATTRIBUTE5 = aATTRIBUTE5;
	}
	public String getATTRIBUTE6()
	{
		return ATTRIBUTE6;
	}
	public void setATTRIBUTE6(String aATTRIBUTE6)
	{
            ATTRIBUTE6 = aATTRIBUTE6;
	}
	public String getATTRIBUTE7()
	{
		return ATTRIBUTE7;
	}
	public void setATTRIBUTE7(String aATTRIBUTE7)
	{
            ATTRIBUTE7 = aATTRIBUTE7;
	}
	public String getATTRIBUTE8()
	{
		return ATTRIBUTE8;
	}
	public void setATTRIBUTE8(String aATTRIBUTE8)
	{
            ATTRIBUTE8 = aATTRIBUTE8;
	}
	public String getATTRIBUTE9()
	{
		return ATTRIBUTE9;
	}
	public void setATTRIBUTE9(String aATTRIBUTE9)
	{
            ATTRIBUTE9 = aATTRIBUTE9;
	}
	public String getATTRIBUTE10()
	{
		return ATTRIBUTE10;
	}
	public void setATTRIBUTE10(String aATTRIBUTE10)
	{
            ATTRIBUTE10 = aATTRIBUTE10;
	}

	/**
	* 使用另外一个 LP_GL_INTERFACESchema 对象给 Schema 赋值
	* @param: aLP_GL_INTERFACESchema LP_GL_INTERFACESchema
	**/
	public void setSchema(LP_GL_INTERFACESchema aLP_GL_INTERFACESchema)
	{
		this.ROW_ID = aLP_GL_INTERFACESchema.getROW_ID();
		this.CURRENCY_CODE = aLP_GL_INTERFACESchema.getCURRENCY_CODE();
		this.USER_JE_CATEGORY_NAME = aLP_GL_INTERFACESchema.getUSER_JE_CATEGORY_NAME();
		this.SEGMENT1 = aLP_GL_INTERFACESchema.getSEGMENT1();
		this.SEGMENT2 = aLP_GL_INTERFACESchema.getSEGMENT2();
		this.SEGMENT3 = aLP_GL_INTERFACESchema.getSEGMENT3();
		this.SEGMENT4 = aLP_GL_INTERFACESchema.getSEGMENT4();
		this.SEGMENT5 = aLP_GL_INTERFACESchema.getSEGMENT5();
		this.SEGMENT6 = aLP_GL_INTERFACESchema.getSEGMENT6();
		this.SEGMENT7 = aLP_GL_INTERFACESchema.getSEGMENT7();
		this.SEGMENT8 = aLP_GL_INTERFACESchema.getSEGMENT8();
		this.TRANSACTION_DATE = fDate.getDate( aLP_GL_INTERFACESchema.getTRANSACTION_DATE());
		this.POSTING_ID = aLP_GL_INTERFACESchema.getPOSTING_ID();
		this.ENTERED_DR = aLP_GL_INTERFACESchema.getENTERED_DR();
		this.ENTERED_CR = aLP_GL_INTERFACESchema.getENTERED_CR();
		this.REFERENCE5 = aLP_GL_INTERFACESchema.getREFERENCE5();
		this.REFERENCE10 = aLP_GL_INTERFACESchema.getREFERENCE10();
		this.CURRENCY_CONVERSION_DATE = fDate.getDate( aLP_GL_INTERFACESchema.getCURRENCY_CONVERSION_DATE());
		this.CURRENCY_CONVERSION_RATE = aLP_GL_INTERFACESchema.getCURRENCY_CONVERSION_RATE();
		this.ACCOUNTED_DR = aLP_GL_INTERFACESchema.getACCOUNTED_DR();
		this.ACCOUNTED_CR = aLP_GL_INTERFACESchema.getACCOUNTED_CR();
		this.ATTRIBUTE1 = aLP_GL_INTERFACESchema.getATTRIBUTE1();
		this.ATTRIBUTE2 = aLP_GL_INTERFACESchema.getATTRIBUTE2();
		this.ATTRIBUTE3 = aLP_GL_INTERFACESchema.getATTRIBUTE3();
		this.ATTRIBUTE4 = aLP_GL_INTERFACESchema.getATTRIBUTE4();
		this.ATTRIBUTE5 = aLP_GL_INTERFACESchema.getATTRIBUTE5();
		this.ATTRIBUTE6 = aLP_GL_INTERFACESchema.getATTRIBUTE6();
		this.ATTRIBUTE7 = aLP_GL_INTERFACESchema.getATTRIBUTE7();
		this.ATTRIBUTE8 = aLP_GL_INTERFACESchema.getATTRIBUTE8();
		this.ATTRIBUTE9 = aLP_GL_INTERFACESchema.getATTRIBUTE9();
		this.ATTRIBUTE10 = aLP_GL_INTERFACESchema.getATTRIBUTE10();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			this.ROW_ID = rs.getDouble("ROW_ID");
			if( rs.getString("CURRENCY_CODE") == null )
				this.CURRENCY_CODE = null;
			else
				this.CURRENCY_CODE = rs.getString("CURRENCY_CODE").trim();

			if( rs.getString("USER_JE_CATEGORY_NAME") == null )
				this.USER_JE_CATEGORY_NAME = null;
			else
				this.USER_JE_CATEGORY_NAME = rs.getString("USER_JE_CATEGORY_NAME").trim();

			if( rs.getString("SEGMENT1") == null )
				this.SEGMENT1 = null;
			else
				this.SEGMENT1 = rs.getString("SEGMENT1").trim();

			if( rs.getString("SEGMENT2") == null )
				this.SEGMENT2 = null;
			else
				this.SEGMENT2 = rs.getString("SEGMENT2").trim();

			if( rs.getString("SEGMENT3") == null )
				this.SEGMENT3 = null;
			else
				this.SEGMENT3 = rs.getString("SEGMENT3").trim();

			if( rs.getString("SEGMENT4") == null )
				this.SEGMENT4 = null;
			else
				this.SEGMENT4 = rs.getString("SEGMENT4").trim();

			if( rs.getString("SEGMENT5") == null )
				this.SEGMENT5 = null;
			else
				this.SEGMENT5 = rs.getString("SEGMENT5").trim();

			if( rs.getString("SEGMENT6") == null )
				this.SEGMENT6 = null;
			else
				this.SEGMENT6 = rs.getString("SEGMENT6").trim();

			if( rs.getString("SEGMENT7") == null )
				this.SEGMENT7 = null;
			else
				this.SEGMENT7 = rs.getString("SEGMENT7").trim();

			if( rs.getString("SEGMENT8") == null )
				this.SEGMENT8 = null;
			else
				this.SEGMENT8 = rs.getString("SEGMENT8").trim();

			this.TRANSACTION_DATE = rs.getDate("TRANSACTION_DATE");
			this.POSTING_ID = rs.getDouble("POSTING_ID");
			this.ENTERED_DR = rs.getDouble("ENTERED_DR");
			this.ENTERED_CR = rs.getDouble("ENTERED_CR");
			if( rs.getString("REFERENCE5") == null )
				this.REFERENCE5 = null;
			else
				this.REFERENCE5 = rs.getString("REFERENCE5").trim();

			if( rs.getString("REFERENCE10") == null )
				this.REFERENCE10 = null;
			else
				this.REFERENCE10 = rs.getString("REFERENCE10").trim();

			this.CURRENCY_CONVERSION_DATE = rs.getDate("CURRENCY_CONVERSION_DATE");
			this.CURRENCY_CONVERSION_RATE = rs.getDouble("CURRENCY_CONVERSION_RATE");
			this.ACCOUNTED_DR = rs.getDouble("ACCOUNTED_DR");
			this.ACCOUNTED_CR = rs.getDouble("ACCOUNTED_CR");
			if( rs.getString("ATTRIBUTE1") == null )
				this.ATTRIBUTE1 = null;
			else
				this.ATTRIBUTE1 = rs.getString("ATTRIBUTE1").trim();

			if( rs.getString("ATTRIBUTE2") == null )
				this.ATTRIBUTE2 = null;
			else
				this.ATTRIBUTE2 = rs.getString("ATTRIBUTE2").trim();

			if( rs.getString("ATTRIBUTE3") == null )
				this.ATTRIBUTE3 = null;
			else
				this.ATTRIBUTE3 = rs.getString("ATTRIBUTE3").trim();

			if( rs.getString("ATTRIBUTE4") == null )
				this.ATTRIBUTE4 = null;
			else
				this.ATTRIBUTE4 = rs.getString("ATTRIBUTE4").trim();

			if( rs.getString("ATTRIBUTE5") == null )
				this.ATTRIBUTE5 = null;
			else
				this.ATTRIBUTE5 = rs.getString("ATTRIBUTE5").trim();

			if( rs.getString("ATTRIBUTE6") == null )
				this.ATTRIBUTE6 = null;
			else
				this.ATTRIBUTE6 = rs.getString("ATTRIBUTE6").trim();

			if( rs.getString("ATTRIBUTE7") == null )
				this.ATTRIBUTE7 = null;
			else
				this.ATTRIBUTE7 = rs.getString("ATTRIBUTE7").trim();

			if( rs.getString("ATTRIBUTE8") == null )
				this.ATTRIBUTE8 = null;
			else
				this.ATTRIBUTE8 = rs.getString("ATTRIBUTE8").trim();

			if( rs.getString("ATTRIBUTE9") == null )
				this.ATTRIBUTE9 = null;
			else
				this.ATTRIBUTE9 = rs.getString("ATTRIBUTE9").trim();

			if( rs.getString("ATTRIBUTE10") == null )
				this.ATTRIBUTE10 = null;
			else
				this.ATTRIBUTE10 = rs.getString("ATTRIBUTE10").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LP_GL_INTERFACE表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LP_GL_INTERFACESchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LP_GL_INTERFACESchema getSchema()
	{
		LP_GL_INTERFACESchema aLP_GL_INTERFACESchema = new LP_GL_INTERFACESchema();
		aLP_GL_INTERFACESchema.setSchema(this);
		return aLP_GL_INTERFACESchema;
	}

	public LP_GL_INTERFACEDB getDB()
	{
		LP_GL_INTERFACEDB aDBOper = new LP_GL_INTERFACEDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLP_GL_INTERFACE描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append( ChgData.chgData(ROW_ID));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CURRENCY_CODE)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(USER_JE_CATEGORY_NAME)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SEGMENT1)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SEGMENT2)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SEGMENT3)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SEGMENT4)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SEGMENT5)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SEGMENT6)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SEGMENT7)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SEGMENT8)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( TRANSACTION_DATE ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(POSTING_ID));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ENTERED_DR));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ENTERED_CR));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(REFERENCE5)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(REFERENCE10)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( CURRENCY_CONVERSION_DATE ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(CURRENCY_CONVERSION_RATE));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ACCOUNTED_DR));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(ACCOUNTED_CR));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ATTRIBUTE1)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ATTRIBUTE2)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ATTRIBUTE3)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ATTRIBUTE4)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ATTRIBUTE5)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ATTRIBUTE6)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ATTRIBUTE7)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ATTRIBUTE8)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ATTRIBUTE9)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ATTRIBUTE10));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLP_GL_INTERFACE>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ROW_ID = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).doubleValue();
			CURRENCY_CODE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			USER_JE_CATEGORY_NAME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			SEGMENT1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			SEGMENT2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			SEGMENT3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			SEGMENT4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			SEGMENT5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			SEGMENT6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			SEGMENT7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			SEGMENT8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			TRANSACTION_DATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			POSTING_ID = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			ENTERED_DR = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			ENTERED_CR = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			REFERENCE5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			REFERENCE10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			CURRENCY_CONVERSION_DATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			CURRENCY_CONVERSION_RATE = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			ACCOUNTED_DR = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			ACCOUNTED_CR = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			ATTRIBUTE1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ATTRIBUTE2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ATTRIBUTE3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ATTRIBUTE4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ATTRIBUTE5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			ATTRIBUTE6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			ATTRIBUTE7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			ATTRIBUTE8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			ATTRIBUTE9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			ATTRIBUTE10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LP_GL_INTERFACESchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ROW_ID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ROW_ID));
		}
		if (FCode.equals("CURRENCY_CODE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CURRENCY_CODE));
		}
		if (FCode.equals("USER_JE_CATEGORY_NAME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(USER_JE_CATEGORY_NAME));
		}
		if (FCode.equals("SEGMENT1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SEGMENT1));
		}
		if (FCode.equals("SEGMENT2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SEGMENT2));
		}
		if (FCode.equals("SEGMENT3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SEGMENT3));
		}
		if (FCode.equals("SEGMENT4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SEGMENT4));
		}
		if (FCode.equals("SEGMENT5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SEGMENT5));
		}
		if (FCode.equals("SEGMENT6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SEGMENT6));
		}
		if (FCode.equals("SEGMENT7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SEGMENT7));
		}
		if (FCode.equals("SEGMENT8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SEGMENT8));
		}
		if (FCode.equals("TRANSACTION_DATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTRANSACTION_DATE()));
		}
		if (FCode.equals("POSTING_ID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(POSTING_ID));
		}
		if (FCode.equals("ENTERED_DR"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ENTERED_DR));
		}
		if (FCode.equals("ENTERED_CR"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ENTERED_CR));
		}
		if (FCode.equals("REFERENCE5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(REFERENCE5));
		}
		if (FCode.equals("REFERENCE10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(REFERENCE10));
		}
		if (FCode.equals("CURRENCY_CONVERSION_DATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCURRENCY_CONVERSION_DATE()));
		}
		if (FCode.equals("CURRENCY_CONVERSION_RATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CURRENCY_CONVERSION_RATE));
		}
		if (FCode.equals("ACCOUNTED_DR"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ACCOUNTED_DR));
		}
		if (FCode.equals("ACCOUNTED_CR"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ACCOUNTED_CR));
		}
		if (FCode.equals("ATTRIBUTE1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ATTRIBUTE1));
		}
		if (FCode.equals("ATTRIBUTE2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ATTRIBUTE2));
		}
		if (FCode.equals("ATTRIBUTE3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ATTRIBUTE3));
		}
		if (FCode.equals("ATTRIBUTE4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ATTRIBUTE4));
		}
		if (FCode.equals("ATTRIBUTE5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ATTRIBUTE5));
		}
		if (FCode.equals("ATTRIBUTE6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ATTRIBUTE6));
		}
		if (FCode.equals("ATTRIBUTE7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ATTRIBUTE7));
		}
		if (FCode.equals("ATTRIBUTE8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ATTRIBUTE8));
		}
		if (FCode.equals("ATTRIBUTE9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ATTRIBUTE9));
		}
		if (FCode.equals("ATTRIBUTE10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ATTRIBUTE10));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = String.valueOf(ROW_ID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CURRENCY_CODE);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(USER_JE_CATEGORY_NAME);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(SEGMENT1);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(SEGMENT2);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(SEGMENT3);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(SEGMENT4);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(SEGMENT5);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(SEGMENT6);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(SEGMENT7);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(SEGMENT8);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTRANSACTION_DATE()));
				break;
			case 12:
				strFieldValue = String.valueOf(POSTING_ID);
				break;
			case 13:
				strFieldValue = String.valueOf(ENTERED_DR);
				break;
			case 14:
				strFieldValue = String.valueOf(ENTERED_CR);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(REFERENCE5);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(REFERENCE10);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCURRENCY_CONVERSION_DATE()));
				break;
			case 18:
				strFieldValue = String.valueOf(CURRENCY_CONVERSION_RATE);
				break;
			case 19:
				strFieldValue = String.valueOf(ACCOUNTED_DR);
				break;
			case 20:
				strFieldValue = String.valueOf(ACCOUNTED_CR);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ATTRIBUTE1);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ATTRIBUTE2);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ATTRIBUTE3);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ATTRIBUTE4);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ATTRIBUTE5);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ATTRIBUTE6);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(ATTRIBUTE7);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(ATTRIBUTE8);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(ATTRIBUTE9);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(ATTRIBUTE10);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ROW_ID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ROW_ID = d;
			}
		}
		if (FCode.equalsIgnoreCase("CURRENCY_CODE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CURRENCY_CODE = FValue.trim();
			}
			else
				CURRENCY_CODE = null;
		}
		if (FCode.equalsIgnoreCase("USER_JE_CATEGORY_NAME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				USER_JE_CATEGORY_NAME = FValue.trim();
			}
			else
				USER_JE_CATEGORY_NAME = null;
		}
		if (FCode.equalsIgnoreCase("SEGMENT1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SEGMENT1 = FValue.trim();
			}
			else
				SEGMENT1 = null;
		}
		if (FCode.equalsIgnoreCase("SEGMENT2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SEGMENT2 = FValue.trim();
			}
			else
				SEGMENT2 = null;
		}
		if (FCode.equalsIgnoreCase("SEGMENT3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SEGMENT3 = FValue.trim();
			}
			else
				SEGMENT3 = null;
		}
		if (FCode.equalsIgnoreCase("SEGMENT4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SEGMENT4 = FValue.trim();
			}
			else
				SEGMENT4 = null;
		}
		if (FCode.equalsIgnoreCase("SEGMENT5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SEGMENT5 = FValue.trim();
			}
			else
				SEGMENT5 = null;
		}
		if (FCode.equalsIgnoreCase("SEGMENT6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SEGMENT6 = FValue.trim();
			}
			else
				SEGMENT6 = null;
		}
		if (FCode.equalsIgnoreCase("SEGMENT7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SEGMENT7 = FValue.trim();
			}
			else
				SEGMENT7 = null;
		}
		if (FCode.equalsIgnoreCase("SEGMENT8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SEGMENT8 = FValue.trim();
			}
			else
				SEGMENT8 = null;
		}
		if (FCode.equalsIgnoreCase("TRANSACTION_DATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TRANSACTION_DATE = fDate.getDate( FValue );
			}
			else
				TRANSACTION_DATE = null;
		}
		if (FCode.equalsIgnoreCase("POSTING_ID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				POSTING_ID = d;
			}
		}
		if (FCode.equalsIgnoreCase("ENTERED_DR"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ENTERED_DR = d;
			}
		}
		if (FCode.equalsIgnoreCase("ENTERED_CR"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ENTERED_CR = d;
			}
		}
		if (FCode.equalsIgnoreCase("REFERENCE5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				REFERENCE5 = FValue.trim();
			}
			else
				REFERENCE5 = null;
		}
		if (FCode.equalsIgnoreCase("REFERENCE10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				REFERENCE10 = FValue.trim();
			}
			else
				REFERENCE10 = null;
		}
		if (FCode.equalsIgnoreCase("CURRENCY_CONVERSION_DATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CURRENCY_CONVERSION_DATE = fDate.getDate( FValue );
			}
			else
				CURRENCY_CONVERSION_DATE = null;
		}
		if (FCode.equalsIgnoreCase("CURRENCY_CONVERSION_RATE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CURRENCY_CONVERSION_RATE = d;
			}
		}
		if (FCode.equalsIgnoreCase("ACCOUNTED_DR"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ACCOUNTED_DR = d;
			}
		}
		if (FCode.equalsIgnoreCase("ACCOUNTED_CR"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ACCOUNTED_CR = d;
			}
		}
		if (FCode.equalsIgnoreCase("ATTRIBUTE1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ATTRIBUTE1 = FValue.trim();
			}
			else
				ATTRIBUTE1 = null;
		}
		if (FCode.equalsIgnoreCase("ATTRIBUTE2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ATTRIBUTE2 = FValue.trim();
			}
			else
				ATTRIBUTE2 = null;
		}
		if (FCode.equalsIgnoreCase("ATTRIBUTE3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ATTRIBUTE3 = FValue.trim();
			}
			else
				ATTRIBUTE3 = null;
		}
		if (FCode.equalsIgnoreCase("ATTRIBUTE4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ATTRIBUTE4 = FValue.trim();
			}
			else
				ATTRIBUTE4 = null;
		}
		if (FCode.equalsIgnoreCase("ATTRIBUTE5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ATTRIBUTE5 = FValue.trim();
			}
			else
				ATTRIBUTE5 = null;
		}
		if (FCode.equalsIgnoreCase("ATTRIBUTE6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ATTRIBUTE6 = FValue.trim();
			}
			else
				ATTRIBUTE6 = null;
		}
		if (FCode.equalsIgnoreCase("ATTRIBUTE7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ATTRIBUTE7 = FValue.trim();
			}
			else
				ATTRIBUTE7 = null;
		}
		if (FCode.equalsIgnoreCase("ATTRIBUTE8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ATTRIBUTE8 = FValue.trim();
			}
			else
				ATTRIBUTE8 = null;
		}
		if (FCode.equalsIgnoreCase("ATTRIBUTE9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ATTRIBUTE9 = FValue.trim();
			}
			else
				ATTRIBUTE9 = null;
		}
		if (FCode.equalsIgnoreCase("ATTRIBUTE10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ATTRIBUTE10 = FValue.trim();
			}
			else
				ATTRIBUTE10 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LP_GL_INTERFACESchema other = (LP_GL_INTERFACESchema)otherObject;
		return
			ROW_ID == other.getROW_ID()
			&& CURRENCY_CODE.equals(other.getCURRENCY_CODE())
			&& USER_JE_CATEGORY_NAME.equals(other.getUSER_JE_CATEGORY_NAME())
			&& SEGMENT1.equals(other.getSEGMENT1())
			&& SEGMENT2.equals(other.getSEGMENT2())
			&& SEGMENT3.equals(other.getSEGMENT3())
			&& SEGMENT4.equals(other.getSEGMENT4())
			&& SEGMENT5.equals(other.getSEGMENT5())
			&& SEGMENT6.equals(other.getSEGMENT6())
			&& SEGMENT7.equals(other.getSEGMENT7())
			&& SEGMENT8.equals(other.getSEGMENT8())
			&& fDate.getString(TRANSACTION_DATE).equals(other.getTRANSACTION_DATE())
			&& POSTING_ID == other.getPOSTING_ID()
			&& ENTERED_DR == other.getENTERED_DR()
			&& ENTERED_CR == other.getENTERED_CR()
			&& REFERENCE5.equals(other.getREFERENCE5())
			&& REFERENCE10.equals(other.getREFERENCE10())
			&& fDate.getString(CURRENCY_CONVERSION_DATE).equals(other.getCURRENCY_CONVERSION_DATE())
			&& CURRENCY_CONVERSION_RATE == other.getCURRENCY_CONVERSION_RATE()
			&& ACCOUNTED_DR == other.getACCOUNTED_DR()
			&& ACCOUNTED_CR == other.getACCOUNTED_CR()
			&& ATTRIBUTE1.equals(other.getATTRIBUTE1())
			&& ATTRIBUTE2.equals(other.getATTRIBUTE2())
			&& ATTRIBUTE3.equals(other.getATTRIBUTE3())
			&& ATTRIBUTE4.equals(other.getATTRIBUTE4())
			&& ATTRIBUTE5.equals(other.getATTRIBUTE5())
			&& ATTRIBUTE6.equals(other.getATTRIBUTE6())
			&& ATTRIBUTE7.equals(other.getATTRIBUTE7())
			&& ATTRIBUTE8.equals(other.getATTRIBUTE8())
			&& ATTRIBUTE9.equals(other.getATTRIBUTE9())
			&& ATTRIBUTE10.equals(other.getATTRIBUTE10());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ROW_ID") ) {
			return 0;
		}
		if( strFieldName.equals("CURRENCY_CODE") ) {
			return 1;
		}
		if( strFieldName.equals("USER_JE_CATEGORY_NAME") ) {
			return 2;
		}
		if( strFieldName.equals("SEGMENT1") ) {
			return 3;
		}
		if( strFieldName.equals("SEGMENT2") ) {
			return 4;
		}
		if( strFieldName.equals("SEGMENT3") ) {
			return 5;
		}
		if( strFieldName.equals("SEGMENT4") ) {
			return 6;
		}
		if( strFieldName.equals("SEGMENT5") ) {
			return 7;
		}
		if( strFieldName.equals("SEGMENT6") ) {
			return 8;
		}
		if( strFieldName.equals("SEGMENT7") ) {
			return 9;
		}
		if( strFieldName.equals("SEGMENT8") ) {
			return 10;
		}
		if( strFieldName.equals("TRANSACTION_DATE") ) {
			return 11;
		}
		if( strFieldName.equals("POSTING_ID") ) {
			return 12;
		}
		if( strFieldName.equals("ENTERED_DR") ) {
			return 13;
		}
		if( strFieldName.equals("ENTERED_CR") ) {
			return 14;
		}
		if( strFieldName.equals("REFERENCE5") ) {
			return 15;
		}
		if( strFieldName.equals("REFERENCE10") ) {
			return 16;
		}
		if( strFieldName.equals("CURRENCY_CONVERSION_DATE") ) {
			return 17;
		}
		if( strFieldName.equals("CURRENCY_CONVERSION_RATE") ) {
			return 18;
		}
		if( strFieldName.equals("ACCOUNTED_DR") ) {
			return 19;
		}
		if( strFieldName.equals("ACCOUNTED_CR") ) {
			return 20;
		}
		if( strFieldName.equals("ATTRIBUTE1") ) {
			return 21;
		}
		if( strFieldName.equals("ATTRIBUTE2") ) {
			return 22;
		}
		if( strFieldName.equals("ATTRIBUTE3") ) {
			return 23;
		}
		if( strFieldName.equals("ATTRIBUTE4") ) {
			return 24;
		}
		if( strFieldName.equals("ATTRIBUTE5") ) {
			return 25;
		}
		if( strFieldName.equals("ATTRIBUTE6") ) {
			return 26;
		}
		if( strFieldName.equals("ATTRIBUTE7") ) {
			return 27;
		}
		if( strFieldName.equals("ATTRIBUTE8") ) {
			return 28;
		}
		if( strFieldName.equals("ATTRIBUTE9") ) {
			return 29;
		}
		if( strFieldName.equals("ATTRIBUTE10") ) {
			return 30;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ROW_ID";
				break;
			case 1:
				strFieldName = "CURRENCY_CODE";
				break;
			case 2:
				strFieldName = "USER_JE_CATEGORY_NAME";
				break;
			case 3:
				strFieldName = "SEGMENT1";
				break;
			case 4:
				strFieldName = "SEGMENT2";
				break;
			case 5:
				strFieldName = "SEGMENT3";
				break;
			case 6:
				strFieldName = "SEGMENT4";
				break;
			case 7:
				strFieldName = "SEGMENT5";
				break;
			case 8:
				strFieldName = "SEGMENT6";
				break;
			case 9:
				strFieldName = "SEGMENT7";
				break;
			case 10:
				strFieldName = "SEGMENT8";
				break;
			case 11:
				strFieldName = "TRANSACTION_DATE";
				break;
			case 12:
				strFieldName = "POSTING_ID";
				break;
			case 13:
				strFieldName = "ENTERED_DR";
				break;
			case 14:
				strFieldName = "ENTERED_CR";
				break;
			case 15:
				strFieldName = "REFERENCE5";
				break;
			case 16:
				strFieldName = "REFERENCE10";
				break;
			case 17:
				strFieldName = "CURRENCY_CONVERSION_DATE";
				break;
			case 18:
				strFieldName = "CURRENCY_CONVERSION_RATE";
				break;
			case 19:
				strFieldName = "ACCOUNTED_DR";
				break;
			case 20:
				strFieldName = "ACCOUNTED_CR";
				break;
			case 21:
				strFieldName = "ATTRIBUTE1";
				break;
			case 22:
				strFieldName = "ATTRIBUTE2";
				break;
			case 23:
				strFieldName = "ATTRIBUTE3";
				break;
			case 24:
				strFieldName = "ATTRIBUTE4";
				break;
			case 25:
				strFieldName = "ATTRIBUTE5";
				break;
			case 26:
				strFieldName = "ATTRIBUTE6";
				break;
			case 27:
				strFieldName = "ATTRIBUTE7";
				break;
			case 28:
				strFieldName = "ATTRIBUTE8";
				break;
			case 29:
				strFieldName = "ATTRIBUTE9";
				break;
			case 30:
				strFieldName = "ATTRIBUTE10";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ROW_ID") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CURRENCY_CODE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("USER_JE_CATEGORY_NAME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SEGMENT1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SEGMENT2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SEGMENT3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SEGMENT4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SEGMENT5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SEGMENT6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SEGMENT7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SEGMENT8") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TRANSACTION_DATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("POSTING_ID") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ENTERED_DR") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ENTERED_CR") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("REFERENCE5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("REFERENCE10") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CURRENCY_CONVERSION_DATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CURRENCY_CONVERSION_RATE") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ACCOUNTED_DR") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ACCOUNTED_CR") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ATTRIBUTE1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ATTRIBUTE2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ATTRIBUTE3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ATTRIBUTE4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ATTRIBUTE5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ATTRIBUTE6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ATTRIBUTE7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ATTRIBUTE8") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ATTRIBUTE9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ATTRIBUTE10") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
