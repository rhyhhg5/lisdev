/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIVoucherDataDetailDB;

/*
 * <p>ClassName: FIVoucherDataDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIVoucherDataDetailSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 分录数据流水号 */
	private String FSerialNo;
	/** 批次号 */
	private String BatchNo;
	/** 版本编号 */
	private String VersionNo;
	/** 核心凭证类型 */
	private String ClassType;
	/** 凭证类型编号 */
	private String CertificateID;
	/** 费用类型编码 */
	private String CostID;
	/** 接口凭证类型 */
	private String VoucherType;
	/** 科目代码 */
	private String AccountCode;
	/** 借贷标志 */
	private String FinItemType;
	/** 费用金额 */
	private double SumMoney;
	/** 账务日期 */
	private Date AccountDate;
	/** 销售渠道 */
	private String SaleChnl;
	/** 管理机构 */
	private String ManageCom;
	/** 对方机构 */
	private String ExecuteCom;
	/** 险种编码 */
	private String RiskCode;
	/** 成本中心 */
	private String CostCenter;
	/** 交费方式(期数) */
	private String Pcont;
	/** 保费收入类型 */
	private String PremiumType;
	/** 保单年度 */
	private int PolYear;
	/** 保险年期 */
	private int Years;
	/** 大客户 */
	private String BClient;
	/** 供应商号码 */
	private String SupplierNo;
	/** 市场类型 */
	private String MarketType;
	/** 预算信息 */
	private String Budget;
	/** 现金流量 */
	private String CashFlowNo;
	/** 计费首年 */
	private String FirstYear;
	/** 币别 */
	private String Currency;
	/** 票据号码 */
	private String NotesNo;
	/** 银行帐号 */
	private String BankAccNo;
	/** 银行编码 */
	private String BankCode;
	/** 代理机构 */
	private String AgentCom;
	/** 代理人编码 */
	private String AgentCode;
	/** 代理人组别 */
	private String AgentGroup;
	/** 个团银明细标志 */
	private String ListFlag;
	/** 保单号码 */
	private String ContNo;
	/** 凭证汇总流水号 */
	private String VSerialNo;
	/** 备用字符串一 */
	private String StandByString1;
	/** 备用字符串二 */
	private String StandByString2;
	/** 备用字符串三 */
	private String StandByString3;
	/** 备用数字一 */
	private double StandByNum1;
	/** 备用数字二 */
	private double StandByNum2;
	/** 备用日期一 */
	private Date StandByDate1;
	/** 备用日期二 */
	private Date StandByDate2;
	/** 读取状态 */
	private String ReadState;
	/** 校检标记 */
	private String CheckFlag;

	public static final int FIELDNUM = 46;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIVoucherDataDetailSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "FSerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIVoucherDataDetailSchema cloned = (FIVoucherDataDetailSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getFSerialNo()
	{
		return FSerialNo;
	}
	public void setFSerialNo(String aFSerialNo)
	{
		FSerialNo = aFSerialNo;
	}
	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getClassType()
	{
		return ClassType;
	}
	public void setClassType(String aClassType)
	{
		ClassType = aClassType;
	}
	public String getCertificateID()
	{
		return CertificateID;
	}
	public void setCertificateID(String aCertificateID)
	{
		CertificateID = aCertificateID;
	}
	public String getCostID()
	{
		return CostID;
	}
	public void setCostID(String aCostID)
	{
		CostID = aCostID;
	}
	public String getVoucherType()
	{
		return VoucherType;
	}
	public void setVoucherType(String aVoucherType)
	{
		VoucherType = aVoucherType;
	}
	public String getAccountCode()
	{
		return AccountCode;
	}
	public void setAccountCode(String aAccountCode)
	{
		AccountCode = aAccountCode;
	}
	public String getFinItemType()
	{
		return FinItemType;
	}
	public void setFinItemType(String aFinItemType)
	{
		FinItemType = aFinItemType;
	}
	public double getSumMoney()
	{
		return SumMoney;
	}
	public void setSumMoney(double aSumMoney)
	{
		SumMoney = Arith.round(aSumMoney,2);
	}
	public void setSumMoney(String aSumMoney)
	{
		if (aSumMoney != null && !aSumMoney.equals(""))
		{
			Double tDouble = new Double(aSumMoney);
			double d = tDouble.doubleValue();
                SumMoney = Arith.round(d,2);
		}
	}

	public String getAccountDate()
	{
		if( AccountDate != null )
			return fDate.getString(AccountDate);
		else
			return null;
	}
	public void setAccountDate(Date aAccountDate)
	{
		AccountDate = aAccountDate;
	}
	public void setAccountDate(String aAccountDate)
	{
		if (aAccountDate != null && !aAccountDate.equals("") )
		{
			AccountDate = fDate.getDate( aAccountDate );
		}
		else
			AccountDate = null;
	}

	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getExecuteCom()
	{
		return ExecuteCom;
	}
	public void setExecuteCom(String aExecuteCom)
	{
		ExecuteCom = aExecuteCom;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getPcont()
	{
		return Pcont;
	}
	public void setPcont(String aPcont)
	{
		Pcont = aPcont;
	}
	public String getPremiumType()
	{
		return PremiumType;
	}
	public void setPremiumType(String aPremiumType)
	{
		PremiumType = aPremiumType;
	}
	public int getPolYear()
	{
		return PolYear;
	}
	public void setPolYear(int aPolYear)
	{
		PolYear = aPolYear;
	}
	public void setPolYear(String aPolYear)
	{
		if (aPolYear != null && !aPolYear.equals(""))
		{
			Integer tInteger = new Integer(aPolYear);
			int i = tInteger.intValue();
			PolYear = i;
		}
	}

	public int getYears()
	{
		return Years;
	}
	public void setYears(int aYears)
	{
		Years = aYears;
	}
	public void setYears(String aYears)
	{
		if (aYears != null && !aYears.equals(""))
		{
			Integer tInteger = new Integer(aYears);
			int i = tInteger.intValue();
			Years = i;
		}
	}

	public String getBClient()
	{
		return BClient;
	}
	public void setBClient(String aBClient)
	{
		BClient = aBClient;
	}
	public String getSupplierNo()
	{
		return SupplierNo;
	}
	public void setSupplierNo(String aSupplierNo)
	{
		SupplierNo = aSupplierNo;
	}
	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}
	public String getBudget()
	{
		return Budget;
	}
	public void setBudget(String aBudget)
	{
		Budget = aBudget;
	}
	public String getCashFlowNo()
	{
		return CashFlowNo;
	}
	public void setCashFlowNo(String aCashFlowNo)
	{
		CashFlowNo = aCashFlowNo;
	}
	public String getFirstYear()
	{
		return FirstYear;
	}
	public void setFirstYear(String aFirstYear)
	{
		FirstYear = aFirstYear;
	}
	public String getCurrency()
	{
		return Currency;
	}
	public void setCurrency(String aCurrency)
	{
		Currency = aCurrency;
	}
	public String getNotesNo()
	{
		return NotesNo;
	}
	public void setNotesNo(String aNotesNo)
	{
		NotesNo = aNotesNo;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getAgentCom()
	{
		return AgentCom;
	}
	public void setAgentCom(String aAgentCom)
	{
		AgentCom = aAgentCom;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getListFlag()
	{
		return ListFlag;
	}
	public void setListFlag(String aListFlag)
	{
		ListFlag = aListFlag;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getVSerialNo()
	{
		return VSerialNo;
	}
	public void setVSerialNo(String aVSerialNo)
	{
		VSerialNo = aVSerialNo;
	}
	public String getStandByString1()
	{
		return StandByString1;
	}
	public void setStandByString1(String aStandByString1)
	{
		StandByString1 = aStandByString1;
	}
	public String getStandByString2()
	{
		return StandByString2;
	}
	public void setStandByString2(String aStandByString2)
	{
		StandByString2 = aStandByString2;
	}
	public String getStandByString3()
	{
		return StandByString3;
	}
	public void setStandByString3(String aStandByString3)
	{
		StandByString3 = aStandByString3;
	}
	public double getStandByNum1()
	{
		return StandByNum1;
	}
	public void setStandByNum1(double aStandByNum1)
	{
		StandByNum1 = Arith.round(aStandByNum1,2);
	}
	public void setStandByNum1(String aStandByNum1)
	{
		if (aStandByNum1 != null && !aStandByNum1.equals(""))
		{
			Double tDouble = new Double(aStandByNum1);
			double d = tDouble.doubleValue();
                StandByNum1 = Arith.round(d,2);
		}
	}

	public double getStandByNum2()
	{
		return StandByNum2;
	}
	public void setStandByNum2(double aStandByNum2)
	{
		StandByNum2 = Arith.round(aStandByNum2,2);
	}
	public void setStandByNum2(String aStandByNum2)
	{
		if (aStandByNum2 != null && !aStandByNum2.equals(""))
		{
			Double tDouble = new Double(aStandByNum2);
			double d = tDouble.doubleValue();
                StandByNum2 = Arith.round(d,2);
		}
	}

	public String getStandByDate1()
	{
		if( StandByDate1 != null )
			return fDate.getString(StandByDate1);
		else
			return null;
	}
	public void setStandByDate1(Date aStandByDate1)
	{
		StandByDate1 = aStandByDate1;
	}
	public void setStandByDate1(String aStandByDate1)
	{
		if (aStandByDate1 != null && !aStandByDate1.equals("") )
		{
			StandByDate1 = fDate.getDate( aStandByDate1 );
		}
		else
			StandByDate1 = null;
	}

	public String getStandByDate2()
	{
		if( StandByDate2 != null )
			return fDate.getString(StandByDate2);
		else
			return null;
	}
	public void setStandByDate2(Date aStandByDate2)
	{
		StandByDate2 = aStandByDate2;
	}
	public void setStandByDate2(String aStandByDate2)
	{
		if (aStandByDate2 != null && !aStandByDate2.equals("") )
		{
			StandByDate2 = fDate.getDate( aStandByDate2 );
		}
		else
			StandByDate2 = null;
	}

	public String getReadState()
	{
		return ReadState;
	}
	public void setReadState(String aReadState)
	{
		ReadState = aReadState;
	}
	public String getCheckFlag()
	{
		return CheckFlag;
	}
	public void setCheckFlag(String aCheckFlag)
	{
		CheckFlag = aCheckFlag;
	}

	/**
	* 使用另外一个 FIVoucherDataDetailSchema 对象给 Schema 赋值
	* @param: aFIVoucherDataDetailSchema FIVoucherDataDetailSchema
	**/
	public void setSchema(FIVoucherDataDetailSchema aFIVoucherDataDetailSchema)
	{
		this.SerialNo = aFIVoucherDataDetailSchema.getSerialNo();
		this.FSerialNo = aFIVoucherDataDetailSchema.getFSerialNo();
		this.BatchNo = aFIVoucherDataDetailSchema.getBatchNo();
		this.VersionNo = aFIVoucherDataDetailSchema.getVersionNo();
		this.ClassType = aFIVoucherDataDetailSchema.getClassType();
		this.CertificateID = aFIVoucherDataDetailSchema.getCertificateID();
		this.CostID = aFIVoucherDataDetailSchema.getCostID();
		this.VoucherType = aFIVoucherDataDetailSchema.getVoucherType();
		this.AccountCode = aFIVoucherDataDetailSchema.getAccountCode();
		this.FinItemType = aFIVoucherDataDetailSchema.getFinItemType();
		this.SumMoney = aFIVoucherDataDetailSchema.getSumMoney();
		this.AccountDate = fDate.getDate( aFIVoucherDataDetailSchema.getAccountDate());
		this.SaleChnl = aFIVoucherDataDetailSchema.getSaleChnl();
		this.ManageCom = aFIVoucherDataDetailSchema.getManageCom();
		this.ExecuteCom = aFIVoucherDataDetailSchema.getExecuteCom();
		this.RiskCode = aFIVoucherDataDetailSchema.getRiskCode();
		this.CostCenter = aFIVoucherDataDetailSchema.getCostCenter();
		this.Pcont = aFIVoucherDataDetailSchema.getPcont();
		this.PremiumType = aFIVoucherDataDetailSchema.getPremiumType();
		this.PolYear = aFIVoucherDataDetailSchema.getPolYear();
		this.Years = aFIVoucherDataDetailSchema.getYears();
		this.BClient = aFIVoucherDataDetailSchema.getBClient();
		this.SupplierNo = aFIVoucherDataDetailSchema.getSupplierNo();
		this.MarketType = aFIVoucherDataDetailSchema.getMarketType();
		this.Budget = aFIVoucherDataDetailSchema.getBudget();
		this.CashFlowNo = aFIVoucherDataDetailSchema.getCashFlowNo();
		this.FirstYear = aFIVoucherDataDetailSchema.getFirstYear();
		this.Currency = aFIVoucherDataDetailSchema.getCurrency();
		this.NotesNo = aFIVoucherDataDetailSchema.getNotesNo();
		this.BankAccNo = aFIVoucherDataDetailSchema.getBankAccNo();
		this.BankCode = aFIVoucherDataDetailSchema.getBankCode();
		this.AgentCom = aFIVoucherDataDetailSchema.getAgentCom();
		this.AgentCode = aFIVoucherDataDetailSchema.getAgentCode();
		this.AgentGroup = aFIVoucherDataDetailSchema.getAgentGroup();
		this.ListFlag = aFIVoucherDataDetailSchema.getListFlag();
		this.ContNo = aFIVoucherDataDetailSchema.getContNo();
		this.VSerialNo = aFIVoucherDataDetailSchema.getVSerialNo();
		this.StandByString1 = aFIVoucherDataDetailSchema.getStandByString1();
		this.StandByString2 = aFIVoucherDataDetailSchema.getStandByString2();
		this.StandByString3 = aFIVoucherDataDetailSchema.getStandByString3();
		this.StandByNum1 = aFIVoucherDataDetailSchema.getStandByNum1();
		this.StandByNum2 = aFIVoucherDataDetailSchema.getStandByNum2();
		this.StandByDate1 = fDate.getDate( aFIVoucherDataDetailSchema.getStandByDate1());
		this.StandByDate2 = fDate.getDate( aFIVoucherDataDetailSchema.getStandByDate2());
		this.ReadState = aFIVoucherDataDetailSchema.getReadState();
		this.CheckFlag = aFIVoucherDataDetailSchema.getCheckFlag();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("FSerialNo") == null )
				this.FSerialNo = null;
			else
				this.FSerialNo = rs.getString("FSerialNo").trim();

			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("ClassType") == null )
				this.ClassType = null;
			else
				this.ClassType = rs.getString("ClassType").trim();

			if( rs.getString("CertificateID") == null )
				this.CertificateID = null;
			else
				this.CertificateID = rs.getString("CertificateID").trim();

			if( rs.getString("CostID") == null )
				this.CostID = null;
			else
				this.CostID = rs.getString("CostID").trim();

			if( rs.getString("VoucherType") == null )
				this.VoucherType = null;
			else
				this.VoucherType = rs.getString("VoucherType").trim();

			if( rs.getString("AccountCode") == null )
				this.AccountCode = null;
			else
				this.AccountCode = rs.getString("AccountCode").trim();

			if( rs.getString("FinItemType") == null )
				this.FinItemType = null;
			else
				this.FinItemType = rs.getString("FinItemType").trim();

			this.SumMoney = rs.getDouble("SumMoney");
			this.AccountDate = rs.getDate("AccountDate");
			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ExecuteCom") == null )
				this.ExecuteCom = null;
			else
				this.ExecuteCom = rs.getString("ExecuteCom").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("Pcont") == null )
				this.Pcont = null;
			else
				this.Pcont = rs.getString("Pcont").trim();

			if( rs.getString("PremiumType") == null )
				this.PremiumType = null;
			else
				this.PremiumType = rs.getString("PremiumType").trim();

			this.PolYear = rs.getInt("PolYear");
			this.Years = rs.getInt("Years");
			if( rs.getString("BClient") == null )
				this.BClient = null;
			else
				this.BClient = rs.getString("BClient").trim();

			if( rs.getString("SupplierNo") == null )
				this.SupplierNo = null;
			else
				this.SupplierNo = rs.getString("SupplierNo").trim();

			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

			if( rs.getString("Budget") == null )
				this.Budget = null;
			else
				this.Budget = rs.getString("Budget").trim();

			if( rs.getString("CashFlowNo") == null )
				this.CashFlowNo = null;
			else
				this.CashFlowNo = rs.getString("CashFlowNo").trim();

			if( rs.getString("FirstYear") == null )
				this.FirstYear = null;
			else
				this.FirstYear = rs.getString("FirstYear").trim();

			if( rs.getString("Currency") == null )
				this.Currency = null;
			else
				this.Currency = rs.getString("Currency").trim();

			if( rs.getString("NotesNo") == null )
				this.NotesNo = null;
			else
				this.NotesNo = rs.getString("NotesNo").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("AgentCom") == null )
				this.AgentCom = null;
			else
				this.AgentCom = rs.getString("AgentCom").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("ListFlag") == null )
				this.ListFlag = null;
			else
				this.ListFlag = rs.getString("ListFlag").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("VSerialNo") == null )
				this.VSerialNo = null;
			else
				this.VSerialNo = rs.getString("VSerialNo").trim();

			if( rs.getString("StandByString1") == null )
				this.StandByString1 = null;
			else
				this.StandByString1 = rs.getString("StandByString1").trim();

			if( rs.getString("StandByString2") == null )
				this.StandByString2 = null;
			else
				this.StandByString2 = rs.getString("StandByString2").trim();

			if( rs.getString("StandByString3") == null )
				this.StandByString3 = null;
			else
				this.StandByString3 = rs.getString("StandByString3").trim();

			this.StandByNum1 = rs.getDouble("StandByNum1");
			this.StandByNum2 = rs.getDouble("StandByNum2");
			this.StandByDate1 = rs.getDate("StandByDate1");
			this.StandByDate2 = rs.getDate("StandByDate2");
			if( rs.getString("ReadState") == null )
				this.ReadState = null;
			else
				this.ReadState = rs.getString("ReadState").trim();

			if( rs.getString("CheckFlag") == null )
				this.CheckFlag = null;
			else
				this.CheckFlag = rs.getString("CheckFlag").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIVoucherDataDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherDataDetailSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIVoucherDataDetailSchema getSchema()
	{
		FIVoucherDataDetailSchema aFIVoucherDataDetailSchema = new FIVoucherDataDetailSchema();
		aFIVoucherDataDetailSchema.setSchema(this);
		return aFIVoucherDataDetailSchema;
	}

	public FIVoucherDataDetailDB getDB()
	{
		FIVoucherDataDetailDB aDBOper = new FIVoucherDataDetailDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVoucherDataDetail描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClassType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertificateID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccountCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FinItemType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccountDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ExecuteCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Pcont)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PremiumType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PolYear));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Years));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BClient)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SupplierNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Budget)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CashFlowNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FirstYear)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Currency)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NotesNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ListFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandByNum1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(StandByNum2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandByDate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandByDate2 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReadState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CheckFlag));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVoucherDataDetail>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			FSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ClassType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CertificateID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CostID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			VoucherType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			AccountCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			FinItemType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			SumMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			AccountDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ExecuteCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Pcont = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			PremiumType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			PolYear= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).intValue();
			Years= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).intValue();
			BClient = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			SupplierNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			Budget = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			CashFlowNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			FirstYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Currency = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			NotesNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			ListFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			VSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			StandByString1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			StandByString2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			StandByString3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			StandByNum1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,41,SysConst.PACKAGESPILTER))).doubleValue();
			StandByNum2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,42,SysConst.PACKAGESPILTER))).doubleValue();
			StandByDate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
			StandByDate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,SysConst.PACKAGESPILTER));
			ReadState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			CheckFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherDataDetailSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("FSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FSerialNo));
		}
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("ClassType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassType));
		}
		if (FCode.equals("CertificateID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertificateID));
		}
		if (FCode.equals("CostID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostID));
		}
		if (FCode.equals("VoucherType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherType));
		}
		if (FCode.equals("AccountCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountCode));
		}
		if (FCode.equals("FinItemType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FinItemType));
		}
		if (FCode.equals("SumMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumMoney));
		}
		if (FCode.equals("AccountDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccountDate()));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ExecuteCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteCom));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("Pcont"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Pcont));
		}
		if (FCode.equals("PremiumType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PremiumType));
		}
		if (FCode.equals("PolYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolYear));
		}
		if (FCode.equals("Years"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Years));
		}
		if (FCode.equals("BClient"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BClient));
		}
		if (FCode.equals("SupplierNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SupplierNo));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (FCode.equals("Budget"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Budget));
		}
		if (FCode.equals("CashFlowNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CashFlowNo));
		}
		if (FCode.equals("FirstYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstYear));
		}
		if (FCode.equals("Currency"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Currency));
		}
		if (FCode.equals("NotesNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NotesNo));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("AgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("ListFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ListFlag));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("VSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VSerialNo));
		}
		if (FCode.equals("StandByString1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString1));
		}
		if (FCode.equals("StandByString2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString2));
		}
		if (FCode.equals("StandByString3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString3));
		}
		if (FCode.equals("StandByNum1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByNum1));
		}
		if (FCode.equals("StandByNum2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByNum2));
		}
		if (FCode.equals("StandByDate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate1()));
		}
		if (FCode.equals("StandByDate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate2()));
		}
		if (FCode.equals("ReadState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReadState));
		}
		if (FCode.equals("CheckFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFlag));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(FSerialNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ClassType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CertificateID);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CostID);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(VoucherType);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(AccountCode);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(FinItemType);
				break;
			case 10:
				strFieldValue = String.valueOf(SumMoney);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccountDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ExecuteCom);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Pcont);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(PremiumType);
				break;
			case 19:
				strFieldValue = String.valueOf(PolYear);
				break;
			case 20:
				strFieldValue = String.valueOf(Years);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(BClient);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(SupplierNo);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Budget);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(CashFlowNo);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(FirstYear);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Currency);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(NotesNo);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(AgentCom);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(ListFlag);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(VSerialNo);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(StandByString1);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(StandByString2);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(StandByString3);
				break;
			case 40:
				strFieldValue = String.valueOf(StandByNum1);
				break;
			case 41:
				strFieldValue = String.valueOf(StandByNum2);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate1()));
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate2()));
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(ReadState);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(CheckFlag);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("FSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FSerialNo = FValue.trim();
			}
			else
				FSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("ClassType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassType = FValue.trim();
			}
			else
				ClassType = null;
		}
		if (FCode.equalsIgnoreCase("CertificateID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertificateID = FValue.trim();
			}
			else
				CertificateID = null;
		}
		if (FCode.equalsIgnoreCase("CostID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostID = FValue.trim();
			}
			else
				CostID = null;
		}
		if (FCode.equalsIgnoreCase("VoucherType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherType = FValue.trim();
			}
			else
				VoucherType = null;
		}
		if (FCode.equalsIgnoreCase("AccountCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccountCode = FValue.trim();
			}
			else
				AccountCode = null;
		}
		if (FCode.equalsIgnoreCase("FinItemType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FinItemType = FValue.trim();
			}
			else
				FinItemType = null;
		}
		if (FCode.equalsIgnoreCase("SumMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("AccountDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccountDate = fDate.getDate( FValue );
			}
			else
				AccountDate = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ExecuteCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExecuteCom = FValue.trim();
			}
			else
				ExecuteCom = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("Pcont"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Pcont = FValue.trim();
			}
			else
				Pcont = null;
		}
		if (FCode.equalsIgnoreCase("PremiumType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PremiumType = FValue.trim();
			}
			else
				PremiumType = null;
		}
		if (FCode.equalsIgnoreCase("PolYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolYear = i;
			}
		}
		if (FCode.equalsIgnoreCase("Years"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Years = i;
			}
		}
		if (FCode.equalsIgnoreCase("BClient"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BClient = FValue.trim();
			}
			else
				BClient = null;
		}
		if (FCode.equalsIgnoreCase("SupplierNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SupplierNo = FValue.trim();
			}
			else
				SupplierNo = null;
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		if (FCode.equalsIgnoreCase("Budget"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Budget = FValue.trim();
			}
			else
				Budget = null;
		}
		if (FCode.equalsIgnoreCase("CashFlowNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CashFlowNo = FValue.trim();
			}
			else
				CashFlowNo = null;
		}
		if (FCode.equalsIgnoreCase("FirstYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FirstYear = FValue.trim();
			}
			else
				FirstYear = null;
		}
		if (FCode.equalsIgnoreCase("Currency"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Currency = FValue.trim();
			}
			else
				Currency = null;
		}
		if (FCode.equalsIgnoreCase("NotesNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NotesNo = FValue.trim();
			}
			else
				NotesNo = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCom = FValue.trim();
			}
			else
				AgentCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("ListFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ListFlag = FValue.trim();
			}
			else
				ListFlag = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("VSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VSerialNo = FValue.trim();
			}
			else
				VSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("StandByString1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString1 = FValue.trim();
			}
			else
				StandByString1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByString2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString2 = FValue.trim();
			}
			else
				StandByString2 = null;
		}
		if (FCode.equalsIgnoreCase("StandByString3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString3 = FValue.trim();
			}
			else
				StandByString3 = null;
		}
		if (FCode.equalsIgnoreCase("StandByNum1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandByNum1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandByNum2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				StandByNum2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("StandByDate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandByDate1 = fDate.getDate( FValue );
			}
			else
				StandByDate1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByDate2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandByDate2 = fDate.getDate( FValue );
			}
			else
				StandByDate2 = null;
		}
		if (FCode.equalsIgnoreCase("ReadState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReadState = FValue.trim();
			}
			else
				ReadState = null;
		}
		if (FCode.equalsIgnoreCase("CheckFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CheckFlag = FValue.trim();
			}
			else
				CheckFlag = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIVoucherDataDetailSchema other = (FIVoucherDataDetailSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (FSerialNo == null ? other.getFSerialNo() == null : FSerialNo.equals(other.getFSerialNo()))
			&& (BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (ClassType == null ? other.getClassType() == null : ClassType.equals(other.getClassType()))
			&& (CertificateID == null ? other.getCertificateID() == null : CertificateID.equals(other.getCertificateID()))
			&& (CostID == null ? other.getCostID() == null : CostID.equals(other.getCostID()))
			&& (VoucherType == null ? other.getVoucherType() == null : VoucherType.equals(other.getVoucherType()))
			&& (AccountCode == null ? other.getAccountCode() == null : AccountCode.equals(other.getAccountCode()))
			&& (FinItemType == null ? other.getFinItemType() == null : FinItemType.equals(other.getFinItemType()))
			&& SumMoney == other.getSumMoney()
			&& (AccountDate == null ? other.getAccountDate() == null : fDate.getString(AccountDate).equals(other.getAccountDate()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (ExecuteCom == null ? other.getExecuteCom() == null : ExecuteCom.equals(other.getExecuteCom()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()))
			&& (Pcont == null ? other.getPcont() == null : Pcont.equals(other.getPcont()))
			&& (PremiumType == null ? other.getPremiumType() == null : PremiumType.equals(other.getPremiumType()))
			&& PolYear == other.getPolYear()
			&& Years == other.getYears()
			&& (BClient == null ? other.getBClient() == null : BClient.equals(other.getBClient()))
			&& (SupplierNo == null ? other.getSupplierNo() == null : SupplierNo.equals(other.getSupplierNo()))
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()))
			&& (Budget == null ? other.getBudget() == null : Budget.equals(other.getBudget()))
			&& (CashFlowNo == null ? other.getCashFlowNo() == null : CashFlowNo.equals(other.getCashFlowNo()))
			&& (FirstYear == null ? other.getFirstYear() == null : FirstYear.equals(other.getFirstYear()))
			&& (Currency == null ? other.getCurrency() == null : Currency.equals(other.getCurrency()))
			&& (NotesNo == null ? other.getNotesNo() == null : NotesNo.equals(other.getNotesNo()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (AgentCom == null ? other.getAgentCom() == null : AgentCom.equals(other.getAgentCom()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (ListFlag == null ? other.getListFlag() == null : ListFlag.equals(other.getListFlag()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (VSerialNo == null ? other.getVSerialNo() == null : VSerialNo.equals(other.getVSerialNo()))
			&& (StandByString1 == null ? other.getStandByString1() == null : StandByString1.equals(other.getStandByString1()))
			&& (StandByString2 == null ? other.getStandByString2() == null : StandByString2.equals(other.getStandByString2()))
			&& (StandByString3 == null ? other.getStandByString3() == null : StandByString3.equals(other.getStandByString3()))
			&& StandByNum1 == other.getStandByNum1()
			&& StandByNum2 == other.getStandByNum2()
			&& (StandByDate1 == null ? other.getStandByDate1() == null : fDate.getString(StandByDate1).equals(other.getStandByDate1()))
			&& (StandByDate2 == null ? other.getStandByDate2() == null : fDate.getString(StandByDate2).equals(other.getStandByDate2()))
			&& (ReadState == null ? other.getReadState() == null : ReadState.equals(other.getReadState()))
			&& (CheckFlag == null ? other.getCheckFlag() == null : CheckFlag.equals(other.getCheckFlag()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("FSerialNo") ) {
			return 1;
		}
		if( strFieldName.equals("BatchNo") ) {
			return 2;
		}
		if( strFieldName.equals("VersionNo") ) {
			return 3;
		}
		if( strFieldName.equals("ClassType") ) {
			return 4;
		}
		if( strFieldName.equals("CertificateID") ) {
			return 5;
		}
		if( strFieldName.equals("CostID") ) {
			return 6;
		}
		if( strFieldName.equals("VoucherType") ) {
			return 7;
		}
		if( strFieldName.equals("AccountCode") ) {
			return 8;
		}
		if( strFieldName.equals("FinItemType") ) {
			return 9;
		}
		if( strFieldName.equals("SumMoney") ) {
			return 10;
		}
		if( strFieldName.equals("AccountDate") ) {
			return 11;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 12;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 13;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return 14;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 15;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 16;
		}
		if( strFieldName.equals("Pcont") ) {
			return 17;
		}
		if( strFieldName.equals("PremiumType") ) {
			return 18;
		}
		if( strFieldName.equals("PolYear") ) {
			return 19;
		}
		if( strFieldName.equals("Years") ) {
			return 20;
		}
		if( strFieldName.equals("BClient") ) {
			return 21;
		}
		if( strFieldName.equals("SupplierNo") ) {
			return 22;
		}
		if( strFieldName.equals("MarketType") ) {
			return 23;
		}
		if( strFieldName.equals("Budget") ) {
			return 24;
		}
		if( strFieldName.equals("CashFlowNo") ) {
			return 25;
		}
		if( strFieldName.equals("FirstYear") ) {
			return 26;
		}
		if( strFieldName.equals("Currency") ) {
			return 27;
		}
		if( strFieldName.equals("NotesNo") ) {
			return 28;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 29;
		}
		if( strFieldName.equals("BankCode") ) {
			return 30;
		}
		if( strFieldName.equals("AgentCom") ) {
			return 31;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 32;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 33;
		}
		if( strFieldName.equals("ListFlag") ) {
			return 34;
		}
		if( strFieldName.equals("ContNo") ) {
			return 35;
		}
		if( strFieldName.equals("VSerialNo") ) {
			return 36;
		}
		if( strFieldName.equals("StandByString1") ) {
			return 37;
		}
		if( strFieldName.equals("StandByString2") ) {
			return 38;
		}
		if( strFieldName.equals("StandByString3") ) {
			return 39;
		}
		if( strFieldName.equals("StandByNum1") ) {
			return 40;
		}
		if( strFieldName.equals("StandByNum2") ) {
			return 41;
		}
		if( strFieldName.equals("StandByDate1") ) {
			return 42;
		}
		if( strFieldName.equals("StandByDate2") ) {
			return 43;
		}
		if( strFieldName.equals("ReadState") ) {
			return 44;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return 45;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "FSerialNo";
				break;
			case 2:
				strFieldName = "BatchNo";
				break;
			case 3:
				strFieldName = "VersionNo";
				break;
			case 4:
				strFieldName = "ClassType";
				break;
			case 5:
				strFieldName = "CertificateID";
				break;
			case 6:
				strFieldName = "CostID";
				break;
			case 7:
				strFieldName = "VoucherType";
				break;
			case 8:
				strFieldName = "AccountCode";
				break;
			case 9:
				strFieldName = "FinItemType";
				break;
			case 10:
				strFieldName = "SumMoney";
				break;
			case 11:
				strFieldName = "AccountDate";
				break;
			case 12:
				strFieldName = "SaleChnl";
				break;
			case 13:
				strFieldName = "ManageCom";
				break;
			case 14:
				strFieldName = "ExecuteCom";
				break;
			case 15:
				strFieldName = "RiskCode";
				break;
			case 16:
				strFieldName = "CostCenter";
				break;
			case 17:
				strFieldName = "Pcont";
				break;
			case 18:
				strFieldName = "PremiumType";
				break;
			case 19:
				strFieldName = "PolYear";
				break;
			case 20:
				strFieldName = "Years";
				break;
			case 21:
				strFieldName = "BClient";
				break;
			case 22:
				strFieldName = "SupplierNo";
				break;
			case 23:
				strFieldName = "MarketType";
				break;
			case 24:
				strFieldName = "Budget";
				break;
			case 25:
				strFieldName = "CashFlowNo";
				break;
			case 26:
				strFieldName = "FirstYear";
				break;
			case 27:
				strFieldName = "Currency";
				break;
			case 28:
				strFieldName = "NotesNo";
				break;
			case 29:
				strFieldName = "BankAccNo";
				break;
			case 30:
				strFieldName = "BankCode";
				break;
			case 31:
				strFieldName = "AgentCom";
				break;
			case 32:
				strFieldName = "AgentCode";
				break;
			case 33:
				strFieldName = "AgentGroup";
				break;
			case 34:
				strFieldName = "ListFlag";
				break;
			case 35:
				strFieldName = "ContNo";
				break;
			case 36:
				strFieldName = "VSerialNo";
				break;
			case 37:
				strFieldName = "StandByString1";
				break;
			case 38:
				strFieldName = "StandByString2";
				break;
			case 39:
				strFieldName = "StandByString3";
				break;
			case 40:
				strFieldName = "StandByNum1";
				break;
			case 41:
				strFieldName = "StandByNum2";
				break;
			case 42:
				strFieldName = "StandByDate1";
				break;
			case 43:
				strFieldName = "StandByDate2";
				break;
			case 44:
				strFieldName = "ReadState";
				break;
			case 45:
				strFieldName = "CheckFlag";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClassType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertificateID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccountCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FinItemType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SumMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("AccountDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExecuteCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Pcont") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PremiumType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolYear") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Years") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BClient") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SupplierNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Budget") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CashFlowNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FirstYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Currency") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NotesNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ListFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByNum1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandByNum2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StandByDate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StandByDate2") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReadState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CheckFlag") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_INT;
				break;
			case 20:
				nFieldType = Schema.TYPE_INT;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 41:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 43:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
