/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCGrpContSubDB;

/*
 * <p>ClassName: LCGrpContSubSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 核心业务系统风险调节机制相关功能更新
 * @CreateDate：2018-05-08
 */
public class LCGrpContSubSchema implements Schema, Cloneable
{
	// @Field
	/** 印刷号码 */
	private String PrtNo;
	/** 区县 */
	private String County;
	/** 客户银行代码 */
	private String CustomerBankCode;
	/** 委托付款授权协议编号 */
	private String AuthorizeCode;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 是否有风险调节机制 */
	private String BalanceTermFlag;
	/** 结余线 */
	private String BalanceLine;
	/** 返还比例 */
	private String BalanceRate;
	/** 风险调节文本 */
	private String BalanceTxt;
	/** 结余返还公式 */
	private String BalanceFormula;
	/** 项目名称 */
	private String ProjectName;
	/** 核算周期 */
	private String AccountCycle;
	/** 成本占比 */
	private String CostRate;
	/** 返还类型 */
	private String BalanceType;
	/** 预估返还金额 */
	private String BalanceAmnt;
	/** 止损线 */
	private String StopLine;
	/** 共担线 */
	private String SharedLine;
	/** 共担比例 */
	private String SharedRate;
	/** 回补线 */
	private String RevertantLine;
	/** 回补比例 */
	private String RevertantRate;
	/** 项目编码 */
	private String ProjectNo;
	/** 备用字段1 */
	private String bak1;
	/** 备用字段2 */
	private String bak2;
	/** 备用字段3 */
	private String bak3;
	/** 备用字段4 */
	private String bak4;
	/** 备用字段5 */
	private String bak5;
	/** 是否有结余返还约定 */
	private String Rebalance;
	/** 是否有保费回补约定 */
	private String Recharge;

	public static final int FIELDNUM = 33;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCGrpContSubSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "PrtNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCGrpContSubSchema cloned = (LCGrpContSubSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getCounty()
	{
		return County;
	}
	public void setCounty(String aCounty)
	{
		County = aCounty;
	}
	public String getCustomerBankCode()
	{
		return CustomerBankCode;
	}
	public void setCustomerBankCode(String aCustomerBankCode)
	{
		CustomerBankCode = aCustomerBankCode;
	}
	public String getAuthorizeCode()
	{
		return AuthorizeCode;
	}
	public void setAuthorizeCode(String aAuthorizeCode)
	{
		AuthorizeCode = aAuthorizeCode;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBalanceTermFlag()
	{
		return BalanceTermFlag;
	}
	public void setBalanceTermFlag(String aBalanceTermFlag)
	{
		BalanceTermFlag = aBalanceTermFlag;
	}
	public String getBalanceLine()
	{
		return BalanceLine;
	}
	public void setBalanceLine(String aBalanceLine)
	{
		BalanceLine = aBalanceLine;
	}
	public String getBalanceRate()
	{
		return BalanceRate;
	}
	public void setBalanceRate(String aBalanceRate)
	{
		BalanceRate = aBalanceRate;
	}
	public String getBalanceTxt()
	{
		return BalanceTxt;
	}
	public void setBalanceTxt(String aBalanceTxt)
	{
		BalanceTxt = aBalanceTxt;
	}
	public String getBalanceFormula()
	{
		return BalanceFormula;
	}
	public void setBalanceFormula(String aBalanceFormula)
	{
		BalanceFormula = aBalanceFormula;
	}
	public String getProjectName()
	{
		return ProjectName;
	}
	public void setProjectName(String aProjectName)
	{
		ProjectName = aProjectName;
	}
	public String getAccountCycle()
	{
		return AccountCycle;
	}
	public void setAccountCycle(String aAccountCycle)
	{
		AccountCycle = aAccountCycle;
	}
	public String getCostRate()
	{
		return CostRate;
	}
	public void setCostRate(String aCostRate)
	{
		CostRate = aCostRate;
	}
	public String getBalanceType()
	{
		return BalanceType;
	}
	public void setBalanceType(String aBalanceType)
	{
		BalanceType = aBalanceType;
	}
	public String getBalanceAmnt()
	{
		return BalanceAmnt;
	}
	public void setBalanceAmnt(String aBalanceAmnt)
	{
		BalanceAmnt = aBalanceAmnt;
	}
	public String getStopLine()
	{
		return StopLine;
	}
	public void setStopLine(String aStopLine)
	{
		StopLine = aStopLine;
	}
	public String getSharedLine()
	{
		return SharedLine;
	}
	public void setSharedLine(String aSharedLine)
	{
		SharedLine = aSharedLine;
	}
	public String getSharedRate()
	{
		return SharedRate;
	}
	public void setSharedRate(String aSharedRate)
	{
		SharedRate = aSharedRate;
	}
	public String getRevertantLine()
	{
		return RevertantLine;
	}
	public void setRevertantLine(String aRevertantLine)
	{
		RevertantLine = aRevertantLine;
	}
	public String getRevertantRate()
	{
		return RevertantRate;
	}
	public void setRevertantRate(String aRevertantRate)
	{
		RevertantRate = aRevertantRate;
	}
	public String getProjectNo()
	{
		return ProjectNo;
	}
	public void setProjectNo(String aProjectNo)
	{
		ProjectNo = aProjectNo;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}
	public String getbak4()
	{
		return bak4;
	}
	public void setbak4(String abak4)
	{
		bak4 = abak4;
	}
	public String getbak5()
	{
		return bak5;
	}
	public void setbak5(String abak5)
	{
		bak5 = abak5;
	}
	public String getRebalance()
	{
		return Rebalance;
	}
	public void setRebalance(String aRebalance)
	{
		Rebalance = aRebalance;
	}
	public String getRecharge()
	{
		return Recharge;
	}
	public void setRecharge(String aRecharge)
	{
		Recharge = aRecharge;
	}

	/**
	* 使用另外一个 LCGrpContSubSchema 对象给 Schema 赋值
	* @param: aLCGrpContSubSchema LCGrpContSubSchema
	**/
	public void setSchema(LCGrpContSubSchema aLCGrpContSubSchema)
	{
		this.PrtNo = aLCGrpContSubSchema.getPrtNo();
		this.County = aLCGrpContSubSchema.getCounty();
		this.CustomerBankCode = aLCGrpContSubSchema.getCustomerBankCode();
		this.AuthorizeCode = aLCGrpContSubSchema.getAuthorizeCode();
		this.ManageCom = aLCGrpContSubSchema.getManageCom();
		this.Operator = aLCGrpContSubSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCGrpContSubSchema.getMakeDate());
		this.MakeTime = aLCGrpContSubSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCGrpContSubSchema.getModifyDate());
		this.ModifyTime = aLCGrpContSubSchema.getModifyTime();
		this.BalanceTermFlag = aLCGrpContSubSchema.getBalanceTermFlag();
		this.BalanceLine = aLCGrpContSubSchema.getBalanceLine();
		this.BalanceRate = aLCGrpContSubSchema.getBalanceRate();
		this.BalanceTxt = aLCGrpContSubSchema.getBalanceTxt();
		this.BalanceFormula = aLCGrpContSubSchema.getBalanceFormula();
		this.ProjectName = aLCGrpContSubSchema.getProjectName();
		this.AccountCycle = aLCGrpContSubSchema.getAccountCycle();
		this.CostRate = aLCGrpContSubSchema.getCostRate();
		this.BalanceType = aLCGrpContSubSchema.getBalanceType();
		this.BalanceAmnt = aLCGrpContSubSchema.getBalanceAmnt();
		this.StopLine = aLCGrpContSubSchema.getStopLine();
		this.SharedLine = aLCGrpContSubSchema.getSharedLine();
		this.SharedRate = aLCGrpContSubSchema.getSharedRate();
		this.RevertantLine = aLCGrpContSubSchema.getRevertantLine();
		this.RevertantRate = aLCGrpContSubSchema.getRevertantRate();
		this.ProjectNo = aLCGrpContSubSchema.getProjectNo();
		this.bak1 = aLCGrpContSubSchema.getbak1();
		this.bak2 = aLCGrpContSubSchema.getbak2();
		this.bak3 = aLCGrpContSubSchema.getbak3();
		this.bak4 = aLCGrpContSubSchema.getbak4();
		this.bak5 = aLCGrpContSubSchema.getbak5();
		this.Rebalance = aLCGrpContSubSchema.getRebalance();
		this.Recharge = aLCGrpContSubSchema.getRecharge();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("County") == null )
				this.County = null;
			else
				this.County = rs.getString("County").trim();

			if( rs.getString("CustomerBankCode") == null )
				this.CustomerBankCode = null;
			else
				this.CustomerBankCode = rs.getString("CustomerBankCode").trim();

			if( rs.getString("AuthorizeCode") == null )
				this.AuthorizeCode = null;
			else
				this.AuthorizeCode = rs.getString("AuthorizeCode").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BalanceTermFlag") == null )
				this.BalanceTermFlag = null;
			else
				this.BalanceTermFlag = rs.getString("BalanceTermFlag").trim();

			if( rs.getString("BalanceLine") == null )
				this.BalanceLine = null;
			else
				this.BalanceLine = rs.getString("BalanceLine").trim();

			if( rs.getString("BalanceRate") == null )
				this.BalanceRate = null;
			else
				this.BalanceRate = rs.getString("BalanceRate").trim();

			if( rs.getString("BalanceTxt") == null )
				this.BalanceTxt = null;
			else
				this.BalanceTxt = rs.getString("BalanceTxt").trim();

			if( rs.getString("BalanceFormula") == null )
				this.BalanceFormula = null;
			else
				this.BalanceFormula = rs.getString("BalanceFormula").trim();

			if( rs.getString("ProjectName") == null )
				this.ProjectName = null;
			else
				this.ProjectName = rs.getString("ProjectName").trim();

			if( rs.getString("AccountCycle") == null )
				this.AccountCycle = null;
			else
				this.AccountCycle = rs.getString("AccountCycle").trim();

			if( rs.getString("CostRate") == null )
				this.CostRate = null;
			else
				this.CostRate = rs.getString("CostRate").trim();

			if( rs.getString("BalanceType") == null )
				this.BalanceType = null;
			else
				this.BalanceType = rs.getString("BalanceType").trim();

			if( rs.getString("BalanceAmnt") == null )
				this.BalanceAmnt = null;
			else
				this.BalanceAmnt = rs.getString("BalanceAmnt").trim();

			if( rs.getString("StopLine") == null )
				this.StopLine = null;
			else
				this.StopLine = rs.getString("StopLine").trim();

			if( rs.getString("SharedLine") == null )
				this.SharedLine = null;
			else
				this.SharedLine = rs.getString("SharedLine").trim();

			if( rs.getString("SharedRate") == null )
				this.SharedRate = null;
			else
				this.SharedRate = rs.getString("SharedRate").trim();

			if( rs.getString("RevertantLine") == null )
				this.RevertantLine = null;
			else
				this.RevertantLine = rs.getString("RevertantLine").trim();

			if( rs.getString("RevertantRate") == null )
				this.RevertantRate = null;
			else
				this.RevertantRate = rs.getString("RevertantRate").trim();

			if( rs.getString("ProjectNo") == null )
				this.ProjectNo = null;
			else
				this.ProjectNo = rs.getString("ProjectNo").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

			if( rs.getString("bak4") == null )
				this.bak4 = null;
			else
				this.bak4 = rs.getString("bak4").trim();

			if( rs.getString("bak5") == null )
				this.bak5 = null;
			else
				this.bak5 = rs.getString("bak5").trim();

			if( rs.getString("Rebalance") == null )
				this.Rebalance = null;
			else
				this.Rebalance = rs.getString("Rebalance").trim();

			if( rs.getString("Recharge") == null )
				this.Recharge = null;
			else
				this.Recharge = rs.getString("Recharge").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCGrpContSub表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCGrpContSubSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCGrpContSubSchema getSchema()
	{
		LCGrpContSubSchema aLCGrpContSubSchema = new LCGrpContSubSchema();
		aLCGrpContSubSchema.setSchema(this);
		return aLCGrpContSubSchema;
	}

	public LCGrpContSubDB getDB()
	{
		LCGrpContSubDB aDBOper = new LCGrpContSubDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpContSub描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(County)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerBankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AuthorizeCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BalanceTermFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BalanceLine)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BalanceRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BalanceTxt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BalanceFormula)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccountCycle)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BalanceType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BalanceAmnt)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StopLine)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SharedLine)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SharedRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RevertantLine)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RevertantRate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Rebalance)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Recharge));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCGrpContSub>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			County = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CustomerBankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AuthorizeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			BalanceTermFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			BalanceLine = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			BalanceRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			BalanceTxt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			BalanceFormula = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ProjectName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			AccountCycle = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			CostRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			BalanceType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			BalanceAmnt = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			StopLine = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			SharedLine = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			SharedRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			RevertantLine = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			RevertantRate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ProjectNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			bak5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			Rebalance = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			Recharge = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCGrpContSubSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("County"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(County));
		}
		if (FCode.equals("CustomerBankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerBankCode));
		}
		if (FCode.equals("AuthorizeCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AuthorizeCode));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BalanceTermFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceTermFlag));
		}
		if (FCode.equals("BalanceLine"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceLine));
		}
		if (FCode.equals("BalanceRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceRate));
		}
		if (FCode.equals("BalanceTxt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceTxt));
		}
		if (FCode.equals("BalanceFormula"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceFormula));
		}
		if (FCode.equals("ProjectName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectName));
		}
		if (FCode.equals("AccountCycle"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountCycle));
		}
		if (FCode.equals("CostRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostRate));
		}
		if (FCode.equals("BalanceType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceType));
		}
		if (FCode.equals("BalanceAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BalanceAmnt));
		}
		if (FCode.equals("StopLine"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StopLine));
		}
		if (FCode.equals("SharedLine"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SharedLine));
		}
		if (FCode.equals("SharedRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SharedRate));
		}
		if (FCode.equals("RevertantLine"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RevertantLine));
		}
		if (FCode.equals("RevertantRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RevertantRate));
		}
		if (FCode.equals("ProjectNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectNo));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (FCode.equals("bak4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak4));
		}
		if (FCode.equals("bak5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak5));
		}
		if (FCode.equals("Rebalance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rebalance));
		}
		if (FCode.equals("Recharge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Recharge));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(County);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CustomerBankCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AuthorizeCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(BalanceTermFlag);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(BalanceLine);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(BalanceRate);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(BalanceTxt);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(BalanceFormula);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ProjectName);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(AccountCycle);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(CostRate);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(BalanceType);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(BalanceAmnt);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(StopLine);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(SharedLine);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(SharedRate);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(RevertantLine);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(RevertantRate);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ProjectNo);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(bak4);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(bak5);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(Rebalance);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(Recharge);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("County"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				County = FValue.trim();
			}
			else
				County = null;
		}
		if (FCode.equalsIgnoreCase("CustomerBankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerBankCode = FValue.trim();
			}
			else
				CustomerBankCode = null;
		}
		if (FCode.equalsIgnoreCase("AuthorizeCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AuthorizeCode = FValue.trim();
			}
			else
				AuthorizeCode = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BalanceTermFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BalanceTermFlag = FValue.trim();
			}
			else
				BalanceTermFlag = null;
		}
		if (FCode.equalsIgnoreCase("BalanceLine"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BalanceLine = FValue.trim();
			}
			else
				BalanceLine = null;
		}
		if (FCode.equalsIgnoreCase("BalanceRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BalanceRate = FValue.trim();
			}
			else
				BalanceRate = null;
		}
		if (FCode.equalsIgnoreCase("BalanceTxt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BalanceTxt = FValue.trim();
			}
			else
				BalanceTxt = null;
		}
		if (FCode.equalsIgnoreCase("BalanceFormula"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BalanceFormula = FValue.trim();
			}
			else
				BalanceFormula = null;
		}
		if (FCode.equalsIgnoreCase("ProjectName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectName = FValue.trim();
			}
			else
				ProjectName = null;
		}
		if (FCode.equalsIgnoreCase("AccountCycle"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccountCycle = FValue.trim();
			}
			else
				AccountCycle = null;
		}
		if (FCode.equalsIgnoreCase("CostRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostRate = FValue.trim();
			}
			else
				CostRate = null;
		}
		if (FCode.equalsIgnoreCase("BalanceType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BalanceType = FValue.trim();
			}
			else
				BalanceType = null;
		}
		if (FCode.equalsIgnoreCase("BalanceAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BalanceAmnt = FValue.trim();
			}
			else
				BalanceAmnt = null;
		}
		if (FCode.equalsIgnoreCase("StopLine"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StopLine = FValue.trim();
			}
			else
				StopLine = null;
		}
		if (FCode.equalsIgnoreCase("SharedLine"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SharedLine = FValue.trim();
			}
			else
				SharedLine = null;
		}
		if (FCode.equalsIgnoreCase("SharedRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SharedRate = FValue.trim();
			}
			else
				SharedRate = null;
		}
		if (FCode.equalsIgnoreCase("RevertantLine"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RevertantLine = FValue.trim();
			}
			else
				RevertantLine = null;
		}
		if (FCode.equalsIgnoreCase("RevertantRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RevertantRate = FValue.trim();
			}
			else
				RevertantRate = null;
		}
		if (FCode.equalsIgnoreCase("ProjectNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectNo = FValue.trim();
			}
			else
				ProjectNo = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		if (FCode.equalsIgnoreCase("bak4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak4 = FValue.trim();
			}
			else
				bak4 = null;
		}
		if (FCode.equalsIgnoreCase("bak5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak5 = FValue.trim();
			}
			else
				bak5 = null;
		}
		if (FCode.equalsIgnoreCase("Rebalance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Rebalance = FValue.trim();
			}
			else
				Rebalance = null;
		}
		if (FCode.equalsIgnoreCase("Recharge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Recharge = FValue.trim();
			}
			else
				Recharge = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCGrpContSubSchema other = (LCGrpContSubSchema)otherObject;
		return
			(PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (County == null ? other.getCounty() == null : County.equals(other.getCounty()))
			&& (CustomerBankCode == null ? other.getCustomerBankCode() == null : CustomerBankCode.equals(other.getCustomerBankCode()))
			&& (AuthorizeCode == null ? other.getAuthorizeCode() == null : AuthorizeCode.equals(other.getAuthorizeCode()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BalanceTermFlag == null ? other.getBalanceTermFlag() == null : BalanceTermFlag.equals(other.getBalanceTermFlag()))
			&& (BalanceLine == null ? other.getBalanceLine() == null : BalanceLine.equals(other.getBalanceLine()))
			&& (BalanceRate == null ? other.getBalanceRate() == null : BalanceRate.equals(other.getBalanceRate()))
			&& (BalanceTxt == null ? other.getBalanceTxt() == null : BalanceTxt.equals(other.getBalanceTxt()))
			&& (BalanceFormula == null ? other.getBalanceFormula() == null : BalanceFormula.equals(other.getBalanceFormula()))
			&& (ProjectName == null ? other.getProjectName() == null : ProjectName.equals(other.getProjectName()))
			&& (AccountCycle == null ? other.getAccountCycle() == null : AccountCycle.equals(other.getAccountCycle()))
			&& (CostRate == null ? other.getCostRate() == null : CostRate.equals(other.getCostRate()))
			&& (BalanceType == null ? other.getBalanceType() == null : BalanceType.equals(other.getBalanceType()))
			&& (BalanceAmnt == null ? other.getBalanceAmnt() == null : BalanceAmnt.equals(other.getBalanceAmnt()))
			&& (StopLine == null ? other.getStopLine() == null : StopLine.equals(other.getStopLine()))
			&& (SharedLine == null ? other.getSharedLine() == null : SharedLine.equals(other.getSharedLine()))
			&& (SharedRate == null ? other.getSharedRate() == null : SharedRate.equals(other.getSharedRate()))
			&& (RevertantLine == null ? other.getRevertantLine() == null : RevertantLine.equals(other.getRevertantLine()))
			&& (RevertantRate == null ? other.getRevertantRate() == null : RevertantRate.equals(other.getRevertantRate()))
			&& (ProjectNo == null ? other.getProjectNo() == null : ProjectNo.equals(other.getProjectNo()))
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()))
			&& (bak4 == null ? other.getbak4() == null : bak4.equals(other.getbak4()))
			&& (bak5 == null ? other.getbak5() == null : bak5.equals(other.getbak5()))
			&& (Rebalance == null ? other.getRebalance() == null : Rebalance.equals(other.getRebalance()))
			&& (Recharge == null ? other.getRecharge() == null : Recharge.equals(other.getRecharge()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PrtNo") ) {
			return 0;
		}
		if( strFieldName.equals("County") ) {
			return 1;
		}
		if( strFieldName.equals("CustomerBankCode") ) {
			return 2;
		}
		if( strFieldName.equals("AuthorizeCode") ) {
			return 3;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 4;
		}
		if( strFieldName.equals("Operator") ) {
			return 5;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 6;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 7;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 8;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 9;
		}
		if( strFieldName.equals("BalanceTermFlag") ) {
			return 10;
		}
		if( strFieldName.equals("BalanceLine") ) {
			return 11;
		}
		if( strFieldName.equals("BalanceRate") ) {
			return 12;
		}
		if( strFieldName.equals("BalanceTxt") ) {
			return 13;
		}
		if( strFieldName.equals("BalanceFormula") ) {
			return 14;
		}
		if( strFieldName.equals("ProjectName") ) {
			return 15;
		}
		if( strFieldName.equals("AccountCycle") ) {
			return 16;
		}
		if( strFieldName.equals("CostRate") ) {
			return 17;
		}
		if( strFieldName.equals("BalanceType") ) {
			return 18;
		}
		if( strFieldName.equals("BalanceAmnt") ) {
			return 19;
		}
		if( strFieldName.equals("StopLine") ) {
			return 20;
		}
		if( strFieldName.equals("SharedLine") ) {
			return 21;
		}
		if( strFieldName.equals("SharedRate") ) {
			return 22;
		}
		if( strFieldName.equals("RevertantLine") ) {
			return 23;
		}
		if( strFieldName.equals("RevertantRate") ) {
			return 24;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return 25;
		}
		if( strFieldName.equals("bak1") ) {
			return 26;
		}
		if( strFieldName.equals("bak2") ) {
			return 27;
		}
		if( strFieldName.equals("bak3") ) {
			return 28;
		}
		if( strFieldName.equals("bak4") ) {
			return 29;
		}
		if( strFieldName.equals("bak5") ) {
			return 30;
		}
		if( strFieldName.equals("Rebalance") ) {
			return 31;
		}
		if( strFieldName.equals("Recharge") ) {
			return 32;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PrtNo";
				break;
			case 1:
				strFieldName = "County";
				break;
			case 2:
				strFieldName = "CustomerBankCode";
				break;
			case 3:
				strFieldName = "AuthorizeCode";
				break;
			case 4:
				strFieldName = "ManageCom";
				break;
			case 5:
				strFieldName = "Operator";
				break;
			case 6:
				strFieldName = "MakeDate";
				break;
			case 7:
				strFieldName = "MakeTime";
				break;
			case 8:
				strFieldName = "ModifyDate";
				break;
			case 9:
				strFieldName = "ModifyTime";
				break;
			case 10:
				strFieldName = "BalanceTermFlag";
				break;
			case 11:
				strFieldName = "BalanceLine";
				break;
			case 12:
				strFieldName = "BalanceRate";
				break;
			case 13:
				strFieldName = "BalanceTxt";
				break;
			case 14:
				strFieldName = "BalanceFormula";
				break;
			case 15:
				strFieldName = "ProjectName";
				break;
			case 16:
				strFieldName = "AccountCycle";
				break;
			case 17:
				strFieldName = "CostRate";
				break;
			case 18:
				strFieldName = "BalanceType";
				break;
			case 19:
				strFieldName = "BalanceAmnt";
				break;
			case 20:
				strFieldName = "StopLine";
				break;
			case 21:
				strFieldName = "SharedLine";
				break;
			case 22:
				strFieldName = "SharedRate";
				break;
			case 23:
				strFieldName = "RevertantLine";
				break;
			case 24:
				strFieldName = "RevertantRate";
				break;
			case 25:
				strFieldName = "ProjectNo";
				break;
			case 26:
				strFieldName = "bak1";
				break;
			case 27:
				strFieldName = "bak2";
				break;
			case 28:
				strFieldName = "bak3";
				break;
			case 29:
				strFieldName = "bak4";
				break;
			case 30:
				strFieldName = "bak5";
				break;
			case 31:
				strFieldName = "Rebalance";
				break;
			case 32:
				strFieldName = "Recharge";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("County") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerBankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AuthorizeCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalanceTermFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalanceLine") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalanceRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalanceTxt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalanceFormula") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccountCycle") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalanceType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BalanceAmnt") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StopLine") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SharedLine") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SharedRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RevertantLine") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RevertantRate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Rebalance") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Recharge") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
