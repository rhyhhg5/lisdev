/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLClaimUWMainDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LLClaimUWMainSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-02-28
 */
public class LLClaimUWMainSchema implements Schema
{
    // @Field
    /** 赔案号 */
    private String ClmNo;
    /** 立案号 */
    private String RgtNo;
    /** 分案号 */
    private String CaseNo;
    /** 核赔员 */
    private String ClmUWer;
    /** 核赔级别 */
    private String ClmUWGrade;
    /** 核赔结论 */
    private String ClmDecision;
    /** 申请级别 */
    private String AppGrade;
    /** 审核类型 */
    private String CheckType;
    /** 申请审核人员 */
    private String AppClmUWer;
    /** 申请阶段 */
    private String AppPhase;
    /** 申请动作 */
    private String AppActionType;
    /** 审批意见 */
    private String Remark1;
    /** 审定意见 */
    private String Remark2;
    /** 审批结论 */
    private String checkDecision1;
    /** 审定结论 */
    private String checkDecision2;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 管理机构 */
    private String MngCom;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 22; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLClaimUWMainSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "ClmNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getClmNo()
    {
        if (ClmNo != null && !ClmNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmNo = StrTool.unicodeToGBK(ClmNo);
        }
        return ClmNo;
    }

    public void setClmNo(String aClmNo)
    {
        ClmNo = aClmNo;
    }

    public String getRgtNo()
    {
        if (RgtNo != null && !RgtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            RgtNo = StrTool.unicodeToGBK(RgtNo);
        }
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo)
    {
        RgtNo = aRgtNo;
    }

    public String getCaseNo()
    {
        if (CaseNo != null && !CaseNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            CaseNo = StrTool.unicodeToGBK(CaseNo);
        }
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo)
    {
        CaseNo = aCaseNo;
    }

    public String getClmUWer()
    {
        if (ClmUWer != null && !ClmUWer.equals("") && SysConst.CHANGECHARSET == true)
        {
            ClmUWer = StrTool.unicodeToGBK(ClmUWer);
        }
        return ClmUWer;
    }

    public void setClmUWer(String aClmUWer)
    {
        ClmUWer = aClmUWer;
    }

    public String getClmUWGrade()
    {
        if (ClmUWGrade != null && !ClmUWGrade.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClmUWGrade = StrTool.unicodeToGBK(ClmUWGrade);
        }
        return ClmUWGrade;
    }

    public void setClmUWGrade(String aClmUWGrade)
    {
        ClmUWGrade = aClmUWGrade;
    }

    public String getClmDecision()
    {
        if (ClmDecision != null && !ClmDecision.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClmDecision = StrTool.unicodeToGBK(ClmDecision);
        }
        return ClmDecision;
    }

    public void setClmDecision(String aClmDecision)
    {
        ClmDecision = aClmDecision;
    }

    public String getAppGrade()
    {
        if (AppGrade != null && !AppGrade.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppGrade = StrTool.unicodeToGBK(AppGrade);
        }
        return AppGrade;
    }

    public void setAppGrade(String aAppGrade)
    {
        AppGrade = aAppGrade;
    }

    public String getCheckType()
    {
        if (CheckType != null && !CheckType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CheckType = StrTool.unicodeToGBK(CheckType);
        }
        return CheckType;
    }

    public void setCheckType(String aCheckType)
    {
        CheckType = aCheckType;
    }

    public String getAppClmUWer()
    {
        if (AppClmUWer != null && !AppClmUWer.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppClmUWer = StrTool.unicodeToGBK(AppClmUWer);
        }
        return AppClmUWer;
    }

    public void setAppClmUWer(String aAppClmUWer)
    {
        AppClmUWer = aAppClmUWer;
    }

    public String getAppPhase()
    {
        if (AppPhase != null && !AppPhase.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppPhase = StrTool.unicodeToGBK(AppPhase);
        }
        return AppPhase;
    }

    public void setAppPhase(String aAppPhase)
    {
        AppPhase = aAppPhase;
    }

    public String getAppActionType()
    {
        if (AppActionType != null && !AppActionType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AppActionType = StrTool.unicodeToGBK(AppActionType);
        }
        return AppActionType;
    }

    public void setAppActionType(String aAppActionType)
    {
        AppActionType = aAppActionType;
    }

    public String getRemark1()
    {
        if (Remark1 != null && !Remark1.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark1 = StrTool.unicodeToGBK(Remark1);
        }
        return Remark1;
    }

    public void setRemark1(String aRemark1)
    {
        Remark1 = aRemark1;
    }

    public String getRemark2()
    {
        if (Remark2 != null && !Remark2.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark2 = StrTool.unicodeToGBK(Remark2);
        }
        return Remark2;
    }

    public void setRemark2(String aRemark2)
    {
        Remark2 = aRemark2;
    }

    public String getcheckDecision1()
    {
        if (checkDecision1 != null && !checkDecision1.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            checkDecision1 = StrTool.unicodeToGBK(checkDecision1);
        }
        return checkDecision1;
    }

    public void setcheckDecision1(String acheckDecision1)
    {
        checkDecision1 = acheckDecision1;
    }

    public String getcheckDecision2()
    {
        if (checkDecision2 != null && !checkDecision2.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            checkDecision2 = StrTool.unicodeToGBK(checkDecision2);
        }
        return checkDecision2;
    }

    public void setcheckDecision2(String acheckDecision2)
    {
        checkDecision2 = acheckDecision2;
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMngCom()
    {
        if (MngCom != null && !MngCom.equals("") && SysConst.CHANGECHARSET == true)
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLClaimUWMainSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LLClaimUWMainSchema aLLClaimUWMainSchema)
    {
        this.ClmNo = aLLClaimUWMainSchema.getClmNo();
        this.RgtNo = aLLClaimUWMainSchema.getRgtNo();
        this.CaseNo = aLLClaimUWMainSchema.getCaseNo();
        this.ClmUWer = aLLClaimUWMainSchema.getClmUWer();
        this.ClmUWGrade = aLLClaimUWMainSchema.getClmUWGrade();
        this.ClmDecision = aLLClaimUWMainSchema.getClmDecision();
        this.AppGrade = aLLClaimUWMainSchema.getAppGrade();
        this.CheckType = aLLClaimUWMainSchema.getCheckType();
        this.AppClmUWer = aLLClaimUWMainSchema.getAppClmUWer();
        this.AppPhase = aLLClaimUWMainSchema.getAppPhase();
        this.AppActionType = aLLClaimUWMainSchema.getAppActionType();
        this.Remark1 = aLLClaimUWMainSchema.getRemark1();
        this.Remark2 = aLLClaimUWMainSchema.getRemark2();
        this.checkDecision1 = aLLClaimUWMainSchema.getcheckDecision1();
        this.checkDecision2 = aLLClaimUWMainSchema.getcheckDecision2();
        this.Remark = aLLClaimUWMainSchema.getRemark();
        this.Operator = aLLClaimUWMainSchema.getOperator();
        this.MngCom = aLLClaimUWMainSchema.getMngCom();
        this.MakeDate = fDate.getDate(aLLClaimUWMainSchema.getMakeDate());
        this.MakeTime = aLLClaimUWMainSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLClaimUWMainSchema.getModifyDate());
        this.ModifyTime = aLLClaimUWMainSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ClmNo") == null)
            {
                this.ClmNo = null;
            }
            else
            {
                this.ClmNo = rs.getString("ClmNo").trim();
            }

            if (rs.getString("RgtNo") == null)
            {
                this.RgtNo = null;
            }
            else
            {
                this.RgtNo = rs.getString("RgtNo").trim();
            }

            if (rs.getString("CaseNo") == null)
            {
                this.CaseNo = null;
            }
            else
            {
                this.CaseNo = rs.getString("CaseNo").trim();
            }

            if (rs.getString("ClmUWer") == null)
            {
                this.ClmUWer = null;
            }
            else
            {
                this.ClmUWer = rs.getString("ClmUWer").trim();
            }

            if (rs.getString("ClmUWGrade") == null)
            {
                this.ClmUWGrade = null;
            }
            else
            {
                this.ClmUWGrade = rs.getString("ClmUWGrade").trim();
            }

            if (rs.getString("ClmDecision") == null)
            {
                this.ClmDecision = null;
            }
            else
            {
                this.ClmDecision = rs.getString("ClmDecision").trim();
            }

            if (rs.getString("AppGrade") == null)
            {
                this.AppGrade = null;
            }
            else
            {
                this.AppGrade = rs.getString("AppGrade").trim();
            }

            if (rs.getString("CheckType") == null)
            {
                this.CheckType = null;
            }
            else
            {
                this.CheckType = rs.getString("CheckType").trim();
            }

            if (rs.getString("AppClmUWer") == null)
            {
                this.AppClmUWer = null;
            }
            else
            {
                this.AppClmUWer = rs.getString("AppClmUWer").trim();
            }

            if (rs.getString("AppPhase") == null)
            {
                this.AppPhase = null;
            }
            else
            {
                this.AppPhase = rs.getString("AppPhase").trim();
            }

            if (rs.getString("AppActionType") == null)
            {
                this.AppActionType = null;
            }
            else
            {
                this.AppActionType = rs.getString("AppActionType").trim();
            }

            if (rs.getString("Remark1") == null)
            {
                this.Remark1 = null;
            }
            else
            {
                this.Remark1 = rs.getString("Remark1").trim();
            }

            if (rs.getString("Remark2") == null)
            {
                this.Remark2 = null;
            }
            else
            {
                this.Remark2 = rs.getString("Remark2").trim();
            }

            if (rs.getString("checkDecision1") == null)
            {
                this.checkDecision1 = null;
            }
            else
            {
                this.checkDecision1 = rs.getString("checkDecision1").trim();
            }

            if (rs.getString("checkDecision2") == null)
            {
                this.checkDecision2 = null;
            }
            else
            {
                this.checkDecision2 = rs.getString("checkDecision2").trim();
            }

            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("MngCom") == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString("MngCom").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimUWMainSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLClaimUWMainSchema getSchema()
    {
        LLClaimUWMainSchema aLLClaimUWMainSchema = new LLClaimUWMainSchema();
        aLLClaimUWMainSchema.setSchema(this);
        return aLLClaimUWMainSchema;
    }

    public LLClaimUWMainDB getDB()
    {
        LLClaimUWMainDB aDBOper = new LLClaimUWMainDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimUWMain描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ClmNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RgtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CaseNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmUWer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmUWGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClmDecision)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CheckType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppClmUWer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppPhase)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppActionType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(checkDecision1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(checkDecision2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MngCom)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLClaimUWMain>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            ClmUWer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            ClmUWGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            ClmDecision = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                         SysConst.PACKAGESPILTER);
            AppGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            CheckType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            AppClmUWer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            AppPhase = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            AppActionType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                           SysConst.PACKAGESPILTER);
            Remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                     SysConst.PACKAGESPILTER);
            Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            checkDecision1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            14, SysConst.PACKAGESPILTER);
            checkDecision2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            15, SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                    SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 21, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLClaimUWMainSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ClmNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmNo));
        }
        if (FCode.equals("RgtNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RgtNo));
        }
        if (FCode.equals("CaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CaseNo));
        }
        if (FCode.equals("ClmUWer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmUWer));
        }
        if (FCode.equals("ClmUWGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmUWGrade));
        }
        if (FCode.equals("ClmDecision"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClmDecision));
        }
        if (FCode.equals("AppGrade"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppGrade));
        }
        if (FCode.equals("CheckType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CheckType));
        }
        if (FCode.equals("AppClmUWer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppClmUWer));
        }
        if (FCode.equals("AppPhase"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppPhase));
        }
        if (FCode.equals("AppActionType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppActionType));
        }
        if (FCode.equals("Remark1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark1));
        }
        if (FCode.equals("Remark2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark2));
        }
        if (FCode.equals("checkDecision1"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(checkDecision1));
        }
        if (FCode.equals("checkDecision2"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(checkDecision2));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MngCom));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ClmNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RgtNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ClmUWer);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ClmUWGrade);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ClmDecision);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AppGrade);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(CheckType);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(AppClmUWer);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(AppPhase);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(AppActionType);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(Remark1);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(Remark2);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(checkDecision1);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(checkDecision2);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ClmNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmNo = FValue.trim();
            }
            else
            {
                ClmNo = null;
            }
        }
        if (FCode.equals("RgtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtNo = FValue.trim();
            }
            else
            {
                RgtNo = null;
            }
        }
        if (FCode.equals("CaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
            {
                CaseNo = null;
            }
        }
        if (FCode.equals("ClmUWer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmUWer = FValue.trim();
            }
            else
            {
                ClmUWer = null;
            }
        }
        if (FCode.equals("ClmUWGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmUWGrade = FValue.trim();
            }
            else
            {
                ClmUWGrade = null;
            }
        }
        if (FCode.equals("ClmDecision"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmDecision = FValue.trim();
            }
            else
            {
                ClmDecision = null;
            }
        }
        if (FCode.equals("AppGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppGrade = FValue.trim();
            }
            else
            {
                AppGrade = null;
            }
        }
        if (FCode.equals("CheckType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CheckType = FValue.trim();
            }
            else
            {
                CheckType = null;
            }
        }
        if (FCode.equals("AppClmUWer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppClmUWer = FValue.trim();
            }
            else
            {
                AppClmUWer = null;
            }
        }
        if (FCode.equals("AppPhase"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppPhase = FValue.trim();
            }
            else
            {
                AppPhase = null;
            }
        }
        if (FCode.equals("AppActionType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppActionType = FValue.trim();
            }
            else
            {
                AppActionType = null;
            }
        }
        if (FCode.equals("Remark1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark1 = FValue.trim();
            }
            else
            {
                Remark1 = null;
            }
        }
        if (FCode.equals("Remark2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark2 = FValue.trim();
            }
            else
            {
                Remark2 = null;
            }
        }
        if (FCode.equals("checkDecision1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                checkDecision1 = FValue.trim();
            }
            else
            {
                checkDecision1 = null;
            }
        }
        if (FCode.equals("checkDecision2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                checkDecision2 = FValue.trim();
            }
            else
            {
                checkDecision2 = null;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLClaimUWMainSchema other = (LLClaimUWMainSchema) otherObject;
        return
                ClmNo.equals(other.getClmNo())
                && RgtNo.equals(other.getRgtNo())
                && CaseNo.equals(other.getCaseNo())
                && ClmUWer.equals(other.getClmUWer())
                && ClmUWGrade.equals(other.getClmUWGrade())
                && ClmDecision.equals(other.getClmDecision())
                && AppGrade.equals(other.getAppGrade())
                && CheckType.equals(other.getCheckType())
                && AppClmUWer.equals(other.getAppClmUWer())
                && AppPhase.equals(other.getAppPhase())
                && AppActionType.equals(other.getAppActionType())
                && Remark1.equals(other.getRemark1())
                && Remark2.equals(other.getRemark2())
                && checkDecision1.equals(other.getcheckDecision1())
                && checkDecision2.equals(other.getcheckDecision2())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && MngCom.equals(other.getMngCom())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ClmNo"))
        {
            return 0;
        }
        if (strFieldName.equals("RgtNo"))
        {
            return 1;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return 2;
        }
        if (strFieldName.equals("ClmUWer"))
        {
            return 3;
        }
        if (strFieldName.equals("ClmUWGrade"))
        {
            return 4;
        }
        if (strFieldName.equals("ClmDecision"))
        {
            return 5;
        }
        if (strFieldName.equals("AppGrade"))
        {
            return 6;
        }
        if (strFieldName.equals("CheckType"))
        {
            return 7;
        }
        if (strFieldName.equals("AppClmUWer"))
        {
            return 8;
        }
        if (strFieldName.equals("AppPhase"))
        {
            return 9;
        }
        if (strFieldName.equals("AppActionType"))
        {
            return 10;
        }
        if (strFieldName.equals("Remark1"))
        {
            return 11;
        }
        if (strFieldName.equals("Remark2"))
        {
            return 12;
        }
        if (strFieldName.equals("checkDecision1"))
        {
            return 13;
        }
        if (strFieldName.equals("checkDecision2"))
        {
            return 14;
        }
        if (strFieldName.equals("Remark"))
        {
            return 15;
        }
        if (strFieldName.equals("Operator"))
        {
            return 16;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 17;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 19;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 21;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ClmNo";
                break;
            case 1:
                strFieldName = "RgtNo";
                break;
            case 2:
                strFieldName = "CaseNo";
                break;
            case 3:
                strFieldName = "ClmUWer";
                break;
            case 4:
                strFieldName = "ClmUWGrade";
                break;
            case 5:
                strFieldName = "ClmDecision";
                break;
            case 6:
                strFieldName = "AppGrade";
                break;
            case 7:
                strFieldName = "CheckType";
                break;
            case 8:
                strFieldName = "AppClmUWer";
                break;
            case 9:
                strFieldName = "AppPhase";
                break;
            case 10:
                strFieldName = "AppActionType";
                break;
            case 11:
                strFieldName = "Remark1";
                break;
            case 12:
                strFieldName = "Remark2";
                break;
            case 13:
                strFieldName = "checkDecision1";
                break;
            case 14:
                strFieldName = "checkDecision2";
                break;
            case 15:
                strFieldName = "Remark";
                break;
            case 16:
                strFieldName = "Operator";
                break;
            case 17:
                strFieldName = "MngCom";
                break;
            case 18:
                strFieldName = "MakeDate";
                break;
            case 19:
                strFieldName = "MakeTime";
                break;
            case 20:
                strFieldName = "ModifyDate";
                break;
            case 21:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ClmNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RgtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmUWer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmUWGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmDecision"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CheckType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppClmUWer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppPhase"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppActionType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("checkDecision1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("checkDecision2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
