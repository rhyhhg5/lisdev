/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LACertificationBDB;

/*
 * <p>ClassName: LACertificationBSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2013-03-26
 */
public class LACertificationBSchema implements Schema, Cloneable
{
	// @Field
	/** 转储号码 */
	private String EdorNo;
	/** 业务员编码 */
	private String AgentCode;
	/** 展业证书号 */
	private String CertifNo;
	/** 流水号 */
	private int Idx;
	/** 批准单位 */
	private String AuthorUnit;
	/** 发放日期 */
	private Date GrantDate;
	/** 失效日期 */
	private Date InvalidDate;
	/** 失效原因 */
	private String InvalidRsn;
	/** 补发日期 */
	private Date reissueDate;
	/** 补发原因 */
	private String reissueRsn;
	/** 有效日期 */
	private int ValidPeriod;
	/** 展业证书状态 */
	private String State;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 上一次修改日期 */
	private Date ModifyDate;
	/** 上一次修改时间 */
	private String ModifyTime;
	/** 有效起期 */
	private Date ValidStart;
	/** 有效止期 */
	private Date ValidEnd;
	/** 旧展业证号码 */
	private String OldCeritifNo;
	/** 操作员代码 */
	private String Operator;
	/** 原入机日期 */
	private Date OldMakeDate;
	/** 原入机时间 */
	private String OldMakeTime;
	/** 原上一次修改日期 */
	private Date OldModifyDate;
	/** 原上一次修改时间 */
	private String OldModifyTime;
	/** 原操作员代码 */
	private String OldOperator;

	public static final int FIELDNUM = 25;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LACertificationBSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "EdorNo";
		pk[1] = "AgentCode";
		pk[2] = "CertifNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LACertificationBSchema cloned = (LACertificationBSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getCertifNo()
	{
		return CertifNo;
	}
	public void setCertifNo(String aCertifNo)
	{
		CertifNo = aCertifNo;
	}
	public int getIdx()
	{
		return Idx;
	}
	public void setIdx(int aIdx)
	{
		Idx = aIdx;
	}
	public void setIdx(String aIdx)
	{
		if (aIdx != null && !aIdx.equals(""))
		{
			Integer tInteger = new Integer(aIdx);
			int i = tInteger.intValue();
			Idx = i;
		}
	}

	public String getAuthorUnit()
	{
		return AuthorUnit;
	}
	public void setAuthorUnit(String aAuthorUnit)
	{
		AuthorUnit = aAuthorUnit;
	}
	public String getGrantDate()
	{
		if( GrantDate != null )
			return fDate.getString(GrantDate);
		else
			return null;
	}
	public void setGrantDate(Date aGrantDate)
	{
		GrantDate = aGrantDate;
	}
	public void setGrantDate(String aGrantDate)
	{
		if (aGrantDate != null && !aGrantDate.equals("") )
		{
			GrantDate = fDate.getDate( aGrantDate );
		}
		else
			GrantDate = null;
	}

	public String getInvalidDate()
	{
		if( InvalidDate != null )
			return fDate.getString(InvalidDate);
		else
			return null;
	}
	public void setInvalidDate(Date aInvalidDate)
	{
		InvalidDate = aInvalidDate;
	}
	public void setInvalidDate(String aInvalidDate)
	{
		if (aInvalidDate != null && !aInvalidDate.equals("") )
		{
			InvalidDate = fDate.getDate( aInvalidDate );
		}
		else
			InvalidDate = null;
	}

	public String getInvalidRsn()
	{
		return InvalidRsn;
	}
	public void setInvalidRsn(String aInvalidRsn)
	{
		InvalidRsn = aInvalidRsn;
	}
	public String getreissueDate()
	{
		if( reissueDate != null )
			return fDate.getString(reissueDate);
		else
			return null;
	}
	public void setreissueDate(Date areissueDate)
	{
		reissueDate = areissueDate;
	}
	public void setreissueDate(String areissueDate)
	{
		if (areissueDate != null && !areissueDate.equals("") )
		{
			reissueDate = fDate.getDate( areissueDate );
		}
		else
			reissueDate = null;
	}

	public String getreissueRsn()
	{
		return reissueRsn;
	}
	public void setreissueRsn(String areissueRsn)
	{
		reissueRsn = areissueRsn;
	}
	public int getValidPeriod()
	{
		return ValidPeriod;
	}
	public void setValidPeriod(int aValidPeriod)
	{
		ValidPeriod = aValidPeriod;
	}
	public void setValidPeriod(String aValidPeriod)
	{
		if (aValidPeriod != null && !aValidPeriod.equals(""))
		{
			Integer tInteger = new Integer(aValidPeriod);
			int i = tInteger.intValue();
			ValidPeriod = i;
		}
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getValidStart()
	{
		if( ValidStart != null )
			return fDate.getString(ValidStart);
		else
			return null;
	}
	public void setValidStart(Date aValidStart)
	{
		ValidStart = aValidStart;
	}
	public void setValidStart(String aValidStart)
	{
		if (aValidStart != null && !aValidStart.equals("") )
		{
			ValidStart = fDate.getDate( aValidStart );
		}
		else
			ValidStart = null;
	}

	public String getValidEnd()
	{
		if( ValidEnd != null )
			return fDate.getString(ValidEnd);
		else
			return null;
	}
	public void setValidEnd(Date aValidEnd)
	{
		ValidEnd = aValidEnd;
	}
	public void setValidEnd(String aValidEnd)
	{
		if (aValidEnd != null && !aValidEnd.equals("") )
		{
			ValidEnd = fDate.getDate( aValidEnd );
		}
		else
			ValidEnd = null;
	}

	public String getOldCeritifNo()
	{
		return OldCeritifNo;
	}
	public void setOldCeritifNo(String aOldCeritifNo)
	{
		OldCeritifNo = aOldCeritifNo;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getOldMakeDate()
	{
		if( OldMakeDate != null )
			return fDate.getString(OldMakeDate);
		else
			return null;
	}
	public void setOldMakeDate(Date aOldMakeDate)
	{
		OldMakeDate = aOldMakeDate;
	}
	public void setOldMakeDate(String aOldMakeDate)
	{
		if (aOldMakeDate != null && !aOldMakeDate.equals("") )
		{
			OldMakeDate = fDate.getDate( aOldMakeDate );
		}
		else
			OldMakeDate = null;
	}

	public String getOldMakeTime()
	{
		return OldMakeTime;
	}
	public void setOldMakeTime(String aOldMakeTime)
	{
		OldMakeTime = aOldMakeTime;
	}
	public String getOldModifyDate()
	{
		if( OldModifyDate != null )
			return fDate.getString(OldModifyDate);
		else
			return null;
	}
	public void setOldModifyDate(Date aOldModifyDate)
	{
		OldModifyDate = aOldModifyDate;
	}
	public void setOldModifyDate(String aOldModifyDate)
	{
		if (aOldModifyDate != null && !aOldModifyDate.equals("") )
		{
			OldModifyDate = fDate.getDate( aOldModifyDate );
		}
		else
			OldModifyDate = null;
	}

	public String getOldModifyTime()
	{
		return OldModifyTime;
	}
	public void setOldModifyTime(String aOldModifyTime)
	{
		OldModifyTime = aOldModifyTime;
	}
	public String getOldOperator()
	{
		return OldOperator;
	}
	public void setOldOperator(String aOldOperator)
	{
		OldOperator = aOldOperator;
	}

	/**
	* 使用另外一个 LACertificationBSchema 对象给 Schema 赋值
	* @param: aLACertificationBSchema LACertificationBSchema
	**/
	public void setSchema(LACertificationBSchema aLACertificationBSchema)
	{
		this.EdorNo = aLACertificationBSchema.getEdorNo();
		this.AgentCode = aLACertificationBSchema.getAgentCode();
		this.CertifNo = aLACertificationBSchema.getCertifNo();
		this.Idx = aLACertificationBSchema.getIdx();
		this.AuthorUnit = aLACertificationBSchema.getAuthorUnit();
		this.GrantDate = fDate.getDate( aLACertificationBSchema.getGrantDate());
		this.InvalidDate = fDate.getDate( aLACertificationBSchema.getInvalidDate());
		this.InvalidRsn = aLACertificationBSchema.getInvalidRsn();
		this.reissueDate = fDate.getDate( aLACertificationBSchema.getreissueDate());
		this.reissueRsn = aLACertificationBSchema.getreissueRsn();
		this.ValidPeriod = aLACertificationBSchema.getValidPeriod();
		this.State = aLACertificationBSchema.getState();
		this.MakeDate = fDate.getDate( aLACertificationBSchema.getMakeDate());
		this.MakeTime = aLACertificationBSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLACertificationBSchema.getModifyDate());
		this.ModifyTime = aLACertificationBSchema.getModifyTime();
		this.ValidStart = fDate.getDate( aLACertificationBSchema.getValidStart());
		this.ValidEnd = fDate.getDate( aLACertificationBSchema.getValidEnd());
		this.OldCeritifNo = aLACertificationBSchema.getOldCeritifNo();
		this.Operator = aLACertificationBSchema.getOperator();
		this.OldMakeDate = fDate.getDate( aLACertificationBSchema.getOldMakeDate());
		this.OldMakeTime = aLACertificationBSchema.getOldMakeTime();
		this.OldModifyDate = fDate.getDate( aLACertificationBSchema.getOldModifyDate());
		this.OldModifyTime = aLACertificationBSchema.getOldModifyTime();
		this.OldOperator = aLACertificationBSchema.getOldOperator();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("CertifNo") == null )
				this.CertifNo = null;
			else
				this.CertifNo = rs.getString("CertifNo").trim();

			this.Idx = rs.getInt("Idx");
			if( rs.getString("AuthorUnit") == null )
				this.AuthorUnit = null;
			else
				this.AuthorUnit = rs.getString("AuthorUnit").trim();

			this.GrantDate = rs.getDate("GrantDate");
			this.InvalidDate = rs.getDate("InvalidDate");
			if( rs.getString("InvalidRsn") == null )
				this.InvalidRsn = null;
			else
				this.InvalidRsn = rs.getString("InvalidRsn").trim();

			this.reissueDate = rs.getDate("reissueDate");
			if( rs.getString("reissueRsn") == null )
				this.reissueRsn = null;
			else
				this.reissueRsn = rs.getString("reissueRsn").trim();

			this.ValidPeriod = rs.getInt("ValidPeriod");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.ValidStart = rs.getDate("ValidStart");
			this.ValidEnd = rs.getDate("ValidEnd");
			if( rs.getString("OldCeritifNo") == null )
				this.OldCeritifNo = null;
			else
				this.OldCeritifNo = rs.getString("OldCeritifNo").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.OldMakeDate = rs.getDate("OldMakeDate");
			if( rs.getString("OldMakeTime") == null )
				this.OldMakeTime = null;
			else
				this.OldMakeTime = rs.getString("OldMakeTime").trim();

			this.OldModifyDate = rs.getDate("OldModifyDate");
			if( rs.getString("OldModifyTime") == null )
				this.OldModifyTime = null;
			else
				this.OldModifyTime = rs.getString("OldModifyTime").trim();

			if( rs.getString("OldOperator") == null )
				this.OldOperator = null;
			else
				this.OldOperator = rs.getString("OldOperator").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LACertificationB表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LACertificationBSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LACertificationBSchema getSchema()
	{
		LACertificationBSchema aLACertificationBSchema = new LACertificationBSchema();
		aLACertificationBSchema.setSchema(this);
		return aLACertificationBSchema;
	}

	public LACertificationBDB getDB()
	{
		LACertificationBDB aDBOper = new LACertificationBDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACertificationB描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CertifNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Idx));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AuthorUnit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GrantDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InvalidDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InvalidRsn)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( reissueDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(reissueRsn)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ValidPeriod));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ValidStart ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ValidEnd ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OldCeritifNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( OldMakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OldMakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( OldModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OldModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OldOperator));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLACertificationB>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			CertifNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Idx= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			AuthorUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			GrantDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			InvalidDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			InvalidRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			reissueDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			reissueRsn = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ValidPeriod= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).intValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ValidStart = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
			ValidEnd = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			OldCeritifNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			OldMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			OldMakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			OldModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			OldModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			OldOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LACertificationBSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("CertifNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CertifNo));
		}
		if (FCode.equals("Idx"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
		}
		if (FCode.equals("AuthorUnit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AuthorUnit));
		}
		if (FCode.equals("GrantDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGrantDate()));
		}
		if (FCode.equals("InvalidDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInvalidDate()));
		}
		if (FCode.equals("InvalidRsn"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InvalidRsn));
		}
		if (FCode.equals("reissueDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getreissueDate()));
		}
		if (FCode.equals("reissueRsn"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(reissueRsn));
		}
		if (FCode.equals("ValidPeriod"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ValidPeriod));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ValidStart"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getValidStart()));
		}
		if (FCode.equals("ValidEnd"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getValidEnd()));
		}
		if (FCode.equals("OldCeritifNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OldCeritifNo));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("OldMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOldMakeDate()));
		}
		if (FCode.equals("OldMakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OldMakeTime));
		}
		if (FCode.equals("OldModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOldModifyDate()));
		}
		if (FCode.equals("OldModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OldModifyTime));
		}
		if (FCode.equals("OldOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OldOperator));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(CertifNo);
				break;
			case 3:
				strFieldValue = String.valueOf(Idx);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AuthorUnit);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGrantDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInvalidDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(InvalidRsn);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getreissueDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(reissueRsn);
				break;
			case 10:
				strFieldValue = String.valueOf(ValidPeriod);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getValidStart()));
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getValidEnd()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(OldCeritifNo);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOldMakeDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(OldMakeTime);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOldModifyDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(OldModifyTime);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(OldOperator);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("CertifNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CertifNo = FValue.trim();
			}
			else
				CertifNo = null;
		}
		if (FCode.equalsIgnoreCase("Idx"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Idx = i;
			}
		}
		if (FCode.equalsIgnoreCase("AuthorUnit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AuthorUnit = FValue.trim();
			}
			else
				AuthorUnit = null;
		}
		if (FCode.equalsIgnoreCase("GrantDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GrantDate = fDate.getDate( FValue );
			}
			else
				GrantDate = null;
		}
		if (FCode.equalsIgnoreCase("InvalidDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InvalidDate = fDate.getDate( FValue );
			}
			else
				InvalidDate = null;
		}
		if (FCode.equalsIgnoreCase("InvalidRsn"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InvalidRsn = FValue.trim();
			}
			else
				InvalidRsn = null;
		}
		if (FCode.equalsIgnoreCase("reissueDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				reissueDate = fDate.getDate( FValue );
			}
			else
				reissueDate = null;
		}
		if (FCode.equalsIgnoreCase("reissueRsn"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				reissueRsn = FValue.trim();
			}
			else
				reissueRsn = null;
		}
		if (FCode.equalsIgnoreCase("ValidPeriod"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ValidPeriod = i;
			}
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ValidStart"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ValidStart = fDate.getDate( FValue );
			}
			else
				ValidStart = null;
		}
		if (FCode.equalsIgnoreCase("ValidEnd"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ValidEnd = fDate.getDate( FValue );
			}
			else
				ValidEnd = null;
		}
		if (FCode.equalsIgnoreCase("OldCeritifNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OldCeritifNo = FValue.trim();
			}
			else
				OldCeritifNo = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("OldMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OldMakeDate = fDate.getDate( FValue );
			}
			else
				OldMakeDate = null;
		}
		if (FCode.equalsIgnoreCase("OldMakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OldMakeTime = FValue.trim();
			}
			else
				OldMakeTime = null;
		}
		if (FCode.equalsIgnoreCase("OldModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OldModifyDate = fDate.getDate( FValue );
			}
			else
				OldModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("OldModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OldModifyTime = FValue.trim();
			}
			else
				OldModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("OldOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OldOperator = FValue.trim();
			}
			else
				OldOperator = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LACertificationBSchema other = (LACertificationBSchema)otherObject;
		return
			(EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (CertifNo == null ? other.getCertifNo() == null : CertifNo.equals(other.getCertifNo()))
			&& Idx == other.getIdx()
			&& (AuthorUnit == null ? other.getAuthorUnit() == null : AuthorUnit.equals(other.getAuthorUnit()))
			&& (GrantDate == null ? other.getGrantDate() == null : fDate.getString(GrantDate).equals(other.getGrantDate()))
			&& (InvalidDate == null ? other.getInvalidDate() == null : fDate.getString(InvalidDate).equals(other.getInvalidDate()))
			&& (InvalidRsn == null ? other.getInvalidRsn() == null : InvalidRsn.equals(other.getInvalidRsn()))
			&& (reissueDate == null ? other.getreissueDate() == null : fDate.getString(reissueDate).equals(other.getreissueDate()))
			&& (reissueRsn == null ? other.getreissueRsn() == null : reissueRsn.equals(other.getreissueRsn()))
			&& ValidPeriod == other.getValidPeriod()
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (ValidStart == null ? other.getValidStart() == null : fDate.getString(ValidStart).equals(other.getValidStart()))
			&& (ValidEnd == null ? other.getValidEnd() == null : fDate.getString(ValidEnd).equals(other.getValidEnd()))
			&& (OldCeritifNo == null ? other.getOldCeritifNo() == null : OldCeritifNo.equals(other.getOldCeritifNo()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (OldMakeDate == null ? other.getOldMakeDate() == null : fDate.getString(OldMakeDate).equals(other.getOldMakeDate()))
			&& (OldMakeTime == null ? other.getOldMakeTime() == null : OldMakeTime.equals(other.getOldMakeTime()))
			&& (OldModifyDate == null ? other.getOldModifyDate() == null : fDate.getString(OldModifyDate).equals(other.getOldModifyDate()))
			&& (OldModifyTime == null ? other.getOldModifyTime() == null : OldModifyTime.equals(other.getOldModifyTime()))
			&& (OldOperator == null ? other.getOldOperator() == null : OldOperator.equals(other.getOldOperator()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return 0;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 1;
		}
		if( strFieldName.equals("CertifNo") ) {
			return 2;
		}
		if( strFieldName.equals("Idx") ) {
			return 3;
		}
		if( strFieldName.equals("AuthorUnit") ) {
			return 4;
		}
		if( strFieldName.equals("GrantDate") ) {
			return 5;
		}
		if( strFieldName.equals("InvalidDate") ) {
			return 6;
		}
		if( strFieldName.equals("InvalidRsn") ) {
			return 7;
		}
		if( strFieldName.equals("reissueDate") ) {
			return 8;
		}
		if( strFieldName.equals("reissueRsn") ) {
			return 9;
		}
		if( strFieldName.equals("ValidPeriod") ) {
			return 10;
		}
		if( strFieldName.equals("State") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		if( strFieldName.equals("ValidStart") ) {
			return 16;
		}
		if( strFieldName.equals("ValidEnd") ) {
			return 17;
		}
		if( strFieldName.equals("OldCeritifNo") ) {
			return 18;
		}
		if( strFieldName.equals("Operator") ) {
			return 19;
		}
		if( strFieldName.equals("OldMakeDate") ) {
			return 20;
		}
		if( strFieldName.equals("OldMakeTime") ) {
			return 21;
		}
		if( strFieldName.equals("OldModifyDate") ) {
			return 22;
		}
		if( strFieldName.equals("OldModifyTime") ) {
			return 23;
		}
		if( strFieldName.equals("OldOperator") ) {
			return 24;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "EdorNo";
				break;
			case 1:
				strFieldName = "AgentCode";
				break;
			case 2:
				strFieldName = "CertifNo";
				break;
			case 3:
				strFieldName = "Idx";
				break;
			case 4:
				strFieldName = "AuthorUnit";
				break;
			case 5:
				strFieldName = "GrantDate";
				break;
			case 6:
				strFieldName = "InvalidDate";
				break;
			case 7:
				strFieldName = "InvalidRsn";
				break;
			case 8:
				strFieldName = "reissueDate";
				break;
			case 9:
				strFieldName = "reissueRsn";
				break;
			case 10:
				strFieldName = "ValidPeriod";
				break;
			case 11:
				strFieldName = "State";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			case 16:
				strFieldName = "ValidStart";
				break;
			case 17:
				strFieldName = "ValidEnd";
				break;
			case 18:
				strFieldName = "OldCeritifNo";
				break;
			case 19:
				strFieldName = "Operator";
				break;
			case 20:
				strFieldName = "OldMakeDate";
				break;
			case 21:
				strFieldName = "OldMakeTime";
				break;
			case 22:
				strFieldName = "OldModifyDate";
				break;
			case 23:
				strFieldName = "OldModifyTime";
				break;
			case 24:
				strFieldName = "OldOperator";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CertifNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Idx") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AuthorUnit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrantDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InvalidDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InvalidRsn") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("reissueDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("reissueRsn") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ValidPeriod") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ValidStart") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ValidEnd") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OldCeritifNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OldMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OldMakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OldModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OldModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OldOperator") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_INT;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
