/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LHICDOPSTypeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LHICDOPSTypeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_3
 * @CreateDate：2005-03-03
 */
public class LHICDOPSTypeSchema implements Schema
{
    // @Field
    /** 手术分类等级 */
    private String OPSTypeLevel;
    /** 起始icd号 */
    private String ICDBeginCode;
    /** 终止icd号 */
    private String ICDEndCode;
    /** 手术分类名称 */
    private String OPSType;

    public static final int FIELDNUM = 4; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHICDOPSTypeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "OPSTypeLevel";
        pk[1] = "ICDBeginCode";
        pk[2] = "ICDEndCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getOPSTypeLevel()
    {
        if (OPSTypeLevel != null && !OPSTypeLevel.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OPSTypeLevel = StrTool.unicodeToGBK(OPSTypeLevel);
        }
        return OPSTypeLevel;
    }

    public void setOPSTypeLevel(String aOPSTypeLevel)
    {
        OPSTypeLevel = aOPSTypeLevel;
    }

    public String getICDBeginCode()
    {
        if (ICDBeginCode != null && !ICDBeginCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ICDBeginCode = StrTool.unicodeToGBK(ICDBeginCode);
        }
        return ICDBeginCode;
    }

    public void setICDBeginCode(String aICDBeginCode)
    {
        ICDBeginCode = aICDBeginCode;
    }

    public String getICDEndCode()
    {
        if (ICDEndCode != null && !ICDEndCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ICDEndCode = StrTool.unicodeToGBK(ICDEndCode);
        }
        return ICDEndCode;
    }

    public void setICDEndCode(String aICDEndCode)
    {
        ICDEndCode = aICDEndCode;
    }

    public String getOPSType()
    {
        if (OPSType != null && !OPSType.equals("") && SysConst.CHANGECHARSET == true)
        {
            OPSType = StrTool.unicodeToGBK(OPSType);
        }
        return OPSType;
    }

    public void setOPSType(String aOPSType)
    {
        OPSType = aOPSType;
    }

    /**
     * 使用另外一个 LHICDOPSTypeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LHICDOPSTypeSchema aLHICDOPSTypeSchema)
    {
        this.OPSTypeLevel = aLHICDOPSTypeSchema.getOPSTypeLevel();
        this.ICDBeginCode = aLHICDOPSTypeSchema.getICDBeginCode();
        this.ICDEndCode = aLHICDOPSTypeSchema.getICDEndCode();
        this.OPSType = aLHICDOPSTypeSchema.getOPSType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("OPSTypeLevel") == null)
            {
                this.OPSTypeLevel = null;
            }
            else
            {
                this.OPSTypeLevel = rs.getString("OPSTypeLevel").trim();
            }

            if (rs.getString("ICDBeginCode") == null)
            {
                this.ICDBeginCode = null;
            }
            else
            {
                this.ICDBeginCode = rs.getString("ICDBeginCode").trim();
            }

            if (rs.getString("ICDEndCode") == null)
            {
                this.ICDEndCode = null;
            }
            else
            {
                this.ICDEndCode = rs.getString("ICDEndCode").trim();
            }

            if (rs.getString("OPSType") == null)
            {
                this.OPSType = null;
            }
            else
            {
                this.OPSType = rs.getString("OPSType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHICDOPSTypeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LHICDOPSTypeSchema getSchema()
    {
        LHICDOPSTypeSchema aLHICDOPSTypeSchema = new LHICDOPSTypeSchema();
        aLHICDOPSTypeSchema.setSchema(this);
        return aLHICDOPSTypeSchema;
    }

    public LHICDOPSTypeDB getDB()
    {
        LHICDOPSTypeDB aDBOper = new LHICDOPSTypeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHICDOPSType描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(OPSTypeLevel)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ICDBeginCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ICDEndCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OPSType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHICDOPSType>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            OPSTypeLevel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            ICDBeginCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ICDEndCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            OPSType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHICDOPSTypeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("OPSTypeLevel"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OPSTypeLevel));
        }
        if (FCode.equals("ICDBeginCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ICDBeginCode));
        }
        if (FCode.equals("ICDEndCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ICDEndCode));
        }
        if (FCode.equals("OPSType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OPSType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(OPSTypeLevel);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ICDBeginCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ICDEndCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OPSType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("OPSTypeLevel"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OPSTypeLevel = FValue.trim();
            }
            else
            {
                OPSTypeLevel = null;
            }
        }
        if (FCode.equals("ICDBeginCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ICDBeginCode = FValue.trim();
            }
            else
            {
                ICDBeginCode = null;
            }
        }
        if (FCode.equals("ICDEndCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ICDEndCode = FValue.trim();
            }
            else
            {
                ICDEndCode = null;
            }
        }
        if (FCode.equals("OPSType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OPSType = FValue.trim();
            }
            else
            {
                OPSType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LHICDOPSTypeSchema other = (LHICDOPSTypeSchema) otherObject;
        return
                OPSTypeLevel.equals(other.getOPSTypeLevel())
                && ICDBeginCode.equals(other.getICDBeginCode())
                && ICDEndCode.equals(other.getICDEndCode())
                && OPSType.equals(other.getOPSType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("OPSTypeLevel"))
        {
            return 0;
        }
        if (strFieldName.equals("ICDBeginCode"))
        {
            return 1;
        }
        if (strFieldName.equals("ICDEndCode"))
        {
            return 2;
        }
        if (strFieldName.equals("OPSType"))
        {
            return 3;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "OPSTypeLevel";
                break;
            case 1:
                strFieldName = "ICDBeginCode";
                break;
            case 2:
                strFieldName = "ICDEndCode";
                break;
            case 3:
                strFieldName = "OPSType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("OPSTypeLevel"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ICDBeginCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ICDEndCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OPSType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
