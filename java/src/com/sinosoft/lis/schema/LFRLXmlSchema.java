/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LFRLXmlDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LFRLXmlSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2005-03-06
 */
public class LFRLXmlSchema implements Schema
{
    // @Field
    /** Comcodeisc */
    private String ComCodeISC;
    /** Itemcode */
    private String ItemCode;
    /** Reptype */
    private String RepType;
    /** Statyear */
    private double StatYear;
    /** Statmon */
    private double StatMon;
    /** Upitemcode */
    private String UpItemCode;
    /** Parentcomcodeisc */
    private String ParentComCodeISC;
    /** Layer */
    private double Layer;
    /** Statvalue */
    private double StatValue;
    /** Remark */
    private String Remark;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFRLXmlSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "ComCodeISC";
        pk[1] = "ItemCode";
        pk[2] = "RepType";
        pk[3] = "StatYear";
        pk[4] = "StatMon";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getComCodeISC()
    {
        if (ComCodeISC != null && !ComCodeISC.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ComCodeISC = StrTool.unicodeToGBK(ComCodeISC);
        }
        return ComCodeISC;
    }

    public void setComCodeISC(String aComCodeISC)
    {
        ComCodeISC = aComCodeISC;
    }

    public String getItemCode()
    {
        if (ItemCode != null && !ItemCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            ItemCode = StrTool.unicodeToGBK(ItemCode);
        }
        return ItemCode;
    }

    public void setItemCode(String aItemCode)
    {
        ItemCode = aItemCode;
    }

    public String getRepType()
    {
        if (RepType != null && !RepType.equals("") && SysConst.CHANGECHARSET == true)
        {
            RepType = StrTool.unicodeToGBK(RepType);
        }
        return RepType;
    }

    public void setRepType(String aRepType)
    {
        RepType = aRepType;
    }

    public double getStatYear()
    {
        return StatYear;
    }

    public void setStatYear(double aStatYear)
    {
        StatYear = aStatYear;
    }

    public void setStatYear(String aStatYear)
    {
        if (aStatYear != null && !aStatYear.equals(""))
        {
            Double tDouble = new Double(aStatYear);
            double d = tDouble.doubleValue();
            StatYear = d;
        }
    }

    public double getStatMon()
    {
        return StatMon;
    }

    public void setStatMon(double aStatMon)
    {
        StatMon = aStatMon;
    }

    public void setStatMon(String aStatMon)
    {
        if (aStatMon != null && !aStatMon.equals(""))
        {
            Double tDouble = new Double(aStatMon);
            double d = tDouble.doubleValue();
            StatMon = d;
        }
    }

    public String getUpItemCode()
    {
        if (UpItemCode != null && !UpItemCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            UpItemCode = StrTool.unicodeToGBK(UpItemCode);
        }
        return UpItemCode;
    }

    public void setUpItemCode(String aUpItemCode)
    {
        UpItemCode = aUpItemCode;
    }

    public String getParentComCodeISC()
    {
        if (ParentComCodeISC != null && !ParentComCodeISC.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ParentComCodeISC = StrTool.unicodeToGBK(ParentComCodeISC);
        }
        return ParentComCodeISC;
    }

    public void setParentComCodeISC(String aParentComCodeISC)
    {
        ParentComCodeISC = aParentComCodeISC;
    }

    public double getLayer()
    {
        return Layer;
    }

    public void setLayer(double aLayer)
    {
        Layer = aLayer;
    }

    public void setLayer(String aLayer)
    {
        if (aLayer != null && !aLayer.equals(""))
        {
            Double tDouble = new Double(aLayer);
            double d = tDouble.doubleValue();
            Layer = d;
        }
    }

    public double getStatValue()
    {
        return StatValue;
    }

    public void setStatValue(double aStatValue)
    {
        StatValue = aStatValue;
    }

    public void setStatValue(String aStatValue)
    {
        if (aStatValue != null && !aStatValue.equals(""))
        {
            Double tDouble = new Double(aStatValue);
            double d = tDouble.doubleValue();
            StatValue = d;
        }
    }

    public String getRemark()
    {
        if (Remark != null && !Remark.equals("") && SysConst.CHANGECHARSET == true)
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }

    /**
     * 使用另外一个 LFRLXmlSchema 对象给 Schema 赋值
     * @param: aLFRLXmlSchema LFRLXmlSchema
     **/
    public void setSchema(LFRLXmlSchema aLFRLXmlSchema)
    {
        this.ComCodeISC = aLFRLXmlSchema.getComCodeISC();
        this.ItemCode = aLFRLXmlSchema.getItemCode();
        this.RepType = aLFRLXmlSchema.getRepType();
        this.StatYear = aLFRLXmlSchema.getStatYear();
        this.StatMon = aLFRLXmlSchema.getStatMon();
        this.UpItemCode = aLFRLXmlSchema.getUpItemCode();
        this.ParentComCodeISC = aLFRLXmlSchema.getParentComCodeISC();
        this.Layer = aLFRLXmlSchema.getLayer();
        this.StatValue = aLFRLXmlSchema.getStatValue();
        this.Remark = aLFRLXmlSchema.getRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ComCodeISC") == null)
            {
                this.ComCodeISC = null;
            }
            else
            {
                this.ComCodeISC = rs.getString("ComCodeISC").trim();
            }

            if (rs.getString("ItemCode") == null)
            {
                this.ItemCode = null;
            }
            else
            {
                this.ItemCode = rs.getString("ItemCode").trim();
            }

            if (rs.getString("RepType") == null)
            {
                this.RepType = null;
            }
            else
            {
                this.RepType = rs.getString("RepType").trim();
            }

            this.StatYear = rs.getDouble("StatYear");
            this.StatMon = rs.getDouble("StatMon");
            if (rs.getString("UpItemCode") == null)
            {
                this.UpItemCode = null;
            }
            else
            {
                this.UpItemCode = rs.getString("UpItemCode").trim();
            }

            if (rs.getString("ParentComCodeISC") == null)
            {
                this.ParentComCodeISC = null;
            }
            else
            {
                this.ParentComCodeISC = rs.getString("ParentComCodeISC").trim();
            }

            this.Layer = rs.getDouble("Layer");
            this.StatValue = rs.getDouble("StatValue");
            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFRLXmlSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFRLXmlSchema getSchema()
    {
        LFRLXmlSchema aLFRLXmlSchema = new LFRLXmlSchema();
        aLFRLXmlSchema.setSchema(this);
        return aLFRLXmlSchema;
    }

    public LFRLXmlDB getDB()
    {
        LFRLXmlDB aDBOper = new LFRLXmlDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFRLXml描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ComCodeISC)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ItemCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RepType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(StatYear) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StatMon) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(UpItemCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ParentComCodeISC)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Layer) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StatValue) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Remark));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFRLXml>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ComCodeISC = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            ItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            RepType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            StatYear = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            StatMon = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            UpItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            ParentComCodeISC = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              7, SysConst.PACKAGESPILTER);
            Layer = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    8, SysConst.PACKAGESPILTER))).doubleValue();
            StatValue = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFRLXmlSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ComCodeISC"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ComCodeISC));
        }
        if (FCode.equals("ItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ItemCode));
        }
        if (FCode.equals("RepType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RepType));
        }
        if (FCode.equals("StatYear"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StatYear));
        }
        if (FCode.equals("StatMon"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StatMon));
        }
        if (FCode.equals("UpItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(UpItemCode));
        }
        if (FCode.equals("ParentComCodeISC"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ParentComCodeISC));
        }
        if (FCode.equals("Layer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Layer));
        }
        if (FCode.equals("StatValue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(StatValue));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Remark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ComCodeISC);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ItemCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RepType);
                break;
            case 3:
                strFieldValue = String.valueOf(StatYear);
                break;
            case 4:
                strFieldValue = String.valueOf(StatMon);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(UpItemCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ParentComCodeISC);
                break;
            case 7:
                strFieldValue = String.valueOf(Layer);
                break;
            case 8:
                strFieldValue = String.valueOf(StatValue);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ComCodeISC"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ComCodeISC = FValue.trim();
            }
            else
            {
                ComCodeISC = null;
            }
        }
        if (FCode.equals("ItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ItemCode = FValue.trim();
            }
            else
            {
                ItemCode = null;
            }
        }
        if (FCode.equals("RepType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RepType = FValue.trim();
            }
            else
            {
                RepType = null;
            }
        }
        if (FCode.equals("StatYear"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StatYear = d;
            }
        }
        if (FCode.equals("StatMon"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StatMon = d;
            }
        }
        if (FCode.equals("UpItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                UpItemCode = FValue.trim();
            }
            else
            {
                UpItemCode = null;
            }
        }
        if (FCode.equals("ParentComCodeISC"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ParentComCodeISC = FValue.trim();
            }
            else
            {
                ParentComCodeISC = null;
            }
        }
        if (FCode.equals("Layer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Layer = d;
            }
        }
        if (FCode.equals("StatValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                StatValue = d;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFRLXmlSchema other = (LFRLXmlSchema) otherObject;
        return
                ComCodeISC.equals(other.getComCodeISC())
                && ItemCode.equals(other.getItemCode())
                && RepType.equals(other.getRepType())
                && StatYear == other.getStatYear()
                && StatMon == other.getStatMon()
                && UpItemCode.equals(other.getUpItemCode())
                && ParentComCodeISC.equals(other.getParentComCodeISC())
                && Layer == other.getLayer()
                && StatValue == other.getStatValue()
                && Remark.equals(other.getRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ComCodeISC"))
        {
            return 0;
        }
        if (strFieldName.equals("ItemCode"))
        {
            return 1;
        }
        if (strFieldName.equals("RepType"))
        {
            return 2;
        }
        if (strFieldName.equals("StatYear"))
        {
            return 3;
        }
        if (strFieldName.equals("StatMon"))
        {
            return 4;
        }
        if (strFieldName.equals("UpItemCode"))
        {
            return 5;
        }
        if (strFieldName.equals("ParentComCodeISC"))
        {
            return 6;
        }
        if (strFieldName.equals("Layer"))
        {
            return 7;
        }
        if (strFieldName.equals("StatValue"))
        {
            return 8;
        }
        if (strFieldName.equals("Remark"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ComCodeISC";
                break;
            case 1:
                strFieldName = "ItemCode";
                break;
            case 2:
                strFieldName = "RepType";
                break;
            case 3:
                strFieldName = "StatYear";
                break;
            case 4:
                strFieldName = "StatMon";
                break;
            case 5:
                strFieldName = "UpItemCode";
                break;
            case 6:
                strFieldName = "ParentComCodeISC";
                break;
            case 7:
                strFieldName = "Layer";
                break;
            case 8:
                strFieldName = "StatValue";
                break;
            case 9:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ComCodeISC"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RepType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("StatYear"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StatMon"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("UpItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParentComCodeISC"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Layer"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("StatValue"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
