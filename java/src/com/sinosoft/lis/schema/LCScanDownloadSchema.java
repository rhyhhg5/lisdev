/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCScanDownloadDB;

/*
 * <p>ClassName: LCScanDownloadSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_3
 * @CreateDate：2005-10-13
 */
public class LCScanDownloadSchema implements Schema, Cloneable {
    // @Field
    /** 单证编号 */
    private double DocID;
    /** 单证号码 */
    private String DocCode;
    /** 业务类型 */
    private String BussType;
    /** 单证细类 */
    private String SubType;
    /** 单证页数 */
    private double NumPages;
    /** 单证状态 */
    private String DocFlag;
    /** 管理机构 */
    private String ManageCom;
    /** 下载次数 */
    private String DownCount;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCScanDownloadSchema() {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCScanDownloadSchema cloned = (LCScanDownloadSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public double getDocID() {
        return DocID;
    }

    public void setDocID(double aDocID) {
        DocID = Arith.round(aDocID, 0);
    }

    public void setDocID(String aDocID) {
        if (aDocID != null && !aDocID.equals("")) {
            Double tDouble = new Double(aDocID);
            double d = tDouble.doubleValue();
            DocID = Arith.round(d, 0);
        }
    }

    public String getDocCode() {
        return DocCode;
    }

    public void setDocCode(String aDocCode) {
        DocCode = aDocCode;
    }

    public String getBussType() {
        return BussType;
    }

    public void setBussType(String aBussType) {
        BussType = aBussType;
    }

    public String getSubType() {
        return SubType;
    }

    public void setSubType(String aSubType) {
        SubType = aSubType;
    }

    public double getNumPages() {
        return NumPages;
    }

    public void setNumPages(double aNumPages) {
        NumPages = Arith.round(aNumPages, 0);
    }

    public void setNumPages(String aNumPages) {
        if (aNumPages != null && !aNumPages.equals("")) {
            Double tDouble = new Double(aNumPages);
            double d = tDouble.doubleValue();
            NumPages = Arith.round(d, 0);
        }
    }

    public String getDocFlag() {
        return DocFlag;
    }

    public void setDocFlag(String aDocFlag) {
        DocFlag = aDocFlag;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getDownCount() {
        return DownCount;
    }

    public void setDownCount(String aDownCount) {
        DownCount = aDownCount;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCScanDownloadSchema 对象给 Schema 赋值
     * @param: aLCScanDownloadSchema LCScanDownloadSchema
     **/
    public void setSchema(LCScanDownloadSchema aLCScanDownloadSchema) {
        this.DocID = aLCScanDownloadSchema.getDocID();
        this.DocCode = aLCScanDownloadSchema.getDocCode();
        this.BussType = aLCScanDownloadSchema.getBussType();
        this.SubType = aLCScanDownloadSchema.getSubType();
        this.NumPages = aLCScanDownloadSchema.getNumPages();
        this.DocFlag = aLCScanDownloadSchema.getDocFlag();
        this.ManageCom = aLCScanDownloadSchema.getManageCom();
        this.DownCount = aLCScanDownloadSchema.getDownCount();
        this.Operator = aLCScanDownloadSchema.getOperator();
        this.MakeDate = fDate.getDate(aLCScanDownloadSchema.getMakeDate());
        this.MakeTime = aLCScanDownloadSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCScanDownloadSchema.getModifyDate());
        this.ModifyTime = aLCScanDownloadSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            this.DocID = rs.getDouble("DocID");
            if (rs.getString("DocCode") == null) {
                this.DocCode = null;
            } else {
                this.DocCode = rs.getString("DocCode").trim();
            }

            if (rs.getString("BussType") == null) {
                this.BussType = null;
            } else {
                this.BussType = rs.getString("BussType").trim();
            }

            if (rs.getString("SubType") == null) {
                this.SubType = null;
            } else {
                this.SubType = rs.getString("SubType").trim();
            }

            this.NumPages = rs.getDouble("NumPages");
            if (rs.getString("DocFlag") == null) {
                this.DocFlag = null;
            } else {
                this.DocFlag = rs.getString("DocFlag").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("DownCount") == null) {
                this.DownCount = null;
            } else {
                this.DownCount = rs.getString("DownCount").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LCScanDownload表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCScanDownloadSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LCScanDownloadSchema getSchema() {
        LCScanDownloadSchema aLCScanDownloadSchema = new LCScanDownloadSchema();
        aLCScanDownloadSchema.setSchema(this);
        return aLCScanDownloadSchema;
    }

    public LCScanDownloadDB getDB() {
        LCScanDownloadDB aDBOper = new LCScanDownloadDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCScanDownload描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(DocID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DocCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BussType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SubType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(NumPages));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DocFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DownCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCScanDownload>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            DocID = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    1, SysConst.PACKAGESPILTER))).doubleValue();
            DocCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            SubType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            NumPages = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            DocFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            DownCount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCScanDownloadSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("DocID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocID));
        }
        if (FCode.equals("DocCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocCode));
        }
        if (FCode.equals("BussType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BussType));
        }
        if (FCode.equals("SubType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubType));
        }
        if (FCode.equals("NumPages")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NumPages));
        }
        if (FCode.equals("DocFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DocFlag));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("DownCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DownCount));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = String.valueOf(DocID);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(DocCode);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(BussType);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(SubType);
            break;
        case 4:
            strFieldValue = String.valueOf(NumPages);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(DocFlag);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(DownCount);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("DocID")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DocID = d;
            }
        }
        if (FCode.equalsIgnoreCase("DocCode")) {
            if (FValue != null && !FValue.equals("")) {
                DocCode = FValue.trim();
            } else {
                DocCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BussType")) {
            if (FValue != null && !FValue.equals("")) {
                BussType = FValue.trim();
            } else {
                BussType = null;
            }
        }
        if (FCode.equalsIgnoreCase("SubType")) {
            if (FValue != null && !FValue.equals("")) {
                SubType = FValue.trim();
            } else {
                SubType = null;
            }
        }
        if (FCode.equalsIgnoreCase("NumPages")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                NumPages = d;
            }
        }
        if (FCode.equalsIgnoreCase("DocFlag")) {
            if (FValue != null && !FValue.equals("")) {
                DocFlag = FValue.trim();
            } else {
                DocFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("DownCount")) {
            if (FValue != null && !FValue.equals("")) {
                DownCount = FValue.trim();
            } else {
                DownCount = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LCScanDownloadSchema other = (LCScanDownloadSchema) otherObject;
        return
                DocID == other.getDocID()
                && DocCode.equals(other.getDocCode())
                && BussType.equals(other.getBussType())
                && SubType.equals(other.getSubType())
                && NumPages == other.getNumPages()
                && DocFlag.equals(other.getDocFlag())
                && ManageCom.equals(other.getManageCom())
                && DownCount.equals(other.getDownCount())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("DocID")) {
            return 0;
        }
        if (strFieldName.equals("DocCode")) {
            return 1;
        }
        if (strFieldName.equals("BussType")) {
            return 2;
        }
        if (strFieldName.equals("SubType")) {
            return 3;
        }
        if (strFieldName.equals("NumPages")) {
            return 4;
        }
        if (strFieldName.equals("DocFlag")) {
            return 5;
        }
        if (strFieldName.equals("ManageCom")) {
            return 6;
        }
        if (strFieldName.equals("DownCount")) {
            return 7;
        }
        if (strFieldName.equals("Operator")) {
            return 8;
        }
        if (strFieldName.equals("MakeDate")) {
            return 9;
        }
        if (strFieldName.equals("MakeTime")) {
            return 10;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 11;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "DocID";
            break;
        case 1:
            strFieldName = "DocCode";
            break;
        case 2:
            strFieldName = "BussType";
            break;
        case 3:
            strFieldName = "SubType";
            break;
        case 4:
            strFieldName = "NumPages";
            break;
        case 5:
            strFieldName = "DocFlag";
            break;
        case 6:
            strFieldName = "ManageCom";
            break;
        case 7:
            strFieldName = "DownCount";
            break;
        case 8:
            strFieldName = "Operator";
            break;
        case 9:
            strFieldName = "MakeDate";
            break;
        case 10:
            strFieldName = "MakeTime";
            break;
        case 11:
            strFieldName = "ModifyDate";
            break;
        case 12:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("DocID")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DocCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BussType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NumPages")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DocFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DownCount")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
