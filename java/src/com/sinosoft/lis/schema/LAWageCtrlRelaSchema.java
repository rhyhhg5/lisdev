/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAWageCtrlRelaDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LAWageCtrlRelaSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-29
 */
public class LAWageCtrlRelaSchema implements Schema
{
    // @Field
    /** 提取控制类型 */
    private String CtrlRelaType;
    /** 险种编码 */
    private String RiskCode;
    /** 源控制要素1 */
    private String OriginFactor1;
    /** 源控制要素 */
    private String OriginFactor;
    /** 源控制要素2 */
    private String OriginFactor2;
    /** 目标控制要素 */
    private String DestFactor;
    /** 目标控制要素1 */
    private String DestFactor1;
    /** 目标控制要素2 */
    private String DestFactor2;

    public static final int FIELDNUM = 8; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAWageCtrlRelaSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "CtrlRelaType";
        pk[1] = "RiskCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCtrlRelaType()
    {
        if (SysConst.CHANGECHARSET && CtrlRelaType != null &&
            !CtrlRelaType.equals(""))
        {
            CtrlRelaType = StrTool.unicodeToGBK(CtrlRelaType);
        }
        return CtrlRelaType;
    }

    public void setCtrlRelaType(String aCtrlRelaType)
    {
        CtrlRelaType = aCtrlRelaType;
    }

    public String getRiskCode()
    {
        if (SysConst.CHANGECHARSET && RiskCode != null && !RiskCode.equals(""))
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getOriginFactor1()
    {
        if (SysConst.CHANGECHARSET && OriginFactor1 != null &&
            !OriginFactor1.equals(""))
        {
            OriginFactor1 = StrTool.unicodeToGBK(OriginFactor1);
        }
        return OriginFactor1;
    }

    public void setOriginFactor1(String aOriginFactor1)
    {
        OriginFactor1 = aOriginFactor1;
    }

    public String getOriginFactor()
    {
        if (SysConst.CHANGECHARSET && OriginFactor != null &&
            !OriginFactor.equals(""))
        {
            OriginFactor = StrTool.unicodeToGBK(OriginFactor);
        }
        return OriginFactor;
    }

    public void setOriginFactor(String aOriginFactor)
    {
        OriginFactor = aOriginFactor;
    }

    public String getOriginFactor2()
    {
        if (SysConst.CHANGECHARSET && OriginFactor2 != null &&
            !OriginFactor2.equals(""))
        {
            OriginFactor2 = StrTool.unicodeToGBK(OriginFactor2);
        }
        return OriginFactor2;
    }

    public void setOriginFactor2(String aOriginFactor2)
    {
        OriginFactor2 = aOriginFactor2;
    }

    public String getDestFactor()
    {
        if (SysConst.CHANGECHARSET && DestFactor != null &&
            !DestFactor.equals(""))
        {
            DestFactor = StrTool.unicodeToGBK(DestFactor);
        }
        return DestFactor;
    }

    public void setDestFactor(String aDestFactor)
    {
        DestFactor = aDestFactor;
    }

    public String getDestFactor1()
    {
        if (SysConst.CHANGECHARSET && DestFactor1 != null &&
            !DestFactor1.equals(""))
        {
            DestFactor1 = StrTool.unicodeToGBK(DestFactor1);
        }
        return DestFactor1;
    }

    public void setDestFactor1(String aDestFactor1)
    {
        DestFactor1 = aDestFactor1;
    }

    public String getDestFactor2()
    {
        if (SysConst.CHANGECHARSET && DestFactor2 != null &&
            !DestFactor2.equals(""))
        {
            DestFactor2 = StrTool.unicodeToGBK(DestFactor2);
        }
        return DestFactor2;
    }

    public void setDestFactor2(String aDestFactor2)
    {
        DestFactor2 = aDestFactor2;
    }

    /**
     * 使用另外一个 LAWageCtrlRelaSchema 对象给 Schema 赋值
     * @param: aLAWageCtrlRelaSchema LAWageCtrlRelaSchema
     **/
    public void setSchema(LAWageCtrlRelaSchema aLAWageCtrlRelaSchema)
    {
        this.CtrlRelaType = aLAWageCtrlRelaSchema.getCtrlRelaType();
        this.RiskCode = aLAWageCtrlRelaSchema.getRiskCode();
        this.OriginFactor1 = aLAWageCtrlRelaSchema.getOriginFactor1();
        this.OriginFactor = aLAWageCtrlRelaSchema.getOriginFactor();
        this.OriginFactor2 = aLAWageCtrlRelaSchema.getOriginFactor2();
        this.DestFactor = aLAWageCtrlRelaSchema.getDestFactor();
        this.DestFactor1 = aLAWageCtrlRelaSchema.getDestFactor1();
        this.DestFactor2 = aLAWageCtrlRelaSchema.getDestFactor2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CtrlRelaType") == null)
            {
                this.CtrlRelaType = null;
            }
            else
            {
                this.CtrlRelaType = rs.getString("CtrlRelaType").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("OriginFactor1") == null)
            {
                this.OriginFactor1 = null;
            }
            else
            {
                this.OriginFactor1 = rs.getString("OriginFactor1").trim();
            }

            if (rs.getString("OriginFactor") == null)
            {
                this.OriginFactor = null;
            }
            else
            {
                this.OriginFactor = rs.getString("OriginFactor").trim();
            }

            if (rs.getString("OriginFactor2") == null)
            {
                this.OriginFactor2 = null;
            }
            else
            {
                this.OriginFactor2 = rs.getString("OriginFactor2").trim();
            }

            if (rs.getString("DestFactor") == null)
            {
                this.DestFactor = null;
            }
            else
            {
                this.DestFactor = rs.getString("DestFactor").trim();
            }

            if (rs.getString("DestFactor1") == null)
            {
                this.DestFactor1 = null;
            }
            else
            {
                this.DestFactor1 = rs.getString("DestFactor1").trim();
            }

            if (rs.getString("DestFactor2") == null)
            {
                this.DestFactor2 = null;
            }
            else
            {
                this.DestFactor2 = rs.getString("DestFactor2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageCtrlRelaSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAWageCtrlRelaSchema getSchema()
    {
        LAWageCtrlRelaSchema aLAWageCtrlRelaSchema = new LAWageCtrlRelaSchema();
        aLAWageCtrlRelaSchema.setSchema(this);
        return aLAWageCtrlRelaSchema;
    }

    public LAWageCtrlRelaDB getDB()
    {
        LAWageCtrlRelaDB aDBOper = new LAWageCtrlRelaDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageCtrlRela描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(CtrlRelaType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OriginFactor1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OriginFactor)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OriginFactor2)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DestFactor)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DestFactor1)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DestFactor2));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageCtrlRela>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CtrlRelaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                          SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            OriginFactor1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            OriginFactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            OriginFactor2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            DestFactor = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            DestFactor1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
            DestFactor2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageCtrlRelaSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CtrlRelaType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CtrlRelaType));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
        }
        if (FCode.equals("OriginFactor1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OriginFactor1));
        }
        if (FCode.equals("OriginFactor"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OriginFactor));
        }
        if (FCode.equals("OriginFactor2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OriginFactor2));
        }
        if (FCode.equals("DestFactor"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DestFactor));
        }
        if (FCode.equals("DestFactor1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DestFactor1));
        }
        if (FCode.equals("DestFactor2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DestFactor2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CtrlRelaType);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(OriginFactor1);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(OriginFactor);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(OriginFactor2);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DestFactor);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(DestFactor1);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(DestFactor2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CtrlRelaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CtrlRelaType = FValue.trim();
            }
            else
            {
                CtrlRelaType = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("OriginFactor1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OriginFactor1 = FValue.trim();
            }
            else
            {
                OriginFactor1 = null;
            }
        }
        if (FCode.equals("OriginFactor"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OriginFactor = FValue.trim();
            }
            else
            {
                OriginFactor = null;
            }
        }
        if (FCode.equals("OriginFactor2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OriginFactor2 = FValue.trim();
            }
            else
            {
                OriginFactor2 = null;
            }
        }
        if (FCode.equals("DestFactor"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestFactor = FValue.trim();
            }
            else
            {
                DestFactor = null;
            }
        }
        if (FCode.equals("DestFactor1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestFactor1 = FValue.trim();
            }
            else
            {
                DestFactor1 = null;
            }
        }
        if (FCode.equals("DestFactor2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DestFactor2 = FValue.trim();
            }
            else
            {
                DestFactor2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAWageCtrlRelaSchema other = (LAWageCtrlRelaSchema) otherObject;
        return
                CtrlRelaType.equals(other.getCtrlRelaType())
                && RiskCode.equals(other.getRiskCode())
                && OriginFactor1.equals(other.getOriginFactor1())
                && OriginFactor.equals(other.getOriginFactor())
                && OriginFactor2.equals(other.getOriginFactor2())
                && DestFactor.equals(other.getDestFactor())
                && DestFactor1.equals(other.getDestFactor1())
                && DestFactor2.equals(other.getDestFactor2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CtrlRelaType"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 1;
        }
        if (strFieldName.equals("OriginFactor1"))
        {
            return 2;
        }
        if (strFieldName.equals("OriginFactor"))
        {
            return 3;
        }
        if (strFieldName.equals("OriginFactor2"))
        {
            return 4;
        }
        if (strFieldName.equals("DestFactor"))
        {
            return 5;
        }
        if (strFieldName.equals("DestFactor1"))
        {
            return 6;
        }
        if (strFieldName.equals("DestFactor2"))
        {
            return 7;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CtrlRelaType";
                break;
            case 1:
                strFieldName = "RiskCode";
                break;
            case 2:
                strFieldName = "OriginFactor1";
                break;
            case 3:
                strFieldName = "OriginFactor";
                break;
            case 4:
                strFieldName = "OriginFactor2";
                break;
            case 5:
                strFieldName = "DestFactor";
                break;
            case 6:
                strFieldName = "DestFactor1";
                break;
            case 7:
                strFieldName = "DestFactor2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CtrlRelaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OriginFactor1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OriginFactor"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OriginFactor2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestFactor"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestFactor1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DestFactor2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
