/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.tab_db_connectionDB;

/*
 * <p>ClassName: tab_db_connectionSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 产品化中间业务平台
 * @CreateDate：2007-04-17
 */
public class tab_db_connectionSchema implements Schema, Cloneable
{
	// @Field
	/** 服务id */
	private String service_id;
	/** 数据库连接类型 */
	private String conn_type;
	/** 连接串 */
	private String db_conn_string;
	/** 连接用户名 */
	private String db_conn_user;
	/** 连接密码 */
	private String db_conn_pass;
	/** 数据库连接data source */
	private String datasource;
	/** 数据库类型 */
	private String database;
	/** 数据库版本 */
	private String db_version;
	/** 说明 */
	private String info;
	/** 备用字段 */
	private String bak1;
	/** 备用字段2 */
	private String bak2;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后修改日期 */
	private Date ModifyDate;
	/** 最后修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 15;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public tab_db_connectionSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "service_id";
		pk[1] = "conn_type";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                tab_db_connectionSchema cloned = (tab_db_connectionSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getservice_id()
	{
		return service_id;
	}
	public void setservice_id(String aservice_id)
	{
		service_id = aservice_id;
	}
	public String getconn_type()
	{
		return conn_type;
	}
	public void setconn_type(String aconn_type)
	{
		conn_type = aconn_type;
	}
	public String getdb_conn_string()
	{
		return db_conn_string;
	}
	public void setdb_conn_string(String adb_conn_string)
	{
		db_conn_string = adb_conn_string;
	}
	public String getdb_conn_user()
	{
		return db_conn_user;
	}
	public void setdb_conn_user(String adb_conn_user)
	{
		db_conn_user = adb_conn_user;
	}
	public String getdb_conn_pass()
	{
		return db_conn_pass;
	}
	public void setdb_conn_pass(String adb_conn_pass)
	{
		db_conn_pass = adb_conn_pass;
	}
	public String getdatasource()
	{
		return datasource;
	}
	public void setdatasource(String adatasource)
	{
		datasource = adatasource;
	}
	public String getdatabase()
	{
		return database;
	}
	public void setdatabase(String adatabase)
	{
		database = adatabase;
	}
	public String getdb_version()
	{
		return db_version;
	}
	public void setdb_version(String adb_version)
	{
		db_version = adb_version;
	}
	public String getinfo()
	{
		return info;
	}
	public void setinfo(String ainfo)
	{
		info = ainfo;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 tab_db_connectionSchema 对象给 Schema 赋值
	* @param: atab_db_connectionSchema tab_db_connectionSchema
	**/
	public void setSchema(tab_db_connectionSchema atab_db_connectionSchema)
	{
		this.service_id = atab_db_connectionSchema.getservice_id();
		this.conn_type = atab_db_connectionSchema.getconn_type();
		this.db_conn_string = atab_db_connectionSchema.getdb_conn_string();
		this.db_conn_user = atab_db_connectionSchema.getdb_conn_user();
		this.db_conn_pass = atab_db_connectionSchema.getdb_conn_pass();
		this.datasource = atab_db_connectionSchema.getdatasource();
		this.database = atab_db_connectionSchema.getdatabase();
		this.db_version = atab_db_connectionSchema.getdb_version();
		this.info = atab_db_connectionSchema.getinfo();
		this.bak1 = atab_db_connectionSchema.getbak1();
		this.bak2 = atab_db_connectionSchema.getbak2();
		this.MakeDate = fDate.getDate( atab_db_connectionSchema.getMakeDate());
		this.MakeTime = atab_db_connectionSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( atab_db_connectionSchema.getModifyDate());
		this.ModifyTime = atab_db_connectionSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("service_id") == null )
				this.service_id = null;
			else
				this.service_id = rs.getString("service_id").trim();

			if( rs.getString("conn_type") == null )
				this.conn_type = null;
			else
				this.conn_type = rs.getString("conn_type").trim();

			if( rs.getString("db_conn_string") == null )
				this.db_conn_string = null;
			else
				this.db_conn_string = rs.getString("db_conn_string").trim();

			if( rs.getString("db_conn_user") == null )
				this.db_conn_user = null;
			else
				this.db_conn_user = rs.getString("db_conn_user").trim();

			if( rs.getString("db_conn_pass") == null )
				this.db_conn_pass = null;
			else
				this.db_conn_pass = rs.getString("db_conn_pass").trim();

			if( rs.getString("datasource") == null )
				this.datasource = null;
			else
				this.datasource = rs.getString("datasource").trim();

			if( rs.getString("database") == null )
				this.database = null;
			else
				this.database = rs.getString("database").trim();

			if( rs.getString("db_version") == null )
				this.db_version = null;
			else
				this.db_version = rs.getString("db_version").trim();

			if( rs.getString("info") == null )
				this.info = null;
			else
				this.info = rs.getString("info").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的tab_db_connection表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "tab_db_connectionSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public tab_db_connectionSchema getSchema()
	{
		tab_db_connectionSchema atab_db_connectionSchema = new tab_db_connectionSchema();
		atab_db_connectionSchema.setSchema(this);
		return atab_db_connectionSchema;
	}

	public tab_db_connectionDB getDB()
	{
		tab_db_connectionDB aDBOper = new tab_db_connectionDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prptab_db_connection描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(service_id)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(conn_type)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(db_conn_string)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(db_conn_user)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(db_conn_pass)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(datasource)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(database)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(db_version)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(info)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prptab_db_connection>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			service_id = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			conn_type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			db_conn_string = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			db_conn_user = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			db_conn_pass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			datasource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			database = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			db_version = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			info = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "tab_db_connectionSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equalsIgnoreCase("service_id"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(service_id));
		}
		if (FCode.equalsIgnoreCase("conn_type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(conn_type));
		}
		if (FCode.equalsIgnoreCase("db_conn_string"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(db_conn_string));
		}
		if (FCode.equalsIgnoreCase("db_conn_user"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(db_conn_user));
		}
		if (FCode.equalsIgnoreCase("db_conn_pass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(db_conn_pass));
		}
		if (FCode.equalsIgnoreCase("datasource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(datasource));
		}
		if (FCode.equalsIgnoreCase("database"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(database));
		}
		if (FCode.equalsIgnoreCase("db_version"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(db_version));
		}
		if (FCode.equalsIgnoreCase("info"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(info));
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(service_id);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(conn_type);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(db_conn_string);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(db_conn_user);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(db_conn_pass);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(datasource);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(database);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(db_version);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(info);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("service_id"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				service_id = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("conn_type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				conn_type = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("db_conn_string"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				db_conn_string = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("db_conn_user"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				db_conn_user = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("db_conn_pass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				db_conn_pass = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("datasource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				datasource = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("database"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				database = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("db_version"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				db_version = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("info"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				info = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		tab_db_connectionSchema other = (tab_db_connectionSchema)otherObject;
		return
			service_id.equals(other.getservice_id())
			&& conn_type.equals(other.getconn_type())
			&& db_conn_string.equals(other.getdb_conn_string())
			&& db_conn_user.equals(other.getdb_conn_user())
			&& db_conn_pass.equals(other.getdb_conn_pass())
			&& datasource.equals(other.getdatasource())
			&& database.equals(other.getdatabase())
			&& db_version.equals(other.getdb_version())
			&& info.equals(other.getinfo())
			&& bak1.equals(other.getbak1())
			&& bak2.equals(other.getbak2())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("service_id") ) {
			return 0;
		}
		if( strFieldName.equals("conn_type") ) {
			return 1;
		}
		if( strFieldName.equals("db_conn_string") ) {
			return 2;
		}
		if( strFieldName.equals("db_conn_user") ) {
			return 3;
		}
		if( strFieldName.equals("db_conn_pass") ) {
			return 4;
		}
		if( strFieldName.equals("datasource") ) {
			return 5;
		}
		if( strFieldName.equals("database") ) {
			return 6;
		}
		if( strFieldName.equals("db_version") ) {
			return 7;
		}
		if( strFieldName.equals("info") ) {
			return 8;
		}
		if( strFieldName.equals("bak1") ) {
			return 9;
		}
		if( strFieldName.equals("bak2") ) {
			return 10;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 14;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "service_id";
				break;
			case 1:
				strFieldName = "conn_type";
				break;
			case 2:
				strFieldName = "db_conn_string";
				break;
			case 3:
				strFieldName = "db_conn_user";
				break;
			case 4:
				strFieldName = "db_conn_pass";
				break;
			case 5:
				strFieldName = "datasource";
				break;
			case 6:
				strFieldName = "database";
				break;
			case 7:
				strFieldName = "db_version";
				break;
			case 8:
				strFieldName = "info";
				break;
			case 9:
				strFieldName = "bak1";
				break;
			case 10:
				strFieldName = "bak2";
				break;
			case 11:
				strFieldName = "MakeDate";
				break;
			case 12:
				strFieldName = "MakeTime";
				break;
			case 13:
				strFieldName = "ModifyDate";
				break;
			case 14:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("service_id") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("conn_type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("db_conn_string") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("db_conn_user") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("db_conn_pass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("datasource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("database") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("db_version") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("info") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
