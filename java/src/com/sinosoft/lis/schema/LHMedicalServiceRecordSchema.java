/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHMedicalServiceRecordDB;

/*
 * <p>ClassName: LHMedicalServiceRecordSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 健康管理表修改_2005-12-06
 * @CreateDate：2005-12-06
 */
public class LHMedicalServiceRecordSchema implements Schema, Cloneable {
    // @Field
    /** 记录编号 */
    private String RecordNo;
    /** 客户号码 */
    private String CustomerNo;
    /** 首次记录日期 */
    private Date FirstRecoDate;
    /** 首次记录时间 */
    private String FirstRecoTime;
    /** 服务状态 */
    private String ServiceState;
    /** 服务项目编号 */
    private String ApplyDutyItemCode;
    /** 详细描述 */
    private String DescriptionDetail;
    /** 服务日期 */
    private Date ServiceDate;
    /** 服务时间 */
    private String ServiceTime;
    /** 服务机构编号 */
    private String HospitCode;
    /** 服务人代码 */
    private String DoctNo;
    /** 申请受理方式 */
    private String ApplyReceiveType;
    /** 是否需要预约 */
    private String ServiceFlag;
    /** 是否预约成功 */
    private String IsBespeakOk;
    /** 预约服务项目 */
    private String BespeakServiceItem;
    /** 预约服务描述 */
    private String ApplyServiceDes;
    /** 预约服务日期 */
    private Date BespeakServiceDate;
    /** 预约服务时间 */
    private String BespeakServiceTime;
    /** 预约服务机构 */
    private String BespeakServiceHospit;
    /** 预约服务人员 */
    private String BespeakServiceDoctNo;
    /** 不相符原因 */
    private String NoCompareCause;
    /** 预约失败原因 */
    private String BespeaLostCause;
    /** 服务执行状态 */
    private String ServiceExecState;
    /** 执行服务项目 */
    private String ExcuteServiceItem;
    /** 服务执行详细描述 */
    private String ExcuteDescription;
    /** 执行服务日期 */
    private Date ExcuteServiceDate;
    /** 执行服务时间 */
    private String ExcuteServiceTime;
    /** 执行服务机构 */
    private String ExcuteServiceHospit;
    /** 执行服务人员 */
    private String ExcuteServiceDocNO;
    /** 执行不相符原因 */
    private String ExNoCompareCause;
    /** 实际支付费用 */
    private double ExcutePay;
    /** 客户满意度 */
    private String SatisfactionDegree;
    /** 其他服务信息 */
    private String OtherServiceInfo;
    /** 取消服务原因 */
    private String CancleServiceCause;
    /** 服务人员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 预约是否相符 */
    private String IsBookSame;
    /** 服务执行是否相符 */
    private String IsExecSame;
    /** 管理机构 */
    private String ManageCom;
    /** 预约剩余时间 */
    private String BespealLeaveTime;

    public static final int FIELDNUM = 43; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHMedicalServiceRecordSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "RecordNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHMedicalServiceRecordSchema cloned = (LHMedicalServiceRecordSchema)super.
                                              clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getRecordNo() {
        return RecordNo;
    }

    public void setRecordNo(String aRecordNo) {
        RecordNo = aRecordNo;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getFirstRecoDate() {
        if (FirstRecoDate != null) {
            return fDate.getString(FirstRecoDate);
        } else {
            return null;
        }
    }

    public void setFirstRecoDate(Date aFirstRecoDate) {
        FirstRecoDate = aFirstRecoDate;
    }

    public void setFirstRecoDate(String aFirstRecoDate) {
        if (aFirstRecoDate != null && !aFirstRecoDate.equals("")) {
            FirstRecoDate = fDate.getDate(aFirstRecoDate);
        } else {
            FirstRecoDate = null;
        }
    }

    public String getFirstRecoTime() {
        return FirstRecoTime;
    }

    public void setFirstRecoTime(String aFirstRecoTime) {
        FirstRecoTime = aFirstRecoTime;
    }

    public String getServiceState() {
        return ServiceState;
    }

    public void setServiceState(String aServiceState) {
        ServiceState = aServiceState;
    }

    public String getApplyDutyItemCode() {
        return ApplyDutyItemCode;
    }

    public void setApplyDutyItemCode(String aApplyDutyItemCode) {
        ApplyDutyItemCode = aApplyDutyItemCode;
    }

    public String getDescriptionDetail() {
        return DescriptionDetail;
    }

    public void setDescriptionDetail(String aDescriptionDetail) {
        DescriptionDetail = aDescriptionDetail;
    }

    public String getServiceDate() {
        if (ServiceDate != null) {
            return fDate.getString(ServiceDate);
        } else {
            return null;
        }
    }

    public void setServiceDate(Date aServiceDate) {
        ServiceDate = aServiceDate;
    }

    public void setServiceDate(String aServiceDate) {
        if (aServiceDate != null && !aServiceDate.equals("")) {
            ServiceDate = fDate.getDate(aServiceDate);
        } else {
            ServiceDate = null;
        }
    }

    public String getServiceTime() {
        return ServiceTime;
    }

    public void setServiceTime(String aServiceTime) {
        ServiceTime = aServiceTime;
    }

    public String getHospitCode() {
        return HospitCode;
    }

    public void setHospitCode(String aHospitCode) {
        HospitCode = aHospitCode;
    }

    public String getDoctNo() {
        return DoctNo;
    }

    public void setDoctNo(String aDoctNo) {
        DoctNo = aDoctNo;
    }

    public String getApplyReceiveType() {
        return ApplyReceiveType;
    }

    public void setApplyReceiveType(String aApplyReceiveType) {
        ApplyReceiveType = aApplyReceiveType;
    }

    public String getServiceFlag() {
        return ServiceFlag;
    }

    public void setServiceFlag(String aServiceFlag) {
        ServiceFlag = aServiceFlag;
    }

    public String getIsBespeakOk() {
        return IsBespeakOk;
    }

    public void setIsBespeakOk(String aIsBespeakOk) {
        IsBespeakOk = aIsBespeakOk;
    }

    public String getBespeakServiceItem() {
        return BespeakServiceItem;
    }

    public void setBespeakServiceItem(String aBespeakServiceItem) {
        BespeakServiceItem = aBespeakServiceItem;
    }

    public String getApplyServiceDes() {
        return ApplyServiceDes;
    }

    public void setApplyServiceDes(String aApplyServiceDes) {
        ApplyServiceDes = aApplyServiceDes;
    }

    public String getBespeakServiceDate() {
        if (BespeakServiceDate != null) {
            return fDate.getString(BespeakServiceDate);
        } else {
            return null;
        }
    }

    public void setBespeakServiceDate(Date aBespeakServiceDate) {
        BespeakServiceDate = aBespeakServiceDate;
    }

    public void setBespeakServiceDate(String aBespeakServiceDate) {
        if (aBespeakServiceDate != null && !aBespeakServiceDate.equals("")) {
            BespeakServiceDate = fDate.getDate(aBespeakServiceDate);
        } else {
            BespeakServiceDate = null;
        }
    }

    public String getBespeakServiceTime() {
        return BespeakServiceTime;
    }

    public void setBespeakServiceTime(String aBespeakServiceTime) {
        BespeakServiceTime = aBespeakServiceTime;
    }

    public String getBespeakServiceHospit() {
        return BespeakServiceHospit;
    }

    public void setBespeakServiceHospit(String aBespeakServiceHospit) {
        BespeakServiceHospit = aBespeakServiceHospit;
    }

    public String getBespeakServiceDoctNo() {
        return BespeakServiceDoctNo;
    }

    public void setBespeakServiceDoctNo(String aBespeakServiceDoctNo) {
        BespeakServiceDoctNo = aBespeakServiceDoctNo;
    }

    public String getNoCompareCause() {
        return NoCompareCause;
    }

    public void setNoCompareCause(String aNoCompareCause) {
        NoCompareCause = aNoCompareCause;
    }

    public String getBespeaLostCause() {
        return BespeaLostCause;
    }

    public void setBespeaLostCause(String aBespeaLostCause) {
        BespeaLostCause = aBespeaLostCause;
    }

    public String getServiceExecState() {
        return ServiceExecState;
    }

    public void setServiceExecState(String aServiceExecState) {
        ServiceExecState = aServiceExecState;
    }

    public String getExcuteServiceItem() {
        return ExcuteServiceItem;
    }

    public void setExcuteServiceItem(String aExcuteServiceItem) {
        ExcuteServiceItem = aExcuteServiceItem;
    }

    public String getExcuteDescription() {
        return ExcuteDescription;
    }

    public void setExcuteDescription(String aExcuteDescription) {
        ExcuteDescription = aExcuteDescription;
    }

    public String getExcuteServiceDate() {
        if (ExcuteServiceDate != null) {
            return fDate.getString(ExcuteServiceDate);
        } else {
            return null;
        }
    }

    public void setExcuteServiceDate(Date aExcuteServiceDate) {
        ExcuteServiceDate = aExcuteServiceDate;
    }

    public void setExcuteServiceDate(String aExcuteServiceDate) {
        if (aExcuteServiceDate != null && !aExcuteServiceDate.equals("")) {
            ExcuteServiceDate = fDate.getDate(aExcuteServiceDate);
        } else {
            ExcuteServiceDate = null;
        }
    }

    public String getExcuteServiceTime() {
        return ExcuteServiceTime;
    }

    public void setExcuteServiceTime(String aExcuteServiceTime) {
        ExcuteServiceTime = aExcuteServiceTime;
    }

    public String getExcuteServiceHospit() {
        return ExcuteServiceHospit;
    }

    public void setExcuteServiceHospit(String aExcuteServiceHospit) {
        ExcuteServiceHospit = aExcuteServiceHospit;
    }

    public String getExcuteServiceDocNO() {
        return ExcuteServiceDocNO;
    }

    public void setExcuteServiceDocNO(String aExcuteServiceDocNO) {
        ExcuteServiceDocNO = aExcuteServiceDocNO;
    }

    public String getExNoCompareCause() {
        return ExNoCompareCause;
    }

    public void setExNoCompareCause(String aExNoCompareCause) {
        ExNoCompareCause = aExNoCompareCause;
    }

    public double getExcutePay() {
        return ExcutePay;
    }

    public void setExcutePay(double aExcutePay) {
        ExcutePay = Arith.round(aExcutePay, 2);
    }

    public void setExcutePay(String aExcutePay) {
        if (aExcutePay != null && !aExcutePay.equals("")) {
            Double tDouble = new Double(aExcutePay);
            double d = tDouble.doubleValue();
            ExcutePay = Arith.round(d, 2);
        }
    }

    public String getSatisfactionDegree() {
        return SatisfactionDegree;
    }

    public void setSatisfactionDegree(String aSatisfactionDegree) {
        SatisfactionDegree = aSatisfactionDegree;
    }

    public String getOtherServiceInfo() {
        return OtherServiceInfo;
    }

    public void setOtherServiceInfo(String aOtherServiceInfo) {
        OtherServiceInfo = aOtherServiceInfo;
    }

    public String getCancleServiceCause() {
        return CancleServiceCause;
    }

    public void setCancleServiceCause(String aCancleServiceCause) {
        CancleServiceCause = aCancleServiceCause;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getIsBookSame() {
        return IsBookSame;
    }

    public void setIsBookSame(String aIsBookSame) {
        IsBookSame = aIsBookSame;
    }

    public String getIsExecSame() {
        return IsExecSame;
    }

    public void setIsExecSame(String aIsExecSame) {
        IsExecSame = aIsExecSame;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getBespealLeaveTime() {
        return BespealLeaveTime;
    }

    public void setBespealLeaveTime(String aBespealLeaveTime) {
        BespealLeaveTime = aBespealLeaveTime;
    }

    /**
     * 使用另外一个 LHMedicalServiceRecordSchema 对象给 Schema 赋值
     * @param: aLHMedicalServiceRecordSchema LHMedicalServiceRecordSchema
     **/
    public void setSchema(LHMedicalServiceRecordSchema
                          aLHMedicalServiceRecordSchema) {
        this.RecordNo = aLHMedicalServiceRecordSchema.getRecordNo();
        this.CustomerNo = aLHMedicalServiceRecordSchema.getCustomerNo();
        this.FirstRecoDate = fDate.getDate(aLHMedicalServiceRecordSchema.
                                           getFirstRecoDate());
        this.FirstRecoTime = aLHMedicalServiceRecordSchema.getFirstRecoTime();
        this.ServiceState = aLHMedicalServiceRecordSchema.getServiceState();
        this.ApplyDutyItemCode = aLHMedicalServiceRecordSchema.
                                 getApplyDutyItemCode();
        this.DescriptionDetail = aLHMedicalServiceRecordSchema.
                                 getDescriptionDetail();
        this.ServiceDate = fDate.getDate(aLHMedicalServiceRecordSchema.
                                         getServiceDate());
        this.ServiceTime = aLHMedicalServiceRecordSchema.getServiceTime();
        this.HospitCode = aLHMedicalServiceRecordSchema.getHospitCode();
        this.DoctNo = aLHMedicalServiceRecordSchema.getDoctNo();
        this.ApplyReceiveType = aLHMedicalServiceRecordSchema.
                                getApplyReceiveType();
        this.ServiceFlag = aLHMedicalServiceRecordSchema.getServiceFlag();
        this.IsBespeakOk = aLHMedicalServiceRecordSchema.getIsBespeakOk();
        this.BespeakServiceItem = aLHMedicalServiceRecordSchema.
                                  getBespeakServiceItem();
        this.ApplyServiceDes = aLHMedicalServiceRecordSchema.getApplyServiceDes();
        this.BespeakServiceDate = fDate.getDate(aLHMedicalServiceRecordSchema.
                                                getBespeakServiceDate());
        this.BespeakServiceTime = aLHMedicalServiceRecordSchema.
                                  getBespeakServiceTime();
        this.BespeakServiceHospit = aLHMedicalServiceRecordSchema.
                                    getBespeakServiceHospit();
        this.BespeakServiceDoctNo = aLHMedicalServiceRecordSchema.
                                    getBespeakServiceDoctNo();
        this.NoCompareCause = aLHMedicalServiceRecordSchema.getNoCompareCause();
        this.BespeaLostCause = aLHMedicalServiceRecordSchema.getBespeaLostCause();
        this.ServiceExecState = aLHMedicalServiceRecordSchema.
                                getServiceExecState();
        this.ExcuteServiceItem = aLHMedicalServiceRecordSchema.
                                 getExcuteServiceItem();
        this.ExcuteDescription = aLHMedicalServiceRecordSchema.
                                 getExcuteDescription();
        this.ExcuteServiceDate = fDate.getDate(aLHMedicalServiceRecordSchema.
                                               getExcuteServiceDate());
        this.ExcuteServiceTime = aLHMedicalServiceRecordSchema.
                                 getExcuteServiceTime();
        this.ExcuteServiceHospit = aLHMedicalServiceRecordSchema.
                                   getExcuteServiceHospit();
        this.ExcuteServiceDocNO = aLHMedicalServiceRecordSchema.
                                  getExcuteServiceDocNO();
        this.ExNoCompareCause = aLHMedicalServiceRecordSchema.
                                getExNoCompareCause();
        this.ExcutePay = aLHMedicalServiceRecordSchema.getExcutePay();
        this.SatisfactionDegree = aLHMedicalServiceRecordSchema.
                                  getSatisfactionDegree();
        this.OtherServiceInfo = aLHMedicalServiceRecordSchema.
                                getOtherServiceInfo();
        this.CancleServiceCause = aLHMedicalServiceRecordSchema.
                                  getCancleServiceCause();
        this.Operator = aLHMedicalServiceRecordSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHMedicalServiceRecordSchema.getMakeDate());
        this.MakeTime = aLHMedicalServiceRecordSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHMedicalServiceRecordSchema.
                                        getModifyDate());
        this.ModifyTime = aLHMedicalServiceRecordSchema.getModifyTime();
        this.IsBookSame = aLHMedicalServiceRecordSchema.getIsBookSame();
        this.IsExecSame = aLHMedicalServiceRecordSchema.getIsExecSame();
        this.ManageCom = aLHMedicalServiceRecordSchema.getManageCom();
        this.BespealLeaveTime = aLHMedicalServiceRecordSchema.
                                getBespealLeaveTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RecordNo") == null) {
                this.RecordNo = null;
            } else {
                this.RecordNo = rs.getString("RecordNo").trim();
            }

            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            this.FirstRecoDate = rs.getDate("FirstRecoDate");
            if (rs.getString("FirstRecoTime") == null) {
                this.FirstRecoTime = null;
            } else {
                this.FirstRecoTime = rs.getString("FirstRecoTime").trim();
            }

            if (rs.getString("ServiceState") == null) {
                this.ServiceState = null;
            } else {
                this.ServiceState = rs.getString("ServiceState").trim();
            }

            if (rs.getString("ApplyDutyItemCode") == null) {
                this.ApplyDutyItemCode = null;
            } else {
                this.ApplyDutyItemCode = rs.getString("ApplyDutyItemCode").trim();
            }

            if (rs.getString("DescriptionDetail") == null) {
                this.DescriptionDetail = null;
            } else {
                this.DescriptionDetail = rs.getString("DescriptionDetail").trim();
            }

            this.ServiceDate = rs.getDate("ServiceDate");
            if (rs.getString("ServiceTime") == null) {
                this.ServiceTime = null;
            } else {
                this.ServiceTime = rs.getString("ServiceTime").trim();
            }

            if (rs.getString("HospitCode") == null) {
                this.HospitCode = null;
            } else {
                this.HospitCode = rs.getString("HospitCode").trim();
            }

            if (rs.getString("DoctNo") == null) {
                this.DoctNo = null;
            } else {
                this.DoctNo = rs.getString("DoctNo").trim();
            }

            if (rs.getString("ApplyReceiveType") == null) {
                this.ApplyReceiveType = null;
            } else {
                this.ApplyReceiveType = rs.getString("ApplyReceiveType").trim();
            }

            if (rs.getString("ServiceFlag") == null) {
                this.ServiceFlag = null;
            } else {
                this.ServiceFlag = rs.getString("ServiceFlag").trim();
            }

            if (rs.getString("IsBespeakOk") == null) {
                this.IsBespeakOk = null;
            } else {
                this.IsBespeakOk = rs.getString("IsBespeakOk").trim();
            }

            if (rs.getString("BespeakServiceItem") == null) {
                this.BespeakServiceItem = null;
            } else {
                this.BespeakServiceItem = rs.getString("BespeakServiceItem").
                                          trim();
            }

            if (rs.getString("ApplyServiceDes") == null) {
                this.ApplyServiceDes = null;
            } else {
                this.ApplyServiceDes = rs.getString("ApplyServiceDes").trim();
            }

            this.BespeakServiceDate = rs.getDate("BespeakServiceDate");
            if (rs.getString("BespeakServiceTime") == null) {
                this.BespeakServiceTime = null;
            } else {
                this.BespeakServiceTime = rs.getString("BespeakServiceTime").
                                          trim();
            }

            if (rs.getString("BespeakServiceHospit") == null) {
                this.BespeakServiceHospit = null;
            } else {
                this.BespeakServiceHospit = rs.getString("BespeakServiceHospit").
                                            trim();
            }

            if (rs.getString("BespeakServiceDoctNo") == null) {
                this.BespeakServiceDoctNo = null;
            } else {
                this.BespeakServiceDoctNo = rs.getString("BespeakServiceDoctNo").
                                            trim();
            }

            if (rs.getString("NoCompareCause") == null) {
                this.NoCompareCause = null;
            } else {
                this.NoCompareCause = rs.getString("NoCompareCause").trim();
            }

            if (rs.getString("BespeaLostCause") == null) {
                this.BespeaLostCause = null;
            } else {
                this.BespeaLostCause = rs.getString("BespeaLostCause").trim();
            }

            if (rs.getString("ServiceExecState") == null) {
                this.ServiceExecState = null;
            } else {
                this.ServiceExecState = rs.getString("ServiceExecState").trim();
            }

            if (rs.getString("ExcuteServiceItem") == null) {
                this.ExcuteServiceItem = null;
            } else {
                this.ExcuteServiceItem = rs.getString("ExcuteServiceItem").trim();
            }

            if (rs.getString("ExcuteDescription") == null) {
                this.ExcuteDescription = null;
            } else {
                this.ExcuteDescription = rs.getString("ExcuteDescription").trim();
            }

            this.ExcuteServiceDate = rs.getDate("ExcuteServiceDate");
            if (rs.getString("ExcuteServiceTime") == null) {
                this.ExcuteServiceTime = null;
            } else {
                this.ExcuteServiceTime = rs.getString("ExcuteServiceTime").trim();
            }

            if (rs.getString("ExcuteServiceHospit") == null) {
                this.ExcuteServiceHospit = null;
            } else {
                this.ExcuteServiceHospit = rs.getString("ExcuteServiceHospit").
                                           trim();
            }

            if (rs.getString("ExcuteServiceDocNO") == null) {
                this.ExcuteServiceDocNO = null;
            } else {
                this.ExcuteServiceDocNO = rs.getString("ExcuteServiceDocNO").
                                          trim();
            }

            if (rs.getString("ExNoCompareCause") == null) {
                this.ExNoCompareCause = null;
            } else {
                this.ExNoCompareCause = rs.getString("ExNoCompareCause").trim();
            }

            this.ExcutePay = rs.getDouble("ExcutePay");
            if (rs.getString("SatisfactionDegree") == null) {
                this.SatisfactionDegree = null;
            } else {
                this.SatisfactionDegree = rs.getString("SatisfactionDegree").
                                          trim();
            }

            if (rs.getString("OtherServiceInfo") == null) {
                this.OtherServiceInfo = null;
            } else {
                this.OtherServiceInfo = rs.getString("OtherServiceInfo").trim();
            }

            if (rs.getString("CancleServiceCause") == null) {
                this.CancleServiceCause = null;
            } else {
                this.CancleServiceCause = rs.getString("CancleServiceCause").
                                          trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("IsBookSame") == null) {
                this.IsBookSame = null;
            } else {
                this.IsBookSame = rs.getString("IsBookSame").trim();
            }

            if (rs.getString("IsExecSame") == null) {
                this.IsExecSame = null;
            } else {
                this.IsExecSame = rs.getString("IsExecSame").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("BespealLeaveTime") == null) {
                this.BespealLeaveTime = null;
            } else {
                this.BespealLeaveTime = rs.getString("BespealLeaveTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHMedicalServiceRecord表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHMedicalServiceRecordSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHMedicalServiceRecordSchema getSchema() {
        LHMedicalServiceRecordSchema aLHMedicalServiceRecordSchema = new
                LHMedicalServiceRecordSchema();
        aLHMedicalServiceRecordSchema.setSchema(this);
        return aLHMedicalServiceRecordSchema;
    }

    public LHMedicalServiceRecordDB getDB() {
        LHMedicalServiceRecordDB aDBOper = new LHMedicalServiceRecordDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHMedicalServiceRecord描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(RecordNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(FirstRecoDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FirstRecoTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServiceState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApplyDutyItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DescriptionDetail));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ServiceDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServiceTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(HospitCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DoctNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApplyReceiveType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServiceFlag));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsBespeakOk));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BespeakServiceItem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ApplyServiceDes));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(BespeakServiceDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BespeakServiceTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BespeakServiceHospit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BespeakServiceDoctNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NoCompareCause));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BespeaLostCause));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServiceExecState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExcuteServiceItem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExcuteDescription));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ExcuteServiceDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExcuteServiceTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExcuteServiceHospit));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExcuteServiceDocNO));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExNoCompareCause));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ExcutePay));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SatisfactionDegree));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherServiceInfo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CancleServiceCause));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsBookSame));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(IsExecSame));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BespealLeaveTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHMedicalServiceRecord>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            RecordNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            FirstRecoDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 3, SysConst.PACKAGESPILTER));
            FirstRecoTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                           SysConst.PACKAGESPILTER);
            ServiceState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            ApplyDutyItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               6, SysConst.PACKAGESPILTER);
            DescriptionDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               7, SysConst.PACKAGESPILTER);
            ServiceDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            ServiceTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            HospitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            DoctNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                    SysConst.PACKAGESPILTER);
            ApplyReceiveType = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              12, SysConst.PACKAGESPILTER);
            ServiceFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                         SysConst.PACKAGESPILTER);
            IsBespeakOk = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                         SysConst.PACKAGESPILTER);
            BespeakServiceItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                15, SysConst.PACKAGESPILTER);
            ApplyServiceDes = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             16, SysConst.PACKAGESPILTER);
            BespeakServiceDate = fDate.getDate(StrTool.getStr(StrTool.
                    GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER));
            BespeakServiceTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                18, SysConst.PACKAGESPILTER);
            BespeakServiceHospit = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 19, SysConst.PACKAGESPILTER);
            BespeakServiceDoctNo = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER);
            NoCompareCause = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            21, SysConst.PACKAGESPILTER);
            BespeaLostCause = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             22, SysConst.PACKAGESPILTER);
            ServiceExecState = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              23, SysConst.PACKAGESPILTER);
            ExcuteServiceItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               24, SysConst.PACKAGESPILTER);
            ExcuteDescription = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               25, SysConst.PACKAGESPILTER);
            ExcuteServiceDate = fDate.getDate(StrTool.getStr(StrTool.
                    GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER));
            ExcuteServiceTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               27, SysConst.PACKAGESPILTER);
            ExcuteServiceHospit = StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 28, SysConst.PACKAGESPILTER);
            ExcuteServiceDocNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                29, SysConst.PACKAGESPILTER);
            ExNoCompareCause = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              30, SysConst.PACKAGESPILTER);
            ExcutePay = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 31, SysConst.PACKAGESPILTER))).doubleValue();
            SatisfactionDegree = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                32, SysConst.PACKAGESPILTER);
            OtherServiceInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              33, SysConst.PACKAGESPILTER);
            CancleServiceCause = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                                34, SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 36, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 38, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39,
                                        SysConst.PACKAGESPILTER);
            IsBookSame = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,
                                        SysConst.PACKAGESPILTER);
            IsExecSame = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,
                                       SysConst.PACKAGESPILTER);
            BespealLeaveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              43, SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHMedicalServiceRecordSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("RecordNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecordNo));
        }
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("FirstRecoDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getFirstRecoDate()));
        }
        if (FCode.equals("FirstRecoTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FirstRecoTime));
        }
        if (FCode.equals("ServiceState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceState));
        }
        if (FCode.equals("ApplyDutyItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyDutyItemCode));
        }
        if (FCode.equals("DescriptionDetail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DescriptionDetail));
        }
        if (FCode.equals("ServiceDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getServiceDate()));
        }
        if (FCode.equals("ServiceTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceTime));
        }
        if (FCode.equals("HospitCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitCode));
        }
        if (FCode.equals("DoctNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoctNo));
        }
        if (FCode.equals("ApplyReceiveType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyReceiveType));
        }
        if (FCode.equals("ServiceFlag")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceFlag));
        }
        if (FCode.equals("IsBespeakOk")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsBespeakOk));
        }
        if (FCode.equals("BespeakServiceItem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BespeakServiceItem));
        }
        if (FCode.equals("ApplyServiceDes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyServiceDes));
        }
        if (FCode.equals("BespeakServiceDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getBespeakServiceDate()));
        }
        if (FCode.equals("BespeakServiceTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BespeakServiceTime));
        }
        if (FCode.equals("BespeakServiceHospit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(
                    BespeakServiceHospit));
        }
        if (FCode.equals("BespeakServiceDoctNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(
                    BespeakServiceDoctNo));
        }
        if (FCode.equals("NoCompareCause")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NoCompareCause));
        }
        if (FCode.equals("BespeaLostCause")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BespeaLostCause));
        }
        if (FCode.equals("ServiceExecState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceExecState));
        }
        if (FCode.equals("ExcuteServiceItem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExcuteServiceItem));
        }
        if (FCode.equals("ExcuteDescription")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExcuteDescription));
        }
        if (FCode.equals("ExcuteServiceDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getExcuteServiceDate()));
        }
        if (FCode.equals("ExcuteServiceTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExcuteServiceTime));
        }
        if (FCode.equals("ExcuteServiceHospit")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExcuteServiceHospit));
        }
        if (FCode.equals("ExcuteServiceDocNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExcuteServiceDocNO));
        }
        if (FCode.equals("ExNoCompareCause")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExNoCompareCause));
        }
        if (FCode.equals("ExcutePay")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExcutePay));
        }
        if (FCode.equals("SatisfactionDegree")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SatisfactionDegree));
        }
        if (FCode.equals("OtherServiceInfo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherServiceInfo));
        }
        if (FCode.equals("CancleServiceCause")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CancleServiceCause));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("IsBookSame")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsBookSame));
        }
        if (FCode.equals("IsExecSame")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IsExecSame));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("BespealLeaveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BespealLeaveTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(RecordNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getFirstRecoDate()));
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(FirstRecoTime);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ServiceState);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ApplyDutyItemCode);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(DescriptionDetail);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getServiceDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ServiceTime);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(HospitCode);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(DoctNo);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ApplyReceiveType);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ServiceFlag);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(IsBespeakOk);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(BespeakServiceItem);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(ApplyServiceDes);
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getBespeakServiceDate()));
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(BespeakServiceTime);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(BespeakServiceHospit);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(BespeakServiceDoctNo);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(NoCompareCause);
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(BespeaLostCause);
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(ServiceExecState);
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(ExcuteServiceItem);
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(ExcuteDescription);
            break;
        case 25:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getExcuteServiceDate()));
            break;
        case 26:
            strFieldValue = StrTool.GBKToUnicode(ExcuteServiceTime);
            break;
        case 27:
            strFieldValue = StrTool.GBKToUnicode(ExcuteServiceHospit);
            break;
        case 28:
            strFieldValue = StrTool.GBKToUnicode(ExcuteServiceDocNO);
            break;
        case 29:
            strFieldValue = StrTool.GBKToUnicode(ExNoCompareCause);
            break;
        case 30:
            strFieldValue = String.valueOf(ExcutePay);
            break;
        case 31:
            strFieldValue = StrTool.GBKToUnicode(SatisfactionDegree);
            break;
        case 32:
            strFieldValue = StrTool.GBKToUnicode(OtherServiceInfo);
            break;
        case 33:
            strFieldValue = StrTool.GBKToUnicode(CancleServiceCause);
            break;
        case 34:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 35:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 36:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 37:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 38:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 39:
            strFieldValue = StrTool.GBKToUnicode(IsBookSame);
            break;
        case 40:
            strFieldValue = StrTool.GBKToUnicode(IsExecSame);
            break;
        case 41:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 42:
            strFieldValue = StrTool.GBKToUnicode(BespealLeaveTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("RecordNo")) {
            if (FValue != null && !FValue.equals("")) {
                RecordNo = FValue.trim();
            } else {
                RecordNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("FirstRecoDate")) {
            if (FValue != null && !FValue.equals("")) {
                FirstRecoDate = fDate.getDate(FValue);
            } else {
                FirstRecoDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("FirstRecoTime")) {
            if (FValue != null && !FValue.equals("")) {
                FirstRecoTime = FValue.trim();
            } else {
                FirstRecoTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServiceState")) {
            if (FValue != null && !FValue.equals("")) {
                ServiceState = FValue.trim();
            } else {
                ServiceState = null;
            }
        }
        if (FCode.equalsIgnoreCase("ApplyDutyItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                ApplyDutyItemCode = FValue.trim();
            } else {
                ApplyDutyItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("DescriptionDetail")) {
            if (FValue != null && !FValue.equals("")) {
                DescriptionDetail = FValue.trim();
            } else {
                DescriptionDetail = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServiceDate")) {
            if (FValue != null && !FValue.equals("")) {
                ServiceDate = fDate.getDate(FValue);
            } else {
                ServiceDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServiceTime")) {
            if (FValue != null && !FValue.equals("")) {
                ServiceTime = FValue.trim();
            } else {
                ServiceTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("HospitCode")) {
            if (FValue != null && !FValue.equals("")) {
                HospitCode = FValue.trim();
            } else {
                HospitCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("DoctNo")) {
            if (FValue != null && !FValue.equals("")) {
                DoctNo = FValue.trim();
            } else {
                DoctNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ApplyReceiveType")) {
            if (FValue != null && !FValue.equals("")) {
                ApplyReceiveType = FValue.trim();
            } else {
                ApplyReceiveType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServiceFlag")) {
            if (FValue != null && !FValue.equals("")) {
                ServiceFlag = FValue.trim();
            } else {
                ServiceFlag = null;
            }
        }
        if (FCode.equalsIgnoreCase("IsBespeakOk")) {
            if (FValue != null && !FValue.equals("")) {
                IsBespeakOk = FValue.trim();
            } else {
                IsBespeakOk = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespeakServiceItem")) {
            if (FValue != null && !FValue.equals("")) {
                BespeakServiceItem = FValue.trim();
            } else {
                BespeakServiceItem = null;
            }
        }
        if (FCode.equalsIgnoreCase("ApplyServiceDes")) {
            if (FValue != null && !FValue.equals("")) {
                ApplyServiceDes = FValue.trim();
            } else {
                ApplyServiceDes = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespeakServiceDate")) {
            if (FValue != null && !FValue.equals("")) {
                BespeakServiceDate = fDate.getDate(FValue);
            } else {
                BespeakServiceDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespeakServiceTime")) {
            if (FValue != null && !FValue.equals("")) {
                BespeakServiceTime = FValue.trim();
            } else {
                BespeakServiceTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespeakServiceHospit")) {
            if (FValue != null && !FValue.equals("")) {
                BespeakServiceHospit = FValue.trim();
            } else {
                BespeakServiceHospit = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespeakServiceDoctNo")) {
            if (FValue != null && !FValue.equals("")) {
                BespeakServiceDoctNo = FValue.trim();
            } else {
                BespeakServiceDoctNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("NoCompareCause")) {
            if (FValue != null && !FValue.equals("")) {
                NoCompareCause = FValue.trim();
            } else {
                NoCompareCause = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespeaLostCause")) {
            if (FValue != null && !FValue.equals("")) {
                BespeaLostCause = FValue.trim();
            } else {
                BespeaLostCause = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServiceExecState")) {
            if (FValue != null && !FValue.equals("")) {
                ServiceExecState = FValue.trim();
            } else {
                ServiceExecState = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExcuteServiceItem")) {
            if (FValue != null && !FValue.equals("")) {
                ExcuteServiceItem = FValue.trim();
            } else {
                ExcuteServiceItem = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExcuteDescription")) {
            if (FValue != null && !FValue.equals("")) {
                ExcuteDescription = FValue.trim();
            } else {
                ExcuteDescription = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExcuteServiceDate")) {
            if (FValue != null && !FValue.equals("")) {
                ExcuteServiceDate = fDate.getDate(FValue);
            } else {
                ExcuteServiceDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExcuteServiceTime")) {
            if (FValue != null && !FValue.equals("")) {
                ExcuteServiceTime = FValue.trim();
            } else {
                ExcuteServiceTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExcuteServiceHospit")) {
            if (FValue != null && !FValue.equals("")) {
                ExcuteServiceHospit = FValue.trim();
            } else {
                ExcuteServiceHospit = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExcuteServiceDocNO")) {
            if (FValue != null && !FValue.equals("")) {
                ExcuteServiceDocNO = FValue.trim();
            } else {
                ExcuteServiceDocNO = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExNoCompareCause")) {
            if (FValue != null && !FValue.equals("")) {
                ExNoCompareCause = FValue.trim();
            } else {
                ExNoCompareCause = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExcutePay")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ExcutePay = d;
            }
        }
        if (FCode.equalsIgnoreCase("SatisfactionDegree")) {
            if (FValue != null && !FValue.equals("")) {
                SatisfactionDegree = FValue.trim();
            } else {
                SatisfactionDegree = null;
            }
        }
        if (FCode.equalsIgnoreCase("OtherServiceInfo")) {
            if (FValue != null && !FValue.equals("")) {
                OtherServiceInfo = FValue.trim();
            } else {
                OtherServiceInfo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CancleServiceCause")) {
            if (FValue != null && !FValue.equals("")) {
                CancleServiceCause = FValue.trim();
            } else {
                CancleServiceCause = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("IsBookSame")) {
            if (FValue != null && !FValue.equals("")) {
                IsBookSame = FValue.trim();
            } else {
                IsBookSame = null;
            }
        }
        if (FCode.equalsIgnoreCase("IsExecSame")) {
            if (FValue != null && !FValue.equals("")) {
                IsExecSame = FValue.trim();
            } else {
                IsExecSame = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("BespealLeaveTime")) {
            if (FValue != null && !FValue.equals("")) {
                BespealLeaveTime = FValue.trim();
            } else {
                BespealLeaveTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHMedicalServiceRecordSchema other = (LHMedicalServiceRecordSchema)
                                             otherObject;
        return
                RecordNo.equals(other.getRecordNo())
                && CustomerNo.equals(other.getCustomerNo())
                && fDate.getString(FirstRecoDate).equals(other.getFirstRecoDate())
                && FirstRecoTime.equals(other.getFirstRecoTime())
                && ServiceState.equals(other.getServiceState())
                && ApplyDutyItemCode.equals(other.getApplyDutyItemCode())
                && DescriptionDetail.equals(other.getDescriptionDetail())
                && fDate.getString(ServiceDate).equals(other.getServiceDate())
                && ServiceTime.equals(other.getServiceTime())
                && HospitCode.equals(other.getHospitCode())
                && DoctNo.equals(other.getDoctNo())
                && ApplyReceiveType.equals(other.getApplyReceiveType())
                && ServiceFlag.equals(other.getServiceFlag())
                && IsBespeakOk.equals(other.getIsBespeakOk())
                && BespeakServiceItem.equals(other.getBespeakServiceItem())
                && ApplyServiceDes.equals(other.getApplyServiceDes())
                &&
                fDate.getString(BespeakServiceDate).equals(other.getBespeakServiceDate())
                && BespeakServiceTime.equals(other.getBespeakServiceTime())
                && BespeakServiceHospit.equals(other.getBespeakServiceHospit())
                && BespeakServiceDoctNo.equals(other.getBespeakServiceDoctNo())
                && NoCompareCause.equals(other.getNoCompareCause())
                && BespeaLostCause.equals(other.getBespeaLostCause())
                && ServiceExecState.equals(other.getServiceExecState())
                && ExcuteServiceItem.equals(other.getExcuteServiceItem())
                && ExcuteDescription.equals(other.getExcuteDescription())
                &&
                fDate.getString(ExcuteServiceDate).equals(other.getExcuteServiceDate())
                && ExcuteServiceTime.equals(other.getExcuteServiceTime())
                && ExcuteServiceHospit.equals(other.getExcuteServiceHospit())
                && ExcuteServiceDocNO.equals(other.getExcuteServiceDocNO())
                && ExNoCompareCause.equals(other.getExNoCompareCause())
                && ExcutePay == other.getExcutePay()
                && SatisfactionDegree.equals(other.getSatisfactionDegree())
                && OtherServiceInfo.equals(other.getOtherServiceInfo())
                && CancleServiceCause.equals(other.getCancleServiceCause())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && IsBookSame.equals(other.getIsBookSame())
                && IsExecSame.equals(other.getIsExecSame())
                && ManageCom.equals(other.getManageCom())
                && BespealLeaveTime.equals(other.getBespealLeaveTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("RecordNo")) {
            return 0;
        }
        if (strFieldName.equals("CustomerNo")) {
            return 1;
        }
        if (strFieldName.equals("FirstRecoDate")) {
            return 2;
        }
        if (strFieldName.equals("FirstRecoTime")) {
            return 3;
        }
        if (strFieldName.equals("ServiceState")) {
            return 4;
        }
        if (strFieldName.equals("ApplyDutyItemCode")) {
            return 5;
        }
        if (strFieldName.equals("DescriptionDetail")) {
            return 6;
        }
        if (strFieldName.equals("ServiceDate")) {
            return 7;
        }
        if (strFieldName.equals("ServiceTime")) {
            return 8;
        }
        if (strFieldName.equals("HospitCode")) {
            return 9;
        }
        if (strFieldName.equals("DoctNo")) {
            return 10;
        }
        if (strFieldName.equals("ApplyReceiveType")) {
            return 11;
        }
        if (strFieldName.equals("ServiceFlag")) {
            return 12;
        }
        if (strFieldName.equals("IsBespeakOk")) {
            return 13;
        }
        if (strFieldName.equals("BespeakServiceItem")) {
            return 14;
        }
        if (strFieldName.equals("ApplyServiceDes")) {
            return 15;
        }
        if (strFieldName.equals("BespeakServiceDate")) {
            return 16;
        }
        if (strFieldName.equals("BespeakServiceTime")) {
            return 17;
        }
        if (strFieldName.equals("BespeakServiceHospit")) {
            return 18;
        }
        if (strFieldName.equals("BespeakServiceDoctNo")) {
            return 19;
        }
        if (strFieldName.equals("NoCompareCause")) {
            return 20;
        }
        if (strFieldName.equals("BespeaLostCause")) {
            return 21;
        }
        if (strFieldName.equals("ServiceExecState")) {
            return 22;
        }
        if (strFieldName.equals("ExcuteServiceItem")) {
            return 23;
        }
        if (strFieldName.equals("ExcuteDescription")) {
            return 24;
        }
        if (strFieldName.equals("ExcuteServiceDate")) {
            return 25;
        }
        if (strFieldName.equals("ExcuteServiceTime")) {
            return 26;
        }
        if (strFieldName.equals("ExcuteServiceHospit")) {
            return 27;
        }
        if (strFieldName.equals("ExcuteServiceDocNO")) {
            return 28;
        }
        if (strFieldName.equals("ExNoCompareCause")) {
            return 29;
        }
        if (strFieldName.equals("ExcutePay")) {
            return 30;
        }
        if (strFieldName.equals("SatisfactionDegree")) {
            return 31;
        }
        if (strFieldName.equals("OtherServiceInfo")) {
            return 32;
        }
        if (strFieldName.equals("CancleServiceCause")) {
            return 33;
        }
        if (strFieldName.equals("Operator")) {
            return 34;
        }
        if (strFieldName.equals("MakeDate")) {
            return 35;
        }
        if (strFieldName.equals("MakeTime")) {
            return 36;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 37;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 38;
        }
        if (strFieldName.equals("IsBookSame")) {
            return 39;
        }
        if (strFieldName.equals("IsExecSame")) {
            return 40;
        }
        if (strFieldName.equals("ManageCom")) {
            return 41;
        }
        if (strFieldName.equals("BespealLeaveTime")) {
            return 42;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "RecordNo";
            break;
        case 1:
            strFieldName = "CustomerNo";
            break;
        case 2:
            strFieldName = "FirstRecoDate";
            break;
        case 3:
            strFieldName = "FirstRecoTime";
            break;
        case 4:
            strFieldName = "ServiceState";
            break;
        case 5:
            strFieldName = "ApplyDutyItemCode";
            break;
        case 6:
            strFieldName = "DescriptionDetail";
            break;
        case 7:
            strFieldName = "ServiceDate";
            break;
        case 8:
            strFieldName = "ServiceTime";
            break;
        case 9:
            strFieldName = "HospitCode";
            break;
        case 10:
            strFieldName = "DoctNo";
            break;
        case 11:
            strFieldName = "ApplyReceiveType";
            break;
        case 12:
            strFieldName = "ServiceFlag";
            break;
        case 13:
            strFieldName = "IsBespeakOk";
            break;
        case 14:
            strFieldName = "BespeakServiceItem";
            break;
        case 15:
            strFieldName = "ApplyServiceDes";
            break;
        case 16:
            strFieldName = "BespeakServiceDate";
            break;
        case 17:
            strFieldName = "BespeakServiceTime";
            break;
        case 18:
            strFieldName = "BespeakServiceHospit";
            break;
        case 19:
            strFieldName = "BespeakServiceDoctNo";
            break;
        case 20:
            strFieldName = "NoCompareCause";
            break;
        case 21:
            strFieldName = "BespeaLostCause";
            break;
        case 22:
            strFieldName = "ServiceExecState";
            break;
        case 23:
            strFieldName = "ExcuteServiceItem";
            break;
        case 24:
            strFieldName = "ExcuteDescription";
            break;
        case 25:
            strFieldName = "ExcuteServiceDate";
            break;
        case 26:
            strFieldName = "ExcuteServiceTime";
            break;
        case 27:
            strFieldName = "ExcuteServiceHospit";
            break;
        case 28:
            strFieldName = "ExcuteServiceDocNO";
            break;
        case 29:
            strFieldName = "ExNoCompareCause";
            break;
        case 30:
            strFieldName = "ExcutePay";
            break;
        case 31:
            strFieldName = "SatisfactionDegree";
            break;
        case 32:
            strFieldName = "OtherServiceInfo";
            break;
        case 33:
            strFieldName = "CancleServiceCause";
            break;
        case 34:
            strFieldName = "Operator";
            break;
        case 35:
            strFieldName = "MakeDate";
            break;
        case 36:
            strFieldName = "MakeTime";
            break;
        case 37:
            strFieldName = "ModifyDate";
            break;
        case 38:
            strFieldName = "ModifyTime";
            break;
        case 39:
            strFieldName = "IsBookSame";
            break;
        case 40:
            strFieldName = "IsExecSame";
            break;
        case 41:
            strFieldName = "ManageCom";
            break;
        case 42:
            strFieldName = "BespealLeaveTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("RecordNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FirstRecoDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("FirstRecoTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServiceState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApplyDutyItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DescriptionDetail")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServiceDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ServiceTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HospitCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DoctNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApplyReceiveType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServiceFlag")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IsBespeakOk")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BespeakServiceItem")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ApplyServiceDes")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BespeakServiceDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("BespeakServiceTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BespeakServiceHospit")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BespeakServiceDoctNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NoCompareCause")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BespeaLostCause")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServiceExecState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExcuteServiceItem")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExcuteDescription")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExcuteServiceDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ExcuteServiceTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExcuteServiceHospit")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExcuteServiceDocNO")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExNoCompareCause")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExcutePay")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("SatisfactionDegree")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherServiceInfo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CancleServiceCause")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IsBookSame")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IsExecSame")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BespealLeaveTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 21:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 22:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 23:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 24:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 25:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 26:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 27:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 28:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 29:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 30:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 31:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 32:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 33:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 34:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 35:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 36:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 37:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 38:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 39:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 40:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 41:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 42:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
