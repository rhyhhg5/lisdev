/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.feedbackmindDB;

/*
 * <p>ClassName: feedbackmindSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 数据运维
 * @CreateDate：2011-02-23
 */
public class feedbackmindSchema implements Schema, Cloneable
{
	// @Field
	/** 反馈号 */
	private String FeedBackId;
	/** 反馈模块 */
	private String FeedBackModule;
	/** 模块负责人 */
	private String Principal;
	/** 回复日期 */
	private Date RestoreDate;
	/** 反馈意见 */
	private String Attitude;
	/** 回应状态 */
	private String State;

	public static final int FIELDNUM = 6;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public feedbackmindSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "FeedBackId";
		pk[1] = "FeedBackModule";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		feedbackmindSchema cloned = (feedbackmindSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getFeedBackId()
	{
		return FeedBackId;
	}
	public void setFeedBackId(String aFeedBackId)
	{
		FeedBackId = aFeedBackId;
	}
	public String getFeedBackModule()
	{
		return FeedBackModule;
	}
	public void setFeedBackModule(String aFeedBackModule)
	{
		FeedBackModule = aFeedBackModule;
	}
	public String getPrincipal()
	{
		return Principal;
	}
	public void setPrincipal(String aPrincipal)
	{
		Principal = aPrincipal;
	}
	public String getRestoreDate()
	{
		if( RestoreDate != null )
			return fDate.getString(RestoreDate);
		else
			return null;
	}
	public void setRestoreDate(Date aRestoreDate)
	{
		RestoreDate = aRestoreDate;
	}
	public void setRestoreDate(String aRestoreDate)
	{
		if (aRestoreDate != null && !aRestoreDate.equals("") )
		{
			RestoreDate = fDate.getDate( aRestoreDate );
		}
		else
			RestoreDate = null;
	}

	public String getAttitude()
	{
		return Attitude;
	}
	public void setAttitude(String aAttitude)
	{
		Attitude = aAttitude;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}

	/**
	* 使用另外一个 feedbackmindSchema 对象给 Schema 赋值
	* @param: afeedbackmindSchema feedbackmindSchema
	**/
	public void setSchema(feedbackmindSchema afeedbackmindSchema)
	{
		this.FeedBackId = afeedbackmindSchema.getFeedBackId();
		this.FeedBackModule = afeedbackmindSchema.getFeedBackModule();
		this.Principal = afeedbackmindSchema.getPrincipal();
		this.RestoreDate = fDate.getDate( afeedbackmindSchema.getRestoreDate());
		this.Attitude = afeedbackmindSchema.getAttitude();
		this.State = afeedbackmindSchema.getState();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("FeedBackId") == null )
				this.FeedBackId = null;
			else
				this.FeedBackId = rs.getString("FeedBackId").trim();

			if( rs.getString("FeedBackModule") == null )
				this.FeedBackModule = null;
			else
				this.FeedBackModule = rs.getString("FeedBackModule").trim();

			if( rs.getString("Principal") == null )
				this.Principal = null;
			else
				this.Principal = rs.getString("Principal").trim();

			this.RestoreDate = rs.getDate("RestoreDate");
			if( rs.getString("Attitude") == null )
				this.Attitude = null;
			else
				this.Attitude = rs.getString("Attitude").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的feedbackmind表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "feedbackmindSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public feedbackmindSchema getSchema()
	{
		feedbackmindSchema afeedbackmindSchema = new feedbackmindSchema();
		afeedbackmindSchema.setSchema(this);
		return afeedbackmindSchema;
	}

	public feedbackmindDB getDB()
	{
		feedbackmindDB aDBOper = new feedbackmindDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpfeedbackmind描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(FeedBackId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeedBackModule)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Principal)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RestoreDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Attitude)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prpfeedbackmind>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			FeedBackId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			FeedBackModule = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Principal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RestoreDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			Attitude = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "feedbackmindSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("FeedBackId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeedBackId));
		}
		if (FCode.equals("FeedBackModule"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeedBackModule));
		}
		if (FCode.equals("Principal"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Principal));
		}
		if (FCode.equals("RestoreDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRestoreDate()));
		}
		if (FCode.equals("Attitude"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Attitude));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(FeedBackId);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(FeedBackModule);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Principal);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRestoreDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Attitude);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("FeedBackId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeedBackId = FValue.trim();
			}
			else
				FeedBackId = null;
		}
		if (FCode.equalsIgnoreCase("FeedBackModule"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeedBackModule = FValue.trim();
			}
			else
				FeedBackModule = null;
		}
		if (FCode.equalsIgnoreCase("Principal"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Principal = FValue.trim();
			}
			else
				Principal = null;
		}
		if (FCode.equalsIgnoreCase("RestoreDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RestoreDate = fDate.getDate( FValue );
			}
			else
				RestoreDate = null;
		}
		if (FCode.equalsIgnoreCase("Attitude"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Attitude = FValue.trim();
			}
			else
				Attitude = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		feedbackmindSchema other = (feedbackmindSchema)otherObject;
		return
			(FeedBackId == null ? other.getFeedBackId() == null : FeedBackId.equals(other.getFeedBackId()))
			&& (FeedBackModule == null ? other.getFeedBackModule() == null : FeedBackModule.equals(other.getFeedBackModule()))
			&& (Principal == null ? other.getPrincipal() == null : Principal.equals(other.getPrincipal()))
			&& (RestoreDate == null ? other.getRestoreDate() == null : fDate.getString(RestoreDate).equals(other.getRestoreDate()))
			&& (Attitude == null ? other.getAttitude() == null : Attitude.equals(other.getAttitude()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("FeedBackId") ) {
			return 0;
		}
		if( strFieldName.equals("FeedBackModule") ) {
			return 1;
		}
		if( strFieldName.equals("Principal") ) {
			return 2;
		}
		if( strFieldName.equals("RestoreDate") ) {
			return 3;
		}
		if( strFieldName.equals("Attitude") ) {
			return 4;
		}
		if( strFieldName.equals("State") ) {
			return 5;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "FeedBackId";
				break;
			case 1:
				strFieldName = "FeedBackModule";
				break;
			case 2:
				strFieldName = "Principal";
				break;
			case 3:
				strFieldName = "RestoreDate";
				break;
			case 4:
				strFieldName = "Attitude";
				break;
			case 5:
				strFieldName = "State";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("FeedBackId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeedBackModule") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Principal") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RestoreDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Attitude") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
