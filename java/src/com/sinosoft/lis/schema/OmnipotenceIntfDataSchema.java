/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.OmnipotenceIntfDataDB;

/*
 * <p>ClassName: OmnipotenceIntfDataSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2008-04-15
 */
public class OmnipotenceIntfDataSchema implements Schema, Cloneable
{
	// @Field
	/** 单位代码 */
	private String ManageCom;
	/** 帐套类型 */
	private String AccBookType;
	/** 帐套编码 */
	private String AccBookCode;
	/** 业务类型 */
	private String BussType;
	/** 业务日期 */
	private Date BussDate;
	/** 批次号 */
	private int SuffixNo;
	/** 会计月度 */
	private String YearMonth;
	/** 凭证号 */
	private String VoucherNo;
	/** 保单生效日期 */
	private Date PolicyValiDate;
	/** 业务数据入帐日 */
	private Date OperDate;
	/** 保单终止日期 */
	private Date PolicyStopDate;
	/** 印刷号 */
	private String PrtNo;
	/** 保单号 */
	private String ContNo;
	/** 投保单号 */
	private String ProPosalContNo;
	/** 业务号 */
	private String OtherNo;
	/** 客户名称 */
	private String AppntName;
	/** 险种大类 */
	private String RiskType;
	/** 摘要 */
	private String Remark;
	/** 收费方式 */
	private String PayGetFlag;
	/** 缴费方式 */
	private String PayGetMode;
	/** 缴费频次 */
	private String PayIntv;
	/** 险种 */
	private String RiskCode;
	/** 渠道 */
	private String SaleChnl;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐号 */
	private String BankAccNo;
	/** 支票号 */
	private String ChargeNo;
	/** 费率 */
	private int FeeRate;
	/** 币别 */
	private String CurrencyType;
	/** 缴费金额 */
	private int PayMoney;
	/** 初始管理费 */
	private int FirstManageFee;
	/** 保单管理费 */
	private int PolicyManageFee;
	/** 风险保费 */
	private int VentureFee;
	/** 死伤医疗给付金额 */
	private int SHYLGetMoney;
	/** 体检费用 */
	private int CheckFee;
	/** 满期给付金额 */
	private int MJGetMoney;
	/** 提前领取金额 */
	private int AheadGetMoney;
	/** 部分领取金额 */
	private int PartGetMoney;
	/** 退保金 */
	private int TBGetMoney;
	/** 退还保费 */
	private int TFGetMoney;
	/** 账户结息 */
	private int LXFee;
	/** 借方科目 */
	private String DebitItem;
	/** 贷方科目 */
	private String CreditItem;
	/** 借方专项段 */
	private String DebDirectionother;
	/** 借方专项段值 */
	private String DebDirectionotherValue;
	/** 贷方专线段 */
	private String CreDirectionother;
	/** 贷方专项段值 */
	private String CreDirectionotherValue;
	/** 持续奖金 */
	private int JJFee;
	/** 状态 */
	private String State;
	/** 业务行为说明 */
	private String BussDescribe;
	/** 业务员 */
	private String AgentCode;
	/** 收付费人员代码 */
	private String IntfOperater;
	/** 数据验证标志 */
	private String DataCheckFLag;
	/** 保险期范围 */
	private String S06;
	/** 销售对象 */
	private String S07;
	/** 新单续期 */
	private String S09;
	/** 备用1 */
	private String Remark1;
	/** 备用2 */
	private String Remark2;
	/** 备用3 */
	private String Remark3;
	/** 备用4 */
	private String Remark4;
	/** 错误信息 */
	private String ErrorInfo;
	/** 对应的应收应付日期 */
	private Date RevPayDate;

	public static final int FIELDNUM = 61;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public OmnipotenceIntfDataSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "ManageCom";
		pk[1] = "BussType";
		pk[2] = "BussDate";
		pk[3] = "SuffixNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                OmnipotenceIntfDataSchema cloned = (OmnipotenceIntfDataSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getAccBookType()
	{
		return AccBookType;
	}
	public void setAccBookType(String aAccBookType)
	{
            AccBookType = aAccBookType;
	}
	public String getAccBookCode()
	{
		return AccBookCode;
	}
	public void setAccBookCode(String aAccBookCode)
	{
            AccBookCode = aAccBookCode;
	}
	public String getBussType()
	{
		return BussType;
	}
	public void setBussType(String aBussType)
	{
            BussType = aBussType;
	}
	public String getBussDate()
	{
		if( BussDate != null )
			return fDate.getString(BussDate);
		else
			return null;
	}
	public void setBussDate(Date aBussDate)
	{
            BussDate = aBussDate;
	}
	public void setBussDate(String aBussDate)
	{
		if (aBussDate != null && !aBussDate.equals("") )
		{
			BussDate = fDate.getDate( aBussDate );
		}
		else
			BussDate = null;
	}

	public int getSuffixNo()
	{
		return SuffixNo;
	}
	public void setSuffixNo(int aSuffixNo)
	{
            SuffixNo = aSuffixNo;
	}
	public void setSuffixNo(String aSuffixNo)
	{
		if (aSuffixNo != null && !aSuffixNo.equals(""))
		{
			Integer tInteger = new Integer(aSuffixNo);
			int i = tInteger.intValue();
			SuffixNo = i;
		}
	}

	public String getYearMonth()
	{
		return YearMonth;
	}
	public void setYearMonth(String aYearMonth)
	{
            YearMonth = aYearMonth;
	}
	public String getVoucherNo()
	{
		return VoucherNo;
	}
	public void setVoucherNo(String aVoucherNo)
	{
            VoucherNo = aVoucherNo;
	}
	public String getPolicyValiDate()
	{
		if( PolicyValiDate != null )
			return fDate.getString(PolicyValiDate);
		else
			return null;
	}
	public void setPolicyValiDate(Date aPolicyValiDate)
	{
            PolicyValiDate = aPolicyValiDate;
	}
	public void setPolicyValiDate(String aPolicyValiDate)
	{
		if (aPolicyValiDate != null && !aPolicyValiDate.equals("") )
		{
			PolicyValiDate = fDate.getDate( aPolicyValiDate );
		}
		else
			PolicyValiDate = null;
	}

	public String getOperDate()
	{
		if( OperDate != null )
			return fDate.getString(OperDate);
		else
			return null;
	}
	public void setOperDate(Date aOperDate)
	{
            OperDate = aOperDate;
	}
	public void setOperDate(String aOperDate)
	{
		if (aOperDate != null && !aOperDate.equals("") )
		{
			OperDate = fDate.getDate( aOperDate );
		}
		else
			OperDate = null;
	}

	public String getPolicyStopDate()
	{
		if( PolicyStopDate != null )
			return fDate.getString(PolicyStopDate);
		else
			return null;
	}
	public void setPolicyStopDate(Date aPolicyStopDate)
	{
            PolicyStopDate = aPolicyStopDate;
	}
	public void setPolicyStopDate(String aPolicyStopDate)
	{
		if (aPolicyStopDate != null && !aPolicyStopDate.equals("") )
		{
			PolicyStopDate = fDate.getDate( aPolicyStopDate );
		}
		else
			PolicyStopDate = null;
	}

	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
            PrtNo = aPrtNo;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
            ContNo = aContNo;
	}
	public String getProPosalContNo()
	{
		return ProPosalContNo;
	}
	public void setProPosalContNo(String aProPosalContNo)
	{
            ProPosalContNo = aProPosalContNo;
	}
	public String getOtherNo()
	{
		return OtherNo;
	}
	public void setOtherNo(String aOtherNo)
	{
            OtherNo = aOtherNo;
	}
	public String getAppntName()
	{
		return AppntName;
	}
	public void setAppntName(String aAppntName)
	{
            AppntName = aAppntName;
	}
	public String getRiskType()
	{
		return RiskType;
	}
	public void setRiskType(String aRiskType)
	{
            RiskType = aRiskType;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
            Remark = aRemark;
	}
	public String getPayGetFlag()
	{
		return PayGetFlag;
	}
	public void setPayGetFlag(String aPayGetFlag)
	{
            PayGetFlag = aPayGetFlag;
	}
	public String getPayGetMode()
	{
		return PayGetMode;
	}
	public void setPayGetMode(String aPayGetMode)
	{
            PayGetMode = aPayGetMode;
	}
	public String getPayIntv()
	{
		return PayIntv;
	}
	public void setPayIntv(String aPayIntv)
	{
            PayIntv = aPayIntv;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
            RiskCode = aRiskCode;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
            SaleChnl = aSaleChnl;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
            BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
            BankAccNo = aBankAccNo;
	}
	public String getChargeNo()
	{
		return ChargeNo;
	}
	public void setChargeNo(String aChargeNo)
	{
            ChargeNo = aChargeNo;
	}
	public int getFeeRate()
	{
		return FeeRate;
	}
	public void setFeeRate(int aFeeRate)
	{
            FeeRate = aFeeRate;
	}
	public void setFeeRate(String aFeeRate)
	{
		if (aFeeRate != null && !aFeeRate.equals(""))
		{
			Integer tInteger = new Integer(aFeeRate);
			int i = tInteger.intValue();
			FeeRate = i;
		}
	}

	public String getCurrencyType()
	{
		return CurrencyType;
	}
	public void setCurrencyType(String aCurrencyType)
	{
            CurrencyType = aCurrencyType;
	}
	public int getPayMoney()
	{
		return PayMoney;
	}
	public void setPayMoney(int aPayMoney)
	{
            PayMoney = aPayMoney;
	}
	public void setPayMoney(String aPayMoney)
	{
		if (aPayMoney != null && !aPayMoney.equals(""))
		{
			Integer tInteger = new Integer(aPayMoney);
			int i = tInteger.intValue();
			PayMoney = i;
		}
	}

	public int getFirstManageFee()
	{
		return FirstManageFee;
	}
	public void setFirstManageFee(int aFirstManageFee)
	{
            FirstManageFee = aFirstManageFee;
	}
	public void setFirstManageFee(String aFirstManageFee)
	{
		if (aFirstManageFee != null && !aFirstManageFee.equals(""))
		{
			Integer tInteger = new Integer(aFirstManageFee);
			int i = tInteger.intValue();
			FirstManageFee = i;
		}
	}

	public int getPolicyManageFee()
	{
		return PolicyManageFee;
	}
	public void setPolicyManageFee(int aPolicyManageFee)
	{
            PolicyManageFee = aPolicyManageFee;
	}
	public void setPolicyManageFee(String aPolicyManageFee)
	{
		if (aPolicyManageFee != null && !aPolicyManageFee.equals(""))
		{
			Integer tInteger = new Integer(aPolicyManageFee);
			int i = tInteger.intValue();
			PolicyManageFee = i;
		}
	}

	public int getVentureFee()
	{
		return VentureFee;
	}
	public void setVentureFee(int aVentureFee)
	{
            VentureFee = aVentureFee;
	}
	public void setVentureFee(String aVentureFee)
	{
		if (aVentureFee != null && !aVentureFee.equals(""))
		{
			Integer tInteger = new Integer(aVentureFee);
			int i = tInteger.intValue();
			VentureFee = i;
		}
	}

	public int getSHYLGetMoney()
	{
		return SHYLGetMoney;
	}
	public void setSHYLGetMoney(int aSHYLGetMoney)
	{
            SHYLGetMoney = aSHYLGetMoney;
	}
	public void setSHYLGetMoney(String aSHYLGetMoney)
	{
		if (aSHYLGetMoney != null && !aSHYLGetMoney.equals(""))
		{
			Integer tInteger = new Integer(aSHYLGetMoney);
			int i = tInteger.intValue();
			SHYLGetMoney = i;
		}
	}

	public int getCheckFee()
	{
		return CheckFee;
	}
	public void setCheckFee(int aCheckFee)
	{
            CheckFee = aCheckFee;
	}
	public void setCheckFee(String aCheckFee)
	{
		if (aCheckFee != null && !aCheckFee.equals(""))
		{
			Integer tInteger = new Integer(aCheckFee);
			int i = tInteger.intValue();
			CheckFee = i;
		}
	}

	public int getMJGetMoney()
	{
		return MJGetMoney;
	}
	public void setMJGetMoney(int aMJGetMoney)
	{
            MJGetMoney = aMJGetMoney;
	}
	public void setMJGetMoney(String aMJGetMoney)
	{
		if (aMJGetMoney != null && !aMJGetMoney.equals(""))
		{
			Integer tInteger = new Integer(aMJGetMoney);
			int i = tInteger.intValue();
			MJGetMoney = i;
		}
	}

	public int getAheadGetMoney()
	{
		return AheadGetMoney;
	}
	public void setAheadGetMoney(int aAheadGetMoney)
	{
            AheadGetMoney = aAheadGetMoney;
	}
	public void setAheadGetMoney(String aAheadGetMoney)
	{
		if (aAheadGetMoney != null && !aAheadGetMoney.equals(""))
		{
			Integer tInteger = new Integer(aAheadGetMoney);
			int i = tInteger.intValue();
			AheadGetMoney = i;
		}
	}

	public int getPartGetMoney()
	{
		return PartGetMoney;
	}
	public void setPartGetMoney(int aPartGetMoney)
	{
            PartGetMoney = aPartGetMoney;
	}
	public void setPartGetMoney(String aPartGetMoney)
	{
		if (aPartGetMoney != null && !aPartGetMoney.equals(""))
		{
			Integer tInteger = new Integer(aPartGetMoney);
			int i = tInteger.intValue();
			PartGetMoney = i;
		}
	}

	public int getTBGetMoney()
	{
		return TBGetMoney;
	}
	public void setTBGetMoney(int aTBGetMoney)
	{
            TBGetMoney = aTBGetMoney;
	}
	public void setTBGetMoney(String aTBGetMoney)
	{
		if (aTBGetMoney != null && !aTBGetMoney.equals(""))
		{
			Integer tInteger = new Integer(aTBGetMoney);
			int i = tInteger.intValue();
			TBGetMoney = i;
		}
	}

	public int getTFGetMoney()
	{
		return TFGetMoney;
	}
	public void setTFGetMoney(int aTFGetMoney)
	{
            TFGetMoney = aTFGetMoney;
	}
	public void setTFGetMoney(String aTFGetMoney)
	{
		if (aTFGetMoney != null && !aTFGetMoney.equals(""))
		{
			Integer tInteger = new Integer(aTFGetMoney);
			int i = tInteger.intValue();
			TFGetMoney = i;
		}
	}

	public int getLXFee()
	{
		return LXFee;
	}
	public void setLXFee(int aLXFee)
	{
            LXFee = aLXFee;
	}
	public void setLXFee(String aLXFee)
	{
		if (aLXFee != null && !aLXFee.equals(""))
		{
			Integer tInteger = new Integer(aLXFee);
			int i = tInteger.intValue();
			LXFee = i;
		}
	}

	public String getDebitItem()
	{
		return DebitItem;
	}
	public void setDebitItem(String aDebitItem)
	{
            DebitItem = aDebitItem;
	}
	public String getCreditItem()
	{
		return CreditItem;
	}
	public void setCreditItem(String aCreditItem)
	{
            CreditItem = aCreditItem;
	}
	public String getDebDirectionother()
	{
		return DebDirectionother;
	}
	public void setDebDirectionother(String aDebDirectionother)
	{
            DebDirectionother = aDebDirectionother;
	}
	public String getDebDirectionotherValue()
	{
		return DebDirectionotherValue;
	}
	public void setDebDirectionotherValue(String aDebDirectionotherValue)
	{
            DebDirectionotherValue = aDebDirectionotherValue;
	}
	public String getCreDirectionother()
	{
		return CreDirectionother;
	}
	public void setCreDirectionother(String aCreDirectionother)
	{
            CreDirectionother = aCreDirectionother;
	}
	public String getCreDirectionotherValue()
	{
		return CreDirectionotherValue;
	}
	public void setCreDirectionotherValue(String aCreDirectionotherValue)
	{
            CreDirectionotherValue = aCreDirectionotherValue;
	}
	public int getJJFee()
	{
		return JJFee;
	}
	public void setJJFee(int aJJFee)
	{
            JJFee = aJJFee;
	}
	public void setJJFee(String aJJFee)
	{
		if (aJJFee != null && !aJJFee.equals(""))
		{
			Integer tInteger = new Integer(aJJFee);
			int i = tInteger.intValue();
			JJFee = i;
		}
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
            State = aState;
	}
	public String getBussDescribe()
	{
		return BussDescribe;
	}
	public void setBussDescribe(String aBussDescribe)
	{
            BussDescribe = aBussDescribe;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
            AgentCode = aAgentCode;
	}
	public String getIntfOperater()
	{
		return IntfOperater;
	}
	public void setIntfOperater(String aIntfOperater)
	{
            IntfOperater = aIntfOperater;
	}
	public String getDataCheckFLag()
	{
		return DataCheckFLag;
	}
	public void setDataCheckFLag(String aDataCheckFLag)
	{
            DataCheckFLag = aDataCheckFLag;
	}
	public String getS06()
	{
		return S06;
	}
	public void setS06(String aS06)
	{
            S06 = aS06;
	}
	public String getS07()
	{
		return S07;
	}
	public void setS07(String aS07)
	{
            S07 = aS07;
	}
	public String getS09()
	{
		return S09;
	}
	public void setS09(String aS09)
	{
            S09 = aS09;
	}
	public String getRemark1()
	{
		return Remark1;
	}
	public void setRemark1(String aRemark1)
	{
            Remark1 = aRemark1;
	}
	public String getRemark2()
	{
		return Remark2;
	}
	public void setRemark2(String aRemark2)
	{
            Remark2 = aRemark2;
	}
	public String getRemark3()
	{
		return Remark3;
	}
	public void setRemark3(String aRemark3)
	{
            Remark3 = aRemark3;
	}
	public String getRemark4()
	{
		return Remark4;
	}
	public void setRemark4(String aRemark4)
	{
            Remark4 = aRemark4;
	}
	public String getErrorInfo()
	{
		return ErrorInfo;
	}
	public void setErrorInfo(String aErrorInfo)
	{
            ErrorInfo = aErrorInfo;
	}
	public String getRevPayDate()
	{
		if( RevPayDate != null )
			return fDate.getString(RevPayDate);
		else
			return null;
	}
	public void setRevPayDate(Date aRevPayDate)
	{
            RevPayDate = aRevPayDate;
	}
	public void setRevPayDate(String aRevPayDate)
	{
		if (aRevPayDate != null && !aRevPayDate.equals("") )
		{
			RevPayDate = fDate.getDate( aRevPayDate );
		}
		else
			RevPayDate = null;
	}


	/**
	* 使用另外一个 OmnipotenceIntfDataSchema 对象给 Schema 赋值
	* @param: aOmnipotenceIntfDataSchema OmnipotenceIntfDataSchema
	**/
	public void setSchema(OmnipotenceIntfDataSchema aOmnipotenceIntfDataSchema)
	{
		this.ManageCom = aOmnipotenceIntfDataSchema.getManageCom();
		this.AccBookType = aOmnipotenceIntfDataSchema.getAccBookType();
		this.AccBookCode = aOmnipotenceIntfDataSchema.getAccBookCode();
		this.BussType = aOmnipotenceIntfDataSchema.getBussType();
		this.BussDate = fDate.getDate( aOmnipotenceIntfDataSchema.getBussDate());
		this.SuffixNo = aOmnipotenceIntfDataSchema.getSuffixNo();
		this.YearMonth = aOmnipotenceIntfDataSchema.getYearMonth();
		this.VoucherNo = aOmnipotenceIntfDataSchema.getVoucherNo();
		this.PolicyValiDate = fDate.getDate( aOmnipotenceIntfDataSchema.getPolicyValiDate());
		this.OperDate = fDate.getDate( aOmnipotenceIntfDataSchema.getOperDate());
		this.PolicyStopDate = fDate.getDate( aOmnipotenceIntfDataSchema.getPolicyStopDate());
		this.PrtNo = aOmnipotenceIntfDataSchema.getPrtNo();
		this.ContNo = aOmnipotenceIntfDataSchema.getContNo();
		this.ProPosalContNo = aOmnipotenceIntfDataSchema.getProPosalContNo();
		this.OtherNo = aOmnipotenceIntfDataSchema.getOtherNo();
		this.AppntName = aOmnipotenceIntfDataSchema.getAppntName();
		this.RiskType = aOmnipotenceIntfDataSchema.getRiskType();
		this.Remark = aOmnipotenceIntfDataSchema.getRemark();
		this.PayGetFlag = aOmnipotenceIntfDataSchema.getPayGetFlag();
		this.PayGetMode = aOmnipotenceIntfDataSchema.getPayGetMode();
		this.PayIntv = aOmnipotenceIntfDataSchema.getPayIntv();
		this.RiskCode = aOmnipotenceIntfDataSchema.getRiskCode();
		this.SaleChnl = aOmnipotenceIntfDataSchema.getSaleChnl();
		this.BankCode = aOmnipotenceIntfDataSchema.getBankCode();
		this.BankAccNo = aOmnipotenceIntfDataSchema.getBankAccNo();
		this.ChargeNo = aOmnipotenceIntfDataSchema.getChargeNo();
		this.FeeRate = aOmnipotenceIntfDataSchema.getFeeRate();
		this.CurrencyType = aOmnipotenceIntfDataSchema.getCurrencyType();
		this.PayMoney = aOmnipotenceIntfDataSchema.getPayMoney();
		this.FirstManageFee = aOmnipotenceIntfDataSchema.getFirstManageFee();
		this.PolicyManageFee = aOmnipotenceIntfDataSchema.getPolicyManageFee();
		this.VentureFee = aOmnipotenceIntfDataSchema.getVentureFee();
		this.SHYLGetMoney = aOmnipotenceIntfDataSchema.getSHYLGetMoney();
		this.CheckFee = aOmnipotenceIntfDataSchema.getCheckFee();
		this.MJGetMoney = aOmnipotenceIntfDataSchema.getMJGetMoney();
		this.AheadGetMoney = aOmnipotenceIntfDataSchema.getAheadGetMoney();
		this.PartGetMoney = aOmnipotenceIntfDataSchema.getPartGetMoney();
		this.TBGetMoney = aOmnipotenceIntfDataSchema.getTBGetMoney();
		this.TFGetMoney = aOmnipotenceIntfDataSchema.getTFGetMoney();
		this.LXFee = aOmnipotenceIntfDataSchema.getLXFee();
		this.DebitItem = aOmnipotenceIntfDataSchema.getDebitItem();
		this.CreditItem = aOmnipotenceIntfDataSchema.getCreditItem();
		this.DebDirectionother = aOmnipotenceIntfDataSchema.getDebDirectionother();
		this.DebDirectionotherValue = aOmnipotenceIntfDataSchema.getDebDirectionotherValue();
		this.CreDirectionother = aOmnipotenceIntfDataSchema.getCreDirectionother();
		this.CreDirectionotherValue = aOmnipotenceIntfDataSchema.getCreDirectionotherValue();
		this.JJFee = aOmnipotenceIntfDataSchema.getJJFee();
		this.State = aOmnipotenceIntfDataSchema.getState();
		this.BussDescribe = aOmnipotenceIntfDataSchema.getBussDescribe();
		this.AgentCode = aOmnipotenceIntfDataSchema.getAgentCode();
		this.IntfOperater = aOmnipotenceIntfDataSchema.getIntfOperater();
		this.DataCheckFLag = aOmnipotenceIntfDataSchema.getDataCheckFLag();
		this.S06 = aOmnipotenceIntfDataSchema.getS06();
		this.S07 = aOmnipotenceIntfDataSchema.getS07();
		this.S09 = aOmnipotenceIntfDataSchema.getS09();
		this.Remark1 = aOmnipotenceIntfDataSchema.getRemark1();
		this.Remark2 = aOmnipotenceIntfDataSchema.getRemark2();
		this.Remark3 = aOmnipotenceIntfDataSchema.getRemark3();
		this.Remark4 = aOmnipotenceIntfDataSchema.getRemark4();
		this.ErrorInfo = aOmnipotenceIntfDataSchema.getErrorInfo();
		this.RevPayDate = fDate.getDate( aOmnipotenceIntfDataSchema.getRevPayDate());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("AccBookType") == null )
				this.AccBookType = null;
			else
				this.AccBookType = rs.getString("AccBookType").trim();

			if( rs.getString("AccBookCode") == null )
				this.AccBookCode = null;
			else
				this.AccBookCode = rs.getString("AccBookCode").trim();

			if( rs.getString("BussType") == null )
				this.BussType = null;
			else
				this.BussType = rs.getString("BussType").trim();

			this.BussDate = rs.getDate("BussDate");
			this.SuffixNo = rs.getInt("SuffixNo");
			if( rs.getString("YearMonth") == null )
				this.YearMonth = null;
			else
				this.YearMonth = rs.getString("YearMonth").trim();

			if( rs.getString("VoucherNo") == null )
				this.VoucherNo = null;
			else
				this.VoucherNo = rs.getString("VoucherNo").trim();

			this.PolicyValiDate = rs.getDate("PolicyValiDate");
			this.OperDate = rs.getDate("OperDate");
			this.PolicyStopDate = rs.getDate("PolicyStopDate");
			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("ProPosalContNo") == null )
				this.ProPosalContNo = null;
			else
				this.ProPosalContNo = rs.getString("ProPosalContNo").trim();

			if( rs.getString("OtherNo") == null )
				this.OtherNo = null;
			else
				this.OtherNo = rs.getString("OtherNo").trim();

			if( rs.getString("AppntName") == null )
				this.AppntName = null;
			else
				this.AppntName = rs.getString("AppntName").trim();

			if( rs.getString("RiskType") == null )
				this.RiskType = null;
			else
				this.RiskType = rs.getString("RiskType").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("PayGetFlag") == null )
				this.PayGetFlag = null;
			else
				this.PayGetFlag = rs.getString("PayGetFlag").trim();

			if( rs.getString("PayGetMode") == null )
				this.PayGetMode = null;
			else
				this.PayGetMode = rs.getString("PayGetMode").trim();

			if( rs.getString("PayIntv") == null )
				this.PayIntv = null;
			else
				this.PayIntv = rs.getString("PayIntv").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("ChargeNo") == null )
				this.ChargeNo = null;
			else
				this.ChargeNo = rs.getString("ChargeNo").trim();

			this.FeeRate = rs.getInt("FeeRate");
			if( rs.getString("CurrencyType") == null )
				this.CurrencyType = null;
			else
				this.CurrencyType = rs.getString("CurrencyType").trim();

			this.PayMoney = rs.getInt("PayMoney");
			this.FirstManageFee = rs.getInt("FirstManageFee");
			this.PolicyManageFee = rs.getInt("PolicyManageFee");
			this.VentureFee = rs.getInt("VentureFee");
			this.SHYLGetMoney = rs.getInt("SHYLGetMoney");
			this.CheckFee = rs.getInt("CheckFee");
			this.MJGetMoney = rs.getInt("MJGetMoney");
			this.AheadGetMoney = rs.getInt("AheadGetMoney");
			this.PartGetMoney = rs.getInt("PartGetMoney");
			this.TBGetMoney = rs.getInt("TBGetMoney");
			this.TFGetMoney = rs.getInt("TFGetMoney");
			this.LXFee = rs.getInt("LXFee");
			if( rs.getString("DebitItem") == null )
				this.DebitItem = null;
			else
				this.DebitItem = rs.getString("DebitItem").trim();

			if( rs.getString("CreditItem") == null )
				this.CreditItem = null;
			else
				this.CreditItem = rs.getString("CreditItem").trim();

			if( rs.getString("DebDirectionother") == null )
				this.DebDirectionother = null;
			else
				this.DebDirectionother = rs.getString("DebDirectionother").trim();

			if( rs.getString("DebDirectionotherValue") == null )
				this.DebDirectionotherValue = null;
			else
				this.DebDirectionotherValue = rs.getString("DebDirectionotherValue").trim();

			if( rs.getString("CreDirectionother") == null )
				this.CreDirectionother = null;
			else
				this.CreDirectionother = rs.getString("CreDirectionother").trim();

			if( rs.getString("CreDirectionotherValue") == null )
				this.CreDirectionotherValue = null;
			else
				this.CreDirectionotherValue = rs.getString("CreDirectionotherValue").trim();

			this.JJFee = rs.getInt("JJFee");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("BussDescribe") == null )
				this.BussDescribe = null;
			else
				this.BussDescribe = rs.getString("BussDescribe").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("IntfOperater") == null )
				this.IntfOperater = null;
			else
				this.IntfOperater = rs.getString("IntfOperater").trim();

			if( rs.getString("DataCheckFLag") == null )
				this.DataCheckFLag = null;
			else
				this.DataCheckFLag = rs.getString("DataCheckFLag").trim();

			if( rs.getString("S06") == null )
				this.S06 = null;
			else
				this.S06 = rs.getString("S06").trim();

			if( rs.getString("S07") == null )
				this.S07 = null;
			else
				this.S07 = rs.getString("S07").trim();

			if( rs.getString("S09") == null )
				this.S09 = null;
			else
				this.S09 = rs.getString("S09").trim();

			if( rs.getString("Remark1") == null )
				this.Remark1 = null;
			else
				this.Remark1 = rs.getString("Remark1").trim();

			if( rs.getString("Remark2") == null )
				this.Remark2 = null;
			else
				this.Remark2 = rs.getString("Remark2").trim();

			if( rs.getString("Remark3") == null )
				this.Remark3 = null;
			else
				this.Remark3 = rs.getString("Remark3").trim();

			if( rs.getString("Remark4") == null )
				this.Remark4 = null;
			else
				this.Remark4 = rs.getString("Remark4").trim();

			if( rs.getString("ErrorInfo") == null )
				this.ErrorInfo = null;
			else
				this.ErrorInfo = rs.getString("ErrorInfo").trim();

			this.RevPayDate = rs.getDate("RevPayDate");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的OmnipotenceIntfData表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "OmnipotenceIntfDataSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public OmnipotenceIntfDataSchema getSchema()
	{
		OmnipotenceIntfDataSchema aOmnipotenceIntfDataSchema = new OmnipotenceIntfDataSchema();
		aOmnipotenceIntfDataSchema.setSchema(this);
		return aOmnipotenceIntfDataSchema;
	}

	public OmnipotenceIntfDataDB getDB()
	{
		OmnipotenceIntfDataDB aDBOper = new OmnipotenceIntfDataDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpOmnipotenceIntfData描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AccBookType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AccBookCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BussType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( BussDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SuffixNo));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(YearMonth)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(VoucherNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( PolicyValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( OperDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( PolicyStopDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ProPosalContNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(OtherNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RiskType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayGetFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayIntv)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ChargeNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(FeeRate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CurrencyType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PayMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(FirstManageFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PolicyManageFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(VentureFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(SHYLGetMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(CheckFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(MJGetMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(AheadGetMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(PartGetMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(TBGetMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(TFGetMoney));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(LXFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DebitItem)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CreditItem)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DebDirectionother)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DebDirectionotherValue)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CreDirectionother)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CreDirectionotherValue)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(JJFee));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BussDescribe)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(IntfOperater)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DataCheckFLag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(S06)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(S07)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(S09)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark1)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark2)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark3)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark4)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ErrorInfo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( RevPayDate )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpOmnipotenceIntfData>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AccBookType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			AccBookCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BussDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			SuffixNo= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
			YearMonth = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			VoucherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			PolicyValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			OperDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			PolicyStopDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ProPosalContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			OtherNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			RiskType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			PayGetFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			PayGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			PayIntv = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ChargeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			FeeRate= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).intValue();
			CurrencyType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			PayMoney= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).intValue();
			FirstManageFee= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).intValue();
			PolicyManageFee= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).intValue();
			VentureFee= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).intValue();
			SHYLGetMoney= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,33,SysConst.PACKAGESPILTER))).intValue();
			CheckFee= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,34,SysConst.PACKAGESPILTER))).intValue();
			MJGetMoney= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,35,SysConst.PACKAGESPILTER))).intValue();
			AheadGetMoney= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,36,SysConst.PACKAGESPILTER))).intValue();
			PartGetMoney= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,37,SysConst.PACKAGESPILTER))).intValue();
			TBGetMoney= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,38,SysConst.PACKAGESPILTER))).intValue();
			TFGetMoney= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,39,SysConst.PACKAGESPILTER))).intValue();
			LXFee= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,40,SysConst.PACKAGESPILTER))).intValue();
			DebitItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			CreditItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			DebDirectionother = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			DebDirectionotherValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			CreDirectionother = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			CreDirectionotherValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			JJFee= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,47,SysConst.PACKAGESPILTER))).intValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			BussDescribe = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			IntfOperater = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			DataCheckFLag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			S06 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			S07 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			S09 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			Remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			Remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			Remark3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			Remark4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			ErrorInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			RevPayDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "OmnipotenceIntfDataSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("AccBookType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccBookType));
		}
		if (FCode.equals("AccBookCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccBookCode));
		}
		if (FCode.equals("BussType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BussType));
		}
		if (FCode.equals("BussDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBussDate()));
		}
		if (FCode.equals("SuffixNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SuffixNo));
		}
		if (FCode.equals("YearMonth"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(YearMonth));
		}
		if (FCode.equals("VoucherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherNo));
		}
		if (FCode.equals("PolicyValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolicyValiDate()));
		}
		if (FCode.equals("OperDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOperDate()));
		}
		if (FCode.equals("PolicyStopDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPolicyStopDate()));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("ProPosalContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProPosalContNo));
		}
		if (FCode.equals("OtherNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherNo));
		}
		if (FCode.equals("AppntName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
		}
		if (FCode.equals("RiskType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskType));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("PayGetFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayGetFlag));
		}
		if (FCode.equals("PayGetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayGetMode));
		}
		if (FCode.equals("PayIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayIntv));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("ChargeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChargeNo));
		}
		if (FCode.equals("FeeRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeRate));
		}
		if (FCode.equals("CurrencyType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CurrencyType));
		}
		if (FCode.equals("PayMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayMoney));
		}
		if (FCode.equals("FirstManageFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FirstManageFee));
		}
		if (FCode.equals("PolicyManageFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyManageFee));
		}
		if (FCode.equals("VentureFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VentureFee));
		}
		if (FCode.equals("SHYLGetMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SHYLGetMoney));
		}
		if (FCode.equals("CheckFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CheckFee));
		}
		if (FCode.equals("MJGetMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MJGetMoney));
		}
		if (FCode.equals("AheadGetMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AheadGetMoney));
		}
		if (FCode.equals("PartGetMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PartGetMoney));
		}
		if (FCode.equals("TBGetMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TBGetMoney));
		}
		if (FCode.equals("TFGetMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TFGetMoney));
		}
		if (FCode.equals("LXFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LXFee));
		}
		if (FCode.equals("DebitItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DebitItem));
		}
		if (FCode.equals("CreditItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CreditItem));
		}
		if (FCode.equals("DebDirectionother"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DebDirectionother));
		}
		if (FCode.equals("DebDirectionotherValue"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DebDirectionotherValue));
		}
		if (FCode.equals("CreDirectionother"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CreDirectionother));
		}
		if (FCode.equals("CreDirectionotherValue"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CreDirectionotherValue));
		}
		if (FCode.equals("JJFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(JJFee));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("BussDescribe"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BussDescribe));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("IntfOperater"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IntfOperater));
		}
		if (FCode.equals("DataCheckFLag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataCheckFLag));
		}
		if (FCode.equals("S06"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(S06));
		}
		if (FCode.equals("S07"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(S07));
		}
		if (FCode.equals("S09"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(S09));
		}
		if (FCode.equals("Remark1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark1));
		}
		if (FCode.equals("Remark2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark2));
		}
		if (FCode.equals("Remark3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark3));
		}
		if (FCode.equals("Remark4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark4));
		}
		if (FCode.equals("ErrorInfo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ErrorInfo));
		}
		if (FCode.equals("RevPayDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRevPayDate()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AccBookType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(AccBookCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BussType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBussDate()));
				break;
			case 5:
				strFieldValue = String.valueOf(SuffixNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(YearMonth);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(VoucherNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolicyValiDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOperDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPolicyStopDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ProPosalContNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(OtherNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AppntName);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(RiskType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(PayGetFlag);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(PayGetMode);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(PayIntv);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ChargeNo);
				break;
			case 26:
				strFieldValue = String.valueOf(FeeRate);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(CurrencyType);
				break;
			case 28:
				strFieldValue = String.valueOf(PayMoney);
				break;
			case 29:
				strFieldValue = String.valueOf(FirstManageFee);
				break;
			case 30:
				strFieldValue = String.valueOf(PolicyManageFee);
				break;
			case 31:
				strFieldValue = String.valueOf(VentureFee);
				break;
			case 32:
				strFieldValue = String.valueOf(SHYLGetMoney);
				break;
			case 33:
				strFieldValue = String.valueOf(CheckFee);
				break;
			case 34:
				strFieldValue = String.valueOf(MJGetMoney);
				break;
			case 35:
				strFieldValue = String.valueOf(AheadGetMoney);
				break;
			case 36:
				strFieldValue = String.valueOf(PartGetMoney);
				break;
			case 37:
				strFieldValue = String.valueOf(TBGetMoney);
				break;
			case 38:
				strFieldValue = String.valueOf(TFGetMoney);
				break;
			case 39:
				strFieldValue = String.valueOf(LXFee);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(DebitItem);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(CreditItem);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(DebDirectionother);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(DebDirectionotherValue);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(CreDirectionother);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(CreDirectionotherValue);
				break;
			case 46:
				strFieldValue = String.valueOf(JJFee);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(BussDescribe);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(IntfOperater);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(DataCheckFLag);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(S06);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(S07);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(S09);
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(Remark1);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(Remark2);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(Remark3);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(Remark4);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(ErrorInfo);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRevPayDate()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AccBookType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccBookType = FValue.trim();
			}
			else
				AccBookType = null;
		}
		if (FCode.equalsIgnoreCase("AccBookCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccBookCode = FValue.trim();
			}
			else
				AccBookCode = null;
		}
		if (FCode.equalsIgnoreCase("BussType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BussType = FValue.trim();
			}
			else
				BussType = null;
		}
		if (FCode.equalsIgnoreCase("BussDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BussDate = fDate.getDate( FValue );
			}
			else
				BussDate = null;
		}
		if (FCode.equalsIgnoreCase("SuffixNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SuffixNo = i;
			}
		}
		if (FCode.equalsIgnoreCase("YearMonth"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				YearMonth = FValue.trim();
			}
			else
				YearMonth = null;
		}
		if (FCode.equalsIgnoreCase("VoucherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherNo = FValue.trim();
			}
			else
				VoucherNo = null;
		}
		if (FCode.equalsIgnoreCase("PolicyValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PolicyValiDate = fDate.getDate( FValue );
			}
			else
				PolicyValiDate = null;
		}
		if (FCode.equalsIgnoreCase("OperDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OperDate = fDate.getDate( FValue );
			}
			else
				OperDate = null;
		}
		if (FCode.equalsIgnoreCase("PolicyStopDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PolicyStopDate = fDate.getDate( FValue );
			}
			else
				PolicyStopDate = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("ProPosalContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProPosalContNo = FValue.trim();
			}
			else
				ProPosalContNo = null;
		}
		if (FCode.equalsIgnoreCase("OtherNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OtherNo = FValue.trim();
			}
			else
				OtherNo = null;
		}
		if (FCode.equalsIgnoreCase("AppntName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntName = FValue.trim();
			}
			else
				AppntName = null;
		}
		if (FCode.equalsIgnoreCase("RiskType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskType = FValue.trim();
			}
			else
				RiskType = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("PayGetFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayGetFlag = FValue.trim();
			}
			else
				PayGetFlag = null;
		}
		if (FCode.equalsIgnoreCase("PayGetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayGetMode = FValue.trim();
			}
			else
				PayGetMode = null;
		}
		if (FCode.equalsIgnoreCase("PayIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayIntv = FValue.trim();
			}
			else
				PayIntv = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("ChargeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChargeNo = FValue.trim();
			}
			else
				ChargeNo = null;
		}
		if (FCode.equalsIgnoreCase("FeeRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				FeeRate = i;
			}
		}
		if (FCode.equalsIgnoreCase("CurrencyType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CurrencyType = FValue.trim();
			}
			else
				CurrencyType = null;
		}
		if (FCode.equalsIgnoreCase("PayMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayMoney = i;
			}
		}
		if (FCode.equalsIgnoreCase("FirstManageFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				FirstManageFee = i;
			}
		}
		if (FCode.equalsIgnoreCase("PolicyManageFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PolicyManageFee = i;
			}
		}
		if (FCode.equalsIgnoreCase("VentureFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				VentureFee = i;
			}
		}
		if (FCode.equalsIgnoreCase("SHYLGetMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				SHYLGetMoney = i;
			}
		}
		if (FCode.equalsIgnoreCase("CheckFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				CheckFee = i;
			}
		}
		if (FCode.equalsIgnoreCase("MJGetMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MJGetMoney = i;
			}
		}
		if (FCode.equalsIgnoreCase("AheadGetMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AheadGetMoney = i;
			}
		}
		if (FCode.equalsIgnoreCase("PartGetMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PartGetMoney = i;
			}
		}
		if (FCode.equalsIgnoreCase("TBGetMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				TBGetMoney = i;
			}
		}
		if (FCode.equalsIgnoreCase("TFGetMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				TFGetMoney = i;
			}
		}
		if (FCode.equalsIgnoreCase("LXFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				LXFee = i;
			}
		}
		if (FCode.equalsIgnoreCase("DebitItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DebitItem = FValue.trim();
			}
			else
				DebitItem = null;
		}
		if (FCode.equalsIgnoreCase("CreditItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CreditItem = FValue.trim();
			}
			else
				CreditItem = null;
		}
		if (FCode.equalsIgnoreCase("DebDirectionother"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DebDirectionother = FValue.trim();
			}
			else
				DebDirectionother = null;
		}
		if (FCode.equalsIgnoreCase("DebDirectionotherValue"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DebDirectionotherValue = FValue.trim();
			}
			else
				DebDirectionotherValue = null;
		}
		if (FCode.equalsIgnoreCase("CreDirectionother"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CreDirectionother = FValue.trim();
			}
			else
				CreDirectionother = null;
		}
		if (FCode.equalsIgnoreCase("CreDirectionotherValue"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CreDirectionotherValue = FValue.trim();
			}
			else
				CreDirectionotherValue = null;
		}
		if (FCode.equalsIgnoreCase("JJFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				JJFee = i;
			}
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("BussDescribe"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BussDescribe = FValue.trim();
			}
			else
				BussDescribe = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("IntfOperater"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IntfOperater = FValue.trim();
			}
			else
				IntfOperater = null;
		}
		if (FCode.equalsIgnoreCase("DataCheckFLag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataCheckFLag = FValue.trim();
			}
			else
				DataCheckFLag = null;
		}
		if (FCode.equalsIgnoreCase("S06"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				S06 = FValue.trim();
			}
			else
				S06 = null;
		}
		if (FCode.equalsIgnoreCase("S07"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				S07 = FValue.trim();
			}
			else
				S07 = null;
		}
		if (FCode.equalsIgnoreCase("S09"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				S09 = FValue.trim();
			}
			else
				S09 = null;
		}
		if (FCode.equalsIgnoreCase("Remark1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark1 = FValue.trim();
			}
			else
				Remark1 = null;
		}
		if (FCode.equalsIgnoreCase("Remark2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark2 = FValue.trim();
			}
			else
				Remark2 = null;
		}
		if (FCode.equalsIgnoreCase("Remark3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark3 = FValue.trim();
			}
			else
				Remark3 = null;
		}
		if (FCode.equalsIgnoreCase("Remark4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark4 = FValue.trim();
			}
			else
				Remark4 = null;
		}
		if (FCode.equalsIgnoreCase("ErrorInfo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ErrorInfo = FValue.trim();
			}
			else
				ErrorInfo = null;
		}
		if (FCode.equalsIgnoreCase("RevPayDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RevPayDate = fDate.getDate( FValue );
			}
			else
				RevPayDate = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		OmnipotenceIntfDataSchema other = (OmnipotenceIntfDataSchema)otherObject;
		return
			ManageCom.equals(other.getManageCom())
			&& AccBookType.equals(other.getAccBookType())
			&& AccBookCode.equals(other.getAccBookCode())
			&& BussType.equals(other.getBussType())
			&& fDate.getString(BussDate).equals(other.getBussDate())
			&& SuffixNo == other.getSuffixNo()
			&& YearMonth.equals(other.getYearMonth())
			&& VoucherNo.equals(other.getVoucherNo())
			&& fDate.getString(PolicyValiDate).equals(other.getPolicyValiDate())
			&& fDate.getString(OperDate).equals(other.getOperDate())
			&& fDate.getString(PolicyStopDate).equals(other.getPolicyStopDate())
			&& PrtNo.equals(other.getPrtNo())
			&& ContNo.equals(other.getContNo())
			&& ProPosalContNo.equals(other.getProPosalContNo())
			&& OtherNo.equals(other.getOtherNo())
			&& AppntName.equals(other.getAppntName())
			&& RiskType.equals(other.getRiskType())
			&& Remark.equals(other.getRemark())
			&& PayGetFlag.equals(other.getPayGetFlag())
			&& PayGetMode.equals(other.getPayGetMode())
			&& PayIntv.equals(other.getPayIntv())
			&& RiskCode.equals(other.getRiskCode())
			&& SaleChnl.equals(other.getSaleChnl())
			&& BankCode.equals(other.getBankCode())
			&& BankAccNo.equals(other.getBankAccNo())
			&& ChargeNo.equals(other.getChargeNo())
			&& FeeRate == other.getFeeRate()
			&& CurrencyType.equals(other.getCurrencyType())
			&& PayMoney == other.getPayMoney()
			&& FirstManageFee == other.getFirstManageFee()
			&& PolicyManageFee == other.getPolicyManageFee()
			&& VentureFee == other.getVentureFee()
			&& SHYLGetMoney == other.getSHYLGetMoney()
			&& CheckFee == other.getCheckFee()
			&& MJGetMoney == other.getMJGetMoney()
			&& AheadGetMoney == other.getAheadGetMoney()
			&& PartGetMoney == other.getPartGetMoney()
			&& TBGetMoney == other.getTBGetMoney()
			&& TFGetMoney == other.getTFGetMoney()
			&& LXFee == other.getLXFee()
			&& DebitItem.equals(other.getDebitItem())
			&& CreditItem.equals(other.getCreditItem())
			&& DebDirectionother.equals(other.getDebDirectionother())
			&& DebDirectionotherValue.equals(other.getDebDirectionotherValue())
			&& CreDirectionother.equals(other.getCreDirectionother())
			&& CreDirectionotherValue.equals(other.getCreDirectionotherValue())
			&& JJFee == other.getJJFee()
			&& State.equals(other.getState())
			&& BussDescribe.equals(other.getBussDescribe())
			&& AgentCode.equals(other.getAgentCode())
			&& IntfOperater.equals(other.getIntfOperater())
			&& DataCheckFLag.equals(other.getDataCheckFLag())
			&& S06.equals(other.getS06())
			&& S07.equals(other.getS07())
			&& S09.equals(other.getS09())
			&& Remark1.equals(other.getRemark1())
			&& Remark2.equals(other.getRemark2())
			&& Remark3.equals(other.getRemark3())
			&& Remark4.equals(other.getRemark4())
			&& ErrorInfo.equals(other.getErrorInfo())
			&& fDate.getString(RevPayDate).equals(other.getRevPayDate());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ManageCom") ) {
			return 0;
		}
		if( strFieldName.equals("AccBookType") ) {
			return 1;
		}
		if( strFieldName.equals("AccBookCode") ) {
			return 2;
		}
		if( strFieldName.equals("BussType") ) {
			return 3;
		}
		if( strFieldName.equals("BussDate") ) {
			return 4;
		}
		if( strFieldName.equals("SuffixNo") ) {
			return 5;
		}
		if( strFieldName.equals("YearMonth") ) {
			return 6;
		}
		if( strFieldName.equals("VoucherNo") ) {
			return 7;
		}
		if( strFieldName.equals("PolicyValiDate") ) {
			return 8;
		}
		if( strFieldName.equals("OperDate") ) {
			return 9;
		}
		if( strFieldName.equals("PolicyStopDate") ) {
			return 10;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 11;
		}
		if( strFieldName.equals("ContNo") ) {
			return 12;
		}
		if( strFieldName.equals("ProPosalContNo") ) {
			return 13;
		}
		if( strFieldName.equals("OtherNo") ) {
			return 14;
		}
		if( strFieldName.equals("AppntName") ) {
			return 15;
		}
		if( strFieldName.equals("RiskType") ) {
			return 16;
		}
		if( strFieldName.equals("Remark") ) {
			return 17;
		}
		if( strFieldName.equals("PayGetFlag") ) {
			return 18;
		}
		if( strFieldName.equals("PayGetMode") ) {
			return 19;
		}
		if( strFieldName.equals("PayIntv") ) {
			return 20;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 21;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 22;
		}
		if( strFieldName.equals("BankCode") ) {
			return 23;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 24;
		}
		if( strFieldName.equals("ChargeNo") ) {
			return 25;
		}
		if( strFieldName.equals("FeeRate") ) {
			return 26;
		}
		if( strFieldName.equals("CurrencyType") ) {
			return 27;
		}
		if( strFieldName.equals("PayMoney") ) {
			return 28;
		}
		if( strFieldName.equals("FirstManageFee") ) {
			return 29;
		}
		if( strFieldName.equals("PolicyManageFee") ) {
			return 30;
		}
		if( strFieldName.equals("VentureFee") ) {
			return 31;
		}
		if( strFieldName.equals("SHYLGetMoney") ) {
			return 32;
		}
		if( strFieldName.equals("CheckFee") ) {
			return 33;
		}
		if( strFieldName.equals("MJGetMoney") ) {
			return 34;
		}
		if( strFieldName.equals("AheadGetMoney") ) {
			return 35;
		}
		if( strFieldName.equals("PartGetMoney") ) {
			return 36;
		}
		if( strFieldName.equals("TBGetMoney") ) {
			return 37;
		}
		if( strFieldName.equals("TFGetMoney") ) {
			return 38;
		}
		if( strFieldName.equals("LXFee") ) {
			return 39;
		}
		if( strFieldName.equals("DebitItem") ) {
			return 40;
		}
		if( strFieldName.equals("CreditItem") ) {
			return 41;
		}
		if( strFieldName.equals("DebDirectionother") ) {
			return 42;
		}
		if( strFieldName.equals("DebDirectionotherValue") ) {
			return 43;
		}
		if( strFieldName.equals("CreDirectionother") ) {
			return 44;
		}
		if( strFieldName.equals("CreDirectionotherValue") ) {
			return 45;
		}
		if( strFieldName.equals("JJFee") ) {
			return 46;
		}
		if( strFieldName.equals("State") ) {
			return 47;
		}
		if( strFieldName.equals("BussDescribe") ) {
			return 48;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 49;
		}
		if( strFieldName.equals("IntfOperater") ) {
			return 50;
		}
		if( strFieldName.equals("DataCheckFLag") ) {
			return 51;
		}
		if( strFieldName.equals("S06") ) {
			return 52;
		}
		if( strFieldName.equals("S07") ) {
			return 53;
		}
		if( strFieldName.equals("S09") ) {
			return 54;
		}
		if( strFieldName.equals("Remark1") ) {
			return 55;
		}
		if( strFieldName.equals("Remark2") ) {
			return 56;
		}
		if( strFieldName.equals("Remark3") ) {
			return 57;
		}
		if( strFieldName.equals("Remark4") ) {
			return 58;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return 59;
		}
		if( strFieldName.equals("RevPayDate") ) {
			return 60;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ManageCom";
				break;
			case 1:
				strFieldName = "AccBookType";
				break;
			case 2:
				strFieldName = "AccBookCode";
				break;
			case 3:
				strFieldName = "BussType";
				break;
			case 4:
				strFieldName = "BussDate";
				break;
			case 5:
				strFieldName = "SuffixNo";
				break;
			case 6:
				strFieldName = "YearMonth";
				break;
			case 7:
				strFieldName = "VoucherNo";
				break;
			case 8:
				strFieldName = "PolicyValiDate";
				break;
			case 9:
				strFieldName = "OperDate";
				break;
			case 10:
				strFieldName = "PolicyStopDate";
				break;
			case 11:
				strFieldName = "PrtNo";
				break;
			case 12:
				strFieldName = "ContNo";
				break;
			case 13:
				strFieldName = "ProPosalContNo";
				break;
			case 14:
				strFieldName = "OtherNo";
				break;
			case 15:
				strFieldName = "AppntName";
				break;
			case 16:
				strFieldName = "RiskType";
				break;
			case 17:
				strFieldName = "Remark";
				break;
			case 18:
				strFieldName = "PayGetFlag";
				break;
			case 19:
				strFieldName = "PayGetMode";
				break;
			case 20:
				strFieldName = "PayIntv";
				break;
			case 21:
				strFieldName = "RiskCode";
				break;
			case 22:
				strFieldName = "SaleChnl";
				break;
			case 23:
				strFieldName = "BankCode";
				break;
			case 24:
				strFieldName = "BankAccNo";
				break;
			case 25:
				strFieldName = "ChargeNo";
				break;
			case 26:
				strFieldName = "FeeRate";
				break;
			case 27:
				strFieldName = "CurrencyType";
				break;
			case 28:
				strFieldName = "PayMoney";
				break;
			case 29:
				strFieldName = "FirstManageFee";
				break;
			case 30:
				strFieldName = "PolicyManageFee";
				break;
			case 31:
				strFieldName = "VentureFee";
				break;
			case 32:
				strFieldName = "SHYLGetMoney";
				break;
			case 33:
				strFieldName = "CheckFee";
				break;
			case 34:
				strFieldName = "MJGetMoney";
				break;
			case 35:
				strFieldName = "AheadGetMoney";
				break;
			case 36:
				strFieldName = "PartGetMoney";
				break;
			case 37:
				strFieldName = "TBGetMoney";
				break;
			case 38:
				strFieldName = "TFGetMoney";
				break;
			case 39:
				strFieldName = "LXFee";
				break;
			case 40:
				strFieldName = "DebitItem";
				break;
			case 41:
				strFieldName = "CreditItem";
				break;
			case 42:
				strFieldName = "DebDirectionother";
				break;
			case 43:
				strFieldName = "DebDirectionotherValue";
				break;
			case 44:
				strFieldName = "CreDirectionother";
				break;
			case 45:
				strFieldName = "CreDirectionotherValue";
				break;
			case 46:
				strFieldName = "JJFee";
				break;
			case 47:
				strFieldName = "State";
				break;
			case 48:
				strFieldName = "BussDescribe";
				break;
			case 49:
				strFieldName = "AgentCode";
				break;
			case 50:
				strFieldName = "IntfOperater";
				break;
			case 51:
				strFieldName = "DataCheckFLag";
				break;
			case 52:
				strFieldName = "S06";
				break;
			case 53:
				strFieldName = "S07";
				break;
			case 54:
				strFieldName = "S09";
				break;
			case 55:
				strFieldName = "Remark1";
				break;
			case 56:
				strFieldName = "Remark2";
				break;
			case 57:
				strFieldName = "Remark3";
				break;
			case 58:
				strFieldName = "Remark4";
				break;
			case 59:
				strFieldName = "ErrorInfo";
				break;
			case 60:
				strFieldName = "RevPayDate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccBookType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccBookCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BussType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BussDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SuffixNo") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("YearMonth") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicyValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("OperDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PolicyStopDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProPosalContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OtherNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayGetFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayGetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayIntv") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChargeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeRate") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CurrencyType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayMoney") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("FirstManageFee") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PolicyManageFee") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("VentureFee") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SHYLGetMoney") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CheckFee") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MJGetMoney") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AheadGetMoney") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PartGetMoney") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("TBGetMoney") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("TFGetMoney") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("LXFee") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("DebitItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CreditItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DebDirectionother") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DebDirectionotherValue") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CreDirectionother") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CreDirectionotherValue") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JJFee") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BussDescribe") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IntfOperater") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataCheckFLag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("S06") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("S07") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("S09") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ErrorInfo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RevPayDate") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_INT;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_INT;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_INT;
				break;
			case 29:
				nFieldType = Schema.TYPE_INT;
				break;
			case 30:
				nFieldType = Schema.TYPE_INT;
				break;
			case 31:
				nFieldType = Schema.TYPE_INT;
				break;
			case 32:
				nFieldType = Schema.TYPE_INT;
				break;
			case 33:
				nFieldType = Schema.TYPE_INT;
				break;
			case 34:
				nFieldType = Schema.TYPE_INT;
				break;
			case 35:
				nFieldType = Schema.TYPE_INT;
				break;
			case 36:
				nFieldType = Schema.TYPE_INT;
				break;
			case 37:
				nFieldType = Schema.TYPE_INT;
				break;
			case 38:
				nFieldType = Schema.TYPE_INT;
				break;
			case 39:
				nFieldType = Schema.TYPE_INT;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_INT;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
