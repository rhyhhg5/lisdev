/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.Staff_InfoDB;

/*
 * <p>ClassName: Staff_InfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2009-02-24
 */
public class Staff_InfoSchema implements Schema, Cloneable
{
	// @Field
	/** 员工代码 */
	private String Staff_No;
	/** 员工姓名 */
	private String Staff_Name;
	/** 员工性别 */
	private String Sex;
	/** 证件类型 */
	private String Id_type;
	/** 证件号码 */
	private String Idno;
	/** 代理资格证号 */
	private String quano;
	/** 入司日期 */
	private Date JoinDate;
	/** 离司日期 */
	private Date LeaveDate;
	/** 学历 */
	private String Edu_Degree;
	/** 专业技术资格 */
	private String Accreditation;
	/** 所属分支机构 */
	private String Depart_info;
	/** 是否高管 */
	private String Is_Leader;
	/** 职位 */
	private String Staff_Rank;

	public static final int FIELDNUM = 13;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public Staff_InfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0]="Staff_No";
		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		Staff_InfoSchema cloned = (Staff_InfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getStaff_No()
	{
		return Staff_No;
	}
	public void setStaff_No(String aStaff_No)
	{
		Staff_No = aStaff_No;
	}
	public String getStaff_Name()
	{
		return Staff_Name;
	}
	public void setStaff_Name(String aStaff_Name)
	{
		Staff_Name = aStaff_Name;
	}
	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
		Sex = aSex;
	}
	public String getId_type()
	{
		return Id_type;
	}
	public void setId_type(String aId_type)
	{
		Id_type = aId_type;
	}
	public String getIdno()
	{
		return Idno;
	}
	public void setIdno(String aIdno)
	{
		Idno = aIdno;
	}
	public String getquano()
	{
		return quano;
	}
	public void setquano(String aquano)
	{
		quano = aquano;
	}
	public String getJoinDate()
	{
		if( JoinDate != null )
			return fDate.getString(JoinDate);
		else
			return null;
	}
	public void setJoinDate(Date aJoinDate)
	{
		JoinDate = aJoinDate;
	}
	public void setJoinDate(String aJoinDate)
	{
		if (aJoinDate != null && !aJoinDate.equals("") )
		{
			JoinDate = fDate.getDate( aJoinDate );
		}
		else
			JoinDate = null;
	}

	public String getLeaveDate()
	{
		if( LeaveDate != null )
			return fDate.getString(LeaveDate);
		else
			return null;
	}
	public void setLeaveDate(Date aLeaveDate)
	{
		LeaveDate = aLeaveDate;
	}
	public void setLeaveDate(String aLeaveDate)
	{
		if (aLeaveDate != null && !aLeaveDate.equals("") )
		{
			LeaveDate = fDate.getDate( aLeaveDate );
		}
		else
			LeaveDate = null;
	}

	public String getEdu_Degree()
	{
		return Edu_Degree;
	}
	public void setEdu_Degree(String aEdu_Degree)
	{
		Edu_Degree = aEdu_Degree;
	}
	public String getAccreditation()
	{
		return Accreditation;
	}
	public void setAccreditation(String aAccreditation)
	{
		Accreditation = aAccreditation;
	}
	public String getDepart_info()
	{
		return Depart_info;
	}
	public void setDepart_info(String aDepart_info)
	{
		Depart_info = aDepart_info;
	}
	public String getIs_Leader()
	{
		return Is_Leader;
	}
	public void setIs_Leader(String aIs_Leader)
	{
		Is_Leader = aIs_Leader;
	}
	public String getStaff_Rank()
	{
		return Staff_Rank;
	}
	public void setStaff_Rank(String aStaff_Rank)
	{
		Staff_Rank = aStaff_Rank;
	}

	/**
	* 使用另外一个 Staff_InfoSchema 对象给 Schema 赋值
	* @param: aStaff_InfoSchema Staff_InfoSchema
	**/
	public void setSchema(Staff_InfoSchema aStaff_InfoSchema)
	{
		this.Staff_No = aStaff_InfoSchema.getStaff_No();
		this.Staff_Name = aStaff_InfoSchema.getStaff_Name();
		this.Sex = aStaff_InfoSchema.getSex();
		this.Id_type = aStaff_InfoSchema.getId_type();
		this.Idno = aStaff_InfoSchema.getIdno();
		this.quano = aStaff_InfoSchema.getquano();
		this.JoinDate = fDate.getDate( aStaff_InfoSchema.getJoinDate());
		this.LeaveDate = fDate.getDate( aStaff_InfoSchema.getLeaveDate());
		this.Edu_Degree = aStaff_InfoSchema.getEdu_Degree();
		this.Accreditation = aStaff_InfoSchema.getAccreditation();
		this.Depart_info = aStaff_InfoSchema.getDepart_info();
		this.Is_Leader = aStaff_InfoSchema.getIs_Leader();
		this.Staff_Rank = aStaff_InfoSchema.getStaff_Rank();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Staff_No") == null )
				this.Staff_No = null;
			else
				this.Staff_No = rs.getString("Staff_No").trim();

			if( rs.getString("Staff_Name") == null )
				this.Staff_Name = null;
			else
				this.Staff_Name = rs.getString("Staff_Name").trim();

			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			if( rs.getString("Id_type") == null )
				this.Id_type = null;
			else
				this.Id_type = rs.getString("Id_type").trim();

			if( rs.getString("Idno") == null )
				this.Idno = null;
			else
				this.Idno = rs.getString("Idno").trim();

			if( rs.getString("quano") == null )
				this.quano = null;
			else
				this.quano = rs.getString("quano").trim();

			this.JoinDate = rs.getDate("JoinDate");
			this.LeaveDate = rs.getDate("LeaveDate");
			if( rs.getString("Edu_Degree") == null )
				this.Edu_Degree = null;
			else
				this.Edu_Degree = rs.getString("Edu_Degree").trim();

			if( rs.getString("Accreditation") == null )
				this.Accreditation = null;
			else
				this.Accreditation = rs.getString("Accreditation").trim();

			if( rs.getString("Depart_info") == null )
				this.Depart_info = null;
			else
				this.Depart_info = rs.getString("Depart_info").trim();

			if( rs.getString("Is_Leader") == null )
				this.Is_Leader = null;
			else
				this.Is_Leader = rs.getString("Is_Leader").trim();

			if( rs.getString("Staff_Rank") == null )
				this.Staff_Rank = null;
			else
				this.Staff_Rank = rs.getString("Staff_Rank").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的Staff_Info表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Staff_InfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public Staff_InfoSchema getSchema()
	{
		Staff_InfoSchema aStaff_InfoSchema = new Staff_InfoSchema();
		aStaff_InfoSchema.setSchema(this);
		return aStaff_InfoSchema;
	}

	public Staff_InfoDB getDB()
	{
		Staff_InfoDB aDBOper = new Staff_InfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStaff_Info描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Staff_No)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Staff_Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Id_type)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Idno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(quano)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( JoinDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LeaveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Edu_Degree)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Accreditation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Depart_info)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Is_Leader)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Staff_Rank));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStaff_Info>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Staff_No = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Staff_Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Id_type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Idno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			quano = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			JoinDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			LeaveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			Edu_Degree = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Accreditation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Depart_info = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Is_Leader = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Staff_Rank = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Staff_InfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Staff_No"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Staff_No));
		}
		if (FCode.equals("Staff_Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Staff_Name));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("Id_type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Id_type));
		}
		if (FCode.equals("Idno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Idno));
		}
		if (FCode.equals("quano"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(quano));
		}
		if (FCode.equals("JoinDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getJoinDate()));
		}
		if (FCode.equals("LeaveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLeaveDate()));
		}
		if (FCode.equals("Edu_Degree"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Edu_Degree));
		}
		if (FCode.equals("Accreditation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Accreditation));
		}
		if (FCode.equals("Depart_info"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Depart_info));
		}
		if (FCode.equals("Is_Leader"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Is_Leader));
		}
		if (FCode.equals("Staff_Rank"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Staff_Rank));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Staff_No);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Staff_Name);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Id_type);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Idno);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(quano);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getJoinDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLeaveDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Edu_Degree);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Accreditation);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Depart_info);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Is_Leader);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Staff_Rank);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Staff_No"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Staff_No = FValue.trim();
			}
			else
				Staff_No = null;
		}
		if (FCode.equalsIgnoreCase("Staff_Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Staff_Name = FValue.trim();
			}
			else
				Staff_Name = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("Id_type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Id_type = FValue.trim();
			}
			else
				Id_type = null;
		}
		if (FCode.equalsIgnoreCase("Idno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Idno = FValue.trim();
			}
			else
				Idno = null;
		}
		if (FCode.equalsIgnoreCase("quano"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				quano = FValue.trim();
			}
			else
				quano = null;
		}
		if (FCode.equalsIgnoreCase("JoinDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				JoinDate = fDate.getDate( FValue );
			}
			else
				JoinDate = null;
		}
		if (FCode.equalsIgnoreCase("LeaveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LeaveDate = fDate.getDate( FValue );
			}
			else
				LeaveDate = null;
		}
		if (FCode.equalsIgnoreCase("Edu_Degree"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Edu_Degree = FValue.trim();
			}
			else
				Edu_Degree = null;
		}
		if (FCode.equalsIgnoreCase("Accreditation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Accreditation = FValue.trim();
			}
			else
				Accreditation = null;
		}
		if (FCode.equalsIgnoreCase("Depart_info"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Depart_info = FValue.trim();
			}
			else
				Depart_info = null;
		}
		if (FCode.equalsIgnoreCase("Is_Leader"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Is_Leader = FValue.trim();
			}
			else
				Is_Leader = null;
		}
		if (FCode.equalsIgnoreCase("Staff_Rank"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Staff_Rank = FValue.trim();
			}
			else
				Staff_Rank = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		Staff_InfoSchema other = (Staff_InfoSchema)otherObject;
		return
			Staff_No.equals(other.getStaff_No())
			&& Staff_Name.equals(other.getStaff_Name())
			&& Sex.equals(other.getSex())
			&& Id_type.equals(other.getId_type())
			&& Idno.equals(other.getIdno())
			&& quano.equals(other.getquano())
			&& fDate.getString(JoinDate).equals(other.getJoinDate())
			&& fDate.getString(LeaveDate).equals(other.getLeaveDate())
			&& Edu_Degree.equals(other.getEdu_Degree())
			&& Accreditation.equals(other.getAccreditation())
			&& Depart_info.equals(other.getDepart_info())
			&& Is_Leader.equals(other.getIs_Leader())
			&& Staff_Rank.equals(other.getStaff_Rank());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Staff_No") ) {
			return 0;
		}
		if( strFieldName.equals("Staff_Name") ) {
			return 1;
		}
		if( strFieldName.equals("Sex") ) {
			return 2;
		}
		if( strFieldName.equals("Id_type") ) {
			return 3;
		}
		if( strFieldName.equals("Idno") ) {
			return 4;
		}
		if( strFieldName.equals("quano") ) {
			return 5;
		}
		if( strFieldName.equals("JoinDate") ) {
			return 6;
		}
		if( strFieldName.equals("LeaveDate") ) {
			return 7;
		}
		if( strFieldName.equals("Edu_Degree") ) {
			return 8;
		}
		if( strFieldName.equals("Accreditation") ) {
			return 9;
		}
		if( strFieldName.equals("Depart_info") ) {
			return 10;
		}
		if( strFieldName.equals("Is_Leader") ) {
			return 11;
		}
		if( strFieldName.equals("Staff_Rank") ) {
			return 12;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Staff_No";
				break;
			case 1:
				strFieldName = "Staff_Name";
				break;
			case 2:
				strFieldName = "Sex";
				break;
			case 3:
				strFieldName = "Id_type";
				break;
			case 4:
				strFieldName = "Idno";
				break;
			case 5:
				strFieldName = "quano";
				break;
			case 6:
				strFieldName = "JoinDate";
				break;
			case 7:
				strFieldName = "LeaveDate";
				break;
			case 8:
				strFieldName = "Edu_Degree";
				break;
			case 9:
				strFieldName = "Accreditation";
				break;
			case 10:
				strFieldName = "Depart_info";
				break;
			case 11:
				strFieldName = "Is_Leader";
				break;
			case 12:
				strFieldName = "Staff_Rank";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Staff_No") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Staff_Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Id_type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Idno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("quano") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JoinDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LeaveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Edu_Degree") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Accreditation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Depart_info") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Is_Leader") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Staff_Rank") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
