/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATEMPMISIONDB;

/*
 * <p>ClassName: LATEMPMISIONSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-08-22
 */
public class LATEMPMISIONSchema implements Schema, Cloneable
{
	// @Field
	/** 系列号 */
	private String CommisionSN;
	/** 部团队代码 */
	private String Department;
	/** 区团队代码 */
	private String Section;
	/** 处团队代码 */
	private String Branch;
	/** 佣金计算年月代码 */
	private String WageNo;
	/** 交易入机日期 */
	private Date TMakeDate;

	public static final int FIELDNUM = 6;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LATEMPMISIONSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CommisionSN";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LATEMPMISIONSchema cloned = (LATEMPMISIONSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCommisionSN()
	{
		return CommisionSN;
	}
	public void setCommisionSN(String aCommisionSN)
	{
		CommisionSN = aCommisionSN;
	}
	public String getDepartment()
	{
		return Department;
	}
	public void setDepartment(String aDepartment)
	{
		Department = aDepartment;
	}
	public String getSection()
	{
		return Section;
	}
	public void setSection(String aSection)
	{
		Section = aSection;
	}
	public String getBranch()
	{
		return Branch;
	}
	public void setBranch(String aBranch)
	{
		Branch = aBranch;
	}
	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public String getTMakeDate()
	{
		if( TMakeDate != null )
			return fDate.getString(TMakeDate);
		else
			return null;
	}
	public void setTMakeDate(Date aTMakeDate)
	{
		TMakeDate = aTMakeDate;
	}
	public void setTMakeDate(String aTMakeDate)
	{
		if (aTMakeDate != null && !aTMakeDate.equals("") )
		{
			TMakeDate = fDate.getDate( aTMakeDate );
		}
		else
			TMakeDate = null;
	}


	/**
	* 使用另外一个 LATEMPMISIONSchema 对象给 Schema 赋值
	* @param: aLATEMPMISIONSchema LATEMPMISIONSchema
	**/
	public void setSchema(LATEMPMISIONSchema aLATEMPMISIONSchema)
	{
		this.CommisionSN = aLATEMPMISIONSchema.getCommisionSN();
		this.Department = aLATEMPMISIONSchema.getDepartment();
		this.Section = aLATEMPMISIONSchema.getSection();
		this.Branch = aLATEMPMISIONSchema.getBranch();
		this.WageNo = aLATEMPMISIONSchema.getWageNo();
		this.TMakeDate = fDate.getDate( aLATEMPMISIONSchema.getTMakeDate());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CommisionSN") == null )
				this.CommisionSN = null;
			else
				this.CommisionSN = rs.getString("CommisionSN").trim();

			if( rs.getString("Department") == null )
				this.Department = null;
			else
				this.Department = rs.getString("Department").trim();

			if( rs.getString("Section") == null )
				this.Section = null;
			else
				this.Section = rs.getString("Section").trim();

			if( rs.getString("Branch") == null )
				this.Branch = null;
			else
				this.Branch = rs.getString("Branch").trim();

			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			this.TMakeDate = rs.getDate("TMakeDate");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LATEMPMISION表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATEMPMISIONSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LATEMPMISIONSchema getSchema()
	{
		LATEMPMISIONSchema aLATEMPMISIONSchema = new LATEMPMISIONSchema();
		aLATEMPMISIONSchema.setSchema(this);
		return aLATEMPMISIONSchema;
	}

	public LATEMPMISIONDB getDB()
	{
		LATEMPMISIONDB aDBOper = new LATEMPMISIONDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATEMPMISION描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CommisionSN)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Department)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Section)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Branch)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TMakeDate )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATEMPMISION>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CommisionSN = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Department = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Section = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Branch = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			TMakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATEMPMISIONSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CommisionSN"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CommisionSN));
		}
		if (FCode.equals("Department"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Department));
		}
		if (FCode.equals("Section"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Section));
		}
		if (FCode.equals("Branch"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Branch));
		}
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("TMakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTMakeDate()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CommisionSN);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Department);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Section);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Branch);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTMakeDate()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CommisionSN"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CommisionSN = FValue.trim();
			}
			else
				CommisionSN = null;
		}
		if (FCode.equalsIgnoreCase("Department"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Department = FValue.trim();
			}
			else
				Department = null;
		}
		if (FCode.equalsIgnoreCase("Section"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Section = FValue.trim();
			}
			else
				Section = null;
		}
		if (FCode.equalsIgnoreCase("Branch"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Branch = FValue.trim();
			}
			else
				Branch = null;
		}
		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("TMakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TMakeDate = fDate.getDate( FValue );
			}
			else
				TMakeDate = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LATEMPMISIONSchema other = (LATEMPMISIONSchema)otherObject;
		return
			(CommisionSN == null ? other.getCommisionSN() == null : CommisionSN.equals(other.getCommisionSN()))
			&& (Department == null ? other.getDepartment() == null : Department.equals(other.getDepartment()))
			&& (Section == null ? other.getSection() == null : Section.equals(other.getSection()))
			&& (Branch == null ? other.getBranch() == null : Branch.equals(other.getBranch()))
			&& (WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& (TMakeDate == null ? other.getTMakeDate() == null : fDate.getString(TMakeDate).equals(other.getTMakeDate()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CommisionSN") ) {
			return 0;
		}
		if( strFieldName.equals("Department") ) {
			return 1;
		}
		if( strFieldName.equals("Section") ) {
			return 2;
		}
		if( strFieldName.equals("Branch") ) {
			return 3;
		}
		if( strFieldName.equals("WageNo") ) {
			return 4;
		}
		if( strFieldName.equals("TMakeDate") ) {
			return 5;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CommisionSN";
				break;
			case 1:
				strFieldName = "Department";
				break;
			case 2:
				strFieldName = "Section";
				break;
			case 3:
				strFieldName = "Branch";
				break;
			case 4:
				strFieldName = "WageNo";
				break;
			case 5:
				strFieldName = "TMakeDate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CommisionSN") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Department") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Section") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Branch") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TMakeDate") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
