/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLOutcoucingAverageDB;

/*
 * <p>ClassName: LLOutcoucingAverageSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2013-12-25
 */
public class LLOutcoucingAverageSchema implements Schema, Cloneable
{
	// @Field
	/** 批次号 */
	private String Batchno;
	/** 保单号 */
	private String Grpcontno;
	/** 外包平均费用 */
	private double Osaverage;
	/** 备用字段p1 */
	private String P1;
	/** 备用字段p2 */
	private String P2;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date Makedate;
	/** 入机时间 */
	private String Maketime;
	/** 修改日期 */
	private Date Modifydate;
	/** 修改时间 */
	private String Modifytime;

	public static final int FIELDNUM = 10;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLOutcoucingAverageSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "Batchno";
		pk[1] = "Grpcontno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLOutcoucingAverageSchema cloned = (LLOutcoucingAverageSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchno()
	{
		return Batchno;
	}
	public void setBatchno(String aBatchno)
	{
		Batchno = aBatchno;
	}
	public String getGrpcontno()
	{
		return Grpcontno;
	}
	public void setGrpcontno(String aGrpcontno)
	{
		Grpcontno = aGrpcontno;
	}
	public double getOsaverage()
	{
		return Osaverage;
	}
	public void setOsaverage(double aOsaverage)
	{
		Osaverage = Arith.round(aOsaverage,2);
	}
	public void setOsaverage(String aOsaverage)
	{
		if (aOsaverage != null && !aOsaverage.equals(""))
		{
			Double tDouble = new Double(aOsaverage);
			double d = tDouble.doubleValue();
                Osaverage = Arith.round(d,2);
		}
	}

	public String getP1()
	{
		return P1;
	}
	public void setP1(String aP1)
	{
		P1 = aP1;
	}
	public String getP2()
	{
		return P2;
	}
	public void setP2(String aP2)
	{
		P2 = aP2;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakedate()
	{
		if( Makedate != null )
			return fDate.getString(Makedate);
		else
			return null;
	}
	public void setMakedate(Date aMakedate)
	{
		Makedate = aMakedate;
	}
	public void setMakedate(String aMakedate)
	{
		if (aMakedate != null && !aMakedate.equals("") )
		{
			Makedate = fDate.getDate( aMakedate );
		}
		else
			Makedate = null;
	}

	public String getMaketime()
	{
		return Maketime;
	}
	public void setMaketime(String aMaketime)
	{
		Maketime = aMaketime;
	}
	public String getModifydate()
	{
		if( Modifydate != null )
			return fDate.getString(Modifydate);
		else
			return null;
	}
	public void setModifydate(Date aModifydate)
	{
		Modifydate = aModifydate;
	}
	public void setModifydate(String aModifydate)
	{
		if (aModifydate != null && !aModifydate.equals("") )
		{
			Modifydate = fDate.getDate( aModifydate );
		}
		else
			Modifydate = null;
	}

	public String getModifytime()
	{
		return Modifytime;
	}
	public void setModifytime(String aModifytime)
	{
		Modifytime = aModifytime;
	}

	/**
	* 使用另外一个 LLOutcoucingAverageSchema 对象给 Schema 赋值
	* @param: aLLOutcoucingAverageSchema LLOutcoucingAverageSchema
	**/
	public void setSchema(LLOutcoucingAverageSchema aLLOutcoucingAverageSchema)
	{
		this.Batchno = aLLOutcoucingAverageSchema.getBatchno();
		this.Grpcontno = aLLOutcoucingAverageSchema.getGrpcontno();
		this.Osaverage = aLLOutcoucingAverageSchema.getOsaverage();
		this.P1 = aLLOutcoucingAverageSchema.getP1();
		this.P2 = aLLOutcoucingAverageSchema.getP2();
		this.Operator = aLLOutcoucingAverageSchema.getOperator();
		this.Makedate = fDate.getDate( aLLOutcoucingAverageSchema.getMakedate());
		this.Maketime = aLLOutcoucingAverageSchema.getMaketime();
		this.Modifydate = fDate.getDate( aLLOutcoucingAverageSchema.getModifydate());
		this.Modifytime = aLLOutcoucingAverageSchema.getModifytime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Batchno") == null )
				this.Batchno = null;
			else
				this.Batchno = rs.getString("Batchno").trim();

			if( rs.getString("Grpcontno") == null )
				this.Grpcontno = null;
			else
				this.Grpcontno = rs.getString("Grpcontno").trim();

			this.Osaverage = rs.getDouble("Osaverage");
			if( rs.getString("P1") == null )
				this.P1 = null;
			else
				this.P1 = rs.getString("P1").trim();

			if( rs.getString("P2") == null )
				this.P2 = null;
			else
				this.P2 = rs.getString("P2").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.Makedate = rs.getDate("Makedate");
			if( rs.getString("Maketime") == null )
				this.Maketime = null;
			else
				this.Maketime = rs.getString("Maketime").trim();

			this.Modifydate = rs.getDate("Modifydate");
			if( rs.getString("Modifytime") == null )
				this.Modifytime = null;
			else
				this.Modifytime = rs.getString("Modifytime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLOutcoucingAverage表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLOutcoucingAverageSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLOutcoucingAverageSchema getSchema()
	{
		LLOutcoucingAverageSchema aLLOutcoucingAverageSchema = new LLOutcoucingAverageSchema();
		aLLOutcoucingAverageSchema.setSchema(this);
		return aLLOutcoucingAverageSchema;
	}

	public LLOutcoucingAverageDB getDB()
	{
		LLOutcoucingAverageDB aDBOper = new LLOutcoucingAverageDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLOutcoucingAverage描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Batchno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Grpcontno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Osaverage));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(P2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Maketime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Modifydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Modifytime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLOutcoucingAverage>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Batchno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Grpcontno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Osaverage = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).doubleValue();
			P1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			P2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			Maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Modifydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			Modifytime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLOutcoucingAverageSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Batchno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Batchno));
		}
		if (FCode.equals("Grpcontno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Grpcontno));
		}
		if (FCode.equals("Osaverage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Osaverage));
		}
		if (FCode.equals("P1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P1));
		}
		if (FCode.equals("P2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(P2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
		}
		if (FCode.equals("Maketime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Maketime));
		}
		if (FCode.equals("Modifydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifydate()));
		}
		if (FCode.equals("Modifytime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Modifytime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Batchno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Grpcontno);
				break;
			case 2:
				strFieldValue = String.valueOf(Osaverage);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(P1);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(P2);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Maketime);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifydate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Modifytime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Batchno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Batchno = FValue.trim();
			}
			else
				Batchno = null;
		}
		if (FCode.equalsIgnoreCase("Grpcontno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Grpcontno = FValue.trim();
			}
			else
				Grpcontno = null;
		}
		if (FCode.equalsIgnoreCase("Osaverage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Osaverage = d;
			}
		}
		if (FCode.equalsIgnoreCase("P1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P1 = FValue.trim();
			}
			else
				P1 = null;
		}
		if (FCode.equalsIgnoreCase("P2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				P2 = FValue.trim();
			}
			else
				P2 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Makedate = fDate.getDate( FValue );
			}
			else
				Makedate = null;
		}
		if (FCode.equalsIgnoreCase("Maketime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Maketime = FValue.trim();
			}
			else
				Maketime = null;
		}
		if (FCode.equalsIgnoreCase("Modifydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Modifydate = fDate.getDate( FValue );
			}
			else
				Modifydate = null;
		}
		if (FCode.equalsIgnoreCase("Modifytime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Modifytime = FValue.trim();
			}
			else
				Modifytime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLOutcoucingAverageSchema other = (LLOutcoucingAverageSchema)otherObject;
		return
			(Batchno == null ? other.getBatchno() == null : Batchno.equals(other.getBatchno()))
			&& (Grpcontno == null ? other.getGrpcontno() == null : Grpcontno.equals(other.getGrpcontno()))
			&& Osaverage == other.getOsaverage()
			&& (P1 == null ? other.getP1() == null : P1.equals(other.getP1()))
			&& (P2 == null ? other.getP2() == null : P2.equals(other.getP2()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (Makedate == null ? other.getMakedate() == null : fDate.getString(Makedate).equals(other.getMakedate()))
			&& (Maketime == null ? other.getMaketime() == null : Maketime.equals(other.getMaketime()))
			&& (Modifydate == null ? other.getModifydate() == null : fDate.getString(Modifydate).equals(other.getModifydate()))
			&& (Modifytime == null ? other.getModifytime() == null : Modifytime.equals(other.getModifytime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Batchno") ) {
			return 0;
		}
		if( strFieldName.equals("Grpcontno") ) {
			return 1;
		}
		if( strFieldName.equals("Osaverage") ) {
			return 2;
		}
		if( strFieldName.equals("P1") ) {
			return 3;
		}
		if( strFieldName.equals("P2") ) {
			return 4;
		}
		if( strFieldName.equals("Operator") ) {
			return 5;
		}
		if( strFieldName.equals("Makedate") ) {
			return 6;
		}
		if( strFieldName.equals("Maketime") ) {
			return 7;
		}
		if( strFieldName.equals("Modifydate") ) {
			return 8;
		}
		if( strFieldName.equals("Modifytime") ) {
			return 9;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Batchno";
				break;
			case 1:
				strFieldName = "Grpcontno";
				break;
			case 2:
				strFieldName = "Osaverage";
				break;
			case 3:
				strFieldName = "P1";
				break;
			case 4:
				strFieldName = "P2";
				break;
			case 5:
				strFieldName = "Operator";
				break;
			case 6:
				strFieldName = "Makedate";
				break;
			case 7:
				strFieldName = "Maketime";
				break;
			case 8:
				strFieldName = "Modifydate";
				break;
			case 9:
				strFieldName = "Modifytime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Batchno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Grpcontno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Osaverage") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("P1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("P2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Maketime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Modifydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Modifytime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
