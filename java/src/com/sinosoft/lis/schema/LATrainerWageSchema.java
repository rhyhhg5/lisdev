/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATrainerWageDB;

/*
 * <p>ClassName: LATrainerWageSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险培训组训信息表
 * @CreateDate：2018-05-03
 */
public class LATrainerWageSchema implements Schema, Cloneable
{
	// @Field
	/** 薪资年月 */
	private String WageNo;
	/** 组训人员工号 */
	private String TrainerCode;
	/** 组训职级 */
	private String TrainerGrade;
	/** 营业部 */
	private String AgentGroup;
	/** 管理机构 */
	private String ManageCom;
	/** 基本工资 */
	private double BasicWage;
	/** 岗位津贴 */
	private double JobSubsidies;
	/** 组训月绩效工资 */
	private double TPMoney;
	/** 加扣款金额 */
	private double ADMoney;
	/** 应发工资 */
	private double ShouldMoney;
	/** 发放状态 */
	private String State;
	/** 展业类型 */
	private String BranchType;
	/** 渠道 */
	private String BranchType2;
	/** 展业机构外部编码 */
	private String BranchAttr;
	/** 备用佣金项01 */
	private double W03;
	/** 备用佣金项02 */
	private double W04;
	/** 备用佣金项03 */
	private double W05;
	/** 备用佣金项04 */
	private double W06;
	/** 备用佣金项05 */
	private double W07;
	/** 备用佣金项06 */
	private double W08;
	/** 备用佣金项07 */
	private double W09;
	/** 备用佣金项08 */
	private double W10;
	/** 备用佣金项09 */
	private double W11;
	/** 备用佣金项10 */
	private double W12;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 29;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LATrainerWageSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "WageNo";
		pk[1] = "TrainerCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LATrainerWageSchema cloned = (LATrainerWageSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public String getTrainerCode()
	{
		return TrainerCode;
	}
	public void setTrainerCode(String aTrainerCode)
	{
		TrainerCode = aTrainerCode;
	}
	public String getTrainerGrade()
	{
		return TrainerGrade;
	}
	public void setTrainerGrade(String aTrainerGrade)
	{
		TrainerGrade = aTrainerGrade;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public double getBasicWage()
	{
		return BasicWage;
	}
	public void setBasicWage(double aBasicWage)
	{
		BasicWage = Arith.round(aBasicWage,2);
	}
	public void setBasicWage(String aBasicWage)
	{
		if (aBasicWage != null && !aBasicWage.equals(""))
		{
			Double tDouble = new Double(aBasicWage);
			double d = tDouble.doubleValue();
                BasicWage = Arith.round(d,2);
		}
	}

	public double getJobSubsidies()
	{
		return JobSubsidies;
	}
	public void setJobSubsidies(double aJobSubsidies)
	{
		JobSubsidies = Arith.round(aJobSubsidies,2);
	}
	public void setJobSubsidies(String aJobSubsidies)
	{
		if (aJobSubsidies != null && !aJobSubsidies.equals(""))
		{
			Double tDouble = new Double(aJobSubsidies);
			double d = tDouble.doubleValue();
                JobSubsidies = Arith.round(d,2);
		}
	}

	public double getTPMoney()
	{
		return TPMoney;
	}
	public void setTPMoney(double aTPMoney)
	{
		TPMoney = Arith.round(aTPMoney,2);
	}
	public void setTPMoney(String aTPMoney)
	{
		if (aTPMoney != null && !aTPMoney.equals(""))
		{
			Double tDouble = new Double(aTPMoney);
			double d = tDouble.doubleValue();
                TPMoney = Arith.round(d,2);
		}
	}

	public double getADMoney()
	{
		return ADMoney;
	}
	public void setADMoney(double aADMoney)
	{
		ADMoney = Arith.round(aADMoney,2);
	}
	public void setADMoney(String aADMoney)
	{
		if (aADMoney != null && !aADMoney.equals(""))
		{
			Double tDouble = new Double(aADMoney);
			double d = tDouble.doubleValue();
                ADMoney = Arith.round(d,2);
		}
	}

	public double getShouldMoney()
	{
		return ShouldMoney;
	}
	public void setShouldMoney(double aShouldMoney)
	{
		ShouldMoney = Arith.round(aShouldMoney,2);
	}
	public void setShouldMoney(String aShouldMoney)
	{
		if (aShouldMoney != null && !aShouldMoney.equals(""))
		{
			Double tDouble = new Double(aShouldMoney);
			double d = tDouble.doubleValue();
                ShouldMoney = Arith.round(d,2);
		}
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getBranchAttr()
	{
		return BranchAttr;
	}
	public void setBranchAttr(String aBranchAttr)
	{
		BranchAttr = aBranchAttr;
	}
	public double getW03()
	{
		return W03;
	}
	public void setW03(double aW03)
	{
		W03 = Arith.round(aW03,2);
	}
	public void setW03(String aW03)
	{
		if (aW03 != null && !aW03.equals(""))
		{
			Double tDouble = new Double(aW03);
			double d = tDouble.doubleValue();
                W03 = Arith.round(d,2);
		}
	}

	public double getW04()
	{
		return W04;
	}
	public void setW04(double aW04)
	{
		W04 = Arith.round(aW04,2);
	}
	public void setW04(String aW04)
	{
		if (aW04 != null && !aW04.equals(""))
		{
			Double tDouble = new Double(aW04);
			double d = tDouble.doubleValue();
                W04 = Arith.round(d,2);
		}
	}

	public double getW05()
	{
		return W05;
	}
	public void setW05(double aW05)
	{
		W05 = Arith.round(aW05,2);
	}
	public void setW05(String aW05)
	{
		if (aW05 != null && !aW05.equals(""))
		{
			Double tDouble = new Double(aW05);
			double d = tDouble.doubleValue();
                W05 = Arith.round(d,2);
		}
	}

	public double getW06()
	{
		return W06;
	}
	public void setW06(double aW06)
	{
		W06 = Arith.round(aW06,2);
	}
	public void setW06(String aW06)
	{
		if (aW06 != null && !aW06.equals(""))
		{
			Double tDouble = new Double(aW06);
			double d = tDouble.doubleValue();
                W06 = Arith.round(d,2);
		}
	}

	public double getW07()
	{
		return W07;
	}
	public void setW07(double aW07)
	{
		W07 = Arith.round(aW07,2);
	}
	public void setW07(String aW07)
	{
		if (aW07 != null && !aW07.equals(""))
		{
			Double tDouble = new Double(aW07);
			double d = tDouble.doubleValue();
                W07 = Arith.round(d,2);
		}
	}

	public double getW08()
	{
		return W08;
	}
	public void setW08(double aW08)
	{
		W08 = Arith.round(aW08,2);
	}
	public void setW08(String aW08)
	{
		if (aW08 != null && !aW08.equals(""))
		{
			Double tDouble = new Double(aW08);
			double d = tDouble.doubleValue();
                W08 = Arith.round(d,2);
		}
	}

	public double getW09()
	{
		return W09;
	}
	public void setW09(double aW09)
	{
		W09 = Arith.round(aW09,2);
	}
	public void setW09(String aW09)
	{
		if (aW09 != null && !aW09.equals(""))
		{
			Double tDouble = new Double(aW09);
			double d = tDouble.doubleValue();
                W09 = Arith.round(d,2);
		}
	}

	public double getW10()
	{
		return W10;
	}
	public void setW10(double aW10)
	{
		W10 = Arith.round(aW10,2);
	}
	public void setW10(String aW10)
	{
		if (aW10 != null && !aW10.equals(""))
		{
			Double tDouble = new Double(aW10);
			double d = tDouble.doubleValue();
                W10 = Arith.round(d,2);
		}
	}

	public double getW11()
	{
		return W11;
	}
	public void setW11(double aW11)
	{
		W11 = Arith.round(aW11,2);
	}
	public void setW11(String aW11)
	{
		if (aW11 != null && !aW11.equals(""))
		{
			Double tDouble = new Double(aW11);
			double d = tDouble.doubleValue();
                W11 = Arith.round(d,2);
		}
	}

	public double getW12()
	{
		return W12;
	}
	public void setW12(double aW12)
	{
		W12 = Arith.round(aW12,2);
	}
	public void setW12(String aW12)
	{
		if (aW12 != null && !aW12.equals(""))
		{
			Double tDouble = new Double(aW12);
			double d = tDouble.doubleValue();
                W12 = Arith.round(d,2);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LATrainerWageSchema 对象给 Schema 赋值
	* @param: aLATrainerWageSchema LATrainerWageSchema
	**/
	public void setSchema(LATrainerWageSchema aLATrainerWageSchema)
	{
		this.WageNo = aLATrainerWageSchema.getWageNo();
		this.TrainerCode = aLATrainerWageSchema.getTrainerCode();
		this.TrainerGrade = aLATrainerWageSchema.getTrainerGrade();
		this.AgentGroup = aLATrainerWageSchema.getAgentGroup();
		this.ManageCom = aLATrainerWageSchema.getManageCom();
		this.BasicWage = aLATrainerWageSchema.getBasicWage();
		this.JobSubsidies = aLATrainerWageSchema.getJobSubsidies();
		this.TPMoney = aLATrainerWageSchema.getTPMoney();
		this.ADMoney = aLATrainerWageSchema.getADMoney();
		this.ShouldMoney = aLATrainerWageSchema.getShouldMoney();
		this.State = aLATrainerWageSchema.getState();
		this.BranchType = aLATrainerWageSchema.getBranchType();
		this.BranchType2 = aLATrainerWageSchema.getBranchType2();
		this.BranchAttr = aLATrainerWageSchema.getBranchAttr();
		this.W03 = aLATrainerWageSchema.getW03();
		this.W04 = aLATrainerWageSchema.getW04();
		this.W05 = aLATrainerWageSchema.getW05();
		this.W06 = aLATrainerWageSchema.getW06();
		this.W07 = aLATrainerWageSchema.getW07();
		this.W08 = aLATrainerWageSchema.getW08();
		this.W09 = aLATrainerWageSchema.getW09();
		this.W10 = aLATrainerWageSchema.getW10();
		this.W11 = aLATrainerWageSchema.getW11();
		this.W12 = aLATrainerWageSchema.getW12();
		this.Operator = aLATrainerWageSchema.getOperator();
		this.MakeDate = fDate.getDate( aLATrainerWageSchema.getMakeDate());
		this.MakeTime = aLATrainerWageSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLATrainerWageSchema.getModifyDate());
		this.ModifyTime = aLATrainerWageSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			if( rs.getString("TrainerCode") == null )
				this.TrainerCode = null;
			else
				this.TrainerCode = rs.getString("TrainerCode").trim();

			if( rs.getString("TrainerGrade") == null )
				this.TrainerGrade = null;
			else
				this.TrainerGrade = rs.getString("TrainerGrade").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.BasicWage = rs.getDouble("BasicWage");
			this.JobSubsidies = rs.getDouble("JobSubsidies");
			this.TPMoney = rs.getDouble("TPMoney");
			this.ADMoney = rs.getDouble("ADMoney");
			this.ShouldMoney = rs.getDouble("ShouldMoney");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("BranchAttr") == null )
				this.BranchAttr = null;
			else
				this.BranchAttr = rs.getString("BranchAttr").trim();

			this.W03 = rs.getDouble("W03");
			this.W04 = rs.getDouble("W04");
			this.W05 = rs.getDouble("W05");
			this.W06 = rs.getDouble("W06");
			this.W07 = rs.getDouble("W07");
			this.W08 = rs.getDouble("W08");
			this.W09 = rs.getDouble("W09");
			this.W10 = rs.getDouble("W10");
			this.W11 = rs.getDouble("W11");
			this.W12 = rs.getDouble("W12");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LATrainerWage表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainerWageSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LATrainerWageSchema getSchema()
	{
		LATrainerWageSchema aLATrainerWageSchema = new LATrainerWageSchema();
		aLATrainerWageSchema.setSchema(this);
		return aLATrainerWageSchema;
	}

	public LATrainerWageDB getDB()
	{
		LATrainerWageDB aDBOper = new LATrainerWageDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainerWage描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainerCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TrainerGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(BasicWage));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(JobSubsidies));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TPMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ADMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ShouldMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchAttr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W03));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W04));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W05));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W06));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W07));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W08));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W09));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W10));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W11));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W12));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainerWage>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			TrainerCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			TrainerGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BasicWage = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			JobSubsidies = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			TPMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			ADMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			ShouldMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			W03 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			W04 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			W05 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			W06 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			W07 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,19,SysConst.PACKAGESPILTER))).doubleValue();
			W08 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			W09 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).doubleValue();
			W10 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).doubleValue();
			W11 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,23,SysConst.PACKAGESPILTER))).doubleValue();
			W12 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,24,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainerWageSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("TrainerCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerCode));
		}
		if (FCode.equals("TrainerGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TrainerGrade));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("BasicWage"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BasicWage));
		}
		if (FCode.equals("JobSubsidies"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(JobSubsidies));
		}
		if (FCode.equals("TPMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TPMoney));
		}
		if (FCode.equals("ADMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ADMoney));
		}
		if (FCode.equals("ShouldMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShouldMoney));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("BranchAttr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
		}
		if (FCode.equals("W03"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W03));
		}
		if (FCode.equals("W04"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W04));
		}
		if (FCode.equals("W05"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W05));
		}
		if (FCode.equals("W06"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W06));
		}
		if (FCode.equals("W07"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W07));
		}
		if (FCode.equals("W08"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W08));
		}
		if (FCode.equals("W09"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W09));
		}
		if (FCode.equals("W10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W10));
		}
		if (FCode.equals("W11"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W11));
		}
		if (FCode.equals("W12"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W12));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(TrainerCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(TrainerGrade);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 5:
				strFieldValue = String.valueOf(BasicWage);
				break;
			case 6:
				strFieldValue = String.valueOf(JobSubsidies);
				break;
			case 7:
				strFieldValue = String.valueOf(TPMoney);
				break;
			case 8:
				strFieldValue = String.valueOf(ADMoney);
				break;
			case 9:
				strFieldValue = String.valueOf(ShouldMoney);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(BranchAttr);
				break;
			case 14:
				strFieldValue = String.valueOf(W03);
				break;
			case 15:
				strFieldValue = String.valueOf(W04);
				break;
			case 16:
				strFieldValue = String.valueOf(W05);
				break;
			case 17:
				strFieldValue = String.valueOf(W06);
				break;
			case 18:
				strFieldValue = String.valueOf(W07);
				break;
			case 19:
				strFieldValue = String.valueOf(W08);
				break;
			case 20:
				strFieldValue = String.valueOf(W09);
				break;
			case 21:
				strFieldValue = String.valueOf(W10);
				break;
			case 22:
				strFieldValue = String.valueOf(W11);
				break;
			case 23:
				strFieldValue = String.valueOf(W12);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("TrainerCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerCode = FValue.trim();
			}
			else
				TrainerCode = null;
		}
		if (FCode.equalsIgnoreCase("TrainerGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TrainerGrade = FValue.trim();
			}
			else
				TrainerGrade = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("BasicWage"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				BasicWage = d;
			}
		}
		if (FCode.equalsIgnoreCase("JobSubsidies"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				JobSubsidies = d;
			}
		}
		if (FCode.equalsIgnoreCase("TPMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TPMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("ADMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ADMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("ShouldMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ShouldMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("BranchAttr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchAttr = FValue.trim();
			}
			else
				BranchAttr = null;
		}
		if (FCode.equalsIgnoreCase("W03"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W03 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W04"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W04 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W05"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W05 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W06"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W06 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W07"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W07 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W08"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W08 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W09"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W09 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W10 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W11"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W11 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W12"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W12 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LATrainerWageSchema other = (LATrainerWageSchema)otherObject;
		return
			(WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& (TrainerCode == null ? other.getTrainerCode() == null : TrainerCode.equals(other.getTrainerCode()))
			&& (TrainerGrade == null ? other.getTrainerGrade() == null : TrainerGrade.equals(other.getTrainerGrade()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& BasicWage == other.getBasicWage()
			&& JobSubsidies == other.getJobSubsidies()
			&& TPMoney == other.getTPMoney()
			&& ADMoney == other.getADMoney()
			&& ShouldMoney == other.getShouldMoney()
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (BranchAttr == null ? other.getBranchAttr() == null : BranchAttr.equals(other.getBranchAttr()))
			&& W03 == other.getW03()
			&& W04 == other.getW04()
			&& W05 == other.getW05()
			&& W06 == other.getW06()
			&& W07 == other.getW07()
			&& W08 == other.getW08()
			&& W09 == other.getW09()
			&& W10 == other.getW10()
			&& W11 == other.getW11()
			&& W12 == other.getW12()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("WageNo") ) {
			return 0;
		}
		if( strFieldName.equals("TrainerCode") ) {
			return 1;
		}
		if( strFieldName.equals("TrainerGrade") ) {
			return 2;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 3;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 4;
		}
		if( strFieldName.equals("BasicWage") ) {
			return 5;
		}
		if( strFieldName.equals("JobSubsidies") ) {
			return 6;
		}
		if( strFieldName.equals("TPMoney") ) {
			return 7;
		}
		if( strFieldName.equals("ADMoney") ) {
			return 8;
		}
		if( strFieldName.equals("ShouldMoney") ) {
			return 9;
		}
		if( strFieldName.equals("State") ) {
			return 10;
		}
		if( strFieldName.equals("BranchType") ) {
			return 11;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 12;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return 13;
		}
		if( strFieldName.equals("W03") ) {
			return 14;
		}
		if( strFieldName.equals("W04") ) {
			return 15;
		}
		if( strFieldName.equals("W05") ) {
			return 16;
		}
		if( strFieldName.equals("W06") ) {
			return 17;
		}
		if( strFieldName.equals("W07") ) {
			return 18;
		}
		if( strFieldName.equals("W08") ) {
			return 19;
		}
		if( strFieldName.equals("W09") ) {
			return 20;
		}
		if( strFieldName.equals("W10") ) {
			return 21;
		}
		if( strFieldName.equals("W11") ) {
			return 22;
		}
		if( strFieldName.equals("W12") ) {
			return 23;
		}
		if( strFieldName.equals("Operator") ) {
			return 24;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 25;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 26;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 27;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 28;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "WageNo";
				break;
			case 1:
				strFieldName = "TrainerCode";
				break;
			case 2:
				strFieldName = "TrainerGrade";
				break;
			case 3:
				strFieldName = "AgentGroup";
				break;
			case 4:
				strFieldName = "ManageCom";
				break;
			case 5:
				strFieldName = "BasicWage";
				break;
			case 6:
				strFieldName = "JobSubsidies";
				break;
			case 7:
				strFieldName = "TPMoney";
				break;
			case 8:
				strFieldName = "ADMoney";
				break;
			case 9:
				strFieldName = "ShouldMoney";
				break;
			case 10:
				strFieldName = "State";
				break;
			case 11:
				strFieldName = "BranchType";
				break;
			case 12:
				strFieldName = "BranchType2";
				break;
			case 13:
				strFieldName = "BranchAttr";
				break;
			case 14:
				strFieldName = "W03";
				break;
			case 15:
				strFieldName = "W04";
				break;
			case 16:
				strFieldName = "W05";
				break;
			case 17:
				strFieldName = "W06";
				break;
			case 18:
				strFieldName = "W07";
				break;
			case 19:
				strFieldName = "W08";
				break;
			case 20:
				strFieldName = "W09";
				break;
			case 21:
				strFieldName = "W10";
				break;
			case 22:
				strFieldName = "W11";
				break;
			case 23:
				strFieldName = "W12";
				break;
			case 24:
				strFieldName = "Operator";
				break;
			case 25:
				strFieldName = "MakeDate";
				break;
			case 26:
				strFieldName = "MakeTime";
				break;
			case 27:
				strFieldName = "ModifyDate";
				break;
			case 28:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TrainerGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BasicWage") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("JobSubsidies") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TPMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ADMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ShouldMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("W03") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W04") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W05") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W06") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W07") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W08") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W09") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W10") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W11") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W12") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 23:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
