/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIOperationDataClassDefDB;

/*
 * <p>ClassName: LIOperationDataClassDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2008-05-20
 */
public class LIOperationDataClassDefSchema implements Schema, Cloneable
{
	// @Field
	/** 数据类型编码 */
	private String ClassID;
	/** 数据类型名称 */
	private String ClassName;
	/** 数据提取方式 */
	private String DistillMode;
	/** 提数sql */
	private String DistillSQL;
	/** 提数处理类 */
	private String DistillClass;
	/** 数据类型描述 */
	private String Remark;
	/** 凭证类型 */
	private String ClassType;
	/** 状态 */
	private String State;

	public static final int FIELDNUM = 8;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIOperationDataClassDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ClassID";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LIOperationDataClassDefSchema cloned = (LIOperationDataClassDefSchema)super.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getClassID()
	{
		return ClassID;
	}
	public void setClassID(String aClassID)
	{
            ClassID = aClassID;
	}
	public String getClassName()
	{
		return ClassName;
	}
	public void setClassName(String aClassName)
	{
            ClassName = aClassName;
	}
	public String getDistillMode()
	{
		return DistillMode;
	}
	public void setDistillMode(String aDistillMode)
	{
            DistillMode = aDistillMode;
	}
	public String getDistillSQL()
	{
		return DistillSQL;
	}
	public void setDistillSQL(String aDistillSQL)
	{
            DistillSQL = aDistillSQL;
	}
	public String getDistillClass()
	{
		return DistillClass;
	}
	public void setDistillClass(String aDistillClass)
	{
            DistillClass = aDistillClass;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
            Remark = aRemark;
	}
	public String getClassType()
	{
		return ClassType;
	}
	public void setClassType(String aClassType)
	{
            ClassType = aClassType;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
            State = aState;
	}

	/**
	* 使用另外一个 LIOperationDataClassDefSchema 对象给 Schema 赋值
	* @param: aLIOperationDataClassDefSchema LIOperationDataClassDefSchema
	**/
	public void setSchema(LIOperationDataClassDefSchema aLIOperationDataClassDefSchema)
	{
		this.ClassID = aLIOperationDataClassDefSchema.getClassID();
		this.ClassName = aLIOperationDataClassDefSchema.getClassName();
		this.DistillMode = aLIOperationDataClassDefSchema.getDistillMode();
		this.DistillSQL = aLIOperationDataClassDefSchema.getDistillSQL();
		this.DistillClass = aLIOperationDataClassDefSchema.getDistillClass();
		this.Remark = aLIOperationDataClassDefSchema.getRemark();
		this.ClassType = aLIOperationDataClassDefSchema.getClassType();
		this.State = aLIOperationDataClassDefSchema.getState();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ClassID") == null )
				this.ClassID = null;
			else
				this.ClassID = rs.getString("ClassID").trim();

			if( rs.getString("ClassName") == null )
				this.ClassName = null;
			else
				this.ClassName = rs.getString("ClassName").trim();

			if( rs.getString("DistillMode") == null )
				this.DistillMode = null;
			else
				this.DistillMode = rs.getString("DistillMode").trim();

			if( rs.getString("DistillSQL") == null )
				this.DistillSQL = null;
			else
				this.DistillSQL = rs.getString("DistillSQL").trim();

			if( rs.getString("DistillClass") == null )
				this.DistillClass = null;
			else
				this.DistillClass = rs.getString("DistillClass").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("ClassType") == null )
				this.ClassType = null;
			else
				this.ClassType = rs.getString("ClassType").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIOperationDataClassDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIOperationDataClassDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIOperationDataClassDefSchema getSchema()
	{
		LIOperationDataClassDefSchema aLIOperationDataClassDefSchema = new LIOperationDataClassDefSchema();
		aLIOperationDataClassDefSchema.setSchema(this);
		return aLIOperationDataClassDefSchema;
	}

	public LIOperationDataClassDefDB getDB()
	{
		LIOperationDataClassDefDB aDBOper = new LIOperationDataClassDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIOperationDataClassDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ClassID)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ClassName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DistillMode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DistillSQL)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DistillClass)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ClassType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(State));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIOperationDataClassDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ClassID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ClassName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			DistillMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DistillSQL = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			DistillClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ClassType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIOperationDataClassDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ClassID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassID));
		}
		if (FCode.equals("ClassName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassName));
		}
		if (FCode.equals("DistillMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DistillMode));
		}
		if (FCode.equals("DistillSQL"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DistillSQL));
		}
		if (FCode.equals("DistillClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DistillClass));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("ClassType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClassType));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ClassID);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ClassName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(DistillMode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(DistillSQL);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(DistillClass);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ClassType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ClassID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassID = FValue.trim();
			}
			else
				ClassID = null;
		}
		if (FCode.equalsIgnoreCase("ClassName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassName = FValue.trim();
			}
			else
				ClassName = null;
		}
		if (FCode.equalsIgnoreCase("DistillMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DistillMode = FValue.trim();
			}
			else
				DistillMode = null;
		}
		if (FCode.equalsIgnoreCase("DistillSQL"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DistillSQL = FValue.trim();
			}
			else
				DistillSQL = null;
		}
		if (FCode.equalsIgnoreCase("DistillClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DistillClass = FValue.trim();
			}
			else
				DistillClass = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("ClassType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClassType = FValue.trim();
			}
			else
				ClassType = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIOperationDataClassDefSchema other = (LIOperationDataClassDefSchema)otherObject;
		return
			ClassID.equals(other.getClassID())
			&& ClassName.equals(other.getClassName())
			&& DistillMode.equals(other.getDistillMode())
			&& DistillSQL.equals(other.getDistillSQL())
			&& DistillClass.equals(other.getDistillClass())
			&& Remark.equals(other.getRemark())
			&& ClassType.equals(other.getClassType())
			&& State.equals(other.getState());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ClassID") ) {
			return 0;
		}
		if( strFieldName.equals("ClassName") ) {
			return 1;
		}
		if( strFieldName.equals("DistillMode") ) {
			return 2;
		}
		if( strFieldName.equals("DistillSQL") ) {
			return 3;
		}
		if( strFieldName.equals("DistillClass") ) {
			return 4;
		}
		if( strFieldName.equals("Remark") ) {
			return 5;
		}
		if( strFieldName.equals("ClassType") ) {
			return 6;
		}
		if( strFieldName.equals("State") ) {
			return 7;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ClassID";
				break;
			case 1:
				strFieldName = "ClassName";
				break;
			case 2:
				strFieldName = "DistillMode";
				break;
			case 3:
				strFieldName = "DistillSQL";
				break;
			case 4:
				strFieldName = "DistillClass";
				break;
			case 5:
				strFieldName = "Remark";
				break;
			case 6:
				strFieldName = "ClassType";
				break;
			case 7:
				strFieldName = "State";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ClassID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClassName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DistillMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DistillSQL") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DistillClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClassType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
