/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.rate2602DB;

/*
 * <p>ClassName: rate2602Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-09-06
 */
public class rate2602Schema implements Schema, Cloneable {
    // @Field
    /** 起始日期 */
    private Date StartDate;
    /** 管理机构 */
    private String ManageCom;
    /** 类型 */
    private String Type;
    /** 性别 */
    private String Sex;
    /** 年龄 */
    private int Age;
    /** 费率 */
    private double FeeRate;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public rate2602Schema() {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "StartDate";
        pk[1] = "ManageCom";
        pk[2] = "Type";
        pk[3] = "Sex";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        rate2602Schema cloned = (rate2602Schema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getStartDate() {
        if (StartDate != null) {
            return fDate.getString(StartDate);
        } else {
            return null;
        }
    }

    public void setStartDate(Date aStartDate) {
        StartDate = aStartDate;
    }

    public void setStartDate(String aStartDate) {
        if (aStartDate != null && !aStartDate.equals("")) {
            StartDate = fDate.getDate(aStartDate);
        } else {
            StartDate = null;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getType() {
        return Type;
    }

    public void setType(String aType) {
        Type = aType;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String aSex) {
        Sex = aSex;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int aAge) {
        Age = aAge;
    }

    public void setAge(String aAge) {
        if (aAge != null && !aAge.equals("")) {
            Integer tInteger = new Integer(aAge);
            int i = tInteger.intValue();
            Age = i;
        }
    }

    public double getFeeRate() {
        return FeeRate;
    }

    public void setFeeRate(double aFeeRate) {
        FeeRate = Arith.round(aFeeRate, 2);
    }

    public void setFeeRate(String aFeeRate) {
        if (aFeeRate != null && !aFeeRate.equals("")) {
            Double tDouble = new Double(aFeeRate);
            double d = tDouble.doubleValue();
            FeeRate = Arith.round(d, 2);
        }
    }


    /**
     * 使用另外一个 rate2602Schema 对象给 Schema 赋值
     * @param: arate2602Schema rate2602Schema
     **/
    public void setSchema(rate2602Schema arate2602Schema) {
        this.StartDate = fDate.getDate(arate2602Schema.getStartDate());
        this.ManageCom = arate2602Schema.getManageCom();
        this.Type = arate2602Schema.getType();
        this.Sex = arate2602Schema.getSex();
        this.Age = arate2602Schema.getAge();
        this.FeeRate = arate2602Schema.getFeeRate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            this.StartDate = rs.getDate("StartDate");
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Type") == null) {
                this.Type = null;
            } else {
                this.Type = rs.getString("Type").trim();
            }

            if (rs.getString("Sex") == null) {
                this.Sex = null;
            } else {
                this.Sex = rs.getString("Sex").trim();
            }

            this.Age = rs.getInt("Age");
            this.FeeRate = rs.getDouble("FeeRate");
        } catch (SQLException sqle) {
            System.out.println("数据库中的rate2602表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "rate2602Schema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public rate2602Schema getSchema() {
        rate2602Schema arate2602Schema = new rate2602Schema();
        arate2602Schema.setSchema(this);
        return arate2602Schema;
    }

    public rate2602DB getDB() {
        rate2602DB aDBOper = new rate2602DB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prprate2602描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(fDate.getString(StartDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Type));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Age));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FeeRate));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#Prprate2602>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 1, SysConst.PACKAGESPILTER));
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            Type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                  SysConst.PACKAGESPILTER);
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                 SysConst.PACKAGESPILTER);
            Age = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    5, SysConst.PACKAGESPILTER))).intValue();
            FeeRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "rate2602Schema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("StartDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getStartDate()));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Type")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Type));
        }
        if (FCode.equals("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equals("Age")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Age));
        }
        if (FCode.equals("FeeRate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeRate));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getStartDate()));
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(Type);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(Sex);
            break;
        case 4:
            strFieldValue = String.valueOf(Age);
            break;
        case 5:
            strFieldValue = String.valueOf(FeeRate);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("StartDate")) {
            if (FValue != null && !FValue.equals("")) {
                StartDate = fDate.getDate(FValue);
            } else {
                StartDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Type")) {
            if (FValue != null && !FValue.equals("")) {
                Type = FValue.trim();
            } else {
                Type = null;
            }
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if (FValue != null && !FValue.equals("")) {
                Sex = FValue.trim();
            } else {
                Sex = null;
            }
        }
        if (FCode.equalsIgnoreCase("Age")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Age = i;
            }
        }
        if (FCode.equalsIgnoreCase("FeeRate")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FeeRate = d;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        rate2602Schema other = (rate2602Schema) otherObject;
        return
                fDate.getString(StartDate).equals(other.getStartDate())
                && ManageCom.equals(other.getManageCom())
                && Type.equals(other.getType())
                && Sex.equals(other.getSex())
                && Age == other.getAge()
                && FeeRate == other.getFeeRate();
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("StartDate")) {
            return 0;
        }
        if (strFieldName.equals("ManageCom")) {
            return 1;
        }
        if (strFieldName.equals("Type")) {
            return 2;
        }
        if (strFieldName.equals("Sex")) {
            return 3;
        }
        if (strFieldName.equals("Age")) {
            return 4;
        }
        if (strFieldName.equals("FeeRate")) {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "StartDate";
            break;
        case 1:
            strFieldName = "ManageCom";
            break;
        case 2:
            strFieldName = "Type";
            break;
        case 3:
            strFieldName = "Sex";
            break;
        case 4:
            strFieldName = "Age";
            break;
        case 5:
            strFieldName = "FeeRate";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("StartDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Type")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Sex")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Age")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("FeeRate")) {
            return Schema.TYPE_DOUBLE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_INT;
            break;
        case 5:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
