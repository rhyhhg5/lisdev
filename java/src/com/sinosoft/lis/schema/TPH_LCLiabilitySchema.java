/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.TPH_LCLiabilityDB;

/*
 * <p>ClassName: TPH_LCLiabilitySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: SY
 * @CreateDate：2015-11-02
 */
public class TPH_LCLiabilitySchema implements Schema, Cloneable
{
	// @Field
	/** 保单号 */
	private String policyNo;
	/** 分单号 */
	private String sequenceNo;
	/** 被保人客户号 */
	private String insuredNo;
	/** 公司险种代码 */
	private String comCoverageCode;
	/** 公司责任代码 */
	private String liabilityCode;
	/** 责任保额 */
	private double liabilitySa;
	/** 责任状态 */
	private String liabilityStatus;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;

	public static final int FIELDNUM = 9;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public TPH_LCLiabilitySchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[5];
		pk[0] = "policyNo";
		pk[1] = "sequenceNo";
		pk[2] = "insuredNo";
		pk[3] = "comCoverageCode";
		pk[4] = "liabilityCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		TPH_LCLiabilitySchema cloned = (TPH_LCLiabilitySchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getpolicyNo()
	{
		return policyNo;
	}
	public void setpolicyNo(String apolicyNo)
	{
		policyNo = apolicyNo;
	}
	public String getsequenceNo()
	{
		return sequenceNo;
	}
	public void setsequenceNo(String asequenceNo)
	{
		sequenceNo = asequenceNo;
	}
	public String getinsuredNo()
	{
		return insuredNo;
	}
	public void setinsuredNo(String ainsuredNo)
	{
		insuredNo = ainsuredNo;
	}
	public String getcomCoverageCode()
	{
		return comCoverageCode;
	}
	public void setcomCoverageCode(String acomCoverageCode)
	{
		comCoverageCode = acomCoverageCode;
	}
	public String getliabilityCode()
	{
		return liabilityCode;
	}
	public void setliabilityCode(String aliabilityCode)
	{
		liabilityCode = aliabilityCode;
	}
	public double getliabilitySa()
	{
		return liabilitySa;
	}
	public void setliabilitySa(double aliabilitySa)
	{
		liabilitySa = Arith.round(aliabilitySa,2);
	}
	public void setliabilitySa(String aliabilitySa)
	{
		if (aliabilitySa != null && !aliabilitySa.equals(""))
		{
			Double tDouble = new Double(aliabilitySa);
			double d = tDouble.doubleValue();
                liabilitySa = Arith.round(d,2);
		}
	}

	public String getliabilityStatus()
	{
		return liabilityStatus;
	}
	public void setliabilityStatus(String aliabilityStatus)
	{
		liabilityStatus = aliabilityStatus;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}

	/**
	* 使用另外一个 TPH_LCLiabilitySchema 对象给 Schema 赋值
	* @param: aTPH_LCLiabilitySchema TPH_LCLiabilitySchema
	**/
	public void setSchema(TPH_LCLiabilitySchema aTPH_LCLiabilitySchema)
	{
		this.policyNo = aTPH_LCLiabilitySchema.getpolicyNo();
		this.sequenceNo = aTPH_LCLiabilitySchema.getsequenceNo();
		this.insuredNo = aTPH_LCLiabilitySchema.getinsuredNo();
		this.comCoverageCode = aTPH_LCLiabilitySchema.getcomCoverageCode();
		this.liabilityCode = aTPH_LCLiabilitySchema.getliabilityCode();
		this.liabilitySa = aTPH_LCLiabilitySchema.getliabilitySa();
		this.liabilityStatus = aTPH_LCLiabilitySchema.getliabilityStatus();
		this.MakeDate = fDate.getDate( aTPH_LCLiabilitySchema.getMakeDate());
		this.MakeTime = aTPH_LCLiabilitySchema.getMakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("policyNo") == null )
				this.policyNo = null;
			else
				this.policyNo = rs.getString("policyNo").trim();

			if( rs.getString("sequenceNo") == null )
				this.sequenceNo = null;
			else
				this.sequenceNo = rs.getString("sequenceNo").trim();

			if( rs.getString("insuredNo") == null )
				this.insuredNo = null;
			else
				this.insuredNo = rs.getString("insuredNo").trim();

			if( rs.getString("comCoverageCode") == null )
				this.comCoverageCode = null;
			else
				this.comCoverageCode = rs.getString("comCoverageCode").trim();

			if( rs.getString("liabilityCode") == null )
				this.liabilityCode = null;
			else
				this.liabilityCode = rs.getString("liabilityCode").trim();

			this.liabilitySa = rs.getDouble("liabilitySa");
			if( rs.getString("liabilityStatus") == null )
				this.liabilityStatus = null;
			else
				this.liabilityStatus = rs.getString("liabilityStatus").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的TPH_LCLiability表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LCLiabilitySchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public TPH_LCLiabilitySchema getSchema()
	{
		TPH_LCLiabilitySchema aTPH_LCLiabilitySchema = new TPH_LCLiabilitySchema();
		aTPH_LCLiabilitySchema.setSchema(this);
		return aTPH_LCLiabilitySchema;
	}

	public TPH_LCLiabilityDB getDB()
	{
		TPH_LCLiabilityDB aDBOper = new TPH_LCLiabilityDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LCLiability描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(policyNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(sequenceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(insuredNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(comCoverageCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(liabilityCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(liabilitySa));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(liabilityStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpTPH_LCLiability>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			policyNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			sequenceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			insuredNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			comCoverageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			liabilityCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			liabilitySa = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			liabilityStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TPH_LCLiabilitySchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("policyNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(policyNo));
		}
		if (FCode.equals("sequenceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(sequenceNo));
		}
		if (FCode.equals("insuredNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(insuredNo));
		}
		if (FCode.equals("comCoverageCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(comCoverageCode));
		}
		if (FCode.equals("liabilityCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(liabilityCode));
		}
		if (FCode.equals("liabilitySa"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(liabilitySa));
		}
		if (FCode.equals("liabilityStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(liabilityStatus));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(policyNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(sequenceNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(insuredNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(comCoverageCode);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(liabilityCode);
				break;
			case 5:
				strFieldValue = String.valueOf(liabilitySa);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(liabilityStatus);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("policyNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				policyNo = FValue.trim();
			}
			else
				policyNo = null;
		}
		if (FCode.equalsIgnoreCase("sequenceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				sequenceNo = FValue.trim();
			}
			else
				sequenceNo = null;
		}
		if (FCode.equalsIgnoreCase("insuredNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				insuredNo = FValue.trim();
			}
			else
				insuredNo = null;
		}
		if (FCode.equalsIgnoreCase("comCoverageCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				comCoverageCode = FValue.trim();
			}
			else
				comCoverageCode = null;
		}
		if (FCode.equalsIgnoreCase("liabilityCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				liabilityCode = FValue.trim();
			}
			else
				liabilityCode = null;
		}
		if (FCode.equalsIgnoreCase("liabilitySa"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				liabilitySa = d;
			}
		}
		if (FCode.equalsIgnoreCase("liabilityStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				liabilityStatus = FValue.trim();
			}
			else
				liabilityStatus = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		TPH_LCLiabilitySchema other = (TPH_LCLiabilitySchema)otherObject;
		return
			(policyNo == null ? other.getpolicyNo() == null : policyNo.equals(other.getpolicyNo()))
			&& (sequenceNo == null ? other.getsequenceNo() == null : sequenceNo.equals(other.getsequenceNo()))
			&& (insuredNo == null ? other.getinsuredNo() == null : insuredNo.equals(other.getinsuredNo()))
			&& (comCoverageCode == null ? other.getcomCoverageCode() == null : comCoverageCode.equals(other.getcomCoverageCode()))
			&& (liabilityCode == null ? other.getliabilityCode() == null : liabilityCode.equals(other.getliabilityCode()))
			&& liabilitySa == other.getliabilitySa()
			&& (liabilityStatus == null ? other.getliabilityStatus() == null : liabilityStatus.equals(other.getliabilityStatus()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return 0;
		}
		if( strFieldName.equals("sequenceNo") ) {
			return 1;
		}
		if( strFieldName.equals("insuredNo") ) {
			return 2;
		}
		if( strFieldName.equals("comCoverageCode") ) {
			return 3;
		}
		if( strFieldName.equals("liabilityCode") ) {
			return 4;
		}
		if( strFieldName.equals("liabilitySa") ) {
			return 5;
		}
		if( strFieldName.equals("liabilityStatus") ) {
			return 6;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 7;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 8;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "policyNo";
				break;
			case 1:
				strFieldName = "sequenceNo";
				break;
			case 2:
				strFieldName = "insuredNo";
				break;
			case 3:
				strFieldName = "comCoverageCode";
				break;
			case 4:
				strFieldName = "liabilityCode";
				break;
			case 5:
				strFieldName = "liabilitySa";
				break;
			case 6:
				strFieldName = "liabilityStatus";
				break;
			case 7:
				strFieldName = "MakeDate";
				break;
			case 8:
				strFieldName = "MakeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("policyNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("sequenceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("insuredNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("comCoverageCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("liabilityCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("liabilitySa") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("liabilityStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
