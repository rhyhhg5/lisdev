/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKTransStatusDB;

/*
 * <p>ClassName: LKTransStatusSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2015-08-07
 */
public class LKTransStatusSchema implements Schema, Cloneable
{
	// @Field
	/** 业务流水号 */
	private String TransCode;
	/** 报文编号 */
	private String ReportNo;
	/** 银行代码 */
	private String BankCode;
	/** 银行机构代码 */
	private String BankBranch;
	/** 银行网点代码 */
	private String BankNode;
	/** 银行操作员代码 */
	private String BankOperator;
	/** 交易流水号(银行) */
	private String TransNo;
	/** 处理标志 */
	private String FuncFlag;
	/** 交易日期 */
	private Date TransDate;
	/** 交易时间 */
	private String TransTime;
	/** 区站(管理机构) */
	private String ManageCom;
	/** 险种代码 */
	private String RiskCode;
	/** 投保单号 */
	private String ProposalNo;
	/** 印刷号 */
	private String PrtNo;
	/** 保单号 */
	private String PolNo;
	/** 批单号 */
	private String EdorNo;
	/** 收据号 */
	private String TempFeeNo;
	/** 交易金额 */
	private double TransAmnt;
	/** 银行信用卡号 */
	private String BankAcc;
	/** 返还码 */
	private String RCode;
	/** 交易状态 */
	private String TransStatus;
	/** 业务状态 */
	private String Status;
	/** 描述 */
	private String Descr;
	/** 备用字段 */
	private String Temp;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后修改日期 */
	private Date ModifyDate;
	/** 最后修改时间 */
	private String ModifyTime;
	/** 状态代码 */
	private String State_Code;
	/** 内部服务流水号 */
	private String RequestId;
	/** 外部的服务代码 */
	private String OutServiceCode;
	/** 客户端ip */
	private String ClientIP;
	/** 客户端port */
	private String ClientPort;
	/** 渠道类型id */
	private String IssueWay;
	/** 服务处理开始时间 */
	private Date ServiceStartTime;
	/** 服务处理结束时间 */
	private Date ServiceEndTime;
	/** 银行与midplat的对账比对结果 */
	private String RBankVSMP;
	/** 银行与midplat的对账比对结果描述 */
	private String DesBankVSMP;
	/** Midplat与核心的对账比对结果 */
	private String RMPVSKernel;
	/** Midplat与核心的对账比对结果描述 */
	private String DesMPVSKernel;
	/** 对账后续处理结果 */
	private String ResultBalance;
	/** 对账后续处理结果描述 */
	private String DesBalance;
	/** 备用字段1 */
	private String bak1;
	/** 备用字段2 */
	private String bak2;
	/** 备用字段3 */
	private String bak3;
	/** 备用字段4 */
	private String bak4;
	/** 客户风险承载能力代码 */
	private String CutoleaCode;
	/** 风险评测有效期 */
	private String RiskTravaliDate;
	/** 预算金额 */
	private String BudgeTamount;
	/** 备用字段6 */
	private String bak6;
	/** 备用字段7 */
	private String bak7;
	/** 备用字段8 */
	private String bak8;
	/** 备用字段9 */
	private String bak9;
	/** 备用字段10 */
	private String bak10;

	public static final int FIELDNUM = 54;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKTransStatusSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "BankCode";
		pk[1] = "BankBranch";
		pk[2] = "BankNode";
		pk[3] = "TransNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LKTransStatusSchema cloned = (LKTransStatusSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getTransCode()
	{
		return TransCode;
	}
	public void setTransCode(String aTransCode)
	{
		TransCode = aTransCode;
	}
	public String getReportNo()
	{
		return ReportNo;
	}
	public void setReportNo(String aReportNo)
	{
		ReportNo = aReportNo;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankBranch()
	{
		return BankBranch;
	}
	public void setBankBranch(String aBankBranch)
	{
		BankBranch = aBankBranch;
	}
	public String getBankNode()
	{
		return BankNode;
	}
	public void setBankNode(String aBankNode)
	{
		BankNode = aBankNode;
	}
	public String getBankOperator()
	{
		return BankOperator;
	}
	public void setBankOperator(String aBankOperator)
	{
		BankOperator = aBankOperator;
	}
	public String getTransNo()
	{
		return TransNo;
	}
	public void setTransNo(String aTransNo)
	{
		TransNo = aTransNo;
	}
	public String getFuncFlag()
	{
		return FuncFlag;
	}
	public void setFuncFlag(String aFuncFlag)
	{
		FuncFlag = aFuncFlag;
	}
	public String getTransDate()
	{
		if( TransDate != null )
			return fDate.getString(TransDate);
		else
			return null;
	}
	public void setTransDate(Date aTransDate)
	{
		TransDate = aTransDate;
	}
	public void setTransDate(String aTransDate)
	{
		if (aTransDate != null && !aTransDate.equals("") )
		{
			TransDate = fDate.getDate( aTransDate );
		}
		else
			TransDate = null;
	}

	public String getTransTime()
	{
		return TransTime;
	}
	public void setTransTime(String aTransTime)
	{
		TransTime = aTransTime;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getProposalNo()
	{
		return ProposalNo;
	}
	public void setProposalNo(String aProposalNo)
	{
		ProposalNo = aProposalNo;
	}
	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getEdorNo()
	{
		return EdorNo;
	}
	public void setEdorNo(String aEdorNo)
	{
		EdorNo = aEdorNo;
	}
	public String getTempFeeNo()
	{
		return TempFeeNo;
	}
	public void setTempFeeNo(String aTempFeeNo)
	{
		TempFeeNo = aTempFeeNo;
	}
	public double getTransAmnt()
	{
		return TransAmnt;
	}
	public void setTransAmnt(double aTransAmnt)
	{
		TransAmnt = Arith.round(aTransAmnt, 2);
	}
	public void setTransAmnt(String aTransAmnt)
	{
		if (aTransAmnt != null && !aTransAmnt.equals(""))
		{
			Double tDouble = new Double(aTransAmnt);
			double d = tDouble.doubleValue();
                TransAmnt = Arith.round(d, 2);
		}
	}

	public String getBankAcc()
	{
		return BankAcc;
	}
	public void setBankAcc(String aBankAcc)
	{
		BankAcc = aBankAcc;
	}
	public String getRCode()
	{
		return RCode;
	}
	public void setRCode(String aRCode)
	{
		RCode = aRCode;
	}
	public String getTransStatus()
	{
		return TransStatus;
	}
	public void setTransStatus(String aTransStatus)
	{
		TransStatus = aTransStatus;
	}
	public String getStatus()
	{
		return Status;
	}
	public void setStatus(String aStatus)
	{
		Status = aStatus;
	}
	public String getDescr()
	{
		return Descr;
	}
	public void setDescr(String aDescr)
	{
		Descr = aDescr;
	}
	public String getTemp()
	{
		return Temp;
	}
	public void setTemp(String aTemp)
	{
		Temp = aTemp;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getState_Code()
	{
		return State_Code;
	}
	public void setState_Code(String aState_Code)
	{
		State_Code = aState_Code;
	}
	public String getRequestId()
	{
		return RequestId;
	}
	public void setRequestId(String aRequestId)
	{
		RequestId = aRequestId;
	}
	public String getOutServiceCode()
	{
		return OutServiceCode;
	}
	public void setOutServiceCode(String aOutServiceCode)
	{
		OutServiceCode = aOutServiceCode;
	}
	public String getClientIP()
	{
		return ClientIP;
	}
	public void setClientIP(String aClientIP)
	{
		ClientIP = aClientIP;
	}
	public String getClientPort()
	{
		return ClientPort;
	}
	public void setClientPort(String aClientPort)
	{
		ClientPort = aClientPort;
	}
	public String getIssueWay()
	{
		return IssueWay;
	}
	public void setIssueWay(String aIssueWay)
	{
		IssueWay = aIssueWay;
	}
	public String getServiceStartTime()
	{
		if( ServiceStartTime != null )
			return fDate.getString(ServiceStartTime);
		else
			return null;
	}
	public void setServiceStartTime(Date aServiceStartTime)
	{
		ServiceStartTime = aServiceStartTime;
	}
	public void setServiceStartTime(String aServiceStartTime)
	{
		if (aServiceStartTime != null && !aServiceStartTime.equals("") )
		{
			ServiceStartTime = fDate.getDate( aServiceStartTime );
		}
		else
			ServiceStartTime = null;
	}

	public String getServiceEndTime()
	{
		if( ServiceEndTime != null )
			return fDate.getString(ServiceEndTime);
		else
			return null;
	}
	public void setServiceEndTime(Date aServiceEndTime)
	{
		ServiceEndTime = aServiceEndTime;
	}
	public void setServiceEndTime(String aServiceEndTime)
	{
		if (aServiceEndTime != null && !aServiceEndTime.equals("") )
		{
			ServiceEndTime = fDate.getDate( aServiceEndTime );
		}
		else
			ServiceEndTime = null;
	}

	public String getRBankVSMP()
	{
		return RBankVSMP;
	}
	public void setRBankVSMP(String aRBankVSMP)
	{
		RBankVSMP = aRBankVSMP;
	}
	public String getDesBankVSMP()
	{
		return DesBankVSMP;
	}
	public void setDesBankVSMP(String aDesBankVSMP)
	{
		DesBankVSMP = aDesBankVSMP;
	}
	public String getRMPVSKernel()
	{
		return RMPVSKernel;
	}
	public void setRMPVSKernel(String aRMPVSKernel)
	{
		RMPVSKernel = aRMPVSKernel;
	}
	public String getDesMPVSKernel()
	{
		return DesMPVSKernel;
	}
	public void setDesMPVSKernel(String aDesMPVSKernel)
	{
		DesMPVSKernel = aDesMPVSKernel;
	}
	public String getResultBalance()
	{
		return ResultBalance;
	}
	public void setResultBalance(String aResultBalance)
	{
		ResultBalance = aResultBalance;
	}
	public String getDesBalance()
	{
		return DesBalance;
	}
	public void setDesBalance(String aDesBalance)
	{
		DesBalance = aDesBalance;
	}
	public String getbak1()
	{
		return bak1;
	}
	public void setbak1(String abak1)
	{
		bak1 = abak1;
	}
	public String getbak2()
	{
		return bak2;
	}
	public void setbak2(String abak2)
	{
		bak2 = abak2;
	}
	public String getbak3()
	{
		return bak3;
	}
	public void setbak3(String abak3)
	{
		bak3 = abak3;
	}
	public String getbak4()
	{
		return bak4;
	}
	public void setbak4(String abak4)
	{
		bak4 = abak4;
	}
	public String getCutoleaCode()
	{
		return CutoleaCode;
	}
	public void setCutoleaCode(String aCutoleaCode)
	{
		CutoleaCode = aCutoleaCode;
	}
	public String getRiskTravaliDate()
	{
		return RiskTravaliDate;
	}
	public void setRiskTravaliDate(String aRiskTravaliDate)
	{
		RiskTravaliDate = aRiskTravaliDate;
	}
	public String getBudgeTamount()
	{
		return BudgeTamount;
	}
	public void setBudgeTamount(String aBudgeTamount)
	{
		BudgeTamount = aBudgeTamount;
	}
	public String getbak6()
	{
		return bak6;
	}
	public void setbak6(String abak6)
	{
		bak6 = abak6;
	}
	public String getbak7()
	{
		return bak7;
	}
	public void setbak7(String abak7)
	{
		bak7 = abak7;
	}
	public String getbak8()
	{
		return bak8;
	}
	public void setbak8(String abak8)
	{
		bak8 = abak8;
	}
	public String getbak9()
	{
		return bak9;
	}
	public void setbak9(String abak9)
	{
		bak9 = abak9;
	}
	public String getbak10()
	{
		return bak10;
	}
	public void setbak10(String abak10)
	{
		bak10 = abak10;
	}

	/**
	* 使用另外一个 LKTransStatusSchema 对象给 Schema 赋值
	* @param: aLKTransStatusSchema LKTransStatusSchema
	**/
	public void setSchema(LKTransStatusSchema aLKTransStatusSchema)
	{
		this.TransCode = aLKTransStatusSchema.getTransCode();
		this.ReportNo = aLKTransStatusSchema.getReportNo();
		this.BankCode = aLKTransStatusSchema.getBankCode();
		this.BankBranch = aLKTransStatusSchema.getBankBranch();
		this.BankNode = aLKTransStatusSchema.getBankNode();
		this.BankOperator = aLKTransStatusSchema.getBankOperator();
		this.TransNo = aLKTransStatusSchema.getTransNo();
		this.FuncFlag = aLKTransStatusSchema.getFuncFlag();
		this.TransDate = fDate.getDate( aLKTransStatusSchema.getTransDate());
		this.TransTime = aLKTransStatusSchema.getTransTime();
		this.ManageCom = aLKTransStatusSchema.getManageCom();
		this.RiskCode = aLKTransStatusSchema.getRiskCode();
		this.ProposalNo = aLKTransStatusSchema.getProposalNo();
		this.PrtNo = aLKTransStatusSchema.getPrtNo();
		this.PolNo = aLKTransStatusSchema.getPolNo();
		this.EdorNo = aLKTransStatusSchema.getEdorNo();
		this.TempFeeNo = aLKTransStatusSchema.getTempFeeNo();
		this.TransAmnt = aLKTransStatusSchema.getTransAmnt();
		this.BankAcc = aLKTransStatusSchema.getBankAcc();
		this.RCode = aLKTransStatusSchema.getRCode();
		this.TransStatus = aLKTransStatusSchema.getTransStatus();
		this.Status = aLKTransStatusSchema.getStatus();
		this.Descr = aLKTransStatusSchema.getDescr();
		this.Temp = aLKTransStatusSchema.getTemp();
		this.MakeDate = fDate.getDate( aLKTransStatusSchema.getMakeDate());
		this.MakeTime = aLKTransStatusSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLKTransStatusSchema.getModifyDate());
		this.ModifyTime = aLKTransStatusSchema.getModifyTime();
		this.State_Code = aLKTransStatusSchema.getState_Code();
		this.RequestId = aLKTransStatusSchema.getRequestId();
		this.OutServiceCode = aLKTransStatusSchema.getOutServiceCode();
		this.ClientIP = aLKTransStatusSchema.getClientIP();
		this.ClientPort = aLKTransStatusSchema.getClientPort();
		this.IssueWay = aLKTransStatusSchema.getIssueWay();
		this.ServiceStartTime = fDate.getDate( aLKTransStatusSchema.getServiceStartTime());
		this.ServiceEndTime = fDate.getDate( aLKTransStatusSchema.getServiceEndTime());
		this.RBankVSMP = aLKTransStatusSchema.getRBankVSMP();
		this.DesBankVSMP = aLKTransStatusSchema.getDesBankVSMP();
		this.RMPVSKernel = aLKTransStatusSchema.getRMPVSKernel();
		this.DesMPVSKernel = aLKTransStatusSchema.getDesMPVSKernel();
		this.ResultBalance = aLKTransStatusSchema.getResultBalance();
		this.DesBalance = aLKTransStatusSchema.getDesBalance();
		this.bak1 = aLKTransStatusSchema.getbak1();
		this.bak2 = aLKTransStatusSchema.getbak2();
		this.bak3 = aLKTransStatusSchema.getbak3();
		this.bak4 = aLKTransStatusSchema.getbak4();
		this.CutoleaCode = aLKTransStatusSchema.getCutoleaCode();
		this.RiskTravaliDate = aLKTransStatusSchema.getRiskTravaliDate();
		this.BudgeTamount = aLKTransStatusSchema.getBudgeTamount();
		this.bak6 = aLKTransStatusSchema.getbak6();
		this.bak7 = aLKTransStatusSchema.getbak7();
		this.bak8 = aLKTransStatusSchema.getbak8();
		this.bak9 = aLKTransStatusSchema.getbak9();
		this.bak10 = aLKTransStatusSchema.getbak10();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("TransCode") == null )
				this.TransCode = null;
			else
				this.TransCode = rs.getString("TransCode").trim();

			if( rs.getString("ReportNo") == null )
				this.ReportNo = null;
			else
				this.ReportNo = rs.getString("ReportNo").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankBranch") == null )
				this.BankBranch = null;
			else
				this.BankBranch = rs.getString("BankBranch").trim();

			if( rs.getString("BankNode") == null )
				this.BankNode = null;
			else
				this.BankNode = rs.getString("BankNode").trim();

			if( rs.getString("BankOperator") == null )
				this.BankOperator = null;
			else
				this.BankOperator = rs.getString("BankOperator").trim();

			if( rs.getString("TransNo") == null )
				this.TransNo = null;
			else
				this.TransNo = rs.getString("TransNo").trim();

			if( rs.getString("FuncFlag") == null )
				this.FuncFlag = null;
			else
				this.FuncFlag = rs.getString("FuncFlag").trim();

			this.TransDate = rs.getDate("TransDate");
			if( rs.getString("TransTime") == null )
				this.TransTime = null;
			else
				this.TransTime = rs.getString("TransTime").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("ProposalNo") == null )
				this.ProposalNo = null;
			else
				this.ProposalNo = rs.getString("ProposalNo").trim();

			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("EdorNo") == null )
				this.EdorNo = null;
			else
				this.EdorNo = rs.getString("EdorNo").trim();

			if( rs.getString("TempFeeNo") == null )
				this.TempFeeNo = null;
			else
				this.TempFeeNo = rs.getString("TempFeeNo").trim();

			this.TransAmnt = rs.getDouble("TransAmnt");
			if( rs.getString("BankAcc") == null )
				this.BankAcc = null;
			else
				this.BankAcc = rs.getString("BankAcc").trim();

			if( rs.getString("RCode") == null )
				this.RCode = null;
			else
				this.RCode = rs.getString("RCode").trim();

			if( rs.getString("TransStatus") == null )
				this.TransStatus = null;
			else
				this.TransStatus = rs.getString("TransStatus").trim();

			if( rs.getString("Status") == null )
				this.Status = null;
			else
				this.Status = rs.getString("Status").trim();

			if( rs.getString("Descr") == null )
				this.Descr = null;
			else
				this.Descr = rs.getString("Descr").trim();

			if( rs.getString("Temp") == null )
				this.Temp = null;
			else
				this.Temp = rs.getString("Temp").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("State_Code") == null )
				this.State_Code = null;
			else
				this.State_Code = rs.getString("State_Code").trim();

			if( rs.getString("RequestId") == null )
				this.RequestId = null;
			else
				this.RequestId = rs.getString("RequestId").trim();

			if( rs.getString("OutServiceCode") == null )
				this.OutServiceCode = null;
			else
				this.OutServiceCode = rs.getString("OutServiceCode").trim();

			if( rs.getString("ClientIP") == null )
				this.ClientIP = null;
			else
				this.ClientIP = rs.getString("ClientIP").trim();

			if( rs.getString("ClientPort") == null )
				this.ClientPort = null;
			else
				this.ClientPort = rs.getString("ClientPort").trim();

			if( rs.getString("IssueWay") == null )
				this.IssueWay = null;
			else
				this.IssueWay = rs.getString("IssueWay").trim();

			this.ServiceStartTime = rs.getDate("ServiceStartTime");
			this.ServiceEndTime = rs.getDate("ServiceEndTime");
			if( rs.getString("RBankVSMP") == null )
				this.RBankVSMP = null;
			else
				this.RBankVSMP = rs.getString("RBankVSMP").trim();

			if( rs.getString("DesBankVSMP") == null )
				this.DesBankVSMP = null;
			else
				this.DesBankVSMP = rs.getString("DesBankVSMP").trim();

			if( rs.getString("RMPVSKernel") == null )
				this.RMPVSKernel = null;
			else
				this.RMPVSKernel = rs.getString("RMPVSKernel").trim();

			if( rs.getString("DesMPVSKernel") == null )
				this.DesMPVSKernel = null;
			else
				this.DesMPVSKernel = rs.getString("DesMPVSKernel").trim();

			if( rs.getString("ResultBalance") == null )
				this.ResultBalance = null;
			else
				this.ResultBalance = rs.getString("ResultBalance").trim();

			if( rs.getString("DesBalance") == null )
				this.DesBalance = null;
			else
				this.DesBalance = rs.getString("DesBalance").trim();

			if( rs.getString("bak1") == null )
				this.bak1 = null;
			else
				this.bak1 = rs.getString("bak1").trim();

			if( rs.getString("bak2") == null )
				this.bak2 = null;
			else
				this.bak2 = rs.getString("bak2").trim();

			if( rs.getString("bak3") == null )
				this.bak3 = null;
			else
				this.bak3 = rs.getString("bak3").trim();

			if( rs.getString("bak4") == null )
				this.bak4 = null;
			else
				this.bak4 = rs.getString("bak4").trim();

			if( rs.getString("CutoleaCode") == null )
				this.CutoleaCode = null;
			else
				this.CutoleaCode = rs.getString("CutoleaCode").trim();

			if( rs.getString("RiskTravaliDate") == null )
				this.RiskTravaliDate = null;
			else
				this.RiskTravaliDate = rs.getString("RiskTravaliDate").trim();

			if( rs.getString("BudgeTamount") == null )
				this.BudgeTamount = null;
			else
				this.BudgeTamount = rs.getString("BudgeTamount").trim();

			if( rs.getString("bak6") == null )
				this.bak6 = null;
			else
				this.bak6 = rs.getString("bak6").trim();

			if( rs.getString("bak7") == null )
				this.bak7 = null;
			else
				this.bak7 = rs.getString("bak7").trim();

			if( rs.getString("bak8") == null )
				this.bak8 = null;
			else
				this.bak8 = rs.getString("bak8").trim();

			if( rs.getString("bak9") == null )
				this.bak9 = null;
			else
				this.bak9 = rs.getString("bak9").trim();

			if( rs.getString("bak10") == null )
				this.bak10 = null;
			else
				this.bak10 = rs.getString("bak10").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LKTransStatus表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKTransStatusSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LKTransStatusSchema getSchema()
	{
		LKTransStatusSchema aLKTransStatusSchema = new LKTransStatusSchema();
		aLKTransStatusSchema.setSchema(this);
		return aLKTransStatusSchema;
	}

	public LKTransStatusDB getDB()
	{
		LKTransStatusDB aDBOper = new LKTransStatusDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransStatus描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(TransCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReportNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankBranch)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankNode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FuncFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( TransDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProposalNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EdorNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempFeeNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(TransAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAcc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TransStatus)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Descr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Temp)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State_Code)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RequestId)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OutServiceCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClientIP)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClientPort)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IssueWay)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ServiceStartTime ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ServiceEndTime ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RBankVSMP)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DesBankVSMP)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RMPVSKernel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DesMPVSKernel)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ResultBalance)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DesBalance)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CutoleaCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskTravaliDate)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BudgeTamount)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak7)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak8)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak9)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(bak10));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKTransStatus>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			TransCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ReportNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BankBranch = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BankNode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			BankOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			TransNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			FuncFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			TransDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			TransTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ProposalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			TempFeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			TransAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,18,SysConst.PACKAGESPILTER))).doubleValue();
			BankAcc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			RCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			TransStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			Descr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			Temp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			State_Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			RequestId = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			OutServiceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			ClientIP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			ClientPort = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			IssueWay = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			ServiceStartTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,SysConst.PACKAGESPILTER));
			ServiceEndTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,SysConst.PACKAGESPILTER));
			RBankVSMP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			DesBankVSMP = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			RMPVSKernel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			DesMPVSKernel = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			ResultBalance = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			DesBalance = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			bak1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			bak2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			bak3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			bak4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			CutoleaCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			RiskTravaliDate = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			BudgeTamount = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			bak6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			bak7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			bak8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			bak9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			bak10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKTransStatusSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("TransCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransCode));
		}
		if (FCode.equals("ReportNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReportNo));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankBranch"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankBranch));
		}
		if (FCode.equals("BankNode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
		}
		if (FCode.equals("BankOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankOperator));
		}
		if (FCode.equals("TransNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransNo));
		}
		if (FCode.equals("FuncFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FuncFlag));
		}
		if (FCode.equals("TransDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getTransDate()));
		}
		if (FCode.equals("TransTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransTime));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("ProposalNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalNo));
		}
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("EdorNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
		}
		if (FCode.equals("TempFeeNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempFeeNo));
		}
		if (FCode.equals("TransAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransAmnt));
		}
		if (FCode.equals("BankAcc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAcc));
		}
		if (FCode.equals("RCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RCode));
		}
		if (FCode.equals("TransStatus"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TransStatus));
		}
		if (FCode.equals("Status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Status));
		}
		if (FCode.equals("Descr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Descr));
		}
		if (FCode.equals("Temp"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Temp));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("State_Code"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State_Code));
		}
		if (FCode.equals("RequestId"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RequestId));
		}
		if (FCode.equals("OutServiceCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutServiceCode));
		}
		if (FCode.equals("ClientIP"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClientIP));
		}
		if (FCode.equals("ClientPort"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClientPort));
		}
		if (FCode.equals("IssueWay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IssueWay));
		}
		if (FCode.equals("ServiceStartTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getServiceStartTime()));
		}
		if (FCode.equals("ServiceEndTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getServiceEndTime()));
		}
		if (FCode.equals("RBankVSMP"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RBankVSMP));
		}
		if (FCode.equals("DesBankVSMP"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DesBankVSMP));
		}
		if (FCode.equals("RMPVSKernel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RMPVSKernel));
		}
		if (FCode.equals("DesMPVSKernel"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DesMPVSKernel));
		}
		if (FCode.equals("ResultBalance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ResultBalance));
		}
		if (FCode.equals("DesBalance"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DesBalance));
		}
		if (FCode.equals("bak1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak1));
		}
		if (FCode.equals("bak2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak2));
		}
		if (FCode.equals("bak3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak3));
		}
		if (FCode.equals("bak4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak4));
		}
		if (FCode.equals("CutoleaCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CutoleaCode));
		}
		if (FCode.equals("RiskTravaliDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskTravaliDate));
		}
		if (FCode.equals("BudgeTamount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BudgeTamount));
		}
		if (FCode.equals("bak6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak6));
		}
		if (FCode.equals("bak7"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak7));
		}
		if (FCode.equals("bak8"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak8));
		}
		if (FCode.equals("bak9"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak9));
		}
		if (FCode.equals("bak10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(bak10));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(TransCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ReportNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BankBranch);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BankNode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(BankOperator);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(TransNo);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(FuncFlag);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getTransDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(TransTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(ProposalNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(EdorNo);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(TempFeeNo);
				break;
			case 17:
				strFieldValue = String.valueOf(TransAmnt);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(BankAcc);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(RCode);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(TransStatus);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(Status);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(Descr);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(Temp);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(State_Code);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(RequestId);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(OutServiceCode);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(ClientIP);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(ClientPort);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(IssueWay);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getServiceStartTime()));
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getServiceEndTime()));
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(RBankVSMP);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(DesBankVSMP);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(RMPVSKernel);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(DesMPVSKernel);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(ResultBalance);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(DesBalance);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(bak1);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(bak2);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(bak3);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(bak4);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(CutoleaCode);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(RiskTravaliDate);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(BudgeTamount);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(bak6);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(bak7);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(bak8);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(bak9);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(bak10);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("TransCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransCode = FValue.trim();
			}
			else
				TransCode = null;
		}
		if (FCode.equalsIgnoreCase("ReportNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReportNo = FValue.trim();
			}
			else
				ReportNo = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankBranch"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankBranch = FValue.trim();
			}
			else
				BankBranch = null;
		}
		if (FCode.equalsIgnoreCase("BankNode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankNode = FValue.trim();
			}
			else
				BankNode = null;
		}
		if (FCode.equalsIgnoreCase("BankOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankOperator = FValue.trim();
			}
			else
				BankOperator = null;
		}
		if (FCode.equalsIgnoreCase("TransNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransNo = FValue.trim();
			}
			else
				TransNo = null;
		}
		if (FCode.equalsIgnoreCase("FuncFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FuncFlag = FValue.trim();
			}
			else
				FuncFlag = null;
		}
		if (FCode.equalsIgnoreCase("TransDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				TransDate = fDate.getDate( FValue );
			}
			else
				TransDate = null;
		}
		if (FCode.equalsIgnoreCase("TransTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransTime = FValue.trim();
			}
			else
				TransTime = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("ProposalNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProposalNo = FValue.trim();
			}
			else
				ProposalNo = null;
		}
		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("EdorNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EdorNo = FValue.trim();
			}
			else
				EdorNo = null;
		}
		if (FCode.equalsIgnoreCase("TempFeeNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempFeeNo = FValue.trim();
			}
			else
				TempFeeNo = null;
		}
		if (FCode.equalsIgnoreCase("TransAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				TransAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("BankAcc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAcc = FValue.trim();
			}
			else
				BankAcc = null;
		}
		if (FCode.equalsIgnoreCase("RCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RCode = FValue.trim();
			}
			else
				RCode = null;
		}
		if (FCode.equalsIgnoreCase("TransStatus"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TransStatus = FValue.trim();
			}
			else
				TransStatus = null;
		}
		if (FCode.equalsIgnoreCase("Status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Status = FValue.trim();
			}
			else
				Status = null;
		}
		if (FCode.equalsIgnoreCase("Descr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Descr = FValue.trim();
			}
			else
				Descr = null;
		}
		if (FCode.equalsIgnoreCase("Temp"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Temp = FValue.trim();
			}
			else
				Temp = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("State_Code"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State_Code = FValue.trim();
			}
			else
				State_Code = null;
		}
		if (FCode.equalsIgnoreCase("RequestId"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RequestId = FValue.trim();
			}
			else
				RequestId = null;
		}
		if (FCode.equalsIgnoreCase("OutServiceCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OutServiceCode = FValue.trim();
			}
			else
				OutServiceCode = null;
		}
		if (FCode.equalsIgnoreCase("ClientIP"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClientIP = FValue.trim();
			}
			else
				ClientIP = null;
		}
		if (FCode.equalsIgnoreCase("ClientPort"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClientPort = FValue.trim();
			}
			else
				ClientPort = null;
		}
		if (FCode.equalsIgnoreCase("IssueWay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IssueWay = FValue.trim();
			}
			else
				IssueWay = null;
		}
		if (FCode.equalsIgnoreCase("ServiceStartTime"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ServiceStartTime = fDate.getDate( FValue );
			}
			else
				ServiceStartTime = null;
		}
		if (FCode.equalsIgnoreCase("ServiceEndTime"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ServiceEndTime = fDate.getDate( FValue );
			}
			else
				ServiceEndTime = null;
		}
		if (FCode.equalsIgnoreCase("RBankVSMP"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RBankVSMP = FValue.trim();
			}
			else
				RBankVSMP = null;
		}
		if (FCode.equalsIgnoreCase("DesBankVSMP"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DesBankVSMP = FValue.trim();
			}
			else
				DesBankVSMP = null;
		}
		if (FCode.equalsIgnoreCase("RMPVSKernel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RMPVSKernel = FValue.trim();
			}
			else
				RMPVSKernel = null;
		}
		if (FCode.equalsIgnoreCase("DesMPVSKernel"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DesMPVSKernel = FValue.trim();
			}
			else
				DesMPVSKernel = null;
		}
		if (FCode.equalsIgnoreCase("ResultBalance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ResultBalance = FValue.trim();
			}
			else
				ResultBalance = null;
		}
		if (FCode.equalsIgnoreCase("DesBalance"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DesBalance = FValue.trim();
			}
			else
				DesBalance = null;
		}
		if (FCode.equalsIgnoreCase("bak1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak1 = FValue.trim();
			}
			else
				bak1 = null;
		}
		if (FCode.equalsIgnoreCase("bak2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak2 = FValue.trim();
			}
			else
				bak2 = null;
		}
		if (FCode.equalsIgnoreCase("bak3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak3 = FValue.trim();
			}
			else
				bak3 = null;
		}
		if (FCode.equalsIgnoreCase("bak4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak4 = FValue.trim();
			}
			else
				bak4 = null;
		}
		if (FCode.equalsIgnoreCase("CutoleaCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CutoleaCode = FValue.trim();
			}
			else
				CutoleaCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskTravaliDate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskTravaliDate = FValue.trim();
			}
			else
				RiskTravaliDate = null;
		}
		if (FCode.equalsIgnoreCase("BudgeTamount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BudgeTamount = FValue.trim();
			}
			else
				BudgeTamount = null;
		}
		if (FCode.equalsIgnoreCase("bak6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak6 = FValue.trim();
			}
			else
				bak6 = null;
		}
		if (FCode.equalsIgnoreCase("bak7"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak7 = FValue.trim();
			}
			else
				bak7 = null;
		}
		if (FCode.equalsIgnoreCase("bak8"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak8 = FValue.trim();
			}
			else
				bak8 = null;
		}
		if (FCode.equalsIgnoreCase("bak9"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak9 = FValue.trim();
			}
			else
				bak9 = null;
		}
		if (FCode.equalsIgnoreCase("bak10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				bak10 = FValue.trim();
			}
			else
				bak10 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKTransStatusSchema other = (LKTransStatusSchema)otherObject;
		return
			(TransCode == null ? other.getTransCode() == null : TransCode.equals(other.getTransCode()))
			&& (ReportNo == null ? other.getReportNo() == null : ReportNo.equals(other.getReportNo()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankBranch == null ? other.getBankBranch() == null : BankBranch.equals(other.getBankBranch()))
			&& (BankNode == null ? other.getBankNode() == null : BankNode.equals(other.getBankNode()))
			&& (BankOperator == null ? other.getBankOperator() == null : BankOperator.equals(other.getBankOperator()))
			&& (TransNo == null ? other.getTransNo() == null : TransNo.equals(other.getTransNo()))
			&& (FuncFlag == null ? other.getFuncFlag() == null : FuncFlag.equals(other.getFuncFlag()))
			&& (TransDate == null ? other.getTransDate() == null : fDate.getString(TransDate).equals(other.getTransDate()))
			&& (TransTime == null ? other.getTransTime() == null : TransTime.equals(other.getTransTime()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (ProposalNo == null ? other.getProposalNo() == null : ProposalNo.equals(other.getProposalNo()))
			&& (PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (EdorNo == null ? other.getEdorNo() == null : EdorNo.equals(other.getEdorNo()))
			&& (TempFeeNo == null ? other.getTempFeeNo() == null : TempFeeNo.equals(other.getTempFeeNo()))
			&& TransAmnt == other.getTransAmnt()
			&& (BankAcc == null ? other.getBankAcc() == null : BankAcc.equals(other.getBankAcc()))
			&& (RCode == null ? other.getRCode() == null : RCode.equals(other.getRCode()))
			&& (TransStatus == null ? other.getTransStatus() == null : TransStatus.equals(other.getTransStatus()))
			&& (Status == null ? other.getStatus() == null : Status.equals(other.getStatus()))
			&& (Descr == null ? other.getDescr() == null : Descr.equals(other.getDescr()))
			&& (Temp == null ? other.getTemp() == null : Temp.equals(other.getTemp()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (State_Code == null ? other.getState_Code() == null : State_Code.equals(other.getState_Code()))
			&& (RequestId == null ? other.getRequestId() == null : RequestId.equals(other.getRequestId()))
			&& (OutServiceCode == null ? other.getOutServiceCode() == null : OutServiceCode.equals(other.getOutServiceCode()))
			&& (ClientIP == null ? other.getClientIP() == null : ClientIP.equals(other.getClientIP()))
			&& (ClientPort == null ? other.getClientPort() == null : ClientPort.equals(other.getClientPort()))
			&& (IssueWay == null ? other.getIssueWay() == null : IssueWay.equals(other.getIssueWay()))
			&& (ServiceStartTime == null ? other.getServiceStartTime() == null : fDate.getString(ServiceStartTime).equals(other.getServiceStartTime()))
			&& (ServiceEndTime == null ? other.getServiceEndTime() == null : fDate.getString(ServiceEndTime).equals(other.getServiceEndTime()))
			&& (RBankVSMP == null ? other.getRBankVSMP() == null : RBankVSMP.equals(other.getRBankVSMP()))
			&& (DesBankVSMP == null ? other.getDesBankVSMP() == null : DesBankVSMP.equals(other.getDesBankVSMP()))
			&& (RMPVSKernel == null ? other.getRMPVSKernel() == null : RMPVSKernel.equals(other.getRMPVSKernel()))
			&& (DesMPVSKernel == null ? other.getDesMPVSKernel() == null : DesMPVSKernel.equals(other.getDesMPVSKernel()))
			&& (ResultBalance == null ? other.getResultBalance() == null : ResultBalance.equals(other.getResultBalance()))
			&& (DesBalance == null ? other.getDesBalance() == null : DesBalance.equals(other.getDesBalance()))
			&& (bak1 == null ? other.getbak1() == null : bak1.equals(other.getbak1()))
			&& (bak2 == null ? other.getbak2() == null : bak2.equals(other.getbak2()))
			&& (bak3 == null ? other.getbak3() == null : bak3.equals(other.getbak3()))
			&& (bak4 == null ? other.getbak4() == null : bak4.equals(other.getbak4()))
			&& (CutoleaCode == null ? other.getCutoleaCode() == null : CutoleaCode.equals(other.getCutoleaCode()))
			&& (RiskTravaliDate == null ? other.getRiskTravaliDate() == null : RiskTravaliDate.equals(other.getRiskTravaliDate()))
			&& (BudgeTamount == null ? other.getBudgeTamount() == null : BudgeTamount.equals(other.getBudgeTamount()))
			&& (bak6 == null ? other.getbak6() == null : bak6.equals(other.getbak6()))
			&& (bak7 == null ? other.getbak7() == null : bak7.equals(other.getbak7()))
			&& (bak8 == null ? other.getbak8() == null : bak8.equals(other.getbak8()))
			&& (bak9 == null ? other.getbak9() == null : bak9.equals(other.getbak9()))
			&& (bak10 == null ? other.getbak10() == null : bak10.equals(other.getbak10()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("TransCode") ) {
			return 0;
		}
		if( strFieldName.equals("ReportNo") ) {
			return 1;
		}
		if( strFieldName.equals("BankCode") ) {
			return 2;
		}
		if( strFieldName.equals("BankBranch") ) {
			return 3;
		}
		if( strFieldName.equals("BankNode") ) {
			return 4;
		}
		if( strFieldName.equals("BankOperator") ) {
			return 5;
		}
		if( strFieldName.equals("TransNo") ) {
			return 6;
		}
		if( strFieldName.equals("FuncFlag") ) {
			return 7;
		}
		if( strFieldName.equals("TransDate") ) {
			return 8;
		}
		if( strFieldName.equals("TransTime") ) {
			return 9;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 10;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 11;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return 12;
		}
		if( strFieldName.equals("PrtNo") ) {
			return 13;
		}
		if( strFieldName.equals("PolNo") ) {
			return 14;
		}
		if( strFieldName.equals("EdorNo") ) {
			return 15;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return 16;
		}
		if( strFieldName.equals("TransAmnt") ) {
			return 17;
		}
		if( strFieldName.equals("BankAcc") ) {
			return 18;
		}
		if( strFieldName.equals("RCode") ) {
			return 19;
		}
		if( strFieldName.equals("TransStatus") ) {
			return 20;
		}
		if( strFieldName.equals("Status") ) {
			return 21;
		}
		if( strFieldName.equals("Descr") ) {
			return 22;
		}
		if( strFieldName.equals("Temp") ) {
			return 23;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 24;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 25;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 26;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 27;
		}
		if( strFieldName.equals("State_Code") ) {
			return 28;
		}
		if( strFieldName.equals("RequestId") ) {
			return 29;
		}
		if( strFieldName.equals("OutServiceCode") ) {
			return 30;
		}
		if( strFieldName.equals("ClientIP") ) {
			return 31;
		}
		if( strFieldName.equals("ClientPort") ) {
			return 32;
		}
		if( strFieldName.equals("IssueWay") ) {
			return 33;
		}
		if( strFieldName.equals("ServiceStartTime") ) {
			return 34;
		}
		if( strFieldName.equals("ServiceEndTime") ) {
			return 35;
		}
		if( strFieldName.equals("RBankVSMP") ) {
			return 36;
		}
		if( strFieldName.equals("DesBankVSMP") ) {
			return 37;
		}
		if( strFieldName.equals("RMPVSKernel") ) {
			return 38;
		}
		if( strFieldName.equals("DesMPVSKernel") ) {
			return 39;
		}
		if( strFieldName.equals("ResultBalance") ) {
			return 40;
		}
		if( strFieldName.equals("DesBalance") ) {
			return 41;
		}
		if( strFieldName.equals("bak1") ) {
			return 42;
		}
		if( strFieldName.equals("bak2") ) {
			return 43;
		}
		if( strFieldName.equals("bak3") ) {
			return 44;
		}
		if( strFieldName.equals("bak4") ) {
			return 45;
		}
		if( strFieldName.equals("CutoleaCode") ) {
			return 46;
		}
		if( strFieldName.equals("RiskTravaliDate") ) {
			return 47;
		}
		if( strFieldName.equals("BudgeTamount") ) {
			return 48;
		}
		if( strFieldName.equals("bak6") ) {
			return 49;
		}
		if( strFieldName.equals("bak7") ) {
			return 50;
		}
		if( strFieldName.equals("bak8") ) {
			return 51;
		}
		if( strFieldName.equals("bak9") ) {
			return 52;
		}
		if( strFieldName.equals("bak10") ) {
			return 53;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "TransCode";
				break;
			case 1:
				strFieldName = "ReportNo";
				break;
			case 2:
				strFieldName = "BankCode";
				break;
			case 3:
				strFieldName = "BankBranch";
				break;
			case 4:
				strFieldName = "BankNode";
				break;
			case 5:
				strFieldName = "BankOperator";
				break;
			case 6:
				strFieldName = "TransNo";
				break;
			case 7:
				strFieldName = "FuncFlag";
				break;
			case 8:
				strFieldName = "TransDate";
				break;
			case 9:
				strFieldName = "TransTime";
				break;
			case 10:
				strFieldName = "ManageCom";
				break;
			case 11:
				strFieldName = "RiskCode";
				break;
			case 12:
				strFieldName = "ProposalNo";
				break;
			case 13:
				strFieldName = "PrtNo";
				break;
			case 14:
				strFieldName = "PolNo";
				break;
			case 15:
				strFieldName = "EdorNo";
				break;
			case 16:
				strFieldName = "TempFeeNo";
				break;
			case 17:
				strFieldName = "TransAmnt";
				break;
			case 18:
				strFieldName = "BankAcc";
				break;
			case 19:
				strFieldName = "RCode";
				break;
			case 20:
				strFieldName = "TransStatus";
				break;
			case 21:
				strFieldName = "Status";
				break;
			case 22:
				strFieldName = "Descr";
				break;
			case 23:
				strFieldName = "Temp";
				break;
			case 24:
				strFieldName = "MakeDate";
				break;
			case 25:
				strFieldName = "MakeTime";
				break;
			case 26:
				strFieldName = "ModifyDate";
				break;
			case 27:
				strFieldName = "ModifyTime";
				break;
			case 28:
				strFieldName = "State_Code";
				break;
			case 29:
				strFieldName = "RequestId";
				break;
			case 30:
				strFieldName = "OutServiceCode";
				break;
			case 31:
				strFieldName = "ClientIP";
				break;
			case 32:
				strFieldName = "ClientPort";
				break;
			case 33:
				strFieldName = "IssueWay";
				break;
			case 34:
				strFieldName = "ServiceStartTime";
				break;
			case 35:
				strFieldName = "ServiceEndTime";
				break;
			case 36:
				strFieldName = "RBankVSMP";
				break;
			case 37:
				strFieldName = "DesBankVSMP";
				break;
			case 38:
				strFieldName = "RMPVSKernel";
				break;
			case 39:
				strFieldName = "DesMPVSKernel";
				break;
			case 40:
				strFieldName = "ResultBalance";
				break;
			case 41:
				strFieldName = "DesBalance";
				break;
			case 42:
				strFieldName = "bak1";
				break;
			case 43:
				strFieldName = "bak2";
				break;
			case 44:
				strFieldName = "bak3";
				break;
			case 45:
				strFieldName = "bak4";
				break;
			case 46:
				strFieldName = "CutoleaCode";
				break;
			case 47:
				strFieldName = "RiskTravaliDate";
				break;
			case 48:
				strFieldName = "BudgeTamount";
				break;
			case 49:
				strFieldName = "bak6";
				break;
			case 50:
				strFieldName = "bak7";
				break;
			case 51:
				strFieldName = "bak8";
				break;
			case 52:
				strFieldName = "bak9";
				break;
			case 53:
				strFieldName = "bak10";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("TransCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReportNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankBranch") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankNode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FuncFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("TransTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProposalNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EdorNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempFeeNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BankAcc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TransStatus") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Descr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Temp") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State_Code") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RequestId") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OutServiceCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClientIP") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClientPort") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IssueWay") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServiceStartTime") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ServiceEndTime") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RBankVSMP") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DesBankVSMP") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RMPVSKernel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DesMPVSKernel") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ResultBalance") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DesBalance") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CutoleaCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskTravaliDate") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BudgeTamount") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak7") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak8") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak9") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("bak10") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 35:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
