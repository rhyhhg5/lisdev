/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLPrepaidBackDB;

/*
 * <p>ClassName: LLPrepaidBackSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新系统模型
 * @CreateDate：2010-12-08
 */
public class LLPrepaidBackSchema implements Schema, Cloneable
{
	// @Field
	/** 回退号 */
	private String PrepaidBackNo;
	/** 预付赔款号 */
	private String PrepaidNo;
	/** 回退类型 */
	private String BackType;
	/** 回退时状态 */
	private String BeforState;
	/** 回退后状态 */
	private String AfterState;
	/** 回退人 */
	private String Handler;
	/** 接收人 */
	private String NewHanler;
	/** 回退日期 */
	private Date BackDate;
	/** 备注 */
	private String Remark;
	/** 管理机构 */
	private String ManageCom;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 15;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLPrepaidBackSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "PrepaidBackNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLPrepaidBackSchema cloned = (LLPrepaidBackSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPrepaidBackNo()
	{
		return PrepaidBackNo;
	}
	public void setPrepaidBackNo(String aPrepaidBackNo)
	{
		PrepaidBackNo = aPrepaidBackNo;
	}
	public String getPrepaidNo()
	{
		return PrepaidNo;
	}
	public void setPrepaidNo(String aPrepaidNo)
	{
		PrepaidNo = aPrepaidNo;
	}
	public String getBackType()
	{
		return BackType;
	}
	public void setBackType(String aBackType)
	{
		BackType = aBackType;
	}
	public String getBeforState()
	{
		return BeforState;
	}
	public void setBeforState(String aBeforState)
	{
		BeforState = aBeforState;
	}
	public String getAfterState()
	{
		return AfterState;
	}
	public void setAfterState(String aAfterState)
	{
		AfterState = aAfterState;
	}
	public String getHandler()
	{
		return Handler;
	}
	public void setHandler(String aHandler)
	{
		Handler = aHandler;
	}
	public String getNewHanler()
	{
		return NewHanler;
	}
	public void setNewHanler(String aNewHanler)
	{
		NewHanler = aNewHanler;
	}
	public String getBackDate()
	{
		if( BackDate != null )
			return fDate.getString(BackDate);
		else
			return null;
	}
	public void setBackDate(Date aBackDate)
	{
		BackDate = aBackDate;
	}
	public void setBackDate(String aBackDate)
	{
		if (aBackDate != null && !aBackDate.equals("") )
		{
			BackDate = fDate.getDate( aBackDate );
		}
		else
			BackDate = null;
	}

	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLPrepaidBackSchema 对象给 Schema 赋值
	* @param: aLLPrepaidBackSchema LLPrepaidBackSchema
	**/
	public void setSchema(LLPrepaidBackSchema aLLPrepaidBackSchema)
	{
		this.PrepaidBackNo = aLLPrepaidBackSchema.getPrepaidBackNo();
		this.PrepaidNo = aLLPrepaidBackSchema.getPrepaidNo();
		this.BackType = aLLPrepaidBackSchema.getBackType();
		this.BeforState = aLLPrepaidBackSchema.getBeforState();
		this.AfterState = aLLPrepaidBackSchema.getAfterState();
		this.Handler = aLLPrepaidBackSchema.getHandler();
		this.NewHanler = aLLPrepaidBackSchema.getNewHanler();
		this.BackDate = fDate.getDate( aLLPrepaidBackSchema.getBackDate());
		this.Remark = aLLPrepaidBackSchema.getRemark();
		this.ManageCom = aLLPrepaidBackSchema.getManageCom();
		this.Operator = aLLPrepaidBackSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLPrepaidBackSchema.getMakeDate());
		this.MakeTime = aLLPrepaidBackSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLPrepaidBackSchema.getModifyDate());
		this.ModifyTime = aLLPrepaidBackSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PrepaidBackNo") == null )
				this.PrepaidBackNo = null;
			else
				this.PrepaidBackNo = rs.getString("PrepaidBackNo").trim();

			if( rs.getString("PrepaidNo") == null )
				this.PrepaidNo = null;
			else
				this.PrepaidNo = rs.getString("PrepaidNo").trim();

			if( rs.getString("BackType") == null )
				this.BackType = null;
			else
				this.BackType = rs.getString("BackType").trim();

			if( rs.getString("BeforState") == null )
				this.BeforState = null;
			else
				this.BeforState = rs.getString("BeforState").trim();

			if( rs.getString("AfterState") == null )
				this.AfterState = null;
			else
				this.AfterState = rs.getString("AfterState").trim();

			if( rs.getString("Handler") == null )
				this.Handler = null;
			else
				this.Handler = rs.getString("Handler").trim();

			if( rs.getString("NewHanler") == null )
				this.NewHanler = null;
			else
				this.NewHanler = rs.getString("NewHanler").trim();

			this.BackDate = rs.getDate("BackDate");
			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLPrepaidBack表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLPrepaidBackSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLPrepaidBackSchema getSchema()
	{
		LLPrepaidBackSchema aLLPrepaidBackSchema = new LLPrepaidBackSchema();
		aLLPrepaidBackSchema.setSchema(this);
		return aLLPrepaidBackSchema;
	}

	public LLPrepaidBackDB getDB()
	{
		LLPrepaidBackDB aDBOper = new LLPrepaidBackDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLPrepaidBack描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PrepaidBackNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrepaidNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BackType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BeforState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AfterState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NewHanler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( BackDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLPrepaidBack>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PrepaidBackNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			PrepaidNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BackType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			BeforState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AfterState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			NewHanler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			BackDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLPrepaidBackSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PrepaidBackNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidBackNo));
		}
		if (FCode.equals("PrepaidNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrepaidNo));
		}
		if (FCode.equals("BackType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackType));
		}
		if (FCode.equals("BeforState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BeforState));
		}
		if (FCode.equals("AfterState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AfterState));
		}
		if (FCode.equals("Handler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
		}
		if (FCode.equals("NewHanler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NewHanler));
		}
		if (FCode.equals("BackDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBackDate()));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PrepaidBackNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(PrepaidNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BackType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(BeforState);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AfterState);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Handler);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(NewHanler);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBackDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PrepaidBackNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrepaidBackNo = FValue.trim();
			}
			else
				PrepaidBackNo = null;
		}
		if (FCode.equalsIgnoreCase("PrepaidNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrepaidNo = FValue.trim();
			}
			else
				PrepaidNo = null;
		}
		if (FCode.equalsIgnoreCase("BackType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackType = FValue.trim();
			}
			else
				BackType = null;
		}
		if (FCode.equalsIgnoreCase("BeforState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BeforState = FValue.trim();
			}
			else
				BeforState = null;
		}
		if (FCode.equalsIgnoreCase("AfterState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AfterState = FValue.trim();
			}
			else
				AfterState = null;
		}
		if (FCode.equalsIgnoreCase("Handler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler = FValue.trim();
			}
			else
				Handler = null;
		}
		if (FCode.equalsIgnoreCase("NewHanler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NewHanler = FValue.trim();
			}
			else
				NewHanler = null;
		}
		if (FCode.equalsIgnoreCase("BackDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BackDate = fDate.getDate( FValue );
			}
			else
				BackDate = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLPrepaidBackSchema other = (LLPrepaidBackSchema)otherObject;
		return
			(PrepaidBackNo == null ? other.getPrepaidBackNo() == null : PrepaidBackNo.equals(other.getPrepaidBackNo()))
			&& (PrepaidNo == null ? other.getPrepaidNo() == null : PrepaidNo.equals(other.getPrepaidNo()))
			&& (BackType == null ? other.getBackType() == null : BackType.equals(other.getBackType()))
			&& (BeforState == null ? other.getBeforState() == null : BeforState.equals(other.getBeforState()))
			&& (AfterState == null ? other.getAfterState() == null : AfterState.equals(other.getAfterState()))
			&& (Handler == null ? other.getHandler() == null : Handler.equals(other.getHandler()))
			&& (NewHanler == null ? other.getNewHanler() == null : NewHanler.equals(other.getNewHanler()))
			&& (BackDate == null ? other.getBackDate() == null : fDate.getString(BackDate).equals(other.getBackDate()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PrepaidBackNo") ) {
			return 0;
		}
		if( strFieldName.equals("PrepaidNo") ) {
			return 1;
		}
		if( strFieldName.equals("BackType") ) {
			return 2;
		}
		if( strFieldName.equals("BeforState") ) {
			return 3;
		}
		if( strFieldName.equals("AfterState") ) {
			return 4;
		}
		if( strFieldName.equals("Handler") ) {
			return 5;
		}
		if( strFieldName.equals("NewHanler") ) {
			return 6;
		}
		if( strFieldName.equals("BackDate") ) {
			return 7;
		}
		if( strFieldName.equals("Remark") ) {
			return 8;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 9;
		}
		if( strFieldName.equals("Operator") ) {
			return 10;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 14;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PrepaidBackNo";
				break;
			case 1:
				strFieldName = "PrepaidNo";
				break;
			case 2:
				strFieldName = "BackType";
				break;
			case 3:
				strFieldName = "BeforState";
				break;
			case 4:
				strFieldName = "AfterState";
				break;
			case 5:
				strFieldName = "Handler";
				break;
			case 6:
				strFieldName = "NewHanler";
				break;
			case 7:
				strFieldName = "BackDate";
				break;
			case 8:
				strFieldName = "Remark";
				break;
			case 9:
				strFieldName = "ManageCom";
				break;
			case 10:
				strFieldName = "Operator";
				break;
			case 11:
				strFieldName = "MakeDate";
				break;
			case 12:
				strFieldName = "MakeTime";
				break;
			case 13:
				strFieldName = "ModifyDate";
				break;
			case 14:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PrepaidBackNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrepaidNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BeforState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AfterState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Handler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NewHanler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
