/*
 * <p>ClassName: LHAffixServeicSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 客户健康档案
 * @CreateDate：2005-01-17
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LHAffixServeicDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LHAffixServeicSchema implements Schema
{
    // @Field
    /** 家庭医生服务登记号 */
    private String FDSNo;
    /** 附件代码 */
    private String AffixFileCode;
    /** 附件名称 */
    private String AffixFileName;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHAffixServeicSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "FDSNo";
        pk[1] = "AffixFileCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getFDSNo()
    {
        if (FDSNo != null && !FDSNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            FDSNo = StrTool.unicodeToGBK(FDSNo);
        }
        return FDSNo;
    }

    public void setFDSNo(String aFDSNo)
    {
        FDSNo = aFDSNo;
    }

    public String getAffixFileCode()
    {
        if (AffixFileCode != null && !AffixFileCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AffixFileCode = StrTool.unicodeToGBK(AffixFileCode);
        }
        return AffixFileCode;
    }

    public void setAffixFileCode(String aAffixFileCode)
    {
        AffixFileCode = aAffixFileCode;
    }

    public String getAffixFileName()
    {
        if (AffixFileName != null && !AffixFileName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            AffixFileName = StrTool.unicodeToGBK(AffixFileName);
        }
        return AffixFileName;
    }

    public void setAffixFileName(String aAffixFileName)
    {
        AffixFileName = aAffixFileName;
    }

    /**
     * 使用另外一个 LHAffixServeicSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LHAffixServeicSchema aLHAffixServeicSchema)
    {
        this.FDSNo = aLHAffixServeicSchema.getFDSNo();
        this.AffixFileCode = aLHAffixServeicSchema.getAffixFileCode();
        this.AffixFileName = aLHAffixServeicSchema.getAffixFileName();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("FDSNo") == null)
            {
                this.FDSNo = null;
            }
            else
            {
                this.FDSNo = rs.getString("FDSNo").trim();
            }

            if (rs.getString("AffixFileCode") == null)
            {
                this.AffixFileCode = null;
            }
            else
            {
                this.AffixFileCode = rs.getString("AffixFileCode").trim();
            }

            if (rs.getString("AffixFileName") == null)
            {
                this.AffixFileName = null;
            }
            else
            {
                this.AffixFileName = rs.getString("AffixFileName").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHAffixServeicSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LHAffixServeicSchema getSchema()
    {
        LHAffixServeicSchema aLHAffixServeicSchema = new LHAffixServeicSchema();
        aLHAffixServeicSchema.setSchema(this);
        return aLHAffixServeicSchema;
    }

    public LHAffixServeicDB getDB()
    {
        LHAffixServeicDB aDBOper = new LHAffixServeicDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHAffixServeic描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(FDSNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AffixFileCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AffixFileName));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHAffixServeic>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            FDSNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            AffixFileCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                           SysConst.PACKAGESPILTER);
            AffixFileName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHAffixServeicSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("FDSNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FDSNo));
        }
        if (FCode.equals("AffixFileCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AffixFileCode));
        }
        if (FCode.equals("AffixFileName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AffixFileName));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(FDSNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AffixFileCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AffixFileName);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("FDSNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FDSNo = FValue.trim();
            }
            else
            {
                FDSNo = null;
            }
        }
        if (FCode.equals("AffixFileCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixFileCode = FValue.trim();
            }
            else
            {
                AffixFileCode = null;
            }
        }
        if (FCode.equals("AffixFileName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixFileName = FValue.trim();
            }
            else
            {
                AffixFileName = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LHAffixServeicSchema other = (LHAffixServeicSchema) otherObject;
        return
                FDSNo.equals(other.getFDSNo())
                && AffixFileCode.equals(other.getAffixFileCode())
                && AffixFileName.equals(other.getAffixFileName());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("FDSNo"))
        {
            return 0;
        }
        if (strFieldName.equals("AffixFileCode"))
        {
            return 1;
        }
        if (strFieldName.equals("AffixFileName"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "FDSNo";
                break;
            case 1:
                strFieldName = "AffixFileCode";
                break;
            case 2:
                strFieldName = "AffixFileName";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("FDSNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AffixFileCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AffixFileName"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
