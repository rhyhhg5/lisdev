/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIPlaceNextInfoDB;

/*
 * <p>ClassName: LIPlaceNextInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 职场管理
 * @CreateDate：2012-08-22
 */
public class LIPlaceNextInfoSchema implements Schema, Cloneable
{
	// @Field
	/** 原职场编码 */
	private String Preplacno;
	/** 新职场编码 */
	private String Placeno;
	/** 保证金 */
	private double Marginfee;
	/** 保证金收回日期 */
	private Date Marginfeegetdate;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date Makedate;
	/** 入机时间 */
	private String Maketime;
	/** 修改日期 */
	private Date Modifydate;
	/** 修改时间 */
	private String Modifytime;
	/** 备用字符串属性1 */
	private String Standbystring1;
	/** 备用字符串属性2 */
	private String Standbystring2;
	/** 备用字符串属性3 */
	private String Standbystring3;
	/** 备用数字属性1 */
	private double Standbynum1;
	/** 备用数字属性2 */
	private double Standbynum2;
	/** 备用数字属性3 */
	private double Standbynum3;
	/** 备用日期属性1 */
	private Date Standbydate1;
	/** 备用日期属性2 */
	private Date Standbydate2;

	public static final int FIELDNUM = 17;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LIPlaceNextInfoSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "Preplacno";
		pk[1] = "Placeno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LIPlaceNextInfoSchema cloned = (LIPlaceNextInfoSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPreplacno()
	{
		return Preplacno;
	}
	public void setPreplacno(String aPreplacno)
	{
		Preplacno = aPreplacno;
	}
	public String getPlaceno()
	{
		return Placeno;
	}
	public void setPlaceno(String aPlaceno)
	{
		Placeno = aPlaceno;
	}
	public double getMarginfee()
	{
		return Marginfee;
	}
	public void setMarginfee(double aMarginfee)
	{
		Marginfee = Arith.round(aMarginfee,2);
	}
	public void setMarginfee(String aMarginfee)
	{
		if (aMarginfee != null && !aMarginfee.equals(""))
		{
			Double tDouble = new Double(aMarginfee);
			double d = tDouble.doubleValue();
                Marginfee = Arith.round(d,2);
		}
	}

	public String getMarginfeegetdate()
	{
		if( Marginfeegetdate != null )
			return fDate.getString(Marginfeegetdate);
		else
			return null;
	}
	public void setMarginfeegetdate(Date aMarginfeegetdate)
	{
		Marginfeegetdate = aMarginfeegetdate;
	}
	public void setMarginfeegetdate(String aMarginfeegetdate)
	{
		if (aMarginfeegetdate != null && !aMarginfeegetdate.equals("") )
		{
			Marginfeegetdate = fDate.getDate( aMarginfeegetdate );
		}
		else
			Marginfeegetdate = null;
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakedate()
	{
		if( Makedate != null )
			return fDate.getString(Makedate);
		else
			return null;
	}
	public void setMakedate(Date aMakedate)
	{
		Makedate = aMakedate;
	}
	public void setMakedate(String aMakedate)
	{
		if (aMakedate != null && !aMakedate.equals("") )
		{
			Makedate = fDate.getDate( aMakedate );
		}
		else
			Makedate = null;
	}

	public String getMaketime()
	{
		return Maketime;
	}
	public void setMaketime(String aMaketime)
	{
		Maketime = aMaketime;
	}
	public String getModifydate()
	{
		if( Modifydate != null )
			return fDate.getString(Modifydate);
		else
			return null;
	}
	public void setModifydate(Date aModifydate)
	{
		Modifydate = aModifydate;
	}
	public void setModifydate(String aModifydate)
	{
		if (aModifydate != null && !aModifydate.equals("") )
		{
			Modifydate = fDate.getDate( aModifydate );
		}
		else
			Modifydate = null;
	}

	public String getModifytime()
	{
		return Modifytime;
	}
	public void setModifytime(String aModifytime)
	{
		Modifytime = aModifytime;
	}
	public String getStandbystring1()
	{
		return Standbystring1;
	}
	public void setStandbystring1(String aStandbystring1)
	{
		Standbystring1 = aStandbystring1;
	}
	public String getStandbystring2()
	{
		return Standbystring2;
	}
	public void setStandbystring2(String aStandbystring2)
	{
		Standbystring2 = aStandbystring2;
	}
	public String getStandbystring3()
	{
		return Standbystring3;
	}
	public void setStandbystring3(String aStandbystring3)
	{
		Standbystring3 = aStandbystring3;
	}
	public double getStandbynum1()
	{
		return Standbynum1;
	}
	public void setStandbynum1(double aStandbynum1)
	{
		Standbynum1 = Arith.round(aStandbynum1,2);
	}
	public void setStandbynum1(String aStandbynum1)
	{
		if (aStandbynum1 != null && !aStandbynum1.equals(""))
		{
			Double tDouble = new Double(aStandbynum1);
			double d = tDouble.doubleValue();
                Standbynum1 = Arith.round(d,2);
		}
	}

	public double getStandbynum2()
	{
		return Standbynum2;
	}
	public void setStandbynum2(double aStandbynum2)
	{
		Standbynum2 = Arith.round(aStandbynum2,2);
	}
	public void setStandbynum2(String aStandbynum2)
	{
		if (aStandbynum2 != null && !aStandbynum2.equals(""))
		{
			Double tDouble = new Double(aStandbynum2);
			double d = tDouble.doubleValue();
                Standbynum2 = Arith.round(d,2);
		}
	}

	public double getStandbynum3()
	{
		return Standbynum3;
	}
	public void setStandbynum3(double aStandbynum3)
	{
		Standbynum3 = Arith.round(aStandbynum3,2);
	}
	public void setStandbynum3(String aStandbynum3)
	{
		if (aStandbynum3 != null && !aStandbynum3.equals(""))
		{
			Double tDouble = new Double(aStandbynum3);
			double d = tDouble.doubleValue();
                Standbynum3 = Arith.round(d,2);
		}
	}

	public String getStandbydate1()
	{
		if( Standbydate1 != null )
			return fDate.getString(Standbydate1);
		else
			return null;
	}
	public void setStandbydate1(Date aStandbydate1)
	{
		Standbydate1 = aStandbydate1;
	}
	public void setStandbydate1(String aStandbydate1)
	{
		if (aStandbydate1 != null && !aStandbydate1.equals("") )
		{
			Standbydate1 = fDate.getDate( aStandbydate1 );
		}
		else
			Standbydate1 = null;
	}

	public String getStandbydate2()
	{
		if( Standbydate2 != null )
			return fDate.getString(Standbydate2);
		else
			return null;
	}
	public void setStandbydate2(Date aStandbydate2)
	{
		Standbydate2 = aStandbydate2;
	}
	public void setStandbydate2(String aStandbydate2)
	{
		if (aStandbydate2 != null && !aStandbydate2.equals("") )
		{
			Standbydate2 = fDate.getDate( aStandbydate2 );
		}
		else
			Standbydate2 = null;
	}


	/**
	* 使用另外一个 LIPlaceNextInfoSchema 对象给 Schema 赋值
	* @param: aLIPlaceNextInfoSchema LIPlaceNextInfoSchema
	**/
	public void setSchema(LIPlaceNextInfoSchema aLIPlaceNextInfoSchema)
	{
		this.Preplacno = aLIPlaceNextInfoSchema.getPreplacno();
		this.Placeno = aLIPlaceNextInfoSchema.getPlaceno();
		this.Marginfee = aLIPlaceNextInfoSchema.getMarginfee();
		this.Marginfeegetdate = fDate.getDate( aLIPlaceNextInfoSchema.getMarginfeegetdate());
		this.Operator = aLIPlaceNextInfoSchema.getOperator();
		this.Makedate = fDate.getDate( aLIPlaceNextInfoSchema.getMakedate());
		this.Maketime = aLIPlaceNextInfoSchema.getMaketime();
		this.Modifydate = fDate.getDate( aLIPlaceNextInfoSchema.getModifydate());
		this.Modifytime = aLIPlaceNextInfoSchema.getModifytime();
		this.Standbystring1 = aLIPlaceNextInfoSchema.getStandbystring1();
		this.Standbystring2 = aLIPlaceNextInfoSchema.getStandbystring2();
		this.Standbystring3 = aLIPlaceNextInfoSchema.getStandbystring3();
		this.Standbynum1 = aLIPlaceNextInfoSchema.getStandbynum1();
		this.Standbynum2 = aLIPlaceNextInfoSchema.getStandbynum2();
		this.Standbynum3 = aLIPlaceNextInfoSchema.getStandbynum3();
		this.Standbydate1 = fDate.getDate( aLIPlaceNextInfoSchema.getStandbydate1());
		this.Standbydate2 = fDate.getDate( aLIPlaceNextInfoSchema.getStandbydate2());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Preplacno") == null )
				this.Preplacno = null;
			else
				this.Preplacno = rs.getString("Preplacno").trim();

			if( rs.getString("Placeno") == null )
				this.Placeno = null;
			else
				this.Placeno = rs.getString("Placeno").trim();

			this.Marginfee = rs.getDouble("Marginfee");
			this.Marginfeegetdate = rs.getDate("Marginfeegetdate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.Makedate = rs.getDate("Makedate");
			if( rs.getString("Maketime") == null )
				this.Maketime = null;
			else
				this.Maketime = rs.getString("Maketime").trim();

			this.Modifydate = rs.getDate("Modifydate");
			if( rs.getString("Modifytime") == null )
				this.Modifytime = null;
			else
				this.Modifytime = rs.getString("Modifytime").trim();

			if( rs.getString("Standbystring1") == null )
				this.Standbystring1 = null;
			else
				this.Standbystring1 = rs.getString("Standbystring1").trim();

			if( rs.getString("Standbystring2") == null )
				this.Standbystring2 = null;
			else
				this.Standbystring2 = rs.getString("Standbystring2").trim();

			if( rs.getString("Standbystring3") == null )
				this.Standbystring3 = null;
			else
				this.Standbystring3 = rs.getString("Standbystring3").trim();

			this.Standbynum1 = rs.getDouble("Standbynum1");
			this.Standbynum2 = rs.getDouble("Standbynum2");
			this.Standbynum3 = rs.getDouble("Standbynum3");
			this.Standbydate1 = rs.getDate("Standbydate1");
			this.Standbydate2 = rs.getDate("Standbydate2");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LIPlaceNextInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIPlaceNextInfoSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LIPlaceNextInfoSchema getSchema()
	{
		LIPlaceNextInfoSchema aLIPlaceNextInfoSchema = new LIPlaceNextInfoSchema();
		aLIPlaceNextInfoSchema.setSchema(this);
		return aLIPlaceNextInfoSchema;
	}

	public LIPlaceNextInfoDB getDB()
	{
		LIPlaceNextInfoDB aDBOper = new LIPlaceNextInfoDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIPlaceNextInfo描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Preplacno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Placeno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Marginfee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Marginfeegetdate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Makedate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Maketime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Modifydate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Modifytime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standbystring3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum1));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum2));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Standbynum3));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Standbydate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Standbydate2 )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLIPlaceNextInfo>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Preplacno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Placeno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Marginfee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).doubleValue();
			Marginfeegetdate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Makedate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			Maketime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Modifydate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			Modifytime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Standbystring1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Standbystring2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Standbystring3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Standbynum1 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			Standbynum2 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			Standbynum3 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			Standbydate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			Standbydate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LIPlaceNextInfoSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Preplacno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Preplacno));
		}
		if (FCode.equals("Placeno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Placeno));
		}
		if (FCode.equals("Marginfee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Marginfee));
		}
		if (FCode.equals("Marginfeegetdate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMarginfeegetdate()));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("Makedate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
		}
		if (FCode.equals("Maketime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Maketime));
		}
		if (FCode.equals("Modifydate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifydate()));
		}
		if (FCode.equals("Modifytime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Modifytime));
		}
		if (FCode.equals("Standbystring1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring1));
		}
		if (FCode.equals("Standbystring2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring2));
		}
		if (FCode.equals("Standbystring3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbystring3));
		}
		if (FCode.equals("Standbynum1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum1));
		}
		if (FCode.equals("Standbynum2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum2));
		}
		if (FCode.equals("Standbynum3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standbynum3));
		}
		if (FCode.equals("Standbydate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate1()));
		}
		if (FCode.equals("Standbydate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate2()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Preplacno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Placeno);
				break;
			case 2:
				strFieldValue = String.valueOf(Marginfee);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMarginfeegetdate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakedate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Maketime);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifydate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Modifytime);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Standbystring1);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Standbystring2);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Standbystring3);
				break;
			case 12:
				strFieldValue = String.valueOf(Standbynum1);
				break;
			case 13:
				strFieldValue = String.valueOf(Standbynum2);
				break;
			case 14:
				strFieldValue = String.valueOf(Standbynum3);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate1()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandbydate2()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Preplacno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Preplacno = FValue.trim();
			}
			else
				Preplacno = null;
		}
		if (FCode.equalsIgnoreCase("Placeno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Placeno = FValue.trim();
			}
			else
				Placeno = null;
		}
		if (FCode.equalsIgnoreCase("Marginfee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Marginfee = d;
			}
		}
		if (FCode.equalsIgnoreCase("Marginfeegetdate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Marginfeegetdate = fDate.getDate( FValue );
			}
			else
				Marginfeegetdate = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("Makedate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Makedate = fDate.getDate( FValue );
			}
			else
				Makedate = null;
		}
		if (FCode.equalsIgnoreCase("Maketime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Maketime = FValue.trim();
			}
			else
				Maketime = null;
		}
		if (FCode.equalsIgnoreCase("Modifydate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Modifydate = fDate.getDate( FValue );
			}
			else
				Modifydate = null;
		}
		if (FCode.equalsIgnoreCase("Modifytime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Modifytime = FValue.trim();
			}
			else
				Modifytime = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring1 = FValue.trim();
			}
			else
				Standbystring1 = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring2 = FValue.trim();
			}
			else
				Standbystring2 = null;
		}
		if (FCode.equalsIgnoreCase("Standbystring3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standbystring3 = FValue.trim();
			}
			else
				Standbystring3 = null;
		}
		if (FCode.equalsIgnoreCase("Standbynum1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum1 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbynum2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum2 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbynum3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Standbynum3 = d;
			}
		}
		if (FCode.equalsIgnoreCase("Standbydate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Standbydate1 = fDate.getDate( FValue );
			}
			else
				Standbydate1 = null;
		}
		if (FCode.equalsIgnoreCase("Standbydate2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Standbydate2 = fDate.getDate( FValue );
			}
			else
				Standbydate2 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LIPlaceNextInfoSchema other = (LIPlaceNextInfoSchema)otherObject;
		return
			(Preplacno == null ? other.getPreplacno() == null : Preplacno.equals(other.getPreplacno()))
			&& (Placeno == null ? other.getPlaceno() == null : Placeno.equals(other.getPlaceno()))
			&& Marginfee == other.getMarginfee()
			&& (Marginfeegetdate == null ? other.getMarginfeegetdate() == null : fDate.getString(Marginfeegetdate).equals(other.getMarginfeegetdate()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (Makedate == null ? other.getMakedate() == null : fDate.getString(Makedate).equals(other.getMakedate()))
			&& (Maketime == null ? other.getMaketime() == null : Maketime.equals(other.getMaketime()))
			&& (Modifydate == null ? other.getModifydate() == null : fDate.getString(Modifydate).equals(other.getModifydate()))
			&& (Modifytime == null ? other.getModifytime() == null : Modifytime.equals(other.getModifytime()))
			&& (Standbystring1 == null ? other.getStandbystring1() == null : Standbystring1.equals(other.getStandbystring1()))
			&& (Standbystring2 == null ? other.getStandbystring2() == null : Standbystring2.equals(other.getStandbystring2()))
			&& (Standbystring3 == null ? other.getStandbystring3() == null : Standbystring3.equals(other.getStandbystring3()))
			&& Standbynum1 == other.getStandbynum1()
			&& Standbynum2 == other.getStandbynum2()
			&& Standbynum3 == other.getStandbynum3()
			&& (Standbydate1 == null ? other.getStandbydate1() == null : fDate.getString(Standbydate1).equals(other.getStandbydate1()))
			&& (Standbydate2 == null ? other.getStandbydate2() == null : fDate.getString(Standbydate2).equals(other.getStandbydate2()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Preplacno") ) {
			return 0;
		}
		if( strFieldName.equals("Placeno") ) {
			return 1;
		}
		if( strFieldName.equals("Marginfee") ) {
			return 2;
		}
		if( strFieldName.equals("Marginfeegetdate") ) {
			return 3;
		}
		if( strFieldName.equals("Operator") ) {
			return 4;
		}
		if( strFieldName.equals("Makedate") ) {
			return 5;
		}
		if( strFieldName.equals("Maketime") ) {
			return 6;
		}
		if( strFieldName.equals("Modifydate") ) {
			return 7;
		}
		if( strFieldName.equals("Modifytime") ) {
			return 8;
		}
		if( strFieldName.equals("Standbystring1") ) {
			return 9;
		}
		if( strFieldName.equals("Standbystring2") ) {
			return 10;
		}
		if( strFieldName.equals("Standbystring3") ) {
			return 11;
		}
		if( strFieldName.equals("Standbynum1") ) {
			return 12;
		}
		if( strFieldName.equals("Standbynum2") ) {
			return 13;
		}
		if( strFieldName.equals("Standbynum3") ) {
			return 14;
		}
		if( strFieldName.equals("Standbydate1") ) {
			return 15;
		}
		if( strFieldName.equals("Standbydate2") ) {
			return 16;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Preplacno";
				break;
			case 1:
				strFieldName = "Placeno";
				break;
			case 2:
				strFieldName = "Marginfee";
				break;
			case 3:
				strFieldName = "Marginfeegetdate";
				break;
			case 4:
				strFieldName = "Operator";
				break;
			case 5:
				strFieldName = "Makedate";
				break;
			case 6:
				strFieldName = "Maketime";
				break;
			case 7:
				strFieldName = "Modifydate";
				break;
			case 8:
				strFieldName = "Modifytime";
				break;
			case 9:
				strFieldName = "Standbystring1";
				break;
			case 10:
				strFieldName = "Standbystring2";
				break;
			case 11:
				strFieldName = "Standbystring3";
				break;
			case 12:
				strFieldName = "Standbynum1";
				break;
			case 13:
				strFieldName = "Standbynum2";
				break;
			case 14:
				strFieldName = "Standbynum3";
				break;
			case 15:
				strFieldName = "Standbydate1";
				break;
			case 16:
				strFieldName = "Standbydate2";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Preplacno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Placeno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Marginfee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Marginfeegetdate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Makedate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Maketime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Modifydate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Modifytime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbystring3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standbynum1") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbynum2") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbynum3") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Standbydate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Standbydate2") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
