/*
 * <p>ClassName: LDCodeModSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDCodeModDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDCodeModSchema implements Schema
{
    // @Field
    /** 代码类型 */
    private String CodeType;
    /** 编码 */
    private String Code;
    /** 编码名称 */
    private String CodeName;
    /** 内容 */
    private String Cont;

    public static final int FIELDNUM = 4; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDCodeModSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "CodeType";
        pk[1] = "Code";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCodeType()
    {
        if (CodeType != null && !CodeType.equals("") && SysConst.CHANGECHARSET == true)
        {
            CodeType = StrTool.unicodeToGBK(CodeType);
        }
        return CodeType;
    }

    public void setCodeType(String aCodeType)
    {
        CodeType = aCodeType;
    }

    public String getCode()
    {
        if (Code != null && !Code.equals("") && SysConst.CHANGECHARSET == true)
        {
            Code = StrTool.unicodeToGBK(Code);
        }
        return Code;
    }

    public void setCode(String aCode)
    {
        Code = aCode;
    }

    public String getCodeName()
    {
        if (CodeName != null && !CodeName.equals("") && SysConst.CHANGECHARSET == true)
        {
            CodeName = StrTool.unicodeToGBK(CodeName);
        }
        return CodeName;
    }

    public void setCodeName(String aCodeName)
    {
        CodeName = aCodeName;
    }

    public String getCont()
    {
        if (Cont != null && !Cont.equals("") && SysConst.CHANGECHARSET == true)
        {
            Cont = StrTool.unicodeToGBK(Cont);
        }
        return Cont;
    }

    public void setCont(String aCont)
    {
        Cont = aCont;
    }

    /**
     * 使用另外一个 LDCodeModSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDCodeModSchema aLDCodeModSchema)
    {
        this.CodeType = aLDCodeModSchema.getCodeType();
        this.Code = aLDCodeModSchema.getCode();
        this.CodeName = aLDCodeModSchema.getCodeName();
        this.Cont = aLDCodeModSchema.getCont();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CodeType") == null)
            {
                this.CodeType = null;
            }
            else
            {
                this.CodeType = rs.getString("CodeType").trim();
            }

            if (rs.getString("Code") == null)
            {
                this.Code = null;
            }
            else
            {
                this.Code = rs.getString("Code").trim();
            }

            if (rs.getString("CodeName") == null)
            {
                this.CodeName = null;
            }
            else
            {
                this.CodeName = rs.getString("CodeName").trim();
            }

            if (rs.getString("Cont") == null)
            {
                this.Cont = null;
            }
            else
            {
                this.Cont = rs.getString("Cont").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDCodeModSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDCodeModSchema getSchema()
    {
        LDCodeModSchema aLDCodeModSchema = new LDCodeModSchema();
        aLDCodeModSchema.setSchema(this);
        return aLDCodeModSchema;
    }

    public LDCodeModDB getDB()
    {
        LDCodeModDB aDBOper = new LDCodeModDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDCodeMod描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(CodeType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Code)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CodeName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Cont));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDCodeMod>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CodeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                  SysConst.PACKAGESPILTER);
            CodeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            Cont = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                  SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDCodeModSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CodeType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CodeType));
        }
        if (FCode.equals("Code"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Code));
        }
        if (FCode.equals("CodeName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CodeName));
        }
        if (FCode.equals("Cont"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Cont));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CodeType);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(Code);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CodeName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(Cont);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CodeType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CodeType = FValue.trim();
            }
            else
            {
                CodeType = null;
            }
        }
        if (FCode.equals("Code"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Code = FValue.trim();
            }
            else
            {
                Code = null;
            }
        }
        if (FCode.equals("CodeName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CodeName = FValue.trim();
            }
            else
            {
                CodeName = null;
            }
        }
        if (FCode.equals("Cont"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Cont = FValue.trim();
            }
            else
            {
                Cont = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDCodeModSchema other = (LDCodeModSchema) otherObject;
        return
                CodeType.equals(other.getCodeType())
                && Code.equals(other.getCode())
                && CodeName.equals(other.getCodeName())
                && Cont.equals(other.getCont());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CodeType"))
        {
            return 0;
        }
        if (strFieldName.equals("Code"))
        {
            return 1;
        }
        if (strFieldName.equals("CodeName"))
        {
            return 2;
        }
        if (strFieldName.equals("Cont"))
        {
            return 3;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CodeType";
                break;
            case 1:
                strFieldName = "Code";
                break;
            case 2:
                strFieldName = "CodeName";
                break;
            case 3:
                strFieldName = "Cont";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CodeType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Code"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CodeName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Cont"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
