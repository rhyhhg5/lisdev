/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAAgentCompreScoreDB;

/*
 * <p>ClassName: LAAgentCompreScoreSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 互动开发表
 * @CreateDate：2014-12-22
 */
public class LAAgentCompreScoreSchema implements Schema, Cloneable
{
	// @Field
	/** 代理人编码 */
	private String AgentCode;
	/** 佣金计算年月代码 */
	private String WageNo;
	/** 项目代码 */
	private String ProjectCode;
	/** 项目名称 */
	private String ProjectName;
	/** 展业类型 */
	private String BranchType;
	/** 寿险得分 */
	private double LifeScore;
	/** 财险得分 */
	private double PropertyScore;
	/** 得分小计 */
	private double SumScore;
	/** 渠道 */
	private String BranchType2;
	/** 状态 */
	private String State;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 15;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAAgentCompreScoreSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "AgentCode";
		pk[1] = "WageNo";
		pk[2] = "ProjectCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAAgentCompreScoreSchema cloned = (LAAgentCompreScoreSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public String getProjectCode()
	{
		return ProjectCode;
	}
	public void setProjectCode(String aProjectCode)
	{
		ProjectCode = aProjectCode;
	}
	public String getProjectName()
	{
		return ProjectName;
	}
	public void setProjectName(String aProjectName)
	{
		ProjectName = aProjectName;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public double getLifeScore()
	{
		return LifeScore;
	}
	public void setLifeScore(double aLifeScore)
	{
		LifeScore = Arith.round(aLifeScore,2);
	}
	public void setLifeScore(String aLifeScore)
	{
		if (aLifeScore != null && !aLifeScore.equals(""))
		{
			Double tDouble = new Double(aLifeScore);
			double d = tDouble.doubleValue();
                LifeScore = Arith.round(d,2);
		}
	}

	public double getPropertyScore()
	{
		return PropertyScore;
	}
	public void setPropertyScore(double aPropertyScore)
	{
		PropertyScore = Arith.round(aPropertyScore,2);
	}
	public void setPropertyScore(String aPropertyScore)
	{
		if (aPropertyScore != null && !aPropertyScore.equals(""))
		{
			Double tDouble = new Double(aPropertyScore);
			double d = tDouble.doubleValue();
                PropertyScore = Arith.round(d,2);
		}
	}

	public double getSumScore()
	{
		return SumScore;
	}
	public void setSumScore(double aSumScore)
	{
		SumScore = Arith.round(aSumScore,2);
	}
	public void setSumScore(String aSumScore)
	{
		if (aSumScore != null && !aSumScore.equals(""))
		{
			Double tDouble = new Double(aSumScore);
			double d = tDouble.doubleValue();
                SumScore = Arith.round(d,2);
		}
	}

	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LAAgentCompreScoreSchema 对象给 Schema 赋值
	* @param: aLAAgentCompreScoreSchema LAAgentCompreScoreSchema
	**/
	public void setSchema(LAAgentCompreScoreSchema aLAAgentCompreScoreSchema)
	{
		this.AgentCode = aLAAgentCompreScoreSchema.getAgentCode();
		this.WageNo = aLAAgentCompreScoreSchema.getWageNo();
		this.ProjectCode = aLAAgentCompreScoreSchema.getProjectCode();
		this.ProjectName = aLAAgentCompreScoreSchema.getProjectName();
		this.BranchType = aLAAgentCompreScoreSchema.getBranchType();
		this.LifeScore = aLAAgentCompreScoreSchema.getLifeScore();
		this.PropertyScore = aLAAgentCompreScoreSchema.getPropertyScore();
		this.SumScore = aLAAgentCompreScoreSchema.getSumScore();
		this.BranchType2 = aLAAgentCompreScoreSchema.getBranchType2();
		this.State = aLAAgentCompreScoreSchema.getState();
		this.Operator = aLAAgentCompreScoreSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAAgentCompreScoreSchema.getMakeDate());
		this.MakeTime = aLAAgentCompreScoreSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAAgentCompreScoreSchema.getModifyDate());
		this.ModifyTime = aLAAgentCompreScoreSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			if( rs.getString("ProjectCode") == null )
				this.ProjectCode = null;
			else
				this.ProjectCode = rs.getString("ProjectCode").trim();

			if( rs.getString("ProjectName") == null )
				this.ProjectName = null;
			else
				this.ProjectName = rs.getString("ProjectName").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			this.LifeScore = rs.getDouble("LifeScore");
			this.PropertyScore = rs.getDouble("PropertyScore");
			this.SumScore = rs.getDouble("SumScore");
			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAAgentCompreScore表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAgentCompreScoreSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAAgentCompreScoreSchema getSchema()
	{
		LAAgentCompreScoreSchema aLAAgentCompreScoreSchema = new LAAgentCompreScoreSchema();
		aLAAgentCompreScoreSchema.setSchema(this);
		return aLAAgentCompreScoreSchema;
	}

	public LAAgentCompreScoreDB getDB()
	{
		LAAgentCompreScoreDB aDBOper = new LAAgentCompreScoreDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentCompreScore描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(LifeScore));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PropertyScore));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumScore));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentCompreScore>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ProjectCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ProjectName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			LifeScore = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			PropertyScore = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			SumScore = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAgentCompreScoreSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("ProjectCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectCode));
		}
		if (FCode.equals("ProjectName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectName));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("LifeScore"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LifeScore));
		}
		if (FCode.equals("PropertyScore"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PropertyScore));
		}
		if (FCode.equals("SumScore"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumScore));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ProjectCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ProjectName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 5:
				strFieldValue = String.valueOf(LifeScore);
				break;
			case 6:
				strFieldValue = String.valueOf(PropertyScore);
				break;
			case 7:
				strFieldValue = String.valueOf(SumScore);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("ProjectCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectCode = FValue.trim();
			}
			else
				ProjectCode = null;
		}
		if (FCode.equalsIgnoreCase("ProjectName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectName = FValue.trim();
			}
			else
				ProjectName = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("LifeScore"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				LifeScore = d;
			}
		}
		if (FCode.equalsIgnoreCase("PropertyScore"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				PropertyScore = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumScore"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumScore = d;
			}
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAAgentCompreScoreSchema other = (LAAgentCompreScoreSchema)otherObject;
		return
			(AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& (ProjectCode == null ? other.getProjectCode() == null : ProjectCode.equals(other.getProjectCode()))
			&& (ProjectName == null ? other.getProjectName() == null : ProjectName.equals(other.getProjectName()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& LifeScore == other.getLifeScore()
			&& PropertyScore == other.getPropertyScore()
			&& SumScore == other.getSumScore()
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("AgentCode") ) {
			return 0;
		}
		if( strFieldName.equals("WageNo") ) {
			return 1;
		}
		if( strFieldName.equals("ProjectCode") ) {
			return 2;
		}
		if( strFieldName.equals("ProjectName") ) {
			return 3;
		}
		if( strFieldName.equals("BranchType") ) {
			return 4;
		}
		if( strFieldName.equals("LifeScore") ) {
			return 5;
		}
		if( strFieldName.equals("PropertyScore") ) {
			return 6;
		}
		if( strFieldName.equals("SumScore") ) {
			return 7;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 8;
		}
		if( strFieldName.equals("State") ) {
			return 9;
		}
		if( strFieldName.equals("Operator") ) {
			return 10;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 14;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "AgentCode";
				break;
			case 1:
				strFieldName = "WageNo";
				break;
			case 2:
				strFieldName = "ProjectCode";
				break;
			case 3:
				strFieldName = "ProjectName";
				break;
			case 4:
				strFieldName = "BranchType";
				break;
			case 5:
				strFieldName = "LifeScore";
				break;
			case 6:
				strFieldName = "PropertyScore";
				break;
			case 7:
				strFieldName = "SumScore";
				break;
			case 8:
				strFieldName = "BranchType2";
				break;
			case 9:
				strFieldName = "State";
				break;
			case 10:
				strFieldName = "Operator";
				break;
			case 11:
				strFieldName = "MakeDate";
				break;
			case 12:
				strFieldName = "MakeTime";
				break;
			case 13:
				strFieldName = "ModifyDate";
				break;
			case 14:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LifeScore") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("PropertyScore") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumScore") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
