/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FMVoucherDefDB;

/*
 * <p>ClassName: FMVoucherDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FMVoucherDefSchema implements Schema, Cloneable
{
	// @Field
	/** 维护明细号 */
	private String MaintNo;
	/** 版本编号 */
	private String VersionNo;
	/** 凭证编号 */
	private String VoucherID;
	/** 凭证名称 */
	private String VoucherName;
	/** 凭证类型 */
	private String VoucherType;
	/** 凭证类型名称 */
	private String VoucherTypeName;
	/** 凭证描述 */
	private String Remark;
	/** 状态 */
	private String State;

	public static final int FIELDNUM = 8;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FMVoucherDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "MaintNo";
		pk[1] = "VoucherID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FMVoucherDefSchema cloned = (FMVoucherDefSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMaintNo()
	{
		return MaintNo;
	}
	public void setMaintNo(String aMaintNo)
	{
		MaintNo = aMaintNo;
	}
	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getVoucherID()
	{
		return VoucherID;
	}
	public void setVoucherID(String aVoucherID)
	{
		VoucherID = aVoucherID;
	}
	public String getVoucherName()
	{
		return VoucherName;
	}
	public void setVoucherName(String aVoucherName)
	{
		VoucherName = aVoucherName;
	}
	public String getVoucherType()
	{
		return VoucherType;
	}
	public void setVoucherType(String aVoucherType)
	{
		VoucherType = aVoucherType;
	}
	public String getVoucherTypeName()
	{
		return VoucherTypeName;
	}
	public void setVoucherTypeName(String aVoucherTypeName)
	{
		VoucherTypeName = aVoucherTypeName;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}

	/**
	* 使用另外一个 FMVoucherDefSchema 对象给 Schema 赋值
	* @param: aFMVoucherDefSchema FMVoucherDefSchema
	**/
	public void setSchema(FMVoucherDefSchema aFMVoucherDefSchema)
	{
		this.MaintNo = aFMVoucherDefSchema.getMaintNo();
		this.VersionNo = aFMVoucherDefSchema.getVersionNo();
		this.VoucherID = aFMVoucherDefSchema.getVoucherID();
		this.VoucherName = aFMVoucherDefSchema.getVoucherName();
		this.VoucherType = aFMVoucherDefSchema.getVoucherType();
		this.VoucherTypeName = aFMVoucherDefSchema.getVoucherTypeName();
		this.Remark = aFMVoucherDefSchema.getRemark();
		this.State = aFMVoucherDefSchema.getState();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MaintNo") == null )
				this.MaintNo = null;
			else
				this.MaintNo = rs.getString("MaintNo").trim();

			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("VoucherID") == null )
				this.VoucherID = null;
			else
				this.VoucherID = rs.getString("VoucherID").trim();

			if( rs.getString("VoucherName") == null )
				this.VoucherName = null;
			else
				this.VoucherName = rs.getString("VoucherName").trim();

			if( rs.getString("VoucherType") == null )
				this.VoucherType = null;
			else
				this.VoucherType = rs.getString("VoucherType").trim();

			if( rs.getString("VoucherTypeName") == null )
				this.VoucherTypeName = null;
			else
				this.VoucherTypeName = rs.getString("VoucherTypeName").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FMVoucherDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FMVoucherDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FMVoucherDefSchema getSchema()
	{
		FMVoucherDefSchema aFMVoucherDefSchema = new FMVoucherDefSchema();
		aFMVoucherDefSchema.setSchema(this);
		return aFMVoucherDefSchema;
	}

	public FMVoucherDefDB getDB()
	{
		FMVoucherDefDB aDBOper = new FMVoucherDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFMVoucherDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(MaintNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherTypeName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFMVoucherDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MaintNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			VoucherID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			VoucherName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			VoucherType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			VoucherTypeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FMVoucherDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MaintNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaintNo));
		}
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("VoucherID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherID));
		}
		if (FCode.equals("VoucherName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherName));
		}
		if (FCode.equals("VoucherType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherType));
		}
		if (FCode.equals("VoucherTypeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherTypeName));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MaintNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(VoucherID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(VoucherName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(VoucherType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(VoucherTypeName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MaintNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaintNo = FValue.trim();
			}
			else
				MaintNo = null;
		}
		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("VoucherID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherID = FValue.trim();
			}
			else
				VoucherID = null;
		}
		if (FCode.equalsIgnoreCase("VoucherName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherName = FValue.trim();
			}
			else
				VoucherName = null;
		}
		if (FCode.equalsIgnoreCase("VoucherType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherType = FValue.trim();
			}
			else
				VoucherType = null;
		}
		if (FCode.equalsIgnoreCase("VoucherTypeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherTypeName = FValue.trim();
			}
			else
				VoucherTypeName = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FMVoucherDefSchema other = (FMVoucherDefSchema)otherObject;
		return
			(MaintNo == null ? other.getMaintNo() == null : MaintNo.equals(other.getMaintNo()))
			&& (VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (VoucherID == null ? other.getVoucherID() == null : VoucherID.equals(other.getVoucherID()))
			&& (VoucherName == null ? other.getVoucherName() == null : VoucherName.equals(other.getVoucherName()))
			&& (VoucherType == null ? other.getVoucherType() == null : VoucherType.equals(other.getVoucherType()))
			&& (VoucherTypeName == null ? other.getVoucherTypeName() == null : VoucherTypeName.equals(other.getVoucherTypeName()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MaintNo") ) {
			return 0;
		}
		if( strFieldName.equals("VersionNo") ) {
			return 1;
		}
		if( strFieldName.equals("VoucherID") ) {
			return 2;
		}
		if( strFieldName.equals("VoucherName") ) {
			return 3;
		}
		if( strFieldName.equals("VoucherType") ) {
			return 4;
		}
		if( strFieldName.equals("VoucherTypeName") ) {
			return 5;
		}
		if( strFieldName.equals("Remark") ) {
			return 6;
		}
		if( strFieldName.equals("State") ) {
			return 7;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MaintNo";
				break;
			case 1:
				strFieldName = "VersionNo";
				break;
			case 2:
				strFieldName = "VoucherID";
				break;
			case 3:
				strFieldName = "VoucherName";
				break;
			case 4:
				strFieldName = "VoucherType";
				break;
			case 5:
				strFieldName = "VoucherTypeName";
				break;
			case 6:
				strFieldName = "Remark";
				break;
			case 7:
				strFieldName = "State";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MaintNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherTypeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
