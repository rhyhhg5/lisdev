/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHContItemDB;

/*
 * <p>ClassName: LHContItemSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-03-06
 */
public class LHContItemSchema implements Schema, Cloneable
{
	// @Field
	/** 合同编号 */
	private String ContraNo;
	/** 合同责任项目编号 */
	private String ContraItemNo;
	/** 责任项目编号 */
	private String DutyItemCode;
	/** 责任当前状态 */
	private String DutyState;
	/** 责任说明 */
	private String DutyExplai;
	/** 责任执行人 */
	private String DoctNo;
	/** 责任项目联系人 */
	private String Dutylinkman;
	/** 责任项目联系方式 */
	private String DutyContact;
	/** 费用说明 */
	private String FeeExplai;
	/** 责任起始日期 */
	private Date DutyBeginDate;
	/** 责任终止日期 */
	private Date DutyEndDate;
	/** 计费方式代码 */
	private String PayModeCode;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 管理机构 */
	private String ManageCom;
	/** 合同责任项目类型 */
	private String ContraType;
	/** 关联责任标识 */
	private String ContraFlag;

	public static final int FIELDNUM = 20;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LHContItemSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "ContraNo";
		pk[1] = "ContraItemNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LHContItemSchema cloned = (LHContItemSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getContraNo()
	{
		return ContraNo;
	}
	public void setContraNo(String aContraNo)
	{
            ContraNo = aContraNo;
	}
	public String getContraItemNo()
	{
		return ContraItemNo;
	}
	public void setContraItemNo(String aContraItemNo)
	{
            ContraItemNo = aContraItemNo;
	}
	public String getDutyItemCode()
	{
		return DutyItemCode;
	}
	public void setDutyItemCode(String aDutyItemCode)
	{
            DutyItemCode = aDutyItemCode;
	}
	public String getDutyState()
	{
		return DutyState;
	}
	public void setDutyState(String aDutyState)
	{
            DutyState = aDutyState;
	}
	public String getDutyExplai()
	{
		return DutyExplai;
	}
	public void setDutyExplai(String aDutyExplai)
	{
            DutyExplai = aDutyExplai;
	}
	public String getDoctNo()
	{
		return DoctNo;
	}
	public void setDoctNo(String aDoctNo)
	{
            DoctNo = aDoctNo;
	}
	public String getDutylinkman()
	{
		return Dutylinkman;
	}
	public void setDutylinkman(String aDutylinkman)
	{
            Dutylinkman = aDutylinkman;
	}
	public String getDutyContact()
	{
		return DutyContact;
	}
	public void setDutyContact(String aDutyContact)
	{
            DutyContact = aDutyContact;
	}
	public String getFeeExplai()
	{
		return FeeExplai;
	}
	public void setFeeExplai(String aFeeExplai)
	{
            FeeExplai = aFeeExplai;
	}
	public String getDutyBeginDate()
	{
		if( DutyBeginDate != null )
			return fDate.getString(DutyBeginDate);
		else
			return null;
	}
	public void setDutyBeginDate(Date aDutyBeginDate)
	{
            DutyBeginDate = aDutyBeginDate;
	}
	public void setDutyBeginDate(String aDutyBeginDate)
	{
		if (aDutyBeginDate != null && !aDutyBeginDate.equals("") )
		{
			DutyBeginDate = fDate.getDate( aDutyBeginDate );
		}
		else
			DutyBeginDate = null;
	}

	public String getDutyEndDate()
	{
		if( DutyEndDate != null )
			return fDate.getString(DutyEndDate);
		else
			return null;
	}
	public void setDutyEndDate(Date aDutyEndDate)
	{
            DutyEndDate = aDutyEndDate;
	}
	public void setDutyEndDate(String aDutyEndDate)
	{
		if (aDutyEndDate != null && !aDutyEndDate.equals("") )
		{
			DutyEndDate = fDate.getDate( aDutyEndDate );
		}
		else
			DutyEndDate = null;
	}

	public String getPayModeCode()
	{
		return PayModeCode;
	}
	public void setPayModeCode(String aPayModeCode)
	{
            PayModeCode = aPayModeCode;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getContraType()
	{
		return ContraType;
	}
	public void setContraType(String aContraType)
	{
            ContraType = aContraType;
	}
	public String getContraFlag()
	{
		return ContraFlag;
	}
	public void setContraFlag(String aContraFlag)
	{
            ContraFlag = aContraFlag;
	}

	/**
	* 使用另外一个 LHContItemSchema 对象给 Schema 赋值
	* @param: aLHContItemSchema LHContItemSchema
	**/
	public void setSchema(LHContItemSchema aLHContItemSchema)
	{
		this.ContraNo = aLHContItemSchema.getContraNo();
		this.ContraItemNo = aLHContItemSchema.getContraItemNo();
		this.DutyItemCode = aLHContItemSchema.getDutyItemCode();
		this.DutyState = aLHContItemSchema.getDutyState();
		this.DutyExplai = aLHContItemSchema.getDutyExplai();
		this.DoctNo = aLHContItemSchema.getDoctNo();
		this.Dutylinkman = aLHContItemSchema.getDutylinkman();
		this.DutyContact = aLHContItemSchema.getDutyContact();
		this.FeeExplai = aLHContItemSchema.getFeeExplai();
		this.DutyBeginDate = fDate.getDate( aLHContItemSchema.getDutyBeginDate());
		this.DutyEndDate = fDate.getDate( aLHContItemSchema.getDutyEndDate());
		this.PayModeCode = aLHContItemSchema.getPayModeCode();
		this.Operator = aLHContItemSchema.getOperator();
		this.MakeDate = fDate.getDate( aLHContItemSchema.getMakeDate());
		this.MakeTime = aLHContItemSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLHContItemSchema.getModifyDate());
		this.ModifyTime = aLHContItemSchema.getModifyTime();
		this.ManageCom = aLHContItemSchema.getManageCom();
		this.ContraType = aLHContItemSchema.getContraType();
		this.ContraFlag = aLHContItemSchema.getContraFlag();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ContraNo") == null )
				this.ContraNo = null;
			else
				this.ContraNo = rs.getString("ContraNo").trim();

			if( rs.getString("ContraItemNo") == null )
				this.ContraItemNo = null;
			else
				this.ContraItemNo = rs.getString("ContraItemNo").trim();

			if( rs.getString("DutyItemCode") == null )
				this.DutyItemCode = null;
			else
				this.DutyItemCode = rs.getString("DutyItemCode").trim();

			if( rs.getString("DutyState") == null )
				this.DutyState = null;
			else
				this.DutyState = rs.getString("DutyState").trim();

			if( rs.getString("DutyExplai") == null )
				this.DutyExplai = null;
			else
				this.DutyExplai = rs.getString("DutyExplai").trim();

			if( rs.getString("DoctNo") == null )
				this.DoctNo = null;
			else
				this.DoctNo = rs.getString("DoctNo").trim();

			if( rs.getString("Dutylinkman") == null )
				this.Dutylinkman = null;
			else
				this.Dutylinkman = rs.getString("Dutylinkman").trim();

			if( rs.getString("DutyContact") == null )
				this.DutyContact = null;
			else
				this.DutyContact = rs.getString("DutyContact").trim();

			if( rs.getString("FeeExplai") == null )
				this.FeeExplai = null;
			else
				this.FeeExplai = rs.getString("FeeExplai").trim();

			this.DutyBeginDate = rs.getDate("DutyBeginDate");
			this.DutyEndDate = rs.getDate("DutyEndDate");
			if( rs.getString("PayModeCode") == null )
				this.PayModeCode = null;
			else
				this.PayModeCode = rs.getString("PayModeCode").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("ContraType") == null )
				this.ContraType = null;
			else
				this.ContraType = rs.getString("ContraType").trim();

			if( rs.getString("ContraFlag") == null )
				this.ContraFlag = null;
			else
				this.ContraFlag = rs.getString("ContraFlag").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LHContItem表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHContItemSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LHContItemSchema getSchema()
	{
		LHContItemSchema aLHContItemSchema = new LHContItemSchema();
		aLHContItemSchema.setSchema(this);
		return aLHContItemSchema;
	}

	public LHContItemDB getDB()
	{
		LHContItemDB aDBOper = new LHContItemDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHContItem描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(ContraNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContraItemNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DutyItemCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DutyState)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DutyExplai)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DoctNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Dutylinkman)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DutyContact)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FeeExplai)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( DutyBeginDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( DutyEndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PayModeCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContraType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ContraFlag));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHContItem>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ContraNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ContraItemNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			DutyItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DutyState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			DutyExplai = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			DoctNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Dutylinkman = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			DutyContact = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			FeeExplai = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			DutyBeginDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			DutyEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			PayModeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ContraType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ContraFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LHContItemSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ContraNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraNo));
		}
		if (FCode.equals("ContraItemNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraItemNo));
		}
		if (FCode.equals("DutyItemCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DutyItemCode));
		}
		if (FCode.equals("DutyState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DutyState));
		}
		if (FCode.equals("DutyExplai"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DutyExplai));
		}
		if (FCode.equals("DoctNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctNo));
		}
		if (FCode.equals("Dutylinkman"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Dutylinkman));
		}
		if (FCode.equals("DutyContact"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DutyContact));
		}
		if (FCode.equals("FeeExplai"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeExplai));
		}
		if (FCode.equals("DutyBeginDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDutyBeginDate()));
		}
		if (FCode.equals("DutyEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDutyEndDate()));
		}
		if (FCode.equals("PayModeCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayModeCode));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ContraType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraType));
		}
		if (FCode.equals("ContraFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContraFlag));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ContraNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ContraItemNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(DutyItemCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(DutyState);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(DutyExplai);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(DoctNo);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Dutylinkman);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(DutyContact);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(FeeExplai);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDutyBeginDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDutyEndDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(PayModeCode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ContraType);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ContraFlag);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ContraNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraNo = FValue.trim();
			}
			else
				ContraNo = null;
		}
		if (FCode.equalsIgnoreCase("ContraItemNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraItemNo = FValue.trim();
			}
			else
				ContraItemNo = null;
		}
		if (FCode.equalsIgnoreCase("DutyItemCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DutyItemCode = FValue.trim();
			}
			else
				DutyItemCode = null;
		}
		if (FCode.equalsIgnoreCase("DutyState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DutyState = FValue.trim();
			}
			else
				DutyState = null;
		}
		if (FCode.equalsIgnoreCase("DutyExplai"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DutyExplai = FValue.trim();
			}
			else
				DutyExplai = null;
		}
		if (FCode.equalsIgnoreCase("DoctNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctNo = FValue.trim();
			}
			else
				DoctNo = null;
		}
		if (FCode.equalsIgnoreCase("Dutylinkman"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Dutylinkman = FValue.trim();
			}
			else
				Dutylinkman = null;
		}
		if (FCode.equalsIgnoreCase("DutyContact"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DutyContact = FValue.trim();
			}
			else
				DutyContact = null;
		}
		if (FCode.equalsIgnoreCase("FeeExplai"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeExplai = FValue.trim();
			}
			else
				FeeExplai = null;
		}
		if (FCode.equalsIgnoreCase("DutyBeginDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DutyBeginDate = fDate.getDate( FValue );
			}
			else
				DutyBeginDate = null;
		}
		if (FCode.equalsIgnoreCase("DutyEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DutyEndDate = fDate.getDate( FValue );
			}
			else
				DutyEndDate = null;
		}
		if (FCode.equalsIgnoreCase("PayModeCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayModeCode = FValue.trim();
			}
			else
				PayModeCode = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ContraType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraType = FValue.trim();
			}
			else
				ContraType = null;
		}
		if (FCode.equalsIgnoreCase("ContraFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContraFlag = FValue.trim();
			}
			else
				ContraFlag = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LHContItemSchema other = (LHContItemSchema)otherObject;
		return
			ContraNo.equals(other.getContraNo())
			&& ContraItemNo.equals(other.getContraItemNo())
			&& DutyItemCode.equals(other.getDutyItemCode())
			&& DutyState.equals(other.getDutyState())
			&& DutyExplai.equals(other.getDutyExplai())
			&& DoctNo.equals(other.getDoctNo())
			&& Dutylinkman.equals(other.getDutylinkman())
			&& DutyContact.equals(other.getDutyContact())
			&& FeeExplai.equals(other.getFeeExplai())
			&& fDate.getString(DutyBeginDate).equals(other.getDutyBeginDate())
			&& fDate.getString(DutyEndDate).equals(other.getDutyEndDate())
			&& PayModeCode.equals(other.getPayModeCode())
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& ManageCom.equals(other.getManageCom())
			&& ContraType.equals(other.getContraType())
			&& ContraFlag.equals(other.getContraFlag());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ContraNo") ) {
			return 0;
		}
		if( strFieldName.equals("ContraItemNo") ) {
			return 1;
		}
		if( strFieldName.equals("DutyItemCode") ) {
			return 2;
		}
		if( strFieldName.equals("DutyState") ) {
			return 3;
		}
		if( strFieldName.equals("DutyExplai") ) {
			return 4;
		}
		if( strFieldName.equals("DoctNo") ) {
			return 5;
		}
		if( strFieldName.equals("Dutylinkman") ) {
			return 6;
		}
		if( strFieldName.equals("DutyContact") ) {
			return 7;
		}
		if( strFieldName.equals("FeeExplai") ) {
			return 8;
		}
		if( strFieldName.equals("DutyBeginDate") ) {
			return 9;
		}
		if( strFieldName.equals("DutyEndDate") ) {
			return 10;
		}
		if( strFieldName.equals("PayModeCode") ) {
			return 11;
		}
		if( strFieldName.equals("Operator") ) {
			return 12;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 13;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 15;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 16;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 17;
		}
		if( strFieldName.equals("ContraType") ) {
			return 18;
		}
		if( strFieldName.equals("ContraFlag") ) {
			return 19;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ContraNo";
				break;
			case 1:
				strFieldName = "ContraItemNo";
				break;
			case 2:
				strFieldName = "DutyItemCode";
				break;
			case 3:
				strFieldName = "DutyState";
				break;
			case 4:
				strFieldName = "DutyExplai";
				break;
			case 5:
				strFieldName = "DoctNo";
				break;
			case 6:
				strFieldName = "Dutylinkman";
				break;
			case 7:
				strFieldName = "DutyContact";
				break;
			case 8:
				strFieldName = "FeeExplai";
				break;
			case 9:
				strFieldName = "DutyBeginDate";
				break;
			case 10:
				strFieldName = "DutyEndDate";
				break;
			case 11:
				strFieldName = "PayModeCode";
				break;
			case 12:
				strFieldName = "Operator";
				break;
			case 13:
				strFieldName = "MakeDate";
				break;
			case 14:
				strFieldName = "MakeTime";
				break;
			case 15:
				strFieldName = "ModifyDate";
				break;
			case 16:
				strFieldName = "ModifyTime";
				break;
			case 17:
				strFieldName = "ManageCom";
				break;
			case 18:
				strFieldName = "ContraType";
				break;
			case 19:
				strFieldName = "ContraFlag";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ContraNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContraItemNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DutyItemCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DutyState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DutyExplai") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoctNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Dutylinkman") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DutyContact") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeExplai") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DutyBeginDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DutyEndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PayModeCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContraType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContraFlag") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
