/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;


/*
 * <p>ClassName: LWMissionSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 工作流时间统计
 * @CreateDate：2005-04-07
 */
public class LWMissionSchema implements Schema
{
    // @Field
    /** 任务id */
    private String MissionID;

    /** 子任务id */
    private String SubMissionID;

    /** 过程id */
    private String ProcessID;

    /** 当前活动id */
    private String ActivityID;

    /** 当前活动状态 */
    private String ActivityStatus;

    /** 任务属性1 */
    private String MissionProp1;

    /** 任务属性2 */
    private String MissionProp2;

    /** 任务属性3 */
    private String MissionProp3;

    /** 任务属性4 */
    private String MissionProp4;

    /** 任务属性5 */
    private String MissionProp5;

    /** 任务属性6 */
    private String MissionProp6;

    /** 任务属性7 */
    private String MissionProp7;

    /** 任务属性8 */
    private String MissionProp8;

    /** 任务属性9 */
    private String MissionProp9;

    /** 任务属性10 */
    private String MissionProp10;

    /** 任务属性11 */
    private String MissionProp11;

    /** 任务属性12 */
    private String MissionProp12;

    /** 任务属性13 */
    private String MissionProp13;

    /** 任务属性14 */
    private String MissionProp14;

    /** 任务属性15 */
    private String MissionProp15;

    /** 任务属性16 */
    private String MissionProp16;

    /** 任务属性17 */
    private String MissionProp17;

    /** 任务属性18 */
    private String MissionProp18;

    /** 任务属性19 */
    private String MissionProp19;

    /** 任务属性20 */
    private String MissionProp20;

    /** 默认提交的操作员代码 */
    private String DefaultOperator;

    /** 最后操作员代码 */
    private String LastOperator;

    /** 创建者操作员代码 */
    private String CreateOperator;

    /** 入机日期 */
    private Date MakeDate;

    /** 入机时间 */
    private String MakeTime;

    /** 最后一次修改日期 */
    private Date ModifyDate;

    /** 最后一次修改时间 */
    private String ModifyTime;

    /** 进入日期 */
    private Date InDate;

    /** 进入时间 */
    private String InTime;

    /** 退出日期 */
    private Date OutDate;

    /** 退出时间 */
    private String OutTime;

    public static final int FIELDNUM = 36; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息


    // @Constructor
    public LWMissionSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "MissionID";
        pk[1] = "SubMissionID";
        pk[2] = "ActivityID";

        PK = pk;
    }


    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getMissionID()
    {
        if (SysConst.CHANGECHARSET && MissionID != null && !MissionID.equals(""))
        {
            MissionID = StrTool.unicodeToGBK(MissionID);
        }
        return MissionID;
    }

    public void setMissionID(String aMissionID)
    {
        MissionID = aMissionID;
    }

    public String getSubMissionID()
    {
        if (SysConst.CHANGECHARSET && SubMissionID != null &&
            !SubMissionID.equals(""))
        {
            SubMissionID = StrTool.unicodeToGBK(SubMissionID);
        }
        return SubMissionID;
    }

    public void setSubMissionID(String aSubMissionID)
    {
        SubMissionID = aSubMissionID;
    }

    public String getProcessID()
    {
        if (SysConst.CHANGECHARSET && ProcessID != null && !ProcessID.equals(""))
        {
            ProcessID = StrTool.unicodeToGBK(ProcessID);
        }
        return ProcessID;
    }

    public void setProcessID(String aProcessID)
    {
        ProcessID = aProcessID;
    }

    public String getActivityID()
    {
        if (SysConst.CHANGECHARSET && ActivityID != null &&
            !ActivityID.equals(""))
        {
            ActivityID = StrTool.unicodeToGBK(ActivityID);
        }
        return ActivityID;
    }

    public void setActivityID(String aActivityID)
    {
        ActivityID = aActivityID;
    }

    public String getActivityStatus()
    {
        if (SysConst.CHANGECHARSET && ActivityStatus != null &&
            !ActivityStatus.equals(""))
        {
            ActivityStatus = StrTool.unicodeToGBK(ActivityStatus);
        }
        return ActivityStatus;
    }

    public void setActivityStatus(String aActivityStatus)
    {
        ActivityStatus = aActivityStatus;
    }

    public String getMissionProp1()
    {
        if (SysConst.CHANGECHARSET && MissionProp1 != null &&
            !MissionProp1.equals(""))
        {
            MissionProp1 = StrTool.unicodeToGBK(MissionProp1);
        }
        return MissionProp1;
    }

    public void setMissionProp1(String aMissionProp1)
    {
        MissionProp1 = aMissionProp1;
    }

    public String getMissionProp2()
    {
        if (SysConst.CHANGECHARSET && MissionProp2 != null &&
            !MissionProp2.equals(""))
        {
            MissionProp2 = StrTool.unicodeToGBK(MissionProp2);
        }
        return MissionProp2;
    }

    public void setMissionProp2(String aMissionProp2)
    {
        MissionProp2 = aMissionProp2;
    }

    public String getMissionProp3()
    {
        if (SysConst.CHANGECHARSET && MissionProp3 != null &&
            !MissionProp3.equals(""))
        {
            MissionProp3 = StrTool.unicodeToGBK(MissionProp3);
        }
        return MissionProp3;
    }

    public void setMissionProp3(String aMissionProp3)
    {
        MissionProp3 = aMissionProp3;
    }

    public String getMissionProp4()
    {
        if (SysConst.CHANGECHARSET && MissionProp4 != null &&
            !MissionProp4.equals(""))
        {
            MissionProp4 = StrTool.unicodeToGBK(MissionProp4);
        }
        return MissionProp4;
    }

    public void setMissionProp4(String aMissionProp4)
    {
        MissionProp4 = aMissionProp4;
    }

    public String getMissionProp5()
    {
        if (SysConst.CHANGECHARSET && MissionProp5 != null &&
            !MissionProp5.equals(""))
        {
            MissionProp5 = StrTool.unicodeToGBK(MissionProp5);
        }
        return MissionProp5;
    }

    public void setMissionProp5(String aMissionProp5)
    {
        MissionProp5 = aMissionProp5;
    }

    public String getMissionProp6()
    {
        if (SysConst.CHANGECHARSET && MissionProp6 != null &&
            !MissionProp6.equals(""))
        {
            MissionProp6 = StrTool.unicodeToGBK(MissionProp6);
        }
        return MissionProp6;
    }

    public void setMissionProp6(String aMissionProp6)
    {
        MissionProp6 = aMissionProp6;
    }

    public String getMissionProp7()
    {
        if (SysConst.CHANGECHARSET && MissionProp7 != null &&
            !MissionProp7.equals(""))
        {
            MissionProp7 = StrTool.unicodeToGBK(MissionProp7);
        }
        return MissionProp7;
    }

    public void setMissionProp7(String aMissionProp7)
    {
        MissionProp7 = aMissionProp7;
    }

    public String getMissionProp8()
    {
        if (SysConst.CHANGECHARSET && MissionProp8 != null &&
            !MissionProp8.equals(""))
        {
            MissionProp8 = StrTool.unicodeToGBK(MissionProp8);
        }
        return MissionProp8;
    }

    public void setMissionProp8(String aMissionProp8)
    {
        MissionProp8 = aMissionProp8;
    }

    public String getMissionProp9()
    {
        if (SysConst.CHANGECHARSET && MissionProp9 != null &&
            !MissionProp9.equals(""))
        {
            MissionProp9 = StrTool.unicodeToGBK(MissionProp9);
        }
        return MissionProp9;
    }

    public void setMissionProp9(String aMissionProp9)
    {
        MissionProp9 = aMissionProp9;
    }

    public String getMissionProp10()
    {
        if (SysConst.CHANGECHARSET && MissionProp10 != null &&
            !MissionProp10.equals(""))
        {
            MissionProp10 = StrTool.unicodeToGBK(MissionProp10);
        }
        return MissionProp10;
    }

    public void setMissionProp10(String aMissionProp10)
    {
        MissionProp10 = aMissionProp10;
    }

    public String getMissionProp11()
    {
        if (SysConst.CHANGECHARSET && MissionProp11 != null &&
            !MissionProp11.equals(""))
        {
            MissionProp11 = StrTool.unicodeToGBK(MissionProp11);
        }
        return MissionProp11;
    }

    public void setMissionProp11(String aMissionProp11)
    {
        MissionProp11 = aMissionProp11;
    }

    public String getMissionProp12()
    {
        if (SysConst.CHANGECHARSET && MissionProp12 != null &&
            !MissionProp12.equals(""))
        {
            MissionProp12 = StrTool.unicodeToGBK(MissionProp12);
        }
        return MissionProp12;
    }

    public void setMissionProp12(String aMissionProp12)
    {
        MissionProp12 = aMissionProp12;
    }

    public String getMissionProp13()
    {
        if (SysConst.CHANGECHARSET && MissionProp13 != null &&
            !MissionProp13.equals(""))
        {
            MissionProp13 = StrTool.unicodeToGBK(MissionProp13);
        }
        return MissionProp13;
    }

    public void setMissionProp13(String aMissionProp13)
    {
        MissionProp13 = aMissionProp13;
    }

    public String getMissionProp14()
    {
        if (SysConst.CHANGECHARSET && MissionProp14 != null &&
            !MissionProp14.equals(""))
        {
            MissionProp14 = StrTool.unicodeToGBK(MissionProp14);
        }
        return MissionProp14;
    }

    public void setMissionProp14(String aMissionProp14)
    {
        MissionProp14 = aMissionProp14;
    }

    public String getMissionProp15()
    {
        if (SysConst.CHANGECHARSET && MissionProp15 != null &&
            !MissionProp15.equals(""))
        {
            MissionProp15 = StrTool.unicodeToGBK(MissionProp15);
        }
        return MissionProp15;
    }

    public void setMissionProp15(String aMissionProp15)
    {
        MissionProp15 = aMissionProp15;
    }

    public String getMissionProp16()
    {
        if (SysConst.CHANGECHARSET && MissionProp16 != null &&
            !MissionProp16.equals(""))
        {
            MissionProp16 = StrTool.unicodeToGBK(MissionProp16);
        }
        return MissionProp16;
    }

    public void setMissionProp16(String aMissionProp16)
    {
        MissionProp16 = aMissionProp16;
    }

    public String getMissionProp17()
    {
        if (SysConst.CHANGECHARSET && MissionProp17 != null &&
            !MissionProp17.equals(""))
        {
            MissionProp17 = StrTool.unicodeToGBK(MissionProp17);
        }
        return MissionProp17;
    }

    public void setMissionProp17(String aMissionProp17)
    {
        MissionProp17 = aMissionProp17;
    }

    public String getMissionProp18()
    {
        if (SysConst.CHANGECHARSET && MissionProp18 != null &&
            !MissionProp18.equals(""))
        {
            MissionProp18 = StrTool.unicodeToGBK(MissionProp18);
        }
        return MissionProp18;
    }

    public void setMissionProp18(String aMissionProp18)
    {
        MissionProp18 = aMissionProp18;
    }

    public String getMissionProp19()
    {
        if (SysConst.CHANGECHARSET && MissionProp19 != null &&
            !MissionProp19.equals(""))
        {
            MissionProp19 = StrTool.unicodeToGBK(MissionProp19);
        }
        return MissionProp19;
    }

    public void setMissionProp19(String aMissionProp19)
    {
        MissionProp19 = aMissionProp19;
    }

    public String getMissionProp20()
    {
        if (SysConst.CHANGECHARSET && MissionProp20 != null &&
            !MissionProp20.equals(""))
        {
            MissionProp20 = StrTool.unicodeToGBK(MissionProp20);
        }
        return MissionProp20;
    }

    public void setMissionProp20(String aMissionProp20)
    {
        MissionProp20 = aMissionProp20;
    }

    public String getDefaultOperator()
    {
        if (SysConst.CHANGECHARSET && DefaultOperator != null &&
            !DefaultOperator.equals(""))
        {
            DefaultOperator = StrTool.unicodeToGBK(DefaultOperator);
        }
        return DefaultOperator;
    }

    public void setDefaultOperator(String aDefaultOperator)
    {
        DefaultOperator = aDefaultOperator;
    }

    public String getLastOperator()
    {
        if (SysConst.CHANGECHARSET && LastOperator != null &&
            !LastOperator.equals(""))
        {
            LastOperator = StrTool.unicodeToGBK(LastOperator);
        }
        return LastOperator;
    }

    public void setLastOperator(String aLastOperator)
    {
        LastOperator = aLastOperator;
    }

    public String getCreateOperator()
    {
        if (SysConst.CHANGECHARSET && CreateOperator != null &&
            !CreateOperator.equals(""))
        {
            CreateOperator = StrTool.unicodeToGBK(CreateOperator);
        }
        return CreateOperator;
    }

    public void setCreateOperator(String aCreateOperator)
    {
        CreateOperator = aCreateOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getInDate()
    {
        if (InDate != null)
        {
            return fDate.getString(InDate);
        }
        else
        {
            return null;
        }
    }

    public void setInDate(Date aInDate)
    {
        InDate = aInDate;
    }

    public void setInDate(String aInDate)
    {
        if (aInDate != null && !aInDate.equals(""))
        {
            InDate = fDate.getDate(aInDate);
        }
        else
        {
            InDate = null;
        }
    }

    public String getInTime()
    {
        if (SysConst.CHANGECHARSET && InTime != null && !InTime.equals(""))
        {
            InTime = StrTool.unicodeToGBK(InTime);
        }
        return InTime;
    }

    public void setInTime(String aInTime)
    {
        InTime = aInTime;
    }

    public String getOutDate()
    {
        if (OutDate != null)
        {
            return fDate.getString(OutDate);
        }
        else
        {
            return null;
        }
    }

    public void setOutDate(Date aOutDate)
    {
        OutDate = aOutDate;
    }

    public void setOutDate(String aOutDate)
    {
        if (aOutDate != null && !aOutDate.equals(""))
        {
            OutDate = fDate.getDate(aOutDate);
        }
        else
        {
            OutDate = null;
        }
    }

    public String getOutTime()
    {
        if (SysConst.CHANGECHARSET && OutTime != null && !OutTime.equals(""))
        {
            OutTime = StrTool.unicodeToGBK(OutTime);
        }
        return OutTime;
    }

    public void setOutTime(String aOutTime)
    {
        OutTime = aOutTime;
    }


    /**
     * 使用另外一个 LWMissionSchema 对象给 Schema 赋值
     * @param: aLWMissionSchema LWMissionSchema
     **/
    public void setSchema(LWMissionSchema aLWMissionSchema)
    {
        this.MissionID = aLWMissionSchema.getMissionID();
        this.SubMissionID = aLWMissionSchema.getSubMissionID();
        this.ProcessID = aLWMissionSchema.getProcessID();
        this.ActivityID = aLWMissionSchema.getActivityID();
        this.ActivityStatus = aLWMissionSchema.getActivityStatus();
        this.MissionProp1 = aLWMissionSchema.getMissionProp1();
        this.MissionProp2 = aLWMissionSchema.getMissionProp2();
        this.MissionProp3 = aLWMissionSchema.getMissionProp3();
        this.MissionProp4 = aLWMissionSchema.getMissionProp4();
        this.MissionProp5 = aLWMissionSchema.getMissionProp5();
        this.MissionProp6 = aLWMissionSchema.getMissionProp6();
        this.MissionProp7 = aLWMissionSchema.getMissionProp7();
        this.MissionProp8 = aLWMissionSchema.getMissionProp8();
        this.MissionProp9 = aLWMissionSchema.getMissionProp9();
        this.MissionProp10 = aLWMissionSchema.getMissionProp10();
        this.MissionProp11 = aLWMissionSchema.getMissionProp11();
        this.MissionProp12 = aLWMissionSchema.getMissionProp12();
        this.MissionProp13 = aLWMissionSchema.getMissionProp13();
        this.MissionProp14 = aLWMissionSchema.getMissionProp14();
        this.MissionProp15 = aLWMissionSchema.getMissionProp15();
        this.MissionProp16 = aLWMissionSchema.getMissionProp16();
        this.MissionProp17 = aLWMissionSchema.getMissionProp17();
        this.MissionProp18 = aLWMissionSchema.getMissionProp18();
        this.MissionProp19 = aLWMissionSchema.getMissionProp19();
        this.MissionProp20 = aLWMissionSchema.getMissionProp20();
        this.DefaultOperator = aLWMissionSchema.getDefaultOperator();
        this.LastOperator = aLWMissionSchema.getLastOperator();
        this.CreateOperator = aLWMissionSchema.getCreateOperator();
        this.MakeDate = fDate.getDate(aLWMissionSchema.getMakeDate());
        this.MakeTime = aLWMissionSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLWMissionSchema.getModifyDate());
        this.ModifyTime = aLWMissionSchema.getModifyTime();
        this.InDate = fDate.getDate(aLWMissionSchema.getInDate());
        this.InTime = aLWMissionSchema.getInTime();
        this.OutDate = fDate.getDate(aLWMissionSchema.getOutDate());
        this.OutTime = aLWMissionSchema.getOutTime();
    }


    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("MissionID") == null)
            {
                this.MissionID = null;
            }
            else
            {
                this.MissionID = rs.getString("MissionID").trim();
            }

            if (rs.getString("SubMissionID") == null)
            {
                this.SubMissionID = null;
            }
            else
            {
                this.SubMissionID = rs.getString("SubMissionID").trim();
            }

            if (rs.getString("ProcessID") == null)
            {
                this.ProcessID = null;
            }
            else
            {
                this.ProcessID = rs.getString("ProcessID").trim();
            }

            if (rs.getString("ActivityID") == null)
            {
                this.ActivityID = null;
            }
            else
            {
                this.ActivityID = rs.getString("ActivityID").trim();
            }

            if (rs.getString("ActivityStatus") == null)
            {
                this.ActivityStatus = null;
            }
            else
            {
                this.ActivityStatus = rs.getString("ActivityStatus").trim();
            }

            if (rs.getString("MissionProp1") == null)
            {
                this.MissionProp1 = null;
            }
            else
            {
                this.MissionProp1 = rs.getString("MissionProp1").trim();
            }

            if (rs.getString("MissionProp2") == null)
            {
                this.MissionProp2 = null;
            }
            else
            {
                this.MissionProp2 = rs.getString("MissionProp2").trim();
            }

            if (rs.getString("MissionProp3") == null)
            {
                this.MissionProp3 = null;
            }
            else
            {
                this.MissionProp3 = rs.getString("MissionProp3").trim();
            }

            if (rs.getString("MissionProp4") == null)
            {
                this.MissionProp4 = null;
            }
            else
            {
                this.MissionProp4 = rs.getString("MissionProp4").trim();
            }

            if (rs.getString("MissionProp5") == null)
            {
                this.MissionProp5 = null;
            }
            else
            {
                this.MissionProp5 = rs.getString("MissionProp5").trim();
            }

            if (rs.getString("MissionProp6") == null)
            {
                this.MissionProp6 = null;
            }
            else
            {
                this.MissionProp6 = rs.getString("MissionProp6").trim();
            }

            if (rs.getString("MissionProp7") == null)
            {
                this.MissionProp7 = null;
            }
            else
            {
                this.MissionProp7 = rs.getString("MissionProp7").trim();
            }

            if (rs.getString("MissionProp8") == null)
            {
                this.MissionProp8 = null;
            }
            else
            {
                this.MissionProp8 = rs.getString("MissionProp8").trim();
            }

            if (rs.getString("MissionProp9") == null)
            {
                this.MissionProp9 = null;
            }
            else
            {
                this.MissionProp9 = rs.getString("MissionProp9").trim();
            }

            if (rs.getString("MissionProp10") == null)
            {
                this.MissionProp10 = null;
            }
            else
            {
                this.MissionProp10 = rs.getString("MissionProp10").trim();
            }

            if (rs.getString("MissionProp11") == null)
            {
                this.MissionProp11 = null;
            }
            else
            {
                this.MissionProp11 = rs.getString("MissionProp11").trim();
            }

            if (rs.getString("MissionProp12") == null)
            {
                this.MissionProp12 = null;
            }
            else
            {
                this.MissionProp12 = rs.getString("MissionProp12").trim();
            }

            if (rs.getString("MissionProp13") == null)
            {
                this.MissionProp13 = null;
            }
            else
            {
                this.MissionProp13 = rs.getString("MissionProp13").trim();
            }

            if (rs.getString("MissionProp14") == null)
            {
                this.MissionProp14 = null;
            }
            else
            {
                this.MissionProp14 = rs.getString("MissionProp14").trim();
            }

            if (rs.getString("MissionProp15") == null)
            {
                this.MissionProp15 = null;
            }
            else
            {
                this.MissionProp15 = rs.getString("MissionProp15").trim();
            }

            if (rs.getString("MissionProp16") == null)
            {
                this.MissionProp16 = null;
            }
            else
            {
                this.MissionProp16 = rs.getString("MissionProp16").trim();
            }

            if (rs.getString("MissionProp17") == null)
            {
                this.MissionProp17 = null;
            }
            else
            {
                this.MissionProp17 = rs.getString("MissionProp17").trim();
            }

            if (rs.getString("MissionProp18") == null)
            {
                this.MissionProp18 = null;
            }
            else
            {
                this.MissionProp18 = rs.getString("MissionProp18").trim();
            }

            if (rs.getString("MissionProp19") == null)
            {
                this.MissionProp19 = null;
            }
            else
            {
                this.MissionProp19 = rs.getString("MissionProp19").trim();
            }

            if (rs.getString("MissionProp20") == null)
            {
                this.MissionProp20 = null;
            }
            else
            {
                this.MissionProp20 = rs.getString("MissionProp20").trim();
            }

            if (rs.getString("DefaultOperator") == null)
            {
                this.DefaultOperator = null;
            }
            else
            {
                this.DefaultOperator = rs.getString("DefaultOperator").trim();
            }

            if (rs.getString("LastOperator") == null)
            {
                this.LastOperator = null;
            }
            else
            {
                this.LastOperator = rs.getString("LastOperator").trim();
            }

            if (rs.getString("CreateOperator") == null)
            {
                this.CreateOperator = null;
            }
            else
            {
                this.CreateOperator = rs.getString("CreateOperator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.InDate = rs.getDate("InDate");
            if (rs.getString("InTime") == null)
            {
                this.InTime = null;
            }
            else
            {
                this.InTime = rs.getString("InTime").trim();
            }

            this.OutDate = rs.getDate("OutDate");
            if (rs.getString("OutTime") == null)
            {
                this.OutTime = null;
            }
            else
            {
                this.OutTime = rs.getString("OutTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWMissionSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LWMissionSchema getSchema()
    {
        LWMissionSchema aLWMissionSchema = new LWMissionSchema();
        aLWMissionSchema.setSchema(this);
        return aLWMissionSchema;
    }

    public LWMissionDB getDB()
    {
        LWMissionDB aDBOper = new LWMissionDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWMission描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionID)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(SubMissionID)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ProcessID)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ActivityID)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ActivityStatus)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp1)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp2)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp3)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp4)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp5)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp6)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp7)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp8)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp9)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp10)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp11)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp12)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp13)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp14)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp15)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp16)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp17)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp18)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp19)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MissionProp20)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DefaultOperator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(LastOperator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CreateOperator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                InDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(InTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                OutDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(OutTime)));
        return strReturn.toString();
    }


    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLWMission>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            MissionID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            SubMissionID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ProcessID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            ActivityID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            ActivityStatus = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                            SysConst.PACKAGESPILTER);
            MissionProp1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            MissionProp2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            MissionProp3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            MissionProp4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            MissionProp5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
            MissionProp6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                          SysConst.PACKAGESPILTER);
            MissionProp7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                          SysConst.PACKAGESPILTER);
            MissionProp8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                          SysConst.PACKAGESPILTER);
            MissionProp9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                          SysConst.PACKAGESPILTER);
            MissionProp10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                           SysConst.PACKAGESPILTER);
            MissionProp11 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                           SysConst.PACKAGESPILTER);
            MissionProp12 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                           SysConst.PACKAGESPILTER);
            MissionProp13 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,
                                           SysConst.PACKAGESPILTER);
            MissionProp14 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                           SysConst.PACKAGESPILTER);
            MissionProp15 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                           SysConst.PACKAGESPILTER);
            MissionProp16 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                           SysConst.PACKAGESPILTER);
            MissionProp17 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                           SysConst.PACKAGESPILTER);
            MissionProp18 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                           SysConst.PACKAGESPILTER);
            MissionProp19 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                           SysConst.PACKAGESPILTER);
            MissionProp20 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                           SysConst.PACKAGESPILTER);
            DefaultOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             26, SysConst.PACKAGESPILTER);
            LastOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                          SysConst.PACKAGESPILTER);
            CreateOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            28, SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 29, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 31, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                        SysConst.PACKAGESPILTER);
            InDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 33, SysConst.PACKAGESPILTER));
            InTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34,
                                    SysConst.PACKAGESPILTER);
            OutDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 35, SysConst.PACKAGESPILTER));
            OutTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36,
                                     SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LWMissionSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }


    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("MissionID"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionID));
        }
        if (FCode.equals("SubMissionID"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SubMissionID));
        }
        if (FCode.equals("ProcessID"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProcessID));
        }
        if (FCode.equals("ActivityID"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActivityID));
        }
        if (FCode.equals("ActivityStatus"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActivityStatus));
        }
        if (FCode.equals("MissionProp1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp1));
        }
        if (FCode.equals("MissionProp2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp2));
        }
        if (FCode.equals("MissionProp3"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp3));
        }
        if (FCode.equals("MissionProp4"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp4));
        }
        if (FCode.equals("MissionProp5"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp5));
        }
        if (FCode.equals("MissionProp6"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp6));
        }
        if (FCode.equals("MissionProp7"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp7));
        }
        if (FCode.equals("MissionProp8"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp8));
        }
        if (FCode.equals("MissionProp9"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp9));
        }
        if (FCode.equals("MissionProp10"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp10));
        }
        if (FCode.equals("MissionProp11"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp11));
        }
        if (FCode.equals("MissionProp12"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp12));
        }
        if (FCode.equals("MissionProp13"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp13));
        }
        if (FCode.equals("MissionProp14"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp14));
        }
        if (FCode.equals("MissionProp15"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp15));
        }
        if (FCode.equals("MissionProp16"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp16));
        }
        if (FCode.equals("MissionProp17"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp17));
        }
        if (FCode.equals("MissionProp18"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp18));
        }
        if (FCode.equals("MissionProp19"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp19));
        }
        if (FCode.equals("MissionProp20"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MissionProp20));
        }
        if (FCode.equals("DefaultOperator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DefaultOperator));
        }
        if (FCode.equals("LastOperator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LastOperator));
        }
        if (FCode.equals("CreateOperator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CreateOperator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("InDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getInDate()));
        }
        if (FCode.equals("InTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InTime));
        }
        if (FCode.equals("OutDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getOutDate()));
        }
        if (FCode.equals("OutTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OutTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(MissionID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(SubMissionID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ProcessID);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ActivityID);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(ActivityStatus);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(MissionProp1);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MissionProp2);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(MissionProp3);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MissionProp4);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(MissionProp5);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(MissionProp6);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MissionProp7);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MissionProp8);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(MissionProp9);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(MissionProp10);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(MissionProp11);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(MissionProp12);
                break;
            case 17:
                strFieldValue = StrTool.GBKToUnicode(MissionProp13);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(MissionProp14);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(MissionProp15);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(MissionProp16);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(MissionProp17);
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(MissionProp18);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(MissionProp19);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(MissionProp20);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(DefaultOperator);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(LastOperator);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(CreateOperator);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getInDate()));
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(InTime);
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getOutDate()));
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(OutTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }


    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("MissionID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionID = FValue.trim();
            }
            else
            {
                MissionID = null;
            }
        }
        if (FCode.equals("SubMissionID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SubMissionID = FValue.trim();
            }
            else
            {
                SubMissionID = null;
            }
        }
        if (FCode.equals("ProcessID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProcessID = FValue.trim();
            }
            else
            {
                ProcessID = null;
            }
        }
        if (FCode.equals("ActivityID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ActivityID = FValue.trim();
            }
            else
            {
                ActivityID = null;
            }
        }
        if (FCode.equals("ActivityStatus"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ActivityStatus = FValue.trim();
            }
            else
            {
                ActivityStatus = null;
            }
        }
        if (FCode.equals("MissionProp1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp1 = FValue.trim();
            }
            else
            {
                MissionProp1 = null;
            }
        }
        if (FCode.equals("MissionProp2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp2 = FValue.trim();
            }
            else
            {
                MissionProp2 = null;
            }
        }
        if (FCode.equals("MissionProp3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp3 = FValue.trim();
            }
            else
            {
                MissionProp3 = null;
            }
        }
        if (FCode.equals("MissionProp4"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp4 = FValue.trim();
            }
            else
            {
                MissionProp4 = null;
            }
        }
        if (FCode.equals("MissionProp5"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp5 = FValue.trim();
            }
            else
            {
                MissionProp5 = null;
            }
        }
        if (FCode.equals("MissionProp6"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp6 = FValue.trim();
            }
            else
            {
                MissionProp6 = null;
            }
        }
        if (FCode.equals("MissionProp7"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp7 = FValue.trim();
            }
            else
            {
                MissionProp7 = null;
            }
        }
        if (FCode.equals("MissionProp8"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp8 = FValue.trim();
            }
            else
            {
                MissionProp8 = null;
            }
        }
        if (FCode.equals("MissionProp9"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp9 = FValue.trim();
            }
            else
            {
                MissionProp9 = null;
            }
        }
        if (FCode.equals("MissionProp10"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp10 = FValue.trim();
            }
            else
            {
                MissionProp10 = null;
            }
        }
        if (FCode.equals("MissionProp11"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp11 = FValue.trim();
            }
            else
            {
                MissionProp11 = null;
            }
        }
        if (FCode.equals("MissionProp12"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp12 = FValue.trim();
            }
            else
            {
                MissionProp12 = null;
            }
        }
        if (FCode.equals("MissionProp13"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp13 = FValue.trim();
            }
            else
            {
                MissionProp13 = null;
            }
        }
        if (FCode.equals("MissionProp14"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp14 = FValue.trim();
            }
            else
            {
                MissionProp14 = null;
            }
        }
        if (FCode.equals("MissionProp15"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp15 = FValue.trim();
            }
            else
            {
                MissionProp15 = null;
            }
        }
        if (FCode.equals("MissionProp16"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp16 = FValue.trim();
            }
            else
            {
                MissionProp16 = null;
            }
        }
        if (FCode.equals("MissionProp17"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp17 = FValue.trim();
            }
            else
            {
                MissionProp17 = null;
            }
        }
        if (FCode.equals("MissionProp18"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp18 = FValue.trim();
            }
            else
            {
                MissionProp18 = null;
            }
        }
        if (FCode.equals("MissionProp19"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp19 = FValue.trim();
            }
            else
            {
                MissionProp19 = null;
            }
        }
        if (FCode.equals("MissionProp20"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MissionProp20 = FValue.trim();
            }
            else
            {
                MissionProp20 = null;
            }
        }
        if (FCode.equals("DefaultOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DefaultOperator = FValue.trim();
            }
            else
            {
                DefaultOperator = null;
            }
        }
        if (FCode.equals("LastOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LastOperator = FValue.trim();
            }
            else
            {
                LastOperator = null;
            }
        }
        if (FCode.equals("CreateOperator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CreateOperator = FValue.trim();
            }
            else
            {
                CreateOperator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("InDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InDate = fDate.getDate(FValue);
            }
            else
            {
                InDate = null;
            }
        }
        if (FCode.equals("InTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InTime = FValue.trim();
            }
            else
            {
                InTime = null;
            }
        }
        if (FCode.equals("OutDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OutDate = fDate.getDate(FValue);
            }
            else
            {
                OutDate = null;
            }
        }
        if (FCode.equals("OutTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OutTime = FValue.trim();
            }
            else
            {
                OutTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LWMissionSchema other = (LWMissionSchema) otherObject;
        return
                MissionID.equals(other.getMissionID())
                && SubMissionID.equals(other.getSubMissionID())
                && ProcessID.equals(other.getProcessID())
                && ActivityID.equals(other.getActivityID())
                && ActivityStatus.equals(other.getActivityStatus())
                && MissionProp1.equals(other.getMissionProp1())
                && MissionProp2.equals(other.getMissionProp2())
                && MissionProp3.equals(other.getMissionProp3())
                && MissionProp4.equals(other.getMissionProp4())
                && MissionProp5.equals(other.getMissionProp5())
                && MissionProp6.equals(other.getMissionProp6())
                && MissionProp7.equals(other.getMissionProp7())
                && MissionProp8.equals(other.getMissionProp8())
                && MissionProp9.equals(other.getMissionProp9())
                && MissionProp10.equals(other.getMissionProp10())
                && MissionProp11.equals(other.getMissionProp11())
                && MissionProp12.equals(other.getMissionProp12())
                && MissionProp13.equals(other.getMissionProp13())
                && MissionProp14.equals(other.getMissionProp14())
                && MissionProp15.equals(other.getMissionProp15())
                && MissionProp16.equals(other.getMissionProp16())
                && MissionProp17.equals(other.getMissionProp17())
                && MissionProp18.equals(other.getMissionProp18())
                && MissionProp19.equals(other.getMissionProp19())
                && MissionProp20.equals(other.getMissionProp20())
                && DefaultOperator.equals(other.getDefaultOperator())
                && LastOperator.equals(other.getLastOperator())
                && CreateOperator.equals(other.getCreateOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && fDate.getString(InDate).equals(other.getInDate())
                && InTime.equals(other.getInTime())
                && fDate.getString(OutDate).equals(other.getOutDate())
                && OutTime.equals(other.getOutTime());
    }


    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }


    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("MissionID"))
        {
            return 0;
        }
        if (strFieldName.equals("SubMissionID"))
        {
            return 1;
        }
        if (strFieldName.equals("ProcessID"))
        {
            return 2;
        }
        if (strFieldName.equals("ActivityID"))
        {
            return 3;
        }
        if (strFieldName.equals("ActivityStatus"))
        {
            return 4;
        }
        if (strFieldName.equals("MissionProp1"))
        {
            return 5;
        }
        if (strFieldName.equals("MissionProp2"))
        {
            return 6;
        }
        if (strFieldName.equals("MissionProp3"))
        {
            return 7;
        }
        if (strFieldName.equals("MissionProp4"))
        {
            return 8;
        }
        if (strFieldName.equals("MissionProp5"))
        {
            return 9;
        }
        if (strFieldName.equals("MissionProp6"))
        {
            return 10;
        }
        if (strFieldName.equals("MissionProp7"))
        {
            return 11;
        }
        if (strFieldName.equals("MissionProp8"))
        {
            return 12;
        }
        if (strFieldName.equals("MissionProp9"))
        {
            return 13;
        }
        if (strFieldName.equals("MissionProp10"))
        {
            return 14;
        }
        if (strFieldName.equals("MissionProp11"))
        {
            return 15;
        }
        if (strFieldName.equals("MissionProp12"))
        {
            return 16;
        }
        if (strFieldName.equals("MissionProp13"))
        {
            return 17;
        }
        if (strFieldName.equals("MissionProp14"))
        {
            return 18;
        }
        if (strFieldName.equals("MissionProp15"))
        {
            return 19;
        }
        if (strFieldName.equals("MissionProp16"))
        {
            return 20;
        }
        if (strFieldName.equals("MissionProp17"))
        {
            return 21;
        }
        if (strFieldName.equals("MissionProp18"))
        {
            return 22;
        }
        if (strFieldName.equals("MissionProp19"))
        {
            return 23;
        }
        if (strFieldName.equals("MissionProp20"))
        {
            return 24;
        }
        if (strFieldName.equals("DefaultOperator"))
        {
            return 25;
        }
        if (strFieldName.equals("LastOperator"))
        {
            return 26;
        }
        if (strFieldName.equals("CreateOperator"))
        {
            return 27;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 28;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 29;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 30;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 31;
        }
        if (strFieldName.equals("InDate"))
        {
            return 32;
        }
        if (strFieldName.equals("InTime"))
        {
            return 33;
        }
        if (strFieldName.equals("OutDate"))
        {
            return 34;
        }
        if (strFieldName.equals("OutTime"))
        {
            return 35;
        }
        return -1;
    }


    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "MissionID";
                break;
            case 1:
                strFieldName = "SubMissionID";
                break;
            case 2:
                strFieldName = "ProcessID";
                break;
            case 3:
                strFieldName = "ActivityID";
                break;
            case 4:
                strFieldName = "ActivityStatus";
                break;
            case 5:
                strFieldName = "MissionProp1";
                break;
            case 6:
                strFieldName = "MissionProp2";
                break;
            case 7:
                strFieldName = "MissionProp3";
                break;
            case 8:
                strFieldName = "MissionProp4";
                break;
            case 9:
                strFieldName = "MissionProp5";
                break;
            case 10:
                strFieldName = "MissionProp6";
                break;
            case 11:
                strFieldName = "MissionProp7";
                break;
            case 12:
                strFieldName = "MissionProp8";
                break;
            case 13:
                strFieldName = "MissionProp9";
                break;
            case 14:
                strFieldName = "MissionProp10";
                break;
            case 15:
                strFieldName = "MissionProp11";
                break;
            case 16:
                strFieldName = "MissionProp12";
                break;
            case 17:
                strFieldName = "MissionProp13";
                break;
            case 18:
                strFieldName = "MissionProp14";
                break;
            case 19:
                strFieldName = "MissionProp15";
                break;
            case 20:
                strFieldName = "MissionProp16";
                break;
            case 21:
                strFieldName = "MissionProp17";
                break;
            case 22:
                strFieldName = "MissionProp18";
                break;
            case 23:
                strFieldName = "MissionProp19";
                break;
            case 24:
                strFieldName = "MissionProp20";
                break;
            case 25:
                strFieldName = "DefaultOperator";
                break;
            case 26:
                strFieldName = "LastOperator";
                break;
            case 27:
                strFieldName = "CreateOperator";
                break;
            case 28:
                strFieldName = "MakeDate";
                break;
            case 29:
                strFieldName = "MakeTime";
                break;
            case 30:
                strFieldName = "ModifyDate";
                break;
            case 31:
                strFieldName = "ModifyTime";
                break;
            case 32:
                strFieldName = "InDate";
                break;
            case 33:
                strFieldName = "InTime";
                break;
            case 34:
                strFieldName = "OutDate";
                break;
            case 35:
                strFieldName = "OutTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }


    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("MissionID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SubMissionID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProcessID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ActivityID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ActivityStatus"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp3"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp4"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp5"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp6"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp7"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp8"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp9"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp10"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp11"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp12"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp13"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp14"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp15"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp16"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp17"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp18"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp19"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MissionProp20"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DefaultOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LastOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CreateOperator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("InTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OutDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("OutTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }


    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 29:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 30:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 33:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 34:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 35:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
