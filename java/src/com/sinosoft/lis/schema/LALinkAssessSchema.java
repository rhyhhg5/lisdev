/*
 * <p>ClassName: LALinkAssessSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LALinkAssessDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

public class LALinkAssessSchema implements Schema
{
    // @Field
    /** 地区编码 */
    private String ManageCom;
    /** 代理人职级 */
    private String AgentGrade;
    /** 筹备期间 */
    private int ArrangePeriod;
    /** 考核比率 */
    private double AssessRate;
    /** 考核比率1 */
    private double AssessRate1;
    /** 衔接止期 */
    private Date LinkEndDate;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 版本号 */
    private String AreaType;

    public static final int FIELDNUM = 12; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LALinkAssessSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ManageCom";
        pk[1] = "AgentGrade";
        pk[2] = "AreaType";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getAgentGrade()
    {
        if (AgentGrade != null && !AgentGrade.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public int getArrangePeriod()
    {
        return ArrangePeriod;
    }

    public void setArrangePeriod(int aArrangePeriod)
    {
        ArrangePeriod = aArrangePeriod;
    }

    public void setArrangePeriod(String aArrangePeriod)
    {
        if (aArrangePeriod != null && !aArrangePeriod.equals(""))
        {
            Integer tInteger = new Integer(aArrangePeriod);
            int i = tInteger.intValue();
            ArrangePeriod = i;
        }
    }

    public double getAssessRate()
    {
        return AssessRate;
    }

    public void setAssessRate(double aAssessRate)
    {
        AssessRate = aAssessRate;
    }

    public void setAssessRate(String aAssessRate)
    {
        if (aAssessRate != null && !aAssessRate.equals(""))
        {
            Double tDouble = new Double(aAssessRate);
            double d = tDouble.doubleValue();
            AssessRate = d;
        }
    }

    public double getAssessRate1()
    {
        return AssessRate1;
    }

    public void setAssessRate1(double aAssessRate1)
    {
        AssessRate1 = aAssessRate1;
    }

    public void setAssessRate1(String aAssessRate1)
    {
        if (aAssessRate1 != null && !aAssessRate1.equals(""))
        {
            Double tDouble = new Double(aAssessRate1);
            double d = tDouble.doubleValue();
            AssessRate1 = d;
        }
    }

    public String getLinkEndDate()
    {
        if (LinkEndDate != null)
        {
            return fDate.getString(LinkEndDate);
        }
        else
        {
            return null;
        }
    }

    public void setLinkEndDate(Date aLinkEndDate)
    {
        LinkEndDate = aLinkEndDate;
    }

    public void setLinkEndDate(String aLinkEndDate)
    {
        if (aLinkEndDate != null && !aLinkEndDate.equals(""))
        {
            LinkEndDate = fDate.getDate(aLinkEndDate);
        }
        else
        {
            LinkEndDate = null;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getAreaType()
    {
        if (AreaType != null && !AreaType.equals("") && SysConst.CHANGECHARSET)
        {
            AreaType = StrTool.unicodeToGBK(AreaType);
        }
        return AreaType;
    }

    public void setAreaType(String aAreaType)
    {
        AreaType = aAreaType;
    }

    /**
     * 使用另外一个 LALinkAssessSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LALinkAssessSchema aLALinkAssessSchema)
    {
        this.ManageCom = aLALinkAssessSchema.getManageCom();
        this.AgentGrade = aLALinkAssessSchema.getAgentGrade();
        this.ArrangePeriod = aLALinkAssessSchema.getArrangePeriod();
        this.AssessRate = aLALinkAssessSchema.getAssessRate();
        this.AssessRate1 = aLALinkAssessSchema.getAssessRate1();
        this.LinkEndDate = fDate.getDate(aLALinkAssessSchema.getLinkEndDate());
        this.Operator = aLALinkAssessSchema.getOperator();
        this.MakeDate = fDate.getDate(aLALinkAssessSchema.getMakeDate());
        this.MakeTime = aLALinkAssessSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLALinkAssessSchema.getModifyDate());
        this.ModifyTime = aLALinkAssessSchema.getModifyTime();
        this.AreaType = aLALinkAssessSchema.getAreaType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            this.ArrangePeriod = rs.getInt("ArrangePeriod");
            this.AssessRate = rs.getDouble("AssessRate");
            this.AssessRate1 = rs.getDouble("AssessRate1");
            this.LinkEndDate = rs.getDate("LinkEndDate");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("AreaType") == null)
            {
                this.AreaType = null;
            }
            else
            {
                this.AreaType = rs.getString("AreaType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LALinkAssessSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LALinkAssessSchema getSchema()
    {
        LALinkAssessSchema aLALinkAssessSchema = new LALinkAssessSchema();
        aLALinkAssessSchema.setSchema(this);
        return aLALinkAssessSchema;
    }

    public LALinkAssessDB getDB()
    {
        LALinkAssessDB aDBOper = new LALinkAssessDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLALinkAssess描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ArrangePeriod) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(AssessRate) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(AssessRate1) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            LinkEndDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AreaType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLALinkAssess>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ArrangePeriod = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).intValue();
            AssessRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            AssessRate1 = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).doubleValue();
            LinkEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
            AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LALinkAssessSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("ArrangePeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ArrangePeriod));
        }
        if (FCode.equals("AssessRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessRate));
        }
        if (FCode.equals("AssessRate1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AssessRate1));
        }
        if (FCode.equals("LinkEndDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getLinkEndDate()));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("AreaType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 2:
                strFieldValue = String.valueOf(ArrangePeriod);
                break;
            case 3:
                strFieldValue = String.valueOf(AssessRate);
                break;
            case 4:
                strFieldValue = String.valueOf(AssessRate1);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getLinkEndDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(AreaType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("ArrangePeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ArrangePeriod = i;
            }
        }
        if (FCode.equals("AssessRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AssessRate = d;
            }
        }
        if (FCode.equals("AssessRate1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AssessRate1 = d;
            }
        }
        if (FCode.equals("LinkEndDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                LinkEndDate = fDate.getDate(FValue);
            }
            else
            {
                LinkEndDate = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("AreaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AreaType = FValue.trim();
            }
            else
            {
                AreaType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LALinkAssessSchema other = (LALinkAssessSchema) otherObject;
        return
                ManageCom.equals(other.getManageCom())
                && AgentGrade.equals(other.getAgentGrade())
                && ArrangePeriod == other.getArrangePeriod()
                && AssessRate == other.getAssessRate()
                && AssessRate1 == other.getAssessRate1()
                && fDate.getString(LinkEndDate).equals(other.getLinkEndDate())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && AreaType.equals(other.getAreaType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return 0;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 1;
        }
        if (strFieldName.equals("ArrangePeriod"))
        {
            return 2;
        }
        if (strFieldName.equals("AssessRate"))
        {
            return 3;
        }
        if (strFieldName.equals("AssessRate1"))
        {
            return 4;
        }
        if (strFieldName.equals("LinkEndDate"))
        {
            return 5;
        }
        if (strFieldName.equals("Operator"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 10;
        }
        if (strFieldName.equals("AreaType"))
        {
            return 11;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ManageCom";
                break;
            case 1:
                strFieldName = "AgentGrade";
                break;
            case 2:
                strFieldName = "ArrangePeriod";
                break;
            case 3:
                strFieldName = "AssessRate";
                break;
            case 4:
                strFieldName = "AssessRate1";
                break;
            case 5:
                strFieldName = "LinkEndDate";
                break;
            case 6:
                strFieldName = "Operator";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            case 9:
                strFieldName = "ModifyDate";
                break;
            case 10:
                strFieldName = "ModifyTime";
                break;
            case 11:
                strFieldName = "AreaType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ArrangePeriod"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AssessRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AssessRate1"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("LinkEndDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_INT;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
