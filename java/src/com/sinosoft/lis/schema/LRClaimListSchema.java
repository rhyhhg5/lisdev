/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRClaimListDB;

/*
 * <p>ClassName: LRClaimListSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 再保新系统模型
 * @CreateDate：2017-02-21
 */
public class LRClaimListSchema implements Schema, Cloneable
{
	// @Field
	/** 再保合同号 */
	private String ReContCode;
	/** 产品代码 */
	private String RiskCode;
	/** 保单号 */
	private String ContNo;
	/** 团单号 */
	private String GrpContNo;
	/** 生效日期 */
	private Date CValiDate;
	/** 失效日期 */
	private Date EndDate;
	/** 签单日期 */
	private Date SignDate;
	/** 投保人姓名 */
	private String AppntName;
	/** 投保人客户号 */
	private String AppntNo;
	/** 被保险人姓名 */
	private String InsuredName;
	/** 出生日期 */
	private Date InsuredBirthday;
	/** 性别 */
	private String InsuredSex;
	/** 身份证号码 */
	private String InsuredIdNo;
	/** 职业类别 */
	private String OccupationType;
	/** 保额 */
	private double Amnt;
	/** 索赔额 */
	private double ClaimMoney;
	/** 赔付金额 */
	private double RealPay;
	/** 诊断 */
	private String Diagnoses;
	/** 出险原因 */
	private String AccDesc;
	/** 出险日期 */
	private Date AccidentDate;
	/** 出院日期 */
	private Date LeaveHospDate;
	/** 索赔日期 */
	private Date RgtDate;
	/** 结案日期 */
	private Date getdatadate;
	/** 理赔结论 */
	private String GiveTypeDesc;
	/** 分保类型 */
	private String TempCessFlag;
	/** 分保比例 */
	private double CessionRate;
	/** 分保保额 */
	private double CessionAmount;
	/** 分保摊回赔款 */
	private double ClaimBackFee;
	/** 机构代码 */
	private String ManageCom;
	/** 清单时间 */
	private Date ListDate;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private String ModifyTime;
	/** 操作用户 */
	private String Oprater;
	/** 险种号码 */
	private String PolNo;
	/** 赔案号 */
	private String ClmNo;
	/** 再保项目 */
	private String ReinsureItem;
	/** 成本中心编码 */
	private String CostCenter;
	/** 市场类型 */
	private String MarketType;

	public static final int FIELDNUM = 40;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRClaimListSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[7];
		pk[0] = "ReContCode";
		pk[1] = "ContNo";
		pk[2] = "GrpContNo";
		pk[3] = "ListDate";
		pk[4] = "PolNo";
		pk[5] = "ClmNo";
		pk[6] = "ReinsureItem";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRClaimListSchema cloned = (LRClaimListSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getReContCode()
	{
		return ReContCode;
	}
	public void setReContCode(String aReContCode)
	{
		ReContCode = aReContCode;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getContNo()
	{
		return ContNo;
	}
	public void setContNo(String aContNo)
	{
		ContNo = aContNo;
	}
	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getCValiDate()
	{
		if( CValiDate != null )
			return fDate.getString(CValiDate);
		else
			return null;
	}
	public void setCValiDate(Date aCValiDate)
	{
		CValiDate = aCValiDate;
	}
	public void setCValiDate(String aCValiDate)
	{
		if (aCValiDate != null && !aCValiDate.equals("") )
		{
			CValiDate = fDate.getDate( aCValiDate );
		}
		else
			CValiDate = null;
	}

	public String getEndDate()
	{
		if( EndDate != null )
			return fDate.getString(EndDate);
		else
			return null;
	}
	public void setEndDate(Date aEndDate)
	{
		EndDate = aEndDate;
	}
	public void setEndDate(String aEndDate)
	{
		if (aEndDate != null && !aEndDate.equals("") )
		{
			EndDate = fDate.getDate( aEndDate );
		}
		else
			EndDate = null;
	}

	public String getSignDate()
	{
		if( SignDate != null )
			return fDate.getString(SignDate);
		else
			return null;
	}
	public void setSignDate(Date aSignDate)
	{
		SignDate = aSignDate;
	}
	public void setSignDate(String aSignDate)
	{
		if (aSignDate != null && !aSignDate.equals("") )
		{
			SignDate = fDate.getDate( aSignDate );
		}
		else
			SignDate = null;
	}

	public String getAppntName()
	{
		return AppntName;
	}
	public void setAppntName(String aAppntName)
	{
		AppntName = aAppntName;
	}
	public String getAppntNo()
	{
		return AppntNo;
	}
	public void setAppntNo(String aAppntNo)
	{
		AppntNo = aAppntNo;
	}
	public String getInsuredName()
	{
		return InsuredName;
	}
	public void setInsuredName(String aInsuredName)
	{
		InsuredName = aInsuredName;
	}
	public String getInsuredBirthday()
	{
		if( InsuredBirthday != null )
			return fDate.getString(InsuredBirthday);
		else
			return null;
	}
	public void setInsuredBirthday(Date aInsuredBirthday)
	{
		InsuredBirthday = aInsuredBirthday;
	}
	public void setInsuredBirthday(String aInsuredBirthday)
	{
		if (aInsuredBirthday != null && !aInsuredBirthday.equals("") )
		{
			InsuredBirthday = fDate.getDate( aInsuredBirthday );
		}
		else
			InsuredBirthday = null;
	}

	public String getInsuredSex()
	{
		return InsuredSex;
	}
	public void setInsuredSex(String aInsuredSex)
	{
		InsuredSex = aInsuredSex;
	}
	public String getInsuredIdNo()
	{
		return InsuredIdNo;
	}
	public void setInsuredIdNo(String aInsuredIdNo)
	{
		InsuredIdNo = aInsuredIdNo;
	}
	public String getOccupationType()
	{
		return OccupationType;
	}
	public void setOccupationType(String aOccupationType)
	{
		OccupationType = aOccupationType;
	}
	public double getAmnt()
	{
		return Amnt;
	}
	public void setAmnt(double aAmnt)
	{
		Amnt = Arith.round(aAmnt,2);
	}
	public void setAmnt(String aAmnt)
	{
		if (aAmnt != null && !aAmnt.equals(""))
		{
			Double tDouble = new Double(aAmnt);
			double d = tDouble.doubleValue();
                Amnt = Arith.round(d,2);
		}
	}

	public double getClaimMoney()
	{
		return ClaimMoney;
	}
	public void setClaimMoney(double aClaimMoney)
	{
		ClaimMoney = Arith.round(aClaimMoney,2);
	}
	public void setClaimMoney(String aClaimMoney)
	{
		if (aClaimMoney != null && !aClaimMoney.equals(""))
		{
			Double tDouble = new Double(aClaimMoney);
			double d = tDouble.doubleValue();
                ClaimMoney = Arith.round(d,2);
		}
	}

	public double getRealPay()
	{
		return RealPay;
	}
	public void setRealPay(double aRealPay)
	{
		RealPay = Arith.round(aRealPay,2);
	}
	public void setRealPay(String aRealPay)
	{
		if (aRealPay != null && !aRealPay.equals(""))
		{
			Double tDouble = new Double(aRealPay);
			double d = tDouble.doubleValue();
                RealPay = Arith.round(d,2);
		}
	}

	public String getDiagnoses()
	{
		return Diagnoses;
	}
	public void setDiagnoses(String aDiagnoses)
	{
		Diagnoses = aDiagnoses;
	}
	public String getAccDesc()
	{
		return AccDesc;
	}
	public void setAccDesc(String aAccDesc)
	{
		AccDesc = aAccDesc;
	}
	public String getAccidentDate()
	{
		if( AccidentDate != null )
			return fDate.getString(AccidentDate);
		else
			return null;
	}
	public void setAccidentDate(Date aAccidentDate)
	{
		AccidentDate = aAccidentDate;
	}
	public void setAccidentDate(String aAccidentDate)
	{
		if (aAccidentDate != null && !aAccidentDate.equals("") )
		{
			AccidentDate = fDate.getDate( aAccidentDate );
		}
		else
			AccidentDate = null;
	}

	public String getLeaveHospDate()
	{
		if( LeaveHospDate != null )
			return fDate.getString(LeaveHospDate);
		else
			return null;
	}
	public void setLeaveHospDate(Date aLeaveHospDate)
	{
		LeaveHospDate = aLeaveHospDate;
	}
	public void setLeaveHospDate(String aLeaveHospDate)
	{
		if (aLeaveHospDate != null && !aLeaveHospDate.equals("") )
		{
			LeaveHospDate = fDate.getDate( aLeaveHospDate );
		}
		else
			LeaveHospDate = null;
	}

	public String getRgtDate()
	{
		if( RgtDate != null )
			return fDate.getString(RgtDate);
		else
			return null;
	}
	public void setRgtDate(Date aRgtDate)
	{
		RgtDate = aRgtDate;
	}
	public void setRgtDate(String aRgtDate)
	{
		if (aRgtDate != null && !aRgtDate.equals("") )
		{
			RgtDate = fDate.getDate( aRgtDate );
		}
		else
			RgtDate = null;
	}

	public String getgetdatadate()
	{
		if( getdatadate != null )
			return fDate.getString(getdatadate);
		else
			return null;
	}
	public void setgetdatadate(Date agetdatadate)
	{
		getdatadate = agetdatadate;
	}
	public void setgetdatadate(String agetdatadate)
	{
		if (agetdatadate != null && !agetdatadate.equals("") )
		{
			getdatadate = fDate.getDate( agetdatadate );
		}
		else
			getdatadate = null;
	}

	public String getGiveTypeDesc()
	{
		return GiveTypeDesc;
	}
	public void setGiveTypeDesc(String aGiveTypeDesc)
	{
		GiveTypeDesc = aGiveTypeDesc;
	}
	public String getTempCessFlag()
	{
		return TempCessFlag;
	}
	public void setTempCessFlag(String aTempCessFlag)
	{
		TempCessFlag = aTempCessFlag;
	}
	public double getCessionRate()
	{
		return CessionRate;
	}
	public void setCessionRate(double aCessionRate)
	{
		CessionRate = Arith.round(aCessionRate,6);
	}
	public void setCessionRate(String aCessionRate)
	{
		if (aCessionRate != null && !aCessionRate.equals(""))
		{
			Double tDouble = new Double(aCessionRate);
			double d = tDouble.doubleValue();
                CessionRate = Arith.round(d,6);
		}
	}

	public double getCessionAmount()
	{
		return CessionAmount;
	}
	public void setCessionAmount(double aCessionAmount)
	{
		CessionAmount = Arith.round(aCessionAmount,2);
	}
	public void setCessionAmount(String aCessionAmount)
	{
		if (aCessionAmount != null && !aCessionAmount.equals(""))
		{
			Double tDouble = new Double(aCessionAmount);
			double d = tDouble.doubleValue();
                CessionAmount = Arith.round(d,2);
		}
	}

	public double getClaimBackFee()
	{
		return ClaimBackFee;
	}
	public void setClaimBackFee(double aClaimBackFee)
	{
		ClaimBackFee = Arith.round(aClaimBackFee,2);
	}
	public void setClaimBackFee(String aClaimBackFee)
	{
		if (aClaimBackFee != null && !aClaimBackFee.equals(""))
		{
			Double tDouble = new Double(aClaimBackFee);
			double d = tDouble.doubleValue();
                ClaimBackFee = Arith.round(d,2);
		}
	}

	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getListDate()
	{
		if( ListDate != null )
			return fDate.getString(ListDate);
		else
			return null;
	}
	public void setListDate(Date aListDate)
	{
		ListDate = aListDate;
	}
	public void setListDate(String aListDate)
	{
		if (aListDate != null && !aListDate.equals("") )
		{
			ListDate = fDate.getDate( aListDate );
		}
		else
			ListDate = null;
	}

	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getOprater()
	{
		return Oprater;
	}
	public void setOprater(String aOprater)
	{
		Oprater = aOprater;
	}
	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public String getClmNo()
	{
		return ClmNo;
	}
	public void setClmNo(String aClmNo)
	{
		ClmNo = aClmNo;
	}
	public String getReinsureItem()
	{
		return ReinsureItem;
	}
	public void setReinsureItem(String aReinsureItem)
	{
		ReinsureItem = aReinsureItem;
	}
	public String getCostCenter()
	{
		return CostCenter;
	}
	public void setCostCenter(String aCostCenter)
	{
		CostCenter = aCostCenter;
	}
	public String getMarketType()
	{
		return MarketType;
	}
	public void setMarketType(String aMarketType)
	{
		MarketType = aMarketType;
	}

	/**
	* 使用另外一个 LRClaimListSchema 对象给 Schema 赋值
	* @param: aLRClaimListSchema LRClaimListSchema
	**/
	public void setSchema(LRClaimListSchema aLRClaimListSchema)
	{
		this.ReContCode = aLRClaimListSchema.getReContCode();
		this.RiskCode = aLRClaimListSchema.getRiskCode();
		this.ContNo = aLRClaimListSchema.getContNo();
		this.GrpContNo = aLRClaimListSchema.getGrpContNo();
		this.CValiDate = fDate.getDate( aLRClaimListSchema.getCValiDate());
		this.EndDate = fDate.getDate( aLRClaimListSchema.getEndDate());
		this.SignDate = fDate.getDate( aLRClaimListSchema.getSignDate());
		this.AppntName = aLRClaimListSchema.getAppntName();
		this.AppntNo = aLRClaimListSchema.getAppntNo();
		this.InsuredName = aLRClaimListSchema.getInsuredName();
		this.InsuredBirthday = fDate.getDate( aLRClaimListSchema.getInsuredBirthday());
		this.InsuredSex = aLRClaimListSchema.getInsuredSex();
		this.InsuredIdNo = aLRClaimListSchema.getInsuredIdNo();
		this.OccupationType = aLRClaimListSchema.getOccupationType();
		this.Amnt = aLRClaimListSchema.getAmnt();
		this.ClaimMoney = aLRClaimListSchema.getClaimMoney();
		this.RealPay = aLRClaimListSchema.getRealPay();
		this.Diagnoses = aLRClaimListSchema.getDiagnoses();
		this.AccDesc = aLRClaimListSchema.getAccDesc();
		this.AccidentDate = fDate.getDate( aLRClaimListSchema.getAccidentDate());
		this.LeaveHospDate = fDate.getDate( aLRClaimListSchema.getLeaveHospDate());
		this.RgtDate = fDate.getDate( aLRClaimListSchema.getRgtDate());
		this.getdatadate = fDate.getDate( aLRClaimListSchema.getgetdatadate());
		this.GiveTypeDesc = aLRClaimListSchema.getGiveTypeDesc();
		this.TempCessFlag = aLRClaimListSchema.getTempCessFlag();
		this.CessionRate = aLRClaimListSchema.getCessionRate();
		this.CessionAmount = aLRClaimListSchema.getCessionAmount();
		this.ClaimBackFee = aLRClaimListSchema.getClaimBackFee();
		this.ManageCom = aLRClaimListSchema.getManageCom();
		this.ListDate = fDate.getDate( aLRClaimListSchema.getListDate());
		this.MakeDate = fDate.getDate( aLRClaimListSchema.getMakeDate());
		this.MakeTime = aLRClaimListSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRClaimListSchema.getModifyDate());
		this.ModifyTime = aLRClaimListSchema.getModifyTime();
		this.Oprater = aLRClaimListSchema.getOprater();
		this.PolNo = aLRClaimListSchema.getPolNo();
		this.ClmNo = aLRClaimListSchema.getClmNo();
		this.ReinsureItem = aLRClaimListSchema.getReinsureItem();
		this.CostCenter = aLRClaimListSchema.getCostCenter();
		this.MarketType = aLRClaimListSchema.getMarketType();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ReContCode") == null )
				this.ReContCode = null;
			else
				this.ReContCode = rs.getString("ReContCode").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("ContNo") == null )
				this.ContNo = null;
			else
				this.ContNo = rs.getString("ContNo").trim();

			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			this.CValiDate = rs.getDate("CValiDate");
			this.EndDate = rs.getDate("EndDate");
			this.SignDate = rs.getDate("SignDate");
			if( rs.getString("AppntName") == null )
				this.AppntName = null;
			else
				this.AppntName = rs.getString("AppntName").trim();

			if( rs.getString("AppntNo") == null )
				this.AppntNo = null;
			else
				this.AppntNo = rs.getString("AppntNo").trim();

			if( rs.getString("InsuredName") == null )
				this.InsuredName = null;
			else
				this.InsuredName = rs.getString("InsuredName").trim();

			this.InsuredBirthday = rs.getDate("InsuredBirthday");
			if( rs.getString("InsuredSex") == null )
				this.InsuredSex = null;
			else
				this.InsuredSex = rs.getString("InsuredSex").trim();

			if( rs.getString("InsuredIdNo") == null )
				this.InsuredIdNo = null;
			else
				this.InsuredIdNo = rs.getString("InsuredIdNo").trim();

			if( rs.getString("OccupationType") == null )
				this.OccupationType = null;
			else
				this.OccupationType = rs.getString("OccupationType").trim();

			this.Amnt = rs.getDouble("Amnt");
			this.ClaimMoney = rs.getDouble("ClaimMoney");
			this.RealPay = rs.getDouble("RealPay");
			if( rs.getString("Diagnoses") == null )
				this.Diagnoses = null;
			else
				this.Diagnoses = rs.getString("Diagnoses").trim();

			if( rs.getString("AccDesc") == null )
				this.AccDesc = null;
			else
				this.AccDesc = rs.getString("AccDesc").trim();

			this.AccidentDate = rs.getDate("AccidentDate");
			this.LeaveHospDate = rs.getDate("LeaveHospDate");
			this.RgtDate = rs.getDate("RgtDate");
			this.getdatadate = rs.getDate("getdatadate");
			if( rs.getString("GiveTypeDesc") == null )
				this.GiveTypeDesc = null;
			else
				this.GiveTypeDesc = rs.getString("GiveTypeDesc").trim();

			if( rs.getString("TempCessFlag") == null )
				this.TempCessFlag = null;
			else
				this.TempCessFlag = rs.getString("TempCessFlag").trim();

			this.CessionRate = rs.getDouble("CessionRate");
			this.CessionAmount = rs.getDouble("CessionAmount");
			this.ClaimBackFee = rs.getDouble("ClaimBackFee");
			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.ListDate = rs.getDate("ListDate");
			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Oprater") == null )
				this.Oprater = null;
			else
				this.Oprater = rs.getString("Oprater").trim();

			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			if( rs.getString("ClmNo") == null )
				this.ClmNo = null;
			else
				this.ClmNo = rs.getString("ClmNo").trim();

			if( rs.getString("ReinsureItem") == null )
				this.ReinsureItem = null;
			else
				this.ReinsureItem = rs.getString("ReinsureItem").trim();

			if( rs.getString("CostCenter") == null )
				this.CostCenter = null;
			else
				this.CostCenter = rs.getString("CostCenter").trim();

			if( rs.getString("MarketType") == null )
				this.MarketType = null;
			else
				this.MarketType = rs.getString("MarketType").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRClaimList表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRClaimListSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRClaimListSchema getSchema()
	{
		LRClaimListSchema aLRClaimListSchema = new LRClaimListSchema();
		aLRClaimListSchema.setSchema(this);
		return aLRClaimListSchema;
	}

	public LRClaimListDB getDB()
	{
		LRClaimListDB aDBOper = new LRClaimListDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRClaimList描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ReContCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CValiDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SignDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AppntNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( InsuredBirthday ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuredIdNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OccupationType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Amnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ClaimMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RealPay));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Diagnoses)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccidentDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LeaveHospDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RgtDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( getdatadate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GiveTypeDesc)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TempCessFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessionRate));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessionAmount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ClaimBackFee));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ListDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Oprater)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClmNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReinsureItem)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostCenter)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MarketType));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRClaimList>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ReContCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			InsuredName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			InsuredBirthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			InsuredSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			InsuredIdNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			OccupationType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			Amnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			ClaimMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			RealPay = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,17,SysConst.PACKAGESPILTER))).doubleValue();
			Diagnoses = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			AccDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			AccidentDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			LeaveHospDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			RgtDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,SysConst.PACKAGESPILTER));
			getdatadate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			GiveTypeDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			TempCessFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			CessionRate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			CessionAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			ClaimBackFee = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			ListDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30,SysConst.PACKAGESPILTER));
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			Oprater = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			ClmNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			ReinsureItem = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			CostCenter = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			MarketType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRClaimListSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ReContCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContCode));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("ContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
		}
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("CValiDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
		}
		if (FCode.equals("EndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
		}
		if (FCode.equals("SignDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
		}
		if (FCode.equals("AppntName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
		}
		if (FCode.equals("AppntNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
		}
		if (FCode.equals("InsuredName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredName));
		}
		if (FCode.equals("InsuredBirthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInsuredBirthday()));
		}
		if (FCode.equals("InsuredSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredSex));
		}
		if (FCode.equals("InsuredIdNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuredIdNo));
		}
		if (FCode.equals("OccupationType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OccupationType));
		}
		if (FCode.equals("Amnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Amnt));
		}
		if (FCode.equals("ClaimMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimMoney));
		}
		if (FCode.equals("RealPay"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RealPay));
		}
		if (FCode.equals("Diagnoses"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Diagnoses));
		}
		if (FCode.equals("AccDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccDesc));
		}
		if (FCode.equals("AccidentDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccidentDate()));
		}
		if (FCode.equals("LeaveHospDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLeaveHospDate()));
		}
		if (FCode.equals("RgtDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRgtDate()));
		}
		if (FCode.equals("getdatadate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getgetdatadate()));
		}
		if (FCode.equals("GiveTypeDesc"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GiveTypeDesc));
		}
		if (FCode.equals("TempCessFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TempCessFlag));
		}
		if (FCode.equals("CessionRate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionRate));
		}
		if (FCode.equals("CessionAmount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessionAmount));
		}
		if (FCode.equals("ClaimBackFee"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimBackFee));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("ListDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getListDate()));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Oprater"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Oprater));
		}
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("ClmNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClmNo));
		}
		if (FCode.equals("ReinsureItem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReinsureItem));
		}
		if (FCode.equals("CostCenter"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostCenter));
		}
		if (FCode.equals("MarketType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MarketType));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ReContCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ContNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCValiDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSignDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AppntName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(AppntNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(InsuredName);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInsuredBirthday()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(InsuredSex);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(InsuredIdNo);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(OccupationType);
				break;
			case 14:
				strFieldValue = String.valueOf(Amnt);
				break;
			case 15:
				strFieldValue = String.valueOf(ClaimMoney);
				break;
			case 16:
				strFieldValue = String.valueOf(RealPay);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Diagnoses);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(AccDesc);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccidentDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLeaveHospDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRgtDate()));
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getgetdatadate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(GiveTypeDesc);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(TempCessFlag);
				break;
			case 25:
				strFieldValue = String.valueOf(CessionRate);
				break;
			case 26:
				strFieldValue = String.valueOf(CessionAmount);
				break;
			case 27:
				strFieldValue = String.valueOf(ClaimBackFee);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getListDate()));
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(Oprater);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(ClmNo);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(ReinsureItem);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(CostCenter);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(MarketType);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ReContCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContCode = FValue.trim();
			}
			else
				ReContCode = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("ContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContNo = FValue.trim();
			}
			else
				ContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("CValiDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CValiDate = fDate.getDate( FValue );
			}
			else
				CValiDate = null;
		}
		if (FCode.equalsIgnoreCase("EndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndDate = fDate.getDate( FValue );
			}
			else
				EndDate = null;
		}
		if (FCode.equalsIgnoreCase("SignDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SignDate = fDate.getDate( FValue );
			}
			else
				SignDate = null;
		}
		if (FCode.equalsIgnoreCase("AppntName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntName = FValue.trim();
			}
			else
				AppntName = null;
		}
		if (FCode.equalsIgnoreCase("AppntNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AppntNo = FValue.trim();
			}
			else
				AppntNo = null;
		}
		if (FCode.equalsIgnoreCase("InsuredName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredName = FValue.trim();
			}
			else
				InsuredName = null;
		}
		if (FCode.equalsIgnoreCase("InsuredBirthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				InsuredBirthday = fDate.getDate( FValue );
			}
			else
				InsuredBirthday = null;
		}
		if (FCode.equalsIgnoreCase("InsuredSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredSex = FValue.trim();
			}
			else
				InsuredSex = null;
		}
		if (FCode.equalsIgnoreCase("InsuredIdNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuredIdNo = FValue.trim();
			}
			else
				InsuredIdNo = null;
		}
		if (FCode.equalsIgnoreCase("OccupationType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OccupationType = FValue.trim();
			}
			else
				OccupationType = null;
		}
		if (FCode.equalsIgnoreCase("Amnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Amnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClaimMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ClaimMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("RealPay"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RealPay = d;
			}
		}
		if (FCode.equalsIgnoreCase("Diagnoses"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Diagnoses = FValue.trim();
			}
			else
				Diagnoses = null;
		}
		if (FCode.equalsIgnoreCase("AccDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccDesc = FValue.trim();
			}
			else
				AccDesc = null;
		}
		if (FCode.equalsIgnoreCase("AccidentDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccidentDate = fDate.getDate( FValue );
			}
			else
				AccidentDate = null;
		}
		if (FCode.equalsIgnoreCase("LeaveHospDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LeaveHospDate = fDate.getDate( FValue );
			}
			else
				LeaveHospDate = null;
		}
		if (FCode.equalsIgnoreCase("RgtDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RgtDate = fDate.getDate( FValue );
			}
			else
				RgtDate = null;
		}
		if (FCode.equalsIgnoreCase("getdatadate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				getdatadate = fDate.getDate( FValue );
			}
			else
				getdatadate = null;
		}
		if (FCode.equalsIgnoreCase("GiveTypeDesc"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GiveTypeDesc = FValue.trim();
			}
			else
				GiveTypeDesc = null;
		}
		if (FCode.equalsIgnoreCase("TempCessFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TempCessFlag = FValue.trim();
			}
			else
				TempCessFlag = null;
		}
		if (FCode.equalsIgnoreCase("CessionRate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessionRate = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessionAmount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessionAmount = d;
			}
		}
		if (FCode.equalsIgnoreCase("ClaimBackFee"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ClaimBackFee = d;
			}
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("ListDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ListDate = fDate.getDate( FValue );
			}
			else
				ListDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Oprater"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Oprater = FValue.trim();
			}
			else
				Oprater = null;
		}
		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("ClmNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClmNo = FValue.trim();
			}
			else
				ClmNo = null;
		}
		if (FCode.equalsIgnoreCase("ReinsureItem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReinsureItem = FValue.trim();
			}
			else
				ReinsureItem = null;
		}
		if (FCode.equalsIgnoreCase("CostCenter"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostCenter = FValue.trim();
			}
			else
				CostCenter = null;
		}
		if (FCode.equalsIgnoreCase("MarketType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MarketType = FValue.trim();
			}
			else
				MarketType = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRClaimListSchema other = (LRClaimListSchema)otherObject;
		return
			(ReContCode == null ? other.getReContCode() == null : ReContCode.equals(other.getReContCode()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (ContNo == null ? other.getContNo() == null : ContNo.equals(other.getContNo()))
			&& (GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (CValiDate == null ? other.getCValiDate() == null : fDate.getString(CValiDate).equals(other.getCValiDate()))
			&& (EndDate == null ? other.getEndDate() == null : fDate.getString(EndDate).equals(other.getEndDate()))
			&& (SignDate == null ? other.getSignDate() == null : fDate.getString(SignDate).equals(other.getSignDate()))
			&& (AppntName == null ? other.getAppntName() == null : AppntName.equals(other.getAppntName()))
			&& (AppntNo == null ? other.getAppntNo() == null : AppntNo.equals(other.getAppntNo()))
			&& (InsuredName == null ? other.getInsuredName() == null : InsuredName.equals(other.getInsuredName()))
			&& (InsuredBirthday == null ? other.getInsuredBirthday() == null : fDate.getString(InsuredBirthday).equals(other.getInsuredBirthday()))
			&& (InsuredSex == null ? other.getInsuredSex() == null : InsuredSex.equals(other.getInsuredSex()))
			&& (InsuredIdNo == null ? other.getInsuredIdNo() == null : InsuredIdNo.equals(other.getInsuredIdNo()))
			&& (OccupationType == null ? other.getOccupationType() == null : OccupationType.equals(other.getOccupationType()))
			&& Amnt == other.getAmnt()
			&& ClaimMoney == other.getClaimMoney()
			&& RealPay == other.getRealPay()
			&& (Diagnoses == null ? other.getDiagnoses() == null : Diagnoses.equals(other.getDiagnoses()))
			&& (AccDesc == null ? other.getAccDesc() == null : AccDesc.equals(other.getAccDesc()))
			&& (AccidentDate == null ? other.getAccidentDate() == null : fDate.getString(AccidentDate).equals(other.getAccidentDate()))
			&& (LeaveHospDate == null ? other.getLeaveHospDate() == null : fDate.getString(LeaveHospDate).equals(other.getLeaveHospDate()))
			&& (RgtDate == null ? other.getRgtDate() == null : fDate.getString(RgtDate).equals(other.getRgtDate()))
			&& (getdatadate == null ? other.getgetdatadate() == null : fDate.getString(getdatadate).equals(other.getgetdatadate()))
			&& (GiveTypeDesc == null ? other.getGiveTypeDesc() == null : GiveTypeDesc.equals(other.getGiveTypeDesc()))
			&& (TempCessFlag == null ? other.getTempCessFlag() == null : TempCessFlag.equals(other.getTempCessFlag()))
			&& CessionRate == other.getCessionRate()
			&& CessionAmount == other.getCessionAmount()
			&& ClaimBackFee == other.getClaimBackFee()
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (ListDate == null ? other.getListDate() == null : fDate.getString(ListDate).equals(other.getListDate()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Oprater == null ? other.getOprater() == null : Oprater.equals(other.getOprater()))
			&& (PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& (ClmNo == null ? other.getClmNo() == null : ClmNo.equals(other.getClmNo()))
			&& (ReinsureItem == null ? other.getReinsureItem() == null : ReinsureItem.equals(other.getReinsureItem()))
			&& (CostCenter == null ? other.getCostCenter() == null : CostCenter.equals(other.getCostCenter()))
			&& (MarketType == null ? other.getMarketType() == null : MarketType.equals(other.getMarketType()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ReContCode") ) {
			return 0;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 1;
		}
		if( strFieldName.equals("ContNo") ) {
			return 2;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return 3;
		}
		if( strFieldName.equals("CValiDate") ) {
			return 4;
		}
		if( strFieldName.equals("EndDate") ) {
			return 5;
		}
		if( strFieldName.equals("SignDate") ) {
			return 6;
		}
		if( strFieldName.equals("AppntName") ) {
			return 7;
		}
		if( strFieldName.equals("AppntNo") ) {
			return 8;
		}
		if( strFieldName.equals("InsuredName") ) {
			return 9;
		}
		if( strFieldName.equals("InsuredBirthday") ) {
			return 10;
		}
		if( strFieldName.equals("InsuredSex") ) {
			return 11;
		}
		if( strFieldName.equals("InsuredIdNo") ) {
			return 12;
		}
		if( strFieldName.equals("OccupationType") ) {
			return 13;
		}
		if( strFieldName.equals("Amnt") ) {
			return 14;
		}
		if( strFieldName.equals("ClaimMoney") ) {
			return 15;
		}
		if( strFieldName.equals("RealPay") ) {
			return 16;
		}
		if( strFieldName.equals("Diagnoses") ) {
			return 17;
		}
		if( strFieldName.equals("AccDesc") ) {
			return 18;
		}
		if( strFieldName.equals("AccidentDate") ) {
			return 19;
		}
		if( strFieldName.equals("LeaveHospDate") ) {
			return 20;
		}
		if( strFieldName.equals("RgtDate") ) {
			return 21;
		}
		if( strFieldName.equals("getdatadate") ) {
			return 22;
		}
		if( strFieldName.equals("GiveTypeDesc") ) {
			return 23;
		}
		if( strFieldName.equals("TempCessFlag") ) {
			return 24;
		}
		if( strFieldName.equals("CessionRate") ) {
			return 25;
		}
		if( strFieldName.equals("CessionAmount") ) {
			return 26;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return 27;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 28;
		}
		if( strFieldName.equals("ListDate") ) {
			return 29;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 30;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 31;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 32;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 33;
		}
		if( strFieldName.equals("Oprater") ) {
			return 34;
		}
		if( strFieldName.equals("PolNo") ) {
			return 35;
		}
		if( strFieldName.equals("ClmNo") ) {
			return 36;
		}
		if( strFieldName.equals("ReinsureItem") ) {
			return 37;
		}
		if( strFieldName.equals("CostCenter") ) {
			return 38;
		}
		if( strFieldName.equals("MarketType") ) {
			return 39;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ReContCode";
				break;
			case 1:
				strFieldName = "RiskCode";
				break;
			case 2:
				strFieldName = "ContNo";
				break;
			case 3:
				strFieldName = "GrpContNo";
				break;
			case 4:
				strFieldName = "CValiDate";
				break;
			case 5:
				strFieldName = "EndDate";
				break;
			case 6:
				strFieldName = "SignDate";
				break;
			case 7:
				strFieldName = "AppntName";
				break;
			case 8:
				strFieldName = "AppntNo";
				break;
			case 9:
				strFieldName = "InsuredName";
				break;
			case 10:
				strFieldName = "InsuredBirthday";
				break;
			case 11:
				strFieldName = "InsuredSex";
				break;
			case 12:
				strFieldName = "InsuredIdNo";
				break;
			case 13:
				strFieldName = "OccupationType";
				break;
			case 14:
				strFieldName = "Amnt";
				break;
			case 15:
				strFieldName = "ClaimMoney";
				break;
			case 16:
				strFieldName = "RealPay";
				break;
			case 17:
				strFieldName = "Diagnoses";
				break;
			case 18:
				strFieldName = "AccDesc";
				break;
			case 19:
				strFieldName = "AccidentDate";
				break;
			case 20:
				strFieldName = "LeaveHospDate";
				break;
			case 21:
				strFieldName = "RgtDate";
				break;
			case 22:
				strFieldName = "getdatadate";
				break;
			case 23:
				strFieldName = "GiveTypeDesc";
				break;
			case 24:
				strFieldName = "TempCessFlag";
				break;
			case 25:
				strFieldName = "CessionRate";
				break;
			case 26:
				strFieldName = "CessionAmount";
				break;
			case 27:
				strFieldName = "ClaimBackFee";
				break;
			case 28:
				strFieldName = "ManageCom";
				break;
			case 29:
				strFieldName = "ListDate";
				break;
			case 30:
				strFieldName = "MakeDate";
				break;
			case 31:
				strFieldName = "MakeTime";
				break;
			case 32:
				strFieldName = "ModifyDate";
				break;
			case 33:
				strFieldName = "ModifyTime";
				break;
			case 34:
				strFieldName = "Oprater";
				break;
			case 35:
				strFieldName = "PolNo";
				break;
			case 36:
				strFieldName = "ClmNo";
				break;
			case 37:
				strFieldName = "ReinsureItem";
				break;
			case 38:
				strFieldName = "CostCenter";
				break;
			case 39:
				strFieldName = "MarketType";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ReContCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CValiDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SignDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AppntName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppntNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredBirthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("InsuredSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuredIdNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OccupationType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Amnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClaimMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RealPay") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Diagnoses") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccidentDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("LeaveHospDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RgtDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("getdatadate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("GiveTypeDesc") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TempCessFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CessionRate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessionAmount") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ClaimBackFee") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ListDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Oprater") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClmNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReinsureItem") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostCenter") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MarketType") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
