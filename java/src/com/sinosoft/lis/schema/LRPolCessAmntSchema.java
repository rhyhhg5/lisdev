/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRPolCessAmntDB;

/*
 * <p>ClassName: LRPolCessAmntSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2009-11-20
 */
public class LRPolCessAmntSchema implements Schema, Cloneable
{
	// @Field
	/** 保单险种编码 */
	private String PolNo;
	/** 续保次数 */
	private int ReNewCount;
	/** 续期次数 */
	private int PayCount;
	/** 累计保额 */
	private double SumAmnt;
	/** 累计风险保额 */
	private double SumRiskAmnt;
	/** 累计风险准备金 */
	private double SumReserve;
	/** 累计已分出保额 */
	private double SumCessAmnt;
	/** 自留额 */
	private double RemainAmnt;
	/** 本次分保保额 */
	private double CessAmnt;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 给付责任编码 */
	private String GetDutyCode;

	public static final int FIELDNUM = 15;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRPolCessAmntSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "PolNo";
		pk[1] = "ReNewCount";
		pk[2] = "PayCount";
		pk[3] = "GetDutyCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRPolCessAmntSchema cloned = (LRPolCessAmntSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPolNo()
	{
		return PolNo;
	}
	public void setPolNo(String aPolNo)
	{
		PolNo = aPolNo;
	}
	public int getReNewCount()
	{
		return ReNewCount;
	}
	public void setReNewCount(int aReNewCount)
	{
		ReNewCount = aReNewCount;
	}
	public void setReNewCount(String aReNewCount)
	{
		if (aReNewCount != null && !aReNewCount.equals(""))
		{
			Integer tInteger = new Integer(aReNewCount);
			int i = tInteger.intValue();
			ReNewCount = i;
		}
	}

	public int getPayCount()
	{
		return PayCount;
	}
	public void setPayCount(int aPayCount)
	{
		PayCount = aPayCount;
	}
	public void setPayCount(String aPayCount)
	{
		if (aPayCount != null && !aPayCount.equals(""))
		{
			Integer tInteger = new Integer(aPayCount);
			int i = tInteger.intValue();
			PayCount = i;
		}
	}

	public double getSumAmnt()
	{
		return SumAmnt;
	}
	public void setSumAmnt(double aSumAmnt)
	{
		SumAmnt = Arith.round(aSumAmnt,2);
	}
	public void setSumAmnt(String aSumAmnt)
	{
		if (aSumAmnt != null && !aSumAmnt.equals(""))
		{
			Double tDouble = new Double(aSumAmnt);
			double d = tDouble.doubleValue();
                SumAmnt = Arith.round(d,2);
		}
	}

	public double getSumRiskAmnt()
	{
		return SumRiskAmnt;
	}
	public void setSumRiskAmnt(double aSumRiskAmnt)
	{
		SumRiskAmnt = Arith.round(aSumRiskAmnt,2);
	}
	public void setSumRiskAmnt(String aSumRiskAmnt)
	{
		if (aSumRiskAmnt != null && !aSumRiskAmnt.equals(""))
		{
			Double tDouble = new Double(aSumRiskAmnt);
			double d = tDouble.doubleValue();
                SumRiskAmnt = Arith.round(d,2);
		}
	}

	public double getSumReserve()
	{
		return SumReserve;
	}
	public void setSumReserve(double aSumReserve)
	{
		SumReserve = Arith.round(aSumReserve,2);
	}
	public void setSumReserve(String aSumReserve)
	{
		if (aSumReserve != null && !aSumReserve.equals(""))
		{
			Double tDouble = new Double(aSumReserve);
			double d = tDouble.doubleValue();
                SumReserve = Arith.round(d,2);
		}
	}

	public double getSumCessAmnt()
	{
		return SumCessAmnt;
	}
	public void setSumCessAmnt(double aSumCessAmnt)
	{
		SumCessAmnt = Arith.round(aSumCessAmnt,2);
	}
	public void setSumCessAmnt(String aSumCessAmnt)
	{
		if (aSumCessAmnt != null && !aSumCessAmnt.equals(""))
		{
			Double tDouble = new Double(aSumCessAmnt);
			double d = tDouble.doubleValue();
                SumCessAmnt = Arith.round(d,2);
		}
	}

	public double getRemainAmnt()
	{
		return RemainAmnt;
	}
	public void setRemainAmnt(double aRemainAmnt)
	{
		RemainAmnt = Arith.round(aRemainAmnt,2);
	}
	public void setRemainAmnt(String aRemainAmnt)
	{
		if (aRemainAmnt != null && !aRemainAmnt.equals(""))
		{
			Double tDouble = new Double(aRemainAmnt);
			double d = tDouble.doubleValue();
                RemainAmnt = Arith.round(d,2);
		}
	}

	public double getCessAmnt()
	{
		return CessAmnt;
	}
	public void setCessAmnt(double aCessAmnt)
	{
		CessAmnt = Arith.round(aCessAmnt,2);
	}
	public void setCessAmnt(String aCessAmnt)
	{
		if (aCessAmnt != null && !aCessAmnt.equals(""))
		{
			Double tDouble = new Double(aCessAmnt);
			double d = tDouble.doubleValue();
                CessAmnt = Arith.round(d,2);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getGetDutyCode()
	{
		return GetDutyCode;
	}
	public void setGetDutyCode(String aGetDutyCode)
	{
		GetDutyCode = aGetDutyCode;
	}

	/**
	* 使用另外一个 LRPolCessAmntSchema 对象给 Schema 赋值
	* @param: aLRPolCessAmntSchema LRPolCessAmntSchema
	**/
	public void setSchema(LRPolCessAmntSchema aLRPolCessAmntSchema)
	{
		this.PolNo = aLRPolCessAmntSchema.getPolNo();
		this.ReNewCount = aLRPolCessAmntSchema.getReNewCount();
		this.PayCount = aLRPolCessAmntSchema.getPayCount();
		this.SumAmnt = aLRPolCessAmntSchema.getSumAmnt();
		this.SumRiskAmnt = aLRPolCessAmntSchema.getSumRiskAmnt();
		this.SumReserve = aLRPolCessAmntSchema.getSumReserve();
		this.SumCessAmnt = aLRPolCessAmntSchema.getSumCessAmnt();
		this.RemainAmnt = aLRPolCessAmntSchema.getRemainAmnt();
		this.CessAmnt = aLRPolCessAmntSchema.getCessAmnt();
		this.Operator = aLRPolCessAmntSchema.getOperator();
		this.MakeDate = fDate.getDate( aLRPolCessAmntSchema.getMakeDate());
		this.MakeTime = aLRPolCessAmntSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRPolCessAmntSchema.getModifyDate());
		this.ModifyTime = aLRPolCessAmntSchema.getModifyTime();
		this.GetDutyCode = aLRPolCessAmntSchema.getGetDutyCode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PolNo") == null )
				this.PolNo = null;
			else
				this.PolNo = rs.getString("PolNo").trim();

			this.ReNewCount = rs.getInt("ReNewCount");
			this.PayCount = rs.getInt("PayCount");
			this.SumAmnt = rs.getDouble("SumAmnt");
			this.SumRiskAmnt = rs.getDouble("SumRiskAmnt");
			this.SumReserve = rs.getDouble("SumReserve");
			this.SumCessAmnt = rs.getDouble("SumCessAmnt");
			this.RemainAmnt = rs.getDouble("RemainAmnt");
			this.CessAmnt = rs.getDouble("CessAmnt");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("GetDutyCode") == null )
				this.GetDutyCode = null;
			else
				this.GetDutyCode = rs.getString("GetDutyCode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRPolCessAmnt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRPolCessAmntSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRPolCessAmntSchema getSchema()
	{
		LRPolCessAmntSchema aLRPolCessAmntSchema = new LRPolCessAmntSchema();
		aLRPolCessAmntSchema.setSchema(this);
		return aLRPolCessAmntSchema;
	}

	public LRPolCessAmntDB getDB()
	{
		LRPolCessAmntDB aDBOper = new LRPolCessAmntDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRPolCessAmnt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReNewCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PayCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumRiskAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumReserve));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(SumCessAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RemainAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetDutyCode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRPolCessAmnt>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ReNewCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,2,SysConst.PACKAGESPILTER))).intValue();
			PayCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).intValue();
			SumAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).doubleValue();
			SumRiskAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			SumReserve = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			SumCessAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			RemainAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			CessAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			GetDutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRPolCessAmntSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolNo));
		}
		if (FCode.equals("ReNewCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReNewCount));
		}
		if (FCode.equals("PayCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayCount));
		}
		if (FCode.equals("SumAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumAmnt));
		}
		if (FCode.equals("SumRiskAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumRiskAmnt));
		}
		if (FCode.equals("SumReserve"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumReserve));
		}
		if (FCode.equals("SumCessAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SumCessAmnt));
		}
		if (FCode.equals("RemainAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RemainAmnt));
		}
		if (FCode.equals("CessAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessAmnt));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("GetDutyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetDutyCode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PolNo);
				break;
			case 1:
				strFieldValue = String.valueOf(ReNewCount);
				break;
			case 2:
				strFieldValue = String.valueOf(PayCount);
				break;
			case 3:
				strFieldValue = String.valueOf(SumAmnt);
				break;
			case 4:
				strFieldValue = String.valueOf(SumRiskAmnt);
				break;
			case 5:
				strFieldValue = String.valueOf(SumReserve);
				break;
			case 6:
				strFieldValue = String.valueOf(SumCessAmnt);
				break;
			case 7:
				strFieldValue = String.valueOf(RemainAmnt);
				break;
			case 8:
				strFieldValue = String.valueOf(CessAmnt);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(GetDutyCode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolNo = FValue.trim();
			}
			else
				PolNo = null;
		}
		if (FCode.equalsIgnoreCase("ReNewCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ReNewCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("PayCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PayCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("SumAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumRiskAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumRiskAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumReserve"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumReserve = d;
			}
		}
		if (FCode.equalsIgnoreCase("SumCessAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				SumCessAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("RemainAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RemainAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("GetDutyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetDutyCode = FValue.trim();
			}
			else
				GetDutyCode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRPolCessAmntSchema other = (LRPolCessAmntSchema)otherObject;
		return
			(PolNo == null ? other.getPolNo() == null : PolNo.equals(other.getPolNo()))
			&& ReNewCount == other.getReNewCount()
			&& PayCount == other.getPayCount()
			&& SumAmnt == other.getSumAmnt()
			&& SumRiskAmnt == other.getSumRiskAmnt()
			&& SumReserve == other.getSumReserve()
			&& SumCessAmnt == other.getSumCessAmnt()
			&& RemainAmnt == other.getRemainAmnt()
			&& CessAmnt == other.getCessAmnt()
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (GetDutyCode == null ? other.getGetDutyCode() == null : GetDutyCode.equals(other.getGetDutyCode()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PolNo") ) {
			return 0;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return 1;
		}
		if( strFieldName.equals("PayCount") ) {
			return 2;
		}
		if( strFieldName.equals("SumAmnt") ) {
			return 3;
		}
		if( strFieldName.equals("SumRiskAmnt") ) {
			return 4;
		}
		if( strFieldName.equals("SumReserve") ) {
			return 5;
		}
		if( strFieldName.equals("SumCessAmnt") ) {
			return 6;
		}
		if( strFieldName.equals("RemainAmnt") ) {
			return 7;
		}
		if( strFieldName.equals("CessAmnt") ) {
			return 8;
		}
		if( strFieldName.equals("Operator") ) {
			return 9;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 11;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 13;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return 14;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PolNo";
				break;
			case 1:
				strFieldName = "ReNewCount";
				break;
			case 2:
				strFieldName = "PayCount";
				break;
			case 3:
				strFieldName = "SumAmnt";
				break;
			case 4:
				strFieldName = "SumRiskAmnt";
				break;
			case 5:
				strFieldName = "SumReserve";
				break;
			case 6:
				strFieldName = "SumCessAmnt";
				break;
			case 7:
				strFieldName = "RemainAmnt";
				break;
			case 8:
				strFieldName = "CessAmnt";
				break;
			case 9:
				strFieldName = "Operator";
				break;
			case 10:
				strFieldName = "MakeDate";
				break;
			case 11:
				strFieldName = "MakeTime";
				break;
			case 12:
				strFieldName = "ModifyDate";
				break;
			case 13:
				strFieldName = "ModifyTime";
				break;
			case 14:
				strFieldName = "GetDutyCode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReNewCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PayCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("SumAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumRiskAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumReserve") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("SumCessAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RemainAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDutyCode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_INT;
				break;
			case 2:
				nFieldType = Schema.TYPE_INT;
				break;
			case 3:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
