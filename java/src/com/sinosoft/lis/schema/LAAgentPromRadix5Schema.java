/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAAgentPromRadix5DB;

/*
 * <p>ClassName: LAAgentPromRadix5Schema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 互动渠道考核标准录入
 * @CreateDate：2015-01-13
 */
public class LAAgentPromRadix5Schema implements Schema, Cloneable
{
	// @Field
	/** 管理机构 */
	private String ManageCom;
	/** 代理人职级 */
	private String AgentGrade;
	/** 展业类型 */
	private String BranchType;
	/** 目标代理人职级 */
	private String DestAgentGrade;
	/** 版本号 */
	private String AreaType;
	/** 考核方式 */
	private String AssessType;
	/** 晋升标准 */
	private double UpStandPrem;
	/** 维持标准 */
	private double KeepStandPrem;
	/** 渠道 */
	private String BranchType2;
	/** 状态 */
	private String AssessState;
	/** 是否为考核标准 */
	private String StandFlag;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAAgentPromRadix5Schema()
	{
		mErrors = new CErrors();

		String[] pk = new String[5];
		pk[0] = "ManageCom";
		pk[1] = "AgentGrade";
		pk[2] = "BranchType";
		pk[3] = "AssessType";
		pk[4] = "BranchType2";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAAgentPromRadix5Schema cloned = (LAAgentPromRadix5Schema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getAgentGrade()
	{
		return AgentGrade;
	}
	public void setAgentGrade(String aAgentGrade)
	{
		AgentGrade = aAgentGrade;
	}
	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getDestAgentGrade()
	{
		return DestAgentGrade;
	}
	public void setDestAgentGrade(String aDestAgentGrade)
	{
		DestAgentGrade = aDestAgentGrade;
	}
	public String getAreaType()
	{
		return AreaType;
	}
	public void setAreaType(String aAreaType)
	{
		AreaType = aAreaType;
	}
	public String getAssessType()
	{
		return AssessType;
	}
	public void setAssessType(String aAssessType)
	{
		AssessType = aAssessType;
	}
	public double getUpStandPrem()
	{
		return UpStandPrem;
	}
	public void setUpStandPrem(double aUpStandPrem)
	{
		UpStandPrem = Arith.round(aUpStandPrem,2);
	}
	public void setUpStandPrem(String aUpStandPrem)
	{
		if (aUpStandPrem != null && !aUpStandPrem.equals(""))
		{
			Double tDouble = new Double(aUpStandPrem);
			double d = tDouble.doubleValue();
                UpStandPrem = Arith.round(d,2);
		}
	}

	public double getKeepStandPrem()
	{
		return KeepStandPrem;
	}
	public void setKeepStandPrem(double aKeepStandPrem)
	{
		KeepStandPrem = Arith.round(aKeepStandPrem,2);
	}
	public void setKeepStandPrem(String aKeepStandPrem)
	{
		if (aKeepStandPrem != null && !aKeepStandPrem.equals(""))
		{
			Double tDouble = new Double(aKeepStandPrem);
			double d = tDouble.doubleValue();
                KeepStandPrem = Arith.round(d,2);
		}
	}

	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getAssessState()
	{
		return AssessState;
	}
	public void setAssessState(String aAssessState)
	{
		AssessState = aAssessState;
	}
	public String getStandFlag()
	{
		return StandFlag;
	}
	public void setStandFlag(String aStandFlag)
	{
		StandFlag = aStandFlag;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LAAgentPromRadix5Schema 对象给 Schema 赋值
	* @param: aLAAgentPromRadix5Schema LAAgentPromRadix5Schema
	**/
	public void setSchema(LAAgentPromRadix5Schema aLAAgentPromRadix5Schema)
	{
		this.ManageCom = aLAAgentPromRadix5Schema.getManageCom();
		this.AgentGrade = aLAAgentPromRadix5Schema.getAgentGrade();
		this.BranchType = aLAAgentPromRadix5Schema.getBranchType();
		this.DestAgentGrade = aLAAgentPromRadix5Schema.getDestAgentGrade();
		this.AreaType = aLAAgentPromRadix5Schema.getAreaType();
		this.AssessType = aLAAgentPromRadix5Schema.getAssessType();
		this.UpStandPrem = aLAAgentPromRadix5Schema.getUpStandPrem();
		this.KeepStandPrem = aLAAgentPromRadix5Schema.getKeepStandPrem();
		this.BranchType2 = aLAAgentPromRadix5Schema.getBranchType2();
		this.AssessState = aLAAgentPromRadix5Schema.getAssessState();
		this.StandFlag = aLAAgentPromRadix5Schema.getStandFlag();
		this.Operator = aLAAgentPromRadix5Schema.getOperator();
		this.MakeDate = fDate.getDate( aLAAgentPromRadix5Schema.getMakeDate());
		this.MakeTime = aLAAgentPromRadix5Schema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAAgentPromRadix5Schema.getModifyDate());
		this.ModifyTime = aLAAgentPromRadix5Schema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("AgentGrade") == null )
				this.AgentGrade = null;
			else
				this.AgentGrade = rs.getString("AgentGrade").trim();

			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("DestAgentGrade") == null )
				this.DestAgentGrade = null;
			else
				this.DestAgentGrade = rs.getString("DestAgentGrade").trim();

			if( rs.getString("AreaType") == null )
				this.AreaType = null;
			else
				this.AreaType = rs.getString("AreaType").trim();

			if( rs.getString("AssessType") == null )
				this.AssessType = null;
			else
				this.AssessType = rs.getString("AssessType").trim();

			this.UpStandPrem = rs.getDouble("UpStandPrem");
			this.KeepStandPrem = rs.getDouble("KeepStandPrem");
			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("AssessState") == null )
				this.AssessState = null;
			else
				this.AssessState = rs.getString("AssessState").trim();

			if( rs.getString("StandFlag") == null )
				this.StandFlag = null;
			else
				this.StandFlag = rs.getString("StandFlag").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAAgentPromRadix5表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAgentPromRadix5Schema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAAgentPromRadix5Schema getSchema()
	{
		LAAgentPromRadix5Schema aLAAgentPromRadix5Schema = new LAAgentPromRadix5Schema();
		aLAAgentPromRadix5Schema.setSchema(this);
		return aLAAgentPromRadix5Schema;
	}

	public LAAgentPromRadix5DB getDB()
	{
		LAAgentPromRadix5DB aDBOper = new LAAgentPromRadix5DB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentPromRadix5描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DestAgentGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AreaType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AssessType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(UpStandPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(KeepStandPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AssessState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentPromRadix5>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DestAgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AssessType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			UpStandPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			KeepStandPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AssessState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			StandFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAgentPromRadix5Schema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("AgentGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("DestAgentGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DestAgentGrade));
		}
		if (FCode.equals("AreaType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
		}
		if (FCode.equals("AssessType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AssessType));
		}
		if (FCode.equals("UpStandPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UpStandPrem));
		}
		if (FCode.equals("KeepStandPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(KeepStandPrem));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("AssessState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AssessState));
		}
		if (FCode.equals("StandFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandFlag));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(AgentGrade);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(DestAgentGrade);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(AreaType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AssessType);
				break;
			case 6:
				strFieldValue = String.valueOf(UpStandPrem);
				break;
			case 7:
				strFieldValue = String.valueOf(KeepStandPrem);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AssessState);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(StandFlag);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("AgentGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGrade = FValue.trim();
			}
			else
				AgentGrade = null;
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("DestAgentGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DestAgentGrade = FValue.trim();
			}
			else
				DestAgentGrade = null;
		}
		if (FCode.equalsIgnoreCase("AreaType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaType = FValue.trim();
			}
			else
				AreaType = null;
		}
		if (FCode.equalsIgnoreCase("AssessType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AssessType = FValue.trim();
			}
			else
				AssessType = null;
		}
		if (FCode.equalsIgnoreCase("UpStandPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				UpStandPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("KeepStandPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				KeepStandPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("AssessState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AssessState = FValue.trim();
			}
			else
				AssessState = null;
		}
		if (FCode.equalsIgnoreCase("StandFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandFlag = FValue.trim();
			}
			else
				StandFlag = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAAgentPromRadix5Schema other = (LAAgentPromRadix5Schema)otherObject;
		return
			(ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (AgentGrade == null ? other.getAgentGrade() == null : AgentGrade.equals(other.getAgentGrade()))
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (DestAgentGrade == null ? other.getDestAgentGrade() == null : DestAgentGrade.equals(other.getDestAgentGrade()))
			&& (AreaType == null ? other.getAreaType() == null : AreaType.equals(other.getAreaType()))
			&& (AssessType == null ? other.getAssessType() == null : AssessType.equals(other.getAssessType()))
			&& UpStandPrem == other.getUpStandPrem()
			&& KeepStandPrem == other.getKeepStandPrem()
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (AssessState == null ? other.getAssessState() == null : AssessState.equals(other.getAssessState()))
			&& (StandFlag == null ? other.getStandFlag() == null : StandFlag.equals(other.getStandFlag()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ManageCom") ) {
			return 0;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return 1;
		}
		if( strFieldName.equals("BranchType") ) {
			return 2;
		}
		if( strFieldName.equals("DestAgentGrade") ) {
			return 3;
		}
		if( strFieldName.equals("AreaType") ) {
			return 4;
		}
		if( strFieldName.equals("AssessType") ) {
			return 5;
		}
		if( strFieldName.equals("UpStandPrem") ) {
			return 6;
		}
		if( strFieldName.equals("KeepStandPrem") ) {
			return 7;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 8;
		}
		if( strFieldName.equals("AssessState") ) {
			return 9;
		}
		if( strFieldName.equals("StandFlag") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ManageCom";
				break;
			case 1:
				strFieldName = "AgentGrade";
				break;
			case 2:
				strFieldName = "BranchType";
				break;
			case 3:
				strFieldName = "DestAgentGrade";
				break;
			case 4:
				strFieldName = "AreaType";
				break;
			case 5:
				strFieldName = "AssessType";
				break;
			case 6:
				strFieldName = "UpStandPrem";
				break;
			case 7:
				strFieldName = "KeepStandPrem";
				break;
			case 8:
				strFieldName = "BranchType2";
				break;
			case 9:
				strFieldName = "AssessState";
				break;
			case 10:
				strFieldName = "StandFlag";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DestAgentGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AreaType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AssessType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UpStandPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("KeepStandPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AssessState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
