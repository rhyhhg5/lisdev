/*
 * <p>ClassName: LGWorkTypeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 工单管理
 * @CreateDate：2005-01-29
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LGWorkTypeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LGWorkTypeSchema implements Schema
{
    // @Field
    /** 作业类型编号 */
    private String WorkTypeNo;
    /** 作业类型名称 */
    private String WorkTypeName;
    /** 上级分类编号 */
    private String SuperTypeNo;
    /** 是否关联经办 */
    private String IsDeal;
    /** 经办系统路径 */
    private String DealPath;
    /** 完成时限 */
    private String DateLimit;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LGWorkTypeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "WorkTypeNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getWorkTypeNo()
    {
        if (WorkTypeNo != null && !WorkTypeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            WorkTypeNo = StrTool.unicodeToGBK(WorkTypeNo);
        }
        return WorkTypeNo;
    }

    public void setWorkTypeNo(String aWorkTypeNo)
    {
        WorkTypeNo = aWorkTypeNo;
    }

    public String getWorkTypeName()
    {
        if (WorkTypeName != null && !WorkTypeName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            WorkTypeName = StrTool.unicodeToGBK(WorkTypeName);
        }
        return WorkTypeName;
    }

    public void setWorkTypeName(String aWorkTypeName)
    {
        WorkTypeName = aWorkTypeName;
    }

    public String getSuperTypeNo()
    {
        if (SuperTypeNo != null && !SuperTypeNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SuperTypeNo = StrTool.unicodeToGBK(SuperTypeNo);
        }
        return SuperTypeNo;
    }

    public void setSuperTypeNo(String aSuperTypeNo)
    {
        SuperTypeNo = aSuperTypeNo;
    }

    public String getIsDeal()
    {
        if (IsDeal != null && !IsDeal.equals("") && SysConst.CHANGECHARSET == true)
        {
            IsDeal = StrTool.unicodeToGBK(IsDeal);
        }
        return IsDeal;
    }

    public void setIsDeal(String aIsDeal)
    {
        IsDeal = aIsDeal;
    }

    public String getDealPath()
    {
        if (DealPath != null && !DealPath.equals("") && SysConst.CHANGECHARSET == true)
        {
            DealPath = StrTool.unicodeToGBK(DealPath);
        }
        return DealPath;
    }

    public void setDealPath(String aDealPath)
    {
        DealPath = aDealPath;
    }

    public String getDateLimit()
    {
        if (DateLimit != null && !DateLimit.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            DateLimit = StrTool.unicodeToGBK(DateLimit);
        }
        return DateLimit;
    }

    public void setDateLimit(String aDateLimit)
    {
        DateLimit = aDateLimit;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LGWorkTypeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LGWorkTypeSchema aLGWorkTypeSchema)
    {
        this.WorkTypeNo = aLGWorkTypeSchema.getWorkTypeNo();
        this.WorkTypeName = aLGWorkTypeSchema.getWorkTypeName();
        this.SuperTypeNo = aLGWorkTypeSchema.getSuperTypeNo();
        this.IsDeal = aLGWorkTypeSchema.getIsDeal();
        this.DealPath = aLGWorkTypeSchema.getDealPath();
        this.DateLimit = aLGWorkTypeSchema.getDateLimit();
        this.Operator = aLGWorkTypeSchema.getOperator();
        this.MakeDate = fDate.getDate(aLGWorkTypeSchema.getMakeDate());
        this.MakeTime = aLGWorkTypeSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLGWorkTypeSchema.getModifyDate());
        this.ModifyTime = aLGWorkTypeSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("WorkTypeNo") == null)
            {
                this.WorkTypeNo = null;
            }
            else
            {
                this.WorkTypeNo = rs.getString("WorkTypeNo").trim();
            }

            if (rs.getString("WorkTypeName") == null)
            {
                this.WorkTypeName = null;
            }
            else
            {
                this.WorkTypeName = rs.getString("WorkTypeName").trim();
            }

            if (rs.getString("SuperTypeNo") == null)
            {
                this.SuperTypeNo = null;
            }
            else
            {
                this.SuperTypeNo = rs.getString("SuperTypeNo").trim();
            }

            if (rs.getString("IsDeal") == null)
            {
                this.IsDeal = null;
            }
            else
            {
                this.IsDeal = rs.getString("IsDeal").trim();
            }

            if (rs.getString("DealPath") == null)
            {
                this.DealPath = null;
            }
            else
            {
                this.DealPath = rs.getString("DealPath").trim();
            }

            if (rs.getString("DateLimit") == null)
            {
                this.DateLimit = null;
            }
            else
            {
                this.DateLimit = rs.getString("DateLimit").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGWorkTypeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LGWorkTypeSchema getSchema()
    {
        LGWorkTypeSchema aLGWorkTypeSchema = new LGWorkTypeSchema();
        aLGWorkTypeSchema.setSchema(this);
        return aLGWorkTypeSchema;
    }

    public LGWorkTypeDB getDB()
    {
        LGWorkTypeDB aDBOper = new LGWorkTypeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGWorkType描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(WorkTypeNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(WorkTypeName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SuperTypeNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IsDeal)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DealPath)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DateLimit)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGWorkType>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            WorkTypeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            WorkTypeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            SuperTypeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
            IsDeal = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            DealPath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            DateLimit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LGWorkTypeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("WorkTypeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WorkTypeNo));
        }
        if (FCode.equals("WorkTypeName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(WorkTypeName));
        }
        if (FCode.equals("SuperTypeNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SuperTypeNo));
        }
        if (FCode.equals("IsDeal"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(IsDeal));
        }
        if (FCode.equals("DealPath"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DealPath));
        }
        if (FCode.equals("DateLimit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DateLimit));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(WorkTypeNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(WorkTypeName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SuperTypeNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(IsDeal);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(DealPath);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(DateLimit);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("WorkTypeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WorkTypeNo = FValue.trim();
            }
            else
            {
                WorkTypeNo = null;
            }
        }
        if (FCode.equals("WorkTypeName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WorkTypeName = FValue.trim();
            }
            else
            {
                WorkTypeName = null;
            }
        }
        if (FCode.equals("SuperTypeNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SuperTypeNo = FValue.trim();
            }
            else
            {
                SuperTypeNo = null;
            }
        }
        if (FCode.equals("IsDeal"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IsDeal = FValue.trim();
            }
            else
            {
                IsDeal = null;
            }
        }
        if (FCode.equals("DealPath"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DealPath = FValue.trim();
            }
            else
            {
                DealPath = null;
            }
        }
        if (FCode.equals("DateLimit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DateLimit = FValue.trim();
            }
            else
            {
                DateLimit = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LGWorkTypeSchema other = (LGWorkTypeSchema) otherObject;
        return
                WorkTypeNo.equals(other.getWorkTypeNo())
                && WorkTypeName.equals(other.getWorkTypeName())
                && SuperTypeNo.equals(other.getSuperTypeNo())
                && IsDeal.equals(other.getIsDeal())
                && DealPath.equals(other.getDealPath())
                && DateLimit.equals(other.getDateLimit())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("WorkTypeNo"))
        {
            return 0;
        }
        if (strFieldName.equals("WorkTypeName"))
        {
            return 1;
        }
        if (strFieldName.equals("SuperTypeNo"))
        {
            return 2;
        }
        if (strFieldName.equals("IsDeal"))
        {
            return 3;
        }
        if (strFieldName.equals("DealPath"))
        {
            return 4;
        }
        if (strFieldName.equals("DateLimit"))
        {
            return 5;
        }
        if (strFieldName.equals("Operator"))
        {
            return 6;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 7;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 8;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 9;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "WorkTypeNo";
                break;
            case 1:
                strFieldName = "WorkTypeName";
                break;
            case 2:
                strFieldName = "SuperTypeNo";
                break;
            case 3:
                strFieldName = "IsDeal";
                break;
            case 4:
                strFieldName = "DealPath";
                break;
            case 5:
                strFieldName = "DateLimit";
                break;
            case 6:
                strFieldName = "Operator";
                break;
            case 7:
                strFieldName = "MakeDate";
                break;
            case 8:
                strFieldName = "MakeTime";
                break;
            case 9:
                strFieldName = "ModifyDate";
                break;
            case 10:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("WorkTypeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WorkTypeName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SuperTypeNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IsDeal"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DealPath"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DateLimit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
