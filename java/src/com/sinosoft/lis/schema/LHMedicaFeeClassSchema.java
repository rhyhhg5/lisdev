/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LHMedicaFeeClassDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LHMedicaFeeClassSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_3
 * @CreateDate：2005-03-03
 */
public class LHMedicaFeeClassSchema implements Schema
{
    // @Field
    /** 医疗费用类别代码 */
    private String MedicaFeeCode;
    /** 医疗费用类别 */
    private String MedicaFeeName;

    public static final int FIELDNUM = 2; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHMedicaFeeClassSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "MedicaFeeCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getMedicaFeeCode()
    {
        if (MedicaFeeCode != null && !MedicaFeeCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            MedicaFeeCode = StrTool.unicodeToGBK(MedicaFeeCode);
        }
        return MedicaFeeCode;
    }

    public void setMedicaFeeCode(String aMedicaFeeCode)
    {
        MedicaFeeCode = aMedicaFeeCode;
    }

    public String getMedicaFeeName()
    {
        if (MedicaFeeName != null && !MedicaFeeName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            MedicaFeeName = StrTool.unicodeToGBK(MedicaFeeName);
        }
        return MedicaFeeName;
    }

    public void setMedicaFeeName(String aMedicaFeeName)
    {
        MedicaFeeName = aMedicaFeeName;
    }

    /**
     * 使用另外一个 LHMedicaFeeClassSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LHMedicaFeeClassSchema aLHMedicaFeeClassSchema)
    {
        this.MedicaFeeCode = aLHMedicaFeeClassSchema.getMedicaFeeCode();
        this.MedicaFeeName = aLHMedicaFeeClassSchema.getMedicaFeeName();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("MedicaFeeCode") == null)
            {
                this.MedicaFeeCode = null;
            }
            else
            {
                this.MedicaFeeCode = rs.getString("MedicaFeeCode").trim();
            }

            if (rs.getString("MedicaFeeName") == null)
            {
                this.MedicaFeeName = null;
            }
            else
            {
                this.MedicaFeeName = rs.getString("MedicaFeeName").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHMedicaFeeClassSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LHMedicaFeeClassSchema getSchema()
    {
        LHMedicaFeeClassSchema aLHMedicaFeeClassSchema = new
                LHMedicaFeeClassSchema();
        aLHMedicaFeeClassSchema.setSchema(this);
        return aLHMedicaFeeClassSchema;
    }

    public LHMedicaFeeClassDB getDB()
    {
        LHMedicaFeeClassDB aDBOper = new LHMedicaFeeClassDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHMedicaFeeClass描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(MedicaFeeCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MedicaFeeName));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHMedicaFeeClass>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            MedicaFeeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            MedicaFeeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                           SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHMedicaFeeClassSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("MedicaFeeCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MedicaFeeCode));
        }
        if (FCode.equals("MedicaFeeName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MedicaFeeName));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(MedicaFeeCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(MedicaFeeName);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("MedicaFeeCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MedicaFeeCode = FValue.trim();
            }
            else
            {
                MedicaFeeCode = null;
            }
        }
        if (FCode.equals("MedicaFeeName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MedicaFeeName = FValue.trim();
            }
            else
            {
                MedicaFeeName = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LHMedicaFeeClassSchema other = (LHMedicaFeeClassSchema) otherObject;
        return
                MedicaFeeCode.equals(other.getMedicaFeeCode())
                && MedicaFeeName.equals(other.getMedicaFeeName());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("MedicaFeeCode"))
        {
            return 0;
        }
        if (strFieldName.equals("MedicaFeeName"))
        {
            return 1;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "MedicaFeeCode";
                break;
            case 1:
                strFieldName = "MedicaFeeName";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("MedicaFeeCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MedicaFeeName"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
