/*
 * <p>ClassName: LFOutDataSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2005-01-10
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LFOutDataDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LFOutDataSchema implements Schema
{
    // @Field
    /** 数据来源 */
    private String SourceCode;
    /** 外部科目编码 */
    private String OutItemCode;
    /** 机构代码 */
    private String ManageCom;
    /** 数值 */
    private double Value;
    /** 业务日期 */
    private Date BussDate;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LFOutDataSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[0];

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSourceCode()
    {
        if (SourceCode != null && !SourceCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SourceCode = StrTool.unicodeToGBK(SourceCode);
        }
        return SourceCode;
    }

    public void setSourceCode(String aSourceCode)
    {
        SourceCode = aSourceCode;
    }

    public String getOutItemCode()
    {
        if (OutItemCode != null && !OutItemCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OutItemCode = StrTool.unicodeToGBK(OutItemCode);
        }
        return OutItemCode;
    }

    public void setOutItemCode(String aOutItemCode)
    {
        OutItemCode = aOutItemCode;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public double getValue()
    {
        return Value;
    }

    public void setValue(double aValue)
    {
        Value = aValue;
    }

    public void setValue(String aValue)
    {
        if (aValue != null && !aValue.equals(""))
        {
            Double tDouble = new Double(aValue);
            double d = tDouble.doubleValue();
            Value = d;
        }
    }

    public String getBussDate()
    {
        if (BussDate != null)
        {
            return fDate.getString(BussDate);
        }
        else
        {
            return null;
        }
    }

    public void setBussDate(Date aBussDate)
    {
        BussDate = aBussDate;
    }

    public void setBussDate(String aBussDate)
    {
        if (aBussDate != null && !aBussDate.equals(""))
        {
            BussDate = fDate.getDate(aBussDate);
        }
        else
        {
            BussDate = null;
        }
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LFOutDataSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LFOutDataSchema aLFOutDataSchema)
    {
        this.SourceCode = aLFOutDataSchema.getSourceCode();
        this.OutItemCode = aLFOutDataSchema.getOutItemCode();
        this.ManageCom = aLFOutDataSchema.getManageCom();
        this.Value = aLFOutDataSchema.getValue();
        this.BussDate = fDate.getDate(aLFOutDataSchema.getBussDate());
        this.MakeDate = fDate.getDate(aLFOutDataSchema.getMakeDate());
        this.MakeTime = aLFOutDataSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SourceCode") == null)
            {
                this.SourceCode = null;
            }
            else
            {
                this.SourceCode = rs.getString("SourceCode").trim();
            }

            if (rs.getString("OutItemCode") == null)
            {
                this.OutItemCode = null;
            }
            else
            {
                this.OutItemCode = rs.getString("OutItemCode").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            this.Value = rs.getDouble("Value");
            this.BussDate = rs.getDate("BussDate");
            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFOutDataSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LFOutDataSchema getSchema()
    {
        LFOutDataSchema aLFOutDataSchema = new LFOutDataSchema();
        aLFOutDataSchema.setSchema(this);
        return aLFOutDataSchema;
    }

    public LFOutDataDB getDB()
    {
        LFOutDataDB aDBOper = new LFOutDataDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFOutData描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SourceCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OutItemCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Value) + SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(BussDate))) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLFOutData>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SourceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            OutItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            Value = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    4, SysConst.PACKAGESPILTER))).doubleValue();
            BussDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFOutDataSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SourceCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SourceCode));
        }
        if (FCode.equals("OutItemCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OutItemCode));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ManageCom));
        }
        if (FCode.equals("Value"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Value));
        }
        if (FCode.equals("BussDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getBussDate()));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SourceCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(OutItemCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 3:
                strFieldValue = String.valueOf(Value);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getBussDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SourceCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SourceCode = FValue.trim();
            }
            else
            {
                SourceCode = null;
            }
        }
        if (FCode.equals("OutItemCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OutItemCode = FValue.trim();
            }
            else
            {
                OutItemCode = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("Value"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Value = d;
            }
        }
        if (FCode.equals("BussDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BussDate = fDate.getDate(FValue);
            }
            else
            {
                BussDate = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LFOutDataSchema other = (LFOutDataSchema) otherObject;
        return
                SourceCode.equals(other.getSourceCode())
                && OutItemCode.equals(other.getOutItemCode())
                && ManageCom.equals(other.getManageCom())
                && Value == other.getValue()
                && fDate.getString(BussDate).equals(other.getBussDate())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SourceCode"))
        {
            return 0;
        }
        if (strFieldName.equals("OutItemCode"))
        {
            return 1;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 2;
        }
        if (strFieldName.equals("Value"))
        {
            return 3;
        }
        if (strFieldName.equals("BussDate"))
        {
            return 4;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SourceCode";
                break;
            case 1:
                strFieldName = "OutItemCode";
                break;
            case 2:
                strFieldName = "ManageCom";
                break;
            case 3:
                strFieldName = "Value";
                break;
            case 4:
                strFieldName = "BussDate";
                break;
            case 5:
                strFieldName = "MakeDate";
                break;
            case 6:
                strFieldName = "MakeTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SourceCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OutItemCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Value"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("BussDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
