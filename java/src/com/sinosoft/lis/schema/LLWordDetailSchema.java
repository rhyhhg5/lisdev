/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLWordDetailDB;

/*
 * <p>ClassName: LLWordDetailSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 政策标题记录表
 * @CreateDate：2019-08-12
 */
public class LLWordDetailSchema implements Schema, Cloneable
{
	// @Field
	/** 流水号 */
	private String SerialNo;
	/** 政策标题 */
	private String DocName;
	/** 上载日期 */
	private Date UploadDate;
	/** 上载时间 */
	private String UploadTime;
	/** 公司代码 */
	private String CompanyCode;
	/** 公司名称 */
	private String CompanyName;
	/** 操作员 */
	private String Operater;
	/** 管理机构 */
	private String MngCom;
	/** 存储路径 */
	private String WordPath;
	/** 备注1 */
	private String remark1;
	/** 备注2 */
	private String remark2;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 15;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLWordDetailSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "SerialNo";
		pk[1] = "DocName";
		pk[2] = "CompanyCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLWordDetailSchema cloned = (LLWordDetailSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getSerialNo()
	{
		return SerialNo;
	}
	public void setSerialNo(String aSerialNo)
	{
		SerialNo = aSerialNo;
	}
	public String getDocName()
	{
		return DocName;
	}
	public void setDocName(String aDocName)
	{
		DocName = aDocName;
	}
	public String getUploadDate()
	{
		if( UploadDate != null )
			return fDate.getString(UploadDate);
		else
			return null;
	}
	public void setUploadDate(Date aUploadDate)
	{
		UploadDate = aUploadDate;
	}
	public void setUploadDate(String aUploadDate)
	{
		if (aUploadDate != null && !aUploadDate.equals("") )
		{
			UploadDate = fDate.getDate( aUploadDate );
		}
		else
			UploadDate = null;
	}

	public String getUploadTime()
	{
		return UploadTime;
	}
	public void setUploadTime(String aUploadTime)
	{
		UploadTime = aUploadTime;
	}
	public String getCompanyCode()
	{
		return CompanyCode;
	}
	public void setCompanyCode(String aCompanyCode)
	{
		CompanyCode = aCompanyCode;
	}
	public String getCompanyName()
	{
		return CompanyName;
	}
	public void setCompanyName(String aCompanyName)
	{
		CompanyName = aCompanyName;
	}
	public String getOperater()
	{
		return Operater;
	}
	public void setOperater(String aOperater)
	{
		Operater = aOperater;
	}
	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getWordPath()
	{
		return WordPath;
	}
	public void setWordPath(String aWordPath)
	{
		WordPath = aWordPath;
	}
	public String getremark1()
	{
		return remark1;
	}
	public void setremark1(String aremark1)
	{
		remark1 = aremark1;
	}
	public String getremark2()
	{
		return remark2;
	}
	public void setremark2(String aremark2)
	{
		remark2 = aremark2;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LLWordDetailSchema 对象给 Schema 赋值
	* @param: aLLWordDetailSchema LLWordDetailSchema
	**/
	public void setSchema(LLWordDetailSchema aLLWordDetailSchema)
	{
		this.SerialNo = aLLWordDetailSchema.getSerialNo();
		this.DocName = aLLWordDetailSchema.getDocName();
		this.UploadDate = fDate.getDate( aLLWordDetailSchema.getUploadDate());
		this.UploadTime = aLLWordDetailSchema.getUploadTime();
		this.CompanyCode = aLLWordDetailSchema.getCompanyCode();
		this.CompanyName = aLLWordDetailSchema.getCompanyName();
		this.Operater = aLLWordDetailSchema.getOperater();
		this.MngCom = aLLWordDetailSchema.getMngCom();
		this.WordPath = aLLWordDetailSchema.getWordPath();
		this.remark1 = aLLWordDetailSchema.getremark1();
		this.remark2 = aLLWordDetailSchema.getremark2();
		this.MakeDate = fDate.getDate( aLLWordDetailSchema.getMakeDate());
		this.MakeTime = aLLWordDetailSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLWordDetailSchema.getModifyDate());
		this.ModifyTime = aLLWordDetailSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("SerialNo") == null )
				this.SerialNo = null;
			else
				this.SerialNo = rs.getString("SerialNo").trim();

			if( rs.getString("DocName") == null )
				this.DocName = null;
			else
				this.DocName = rs.getString("DocName").trim();

			this.UploadDate = rs.getDate("UploadDate");
			if( rs.getString("UploadTime") == null )
				this.UploadTime = null;
			else
				this.UploadTime = rs.getString("UploadTime").trim();

			if( rs.getString("CompanyCode") == null )
				this.CompanyCode = null;
			else
				this.CompanyCode = rs.getString("CompanyCode").trim();

			if( rs.getString("CompanyName") == null )
				this.CompanyName = null;
			else
				this.CompanyName = rs.getString("CompanyName").trim();

			if( rs.getString("Operater") == null )
				this.Operater = null;
			else
				this.Operater = rs.getString("Operater").trim();

			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("WordPath") == null )
				this.WordPath = null;
			else
				this.WordPath = rs.getString("WordPath").trim();

			if( rs.getString("remark1") == null )
				this.remark1 = null;
			else
				this.remark1 = rs.getString("remark1").trim();

			if( rs.getString("remark2") == null )
				this.remark2 = null;
			else
				this.remark2 = rs.getString("remark2").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLWordDetail表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLWordDetailSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLWordDetailSchema getSchema()
	{
		LLWordDetailSchema aLLWordDetailSchema = new LLWordDetailSchema();
		aLLWordDetailSchema.setSchema(this);
		return aLLWordDetailSchema;
	}

	public LLWordDetailDB getDB()
	{
		LLWordDetailDB aDBOper = new LLWordDetailDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLWordDetail描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(SerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DocName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( UploadDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UploadTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CompanyName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operater)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WordPath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(remark1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(remark2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLWordDetail>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			DocName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			UploadDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			UploadTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CompanyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CompanyName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Operater = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			WordPath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			remark1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			remark2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLWordDetailSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("SerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
		}
		if (FCode.equals("DocName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DocName));
		}
		if (FCode.equals("UploadDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUploadDate()));
		}
		if (FCode.equals("UploadTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UploadTime));
		}
		if (FCode.equals("CompanyCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyCode));
		}
		if (FCode.equals("CompanyName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CompanyName));
		}
		if (FCode.equals("Operater"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operater));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("WordPath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WordPath));
		}
		if (FCode.equals("remark1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(remark1));
		}
		if (FCode.equals("remark2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(remark2));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(SerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(DocName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUploadDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(UploadTime);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CompanyCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CompanyName);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Operater);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(WordPath);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(remark1);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(remark2);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("SerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SerialNo = FValue.trim();
			}
			else
				SerialNo = null;
		}
		if (FCode.equalsIgnoreCase("DocName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DocName = FValue.trim();
			}
			else
				DocName = null;
		}
		if (FCode.equalsIgnoreCase("UploadDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				UploadDate = fDate.getDate( FValue );
			}
			else
				UploadDate = null;
		}
		if (FCode.equalsIgnoreCase("UploadTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UploadTime = FValue.trim();
			}
			else
				UploadTime = null;
		}
		if (FCode.equalsIgnoreCase("CompanyCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyCode = FValue.trim();
			}
			else
				CompanyCode = null;
		}
		if (FCode.equalsIgnoreCase("CompanyName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CompanyName = FValue.trim();
			}
			else
				CompanyName = null;
		}
		if (FCode.equalsIgnoreCase("Operater"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operater = FValue.trim();
			}
			else
				Operater = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("WordPath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WordPath = FValue.trim();
			}
			else
				WordPath = null;
		}
		if (FCode.equalsIgnoreCase("remark1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				remark1 = FValue.trim();
			}
			else
				remark1 = null;
		}
		if (FCode.equalsIgnoreCase("remark2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				remark2 = FValue.trim();
			}
			else
				remark2 = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLWordDetailSchema other = (LLWordDetailSchema)otherObject;
		return
			(SerialNo == null ? other.getSerialNo() == null : SerialNo.equals(other.getSerialNo()))
			&& (DocName == null ? other.getDocName() == null : DocName.equals(other.getDocName()))
			&& (UploadDate == null ? other.getUploadDate() == null : fDate.getString(UploadDate).equals(other.getUploadDate()))
			&& (UploadTime == null ? other.getUploadTime() == null : UploadTime.equals(other.getUploadTime()))
			&& (CompanyCode == null ? other.getCompanyCode() == null : CompanyCode.equals(other.getCompanyCode()))
			&& (CompanyName == null ? other.getCompanyName() == null : CompanyName.equals(other.getCompanyName()))
			&& (Operater == null ? other.getOperater() == null : Operater.equals(other.getOperater()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (WordPath == null ? other.getWordPath() == null : WordPath.equals(other.getWordPath()))
			&& (remark1 == null ? other.getremark1() == null : remark1.equals(other.getremark1()))
			&& (remark2 == null ? other.getremark2() == null : remark2.equals(other.getremark2()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("DocName") ) {
			return 1;
		}
		if( strFieldName.equals("UploadDate") ) {
			return 2;
		}
		if( strFieldName.equals("UploadTime") ) {
			return 3;
		}
		if( strFieldName.equals("CompanyCode") ) {
			return 4;
		}
		if( strFieldName.equals("CompanyName") ) {
			return 5;
		}
		if( strFieldName.equals("Operater") ) {
			return 6;
		}
		if( strFieldName.equals("MngCom") ) {
			return 7;
		}
		if( strFieldName.equals("WordPath") ) {
			return 8;
		}
		if( strFieldName.equals("remark1") ) {
			return 9;
		}
		if( strFieldName.equals("remark2") ) {
			return 10;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 14;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "SerialNo";
				break;
			case 1:
				strFieldName = "DocName";
				break;
			case 2:
				strFieldName = "UploadDate";
				break;
			case 3:
				strFieldName = "UploadTime";
				break;
			case 4:
				strFieldName = "CompanyCode";
				break;
			case 5:
				strFieldName = "CompanyName";
				break;
			case 6:
				strFieldName = "Operater";
				break;
			case 7:
				strFieldName = "MngCom";
				break;
			case 8:
				strFieldName = "WordPath";
				break;
			case 9:
				strFieldName = "remark1";
				break;
			case 10:
				strFieldName = "remark2";
				break;
			case 11:
				strFieldName = "MakeDate";
				break;
			case 12:
				strFieldName = "MakeTime";
				break;
			case 13:
				strFieldName = "ModifyDate";
				break;
			case 14:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("SerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DocName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UploadDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("UploadTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CompanyName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operater") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WordPath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("remark1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("remark2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
