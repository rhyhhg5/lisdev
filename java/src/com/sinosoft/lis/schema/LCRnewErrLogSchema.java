/*
 * <p>ClassName: LCRnewErrLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LCRnewErrLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LCRnewErrLogSchema implements Schema
{
    // @Field
    /** 错误号 */
    private int ErrNo;
    /** 印刷号 */
    private String PrtNo;
    /** 险种编码 */
    private String RiskCode;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 错误信息 */
    private String ErrInfo;
    /** 过程类别 */
    private String ProType;
    /** 操作员 */
    private String Operator;

    public static final int FIELDNUM = 8; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCRnewErrLogSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ErrNo";
        pk[1] = "PrtNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public int getErrNo()
    {
        return ErrNo;
    }

    public void setErrNo(int aErrNo)
    {
        ErrNo = aErrNo;
    }

    public void setErrNo(String aErrNo)
    {
        if (aErrNo != null && !aErrNo.equals(""))
        {
            Integer tInteger = new Integer(aErrNo);
            int i = tInteger.intValue();
            ErrNo = i;
        }
    }

    public String getPrtNo()
    {
        if (PrtNo != null && !PrtNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PrtNo = StrTool.unicodeToGBK(PrtNo);
        }
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo)
    {
        PrtNo = aPrtNo;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getErrInfo()
    {
        if (ErrInfo != null && !ErrInfo.equals("") && SysConst.CHANGECHARSET == true)
        {
            ErrInfo = StrTool.unicodeToGBK(ErrInfo);
        }
        return ErrInfo;
    }

    public void setErrInfo(String aErrInfo)
    {
        ErrInfo = aErrInfo;
    }

    public String getProType()
    {
        if (ProType != null && !ProType.equals("") && SysConst.CHANGECHARSET == true)
        {
            ProType = StrTool.unicodeToGBK(ProType);
        }
        return ProType;
    }

    public void setProType(String aProType)
    {
        ProType = aProType;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    /**
     * 使用另外一个 LCRnewErrLogSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LCRnewErrLogSchema aLCRnewErrLogSchema)
    {
        this.ErrNo = aLCRnewErrLogSchema.getErrNo();
        this.PrtNo = aLCRnewErrLogSchema.getPrtNo();
        this.RiskCode = aLCRnewErrLogSchema.getRiskCode();
        this.MakeDate = fDate.getDate(aLCRnewErrLogSchema.getMakeDate());
        this.MakeTime = aLCRnewErrLogSchema.getMakeTime();
        this.ErrInfo = aLCRnewErrLogSchema.getErrInfo();
        this.ProType = aLCRnewErrLogSchema.getProType();
        this.Operator = aLCRnewErrLogSchema.getOperator();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            this.ErrNo = rs.getInt("ErrNo");
            if (rs.getString("PrtNo") == null)
            {
                this.PrtNo = null;
            }
            else
            {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            if (rs.getString("ErrInfo") == null)
            {
                this.ErrInfo = null;
            }
            else
            {
                this.ErrInfo = rs.getString("ErrInfo").trim();
            }

            if (rs.getString("ProType") == null)
            {
                this.ProType = null;
            }
            else
            {
                this.ProType = rs.getString("ProType").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCRnewErrLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LCRnewErrLogSchema getSchema()
    {
        LCRnewErrLogSchema aLCRnewErrLogSchema = new LCRnewErrLogSchema();
        aLCRnewErrLogSchema.setSchema(this);
        return aLCRnewErrLogSchema;
    }

    public LCRnewErrLogDB getDB()
    {
        LCRnewErrLogDB aDBOper = new LCRnewErrLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCRnewErrLog描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = ChgData.chgData(ErrNo) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PrtNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ErrInfo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ProType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCRnewErrLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ErrNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    1, SysConst.PACKAGESPILTER))).intValue();
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 4, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                      SysConst.PACKAGESPILTER);
            ErrInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            ProType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCRnewErrLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ErrNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrNo));
        }
        if (FCode.equals("PrtNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PrtNo));
        }
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ErrInfo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ErrInfo));
        }
        if (FCode.equals("ProType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ProType));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = String.valueOf(ErrNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PrtNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(ErrInfo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(ProType);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ErrNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ErrNo = i;
            }
        }
        if (FCode.equals("PrtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PrtNo = FValue.trim();
            }
            else
            {
                PrtNo = null;
            }
        }
        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ErrInfo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ErrInfo = FValue.trim();
            }
            else
            {
                ErrInfo = null;
            }
        }
        if (FCode.equals("ProType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ProType = FValue.trim();
            }
            else
            {
                ProType = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LCRnewErrLogSchema other = (LCRnewErrLogSchema) otherObject;
        return
                ErrNo == other.getErrNo()
                && PrtNo.equals(other.getPrtNo())
                && RiskCode.equals(other.getRiskCode())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && ErrInfo.equals(other.getErrInfo())
                && ProType.equals(other.getProType())
                && Operator.equals(other.getOperator());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ErrNo"))
        {
            return 0;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return 1;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return 2;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 3;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 4;
        }
        if (strFieldName.equals("ErrInfo"))
        {
            return 5;
        }
        if (strFieldName.equals("ProType"))
        {
            return 6;
        }
        if (strFieldName.equals("Operator"))
        {
            return 7;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ErrNo";
                break;
            case 1:
                strFieldName = "PrtNo";
                break;
            case 2:
                strFieldName = "RiskCode";
                break;
            case 3:
                strFieldName = "MakeDate";
                break;
            case 4:
                strFieldName = "MakeTime";
                break;
            case 5:
                strFieldName = "ErrInfo";
                break;
            case 6:
                strFieldName = "ProType";
                break;
            case 7:
                strFieldName = "Operator";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ErrNo"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("PrtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrInfo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_INT;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
