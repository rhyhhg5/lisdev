/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLCaseStateDateDB;

/*
 * <p>ClassName: LLCaseStateDateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新系统模型
 * @CreateDate：2012-06-21
 */
public class LLCaseStateDateSchema implements Schema, Cloneable
{
	// @Field
	/** 案件号 */
	private String CaseNo;
	/** 受理日期 */
	private Date RgtNo;
	/** 扫描日期 */
	private Date EsdocDate;
	/** 账单录入日期 */
	private Date FeemainDate;
	/** 检录日期 */
	private Date RollCallDate;
	/** 第一次审批日期 */
	private Date ApprovalDate1;
	/** 最后一次审批日期 */
	private Date ApprovalDate2;
	/** 审定日期 */
	private Date ValidationDate;
	/** 调查起期 */
	private Date StartSurveyDate;
	/** 调查止期 */
	private Date EndSurveyDate;
	/** 结案日期 */
	private Date EndCaseDate;
	/** 抽检日期 */
	private Date SamplingDate;
	/** 通知日期 */
	private Date NoticeDate;
	/** 给付日期 */
	private Date PaymentDate;
	/** 权限标记 */
	private String RightsFlag;
	/** 是否机构处理案件 */
	private String ComCaseFlag;
	/** 机构编码 */
	private String Managecom;
	/** 备用字段1 */
	private String StandByString1;
	/** 备用字段2 */
	private Date StandByDate;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLCaseStateDateSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "CaseNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLCaseStateDateSchema cloned = (LLCaseStateDateSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getCaseNo()
	{
		return CaseNo;
	}
	public void setCaseNo(String aCaseNo)
	{
		CaseNo = aCaseNo;
	}
	public String getRgtNo()
	{
		if( RgtNo != null )
			return fDate.getString(RgtNo);
		else
			return null;
	}
	public void setRgtNo(Date aRgtNo)
	{
		RgtNo = aRgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
		if (aRgtNo != null && !aRgtNo.equals("") )
		{
			RgtNo = fDate.getDate( aRgtNo );
		}
		else
			RgtNo = null;
	}

	public String getEsdocDate()
	{
		if( EsdocDate != null )
			return fDate.getString(EsdocDate);
		else
			return null;
	}
	public void setEsdocDate(Date aEsdocDate)
	{
		EsdocDate = aEsdocDate;
	}
	public void setEsdocDate(String aEsdocDate)
	{
		if (aEsdocDate != null && !aEsdocDate.equals("") )
		{
			EsdocDate = fDate.getDate( aEsdocDate );
		}
		else
			EsdocDate = null;
	}

	public String getFeemainDate()
	{
		if( FeemainDate != null )
			return fDate.getString(FeemainDate);
		else
			return null;
	}
	public void setFeemainDate(Date aFeemainDate)
	{
		FeemainDate = aFeemainDate;
	}
	public void setFeemainDate(String aFeemainDate)
	{
		if (aFeemainDate != null && !aFeemainDate.equals("") )
		{
			FeemainDate = fDate.getDate( aFeemainDate );
		}
		else
			FeemainDate = null;
	}

	public String getRollCallDate()
	{
		if( RollCallDate != null )
			return fDate.getString(RollCallDate);
		else
			return null;
	}
	public void setRollCallDate(Date aRollCallDate)
	{
		RollCallDate = aRollCallDate;
	}
	public void setRollCallDate(String aRollCallDate)
	{
		if (aRollCallDate != null && !aRollCallDate.equals("") )
		{
			RollCallDate = fDate.getDate( aRollCallDate );
		}
		else
			RollCallDate = null;
	}

	public String getApprovalDate1()
	{
		if( ApprovalDate1 != null )
			return fDate.getString(ApprovalDate1);
		else
			return null;
	}
	public void setApprovalDate1(Date aApprovalDate1)
	{
		ApprovalDate1 = aApprovalDate1;
	}
	public void setApprovalDate1(String aApprovalDate1)
	{
		if (aApprovalDate1 != null && !aApprovalDate1.equals("") )
		{
			ApprovalDate1 = fDate.getDate( aApprovalDate1 );
		}
		else
			ApprovalDate1 = null;
	}

	public String getApprovalDate2()
	{
		if( ApprovalDate2 != null )
			return fDate.getString(ApprovalDate2);
		else
			return null;
	}
	public void setApprovalDate2(Date aApprovalDate2)
	{
		ApprovalDate2 = aApprovalDate2;
	}
	public void setApprovalDate2(String aApprovalDate2)
	{
		if (aApprovalDate2 != null && !aApprovalDate2.equals("") )
		{
			ApprovalDate2 = fDate.getDate( aApprovalDate2 );
		}
		else
			ApprovalDate2 = null;
	}

	public String getValidationDate()
	{
		if( ValidationDate != null )
			return fDate.getString(ValidationDate);
		else
			return null;
	}
	public void setValidationDate(Date aValidationDate)
	{
		ValidationDate = aValidationDate;
	}
	public void setValidationDate(String aValidationDate)
	{
		if (aValidationDate != null && !aValidationDate.equals("") )
		{
			ValidationDate = fDate.getDate( aValidationDate );
		}
		else
			ValidationDate = null;
	}

	public String getStartSurveyDate()
	{
		if( StartSurveyDate != null )
			return fDate.getString(StartSurveyDate);
		else
			return null;
	}
	public void setStartSurveyDate(Date aStartSurveyDate)
	{
		StartSurveyDate = aStartSurveyDate;
	}
	public void setStartSurveyDate(String aStartSurveyDate)
	{
		if (aStartSurveyDate != null && !aStartSurveyDate.equals("") )
		{
			StartSurveyDate = fDate.getDate( aStartSurveyDate );
		}
		else
			StartSurveyDate = null;
	}

	public String getEndSurveyDate()
	{
		if( EndSurveyDate != null )
			return fDate.getString(EndSurveyDate);
		else
			return null;
	}
	public void setEndSurveyDate(Date aEndSurveyDate)
	{
		EndSurveyDate = aEndSurveyDate;
	}
	public void setEndSurveyDate(String aEndSurveyDate)
	{
		if (aEndSurveyDate != null && !aEndSurveyDate.equals("") )
		{
			EndSurveyDate = fDate.getDate( aEndSurveyDate );
		}
		else
			EndSurveyDate = null;
	}

	public String getEndCaseDate()
	{
		if( EndCaseDate != null )
			return fDate.getString(EndCaseDate);
		else
			return null;
	}
	public void setEndCaseDate(Date aEndCaseDate)
	{
		EndCaseDate = aEndCaseDate;
	}
	public void setEndCaseDate(String aEndCaseDate)
	{
		if (aEndCaseDate != null && !aEndCaseDate.equals("") )
		{
			EndCaseDate = fDate.getDate( aEndCaseDate );
		}
		else
			EndCaseDate = null;
	}

	public String getSamplingDate()
	{
		if( SamplingDate != null )
			return fDate.getString(SamplingDate);
		else
			return null;
	}
	public void setSamplingDate(Date aSamplingDate)
	{
		SamplingDate = aSamplingDate;
	}
	public void setSamplingDate(String aSamplingDate)
	{
		if (aSamplingDate != null && !aSamplingDate.equals("") )
		{
			SamplingDate = fDate.getDate( aSamplingDate );
		}
		else
			SamplingDate = null;
	}

	public String getNoticeDate()
	{
		if( NoticeDate != null )
			return fDate.getString(NoticeDate);
		else
			return null;
	}
	public void setNoticeDate(Date aNoticeDate)
	{
		NoticeDate = aNoticeDate;
	}
	public void setNoticeDate(String aNoticeDate)
	{
		if (aNoticeDate != null && !aNoticeDate.equals("") )
		{
			NoticeDate = fDate.getDate( aNoticeDate );
		}
		else
			NoticeDate = null;
	}

	public String getPaymentDate()
	{
		if( PaymentDate != null )
			return fDate.getString(PaymentDate);
		else
			return null;
	}
	public void setPaymentDate(Date aPaymentDate)
	{
		PaymentDate = aPaymentDate;
	}
	public void setPaymentDate(String aPaymentDate)
	{
		if (aPaymentDate != null && !aPaymentDate.equals("") )
		{
			PaymentDate = fDate.getDate( aPaymentDate );
		}
		else
			PaymentDate = null;
	}

	public String getRightsFlag()
	{
		return RightsFlag;
	}
	public void setRightsFlag(String aRightsFlag)
	{
		RightsFlag = aRightsFlag;
	}
	public String getComCaseFlag()
	{
		return ComCaseFlag;
	}
	public void setComCaseFlag(String aComCaseFlag)
	{
		ComCaseFlag = aComCaseFlag;
	}
	public String getManagecom()
	{
		return Managecom;
	}
	public void setManagecom(String aManagecom)
	{
		Managecom = aManagecom;
	}
	public String getStandByString1()
	{
		return StandByString1;
	}
	public void setStandByString1(String aStandByString1)
	{
		StandByString1 = aStandByString1;
	}
	public String getStandByDate()
	{
		if( StandByDate != null )
			return fDate.getString(StandByDate);
		else
			return null;
	}
	public void setStandByDate(Date aStandByDate)
	{
		StandByDate = aStandByDate;
	}
	public void setStandByDate(String aStandByDate)
	{
		if (aStandByDate != null && !aStandByDate.equals("") )
		{
			StandByDate = fDate.getDate( aStandByDate );
		}
		else
			StandByDate = null;
	}


	/**
	* 使用另外一个 LLCaseStateDateSchema 对象给 Schema 赋值
	* @param: aLLCaseStateDateSchema LLCaseStateDateSchema
	**/
	public void setSchema(LLCaseStateDateSchema aLLCaseStateDateSchema)
	{
		this.CaseNo = aLLCaseStateDateSchema.getCaseNo();
		this.RgtNo = fDate.getDate( aLLCaseStateDateSchema.getRgtNo());
		this.EsdocDate = fDate.getDate( aLLCaseStateDateSchema.getEsdocDate());
		this.FeemainDate = fDate.getDate( aLLCaseStateDateSchema.getFeemainDate());
		this.RollCallDate = fDate.getDate( aLLCaseStateDateSchema.getRollCallDate());
		this.ApprovalDate1 = fDate.getDate( aLLCaseStateDateSchema.getApprovalDate1());
		this.ApprovalDate2 = fDate.getDate( aLLCaseStateDateSchema.getApprovalDate2());
		this.ValidationDate = fDate.getDate( aLLCaseStateDateSchema.getValidationDate());
		this.StartSurveyDate = fDate.getDate( aLLCaseStateDateSchema.getStartSurveyDate());
		this.EndSurveyDate = fDate.getDate( aLLCaseStateDateSchema.getEndSurveyDate());
		this.EndCaseDate = fDate.getDate( aLLCaseStateDateSchema.getEndCaseDate());
		this.SamplingDate = fDate.getDate( aLLCaseStateDateSchema.getSamplingDate());
		this.NoticeDate = fDate.getDate( aLLCaseStateDateSchema.getNoticeDate());
		this.PaymentDate = fDate.getDate( aLLCaseStateDateSchema.getPaymentDate());
		this.RightsFlag = aLLCaseStateDateSchema.getRightsFlag();
		this.ComCaseFlag = aLLCaseStateDateSchema.getComCaseFlag();
		this.Managecom = aLLCaseStateDateSchema.getManagecom();
		this.StandByString1 = aLLCaseStateDateSchema.getStandByString1();
		this.StandByDate = fDate.getDate( aLLCaseStateDateSchema.getStandByDate());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("CaseNo") == null )
				this.CaseNo = null;
			else
				this.CaseNo = rs.getString("CaseNo").trim();

			this.RgtNo = rs.getDate("RgtNo");
			this.EsdocDate = rs.getDate("EsdocDate");
			this.FeemainDate = rs.getDate("FeemainDate");
			this.RollCallDate = rs.getDate("RollCallDate");
			this.ApprovalDate1 = rs.getDate("ApprovalDate1");
			this.ApprovalDate2 = rs.getDate("ApprovalDate2");
			this.ValidationDate = rs.getDate("ValidationDate");
			this.StartSurveyDate = rs.getDate("StartSurveyDate");
			this.EndSurveyDate = rs.getDate("EndSurveyDate");
			this.EndCaseDate = rs.getDate("EndCaseDate");
			this.SamplingDate = rs.getDate("SamplingDate");
			this.NoticeDate = rs.getDate("NoticeDate");
			this.PaymentDate = rs.getDate("PaymentDate");
			if( rs.getString("RightsFlag") == null )
				this.RightsFlag = null;
			else
				this.RightsFlag = rs.getString("RightsFlag").trim();

			if( rs.getString("ComCaseFlag") == null )
				this.ComCaseFlag = null;
			else
				this.ComCaseFlag = rs.getString("ComCaseFlag").trim();

			if( rs.getString("Managecom") == null )
				this.Managecom = null;
			else
				this.Managecom = rs.getString("Managecom").trim();

			if( rs.getString("StandByString1") == null )
				this.StandByString1 = null;
			else
				this.StandByString1 = rs.getString("StandByString1").trim();

			this.StandByDate = rs.getDate("StandByDate");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLCaseStateDate表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseStateDateSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLCaseStateDateSchema getSchema()
	{
		LLCaseStateDateSchema aLLCaseStateDateSchema = new LLCaseStateDateSchema();
		aLLCaseStateDateSchema.setSchema(this);
		return aLLCaseStateDateSchema;
	}

	public LLCaseStateDateDB getDB()
	{
		LLCaseStateDateDB aDBOper = new LLCaseStateDateDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseStateDate描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(CaseNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RgtNo ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EsdocDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FeemainDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RollCallDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ApprovalDate1 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ApprovalDate2 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ValidationDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartSurveyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndSurveyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndCaseDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( SamplingDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( NoticeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( PaymentDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RightsFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComCaseFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Managecom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByString1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StandByDate )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseStateDate>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RgtNo = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,SysConst.PACKAGESPILTER));
			EsdocDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			FeemainDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			RollCallDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			ApprovalDate1 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			ApprovalDate2 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			ValidationDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			StartSurveyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			EndSurveyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			EndCaseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			SamplingDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			NoticeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			PaymentDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			RightsFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ComCaseFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Managecom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			StandByString1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			StandByDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLCaseStateDateSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("CaseNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
		}
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRgtNo()));
		}
		if (FCode.equals("EsdocDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEsdocDate()));
		}
		if (FCode.equals("FeemainDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFeemainDate()));
		}
		if (FCode.equals("RollCallDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRollCallDate()));
		}
		if (FCode.equals("ApprovalDate1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApprovalDate1()));
		}
		if (FCode.equals("ApprovalDate2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getApprovalDate2()));
		}
		if (FCode.equals("ValidationDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getValidationDate()));
		}
		if (FCode.equals("StartSurveyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartSurveyDate()));
		}
		if (FCode.equals("EndSurveyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndSurveyDate()));
		}
		if (FCode.equals("EndCaseDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndCaseDate()));
		}
		if (FCode.equals("SamplingDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSamplingDate()));
		}
		if (FCode.equals("NoticeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getNoticeDate()));
		}
		if (FCode.equals("PaymentDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getPaymentDate()));
		}
		if (FCode.equals("RightsFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RightsFlag));
		}
		if (FCode.equals("ComCaseFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComCaseFlag));
		}
		if (FCode.equals("Managecom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Managecom));
		}
		if (FCode.equals("StandByString1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByString1));
		}
		if (FCode.equals("StandByDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(CaseNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRgtNo()));
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEsdocDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFeemainDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRollCallDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApprovalDate1()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getApprovalDate2()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getValidationDate()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartSurveyDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndSurveyDate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndCaseDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSamplingDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getNoticeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getPaymentDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(RightsFlag);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ComCaseFlag);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Managecom);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(StandByString1);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStandByDate()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("CaseNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseNo = FValue.trim();
			}
			else
				CaseNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RgtNo = fDate.getDate( FValue );
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("EsdocDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EsdocDate = fDate.getDate( FValue );
			}
			else
				EsdocDate = null;
		}
		if (FCode.equalsIgnoreCase("FeemainDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FeemainDate = fDate.getDate( FValue );
			}
			else
				FeemainDate = null;
		}
		if (FCode.equalsIgnoreCase("RollCallDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RollCallDate = fDate.getDate( FValue );
			}
			else
				RollCallDate = null;
		}
		if (FCode.equalsIgnoreCase("ApprovalDate1"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ApprovalDate1 = fDate.getDate( FValue );
			}
			else
				ApprovalDate1 = null;
		}
		if (FCode.equalsIgnoreCase("ApprovalDate2"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ApprovalDate2 = fDate.getDate( FValue );
			}
			else
				ApprovalDate2 = null;
		}
		if (FCode.equalsIgnoreCase("ValidationDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ValidationDate = fDate.getDate( FValue );
			}
			else
				ValidationDate = null;
		}
		if (FCode.equalsIgnoreCase("StartSurveyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartSurveyDate = fDate.getDate( FValue );
			}
			else
				StartSurveyDate = null;
		}
		if (FCode.equalsIgnoreCase("EndSurveyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndSurveyDate = fDate.getDate( FValue );
			}
			else
				EndSurveyDate = null;
		}
		if (FCode.equalsIgnoreCase("EndCaseDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndCaseDate = fDate.getDate( FValue );
			}
			else
				EndCaseDate = null;
		}
		if (FCode.equalsIgnoreCase("SamplingDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SamplingDate = fDate.getDate( FValue );
			}
			else
				SamplingDate = null;
		}
		if (FCode.equalsIgnoreCase("NoticeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				NoticeDate = fDate.getDate( FValue );
			}
			else
				NoticeDate = null;
		}
		if (FCode.equalsIgnoreCase("PaymentDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				PaymentDate = fDate.getDate( FValue );
			}
			else
				PaymentDate = null;
		}
		if (FCode.equalsIgnoreCase("RightsFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RightsFlag = FValue.trim();
			}
			else
				RightsFlag = null;
		}
		if (FCode.equalsIgnoreCase("ComCaseFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComCaseFlag = FValue.trim();
			}
			else
				ComCaseFlag = null;
		}
		if (FCode.equalsIgnoreCase("Managecom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Managecom = FValue.trim();
			}
			else
				Managecom = null;
		}
		if (FCode.equalsIgnoreCase("StandByString1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByString1 = FValue.trim();
			}
			else
				StandByString1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StandByDate = fDate.getDate( FValue );
			}
			else
				StandByDate = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLCaseStateDateSchema other = (LLCaseStateDateSchema)otherObject;
		return
			(CaseNo == null ? other.getCaseNo() == null : CaseNo.equals(other.getCaseNo()))
			&& (RgtNo == null ? other.getRgtNo() == null : fDate.getString(RgtNo).equals(other.getRgtNo()))
			&& (EsdocDate == null ? other.getEsdocDate() == null : fDate.getString(EsdocDate).equals(other.getEsdocDate()))
			&& (FeemainDate == null ? other.getFeemainDate() == null : fDate.getString(FeemainDate).equals(other.getFeemainDate()))
			&& (RollCallDate == null ? other.getRollCallDate() == null : fDate.getString(RollCallDate).equals(other.getRollCallDate()))
			&& (ApprovalDate1 == null ? other.getApprovalDate1() == null : fDate.getString(ApprovalDate1).equals(other.getApprovalDate1()))
			&& (ApprovalDate2 == null ? other.getApprovalDate2() == null : fDate.getString(ApprovalDate2).equals(other.getApprovalDate2()))
			&& (ValidationDate == null ? other.getValidationDate() == null : fDate.getString(ValidationDate).equals(other.getValidationDate()))
			&& (StartSurveyDate == null ? other.getStartSurveyDate() == null : fDate.getString(StartSurveyDate).equals(other.getStartSurveyDate()))
			&& (EndSurveyDate == null ? other.getEndSurveyDate() == null : fDate.getString(EndSurveyDate).equals(other.getEndSurveyDate()))
			&& (EndCaseDate == null ? other.getEndCaseDate() == null : fDate.getString(EndCaseDate).equals(other.getEndCaseDate()))
			&& (SamplingDate == null ? other.getSamplingDate() == null : fDate.getString(SamplingDate).equals(other.getSamplingDate()))
			&& (NoticeDate == null ? other.getNoticeDate() == null : fDate.getString(NoticeDate).equals(other.getNoticeDate()))
			&& (PaymentDate == null ? other.getPaymentDate() == null : fDate.getString(PaymentDate).equals(other.getPaymentDate()))
			&& (RightsFlag == null ? other.getRightsFlag() == null : RightsFlag.equals(other.getRightsFlag()))
			&& (ComCaseFlag == null ? other.getComCaseFlag() == null : ComCaseFlag.equals(other.getComCaseFlag()))
			&& (Managecom == null ? other.getManagecom() == null : Managecom.equals(other.getManagecom()))
			&& (StandByString1 == null ? other.getStandByString1() == null : StandByString1.equals(other.getStandByString1()))
			&& (StandByDate == null ? other.getStandByDate() == null : fDate.getString(StandByDate).equals(other.getStandByDate()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("CaseNo") ) {
			return 0;
		}
		if( strFieldName.equals("RgtNo") ) {
			return 1;
		}
		if( strFieldName.equals("EsdocDate") ) {
			return 2;
		}
		if( strFieldName.equals("FeemainDate") ) {
			return 3;
		}
		if( strFieldName.equals("RollCallDate") ) {
			return 4;
		}
		if( strFieldName.equals("ApprovalDate1") ) {
			return 5;
		}
		if( strFieldName.equals("ApprovalDate2") ) {
			return 6;
		}
		if( strFieldName.equals("ValidationDate") ) {
			return 7;
		}
		if( strFieldName.equals("StartSurveyDate") ) {
			return 8;
		}
		if( strFieldName.equals("EndSurveyDate") ) {
			return 9;
		}
		if( strFieldName.equals("EndCaseDate") ) {
			return 10;
		}
		if( strFieldName.equals("SamplingDate") ) {
			return 11;
		}
		if( strFieldName.equals("NoticeDate") ) {
			return 12;
		}
		if( strFieldName.equals("PaymentDate") ) {
			return 13;
		}
		if( strFieldName.equals("RightsFlag") ) {
			return 14;
		}
		if( strFieldName.equals("ComCaseFlag") ) {
			return 15;
		}
		if( strFieldName.equals("Managecom") ) {
			return 16;
		}
		if( strFieldName.equals("StandByString1") ) {
			return 17;
		}
		if( strFieldName.equals("StandByDate") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "CaseNo";
				break;
			case 1:
				strFieldName = "RgtNo";
				break;
			case 2:
				strFieldName = "EsdocDate";
				break;
			case 3:
				strFieldName = "FeemainDate";
				break;
			case 4:
				strFieldName = "RollCallDate";
				break;
			case 5:
				strFieldName = "ApprovalDate1";
				break;
			case 6:
				strFieldName = "ApprovalDate2";
				break;
			case 7:
				strFieldName = "ValidationDate";
				break;
			case 8:
				strFieldName = "StartSurveyDate";
				break;
			case 9:
				strFieldName = "EndSurveyDate";
				break;
			case 10:
				strFieldName = "EndCaseDate";
				break;
			case 11:
				strFieldName = "SamplingDate";
				break;
			case 12:
				strFieldName = "NoticeDate";
				break;
			case 13:
				strFieldName = "PaymentDate";
				break;
			case 14:
				strFieldName = "RightsFlag";
				break;
			case 15:
				strFieldName = "ComCaseFlag";
				break;
			case 16:
				strFieldName = "Managecom";
				break;
			case 17:
				strFieldName = "StandByString1";
				break;
			case 18:
				strFieldName = "StandByDate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("CaseNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EsdocDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("FeemainDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RollCallDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ApprovalDate1") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ApprovalDate2") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ValidationDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("StartSurveyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndSurveyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndCaseDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SamplingDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("NoticeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("PaymentDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RightsFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComCaseFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Managecom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByString1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByDate") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
