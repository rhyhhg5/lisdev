/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LLRegisterDB;

/*
 * <p>ClassName: LLRegisterSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: Physical Data _3
 * @CreateDate：2016-03-16
 */
public class LLRegisterSchema implements Schema, Cloneable
{
	// @Field
	/** 立案号(申请登记号) */
	private String RgtNo;
	/** 案件状态 */
	private String RgtState;
	/** 号码类型 */
	private String RgtObj;
	/** 其他号码 */
	private String RgtObjNo;
	/** 受理方式 */
	private String RgtType;
	/** 申请类型 */
	private String RgtClass;
	/** 代理人代码 */
	private String AgentCode;
	/** 代理人组别 */
	private String AgentGroup;
	/** 申请人身份 */
	private String ApplyerType;
	/** 立案人/申请人姓名 */
	private String RgtantName;
	/** 立案人/申请人性别 */
	private String RgtantSex;
	/** 立案人/申请人与被保人关系 */
	private String Relation;
	/** 立案人/申请人地址 */
	private String RgtantAddress;
	/** 立案人/申请人电话 */
	private String RgtantPhone;
	/** 立案人/申请人手机 */
	private String RgtantMobile;
	/** 申请人电邮 */
	private String Email;
	/** 立案人/申请人邮政编码 */
	private String PostCode;
	/** 客户号 */
	private String CustomerNo;
	/** 单位名称 */
	private String GrpName;
	/** 立案日期 */
	private Date RgtDate;
	/** 出险地点 */
	private String AccidentSite;
	/** 出险原因 */
	private String AccidentReason;
	/** 出险过程和结果 */
	private String AccidentCourse;
	/** 出险开始日期 */
	private Date AccStartDate;
	/** 出险日期 */
	private Date AccidentDate;
	/** 立案撤销原因 */
	private String RgtReason;
	/** 申请人数 */
	private int AppPeoples;
	/** 预估申请金额 */
	private double AppAmnt;
	/** 赔付金领取方式 */
	private String GetMode;
	/** 赔付金领取间隔 */
	private int GetIntv;
	/** 保险金领取方式 */
	private String CaseGetMode;
	/** 回执发送方式 */
	private String ReturnMode;
	/** 备注 */
	private String Remark;
	/** 审核人 */
	private String Handler;
	/** 统一给付标记 */
	private String TogetherFlag;
	/** 报案标志 */
	private String RptFlag;
	/** 核算标记 */
	private String CalFlag;
	/** 核赔标记 */
	private String UWFlag;
	/** 拒赔标记 */
	private String DeclineFlag;
	/** 结案标记 */
	private String EndCaseFlag;
	/** 结案日期 */
	private Date EndCaseDate;
	/** 管理机构 */
	private String MngCom;
	/** 赔案状态 */
	private String ClmState;
	/** 银行编码 */
	private String BankCode;
	/** 银行帐号 */
	private String BankAccNo;
	/** 银行帐户名 */
	private String AccName;
	/** 申请人证件类型 */
	private String IDType;
	/** 申请人证件号码 */
	private String IDNo;
	/** 经办人 */
	private String Handler1;
	/** 经办人联系电话 */
	private String Handler1Phone;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 录入申请人数 */
	private int InputPeoples;
	/** 团体批次撤销人 */
	private String Canceler;
	/** 撤销原因 */
	private String CancelReason;
	/** 批次预付赔款回销标记 */
	private String PrePaidFlag;
	/** 申请日期 */
	private Date AppDate;
	/** 证件生效日期 */
	private Date IDStartDate;
	/** 证件失效日期 */
	private Date IDEndDate;

	public static final int FIELDNUM = 62;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LLRegisterSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "RgtNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LLRegisterSchema cloned = (LLRegisterSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRgtNo()
	{
		return RgtNo;
	}
	public void setRgtNo(String aRgtNo)
	{
		RgtNo = aRgtNo;
	}
	public String getRgtState()
	{
		return RgtState;
	}
	public void setRgtState(String aRgtState)
	{
		RgtState = aRgtState;
	}
	public String getRgtObj()
	{
		return RgtObj;
	}
	public void setRgtObj(String aRgtObj)
	{
		RgtObj = aRgtObj;
	}
	public String getRgtObjNo()
	{
		return RgtObjNo;
	}
	public void setRgtObjNo(String aRgtObjNo)
	{
		RgtObjNo = aRgtObjNo;
	}
	public String getRgtType()
	{
		return RgtType;
	}
	public void setRgtType(String aRgtType)
	{
		RgtType = aRgtType;
	}
	public String getRgtClass()
	{
		return RgtClass;
	}
	public void setRgtClass(String aRgtClass)
	{
		RgtClass = aRgtClass;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getApplyerType()
	{
		return ApplyerType;
	}
	public void setApplyerType(String aApplyerType)
	{
		ApplyerType = aApplyerType;
	}
	public String getRgtantName()
	{
		return RgtantName;
	}
	public void setRgtantName(String aRgtantName)
	{
		RgtantName = aRgtantName;
	}
	public String getRgtantSex()
	{
		return RgtantSex;
	}
	public void setRgtantSex(String aRgtantSex)
	{
		RgtantSex = aRgtantSex;
	}
	public String getRelation()
	{
		return Relation;
	}
	public void setRelation(String aRelation)
	{
		Relation = aRelation;
	}
	public String getRgtantAddress()
	{
		return RgtantAddress;
	}
	public void setRgtantAddress(String aRgtantAddress)
	{
		RgtantAddress = aRgtantAddress;
	}
	public String getRgtantPhone()
	{
		return RgtantPhone;
	}
	public void setRgtantPhone(String aRgtantPhone)
	{
		RgtantPhone = aRgtantPhone;
	}
	public String getRgtantMobile()
	{
		return RgtantMobile;
	}
	public void setRgtantMobile(String aRgtantMobile)
	{
		RgtantMobile = aRgtantMobile;
	}
	public String getEmail()
	{
		return Email;
	}
	public void setEmail(String aEmail)
	{
		Email = aEmail;
	}
	public String getPostCode()
	{
		return PostCode;
	}
	public void setPostCode(String aPostCode)
	{
		PostCode = aPostCode;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getGrpName()
	{
		return GrpName;
	}
	public void setGrpName(String aGrpName)
	{
		GrpName = aGrpName;
	}
	public String getRgtDate()
	{
		if( RgtDate != null )
			return fDate.getString(RgtDate);
		else
			return null;
	}
	public void setRgtDate(Date aRgtDate)
	{
		RgtDate = aRgtDate;
	}
	public void setRgtDate(String aRgtDate)
	{
		if (aRgtDate != null && !aRgtDate.equals("") )
		{
			RgtDate = fDate.getDate( aRgtDate );
		}
		else
			RgtDate = null;
	}

	public String getAccidentSite()
	{
		return AccidentSite;
	}
	public void setAccidentSite(String aAccidentSite)
	{
		AccidentSite = aAccidentSite;
	}
	public String getAccidentReason()
	{
		return AccidentReason;
	}
	public void setAccidentReason(String aAccidentReason)
	{
		AccidentReason = aAccidentReason;
	}
	public String getAccidentCourse()
	{
		return AccidentCourse;
	}
	public void setAccidentCourse(String aAccidentCourse)
	{
		AccidentCourse = aAccidentCourse;
	}
	public String getAccStartDate()
	{
		if( AccStartDate != null )
			return fDate.getString(AccStartDate);
		else
			return null;
	}
	public void setAccStartDate(Date aAccStartDate)
	{
		AccStartDate = aAccStartDate;
	}
	public void setAccStartDate(String aAccStartDate)
	{
		if (aAccStartDate != null && !aAccStartDate.equals("") )
		{
			AccStartDate = fDate.getDate( aAccStartDate );
		}
		else
			AccStartDate = null;
	}

	public String getAccidentDate()
	{
		if( AccidentDate != null )
			return fDate.getString(AccidentDate);
		else
			return null;
	}
	public void setAccidentDate(Date aAccidentDate)
	{
		AccidentDate = aAccidentDate;
	}
	public void setAccidentDate(String aAccidentDate)
	{
		if (aAccidentDate != null && !aAccidentDate.equals("") )
		{
			AccidentDate = fDate.getDate( aAccidentDate );
		}
		else
			AccidentDate = null;
	}

	public String getRgtReason()
	{
		return RgtReason;
	}
	public void setRgtReason(String aRgtReason)
	{
		RgtReason = aRgtReason;
	}
	public int getAppPeoples()
	{
		return AppPeoples;
	}
	public void setAppPeoples(int aAppPeoples)
	{
		AppPeoples = aAppPeoples;
	}
	public void setAppPeoples(String aAppPeoples)
	{
		if (aAppPeoples != null && !aAppPeoples.equals(""))
		{
			Integer tInteger = new Integer(aAppPeoples);
			int i = tInteger.intValue();
			AppPeoples = i;
		}
	}

	public double getAppAmnt()
	{
		return AppAmnt;
	}
	public void setAppAmnt(double aAppAmnt)
	{
		AppAmnt = Arith.round(aAppAmnt,2);
	}
	public void setAppAmnt(String aAppAmnt)
	{
		if (aAppAmnt != null && !aAppAmnt.equals(""))
		{
			Double tDouble = new Double(aAppAmnt);
			double d = tDouble.doubleValue();
                AppAmnt = Arith.round(d,2);
		}
	}

	public String getGetMode()
	{
		return GetMode;
	}
	public void setGetMode(String aGetMode)
	{
		GetMode = aGetMode;
	}
	public int getGetIntv()
	{
		return GetIntv;
	}
	public void setGetIntv(int aGetIntv)
	{
		GetIntv = aGetIntv;
	}
	public void setGetIntv(String aGetIntv)
	{
		if (aGetIntv != null && !aGetIntv.equals(""))
		{
			Integer tInteger = new Integer(aGetIntv);
			int i = tInteger.intValue();
			GetIntv = i;
		}
	}

	public String getCaseGetMode()
	{
		return CaseGetMode;
	}
	public void setCaseGetMode(String aCaseGetMode)
	{
		CaseGetMode = aCaseGetMode;
	}
	public String getReturnMode()
	{
		return ReturnMode;
	}
	public void setReturnMode(String aReturnMode)
	{
		ReturnMode = aReturnMode;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getHandler()
	{
		return Handler;
	}
	public void setHandler(String aHandler)
	{
		Handler = aHandler;
	}
	public String getTogetherFlag()
	{
		return TogetherFlag;
	}
	public void setTogetherFlag(String aTogetherFlag)
	{
		TogetherFlag = aTogetherFlag;
	}
	public String getRptFlag()
	{
		return RptFlag;
	}
	public void setRptFlag(String aRptFlag)
	{
		RptFlag = aRptFlag;
	}
	public String getCalFlag()
	{
		return CalFlag;
	}
	public void setCalFlag(String aCalFlag)
	{
		CalFlag = aCalFlag;
	}
	public String getUWFlag()
	{
		return UWFlag;
	}
	public void setUWFlag(String aUWFlag)
	{
		UWFlag = aUWFlag;
	}
	public String getDeclineFlag()
	{
		return DeclineFlag;
	}
	public void setDeclineFlag(String aDeclineFlag)
	{
		DeclineFlag = aDeclineFlag;
	}
	public String getEndCaseFlag()
	{
		return EndCaseFlag;
	}
	public void setEndCaseFlag(String aEndCaseFlag)
	{
		EndCaseFlag = aEndCaseFlag;
	}
	public String getEndCaseDate()
	{
		if( EndCaseDate != null )
			return fDate.getString(EndCaseDate);
		else
			return null;
	}
	public void setEndCaseDate(Date aEndCaseDate)
	{
		EndCaseDate = aEndCaseDate;
	}
	public void setEndCaseDate(String aEndCaseDate)
	{
		if (aEndCaseDate != null && !aEndCaseDate.equals("") )
		{
			EndCaseDate = fDate.getDate( aEndCaseDate );
		}
		else
			EndCaseDate = null;
	}

	public String getMngCom()
	{
		return MngCom;
	}
	public void setMngCom(String aMngCom)
	{
		MngCom = aMngCom;
	}
	public String getClmState()
	{
		return ClmState;
	}
	public void setClmState(String aClmState)
	{
		ClmState = aClmState;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankAccNo()
	{
		return BankAccNo;
	}
	public void setBankAccNo(String aBankAccNo)
	{
		BankAccNo = aBankAccNo;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getIDType()
	{
		return IDType;
	}
	public void setIDType(String aIDType)
	{
		IDType = aIDType;
	}
	public String getIDNo()
	{
		return IDNo;
	}
	public void setIDNo(String aIDNo)
	{
		IDNo = aIDNo;
	}
	public String getHandler1()
	{
		return Handler1;
	}
	public void setHandler1(String aHandler1)
	{
		Handler1 = aHandler1;
	}
	public String getHandler1Phone()
	{
		return Handler1Phone;
	}
	public void setHandler1Phone(String aHandler1Phone)
	{
		Handler1Phone = aHandler1Phone;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public int getInputPeoples()
	{
		return InputPeoples;
	}
	public void setInputPeoples(int aInputPeoples)
	{
		InputPeoples = aInputPeoples;
	}
	public void setInputPeoples(String aInputPeoples)
	{
		if (aInputPeoples != null && !aInputPeoples.equals(""))
		{
			Integer tInteger = new Integer(aInputPeoples);
			int i = tInteger.intValue();
			InputPeoples = i;
		}
	}

	public String getCanceler()
	{
		return Canceler;
	}
	public void setCanceler(String aCanceler)
	{
		Canceler = aCanceler;
	}
	public String getCancelReason()
	{
		return CancelReason;
	}
	public void setCancelReason(String aCancelReason)
	{
		CancelReason = aCancelReason;
	}
	public String getPrePaidFlag()
	{
		return PrePaidFlag;
	}
	public void setPrePaidFlag(String aPrePaidFlag)
	{
		PrePaidFlag = aPrePaidFlag;
	}
	public String getAppDate()
	{
		if( AppDate != null )
			return fDate.getString(AppDate);
		else
			return null;
	}
	public void setAppDate(Date aAppDate)
	{
		AppDate = aAppDate;
	}
	public void setAppDate(String aAppDate)
	{
		if (aAppDate != null && !aAppDate.equals("") )
		{
			AppDate = fDate.getDate( aAppDate );
		}
		else
			AppDate = null;
	}

	public String getIDStartDate()
	{
		if( IDStartDate != null )
			return fDate.getString(IDStartDate);
		else
			return null;
	}
	public void setIDStartDate(Date aIDStartDate)
	{
		IDStartDate = aIDStartDate;
	}
	public void setIDStartDate(String aIDStartDate)
	{
		if (aIDStartDate != null && !aIDStartDate.equals("") )
		{
			IDStartDate = fDate.getDate( aIDStartDate );
		}
		else
			IDStartDate = null;
	}

	public String getIDEndDate()
	{
		if( IDEndDate != null )
			return fDate.getString(IDEndDate);
		else
			return null;
	}
	public void setIDEndDate(Date aIDEndDate)
	{
		IDEndDate = aIDEndDate;
	}
	public void setIDEndDate(String aIDEndDate)
	{
		if (aIDEndDate != null && !aIDEndDate.equals("") )
		{
			IDEndDate = fDate.getDate( aIDEndDate );
		}
		else
			IDEndDate = null;
	}


	/**
	* 使用另外一个 LLRegisterSchema 对象给 Schema 赋值
	* @param: aLLRegisterSchema LLRegisterSchema
	**/
	public void setSchema(LLRegisterSchema aLLRegisterSchema)
	{
		this.RgtNo = aLLRegisterSchema.getRgtNo();
		this.RgtState = aLLRegisterSchema.getRgtState();
		this.RgtObj = aLLRegisterSchema.getRgtObj();
		this.RgtObjNo = aLLRegisterSchema.getRgtObjNo();
		this.RgtType = aLLRegisterSchema.getRgtType();
		this.RgtClass = aLLRegisterSchema.getRgtClass();
		this.AgentCode = aLLRegisterSchema.getAgentCode();
		this.AgentGroup = aLLRegisterSchema.getAgentGroup();
		this.ApplyerType = aLLRegisterSchema.getApplyerType();
		this.RgtantName = aLLRegisterSchema.getRgtantName();
		this.RgtantSex = aLLRegisterSchema.getRgtantSex();
		this.Relation = aLLRegisterSchema.getRelation();
		this.RgtantAddress = aLLRegisterSchema.getRgtantAddress();
		this.RgtantPhone = aLLRegisterSchema.getRgtantPhone();
		this.RgtantMobile = aLLRegisterSchema.getRgtantMobile();
		this.Email = aLLRegisterSchema.getEmail();
		this.PostCode = aLLRegisterSchema.getPostCode();
		this.CustomerNo = aLLRegisterSchema.getCustomerNo();
		this.GrpName = aLLRegisterSchema.getGrpName();
		this.RgtDate = fDate.getDate( aLLRegisterSchema.getRgtDate());
		this.AccidentSite = aLLRegisterSchema.getAccidentSite();
		this.AccidentReason = aLLRegisterSchema.getAccidentReason();
		this.AccidentCourse = aLLRegisterSchema.getAccidentCourse();
		this.AccStartDate = fDate.getDate( aLLRegisterSchema.getAccStartDate());
		this.AccidentDate = fDate.getDate( aLLRegisterSchema.getAccidentDate());
		this.RgtReason = aLLRegisterSchema.getRgtReason();
		this.AppPeoples = aLLRegisterSchema.getAppPeoples();
		this.AppAmnt = aLLRegisterSchema.getAppAmnt();
		this.GetMode = aLLRegisterSchema.getGetMode();
		this.GetIntv = aLLRegisterSchema.getGetIntv();
		this.CaseGetMode = aLLRegisterSchema.getCaseGetMode();
		this.ReturnMode = aLLRegisterSchema.getReturnMode();
		this.Remark = aLLRegisterSchema.getRemark();
		this.Handler = aLLRegisterSchema.getHandler();
		this.TogetherFlag = aLLRegisterSchema.getTogetherFlag();
		this.RptFlag = aLLRegisterSchema.getRptFlag();
		this.CalFlag = aLLRegisterSchema.getCalFlag();
		this.UWFlag = aLLRegisterSchema.getUWFlag();
		this.DeclineFlag = aLLRegisterSchema.getDeclineFlag();
		this.EndCaseFlag = aLLRegisterSchema.getEndCaseFlag();
		this.EndCaseDate = fDate.getDate( aLLRegisterSchema.getEndCaseDate());
		this.MngCom = aLLRegisterSchema.getMngCom();
		this.ClmState = aLLRegisterSchema.getClmState();
		this.BankCode = aLLRegisterSchema.getBankCode();
		this.BankAccNo = aLLRegisterSchema.getBankAccNo();
		this.AccName = aLLRegisterSchema.getAccName();
		this.IDType = aLLRegisterSchema.getIDType();
		this.IDNo = aLLRegisterSchema.getIDNo();
		this.Handler1 = aLLRegisterSchema.getHandler1();
		this.Handler1Phone = aLLRegisterSchema.getHandler1Phone();
		this.Operator = aLLRegisterSchema.getOperator();
		this.MakeDate = fDate.getDate( aLLRegisterSchema.getMakeDate());
		this.MakeTime = aLLRegisterSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLLRegisterSchema.getModifyDate());
		this.ModifyTime = aLLRegisterSchema.getModifyTime();
		this.InputPeoples = aLLRegisterSchema.getInputPeoples();
		this.Canceler = aLLRegisterSchema.getCanceler();
		this.CancelReason = aLLRegisterSchema.getCancelReason();
		this.PrePaidFlag = aLLRegisterSchema.getPrePaidFlag();
		this.AppDate = fDate.getDate( aLLRegisterSchema.getAppDate());
		this.IDStartDate = fDate.getDate( aLLRegisterSchema.getIDStartDate());
		this.IDEndDate = fDate.getDate( aLLRegisterSchema.getIDEndDate());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RgtNo") == null )
				this.RgtNo = null;
			else
				this.RgtNo = rs.getString("RgtNo").trim();

			if( rs.getString("RgtState") == null )
				this.RgtState = null;
			else
				this.RgtState = rs.getString("RgtState").trim();

			if( rs.getString("RgtObj") == null )
				this.RgtObj = null;
			else
				this.RgtObj = rs.getString("RgtObj").trim();

			if( rs.getString("RgtObjNo") == null )
				this.RgtObjNo = null;
			else
				this.RgtObjNo = rs.getString("RgtObjNo").trim();

			if( rs.getString("RgtType") == null )
				this.RgtType = null;
			else
				this.RgtType = rs.getString("RgtType").trim();

			if( rs.getString("RgtClass") == null )
				this.RgtClass = null;
			else
				this.RgtClass = rs.getString("RgtClass").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("ApplyerType") == null )
				this.ApplyerType = null;
			else
				this.ApplyerType = rs.getString("ApplyerType").trim();

			if( rs.getString("RgtantName") == null )
				this.RgtantName = null;
			else
				this.RgtantName = rs.getString("RgtantName").trim();

			if( rs.getString("RgtantSex") == null )
				this.RgtantSex = null;
			else
				this.RgtantSex = rs.getString("RgtantSex").trim();

			if( rs.getString("Relation") == null )
				this.Relation = null;
			else
				this.Relation = rs.getString("Relation").trim();

			if( rs.getString("RgtantAddress") == null )
				this.RgtantAddress = null;
			else
				this.RgtantAddress = rs.getString("RgtantAddress").trim();

			if( rs.getString("RgtantPhone") == null )
				this.RgtantPhone = null;
			else
				this.RgtantPhone = rs.getString("RgtantPhone").trim();

			if( rs.getString("RgtantMobile") == null )
				this.RgtantMobile = null;
			else
				this.RgtantMobile = rs.getString("RgtantMobile").trim();

			if( rs.getString("Email") == null )
				this.Email = null;
			else
				this.Email = rs.getString("Email").trim();

			if( rs.getString("PostCode") == null )
				this.PostCode = null;
			else
				this.PostCode = rs.getString("PostCode").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("GrpName") == null )
				this.GrpName = null;
			else
				this.GrpName = rs.getString("GrpName").trim();

			this.RgtDate = rs.getDate("RgtDate");
			if( rs.getString("AccidentSite") == null )
				this.AccidentSite = null;
			else
				this.AccidentSite = rs.getString("AccidentSite").trim();

			if( rs.getString("AccidentReason") == null )
				this.AccidentReason = null;
			else
				this.AccidentReason = rs.getString("AccidentReason").trim();

			if( rs.getString("AccidentCourse") == null )
				this.AccidentCourse = null;
			else
				this.AccidentCourse = rs.getString("AccidentCourse").trim();

			this.AccStartDate = rs.getDate("AccStartDate");
			this.AccidentDate = rs.getDate("AccidentDate");
			if( rs.getString("RgtReason") == null )
				this.RgtReason = null;
			else
				this.RgtReason = rs.getString("RgtReason").trim();

			this.AppPeoples = rs.getInt("AppPeoples");
			this.AppAmnt = rs.getDouble("AppAmnt");
			if( rs.getString("GetMode") == null )
				this.GetMode = null;
			else
				this.GetMode = rs.getString("GetMode").trim();

			this.GetIntv = rs.getInt("GetIntv");
			if( rs.getString("CaseGetMode") == null )
				this.CaseGetMode = null;
			else
				this.CaseGetMode = rs.getString("CaseGetMode").trim();

			if( rs.getString("ReturnMode") == null )
				this.ReturnMode = null;
			else
				this.ReturnMode = rs.getString("ReturnMode").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Handler") == null )
				this.Handler = null;
			else
				this.Handler = rs.getString("Handler").trim();

			if( rs.getString("TogetherFlag") == null )
				this.TogetherFlag = null;
			else
				this.TogetherFlag = rs.getString("TogetherFlag").trim();

			if( rs.getString("RptFlag") == null )
				this.RptFlag = null;
			else
				this.RptFlag = rs.getString("RptFlag").trim();

			if( rs.getString("CalFlag") == null )
				this.CalFlag = null;
			else
				this.CalFlag = rs.getString("CalFlag").trim();

			if( rs.getString("UWFlag") == null )
				this.UWFlag = null;
			else
				this.UWFlag = rs.getString("UWFlag").trim();

			if( rs.getString("DeclineFlag") == null )
				this.DeclineFlag = null;
			else
				this.DeclineFlag = rs.getString("DeclineFlag").trim();

			if( rs.getString("EndCaseFlag") == null )
				this.EndCaseFlag = null;
			else
				this.EndCaseFlag = rs.getString("EndCaseFlag").trim();

			this.EndCaseDate = rs.getDate("EndCaseDate");
			if( rs.getString("MngCom") == null )
				this.MngCom = null;
			else
				this.MngCom = rs.getString("MngCom").trim();

			if( rs.getString("ClmState") == null )
				this.ClmState = null;
			else
				this.ClmState = rs.getString("ClmState").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankAccNo") == null )
				this.BankAccNo = null;
			else
				this.BankAccNo = rs.getString("BankAccNo").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("IDType") == null )
				this.IDType = null;
			else
				this.IDType = rs.getString("IDType").trim();

			if( rs.getString("IDNo") == null )
				this.IDNo = null;
			else
				this.IDNo = rs.getString("IDNo").trim();

			if( rs.getString("Handler1") == null )
				this.Handler1 = null;
			else
				this.Handler1 = rs.getString("Handler1").trim();

			if( rs.getString("Handler1Phone") == null )
				this.Handler1Phone = null;
			else
				this.Handler1Phone = rs.getString("Handler1Phone").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.InputPeoples = rs.getInt("InputPeoples");
			if( rs.getString("Canceler") == null )
				this.Canceler = null;
			else
				this.Canceler = rs.getString("Canceler").trim();

			if( rs.getString("CancelReason") == null )
				this.CancelReason = null;
			else
				this.CancelReason = rs.getString("CancelReason").trim();

			if( rs.getString("PrePaidFlag") == null )
				this.PrePaidFlag = null;
			else
				this.PrePaidFlag = rs.getString("PrePaidFlag").trim();

			this.AppDate = rs.getDate("AppDate");
			this.IDStartDate = rs.getDate("IDStartDate");
			this.IDEndDate = rs.getDate("IDEndDate");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LLRegister表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLRegisterSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LLRegisterSchema getSchema()
	{
		LLRegisterSchema aLLRegisterSchema = new LLRegisterSchema();
		aLLRegisterSchema.setSchema(this);
		return aLLRegisterSchema;
	}

	public LLRegisterDB getDB()
	{
		LLRegisterDB aDBOper = new LLRegisterDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLRegister描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(RgtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtObj)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtObjNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtClass)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ApplyerType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtantName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtantSex)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Relation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtantAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtantPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtantMobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Email)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( RgtDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccidentSite)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccidentReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccidentCourse)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AccidentDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RgtReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AppAmnt));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GetMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GetIntv));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CaseGetMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReturnMode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TogetherFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RptFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CalFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UWFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DeclineFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EndCaseFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndCaseDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MngCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClmState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Handler1Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(InputPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Canceler)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CancelReason)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrePaidFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( AppDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDStartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( IDEndDate )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLRegister>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			RgtState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RgtObj = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			RgtObjNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			RgtType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			RgtClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			ApplyerType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			RgtantName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			RgtantSex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Relation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			RgtantAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			RgtantPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			RgtantMobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Email = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			PostCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			GrpName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			RgtDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			AccidentSite = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			AccidentReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			AccidentCourse = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			AccStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,SysConst.PACKAGESPILTER));
			AccidentDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,SysConst.PACKAGESPILTER));
			RgtReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			AppPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).intValue();
			AppAmnt = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			GetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			GetIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).intValue();
			CaseGetMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			ReturnMode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			Handler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			TogetherFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			RptFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			CalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			UWFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			DeclineFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			EndCaseFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			EndCaseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41,SysConst.PACKAGESPILTER));
			MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			ClmState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			BankAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			IDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			IDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			Handler1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			Handler1Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55, SysConst.PACKAGESPILTER );
			InputPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,56,SysConst.PACKAGESPILTER))).intValue();
			Canceler = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57, SysConst.PACKAGESPILTER );
			CancelReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			PrePaidFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			AppDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60,SysConst.PACKAGESPILTER));
			IDStartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61,SysConst.PACKAGESPILTER));
			IDEndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LLRegisterSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RgtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
		}
		if (FCode.equals("RgtState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtState));
		}
		if (FCode.equals("RgtObj"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtObj));
		}
		if (FCode.equals("RgtObjNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtObjNo));
		}
		if (FCode.equals("RgtType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtType));
		}
		if (FCode.equals("RgtClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtClass));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("ApplyerType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ApplyerType));
		}
		if (FCode.equals("RgtantName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantName));
		}
		if (FCode.equals("RgtantSex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantSex));
		}
		if (FCode.equals("Relation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Relation));
		}
		if (FCode.equals("RgtantAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantAddress));
		}
		if (FCode.equals("RgtantPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantPhone));
		}
		if (FCode.equals("RgtantMobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtantMobile));
		}
		if (FCode.equals("Email"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Email));
		}
		if (FCode.equals("PostCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostCode));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("GrpName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpName));
		}
		if (FCode.equals("RgtDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getRgtDate()));
		}
		if (FCode.equals("AccidentSite"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentSite));
		}
		if (FCode.equals("AccidentReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentReason));
		}
		if (FCode.equals("AccidentCourse"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccidentCourse));
		}
		if (FCode.equals("AccStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccStartDate()));
		}
		if (FCode.equals("AccidentDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAccidentDate()));
		}
		if (FCode.equals("RgtReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RgtReason));
		}
		if (FCode.equals("AppPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppPeoples));
		}
		if (FCode.equals("AppAmnt"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AppAmnt));
		}
		if (FCode.equals("GetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetMode));
		}
		if (FCode.equals("GetIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GetIntv));
		}
		if (FCode.equals("CaseGetMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CaseGetMode));
		}
		if (FCode.equals("ReturnMode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReturnMode));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Handler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler));
		}
		if (FCode.equals("TogetherFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TogetherFlag));
		}
		if (FCode.equals("RptFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RptFlag));
		}
		if (FCode.equals("CalFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CalFlag));
		}
		if (FCode.equals("UWFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UWFlag));
		}
		if (FCode.equals("DeclineFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DeclineFlag));
		}
		if (FCode.equals("EndCaseFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EndCaseFlag));
		}
		if (FCode.equals("EndCaseDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndCaseDate()));
		}
		if (FCode.equals("MngCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
		}
		if (FCode.equals("ClmState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClmState));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankAccNo));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("IDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDType));
		}
		if (FCode.equals("IDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IDNo));
		}
		if (FCode.equals("Handler1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler1));
		}
		if (FCode.equals("Handler1Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Handler1Phone));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("InputPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InputPeoples));
		}
		if (FCode.equals("Canceler"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Canceler));
		}
		if (FCode.equals("CancelReason"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CancelReason));
		}
		if (FCode.equals("PrePaidFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrePaidFlag));
		}
		if (FCode.equals("AppDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getAppDate()));
		}
		if (FCode.equals("IDStartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
		}
		if (FCode.equals("IDEndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RgtNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(RgtState);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RgtObj);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(RgtObjNo);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(RgtType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(RgtClass);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(ApplyerType);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(RgtantName);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(RgtantSex);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Relation);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(RgtantAddress);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(RgtantPhone);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(RgtantMobile);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Email);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(PostCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(GrpName);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getRgtDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(AccidentSite);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(AccidentReason);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(AccidentCourse);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccStartDate()));
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAccidentDate()));
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(RgtReason);
				break;
			case 26:
				strFieldValue = String.valueOf(AppPeoples);
				break;
			case 27:
				strFieldValue = String.valueOf(AppAmnt);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(GetMode);
				break;
			case 29:
				strFieldValue = String.valueOf(GetIntv);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(CaseGetMode);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(ReturnMode);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(Handler);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(TogetherFlag);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(RptFlag);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(CalFlag);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(UWFlag);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(DeclineFlag);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(EndCaseFlag);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndCaseDate()));
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(MngCom);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(ClmState);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(BankAccNo);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(IDType);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(IDNo);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(Handler1);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(Handler1Phone);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 55:
				strFieldValue = String.valueOf(InputPeoples);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(Canceler);
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(CancelReason);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(PrePaidFlag);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getAppDate()));
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDStartDate()));
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getIDEndDate()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RgtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtNo = FValue.trim();
			}
			else
				RgtNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtState = FValue.trim();
			}
			else
				RgtState = null;
		}
		if (FCode.equalsIgnoreCase("RgtObj"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtObj = FValue.trim();
			}
			else
				RgtObj = null;
		}
		if (FCode.equalsIgnoreCase("RgtObjNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtObjNo = FValue.trim();
			}
			else
				RgtObjNo = null;
		}
		if (FCode.equalsIgnoreCase("RgtType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtType = FValue.trim();
			}
			else
				RgtType = null;
		}
		if (FCode.equalsIgnoreCase("RgtClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtClass = FValue.trim();
			}
			else
				RgtClass = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("ApplyerType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ApplyerType = FValue.trim();
			}
			else
				ApplyerType = null;
		}
		if (FCode.equalsIgnoreCase("RgtantName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtantName = FValue.trim();
			}
			else
				RgtantName = null;
		}
		if (FCode.equalsIgnoreCase("RgtantSex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtantSex = FValue.trim();
			}
			else
				RgtantSex = null;
		}
		if (FCode.equalsIgnoreCase("Relation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Relation = FValue.trim();
			}
			else
				Relation = null;
		}
		if (FCode.equalsIgnoreCase("RgtantAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtantAddress = FValue.trim();
			}
			else
				RgtantAddress = null;
		}
		if (FCode.equalsIgnoreCase("RgtantPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtantPhone = FValue.trim();
			}
			else
				RgtantPhone = null;
		}
		if (FCode.equalsIgnoreCase("RgtantMobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtantMobile = FValue.trim();
			}
			else
				RgtantMobile = null;
		}
		if (FCode.equalsIgnoreCase("Email"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Email = FValue.trim();
			}
			else
				Email = null;
		}
		if (FCode.equalsIgnoreCase("PostCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostCode = FValue.trim();
			}
			else
				PostCode = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpName = FValue.trim();
			}
			else
				GrpName = null;
		}
		if (FCode.equalsIgnoreCase("RgtDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				RgtDate = fDate.getDate( FValue );
			}
			else
				RgtDate = null;
		}
		if (FCode.equalsIgnoreCase("AccidentSite"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccidentSite = FValue.trim();
			}
			else
				AccidentSite = null;
		}
		if (FCode.equalsIgnoreCase("AccidentReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccidentReason = FValue.trim();
			}
			else
				AccidentReason = null;
		}
		if (FCode.equalsIgnoreCase("AccidentCourse"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccidentCourse = FValue.trim();
			}
			else
				AccidentCourse = null;
		}
		if (FCode.equalsIgnoreCase("AccStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccStartDate = fDate.getDate( FValue );
			}
			else
				AccStartDate = null;
		}
		if (FCode.equalsIgnoreCase("AccidentDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AccidentDate = fDate.getDate( FValue );
			}
			else
				AccidentDate = null;
		}
		if (FCode.equalsIgnoreCase("RgtReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RgtReason = FValue.trim();
			}
			else
				RgtReason = null;
		}
		if (FCode.equalsIgnoreCase("AppPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				AppPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("AppAmnt"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AppAmnt = d;
			}
		}
		if (FCode.equalsIgnoreCase("GetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GetMode = FValue.trim();
			}
			else
				GetMode = null;
		}
		if (FCode.equalsIgnoreCase("GetIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				GetIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("CaseGetMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CaseGetMode = FValue.trim();
			}
			else
				CaseGetMode = null;
		}
		if (FCode.equalsIgnoreCase("ReturnMode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReturnMode = FValue.trim();
			}
			else
				ReturnMode = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Handler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler = FValue.trim();
			}
			else
				Handler = null;
		}
		if (FCode.equalsIgnoreCase("TogetherFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TogetherFlag = FValue.trim();
			}
			else
				TogetherFlag = null;
		}
		if (FCode.equalsIgnoreCase("RptFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RptFlag = FValue.trim();
			}
			else
				RptFlag = null;
		}
		if (FCode.equalsIgnoreCase("CalFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CalFlag = FValue.trim();
			}
			else
				CalFlag = null;
		}
		if (FCode.equalsIgnoreCase("UWFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UWFlag = FValue.trim();
			}
			else
				UWFlag = null;
		}
		if (FCode.equalsIgnoreCase("DeclineFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DeclineFlag = FValue.trim();
			}
			else
				DeclineFlag = null;
		}
		if (FCode.equalsIgnoreCase("EndCaseFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EndCaseFlag = FValue.trim();
			}
			else
				EndCaseFlag = null;
		}
		if (FCode.equalsIgnoreCase("EndCaseDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndCaseDate = fDate.getDate( FValue );
			}
			else
				EndCaseDate = null;
		}
		if (FCode.equalsIgnoreCase("MngCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MngCom = FValue.trim();
			}
			else
				MngCom = null;
		}
		if (FCode.equalsIgnoreCase("ClmState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClmState = FValue.trim();
			}
			else
				ClmState = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankAccNo = FValue.trim();
			}
			else
				BankAccNo = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("IDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDType = FValue.trim();
			}
			else
				IDType = null;
		}
		if (FCode.equalsIgnoreCase("IDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IDNo = FValue.trim();
			}
			else
				IDNo = null;
		}
		if (FCode.equalsIgnoreCase("Handler1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler1 = FValue.trim();
			}
			else
				Handler1 = null;
		}
		if (FCode.equalsIgnoreCase("Handler1Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Handler1Phone = FValue.trim();
			}
			else
				Handler1Phone = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("InputPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				InputPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("Canceler"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Canceler = FValue.trim();
			}
			else
				Canceler = null;
		}
		if (FCode.equalsIgnoreCase("CancelReason"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CancelReason = FValue.trim();
			}
			else
				CancelReason = null;
		}
		if (FCode.equalsIgnoreCase("PrePaidFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrePaidFlag = FValue.trim();
			}
			else
				PrePaidFlag = null;
		}
		if (FCode.equalsIgnoreCase("AppDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				AppDate = fDate.getDate( FValue );
			}
			else
				AppDate = null;
		}
		if (FCode.equalsIgnoreCase("IDStartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDStartDate = fDate.getDate( FValue );
			}
			else
				IDStartDate = null;
		}
		if (FCode.equalsIgnoreCase("IDEndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				IDEndDate = fDate.getDate( FValue );
			}
			else
				IDEndDate = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LLRegisterSchema other = (LLRegisterSchema)otherObject;
		return
			(RgtNo == null ? other.getRgtNo() == null : RgtNo.equals(other.getRgtNo()))
			&& (RgtState == null ? other.getRgtState() == null : RgtState.equals(other.getRgtState()))
			&& (RgtObj == null ? other.getRgtObj() == null : RgtObj.equals(other.getRgtObj()))
			&& (RgtObjNo == null ? other.getRgtObjNo() == null : RgtObjNo.equals(other.getRgtObjNo()))
			&& (RgtType == null ? other.getRgtType() == null : RgtType.equals(other.getRgtType()))
			&& (RgtClass == null ? other.getRgtClass() == null : RgtClass.equals(other.getRgtClass()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (ApplyerType == null ? other.getApplyerType() == null : ApplyerType.equals(other.getApplyerType()))
			&& (RgtantName == null ? other.getRgtantName() == null : RgtantName.equals(other.getRgtantName()))
			&& (RgtantSex == null ? other.getRgtantSex() == null : RgtantSex.equals(other.getRgtantSex()))
			&& (Relation == null ? other.getRelation() == null : Relation.equals(other.getRelation()))
			&& (RgtantAddress == null ? other.getRgtantAddress() == null : RgtantAddress.equals(other.getRgtantAddress()))
			&& (RgtantPhone == null ? other.getRgtantPhone() == null : RgtantPhone.equals(other.getRgtantPhone()))
			&& (RgtantMobile == null ? other.getRgtantMobile() == null : RgtantMobile.equals(other.getRgtantMobile()))
			&& (Email == null ? other.getEmail() == null : Email.equals(other.getEmail()))
			&& (PostCode == null ? other.getPostCode() == null : PostCode.equals(other.getPostCode()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (GrpName == null ? other.getGrpName() == null : GrpName.equals(other.getGrpName()))
			&& (RgtDate == null ? other.getRgtDate() == null : fDate.getString(RgtDate).equals(other.getRgtDate()))
			&& (AccidentSite == null ? other.getAccidentSite() == null : AccidentSite.equals(other.getAccidentSite()))
			&& (AccidentReason == null ? other.getAccidentReason() == null : AccidentReason.equals(other.getAccidentReason()))
			&& (AccidentCourse == null ? other.getAccidentCourse() == null : AccidentCourse.equals(other.getAccidentCourse()))
			&& (AccStartDate == null ? other.getAccStartDate() == null : fDate.getString(AccStartDate).equals(other.getAccStartDate()))
			&& (AccidentDate == null ? other.getAccidentDate() == null : fDate.getString(AccidentDate).equals(other.getAccidentDate()))
			&& (RgtReason == null ? other.getRgtReason() == null : RgtReason.equals(other.getRgtReason()))
			&& AppPeoples == other.getAppPeoples()
			&& AppAmnt == other.getAppAmnt()
			&& (GetMode == null ? other.getGetMode() == null : GetMode.equals(other.getGetMode()))
			&& GetIntv == other.getGetIntv()
			&& (CaseGetMode == null ? other.getCaseGetMode() == null : CaseGetMode.equals(other.getCaseGetMode()))
			&& (ReturnMode == null ? other.getReturnMode() == null : ReturnMode.equals(other.getReturnMode()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (Handler == null ? other.getHandler() == null : Handler.equals(other.getHandler()))
			&& (TogetherFlag == null ? other.getTogetherFlag() == null : TogetherFlag.equals(other.getTogetherFlag()))
			&& (RptFlag == null ? other.getRptFlag() == null : RptFlag.equals(other.getRptFlag()))
			&& (CalFlag == null ? other.getCalFlag() == null : CalFlag.equals(other.getCalFlag()))
			&& (UWFlag == null ? other.getUWFlag() == null : UWFlag.equals(other.getUWFlag()))
			&& (DeclineFlag == null ? other.getDeclineFlag() == null : DeclineFlag.equals(other.getDeclineFlag()))
			&& (EndCaseFlag == null ? other.getEndCaseFlag() == null : EndCaseFlag.equals(other.getEndCaseFlag()))
			&& (EndCaseDate == null ? other.getEndCaseDate() == null : fDate.getString(EndCaseDate).equals(other.getEndCaseDate()))
			&& (MngCom == null ? other.getMngCom() == null : MngCom.equals(other.getMngCom()))
			&& (ClmState == null ? other.getClmState() == null : ClmState.equals(other.getClmState()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankAccNo == null ? other.getBankAccNo() == null : BankAccNo.equals(other.getBankAccNo()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (IDType == null ? other.getIDType() == null : IDType.equals(other.getIDType()))
			&& (IDNo == null ? other.getIDNo() == null : IDNo.equals(other.getIDNo()))
			&& (Handler1 == null ? other.getHandler1() == null : Handler1.equals(other.getHandler1()))
			&& (Handler1Phone == null ? other.getHandler1Phone() == null : Handler1Phone.equals(other.getHandler1Phone()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& InputPeoples == other.getInputPeoples()
			&& (Canceler == null ? other.getCanceler() == null : Canceler.equals(other.getCanceler()))
			&& (CancelReason == null ? other.getCancelReason() == null : CancelReason.equals(other.getCancelReason()))
			&& (PrePaidFlag == null ? other.getPrePaidFlag() == null : PrePaidFlag.equals(other.getPrePaidFlag()))
			&& (AppDate == null ? other.getAppDate() == null : fDate.getString(AppDate).equals(other.getAppDate()))
			&& (IDStartDate == null ? other.getIDStartDate() == null : fDate.getString(IDStartDate).equals(other.getIDStartDate()))
			&& (IDEndDate == null ? other.getIDEndDate() == null : fDate.getString(IDEndDate).equals(other.getIDEndDate()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RgtNo") ) {
			return 0;
		}
		if( strFieldName.equals("RgtState") ) {
			return 1;
		}
		if( strFieldName.equals("RgtObj") ) {
			return 2;
		}
		if( strFieldName.equals("RgtObjNo") ) {
			return 3;
		}
		if( strFieldName.equals("RgtType") ) {
			return 4;
		}
		if( strFieldName.equals("RgtClass") ) {
			return 5;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 6;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 7;
		}
		if( strFieldName.equals("ApplyerType") ) {
			return 8;
		}
		if( strFieldName.equals("RgtantName") ) {
			return 9;
		}
		if( strFieldName.equals("RgtantSex") ) {
			return 10;
		}
		if( strFieldName.equals("Relation") ) {
			return 11;
		}
		if( strFieldName.equals("RgtantAddress") ) {
			return 12;
		}
		if( strFieldName.equals("RgtantPhone") ) {
			return 13;
		}
		if( strFieldName.equals("RgtantMobile") ) {
			return 14;
		}
		if( strFieldName.equals("Email") ) {
			return 15;
		}
		if( strFieldName.equals("PostCode") ) {
			return 16;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 17;
		}
		if( strFieldName.equals("GrpName") ) {
			return 18;
		}
		if( strFieldName.equals("RgtDate") ) {
			return 19;
		}
		if( strFieldName.equals("AccidentSite") ) {
			return 20;
		}
		if( strFieldName.equals("AccidentReason") ) {
			return 21;
		}
		if( strFieldName.equals("AccidentCourse") ) {
			return 22;
		}
		if( strFieldName.equals("AccStartDate") ) {
			return 23;
		}
		if( strFieldName.equals("AccidentDate") ) {
			return 24;
		}
		if( strFieldName.equals("RgtReason") ) {
			return 25;
		}
		if( strFieldName.equals("AppPeoples") ) {
			return 26;
		}
		if( strFieldName.equals("AppAmnt") ) {
			return 27;
		}
		if( strFieldName.equals("GetMode") ) {
			return 28;
		}
		if( strFieldName.equals("GetIntv") ) {
			return 29;
		}
		if( strFieldName.equals("CaseGetMode") ) {
			return 30;
		}
		if( strFieldName.equals("ReturnMode") ) {
			return 31;
		}
		if( strFieldName.equals("Remark") ) {
			return 32;
		}
		if( strFieldName.equals("Handler") ) {
			return 33;
		}
		if( strFieldName.equals("TogetherFlag") ) {
			return 34;
		}
		if( strFieldName.equals("RptFlag") ) {
			return 35;
		}
		if( strFieldName.equals("CalFlag") ) {
			return 36;
		}
		if( strFieldName.equals("UWFlag") ) {
			return 37;
		}
		if( strFieldName.equals("DeclineFlag") ) {
			return 38;
		}
		if( strFieldName.equals("EndCaseFlag") ) {
			return 39;
		}
		if( strFieldName.equals("EndCaseDate") ) {
			return 40;
		}
		if( strFieldName.equals("MngCom") ) {
			return 41;
		}
		if( strFieldName.equals("ClmState") ) {
			return 42;
		}
		if( strFieldName.equals("BankCode") ) {
			return 43;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return 44;
		}
		if( strFieldName.equals("AccName") ) {
			return 45;
		}
		if( strFieldName.equals("IDType") ) {
			return 46;
		}
		if( strFieldName.equals("IDNo") ) {
			return 47;
		}
		if( strFieldName.equals("Handler1") ) {
			return 48;
		}
		if( strFieldName.equals("Handler1Phone") ) {
			return 49;
		}
		if( strFieldName.equals("Operator") ) {
			return 50;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 51;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 52;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 53;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 54;
		}
		if( strFieldName.equals("InputPeoples") ) {
			return 55;
		}
		if( strFieldName.equals("Canceler") ) {
			return 56;
		}
		if( strFieldName.equals("CancelReason") ) {
			return 57;
		}
		if( strFieldName.equals("PrePaidFlag") ) {
			return 58;
		}
		if( strFieldName.equals("AppDate") ) {
			return 59;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return 60;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return 61;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RgtNo";
				break;
			case 1:
				strFieldName = "RgtState";
				break;
			case 2:
				strFieldName = "RgtObj";
				break;
			case 3:
				strFieldName = "RgtObjNo";
				break;
			case 4:
				strFieldName = "RgtType";
				break;
			case 5:
				strFieldName = "RgtClass";
				break;
			case 6:
				strFieldName = "AgentCode";
				break;
			case 7:
				strFieldName = "AgentGroup";
				break;
			case 8:
				strFieldName = "ApplyerType";
				break;
			case 9:
				strFieldName = "RgtantName";
				break;
			case 10:
				strFieldName = "RgtantSex";
				break;
			case 11:
				strFieldName = "Relation";
				break;
			case 12:
				strFieldName = "RgtantAddress";
				break;
			case 13:
				strFieldName = "RgtantPhone";
				break;
			case 14:
				strFieldName = "RgtantMobile";
				break;
			case 15:
				strFieldName = "Email";
				break;
			case 16:
				strFieldName = "PostCode";
				break;
			case 17:
				strFieldName = "CustomerNo";
				break;
			case 18:
				strFieldName = "GrpName";
				break;
			case 19:
				strFieldName = "RgtDate";
				break;
			case 20:
				strFieldName = "AccidentSite";
				break;
			case 21:
				strFieldName = "AccidentReason";
				break;
			case 22:
				strFieldName = "AccidentCourse";
				break;
			case 23:
				strFieldName = "AccStartDate";
				break;
			case 24:
				strFieldName = "AccidentDate";
				break;
			case 25:
				strFieldName = "RgtReason";
				break;
			case 26:
				strFieldName = "AppPeoples";
				break;
			case 27:
				strFieldName = "AppAmnt";
				break;
			case 28:
				strFieldName = "GetMode";
				break;
			case 29:
				strFieldName = "GetIntv";
				break;
			case 30:
				strFieldName = "CaseGetMode";
				break;
			case 31:
				strFieldName = "ReturnMode";
				break;
			case 32:
				strFieldName = "Remark";
				break;
			case 33:
				strFieldName = "Handler";
				break;
			case 34:
				strFieldName = "TogetherFlag";
				break;
			case 35:
				strFieldName = "RptFlag";
				break;
			case 36:
				strFieldName = "CalFlag";
				break;
			case 37:
				strFieldName = "UWFlag";
				break;
			case 38:
				strFieldName = "DeclineFlag";
				break;
			case 39:
				strFieldName = "EndCaseFlag";
				break;
			case 40:
				strFieldName = "EndCaseDate";
				break;
			case 41:
				strFieldName = "MngCom";
				break;
			case 42:
				strFieldName = "ClmState";
				break;
			case 43:
				strFieldName = "BankCode";
				break;
			case 44:
				strFieldName = "BankAccNo";
				break;
			case 45:
				strFieldName = "AccName";
				break;
			case 46:
				strFieldName = "IDType";
				break;
			case 47:
				strFieldName = "IDNo";
				break;
			case 48:
				strFieldName = "Handler1";
				break;
			case 49:
				strFieldName = "Handler1Phone";
				break;
			case 50:
				strFieldName = "Operator";
				break;
			case 51:
				strFieldName = "MakeDate";
				break;
			case 52:
				strFieldName = "MakeTime";
				break;
			case 53:
				strFieldName = "ModifyDate";
				break;
			case 54:
				strFieldName = "ModifyTime";
				break;
			case 55:
				strFieldName = "InputPeoples";
				break;
			case 56:
				strFieldName = "Canceler";
				break;
			case 57:
				strFieldName = "CancelReason";
				break;
			case 58:
				strFieldName = "PrePaidFlag";
				break;
			case 59:
				strFieldName = "AppDate";
				break;
			case 60:
				strFieldName = "IDStartDate";
				break;
			case 61:
				strFieldName = "IDEndDate";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RgtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtObj") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtObjNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ApplyerType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtantName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtantSex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Relation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtantAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtantPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtantMobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Email") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RgtDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccidentSite") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccidentReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccidentCourse") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccidentDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("RgtReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("AppAmnt") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CaseGetMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReturnMode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Handler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TogetherFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RptFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CalFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UWFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DeclineFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndCaseFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EndCaseDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MngCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClmState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Handler1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Handler1Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InputPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Canceler") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CancelReason") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrePaidFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AppDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDStartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("IDEndDate") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 24:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_INT;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_INT;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 54:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 55:
				nFieldType = Schema.TYPE_INT;
				break;
			case 56:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 60:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 61:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
