/*
 * <p>ClassName: LAComTransLogSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-20
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LAComTransLogDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

public class LAComTransLogSchema implements Schema
{
    // @Field
    /** 发盘号 */
    private String AppID;
    /** 发盘类型 */
    private String AppIDType;
    /** 上报年份 */
    private int Year;
    /** 上报次数 */
    private int Times;
    /** 接收单位代码 */
    private String RecvUnitCode;
    /** 发送单位代码 */
    private String SendUnitCode;
    /** 发送日期 */
    private Date SendDate;
    /** 发送人 */
    private String Sender;
    /** 原发盘号 */
    private String OrgAppID;
    /** 新增机构数 */
    private int NewComCount;
    /** 信息变更数 */
    private int InfoChgCount;
    /** 险种变更数 */
    private int RiskChgCount;
    /** 黑名单数 */
    private int BlacklistCount;
    /** 建立关系数 */
    private int BuildRelaCount;
    /** 终止关系数 */
    private int StopRelaCount;
    /** 转换关系数 */
    private int ChgRelaCount;
    /** 许可证数 */
    private int LicenseCount;
    /** 备用 */
    private int OtherCount;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 23; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAComTransLogSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "AppID";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAppID()
    {
        if (AppID != null && !AppID.equals("") && SysConst.CHANGECHARSET)
        {
            AppID = StrTool.unicodeToGBK(AppID);
        }
        return AppID;
    }

    public void setAppID(String aAppID)
    {
        AppID = aAppID;
    }

    public String getAppIDType()
    {
        if (AppIDType != null && !AppIDType.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AppIDType = StrTool.unicodeToGBK(AppIDType);
        }
        return AppIDType;
    }

    public void setAppIDType(String aAppIDType)
    {
        AppIDType = aAppIDType;
    }

    public int getYear()
    {
        return Year;
    }

    public void setYear(int aYear)
    {
        Year = aYear;
    }

    public void setYear(String aYear)
    {
        if (aYear != null && !aYear.equals(""))
        {
            Integer tInteger = new Integer(aYear);
            int i = tInteger.intValue();
            Year = i;
        }
    }

    public int getTimes()
    {
        return Times;
    }

    public void setTimes(int aTimes)
    {
        Times = aTimes;
    }

    public void setTimes(String aTimes)
    {
        if (aTimes != null && !aTimes.equals(""))
        {
            Integer tInteger = new Integer(aTimes);
            int i = tInteger.intValue();
            Times = i;
        }
    }

    public String getRecvUnitCode()
    {
        if (RecvUnitCode != null && !RecvUnitCode.equals("") &&
            SysConst.CHANGECHARSET)
        {
            RecvUnitCode = StrTool.unicodeToGBK(RecvUnitCode);
        }
        return RecvUnitCode;
    }

    public void setRecvUnitCode(String aRecvUnitCode)
    {
        RecvUnitCode = aRecvUnitCode;
    }

    public String getSendUnitCode()
    {
        if (SendUnitCode != null && !SendUnitCode.equals("") &&
            SysConst.CHANGECHARSET)
        {
            SendUnitCode = StrTool.unicodeToGBK(SendUnitCode);
        }
        return SendUnitCode;
    }

    public void setSendUnitCode(String aSendUnitCode)
    {
        SendUnitCode = aSendUnitCode;
    }

    public String getSendDate()
    {
        if (SendDate != null)
        {
            return fDate.getString(SendDate);
        }
        else
        {
            return null;
        }
    }

    public void setSendDate(Date aSendDate)
    {
        SendDate = aSendDate;
    }

    public void setSendDate(String aSendDate)
    {
        if (aSendDate != null && !aSendDate.equals(""))
        {
            SendDate = fDate.getDate(aSendDate);
        }
        else
        {
            SendDate = null;
        }
    }

    public String getSender()
    {
        if (Sender != null && !Sender.equals("") && SysConst.CHANGECHARSET)
        {
            Sender = StrTool.unicodeToGBK(Sender);
        }
        return Sender;
    }

    public void setSender(String aSender)
    {
        Sender = aSender;
    }

    public String getOrgAppID()
    {
        if (OrgAppID != null && !OrgAppID.equals("") && SysConst.CHANGECHARSET)
        {
            OrgAppID = StrTool.unicodeToGBK(OrgAppID);
        }
        return OrgAppID;
    }

    public void setOrgAppID(String aOrgAppID)
    {
        OrgAppID = aOrgAppID;
    }

    public int getNewComCount()
    {
        return NewComCount;
    }

    public void setNewComCount(int aNewComCount)
    {
        NewComCount = aNewComCount;
    }

    public void setNewComCount(String aNewComCount)
    {
        if (aNewComCount != null && !aNewComCount.equals(""))
        {
            Integer tInteger = new Integer(aNewComCount);
            int i = tInteger.intValue();
            NewComCount = i;
        }
    }

    public int getInfoChgCount()
    {
        return InfoChgCount;
    }

    public void setInfoChgCount(int aInfoChgCount)
    {
        InfoChgCount = aInfoChgCount;
    }

    public void setInfoChgCount(String aInfoChgCount)
    {
        if (aInfoChgCount != null && !aInfoChgCount.equals(""))
        {
            Integer tInteger = new Integer(aInfoChgCount);
            int i = tInteger.intValue();
            InfoChgCount = i;
        }
    }

    public int getRiskChgCount()
    {
        return RiskChgCount;
    }

    public void setRiskChgCount(int aRiskChgCount)
    {
        RiskChgCount = aRiskChgCount;
    }

    public void setRiskChgCount(String aRiskChgCount)
    {
        if (aRiskChgCount != null && !aRiskChgCount.equals(""))
        {
            Integer tInteger = new Integer(aRiskChgCount);
            int i = tInteger.intValue();
            RiskChgCount = i;
        }
    }

    public int getBlacklistCount()
    {
        return BlacklistCount;
    }

    public void setBlacklistCount(int aBlacklistCount)
    {
        BlacklistCount = aBlacklistCount;
    }

    public void setBlacklistCount(String aBlacklistCount)
    {
        if (aBlacklistCount != null && !aBlacklistCount.equals(""))
        {
            Integer tInteger = new Integer(aBlacklistCount);
            int i = tInteger.intValue();
            BlacklistCount = i;
        }
    }

    public int getBuildRelaCount()
    {
        return BuildRelaCount;
    }

    public void setBuildRelaCount(int aBuildRelaCount)
    {
        BuildRelaCount = aBuildRelaCount;
    }

    public void setBuildRelaCount(String aBuildRelaCount)
    {
        if (aBuildRelaCount != null && !aBuildRelaCount.equals(""))
        {
            Integer tInteger = new Integer(aBuildRelaCount);
            int i = tInteger.intValue();
            BuildRelaCount = i;
        }
    }

    public int getStopRelaCount()
    {
        return StopRelaCount;
    }

    public void setStopRelaCount(int aStopRelaCount)
    {
        StopRelaCount = aStopRelaCount;
    }

    public void setStopRelaCount(String aStopRelaCount)
    {
        if (aStopRelaCount != null && !aStopRelaCount.equals(""))
        {
            Integer tInteger = new Integer(aStopRelaCount);
            int i = tInteger.intValue();
            StopRelaCount = i;
        }
    }

    public int getChgRelaCount()
    {
        return ChgRelaCount;
    }

    public void setChgRelaCount(int aChgRelaCount)
    {
        ChgRelaCount = aChgRelaCount;
    }

    public void setChgRelaCount(String aChgRelaCount)
    {
        if (aChgRelaCount != null && !aChgRelaCount.equals(""))
        {
            Integer tInteger = new Integer(aChgRelaCount);
            int i = tInteger.intValue();
            ChgRelaCount = i;
        }
    }

    public int getLicenseCount()
    {
        return LicenseCount;
    }

    public void setLicenseCount(int aLicenseCount)
    {
        LicenseCount = aLicenseCount;
    }

    public void setLicenseCount(String aLicenseCount)
    {
        if (aLicenseCount != null && !aLicenseCount.equals(""))
        {
            Integer tInteger = new Integer(aLicenseCount);
            int i = tInteger.intValue();
            LicenseCount = i;
        }
    }

    public int getOtherCount()
    {
        return OtherCount;
    }

    public void setOtherCount(int aOtherCount)
    {
        OtherCount = aOtherCount;
    }

    public void setOtherCount(String aOtherCount)
    {
        if (aOtherCount != null && !aOtherCount.equals(""))
        {
            Integer tInteger = new Integer(aOtherCount);
            int i = tInteger.intValue();
            OtherCount = i;
        }
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LAComTransLogSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LAComTransLogSchema aLAComTransLogSchema)
    {
        this.AppID = aLAComTransLogSchema.getAppID();
        this.AppIDType = aLAComTransLogSchema.getAppIDType();
        this.Year = aLAComTransLogSchema.getYear();
        this.Times = aLAComTransLogSchema.getTimes();
        this.RecvUnitCode = aLAComTransLogSchema.getRecvUnitCode();
        this.SendUnitCode = aLAComTransLogSchema.getSendUnitCode();
        this.SendDate = fDate.getDate(aLAComTransLogSchema.getSendDate());
        this.Sender = aLAComTransLogSchema.getSender();
        this.OrgAppID = aLAComTransLogSchema.getOrgAppID();
        this.NewComCount = aLAComTransLogSchema.getNewComCount();
        this.InfoChgCount = aLAComTransLogSchema.getInfoChgCount();
        this.RiskChgCount = aLAComTransLogSchema.getRiskChgCount();
        this.BlacklistCount = aLAComTransLogSchema.getBlacklistCount();
        this.BuildRelaCount = aLAComTransLogSchema.getBuildRelaCount();
        this.StopRelaCount = aLAComTransLogSchema.getStopRelaCount();
        this.ChgRelaCount = aLAComTransLogSchema.getChgRelaCount();
        this.LicenseCount = aLAComTransLogSchema.getLicenseCount();
        this.OtherCount = aLAComTransLogSchema.getOtherCount();
        this.Operator = aLAComTransLogSchema.getOperator();
        this.MakeDate = fDate.getDate(aLAComTransLogSchema.getMakeDate());
        this.MakeTime = aLAComTransLogSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLAComTransLogSchema.getModifyDate());
        this.ModifyTime = aLAComTransLogSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AppID") == null)
            {
                this.AppID = null;
            }
            else
            {
                this.AppID = rs.getString("AppID").trim();
            }

            if (rs.getString("AppIDType") == null)
            {
                this.AppIDType = null;
            }
            else
            {
                this.AppIDType = rs.getString("AppIDType").trim();
            }

            this.Year = rs.getInt("Year");
            this.Times = rs.getInt("Times");
            if (rs.getString("RecvUnitCode") == null)
            {
                this.RecvUnitCode = null;
            }
            else
            {
                this.RecvUnitCode = rs.getString("RecvUnitCode").trim();
            }

            if (rs.getString("SendUnitCode") == null)
            {
                this.SendUnitCode = null;
            }
            else
            {
                this.SendUnitCode = rs.getString("SendUnitCode").trim();
            }

            this.SendDate = rs.getDate("SendDate");
            if (rs.getString("Sender") == null)
            {
                this.Sender = null;
            }
            else
            {
                this.Sender = rs.getString("Sender").trim();
            }

            if (rs.getString("OrgAppID") == null)
            {
                this.OrgAppID = null;
            }
            else
            {
                this.OrgAppID = rs.getString("OrgAppID").trim();
            }

            this.NewComCount = rs.getInt("NewComCount");
            this.InfoChgCount = rs.getInt("InfoChgCount");
            this.RiskChgCount = rs.getInt("RiskChgCount");
            this.BlacklistCount = rs.getInt("BlacklistCount");
            this.BuildRelaCount = rs.getInt("BuildRelaCount");
            this.StopRelaCount = rs.getInt("StopRelaCount");
            this.ChgRelaCount = rs.getInt("ChgRelaCount");
            this.LicenseCount = rs.getInt("LicenseCount");
            this.OtherCount = rs.getInt("OtherCount");
            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComTransLogSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAComTransLogSchema getSchema()
    {
        LAComTransLogSchema aLAComTransLogSchema = new LAComTransLogSchema();
        aLAComTransLogSchema.setSchema(this);
        return aLAComTransLogSchema;
    }

    public LAComTransLogDB getDB()
    {
        LAComTransLogDB aDBOper = new LAComTransLogDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAComTransLog描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AppID)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppIDType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Year) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(Times) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RecvUnitCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SendUnitCode)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(SendDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Sender)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OrgAppID)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(NewComCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(InfoChgCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(RiskChgCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(BlacklistCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(BuildRelaCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(StopRelaCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(ChgRelaCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(LicenseCount) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(OtherCount) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAComTransLog>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AppID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            AppIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            Year = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    3, SysConst.PACKAGESPILTER))).intValue();
            Times = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    4, SysConst.PACKAGESPILTER))).intValue();
            RecvUnitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            SendUnitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            SendDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            Sender = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                    SysConst.PACKAGESPILTER);
            OrgAppID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            NewComCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            InfoChgCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).intValue();
            RiskChgCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).intValue();
            BlacklistCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 13, SysConst.PACKAGESPILTER))).intValue();
            BuildRelaCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).intValue();
            StopRelaCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 15, SysConst.PACKAGESPILTER))).intValue();
            ChgRelaCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 16, SysConst.PACKAGESPILTER))).intValue();
            LicenseCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).intValue();
            OtherCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).intValue();
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComTransLogSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AppID"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppID));
        }
        if (FCode.equals("AppIDType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppIDType));
        }
        if (FCode.equals("Year"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Year));
        }
        if (FCode.equals("Times"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Times));
        }
        if (FCode.equals("RecvUnitCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecvUnitCode));
        }
        if (FCode.equals("SendUnitCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SendUnitCode));
        }
        if (FCode.equals("SendDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getSendDate()));
        }
        if (FCode.equals("Sender"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sender));
        }
        if (FCode.equals("OrgAppID"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrgAppID));
        }
        if (FCode.equals("NewComCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NewComCount));
        }
        if (FCode.equals("InfoChgCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InfoChgCount));
        }
        if (FCode.equals("RiskChgCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RiskChgCount));
        }
        if (FCode.equals("BlacklistCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BlacklistCount));
        }
        if (FCode.equals("BuildRelaCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BuildRelaCount));
        }
        if (FCode.equals("StopRelaCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(StopRelaCount));
        }
        if (FCode.equals("ChgRelaCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ChgRelaCount));
        }
        if (FCode.equals("LicenseCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LicenseCount));
        }
        if (FCode.equals("OtherCount"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherCount));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AppID);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AppIDType);
                break;
            case 2:
                strFieldValue = String.valueOf(Year);
                break;
            case 3:
                strFieldValue = String.valueOf(Times);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(RecvUnitCode);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(SendUnitCode);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getSendDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Sender);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(OrgAppID);
                break;
            case 9:
                strFieldValue = String.valueOf(NewComCount);
                break;
            case 10:
                strFieldValue = String.valueOf(InfoChgCount);
                break;
            case 11:
                strFieldValue = String.valueOf(RiskChgCount);
                break;
            case 12:
                strFieldValue = String.valueOf(BlacklistCount);
                break;
            case 13:
                strFieldValue = String.valueOf(BuildRelaCount);
                break;
            case 14:
                strFieldValue = String.valueOf(StopRelaCount);
                break;
            case 15:
                strFieldValue = String.valueOf(ChgRelaCount);
                break;
            case 16:
                strFieldValue = String.valueOf(LicenseCount);
                break;
            case 17:
                strFieldValue = String.valueOf(OtherCount);
                break;
            case 18:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 19:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AppID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppID = FValue.trim();
            }
            else
            {
                AppID = null;
            }
        }
        if (FCode.equals("AppIDType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppIDType = FValue.trim();
            }
            else
            {
                AppIDType = null;
            }
        }
        if (FCode.equals("Year"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Year = i;
            }
        }
        if (FCode.equals("Times"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Times = i;
            }
        }
        if (FCode.equals("RecvUnitCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RecvUnitCode = FValue.trim();
            }
            else
            {
                RecvUnitCode = null;
            }
        }
        if (FCode.equals("SendUnitCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SendUnitCode = FValue.trim();
            }
            else
            {
                SendUnitCode = null;
            }
        }
        if (FCode.equals("SendDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SendDate = fDate.getDate(FValue);
            }
            else
            {
                SendDate = null;
            }
        }
        if (FCode.equals("Sender"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Sender = FValue.trim();
            }
            else
            {
                Sender = null;
            }
        }
        if (FCode.equals("OrgAppID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OrgAppID = FValue.trim();
            }
            else
            {
                OrgAppID = null;
            }
        }
        if (FCode.equals("NewComCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                NewComCount = i;
            }
        }
        if (FCode.equals("InfoChgCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                InfoChgCount = i;
            }
        }
        if (FCode.equals("RiskChgCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                RiskChgCount = i;
            }
        }
        if (FCode.equals("BlacklistCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                BlacklistCount = i;
            }
        }
        if (FCode.equals("BuildRelaCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                BuildRelaCount = i;
            }
        }
        if (FCode.equals("StopRelaCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                StopRelaCount = i;
            }
        }
        if (FCode.equals("ChgRelaCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ChgRelaCount = i;
            }
        }
        if (FCode.equals("LicenseCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                LicenseCount = i;
            }
        }
        if (FCode.equals("OtherCount"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                OtherCount = i;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAComTransLogSchema other = (LAComTransLogSchema) otherObject;
        return
                AppID.equals(other.getAppID())
                && AppIDType.equals(other.getAppIDType())
                && Year == other.getYear()
                && Times == other.getTimes()
                && RecvUnitCode.equals(other.getRecvUnitCode())
                && SendUnitCode.equals(other.getSendUnitCode())
                && fDate.getString(SendDate).equals(other.getSendDate())
                && Sender.equals(other.getSender())
                && OrgAppID.equals(other.getOrgAppID())
                && NewComCount == other.getNewComCount()
                && InfoChgCount == other.getInfoChgCount()
                && RiskChgCount == other.getRiskChgCount()
                && BlacklistCount == other.getBlacklistCount()
                && BuildRelaCount == other.getBuildRelaCount()
                && StopRelaCount == other.getStopRelaCount()
                && ChgRelaCount == other.getChgRelaCount()
                && LicenseCount == other.getLicenseCount()
                && OtherCount == other.getOtherCount()
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return 0;
        }
        if (strFieldName.equals("AppIDType"))
        {
            return 1;
        }
        if (strFieldName.equals("Year"))
        {
            return 2;
        }
        if (strFieldName.equals("Times"))
        {
            return 3;
        }
        if (strFieldName.equals("RecvUnitCode"))
        {
            return 4;
        }
        if (strFieldName.equals("SendUnitCode"))
        {
            return 5;
        }
        if (strFieldName.equals("SendDate"))
        {
            return 6;
        }
        if (strFieldName.equals("Sender"))
        {
            return 7;
        }
        if (strFieldName.equals("OrgAppID"))
        {
            return 8;
        }
        if (strFieldName.equals("NewComCount"))
        {
            return 9;
        }
        if (strFieldName.equals("InfoChgCount"))
        {
            return 10;
        }
        if (strFieldName.equals("RiskChgCount"))
        {
            return 11;
        }
        if (strFieldName.equals("BlacklistCount"))
        {
            return 12;
        }
        if (strFieldName.equals("BuildRelaCount"))
        {
            return 13;
        }
        if (strFieldName.equals("StopRelaCount"))
        {
            return 14;
        }
        if (strFieldName.equals("ChgRelaCount"))
        {
            return 15;
        }
        if (strFieldName.equals("LicenseCount"))
        {
            return 16;
        }
        if (strFieldName.equals("OtherCount"))
        {
            return 17;
        }
        if (strFieldName.equals("Operator"))
        {
            return 18;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 19;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 20;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 21;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 22;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AppID";
                break;
            case 1:
                strFieldName = "AppIDType";
                break;
            case 2:
                strFieldName = "Year";
                break;
            case 3:
                strFieldName = "Times";
                break;
            case 4:
                strFieldName = "RecvUnitCode";
                break;
            case 5:
                strFieldName = "SendUnitCode";
                break;
            case 6:
                strFieldName = "SendDate";
                break;
            case 7:
                strFieldName = "Sender";
                break;
            case 8:
                strFieldName = "OrgAppID";
                break;
            case 9:
                strFieldName = "NewComCount";
                break;
            case 10:
                strFieldName = "InfoChgCount";
                break;
            case 11:
                strFieldName = "RiskChgCount";
                break;
            case 12:
                strFieldName = "BlacklistCount";
                break;
            case 13:
                strFieldName = "BuildRelaCount";
                break;
            case 14:
                strFieldName = "StopRelaCount";
                break;
            case 15:
                strFieldName = "ChgRelaCount";
                break;
            case 16:
                strFieldName = "LicenseCount";
                break;
            case 17:
                strFieldName = "OtherCount";
                break;
            case 18:
                strFieldName = "Operator";
                break;
            case 19:
                strFieldName = "MakeDate";
                break;
            case 20:
                strFieldName = "MakeTime";
                break;
            case 21:
                strFieldName = "ModifyDate";
                break;
            case 22:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AppID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppIDType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Year"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Times"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RecvUnitCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SendUnitCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SendDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Sender"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OrgAppID"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NewComCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("InfoChgCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RiskChgCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("BlacklistCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("BuildRelaCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("StopRelaCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ChgRelaCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("LicenseCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("OtherCount"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_INT;
                break;
            case 3:
                nFieldType = Schema.TYPE_INT;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_INT;
                break;
            case 10:
                nFieldType = Schema.TYPE_INT;
                break;
            case 11:
                nFieldType = Schema.TYPE_INT;
                break;
            case 12:
                nFieldType = Schema.TYPE_INT;
                break;
            case 13:
                nFieldType = Schema.TYPE_INT;
                break;
            case 14:
                nFieldType = Schema.TYPE_INT;
                break;
            case 15:
                nFieldType = Schema.TYPE_INT;
                break;
            case 16:
                nFieldType = Schema.TYPE_INT;
                break;
            case 17:
                nFieldType = Schema.TYPE_INT;
                break;
            case 18:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 19:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
