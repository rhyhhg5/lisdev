/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDDoctorDB;

/*
 * <p>ClassName: LDDoctorSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2007-12-10
 */
public class LDDoctorSchema implements Schema, Cloneable
{
	// @Field
	/** 医师代码 */
	private String DoctNo;
	/** 医师姓名 */
	private String DoctName;
	/** 出生日期 */
	private Date Birthday;
	/** 性别代码 */
	private String Sex;
	/** 医师资格代码 */
	private String DoctTitCode;
	/** 技术职称代码 */
	private String TechPostCode;
	/** 学历代码 */
	private String EduLevelCode;
	/** 科室 */
	private String SecName;
	/** 从医年度 */
	private String DoctStartTime;
	/** 兼职情况 */
	private String PartTimeFlag;
	/** 联系电话 */
	private String DoctPhone;
	/** 电子信箱 */
	private String DoctEmail;
	/** 主要成就 */
	private String SpecialFLag;
	/** 业务专长 */
	private String TechSpec;
	/** 出诊时间 */
	private String DiagTime;
	/** 专家标志 */
	private String ExpertFlag;
	/** 是否咨询专家 */
	private String CExportFlag;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 签约标识 */
	private String FixFlag;
	/** 职业类型 */
	private String CarreerClass;
	/** 岗位说明 */
	private String JobExplain;
	/** 工作地点 */
	private String WorkPlace;
	/** 联系方式 */
	private String Contact;
	/** 医院代码 */
	private String HospitCode;
	/** 操作员 */
	private String Operator;
	/** 管理机构 */
	private String ManageCom;
	/** 出生年份 */
	private String BirthYear;
	/** 聘用方式 */
	private String EngageClass;

	public static final int FIELDNUM = 31;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDDoctorSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "DoctNo";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LDDoctorSchema cloned = (LDDoctorSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getDoctNo()
	{
		return DoctNo;
	}
	public void setDoctNo(String aDoctNo)
	{
            DoctNo = aDoctNo;
	}
	public String getDoctName()
	{
		return DoctName;
	}
	public void setDoctName(String aDoctName)
	{
            DoctName = aDoctName;
	}
	public String getBirthday()
	{
		if( Birthday != null )
			return fDate.getString(Birthday);
		else
			return null;
	}
	public void setBirthday(Date aBirthday)
	{
            Birthday = aBirthday;
	}
	public void setBirthday(String aBirthday)
	{
		if (aBirthday != null && !aBirthday.equals("") )
		{
			Birthday = fDate.getDate( aBirthday );
		}
		else
			Birthday = null;
	}

	public String getSex()
	{
		return Sex;
	}
	public void setSex(String aSex)
	{
            Sex = aSex;
	}
	public String getDoctTitCode()
	{
		return DoctTitCode;
	}
	public void setDoctTitCode(String aDoctTitCode)
	{
            DoctTitCode = aDoctTitCode;
	}
	public String getTechPostCode()
	{
		return TechPostCode;
	}
	public void setTechPostCode(String aTechPostCode)
	{
            TechPostCode = aTechPostCode;
	}
	public String getEduLevelCode()
	{
		return EduLevelCode;
	}
	public void setEduLevelCode(String aEduLevelCode)
	{
            EduLevelCode = aEduLevelCode;
	}
	public String getSecName()
	{
		return SecName;
	}
	public void setSecName(String aSecName)
	{
            SecName = aSecName;
	}
	public String getDoctStartTime()
	{
		return DoctStartTime;
	}
	public void setDoctStartTime(String aDoctStartTime)
	{
            DoctStartTime = aDoctStartTime;
	}
	public String getPartTimeFlag()
	{
		return PartTimeFlag;
	}
	public void setPartTimeFlag(String aPartTimeFlag)
	{
            PartTimeFlag = aPartTimeFlag;
	}
	public String getDoctPhone()
	{
		return DoctPhone;
	}
	public void setDoctPhone(String aDoctPhone)
	{
            DoctPhone = aDoctPhone;
	}
	public String getDoctEmail()
	{
		return DoctEmail;
	}
	public void setDoctEmail(String aDoctEmail)
	{
            DoctEmail = aDoctEmail;
	}
	public String getSpecialFLag()
	{
		return SpecialFLag;
	}
	public void setSpecialFLag(String aSpecialFLag)
	{
            SpecialFLag = aSpecialFLag;
	}
	public String getTechSpec()
	{
		return TechSpec;
	}
	public void setTechSpec(String aTechSpec)
	{
            TechSpec = aTechSpec;
	}
	public String getDiagTime()
	{
		return DiagTime;
	}
	public void setDiagTime(String aDiagTime)
	{
            DiagTime = aDiagTime;
	}
	public String getExpertFlag()
	{
		return ExpertFlag;
	}
	public void setExpertFlag(String aExpertFlag)
	{
            ExpertFlag = aExpertFlag;
	}
	public String getCExportFlag()
	{
		return CExportFlag;
	}
	public void setCExportFlag(String aCExportFlag)
	{
            CExportFlag = aCExportFlag;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}
	public String getFixFlag()
	{
		return FixFlag;
	}
	public void setFixFlag(String aFixFlag)
	{
            FixFlag = aFixFlag;
	}
	public String getCarreerClass()
	{
		return CarreerClass;
	}
	public void setCarreerClass(String aCarreerClass)
	{
            CarreerClass = aCarreerClass;
	}
	public String getJobExplain()
	{
		return JobExplain;
	}
	public void setJobExplain(String aJobExplain)
	{
            JobExplain = aJobExplain;
	}
	public String getWorkPlace()
	{
		return WorkPlace;
	}
	public void setWorkPlace(String aWorkPlace)
	{
            WorkPlace = aWorkPlace;
	}
	public String getContact()
	{
		return Contact;
	}
	public void setContact(String aContact)
	{
            Contact = aContact;
	}
	public String getHospitCode()
	{
		return HospitCode;
	}
	public void setHospitCode(String aHospitCode)
	{
            HospitCode = aHospitCode;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
            ManageCom = aManageCom;
	}
	public String getBirthYear()
	{
		return BirthYear;
	}
	public void setBirthYear(String aBirthYear)
	{
            BirthYear = aBirthYear;
	}
	public String getEngageClass()
	{
		return EngageClass;
	}
	public void setEngageClass(String aEngageClass)
	{
            EngageClass = aEngageClass;
	}

	/**
	* 使用另外一个 LDDoctorSchema 对象给 Schema 赋值
	* @param: aLDDoctorSchema LDDoctorSchema
	**/
	public void setSchema(LDDoctorSchema aLDDoctorSchema)
	{
		this.DoctNo = aLDDoctorSchema.getDoctNo();
		this.DoctName = aLDDoctorSchema.getDoctName();
		this.Birthday = fDate.getDate( aLDDoctorSchema.getBirthday());
		this.Sex = aLDDoctorSchema.getSex();
		this.DoctTitCode = aLDDoctorSchema.getDoctTitCode();
		this.TechPostCode = aLDDoctorSchema.getTechPostCode();
		this.EduLevelCode = aLDDoctorSchema.getEduLevelCode();
		this.SecName = aLDDoctorSchema.getSecName();
		this.DoctStartTime = aLDDoctorSchema.getDoctStartTime();
		this.PartTimeFlag = aLDDoctorSchema.getPartTimeFlag();
		this.DoctPhone = aLDDoctorSchema.getDoctPhone();
		this.DoctEmail = aLDDoctorSchema.getDoctEmail();
		this.SpecialFLag = aLDDoctorSchema.getSpecialFLag();
		this.TechSpec = aLDDoctorSchema.getTechSpec();
		this.DiagTime = aLDDoctorSchema.getDiagTime();
		this.ExpertFlag = aLDDoctorSchema.getExpertFlag();
		this.CExportFlag = aLDDoctorSchema.getCExportFlag();
		this.MakeDate = fDate.getDate( aLDDoctorSchema.getMakeDate());
		this.MakeTime = aLDDoctorSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLDDoctorSchema.getModifyDate());
		this.ModifyTime = aLDDoctorSchema.getModifyTime();
		this.FixFlag = aLDDoctorSchema.getFixFlag();
		this.CarreerClass = aLDDoctorSchema.getCarreerClass();
		this.JobExplain = aLDDoctorSchema.getJobExplain();
		this.WorkPlace = aLDDoctorSchema.getWorkPlace();
		this.Contact = aLDDoctorSchema.getContact();
		this.HospitCode = aLDDoctorSchema.getHospitCode();
		this.Operator = aLDDoctorSchema.getOperator();
		this.ManageCom = aLDDoctorSchema.getManageCom();
		this.BirthYear = aLDDoctorSchema.getBirthYear();
		this.EngageClass = aLDDoctorSchema.getEngageClass();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("DoctNo") == null )
				this.DoctNo = null;
			else
				this.DoctNo = rs.getString("DoctNo").trim();

			if( rs.getString("DoctName") == null )
				this.DoctName = null;
			else
				this.DoctName = rs.getString("DoctName").trim();

			this.Birthday = rs.getDate("Birthday");
			if( rs.getString("Sex") == null )
				this.Sex = null;
			else
				this.Sex = rs.getString("Sex").trim();

			if( rs.getString("DoctTitCode") == null )
				this.DoctTitCode = null;
			else
				this.DoctTitCode = rs.getString("DoctTitCode").trim();

			if( rs.getString("TechPostCode") == null )
				this.TechPostCode = null;
			else
				this.TechPostCode = rs.getString("TechPostCode").trim();

			if( rs.getString("EduLevelCode") == null )
				this.EduLevelCode = null;
			else
				this.EduLevelCode = rs.getString("EduLevelCode").trim();

			if( rs.getString("SecName") == null )
				this.SecName = null;
			else
				this.SecName = rs.getString("SecName").trim();

			if( rs.getString("DoctStartTime") == null )
				this.DoctStartTime = null;
			else
				this.DoctStartTime = rs.getString("DoctStartTime").trim();

			if( rs.getString("PartTimeFlag") == null )
				this.PartTimeFlag = null;
			else
				this.PartTimeFlag = rs.getString("PartTimeFlag").trim();

			if( rs.getString("DoctPhone") == null )
				this.DoctPhone = null;
			else
				this.DoctPhone = rs.getString("DoctPhone").trim();

			if( rs.getString("DoctEmail") == null )
				this.DoctEmail = null;
			else
				this.DoctEmail = rs.getString("DoctEmail").trim();

			if( rs.getString("SpecialFLag") == null )
				this.SpecialFLag = null;
			else
				this.SpecialFLag = rs.getString("SpecialFLag").trim();

			if( rs.getString("TechSpec") == null )
				this.TechSpec = null;
			else
				this.TechSpec = rs.getString("TechSpec").trim();

			if( rs.getString("DiagTime") == null )
				this.DiagTime = null;
			else
				this.DiagTime = rs.getString("DiagTime").trim();

			if( rs.getString("ExpertFlag") == null )
				this.ExpertFlag = null;
			else
				this.ExpertFlag = rs.getString("ExpertFlag").trim();

			if( rs.getString("CExportFlag") == null )
				this.CExportFlag = null;
			else
				this.CExportFlag = rs.getString("CExportFlag").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("FixFlag") == null )
				this.FixFlag = null;
			else
				this.FixFlag = rs.getString("FixFlag").trim();

			if( rs.getString("CarreerClass") == null )
				this.CarreerClass = null;
			else
				this.CarreerClass = rs.getString("CarreerClass").trim();

			if( rs.getString("JobExplain") == null )
				this.JobExplain = null;
			else
				this.JobExplain = rs.getString("JobExplain").trim();

			if( rs.getString("WorkPlace") == null )
				this.WorkPlace = null;
			else
				this.WorkPlace = rs.getString("WorkPlace").trim();

			if( rs.getString("Contact") == null )
				this.Contact = null;
			else
				this.Contact = rs.getString("Contact").trim();

			if( rs.getString("HospitCode") == null )
				this.HospitCode = null;
			else
				this.HospitCode = rs.getString("HospitCode").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("BirthYear") == null )
				this.BirthYear = null;
			else
				this.BirthYear = rs.getString("BirthYear").trim();

			if( rs.getString("EngageClass") == null )
				this.EngageClass = null;
			else
				this.EngageClass = rs.getString("EngageClass").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDDoctor表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDDoctorSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDDoctorSchema getSchema()
	{
		LDDoctorSchema aLDDoctorSchema = new LDDoctorSchema();
		aLDDoctorSchema.setSchema(this);
		return aLDDoctorSchema;
	}

	public LDDoctorDB getDB()
	{
		LDDoctorDB aDBOper = new LDDoctorDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDoctor描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(DoctNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DoctName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( Birthday ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Sex)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DoctTitCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TechPostCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(EduLevelCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SecName)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DoctStartTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(PartTimeFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DoctPhone)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DoctEmail)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(SpecialFLag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(TechSpec)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DiagTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ExpertFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CExportFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(FixFlag)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(CarreerClass)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(JobExplain)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(WorkPlace)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Contact)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(HospitCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(BirthYear)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(EngageClass));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDoctor>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			DoctNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			DoctName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			DoctTitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			TechPostCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			EduLevelCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			SecName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			DoctStartTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			PartTimeFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			DoctPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			DoctEmail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			SpecialFLag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			TechSpec = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			DiagTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ExpertFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			CExportFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			FixFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			CarreerClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			JobExplain = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			WorkPlace = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			Contact = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			HospitCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			BirthYear = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			EngageClass = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDDoctorSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("DoctNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctNo));
		}
		if (FCode.equals("DoctName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctName));
		}
		if (FCode.equals("Birthday"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
		}
		if (FCode.equals("Sex"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
		}
		if (FCode.equals("DoctTitCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctTitCode));
		}
		if (FCode.equals("TechPostCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TechPostCode));
		}
		if (FCode.equals("EduLevelCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EduLevelCode));
		}
		if (FCode.equals("SecName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SecName));
		}
		if (FCode.equals("DoctStartTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctStartTime));
		}
		if (FCode.equals("PartTimeFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PartTimeFlag));
		}
		if (FCode.equals("DoctPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctPhone));
		}
		if (FCode.equals("DoctEmail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DoctEmail));
		}
		if (FCode.equals("SpecialFLag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SpecialFLag));
		}
		if (FCode.equals("TechSpec"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TechSpec));
		}
		if (FCode.equals("DiagTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiagTime));
		}
		if (FCode.equals("ExpertFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExpertFlag));
		}
		if (FCode.equals("CExportFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CExportFlag));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("FixFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FixFlag));
		}
		if (FCode.equals("CarreerClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CarreerClass));
		}
		if (FCode.equals("JobExplain"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(JobExplain));
		}
		if (FCode.equals("WorkPlace"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WorkPlace));
		}
		if (FCode.equals("Contact"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Contact));
		}
		if (FCode.equals("HospitCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HospitCode));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("BirthYear"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BirthYear));
		}
		if (FCode.equals("EngageClass"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EngageClass));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(DoctNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(DoctName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBirthday()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Sex);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(DoctTitCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(TechPostCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(EduLevelCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(SecName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(DoctStartTime);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(PartTimeFlag);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(DoctPhone);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(DoctEmail);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(SpecialFLag);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(TechSpec);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(DiagTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ExpertFlag);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(CExportFlag);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(FixFlag);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(CarreerClass);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(JobExplain);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(WorkPlace);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(Contact);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(HospitCode);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(BirthYear);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(EngageClass);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("DoctNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctNo = FValue.trim();
			}
			else
				DoctNo = null;
		}
		if (FCode.equalsIgnoreCase("DoctName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctName = FValue.trim();
			}
			else
				DoctName = null;
		}
		if (FCode.equalsIgnoreCase("Birthday"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Birthday = fDate.getDate( FValue );
			}
			else
				Birthday = null;
		}
		if (FCode.equalsIgnoreCase("Sex"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sex = FValue.trim();
			}
			else
				Sex = null;
		}
		if (FCode.equalsIgnoreCase("DoctTitCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctTitCode = FValue.trim();
			}
			else
				DoctTitCode = null;
		}
		if (FCode.equalsIgnoreCase("TechPostCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TechPostCode = FValue.trim();
			}
			else
				TechPostCode = null;
		}
		if (FCode.equalsIgnoreCase("EduLevelCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EduLevelCode = FValue.trim();
			}
			else
				EduLevelCode = null;
		}
		if (FCode.equalsIgnoreCase("SecName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SecName = FValue.trim();
			}
			else
				SecName = null;
		}
		if (FCode.equalsIgnoreCase("DoctStartTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctStartTime = FValue.trim();
			}
			else
				DoctStartTime = null;
		}
		if (FCode.equalsIgnoreCase("PartTimeFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PartTimeFlag = FValue.trim();
			}
			else
				PartTimeFlag = null;
		}
		if (FCode.equalsIgnoreCase("DoctPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctPhone = FValue.trim();
			}
			else
				DoctPhone = null;
		}
		if (FCode.equalsIgnoreCase("DoctEmail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DoctEmail = FValue.trim();
			}
			else
				DoctEmail = null;
		}
		if (FCode.equalsIgnoreCase("SpecialFLag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SpecialFLag = FValue.trim();
			}
			else
				SpecialFLag = null;
		}
		if (FCode.equalsIgnoreCase("TechSpec"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TechSpec = FValue.trim();
			}
			else
				TechSpec = null;
		}
		if (FCode.equalsIgnoreCase("DiagTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DiagTime = FValue.trim();
			}
			else
				DiagTime = null;
		}
		if (FCode.equalsIgnoreCase("ExpertFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ExpertFlag = FValue.trim();
			}
			else
				ExpertFlag = null;
		}
		if (FCode.equalsIgnoreCase("CExportFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CExportFlag = FValue.trim();
			}
			else
				CExportFlag = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("FixFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FixFlag = FValue.trim();
			}
			else
				FixFlag = null;
		}
		if (FCode.equalsIgnoreCase("CarreerClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CarreerClass = FValue.trim();
			}
			else
				CarreerClass = null;
		}
		if (FCode.equalsIgnoreCase("JobExplain"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				JobExplain = FValue.trim();
			}
			else
				JobExplain = null;
		}
		if (FCode.equalsIgnoreCase("WorkPlace"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WorkPlace = FValue.trim();
			}
			else
				WorkPlace = null;
		}
		if (FCode.equalsIgnoreCase("Contact"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Contact = FValue.trim();
			}
			else
				Contact = null;
		}
		if (FCode.equalsIgnoreCase("HospitCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HospitCode = FValue.trim();
			}
			else
				HospitCode = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("BirthYear"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BirthYear = FValue.trim();
			}
			else
				BirthYear = null;
		}
		if (FCode.equalsIgnoreCase("EngageClass"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EngageClass = FValue.trim();
			}
			else
				EngageClass = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDDoctorSchema other = (LDDoctorSchema)otherObject;
		return
			DoctNo.equals(other.getDoctNo())
			&& DoctName.equals(other.getDoctName())
			&& fDate.getString(Birthday).equals(other.getBirthday())
			&& Sex.equals(other.getSex())
			&& DoctTitCode.equals(other.getDoctTitCode())
			&& TechPostCode.equals(other.getTechPostCode())
			&& EduLevelCode.equals(other.getEduLevelCode())
			&& SecName.equals(other.getSecName())
			&& DoctStartTime.equals(other.getDoctStartTime())
			&& PartTimeFlag.equals(other.getPartTimeFlag())
			&& DoctPhone.equals(other.getDoctPhone())
			&& DoctEmail.equals(other.getDoctEmail())
			&& SpecialFLag.equals(other.getSpecialFLag())
			&& TechSpec.equals(other.getTechSpec())
			&& DiagTime.equals(other.getDiagTime())
			&& ExpertFlag.equals(other.getExpertFlag())
			&& CExportFlag.equals(other.getCExportFlag())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime())
			&& FixFlag.equals(other.getFixFlag())
			&& CarreerClass.equals(other.getCarreerClass())
			&& JobExplain.equals(other.getJobExplain())
			&& WorkPlace.equals(other.getWorkPlace())
			&& Contact.equals(other.getContact())
			&& HospitCode.equals(other.getHospitCode())
			&& Operator.equals(other.getOperator())
			&& ManageCom.equals(other.getManageCom())
			&& BirthYear.equals(other.getBirthYear())
			&& EngageClass.equals(other.getEngageClass());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("DoctNo") ) {
			return 0;
		}
		if( strFieldName.equals("DoctName") ) {
			return 1;
		}
		if( strFieldName.equals("Birthday") ) {
			return 2;
		}
		if( strFieldName.equals("Sex") ) {
			return 3;
		}
		if( strFieldName.equals("DoctTitCode") ) {
			return 4;
		}
		if( strFieldName.equals("TechPostCode") ) {
			return 5;
		}
		if( strFieldName.equals("EduLevelCode") ) {
			return 6;
		}
		if( strFieldName.equals("SecName") ) {
			return 7;
		}
		if( strFieldName.equals("DoctStartTime") ) {
			return 8;
		}
		if( strFieldName.equals("PartTimeFlag") ) {
			return 9;
		}
		if( strFieldName.equals("DoctPhone") ) {
			return 10;
		}
		if( strFieldName.equals("DoctEmail") ) {
			return 11;
		}
		if( strFieldName.equals("SpecialFLag") ) {
			return 12;
		}
		if( strFieldName.equals("TechSpec") ) {
			return 13;
		}
		if( strFieldName.equals("DiagTime") ) {
			return 14;
		}
		if( strFieldName.equals("ExpertFlag") ) {
			return 15;
		}
		if( strFieldName.equals("CExportFlag") ) {
			return 16;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 17;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 20;
		}
		if( strFieldName.equals("FixFlag") ) {
			return 21;
		}
		if( strFieldName.equals("CarreerClass") ) {
			return 22;
		}
		if( strFieldName.equals("JobExplain") ) {
			return 23;
		}
		if( strFieldName.equals("WorkPlace") ) {
			return 24;
		}
		if( strFieldName.equals("Contact") ) {
			return 25;
		}
		if( strFieldName.equals("HospitCode") ) {
			return 26;
		}
		if( strFieldName.equals("Operator") ) {
			return 27;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 28;
		}
		if( strFieldName.equals("BirthYear") ) {
			return 29;
		}
		if( strFieldName.equals("EngageClass") ) {
			return 30;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "DoctNo";
				break;
			case 1:
				strFieldName = "DoctName";
				break;
			case 2:
				strFieldName = "Birthday";
				break;
			case 3:
				strFieldName = "Sex";
				break;
			case 4:
				strFieldName = "DoctTitCode";
				break;
			case 5:
				strFieldName = "TechPostCode";
				break;
			case 6:
				strFieldName = "EduLevelCode";
				break;
			case 7:
				strFieldName = "SecName";
				break;
			case 8:
				strFieldName = "DoctStartTime";
				break;
			case 9:
				strFieldName = "PartTimeFlag";
				break;
			case 10:
				strFieldName = "DoctPhone";
				break;
			case 11:
				strFieldName = "DoctEmail";
				break;
			case 12:
				strFieldName = "SpecialFLag";
				break;
			case 13:
				strFieldName = "TechSpec";
				break;
			case 14:
				strFieldName = "DiagTime";
				break;
			case 15:
				strFieldName = "ExpertFlag";
				break;
			case 16:
				strFieldName = "CExportFlag";
				break;
			case 17:
				strFieldName = "MakeDate";
				break;
			case 18:
				strFieldName = "MakeTime";
				break;
			case 19:
				strFieldName = "ModifyDate";
				break;
			case 20:
				strFieldName = "ModifyTime";
				break;
			case 21:
				strFieldName = "FixFlag";
				break;
			case 22:
				strFieldName = "CarreerClass";
				break;
			case 23:
				strFieldName = "JobExplain";
				break;
			case 24:
				strFieldName = "WorkPlace";
				break;
			case 25:
				strFieldName = "Contact";
				break;
			case 26:
				strFieldName = "HospitCode";
				break;
			case 27:
				strFieldName = "Operator";
				break;
			case 28:
				strFieldName = "ManageCom";
				break;
			case 29:
				strFieldName = "BirthYear";
				break;
			case 30:
				strFieldName = "EngageClass";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("DoctNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoctName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Birthday") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Sex") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoctTitCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TechPostCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EduLevelCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SecName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoctStartTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PartTimeFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoctPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DoctEmail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SpecialFLag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TechSpec") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DiagTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ExpertFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CExportFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FixFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CarreerClass") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("JobExplain") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WorkPlace") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Contact") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("HospitCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BirthYear") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EngageClass") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
