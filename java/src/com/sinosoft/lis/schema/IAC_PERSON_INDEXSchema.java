/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.IAC_PERSON_INDEXDB;

/*
 * <p>ClassName: IAC_PERSON_INDEXSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2017-05-23
 */
public class IAC_PERSON_INDEXSchema implements Schema, Cloneable
{
	// @Field
	/** 同步批次号 */
	private String BatchNo;
	/** 上传日期 */
	private Date UploadDate;
	/** 报送日期 */
	private Date ReportDate;
	/** 已报送组数 */
	private int ReportCount;
	/** 客户总数 */
	private int CustomerCount;
	/** 分组数量 */
	private int GroupCount;
	/** 报送状态 */
	private String ReportState;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 备注 */
	private String Remark;
	/** 备用属性字段1 */
	private String StandbyFlag1;
	/** 备用属性字段2 */
	private String StandbyFlag2;
	/** 备用属性字段3 */
	private String StandbyFlag3;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public IAC_PERSON_INDEXSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BatchNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		IAC_PERSON_INDEXSchema cloned = (IAC_PERSON_INDEXSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBatchNo()
	{
		return BatchNo;
	}
	public void setBatchNo(String aBatchNo)
	{
		BatchNo = aBatchNo;
	}
	public String getUploadDate()
	{
		if( UploadDate != null )
			return fDate.getString(UploadDate);
		else
			return null;
	}
	public void setUploadDate(Date aUploadDate)
	{
		UploadDate = aUploadDate;
	}
	public void setUploadDate(String aUploadDate)
	{
		if (aUploadDate != null && !aUploadDate.equals("") )
		{
			UploadDate = fDate.getDate( aUploadDate );
		}
		else
			UploadDate = null;
	}

	public String getReportDate()
	{
		if( ReportDate != null )
			return fDate.getString(ReportDate);
		else
			return null;
	}
	public void setReportDate(Date aReportDate)
	{
		ReportDate = aReportDate;
	}
	public void setReportDate(String aReportDate)
	{
		if (aReportDate != null && !aReportDate.equals("") )
		{
			ReportDate = fDate.getDate( aReportDate );
		}
		else
			ReportDate = null;
	}

	public int getReportCount()
	{
		return ReportCount;
	}
	public void setReportCount(int aReportCount)
	{
		ReportCount = aReportCount;
	}
	public void setReportCount(String aReportCount)
	{
		if (aReportCount != null && !aReportCount.equals(""))
		{
			Integer tInteger = new Integer(aReportCount);
			int i = tInteger.intValue();
			ReportCount = i;
		}
	}

	public int getCustomerCount()
	{
		return CustomerCount;
	}
	public void setCustomerCount(int aCustomerCount)
	{
		CustomerCount = aCustomerCount;
	}
	public void setCustomerCount(String aCustomerCount)
	{
		if (aCustomerCount != null && !aCustomerCount.equals(""))
		{
			Integer tInteger = new Integer(aCustomerCount);
			int i = tInteger.intValue();
			CustomerCount = i;
		}
	}

	public int getGroupCount()
	{
		return GroupCount;
	}
	public void setGroupCount(int aGroupCount)
	{
		GroupCount = aGroupCount;
	}
	public void setGroupCount(String aGroupCount)
	{
		if (aGroupCount != null && !aGroupCount.equals(""))
		{
			Integer tInteger = new Integer(aGroupCount);
			int i = tInteger.intValue();
			GroupCount = i;
		}
	}

	public String getReportState()
	{
		return ReportState;
	}
	public void setReportState(String aReportState)
	{
		ReportState = aReportState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getStandbyFlag1()
	{
		return StandbyFlag1;
	}
	public void setStandbyFlag1(String aStandbyFlag1)
	{
		StandbyFlag1 = aStandbyFlag1;
	}
	public String getStandbyFlag2()
	{
		return StandbyFlag2;
	}
	public void setStandbyFlag2(String aStandbyFlag2)
	{
		StandbyFlag2 = aStandbyFlag2;
	}
	public String getStandbyFlag3()
	{
		return StandbyFlag3;
	}
	public void setStandbyFlag3(String aStandbyFlag3)
	{
		StandbyFlag3 = aStandbyFlag3;
	}

	/**
	* 使用另外一个 IAC_PERSON_INDEXSchema 对象给 Schema 赋值
	* @param: aIAC_PERSON_INDEXSchema IAC_PERSON_INDEXSchema
	**/
	public void setSchema(IAC_PERSON_INDEXSchema aIAC_PERSON_INDEXSchema)
	{
		this.BatchNo = aIAC_PERSON_INDEXSchema.getBatchNo();
		this.UploadDate = fDate.getDate( aIAC_PERSON_INDEXSchema.getUploadDate());
		this.ReportDate = fDate.getDate( aIAC_PERSON_INDEXSchema.getReportDate());
		this.ReportCount = aIAC_PERSON_INDEXSchema.getReportCount();
		this.CustomerCount = aIAC_PERSON_INDEXSchema.getCustomerCount();
		this.GroupCount = aIAC_PERSON_INDEXSchema.getGroupCount();
		this.ReportState = aIAC_PERSON_INDEXSchema.getReportState();
		this.Operator = aIAC_PERSON_INDEXSchema.getOperator();
		this.MakeDate = fDate.getDate( aIAC_PERSON_INDEXSchema.getMakeDate());
		this.MakeTime = aIAC_PERSON_INDEXSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aIAC_PERSON_INDEXSchema.getModifyDate());
		this.ModifyTime = aIAC_PERSON_INDEXSchema.getModifyTime();
		this.Remark = aIAC_PERSON_INDEXSchema.getRemark();
		this.StandbyFlag1 = aIAC_PERSON_INDEXSchema.getStandbyFlag1();
		this.StandbyFlag2 = aIAC_PERSON_INDEXSchema.getStandbyFlag2();
		this.StandbyFlag3 = aIAC_PERSON_INDEXSchema.getStandbyFlag3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BatchNo") == null )
				this.BatchNo = null;
			else
				this.BatchNo = rs.getString("BatchNo").trim();

			this.UploadDate = rs.getDate("UploadDate");
			this.ReportDate = rs.getDate("ReportDate");
			this.ReportCount = rs.getInt("ReportCount");
			this.CustomerCount = rs.getInt("CustomerCount");
			this.GroupCount = rs.getInt("GroupCount");
			if( rs.getString("ReportState") == null )
				this.ReportState = null;
			else
				this.ReportState = rs.getString("ReportState").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("StandbyFlag1") == null )
				this.StandbyFlag1 = null;
			else
				this.StandbyFlag1 = rs.getString("StandbyFlag1").trim();

			if( rs.getString("StandbyFlag2") == null )
				this.StandbyFlag2 = null;
			else
				this.StandbyFlag2 = rs.getString("StandbyFlag2").trim();

			if( rs.getString("StandbyFlag3") == null )
				this.StandbyFlag3 = null;
			else
				this.StandbyFlag3 = rs.getString("StandbyFlag3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的IAC_PERSON_INDEX表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "IAC_PERSON_INDEXSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public IAC_PERSON_INDEXSchema getSchema()
	{
		IAC_PERSON_INDEXSchema aIAC_PERSON_INDEXSchema = new IAC_PERSON_INDEXSchema();
		aIAC_PERSON_INDEXSchema.setSchema(this);
		return aIAC_PERSON_INDEXSchema;
	}

	public IAC_PERSON_INDEXDB getDB()
	{
		IAC_PERSON_INDEXDB aDBOper = new IAC_PERSON_INDEXDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpIAC_PERSON_INDEX描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BatchNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( UploadDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ReportDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ReportCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CustomerCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GroupCount));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReportState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandbyFlag3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpIAC_PERSON_INDEX>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BatchNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			UploadDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,SysConst.PACKAGESPILTER));
			ReportDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			ReportCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			CustomerCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).intValue();
			GroupCount= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).intValue();
			ReportState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			StandbyFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			StandbyFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			StandbyFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "IAC_PERSON_INDEXSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BatchNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BatchNo));
		}
		if (FCode.equals("UploadDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getUploadDate()));
		}
		if (FCode.equals("ReportDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReportDate()));
		}
		if (FCode.equals("ReportCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReportCount));
		}
		if (FCode.equals("CustomerCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerCount));
		}
		if (FCode.equals("GroupCount"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GroupCount));
		}
		if (FCode.equals("ReportState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReportState));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("StandbyFlag1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag1));
		}
		if (FCode.equals("StandbyFlag2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag2));
		}
		if (FCode.equals("StandbyFlag3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandbyFlag3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BatchNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getUploadDate()));
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReportDate()));
				break;
			case 3:
				strFieldValue = String.valueOf(ReportCount);
				break;
			case 4:
				strFieldValue = String.valueOf(CustomerCount);
				break;
			case 5:
				strFieldValue = String.valueOf(GroupCount);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ReportState);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag1);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag2);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(StandbyFlag3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BatchNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BatchNo = FValue.trim();
			}
			else
				BatchNo = null;
		}
		if (FCode.equalsIgnoreCase("UploadDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				UploadDate = fDate.getDate( FValue );
			}
			else
				UploadDate = null;
		}
		if (FCode.equalsIgnoreCase("ReportDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ReportDate = fDate.getDate( FValue );
			}
			else
				ReportDate = null;
		}
		if (FCode.equalsIgnoreCase("ReportCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ReportCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("CustomerCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				CustomerCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("GroupCount"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				GroupCount = i;
			}
		}
		if (FCode.equalsIgnoreCase("ReportState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReportState = FValue.trim();
			}
			else
				ReportState = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag1 = FValue.trim();
			}
			else
				StandbyFlag1 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag2 = FValue.trim();
			}
			else
				StandbyFlag2 = null;
		}
		if (FCode.equalsIgnoreCase("StandbyFlag3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandbyFlag3 = FValue.trim();
			}
			else
				StandbyFlag3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		IAC_PERSON_INDEXSchema other = (IAC_PERSON_INDEXSchema)otherObject;
		return
			(BatchNo == null ? other.getBatchNo() == null : BatchNo.equals(other.getBatchNo()))
			&& (UploadDate == null ? other.getUploadDate() == null : fDate.getString(UploadDate).equals(other.getUploadDate()))
			&& (ReportDate == null ? other.getReportDate() == null : fDate.getString(ReportDate).equals(other.getReportDate()))
			&& ReportCount == other.getReportCount()
			&& CustomerCount == other.getCustomerCount()
			&& GroupCount == other.getGroupCount()
			&& (ReportState == null ? other.getReportState() == null : ReportState.equals(other.getReportState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (StandbyFlag1 == null ? other.getStandbyFlag1() == null : StandbyFlag1.equals(other.getStandbyFlag1()))
			&& (StandbyFlag2 == null ? other.getStandbyFlag2() == null : StandbyFlag2.equals(other.getStandbyFlag2()))
			&& (StandbyFlag3 == null ? other.getStandbyFlag3() == null : StandbyFlag3.equals(other.getStandbyFlag3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return 0;
		}
		if( strFieldName.equals("UploadDate") ) {
			return 1;
		}
		if( strFieldName.equals("ReportDate") ) {
			return 2;
		}
		if( strFieldName.equals("ReportCount") ) {
			return 3;
		}
		if( strFieldName.equals("CustomerCount") ) {
			return 4;
		}
		if( strFieldName.equals("GroupCount") ) {
			return 5;
		}
		if( strFieldName.equals("ReportState") ) {
			return 6;
		}
		if( strFieldName.equals("Operator") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		if( strFieldName.equals("Remark") ) {
			return 12;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return 13;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return 14;
		}
		if( strFieldName.equals("StandbyFlag3") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BatchNo";
				break;
			case 1:
				strFieldName = "UploadDate";
				break;
			case 2:
				strFieldName = "ReportDate";
				break;
			case 3:
				strFieldName = "ReportCount";
				break;
			case 4:
				strFieldName = "CustomerCount";
				break;
			case 5:
				strFieldName = "GroupCount";
				break;
			case 6:
				strFieldName = "ReportState";
				break;
			case 7:
				strFieldName = "Operator";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			case 12:
				strFieldName = "Remark";
				break;
			case 13:
				strFieldName = "StandbyFlag1";
				break;
			case 14:
				strFieldName = "StandbyFlag2";
				break;
			case 15:
				strFieldName = "StandbyFlag3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BatchNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UploadDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReportDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReportCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("CustomerCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("GroupCount") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("ReportState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandbyFlag3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_INT;
				break;
			case 5:
				nFieldType = Schema.TYPE_INT;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
