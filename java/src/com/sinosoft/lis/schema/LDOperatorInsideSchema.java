/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDOperatorInsideDB;

/*
 * <p>ClassName: LDOperatorInsideSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2006-08-23
 */
public class LDOperatorInsideSchema implements Schema, Cloneable {
    // @Field
    /** 用户编码 */
    private String UserCode;
    /** 用户类型 */
    private String UserType;
    /** 姓名 */
    private String UserName;
    /** 性别 */
    private String Sex;
    /** 生日 */
    private Date Birthday;
    /** 联系电话 */
    private String Phone;
    /** 手机 */
    private String Mobile;
    /** 管理机构 */
    private String ManageCom;
    /** 代理机构 */
    private String AgentCom;

    public static final int FIELDNUM = 9; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDOperatorInsideSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "UserCode";
        pk[1] = "UserType";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDOperatorInsideSchema cloned = (LDOperatorInsideSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getUserCode() {
        return UserCode;
    }

    public void setUserCode(String aUserCode) {
        UserCode = aUserCode;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String aUserType) {
        UserType = aUserType;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String aUserName) {
        UserName = aUserName;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String aSex) {
        Sex = aSex;
    }

    public String getBirthday() {
        if (Birthday != null) {
            return fDate.getString(Birthday);
        } else {
            return null;
        }
    }

    public void setBirthday(Date aBirthday) {
        Birthday = aBirthday;
    }

    public void setBirthday(String aBirthday) {
        if (aBirthday != null && !aBirthday.equals("")) {
            Birthday = fDate.getDate(aBirthday);
        } else {
            Birthday = null;
        }
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String aPhone) {
        Phone = aPhone;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String aMobile) {
        Mobile = aMobile;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getAgentCom() {
        return AgentCom;
    }

    public void setAgentCom(String aAgentCom) {
        AgentCom = aAgentCom;
    }

    /**
     * 使用另外一个 LDOperatorInsideSchema 对象给 Schema 赋值
     * @param: aLDOperatorInsideSchema LDOperatorInsideSchema
     **/
    public void setSchema(LDOperatorInsideSchema aLDOperatorInsideSchema) {
        this.UserCode = aLDOperatorInsideSchema.getUserCode();
        this.UserType = aLDOperatorInsideSchema.getUserType();
        this.UserName = aLDOperatorInsideSchema.getUserName();
        this.Sex = aLDOperatorInsideSchema.getSex();
        this.Birthday = fDate.getDate(aLDOperatorInsideSchema.getBirthday());
        this.Phone = aLDOperatorInsideSchema.getPhone();
        this.Mobile = aLDOperatorInsideSchema.getMobile();
        this.ManageCom = aLDOperatorInsideSchema.getManageCom();
        this.AgentCom = aLDOperatorInsideSchema.getAgentCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("UserCode") == null) {
                this.UserCode = null;
            } else {
                this.UserCode = rs.getString("UserCode").trim();
            }

            if (rs.getString("UserType") == null) {
                this.UserType = null;
            } else {
                this.UserType = rs.getString("UserType").trim();
            }

            if (rs.getString("UserName") == null) {
                this.UserName = null;
            } else {
                this.UserName = rs.getString("UserName").trim();
            }

            if (rs.getString("Sex") == null) {
                this.Sex = null;
            } else {
                this.Sex = rs.getString("Sex").trim();
            }

            this.Birthday = rs.getDate("Birthday");
            if (rs.getString("Phone") == null) {
                this.Phone = null;
            } else {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("Mobile") == null) {
                this.Mobile = null;
            } else {
                this.Mobile = rs.getString("Mobile").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("AgentCom") == null) {
                this.AgentCom = null;
            } else {
                this.AgentCom = rs.getString("AgentCom").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDOperatorInside表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDOperatorInsideSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDOperatorInsideSchema getSchema() {
        LDOperatorInsideSchema aLDOperatorInsideSchema = new
                LDOperatorInsideSchema();
        aLDOperatorInsideSchema.setSchema(this);
        return aLDOperatorInsideSchema;
    }

    public LDOperatorInsideDB getDB() {
        LDOperatorInsideDB aDBOper = new LDOperatorInsideDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDOperatorInside描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(UserCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UserType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(UserName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Sex));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(Birthday)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Mobile));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCom));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDOperatorInside>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            UserCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            UserType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            UserName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            Sex = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                 SysConst.PACKAGESPILTER);
            Birthday = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                   SysConst.PACKAGESPILTER);
            Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                    SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            AgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDOperatorInsideSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("UserCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UserCode));
        }
        if (FCode.equals("UserType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UserType));
        }
        if (FCode.equals("UserName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(UserName));
        }
        if (FCode.equals("Sex")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Sex));
        }
        if (FCode.equals("Birthday")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getBirthday()));
        }
        if (FCode.equals("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equals("Mobile")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("AgentCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(UserCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(UserType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(UserName);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(Sex);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getBirthday()));
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Phone);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Mobile);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(AgentCom);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("UserCode")) {
            if (FValue != null && !FValue.equals("")) {
                UserCode = FValue.trim();
            } else {
                UserCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("UserType")) {
            if (FValue != null && !FValue.equals("")) {
                UserType = FValue.trim();
            } else {
                UserType = null;
            }
        }
        if (FCode.equalsIgnoreCase("UserName")) {
            if (FValue != null && !FValue.equals("")) {
                UserName = FValue.trim();
            } else {
                UserName = null;
            }
        }
        if (FCode.equalsIgnoreCase("Sex")) {
            if (FValue != null && !FValue.equals("")) {
                Sex = FValue.trim();
            } else {
                Sex = null;
            }
        }
        if (FCode.equalsIgnoreCase("Birthday")) {
            if (FValue != null && !FValue.equals("")) {
                Birthday = fDate.getDate(FValue);
            } else {
                Birthday = null;
            }
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if (FValue != null && !FValue.equals("")) {
                Phone = FValue.trim();
            } else {
                Phone = null;
            }
        }
        if (FCode.equalsIgnoreCase("Mobile")) {
            if (FValue != null && !FValue.equals("")) {
                Mobile = FValue.trim();
            } else {
                Mobile = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCom")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCom = FValue.trim();
            } else {
                AgentCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDOperatorInsideSchema other = (LDOperatorInsideSchema) otherObject;
        return
                UserCode.equals(other.getUserCode())
                && UserType.equals(other.getUserType())
                && UserName.equals(other.getUserName())
                && Sex.equals(other.getSex())
                && fDate.getString(Birthday).equals(other.getBirthday())
                && Phone.equals(other.getPhone())
                && Mobile.equals(other.getMobile())
                && ManageCom.equals(other.getManageCom())
                && AgentCom.equals(other.getAgentCom());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("UserCode")) {
            return 0;
        }
        if (strFieldName.equals("UserType")) {
            return 1;
        }
        if (strFieldName.equals("UserName")) {
            return 2;
        }
        if (strFieldName.equals("Sex")) {
            return 3;
        }
        if (strFieldName.equals("Birthday")) {
            return 4;
        }
        if (strFieldName.equals("Phone")) {
            return 5;
        }
        if (strFieldName.equals("Mobile")) {
            return 6;
        }
        if (strFieldName.equals("ManageCom")) {
            return 7;
        }
        if (strFieldName.equals("AgentCom")) {
            return 8;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "UserCode";
            break;
        case 1:
            strFieldName = "UserType";
            break;
        case 2:
            strFieldName = "UserName";
            break;
        case 3:
            strFieldName = "Sex";
            break;
        case 4:
            strFieldName = "Birthday";
            break;
        case 5:
            strFieldName = "Phone";
            break;
        case 6:
            strFieldName = "Mobile";
            break;
        case 7:
            strFieldName = "ManageCom";
            break;
        case 8:
            strFieldName = "AgentCom";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("UserCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UserType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("UserName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Sex")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Birthday")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Phone")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Mobile")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentCom")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
