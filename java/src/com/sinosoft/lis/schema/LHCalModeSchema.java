/*
 * <p>ClassName: LHCalModeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LHCalModeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LHCalModeSchema implements Schema
{
    // @Field
    /** 计算编码 */
    private String CalCode;
    /** 计算结果名 */
    private String CalName;
    /** 计算类型 */
    private String Type;
    /** 计算sql */
    private String CalSql;
    /** 参数个数 */
    private int ParamsNum;
    /** 参数形式 */
    private String Params;
    /** 算法备注 */
    private String CalRemark;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHCalModeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "CalCode";
        pk[1] = "Type";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getCalCode()
    {
        if (CalCode != null && !CalCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public String getCalName()
    {
        if (CalName != null && !CalName.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalName = StrTool.unicodeToGBK(CalName);
        }
        return CalName;
    }

    public void setCalName(String aCalName)
    {
        CalName = aCalName;
    }

    public String getType()
    {
        if (Type != null && !Type.equals("") && SysConst.CHANGECHARSET == true)
        {
            Type = StrTool.unicodeToGBK(Type);
        }
        return Type;
    }

    public void setType(String aType)
    {
        Type = aType;
    }

    public String getCalSql()
    {
        if (CalSql != null && !CalSql.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalSql = StrTool.unicodeToGBK(CalSql);
        }
        return CalSql;
    }

    public void setCalSql(String aCalSql)
    {
        CalSql = aCalSql;
    }

    public int getParamsNum()
    {
        return ParamsNum;
    }

    public void setParamsNum(int aParamsNum)
    {
        ParamsNum = aParamsNum;
    }

    public void setParamsNum(String aParamsNum)
    {
        if (aParamsNum != null && !aParamsNum.equals(""))
        {
            Integer tInteger = new Integer(aParamsNum);
            int i = tInteger.intValue();
            ParamsNum = i;
        }
    }

    public String getParams()
    {
        if (Params != null && !Params.equals("") && SysConst.CHANGECHARSET == true)
        {
            Params = StrTool.unicodeToGBK(Params);
        }
        return Params;
    }

    public void setParams(String aParams)
    {
        Params = aParams;
    }

    public String getCalRemark()
    {
        if (CalRemark != null && !CalRemark.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalRemark = StrTool.unicodeToGBK(CalRemark);
        }
        return CalRemark;
    }

    public void setCalRemark(String aCalRemark)
    {
        CalRemark = aCalRemark;
    }

    /**
     * 使用另外一个 LHCalModeSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LHCalModeSchema aLHCalModeSchema)
    {
        this.CalCode = aLHCalModeSchema.getCalCode();
        this.CalName = aLHCalModeSchema.getCalName();
        this.Type = aLHCalModeSchema.getType();
        this.CalSql = aLHCalModeSchema.getCalSql();
        this.ParamsNum = aLHCalModeSchema.getParamsNum();
        this.Params = aLHCalModeSchema.getParams();
        this.CalRemark = aLHCalModeSchema.getCalRemark();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            if (rs.getString("CalName") == null)
            {
                this.CalName = null;
            }
            else
            {
                this.CalName = rs.getString("CalName").trim();
            }

            if (rs.getString("Type") == null)
            {
                this.Type = null;
            }
            else
            {
                this.Type = rs.getString("Type").trim();
            }

            if (rs.getString("CalSql") == null)
            {
                this.CalSql = null;
            }
            else
            {
                this.CalSql = rs.getString("CalSql").trim();
            }

            this.ParamsNum = rs.getInt("ParamsNum");
            if (rs.getString("Params") == null)
            {
                this.Params = null;
            }
            else
            {
                this.Params = rs.getString("Params").trim();
            }

            if (rs.getString("CalRemark") == null)
            {
                this.CalRemark = null;
            }
            else
            {
                this.CalRemark = rs.getString("CalRemark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCalModeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LHCalModeSchema getSchema()
    {
        LHCalModeSchema aLHCalModeSchema = new LHCalModeSchema();
        aLHCalModeSchema.setSchema(this);
        return aLHCalModeSchema;
    }

    public LHCalModeDB getDB()
    {
        LHCalModeDB aDBOper = new LHCalModeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHCalMode描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(CalCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Type)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalSql)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(ParamsNum) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Params)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalRemark));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHCalMode>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            CalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            Type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                  SysConst.PACKAGESPILTER);
            CalSql = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            ParamsNum = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            Params = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            CalRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCalModeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCode));
        }
        if (FCode.equals("CalName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalName));
        }
        if (FCode.equals("Type"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Type));
        }
        if (FCode.equals("CalSql"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalSql));
        }
        if (FCode.equals("ParamsNum"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ParamsNum));
        }
        if (FCode.equals("Params"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Params));
        }
        if (FCode.equals("CalRemark"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalRemark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(CalName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(Type);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CalSql);
                break;
            case 4:
                strFieldValue = String.valueOf(ParamsNum);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Params);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CalRemark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("CalName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalName = FValue.trim();
            }
            else
            {
                CalName = null;
            }
        }
        if (FCode.equals("Type"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Type = FValue.trim();
            }
            else
            {
                Type = null;
            }
        }
        if (FCode.equals("CalSql"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalSql = FValue.trim();
            }
            else
            {
                CalSql = null;
            }
        }
        if (FCode.equals("ParamsNum"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ParamsNum = i;
            }
        }
        if (FCode.equals("Params"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Params = FValue.trim();
            }
            else
            {
                Params = null;
            }
        }
        if (FCode.equals("CalRemark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalRemark = FValue.trim();
            }
            else
            {
                CalRemark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LHCalModeSchema other = (LHCalModeSchema) otherObject;
        return
                CalCode.equals(other.getCalCode())
                && CalName.equals(other.getCalName())
                && Type.equals(other.getType())
                && CalSql.equals(other.getCalSql())
                && ParamsNum == other.getParamsNum()
                && Params.equals(other.getParams())
                && CalRemark.equals(other.getCalRemark());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("CalCode"))
        {
            return 0;
        }
        if (strFieldName.equals("CalName"))
        {
            return 1;
        }
        if (strFieldName.equals("Type"))
        {
            return 2;
        }
        if (strFieldName.equals("CalSql"))
        {
            return 3;
        }
        if (strFieldName.equals("ParamsNum"))
        {
            return 4;
        }
        if (strFieldName.equals("Params"))
        {
            return 5;
        }
        if (strFieldName.equals("CalRemark"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "CalCode";
                break;
            case 1:
                strFieldName = "CalName";
                break;
            case 2:
                strFieldName = "Type";
                break;
            case 3:
                strFieldName = "CalSql";
                break;
            case 4:
                strFieldName = "ParamsNum";
                break;
            case 5:
                strFieldName = "Params";
                break;
            case 6:
                strFieldName = "CalRemark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Type"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalSql"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ParamsNum"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Params"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalRemark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
