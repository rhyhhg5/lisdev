/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHFeeInfoDB;

/*
 * <p>ClassName: LHFeeInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-09-23
 */
public class LHFeeInfoSchema implements Schema, Cloneable {
    // @Field
    /** 客户号码 */
    private String CustomerNo;
    /** 就诊及住院序号 */
    private String InHospitNo;
    /** 流水号 */
    private String FeeNo;
    /** 费用 */
    private double FeeAmount;
    /** 费用代码 */
    private String FeeCode;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHFeeInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "CustomerNo";
        pk[1] = "InHospitNo";
        pk[2] = "FeeNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHFeeInfoSchema cloned = (LHFeeInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getCustomerNo() {
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo) {
        CustomerNo = aCustomerNo;
    }

    public String getInHospitNo() {
        return InHospitNo;
    }

    public void setInHospitNo(String aInHospitNo) {
        InHospitNo = aInHospitNo;
    }

    public String getFeeNo() {
        return FeeNo;
    }

    public void setFeeNo(String aFeeNo) {
        FeeNo = aFeeNo;
    }

    public double getFeeAmount() {
        return FeeAmount;
    }

    public void setFeeAmount(double aFeeAmount) {
        FeeAmount = Arith.round(aFeeAmount, 2);
    }

    public void setFeeAmount(String aFeeAmount) {
        if (aFeeAmount != null && !aFeeAmount.equals("")) {
            Double tDouble = new Double(aFeeAmount);
            double d = tDouble.doubleValue();
            FeeAmount = Arith.round(d, 2);
        }
    }

    public String getFeeCode() {
        return FeeCode;
    }

    public void setFeeCode(String aFeeCode) {
        FeeCode = aFeeCode;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    /**
     * 使用另外一个 LHFeeInfoSchema 对象给 Schema 赋值
     * @param: aLHFeeInfoSchema LHFeeInfoSchema
     **/
    public void setSchema(LHFeeInfoSchema aLHFeeInfoSchema) {
        this.CustomerNo = aLHFeeInfoSchema.getCustomerNo();
        this.InHospitNo = aLHFeeInfoSchema.getInHospitNo();
        this.FeeNo = aLHFeeInfoSchema.getFeeNo();
        this.FeeAmount = aLHFeeInfoSchema.getFeeAmount();
        this.FeeCode = aLHFeeInfoSchema.getFeeCode();
        this.Operator = aLHFeeInfoSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHFeeInfoSchema.getMakeDate());
        this.MakeTime = aLHFeeInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHFeeInfoSchema.getModifyDate());
        this.ModifyTime = aLHFeeInfoSchema.getModifyTime();
        this.ManageCom = aLHFeeInfoSchema.getManageCom();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("CustomerNo") == null) {
                this.CustomerNo = null;
            } else {
                this.CustomerNo = rs.getString("CustomerNo").trim();
            }

            if (rs.getString("InHospitNo") == null) {
                this.InHospitNo = null;
            } else {
                this.InHospitNo = rs.getString("InHospitNo").trim();
            }

            if (rs.getString("FeeNo") == null) {
                this.FeeNo = null;
            } else {
                this.FeeNo = rs.getString("FeeNo").trim();
            }

            this.FeeAmount = rs.getDouble("FeeAmount");
            if (rs.getString("FeeCode") == null) {
                this.FeeCode = null;
            } else {
                this.FeeCode = rs.getString("FeeCode").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHFeeInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHFeeInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHFeeInfoSchema getSchema() {
        LHFeeInfoSchema aLHFeeInfoSchema = new LHFeeInfoSchema();
        aLHFeeInfoSchema.setSchema(this);
        return aLHFeeInfoSchema;
    }

    public LHFeeInfoDB getDB() {
        LHFeeInfoDB aDBOper = new LHFeeInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHFeeInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(CustomerNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InHospitNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FeeNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FeeAmount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FeeCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHFeeInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            InHospitNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            FeeNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            FeeAmount = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).doubleValue();
            FeeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 9, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHFeeInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("CustomerNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("InHospitNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InHospitNo));
        }
        if (FCode.equals("FeeNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeNo));
        }
        if (FCode.equals("FeeAmount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeAmount));
        }
        if (FCode.equals("FeeCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FeeCode));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(CustomerNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(InHospitNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(FeeNo);
            break;
        case 3:
            strFieldValue = String.valueOf(FeeAmount);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(FeeCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("CustomerNo")) {
            if (FValue != null && !FValue.equals("")) {
                CustomerNo = FValue.trim();
            } else {
                CustomerNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("InHospitNo")) {
            if (FValue != null && !FValue.equals("")) {
                InHospitNo = FValue.trim();
            } else {
                InHospitNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("FeeNo")) {
            if (FValue != null && !FValue.equals("")) {
                FeeNo = FValue.trim();
            } else {
                FeeNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("FeeAmount")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FeeAmount = d;
            }
        }
        if (FCode.equalsIgnoreCase("FeeCode")) {
            if (FValue != null && !FValue.equals("")) {
                FeeCode = FValue.trim();
            } else {
                FeeCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHFeeInfoSchema other = (LHFeeInfoSchema) otherObject;
        return
                CustomerNo.equals(other.getCustomerNo())
                && InHospitNo.equals(other.getInHospitNo())
                && FeeNo.equals(other.getFeeNo())
                && FeeAmount == other.getFeeAmount()
                && FeeCode.equals(other.getFeeCode())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ManageCom.equals(other.getManageCom());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("CustomerNo")) {
            return 0;
        }
        if (strFieldName.equals("InHospitNo")) {
            return 1;
        }
        if (strFieldName.equals("FeeNo")) {
            return 2;
        }
        if (strFieldName.equals("FeeAmount")) {
            return 3;
        }
        if (strFieldName.equals("FeeCode")) {
            return 4;
        }
        if (strFieldName.equals("Operator")) {
            return 5;
        }
        if (strFieldName.equals("MakeDate")) {
            return 6;
        }
        if (strFieldName.equals("MakeTime")) {
            return 7;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 8;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 9;
        }
        if (strFieldName.equals("ManageCom")) {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "CustomerNo";
            break;
        case 1:
            strFieldName = "InHospitNo";
            break;
        case 2:
            strFieldName = "FeeNo";
            break;
        case 3:
            strFieldName = "FeeAmount";
            break;
        case 4:
            strFieldName = "FeeCode";
            break;
        case 5:
            strFieldName = "Operator";
            break;
        case 6:
            strFieldName = "MakeDate";
            break;
        case 7:
            strFieldName = "MakeTime";
            break;
        case 8:
            strFieldName = "ModifyDate";
            break;
        case 9:
            strFieldName = "ModifyTime";
            break;
        case 10:
            strFieldName = "ManageCom";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("CustomerNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InHospitNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FeeAmount")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FeeCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
