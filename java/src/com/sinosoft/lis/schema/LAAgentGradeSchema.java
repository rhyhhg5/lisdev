/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAAgentGradeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAAgentGradeSchema implements Schema
{
    // @Field
    /** 级别编码 */
    private String GradeCode;
    /** 级别id */
    private int GradeID;
    /** 职级名称 */
    private String GradeName;
    /** 展业类型 */
    private String BranchType;
    /** 级别属性1 */
    private String GradeProperty1;
    /** 级别属性2 */
    private String GradeProperty2;
    /** 级别属性3 */
    private String GradeProperty3;
    /** 级别属性4 */
    private String GradeProperty4;
    /** 级别属性5 */
    private String GradeProperty5;
    /** 级别属性6 */
    private String GradeProperty6;
    /** 级别属性7 */
    private String GradeProperty7;
    /** 级别属性8 */
    private String GradeProperty8;
    /** 级别属性9 */
    private String GradeProperty9;
    /** 级别属性10 */
    private String GradeProperty10;
    /** 备注 */
    private String Noti;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAgentGradeSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "GradeCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getGradeCode()
    {
        if (SysConst.CHANGECHARSET && GradeCode != null && !GradeCode.equals(""))
        {
            GradeCode = StrTool.unicodeToGBK(GradeCode);
        }
        return GradeCode;
    }

    public void setGradeCode(String aGradeCode)
    {
        GradeCode = aGradeCode;
    }

    public int getGradeID()
    {
        return GradeID;
    }

    public void setGradeID(int aGradeID)
    {
        GradeID = aGradeID;
    }

    public void setGradeID(String aGradeID)
    {
        if (aGradeID != null && !aGradeID.equals(""))
        {
            Integer tInteger = new Integer(aGradeID);
            int i = tInteger.intValue();
            GradeID = i;
        }
    }

    public String getGradeName()
    {
        if (SysConst.CHANGECHARSET && GradeName != null && !GradeName.equals(""))
        {
            GradeName = StrTool.unicodeToGBK(GradeName);
        }
        return GradeName;
    }

    public void setGradeName(String aGradeName)
    {
        GradeName = aGradeName;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public String getGradeProperty1()
    {
        if (SysConst.CHANGECHARSET && GradeProperty1 != null &&
            !GradeProperty1.equals(""))
        {
            GradeProperty1 = StrTool.unicodeToGBK(GradeProperty1);
        }
        return GradeProperty1;
    }

    public void setGradeProperty1(String aGradeProperty1)
    {
        GradeProperty1 = aGradeProperty1;
    }

    public String getGradeProperty2()
    {
        if (SysConst.CHANGECHARSET && GradeProperty2 != null &&
            !GradeProperty2.equals(""))
        {
            GradeProperty2 = StrTool.unicodeToGBK(GradeProperty2);
        }
        return GradeProperty2;
    }

    public void setGradeProperty2(String aGradeProperty2)
    {
        GradeProperty2 = aGradeProperty2;
    }

    public String getGradeProperty3()
    {
        if (SysConst.CHANGECHARSET && GradeProperty3 != null &&
            !GradeProperty3.equals(""))
        {
            GradeProperty3 = StrTool.unicodeToGBK(GradeProperty3);
        }
        return GradeProperty3;
    }

    public void setGradeProperty3(String aGradeProperty3)
    {
        GradeProperty3 = aGradeProperty3;
    }

    public String getGradeProperty4()
    {
        if (SysConst.CHANGECHARSET && GradeProperty4 != null &&
            !GradeProperty4.equals(""))
        {
            GradeProperty4 = StrTool.unicodeToGBK(GradeProperty4);
        }
        return GradeProperty4;
    }

    public void setGradeProperty4(String aGradeProperty4)
    {
        GradeProperty4 = aGradeProperty4;
    }

    public String getGradeProperty5()
    {
        if (SysConst.CHANGECHARSET && GradeProperty5 != null &&
            !GradeProperty5.equals(""))
        {
            GradeProperty5 = StrTool.unicodeToGBK(GradeProperty5);
        }
        return GradeProperty5;
    }

    public void setGradeProperty5(String aGradeProperty5)
    {
        GradeProperty5 = aGradeProperty5;
    }

    public String getGradeProperty6()
    {
        if (SysConst.CHANGECHARSET && GradeProperty6 != null &&
            !GradeProperty6.equals(""))
        {
            GradeProperty6 = StrTool.unicodeToGBK(GradeProperty6);
        }
        return GradeProperty6;
    }

    public void setGradeProperty6(String aGradeProperty6)
    {
        GradeProperty6 = aGradeProperty6;
    }

    public String getGradeProperty7()
    {
        if (SysConst.CHANGECHARSET && GradeProperty7 != null &&
            !GradeProperty7.equals(""))
        {
            GradeProperty7 = StrTool.unicodeToGBK(GradeProperty7);
        }
        return GradeProperty7;
    }

    public void setGradeProperty7(String aGradeProperty7)
    {
        GradeProperty7 = aGradeProperty7;
    }

    public String getGradeProperty8()
    {
        if (SysConst.CHANGECHARSET && GradeProperty8 != null &&
            !GradeProperty8.equals(""))
        {
            GradeProperty8 = StrTool.unicodeToGBK(GradeProperty8);
        }
        return GradeProperty8;
    }

    public void setGradeProperty8(String aGradeProperty8)
    {
        GradeProperty8 = aGradeProperty8;
    }

    public String getGradeProperty9()
    {
        if (SysConst.CHANGECHARSET && GradeProperty9 != null &&
            !GradeProperty9.equals(""))
        {
            GradeProperty9 = StrTool.unicodeToGBK(GradeProperty9);
        }
        return GradeProperty9;
    }

    public void setGradeProperty9(String aGradeProperty9)
    {
        GradeProperty9 = aGradeProperty9;
    }

    public String getGradeProperty10()
    {
        if (SysConst.CHANGECHARSET && GradeProperty10 != null &&
            !GradeProperty10.equals(""))
        {
            GradeProperty10 = StrTool.unicodeToGBK(GradeProperty10);
        }
        return GradeProperty10;
    }

    public void setGradeProperty10(String aGradeProperty10)
    {
        GradeProperty10 = aGradeProperty10;
    }

    public String getNoti()
    {
        if (SysConst.CHANGECHARSET && Noti != null && !Noti.equals(""))
        {
            Noti = StrTool.unicodeToGBK(Noti);
        }
        return Noti;
    }

    public void setNoti(String aNoti)
    {
        Noti = aNoti;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAAgentGradeSchema 对象给 Schema 赋值
     * @param: aLAAgentGradeSchema LAAgentGradeSchema
     **/
    public void setSchema(LAAgentGradeSchema aLAAgentGradeSchema)
    {
        this.GradeCode = aLAAgentGradeSchema.getGradeCode();
        this.GradeID = aLAAgentGradeSchema.getGradeID();
        this.GradeName = aLAAgentGradeSchema.getGradeName();
        this.BranchType = aLAAgentGradeSchema.getBranchType();
        this.GradeProperty1 = aLAAgentGradeSchema.getGradeProperty1();
        this.GradeProperty2 = aLAAgentGradeSchema.getGradeProperty2();
        this.GradeProperty3 = aLAAgentGradeSchema.getGradeProperty3();
        this.GradeProperty4 = aLAAgentGradeSchema.getGradeProperty4();
        this.GradeProperty5 = aLAAgentGradeSchema.getGradeProperty5();
        this.GradeProperty6 = aLAAgentGradeSchema.getGradeProperty6();
        this.GradeProperty7 = aLAAgentGradeSchema.getGradeProperty7();
        this.GradeProperty8 = aLAAgentGradeSchema.getGradeProperty8();
        this.GradeProperty9 = aLAAgentGradeSchema.getGradeProperty9();
        this.GradeProperty10 = aLAAgentGradeSchema.getGradeProperty10();
        this.Noti = aLAAgentGradeSchema.getNoti();
        this.BranchType2 = aLAAgentGradeSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("GradeCode") == null)
            {
                this.GradeCode = null;
            }
            else
            {
                this.GradeCode = rs.getString("GradeCode").trim();
            }

            this.GradeID = rs.getInt("GradeID");
            if (rs.getString("GradeName") == null)
            {
                this.GradeName = null;
            }
            else
            {
                this.GradeName = rs.getString("GradeName").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            if (rs.getString("GradeProperty1") == null)
            {
                this.GradeProperty1 = null;
            }
            else
            {
                this.GradeProperty1 = rs.getString("GradeProperty1").trim();
            }

            if (rs.getString("GradeProperty2") == null)
            {
                this.GradeProperty2 = null;
            }
            else
            {
                this.GradeProperty2 = rs.getString("GradeProperty2").trim();
            }

            if (rs.getString("GradeProperty3") == null)
            {
                this.GradeProperty3 = null;
            }
            else
            {
                this.GradeProperty3 = rs.getString("GradeProperty3").trim();
            }

            if (rs.getString("GradeProperty4") == null)
            {
                this.GradeProperty4 = null;
            }
            else
            {
                this.GradeProperty4 = rs.getString("GradeProperty4").trim();
            }

            if (rs.getString("GradeProperty5") == null)
            {
                this.GradeProperty5 = null;
            }
            else
            {
                this.GradeProperty5 = rs.getString("GradeProperty5").trim();
            }

            if (rs.getString("GradeProperty6") == null)
            {
                this.GradeProperty6 = null;
            }
            else
            {
                this.GradeProperty6 = rs.getString("GradeProperty6").trim();
            }

            if (rs.getString("GradeProperty7") == null)
            {
                this.GradeProperty7 = null;
            }
            else
            {
                this.GradeProperty7 = rs.getString("GradeProperty7").trim();
            }

            if (rs.getString("GradeProperty8") == null)
            {
                this.GradeProperty8 = null;
            }
            else
            {
                this.GradeProperty8 = rs.getString("GradeProperty8").trim();
            }

            if (rs.getString("GradeProperty9") == null)
            {
                this.GradeProperty9 = null;
            }
            else
            {
                this.GradeProperty9 = rs.getString("GradeProperty9").trim();
            }

            if (rs.getString("GradeProperty10") == null)
            {
                this.GradeProperty10 = null;
            }
            else
            {
                this.GradeProperty10 = rs.getString("GradeProperty10").trim();
            }

            if (rs.getString("Noti") == null)
            {
                this.Noti = null;
            }
            else
            {
                this.Noti = rs.getString("Noti").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAAgentGradeSchema getSchema()
    {
        LAAgentGradeSchema aLAAgentGradeSchema = new LAAgentGradeSchema();
        aLAAgentGradeSchema.setSchema(this);
        return aLAAgentGradeSchema;
    }

    public LAAgentGradeDB getDB()
    {
        LAAgentGradeDB aDBOper = new LAAgentGradeDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentGrade描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(GradeID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeProperty1)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeProperty2)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeProperty3)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeProperty4)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeProperty5)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeProperty6)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeProperty7)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeProperty8)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeProperty9)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeProperty10)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Noti)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAgentGrade>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            GradeCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            GradeID = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 2, SysConst.PACKAGESPILTER))).intValue();
            GradeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            GradeProperty1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                            SysConst.PACKAGESPILTER);
            GradeProperty2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                            SysConst.PACKAGESPILTER);
            GradeProperty3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                            SysConst.PACKAGESPILTER);
            GradeProperty4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                            SysConst.PACKAGESPILTER);
            GradeProperty5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                            SysConst.PACKAGESPILTER);
            GradeProperty6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            10, SysConst.PACKAGESPILTER);
            GradeProperty7 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            11, SysConst.PACKAGESPILTER);
            GradeProperty8 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            12, SysConst.PACKAGESPILTER);
            GradeProperty9 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            13, SysConst.PACKAGESPILTER);
            GradeProperty10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             14, SysConst.PACKAGESPILTER);
            Noti = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                  SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentGradeSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("GradeCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeCode));
        }
        if (FCode.equals("GradeID"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeID));
        }
        if (FCode.equals("GradeName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeName));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("GradeProperty1"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeProperty1));
        }
        if (FCode.equals("GradeProperty2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeProperty2));
        }
        if (FCode.equals("GradeProperty3"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeProperty3));
        }
        if (FCode.equals("GradeProperty4"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeProperty4));
        }
        if (FCode.equals("GradeProperty5"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeProperty5));
        }
        if (FCode.equals("GradeProperty6"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeProperty6));
        }
        if (FCode.equals("GradeProperty7"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeProperty7));
        }
        if (FCode.equals("GradeProperty8"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeProperty8));
        }
        if (FCode.equals("GradeProperty9"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeProperty9));
        }
        if (FCode.equals("GradeProperty10"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeProperty10));
        }
        if (FCode.equals("Noti"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Noti));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(GradeCode);
                break;
            case 1:
                strFieldValue = String.valueOf(GradeID);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(GradeName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(GradeProperty1);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(GradeProperty2);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(GradeProperty3);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(GradeProperty4);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(GradeProperty5);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(GradeProperty6);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(GradeProperty7);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(GradeProperty8);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(GradeProperty9);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(GradeProperty10);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(Noti);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("GradeCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeCode = FValue.trim();
            }
            else
            {
                GradeCode = null;
            }
        }
        if (FCode.equals("GradeID"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                GradeID = i;
            }
        }
        if (FCode.equals("GradeName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeName = FValue.trim();
            }
            else
            {
                GradeName = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("GradeProperty1"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeProperty1 = FValue.trim();
            }
            else
            {
                GradeProperty1 = null;
            }
        }
        if (FCode.equals("GradeProperty2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeProperty2 = FValue.trim();
            }
            else
            {
                GradeProperty2 = null;
            }
        }
        if (FCode.equals("GradeProperty3"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeProperty3 = FValue.trim();
            }
            else
            {
                GradeProperty3 = null;
            }
        }
        if (FCode.equals("GradeProperty4"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeProperty4 = FValue.trim();
            }
            else
            {
                GradeProperty4 = null;
            }
        }
        if (FCode.equals("GradeProperty5"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeProperty5 = FValue.trim();
            }
            else
            {
                GradeProperty5 = null;
            }
        }
        if (FCode.equals("GradeProperty6"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeProperty6 = FValue.trim();
            }
            else
            {
                GradeProperty6 = null;
            }
        }
        if (FCode.equals("GradeProperty7"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeProperty7 = FValue.trim();
            }
            else
            {
                GradeProperty7 = null;
            }
        }
        if (FCode.equals("GradeProperty8"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeProperty8 = FValue.trim();
            }
            else
            {
                GradeProperty8 = null;
            }
        }
        if (FCode.equals("GradeProperty9"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeProperty9 = FValue.trim();
            }
            else
            {
                GradeProperty9 = null;
            }
        }
        if (FCode.equals("GradeProperty10"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeProperty10 = FValue.trim();
            }
            else
            {
                GradeProperty10 = null;
            }
        }
        if (FCode.equals("Noti"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Noti = FValue.trim();
            }
            else
            {
                Noti = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAAgentGradeSchema other = (LAAgentGradeSchema) otherObject;
        return
                GradeCode.equals(other.getGradeCode())
                && GradeID == other.getGradeID()
                && GradeName.equals(other.getGradeName())
                && BranchType.equals(other.getBranchType())
                && GradeProperty1.equals(other.getGradeProperty1())
                && GradeProperty2.equals(other.getGradeProperty2())
                && GradeProperty3.equals(other.getGradeProperty3())
                && GradeProperty4.equals(other.getGradeProperty4())
                && GradeProperty5.equals(other.getGradeProperty5())
                && GradeProperty6.equals(other.getGradeProperty6())
                && GradeProperty7.equals(other.getGradeProperty7())
                && GradeProperty8.equals(other.getGradeProperty8())
                && GradeProperty9.equals(other.getGradeProperty9())
                && GradeProperty10.equals(other.getGradeProperty10())
                && Noti.equals(other.getNoti())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("GradeCode"))
        {
            return 0;
        }
        if (strFieldName.equals("GradeID"))
        {
            return 1;
        }
        if (strFieldName.equals("GradeName"))
        {
            return 2;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 3;
        }
        if (strFieldName.equals("GradeProperty1"))
        {
            return 4;
        }
        if (strFieldName.equals("GradeProperty2"))
        {
            return 5;
        }
        if (strFieldName.equals("GradeProperty3"))
        {
            return 6;
        }
        if (strFieldName.equals("GradeProperty4"))
        {
            return 7;
        }
        if (strFieldName.equals("GradeProperty5"))
        {
            return 8;
        }
        if (strFieldName.equals("GradeProperty6"))
        {
            return 9;
        }
        if (strFieldName.equals("GradeProperty7"))
        {
            return 10;
        }
        if (strFieldName.equals("GradeProperty8"))
        {
            return 11;
        }
        if (strFieldName.equals("GradeProperty9"))
        {
            return 12;
        }
        if (strFieldName.equals("GradeProperty10"))
        {
            return 13;
        }
        if (strFieldName.equals("Noti"))
        {
            return 14;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "GradeCode";
                break;
            case 1:
                strFieldName = "GradeID";
                break;
            case 2:
                strFieldName = "GradeName";
                break;
            case 3:
                strFieldName = "BranchType";
                break;
            case 4:
                strFieldName = "GradeProperty1";
                break;
            case 5:
                strFieldName = "GradeProperty2";
                break;
            case 6:
                strFieldName = "GradeProperty3";
                break;
            case 7:
                strFieldName = "GradeProperty4";
                break;
            case 8:
                strFieldName = "GradeProperty5";
                break;
            case 9:
                strFieldName = "GradeProperty6";
                break;
            case 10:
                strFieldName = "GradeProperty7";
                break;
            case 11:
                strFieldName = "GradeProperty8";
                break;
            case 12:
                strFieldName = "GradeProperty9";
                break;
            case 13:
                strFieldName = "GradeProperty10";
                break;
            case 14:
                strFieldName = "Noti";
                break;
            case 15:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("GradeCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeID"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("GradeName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeProperty1"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeProperty2"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeProperty3"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeProperty4"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeProperty5"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeProperty6"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeProperty7"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeProperty8"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeProperty9"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeProperty10"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Noti"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_INT;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
