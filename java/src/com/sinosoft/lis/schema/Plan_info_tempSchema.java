/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.Plan_info_tempDB;

/*
 * <p>ClassName: Plan_info_tempSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2009-02-24
 */
public class Plan_info_tempSchema implements Schema, Cloneable
{
	// @Field
	/** 险种代码 */
	private String Plan_Code;
	/** 产品名称 */
	private String Full_Name;
	/** 简称 */
	private String Abbr_name;
	/** 险类代码 */
	private String Plan_type;
	/** 产品设计类型 */
	private String Design_type;
	/** 管理类型 */
	private String Regu_Type;
	/** 报送文件编号 */
	private String File_Code;
	/** 产品报送日期 */
	private Date File_Date;

	public static final int FIELDNUM = 8;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public Plan_info_tempSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[0];

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		Plan_info_tempSchema cloned = (Plan_info_tempSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPlan_Code()
	{
		return Plan_Code;
	}
	public void setPlan_Code(String aPlan_Code)
	{
		Plan_Code = aPlan_Code;
	}
	public String getFull_Name()
	{
		return Full_Name;
	}
	public void setFull_Name(String aFull_Name)
	{
		Full_Name = aFull_Name;
	}
	public String getAbbr_name()
	{
		return Abbr_name;
	}
	public void setAbbr_name(String aAbbr_name)
	{
		Abbr_name = aAbbr_name;
	}
	public String getPlan_type()
	{
		return Plan_type;
	}
	public void setPlan_type(String aPlan_type)
	{
		Plan_type = aPlan_type;
	}
	public String getDesign_type()
	{
		return Design_type;
	}
	public void setDesign_type(String aDesign_type)
	{
		Design_type = aDesign_type;
	}
	public String getRegu_Type()
	{
		return Regu_Type;
	}
	public void setRegu_Type(String aRegu_Type)
	{
		Regu_Type = aRegu_Type;
	}
	public String getFile_Code()
	{
		return File_Code;
	}
	public void setFile_Code(String aFile_Code)
	{
		File_Code = aFile_Code;
	}
	public String getFile_Date()
	{
		if( File_Date != null )
			return fDate.getString(File_Date);
		else
			return null;
	}
	public void setFile_Date(Date aFile_Date)
	{
		File_Date = aFile_Date;
	}
	public void setFile_Date(String aFile_Date)
	{
		if (aFile_Date != null && !aFile_Date.equals("") )
		{
			File_Date = fDate.getDate( aFile_Date );
		}
		else
			File_Date = null;
	}


	/**
	* 使用另外一个 Plan_info_tempSchema 对象给 Schema 赋值
	* @param: aPlan_info_tempSchema Plan_info_tempSchema
	**/
	public void setSchema(Plan_info_tempSchema aPlan_info_tempSchema)
	{
		this.Plan_Code = aPlan_info_tempSchema.getPlan_Code();
		this.Full_Name = aPlan_info_tempSchema.getFull_Name();
		this.Abbr_name = aPlan_info_tempSchema.getAbbr_name();
		this.Plan_type = aPlan_info_tempSchema.getPlan_type();
		this.Design_type = aPlan_info_tempSchema.getDesign_type();
		this.Regu_Type = aPlan_info_tempSchema.getRegu_Type();
		this.File_Code = aPlan_info_tempSchema.getFile_Code();
		this.File_Date = fDate.getDate( aPlan_info_tempSchema.getFile_Date());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Plan_Code") == null )
				this.Plan_Code = null;
			else
				this.Plan_Code = rs.getString("Plan_Code").trim();

			if( rs.getString("Full_Name") == null )
				this.Full_Name = null;
			else
				this.Full_Name = rs.getString("Full_Name").trim();

			if( rs.getString("Abbr_name") == null )
				this.Abbr_name = null;
			else
				this.Abbr_name = rs.getString("Abbr_name").trim();

			if( rs.getString("Plan_type") == null )
				this.Plan_type = null;
			else
				this.Plan_type = rs.getString("Plan_type").trim();

			if( rs.getString("Design_type") == null )
				this.Design_type = null;
			else
				this.Design_type = rs.getString("Design_type").trim();

			if( rs.getString("Regu_Type") == null )
				this.Regu_Type = null;
			else
				this.Regu_Type = rs.getString("Regu_Type").trim();

			if( rs.getString("File_Code") == null )
				this.File_Code = null;
			else
				this.File_Code = rs.getString("File_Code").trim();

			this.File_Date = rs.getDate("File_Date");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的Plan_info_temp表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Plan_info_tempSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public Plan_info_tempSchema getSchema()
	{
		Plan_info_tempSchema aPlan_info_tempSchema = new Plan_info_tempSchema();
		aPlan_info_tempSchema.setSchema(this);
		return aPlan_info_tempSchema;
	}

	public Plan_info_tempDB getDB()
	{
		Plan_info_tempDB aDBOper = new Plan_info_tempDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpPlan_info_temp描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Plan_Code)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Full_Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Abbr_name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Plan_type)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Design_type)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Regu_Type)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(File_Code)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( File_Date )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpPlan_info_temp>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Plan_Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Full_Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Abbr_name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Plan_type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Design_type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Regu_Type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			File_Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			File_Date = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "Plan_info_tempSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Plan_Code"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Plan_Code));
		}
		if (FCode.equals("Full_Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Full_Name));
		}
		if (FCode.equals("Abbr_name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Abbr_name));
		}
		if (FCode.equals("Plan_type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Plan_type));
		}
		if (FCode.equals("Design_type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Design_type));
		}
		if (FCode.equals("Regu_Type"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Regu_Type));
		}
		if (FCode.equals("File_Code"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(File_Code));
		}
		if (FCode.equals("File_Date"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFile_Date()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Plan_Code);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Full_Name);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Abbr_name);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Plan_type);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Design_type);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Regu_Type);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(File_Code);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFile_Date()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Plan_Code"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Plan_Code = FValue.trim();
			}
			else
				Plan_Code = null;
		}
		if (FCode.equalsIgnoreCase("Full_Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Full_Name = FValue.trim();
			}
			else
				Full_Name = null;
		}
		if (FCode.equalsIgnoreCase("Abbr_name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Abbr_name = FValue.trim();
			}
			else
				Abbr_name = null;
		}
		if (FCode.equalsIgnoreCase("Plan_type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Plan_type = FValue.trim();
			}
			else
				Plan_type = null;
		}
		if (FCode.equalsIgnoreCase("Design_type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Design_type = FValue.trim();
			}
			else
				Design_type = null;
		}
		if (FCode.equalsIgnoreCase("Regu_Type"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Regu_Type = FValue.trim();
			}
			else
				Regu_Type = null;
		}
		if (FCode.equalsIgnoreCase("File_Code"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				File_Code = FValue.trim();
			}
			else
				File_Code = null;
		}
		if (FCode.equalsIgnoreCase("File_Date"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				File_Date = fDate.getDate( FValue );
			}
			else
				File_Date = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		Plan_info_tempSchema other = (Plan_info_tempSchema)otherObject;
		return
			Plan_Code.equals(other.getPlan_Code())
			&& Full_Name.equals(other.getFull_Name())
			&& Abbr_name.equals(other.getAbbr_name())
			&& Plan_type.equals(other.getPlan_type())
			&& Design_type.equals(other.getDesign_type())
			&& Regu_Type.equals(other.getRegu_Type())
			&& File_Code.equals(other.getFile_Code())
			&& fDate.getString(File_Date).equals(other.getFile_Date());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Plan_Code") ) {
			return 0;
		}
		if( strFieldName.equals("Full_Name") ) {
			return 1;
		}
		if( strFieldName.equals("Abbr_name") ) {
			return 2;
		}
		if( strFieldName.equals("Plan_type") ) {
			return 3;
		}
		if( strFieldName.equals("Design_type") ) {
			return 4;
		}
		if( strFieldName.equals("Regu_Type") ) {
			return 5;
		}
		if( strFieldName.equals("File_Code") ) {
			return 6;
		}
		if( strFieldName.equals("File_Date") ) {
			return 7;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Plan_Code";
				break;
			case 1:
				strFieldName = "Full_Name";
				break;
			case 2:
				strFieldName = "Abbr_name";
				break;
			case 3:
				strFieldName = "Plan_type";
				break;
			case 4:
				strFieldName = "Design_type";
				break;
			case 5:
				strFieldName = "Regu_Type";
				break;
			case 6:
				strFieldName = "File_Code";
				break;
			case 7:
				strFieldName = "File_Date";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Plan_Code") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Full_Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Abbr_name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Plan_type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Design_type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Regu_Type") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("File_Code") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("File_Date") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
