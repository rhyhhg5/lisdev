/*
 * <p>ClassName: LDImpartParamSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 告知信息表结构
 * @CreateDate：2004-11-22
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDImpartParamDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDImpartParamSchema implements Schema
{
    // @Field
    /** 告知版别 */
    private String ImpartVer;
    /** 告知编码 */
    private String ImpartCode;
    /** 告知参数顺序号 */
    private String ImpartParamNo;
    /** 告知参数名 */
    private String ImpartParamName;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 8; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDImpartParamSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ImpartVer";
        pk[1] = "ImpartCode";
        pk[2] = "ImpartParamNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getImpartVer()
    {
        if (ImpartVer != null && !ImpartVer.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartVer = StrTool.unicodeToGBK(ImpartVer);
        }
        return ImpartVer;
    }

    public void setImpartVer(String aImpartVer)
    {
        ImpartVer = aImpartVer;
    }

    public String getImpartCode()
    {
        if (ImpartCode != null && !ImpartCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartCode = StrTool.unicodeToGBK(ImpartCode);
        }
        return ImpartCode;
    }

    public void setImpartCode(String aImpartCode)
    {
        ImpartCode = aImpartCode;
    }

    public String getImpartParamNo()
    {
        if (ImpartParamNo != null && !ImpartParamNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartParamNo = StrTool.unicodeToGBK(ImpartParamNo);
        }
        return ImpartParamNo;
    }

    public void setImpartParamNo(String aImpartParamNo)
    {
        ImpartParamNo = aImpartParamNo;
    }

    public String getImpartParamName()
    {
        if (ImpartParamName != null && !ImpartParamName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ImpartParamName = StrTool.unicodeToGBK(ImpartParamName);
        }
        return ImpartParamName;
    }

    public void setImpartParamName(String aImpartParamName)
    {
        ImpartParamName = aImpartParamName;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LDImpartParamSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDImpartParamSchema aLDImpartParamSchema)
    {
        this.ImpartVer = aLDImpartParamSchema.getImpartVer();
        this.ImpartCode = aLDImpartParamSchema.getImpartCode();
        this.ImpartParamNo = aLDImpartParamSchema.getImpartParamNo();
        this.ImpartParamName = aLDImpartParamSchema.getImpartParamName();
        this.MakeDate = fDate.getDate(aLDImpartParamSchema.getMakeDate());
        this.MakeTime = aLDImpartParamSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDImpartParamSchema.getModifyDate());
        this.ModifyTime = aLDImpartParamSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ImpartVer") == null)
            {
                this.ImpartVer = null;
            }
            else
            {
                this.ImpartVer = rs.getString("ImpartVer").trim();
            }

            if (rs.getString("ImpartCode") == null)
            {
                this.ImpartCode = null;
            }
            else
            {
                this.ImpartCode = rs.getString("ImpartCode").trim();
            }

            if (rs.getString("ImpartParamNo") == null)
            {
                this.ImpartParamNo = null;
            }
            else
            {
                this.ImpartParamNo = rs.getString("ImpartParamNo").trim();
            }

            if (rs.getString("ImpartParamName") == null)
            {
                this.ImpartParamName = null;
            }
            else
            {
                this.ImpartParamName = rs.getString("ImpartParamName").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDImpartParamSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDImpartParamSchema getSchema()
    {
        LDImpartParamSchema aLDImpartParamSchema = new LDImpartParamSchema();
        aLDImpartParamSchema.setSchema(this);
        return aLDImpartParamSchema;
    }

    public LDImpartParamDB getDB()
    {
        LDImpartParamDB aDBOper = new LDImpartParamDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDImpartParam描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(ImpartVer)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ImpartCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ImpartParamNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ImpartParamName)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDImpartParam>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            ImpartVer = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            ImpartCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ImpartParamNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                           SysConst.PACKAGESPILTER);
            ImpartParamName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             4, SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 7, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDImpartParamSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("ImpartVer"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ImpartVer));
        }
        if (FCode.equals("ImpartCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ImpartCode));
        }
        if (FCode.equals("ImpartParamNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ImpartParamNo));
        }
        if (FCode.equals("ImpartParamName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ImpartParamName));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(ImpartVer);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ImpartCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ImpartParamNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ImpartParamName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("ImpartVer"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartVer = FValue.trim();
            }
            else
            {
                ImpartVer = null;
            }
        }
        if (FCode.equals("ImpartCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartCode = FValue.trim();
            }
            else
            {
                ImpartCode = null;
            }
        }
        if (FCode.equals("ImpartParamNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartParamNo = FValue.trim();
            }
            else
            {
                ImpartParamNo = null;
            }
        }
        if (FCode.equals("ImpartParamName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ImpartParamName = FValue.trim();
            }
            else
            {
                ImpartParamName = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDImpartParamSchema other = (LDImpartParamSchema) otherObject;
        return
                ImpartVer.equals(other.getImpartVer())
                && ImpartCode.equals(other.getImpartCode())
                && ImpartParamNo.equals(other.getImpartParamNo())
                && ImpartParamName.equals(other.getImpartParamName())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("ImpartVer"))
        {
            return 0;
        }
        if (strFieldName.equals("ImpartCode"))
        {
            return 1;
        }
        if (strFieldName.equals("ImpartParamNo"))
        {
            return 2;
        }
        if (strFieldName.equals("ImpartParamName"))
        {
            return 3;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 4;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 5;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 6;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 7;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "ImpartVer";
                break;
            case 1:
                strFieldName = "ImpartCode";
                break;
            case 2:
                strFieldName = "ImpartParamNo";
                break;
            case 3:
                strFieldName = "ImpartParamName";
                break;
            case 4:
                strFieldName = "MakeDate";
                break;
            case 5:
                strFieldName = "MakeTime";
                break;
            case 6:
                strFieldName = "ModifyDate";
                break;
            case 7:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("ImpartVer"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ImpartCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ImpartParamNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ImpartParamName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
