/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAWageRadixDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LAWageRadixSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-30
 */
public class LAWageRadixSchema implements Schema
{
    // @Field
    /** 顺序号 */
    private int Idx;
    /** 版本号 */
    private String AreaType;
    /** 代理人职级 */
    private String AgentGrade;
    /** 奖金代码 */
    private String WageCode;
    /** 展业类型 */
    private String BranchType;
    /** Fyc上限 */
    private double FYCMin;
    /** Fyc下限 */
    private double FYCMax;
    /** 提取比例 */
    private double DrawRate;
    /** 发放年数/月数限制 */
    private double LimitPeriod;
    /** 奖金金额 */
    private double RewardMoney;
    /** 发放时间起期 */
    private double DrawStart;
    /** 发放时间止期 */
    private double DrawEnd;
    /** 奖金名称 */
    private String WageName;
    /** 其它提取比例 */
    private double DrawRateOth;
    /** 保费标准 */
    private double PremStand;
    /** 育成关系 */
    private String RearType;
    /** 渠道 */
    private String BranchType2;

    public static final int FIELDNUM = 17; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAWageRadixSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "Idx";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public int getIdx()
    {
        return Idx;
    }

    public void setIdx(int aIdx)
    {
        Idx = aIdx;
    }

    public void setIdx(String aIdx)
    {
        if (aIdx != null && !aIdx.equals(""))
        {
            Integer tInteger = new Integer(aIdx);
            int i = tInteger.intValue();
            Idx = i;
        }
    }

    public String getAreaType()
    {
        if (SysConst.CHANGECHARSET && AreaType != null && !AreaType.equals(""))
        {
            AreaType = StrTool.unicodeToGBK(AreaType);
        }
        return AreaType;
    }

    public void setAreaType(String aAreaType)
    {
        AreaType = aAreaType;
    }

    public String getAgentGrade()
    {
        if (SysConst.CHANGECHARSET && AgentGrade != null &&
            !AgentGrade.equals(""))
        {
            AgentGrade = StrTool.unicodeToGBK(AgentGrade);
        }
        return AgentGrade;
    }

    public void setAgentGrade(String aAgentGrade)
    {
        AgentGrade = aAgentGrade;
    }

    public String getWageCode()
    {
        if (SysConst.CHANGECHARSET && WageCode != null && !WageCode.equals(""))
        {
            WageCode = StrTool.unicodeToGBK(WageCode);
        }
        return WageCode;
    }

    public void setWageCode(String aWageCode)
    {
        WageCode = aWageCode;
    }

    public String getBranchType()
    {
        if (SysConst.CHANGECHARSET && BranchType != null &&
            !BranchType.equals(""))
        {
            BranchType = StrTool.unicodeToGBK(BranchType);
        }
        return BranchType;
    }

    public void setBranchType(String aBranchType)
    {
        BranchType = aBranchType;
    }

    public double getFYCMin()
    {
        return FYCMin;
    }

    public void setFYCMin(double aFYCMin)
    {
        FYCMin = aFYCMin;
    }

    public void setFYCMin(String aFYCMin)
    {
        if (aFYCMin != null && !aFYCMin.equals(""))
        {
            Double tDouble = new Double(aFYCMin);
            double d = tDouble.doubleValue();
            FYCMin = d;
        }
    }

    public double getFYCMax()
    {
        return FYCMax;
    }

    public void setFYCMax(double aFYCMax)
    {
        FYCMax = aFYCMax;
    }

    public void setFYCMax(String aFYCMax)
    {
        if (aFYCMax != null && !aFYCMax.equals(""))
        {
            Double tDouble = new Double(aFYCMax);
            double d = tDouble.doubleValue();
            FYCMax = d;
        }
    }

    public double getDrawRate()
    {
        return DrawRate;
    }

    public void setDrawRate(double aDrawRate)
    {
        DrawRate = aDrawRate;
    }

    public void setDrawRate(String aDrawRate)
    {
        if (aDrawRate != null && !aDrawRate.equals(""))
        {
            Double tDouble = new Double(aDrawRate);
            double d = tDouble.doubleValue();
            DrawRate = d;
        }
    }

    public double getLimitPeriod()
    {
        return LimitPeriod;
    }

    public void setLimitPeriod(double aLimitPeriod)
    {
        LimitPeriod = aLimitPeriod;
    }

    public void setLimitPeriod(String aLimitPeriod)
    {
        if (aLimitPeriod != null && !aLimitPeriod.equals(""))
        {
            Double tDouble = new Double(aLimitPeriod);
            double d = tDouble.doubleValue();
            LimitPeriod = d;
        }
    }

    public double getRewardMoney()
    {
        return RewardMoney;
    }

    public void setRewardMoney(double aRewardMoney)
    {
        RewardMoney = aRewardMoney;
    }

    public void setRewardMoney(String aRewardMoney)
    {
        if (aRewardMoney != null && !aRewardMoney.equals(""))
        {
            Double tDouble = new Double(aRewardMoney);
            double d = tDouble.doubleValue();
            RewardMoney = d;
        }
    }

    public double getDrawStart()
    {
        return DrawStart;
    }

    public void setDrawStart(double aDrawStart)
    {
        DrawStart = aDrawStart;
    }

    public void setDrawStart(String aDrawStart)
    {
        if (aDrawStart != null && !aDrawStart.equals(""))
        {
            Double tDouble = new Double(aDrawStart);
            double d = tDouble.doubleValue();
            DrawStart = d;
        }
    }

    public double getDrawEnd()
    {
        return DrawEnd;
    }

    public void setDrawEnd(double aDrawEnd)
    {
        DrawEnd = aDrawEnd;
    }

    public void setDrawEnd(String aDrawEnd)
    {
        if (aDrawEnd != null && !aDrawEnd.equals(""))
        {
            Double tDouble = new Double(aDrawEnd);
            double d = tDouble.doubleValue();
            DrawEnd = d;
        }
    }

    public String getWageName()
    {
        if (SysConst.CHANGECHARSET && WageName != null && !WageName.equals(""))
        {
            WageName = StrTool.unicodeToGBK(WageName);
        }
        return WageName;
    }

    public void setWageName(String aWageName)
    {
        WageName = aWageName;
    }

    public double getDrawRateOth()
    {
        return DrawRateOth;
    }

    public void setDrawRateOth(double aDrawRateOth)
    {
        DrawRateOth = aDrawRateOth;
    }

    public void setDrawRateOth(String aDrawRateOth)
    {
        if (aDrawRateOth != null && !aDrawRateOth.equals(""))
        {
            Double tDouble = new Double(aDrawRateOth);
            double d = tDouble.doubleValue();
            DrawRateOth = d;
        }
    }

    public double getPremStand()
    {
        return PremStand;
    }

    public void setPremStand(double aPremStand)
    {
        PremStand = aPremStand;
    }

    public void setPremStand(String aPremStand)
    {
        if (aPremStand != null && !aPremStand.equals(""))
        {
            Double tDouble = new Double(aPremStand);
            double d = tDouble.doubleValue();
            PremStand = d;
        }
    }

    public String getRearType()
    {
        if (SysConst.CHANGECHARSET && RearType != null && !RearType.equals(""))
        {
            RearType = StrTool.unicodeToGBK(RearType);
        }
        return RearType;
    }

    public void setRearType(String aRearType)
    {
        RearType = aRearType;
    }

    public String getBranchType2()
    {
        if (SysConst.CHANGECHARSET && BranchType2 != null &&
            !BranchType2.equals(""))
        {
            BranchType2 = StrTool.unicodeToGBK(BranchType2);
        }
        return BranchType2;
    }

    public void setBranchType2(String aBranchType2)
    {
        BranchType2 = aBranchType2;
    }

    /**
     * 使用另外一个 LAWageRadixSchema 对象给 Schema 赋值
     * @param: aLAWageRadixSchema LAWageRadixSchema
     **/
    public void setSchema(LAWageRadixSchema aLAWageRadixSchema)
    {
        this.Idx = aLAWageRadixSchema.getIdx();
        this.AreaType = aLAWageRadixSchema.getAreaType();
        this.AgentGrade = aLAWageRadixSchema.getAgentGrade();
        this.WageCode = aLAWageRadixSchema.getWageCode();
        this.BranchType = aLAWageRadixSchema.getBranchType();
        this.FYCMin = aLAWageRadixSchema.getFYCMin();
        this.FYCMax = aLAWageRadixSchema.getFYCMax();
        this.DrawRate = aLAWageRadixSchema.getDrawRate();
        this.LimitPeriod = aLAWageRadixSchema.getLimitPeriod();
        this.RewardMoney = aLAWageRadixSchema.getRewardMoney();
        this.DrawStart = aLAWageRadixSchema.getDrawStart();
        this.DrawEnd = aLAWageRadixSchema.getDrawEnd();
        this.WageName = aLAWageRadixSchema.getWageName();
        this.DrawRateOth = aLAWageRadixSchema.getDrawRateOth();
        this.PremStand = aLAWageRadixSchema.getPremStand();
        this.RearType = aLAWageRadixSchema.getRearType();
        this.BranchType2 = aLAWageRadixSchema.getBranchType2();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            this.Idx = rs.getInt("Idx");
            if (rs.getString("AreaType") == null)
            {
                this.AreaType = null;
            }
            else
            {
                this.AreaType = rs.getString("AreaType").trim();
            }

            if (rs.getString("AgentGrade") == null)
            {
                this.AgentGrade = null;
            }
            else
            {
                this.AgentGrade = rs.getString("AgentGrade").trim();
            }

            if (rs.getString("WageCode") == null)
            {
                this.WageCode = null;
            }
            else
            {
                this.WageCode = rs.getString("WageCode").trim();
            }

            if (rs.getString("BranchType") == null)
            {
                this.BranchType = null;
            }
            else
            {
                this.BranchType = rs.getString("BranchType").trim();
            }

            this.FYCMin = rs.getDouble("FYCMin");
            this.FYCMax = rs.getDouble("FYCMax");
            this.DrawRate = rs.getDouble("DrawRate");
            this.LimitPeriod = rs.getDouble("LimitPeriod");
            this.RewardMoney = rs.getDouble("RewardMoney");
            this.DrawStart = rs.getDouble("DrawStart");
            this.DrawEnd = rs.getDouble("DrawEnd");
            if (rs.getString("WageName") == null)
            {
                this.WageName = null;
            }
            else
            {
                this.WageName = rs.getString("WageName").trim();
            }

            this.DrawRateOth = rs.getDouble("DrawRateOth");
            this.PremStand = rs.getDouble("PremStand");
            if (rs.getString("RearType") == null)
            {
                this.RearType = null;
            }
            else
            {
                this.RearType = rs.getString("RearType").trim();
            }

            if (rs.getString("BranchType2") == null)
            {
                this.BranchType2 = null;
            }
            else
            {
                this.BranchType2 = rs.getString("BranchType2").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageRadixSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAWageRadixSchema getSchema()
    {
        LAWageRadixSchema aLAWageRadixSchema = new LAWageRadixSchema();
        aLAWageRadixSchema.setSchema(this);
        return aLAWageRadixSchema;
    }

    public LAWageRadixDB getDB()
    {
        LAWageRadixDB aDBOper = new LAWageRadixDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageRadix描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(Idx));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AreaType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AgentGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(WageCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FYCMin));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(FYCMax));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(LimitPeriod));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RewardMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawStart));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawEnd));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(WageName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DrawRateOth));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PremStand));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RearType)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(BranchType2)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAWageRadix>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            Idx = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    1, SysConst.PACKAGESPILTER))).intValue();
            AreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            AgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            WageCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            FYCMin = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    6, SysConst.PACKAGESPILTER))).doubleValue();
            FYCMax = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    7, SysConst.PACKAGESPILTER))).doubleValue();
            DrawRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).doubleValue();
            LimitPeriod = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).doubleValue();
            RewardMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).doubleValue();
            DrawStart = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 11, SysConst.PACKAGESPILTER))).doubleValue();
            DrawEnd = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 12, SysConst.PACKAGESPILTER))).doubleValue();
            WageName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            DrawRateOth = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 14, SysConst.PACKAGESPILTER))).doubleValue();
            PremStand = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 15, SysConst.PACKAGESPILTER))).doubleValue();
            RearType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageRadixSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("Idx"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
        }
        if (FCode.equals("AreaType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaType));
        }
        if (FCode.equals("AgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGrade));
        }
        if (FCode.equals("WageCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WageCode));
        }
        if (FCode.equals("BranchType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
        }
        if (FCode.equals("FYCMin"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FYCMin));
        }
        if (FCode.equals("FYCMax"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FYCMax));
        }
        if (FCode.equals("DrawRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawRate));
        }
        if (FCode.equals("LimitPeriod"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(LimitPeriod));
        }
        if (FCode.equals("RewardMoney"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RewardMoney));
        }
        if (FCode.equals("DrawStart"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawStart));
        }
        if (FCode.equals("DrawEnd"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawEnd));
        }
        if (FCode.equals("WageName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WageName));
        }
        if (FCode.equals("DrawRateOth"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DrawRateOth));
        }
        if (FCode.equals("PremStand"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PremStand));
        }
        if (FCode.equals("RearType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RearType));
        }
        if (FCode.equals("BranchType2"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = String.valueOf(Idx);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(AreaType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(AgentGrade);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(WageCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(BranchType);
                break;
            case 5:
                strFieldValue = String.valueOf(FYCMin);
                break;
            case 6:
                strFieldValue = String.valueOf(FYCMax);
                break;
            case 7:
                strFieldValue = String.valueOf(DrawRate);
                break;
            case 8:
                strFieldValue = String.valueOf(LimitPeriod);
                break;
            case 9:
                strFieldValue = String.valueOf(RewardMoney);
                break;
            case 10:
                strFieldValue = String.valueOf(DrawStart);
                break;
            case 11:
                strFieldValue = String.valueOf(DrawEnd);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(WageName);
                break;
            case 13:
                strFieldValue = String.valueOf(DrawRateOth);
                break;
            case 14:
                strFieldValue = String.valueOf(PremStand);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(RearType);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(BranchType2);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("Idx"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                Idx = i;
            }
        }
        if (FCode.equals("AreaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AreaType = FValue.trim();
            }
            else
            {
                AreaType = null;
            }
        }
        if (FCode.equals("AgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentGrade = FValue.trim();
            }
            else
            {
                AgentGrade = null;
            }
        }
        if (FCode.equals("WageCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WageCode = FValue.trim();
            }
            else
            {
                WageCode = null;
            }
        }
        if (FCode.equals("BranchType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType = FValue.trim();
            }
            else
            {
                BranchType = null;
            }
        }
        if (FCode.equals("FYCMin"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FYCMin = d;
            }
        }
        if (FCode.equals("FYCMax"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                FYCMax = d;
            }
        }
        if (FCode.equals("DrawRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawRate = d;
            }
        }
        if (FCode.equals("LimitPeriod"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                LimitPeriod = d;
            }
        }
        if (FCode.equals("RewardMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RewardMoney = d;
            }
        }
        if (FCode.equals("DrawStart"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawStart = d;
            }
        }
        if (FCode.equals("DrawEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawEnd = d;
            }
        }
        if (FCode.equals("WageName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                WageName = FValue.trim();
            }
            else
            {
                WageName = null;
            }
        }
        if (FCode.equals("DrawRateOth"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DrawRateOth = d;
            }
        }
        if (FCode.equals("PremStand"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                PremStand = d;
            }
        }
        if (FCode.equals("RearType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RearType = FValue.trim();
            }
            else
            {
                RearType = null;
            }
        }
        if (FCode.equals("BranchType2"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                BranchType2 = FValue.trim();
            }
            else
            {
                BranchType2 = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAWageRadixSchema other = (LAWageRadixSchema) otherObject;
        return
                Idx == other.getIdx()
                && AreaType.equals(other.getAreaType())
                && AgentGrade.equals(other.getAgentGrade())
                && WageCode.equals(other.getWageCode())
                && BranchType.equals(other.getBranchType())
                && FYCMin == other.getFYCMin()
                && FYCMax == other.getFYCMax()
                && DrawRate == other.getDrawRate()
                && LimitPeriod == other.getLimitPeriod()
                && RewardMoney == other.getRewardMoney()
                && DrawStart == other.getDrawStart()
                && DrawEnd == other.getDrawEnd()
                && WageName.equals(other.getWageName())
                && DrawRateOth == other.getDrawRateOth()
                && PremStand == other.getPremStand()
                && RearType.equals(other.getRearType())
                && BranchType2.equals(other.getBranchType2());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("Idx"))
        {
            return 0;
        }
        if (strFieldName.equals("AreaType"))
        {
            return 1;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return 2;
        }
        if (strFieldName.equals("WageCode"))
        {
            return 3;
        }
        if (strFieldName.equals("BranchType"))
        {
            return 4;
        }
        if (strFieldName.equals("FYCMin"))
        {
            return 5;
        }
        if (strFieldName.equals("FYCMax"))
        {
            return 6;
        }
        if (strFieldName.equals("DrawRate"))
        {
            return 7;
        }
        if (strFieldName.equals("LimitPeriod"))
        {
            return 8;
        }
        if (strFieldName.equals("RewardMoney"))
        {
            return 9;
        }
        if (strFieldName.equals("DrawStart"))
        {
            return 10;
        }
        if (strFieldName.equals("DrawEnd"))
        {
            return 11;
        }
        if (strFieldName.equals("WageName"))
        {
            return 12;
        }
        if (strFieldName.equals("DrawRateOth"))
        {
            return 13;
        }
        if (strFieldName.equals("PremStand"))
        {
            return 14;
        }
        if (strFieldName.equals("RearType"))
        {
            return 15;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return 16;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "Idx";
                break;
            case 1:
                strFieldName = "AreaType";
                break;
            case 2:
                strFieldName = "AgentGrade";
                break;
            case 3:
                strFieldName = "WageCode";
                break;
            case 4:
                strFieldName = "BranchType";
                break;
            case 5:
                strFieldName = "FYCMin";
                break;
            case 6:
                strFieldName = "FYCMax";
                break;
            case 7:
                strFieldName = "DrawRate";
                break;
            case 8:
                strFieldName = "LimitPeriod";
                break;
            case 9:
                strFieldName = "RewardMoney";
                break;
            case 10:
                strFieldName = "DrawStart";
                break;
            case 11:
                strFieldName = "DrawEnd";
                break;
            case 12:
                strFieldName = "WageName";
                break;
            case 13:
                strFieldName = "DrawRateOth";
                break;
            case 14:
                strFieldName = "PremStand";
                break;
            case 15:
                strFieldName = "RearType";
                break;
            case 16:
                strFieldName = "BranchType2";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("Idx"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("AreaType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WageCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FYCMin"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("FYCMax"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("LimitPeriod"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RewardMoney"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawStart"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DrawEnd"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("WageName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DrawRateOth"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("PremStand"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RearType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BranchType2"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_INT;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 7:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 8:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 9:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 10:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 11:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 14:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
