/*
 * <p>ClassName: LDSysVarSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDSysVarSchema implements Schema
{
    // @Field
    /** 系统变量名 */
    private String SysVar;
    /** 系统变量类型 */
    private String SysVarType;
    /** 系统变量值 */
    private String SysVarValue;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDSysVarSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SysVar";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSysVar()
    {
        if (SysVar != null && !SysVar.equals("") && SysConst.CHANGECHARSET == true)
        {
            SysVar = StrTool.unicodeToGBK(SysVar);
        }
        return SysVar;
    }

    public void setSysVar(String aSysVar)
    {
        SysVar = aSysVar;
    }

    public String getSysVarType()
    {
        if (SysVarType != null && !SysVarType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SysVarType = StrTool.unicodeToGBK(SysVarType);
        }
        return SysVarType;
    }

    public void setSysVarType(String aSysVarType)
    {
        SysVarType = aSysVarType;
    }

    public String getSysVarValue()
    {
        if (SysVarValue != null && !SysVarValue.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            SysVarValue = StrTool.unicodeToGBK(SysVarValue);
        }
        return SysVarValue;
    }

    public void setSysVarValue(String aSysVarValue)
    {
        SysVarValue = aSysVarValue;
    }

    /**
     * 使用另外一个 LDSysVarSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDSysVarSchema aLDSysVarSchema)
    {
        this.SysVar = aLDSysVarSchema.getSysVar();
        this.SysVarType = aLDSysVarSchema.getSysVarType();
        this.SysVarValue = aLDSysVarSchema.getSysVarValue();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SysVar") == null)
            {
                this.SysVar = null;
            }
            else
            {
                this.SysVar = rs.getString("SysVar").trim();
            }

            if (rs.getString("SysVarType") == null)
            {
                this.SysVarType = null;
            }
            else
            {
                this.SysVarType = rs.getString("SysVarType").trim();
            }

            if (rs.getString("SysVarValue") == null)
            {
                this.SysVarValue = null;
            }
            else
            {
                this.SysVarValue = rs.getString("SysVarValue").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSysVarSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDSysVarSchema getSchema()
    {
        LDSysVarSchema aLDSysVarSchema = new LDSysVarSchema();
        aLDSysVarSchema.setSchema(this);
        return aLDSysVarSchema;
    }

    public LDSysVarDB getDB()
    {
        LDSysVarDB aDBOper = new LDSysVarDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDSysVar描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SysVar)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SysVarType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(SysVarValue));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDSysVar>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SysVar = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            SysVarType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            SysVarValue = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSysVarSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SysVar"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SysVar));
        }
        if (FCode.equals("SysVarType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SysVarType));
        }
        if (FCode.equals("SysVarValue"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SysVarValue));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SysVar);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(SysVarType);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(SysVarValue);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SysVar"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SysVar = FValue.trim();
            }
            else
            {
                SysVar = null;
            }
        }
        if (FCode.equals("SysVarType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SysVarType = FValue.trim();
            }
            else
            {
                SysVarType = null;
            }
        }
        if (FCode.equals("SysVarValue"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SysVarValue = FValue.trim();
            }
            else
            {
                SysVarValue = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDSysVarSchema other = (LDSysVarSchema) otherObject;
        return
                SysVar.equals(other.getSysVar())
                && SysVarType.equals(other.getSysVarType())
                && SysVarValue.equals(other.getSysVarValue());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SysVar"))
        {
            return 0;
        }
        if (strFieldName.equals("SysVarType"))
        {
            return 1;
        }
        if (strFieldName.equals("SysVarValue"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SysVar";
                break;
            case 1:
                strFieldName = "SysVarType";
                break;
            case 2:
                strFieldName = "SysVarValue";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SysVar"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SysVarType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SysVarValue"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
