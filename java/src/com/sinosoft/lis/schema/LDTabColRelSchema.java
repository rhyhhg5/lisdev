/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDTabColRelDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;


/*
 * <p>ClassName: LDTabColRelSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 费率表动态显示
 * @CreateDate：2005-04-08
 */
public class LDTabColRelSchema implements Schema
{
    // @Field
    /** 表编码 */
    private String TableCode;

    /** 表名称 */
    private String TableName;

    /** 列编码 */
    private String ColumnCode;

    /** 列名称 */
    private String ColumnName;

    /** 列序号 */
    private int SerialNo;

    /** 备注 */
    private String Remark;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息


    // @Constructor
    public LDTabColRelSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "TableCode";
        pk[1] = "ColumnCode";
        pk[2] = "SerialNo";

        PK = pk;
    }


    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getTableCode()
    {
        if (SysConst.CHANGECHARSET && TableCode != null && !TableCode.equals(""))
        {
            TableCode = StrTool.unicodeToGBK(TableCode);
        }
        return TableCode;
    }

    public void setTableCode(String aTableCode)
    {
        TableCode = aTableCode;
    }

    public String getTableName()
    {
        if (SysConst.CHANGECHARSET && TableName != null && !TableName.equals(""))
        {
            TableName = StrTool.unicodeToGBK(TableName);
        }
        return TableName;
    }

    public void setTableName(String aTableName)
    {
        TableName = aTableName;
    }

    public String getColumnCode()
    {
        if (SysConst.CHANGECHARSET && ColumnCode != null &&
            !ColumnCode.equals(""))
        {
            ColumnCode = StrTool.unicodeToGBK(ColumnCode);
        }
        return ColumnCode;
    }

    public void setColumnCode(String aColumnCode)
    {
        ColumnCode = aColumnCode;
    }

    public String getColumnName()
    {
        if (SysConst.CHANGECHARSET && ColumnName != null &&
            !ColumnName.equals(""))
        {
            ColumnName = StrTool.unicodeToGBK(ColumnName);
        }
        return ColumnName;
    }

    public void setColumnName(String aColumnName)
    {
        ColumnName = aColumnName;
    }

    public int getSerialNo()
    {
        return SerialNo;
    }

    public void setSerialNo(int aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        if (aSerialNo != null && !aSerialNo.equals(""))
        {
            Integer tInteger = new Integer(aSerialNo);
            int i = tInteger.intValue();
            SerialNo = i;
        }
    }

    public String getRemark()
    {
        if (SysConst.CHANGECHARSET && Remark != null && !Remark.equals(""))
        {
            Remark = StrTool.unicodeToGBK(Remark);
        }
        return Remark;
    }

    public void setRemark(String aRemark)
    {
        Remark = aRemark;
    }


    /**
     * 使用另外一个 LDTabColRelSchema 对象给 Schema 赋值
     * @param: aLDTabColRelSchema LDTabColRelSchema
     **/
    public void setSchema(LDTabColRelSchema aLDTabColRelSchema)
    {
        this.TableCode = aLDTabColRelSchema.getTableCode();
        this.TableName = aLDTabColRelSchema.getTableName();
        this.ColumnCode = aLDTabColRelSchema.getColumnCode();
        this.ColumnName = aLDTabColRelSchema.getColumnName();
        this.SerialNo = aLDTabColRelSchema.getSerialNo();
        this.Remark = aLDTabColRelSchema.getRemark();
    }


    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("TableCode") == null)
            {
                this.TableCode = null;
            }
            else
            {
                this.TableCode = rs.getString("TableCode").trim();
            }

            if (rs.getString("TableName") == null)
            {
                this.TableName = null;
            }
            else
            {
                this.TableName = rs.getString("TableName").trim();
            }

            if (rs.getString("ColumnCode") == null)
            {
                this.ColumnCode = null;
            }
            else
            {
                this.ColumnCode = rs.getString("ColumnCode").trim();
            }

            if (rs.getString("ColumnName") == null)
            {
                this.ColumnName = null;
            }
            else
            {
                this.ColumnName = rs.getString("ColumnName").trim();
            }

            this.SerialNo = rs.getInt("SerialNo");
            if (rs.getString("Remark") == null)
            {
                this.Remark = null;
            }
            else
            {
                this.Remark = rs.getString("Remark").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTabColRelSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDTabColRelSchema getSchema()
    {
        LDTabColRelSchema aLDTabColRelSchema = new LDTabColRelSchema();
        aLDTabColRelSchema.setSchema(this);
        return aLDTabColRelSchema;
    }

    public LDTabColRelDB getDB()
    {
        LDTabColRelDB aDBOper = new LDTabColRelDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTabColRel描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(TableCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(TableName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ColumnCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ColumnName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Remark)));
        return strReturn.toString();
    }


    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDTabColRel>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            TableCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            TableName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                       SysConst.PACKAGESPILTER);
            ColumnCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                        SysConst.PACKAGESPILTER);
            ColumnName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            SerialNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTabColRelSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }


    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("TableCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TableCode));
        }
        if (FCode.equals("TableName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TableName));
        }
        if (FCode.equals("ColumnCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ColumnCode));
        }
        if (FCode.equals("ColumnName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ColumnName));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("Remark"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(TableCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(TableName);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ColumnCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(ColumnName);
                break;
            case 4:
                strFieldValue = String.valueOf(SerialNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(Remark);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }


    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("TableCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TableCode = FValue.trim();
            }
            else
            {
                TableCode = null;
            }
        }
        if (FCode.equals("TableName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                TableName = FValue.trim();
            }
            else
            {
                TableName = null;
            }
        }
        if (FCode.equals("ColumnCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ColumnCode = FValue.trim();
            }
            else
            {
                ColumnCode = null;
            }
        }
        if (FCode.equals("ColumnName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ColumnName = FValue.trim();
            }
            else
            {
                ColumnName = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SerialNo = i;
            }
        }
        if (FCode.equals("Remark"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Remark = FValue.trim();
            }
            else
            {
                Remark = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDTabColRelSchema other = (LDTabColRelSchema) otherObject;
        return
                TableCode.equals(other.getTableCode())
                && TableName.equals(other.getTableName())
                && ColumnCode.equals(other.getColumnCode())
                && ColumnName.equals(other.getColumnName())
                && SerialNo == other.getSerialNo()
                && Remark.equals(other.getRemark());
    }


    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }


    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("TableCode"))
        {
            return 0;
        }
        if (strFieldName.equals("TableName"))
        {
            return 1;
        }
        if (strFieldName.equals("ColumnCode"))
        {
            return 2;
        }
        if (strFieldName.equals("ColumnName"))
        {
            return 3;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 4;
        }
        if (strFieldName.equals("Remark"))
        {
            return 5;
        }
        return -1;
    }


    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "TableCode";
                break;
            case 1:
                strFieldName = "TableName";
                break;
            case 2:
                strFieldName = "ColumnCode";
                break;
            case 3:
                strFieldName = "ColumnName";
                break;
            case 4:
                strFieldName = "SerialNo";
                break;
            case 5:
                strFieldName = "Remark";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }


    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("TableCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TableName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ColumnCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ColumnName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("Remark"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }


    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
