/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LKGrpAppntDB;

/*
 * <p>ClassName: LKGrpAppntSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 在线报价系统模型
 * @CreateDate：2019-09-02
 */
public class LKGrpAppntSchema implements Schema, Cloneable
{
	// @Field
	/** 报价单号码 */
	private String PrtNo;
	/** 客户号码 */
	private String CustomerNo;
	/** 单位名称 */
	private String Name;
	/** 通讯地址 */
	private String PostalAddress;
	/** 单位邮编 */
	private String ZipCode;
	/** 单位电话 */
	private String Phone;
	/** 组织机构代码 */
	private String OrganComCode;
	/** 税务登记证号码 */
	private String TaxNo;
	/** 统一社会信用代码 */
	private String UnifiedSocialCreditNo;
	/** 总人数 */
	private int Peoples;
	/** 在职人数 */
	private int OnWorkPeoples;
	/** 退休人数 */
	private int OffWorkPeoples;
	/** 其它人员人数 */
	private int OtherPeoples;
	/** 手机 */
	private String Mobile;
	/** 经营范围 */
	private String BusinessScope;
	/** 省 */
	private String PostalProvince;
	/** 市 */
	private String PostalCity;
	/** 县 */
	private String PostalCounty;
	/** 详细地址 */
	private String DetailAddress;
	/** 平均年龄 */
	private double AvgAge;
	/** 最高年龄 */
	private int MaxAge;
	/** 最低年龄 */
	private int MinAge;
	/** 单位所在地 */
	private String UnitLocation;
	/** 单位所在省 */
	private String UnitProvince;
	/** 单位所在市 */
	private String UnitCity;
	/** 单位所在县 */
	private String UnitCounty;
	/** 单位详细地址 */
	private String UnitAddress;
	/** 联系人姓名 */
	private String LinkMan;
	/** 证件类型 */
	private String LinkManIDType;
	/** 证件号码 */
	private String LinkManIDNo;
	/** 固定电话 */
	private String LinkManPhone;
	/** 移动电话 */
	private String LinkManMobile;
	/** 电子邮箱 */
	private String LinkManEmail;
	/** 法人 */
	private String LegalPersonName;
	/** 法人证件类型 */
	private String LegalPersonIDType;
	/** 法人证件号码 */
	private String LegalPersonIDNo;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 41;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LKGrpAppntSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "PrtNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LKGrpAppntSchema cloned = (LKGrpAppntSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getPrtNo()
	{
		return PrtNo;
	}
	public void setPrtNo(String aPrtNo)
	{
		PrtNo = aPrtNo;
	}
	public String getCustomerNo()
	{
		return CustomerNo;
	}
	public void setCustomerNo(String aCustomerNo)
	{
		CustomerNo = aCustomerNo;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getPostalAddress()
	{
		return PostalAddress;
	}
	public void setPostalAddress(String aPostalAddress)
	{
		PostalAddress = aPostalAddress;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getOrganComCode()
	{
		return OrganComCode;
	}
	public void setOrganComCode(String aOrganComCode)
	{
		OrganComCode = aOrganComCode;
	}
	public String getTaxNo()
	{
		return TaxNo;
	}
	public void setTaxNo(String aTaxNo)
	{
		TaxNo = aTaxNo;
	}
	public String getUnifiedSocialCreditNo()
	{
		return UnifiedSocialCreditNo;
	}
	public void setUnifiedSocialCreditNo(String aUnifiedSocialCreditNo)
	{
		UnifiedSocialCreditNo = aUnifiedSocialCreditNo;
	}
	public int getPeoples()
	{
		return Peoples;
	}
	public void setPeoples(int aPeoples)
	{
		Peoples = aPeoples;
	}
	public void setPeoples(String aPeoples)
	{
		if (aPeoples != null && !aPeoples.equals(""))
		{
			Integer tInteger = new Integer(aPeoples);
			int i = tInteger.intValue();
			Peoples = i;
		}
	}

	public int getOnWorkPeoples()
	{
		return OnWorkPeoples;
	}
	public void setOnWorkPeoples(int aOnWorkPeoples)
	{
		OnWorkPeoples = aOnWorkPeoples;
	}
	public void setOnWorkPeoples(String aOnWorkPeoples)
	{
		if (aOnWorkPeoples != null && !aOnWorkPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOnWorkPeoples);
			int i = tInteger.intValue();
			OnWorkPeoples = i;
		}
	}

	public int getOffWorkPeoples()
	{
		return OffWorkPeoples;
	}
	public void setOffWorkPeoples(int aOffWorkPeoples)
	{
		OffWorkPeoples = aOffWorkPeoples;
	}
	public void setOffWorkPeoples(String aOffWorkPeoples)
	{
		if (aOffWorkPeoples != null && !aOffWorkPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOffWorkPeoples);
			int i = tInteger.intValue();
			OffWorkPeoples = i;
		}
	}

	public int getOtherPeoples()
	{
		return OtherPeoples;
	}
	public void setOtherPeoples(int aOtherPeoples)
	{
		OtherPeoples = aOtherPeoples;
	}
	public void setOtherPeoples(String aOtherPeoples)
	{
		if (aOtherPeoples != null && !aOtherPeoples.equals(""))
		{
			Integer tInteger = new Integer(aOtherPeoples);
			int i = tInteger.intValue();
			OtherPeoples = i;
		}
	}

	public String getMobile()
	{
		return Mobile;
	}
	public void setMobile(String aMobile)
	{
		Mobile = aMobile;
	}
	public String getBusinessScope()
	{
		return BusinessScope;
	}
	public void setBusinessScope(String aBusinessScope)
	{
		BusinessScope = aBusinessScope;
	}
	public String getPostalProvince()
	{
		return PostalProvince;
	}
	public void setPostalProvince(String aPostalProvince)
	{
		PostalProvince = aPostalProvince;
	}
	public String getPostalCity()
	{
		return PostalCity;
	}
	public void setPostalCity(String aPostalCity)
	{
		PostalCity = aPostalCity;
	}
	public String getPostalCounty()
	{
		return PostalCounty;
	}
	public void setPostalCounty(String aPostalCounty)
	{
		PostalCounty = aPostalCounty;
	}
	public String getDetailAddress()
	{
		return DetailAddress;
	}
	public void setDetailAddress(String aDetailAddress)
	{
		DetailAddress = aDetailAddress;
	}
	public double getAvgAge()
	{
		return AvgAge;
	}
	public void setAvgAge(double aAvgAge)
	{
		AvgAge = Arith.round(aAvgAge,2);
	}
	public void setAvgAge(String aAvgAge)
	{
		if (aAvgAge != null && !aAvgAge.equals(""))
		{
			Double tDouble = new Double(aAvgAge);
			double d = tDouble.doubleValue();
                AvgAge = Arith.round(d,2);
		}
	}

	public int getMaxAge()
	{
		return MaxAge;
	}
	public void setMaxAge(int aMaxAge)
	{
		MaxAge = aMaxAge;
	}
	public void setMaxAge(String aMaxAge)
	{
		if (aMaxAge != null && !aMaxAge.equals(""))
		{
			Integer tInteger = new Integer(aMaxAge);
			int i = tInteger.intValue();
			MaxAge = i;
		}
	}

	public int getMinAge()
	{
		return MinAge;
	}
	public void setMinAge(int aMinAge)
	{
		MinAge = aMinAge;
	}
	public void setMinAge(String aMinAge)
	{
		if (aMinAge != null && !aMinAge.equals(""))
		{
			Integer tInteger = new Integer(aMinAge);
			int i = tInteger.intValue();
			MinAge = i;
		}
	}

	public String getUnitLocation()
	{
		return UnitLocation;
	}
	public void setUnitLocation(String aUnitLocation)
	{
		UnitLocation = aUnitLocation;
	}
	public String getUnitProvince()
	{
		return UnitProvince;
	}
	public void setUnitProvince(String aUnitProvince)
	{
		UnitProvince = aUnitProvince;
	}
	public String getUnitCity()
	{
		return UnitCity;
	}
	public void setUnitCity(String aUnitCity)
	{
		UnitCity = aUnitCity;
	}
	public String getUnitCounty()
	{
		return UnitCounty;
	}
	public void setUnitCounty(String aUnitCounty)
	{
		UnitCounty = aUnitCounty;
	}
	public String getUnitAddress()
	{
		return UnitAddress;
	}
	public void setUnitAddress(String aUnitAddress)
	{
		UnitAddress = aUnitAddress;
	}
	public String getLinkMan()
	{
		return LinkMan;
	}
	public void setLinkMan(String aLinkMan)
	{
		LinkMan = aLinkMan;
	}
	public String getLinkManIDType()
	{
		return LinkManIDType;
	}
	public void setLinkManIDType(String aLinkManIDType)
	{
		LinkManIDType = aLinkManIDType;
	}
	public String getLinkManIDNo()
	{
		return LinkManIDNo;
	}
	public void setLinkManIDNo(String aLinkManIDNo)
	{
		LinkManIDNo = aLinkManIDNo;
	}
	public String getLinkManPhone()
	{
		return LinkManPhone;
	}
	public void setLinkManPhone(String aLinkManPhone)
	{
		LinkManPhone = aLinkManPhone;
	}
	public String getLinkManMobile()
	{
		return LinkManMobile;
	}
	public void setLinkManMobile(String aLinkManMobile)
	{
		LinkManMobile = aLinkManMobile;
	}
	public String getLinkManEmail()
	{
		return LinkManEmail;
	}
	public void setLinkManEmail(String aLinkManEmail)
	{
		LinkManEmail = aLinkManEmail;
	}
	public String getLegalPersonName()
	{
		return LegalPersonName;
	}
	public void setLegalPersonName(String aLegalPersonName)
	{
		LegalPersonName = aLegalPersonName;
	}
	public String getLegalPersonIDType()
	{
		return LegalPersonIDType;
	}
	public void setLegalPersonIDType(String aLegalPersonIDType)
	{
		LegalPersonIDType = aLegalPersonIDType;
	}
	public String getLegalPersonIDNo()
	{
		return LegalPersonIDNo;
	}
	public void setLegalPersonIDNo(String aLegalPersonIDNo)
	{
		LegalPersonIDNo = aLegalPersonIDNo;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LKGrpAppntSchema 对象给 Schema 赋值
	* @param: aLKGrpAppntSchema LKGrpAppntSchema
	**/
	public void setSchema(LKGrpAppntSchema aLKGrpAppntSchema)
	{
		this.PrtNo = aLKGrpAppntSchema.getPrtNo();
		this.CustomerNo = aLKGrpAppntSchema.getCustomerNo();
		this.Name = aLKGrpAppntSchema.getName();
		this.PostalAddress = aLKGrpAppntSchema.getPostalAddress();
		this.ZipCode = aLKGrpAppntSchema.getZipCode();
		this.Phone = aLKGrpAppntSchema.getPhone();
		this.OrganComCode = aLKGrpAppntSchema.getOrganComCode();
		this.TaxNo = aLKGrpAppntSchema.getTaxNo();
		this.UnifiedSocialCreditNo = aLKGrpAppntSchema.getUnifiedSocialCreditNo();
		this.Peoples = aLKGrpAppntSchema.getPeoples();
		this.OnWorkPeoples = aLKGrpAppntSchema.getOnWorkPeoples();
		this.OffWorkPeoples = aLKGrpAppntSchema.getOffWorkPeoples();
		this.OtherPeoples = aLKGrpAppntSchema.getOtherPeoples();
		this.Mobile = aLKGrpAppntSchema.getMobile();
		this.BusinessScope = aLKGrpAppntSchema.getBusinessScope();
		this.PostalProvince = aLKGrpAppntSchema.getPostalProvince();
		this.PostalCity = aLKGrpAppntSchema.getPostalCity();
		this.PostalCounty = aLKGrpAppntSchema.getPostalCounty();
		this.DetailAddress = aLKGrpAppntSchema.getDetailAddress();
		this.AvgAge = aLKGrpAppntSchema.getAvgAge();
		this.MaxAge = aLKGrpAppntSchema.getMaxAge();
		this.MinAge = aLKGrpAppntSchema.getMinAge();
		this.UnitLocation = aLKGrpAppntSchema.getUnitLocation();
		this.UnitProvince = aLKGrpAppntSchema.getUnitProvince();
		this.UnitCity = aLKGrpAppntSchema.getUnitCity();
		this.UnitCounty = aLKGrpAppntSchema.getUnitCounty();
		this.UnitAddress = aLKGrpAppntSchema.getUnitAddress();
		this.LinkMan = aLKGrpAppntSchema.getLinkMan();
		this.LinkManIDType = aLKGrpAppntSchema.getLinkManIDType();
		this.LinkManIDNo = aLKGrpAppntSchema.getLinkManIDNo();
		this.LinkManPhone = aLKGrpAppntSchema.getLinkManPhone();
		this.LinkManMobile = aLKGrpAppntSchema.getLinkManMobile();
		this.LinkManEmail = aLKGrpAppntSchema.getLinkManEmail();
		this.LegalPersonName = aLKGrpAppntSchema.getLegalPersonName();
		this.LegalPersonIDType = aLKGrpAppntSchema.getLegalPersonIDType();
		this.LegalPersonIDNo = aLKGrpAppntSchema.getLegalPersonIDNo();
		this.Operator = aLKGrpAppntSchema.getOperator();
		this.MakeDate = fDate.getDate( aLKGrpAppntSchema.getMakeDate());
		this.MakeTime = aLKGrpAppntSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLKGrpAppntSchema.getModifyDate());
		this.ModifyTime = aLKGrpAppntSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("PrtNo") == null )
				this.PrtNo = null;
			else
				this.PrtNo = rs.getString("PrtNo").trim();

			if( rs.getString("CustomerNo") == null )
				this.CustomerNo = null;
			else
				this.CustomerNo = rs.getString("CustomerNo").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("PostalAddress") == null )
				this.PostalAddress = null;
			else
				this.PostalAddress = rs.getString("PostalAddress").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("OrganComCode") == null )
				this.OrganComCode = null;
			else
				this.OrganComCode = rs.getString("OrganComCode").trim();

			if( rs.getString("TaxNo") == null )
				this.TaxNo = null;
			else
				this.TaxNo = rs.getString("TaxNo").trim();

			if( rs.getString("UnifiedSocialCreditNo") == null )
				this.UnifiedSocialCreditNo = null;
			else
				this.UnifiedSocialCreditNo = rs.getString("UnifiedSocialCreditNo").trim();

			this.Peoples = rs.getInt("Peoples");
			this.OnWorkPeoples = rs.getInt("OnWorkPeoples");
			this.OffWorkPeoples = rs.getInt("OffWorkPeoples");
			this.OtherPeoples = rs.getInt("OtherPeoples");
			if( rs.getString("Mobile") == null )
				this.Mobile = null;
			else
				this.Mobile = rs.getString("Mobile").trim();

			if( rs.getString("BusinessScope") == null )
				this.BusinessScope = null;
			else
				this.BusinessScope = rs.getString("BusinessScope").trim();

			if( rs.getString("PostalProvince") == null )
				this.PostalProvince = null;
			else
				this.PostalProvince = rs.getString("PostalProvince").trim();

			if( rs.getString("PostalCity") == null )
				this.PostalCity = null;
			else
				this.PostalCity = rs.getString("PostalCity").trim();

			if( rs.getString("PostalCounty") == null )
				this.PostalCounty = null;
			else
				this.PostalCounty = rs.getString("PostalCounty").trim();

			if( rs.getString("DetailAddress") == null )
				this.DetailAddress = null;
			else
				this.DetailAddress = rs.getString("DetailAddress").trim();

			this.AvgAge = rs.getDouble("AvgAge");
			this.MaxAge = rs.getInt("MaxAge");
			this.MinAge = rs.getInt("MinAge");
			if( rs.getString("UnitLocation") == null )
				this.UnitLocation = null;
			else
				this.UnitLocation = rs.getString("UnitLocation").trim();

			if( rs.getString("UnitProvince") == null )
				this.UnitProvince = null;
			else
				this.UnitProvince = rs.getString("UnitProvince").trim();

			if( rs.getString("UnitCity") == null )
				this.UnitCity = null;
			else
				this.UnitCity = rs.getString("UnitCity").trim();

			if( rs.getString("UnitCounty") == null )
				this.UnitCounty = null;
			else
				this.UnitCounty = rs.getString("UnitCounty").trim();

			if( rs.getString("UnitAddress") == null )
				this.UnitAddress = null;
			else
				this.UnitAddress = rs.getString("UnitAddress").trim();

			if( rs.getString("LinkMan") == null )
				this.LinkMan = null;
			else
				this.LinkMan = rs.getString("LinkMan").trim();

			if( rs.getString("LinkManIDType") == null )
				this.LinkManIDType = null;
			else
				this.LinkManIDType = rs.getString("LinkManIDType").trim();

			if( rs.getString("LinkManIDNo") == null )
				this.LinkManIDNo = null;
			else
				this.LinkManIDNo = rs.getString("LinkManIDNo").trim();

			if( rs.getString("LinkManPhone") == null )
				this.LinkManPhone = null;
			else
				this.LinkManPhone = rs.getString("LinkManPhone").trim();

			if( rs.getString("LinkManMobile") == null )
				this.LinkManMobile = null;
			else
				this.LinkManMobile = rs.getString("LinkManMobile").trim();

			if( rs.getString("LinkManEmail") == null )
				this.LinkManEmail = null;
			else
				this.LinkManEmail = rs.getString("LinkManEmail").trim();

			if( rs.getString("LegalPersonName") == null )
				this.LegalPersonName = null;
			else
				this.LegalPersonName = rs.getString("LegalPersonName").trim();

			if( rs.getString("LegalPersonIDType") == null )
				this.LegalPersonIDType = null;
			else
				this.LegalPersonIDType = rs.getString("LegalPersonIDType").trim();

			if( rs.getString("LegalPersonIDNo") == null )
				this.LegalPersonIDNo = null;
			else
				this.LegalPersonIDNo = rs.getString("LegalPersonIDNo").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LKGrpAppnt表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKGrpAppntSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LKGrpAppntSchema getSchema()
	{
		LKGrpAppntSchema aLKGrpAppntSchema = new LKGrpAppntSchema();
		aLKGrpAppntSchema.setSchema(this);
		return aLKGrpAppntSchema;
	}

	public LKGrpAppntDB getDB()
	{
		LKGrpAppntDB aDBOper = new LKGrpAppntDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKGrpAppnt描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(PrtNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CustomerNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OrganComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnifiedSocialCreditNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Peoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OnWorkPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OffWorkPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(OtherPeoples));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Mobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessScope)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalProvince)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostalCounty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DetailAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AvgAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MaxAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MinAge));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnitLocation)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnitProvince)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnitCity)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnitCounty)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(UnitAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LinkMan)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LinkManIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LinkManIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LinkManPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LinkManMobile)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LinkManEmail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonIDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LegalPersonIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLKGrpAppnt>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			PostalAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			OrganComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			TaxNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			UnifiedSocialCreditNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Peoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).intValue();
			OnWorkPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).intValue();
			OffWorkPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).intValue();
			OtherPeoples= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).intValue();
			Mobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			BusinessScope = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			PostalProvince = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			PostalCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			PostalCounty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			DetailAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			AvgAge = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,20,SysConst.PACKAGESPILTER))).doubleValue();
			MaxAge= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,21,SysConst.PACKAGESPILTER))).intValue();
			MinAge= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,22,SysConst.PACKAGESPILTER))).intValue();
			UnitLocation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			UnitProvince = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			UnitCity = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			UnitCounty = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			UnitAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			LinkMan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			LinkManIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			LinkManIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			LinkManPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			LinkManMobile = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			LinkManEmail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			LegalPersonName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			LegalPersonIDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			LegalPersonIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LKGrpAppntSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("PrtNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
		}
		if (FCode.equals("CustomerNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("PostalAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalAddress));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("OrganComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OrganComCode));
		}
		if (FCode.equals("TaxNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxNo));
		}
		if (FCode.equals("UnifiedSocialCreditNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnifiedSocialCreditNo));
		}
		if (FCode.equals("Peoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Peoples));
		}
		if (FCode.equals("OnWorkPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OnWorkPeoples));
		}
		if (FCode.equals("OffWorkPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OffWorkPeoples));
		}
		if (FCode.equals("OtherPeoples"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OtherPeoples));
		}
		if (FCode.equals("Mobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Mobile));
		}
		if (FCode.equals("BusinessScope"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessScope));
		}
		if (FCode.equals("PostalProvince"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalProvince));
		}
		if (FCode.equals("PostalCity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCity));
		}
		if (FCode.equals("PostalCounty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostalCounty));
		}
		if (FCode.equals("DetailAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DetailAddress));
		}
		if (FCode.equals("AvgAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AvgAge));
		}
		if (FCode.equals("MaxAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaxAge));
		}
		if (FCode.equals("MinAge"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MinAge));
		}
		if (FCode.equals("UnitLocation"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnitLocation));
		}
		if (FCode.equals("UnitProvince"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnitProvince));
		}
		if (FCode.equals("UnitCity"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnitCity));
		}
		if (FCode.equals("UnitCounty"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnitCounty));
		}
		if (FCode.equals("UnitAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(UnitAddress));
		}
		if (FCode.equals("LinkMan"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LinkMan));
		}
		if (FCode.equals("LinkManIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LinkManIDType));
		}
		if (FCode.equals("LinkManIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LinkManIDNo));
		}
		if (FCode.equals("LinkManPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LinkManPhone));
		}
		if (FCode.equals("LinkManMobile"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LinkManMobile));
		}
		if (FCode.equals("LinkManEmail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LinkManEmail));
		}
		if (FCode.equals("LegalPersonName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonName));
		}
		if (FCode.equals("LegalPersonIDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonIDType));
		}
		if (FCode.equals("LegalPersonIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LegalPersonIDNo));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(PrtNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(CustomerNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(PostalAddress);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(OrganComCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(TaxNo);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(UnifiedSocialCreditNo);
				break;
			case 9:
				strFieldValue = String.valueOf(Peoples);
				break;
			case 10:
				strFieldValue = String.valueOf(OnWorkPeoples);
				break;
			case 11:
				strFieldValue = String.valueOf(OffWorkPeoples);
				break;
			case 12:
				strFieldValue = String.valueOf(OtherPeoples);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(Mobile);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(BusinessScope);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(PostalProvince);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(PostalCity);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(PostalCounty);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(DetailAddress);
				break;
			case 19:
				strFieldValue = String.valueOf(AvgAge);
				break;
			case 20:
				strFieldValue = String.valueOf(MaxAge);
				break;
			case 21:
				strFieldValue = String.valueOf(MinAge);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(UnitLocation);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(UnitProvince);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(UnitCity);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(UnitCounty);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(UnitAddress);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(LinkMan);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(LinkManIDType);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(LinkManIDNo);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(LinkManPhone);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(LinkManMobile);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(LinkManEmail);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonName);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonIDType);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(LegalPersonIDNo);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("PrtNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrtNo = FValue.trim();
			}
			else
				PrtNo = null;
		}
		if (FCode.equalsIgnoreCase("CustomerNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CustomerNo = FValue.trim();
			}
			else
				CustomerNo = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("PostalAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalAddress = FValue.trim();
			}
			else
				PostalAddress = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("OrganComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OrganComCode = FValue.trim();
			}
			else
				OrganComCode = null;
		}
		if (FCode.equalsIgnoreCase("TaxNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxNo = FValue.trim();
			}
			else
				TaxNo = null;
		}
		if (FCode.equalsIgnoreCase("UnifiedSocialCreditNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnifiedSocialCreditNo = FValue.trim();
			}
			else
				UnifiedSocialCreditNo = null;
		}
		if (FCode.equalsIgnoreCase("Peoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				Peoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("OnWorkPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OnWorkPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("OffWorkPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OffWorkPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("OtherPeoples"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				OtherPeoples = i;
			}
		}
		if (FCode.equalsIgnoreCase("Mobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Mobile = FValue.trim();
			}
			else
				Mobile = null;
		}
		if (FCode.equalsIgnoreCase("BusinessScope"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessScope = FValue.trim();
			}
			else
				BusinessScope = null;
		}
		if (FCode.equalsIgnoreCase("PostalProvince"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalProvince = FValue.trim();
			}
			else
				PostalProvince = null;
		}
		if (FCode.equalsIgnoreCase("PostalCity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCity = FValue.trim();
			}
			else
				PostalCity = null;
		}
		if (FCode.equalsIgnoreCase("PostalCounty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostalCounty = FValue.trim();
			}
			else
				PostalCounty = null;
		}
		if (FCode.equalsIgnoreCase("DetailAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DetailAddress = FValue.trim();
			}
			else
				DetailAddress = null;
		}
		if (FCode.equalsIgnoreCase("AvgAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AvgAge = d;
			}
		}
		if (FCode.equalsIgnoreCase("MaxAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MaxAge = i;
			}
		}
		if (FCode.equalsIgnoreCase("MinAge"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				MinAge = i;
			}
		}
		if (FCode.equalsIgnoreCase("UnitLocation"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnitLocation = FValue.trim();
			}
			else
				UnitLocation = null;
		}
		if (FCode.equalsIgnoreCase("UnitProvince"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnitProvince = FValue.trim();
			}
			else
				UnitProvince = null;
		}
		if (FCode.equalsIgnoreCase("UnitCity"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnitCity = FValue.trim();
			}
			else
				UnitCity = null;
		}
		if (FCode.equalsIgnoreCase("UnitCounty"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnitCounty = FValue.trim();
			}
			else
				UnitCounty = null;
		}
		if (FCode.equalsIgnoreCase("UnitAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				UnitAddress = FValue.trim();
			}
			else
				UnitAddress = null;
		}
		if (FCode.equalsIgnoreCase("LinkMan"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LinkMan = FValue.trim();
			}
			else
				LinkMan = null;
		}
		if (FCode.equalsIgnoreCase("LinkManIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LinkManIDType = FValue.trim();
			}
			else
				LinkManIDType = null;
		}
		if (FCode.equalsIgnoreCase("LinkManIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LinkManIDNo = FValue.trim();
			}
			else
				LinkManIDNo = null;
		}
		if (FCode.equalsIgnoreCase("LinkManPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LinkManPhone = FValue.trim();
			}
			else
				LinkManPhone = null;
		}
		if (FCode.equalsIgnoreCase("LinkManMobile"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LinkManMobile = FValue.trim();
			}
			else
				LinkManMobile = null;
		}
		if (FCode.equalsIgnoreCase("LinkManEmail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LinkManEmail = FValue.trim();
			}
			else
				LinkManEmail = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonName = FValue.trim();
			}
			else
				LegalPersonName = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonIDType = FValue.trim();
			}
			else
				LegalPersonIDType = null;
		}
		if (FCode.equalsIgnoreCase("LegalPersonIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LegalPersonIDNo = FValue.trim();
			}
			else
				LegalPersonIDNo = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LKGrpAppntSchema other = (LKGrpAppntSchema)otherObject;
		return
			(PrtNo == null ? other.getPrtNo() == null : PrtNo.equals(other.getPrtNo()))
			&& (CustomerNo == null ? other.getCustomerNo() == null : CustomerNo.equals(other.getCustomerNo()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (PostalAddress == null ? other.getPostalAddress() == null : PostalAddress.equals(other.getPostalAddress()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (OrganComCode == null ? other.getOrganComCode() == null : OrganComCode.equals(other.getOrganComCode()))
			&& (TaxNo == null ? other.getTaxNo() == null : TaxNo.equals(other.getTaxNo()))
			&& (UnifiedSocialCreditNo == null ? other.getUnifiedSocialCreditNo() == null : UnifiedSocialCreditNo.equals(other.getUnifiedSocialCreditNo()))
			&& Peoples == other.getPeoples()
			&& OnWorkPeoples == other.getOnWorkPeoples()
			&& OffWorkPeoples == other.getOffWorkPeoples()
			&& OtherPeoples == other.getOtherPeoples()
			&& (Mobile == null ? other.getMobile() == null : Mobile.equals(other.getMobile()))
			&& (BusinessScope == null ? other.getBusinessScope() == null : BusinessScope.equals(other.getBusinessScope()))
			&& (PostalProvince == null ? other.getPostalProvince() == null : PostalProvince.equals(other.getPostalProvince()))
			&& (PostalCity == null ? other.getPostalCity() == null : PostalCity.equals(other.getPostalCity()))
			&& (PostalCounty == null ? other.getPostalCounty() == null : PostalCounty.equals(other.getPostalCounty()))
			&& (DetailAddress == null ? other.getDetailAddress() == null : DetailAddress.equals(other.getDetailAddress()))
			&& AvgAge == other.getAvgAge()
			&& MaxAge == other.getMaxAge()
			&& MinAge == other.getMinAge()
			&& (UnitLocation == null ? other.getUnitLocation() == null : UnitLocation.equals(other.getUnitLocation()))
			&& (UnitProvince == null ? other.getUnitProvince() == null : UnitProvince.equals(other.getUnitProvince()))
			&& (UnitCity == null ? other.getUnitCity() == null : UnitCity.equals(other.getUnitCity()))
			&& (UnitCounty == null ? other.getUnitCounty() == null : UnitCounty.equals(other.getUnitCounty()))
			&& (UnitAddress == null ? other.getUnitAddress() == null : UnitAddress.equals(other.getUnitAddress()))
			&& (LinkMan == null ? other.getLinkMan() == null : LinkMan.equals(other.getLinkMan()))
			&& (LinkManIDType == null ? other.getLinkManIDType() == null : LinkManIDType.equals(other.getLinkManIDType()))
			&& (LinkManIDNo == null ? other.getLinkManIDNo() == null : LinkManIDNo.equals(other.getLinkManIDNo()))
			&& (LinkManPhone == null ? other.getLinkManPhone() == null : LinkManPhone.equals(other.getLinkManPhone()))
			&& (LinkManMobile == null ? other.getLinkManMobile() == null : LinkManMobile.equals(other.getLinkManMobile()))
			&& (LinkManEmail == null ? other.getLinkManEmail() == null : LinkManEmail.equals(other.getLinkManEmail()))
			&& (LegalPersonName == null ? other.getLegalPersonName() == null : LegalPersonName.equals(other.getLegalPersonName()))
			&& (LegalPersonIDType == null ? other.getLegalPersonIDType() == null : LegalPersonIDType.equals(other.getLegalPersonIDType()))
			&& (LegalPersonIDNo == null ? other.getLegalPersonIDNo() == null : LegalPersonIDNo.equals(other.getLegalPersonIDNo()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("PrtNo") ) {
			return 0;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return 1;
		}
		if( strFieldName.equals("Name") ) {
			return 2;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return 3;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 4;
		}
		if( strFieldName.equals("Phone") ) {
			return 5;
		}
		if( strFieldName.equals("OrganComCode") ) {
			return 6;
		}
		if( strFieldName.equals("TaxNo") ) {
			return 7;
		}
		if( strFieldName.equals("UnifiedSocialCreditNo") ) {
			return 8;
		}
		if( strFieldName.equals("Peoples") ) {
			return 9;
		}
		if( strFieldName.equals("OnWorkPeoples") ) {
			return 10;
		}
		if( strFieldName.equals("OffWorkPeoples") ) {
			return 11;
		}
		if( strFieldName.equals("OtherPeoples") ) {
			return 12;
		}
		if( strFieldName.equals("Mobile") ) {
			return 13;
		}
		if( strFieldName.equals("BusinessScope") ) {
			return 14;
		}
		if( strFieldName.equals("PostalProvince") ) {
			return 15;
		}
		if( strFieldName.equals("PostalCity") ) {
			return 16;
		}
		if( strFieldName.equals("PostalCounty") ) {
			return 17;
		}
		if( strFieldName.equals("DetailAddress") ) {
			return 18;
		}
		if( strFieldName.equals("AvgAge") ) {
			return 19;
		}
		if( strFieldName.equals("MaxAge") ) {
			return 20;
		}
		if( strFieldName.equals("MinAge") ) {
			return 21;
		}
		if( strFieldName.equals("UnitLocation") ) {
			return 22;
		}
		if( strFieldName.equals("UnitProvince") ) {
			return 23;
		}
		if( strFieldName.equals("UnitCity") ) {
			return 24;
		}
		if( strFieldName.equals("UnitCounty") ) {
			return 25;
		}
		if( strFieldName.equals("UnitAddress") ) {
			return 26;
		}
		if( strFieldName.equals("LinkMan") ) {
			return 27;
		}
		if( strFieldName.equals("LinkManIDType") ) {
			return 28;
		}
		if( strFieldName.equals("LinkManIDNo") ) {
			return 29;
		}
		if( strFieldName.equals("LinkManPhone") ) {
			return 30;
		}
		if( strFieldName.equals("LinkManMobile") ) {
			return 31;
		}
		if( strFieldName.equals("LinkManEmail") ) {
			return 32;
		}
		if( strFieldName.equals("LegalPersonName") ) {
			return 33;
		}
		if( strFieldName.equals("LegalPersonIDType") ) {
			return 34;
		}
		if( strFieldName.equals("LegalPersonIDNo") ) {
			return 35;
		}
		if( strFieldName.equals("Operator") ) {
			return 36;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 37;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 38;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 39;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 40;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "PrtNo";
				break;
			case 1:
				strFieldName = "CustomerNo";
				break;
			case 2:
				strFieldName = "Name";
				break;
			case 3:
				strFieldName = "PostalAddress";
				break;
			case 4:
				strFieldName = "ZipCode";
				break;
			case 5:
				strFieldName = "Phone";
				break;
			case 6:
				strFieldName = "OrganComCode";
				break;
			case 7:
				strFieldName = "TaxNo";
				break;
			case 8:
				strFieldName = "UnifiedSocialCreditNo";
				break;
			case 9:
				strFieldName = "Peoples";
				break;
			case 10:
				strFieldName = "OnWorkPeoples";
				break;
			case 11:
				strFieldName = "OffWorkPeoples";
				break;
			case 12:
				strFieldName = "OtherPeoples";
				break;
			case 13:
				strFieldName = "Mobile";
				break;
			case 14:
				strFieldName = "BusinessScope";
				break;
			case 15:
				strFieldName = "PostalProvince";
				break;
			case 16:
				strFieldName = "PostalCity";
				break;
			case 17:
				strFieldName = "PostalCounty";
				break;
			case 18:
				strFieldName = "DetailAddress";
				break;
			case 19:
				strFieldName = "AvgAge";
				break;
			case 20:
				strFieldName = "MaxAge";
				break;
			case 21:
				strFieldName = "MinAge";
				break;
			case 22:
				strFieldName = "UnitLocation";
				break;
			case 23:
				strFieldName = "UnitProvince";
				break;
			case 24:
				strFieldName = "UnitCity";
				break;
			case 25:
				strFieldName = "UnitCounty";
				break;
			case 26:
				strFieldName = "UnitAddress";
				break;
			case 27:
				strFieldName = "LinkMan";
				break;
			case 28:
				strFieldName = "LinkManIDType";
				break;
			case 29:
				strFieldName = "LinkManIDNo";
				break;
			case 30:
				strFieldName = "LinkManPhone";
				break;
			case 31:
				strFieldName = "LinkManMobile";
				break;
			case 32:
				strFieldName = "LinkManEmail";
				break;
			case 33:
				strFieldName = "LegalPersonName";
				break;
			case 34:
				strFieldName = "LegalPersonIDType";
				break;
			case 35:
				strFieldName = "LegalPersonIDNo";
				break;
			case 36:
				strFieldName = "Operator";
				break;
			case 37:
				strFieldName = "MakeDate";
				break;
			case 38:
				strFieldName = "MakeTime";
				break;
			case 39:
				strFieldName = "ModifyDate";
				break;
			case 40:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("PrtNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CustomerNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OrganComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnifiedSocialCreditNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Peoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OnWorkPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OffWorkPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("OtherPeoples") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("Mobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessScope") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalProvince") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostalCounty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DetailAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AvgAge") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("MaxAge") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("MinAge") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("UnitLocation") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnitProvince") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnitCity") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnitCounty") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("UnitAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LinkMan") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LinkManIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LinkManIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LinkManPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LinkManMobile") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LinkManEmail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonIDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LegalPersonIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_INT;
				break;
			case 10:
				nFieldType = Schema.TYPE_INT;
				break;
			case 11:
				nFieldType = Schema.TYPE_INT;
				break;
			case 12:
				nFieldType = Schema.TYPE_INT;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 20:
				nFieldType = Schema.TYPE_INT;
				break;
			case 21:
				nFieldType = Schema.TYPE_INT;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
