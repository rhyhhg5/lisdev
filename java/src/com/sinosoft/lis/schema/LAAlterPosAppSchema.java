/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LAAlterPosAppDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

/*
 * <p>ClassName: LAAlterPosAppSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-03-16
 */
public class LAAlterPosAppSchema implements Schema
{
    // @Field
    /** 代理人编码 */
    private String AgentCode;
    /** 申请年月 */
    private String IndexCalNo;
    /** 管理机构 */
    private String ManageCom;
    /** 原代理人系列 */
    private String PreAgentLine;
    /** 原代理人职级 */
    private String PreAgentGrade;
    /** 申请系列 */
    private String AppAgentLine;
    /** 申请职级 */
    private String AppAgentGrade;
    /** 是否辞去原系列标记 */
    private String IfAbadict;
    /** 状态 */
    private String state;

    public static final int FIELDNUM = 9; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LAAlterPosAppSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "AgentCode";
        pk[1] = "IndexCalNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getAgentCode()
    {
        if (AgentCode != null && !AgentCode.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AgentCode = StrTool.unicodeToGBK(AgentCode);
        }
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode)
    {
        AgentCode = aAgentCode;
    }

    public String getIndexCalNo()
    {
        if (IndexCalNo != null && !IndexCalNo.equals("") &&
            SysConst.CHANGECHARSET)
        {
            IndexCalNo = StrTool.unicodeToGBK(IndexCalNo);
        }
        return IndexCalNo;
    }

    public void setIndexCalNo(String aIndexCalNo)
    {
        IndexCalNo = aIndexCalNo;
    }

    public String getManageCom()
    {
        if (ManageCom != null && !ManageCom.equals("") &&
            SysConst.CHANGECHARSET)
        {
            ManageCom = StrTool.unicodeToGBK(ManageCom);
        }
        return ManageCom;
    }

    public void setManageCom(String aManageCom)
    {
        ManageCom = aManageCom;
    }

    public String getPreAgentLine()
    {
        if (PreAgentLine != null && !PreAgentLine.equals("") &&
            SysConst.CHANGECHARSET)
        {
            PreAgentLine = StrTool.unicodeToGBK(PreAgentLine);
        }
        return PreAgentLine;
    }

    public void setPreAgentLine(String aPreAgentLine)
    {
        PreAgentLine = aPreAgentLine;
    }

    public String getPreAgentGrade()
    {
        if (PreAgentGrade != null && !PreAgentGrade.equals("") &&
            SysConst.CHANGECHARSET)
        {
            PreAgentGrade = StrTool.unicodeToGBK(PreAgentGrade);
        }
        return PreAgentGrade;
    }

    public void setPreAgentGrade(String aPreAgentGrade)
    {
        PreAgentGrade = aPreAgentGrade;
    }

    public String getAppAgentLine()
    {
        if (AppAgentLine != null && !AppAgentLine.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AppAgentLine = StrTool.unicodeToGBK(AppAgentLine);
        }
        return AppAgentLine;
    }

    public void setAppAgentLine(String aAppAgentLine)
    {
        AppAgentLine = aAppAgentLine;
    }

    public String getAppAgentGrade()
    {
        if (AppAgentGrade != null && !AppAgentGrade.equals("") &&
            SysConst.CHANGECHARSET)
        {
            AppAgentGrade = StrTool.unicodeToGBK(AppAgentGrade);
        }
        return AppAgentGrade;
    }

    public void setAppAgentGrade(String aAppAgentGrade)
    {
        AppAgentGrade = aAppAgentGrade;
    }

    public String getIfAbadict()
    {
        if (IfAbadict != null && !IfAbadict.equals("") &&
            SysConst.CHANGECHARSET)
        {
            IfAbadict = StrTool.unicodeToGBK(IfAbadict);
        }
        return IfAbadict;
    }

    public void setIfAbadict(String aIfAbadict)
    {
        IfAbadict = aIfAbadict;
    }

    public String getstate()
    {
        if (state != null && !state.equals("") && SysConst.CHANGECHARSET)
        {
            state = StrTool.unicodeToGBK(state);
        }
        return state;
    }

    public void setstate(String astate)
    {
        state = astate;
    }

    /**
     * 使用另外一个 LAAlterPosAppSchema 对象给 Schema 赋值
     * @param: aLAAlterPosAppSchema LAAlterPosAppSchema
     **/
    public void setSchema(LAAlterPosAppSchema aLAAlterPosAppSchema)
    {
        this.AgentCode = aLAAlterPosAppSchema.getAgentCode();
        this.IndexCalNo = aLAAlterPosAppSchema.getIndexCalNo();
        this.ManageCom = aLAAlterPosAppSchema.getManageCom();
        this.PreAgentLine = aLAAlterPosAppSchema.getPreAgentLine();
        this.PreAgentGrade = aLAAlterPosAppSchema.getPreAgentGrade();
        this.AppAgentLine = aLAAlterPosAppSchema.getAppAgentLine();
        this.AppAgentGrade = aLAAlterPosAppSchema.getAppAgentGrade();
        this.IfAbadict = aLAAlterPosAppSchema.getIfAbadict();
        this.state = aLAAlterPosAppSchema.getstate();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("AgentCode") == null)
            {
                this.AgentCode = null;
            }
            else
            {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("IndexCalNo") == null)
            {
                this.IndexCalNo = null;
            }
            else
            {
                this.IndexCalNo = rs.getString("IndexCalNo").trim();
            }

            if (rs.getString("ManageCom") == null)
            {
                this.ManageCom = null;
            }
            else
            {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("PreAgentLine") == null)
            {
                this.PreAgentLine = null;
            }
            else
            {
                this.PreAgentLine = rs.getString("PreAgentLine").trim();
            }

            if (rs.getString("PreAgentGrade") == null)
            {
                this.PreAgentGrade = null;
            }
            else
            {
                this.PreAgentGrade = rs.getString("PreAgentGrade").trim();
            }

            if (rs.getString("AppAgentLine") == null)
            {
                this.AppAgentLine = null;
            }
            else
            {
                this.AppAgentLine = rs.getString("AppAgentLine").trim();
            }

            if (rs.getString("AppAgentGrade") == null)
            {
                this.AppAgentGrade = null;
            }
            else
            {
                this.AppAgentGrade = rs.getString("AppAgentGrade").trim();
            }

            if (rs.getString("IfAbadict") == null)
            {
                this.IfAbadict = null;
            }
            else
            {
                this.IfAbadict = rs.getString("IfAbadict").trim();
            }

            if (rs.getString("state") == null)
            {
                this.state = null;
            }
            else
            {
                this.state = rs.getString("state").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAlterPosAppSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LAAlterPosAppSchema getSchema()
    {
        LAAlterPosAppSchema aLAAlterPosAppSchema = new LAAlterPosAppSchema();
        aLAAlterPosAppSchema.setSchema(this);
        return aLAAlterPosAppSchema;
    }

    public LAAlterPosAppDB getDB()
    {
        LAAlterPosAppDB aDBOper = new LAAlterPosAppDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAlterPosApp描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(AgentCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IndexCalNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ManageCom)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PreAgentLine)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PreAgentGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppAgentLine)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppAgentGrade)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IfAbadict)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(state));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAlterPosApp>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                       SysConst.PACKAGESPILTER);
            IndexCalNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            PreAgentLine = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            PreAgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            AppAgentLine = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            AppAgentGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                           SysConst.PACKAGESPILTER);
            IfAbadict = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                       SysConst.PACKAGESPILTER);
            state = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                   SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAlterPosAppSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("AgentCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("IndexCalNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IndexCalNo));
        }
        if (FCode.equals("ManageCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("PreAgentLine"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PreAgentLine));
        }
        if (FCode.equals("PreAgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PreAgentGrade));
        }
        if (FCode.equals("AppAgentLine"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppAgentLine));
        }
        if (FCode.equals("AppAgentGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppAgentGrade));
        }
        if (FCode.equals("IfAbadict"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(IfAbadict));
        }
        if (FCode.equals("state"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(state));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(AgentCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(IndexCalNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ManageCom);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PreAgentLine);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(PreAgentGrade);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(AppAgentLine);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(AppAgentGrade);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(IfAbadict);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(state);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("AgentCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AgentCode = FValue.trim();
            }
            else
            {
                AgentCode = null;
            }
        }
        if (FCode.equals("IndexCalNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IndexCalNo = FValue.trim();
            }
            else
            {
                IndexCalNo = null;
            }
        }
        if (FCode.equals("ManageCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ManageCom = FValue.trim();
            }
            else
            {
                ManageCom = null;
            }
        }
        if (FCode.equals("PreAgentLine"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PreAgentLine = FValue.trim();
            }
            else
            {
                PreAgentLine = null;
            }
        }
        if (FCode.equals("PreAgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PreAgentGrade = FValue.trim();
            }
            else
            {
                PreAgentGrade = null;
            }
        }
        if (FCode.equals("AppAgentLine"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppAgentLine = FValue.trim();
            }
            else
            {
                AppAgentLine = null;
            }
        }
        if (FCode.equals("AppAgentGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppAgentGrade = FValue.trim();
            }
            else
            {
                AppAgentGrade = null;
            }
        }
        if (FCode.equals("IfAbadict"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IfAbadict = FValue.trim();
            }
            else
            {
                IfAbadict = null;
            }
        }
        if (FCode.equals("state"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                state = FValue.trim();
            }
            else
            {
                state = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LAAlterPosAppSchema other = (LAAlterPosAppSchema) otherObject;
        return
                AgentCode.equals(other.getAgentCode())
                && IndexCalNo.equals(other.getIndexCalNo())
                && ManageCom.equals(other.getManageCom())
                && PreAgentLine.equals(other.getPreAgentLine())
                && PreAgentGrade.equals(other.getPreAgentGrade())
                && AppAgentLine.equals(other.getAppAgentLine())
                && AppAgentGrade.equals(other.getAppAgentGrade())
                && IfAbadict.equals(other.getIfAbadict())
                && state.equals(other.getstate());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return 0;
        }
        if (strFieldName.equals("IndexCalNo"))
        {
            return 1;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return 2;
        }
        if (strFieldName.equals("PreAgentLine"))
        {
            return 3;
        }
        if (strFieldName.equals("PreAgentGrade"))
        {
            return 4;
        }
        if (strFieldName.equals("AppAgentLine"))
        {
            return 5;
        }
        if (strFieldName.equals("AppAgentGrade"))
        {
            return 6;
        }
        if (strFieldName.equals("IfAbadict"))
        {
            return 7;
        }
        if (strFieldName.equals("state"))
        {
            return 8;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "AgentCode";
                break;
            case 1:
                strFieldName = "IndexCalNo";
                break;
            case 2:
                strFieldName = "ManageCom";
                break;
            case 3:
                strFieldName = "PreAgentLine";
                break;
            case 4:
                strFieldName = "PreAgentGrade";
                break;
            case 5:
                strFieldName = "AppAgentLine";
                break;
            case 6:
                strFieldName = "AppAgentGrade";
                break;
            case 7:
                strFieldName = "IfAbadict";
                break;
            case 8:
                strFieldName = "state";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("AgentCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IndexCalNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PreAgentLine"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PreAgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppAgentLine"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppAgentGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IfAbadict"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("state"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
