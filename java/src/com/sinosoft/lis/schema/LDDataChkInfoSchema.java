/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDDataChkInfoDB;

/*
 * <p>ClassName: LDDataChkInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-07-04
 */
public class LDDataChkInfoSchema implements Schema, Cloneable {
    // @Field
    /** 顺序号 */
    private String SeriNo;
    /** 检查类型 */
    private String Type;
    /** 校验函数 */
    private String FunName;
    /** Sql编号 */
    private int SQLNo;
    /** 逻辑信息描述 */
    private String InfoMation;
    /** 备注 */
    private String Remark;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;

    public static final int FIELDNUM = 9; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDDataChkInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SeriNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDDataChkInfoSchema cloned = (LDDataChkInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSeriNo() {
        return SeriNo;
    }

    public void setSeriNo(String aSeriNo) {
        SeriNo = aSeriNo;
    }

    public String getType() {
        return Type;
    }

    public void setType(String aType) {
        Type = aType;
    }

    public String getFunName() {
        return FunName;
    }

    public void setFunName(String aFunName) {
        FunName = aFunName;
    }

    public int getSQLNo() {
        return SQLNo;
    }

    public void setSQLNo(int aSQLNo) {
        SQLNo = aSQLNo;
    }

    public void setSQLNo(String aSQLNo) {
        if (aSQLNo != null && !aSQLNo.equals("")) {
            Integer tInteger = new Integer(aSQLNo);
            int i = tInteger.intValue();
            SQLNo = i;
        }
    }

    public String getInfoMation() {
        return InfoMation;
    }

    public void setInfoMation(String aInfoMation) {
        InfoMation = aInfoMation;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String aRemark) {
        Remark = aRemark;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 LDDataChkInfoSchema 对象给 Schema 赋值
     * @param: aLDDataChkInfoSchema LDDataChkInfoSchema
     **/
    public void setSchema(LDDataChkInfoSchema aLDDataChkInfoSchema) {
        this.SeriNo = aLDDataChkInfoSchema.getSeriNo();
        this.Type = aLDDataChkInfoSchema.getType();
        this.FunName = aLDDataChkInfoSchema.getFunName();
        this.SQLNo = aLDDataChkInfoSchema.getSQLNo();
        this.InfoMation = aLDDataChkInfoSchema.getInfoMation();
        this.Remark = aLDDataChkInfoSchema.getRemark();
        this.Operator = aLDDataChkInfoSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDDataChkInfoSchema.getMakeDate());
        this.MakeTime = aLDDataChkInfoSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SeriNo") == null) {
                this.SeriNo = null;
            } else {
                this.SeriNo = rs.getString("SeriNo").trim();
            }

            if (rs.getString("Type") == null) {
                this.Type = null;
            } else {
                this.Type = rs.getString("Type").trim();
            }

            if (rs.getString("FunName") == null) {
                this.FunName = null;
            } else {
                this.FunName = rs.getString("FunName").trim();
            }

            this.SQLNo = rs.getInt("SQLNo");
            if (rs.getString("InfoMation") == null) {
                this.InfoMation = null;
            } else {
                this.InfoMation = rs.getString("InfoMation").trim();
            }

            if (rs.getString("Remark") == null) {
                this.Remark = null;
            } else {
                this.Remark = rs.getString("Remark").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDDataChkInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDataChkInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDDataChkInfoSchema getSchema() {
        LDDataChkInfoSchema aLDDataChkInfoSchema = new LDDataChkInfoSchema();
        aLDDataChkInfoSchema.setSchema(this);
        return aLDDataChkInfoSchema;
    }

    public LDDataChkInfoDB getDB() {
        LDDataChkInfoDB aDBOper = new LDDataChkInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDataChkInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SeriNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Type));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(FunName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(SQLNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InfoMation));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Remark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDDataChkInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SeriNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            Type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                  SysConst.PACKAGESPILTER);
            FunName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            SQLNo = new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    4, SysConst.PACKAGESPILTER))).intValue();
            InfoMation = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDDataChkInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SeriNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SeriNo));
        }
        if (FCode.equals("Type")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Type));
        }
        if (FCode.equals("FunName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(FunName));
        }
        if (FCode.equals("SQLNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SQLNo));
        }
        if (FCode.equals("InfoMation")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InfoMation));
        }
        if (FCode.equals("Remark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SeriNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(Type);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(FunName);
            break;
        case 3:
            strFieldValue = String.valueOf(SQLNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(InfoMation);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Remark);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SeriNo")) {
            if (FValue != null && !FValue.equals("")) {
                SeriNo = FValue.trim();
            } else {
                SeriNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("Type")) {
            if (FValue != null && !FValue.equals("")) {
                Type = FValue.trim();
            } else {
                Type = null;
            }
        }
        if (FCode.equalsIgnoreCase("FunName")) {
            if (FValue != null && !FValue.equals("")) {
                FunName = FValue.trim();
            } else {
                FunName = null;
            }
        }
        if (FCode.equalsIgnoreCase("SQLNo")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                SQLNo = i;
            }
        }
        if (FCode.equalsIgnoreCase("InfoMation")) {
            if (FValue != null && !FValue.equals("")) {
                InfoMation = FValue.trim();
            } else {
                InfoMation = null;
            }
        }
        if (FCode.equalsIgnoreCase("Remark")) {
            if (FValue != null && !FValue.equals("")) {
                Remark = FValue.trim();
            } else {
                Remark = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDDataChkInfoSchema other = (LDDataChkInfoSchema) otherObject;
        return
                SeriNo.equals(other.getSeriNo())
                && Type.equals(other.getType())
                && FunName.equals(other.getFunName())
                && SQLNo == other.getSQLNo()
                && InfoMation.equals(other.getInfoMation())
                && Remark.equals(other.getRemark())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SeriNo")) {
            return 0;
        }
        if (strFieldName.equals("Type")) {
            return 1;
        }
        if (strFieldName.equals("FunName")) {
            return 2;
        }
        if (strFieldName.equals("SQLNo")) {
            return 3;
        }
        if (strFieldName.equals("InfoMation")) {
            return 4;
        }
        if (strFieldName.equals("Remark")) {
            return 5;
        }
        if (strFieldName.equals("Operator")) {
            return 6;
        }
        if (strFieldName.equals("MakeDate")) {
            return 7;
        }
        if (strFieldName.equals("MakeTime")) {
            return 8;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SeriNo";
            break;
        case 1:
            strFieldName = "Type";
            break;
        case 2:
            strFieldName = "FunName";
            break;
        case 3:
            strFieldName = "SQLNo";
            break;
        case 4:
            strFieldName = "InfoMation";
            break;
        case 5:
            strFieldName = "Remark";
            break;
        case 6:
            strFieldName = "Operator";
            break;
        case 7:
            strFieldName = "MakeDate";
            break;
        case 8:
            strFieldName = "MakeTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SeriNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Type")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FunName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SQLNo")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("InfoMation")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Remark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_INT;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
