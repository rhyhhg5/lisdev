/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LLCaseInfoDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LLCaseInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_2
 * @CreateDate：2005-04-15
 */
public class LLCaseInfoSchema implements Schema, Cloneable
{
    // @Field
    /** 立案号(申请登记号) */
    private String RgtNo;
    /** 序号 */
    private String SerialNo;
    /** 分案号(个人理赔号) */
    private String CaseNo;
    /** 受理事故号 */
    private String CaseRelaNo;
    /** 附件序号 */
    private String AffixNo;
    /** 出险人客户号 */
    private String CustomerNo;
    /** 出险人名称 */
    private String CustomerName;
    /** 类型 */
    private String Type;
    /** 代码 */
    private String Code;
    /** 名称 */
    private String Name;
    /** 诊断 */
    private String Diagnosis;
    /** 赔付标志 */
    private String ClmFlag;
    /** 残疾类型 */
    private String DeformityKind;
    /** 伤残级别 */
    private String DeformityGrade;
    /** 伤残级别名称 */
    private String GradeName;
    /** 残疾部位编码 */
    private String PartCode;
    /** 残疾原因 */
    private String DeformityReason;
    /** 残疾给付比例 */
    private double DeformityRate;
    /** 申请给付比例 */
    private double AppDeformityRate;
    /** 实际给付比例 */
    private double RealRate;
    /** 病例号码 */
    private String No;
    /** 确诊日期 */
    private Date DianoseDate;
    /** 确诊依据 */
    private String DiagnoseDesc;
    /** 诊断医院代码 */
    private String HospitalCode;
    /** 诊断医院名称 */
    private String HospitalName;
    /** 诊断医生号码 */
    private String DoctorNo;
    /** 诊断医生名称 */
    private String DoctorName;
    /** 鉴定单位 */
    private String JudgeOrgan;
    /** 鉴定单位名称 */
    private String JudgeOrganName;
    /** 鉴定时间 */
    private Date JudgeDate;
    /** 鉴定依据 */
    private String JudgeDepend;
    /** 管理机构 */
    private String MngCom;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 37; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LLCaseInfoSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SerialNo";
        pk[1] = "CaseNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LLCaseInfoSchema cloned = (LLCaseInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRgtNo()
    {
        if (SysConst.CHANGECHARSET && RgtNo != null && !RgtNo.equals(""))
        {
            RgtNo = StrTool.unicodeToGBK(RgtNo);
        }
        return RgtNo;
    }

    public void setRgtNo(String aRgtNo)
    {
        RgtNo = aRgtNo;
    }

    public String getSerialNo()
    {
        if (SysConst.CHANGECHARSET && SerialNo != null && !SerialNo.equals(""))
        {
            SerialNo = StrTool.unicodeToGBK(SerialNo);
        }
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo)
    {
        SerialNo = aSerialNo;
    }

    public String getCaseNo()
    {
        if (SysConst.CHANGECHARSET && CaseNo != null && !CaseNo.equals(""))
        {
            CaseNo = StrTool.unicodeToGBK(CaseNo);
        }
        return CaseNo;
    }

    public void setCaseNo(String aCaseNo)
    {
        CaseNo = aCaseNo;
    }

    public String getCaseRelaNo()
    {
        if (SysConst.CHANGECHARSET && CaseRelaNo != null &&
            !CaseRelaNo.equals(""))
        {
            CaseRelaNo = StrTool.unicodeToGBK(CaseRelaNo);
        }
        return CaseRelaNo;
    }

    public void setCaseRelaNo(String aCaseRelaNo)
    {
        CaseRelaNo = aCaseRelaNo;
    }

    public String getAffixNo()
    {
        if (SysConst.CHANGECHARSET && AffixNo != null && !AffixNo.equals(""))
        {
            AffixNo = StrTool.unicodeToGBK(AffixNo);
        }
        return AffixNo;
    }

    public void setAffixNo(String aAffixNo)
    {
        AffixNo = aAffixNo;
    }

    public String getCustomerNo()
    {
        if (SysConst.CHANGECHARSET && CustomerNo != null &&
            !CustomerNo.equals(""))
        {
            CustomerNo = StrTool.unicodeToGBK(CustomerNo);
        }
        return CustomerNo;
    }

    public void setCustomerNo(String aCustomerNo)
    {
        CustomerNo = aCustomerNo;
    }

    public String getCustomerName()
    {
        if (SysConst.CHANGECHARSET && CustomerName != null &&
            !CustomerName.equals(""))
        {
            CustomerName = StrTool.unicodeToGBK(CustomerName);
        }
        return CustomerName;
    }

    public void setCustomerName(String aCustomerName)
    {
        CustomerName = aCustomerName;
    }

    public String getType()
    {
        if (SysConst.CHANGECHARSET && Type != null && !Type.equals(""))
        {
            Type = StrTool.unicodeToGBK(Type);
        }
        return Type;
    }

    public void setType(String aType)
    {
        Type = aType;
    }

    public String getCode()
    {
        if (SysConst.CHANGECHARSET && Code != null && !Code.equals(""))
        {
            Code = StrTool.unicodeToGBK(Code);
        }
        return Code;
    }

    public void setCode(String aCode)
    {
        Code = aCode;
    }

    public String getName()
    {
        if (SysConst.CHANGECHARSET && Name != null && !Name.equals(""))
        {
            Name = StrTool.unicodeToGBK(Name);
        }
        return Name;
    }

    public void setName(String aName)
    {
        Name = aName;
    }

    public String getDiagnosis()
    {
        if (SysConst.CHANGECHARSET && Diagnosis != null && !Diagnosis.equals(""))
        {
            Diagnosis = StrTool.unicodeToGBK(Diagnosis);
        }
        return Diagnosis;
    }

    public void setDiagnosis(String aDiagnosis)
    {
        Diagnosis = aDiagnosis;
    }

    public String getClmFlag()
    {
        if (SysConst.CHANGECHARSET && ClmFlag != null && !ClmFlag.equals(""))
        {
            ClmFlag = StrTool.unicodeToGBK(ClmFlag);
        }
        return ClmFlag;
    }

    public void setClmFlag(String aClmFlag)
    {
        ClmFlag = aClmFlag;
    }

    public String getDeformityKind()
    {
        if (SysConst.CHANGECHARSET && DeformityKind != null &&
            !DeformityKind.equals(""))
        {
            DeformityKind = StrTool.unicodeToGBK(DeformityKind);
        }
        return DeformityKind;
    }

    public void setDeformityKind(String aDeformityKind)
    {
        DeformityKind = aDeformityKind;
    }

    public String getDeformityGrade()
    {
        if (SysConst.CHANGECHARSET && DeformityGrade != null &&
            !DeformityGrade.equals(""))
        {
            DeformityGrade = StrTool.unicodeToGBK(DeformityGrade);
        }
        return DeformityGrade;
    }

    public void setDeformityGrade(String aDeformityGrade)
    {
        DeformityGrade = aDeformityGrade;
    }

    public String getGradeName()
    {
        if (SysConst.CHANGECHARSET && GradeName != null && !GradeName.equals(""))
        {
            GradeName = StrTool.unicodeToGBK(GradeName);
        }
        return GradeName;
    }

    public void setGradeName(String aGradeName)
    {
        GradeName = aGradeName;
    }

    public String getPartCode()
    {
        if (SysConst.CHANGECHARSET && PartCode != null && !PartCode.equals(""))
        {
            PartCode = StrTool.unicodeToGBK(PartCode);
        }
        return PartCode;
    }

    public void setPartCode(String aPartCode)
    {
        PartCode = aPartCode;
    }

    public String getDeformityReason()
    {
        if (SysConst.CHANGECHARSET && DeformityReason != null &&
            !DeformityReason.equals(""))
        {
            DeformityReason = StrTool.unicodeToGBK(DeformityReason);
        }
        return DeformityReason;
    }

    public void setDeformityReason(String aDeformityReason)
    {
        DeformityReason = aDeformityReason;
    }

    public double getDeformityRate()
    {
        return DeformityRate;
    }

    public void setDeformityRate(double aDeformityRate)
    {
        DeformityRate = aDeformityRate;
    }

    public void setDeformityRate(String aDeformityRate)
    {
        if (aDeformityRate != null && !aDeformityRate.equals(""))
        {
            Double tDouble = new Double(aDeformityRate);
            double d = tDouble.doubleValue();
            DeformityRate = d;
        }
    }

    public double getAppDeformityRate()
    {
        return AppDeformityRate;
    }

    public void setAppDeformityRate(double aAppDeformityRate)
    {
        AppDeformityRate = aAppDeformityRate;
    }

    public void setAppDeformityRate(String aAppDeformityRate)
    {
        if (aAppDeformityRate != null && !aAppDeformityRate.equals(""))
        {
            Double tDouble = new Double(aAppDeformityRate);
            double d = tDouble.doubleValue();
            AppDeformityRate = d;
        }
    }

    public double getRealRate()
    {
        return RealRate;
    }

    public void setRealRate(double aRealRate)
    {
        RealRate = aRealRate;
    }

    public void setRealRate(String aRealRate)
    {
        if (aRealRate != null && !aRealRate.equals(""))
        {
            Double tDouble = new Double(aRealRate);
            double d = tDouble.doubleValue();
            RealRate = d;
        }
    }

    public String getNo()
    {
        if (SysConst.CHANGECHARSET && No != null && !No.equals(""))
        {
            No = StrTool.unicodeToGBK(No);
        }
        return No;
    }

    public void setNo(String aNo)
    {
        No = aNo;
    }

    public String getDianoseDate()
    {
        if (DianoseDate != null)
        {
            return fDate.getString(DianoseDate);
        }
        else
        {
            return null;
        }
    }

    public void setDianoseDate(Date aDianoseDate)
    {
        DianoseDate = aDianoseDate;
    }

    public void setDianoseDate(String aDianoseDate)
    {
        if (aDianoseDate != null && !aDianoseDate.equals(""))
        {
            DianoseDate = fDate.getDate(aDianoseDate);
        }
        else
        {
            DianoseDate = null;
        }
    }

    public String getDiagnoseDesc()
    {
        if (SysConst.CHANGECHARSET && DiagnoseDesc != null &&
            !DiagnoseDesc.equals(""))
        {
            DiagnoseDesc = StrTool.unicodeToGBK(DiagnoseDesc);
        }
        return DiagnoseDesc;
    }

    public void setDiagnoseDesc(String aDiagnoseDesc)
    {
        DiagnoseDesc = aDiagnoseDesc;
    }

    public String getHospitalCode()
    {
        if (SysConst.CHANGECHARSET && HospitalCode != null &&
            !HospitalCode.equals(""))
        {
            HospitalCode = StrTool.unicodeToGBK(HospitalCode);
        }
        return HospitalCode;
    }

    public void setHospitalCode(String aHospitalCode)
    {
        HospitalCode = aHospitalCode;
    }

    public String getHospitalName()
    {
        if (SysConst.CHANGECHARSET && HospitalName != null &&
            !HospitalName.equals(""))
        {
            HospitalName = StrTool.unicodeToGBK(HospitalName);
        }
        return HospitalName;
    }

    public void setHospitalName(String aHospitalName)
    {
        HospitalName = aHospitalName;
    }

    public String getDoctorNo()
    {
        if (SysConst.CHANGECHARSET && DoctorNo != null && !DoctorNo.equals(""))
        {
            DoctorNo = StrTool.unicodeToGBK(DoctorNo);
        }
        return DoctorNo;
    }

    public void setDoctorNo(String aDoctorNo)
    {
        DoctorNo = aDoctorNo;
    }

    public String getDoctorName()
    {
        if (SysConst.CHANGECHARSET && DoctorName != null &&
            !DoctorName.equals(""))
        {
            DoctorName = StrTool.unicodeToGBK(DoctorName);
        }
        return DoctorName;
    }

    public void setDoctorName(String aDoctorName)
    {
        DoctorName = aDoctorName;
    }

    public String getJudgeOrgan()
    {
        if (SysConst.CHANGECHARSET && JudgeOrgan != null &&
            !JudgeOrgan.equals(""))
        {
            JudgeOrgan = StrTool.unicodeToGBK(JudgeOrgan);
        }
        return JudgeOrgan;
    }

    public void setJudgeOrgan(String aJudgeOrgan)
    {
        JudgeOrgan = aJudgeOrgan;
    }

    public String getJudgeOrganName()
    {
        if (SysConst.CHANGECHARSET && JudgeOrganName != null &&
            !JudgeOrganName.equals(""))
        {
            JudgeOrganName = StrTool.unicodeToGBK(JudgeOrganName);
        }
        return JudgeOrganName;
    }

    public void setJudgeOrganName(String aJudgeOrganName)
    {
        JudgeOrganName = aJudgeOrganName;
    }

    public String getJudgeDate()
    {
        if (JudgeDate != null)
        {
            return fDate.getString(JudgeDate);
        }
        else
        {
            return null;
        }
    }

    public void setJudgeDate(Date aJudgeDate)
    {
        JudgeDate = aJudgeDate;
    }

    public void setJudgeDate(String aJudgeDate)
    {
        if (aJudgeDate != null && !aJudgeDate.equals(""))
        {
            JudgeDate = fDate.getDate(aJudgeDate);
        }
        else
        {
            JudgeDate = null;
        }
    }

    public String getJudgeDepend()
    {
        if (SysConst.CHANGECHARSET && JudgeDepend != null &&
            !JudgeDepend.equals(""))
        {
            JudgeDepend = StrTool.unicodeToGBK(JudgeDepend);
        }
        return JudgeDepend;
    }

    public void setJudgeDepend(String aJudgeDepend)
    {
        JudgeDepend = aJudgeDepend;
    }

    public String getMngCom()
    {
        if (SysConst.CHANGECHARSET && MngCom != null && !MngCom.equals(""))
        {
            MngCom = StrTool.unicodeToGBK(MngCom);
        }
        return MngCom;
    }

    public void setMngCom(String aMngCom)
    {
        MngCom = aMngCom;
    }

    public String getOperator()
    {
        if (SysConst.CHANGECHARSET && Operator != null && !Operator.equals(""))
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (SysConst.CHANGECHARSET && MakeTime != null && !MakeTime.equals(""))
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (SysConst.CHANGECHARSET && ModifyTime != null &&
            !ModifyTime.equals(""))
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LLCaseInfoSchema 对象给 Schema 赋值
     * @param: aLLCaseInfoSchema LLCaseInfoSchema
     **/
    public void setSchema(LLCaseInfoSchema aLLCaseInfoSchema)
    {
        this.RgtNo = aLLCaseInfoSchema.getRgtNo();
        this.SerialNo = aLLCaseInfoSchema.getSerialNo();
        this.CaseNo = aLLCaseInfoSchema.getCaseNo();
        this.CaseRelaNo = aLLCaseInfoSchema.getCaseRelaNo();
        this.AffixNo = aLLCaseInfoSchema.getAffixNo();
        this.CustomerNo = aLLCaseInfoSchema.getCustomerNo();
        this.CustomerName = aLLCaseInfoSchema.getCustomerName();
        this.Type = aLLCaseInfoSchema.getType();
        this.Code = aLLCaseInfoSchema.getCode();
        this.Name = aLLCaseInfoSchema.getName();
        this.Diagnosis = aLLCaseInfoSchema.getDiagnosis();
        this.ClmFlag = aLLCaseInfoSchema.getClmFlag();
        this.DeformityKind = aLLCaseInfoSchema.getDeformityKind();
        this.DeformityGrade = aLLCaseInfoSchema.getDeformityGrade();
        this.GradeName = aLLCaseInfoSchema.getGradeName();
        this.PartCode = aLLCaseInfoSchema.getPartCode();
        this.DeformityReason = aLLCaseInfoSchema.getDeformityReason();
        this.DeformityRate = aLLCaseInfoSchema.getDeformityRate();
        this.AppDeformityRate = aLLCaseInfoSchema.getAppDeformityRate();
        this.RealRate = aLLCaseInfoSchema.getRealRate();
        this.No = aLLCaseInfoSchema.getNo();
        this.DianoseDate = fDate.getDate(aLLCaseInfoSchema.getDianoseDate());
        this.DiagnoseDesc = aLLCaseInfoSchema.getDiagnoseDesc();
        this.HospitalCode = aLLCaseInfoSchema.getHospitalCode();
        this.HospitalName = aLLCaseInfoSchema.getHospitalName();
        this.DoctorNo = aLLCaseInfoSchema.getDoctorNo();
        this.DoctorName = aLLCaseInfoSchema.getDoctorName();
        this.JudgeOrgan = aLLCaseInfoSchema.getJudgeOrgan();
        this.JudgeOrganName = aLLCaseInfoSchema.getJudgeOrganName();
        this.JudgeDate = fDate.getDate(aLLCaseInfoSchema.getJudgeDate());
        this.JudgeDepend = aLLCaseInfoSchema.getJudgeDepend();
        this.MngCom = aLLCaseInfoSchema.getMngCom();
        this.Operator = aLLCaseInfoSchema.getOperator();
        this.MakeDate = fDate.getDate(aLLCaseInfoSchema.getMakeDate());
        this.MakeTime = aLLCaseInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLLCaseInfoSchema.getModifyDate());
        this.ModifyTime = aLLCaseInfoSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString(1) == null)
            {
                this.RgtNo = null;
            }
            else
            {
                this.RgtNo = rs.getString(1).trim();
            }

            if (rs.getString(2) == null)
            {
                this.SerialNo = null;
            }
            else
            {
                this.SerialNo = rs.getString(2).trim();
            }

            if (rs.getString(3) == null)
            {
                this.CaseNo = null;
            }
            else
            {
                this.CaseNo = rs.getString(3).trim();
            }

            if (rs.getString(4) == null)
            {
                this.CaseRelaNo = null;
            }
            else
            {
                this.CaseRelaNo = rs.getString(4).trim();
            }

            if (rs.getString(5) == null)
            {
                this.AffixNo = null;
            }
            else
            {
                this.AffixNo = rs.getString(5).trim();
            }

            if (rs.getString(6) == null)
            {
                this.CustomerNo = null;
            }
            else
            {
                this.CustomerNo = rs.getString(6).trim();
            }

            if (rs.getString(7) == null)
            {
                this.CustomerName = null;
            }
            else
            {
                this.CustomerName = rs.getString(7).trim();
            }

            if (rs.getString(8) == null)
            {
                this.Type = null;
            }
            else
            {
                this.Type = rs.getString(8).trim();
            }

            if (rs.getString(9) == null)
            {
                this.Code = null;
            }
            else
            {
                this.Code = rs.getString(9).trim();
            }

            if (rs.getString(10) == null)
            {
                this.Name = null;
            }
            else
            {
                this.Name = rs.getString(10).trim();
            }

            if (rs.getString(11) == null)
            {
                this.Diagnosis = null;
            }
            else
            {
                this.Diagnosis = rs.getString(11).trim();
            }

            if (rs.getString(12) == null)
            {
                this.ClmFlag = null;
            }
            else
            {
                this.ClmFlag = rs.getString(12).trim();
            }

            if (rs.getString(13) == null)
            {
                this.DeformityKind = null;
            }
            else
            {
                this.DeformityKind = rs.getString(13).trim();
            }

            if (rs.getString(14) == null)
            {
                this.DeformityGrade = null;
            }
            else
            {
                this.DeformityGrade = rs.getString(14).trim();
            }

            if (rs.getString(15) == null)
            {
                this.GradeName = null;
            }
            else
            {
                this.GradeName = rs.getString(15).trim();
            }

            if (rs.getString(16) == null)
            {
                this.PartCode = null;
            }
            else
            {
                this.PartCode = rs.getString(16).trim();
            }

            if (rs.getString(17) == null)
            {
                this.DeformityReason = null;
            }
            else
            {
                this.DeformityReason = rs.getString(17).trim();
            }

            this.DeformityRate = rs.getDouble(18);
            this.AppDeformityRate = rs.getDouble(19);
            this.RealRate = rs.getDouble(20);
            if (rs.getString(21) == null)
            {
                this.No = null;
            }
            else
            {
                this.No = rs.getString(21).trim();
            }

            this.DianoseDate = rs.getDate(22);
            if (rs.getString(23) == null)
            {
                this.DiagnoseDesc = null;
            }
            else
            {
                this.DiagnoseDesc = rs.getString(23).trim();
            }

            if (rs.getString(24) == null)
            {
                this.HospitalCode = null;
            }
            else
            {
                this.HospitalCode = rs.getString(24).trim();
            }

            if (rs.getString(25) == null)
            {
                this.HospitalName = null;
            }
            else
            {
                this.HospitalName = rs.getString(25).trim();
            }

            if (rs.getString(26) == null)
            {
                this.DoctorNo = null;
            }
            else
            {
                this.DoctorNo = rs.getString(26).trim();
            }

            if (rs.getString(27) == null)
            {
                this.DoctorName = null;
            }
            else
            {
                this.DoctorName = rs.getString(27).trim();
            }

            if (rs.getString(28) == null)
            {
                this.JudgeOrgan = null;
            }
            else
            {
                this.JudgeOrgan = rs.getString(28).trim();
            }

            if (rs.getString(29) == null)
            {
                this.JudgeOrganName = null;
            }
            else
            {
                this.JudgeOrganName = rs.getString(29).trim();
            }

            this.JudgeDate = rs.getDate(30);
            if (rs.getString(31) == null)
            {
                this.JudgeDepend = null;
            }
            else
            {
                this.JudgeDepend = rs.getString(31).trim();
            }

            if (rs.getString(32) == null)
            {
                this.MngCom = null;
            }
            else
            {
                this.MngCom = rs.getString(32).trim();
            }

            if (rs.getString(33) == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString(33).trim();
            }

            this.MakeDate = rs.getDate(34);
            if (rs.getString(35) == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString(35).trim();
            }

            this.ModifyDate = rs.getDate(36);
            if (rs.getString(37) == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString(37).trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LLCaseInfoSchema getSchema()
    {
        LLCaseInfoSchema aLLCaseInfoSchema = new LLCaseInfoSchema();
        aLLCaseInfoSchema.setSchema(this);
        return aLLCaseInfoSchema;
    }

    public LLCaseInfoDB getDB()
    {
        LLCaseInfoDB aDBOper = new LLCaseInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(RgtNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(SerialNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CaseNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CaseRelaNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(AffixNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CustomerNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(CustomerName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Type)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Code)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Name)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Diagnosis)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ClmFlag)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DeformityKind)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DeformityGrade)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(GradeName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(PartCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DeformityReason)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(DeformityRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(AppDeformityRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(RealRate));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(No)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                DianoseDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DiagnoseDesc)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(HospitalCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(HospitalName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DoctorNo)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(DoctorName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(JudgeOrgan)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(JudgeOrganName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                JudgeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(JudgeDepend)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MngCom)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(Operator)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                MakeDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                ModifyDate))));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLLCaseInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RgtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                   SysConst.PACKAGESPILTER);
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            CaseNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                    SysConst.PACKAGESPILTER);
            CaseRelaNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                        SysConst.PACKAGESPILTER);
            AffixNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            CustomerNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                        SysConst.PACKAGESPILTER);
            CustomerName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                          SysConst.PACKAGESPILTER);
            Type = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                  SysConst.PACKAGESPILTER);
            Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                  SysConst.PACKAGESPILTER);
            Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                  SysConst.PACKAGESPILTER);
            Diagnosis = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            ClmFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                     SysConst.PACKAGESPILTER);
            DeformityKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                           SysConst.PACKAGESPILTER);
            DeformityGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            14, SysConst.PACKAGESPILTER);
            GradeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                       SysConst.PACKAGESPILTER);
            PartCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                      SysConst.PACKAGESPILTER);
            DeformityReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             17, SysConst.PACKAGESPILTER);
            DeformityRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 18, SysConst.PACKAGESPILTER))).doubleValue();
            AppDeformityRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 19, SysConst.PACKAGESPILTER))).doubleValue();
            RealRate = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 20, SysConst.PACKAGESPILTER))).doubleValue();
            No = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                SysConst.PACKAGESPILTER);
            DianoseDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 22, SysConst.PACKAGESPILTER));
            DiagnoseDesc = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,
                                          SysConst.PACKAGESPILTER);
            HospitalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                          SysConst.PACKAGESPILTER);
            HospitalName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                          SysConst.PACKAGESPILTER);
            DoctorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26,
                                      SysConst.PACKAGESPILTER);
            DoctorName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                        SysConst.PACKAGESPILTER);
            JudgeOrgan = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28,
                                        SysConst.PACKAGESPILTER);
            JudgeOrganName = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            29, SysConst.PACKAGESPILTER);
            JudgeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 30, SysConst.PACKAGESPILTER));
            JudgeDepend = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,
                                         SysConst.PACKAGESPILTER);
            MngCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32,
                                    SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 34, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 36, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LLCaseInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RgtNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RgtNo));
        }
        if (FCode.equals("SerialNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("CaseNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseNo));
        }
        if (FCode.equals("CaseRelaNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CaseRelaNo));
        }
        if (FCode.equals("AffixNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AffixNo));
        }
        if (FCode.equals("CustomerNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerNo));
        }
        if (FCode.equals("CustomerName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CustomerName));
        }
        if (FCode.equals("Type"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Type));
        }
        if (FCode.equals("Code"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Code));
        }
        if (FCode.equals("Name"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
        }
        if (FCode.equals("Diagnosis"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Diagnosis));
        }
        if (FCode.equals("ClmFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ClmFlag));
        }
        if (FCode.equals("DeformityKind"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeformityKind));
        }
        if (FCode.equals("DeformityGrade"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeformityGrade));
        }
        if (FCode.equals("GradeName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GradeName));
        }
        if (FCode.equals("PartCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PartCode));
        }
        if (FCode.equals("DeformityReason"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeformityReason));
        }
        if (FCode.equals("DeformityRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DeformityRate));
        }
        if (FCode.equals("AppDeformityRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppDeformityRate));
        }
        if (FCode.equals("RealRate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RealRate));
        }
        if (FCode.equals("No"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(No));
        }
        if (FCode.equals("DianoseDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getDianoseDate()));
        }
        if (FCode.equals("DiagnoseDesc"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DiagnoseDesc));
        }
        if (FCode.equals("HospitalCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalCode));
        }
        if (FCode.equals("HospitalName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(HospitalName));
        }
        if (FCode.equals("DoctorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoctorNo));
        }
        if (FCode.equals("DoctorName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DoctorName));
        }
        if (FCode.equals("JudgeOrgan"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JudgeOrgan));
        }
        if (FCode.equals("JudgeOrganName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JudgeOrganName));
        }
        if (FCode.equals("JudgeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getJudgeDate()));
        }
        if (FCode.equals("JudgeDepend"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(JudgeDepend));
        }
        if (FCode.equals("MngCom"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MngCom));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RgtNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(SerialNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CaseNo);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CaseRelaNo);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(AffixNo);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(CustomerNo);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(CustomerName);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Type);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(Code);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(Name);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Diagnosis);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(ClmFlag);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(DeformityKind);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(DeformityGrade);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(GradeName);
                break;
            case 15:
                strFieldValue = StrTool.GBKToUnicode(PartCode);
                break;
            case 16:
                strFieldValue = StrTool.GBKToUnicode(DeformityReason);
                break;
            case 17:
                strFieldValue = String.valueOf(DeformityRate);
                break;
            case 18:
                strFieldValue = String.valueOf(AppDeformityRate);
                break;
            case 19:
                strFieldValue = String.valueOf(RealRate);
                break;
            case 20:
                strFieldValue = StrTool.GBKToUnicode(No);
                break;
            case 21:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getDianoseDate()));
                break;
            case 22:
                strFieldValue = StrTool.GBKToUnicode(DiagnoseDesc);
                break;
            case 23:
                strFieldValue = StrTool.GBKToUnicode(HospitalCode);
                break;
            case 24:
                strFieldValue = StrTool.GBKToUnicode(HospitalName);
                break;
            case 25:
                strFieldValue = StrTool.GBKToUnicode(DoctorNo);
                break;
            case 26:
                strFieldValue = StrTool.GBKToUnicode(DoctorName);
                break;
            case 27:
                strFieldValue = StrTool.GBKToUnicode(JudgeOrgan);
                break;
            case 28:
                strFieldValue = StrTool.GBKToUnicode(JudgeOrganName);
                break;
            case 29:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getJudgeDate()));
                break;
            case 30:
                strFieldValue = StrTool.GBKToUnicode(JudgeDepend);
                break;
            case 31:
                strFieldValue = StrTool.GBKToUnicode(MngCom);
                break;
            case 32:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 33:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 34:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 35:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 36:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RgtNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RgtNo = FValue.trim();
            }
            else
            {
                RgtNo = null;
            }
        }
        if (FCode.equals("SerialNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SerialNo = FValue.trim();
            }
            else
            {
                SerialNo = null;
            }
        }
        if (FCode.equals("CaseNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseNo = FValue.trim();
            }
            else
            {
                CaseNo = null;
            }
        }
        if (FCode.equals("CaseRelaNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CaseRelaNo = FValue.trim();
            }
            else
            {
                CaseRelaNo = null;
            }
        }
        if (FCode.equals("AffixNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AffixNo = FValue.trim();
            }
            else
            {
                AffixNo = null;
            }
        }
        if (FCode.equals("CustomerNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerNo = FValue.trim();
            }
            else
            {
                CustomerNo = null;
            }
        }
        if (FCode.equals("CustomerName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CustomerName = FValue.trim();
            }
            else
            {
                CustomerName = null;
            }
        }
        if (FCode.equals("Type"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Type = FValue.trim();
            }
            else
            {
                Type = null;
            }
        }
        if (FCode.equals("Code"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Code = FValue.trim();
            }
            else
            {
                Code = null;
            }
        }
        if (FCode.equals("Name"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Name = FValue.trim();
            }
            else
            {
                Name = null;
            }
        }
        if (FCode.equals("Diagnosis"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Diagnosis = FValue.trim();
            }
            else
            {
                Diagnosis = null;
            }
        }
        if (FCode.equals("ClmFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClmFlag = FValue.trim();
            }
            else
            {
                ClmFlag = null;
            }
        }
        if (FCode.equals("DeformityKind"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeformityKind = FValue.trim();
            }
            else
            {
                DeformityKind = null;
            }
        }
        if (FCode.equals("DeformityGrade"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeformityGrade = FValue.trim();
            }
            else
            {
                DeformityGrade = null;
            }
        }
        if (FCode.equals("GradeName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                GradeName = FValue.trim();
            }
            else
            {
                GradeName = null;
            }
        }
        if (FCode.equals("PartCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PartCode = FValue.trim();
            }
            else
            {
                PartCode = null;
            }
        }
        if (FCode.equals("DeformityReason"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DeformityReason = FValue.trim();
            }
            else
            {
                DeformityReason = null;
            }
        }
        if (FCode.equals("DeformityRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                DeformityRate = d;
            }
        }
        if (FCode.equals("AppDeformityRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                AppDeformityRate = d;
            }
        }
        if (FCode.equals("RealRate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                RealRate = d;
            }
        }
        if (FCode.equals("No"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                No = FValue.trim();
            }
            else
            {
                No = null;
            }
        }
        if (FCode.equals("DianoseDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DianoseDate = fDate.getDate(FValue);
            }
            else
            {
                DianoseDate = null;
            }
        }
        if (FCode.equals("DiagnoseDesc"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DiagnoseDesc = FValue.trim();
            }
            else
            {
                DiagnoseDesc = null;
            }
        }
        if (FCode.equals("HospitalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HospitalCode = FValue.trim();
            }
            else
            {
                HospitalCode = null;
            }
        }
        if (FCode.equals("HospitalName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                HospitalName = FValue.trim();
            }
            else
            {
                HospitalName = null;
            }
        }
        if (FCode.equals("DoctorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DoctorNo = FValue.trim();
            }
            else
            {
                DoctorNo = null;
            }
        }
        if (FCode.equals("DoctorName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DoctorName = FValue.trim();
            }
            else
            {
                DoctorName = null;
            }
        }
        if (FCode.equals("JudgeOrgan"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                JudgeOrgan = FValue.trim();
            }
            else
            {
                JudgeOrgan = null;
            }
        }
        if (FCode.equals("JudgeOrganName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                JudgeOrganName = FValue.trim();
            }
            else
            {
                JudgeOrganName = null;
            }
        }
        if (FCode.equals("JudgeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                JudgeDate = fDate.getDate(FValue);
            }
            else
            {
                JudgeDate = null;
            }
        }
        if (FCode.equals("JudgeDepend"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                JudgeDepend = FValue.trim();
            }
            else
            {
                JudgeDepend = null;
            }
        }
        if (FCode.equals("MngCom"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MngCom = FValue.trim();
            }
            else
            {
                MngCom = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LLCaseInfoSchema other = (LLCaseInfoSchema) otherObject;
        return
                RgtNo.equals(other.getRgtNo())
                && SerialNo.equals(other.getSerialNo())
                && CaseNo.equals(other.getCaseNo())
                && CaseRelaNo.equals(other.getCaseRelaNo())
                && AffixNo.equals(other.getAffixNo())
                && CustomerNo.equals(other.getCustomerNo())
                && CustomerName.equals(other.getCustomerName())
                && Type.equals(other.getType())
                && Code.equals(other.getCode())
                && Name.equals(other.getName())
                && Diagnosis.equals(other.getDiagnosis())
                && ClmFlag.equals(other.getClmFlag())
                && DeformityKind.equals(other.getDeformityKind())
                && DeformityGrade.equals(other.getDeformityGrade())
                && GradeName.equals(other.getGradeName())
                && PartCode.equals(other.getPartCode())
                && DeformityReason.equals(other.getDeformityReason())
                && DeformityRate == other.getDeformityRate()
                && AppDeformityRate == other.getAppDeformityRate()
                && RealRate == other.getRealRate()
                && No.equals(other.getNo())
                && fDate.getString(DianoseDate).equals(other.getDianoseDate())
                && DiagnoseDesc.equals(other.getDiagnoseDesc())
                && HospitalCode.equals(other.getHospitalCode())
                && HospitalName.equals(other.getHospitalName())
                && DoctorNo.equals(other.getDoctorNo())
                && DoctorName.equals(other.getDoctorName())
                && JudgeOrgan.equals(other.getJudgeOrgan())
                && JudgeOrganName.equals(other.getJudgeOrganName())
                && fDate.getString(JudgeDate).equals(other.getJudgeDate())
                && JudgeDepend.equals(other.getJudgeDepend())
                && MngCom.equals(other.getMngCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RgtNo"))
        {
            return 0;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return 1;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return 2;
        }
        if (strFieldName.equals("CaseRelaNo"))
        {
            return 3;
        }
        if (strFieldName.equals("AffixNo"))
        {
            return 4;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return 5;
        }
        if (strFieldName.equals("CustomerName"))
        {
            return 6;
        }
        if (strFieldName.equals("Type"))
        {
            return 7;
        }
        if (strFieldName.equals("Code"))
        {
            return 8;
        }
        if (strFieldName.equals("Name"))
        {
            return 9;
        }
        if (strFieldName.equals("Diagnosis"))
        {
            return 10;
        }
        if (strFieldName.equals("ClmFlag"))
        {
            return 11;
        }
        if (strFieldName.equals("DeformityKind"))
        {
            return 12;
        }
        if (strFieldName.equals("DeformityGrade"))
        {
            return 13;
        }
        if (strFieldName.equals("GradeName"))
        {
            return 14;
        }
        if (strFieldName.equals("PartCode"))
        {
            return 15;
        }
        if (strFieldName.equals("DeformityReason"))
        {
            return 16;
        }
        if (strFieldName.equals("DeformityRate"))
        {
            return 17;
        }
        if (strFieldName.equals("AppDeformityRate"))
        {
            return 18;
        }
        if (strFieldName.equals("RealRate"))
        {
            return 19;
        }
        if (strFieldName.equals("No"))
        {
            return 20;
        }
        if (strFieldName.equals("DianoseDate"))
        {
            return 21;
        }
        if (strFieldName.equals("DiagnoseDesc"))
        {
            return 22;
        }
        if (strFieldName.equals("HospitalCode"))
        {
            return 23;
        }
        if (strFieldName.equals("HospitalName"))
        {
            return 24;
        }
        if (strFieldName.equals("DoctorNo"))
        {
            return 25;
        }
        if (strFieldName.equals("DoctorName"))
        {
            return 26;
        }
        if (strFieldName.equals("JudgeOrgan"))
        {
            return 27;
        }
        if (strFieldName.equals("JudgeOrganName"))
        {
            return 28;
        }
        if (strFieldName.equals("JudgeDate"))
        {
            return 29;
        }
        if (strFieldName.equals("JudgeDepend"))
        {
            return 30;
        }
        if (strFieldName.equals("MngCom"))
        {
            return 31;
        }
        if (strFieldName.equals("Operator"))
        {
            return 32;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 33;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 34;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 35;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 36;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RgtNo";
                break;
            case 1:
                strFieldName = "SerialNo";
                break;
            case 2:
                strFieldName = "CaseNo";
                break;
            case 3:
                strFieldName = "CaseRelaNo";
                break;
            case 4:
                strFieldName = "AffixNo";
                break;
            case 5:
                strFieldName = "CustomerNo";
                break;
            case 6:
                strFieldName = "CustomerName";
                break;
            case 7:
                strFieldName = "Type";
                break;
            case 8:
                strFieldName = "Code";
                break;
            case 9:
                strFieldName = "Name";
                break;
            case 10:
                strFieldName = "Diagnosis";
                break;
            case 11:
                strFieldName = "ClmFlag";
                break;
            case 12:
                strFieldName = "DeformityKind";
                break;
            case 13:
                strFieldName = "DeformityGrade";
                break;
            case 14:
                strFieldName = "GradeName";
                break;
            case 15:
                strFieldName = "PartCode";
                break;
            case 16:
                strFieldName = "DeformityReason";
                break;
            case 17:
                strFieldName = "DeformityRate";
                break;
            case 18:
                strFieldName = "AppDeformityRate";
                break;
            case 19:
                strFieldName = "RealRate";
                break;
            case 20:
                strFieldName = "No";
                break;
            case 21:
                strFieldName = "DianoseDate";
                break;
            case 22:
                strFieldName = "DiagnoseDesc";
                break;
            case 23:
                strFieldName = "HospitalCode";
                break;
            case 24:
                strFieldName = "HospitalName";
                break;
            case 25:
                strFieldName = "DoctorNo";
                break;
            case 26:
                strFieldName = "DoctorName";
                break;
            case 27:
                strFieldName = "JudgeOrgan";
                break;
            case 28:
                strFieldName = "JudgeOrganName";
                break;
            case 29:
                strFieldName = "JudgeDate";
                break;
            case 30:
                strFieldName = "JudgeDepend";
                break;
            case 31:
                strFieldName = "MngCom";
                break;
            case 32:
                strFieldName = "Operator";
                break;
            case 33:
                strFieldName = "MakeDate";
                break;
            case 34:
                strFieldName = "MakeTime";
                break;
            case 35:
                strFieldName = "ModifyDate";
                break;
            case 36:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RgtNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SerialNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CaseRelaNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AffixNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CustomerName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Type"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Code"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Name"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Diagnosis"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClmFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeformityKind"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeformityGrade"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GradeName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PartCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeformityReason"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DeformityRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AppDeformityRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("RealRate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("No"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DianoseDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("DiagnoseDesc"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HospitalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("HospitalName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DoctorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DoctorName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("JudgeOrgan"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("JudgeOrganName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("JudgeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("JudgeDepend"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MngCom"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 15:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 16:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 17:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 18:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 19:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 20:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 21:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 22:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 23:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 24:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 25:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 26:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 27:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 28:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 29:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 30:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 31:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 32:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 33:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 34:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 35:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 36:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
