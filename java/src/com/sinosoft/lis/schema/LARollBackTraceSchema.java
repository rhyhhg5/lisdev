/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LARollBackTraceDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LARollBackTraceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-06-02
 */
public class LARollBackTraceSchema implements Schema, Cloneable
{
    // @Field
    /** 回退序号 */
    private String Idx;
    /** 转储号码 */
    private String EdorNo;
    /** 回退操作员 */
    private String operator;
    /** 回退状态 */
    private String state;
    /** 回退条件 */
    private String Conditions;
    /** 回退日期 */
    private Date MakeDate;
    /** 回退时间 */
    private String MakeTime;
    /** 回退恢复日期 */
    private Date RecoverDate;
    /** 回退恢复时间 */
    private String RecoverTime;
    /** 回退类型 */
    private String RollBackType;

    public static final int FIELDNUM = 10; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LARollBackTraceSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "Idx";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LARollBackTraceSchema cloned = (LARollBackTraceSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getIdx()
    {
        return Idx;
    }

    public void setIdx(String aIdx)
    {
        Idx = aIdx;
    }

    public String getEdorNo()
    {
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getoperator()
    {
        return operator;
    }

    public void setoperator(String aoperator)
    {
        operator = aoperator;
    }

    public String getstate()
    {
        return state;
    }

    public void setstate(String astate)
    {
        state = astate;
    }

    public String getConditions()
    {
        return Conditions;
    }

    public void setConditions(String aConditions)
    {
        Conditions = aConditions;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getRecoverDate()
    {
        if (RecoverDate != null)
        {
            return fDate.getString(RecoverDate);
        }
        else
        {
            return null;
        }
    }

    public void setRecoverDate(Date aRecoverDate)
    {
        RecoverDate = aRecoverDate;
    }

    public void setRecoverDate(String aRecoverDate)
    {
        if (aRecoverDate != null && !aRecoverDate.equals(""))
        {
            RecoverDate = fDate.getDate(aRecoverDate);
        }
        else
        {
            RecoverDate = null;
        }
    }

    public String getRecoverTime()
    {
        return RecoverTime;
    }

    public void setRecoverTime(String aRecoverTime)
    {
        RecoverTime = aRecoverTime;
    }

    public String getRollBackType()
    {
        return RollBackType;
    }

    public void setRollBackType(String aRollBackType)
    {
        RollBackType = aRollBackType;
    }

    /**
     * 使用另外一个 LARollBackTraceSchema 对象给 Schema 赋值
     * @param: aLARollBackTraceSchema LARollBackTraceSchema
     **/
    public void setSchema(LARollBackTraceSchema aLARollBackTraceSchema)
    {
        this.Idx = aLARollBackTraceSchema.getIdx();
        this.EdorNo = aLARollBackTraceSchema.getEdorNo();
        this.operator = aLARollBackTraceSchema.getoperator();
        this.state = aLARollBackTraceSchema.getstate();
        this.Conditions = aLARollBackTraceSchema.getConditions();
        this.MakeDate = fDate.getDate(aLARollBackTraceSchema.getMakeDate());
        this.MakeTime = aLARollBackTraceSchema.getMakeTime();
        this.RecoverDate = fDate.getDate(aLARollBackTraceSchema.getRecoverDate());
        this.RecoverTime = aLARollBackTraceSchema.getRecoverTime();
        this.RollBackType = aLARollBackTraceSchema.getRollBackType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("Idx") == null)
            {
                this.Idx = null;
            }
            else
            {
                this.Idx = rs.getString("Idx").trim();
            }

            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("operator") == null)
            {
                this.operator = null;
            }
            else
            {
                this.operator = rs.getString("operator").trim();
            }

            if (rs.getString("state") == null)
            {
                this.state = null;
            }
            else
            {
                this.state = rs.getString("state").trim();
            }

            if (rs.getString("Conditions") == null)
            {
                this.Conditions = null;
            }
            else
            {
                this.Conditions = rs.getString("Conditions").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.RecoverDate = rs.getDate("RecoverDate");
            if (rs.getString("RecoverTime") == null)
            {
                this.RecoverTime = null;
            }
            else
            {
                this.RecoverTime = rs.getString("RecoverTime").trim();
            }

            if (rs.getString("RollBackType") == null)
            {
                this.RollBackType = null;
            }
            else
            {
                this.RollBackType = rs.getString("RollBackType").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LARollBackTrace表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LARollBackTraceSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LARollBackTraceSchema getSchema()
    {
        LARollBackTraceSchema aLARollBackTraceSchema = new
                LARollBackTraceSchema();
        aLARollBackTraceSchema.setSchema(this);
        return aLARollBackTraceSchema;
    }

    public LARollBackTraceDB getDB()
    {
        LARollBackTraceDB aDBOper = new LARollBackTraceDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARollBackTrace描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(Idx));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(state));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Conditions));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(RecoverDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RecoverTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RollBackType));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLARollBackTrace>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            Idx = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                 SysConst.PACKAGESPILTER);
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            state = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                   SysConst.PACKAGESPILTER);
            Conditions = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                        SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                      SysConst.PACKAGESPILTER);
            RecoverDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 8, SysConst.PACKAGESPILTER));
            RecoverTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            RollBackType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                          SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LARollBackTraceSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("Idx"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
        }
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("operator"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(operator));
        }
        if (FCode.equals("state"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(state));
        }
        if (FCode.equals("Conditions"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Conditions));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("RecoverDate"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getRecoverDate()));
        }
        if (FCode.equals("RecoverTime"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RecoverTime));
        }
        if (FCode.equals("RollBackType"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RollBackType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(Idx);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(operator);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(state);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(Conditions);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getRecoverDate()));
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(RecoverTime);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(RollBackType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("Idx"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Idx = FValue.trim();
            }
            else
            {
                Idx = null;
            }
        }
        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                operator = FValue.trim();
            }
            else
            {
                operator = null;
            }
        }
        if (FCode.equals("state"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                state = FValue.trim();
            }
            else
            {
                state = null;
            }
        }
        if (FCode.equals("Conditions"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Conditions = FValue.trim();
            }
            else
            {
                Conditions = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("RecoverDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RecoverDate = fDate.getDate(FValue);
            }
            else
            {
                RecoverDate = null;
            }
        }
        if (FCode.equals("RecoverTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RecoverTime = FValue.trim();
            }
            else
            {
                RecoverTime = null;
            }
        }
        if (FCode.equals("RollBackType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RollBackType = FValue.trim();
            }
            else
            {
                RollBackType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LARollBackTraceSchema other = (LARollBackTraceSchema) otherObject;
        return
                Idx.equals(other.getIdx())
                && EdorNo.equals(other.getEdorNo())
                && operator.equals(other.getoperator())
                && state.equals(other.getstate())
                && Conditions.equals(other.getConditions())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(RecoverDate).equals(other.getRecoverDate())
                && RecoverTime.equals(other.getRecoverTime())
                && RollBackType.equals(other.getRollBackType());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("Idx"))
        {
            return 0;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return 1;
        }
        if (strFieldName.equals("operator"))
        {
            return 2;
        }
        if (strFieldName.equals("state"))
        {
            return 3;
        }
        if (strFieldName.equals("Conditions"))
        {
            return 4;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 5;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 6;
        }
        if (strFieldName.equals("RecoverDate"))
        {
            return 7;
        }
        if (strFieldName.equals("RecoverTime"))
        {
            return 8;
        }
        if (strFieldName.equals("RollBackType"))
        {
            return 9;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "Idx";
                break;
            case 1:
                strFieldName = "EdorNo";
                break;
            case 2:
                strFieldName = "operator";
                break;
            case 3:
                strFieldName = "state";
                break;
            case 4:
                strFieldName = "Conditions";
                break;
            case 5:
                strFieldName = "MakeDate";
                break;
            case 6:
                strFieldName = "MakeTime";
                break;
            case 7:
                strFieldName = "RecoverDate";
                break;
            case 8:
                strFieldName = "RecoverTime";
                break;
            case 9:
                strFieldName = "RollBackType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("Idx"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("state"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Conditions"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RecoverDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("RecoverTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RollBackType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
