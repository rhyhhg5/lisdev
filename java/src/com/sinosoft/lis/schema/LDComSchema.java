/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDComDB;

/*
 * <p>ClassName: LDComSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 管理机构
 * @CreateDate：2017-09-19
 */
public class LDComSchema implements Schema, Cloneable
{
	// @Field
	/** 机构编码 */
	private String ComCode;
	/** 对外公布的机构代码 */
	private String OutComCode;
	/** 机构名称 */
	private String Name;
	/** 短名称 */
	private String ShortName;
	/** 机构地址 */
	private String Address;
	/** 机构邮编 */
	private String ZipCode;
	/** 机构电话 */
	private String Phone;
	/** 机构传真 */
	private String Fax;
	/** Email */
	private String EMail;
	/** 网址 */
	private String WebAddress;
	/** 主管人姓名 */
	private String SatrapName;
	/** 对应保监办代码 */
	private String InsuMonitorCode;
	/** 保险公司id */
	private String InsureID;
	/** 标识码 */
	private String SignID;
	/** 行政区划代码 */
	private String RegionalismCode;
	/** 公司性质 */
	private String ComNature;
	/** 校验码 */
	private String ValidCode;
	/** 是否开业标志 */
	private String Sign;
	/** 机构???在市规模 */
	private String ComCitySize;
	/** 客户服务机构名称 */
	private String ServiceName;
	/** 客户服务机构编码 */
	private String ServiceNo;
	/** 客户服务电话 */
	private String ServicePhone;
	/** 客户服务投递地址 */
	private String ServicePostAddress;
	/** 客户服务投递邮编 */
	private String ServicePostZipcode;
	/** 信函服务机构名称 */
	private String LetterServiceName;
	/** 信函服务机构编码 */
	private String LetterServiceNo;
	/** 信函服务电话 */
	private String LetterServicePhone;
	/** 信函服务投递地址 */
	private String LetterServicePostAddress;
	/** 信函服务投递邮编 */
	private String LetterServicePostZipcode;
	/** 机构级别 */
	private String ComGrade;
	/** 机构地区类型 */
	private String ComAreaType;
	/** 内部机构划分名称 */
	private String InnerComName;
	/** 销售机构划分名称 */
	private String SaleComName;
	/** 税务登记号 */
	private String TaxRegistryNo;
	/** 系统显示名称 */
	private String ShowName;
	/** 客户服务电话2 */
	private String ServicePhone2;
	/** 理赔报案电话 */
	private String ClaimReportPhone;
	/** 体检预约电话 */
	private String PEOrderPhone;
	/** 备用地址1 */
	private String BackupAddress1;
	/** 备用地址2 */
	private String BackupAddress2;
	/** 备用电话1 */
	private String BackupPhone1;
	/** 备用电话2 */
	private String BackupPhone2;
	/** 机构英文名称 */
	private String EName;
	/** 英文短名称 */
	private String EShortName;
	/** 机构英文地址 */
	private String EAddress;
	/** 客户服务投递英文地址 */
	private String EServicePostAddress;
	/** 信函服务机构英文名称 */
	private String ELetterServiceName;
	/** 信函服务投递英文地址 */
	private String ELetterServicePostAddress;
	/** 备用英文地址1 */
	private String EBackupAddress1;
	/** 备用英文地址2 */
	private String EBackupAddress2;
	/** 客户服务电话1 */
	private String ServicePhone1;
	/** 打印显示机构名称 */
	private String PrintComName;
	/** 所属机构 */
	private String SuperComCode;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 集团审核标志字段 */
	private String Crs_Check_Status;
	/** 县级机构标志 */
	private String ContyFlag;
	/** 互动部标志 */
	private String InteractFlag;
	/** 保单登记平台对应代码 */
	private String PolicyRegistCode;
	/** 地区代码 */
	private String AreaCode;

	public static final int FIELDNUM = 63;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LDComSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ComCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LDComSchema cloned = (LDComSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getComCode()
	{
		return ComCode;
	}
	public void setComCode(String aComCode)
	{
		ComCode = aComCode;
	}
	public String getOutComCode()
	{
		return OutComCode;
	}
	public void setOutComCode(String aOutComCode)
	{
		OutComCode = aOutComCode;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String aName)
	{
		Name = aName;
	}
	public String getShortName()
	{
		return ShortName;
	}
	public void setShortName(String aShortName)
	{
		ShortName = aShortName;
	}
	public String getAddress()
	{
		return Address;
	}
	public void setAddress(String aAddress)
	{
		Address = aAddress;
	}
	public String getZipCode()
	{
		return ZipCode;
	}
	public void setZipCode(String aZipCode)
	{
		ZipCode = aZipCode;
	}
	public String getPhone()
	{
		return Phone;
	}
	public void setPhone(String aPhone)
	{
		Phone = aPhone;
	}
	public String getFax()
	{
		return Fax;
	}
	public void setFax(String aFax)
	{
		Fax = aFax;
	}
	public String getEMail()
	{
		return EMail;
	}
	public void setEMail(String aEMail)
	{
		EMail = aEMail;
	}
	public String getWebAddress()
	{
		return WebAddress;
	}
	public void setWebAddress(String aWebAddress)
	{
		WebAddress = aWebAddress;
	}
	public String getSatrapName()
	{
		return SatrapName;
	}
	public void setSatrapName(String aSatrapName)
	{
		SatrapName = aSatrapName;
	}
	public String getInsuMonitorCode()
	{
		return InsuMonitorCode;
	}
	public void setInsuMonitorCode(String aInsuMonitorCode)
	{
		InsuMonitorCode = aInsuMonitorCode;
	}
	public String getInsureID()
	{
		return InsureID;
	}
	public void setInsureID(String aInsureID)
	{
		InsureID = aInsureID;
	}
	public String getSignID()
	{
		return SignID;
	}
	public void setSignID(String aSignID)
	{
		SignID = aSignID;
	}
	public String getRegionalismCode()
	{
		return RegionalismCode;
	}
	public void setRegionalismCode(String aRegionalismCode)
	{
		RegionalismCode = aRegionalismCode;
	}
	public String getComNature()
	{
		return ComNature;
	}
	public void setComNature(String aComNature)
	{
		ComNature = aComNature;
	}
	public String getValidCode()
	{
		return ValidCode;
	}
	public void setValidCode(String aValidCode)
	{
		ValidCode = aValidCode;
	}
	public String getSign()
	{
		return Sign;
	}
	public void setSign(String aSign)
	{
		Sign = aSign;
	}
	public String getComCitySize()
	{
		return ComCitySize;
	}
	public void setComCitySize(String aComCitySize)
	{
		ComCitySize = aComCitySize;
	}
	public String getServiceName()
	{
		return ServiceName;
	}
	public void setServiceName(String aServiceName)
	{
		ServiceName = aServiceName;
	}
	public String getServiceNo()
	{
		return ServiceNo;
	}
	public void setServiceNo(String aServiceNo)
	{
		ServiceNo = aServiceNo;
	}
	public String getServicePhone()
	{
		return ServicePhone;
	}
	public void setServicePhone(String aServicePhone)
	{
		ServicePhone = aServicePhone;
	}
	public String getServicePostAddress()
	{
		return ServicePostAddress;
	}
	public void setServicePostAddress(String aServicePostAddress)
	{
		ServicePostAddress = aServicePostAddress;
	}
	public String getServicePostZipcode()
	{
		return ServicePostZipcode;
	}
	public void setServicePostZipcode(String aServicePostZipcode)
	{
		ServicePostZipcode = aServicePostZipcode;
	}
	public String getLetterServiceName()
	{
		return LetterServiceName;
	}
	public void setLetterServiceName(String aLetterServiceName)
	{
		LetterServiceName = aLetterServiceName;
	}
	public String getLetterServiceNo()
	{
		return LetterServiceNo;
	}
	public void setLetterServiceNo(String aLetterServiceNo)
	{
		LetterServiceNo = aLetterServiceNo;
	}
	public String getLetterServicePhone()
	{
		return LetterServicePhone;
	}
	public void setLetterServicePhone(String aLetterServicePhone)
	{
		LetterServicePhone = aLetterServicePhone;
	}
	public String getLetterServicePostAddress()
	{
		return LetterServicePostAddress;
	}
	public void setLetterServicePostAddress(String aLetterServicePostAddress)
	{
		LetterServicePostAddress = aLetterServicePostAddress;
	}
	public String getLetterServicePostZipcode()
	{
		return LetterServicePostZipcode;
	}
	public void setLetterServicePostZipcode(String aLetterServicePostZipcode)
	{
		LetterServicePostZipcode = aLetterServicePostZipcode;
	}
	public String getComGrade()
	{
		return ComGrade;
	}
	public void setComGrade(String aComGrade)
	{
		ComGrade = aComGrade;
	}
	public String getComAreaType()
	{
		return ComAreaType;
	}
	public void setComAreaType(String aComAreaType)
	{
		ComAreaType = aComAreaType;
	}
	public String getInnerComName()
	{
		return InnerComName;
	}
	public void setInnerComName(String aInnerComName)
	{
		InnerComName = aInnerComName;
	}
	public String getSaleComName()
	{
		return SaleComName;
	}
	public void setSaleComName(String aSaleComName)
	{
		SaleComName = aSaleComName;
	}
	public String getTaxRegistryNo()
	{
		return TaxRegistryNo;
	}
	public void setTaxRegistryNo(String aTaxRegistryNo)
	{
		TaxRegistryNo = aTaxRegistryNo;
	}
	public String getShowName()
	{
		return ShowName;
	}
	public void setShowName(String aShowName)
	{
		ShowName = aShowName;
	}
	public String getServicePhone2()
	{
		return ServicePhone2;
	}
	public void setServicePhone2(String aServicePhone2)
	{
		ServicePhone2 = aServicePhone2;
	}
	public String getClaimReportPhone()
	{
		return ClaimReportPhone;
	}
	public void setClaimReportPhone(String aClaimReportPhone)
	{
		ClaimReportPhone = aClaimReportPhone;
	}
	public String getPEOrderPhone()
	{
		return PEOrderPhone;
	}
	public void setPEOrderPhone(String aPEOrderPhone)
	{
		PEOrderPhone = aPEOrderPhone;
	}
	public String getBackupAddress1()
	{
		return BackupAddress1;
	}
	public void setBackupAddress1(String aBackupAddress1)
	{
		BackupAddress1 = aBackupAddress1;
	}
	public String getBackupAddress2()
	{
		return BackupAddress2;
	}
	public void setBackupAddress2(String aBackupAddress2)
	{
		BackupAddress2 = aBackupAddress2;
	}
	public String getBackupPhone1()
	{
		return BackupPhone1;
	}
	public void setBackupPhone1(String aBackupPhone1)
	{
		BackupPhone1 = aBackupPhone1;
	}
	public String getBackupPhone2()
	{
		return BackupPhone2;
	}
	public void setBackupPhone2(String aBackupPhone2)
	{
		BackupPhone2 = aBackupPhone2;
	}
	public String getEName()
	{
		return EName;
	}
	public void setEName(String aEName)
	{
		EName = aEName;
	}
	public String getEShortName()
	{
		return EShortName;
	}
	public void setEShortName(String aEShortName)
	{
		EShortName = aEShortName;
	}
	public String getEAddress()
	{
		return EAddress;
	}
	public void setEAddress(String aEAddress)
	{
		EAddress = aEAddress;
	}
	public String getEServicePostAddress()
	{
		return EServicePostAddress;
	}
	public void setEServicePostAddress(String aEServicePostAddress)
	{
		EServicePostAddress = aEServicePostAddress;
	}
	public String getELetterServiceName()
	{
		return ELetterServiceName;
	}
	public void setELetterServiceName(String aELetterServiceName)
	{
		ELetterServiceName = aELetterServiceName;
	}
	public String getELetterServicePostAddress()
	{
		return ELetterServicePostAddress;
	}
	public void setELetterServicePostAddress(String aELetterServicePostAddress)
	{
		ELetterServicePostAddress = aELetterServicePostAddress;
	}
	public String getEBackupAddress1()
	{
		return EBackupAddress1;
	}
	public void setEBackupAddress1(String aEBackupAddress1)
	{
		EBackupAddress1 = aEBackupAddress1;
	}
	public String getEBackupAddress2()
	{
		return EBackupAddress2;
	}
	public void setEBackupAddress2(String aEBackupAddress2)
	{
		EBackupAddress2 = aEBackupAddress2;
	}
	public String getServicePhone1()
	{
		return ServicePhone1;
	}
	public void setServicePhone1(String aServicePhone1)
	{
		ServicePhone1 = aServicePhone1;
	}
	public String getPrintComName()
	{
		return PrintComName;
	}
	public void setPrintComName(String aPrintComName)
	{
		PrintComName = aPrintComName;
	}
	public String getSuperComCode()
	{
		return SuperComCode;
	}
	public void setSuperComCode(String aSuperComCode)
	{
		SuperComCode = aSuperComCode;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getCrs_Check_Status()
	{
		return Crs_Check_Status;
	}
	public void setCrs_Check_Status(String aCrs_Check_Status)
	{
		Crs_Check_Status = aCrs_Check_Status;
	}
	public String getContyFlag()
	{
		return ContyFlag;
	}
	public void setContyFlag(String aContyFlag)
	{
		ContyFlag = aContyFlag;
	}
	public String getInteractFlag()
	{
		return InteractFlag;
	}
	public void setInteractFlag(String aInteractFlag)
	{
		InteractFlag = aInteractFlag;
	}
	public String getPolicyRegistCode()
	{
		return PolicyRegistCode;
	}
	public void setPolicyRegistCode(String aPolicyRegistCode)
	{
		PolicyRegistCode = aPolicyRegistCode;
	}
	public String getAreaCode()
	{
		return AreaCode;
	}
	public void setAreaCode(String aAreaCode)
	{
		AreaCode = aAreaCode;
	}

	/**
	* 使用另外一个 LDComSchema 对象给 Schema 赋值
	* @param: aLDComSchema LDComSchema
	**/
	public void setSchema(LDComSchema aLDComSchema)
	{
		this.ComCode = aLDComSchema.getComCode();
		this.OutComCode = aLDComSchema.getOutComCode();
		this.Name = aLDComSchema.getName();
		this.ShortName = aLDComSchema.getShortName();
		this.Address = aLDComSchema.getAddress();
		this.ZipCode = aLDComSchema.getZipCode();
		this.Phone = aLDComSchema.getPhone();
		this.Fax = aLDComSchema.getFax();
		this.EMail = aLDComSchema.getEMail();
		this.WebAddress = aLDComSchema.getWebAddress();
		this.SatrapName = aLDComSchema.getSatrapName();
		this.InsuMonitorCode = aLDComSchema.getInsuMonitorCode();
		this.InsureID = aLDComSchema.getInsureID();
		this.SignID = aLDComSchema.getSignID();
		this.RegionalismCode = aLDComSchema.getRegionalismCode();
		this.ComNature = aLDComSchema.getComNature();
		this.ValidCode = aLDComSchema.getValidCode();
		this.Sign = aLDComSchema.getSign();
		this.ComCitySize = aLDComSchema.getComCitySize();
		this.ServiceName = aLDComSchema.getServiceName();
		this.ServiceNo = aLDComSchema.getServiceNo();
		this.ServicePhone = aLDComSchema.getServicePhone();
		this.ServicePostAddress = aLDComSchema.getServicePostAddress();
		this.ServicePostZipcode = aLDComSchema.getServicePostZipcode();
		this.LetterServiceName = aLDComSchema.getLetterServiceName();
		this.LetterServiceNo = aLDComSchema.getLetterServiceNo();
		this.LetterServicePhone = aLDComSchema.getLetterServicePhone();
		this.LetterServicePostAddress = aLDComSchema.getLetterServicePostAddress();
		this.LetterServicePostZipcode = aLDComSchema.getLetterServicePostZipcode();
		this.ComGrade = aLDComSchema.getComGrade();
		this.ComAreaType = aLDComSchema.getComAreaType();
		this.InnerComName = aLDComSchema.getInnerComName();
		this.SaleComName = aLDComSchema.getSaleComName();
		this.TaxRegistryNo = aLDComSchema.getTaxRegistryNo();
		this.ShowName = aLDComSchema.getShowName();
		this.ServicePhone2 = aLDComSchema.getServicePhone2();
		this.ClaimReportPhone = aLDComSchema.getClaimReportPhone();
		this.PEOrderPhone = aLDComSchema.getPEOrderPhone();
		this.BackupAddress1 = aLDComSchema.getBackupAddress1();
		this.BackupAddress2 = aLDComSchema.getBackupAddress2();
		this.BackupPhone1 = aLDComSchema.getBackupPhone1();
		this.BackupPhone2 = aLDComSchema.getBackupPhone2();
		this.EName = aLDComSchema.getEName();
		this.EShortName = aLDComSchema.getEShortName();
		this.EAddress = aLDComSchema.getEAddress();
		this.EServicePostAddress = aLDComSchema.getEServicePostAddress();
		this.ELetterServiceName = aLDComSchema.getELetterServiceName();
		this.ELetterServicePostAddress = aLDComSchema.getELetterServicePostAddress();
		this.EBackupAddress1 = aLDComSchema.getEBackupAddress1();
		this.EBackupAddress2 = aLDComSchema.getEBackupAddress2();
		this.ServicePhone1 = aLDComSchema.getServicePhone1();
		this.PrintComName = aLDComSchema.getPrintComName();
		this.SuperComCode = aLDComSchema.getSuperComCode();
		this.Operator = aLDComSchema.getOperator();
		this.MakeDate = fDate.getDate( aLDComSchema.getMakeDate());
		this.MakeTime = aLDComSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLDComSchema.getModifyDate());
		this.ModifyTime = aLDComSchema.getModifyTime();
		this.Crs_Check_Status = aLDComSchema.getCrs_Check_Status();
		this.ContyFlag = aLDComSchema.getContyFlag();
		this.InteractFlag = aLDComSchema.getInteractFlag();
		this.PolicyRegistCode = aLDComSchema.getPolicyRegistCode();
		this.AreaCode = aLDComSchema.getAreaCode();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ComCode") == null )
				this.ComCode = null;
			else
				this.ComCode = rs.getString("ComCode").trim();

			if( rs.getString("OutComCode") == null )
				this.OutComCode = null;
			else
				this.OutComCode = rs.getString("OutComCode").trim();

			if( rs.getString("Name") == null )
				this.Name = null;
			else
				this.Name = rs.getString("Name").trim();

			if( rs.getString("ShortName") == null )
				this.ShortName = null;
			else
				this.ShortName = rs.getString("ShortName").trim();

			if( rs.getString("Address") == null )
				this.Address = null;
			else
				this.Address = rs.getString("Address").trim();

			if( rs.getString("ZipCode") == null )
				this.ZipCode = null;
			else
				this.ZipCode = rs.getString("ZipCode").trim();

			if( rs.getString("Phone") == null )
				this.Phone = null;
			else
				this.Phone = rs.getString("Phone").trim();

			if( rs.getString("Fax") == null )
				this.Fax = null;
			else
				this.Fax = rs.getString("Fax").trim();

			if( rs.getString("EMail") == null )
				this.EMail = null;
			else
				this.EMail = rs.getString("EMail").trim();

			if( rs.getString("WebAddress") == null )
				this.WebAddress = null;
			else
				this.WebAddress = rs.getString("WebAddress").trim();

			if( rs.getString("SatrapName") == null )
				this.SatrapName = null;
			else
				this.SatrapName = rs.getString("SatrapName").trim();

			if( rs.getString("InsuMonitorCode") == null )
				this.InsuMonitorCode = null;
			else
				this.InsuMonitorCode = rs.getString("InsuMonitorCode").trim();

			if( rs.getString("InsureID") == null )
				this.InsureID = null;
			else
				this.InsureID = rs.getString("InsureID").trim();

			if( rs.getString("SignID") == null )
				this.SignID = null;
			else
				this.SignID = rs.getString("SignID").trim();

			if( rs.getString("RegionalismCode") == null )
				this.RegionalismCode = null;
			else
				this.RegionalismCode = rs.getString("RegionalismCode").trim();

			if( rs.getString("ComNature") == null )
				this.ComNature = null;
			else
				this.ComNature = rs.getString("ComNature").trim();

			if( rs.getString("ValidCode") == null )
				this.ValidCode = null;
			else
				this.ValidCode = rs.getString("ValidCode").trim();

			if( rs.getString("Sign") == null )
				this.Sign = null;
			else
				this.Sign = rs.getString("Sign").trim();

			if( rs.getString("ComCitySize") == null )
				this.ComCitySize = null;
			else
				this.ComCitySize = rs.getString("ComCitySize").trim();

			if( rs.getString("ServiceName") == null )
				this.ServiceName = null;
			else
				this.ServiceName = rs.getString("ServiceName").trim();

			if( rs.getString("ServiceNo") == null )
				this.ServiceNo = null;
			else
				this.ServiceNo = rs.getString("ServiceNo").trim();

			if( rs.getString("ServicePhone") == null )
				this.ServicePhone = null;
			else
				this.ServicePhone = rs.getString("ServicePhone").trim();

			if( rs.getString("ServicePostAddress") == null )
				this.ServicePostAddress = null;
			else
				this.ServicePostAddress = rs.getString("ServicePostAddress").trim();

			if( rs.getString("ServicePostZipcode") == null )
				this.ServicePostZipcode = null;
			else
				this.ServicePostZipcode = rs.getString("ServicePostZipcode").trim();

			if( rs.getString("LetterServiceName") == null )
				this.LetterServiceName = null;
			else
				this.LetterServiceName = rs.getString("LetterServiceName").trim();

			if( rs.getString("LetterServiceNo") == null )
				this.LetterServiceNo = null;
			else
				this.LetterServiceNo = rs.getString("LetterServiceNo").trim();

			if( rs.getString("LetterServicePhone") == null )
				this.LetterServicePhone = null;
			else
				this.LetterServicePhone = rs.getString("LetterServicePhone").trim();

			if( rs.getString("LetterServicePostAddress") == null )
				this.LetterServicePostAddress = null;
			else
				this.LetterServicePostAddress = rs.getString("LetterServicePostAddress").trim();

			if( rs.getString("LetterServicePostZipcode") == null )
				this.LetterServicePostZipcode = null;
			else
				this.LetterServicePostZipcode = rs.getString("LetterServicePostZipcode").trim();

			if( rs.getString("ComGrade") == null )
				this.ComGrade = null;
			else
				this.ComGrade = rs.getString("ComGrade").trim();

			if( rs.getString("ComAreaType") == null )
				this.ComAreaType = null;
			else
				this.ComAreaType = rs.getString("ComAreaType").trim();

			if( rs.getString("InnerComName") == null )
				this.InnerComName = null;
			else
				this.InnerComName = rs.getString("InnerComName").trim();

			if( rs.getString("SaleComName") == null )
				this.SaleComName = null;
			else
				this.SaleComName = rs.getString("SaleComName").trim();

			if( rs.getString("TaxRegistryNo") == null )
				this.TaxRegistryNo = null;
			else
				this.TaxRegistryNo = rs.getString("TaxRegistryNo").trim();

			if( rs.getString("ShowName") == null )
				this.ShowName = null;
			else
				this.ShowName = rs.getString("ShowName").trim();

			if( rs.getString("ServicePhone2") == null )
				this.ServicePhone2 = null;
			else
				this.ServicePhone2 = rs.getString("ServicePhone2").trim();

			if( rs.getString("ClaimReportPhone") == null )
				this.ClaimReportPhone = null;
			else
				this.ClaimReportPhone = rs.getString("ClaimReportPhone").trim();

			if( rs.getString("PEOrderPhone") == null )
				this.PEOrderPhone = null;
			else
				this.PEOrderPhone = rs.getString("PEOrderPhone").trim();

			if( rs.getString("BackupAddress1") == null )
				this.BackupAddress1 = null;
			else
				this.BackupAddress1 = rs.getString("BackupAddress1").trim();

			if( rs.getString("BackupAddress2") == null )
				this.BackupAddress2 = null;
			else
				this.BackupAddress2 = rs.getString("BackupAddress2").trim();

			if( rs.getString("BackupPhone1") == null )
				this.BackupPhone1 = null;
			else
				this.BackupPhone1 = rs.getString("BackupPhone1").trim();

			if( rs.getString("BackupPhone2") == null )
				this.BackupPhone2 = null;
			else
				this.BackupPhone2 = rs.getString("BackupPhone2").trim();

			if( rs.getString("EName") == null )
				this.EName = null;
			else
				this.EName = rs.getString("EName").trim();

			if( rs.getString("EShortName") == null )
				this.EShortName = null;
			else
				this.EShortName = rs.getString("EShortName").trim();

			if( rs.getString("EAddress") == null )
				this.EAddress = null;
			else
				this.EAddress = rs.getString("EAddress").trim();

			if( rs.getString("EServicePostAddress") == null )
				this.EServicePostAddress = null;
			else
				this.EServicePostAddress = rs.getString("EServicePostAddress").trim();

			if( rs.getString("ELetterServiceName") == null )
				this.ELetterServiceName = null;
			else
				this.ELetterServiceName = rs.getString("ELetterServiceName").trim();

			if( rs.getString("ELetterServicePostAddress") == null )
				this.ELetterServicePostAddress = null;
			else
				this.ELetterServicePostAddress = rs.getString("ELetterServicePostAddress").trim();

			if( rs.getString("EBackupAddress1") == null )
				this.EBackupAddress1 = null;
			else
				this.EBackupAddress1 = rs.getString("EBackupAddress1").trim();

			if( rs.getString("EBackupAddress2") == null )
				this.EBackupAddress2 = null;
			else
				this.EBackupAddress2 = rs.getString("EBackupAddress2").trim();

			if( rs.getString("ServicePhone1") == null )
				this.ServicePhone1 = null;
			else
				this.ServicePhone1 = rs.getString("ServicePhone1").trim();

			if( rs.getString("PrintComName") == null )
				this.PrintComName = null;
			else
				this.PrintComName = rs.getString("PrintComName").trim();

			if( rs.getString("SuperComCode") == null )
				this.SuperComCode = null;
			else
				this.SuperComCode = rs.getString("SuperComCode").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("Crs_Check_Status") == null )
				this.Crs_Check_Status = null;
			else
				this.Crs_Check_Status = rs.getString("Crs_Check_Status").trim();

			if( rs.getString("ContyFlag") == null )
				this.ContyFlag = null;
			else
				this.ContyFlag = rs.getString("ContyFlag").trim();

			if( rs.getString("InteractFlag") == null )
				this.InteractFlag = null;
			else
				this.InteractFlag = rs.getString("InteractFlag").trim();

			if( rs.getString("PolicyRegistCode") == null )
				this.PolicyRegistCode = null;
			else
				this.PolicyRegistCode = rs.getString("PolicyRegistCode").trim();

			if( rs.getString("AreaCode") == null )
				this.AreaCode = null;
			else
				this.AreaCode = rs.getString("AreaCode").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LDCom表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDComSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LDComSchema getSchema()
	{
		LDComSchema aLDComSchema = new LDComSchema();
		aLDComSchema.setSchema(this);
		return aLDComSchema;
	}

	public LDComDB getDB()
	{
		LDComDB aDBOper = new LDComDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDCom描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OutComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Name)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ShortName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Address)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZipCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Phone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Fax)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EMail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WebAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SatrapName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsuMonitorCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InsureID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SignID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RegionalismCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComNature)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ValidCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Sign)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComCitySize)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServiceName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServiceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServicePhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServicePostAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServicePostZipcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LetterServiceName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LetterServiceNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LetterServicePhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LetterServicePostAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LetterServicePostZipcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComGrade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComAreaType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InnerComName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleComName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TaxRegistryNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ShowName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServicePhone2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ClaimReportPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PEOrderPhone)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BackupAddress1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BackupAddress2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BackupPhone1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BackupPhone2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EShortName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EServicePostAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ELetterServiceName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ELetterServicePostAddress)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EBackupAddress1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(EBackupAddress2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ServicePhone1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PrintComName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SuperComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_Check_Status)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ContyFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(InteractFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PolicyRegistCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AreaCode));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDCom>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			OutComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Name = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ShortName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			EMail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			WebAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			SatrapName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			InsuMonitorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			InsureID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			SignID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			RegionalismCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ComNature = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			ValidCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			Sign = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			ComCitySize = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ServiceName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			ServiceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ServicePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ServicePostAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ServicePostZipcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			LetterServiceName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			LetterServiceNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			LetterServicePhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			LetterServicePostAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			LetterServicePostZipcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			ComGrade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			ComAreaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			InnerComName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			SaleComName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			TaxRegistryNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			ShowName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			ServicePhone2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			ClaimReportPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			PEOrderPhone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			BackupAddress1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			BackupAddress2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			BackupPhone1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			BackupPhone2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42, SysConst.PACKAGESPILTER );
			EName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43, SysConst.PACKAGESPILTER );
			EShortName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44, SysConst.PACKAGESPILTER );
			EAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45, SysConst.PACKAGESPILTER );
			EServicePostAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 46, SysConst.PACKAGESPILTER );
			ELetterServiceName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 47, SysConst.PACKAGESPILTER );
			ELetterServicePostAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 48, SysConst.PACKAGESPILTER );
			EBackupAddress1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 49, SysConst.PACKAGESPILTER );
			EBackupAddress2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 50, SysConst.PACKAGESPILTER );
			ServicePhone1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 51, SysConst.PACKAGESPILTER );
			PrintComName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 52, SysConst.PACKAGESPILTER );
			SuperComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 53, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 54, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 55,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 56, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 57,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 58, SysConst.PACKAGESPILTER );
			Crs_Check_Status = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 59, SysConst.PACKAGESPILTER );
			ContyFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 60, SysConst.PACKAGESPILTER );
			InteractFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 61, SysConst.PACKAGESPILTER );
			PolicyRegistCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 62, SysConst.PACKAGESPILTER );
			AreaCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 63, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LDComSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
		}
		if (FCode.equals("OutComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OutComCode));
		}
		if (FCode.equals("Name"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Name));
		}
		if (FCode.equals("ShortName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShortName));
		}
		if (FCode.equals("Address"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
		}
		if (FCode.equals("ZipCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
		}
		if (FCode.equals("Phone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
		}
		if (FCode.equals("Fax"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
		}
		if (FCode.equals("EMail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EMail));
		}
		if (FCode.equals("WebAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WebAddress));
		}
		if (FCode.equals("SatrapName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SatrapName));
		}
		if (FCode.equals("InsuMonitorCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuMonitorCode));
		}
		if (FCode.equals("InsureID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsureID));
		}
		if (FCode.equals("SignID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SignID));
		}
		if (FCode.equals("RegionalismCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RegionalismCode));
		}
		if (FCode.equals("ComNature"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComNature));
		}
		if (FCode.equals("ValidCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ValidCode));
		}
		if (FCode.equals("Sign"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Sign));
		}
		if (FCode.equals("ComCitySize"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComCitySize));
		}
		if (FCode.equals("ServiceName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceName));
		}
		if (FCode.equals("ServiceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServiceNo));
		}
		if (FCode.equals("ServicePhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServicePhone));
		}
		if (FCode.equals("ServicePostAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServicePostAddress));
		}
		if (FCode.equals("ServicePostZipcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServicePostZipcode));
		}
		if (FCode.equals("LetterServiceName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LetterServiceName));
		}
		if (FCode.equals("LetterServiceNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LetterServiceNo));
		}
		if (FCode.equals("LetterServicePhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LetterServicePhone));
		}
		if (FCode.equals("LetterServicePostAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LetterServicePostAddress));
		}
		if (FCode.equals("LetterServicePostZipcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LetterServicePostZipcode));
		}
		if (FCode.equals("ComGrade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComGrade));
		}
		if (FCode.equals("ComAreaType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComAreaType));
		}
		if (FCode.equals("InnerComName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InnerComName));
		}
		if (FCode.equals("SaleComName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleComName));
		}
		if (FCode.equals("TaxRegistryNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TaxRegistryNo));
		}
		if (FCode.equals("ShowName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ShowName));
		}
		if (FCode.equals("ServicePhone2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServicePhone2));
		}
		if (FCode.equals("ClaimReportPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ClaimReportPhone));
		}
		if (FCode.equals("PEOrderPhone"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PEOrderPhone));
		}
		if (FCode.equals("BackupAddress1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackupAddress1));
		}
		if (FCode.equals("BackupAddress2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackupAddress2));
		}
		if (FCode.equals("BackupPhone1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackupPhone1));
		}
		if (FCode.equals("BackupPhone2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BackupPhone2));
		}
		if (FCode.equals("EName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EName));
		}
		if (FCode.equals("EShortName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EShortName));
		}
		if (FCode.equals("EAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EAddress));
		}
		if (FCode.equals("EServicePostAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EServicePostAddress));
		}
		if (FCode.equals("ELetterServiceName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ELetterServiceName));
		}
		if (FCode.equals("ELetterServicePostAddress"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ELetterServicePostAddress));
		}
		if (FCode.equals("EBackupAddress1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EBackupAddress1));
		}
		if (FCode.equals("EBackupAddress2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EBackupAddress2));
		}
		if (FCode.equals("ServicePhone1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ServicePhone1));
		}
		if (FCode.equals("PrintComName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PrintComName));
		}
		if (FCode.equals("SuperComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SuperComCode));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("Crs_Check_Status"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_Check_Status));
		}
		if (FCode.equals("ContyFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ContyFlag));
		}
		if (FCode.equals("InteractFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InteractFlag));
		}
		if (FCode.equals("PolicyRegistCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PolicyRegistCode));
		}
		if (FCode.equals("AreaCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AreaCode));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ComCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(OutComCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Name);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ShortName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Address);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(ZipCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(Phone);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Fax);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(EMail);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(WebAddress);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(SatrapName);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(InsuMonitorCode);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(InsureID);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(SignID);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(RegionalismCode);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ComNature);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(ValidCode);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(Sign);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(ComCitySize);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(ServiceName);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ServiceNo);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ServicePhone);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ServicePostAddress);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ServicePostZipcode);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(LetterServiceName);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(LetterServiceNo);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(LetterServicePhone);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(LetterServicePostAddress);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(LetterServicePostZipcode);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(ComGrade);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(ComAreaType);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(InnerComName);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(SaleComName);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(TaxRegistryNo);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(ShowName);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(ServicePhone2);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(ClaimReportPhone);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(PEOrderPhone);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(BackupAddress1);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(BackupAddress2);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(BackupPhone1);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(BackupPhone2);
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(EName);
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(EShortName);
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(EAddress);
				break;
			case 45:
				strFieldValue = StrTool.GBKToUnicode(EServicePostAddress);
				break;
			case 46:
				strFieldValue = StrTool.GBKToUnicode(ELetterServiceName);
				break;
			case 47:
				strFieldValue = StrTool.GBKToUnicode(ELetterServicePostAddress);
				break;
			case 48:
				strFieldValue = StrTool.GBKToUnicode(EBackupAddress1);
				break;
			case 49:
				strFieldValue = StrTool.GBKToUnicode(EBackupAddress2);
				break;
			case 50:
				strFieldValue = StrTool.GBKToUnicode(ServicePhone1);
				break;
			case 51:
				strFieldValue = StrTool.GBKToUnicode(PrintComName);
				break;
			case 52:
				strFieldValue = StrTool.GBKToUnicode(SuperComCode);
				break;
			case 53:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 54:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 55:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 56:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 57:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 58:
				strFieldValue = StrTool.GBKToUnicode(Crs_Check_Status);
				break;
			case 59:
				strFieldValue = StrTool.GBKToUnicode(ContyFlag);
				break;
			case 60:
				strFieldValue = StrTool.GBKToUnicode(InteractFlag);
				break;
			case 61:
				strFieldValue = StrTool.GBKToUnicode(PolicyRegistCode);
				break;
			case 62:
				strFieldValue = StrTool.GBKToUnicode(AreaCode);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComCode = FValue.trim();
			}
			else
				ComCode = null;
		}
		if (FCode.equalsIgnoreCase("OutComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OutComCode = FValue.trim();
			}
			else
				OutComCode = null;
		}
		if (FCode.equalsIgnoreCase("Name"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Name = FValue.trim();
			}
			else
				Name = null;
		}
		if (FCode.equalsIgnoreCase("ShortName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ShortName = FValue.trim();
			}
			else
				ShortName = null;
		}
		if (FCode.equalsIgnoreCase("Address"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Address = FValue.trim();
			}
			else
				Address = null;
		}
		if (FCode.equalsIgnoreCase("ZipCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZipCode = FValue.trim();
			}
			else
				ZipCode = null;
		}
		if (FCode.equalsIgnoreCase("Phone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Phone = FValue.trim();
			}
			else
				Phone = null;
		}
		if (FCode.equalsIgnoreCase("Fax"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Fax = FValue.trim();
			}
			else
				Fax = null;
		}
		if (FCode.equalsIgnoreCase("EMail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EMail = FValue.trim();
			}
			else
				EMail = null;
		}
		if (FCode.equalsIgnoreCase("WebAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WebAddress = FValue.trim();
			}
			else
				WebAddress = null;
		}
		if (FCode.equalsIgnoreCase("SatrapName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SatrapName = FValue.trim();
			}
			else
				SatrapName = null;
		}
		if (FCode.equalsIgnoreCase("InsuMonitorCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuMonitorCode = FValue.trim();
			}
			else
				InsuMonitorCode = null;
		}
		if (FCode.equalsIgnoreCase("InsureID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsureID = FValue.trim();
			}
			else
				InsureID = null;
		}
		if (FCode.equalsIgnoreCase("SignID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SignID = FValue.trim();
			}
			else
				SignID = null;
		}
		if (FCode.equalsIgnoreCase("RegionalismCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RegionalismCode = FValue.trim();
			}
			else
				RegionalismCode = null;
		}
		if (FCode.equalsIgnoreCase("ComNature"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComNature = FValue.trim();
			}
			else
				ComNature = null;
		}
		if (FCode.equalsIgnoreCase("ValidCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ValidCode = FValue.trim();
			}
			else
				ValidCode = null;
		}
		if (FCode.equalsIgnoreCase("Sign"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Sign = FValue.trim();
			}
			else
				Sign = null;
		}
		if (FCode.equalsIgnoreCase("ComCitySize"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComCitySize = FValue.trim();
			}
			else
				ComCitySize = null;
		}
		if (FCode.equalsIgnoreCase("ServiceName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServiceName = FValue.trim();
			}
			else
				ServiceName = null;
		}
		if (FCode.equalsIgnoreCase("ServiceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServiceNo = FValue.trim();
			}
			else
				ServiceNo = null;
		}
		if (FCode.equalsIgnoreCase("ServicePhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServicePhone = FValue.trim();
			}
			else
				ServicePhone = null;
		}
		if (FCode.equalsIgnoreCase("ServicePostAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServicePostAddress = FValue.trim();
			}
			else
				ServicePostAddress = null;
		}
		if (FCode.equalsIgnoreCase("ServicePostZipcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServicePostZipcode = FValue.trim();
			}
			else
				ServicePostZipcode = null;
		}
		if (FCode.equalsIgnoreCase("LetterServiceName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LetterServiceName = FValue.trim();
			}
			else
				LetterServiceName = null;
		}
		if (FCode.equalsIgnoreCase("LetterServiceNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LetterServiceNo = FValue.trim();
			}
			else
				LetterServiceNo = null;
		}
		if (FCode.equalsIgnoreCase("LetterServicePhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LetterServicePhone = FValue.trim();
			}
			else
				LetterServicePhone = null;
		}
		if (FCode.equalsIgnoreCase("LetterServicePostAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LetterServicePostAddress = FValue.trim();
			}
			else
				LetterServicePostAddress = null;
		}
		if (FCode.equalsIgnoreCase("LetterServicePostZipcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LetterServicePostZipcode = FValue.trim();
			}
			else
				LetterServicePostZipcode = null;
		}
		if (FCode.equalsIgnoreCase("ComGrade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComGrade = FValue.trim();
			}
			else
				ComGrade = null;
		}
		if (FCode.equalsIgnoreCase("ComAreaType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComAreaType = FValue.trim();
			}
			else
				ComAreaType = null;
		}
		if (FCode.equalsIgnoreCase("InnerComName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InnerComName = FValue.trim();
			}
			else
				InnerComName = null;
		}
		if (FCode.equalsIgnoreCase("SaleComName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleComName = FValue.trim();
			}
			else
				SaleComName = null;
		}
		if (FCode.equalsIgnoreCase("TaxRegistryNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TaxRegistryNo = FValue.trim();
			}
			else
				TaxRegistryNo = null;
		}
		if (FCode.equalsIgnoreCase("ShowName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ShowName = FValue.trim();
			}
			else
				ShowName = null;
		}
		if (FCode.equalsIgnoreCase("ServicePhone2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServicePhone2 = FValue.trim();
			}
			else
				ServicePhone2 = null;
		}
		if (FCode.equalsIgnoreCase("ClaimReportPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ClaimReportPhone = FValue.trim();
			}
			else
				ClaimReportPhone = null;
		}
		if (FCode.equalsIgnoreCase("PEOrderPhone"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PEOrderPhone = FValue.trim();
			}
			else
				PEOrderPhone = null;
		}
		if (FCode.equalsIgnoreCase("BackupAddress1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackupAddress1 = FValue.trim();
			}
			else
				BackupAddress1 = null;
		}
		if (FCode.equalsIgnoreCase("BackupAddress2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackupAddress2 = FValue.trim();
			}
			else
				BackupAddress2 = null;
		}
		if (FCode.equalsIgnoreCase("BackupPhone1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackupPhone1 = FValue.trim();
			}
			else
				BackupPhone1 = null;
		}
		if (FCode.equalsIgnoreCase("BackupPhone2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BackupPhone2 = FValue.trim();
			}
			else
				BackupPhone2 = null;
		}
		if (FCode.equalsIgnoreCase("EName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EName = FValue.trim();
			}
			else
				EName = null;
		}
		if (FCode.equalsIgnoreCase("EShortName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EShortName = FValue.trim();
			}
			else
				EShortName = null;
		}
		if (FCode.equalsIgnoreCase("EAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EAddress = FValue.trim();
			}
			else
				EAddress = null;
		}
		if (FCode.equalsIgnoreCase("EServicePostAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EServicePostAddress = FValue.trim();
			}
			else
				EServicePostAddress = null;
		}
		if (FCode.equalsIgnoreCase("ELetterServiceName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ELetterServiceName = FValue.trim();
			}
			else
				ELetterServiceName = null;
		}
		if (FCode.equalsIgnoreCase("ELetterServicePostAddress"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ELetterServicePostAddress = FValue.trim();
			}
			else
				ELetterServicePostAddress = null;
		}
		if (FCode.equalsIgnoreCase("EBackupAddress1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EBackupAddress1 = FValue.trim();
			}
			else
				EBackupAddress1 = null;
		}
		if (FCode.equalsIgnoreCase("EBackupAddress2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				EBackupAddress2 = FValue.trim();
			}
			else
				EBackupAddress2 = null;
		}
		if (FCode.equalsIgnoreCase("ServicePhone1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ServicePhone1 = FValue.trim();
			}
			else
				ServicePhone1 = null;
		}
		if (FCode.equalsIgnoreCase("PrintComName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PrintComName = FValue.trim();
			}
			else
				PrintComName = null;
		}
		if (FCode.equalsIgnoreCase("SuperComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SuperComCode = FValue.trim();
			}
			else
				SuperComCode = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("Crs_Check_Status"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_Check_Status = FValue.trim();
			}
			else
				Crs_Check_Status = null;
		}
		if (FCode.equalsIgnoreCase("ContyFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ContyFlag = FValue.trim();
			}
			else
				ContyFlag = null;
		}
		if (FCode.equalsIgnoreCase("InteractFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InteractFlag = FValue.trim();
			}
			else
				InteractFlag = null;
		}
		if (FCode.equalsIgnoreCase("PolicyRegistCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PolicyRegistCode = FValue.trim();
			}
			else
				PolicyRegistCode = null;
		}
		if (FCode.equalsIgnoreCase("AreaCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AreaCode = FValue.trim();
			}
			else
				AreaCode = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LDComSchema other = (LDComSchema)otherObject;
		return
			(ComCode == null ? other.getComCode() == null : ComCode.equals(other.getComCode()))
			&& (OutComCode == null ? other.getOutComCode() == null : OutComCode.equals(other.getOutComCode()))
			&& (Name == null ? other.getName() == null : Name.equals(other.getName()))
			&& (ShortName == null ? other.getShortName() == null : ShortName.equals(other.getShortName()))
			&& (Address == null ? other.getAddress() == null : Address.equals(other.getAddress()))
			&& (ZipCode == null ? other.getZipCode() == null : ZipCode.equals(other.getZipCode()))
			&& (Phone == null ? other.getPhone() == null : Phone.equals(other.getPhone()))
			&& (Fax == null ? other.getFax() == null : Fax.equals(other.getFax()))
			&& (EMail == null ? other.getEMail() == null : EMail.equals(other.getEMail()))
			&& (WebAddress == null ? other.getWebAddress() == null : WebAddress.equals(other.getWebAddress()))
			&& (SatrapName == null ? other.getSatrapName() == null : SatrapName.equals(other.getSatrapName()))
			&& (InsuMonitorCode == null ? other.getInsuMonitorCode() == null : InsuMonitorCode.equals(other.getInsuMonitorCode()))
			&& (InsureID == null ? other.getInsureID() == null : InsureID.equals(other.getInsureID()))
			&& (SignID == null ? other.getSignID() == null : SignID.equals(other.getSignID()))
			&& (RegionalismCode == null ? other.getRegionalismCode() == null : RegionalismCode.equals(other.getRegionalismCode()))
			&& (ComNature == null ? other.getComNature() == null : ComNature.equals(other.getComNature()))
			&& (ValidCode == null ? other.getValidCode() == null : ValidCode.equals(other.getValidCode()))
			&& (Sign == null ? other.getSign() == null : Sign.equals(other.getSign()))
			&& (ComCitySize == null ? other.getComCitySize() == null : ComCitySize.equals(other.getComCitySize()))
			&& (ServiceName == null ? other.getServiceName() == null : ServiceName.equals(other.getServiceName()))
			&& (ServiceNo == null ? other.getServiceNo() == null : ServiceNo.equals(other.getServiceNo()))
			&& (ServicePhone == null ? other.getServicePhone() == null : ServicePhone.equals(other.getServicePhone()))
			&& (ServicePostAddress == null ? other.getServicePostAddress() == null : ServicePostAddress.equals(other.getServicePostAddress()))
			&& (ServicePostZipcode == null ? other.getServicePostZipcode() == null : ServicePostZipcode.equals(other.getServicePostZipcode()))
			&& (LetterServiceName == null ? other.getLetterServiceName() == null : LetterServiceName.equals(other.getLetterServiceName()))
			&& (LetterServiceNo == null ? other.getLetterServiceNo() == null : LetterServiceNo.equals(other.getLetterServiceNo()))
			&& (LetterServicePhone == null ? other.getLetterServicePhone() == null : LetterServicePhone.equals(other.getLetterServicePhone()))
			&& (LetterServicePostAddress == null ? other.getLetterServicePostAddress() == null : LetterServicePostAddress.equals(other.getLetterServicePostAddress()))
			&& (LetterServicePostZipcode == null ? other.getLetterServicePostZipcode() == null : LetterServicePostZipcode.equals(other.getLetterServicePostZipcode()))
			&& (ComGrade == null ? other.getComGrade() == null : ComGrade.equals(other.getComGrade()))
			&& (ComAreaType == null ? other.getComAreaType() == null : ComAreaType.equals(other.getComAreaType()))
			&& (InnerComName == null ? other.getInnerComName() == null : InnerComName.equals(other.getInnerComName()))
			&& (SaleComName == null ? other.getSaleComName() == null : SaleComName.equals(other.getSaleComName()))
			&& (TaxRegistryNo == null ? other.getTaxRegistryNo() == null : TaxRegistryNo.equals(other.getTaxRegistryNo()))
			&& (ShowName == null ? other.getShowName() == null : ShowName.equals(other.getShowName()))
			&& (ServicePhone2 == null ? other.getServicePhone2() == null : ServicePhone2.equals(other.getServicePhone2()))
			&& (ClaimReportPhone == null ? other.getClaimReportPhone() == null : ClaimReportPhone.equals(other.getClaimReportPhone()))
			&& (PEOrderPhone == null ? other.getPEOrderPhone() == null : PEOrderPhone.equals(other.getPEOrderPhone()))
			&& (BackupAddress1 == null ? other.getBackupAddress1() == null : BackupAddress1.equals(other.getBackupAddress1()))
			&& (BackupAddress2 == null ? other.getBackupAddress2() == null : BackupAddress2.equals(other.getBackupAddress2()))
			&& (BackupPhone1 == null ? other.getBackupPhone1() == null : BackupPhone1.equals(other.getBackupPhone1()))
			&& (BackupPhone2 == null ? other.getBackupPhone2() == null : BackupPhone2.equals(other.getBackupPhone2()))
			&& (EName == null ? other.getEName() == null : EName.equals(other.getEName()))
			&& (EShortName == null ? other.getEShortName() == null : EShortName.equals(other.getEShortName()))
			&& (EAddress == null ? other.getEAddress() == null : EAddress.equals(other.getEAddress()))
			&& (EServicePostAddress == null ? other.getEServicePostAddress() == null : EServicePostAddress.equals(other.getEServicePostAddress()))
			&& (ELetterServiceName == null ? other.getELetterServiceName() == null : ELetterServiceName.equals(other.getELetterServiceName()))
			&& (ELetterServicePostAddress == null ? other.getELetterServicePostAddress() == null : ELetterServicePostAddress.equals(other.getELetterServicePostAddress()))
			&& (EBackupAddress1 == null ? other.getEBackupAddress1() == null : EBackupAddress1.equals(other.getEBackupAddress1()))
			&& (EBackupAddress2 == null ? other.getEBackupAddress2() == null : EBackupAddress2.equals(other.getEBackupAddress2()))
			&& (ServicePhone1 == null ? other.getServicePhone1() == null : ServicePhone1.equals(other.getServicePhone1()))
			&& (PrintComName == null ? other.getPrintComName() == null : PrintComName.equals(other.getPrintComName()))
			&& (SuperComCode == null ? other.getSuperComCode() == null : SuperComCode.equals(other.getSuperComCode()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (Crs_Check_Status == null ? other.getCrs_Check_Status() == null : Crs_Check_Status.equals(other.getCrs_Check_Status()))
			&& (ContyFlag == null ? other.getContyFlag() == null : ContyFlag.equals(other.getContyFlag()))
			&& (InteractFlag == null ? other.getInteractFlag() == null : InteractFlag.equals(other.getInteractFlag()))
			&& (PolicyRegistCode == null ? other.getPolicyRegistCode() == null : PolicyRegistCode.equals(other.getPolicyRegistCode()))
			&& (AreaCode == null ? other.getAreaCode() == null : AreaCode.equals(other.getAreaCode()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ComCode") ) {
			return 0;
		}
		if( strFieldName.equals("OutComCode") ) {
			return 1;
		}
		if( strFieldName.equals("Name") ) {
			return 2;
		}
		if( strFieldName.equals("ShortName") ) {
			return 3;
		}
		if( strFieldName.equals("Address") ) {
			return 4;
		}
		if( strFieldName.equals("ZipCode") ) {
			return 5;
		}
		if( strFieldName.equals("Phone") ) {
			return 6;
		}
		if( strFieldName.equals("Fax") ) {
			return 7;
		}
		if( strFieldName.equals("EMail") ) {
			return 8;
		}
		if( strFieldName.equals("WebAddress") ) {
			return 9;
		}
		if( strFieldName.equals("SatrapName") ) {
			return 10;
		}
		if( strFieldName.equals("InsuMonitorCode") ) {
			return 11;
		}
		if( strFieldName.equals("InsureID") ) {
			return 12;
		}
		if( strFieldName.equals("SignID") ) {
			return 13;
		}
		if( strFieldName.equals("RegionalismCode") ) {
			return 14;
		}
		if( strFieldName.equals("ComNature") ) {
			return 15;
		}
		if( strFieldName.equals("ValidCode") ) {
			return 16;
		}
		if( strFieldName.equals("Sign") ) {
			return 17;
		}
		if( strFieldName.equals("ComCitySize") ) {
			return 18;
		}
		if( strFieldName.equals("ServiceName") ) {
			return 19;
		}
		if( strFieldName.equals("ServiceNo") ) {
			return 20;
		}
		if( strFieldName.equals("ServicePhone") ) {
			return 21;
		}
		if( strFieldName.equals("ServicePostAddress") ) {
			return 22;
		}
		if( strFieldName.equals("ServicePostZipcode") ) {
			return 23;
		}
		if( strFieldName.equals("LetterServiceName") ) {
			return 24;
		}
		if( strFieldName.equals("LetterServiceNo") ) {
			return 25;
		}
		if( strFieldName.equals("LetterServicePhone") ) {
			return 26;
		}
		if( strFieldName.equals("LetterServicePostAddress") ) {
			return 27;
		}
		if( strFieldName.equals("LetterServicePostZipcode") ) {
			return 28;
		}
		if( strFieldName.equals("ComGrade") ) {
			return 29;
		}
		if( strFieldName.equals("ComAreaType") ) {
			return 30;
		}
		if( strFieldName.equals("InnerComName") ) {
			return 31;
		}
		if( strFieldName.equals("SaleComName") ) {
			return 32;
		}
		if( strFieldName.equals("TaxRegistryNo") ) {
			return 33;
		}
		if( strFieldName.equals("ShowName") ) {
			return 34;
		}
		if( strFieldName.equals("ServicePhone2") ) {
			return 35;
		}
		if( strFieldName.equals("ClaimReportPhone") ) {
			return 36;
		}
		if( strFieldName.equals("PEOrderPhone") ) {
			return 37;
		}
		if( strFieldName.equals("BackupAddress1") ) {
			return 38;
		}
		if( strFieldName.equals("BackupAddress2") ) {
			return 39;
		}
		if( strFieldName.equals("BackupPhone1") ) {
			return 40;
		}
		if( strFieldName.equals("BackupPhone2") ) {
			return 41;
		}
		if( strFieldName.equals("EName") ) {
			return 42;
		}
		if( strFieldName.equals("EShortName") ) {
			return 43;
		}
		if( strFieldName.equals("EAddress") ) {
			return 44;
		}
		if( strFieldName.equals("EServicePostAddress") ) {
			return 45;
		}
		if( strFieldName.equals("ELetterServiceName") ) {
			return 46;
		}
		if( strFieldName.equals("ELetterServicePostAddress") ) {
			return 47;
		}
		if( strFieldName.equals("EBackupAddress1") ) {
			return 48;
		}
		if( strFieldName.equals("EBackupAddress2") ) {
			return 49;
		}
		if( strFieldName.equals("ServicePhone1") ) {
			return 50;
		}
		if( strFieldName.equals("PrintComName") ) {
			return 51;
		}
		if( strFieldName.equals("SuperComCode") ) {
			return 52;
		}
		if( strFieldName.equals("Operator") ) {
			return 53;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 54;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 55;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 56;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 57;
		}
		if( strFieldName.equals("Crs_Check_Status") ) {
			return 58;
		}
		if( strFieldName.equals("ContyFlag") ) {
			return 59;
		}
		if( strFieldName.equals("InteractFlag") ) {
			return 60;
		}
		if( strFieldName.equals("PolicyRegistCode") ) {
			return 61;
		}
		if( strFieldName.equals("AreaCode") ) {
			return 62;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ComCode";
				break;
			case 1:
				strFieldName = "OutComCode";
				break;
			case 2:
				strFieldName = "Name";
				break;
			case 3:
				strFieldName = "ShortName";
				break;
			case 4:
				strFieldName = "Address";
				break;
			case 5:
				strFieldName = "ZipCode";
				break;
			case 6:
				strFieldName = "Phone";
				break;
			case 7:
				strFieldName = "Fax";
				break;
			case 8:
				strFieldName = "EMail";
				break;
			case 9:
				strFieldName = "WebAddress";
				break;
			case 10:
				strFieldName = "SatrapName";
				break;
			case 11:
				strFieldName = "InsuMonitorCode";
				break;
			case 12:
				strFieldName = "InsureID";
				break;
			case 13:
				strFieldName = "SignID";
				break;
			case 14:
				strFieldName = "RegionalismCode";
				break;
			case 15:
				strFieldName = "ComNature";
				break;
			case 16:
				strFieldName = "ValidCode";
				break;
			case 17:
				strFieldName = "Sign";
				break;
			case 18:
				strFieldName = "ComCitySize";
				break;
			case 19:
				strFieldName = "ServiceName";
				break;
			case 20:
				strFieldName = "ServiceNo";
				break;
			case 21:
				strFieldName = "ServicePhone";
				break;
			case 22:
				strFieldName = "ServicePostAddress";
				break;
			case 23:
				strFieldName = "ServicePostZipcode";
				break;
			case 24:
				strFieldName = "LetterServiceName";
				break;
			case 25:
				strFieldName = "LetterServiceNo";
				break;
			case 26:
				strFieldName = "LetterServicePhone";
				break;
			case 27:
				strFieldName = "LetterServicePostAddress";
				break;
			case 28:
				strFieldName = "LetterServicePostZipcode";
				break;
			case 29:
				strFieldName = "ComGrade";
				break;
			case 30:
				strFieldName = "ComAreaType";
				break;
			case 31:
				strFieldName = "InnerComName";
				break;
			case 32:
				strFieldName = "SaleComName";
				break;
			case 33:
				strFieldName = "TaxRegistryNo";
				break;
			case 34:
				strFieldName = "ShowName";
				break;
			case 35:
				strFieldName = "ServicePhone2";
				break;
			case 36:
				strFieldName = "ClaimReportPhone";
				break;
			case 37:
				strFieldName = "PEOrderPhone";
				break;
			case 38:
				strFieldName = "BackupAddress1";
				break;
			case 39:
				strFieldName = "BackupAddress2";
				break;
			case 40:
				strFieldName = "BackupPhone1";
				break;
			case 41:
				strFieldName = "BackupPhone2";
				break;
			case 42:
				strFieldName = "EName";
				break;
			case 43:
				strFieldName = "EShortName";
				break;
			case 44:
				strFieldName = "EAddress";
				break;
			case 45:
				strFieldName = "EServicePostAddress";
				break;
			case 46:
				strFieldName = "ELetterServiceName";
				break;
			case 47:
				strFieldName = "ELetterServicePostAddress";
				break;
			case 48:
				strFieldName = "EBackupAddress1";
				break;
			case 49:
				strFieldName = "EBackupAddress2";
				break;
			case 50:
				strFieldName = "ServicePhone1";
				break;
			case 51:
				strFieldName = "PrintComName";
				break;
			case 52:
				strFieldName = "SuperComCode";
				break;
			case 53:
				strFieldName = "Operator";
				break;
			case 54:
				strFieldName = "MakeDate";
				break;
			case 55:
				strFieldName = "MakeTime";
				break;
			case 56:
				strFieldName = "ModifyDate";
				break;
			case 57:
				strFieldName = "ModifyTime";
				break;
			case 58:
				strFieldName = "Crs_Check_Status";
				break;
			case 59:
				strFieldName = "ContyFlag";
				break;
			case 60:
				strFieldName = "InteractFlag";
				break;
			case 61:
				strFieldName = "PolicyRegistCode";
				break;
			case 62:
				strFieldName = "AreaCode";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OutComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Name") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShortName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Address") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZipCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Phone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Fax") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EMail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WebAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SatrapName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuMonitorCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsureID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SignID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RegionalismCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComNature") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ValidCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Sign") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComCitySize") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServiceName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServiceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServicePhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServicePostAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServicePostZipcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LetterServiceName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LetterServiceNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LetterServicePhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LetterServicePostAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LetterServicePostZipcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComGrade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComAreaType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InnerComName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleComName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TaxRegistryNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ShowName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServicePhone2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ClaimReportPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PEOrderPhone") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackupAddress1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackupAddress2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackupPhone1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BackupPhone2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EShortName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EServicePostAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ELetterServiceName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ELetterServicePostAddress") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EBackupAddress1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EBackupAddress2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ServicePhone1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PrintComName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SuperComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_Check_Status") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ContyFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InteractFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PolicyRegistCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AreaCode") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 42:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 43:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 44:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 45:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 46:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 47:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 48:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 49:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 50:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 51:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 52:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 53:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 54:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 55:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 56:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 57:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 58:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 59:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 60:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 61:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 62:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
