/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LATrainerWageIndexDB;

/*
 * <p>ClassName: LATrainerWageIndexSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险培训组训信息表
 * @CreateDate：2018-05-03
 */
public class LATrainerWageIndexSchema implements Schema, Cloneable
{
	// @Field
	/** 序号 */
	private String Idx;
	/** 指标类型 */
	private String IndexType;
	/** 薪资年月 */
	private String WageNo;
	/** 营业部 */
	private String AgentGroup;
	/** 管理机构 */
	private String ManageCom;
	/** 月新单期缴标准保费 */
	private double F01;
	/** 营业部月绩效工资 */
	private double F02;
	/** 提奖比例 */
	private double F03;
	/** 继续率 */
	private double F04;
	/** 绩优人力 */
	private double F05;
	/** 月初人力 */
	private double F06;
	/** 月末人力 */
	private double F07;
	/** 绩优达成率 */
	private double F08;
	/** 新单期缴大于0人力数 */
	private double F09;
	/** 举绩率 */
	private double F10;
	/** 考核期内平均人力数 */
	private double F11;
	/** 展业类型 */
	private String BranchType;
	/** 渠道 */
	private String BranchType2;
	/** 展业机构外部编码 */
	private String BranchAttr;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 备用佣金项01 */
	private double W01;
	/** 备用佣金项02 */
	private double W02;
	/** 备用佣金项03 */
	private double W03;
	/** 备用佣金项04 */
	private double W04;
	/** 备用佣金项05 */
	private double W05;
	/** 备用佣金项06 */
	private double W06;
	/** 备用佣金项07 */
	private double W07;
	/** 备用佣金项08 */
	private double W08;
	/** 备用佣金项09 */
	private String W09;
	/** 备用佣金项10 */
	private String W10;
	/** 备用佣金项11 */
	private String W11;
	/** 备用佣金项12 */
	private String W12;
	/** 备用佣金项13 */
	private String W13;
	/** 备用佣金项14 */
	private String W14;
	/** 备用佣金项15 */
	private String W15;
	/** 备用佣金项16 */
	private String W16;

	public static final int FIELDNUM = 40;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LATrainerWageIndexSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "Idx";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LATrainerWageIndexSchema cloned = (LATrainerWageIndexSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getIdx()
	{
		return Idx;
	}
	public void setIdx(String aIdx)
	{
		Idx = aIdx;
	}
	public String getIndexType()
	{
		return IndexType;
	}
	public void setIndexType(String aIndexType)
	{
		IndexType = aIndexType;
	}
	public String getWageNo()
	{
		return WageNo;
	}
	public void setWageNo(String aWageNo)
	{
		WageNo = aWageNo;
	}
	public String getAgentGroup()
	{
		return AgentGroup;
	}
	public void setAgentGroup(String aAgentGroup)
	{
		AgentGroup = aAgentGroup;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public double getF01()
	{
		return F01;
	}
	public void setF01(double aF01)
	{
		F01 = Arith.round(aF01,2);
	}
	public void setF01(String aF01)
	{
		if (aF01 != null && !aF01.equals(""))
		{
			Double tDouble = new Double(aF01);
			double d = tDouble.doubleValue();
                F01 = Arith.round(d,2);
		}
	}

	public double getF02()
	{
		return F02;
	}
	public void setF02(double aF02)
	{
		F02 = Arith.round(aF02,2);
	}
	public void setF02(String aF02)
	{
		if (aF02 != null && !aF02.equals(""))
		{
			Double tDouble = new Double(aF02);
			double d = tDouble.doubleValue();
                F02 = Arith.round(d,2);
		}
	}

	public double getF03()
	{
		return F03;
	}
	public void setF03(double aF03)
	{
		F03 = Arith.round(aF03,2);
	}
	public void setF03(String aF03)
	{
		if (aF03 != null && !aF03.equals(""))
		{
			Double tDouble = new Double(aF03);
			double d = tDouble.doubleValue();
                F03 = Arith.round(d,2);
		}
	}

	public double getF04()
	{
		return F04;
	}
	public void setF04(double aF04)
	{
		F04 = Arith.round(aF04,2);
	}
	public void setF04(String aF04)
	{
		if (aF04 != null && !aF04.equals(""))
		{
			Double tDouble = new Double(aF04);
			double d = tDouble.doubleValue();
                F04 = Arith.round(d,2);
		}
	}

	public double getF05()
	{
		return F05;
	}
	public void setF05(double aF05)
	{
		F05 = Arith.round(aF05,2);
	}
	public void setF05(String aF05)
	{
		if (aF05 != null && !aF05.equals(""))
		{
			Double tDouble = new Double(aF05);
			double d = tDouble.doubleValue();
                F05 = Arith.round(d,2);
		}
	}

	public double getF06()
	{
		return F06;
	}
	public void setF06(double aF06)
	{
		F06 = Arith.round(aF06,2);
	}
	public void setF06(String aF06)
	{
		if (aF06 != null && !aF06.equals(""))
		{
			Double tDouble = new Double(aF06);
			double d = tDouble.doubleValue();
                F06 = Arith.round(d,2);
		}
	}

	public double getF07()
	{
		return F07;
	}
	public void setF07(double aF07)
	{
		F07 = Arith.round(aF07,2);
	}
	public void setF07(String aF07)
	{
		if (aF07 != null && !aF07.equals(""))
		{
			Double tDouble = new Double(aF07);
			double d = tDouble.doubleValue();
                F07 = Arith.round(d,2);
		}
	}

	public double getF08()
	{
		return F08;
	}
	public void setF08(double aF08)
	{
		F08 = Arith.round(aF08,2);
	}
	public void setF08(String aF08)
	{
		if (aF08 != null && !aF08.equals(""))
		{
			Double tDouble = new Double(aF08);
			double d = tDouble.doubleValue();
                F08 = Arith.round(d,2);
		}
	}

	public double getF09()
	{
		return F09;
	}
	public void setF09(double aF09)
	{
		F09 = Arith.round(aF09,2);
	}
	public void setF09(String aF09)
	{
		if (aF09 != null && !aF09.equals(""))
		{
			Double tDouble = new Double(aF09);
			double d = tDouble.doubleValue();
                F09 = Arith.round(d,2);
		}
	}

	public double getF10()
	{
		return F10;
	}
	public void setF10(double aF10)
	{
		F10 = Arith.round(aF10,2);
	}
	public void setF10(String aF10)
	{
		if (aF10 != null && !aF10.equals(""))
		{
			Double tDouble = new Double(aF10);
			double d = tDouble.doubleValue();
                F10 = Arith.round(d,2);
		}
	}

	public double getF11()
	{
		return F11;
	}
	public void setF11(double aF11)
	{
		F11 = Arith.round(aF11,2);
	}
	public void setF11(String aF11)
	{
		if (aF11 != null && !aF11.equals(""))
		{
			Double tDouble = new Double(aF11);
			double d = tDouble.doubleValue();
                F11 = Arith.round(d,2);
		}
	}

	public String getBranchType()
	{
		return BranchType;
	}
	public void setBranchType(String aBranchType)
	{
		BranchType = aBranchType;
	}
	public String getBranchType2()
	{
		return BranchType2;
	}
	public void setBranchType2(String aBranchType2)
	{
		BranchType2 = aBranchType2;
	}
	public String getBranchAttr()
	{
		return BranchAttr;
	}
	public void setBranchAttr(String aBranchAttr)
	{
		BranchAttr = aBranchAttr;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public double getW01()
	{
		return W01;
	}
	public void setW01(double aW01)
	{
		W01 = Arith.round(aW01,2);
	}
	public void setW01(String aW01)
	{
		if (aW01 != null && !aW01.equals(""))
		{
			Double tDouble = new Double(aW01);
			double d = tDouble.doubleValue();
                W01 = Arith.round(d,2);
		}
	}

	public double getW02()
	{
		return W02;
	}
	public void setW02(double aW02)
	{
		W02 = Arith.round(aW02,2);
	}
	public void setW02(String aW02)
	{
		if (aW02 != null && !aW02.equals(""))
		{
			Double tDouble = new Double(aW02);
			double d = tDouble.doubleValue();
                W02 = Arith.round(d,2);
		}
	}

	public double getW03()
	{
		return W03;
	}
	public void setW03(double aW03)
	{
		W03 = Arith.round(aW03,2);
	}
	public void setW03(String aW03)
	{
		if (aW03 != null && !aW03.equals(""))
		{
			Double tDouble = new Double(aW03);
			double d = tDouble.doubleValue();
                W03 = Arith.round(d,2);
		}
	}

	public double getW04()
	{
		return W04;
	}
	public void setW04(double aW04)
	{
		W04 = Arith.round(aW04,2);
	}
	public void setW04(String aW04)
	{
		if (aW04 != null && !aW04.equals(""))
		{
			Double tDouble = new Double(aW04);
			double d = tDouble.doubleValue();
                W04 = Arith.round(d,2);
		}
	}

	public double getW05()
	{
		return W05;
	}
	public void setW05(double aW05)
	{
		W05 = Arith.round(aW05,2);
	}
	public void setW05(String aW05)
	{
		if (aW05 != null && !aW05.equals(""))
		{
			Double tDouble = new Double(aW05);
			double d = tDouble.doubleValue();
                W05 = Arith.round(d,2);
		}
	}

	public double getW06()
	{
		return W06;
	}
	public void setW06(double aW06)
	{
		W06 = Arith.round(aW06,2);
	}
	public void setW06(String aW06)
	{
		if (aW06 != null && !aW06.equals(""))
		{
			Double tDouble = new Double(aW06);
			double d = tDouble.doubleValue();
                W06 = Arith.round(d,2);
		}
	}

	public double getW07()
	{
		return W07;
	}
	public void setW07(double aW07)
	{
		W07 = Arith.round(aW07,2);
	}
	public void setW07(String aW07)
	{
		if (aW07 != null && !aW07.equals(""))
		{
			Double tDouble = new Double(aW07);
			double d = tDouble.doubleValue();
                W07 = Arith.round(d,2);
		}
	}

	public double getW08()
	{
		return W08;
	}
	public void setW08(double aW08)
	{
		W08 = Arith.round(aW08,2);
	}
	public void setW08(String aW08)
	{
		if (aW08 != null && !aW08.equals(""))
		{
			Double tDouble = new Double(aW08);
			double d = tDouble.doubleValue();
                W08 = Arith.round(d,2);
		}
	}

	public String getW09()
	{
		return W09;
	}
	public void setW09(String aW09)
	{
		W09 = aW09;
	}
	public String getW10()
	{
		return W10;
	}
	public void setW10(String aW10)
	{
		W10 = aW10;
	}
	public String getW11()
	{
		return W11;
	}
	public void setW11(String aW11)
	{
		W11 = aW11;
	}
	public String getW12()
	{
		return W12;
	}
	public void setW12(String aW12)
	{
		W12 = aW12;
	}
	public String getW13()
	{
		return W13;
	}
	public void setW13(String aW13)
	{
		W13 = aW13;
	}
	public String getW14()
	{
		return W14;
	}
	public void setW14(String aW14)
	{
		W14 = aW14;
	}
	public String getW15()
	{
		return W15;
	}
	public void setW15(String aW15)
	{
		W15 = aW15;
	}
	public String getW16()
	{
		return W16;
	}
	public void setW16(String aW16)
	{
		W16 = aW16;
	}

	/**
	* 使用另外一个 LATrainerWageIndexSchema 对象给 Schema 赋值
	* @param: aLATrainerWageIndexSchema LATrainerWageIndexSchema
	**/
	public void setSchema(LATrainerWageIndexSchema aLATrainerWageIndexSchema)
	{
		this.Idx = aLATrainerWageIndexSchema.getIdx();
		this.IndexType = aLATrainerWageIndexSchema.getIndexType();
		this.WageNo = aLATrainerWageIndexSchema.getWageNo();
		this.AgentGroup = aLATrainerWageIndexSchema.getAgentGroup();
		this.ManageCom = aLATrainerWageIndexSchema.getManageCom();
		this.F01 = aLATrainerWageIndexSchema.getF01();
		this.F02 = aLATrainerWageIndexSchema.getF02();
		this.F03 = aLATrainerWageIndexSchema.getF03();
		this.F04 = aLATrainerWageIndexSchema.getF04();
		this.F05 = aLATrainerWageIndexSchema.getF05();
		this.F06 = aLATrainerWageIndexSchema.getF06();
		this.F07 = aLATrainerWageIndexSchema.getF07();
		this.F08 = aLATrainerWageIndexSchema.getF08();
		this.F09 = aLATrainerWageIndexSchema.getF09();
		this.F10 = aLATrainerWageIndexSchema.getF10();
		this.F11 = aLATrainerWageIndexSchema.getF11();
		this.BranchType = aLATrainerWageIndexSchema.getBranchType();
		this.BranchType2 = aLATrainerWageIndexSchema.getBranchType2();
		this.BranchAttr = aLATrainerWageIndexSchema.getBranchAttr();
		this.Operator = aLATrainerWageIndexSchema.getOperator();
		this.MakeDate = fDate.getDate( aLATrainerWageIndexSchema.getMakeDate());
		this.MakeTime = aLATrainerWageIndexSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLATrainerWageIndexSchema.getModifyDate());
		this.ModifyTime = aLATrainerWageIndexSchema.getModifyTime();
		this.W01 = aLATrainerWageIndexSchema.getW01();
		this.W02 = aLATrainerWageIndexSchema.getW02();
		this.W03 = aLATrainerWageIndexSchema.getW03();
		this.W04 = aLATrainerWageIndexSchema.getW04();
		this.W05 = aLATrainerWageIndexSchema.getW05();
		this.W06 = aLATrainerWageIndexSchema.getW06();
		this.W07 = aLATrainerWageIndexSchema.getW07();
		this.W08 = aLATrainerWageIndexSchema.getW08();
		this.W09 = aLATrainerWageIndexSchema.getW09();
		this.W10 = aLATrainerWageIndexSchema.getW10();
		this.W11 = aLATrainerWageIndexSchema.getW11();
		this.W12 = aLATrainerWageIndexSchema.getW12();
		this.W13 = aLATrainerWageIndexSchema.getW13();
		this.W14 = aLATrainerWageIndexSchema.getW14();
		this.W15 = aLATrainerWageIndexSchema.getW15();
		this.W16 = aLATrainerWageIndexSchema.getW16();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("Idx") == null )
				this.Idx = null;
			else
				this.Idx = rs.getString("Idx").trim();

			if( rs.getString("IndexType") == null )
				this.IndexType = null;
			else
				this.IndexType = rs.getString("IndexType").trim();

			if( rs.getString("WageNo") == null )
				this.WageNo = null;
			else
				this.WageNo = rs.getString("WageNo").trim();

			if( rs.getString("AgentGroup") == null )
				this.AgentGroup = null;
			else
				this.AgentGroup = rs.getString("AgentGroup").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			this.F01 = rs.getDouble("F01");
			this.F02 = rs.getDouble("F02");
			this.F03 = rs.getDouble("F03");
			this.F04 = rs.getDouble("F04");
			this.F05 = rs.getDouble("F05");
			this.F06 = rs.getDouble("F06");
			this.F07 = rs.getDouble("F07");
			this.F08 = rs.getDouble("F08");
			this.F09 = rs.getDouble("F09");
			this.F10 = rs.getDouble("F10");
			this.F11 = rs.getDouble("F11");
			if( rs.getString("BranchType") == null )
				this.BranchType = null;
			else
				this.BranchType = rs.getString("BranchType").trim();

			if( rs.getString("BranchType2") == null )
				this.BranchType2 = null;
			else
				this.BranchType2 = rs.getString("BranchType2").trim();

			if( rs.getString("BranchAttr") == null )
				this.BranchAttr = null;
			else
				this.BranchAttr = rs.getString("BranchAttr").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			this.W01 = rs.getDouble("W01");
			this.W02 = rs.getDouble("W02");
			this.W03 = rs.getDouble("W03");
			this.W04 = rs.getDouble("W04");
			this.W05 = rs.getDouble("W05");
			this.W06 = rs.getDouble("W06");
			this.W07 = rs.getDouble("W07");
			this.W08 = rs.getDouble("W08");
			if( rs.getString("W09") == null )
				this.W09 = null;
			else
				this.W09 = rs.getString("W09").trim();

			if( rs.getString("W10") == null )
				this.W10 = null;
			else
				this.W10 = rs.getString("W10").trim();

			if( rs.getString("W11") == null )
				this.W11 = null;
			else
				this.W11 = rs.getString("W11").trim();

			if( rs.getString("W12") == null )
				this.W12 = null;
			else
				this.W12 = rs.getString("W12").trim();

			if( rs.getString("W13") == null )
				this.W13 = null;
			else
				this.W13 = rs.getString("W13").trim();

			if( rs.getString("W14") == null )
				this.W14 = null;
			else
				this.W14 = rs.getString("W14").trim();

			if( rs.getString("W15") == null )
				this.W15 = null;
			else
				this.W15 = rs.getString("W15").trim();

			if( rs.getString("W16") == null )
				this.W16 = null;
			else
				this.W16 = rs.getString("W16").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LATrainerWageIndex表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainerWageIndexSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LATrainerWageIndexSchema getSchema()
	{
		LATrainerWageIndexSchema aLATrainerWageIndexSchema = new LATrainerWageIndexSchema();
		aLATrainerWageIndexSchema.setSchema(this);
		return aLATrainerWageIndexSchema;
	}

	public LATrainerWageIndexDB getDB()
	{
		LATrainerWageIndexDB aDBOper = new LATrainerWageIndexDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainerWageIndex描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(Idx)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(IndexType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(WageNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGroup)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F01));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F02));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F03));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F04));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F05));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F06));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F07));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F08));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F09));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F10));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(F11));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchType2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BranchAttr)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W01));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W02));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W03));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W04));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W05));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W06));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W07));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(W08));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(W09)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(W10)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(W11)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(W12)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(W13)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(W14)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(W15)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(W16));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLATrainerWageIndex>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			Idx = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			IndexType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			WageNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			AgentGroup = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			F01 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			F02 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			F03 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			F04 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			F05 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			F06 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			F07 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,12,SysConst.PACKAGESPILTER))).doubleValue();
			F08 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).doubleValue();
			F09 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).doubleValue();
			F10 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,15,SysConst.PACKAGESPILTER))).doubleValue();
			F11 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,16,SysConst.PACKAGESPILTER))).doubleValue();
			BranchType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			BranchType2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			BranchAttr = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			W01 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,25,SysConst.PACKAGESPILTER))).doubleValue();
			W02 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,26,SysConst.PACKAGESPILTER))).doubleValue();
			W03 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,27,SysConst.PACKAGESPILTER))).doubleValue();
			W04 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,28,SysConst.PACKAGESPILTER))).doubleValue();
			W05 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,29,SysConst.PACKAGESPILTER))).doubleValue();
			W06 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,30,SysConst.PACKAGESPILTER))).doubleValue();
			W07 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,31,SysConst.PACKAGESPILTER))).doubleValue();
			W08 = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,32,SysConst.PACKAGESPILTER))).doubleValue();
			W09 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			W10 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			W11 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			W12 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			W13 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			W14 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			W15 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			W16 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LATrainerWageIndexSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("Idx"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Idx));
		}
		if (FCode.equals("IndexType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(IndexType));
		}
		if (FCode.equals("WageNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(WageNo));
		}
		if (FCode.equals("AgentGroup"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGroup));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("F01"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F01));
		}
		if (FCode.equals("F02"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F02));
		}
		if (FCode.equals("F03"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F03));
		}
		if (FCode.equals("F04"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F04));
		}
		if (FCode.equals("F05"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F05));
		}
		if (FCode.equals("F06"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F06));
		}
		if (FCode.equals("F07"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F07));
		}
		if (FCode.equals("F08"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F08));
		}
		if (FCode.equals("F09"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F09));
		}
		if (FCode.equals("F10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F10));
		}
		if (FCode.equals("F11"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(F11));
		}
		if (FCode.equals("BranchType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType));
		}
		if (FCode.equals("BranchType2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchType2));
		}
		if (FCode.equals("BranchAttr"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BranchAttr));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("W01"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W01));
		}
		if (FCode.equals("W02"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W02));
		}
		if (FCode.equals("W03"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W03));
		}
		if (FCode.equals("W04"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W04));
		}
		if (FCode.equals("W05"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W05));
		}
		if (FCode.equals("W06"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W06));
		}
		if (FCode.equals("W07"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W07));
		}
		if (FCode.equals("W08"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W08));
		}
		if (FCode.equals("W09"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W09));
		}
		if (FCode.equals("W10"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W10));
		}
		if (FCode.equals("W11"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W11));
		}
		if (FCode.equals("W12"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W12));
		}
		if (FCode.equals("W13"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W13));
		}
		if (FCode.equals("W14"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W14));
		}
		if (FCode.equals("W15"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W15));
		}
		if (FCode.equals("W16"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(W16));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(Idx);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(IndexType);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(WageNo);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(AgentGroup);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 5:
				strFieldValue = String.valueOf(F01);
				break;
			case 6:
				strFieldValue = String.valueOf(F02);
				break;
			case 7:
				strFieldValue = String.valueOf(F03);
				break;
			case 8:
				strFieldValue = String.valueOf(F04);
				break;
			case 9:
				strFieldValue = String.valueOf(F05);
				break;
			case 10:
				strFieldValue = String.valueOf(F06);
				break;
			case 11:
				strFieldValue = String.valueOf(F07);
				break;
			case 12:
				strFieldValue = String.valueOf(F08);
				break;
			case 13:
				strFieldValue = String.valueOf(F09);
				break;
			case 14:
				strFieldValue = String.valueOf(F10);
				break;
			case 15:
				strFieldValue = String.valueOf(F11);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(BranchType);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(BranchType2);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(BranchAttr);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 24:
				strFieldValue = String.valueOf(W01);
				break;
			case 25:
				strFieldValue = String.valueOf(W02);
				break;
			case 26:
				strFieldValue = String.valueOf(W03);
				break;
			case 27:
				strFieldValue = String.valueOf(W04);
				break;
			case 28:
				strFieldValue = String.valueOf(W05);
				break;
			case 29:
				strFieldValue = String.valueOf(W06);
				break;
			case 30:
				strFieldValue = String.valueOf(W07);
				break;
			case 31:
				strFieldValue = String.valueOf(W08);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(W09);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(W10);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(W11);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(W12);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(W13);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(W14);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(W15);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(W16);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("Idx"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Idx = FValue.trim();
			}
			else
				Idx = null;
		}
		if (FCode.equalsIgnoreCase("IndexType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				IndexType = FValue.trim();
			}
			else
				IndexType = null;
		}
		if (FCode.equalsIgnoreCase("WageNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				WageNo = FValue.trim();
			}
			else
				WageNo = null;
		}
		if (FCode.equalsIgnoreCase("AgentGroup"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGroup = FValue.trim();
			}
			else
				AgentGroup = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("F01"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F01 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F02"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F02 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F03"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F03 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F04"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F04 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F05"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F05 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F06"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F06 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F07"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F07 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F08"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F08 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F09"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F09 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F10 = d;
			}
		}
		if (FCode.equalsIgnoreCase("F11"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				F11 = d;
			}
		}
		if (FCode.equalsIgnoreCase("BranchType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType = FValue.trim();
			}
			else
				BranchType = null;
		}
		if (FCode.equalsIgnoreCase("BranchType2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchType2 = FValue.trim();
			}
			else
				BranchType2 = null;
		}
		if (FCode.equalsIgnoreCase("BranchAttr"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BranchAttr = FValue.trim();
			}
			else
				BranchAttr = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("W01"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W01 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W02"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W02 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W03"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W03 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W04"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W04 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W05"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W05 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W06"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W06 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W07"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W07 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W08"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				W08 = d;
			}
		}
		if (FCode.equalsIgnoreCase("W09"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				W09 = FValue.trim();
			}
			else
				W09 = null;
		}
		if (FCode.equalsIgnoreCase("W10"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				W10 = FValue.trim();
			}
			else
				W10 = null;
		}
		if (FCode.equalsIgnoreCase("W11"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				W11 = FValue.trim();
			}
			else
				W11 = null;
		}
		if (FCode.equalsIgnoreCase("W12"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				W12 = FValue.trim();
			}
			else
				W12 = null;
		}
		if (FCode.equalsIgnoreCase("W13"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				W13 = FValue.trim();
			}
			else
				W13 = null;
		}
		if (FCode.equalsIgnoreCase("W14"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				W14 = FValue.trim();
			}
			else
				W14 = null;
		}
		if (FCode.equalsIgnoreCase("W15"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				W15 = FValue.trim();
			}
			else
				W15 = null;
		}
		if (FCode.equalsIgnoreCase("W16"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				W16 = FValue.trim();
			}
			else
				W16 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LATrainerWageIndexSchema other = (LATrainerWageIndexSchema)otherObject;
		return
			(Idx == null ? other.getIdx() == null : Idx.equals(other.getIdx()))
			&& (IndexType == null ? other.getIndexType() == null : IndexType.equals(other.getIndexType()))
			&& (WageNo == null ? other.getWageNo() == null : WageNo.equals(other.getWageNo()))
			&& (AgentGroup == null ? other.getAgentGroup() == null : AgentGroup.equals(other.getAgentGroup()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& F01 == other.getF01()
			&& F02 == other.getF02()
			&& F03 == other.getF03()
			&& F04 == other.getF04()
			&& F05 == other.getF05()
			&& F06 == other.getF06()
			&& F07 == other.getF07()
			&& F08 == other.getF08()
			&& F09 == other.getF09()
			&& F10 == other.getF10()
			&& F11 == other.getF11()
			&& (BranchType == null ? other.getBranchType() == null : BranchType.equals(other.getBranchType()))
			&& (BranchType2 == null ? other.getBranchType2() == null : BranchType2.equals(other.getBranchType2()))
			&& (BranchAttr == null ? other.getBranchAttr() == null : BranchAttr.equals(other.getBranchAttr()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& W01 == other.getW01()
			&& W02 == other.getW02()
			&& W03 == other.getW03()
			&& W04 == other.getW04()
			&& W05 == other.getW05()
			&& W06 == other.getW06()
			&& W07 == other.getW07()
			&& W08 == other.getW08()
			&& (W09 == null ? other.getW09() == null : W09.equals(other.getW09()))
			&& (W10 == null ? other.getW10() == null : W10.equals(other.getW10()))
			&& (W11 == null ? other.getW11() == null : W11.equals(other.getW11()))
			&& (W12 == null ? other.getW12() == null : W12.equals(other.getW12()))
			&& (W13 == null ? other.getW13() == null : W13.equals(other.getW13()))
			&& (W14 == null ? other.getW14() == null : W14.equals(other.getW14()))
			&& (W15 == null ? other.getW15() == null : W15.equals(other.getW15()))
			&& (W16 == null ? other.getW16() == null : W16.equals(other.getW16()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("Idx") ) {
			return 0;
		}
		if( strFieldName.equals("IndexType") ) {
			return 1;
		}
		if( strFieldName.equals("WageNo") ) {
			return 2;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return 3;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 4;
		}
		if( strFieldName.equals("F01") ) {
			return 5;
		}
		if( strFieldName.equals("F02") ) {
			return 6;
		}
		if( strFieldName.equals("F03") ) {
			return 7;
		}
		if( strFieldName.equals("F04") ) {
			return 8;
		}
		if( strFieldName.equals("F05") ) {
			return 9;
		}
		if( strFieldName.equals("F06") ) {
			return 10;
		}
		if( strFieldName.equals("F07") ) {
			return 11;
		}
		if( strFieldName.equals("F08") ) {
			return 12;
		}
		if( strFieldName.equals("F09") ) {
			return 13;
		}
		if( strFieldName.equals("F10") ) {
			return 14;
		}
		if( strFieldName.equals("F11") ) {
			return 15;
		}
		if( strFieldName.equals("BranchType") ) {
			return 16;
		}
		if( strFieldName.equals("BranchType2") ) {
			return 17;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return 18;
		}
		if( strFieldName.equals("Operator") ) {
			return 19;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 20;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 21;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 22;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 23;
		}
		if( strFieldName.equals("W01") ) {
			return 24;
		}
		if( strFieldName.equals("W02") ) {
			return 25;
		}
		if( strFieldName.equals("W03") ) {
			return 26;
		}
		if( strFieldName.equals("W04") ) {
			return 27;
		}
		if( strFieldName.equals("W05") ) {
			return 28;
		}
		if( strFieldName.equals("W06") ) {
			return 29;
		}
		if( strFieldName.equals("W07") ) {
			return 30;
		}
		if( strFieldName.equals("W08") ) {
			return 31;
		}
		if( strFieldName.equals("W09") ) {
			return 32;
		}
		if( strFieldName.equals("W10") ) {
			return 33;
		}
		if( strFieldName.equals("W11") ) {
			return 34;
		}
		if( strFieldName.equals("W12") ) {
			return 35;
		}
		if( strFieldName.equals("W13") ) {
			return 36;
		}
		if( strFieldName.equals("W14") ) {
			return 37;
		}
		if( strFieldName.equals("W15") ) {
			return 38;
		}
		if( strFieldName.equals("W16") ) {
			return 39;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "Idx";
				break;
			case 1:
				strFieldName = "IndexType";
				break;
			case 2:
				strFieldName = "WageNo";
				break;
			case 3:
				strFieldName = "AgentGroup";
				break;
			case 4:
				strFieldName = "ManageCom";
				break;
			case 5:
				strFieldName = "F01";
				break;
			case 6:
				strFieldName = "F02";
				break;
			case 7:
				strFieldName = "F03";
				break;
			case 8:
				strFieldName = "F04";
				break;
			case 9:
				strFieldName = "F05";
				break;
			case 10:
				strFieldName = "F06";
				break;
			case 11:
				strFieldName = "F07";
				break;
			case 12:
				strFieldName = "F08";
				break;
			case 13:
				strFieldName = "F09";
				break;
			case 14:
				strFieldName = "F10";
				break;
			case 15:
				strFieldName = "F11";
				break;
			case 16:
				strFieldName = "BranchType";
				break;
			case 17:
				strFieldName = "BranchType2";
				break;
			case 18:
				strFieldName = "BranchAttr";
				break;
			case 19:
				strFieldName = "Operator";
				break;
			case 20:
				strFieldName = "MakeDate";
				break;
			case 21:
				strFieldName = "MakeTime";
				break;
			case 22:
				strFieldName = "ModifyDate";
				break;
			case 23:
				strFieldName = "ModifyTime";
				break;
			case 24:
				strFieldName = "W01";
				break;
			case 25:
				strFieldName = "W02";
				break;
			case 26:
				strFieldName = "W03";
				break;
			case 27:
				strFieldName = "W04";
				break;
			case 28:
				strFieldName = "W05";
				break;
			case 29:
				strFieldName = "W06";
				break;
			case 30:
				strFieldName = "W07";
				break;
			case 31:
				strFieldName = "W08";
				break;
			case 32:
				strFieldName = "W09";
				break;
			case 33:
				strFieldName = "W10";
				break;
			case 34:
				strFieldName = "W11";
				break;
			case 35:
				strFieldName = "W12";
				break;
			case 36:
				strFieldName = "W13";
				break;
			case 37:
				strFieldName = "W14";
				break;
			case 38:
				strFieldName = "W15";
				break;
			case 39:
				strFieldName = "W16";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("Idx") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("IndexType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("WageNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGroup") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("F01") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F02") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F03") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F04") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F05") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F06") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F07") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F08") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F09") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F10") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("F11") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("BranchType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchType2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BranchAttr") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("W01") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W02") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W03") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W04") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W05") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W06") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W07") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W08") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("W09") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("W10") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("W11") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("W12") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("W13") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("W14") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("W15") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("W16") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 12:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 14:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 15:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 25:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 26:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 27:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 28:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 29:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 30:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 31:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
