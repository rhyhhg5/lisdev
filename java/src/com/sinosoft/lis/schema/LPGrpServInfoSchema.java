/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LPGrpServInfoDB;

/*
 * <p>ClassName: LPGrpServInfoSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LCGrpServInfo
 * @CreateDate：2006-01-19
 */
public class LPGrpServInfoSchema implements Schema, Cloneable {
    // @Field
    /** 批单号 */
    private String EdorNo;
    /** 批改类型 */
    private String EdorType;
    /** 集体合同号码 */
    private String GrpContNo;
    /** 集体投保单号码 */
    private String ProposalGrpContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 服务类型 */
    private String ServKind;
    /** 服务明细 */
    private String ServDetail;
    /** 服务选择值 */
    private String ServChoose;
    /** 服务备注 */
    private String ServRemark;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LPGrpServInfoSchema() {
        mErrors = new CErrors();

        String[] pk = new String[5];
        pk[0] = "EdorNo";
        pk[1] = "EdorType";
        pk[2] = "GrpContNo";
        pk[3] = "ServKind";
        pk[4] = "ServDetail";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LPGrpServInfoSchema cloned = (LPGrpServInfoSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getEdorNo() {
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo) {
        EdorNo = aEdorNo;
    }

    public String getEdorType() {
        return EdorType;
    }

    public void setEdorType(String aEdorType) {
        EdorType = aEdorType;
    }

    public String getGrpContNo() {
        return GrpContNo;
    }

    public void setGrpContNo(String aGrpContNo) {
        GrpContNo = aGrpContNo;
    }

    public String getProposalGrpContNo() {
        return ProposalGrpContNo;
    }

    public void setProposalGrpContNo(String aProposalGrpContNo) {
        ProposalGrpContNo = aProposalGrpContNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }

    public String getServKind() {
        return ServKind;
    }

    public void setServKind(String aServKind) {
        ServKind = aServKind;
    }

    public String getServDetail() {
        return ServDetail;
    }

    public void setServDetail(String aServDetail) {
        ServDetail = aServDetail;
    }

    public String getServChoose() {
        return ServChoose;
    }

    public void setServChoose(String aServChoose) {
        ServChoose = aServChoose;
    }

    public String getServRemark() {
        return ServRemark;
    }

    public void setServRemark(String aServRemark) {
        ServRemark = aServRemark;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LPGrpServInfoSchema 对象给 Schema 赋值
     * @param: aLPGrpServInfoSchema LPGrpServInfoSchema
     **/
    public void setSchema(LPGrpServInfoSchema aLPGrpServInfoSchema) {
        this.EdorNo = aLPGrpServInfoSchema.getEdorNo();
        this.EdorType = aLPGrpServInfoSchema.getEdorType();
        this.GrpContNo = aLPGrpServInfoSchema.getGrpContNo();
        this.ProposalGrpContNo = aLPGrpServInfoSchema.getProposalGrpContNo();
        this.PrtNo = aLPGrpServInfoSchema.getPrtNo();
        this.ServKind = aLPGrpServInfoSchema.getServKind();
        this.ServDetail = aLPGrpServInfoSchema.getServDetail();
        this.ServChoose = aLPGrpServInfoSchema.getServChoose();
        this.ServRemark = aLPGrpServInfoSchema.getServRemark();
        this.MakeDate = fDate.getDate(aLPGrpServInfoSchema.getMakeDate());
        this.MakeTime = aLPGrpServInfoSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLPGrpServInfoSchema.getModifyDate());
        this.ModifyTime = aLPGrpServInfoSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null) {
                this.EdorNo = null;
            } else {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("EdorType") == null) {
                this.EdorType = null;
            } else {
                this.EdorType = rs.getString("EdorType").trim();
            }

            if (rs.getString("GrpContNo") == null) {
                this.GrpContNo = null;
            } else {
                this.GrpContNo = rs.getString("GrpContNo").trim();
            }

            if (rs.getString("ProposalGrpContNo") == null) {
                this.ProposalGrpContNo = null;
            } else {
                this.ProposalGrpContNo = rs.getString("ProposalGrpContNo").trim();
            }

            if (rs.getString("PrtNo") == null) {
                this.PrtNo = null;
            } else {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("ServKind") == null) {
                this.ServKind = null;
            } else {
                this.ServKind = rs.getString("ServKind").trim();
            }

            if (rs.getString("ServDetail") == null) {
                this.ServDetail = null;
            } else {
                this.ServDetail = rs.getString("ServDetail").trim();
            }

            if (rs.getString("ServChoose") == null) {
                this.ServChoose = null;
            } else {
                this.ServChoose = rs.getString("ServChoose").trim();
            }

            if (rs.getString("ServRemark") == null) {
                this.ServRemark = null;
            } else {
                this.ServRemark = rs.getString("ServRemark").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LPGrpServInfo表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPGrpServInfoSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LPGrpServInfoSchema getSchema() {
        LPGrpServInfoSchema aLPGrpServInfoSchema = new LPGrpServInfoSchema();
        aLPGrpServInfoSchema.setSchema(this);
        return aLPGrpServInfoSchema;
    }

    public LPGrpServInfoDB getDB() {
        LPGrpServInfoDB aDBOper = new LPGrpServInfoDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPGrpServInfo描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(EdorNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(EdorType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(GrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ProposalGrpContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServKind));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServDetail));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServChoose));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServRemark));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLPGrpServInfo>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            EdorType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            ProposalGrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                               4, SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                   SysConst.PACKAGESPILTER);
            ServKind = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            ServDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            ServChoose = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            ServRemark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                        SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LPGrpServInfoSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("EdorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorNo));
        }
        if (FCode.equals("EdorType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(EdorType));
        }
        if (FCode.equals("GrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
        }
        if (FCode.equals("ProposalGrpContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ProposalGrpContNo));
        }
        if (FCode.equals("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("ServKind")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServKind));
        }
        if (FCode.equals("ServDetail")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServDetail));
        }
        if (FCode.equals("ServChoose")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServChoose));
        }
        if (FCode.equals("ServRemark")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServRemark));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(EdorNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(EdorType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(GrpContNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ProposalGrpContNo);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(PrtNo);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ServKind);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(ServDetail);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ServChoose);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(ServRemark);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("EdorNo")) {
            if (FValue != null && !FValue.equals("")) {
                EdorNo = FValue.trim();
            } else {
                EdorNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("EdorType")) {
            if (FValue != null && !FValue.equals("")) {
                EdorType = FValue.trim();
            } else {
                EdorType = null;
            }
        }
        if (FCode.equalsIgnoreCase("GrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                GrpContNo = FValue.trim();
            } else {
                GrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ProposalGrpContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ProposalGrpContNo = FValue.trim();
            } else {
                ProposalGrpContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if (FValue != null && !FValue.equals("")) {
                PrtNo = FValue.trim();
            } else {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServKind")) {
            if (FValue != null && !FValue.equals("")) {
                ServKind = FValue.trim();
            } else {
                ServKind = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServDetail")) {
            if (FValue != null && !FValue.equals("")) {
                ServDetail = FValue.trim();
            } else {
                ServDetail = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServChoose")) {
            if (FValue != null && !FValue.equals("")) {
                ServChoose = FValue.trim();
            } else {
                ServChoose = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServRemark")) {
            if (FValue != null && !FValue.equals("")) {
                ServRemark = FValue.trim();
            } else {
                ServRemark = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LPGrpServInfoSchema other = (LPGrpServInfoSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && EdorType.equals(other.getEdorType())
                && GrpContNo.equals(other.getGrpContNo())
                && ProposalGrpContNo.equals(other.getProposalGrpContNo())
                && PrtNo.equals(other.getPrtNo())
                && ServKind.equals(other.getServKind())
                && ServDetail.equals(other.getServDetail())
                && ServChoose.equals(other.getServChoose())
                && ServRemark.equals(other.getServRemark())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("EdorNo")) {
            return 0;
        }
        if (strFieldName.equals("EdorType")) {
            return 1;
        }
        if (strFieldName.equals("GrpContNo")) {
            return 2;
        }
        if (strFieldName.equals("ProposalGrpContNo")) {
            return 3;
        }
        if (strFieldName.equals("PrtNo")) {
            return 4;
        }
        if (strFieldName.equals("ServKind")) {
            return 5;
        }
        if (strFieldName.equals("ServDetail")) {
            return 6;
        }
        if (strFieldName.equals("ServChoose")) {
            return 7;
        }
        if (strFieldName.equals("ServRemark")) {
            return 8;
        }
        if (strFieldName.equals("MakeDate")) {
            return 9;
        }
        if (strFieldName.equals("MakeTime")) {
            return 10;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 11;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "EdorNo";
            break;
        case 1:
            strFieldName = "EdorType";
            break;
        case 2:
            strFieldName = "GrpContNo";
            break;
        case 3:
            strFieldName = "ProposalGrpContNo";
            break;
        case 4:
            strFieldName = "PrtNo";
            break;
        case 5:
            strFieldName = "ServKind";
            break;
        case 6:
            strFieldName = "ServDetail";
            break;
        case 7:
            strFieldName = "ServChoose";
            break;
        case 8:
            strFieldName = "ServRemark";
            break;
        case 9:
            strFieldName = "MakeDate";
            break;
        case 10:
            strFieldName = "MakeTime";
            break;
        case 11:
            strFieldName = "ModifyDate";
            break;
        case 12:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("EdorNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("GrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ProposalGrpContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServKind")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServDetail")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServChoose")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServRemark")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
