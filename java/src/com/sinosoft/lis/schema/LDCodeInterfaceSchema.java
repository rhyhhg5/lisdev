/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDCodeInterfaceDB;

/*
 * <p>ClassName: LDCodeInterfaceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-10-17
 */
public class LDCodeInterfaceSchema implements Schema, Cloneable {
    // @Field
    /** 系统接口名称 */
    private String SysName;
    /** 编码类型 */
    private String CodeType;
    /** 类型名称 */
    private String CodeTypeNmae;
    /** 编码 */
    private String Code;
    /** 接口代码 */
    private String InterfaceCode;
    /** 编码名称 */
    private String CodeName;
    /** 编码别名 */
    private String CodeAlias;
    /** 机构代码 */
    private String ComCode;
    /** 其它标志 */
    private String OtherSign;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 14; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDCodeInterfaceSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "SysName";
        pk[1] = "CodeType";
        pk[2] = "Code";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDCodeInterfaceSchema cloned = (LDCodeInterfaceSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSysName() {
        return SysName;
    }

    public void setSysName(String aSysName) {
        SysName = aSysName;
    }

    public String getCodeType() {
        return CodeType;
    }

    public void setCodeType(String aCodeType) {
        CodeType = aCodeType;
    }

    public String getCodeTypeNmae() {
        return CodeTypeNmae;
    }

    public void setCodeTypeNmae(String aCodeTypeNmae) {
        CodeTypeNmae = aCodeTypeNmae;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String aCode) {
        Code = aCode;
    }

    public String getInterfaceCode() {
        return InterfaceCode;
    }

    public void setInterfaceCode(String aInterfaceCode) {
        InterfaceCode = aInterfaceCode;
    }

    public String getCodeName() {
        return CodeName;
    }

    public void setCodeName(String aCodeName) {
        CodeName = aCodeName;
    }

    public String getCodeAlias() {
        return CodeAlias;
    }

    public void setCodeAlias(String aCodeAlias) {
        CodeAlias = aCodeAlias;
    }

    public String getComCode() {
        return ComCode;
    }

    public void setComCode(String aComCode) {
        ComCode = aComCode;
    }

    public String getOtherSign() {
        return OtherSign;
    }

    public void setOtherSign(String aOtherSign) {
        OtherSign = aOtherSign;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LDCodeInterfaceSchema 对象给 Schema 赋值
     * @param: aLDCodeInterfaceSchema LDCodeInterfaceSchema
     **/
    public void setSchema(LDCodeInterfaceSchema aLDCodeInterfaceSchema) {
        this.SysName = aLDCodeInterfaceSchema.getSysName();
        this.CodeType = aLDCodeInterfaceSchema.getCodeType();
        this.CodeTypeNmae = aLDCodeInterfaceSchema.getCodeTypeNmae();
        this.Code = aLDCodeInterfaceSchema.getCode();
        this.InterfaceCode = aLDCodeInterfaceSchema.getInterfaceCode();
        this.CodeName = aLDCodeInterfaceSchema.getCodeName();
        this.CodeAlias = aLDCodeInterfaceSchema.getCodeAlias();
        this.ComCode = aLDCodeInterfaceSchema.getComCode();
        this.OtherSign = aLDCodeInterfaceSchema.getOtherSign();
        this.Operator = aLDCodeInterfaceSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDCodeInterfaceSchema.getMakeDate());
        this.MakeTime = aLDCodeInterfaceSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDCodeInterfaceSchema.getModifyDate());
        this.ModifyTime = aLDCodeInterfaceSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SysName") == null) {
                this.SysName = null;
            } else {
                this.SysName = rs.getString("SysName").trim();
            }

            if (rs.getString("CodeType") == null) {
                this.CodeType = null;
            } else {
                this.CodeType = rs.getString("CodeType").trim();
            }

            if (rs.getString("CodeTypeNmae") == null) {
                this.CodeTypeNmae = null;
            } else {
                this.CodeTypeNmae = rs.getString("CodeTypeNmae").trim();
            }

            if (rs.getString("Code") == null) {
                this.Code = null;
            } else {
                this.Code = rs.getString("Code").trim();
            }

            if (rs.getString("InterfaceCode") == null) {
                this.InterfaceCode = null;
            } else {
                this.InterfaceCode = rs.getString("InterfaceCode").trim();
            }

            if (rs.getString("CodeName") == null) {
                this.CodeName = null;
            } else {
                this.CodeName = rs.getString("CodeName").trim();
            }

            if (rs.getString("CodeAlias") == null) {
                this.CodeAlias = null;
            } else {
                this.CodeAlias = rs.getString("CodeAlias").trim();
            }

            if (rs.getString("ComCode") == null) {
                this.ComCode = null;
            } else {
                this.ComCode = rs.getString("ComCode").trim();
            }

            if (rs.getString("OtherSign") == null) {
                this.OtherSign = null;
            } else {
                this.OtherSign = rs.getString("OtherSign").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDCodeInterface表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDCodeInterfaceSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDCodeInterfaceSchema getSchema() {
        LDCodeInterfaceSchema aLDCodeInterfaceSchema = new
                LDCodeInterfaceSchema();
        aLDCodeInterfaceSchema.setSchema(this);
        return aLDCodeInterfaceSchema;
    }

    public LDCodeInterfaceDB getDB() {
        LDCodeInterfaceDB aDBOper = new LDCodeInterfaceDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDCodeInterface描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SysName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CodeType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CodeTypeNmae));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Code));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(InterfaceCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CodeName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CodeAlias));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(OtherSign));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDCodeInterface>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SysName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                     SysConst.PACKAGESPILTER);
            CodeType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                      SysConst.PACKAGESPILTER);
            CodeTypeNmae = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
            Code = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                  SysConst.PACKAGESPILTER);
            InterfaceCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                           SysConst.PACKAGESPILTER);
            CodeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
            CodeAlias = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            OtherSign = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDCodeInterfaceSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SysName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SysName));
        }
        if (FCode.equals("CodeType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CodeType));
        }
        if (FCode.equals("CodeTypeNmae")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CodeTypeNmae));
        }
        if (FCode.equals("Code")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Code));
        }
        if (FCode.equals("InterfaceCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(InterfaceCode));
        }
        if (FCode.equals("CodeName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CodeName));
        }
        if (FCode.equals("CodeAlias")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CodeAlias));
        }
        if (FCode.equals("ComCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
        }
        if (FCode.equals("OtherSign")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OtherSign));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SysName);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CodeType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(CodeTypeNmae);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(Code);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(InterfaceCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(CodeName);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(CodeAlias);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(ComCode);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(OtherSign);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SysName")) {
            if (FValue != null && !FValue.equals("")) {
                SysName = FValue.trim();
            } else {
                SysName = null;
            }
        }
        if (FCode.equalsIgnoreCase("CodeType")) {
            if (FValue != null && !FValue.equals("")) {
                CodeType = FValue.trim();
            } else {
                CodeType = null;
            }
        }
        if (FCode.equalsIgnoreCase("CodeTypeNmae")) {
            if (FValue != null && !FValue.equals("")) {
                CodeTypeNmae = FValue.trim();
            } else {
                CodeTypeNmae = null;
            }
        }
        if (FCode.equalsIgnoreCase("Code")) {
            if (FValue != null && !FValue.equals("")) {
                Code = FValue.trim();
            } else {
                Code = null;
            }
        }
        if (FCode.equalsIgnoreCase("InterfaceCode")) {
            if (FValue != null && !FValue.equals("")) {
                InterfaceCode = FValue.trim();
            } else {
                InterfaceCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("CodeName")) {
            if (FValue != null && !FValue.equals("")) {
                CodeName = FValue.trim();
            } else {
                CodeName = null;
            }
        }
        if (FCode.equalsIgnoreCase("CodeAlias")) {
            if (FValue != null && !FValue.equals("")) {
                CodeAlias = FValue.trim();
            } else {
                CodeAlias = null;
            }
        }
        if (FCode.equalsIgnoreCase("ComCode")) {
            if (FValue != null && !FValue.equals("")) {
                ComCode = FValue.trim();
            } else {
                ComCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("OtherSign")) {
            if (FValue != null && !FValue.equals("")) {
                OtherSign = FValue.trim();
            } else {
                OtherSign = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDCodeInterfaceSchema other = (LDCodeInterfaceSchema) otherObject;
        return
                SysName.equals(other.getSysName())
                && CodeType.equals(other.getCodeType())
                && CodeTypeNmae.equals(other.getCodeTypeNmae())
                && Code.equals(other.getCode())
                && InterfaceCode.equals(other.getInterfaceCode())
                && CodeName.equals(other.getCodeName())
                && CodeAlias.equals(other.getCodeAlias())
                && ComCode.equals(other.getComCode())
                && OtherSign.equals(other.getOtherSign())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SysName")) {
            return 0;
        }
        if (strFieldName.equals("CodeType")) {
            return 1;
        }
        if (strFieldName.equals("CodeTypeNmae")) {
            return 2;
        }
        if (strFieldName.equals("Code")) {
            return 3;
        }
        if (strFieldName.equals("InterfaceCode")) {
            return 4;
        }
        if (strFieldName.equals("CodeName")) {
            return 5;
        }
        if (strFieldName.equals("CodeAlias")) {
            return 6;
        }
        if (strFieldName.equals("ComCode")) {
            return 7;
        }
        if (strFieldName.equals("OtherSign")) {
            return 8;
        }
        if (strFieldName.equals("Operator")) {
            return 9;
        }
        if (strFieldName.equals("MakeDate")) {
            return 10;
        }
        if (strFieldName.equals("MakeTime")) {
            return 11;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 12;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 13;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SysName";
            break;
        case 1:
            strFieldName = "CodeType";
            break;
        case 2:
            strFieldName = "CodeTypeNmae";
            break;
        case 3:
            strFieldName = "Code";
            break;
        case 4:
            strFieldName = "InterfaceCode";
            break;
        case 5:
            strFieldName = "CodeName";
            break;
        case 6:
            strFieldName = "CodeAlias";
            break;
        case 7:
            strFieldName = "ComCode";
            break;
        case 8:
            strFieldName = "OtherSign";
            break;
        case 9:
            strFieldName = "Operator";
            break;
        case 10:
            strFieldName = "MakeDate";
            break;
        case 11:
            strFieldName = "MakeTime";
            break;
        case 12:
            strFieldName = "ModifyDate";
            break;
        case 13:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SysName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CodeType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CodeTypeNmae")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Code")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InterfaceCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CodeName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CodeAlias")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OtherSign")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
