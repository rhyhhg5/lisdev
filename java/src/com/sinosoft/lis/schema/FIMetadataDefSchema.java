/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIMetadataDefDB;

/*
 * <p>ClassName: FIMetadataDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIMetadataDefSchema implements Schema, Cloneable
{
	// @Field
	/** 元数据编号 */
	private String MetadataNo;
	/** 元数据名称 */
	private String MetadataName;
	/** 物理对象 */
	private String Object;
	/** 数据源 */
	private String DataSource;
	/** 元数据类型 */
	private String MDType;
	/** 生效日期 */
	private Date EffectiveDate;
	/** 失效日期 */
	private Date ExpiryDate;
	/** 状态 */
	private String State;
	/** 描述 */
	private String Remark;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;

	public static final int FIELDNUM = 12;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIMetadataDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "MetadataNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIMetadataDefSchema cloned = (FIMetadataDefSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMetadataNo()
	{
		return MetadataNo;
	}
	public void setMetadataNo(String aMetadataNo)
	{
		MetadataNo = aMetadataNo;
	}
	public String getMetadataName()
	{
		return MetadataName;
	}
	public void setMetadataName(String aMetadataName)
	{
		MetadataName = aMetadataName;
	}
	public String getObject()
	{
		return Object;
	}
	public void setObject(String aObject)
	{
		Object = aObject;
	}
	public String getDataSource()
	{
		return DataSource;
	}
	public void setDataSource(String aDataSource)
	{
		DataSource = aDataSource;
	}
	public String getMDType()
	{
		return MDType;
	}
	public void setMDType(String aMDType)
	{
		MDType = aMDType;
	}
	public String getEffectiveDate()
	{
		if( EffectiveDate != null )
			return fDate.getString(EffectiveDate);
		else
			return null;
	}
	public void setEffectiveDate(Date aEffectiveDate)
	{
		EffectiveDate = aEffectiveDate;
	}
	public void setEffectiveDate(String aEffectiveDate)
	{
		if (aEffectiveDate != null && !aEffectiveDate.equals("") )
		{
			EffectiveDate = fDate.getDate( aEffectiveDate );
		}
		else
			EffectiveDate = null;
	}

	public String getExpiryDate()
	{
		if( ExpiryDate != null )
			return fDate.getString(ExpiryDate);
		else
			return null;
	}
	public void setExpiryDate(Date aExpiryDate)
	{
		ExpiryDate = aExpiryDate;
	}
	public void setExpiryDate(String aExpiryDate)
	{
		if (aExpiryDate != null && !aExpiryDate.equals("") )
		{
			ExpiryDate = fDate.getDate( aExpiryDate );
		}
		else
			ExpiryDate = null;
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}

	/**
	* 使用另外一个 FIMetadataDefSchema 对象给 Schema 赋值
	* @param: aFIMetadataDefSchema FIMetadataDefSchema
	**/
	public void setSchema(FIMetadataDefSchema aFIMetadataDefSchema)
	{
		this.MetadataNo = aFIMetadataDefSchema.getMetadataNo();
		this.MetadataName = aFIMetadataDefSchema.getMetadataName();
		this.Object = aFIMetadataDefSchema.getObject();
		this.DataSource = aFIMetadataDefSchema.getDataSource();
		this.MDType = aFIMetadataDefSchema.getMDType();
		this.EffectiveDate = fDate.getDate( aFIMetadataDefSchema.getEffectiveDate());
		this.ExpiryDate = fDate.getDate( aFIMetadataDefSchema.getExpiryDate());
		this.State = aFIMetadataDefSchema.getState();
		this.Remark = aFIMetadataDefSchema.getRemark();
		this.Operator = aFIMetadataDefSchema.getOperator();
		this.MakeDate = fDate.getDate( aFIMetadataDefSchema.getMakeDate());
		this.MakeTime = aFIMetadataDefSchema.getMakeTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MetadataNo") == null )
				this.MetadataNo = null;
			else
				this.MetadataNo = rs.getString("MetadataNo").trim();

			if( rs.getString("MetadataName") == null )
				this.MetadataName = null;
			else
				this.MetadataName = rs.getString("MetadataName").trim();

			if( rs.getString("Object") == null )
				this.Object = null;
			else
				this.Object = rs.getString("Object").trim();

			if( rs.getString("DataSource") == null )
				this.DataSource = null;
			else
				this.DataSource = rs.getString("DataSource").trim();

			if( rs.getString("MDType") == null )
				this.MDType = null;
			else
				this.MDType = rs.getString("MDType").trim();

			this.EffectiveDate = rs.getDate("EffectiveDate");
			this.ExpiryDate = rs.getDate("ExpiryDate");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIMetadataDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIMetadataDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIMetadataDefSchema getSchema()
	{
		FIMetadataDefSchema aFIMetadataDefSchema = new FIMetadataDefSchema();
		aFIMetadataDefSchema.setSchema(this);
		return aFIMetadataDefSchema;
	}

	public FIMetadataDefDB getDB()
	{
		FIMetadataDefDB aDBOper = new FIMetadataDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIMetadataDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(MetadataNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MetadataName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Object)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DataSource)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MDType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EffectiveDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ExpiryDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIMetadataDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MetadataNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			MetadataName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Object = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DataSource = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			MDType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			EffectiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			ExpiryDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,SysConst.PACKAGESPILTER));
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIMetadataDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MetadataNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MetadataNo));
		}
		if (FCode.equals("MetadataName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MetadataName));
		}
		if (FCode.equals("Object"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Object));
		}
		if (FCode.equals("DataSource"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DataSource));
		}
		if (FCode.equals("MDType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MDType));
		}
		if (FCode.equals("EffectiveDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEffectiveDate()));
		}
		if (FCode.equals("ExpiryDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getExpiryDate()));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MetadataNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(MetadataName);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Object);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(DataSource);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(MDType);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEffectiveDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getExpiryDate()));
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MetadataNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MetadataNo = FValue.trim();
			}
			else
				MetadataNo = null;
		}
		if (FCode.equalsIgnoreCase("MetadataName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MetadataName = FValue.trim();
			}
			else
				MetadataName = null;
		}
		if (FCode.equalsIgnoreCase("Object"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Object = FValue.trim();
			}
			else
				Object = null;
		}
		if (FCode.equalsIgnoreCase("DataSource"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DataSource = FValue.trim();
			}
			else
				DataSource = null;
		}
		if (FCode.equalsIgnoreCase("MDType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MDType = FValue.trim();
			}
			else
				MDType = null;
		}
		if (FCode.equalsIgnoreCase("EffectiveDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EffectiveDate = fDate.getDate( FValue );
			}
			else
				EffectiveDate = null;
		}
		if (FCode.equalsIgnoreCase("ExpiryDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ExpiryDate = fDate.getDate( FValue );
			}
			else
				ExpiryDate = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIMetadataDefSchema other = (FIMetadataDefSchema)otherObject;
		return
			(MetadataNo == null ? other.getMetadataNo() == null : MetadataNo.equals(other.getMetadataNo()))
			&& (MetadataName == null ? other.getMetadataName() == null : MetadataName.equals(other.getMetadataName()))
			&& (Object == null ? other.getObject() == null : Object.equals(other.getObject()))
			&& (DataSource == null ? other.getDataSource() == null : DataSource.equals(other.getDataSource()))
			&& (MDType == null ? other.getMDType() == null : MDType.equals(other.getMDType()))
			&& (EffectiveDate == null ? other.getEffectiveDate() == null : fDate.getString(EffectiveDate).equals(other.getEffectiveDate()))
			&& (ExpiryDate == null ? other.getExpiryDate() == null : fDate.getString(ExpiryDate).equals(other.getExpiryDate()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MetadataNo") ) {
			return 0;
		}
		if( strFieldName.equals("MetadataName") ) {
			return 1;
		}
		if( strFieldName.equals("Object") ) {
			return 2;
		}
		if( strFieldName.equals("DataSource") ) {
			return 3;
		}
		if( strFieldName.equals("MDType") ) {
			return 4;
		}
		if( strFieldName.equals("EffectiveDate") ) {
			return 5;
		}
		if( strFieldName.equals("ExpiryDate") ) {
			return 6;
		}
		if( strFieldName.equals("State") ) {
			return 7;
		}
		if( strFieldName.equals("Remark") ) {
			return 8;
		}
		if( strFieldName.equals("Operator") ) {
			return 9;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 10;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 11;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MetadataNo";
				break;
			case 1:
				strFieldName = "MetadataName";
				break;
			case 2:
				strFieldName = "Object";
				break;
			case 3:
				strFieldName = "DataSource";
				break;
			case 4:
				strFieldName = "MDType";
				break;
			case 5:
				strFieldName = "EffectiveDate";
				break;
			case 6:
				strFieldName = "ExpiryDate";
				break;
			case 7:
				strFieldName = "State";
				break;
			case 8:
				strFieldName = "Remark";
				break;
			case 9:
				strFieldName = "Operator";
				break;
			case 10:
				strFieldName = "MakeDate";
				break;
			case 11:
				strFieldName = "MakeTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MetadataNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MetadataName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Object") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DataSource") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MDType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("EffectiveDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ExpiryDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
