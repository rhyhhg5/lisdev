/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FMBnFeeTypeDefDB;

/*
 * <p>ClassName: FMBnFeeTypeDefSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FMBnFeeTypeDefSchema implements Schema, Cloneable
{
	// @Field
	/** 维护明细号 */
	private String MaintNo;
	/** 版本编号 */
	private String VersionNo;
	/** 业务交易编号 */
	private String BusinessID;
	/** 费用编码 */
	private String CostID;
	/** 费用名称 */
	private String CostName;
	/** 费用归类 */
	private String CostType;
	/** 明细归类 */
	private String CostDetail;
	/** 报表属性 */
	private String Report;
	/** 费用描述 */
	private String Remark;

	public static final int FIELDNUM = 9;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FMBnFeeTypeDefSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "MaintNo";
		pk[1] = "VersionNo";
		pk[2] = "BusinessID";
		pk[3] = "CostID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FMBnFeeTypeDefSchema cloned = (FMBnFeeTypeDefSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getMaintNo()
	{
		return MaintNo;
	}
	public void setMaintNo(String aMaintNo)
	{
		MaintNo = aMaintNo;
	}
	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getBusinessID()
	{
		return BusinessID;
	}
	public void setBusinessID(String aBusinessID)
	{
		BusinessID = aBusinessID;
	}
	public String getCostID()
	{
		return CostID;
	}
	public void setCostID(String aCostID)
	{
		CostID = aCostID;
	}
	public String getCostName()
	{
		return CostName;
	}
	public void setCostName(String aCostName)
	{
		CostName = aCostName;
	}
	public String getCostType()
	{
		return CostType;
	}
	public void setCostType(String aCostType)
	{
		CostType = aCostType;
	}
	public String getCostDetail()
	{
		return CostDetail;
	}
	public void setCostDetail(String aCostDetail)
	{
		CostDetail = aCostDetail;
	}
	public String getReport()
	{
		return Report;
	}
	public void setReport(String aReport)
	{
		Report = aReport;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}

	/**
	* 使用另外一个 FMBnFeeTypeDefSchema 对象给 Schema 赋值
	* @param: aFMBnFeeTypeDefSchema FMBnFeeTypeDefSchema
	**/
	public void setSchema(FMBnFeeTypeDefSchema aFMBnFeeTypeDefSchema)
	{
		this.MaintNo = aFMBnFeeTypeDefSchema.getMaintNo();
		this.VersionNo = aFMBnFeeTypeDefSchema.getVersionNo();
		this.BusinessID = aFMBnFeeTypeDefSchema.getBusinessID();
		this.CostID = aFMBnFeeTypeDefSchema.getCostID();
		this.CostName = aFMBnFeeTypeDefSchema.getCostName();
		this.CostType = aFMBnFeeTypeDefSchema.getCostType();
		this.CostDetail = aFMBnFeeTypeDefSchema.getCostDetail();
		this.Report = aFMBnFeeTypeDefSchema.getReport();
		this.Remark = aFMBnFeeTypeDefSchema.getRemark();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("MaintNo") == null )
				this.MaintNo = null;
			else
				this.MaintNo = rs.getString("MaintNo").trim();

			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("BusinessID") == null )
				this.BusinessID = null;
			else
				this.BusinessID = rs.getString("BusinessID").trim();

			if( rs.getString("CostID") == null )
				this.CostID = null;
			else
				this.CostID = rs.getString("CostID").trim();

			if( rs.getString("CostName") == null )
				this.CostName = null;
			else
				this.CostName = rs.getString("CostName").trim();

			if( rs.getString("CostType") == null )
				this.CostType = null;
			else
				this.CostType = rs.getString("CostType").trim();

			if( rs.getString("CostDetail") == null )
				this.CostDetail = null;
			else
				this.CostDetail = rs.getString("CostDetail").trim();

			if( rs.getString("Report") == null )
				this.Report = null;
			else
				this.Report = rs.getString("Report").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FMBnFeeTypeDef表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FMBnFeeTypeDefSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FMBnFeeTypeDefSchema getSchema()
	{
		FMBnFeeTypeDefSchema aFMBnFeeTypeDefSchema = new FMBnFeeTypeDefSchema();
		aFMBnFeeTypeDefSchema.setSchema(this);
		return aFMBnFeeTypeDefSchema;
	}

	public FMBnFeeTypeDefDB getDB()
	{
		FMBnFeeTypeDefDB aDBOper = new FMBnFeeTypeDefDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFMBnFeeTypeDef描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(MaintNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CostDetail)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Report)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFMBnFeeTypeDef>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			MaintNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BusinessID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			CostID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			CostName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			CostType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			CostDetail = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Report = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FMBnFeeTypeDefSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("MaintNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MaintNo));
		}
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("BusinessID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessID));
		}
		if (FCode.equals("CostID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostID));
		}
		if (FCode.equals("CostName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostName));
		}
		if (FCode.equals("CostType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostType));
		}
		if (FCode.equals("CostDetail"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CostDetail));
		}
		if (FCode.equals("Report"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Report));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(MaintNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BusinessID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(CostID);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(CostName);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(CostType);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(CostDetail);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Report);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("MaintNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MaintNo = FValue.trim();
			}
			else
				MaintNo = null;
		}
		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("BusinessID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessID = FValue.trim();
			}
			else
				BusinessID = null;
		}
		if (FCode.equalsIgnoreCase("CostID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostID = FValue.trim();
			}
			else
				CostID = null;
		}
		if (FCode.equalsIgnoreCase("CostName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostName = FValue.trim();
			}
			else
				CostName = null;
		}
		if (FCode.equalsIgnoreCase("CostType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostType = FValue.trim();
			}
			else
				CostType = null;
		}
		if (FCode.equalsIgnoreCase("CostDetail"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CostDetail = FValue.trim();
			}
			else
				CostDetail = null;
		}
		if (FCode.equalsIgnoreCase("Report"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Report = FValue.trim();
			}
			else
				Report = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FMBnFeeTypeDefSchema other = (FMBnFeeTypeDefSchema)otherObject;
		return
			(MaintNo == null ? other.getMaintNo() == null : MaintNo.equals(other.getMaintNo()))
			&& (VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (BusinessID == null ? other.getBusinessID() == null : BusinessID.equals(other.getBusinessID()))
			&& (CostID == null ? other.getCostID() == null : CostID.equals(other.getCostID()))
			&& (CostName == null ? other.getCostName() == null : CostName.equals(other.getCostName()))
			&& (CostType == null ? other.getCostType() == null : CostType.equals(other.getCostType()))
			&& (CostDetail == null ? other.getCostDetail() == null : CostDetail.equals(other.getCostDetail()))
			&& (Report == null ? other.getReport() == null : Report.equals(other.getReport()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("MaintNo") ) {
			return 0;
		}
		if( strFieldName.equals("VersionNo") ) {
			return 1;
		}
		if( strFieldName.equals("BusinessID") ) {
			return 2;
		}
		if( strFieldName.equals("CostID") ) {
			return 3;
		}
		if( strFieldName.equals("CostName") ) {
			return 4;
		}
		if( strFieldName.equals("CostType") ) {
			return 5;
		}
		if( strFieldName.equals("CostDetail") ) {
			return 6;
		}
		if( strFieldName.equals("Report") ) {
			return 7;
		}
		if( strFieldName.equals("Remark") ) {
			return 8;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "MaintNo";
				break;
			case 1:
				strFieldName = "VersionNo";
				break;
			case 2:
				strFieldName = "BusinessID";
				break;
			case 3:
				strFieldName = "CostID";
				break;
			case 4:
				strFieldName = "CostName";
				break;
			case 5:
				strFieldName = "CostType";
				break;
			case 6:
				strFieldName = "CostDetail";
				break;
			case 7:
				strFieldName = "Report";
				break;
			case 8:
				strFieldName = "Remark";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("MaintNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CostDetail") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Report") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
