/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LHServTaskRelaDB;

/*
 * <p>ClassName: LHServTaskRelaSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PICCH-郭楠-表结构修改-服务实施管理-20060731
 * @CreateDate：2006-08-01
 */
public class LHServTaskRelaSchema implements Schema, Cloneable {
    // @Field
    /** 流水号 */
    private String SerialNo;
    /** 服务事件类型 */
    private String ServCaseType;
    /** 保单所属机构标识 */
    private String ComID;
    /** 服务项目代码 */
    private String ServItemCode;
    /** 服务任务代码 */
    private String ServTaskCode;
    /** 服务任务名称 */
    private String ServTaskName;
    /** 服务任务状态 */
    private String TaskState;
    /** 计划实施时间 */
    private int PlanExeTime;
    /** 服务任务说明 */
    private String TaskDes;
    /** 任务实施人员 */
    private String ExecuteOperator;
    /** 管理机构 */
    private String ManageCom;
    /** 操作员代码 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后修改日期 */
    private Date ModifyDate;
    /** 最后修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 16; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LHServTaskRelaSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SerialNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LHServTaskRelaSchema cloned = (LHServTaskRelaSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    public String getServCaseType() {
        return ServCaseType;
    }

    public void setServCaseType(String aServCaseType) {
        ServCaseType = aServCaseType;
    }

    public String getComID() {
        return ComID;
    }

    public void setComID(String aComID) {
        ComID = aComID;
    }

    public String getServItemCode() {
        return ServItemCode;
    }

    public void setServItemCode(String aServItemCode) {
        ServItemCode = aServItemCode;
    }

    public String getServTaskCode() {
        return ServTaskCode;
    }

    public void setServTaskCode(String aServTaskCode) {
        ServTaskCode = aServTaskCode;
    }

    public String getServTaskName() {
        return ServTaskName;
    }

    public void setServTaskName(String aServTaskName) {
        ServTaskName = aServTaskName;
    }

    public String getTaskState() {
        return TaskState;
    }

    public void setTaskState(String aTaskState) {
        TaskState = aTaskState;
    }

    public int getPlanExeTime() {
        return PlanExeTime;
    }

    public void setPlanExeTime(int aPlanExeTime) {
        PlanExeTime = aPlanExeTime;
    }

    public void setPlanExeTime(String aPlanExeTime) {
        if (aPlanExeTime != null && !aPlanExeTime.equals("")) {
            Integer tInteger = new Integer(aPlanExeTime);
            int i = tInteger.intValue();
            PlanExeTime = i;
        }
    }

    public String getTaskDes() {
        return TaskDes;
    }

    public void setTaskDes(String aTaskDes) {
        TaskDes = aTaskDes;
    }

    public String getExecuteOperator() {
        return ExecuteOperator;
    }

    public void setExecuteOperator(String aExecuteOperator) {
        ExecuteOperator = aExecuteOperator;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LHServTaskRelaSchema 对象给 Schema 赋值
     * @param: aLHServTaskRelaSchema LHServTaskRelaSchema
     **/
    public void setSchema(LHServTaskRelaSchema aLHServTaskRelaSchema) {
        this.SerialNo = aLHServTaskRelaSchema.getSerialNo();
        this.ServCaseType = aLHServTaskRelaSchema.getServCaseType();
        this.ComID = aLHServTaskRelaSchema.getComID();
        this.ServItemCode = aLHServTaskRelaSchema.getServItemCode();
        this.ServTaskCode = aLHServTaskRelaSchema.getServTaskCode();
        this.ServTaskName = aLHServTaskRelaSchema.getServTaskName();
        this.TaskState = aLHServTaskRelaSchema.getTaskState();
        this.PlanExeTime = aLHServTaskRelaSchema.getPlanExeTime();
        this.TaskDes = aLHServTaskRelaSchema.getTaskDes();
        this.ExecuteOperator = aLHServTaskRelaSchema.getExecuteOperator();
        this.ManageCom = aLHServTaskRelaSchema.getManageCom();
        this.Operator = aLHServTaskRelaSchema.getOperator();
        this.MakeDate = fDate.getDate(aLHServTaskRelaSchema.getMakeDate());
        this.MakeTime = aLHServTaskRelaSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLHServTaskRelaSchema.getModifyDate());
        this.ModifyTime = aLHServTaskRelaSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null) {
                this.SerialNo = null;
            } else {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("ServCaseType") == null) {
                this.ServCaseType = null;
            } else {
                this.ServCaseType = rs.getString("ServCaseType").trim();
            }

            if (rs.getString("ComID") == null) {
                this.ComID = null;
            } else {
                this.ComID = rs.getString("ComID").trim();
            }

            if (rs.getString("ServItemCode") == null) {
                this.ServItemCode = null;
            } else {
                this.ServItemCode = rs.getString("ServItemCode").trim();
            }

            if (rs.getString("ServTaskCode") == null) {
                this.ServTaskCode = null;
            } else {
                this.ServTaskCode = rs.getString("ServTaskCode").trim();
            }

            if (rs.getString("ServTaskName") == null) {
                this.ServTaskName = null;
            } else {
                this.ServTaskName = rs.getString("ServTaskName").trim();
            }

            if (rs.getString("TaskState") == null) {
                this.TaskState = null;
            } else {
                this.TaskState = rs.getString("TaskState").trim();
            }

            this.PlanExeTime = rs.getInt("PlanExeTime");
            if (rs.getString("TaskDes") == null) {
                this.TaskDes = null;
            } else {
                this.TaskDes = rs.getString("TaskDes").trim();
            }

            if (rs.getString("ExecuteOperator") == null) {
                this.ExecuteOperator = null;
            } else {
                this.ExecuteOperator = rs.getString("ExecuteOperator").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LHServTaskRela表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LHServTaskRelaSchema getSchema() {
        LHServTaskRelaSchema aLHServTaskRelaSchema = new LHServTaskRelaSchema();
        aLHServTaskRelaSchema.setSchema(this);
        return aLHServTaskRelaSchema;
    }

    public LHServTaskRelaDB getDB() {
        LHServTaskRelaDB aDBOper = new LHServTaskRelaDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServTaskRela描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServCaseType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ComID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServItemCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ServTaskName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaskState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PlanExeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(TaskDes));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ExecuteOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLHServTaskRela>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            ServCaseType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                          SysConst.PACKAGESPILTER);
            ComID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            ServItemCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            ServTaskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            ServTaskName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                          SysConst.PACKAGESPILTER);
            TaskState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            PlanExeTime = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 8, SysConst.PACKAGESPILTER))).intValue();
            TaskDes = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                     SysConst.PACKAGESPILTER);
            ExecuteOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             10, SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskRelaSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("ServCaseType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServCaseType));
        }
        if (FCode.equals("ComID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ComID));
        }
        if (FCode.equals("ServItemCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServItemCode));
        }
        if (FCode.equals("ServTaskCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskCode));
        }
        if (FCode.equals("ServTaskName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ServTaskName));
        }
        if (FCode.equals("TaskState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskState));
        }
        if (FCode.equals("PlanExeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PlanExeTime));
        }
        if (FCode.equals("TaskDes")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(TaskDes));
        }
        if (FCode.equals("ExecuteOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ExecuteOperator));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SerialNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ServCaseType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(ComID);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ServItemCode);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ServTaskCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(ServTaskName);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(TaskState);
            break;
        case 7:
            strFieldValue = String.valueOf(PlanExeTime);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(TaskDes);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(ExecuteOperator);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNo = FValue.trim();
            } else {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServCaseType")) {
            if (FValue != null && !FValue.equals("")) {
                ServCaseType = FValue.trim();
            } else {
                ServCaseType = null;
            }
        }
        if (FCode.equalsIgnoreCase("ComID")) {
            if (FValue != null && !FValue.equals("")) {
                ComID = FValue.trim();
            } else {
                ComID = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServItemCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServItemCode = FValue.trim();
            } else {
                ServItemCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskCode")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskCode = FValue.trim();
            } else {
                ServTaskCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("ServTaskName")) {
            if (FValue != null && !FValue.equals("")) {
                ServTaskName = FValue.trim();
            } else {
                ServTaskName = null;
            }
        }
        if (FCode.equalsIgnoreCase("TaskState")) {
            if (FValue != null && !FValue.equals("")) {
                TaskState = FValue.trim();
            } else {
                TaskState = null;
            }
        }
        if (FCode.equalsIgnoreCase("PlanExeTime")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PlanExeTime = i;
            }
        }
        if (FCode.equalsIgnoreCase("TaskDes")) {
            if (FValue != null && !FValue.equals("")) {
                TaskDes = FValue.trim();
            } else {
                TaskDes = null;
            }
        }
        if (FCode.equalsIgnoreCase("ExecuteOperator")) {
            if (FValue != null && !FValue.equals("")) {
                ExecuteOperator = FValue.trim();
            } else {
                ExecuteOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LHServTaskRelaSchema other = (LHServTaskRelaSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && ServCaseType.equals(other.getServCaseType())
                && ComID.equals(other.getComID())
                && ServItemCode.equals(other.getServItemCode())
                && ServTaskCode.equals(other.getServTaskCode())
                && ServTaskName.equals(other.getServTaskName())
                && TaskState.equals(other.getTaskState())
                && PlanExeTime == other.getPlanExeTime()
                && TaskDes.equals(other.getTaskDes())
                && ExecuteOperator.equals(other.getExecuteOperator())
                && ManageCom.equals(other.getManageCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return 0;
        }
        if (strFieldName.equals("ServCaseType")) {
            return 1;
        }
        if (strFieldName.equals("ComID")) {
            return 2;
        }
        if (strFieldName.equals("ServItemCode")) {
            return 3;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return 4;
        }
        if (strFieldName.equals("ServTaskName")) {
            return 5;
        }
        if (strFieldName.equals("TaskState")) {
            return 6;
        }
        if (strFieldName.equals("PlanExeTime")) {
            return 7;
        }
        if (strFieldName.equals("TaskDes")) {
            return 8;
        }
        if (strFieldName.equals("ExecuteOperator")) {
            return 9;
        }
        if (strFieldName.equals("ManageCom")) {
            return 10;
        }
        if (strFieldName.equals("Operator")) {
            return 11;
        }
        if (strFieldName.equals("MakeDate")) {
            return 12;
        }
        if (strFieldName.equals("MakeTime")) {
            return 13;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 14;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 15;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SerialNo";
            break;
        case 1:
            strFieldName = "ServCaseType";
            break;
        case 2:
            strFieldName = "ComID";
            break;
        case 3:
            strFieldName = "ServItemCode";
            break;
        case 4:
            strFieldName = "ServTaskCode";
            break;
        case 5:
            strFieldName = "ServTaskName";
            break;
        case 6:
            strFieldName = "TaskState";
            break;
        case 7:
            strFieldName = "PlanExeTime";
            break;
        case 8:
            strFieldName = "TaskDes";
            break;
        case 9:
            strFieldName = "ExecuteOperator";
            break;
        case 10:
            strFieldName = "ManageCom";
            break;
        case 11:
            strFieldName = "Operator";
            break;
        case 12:
            strFieldName = "MakeDate";
            break;
        case 13:
            strFieldName = "MakeTime";
            break;
        case 14:
            strFieldName = "ModifyDate";
            break;
        case 15:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServCaseType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ComID")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServItemCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ServTaskName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("TaskState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PlanExeTime")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("TaskDes")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ExecuteOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_INT;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
