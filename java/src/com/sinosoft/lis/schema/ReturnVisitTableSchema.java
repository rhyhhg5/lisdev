/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.ReturnVisitTableDB;

/*
 * <p>ClassName: ReturnVisitTableSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: ReturnVisitTable
 * @CreateDate：2012-10-24
 */
public class ReturnVisitTableSchema implements Schema, Cloneable
{
	// @Field
	/** 序列号 */
	private int ACTIVITYSERIALNO;
	/** 保单号 */
	private String POLICYNO;
	/** Batchserialno */
	private int BATCHSERIALNO;
	/** Executephase */
	private int EXECUTEPHASE;
	/** Lockuserid */
	private String LOCKUSERID;
	/** Locktime */
	private Date LOCKTIME;
	/** Closeflag */
	private int CLOSEFLAG;
	/** 成功日期 */
	private Date COMPLETETIME;
	/** Visituserid */
	private String VISITUSERID;
	/** Visitphase */
	private String VISITPHASE;
	/** 回访状态 */
	private String VISITPHASEMEMO;
	/** Nextcontacttime */
	private Date NEXTCONTACTTIME;
	/** Callcount */
	private int CALLCOUNT;
	/** Problemcount */
	private int PROBLEMCOUNT;
	/** Belongorg */
	private String BELONGORG;
	/** Referreuser */
	private String REFERREUSER;
	/** Feedbackuser */
	private String FEEDBACKUSER;
	/** Referretime */
	private Date REFERRETIME;
	/** Feedbacktime */
	private Date FEEDBACKTIME;
	/** Hideflg */
	private String HIDEFLG;
	/** Cancelflg */
	private String CANCELFLG;
	/** Pad1 */
	private String PAD1;
	/** Pad2 */
	private String PAD2;
	/** Pad3 */
	private String PAD3;
	/** Pad4 */
	private String PAD4;
	/** 首次回访日期 */
	private String PAD5;
	/** Pad6 */
	private String PAD6;
	/** 操作者 */
	private String OPERRATOR;
	/** 入机日期 */
	private Date MAKEDATE;
	/** 入机时间 */
	private String MAKETIME;
	/** 最后修改日期 */
	private Date MODIFYDATE;
	/** 最后修改时间 */
	private String MODIFYTIME;
	/** 回访成功结果标志 */
	private String RETURNVISITFLAG;

	public static final int FIELDNUM = 33;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public ReturnVisitTableSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "ACTIVITYSERIALNO";
		pk[1] = "POLICYNO";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		ReturnVisitTableSchema cloned = (ReturnVisitTableSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public int getACTIVITYSERIALNO()
	{
		return ACTIVITYSERIALNO;
	}
	public void setACTIVITYSERIALNO(int aACTIVITYSERIALNO)
	{
		ACTIVITYSERIALNO = aACTIVITYSERIALNO;
	}
	public void setACTIVITYSERIALNO(String aACTIVITYSERIALNO)
	{
		if (aACTIVITYSERIALNO != null && !aACTIVITYSERIALNO.equals(""))
		{
			Integer tInteger = new Integer(aACTIVITYSERIALNO);
			int i = tInteger.intValue();
			ACTIVITYSERIALNO = i;
		}
	}

	public String getPOLICYNO()
	{
		return POLICYNO;
	}
	public void setPOLICYNO(String aPOLICYNO)
	{
		POLICYNO = aPOLICYNO;
	}
	public int getBATCHSERIALNO()
	{
		return BATCHSERIALNO;
	}
	public void setBATCHSERIALNO(int aBATCHSERIALNO)
	{
		BATCHSERIALNO = aBATCHSERIALNO;
	}
	public void setBATCHSERIALNO(String aBATCHSERIALNO)
	{
		if (aBATCHSERIALNO != null && !aBATCHSERIALNO.equals(""))
		{
			Integer tInteger = new Integer(aBATCHSERIALNO);
			int i = tInteger.intValue();
			BATCHSERIALNO = i;
		}
	}

	public int getEXECUTEPHASE()
	{
		return EXECUTEPHASE;
	}
	public void setEXECUTEPHASE(int aEXECUTEPHASE)
	{
		EXECUTEPHASE = aEXECUTEPHASE;
	}
	public void setEXECUTEPHASE(String aEXECUTEPHASE)
	{
		if (aEXECUTEPHASE != null && !aEXECUTEPHASE.equals(""))
		{
			Integer tInteger = new Integer(aEXECUTEPHASE);
			int i = tInteger.intValue();
			EXECUTEPHASE = i;
		}
	}

	public String getLOCKUSERID()
	{
		return LOCKUSERID;
	}
	public void setLOCKUSERID(String aLOCKUSERID)
	{
		LOCKUSERID = aLOCKUSERID;
	}
	public String getLOCKTIME()
	{
		if( LOCKTIME != null )
			return fDate.getString(LOCKTIME);
		else
			return null;
	}
	public void setLOCKTIME(Date aLOCKTIME)
	{
		LOCKTIME = aLOCKTIME;
	}
	public void setLOCKTIME(String aLOCKTIME)
	{
		if (aLOCKTIME != null && !aLOCKTIME.equals("") )
		{
			LOCKTIME = fDate.getDate( aLOCKTIME );
		}
		else
			LOCKTIME = null;
	}

	public int getCLOSEFLAG()
	{
		return CLOSEFLAG;
	}
	public void setCLOSEFLAG(int aCLOSEFLAG)
	{
		CLOSEFLAG = aCLOSEFLAG;
	}
	public void setCLOSEFLAG(String aCLOSEFLAG)
	{
		if (aCLOSEFLAG != null && !aCLOSEFLAG.equals(""))
		{
			Integer tInteger = new Integer(aCLOSEFLAG);
			int i = tInteger.intValue();
			CLOSEFLAG = i;
		}
	}

	public String getCOMPLETETIME()
	{
		if( COMPLETETIME != null )
			return fDate.getString(COMPLETETIME);
		else
			return null;
	}
	public void setCOMPLETETIME(Date aCOMPLETETIME)
	{
		COMPLETETIME = aCOMPLETETIME;
	}
	public void setCOMPLETETIME(String aCOMPLETETIME)
	{
		if (aCOMPLETETIME != null && !aCOMPLETETIME.equals("") )
		{
			COMPLETETIME = fDate.getDate( aCOMPLETETIME );
		}
		else
			COMPLETETIME = null;
	}

	public String getVISITUSERID()
	{
		return VISITUSERID;
	}
	public void setVISITUSERID(String aVISITUSERID)
	{
		VISITUSERID = aVISITUSERID;
	}
	public String getVISITPHASE()
	{
		return VISITPHASE;
	}
	public void setVISITPHASE(String aVISITPHASE)
	{
		VISITPHASE = aVISITPHASE;
	}
	public String getVISITPHASEMEMO()
	{
		return VISITPHASEMEMO;
	}
	public void setVISITPHASEMEMO(String aVISITPHASEMEMO)
	{
		VISITPHASEMEMO = aVISITPHASEMEMO;
	}
	public String getNEXTCONTACTTIME()
	{
		if( NEXTCONTACTTIME != null )
			return fDate.getString(NEXTCONTACTTIME);
		else
			return null;
	}
	public void setNEXTCONTACTTIME(Date aNEXTCONTACTTIME)
	{
		NEXTCONTACTTIME = aNEXTCONTACTTIME;
	}
	public void setNEXTCONTACTTIME(String aNEXTCONTACTTIME)
	{
		if (aNEXTCONTACTTIME != null && !aNEXTCONTACTTIME.equals("") )
		{
			NEXTCONTACTTIME = fDate.getDate( aNEXTCONTACTTIME );
		}
		else
			NEXTCONTACTTIME = null;
	}

	public int getCALLCOUNT()
	{
		return CALLCOUNT;
	}
	public void setCALLCOUNT(int aCALLCOUNT)
	{
		CALLCOUNT = aCALLCOUNT;
	}
	public void setCALLCOUNT(String aCALLCOUNT)
	{
		if (aCALLCOUNT != null && !aCALLCOUNT.equals(""))
		{
			Integer tInteger = new Integer(aCALLCOUNT);
			int i = tInteger.intValue();
			CALLCOUNT = i;
		}
	}

	public int getPROBLEMCOUNT()
	{
		return PROBLEMCOUNT;
	}
	public void setPROBLEMCOUNT(int aPROBLEMCOUNT)
	{
		PROBLEMCOUNT = aPROBLEMCOUNT;
	}
	public void setPROBLEMCOUNT(String aPROBLEMCOUNT)
	{
		if (aPROBLEMCOUNT != null && !aPROBLEMCOUNT.equals(""))
		{
			Integer tInteger = new Integer(aPROBLEMCOUNT);
			int i = tInteger.intValue();
			PROBLEMCOUNT = i;
		}
	}

	public String getBELONGORG()
	{
		return BELONGORG;
	}
	public void setBELONGORG(String aBELONGORG)
	{
		BELONGORG = aBELONGORG;
	}
	public String getREFERREUSER()
	{
		return REFERREUSER;
	}
	public void setREFERREUSER(String aREFERREUSER)
	{
		REFERREUSER = aREFERREUSER;
	}
	public String getFEEDBACKUSER()
	{
		return FEEDBACKUSER;
	}
	public void setFEEDBACKUSER(String aFEEDBACKUSER)
	{
		FEEDBACKUSER = aFEEDBACKUSER;
	}
	public String getREFERRETIME()
	{
		if( REFERRETIME != null )
			return fDate.getString(REFERRETIME);
		else
			return null;
	}
	public void setREFERRETIME(Date aREFERRETIME)
	{
		REFERRETIME = aREFERRETIME;
	}
	public void setREFERRETIME(String aREFERRETIME)
	{
		if (aREFERRETIME != null && !aREFERRETIME.equals("") )
		{
			REFERRETIME = fDate.getDate( aREFERRETIME );
		}
		else
			REFERRETIME = null;
	}

	public String getFEEDBACKTIME()
	{
		if( FEEDBACKTIME != null )
			return fDate.getString(FEEDBACKTIME);
		else
			return null;
	}
	public void setFEEDBACKTIME(Date aFEEDBACKTIME)
	{
		FEEDBACKTIME = aFEEDBACKTIME;
	}
	public void setFEEDBACKTIME(String aFEEDBACKTIME)
	{
		if (aFEEDBACKTIME != null && !aFEEDBACKTIME.equals("") )
		{
			FEEDBACKTIME = fDate.getDate( aFEEDBACKTIME );
		}
		else
			FEEDBACKTIME = null;
	}

	public String getHIDEFLG()
	{
		return HIDEFLG;
	}
	public void setHIDEFLG(String aHIDEFLG)
	{
		HIDEFLG = aHIDEFLG;
	}
	public String getCANCELFLG()
	{
		return CANCELFLG;
	}
	public void setCANCELFLG(String aCANCELFLG)
	{
		CANCELFLG = aCANCELFLG;
	}
	public String getPAD1()
	{
		return PAD1;
	}
	public void setPAD1(String aPAD1)
	{
		PAD1 = aPAD1;
	}
	public String getPAD2()
	{
		return PAD2;
	}
	public void setPAD2(String aPAD2)
	{
		PAD2 = aPAD2;
	}
	public String getPAD3()
	{
		return PAD3;
	}
	public void setPAD3(String aPAD3)
	{
		PAD3 = aPAD3;
	}
	public String getPAD4()
	{
		return PAD4;
	}
	public void setPAD4(String aPAD4)
	{
		PAD4 = aPAD4;
	}
	public String getPAD5()
	{
		return PAD5;
	}
	public void setPAD5(String aPAD5)
	{
		PAD5 = aPAD5;
	}
	public String getPAD6()
	{
		return PAD6;
	}
	public void setPAD6(String aPAD6)
	{
		PAD6 = aPAD6;
	}
	public String getOPERRATOR()
	{
		return OPERRATOR;
	}
	public void setOPERRATOR(String aOPERRATOR)
	{
		OPERRATOR = aOPERRATOR;
	}
	public String getMAKEDATE()
	{
		if( MAKEDATE != null )
			return fDate.getString(MAKEDATE);
		else
			return null;
	}
	public void setMAKEDATE(Date aMAKEDATE)
	{
		MAKEDATE = aMAKEDATE;
	}
	public void setMAKEDATE(String aMAKEDATE)
	{
		if (aMAKEDATE != null && !aMAKEDATE.equals("") )
		{
			MAKEDATE = fDate.getDate( aMAKEDATE );
		}
		else
			MAKEDATE = null;
	}

	public String getMAKETIME()
	{
		return MAKETIME;
	}
	public void setMAKETIME(String aMAKETIME)
	{
		MAKETIME = aMAKETIME;
	}
	public String getMODIFYDATE()
	{
		if( MODIFYDATE != null )
			return fDate.getString(MODIFYDATE);
		else
			return null;
	}
	public void setMODIFYDATE(Date aMODIFYDATE)
	{
		MODIFYDATE = aMODIFYDATE;
	}
	public void setMODIFYDATE(String aMODIFYDATE)
	{
		if (aMODIFYDATE != null && !aMODIFYDATE.equals("") )
		{
			MODIFYDATE = fDate.getDate( aMODIFYDATE );
		}
		else
			MODIFYDATE = null;
	}

	public String getMODIFYTIME()
	{
		return MODIFYTIME;
	}
	public void setMODIFYTIME(String aMODIFYTIME)
	{
		MODIFYTIME = aMODIFYTIME;
	}
	public String getRETURNVISITFLAG()
	{
		return RETURNVISITFLAG;
	}
	public void setRETURNVISITFLAG(String aRETURNVISITFLAG)
	{
		RETURNVISITFLAG = aRETURNVISITFLAG;
	}

	/**
	* 使用另外一个 ReturnVisitTableSchema 对象给 Schema 赋值
	* @param: aReturnVisitTableSchema ReturnVisitTableSchema
	**/
	public void setSchema(ReturnVisitTableSchema aReturnVisitTableSchema)
	{
		this.ACTIVITYSERIALNO = aReturnVisitTableSchema.getACTIVITYSERIALNO();
		this.POLICYNO = aReturnVisitTableSchema.getPOLICYNO();
		this.BATCHSERIALNO = aReturnVisitTableSchema.getBATCHSERIALNO();
		this.EXECUTEPHASE = aReturnVisitTableSchema.getEXECUTEPHASE();
		this.LOCKUSERID = aReturnVisitTableSchema.getLOCKUSERID();
		this.LOCKTIME = fDate.getDate( aReturnVisitTableSchema.getLOCKTIME());
		this.CLOSEFLAG = aReturnVisitTableSchema.getCLOSEFLAG();
		this.COMPLETETIME = fDate.getDate( aReturnVisitTableSchema.getCOMPLETETIME());
		this.VISITUSERID = aReturnVisitTableSchema.getVISITUSERID();
		this.VISITPHASE = aReturnVisitTableSchema.getVISITPHASE();
		this.VISITPHASEMEMO = aReturnVisitTableSchema.getVISITPHASEMEMO();
		this.NEXTCONTACTTIME = fDate.getDate( aReturnVisitTableSchema.getNEXTCONTACTTIME());
		this.CALLCOUNT = aReturnVisitTableSchema.getCALLCOUNT();
		this.PROBLEMCOUNT = aReturnVisitTableSchema.getPROBLEMCOUNT();
		this.BELONGORG = aReturnVisitTableSchema.getBELONGORG();
		this.REFERREUSER = aReturnVisitTableSchema.getREFERREUSER();
		this.FEEDBACKUSER = aReturnVisitTableSchema.getFEEDBACKUSER();
		this.REFERRETIME = fDate.getDate( aReturnVisitTableSchema.getREFERRETIME());
		this.FEEDBACKTIME = fDate.getDate( aReturnVisitTableSchema.getFEEDBACKTIME());
		this.HIDEFLG = aReturnVisitTableSchema.getHIDEFLG();
		this.CANCELFLG = aReturnVisitTableSchema.getCANCELFLG();
		this.PAD1 = aReturnVisitTableSchema.getPAD1();
		this.PAD2 = aReturnVisitTableSchema.getPAD2();
		this.PAD3 = aReturnVisitTableSchema.getPAD3();
		this.PAD4 = aReturnVisitTableSchema.getPAD4();
		this.PAD5 = aReturnVisitTableSchema.getPAD5();
		this.PAD6 = aReturnVisitTableSchema.getPAD6();
		this.OPERRATOR = aReturnVisitTableSchema.getOPERRATOR();
		this.MAKEDATE = fDate.getDate( aReturnVisitTableSchema.getMAKEDATE());
		this.MAKETIME = aReturnVisitTableSchema.getMAKETIME();
		this.MODIFYDATE = fDate.getDate( aReturnVisitTableSchema.getMODIFYDATE());
		this.MODIFYTIME = aReturnVisitTableSchema.getMODIFYTIME();
		this.RETURNVISITFLAG = aReturnVisitTableSchema.getRETURNVISITFLAG();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			this.ACTIVITYSERIALNO = rs.getInt("ACTIVITYSERIALNO");
			if( rs.getString("POLICYNO") == null )
				this.POLICYNO = null;
			else
				this.POLICYNO = rs.getString("POLICYNO").trim();

			this.BATCHSERIALNO = rs.getInt("BATCHSERIALNO");
			this.EXECUTEPHASE = rs.getInt("EXECUTEPHASE");
			if( rs.getString("LOCKUSERID") == null )
				this.LOCKUSERID = null;
			else
				this.LOCKUSERID = rs.getString("LOCKUSERID").trim();

			this.LOCKTIME = rs.getDate("LOCKTIME");
			this.CLOSEFLAG = rs.getInt("CLOSEFLAG");
			this.COMPLETETIME = rs.getDate("COMPLETETIME");
			if( rs.getString("VISITUSERID") == null )
				this.VISITUSERID = null;
			else
				this.VISITUSERID = rs.getString("VISITUSERID").trim();

			if( rs.getString("VISITPHASE") == null )
				this.VISITPHASE = null;
			else
				this.VISITPHASE = rs.getString("VISITPHASE").trim();

			if( rs.getString("VISITPHASEMEMO") == null )
				this.VISITPHASEMEMO = null;
			else
				this.VISITPHASEMEMO = rs.getString("VISITPHASEMEMO").trim();

			this.NEXTCONTACTTIME = rs.getDate("NEXTCONTACTTIME");
			this.CALLCOUNT = rs.getInt("CALLCOUNT");
			this.PROBLEMCOUNT = rs.getInt("PROBLEMCOUNT");
			if( rs.getString("BELONGORG") == null )
				this.BELONGORG = null;
			else
				this.BELONGORG = rs.getString("BELONGORG").trim();

			if( rs.getString("REFERREUSER") == null )
				this.REFERREUSER = null;
			else
				this.REFERREUSER = rs.getString("REFERREUSER").trim();

			if( rs.getString("FEEDBACKUSER") == null )
				this.FEEDBACKUSER = null;
			else
				this.FEEDBACKUSER = rs.getString("FEEDBACKUSER").trim();

			this.REFERRETIME = rs.getDate("REFERRETIME");
			this.FEEDBACKTIME = rs.getDate("FEEDBACKTIME");
			if( rs.getString("HIDEFLG") == null )
				this.HIDEFLG = null;
			else
				this.HIDEFLG = rs.getString("HIDEFLG").trim();

			if( rs.getString("CANCELFLG") == null )
				this.CANCELFLG = null;
			else
				this.CANCELFLG = rs.getString("CANCELFLG").trim();

			if( rs.getString("PAD1") == null )
				this.PAD1 = null;
			else
				this.PAD1 = rs.getString("PAD1").trim();

			if( rs.getString("PAD2") == null )
				this.PAD2 = null;
			else
				this.PAD2 = rs.getString("PAD2").trim();

			if( rs.getString("PAD3") == null )
				this.PAD3 = null;
			else
				this.PAD3 = rs.getString("PAD3").trim();

			if( rs.getString("PAD4") == null )
				this.PAD4 = null;
			else
				this.PAD4 = rs.getString("PAD4").trim();

			if( rs.getString("PAD5") == null )
				this.PAD5 = null;
			else
				this.PAD5 = rs.getString("PAD5").trim();

			if( rs.getString("PAD6") == null )
				this.PAD6 = null;
			else
				this.PAD6 = rs.getString("PAD6").trim();

			if( rs.getString("OPERRATOR") == null )
				this.OPERRATOR = null;
			else
				this.OPERRATOR = rs.getString("OPERRATOR").trim();

			this.MAKEDATE = rs.getDate("MAKEDATE");
			if( rs.getString("MAKETIME") == null )
				this.MAKETIME = null;
			else
				this.MAKETIME = rs.getString("MAKETIME").trim();

			this.MODIFYDATE = rs.getDate("MODIFYDATE");
			if( rs.getString("MODIFYTIME") == null )
				this.MODIFYTIME = null;
			else
				this.MODIFYTIME = rs.getString("MODIFYTIME").trim();

			if( rs.getString("RETURNVISITFLAG") == null )
				this.RETURNVISITFLAG = null;
			else
				this.RETURNVISITFLAG = rs.getString("RETURNVISITFLAG").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的ReturnVisitTable表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ReturnVisitTableSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public ReturnVisitTableSchema getSchema()
	{
		ReturnVisitTableSchema aReturnVisitTableSchema = new ReturnVisitTableSchema();
		aReturnVisitTableSchema.setSchema(this);
		return aReturnVisitTableSchema;
	}

	public ReturnVisitTableDB getDB()
	{
		ReturnVisitTableDB aDBOper = new ReturnVisitTableDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpReturnVisitTable描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append( ChgData.chgData(ACTIVITYSERIALNO));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(POLICYNO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(BATCHSERIALNO));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(EXECUTEPHASE));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LOCKUSERID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( LOCKTIME ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CLOSEFLAG));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( COMPLETETIME ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VISITUSERID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VISITPHASE)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VISITPHASEMEMO)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( NEXTCONTACTTIME ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CALLCOUNT));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(PROBLEMCOUNT));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BELONGORG)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(REFERREUSER)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FEEDBACKUSER)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( REFERRETIME ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( FEEDBACKTIME ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(HIDEFLG)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CANCELFLG)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PAD1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PAD2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PAD3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PAD4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PAD5)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PAD6)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(OPERRATOR)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MAKEDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MAKETIME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MODIFYDATE ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MODIFYTIME)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RETURNVISITFLAG));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpReturnVisitTable>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ACTIVITYSERIALNO= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,1,SysConst.PACKAGESPILTER))).intValue();
			POLICYNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BATCHSERIALNO= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,3,SysConst.PACKAGESPILTER))).intValue();
			EXECUTEPHASE= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,4,SysConst.PACKAGESPILTER))).intValue();
			LOCKUSERID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			LOCKTIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			CLOSEFLAG= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).intValue();
			COMPLETETIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,SysConst.PACKAGESPILTER));
			VISITUSERID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			VISITPHASE = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			VISITPHASEMEMO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			NEXTCONTACTTIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			CALLCOUNT= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,13,SysConst.PACKAGESPILTER))).intValue();
			PROBLEMCOUNT= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,14,SysConst.PACKAGESPILTER))).intValue();
			BELONGORG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			REFERREUSER = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			FEEDBACKUSER = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			REFERRETIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			FEEDBACKTIME = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
			HIDEFLG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			CANCELFLG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			PAD1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			PAD2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			PAD3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			PAD4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			PAD5 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			PAD6 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			OPERRATOR = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			MAKEDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,SysConst.PACKAGESPILTER));
			MAKETIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			MODIFYDATE = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31,SysConst.PACKAGESPILTER));
			MODIFYTIME = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			RETURNVISITFLAG = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "ReturnVisitTableSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ACTIVITYSERIALNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ACTIVITYSERIALNO));
		}
		if (FCode.equals("POLICYNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(POLICYNO));
		}
		if (FCode.equals("BATCHSERIALNO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BATCHSERIALNO));
		}
		if (FCode.equals("EXECUTEPHASE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(EXECUTEPHASE));
		}
		if (FCode.equals("LOCKUSERID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LOCKUSERID));
		}
		if (FCode.equals("LOCKTIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getLOCKTIME()));
		}
		if (FCode.equals("CLOSEFLAG"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CLOSEFLAG));
		}
		if (FCode.equals("COMPLETETIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCOMPLETETIME()));
		}
		if (FCode.equals("VISITUSERID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VISITUSERID));
		}
		if (FCode.equals("VISITPHASE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VISITPHASE));
		}
		if (FCode.equals("VISITPHASEMEMO"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VISITPHASEMEMO));
		}
		if (FCode.equals("NEXTCONTACTTIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getNEXTCONTACTTIME()));
		}
		if (FCode.equals("CALLCOUNT"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CALLCOUNT));
		}
		if (FCode.equals("PROBLEMCOUNT"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PROBLEMCOUNT));
		}
		if (FCode.equals("BELONGORG"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BELONGORG));
		}
		if (FCode.equals("REFERREUSER"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(REFERREUSER));
		}
		if (FCode.equals("FEEDBACKUSER"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FEEDBACKUSER));
		}
		if (FCode.equals("REFERRETIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getREFERRETIME()));
		}
		if (FCode.equals("FEEDBACKTIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getFEEDBACKTIME()));
		}
		if (FCode.equals("HIDEFLG"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(HIDEFLG));
		}
		if (FCode.equals("CANCELFLG"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CANCELFLG));
		}
		if (FCode.equals("PAD1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PAD1));
		}
		if (FCode.equals("PAD2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PAD2));
		}
		if (FCode.equals("PAD3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PAD3));
		}
		if (FCode.equals("PAD4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PAD4));
		}
		if (FCode.equals("PAD5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PAD5));
		}
		if (FCode.equals("PAD6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PAD6));
		}
		if (FCode.equals("OPERRATOR"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(OPERRATOR));
		}
		if (FCode.equals("MAKEDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
		}
		if (FCode.equals("MAKETIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MAKETIME));
		}
		if (FCode.equals("MODIFYDATE"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
		}
		if (FCode.equals("MODIFYTIME"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MODIFYTIME));
		}
		if (FCode.equals("RETURNVISITFLAG"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RETURNVISITFLAG));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = String.valueOf(ACTIVITYSERIALNO);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(POLICYNO);
				break;
			case 2:
				strFieldValue = String.valueOf(BATCHSERIALNO);
				break;
			case 3:
				strFieldValue = String.valueOf(EXECUTEPHASE);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(LOCKUSERID);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getLOCKTIME()));
				break;
			case 6:
				strFieldValue = String.valueOf(CLOSEFLAG);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCOMPLETETIME()));
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(VISITUSERID);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(VISITPHASE);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(VISITPHASEMEMO);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getNEXTCONTACTTIME()));
				break;
			case 12:
				strFieldValue = String.valueOf(CALLCOUNT);
				break;
			case 13:
				strFieldValue = String.valueOf(PROBLEMCOUNT);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(BELONGORG);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(REFERREUSER);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(FEEDBACKUSER);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getREFERRETIME()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getFEEDBACKTIME()));
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(HIDEFLG);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(CANCELFLG);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(PAD1);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(PAD2);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(PAD3);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(PAD4);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(PAD5);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(PAD6);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(OPERRATOR);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMAKEDATE()));
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(MAKETIME);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMODIFYDATE()));
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(MODIFYTIME);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(RETURNVISITFLAG);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ACTIVITYSERIALNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				ACTIVITYSERIALNO = i;
			}
		}
		if (FCode.equalsIgnoreCase("POLICYNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				POLICYNO = FValue.trim();
			}
			else
				POLICYNO = null;
		}
		if (FCode.equalsIgnoreCase("BATCHSERIALNO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				BATCHSERIALNO = i;
			}
		}
		if (FCode.equalsIgnoreCase("EXECUTEPHASE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				EXECUTEPHASE = i;
			}
		}
		if (FCode.equalsIgnoreCase("LOCKUSERID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LOCKUSERID = FValue.trim();
			}
			else
				LOCKUSERID = null;
		}
		if (FCode.equalsIgnoreCase("LOCKTIME"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				LOCKTIME = fDate.getDate( FValue );
			}
			else
				LOCKTIME = null;
		}
		if (FCode.equalsIgnoreCase("CLOSEFLAG"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				CLOSEFLAG = i;
			}
		}
		if (FCode.equalsIgnoreCase("COMPLETETIME"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				COMPLETETIME = fDate.getDate( FValue );
			}
			else
				COMPLETETIME = null;
		}
		if (FCode.equalsIgnoreCase("VISITUSERID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VISITUSERID = FValue.trim();
			}
			else
				VISITUSERID = null;
		}
		if (FCode.equalsIgnoreCase("VISITPHASE"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VISITPHASE = FValue.trim();
			}
			else
				VISITPHASE = null;
		}
		if (FCode.equalsIgnoreCase("VISITPHASEMEMO"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VISITPHASEMEMO = FValue.trim();
			}
			else
				VISITPHASEMEMO = null;
		}
		if (FCode.equalsIgnoreCase("NEXTCONTACTTIME"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				NEXTCONTACTTIME = fDate.getDate( FValue );
			}
			else
				NEXTCONTACTTIME = null;
		}
		if (FCode.equalsIgnoreCase("CALLCOUNT"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				CALLCOUNT = i;
			}
		}
		if (FCode.equalsIgnoreCase("PROBLEMCOUNT"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				PROBLEMCOUNT = i;
			}
		}
		if (FCode.equalsIgnoreCase("BELONGORG"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BELONGORG = FValue.trim();
			}
			else
				BELONGORG = null;
		}
		if (FCode.equalsIgnoreCase("REFERREUSER"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				REFERREUSER = FValue.trim();
			}
			else
				REFERREUSER = null;
		}
		if (FCode.equalsIgnoreCase("FEEDBACKUSER"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FEEDBACKUSER = FValue.trim();
			}
			else
				FEEDBACKUSER = null;
		}
		if (FCode.equalsIgnoreCase("REFERRETIME"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				REFERRETIME = fDate.getDate( FValue );
			}
			else
				REFERRETIME = null;
		}
		if (FCode.equalsIgnoreCase("FEEDBACKTIME"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				FEEDBACKTIME = fDate.getDate( FValue );
			}
			else
				FEEDBACKTIME = null;
		}
		if (FCode.equalsIgnoreCase("HIDEFLG"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				HIDEFLG = FValue.trim();
			}
			else
				HIDEFLG = null;
		}
		if (FCode.equalsIgnoreCase("CANCELFLG"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CANCELFLG = FValue.trim();
			}
			else
				CANCELFLG = null;
		}
		if (FCode.equalsIgnoreCase("PAD1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PAD1 = FValue.trim();
			}
			else
				PAD1 = null;
		}
		if (FCode.equalsIgnoreCase("PAD2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PAD2 = FValue.trim();
			}
			else
				PAD2 = null;
		}
		if (FCode.equalsIgnoreCase("PAD3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PAD3 = FValue.trim();
			}
			else
				PAD3 = null;
		}
		if (FCode.equalsIgnoreCase("PAD4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PAD4 = FValue.trim();
			}
			else
				PAD4 = null;
		}
		if (FCode.equalsIgnoreCase("PAD5"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PAD5 = FValue.trim();
			}
			else
				PAD5 = null;
		}
		if (FCode.equalsIgnoreCase("PAD6"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PAD6 = FValue.trim();
			}
			else
				PAD6 = null;
		}
		if (FCode.equalsIgnoreCase("OPERRATOR"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				OPERRATOR = FValue.trim();
			}
			else
				OPERRATOR = null;
		}
		if (FCode.equalsIgnoreCase("MAKEDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MAKEDATE = fDate.getDate( FValue );
			}
			else
				MAKEDATE = null;
		}
		if (FCode.equalsIgnoreCase("MAKETIME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MAKETIME = FValue.trim();
			}
			else
				MAKETIME = null;
		}
		if (FCode.equalsIgnoreCase("MODIFYDATE"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MODIFYDATE = fDate.getDate( FValue );
			}
			else
				MODIFYDATE = null;
		}
		if (FCode.equalsIgnoreCase("MODIFYTIME"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MODIFYTIME = FValue.trim();
			}
			else
				MODIFYTIME = null;
		}
		if (FCode.equalsIgnoreCase("RETURNVISITFLAG"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RETURNVISITFLAG = FValue.trim();
			}
			else
				RETURNVISITFLAG = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		ReturnVisitTableSchema other = (ReturnVisitTableSchema)otherObject;
		return
			ACTIVITYSERIALNO == other.getACTIVITYSERIALNO()
			&& (POLICYNO == null ? other.getPOLICYNO() == null : POLICYNO.equals(other.getPOLICYNO()))
			&& BATCHSERIALNO == other.getBATCHSERIALNO()
			&& EXECUTEPHASE == other.getEXECUTEPHASE()
			&& (LOCKUSERID == null ? other.getLOCKUSERID() == null : LOCKUSERID.equals(other.getLOCKUSERID()))
			&& (LOCKTIME == null ? other.getLOCKTIME() == null : fDate.getString(LOCKTIME).equals(other.getLOCKTIME()))
			&& CLOSEFLAG == other.getCLOSEFLAG()
			&& (COMPLETETIME == null ? other.getCOMPLETETIME() == null : fDate.getString(COMPLETETIME).equals(other.getCOMPLETETIME()))
			&& (VISITUSERID == null ? other.getVISITUSERID() == null : VISITUSERID.equals(other.getVISITUSERID()))
			&& (VISITPHASE == null ? other.getVISITPHASE() == null : VISITPHASE.equals(other.getVISITPHASE()))
			&& (VISITPHASEMEMO == null ? other.getVISITPHASEMEMO() == null : VISITPHASEMEMO.equals(other.getVISITPHASEMEMO()))
			&& (NEXTCONTACTTIME == null ? other.getNEXTCONTACTTIME() == null : fDate.getString(NEXTCONTACTTIME).equals(other.getNEXTCONTACTTIME()))
			&& CALLCOUNT == other.getCALLCOUNT()
			&& PROBLEMCOUNT == other.getPROBLEMCOUNT()
			&& (BELONGORG == null ? other.getBELONGORG() == null : BELONGORG.equals(other.getBELONGORG()))
			&& (REFERREUSER == null ? other.getREFERREUSER() == null : REFERREUSER.equals(other.getREFERREUSER()))
			&& (FEEDBACKUSER == null ? other.getFEEDBACKUSER() == null : FEEDBACKUSER.equals(other.getFEEDBACKUSER()))
			&& (REFERRETIME == null ? other.getREFERRETIME() == null : fDate.getString(REFERRETIME).equals(other.getREFERRETIME()))
			&& (FEEDBACKTIME == null ? other.getFEEDBACKTIME() == null : fDate.getString(FEEDBACKTIME).equals(other.getFEEDBACKTIME()))
			&& (HIDEFLG == null ? other.getHIDEFLG() == null : HIDEFLG.equals(other.getHIDEFLG()))
			&& (CANCELFLG == null ? other.getCANCELFLG() == null : CANCELFLG.equals(other.getCANCELFLG()))
			&& (PAD1 == null ? other.getPAD1() == null : PAD1.equals(other.getPAD1()))
			&& (PAD2 == null ? other.getPAD2() == null : PAD2.equals(other.getPAD2()))
			&& (PAD3 == null ? other.getPAD3() == null : PAD3.equals(other.getPAD3()))
			&& (PAD4 == null ? other.getPAD4() == null : PAD4.equals(other.getPAD4()))
			&& (PAD5 == null ? other.getPAD5() == null : PAD5.equals(other.getPAD5()))
			&& (PAD6 == null ? other.getPAD6() == null : PAD6.equals(other.getPAD6()))
			&& (OPERRATOR == null ? other.getOPERRATOR() == null : OPERRATOR.equals(other.getOPERRATOR()))
			&& (MAKEDATE == null ? other.getMAKEDATE() == null : fDate.getString(MAKEDATE).equals(other.getMAKEDATE()))
			&& (MAKETIME == null ? other.getMAKETIME() == null : MAKETIME.equals(other.getMAKETIME()))
			&& (MODIFYDATE == null ? other.getMODIFYDATE() == null : fDate.getString(MODIFYDATE).equals(other.getMODIFYDATE()))
			&& (MODIFYTIME == null ? other.getMODIFYTIME() == null : MODIFYTIME.equals(other.getMODIFYTIME()))
			&& (RETURNVISITFLAG == null ? other.getRETURNVISITFLAG() == null : RETURNVISITFLAG.equals(other.getRETURNVISITFLAG()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ACTIVITYSERIALNO") ) {
			return 0;
		}
		if( strFieldName.equals("POLICYNO") ) {
			return 1;
		}
		if( strFieldName.equals("BATCHSERIALNO") ) {
			return 2;
		}
		if( strFieldName.equals("EXECUTEPHASE") ) {
			return 3;
		}
		if( strFieldName.equals("LOCKUSERID") ) {
			return 4;
		}
		if( strFieldName.equals("LOCKTIME") ) {
			return 5;
		}
		if( strFieldName.equals("CLOSEFLAG") ) {
			return 6;
		}
		if( strFieldName.equals("COMPLETETIME") ) {
			return 7;
		}
		if( strFieldName.equals("VISITUSERID") ) {
			return 8;
		}
		if( strFieldName.equals("VISITPHASE") ) {
			return 9;
		}
		if( strFieldName.equals("VISITPHASEMEMO") ) {
			return 10;
		}
		if( strFieldName.equals("NEXTCONTACTTIME") ) {
			return 11;
		}
		if( strFieldName.equals("CALLCOUNT") ) {
			return 12;
		}
		if( strFieldName.equals("PROBLEMCOUNT") ) {
			return 13;
		}
		if( strFieldName.equals("BELONGORG") ) {
			return 14;
		}
		if( strFieldName.equals("REFERREUSER") ) {
			return 15;
		}
		if( strFieldName.equals("FEEDBACKUSER") ) {
			return 16;
		}
		if( strFieldName.equals("REFERRETIME") ) {
			return 17;
		}
		if( strFieldName.equals("FEEDBACKTIME") ) {
			return 18;
		}
		if( strFieldName.equals("HIDEFLG") ) {
			return 19;
		}
		if( strFieldName.equals("CANCELFLG") ) {
			return 20;
		}
		if( strFieldName.equals("PAD1") ) {
			return 21;
		}
		if( strFieldName.equals("PAD2") ) {
			return 22;
		}
		if( strFieldName.equals("PAD3") ) {
			return 23;
		}
		if( strFieldName.equals("PAD4") ) {
			return 24;
		}
		if( strFieldName.equals("PAD5") ) {
			return 25;
		}
		if( strFieldName.equals("PAD6") ) {
			return 26;
		}
		if( strFieldName.equals("OPERRATOR") ) {
			return 27;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return 28;
		}
		if( strFieldName.equals("MAKETIME") ) {
			return 29;
		}
		if( strFieldName.equals("MODIFYDATE") ) {
			return 30;
		}
		if( strFieldName.equals("MODIFYTIME") ) {
			return 31;
		}
		if( strFieldName.equals("RETURNVISITFLAG") ) {
			return 32;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ACTIVITYSERIALNO";
				break;
			case 1:
				strFieldName = "POLICYNO";
				break;
			case 2:
				strFieldName = "BATCHSERIALNO";
				break;
			case 3:
				strFieldName = "EXECUTEPHASE";
				break;
			case 4:
				strFieldName = "LOCKUSERID";
				break;
			case 5:
				strFieldName = "LOCKTIME";
				break;
			case 6:
				strFieldName = "CLOSEFLAG";
				break;
			case 7:
				strFieldName = "COMPLETETIME";
				break;
			case 8:
				strFieldName = "VISITUSERID";
				break;
			case 9:
				strFieldName = "VISITPHASE";
				break;
			case 10:
				strFieldName = "VISITPHASEMEMO";
				break;
			case 11:
				strFieldName = "NEXTCONTACTTIME";
				break;
			case 12:
				strFieldName = "CALLCOUNT";
				break;
			case 13:
				strFieldName = "PROBLEMCOUNT";
				break;
			case 14:
				strFieldName = "BELONGORG";
				break;
			case 15:
				strFieldName = "REFERREUSER";
				break;
			case 16:
				strFieldName = "FEEDBACKUSER";
				break;
			case 17:
				strFieldName = "REFERRETIME";
				break;
			case 18:
				strFieldName = "FEEDBACKTIME";
				break;
			case 19:
				strFieldName = "HIDEFLG";
				break;
			case 20:
				strFieldName = "CANCELFLG";
				break;
			case 21:
				strFieldName = "PAD1";
				break;
			case 22:
				strFieldName = "PAD2";
				break;
			case 23:
				strFieldName = "PAD3";
				break;
			case 24:
				strFieldName = "PAD4";
				break;
			case 25:
				strFieldName = "PAD5";
				break;
			case 26:
				strFieldName = "PAD6";
				break;
			case 27:
				strFieldName = "OPERRATOR";
				break;
			case 28:
				strFieldName = "MAKEDATE";
				break;
			case 29:
				strFieldName = "MAKETIME";
				break;
			case 30:
				strFieldName = "MODIFYDATE";
				break;
			case 31:
				strFieldName = "MODIFYTIME";
				break;
			case 32:
				strFieldName = "RETURNVISITFLAG";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ACTIVITYSERIALNO") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("POLICYNO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BATCHSERIALNO") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("EXECUTEPHASE") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("LOCKUSERID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LOCKTIME") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CLOSEFLAG") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("COMPLETETIME") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("VISITUSERID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VISITPHASE") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VISITPHASEMEMO") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NEXTCONTACTTIME") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CALLCOUNT") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("PROBLEMCOUNT") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("BELONGORG") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("REFERREUSER") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FEEDBACKUSER") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("REFERRETIME") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("FEEDBACKTIME") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("HIDEFLG") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CANCELFLG") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PAD1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PAD2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PAD3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PAD4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PAD5") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PAD6") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OPERRATOR") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MAKEDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MAKETIME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MODIFYDATE") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MODIFYTIME") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RETURNVISITFLAG") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_INT;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_INT;
				break;
			case 3:
				nFieldType = Schema.TYPE_INT;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_INT;
				break;
			case 7:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_INT;
				break;
			case 13:
				nFieldType = Schema.TYPE_INT;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
