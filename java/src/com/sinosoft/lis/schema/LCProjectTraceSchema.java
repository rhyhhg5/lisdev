/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCProjectTraceDB;

/*
 * <p>ClassName: LCProjectTraceSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新寿险业务系统模型
 * @CreateDate：2013-01-30
 */
public class LCProjectTraceSchema implements Schema, Cloneable
{
	// @Field
	/** 项目流水号 */
	private String ProjectSerialNo;
	/** 项目编码 */
	private String ProjectNo;
	/** 项目名称 */
	private String ProjectName;
	/** 项目属地（省） */
	private String Province;
	/** 项目属地（市） */
	private String City;
	/** 项目属地（县） */
	private String County;
	/** 项目类型 */
	private String ProjectType;
	/** 项目级别 */
	private String Grade;
	/** 覆盖人群 */
	private String PersonType;
	/** 合作期限 */
	private double Limit;
	/** 协议起期 */
	private Date StartDate;
	/** 协议止期 */
	private Date EndDate;
	/** 项目状态 */
	private String State;
	/** 项目创建日期 */
	private Date CreateDate;
	/** 项目创建时间 */
	private String CreateTime;
	/** 管理机构 */
	private String ManageCom;
	/** 操作员 */
	private String Operator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 21;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LCProjectTraceSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ProjectSerialNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LCProjectTraceSchema cloned = (LCProjectTraceSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getProjectSerialNo()
	{
		return ProjectSerialNo;
	}
	public void setProjectSerialNo(String aProjectSerialNo)
	{
		ProjectSerialNo = aProjectSerialNo;
	}
	public String getProjectNo()
	{
		return ProjectNo;
	}
	public void setProjectNo(String aProjectNo)
	{
		ProjectNo = aProjectNo;
	}
	public String getProjectName()
	{
		return ProjectName;
	}
	public void setProjectName(String aProjectName)
	{
		ProjectName = aProjectName;
	}
	public String getProvince()
	{
		return Province;
	}
	public void setProvince(String aProvince)
	{
		Province = aProvince;
	}
	public String getCity()
	{
		return City;
	}
	public void setCity(String aCity)
	{
		City = aCity;
	}
	public String getCounty()
	{
		return County;
	}
	public void setCounty(String aCounty)
	{
		County = aCounty;
	}
	public String getProjectType()
	{
		return ProjectType;
	}
	public void setProjectType(String aProjectType)
	{
		ProjectType = aProjectType;
	}
	public String getGrade()
	{
		return Grade;
	}
	public void setGrade(String aGrade)
	{
		Grade = aGrade;
	}
	public String getPersonType()
	{
		return PersonType;
	}
	public void setPersonType(String aPersonType)
	{
		PersonType = aPersonType;
	}
	public double getLimit()
	{
		return Limit;
	}
	public void setLimit(double aLimit)
	{
		Limit = Arith.round(aLimit,2);
	}
	public void setLimit(String aLimit)
	{
		if (aLimit != null && !aLimit.equals(""))
		{
			Double tDouble = new Double(aLimit);
			double d = tDouble.doubleValue();
                Limit = Arith.round(d,2);
		}
	}

	public String getStartDate()
	{
		if( StartDate != null )
			return fDate.getString(StartDate);
		else
			return null;
	}
	public void setStartDate(Date aStartDate)
	{
		StartDate = aStartDate;
	}
	public void setStartDate(String aStartDate)
	{
		if (aStartDate != null && !aStartDate.equals("") )
		{
			StartDate = fDate.getDate( aStartDate );
		}
		else
			StartDate = null;
	}

	public String getEndDate()
	{
		if( EndDate != null )
			return fDate.getString(EndDate);
		else
			return null;
	}
	public void setEndDate(Date aEndDate)
	{
		EndDate = aEndDate;
	}
	public void setEndDate(String aEndDate)
	{
		if (aEndDate != null && !aEndDate.equals("") )
		{
			EndDate = fDate.getDate( aEndDate );
		}
		else
			EndDate = null;
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getCreateDate()
	{
		if( CreateDate != null )
			return fDate.getString(CreateDate);
		else
			return null;
	}
	public void setCreateDate(Date aCreateDate)
	{
		CreateDate = aCreateDate;
	}
	public void setCreateDate(String aCreateDate)
	{
		if (aCreateDate != null && !aCreateDate.equals("") )
		{
			CreateDate = fDate.getDate( aCreateDate );
		}
		else
			CreateDate = null;
	}

	public String getCreateTime()
	{
		return CreateTime;
	}
	public void setCreateTime(String aCreateTime)
	{
		CreateTime = aCreateTime;
	}
	public String getManageCom()
	{
		return ManageCom;
	}
	public void setManageCom(String aManageCom)
	{
		ManageCom = aManageCom;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LCProjectTraceSchema 对象给 Schema 赋值
	* @param: aLCProjectTraceSchema LCProjectTraceSchema
	**/
	public void setSchema(LCProjectTraceSchema aLCProjectTraceSchema)
	{
		this.ProjectSerialNo = aLCProjectTraceSchema.getProjectSerialNo();
		this.ProjectNo = aLCProjectTraceSchema.getProjectNo();
		this.ProjectName = aLCProjectTraceSchema.getProjectName();
		this.Province = aLCProjectTraceSchema.getProvince();
		this.City = aLCProjectTraceSchema.getCity();
		this.County = aLCProjectTraceSchema.getCounty();
		this.ProjectType = aLCProjectTraceSchema.getProjectType();
		this.Grade = aLCProjectTraceSchema.getGrade();
		this.PersonType = aLCProjectTraceSchema.getPersonType();
		this.Limit = aLCProjectTraceSchema.getLimit();
		this.StartDate = fDate.getDate( aLCProjectTraceSchema.getStartDate());
		this.EndDate = fDate.getDate( aLCProjectTraceSchema.getEndDate());
		this.State = aLCProjectTraceSchema.getState();
		this.CreateDate = fDate.getDate( aLCProjectTraceSchema.getCreateDate());
		this.CreateTime = aLCProjectTraceSchema.getCreateTime();
		this.ManageCom = aLCProjectTraceSchema.getManageCom();
		this.Operator = aLCProjectTraceSchema.getOperator();
		this.MakeDate = fDate.getDate( aLCProjectTraceSchema.getMakeDate());
		this.MakeTime = aLCProjectTraceSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLCProjectTraceSchema.getModifyDate());
		this.ModifyTime = aLCProjectTraceSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ProjectSerialNo") == null )
				this.ProjectSerialNo = null;
			else
				this.ProjectSerialNo = rs.getString("ProjectSerialNo").trim();

			if( rs.getString("ProjectNo") == null )
				this.ProjectNo = null;
			else
				this.ProjectNo = rs.getString("ProjectNo").trim();

			if( rs.getString("ProjectName") == null )
				this.ProjectName = null;
			else
				this.ProjectName = rs.getString("ProjectName").trim();

			if( rs.getString("Province") == null )
				this.Province = null;
			else
				this.Province = rs.getString("Province").trim();

			if( rs.getString("City") == null )
				this.City = null;
			else
				this.City = rs.getString("City").trim();

			if( rs.getString("County") == null )
				this.County = null;
			else
				this.County = rs.getString("County").trim();

			if( rs.getString("ProjectType") == null )
				this.ProjectType = null;
			else
				this.ProjectType = rs.getString("ProjectType").trim();

			if( rs.getString("Grade") == null )
				this.Grade = null;
			else
				this.Grade = rs.getString("Grade").trim();

			if( rs.getString("PersonType") == null )
				this.PersonType = null;
			else
				this.PersonType = rs.getString("PersonType").trim();

			this.Limit = rs.getDouble("Limit");
			this.StartDate = rs.getDate("StartDate");
			this.EndDate = rs.getDate("EndDate");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			this.CreateDate = rs.getDate("CreateDate");
			if( rs.getString("CreateTime") == null )
				this.CreateTime = null;
			else
				this.CreateTime = rs.getString("CreateTime").trim();

			if( rs.getString("ManageCom") == null )
				this.ManageCom = null;
			else
				this.ManageCom = rs.getString("ManageCom").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LCProjectTrace表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectTraceSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LCProjectTraceSchema getSchema()
	{
		LCProjectTraceSchema aLCProjectTraceSchema = new LCProjectTraceSchema();
		aLCProjectTraceSchema.setSchema(this);
		return aLCProjectTraceSchema;
	}

	public LCProjectTraceDB getDB()
	{
		LCProjectTraceDB aDBOper = new LCProjectTraceDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCProjectTrace描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ProjectSerialNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Province)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(City)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(County)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Grade)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PersonType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(Limit));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( StartDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( EndDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( CreateDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CreateTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ManageCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCProjectTrace>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ProjectSerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ProjectNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ProjectName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Province = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			City = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			County = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			ProjectType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Grade = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			PersonType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Limit = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,10,SysConst.PACKAGESPILTER))).doubleValue();
			StartDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			EndDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			CreateDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			CreateTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LCProjectTraceSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ProjectSerialNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectSerialNo));
		}
		if (FCode.equals("ProjectNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectNo));
		}
		if (FCode.equals("ProjectName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectName));
		}
		if (FCode.equals("Province"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Province));
		}
		if (FCode.equals("City"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(City));
		}
		if (FCode.equals("County"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(County));
		}
		if (FCode.equals("ProjectType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectType));
		}
		if (FCode.equals("Grade"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Grade));
		}
		if (FCode.equals("PersonType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PersonType));
		}
		if (FCode.equals("Limit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Limit));
		}
		if (FCode.equals("StartDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
		}
		if (FCode.equals("EndDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("CreateDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getCreateDate()));
		}
		if (FCode.equals("CreateTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CreateTime));
		}
		if (FCode.equals("ManageCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ProjectSerialNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ProjectNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ProjectName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Province);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(City);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(County);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(ProjectType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Grade);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(PersonType);
				break;
			case 9:
				strFieldValue = String.valueOf(Limit);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getEndDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getCreateDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(CreateTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ManageCom);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ProjectSerialNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectSerialNo = FValue.trim();
			}
			else
				ProjectSerialNo = null;
		}
		if (FCode.equalsIgnoreCase("ProjectNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectNo = FValue.trim();
			}
			else
				ProjectNo = null;
		}
		if (FCode.equalsIgnoreCase("ProjectName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectName = FValue.trim();
			}
			else
				ProjectName = null;
		}
		if (FCode.equalsIgnoreCase("Province"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Province = FValue.trim();
			}
			else
				Province = null;
		}
		if (FCode.equalsIgnoreCase("City"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				City = FValue.trim();
			}
			else
				City = null;
		}
		if (FCode.equalsIgnoreCase("County"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				County = FValue.trim();
			}
			else
				County = null;
		}
		if (FCode.equalsIgnoreCase("ProjectType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectType = FValue.trim();
			}
			else
				ProjectType = null;
		}
		if (FCode.equalsIgnoreCase("Grade"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Grade = FValue.trim();
			}
			else
				Grade = null;
		}
		if (FCode.equalsIgnoreCase("PersonType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PersonType = FValue.trim();
			}
			else
				PersonType = null;
		}
		if (FCode.equalsIgnoreCase("Limit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Limit = d;
			}
		}
		if (FCode.equalsIgnoreCase("StartDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartDate = fDate.getDate( FValue );
			}
			else
				StartDate = null;
		}
		if (FCode.equalsIgnoreCase("EndDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				EndDate = fDate.getDate( FValue );
			}
			else
				EndDate = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("CreateDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				CreateDate = fDate.getDate( FValue );
			}
			else
				CreateDate = null;
		}
		if (FCode.equalsIgnoreCase("CreateTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CreateTime = FValue.trim();
			}
			else
				CreateTime = null;
		}
		if (FCode.equalsIgnoreCase("ManageCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ManageCom = FValue.trim();
			}
			else
				ManageCom = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LCProjectTraceSchema other = (LCProjectTraceSchema)otherObject;
		return
			(ProjectSerialNo == null ? other.getProjectSerialNo() == null : ProjectSerialNo.equals(other.getProjectSerialNo()))
			&& (ProjectNo == null ? other.getProjectNo() == null : ProjectNo.equals(other.getProjectNo()))
			&& (ProjectName == null ? other.getProjectName() == null : ProjectName.equals(other.getProjectName()))
			&& (Province == null ? other.getProvince() == null : Province.equals(other.getProvince()))
			&& (City == null ? other.getCity() == null : City.equals(other.getCity()))
			&& (County == null ? other.getCounty() == null : County.equals(other.getCounty()))
			&& (ProjectType == null ? other.getProjectType() == null : ProjectType.equals(other.getProjectType()))
			&& (Grade == null ? other.getGrade() == null : Grade.equals(other.getGrade()))
			&& (PersonType == null ? other.getPersonType() == null : PersonType.equals(other.getPersonType()))
			&& Limit == other.getLimit()
			&& (StartDate == null ? other.getStartDate() == null : fDate.getString(StartDate).equals(other.getStartDate()))
			&& (EndDate == null ? other.getEndDate() == null : fDate.getString(EndDate).equals(other.getEndDate()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (CreateDate == null ? other.getCreateDate() == null : fDate.getString(CreateDate).equals(other.getCreateDate()))
			&& (CreateTime == null ? other.getCreateTime() == null : CreateTime.equals(other.getCreateTime()))
			&& (ManageCom == null ? other.getManageCom() == null : ManageCom.equals(other.getManageCom()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ProjectSerialNo") ) {
			return 0;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return 1;
		}
		if( strFieldName.equals("ProjectName") ) {
			return 2;
		}
		if( strFieldName.equals("Province") ) {
			return 3;
		}
		if( strFieldName.equals("City") ) {
			return 4;
		}
		if( strFieldName.equals("County") ) {
			return 5;
		}
		if( strFieldName.equals("ProjectType") ) {
			return 6;
		}
		if( strFieldName.equals("Grade") ) {
			return 7;
		}
		if( strFieldName.equals("PersonType") ) {
			return 8;
		}
		if( strFieldName.equals("Limit") ) {
			return 9;
		}
		if( strFieldName.equals("StartDate") ) {
			return 10;
		}
		if( strFieldName.equals("EndDate") ) {
			return 11;
		}
		if( strFieldName.equals("State") ) {
			return 12;
		}
		if( strFieldName.equals("CreateDate") ) {
			return 13;
		}
		if( strFieldName.equals("CreateTime") ) {
			return 14;
		}
		if( strFieldName.equals("ManageCom") ) {
			return 15;
		}
		if( strFieldName.equals("Operator") ) {
			return 16;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 17;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 20;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ProjectSerialNo";
				break;
			case 1:
				strFieldName = "ProjectNo";
				break;
			case 2:
				strFieldName = "ProjectName";
				break;
			case 3:
				strFieldName = "Province";
				break;
			case 4:
				strFieldName = "City";
				break;
			case 5:
				strFieldName = "County";
				break;
			case 6:
				strFieldName = "ProjectType";
				break;
			case 7:
				strFieldName = "Grade";
				break;
			case 8:
				strFieldName = "PersonType";
				break;
			case 9:
				strFieldName = "Limit";
				break;
			case 10:
				strFieldName = "StartDate";
				break;
			case 11:
				strFieldName = "EndDate";
				break;
			case 12:
				strFieldName = "State";
				break;
			case 13:
				strFieldName = "CreateDate";
				break;
			case 14:
				strFieldName = "CreateTime";
				break;
			case 15:
				strFieldName = "ManageCom";
				break;
			case 16:
				strFieldName = "Operator";
				break;
			case 17:
				strFieldName = "MakeDate";
				break;
			case 18:
				strFieldName = "MakeTime";
				break;
			case 19:
				strFieldName = "ModifyDate";
				break;
			case 20:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ProjectSerialNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Province") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("City") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("County") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Grade") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PersonType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Limit") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("StartDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("EndDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CreateDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("CreateTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ManageCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
