/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LBBankDB;

/*
 * <p>ClassName: LBBankSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 银行表修改
 * @CreateDate：2013-07-05
 */
public class LBBankSchema implements Schema, Cloneable
{
	// @Field
	/** 删除流水号 */
	private String DelSerialno;
	/** 银行编码 */
	private String BankCode;
	/** 银行名称 */
	private String BankName;
	/** 操作员代码 */
	private String Operator;
	/** 机构编码 */
	private String ComCode;
	/** 代理人编码 */
	private String AgentCode;
	/** 银行端代码 */
	private String BankSelfCode;
	/** 户名 */
	private String AccName;
	/** 保险公司帐号 */
	private String AccNo;
	/** 代收awk发送解码文件 */
	private String AgentPaySendF;
	/** 代收发送路径 */
	private String AgentPaySendPath;
	/** 代收awk接收解码文件 */
	private String AgentPayReceiveF;
	/** 代收文件存储路径 */
	private String AgentPaySavePath;
	/** 代收银行端约定的正确返回码 */
	private String AgentPaySuccFlag;
	/** 代收银行端约定的错误返回码 */
	private String AgentPayFailFlag;
	/** 代付awk发送解码文件 */
	private String AgentGetSendF;
	/** 代付发送路径 */
	private String AgentGetSendPath;
	/** 代付awk接收解码文件 */
	private String AgentGetReceiveF;
	/** 代付文件存储路径 */
	private String AgentGetSavePath;
	/** 代付银行端约定的正确返回码 */
	private String AgentGetSuccFlag;
	/** 代付银行端约定的错误返回码 */
	private String AgentGetFailFlag;
	/** 校验awk发送文件 */
	private String ChkSendF;
	/** 校验发送awk路径 */
	private String ChkSendPath;
	/** 校验awk接收文件 */
	private String ChkReceiveF;
	/** 校验接收awk路径 */
	private String ChkReceivePath;
	/** 校验成功标志 */
	private String ChkSuccFlag;
	/** 校验失败标志 */
	private String ChkFailFlag;
	/** 银行回单awk文件 */
	private String BankBackF;
	/** 银行回单awk路径 */
	private String BankBackPath;
	/** 代收文件返回类型 */
	private String AgentPayRFType;
	/** 代付文件返回类型 */
	private String AgentGetRFType;
	/** 支持新契约标记 */
	private String TBFlag;
	/** 支持保全标记 */
	private String BQFlag;
	/** 支持理赔标记 */
	private String LPFlag;
	/** 支持银行接口标记 */
	private String CanSendFlag;
	/** 银联标记 */
	private String BankUniteFlag;
	/** 银行端本单位代码 */
	private String BankComcode;
	/** 独立批次序号 */
	private String SeqNo;
	/** 付费银行帐号 */
	private String PayAccNo;
	/** 省级分行标志 */
	private String ProvincialBanch;
	/** 删除操作员 */
	private String DelOperator;
	/** 创建日期 */
	private Date MakeDate;
	/** 创建时间 */
	private Date MakeTime;
	/** 修改日期 */
	private Date ModifyDate;
	/** 修改时间 */
	private Date ModifyTime;

	public static final int FIELDNUM = 45;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LBBankSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "DelSerialno";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LBBankSchema cloned = (LBBankSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getDelSerialno()
	{
		return DelSerialno;
	}
	public void setDelSerialno(String aDelSerialno)
	{
		DelSerialno = aDelSerialno;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getBankName()
	{
		return BankName;
	}
	public void setBankName(String aBankName)
	{
		BankName = aBankName;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getComCode()
	{
		return ComCode;
	}
	public void setComCode(String aComCode)
	{
		ComCode = aComCode;
	}
	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getBankSelfCode()
	{
		return BankSelfCode;
	}
	public void setBankSelfCode(String aBankSelfCode)
	{
		BankSelfCode = aBankSelfCode;
	}
	public String getAccName()
	{
		return AccName;
	}
	public void setAccName(String aAccName)
	{
		AccName = aAccName;
	}
	public String getAccNo()
	{
		return AccNo;
	}
	public void setAccNo(String aAccNo)
	{
		AccNo = aAccNo;
	}
	public String getAgentPaySendF()
	{
		return AgentPaySendF;
	}
	public void setAgentPaySendF(String aAgentPaySendF)
	{
		AgentPaySendF = aAgentPaySendF;
	}
	public String getAgentPaySendPath()
	{
		return AgentPaySendPath;
	}
	public void setAgentPaySendPath(String aAgentPaySendPath)
	{
		AgentPaySendPath = aAgentPaySendPath;
	}
	public String getAgentPayReceiveF()
	{
		return AgentPayReceiveF;
	}
	public void setAgentPayReceiveF(String aAgentPayReceiveF)
	{
		AgentPayReceiveF = aAgentPayReceiveF;
	}
	public String getAgentPaySavePath()
	{
		return AgentPaySavePath;
	}
	public void setAgentPaySavePath(String aAgentPaySavePath)
	{
		AgentPaySavePath = aAgentPaySavePath;
	}
	public String getAgentPaySuccFlag()
	{
		return AgentPaySuccFlag;
	}
	public void setAgentPaySuccFlag(String aAgentPaySuccFlag)
	{
		AgentPaySuccFlag = aAgentPaySuccFlag;
	}
	public String getAgentPayFailFlag()
	{
		return AgentPayFailFlag;
	}
	public void setAgentPayFailFlag(String aAgentPayFailFlag)
	{
		AgentPayFailFlag = aAgentPayFailFlag;
	}
	public String getAgentGetSendF()
	{
		return AgentGetSendF;
	}
	public void setAgentGetSendF(String aAgentGetSendF)
	{
		AgentGetSendF = aAgentGetSendF;
	}
	public String getAgentGetSendPath()
	{
		return AgentGetSendPath;
	}
	public void setAgentGetSendPath(String aAgentGetSendPath)
	{
		AgentGetSendPath = aAgentGetSendPath;
	}
	public String getAgentGetReceiveF()
	{
		return AgentGetReceiveF;
	}
	public void setAgentGetReceiveF(String aAgentGetReceiveF)
	{
		AgentGetReceiveF = aAgentGetReceiveF;
	}
	public String getAgentGetSavePath()
	{
		return AgentGetSavePath;
	}
	public void setAgentGetSavePath(String aAgentGetSavePath)
	{
		AgentGetSavePath = aAgentGetSavePath;
	}
	public String getAgentGetSuccFlag()
	{
		return AgentGetSuccFlag;
	}
	public void setAgentGetSuccFlag(String aAgentGetSuccFlag)
	{
		AgentGetSuccFlag = aAgentGetSuccFlag;
	}
	public String getAgentGetFailFlag()
	{
		return AgentGetFailFlag;
	}
	public void setAgentGetFailFlag(String aAgentGetFailFlag)
	{
		AgentGetFailFlag = aAgentGetFailFlag;
	}
	public String getChkSendF()
	{
		return ChkSendF;
	}
	public void setChkSendF(String aChkSendF)
	{
		ChkSendF = aChkSendF;
	}
	public String getChkSendPath()
	{
		return ChkSendPath;
	}
	public void setChkSendPath(String aChkSendPath)
	{
		ChkSendPath = aChkSendPath;
	}
	public String getChkReceiveF()
	{
		return ChkReceiveF;
	}
	public void setChkReceiveF(String aChkReceiveF)
	{
		ChkReceiveF = aChkReceiveF;
	}
	public String getChkReceivePath()
	{
		return ChkReceivePath;
	}
	public void setChkReceivePath(String aChkReceivePath)
	{
		ChkReceivePath = aChkReceivePath;
	}
	public String getChkSuccFlag()
	{
		return ChkSuccFlag;
	}
	public void setChkSuccFlag(String aChkSuccFlag)
	{
		ChkSuccFlag = aChkSuccFlag;
	}
	public String getChkFailFlag()
	{
		return ChkFailFlag;
	}
	public void setChkFailFlag(String aChkFailFlag)
	{
		ChkFailFlag = aChkFailFlag;
	}
	public String getBankBackF()
	{
		return BankBackF;
	}
	public void setBankBackF(String aBankBackF)
	{
		BankBackF = aBankBackF;
	}
	public String getBankBackPath()
	{
		return BankBackPath;
	}
	public void setBankBackPath(String aBankBackPath)
	{
		BankBackPath = aBankBackPath;
	}
	public String getAgentPayRFType()
	{
		return AgentPayRFType;
	}
	public void setAgentPayRFType(String aAgentPayRFType)
	{
		AgentPayRFType = aAgentPayRFType;
	}
	public String getAgentGetRFType()
	{
		return AgentGetRFType;
	}
	public void setAgentGetRFType(String aAgentGetRFType)
	{
		AgentGetRFType = aAgentGetRFType;
	}
	public String getTBFlag()
	{
		return TBFlag;
	}
	public void setTBFlag(String aTBFlag)
	{
		TBFlag = aTBFlag;
	}
	public String getBQFlag()
	{
		return BQFlag;
	}
	public void setBQFlag(String aBQFlag)
	{
		BQFlag = aBQFlag;
	}
	public String getLPFlag()
	{
		return LPFlag;
	}
	public void setLPFlag(String aLPFlag)
	{
		LPFlag = aLPFlag;
	}
	public String getCanSendFlag()
	{
		return CanSendFlag;
	}
	public void setCanSendFlag(String aCanSendFlag)
	{
		CanSendFlag = aCanSendFlag;
	}
	public String getBankUniteFlag()
	{
		return BankUniteFlag;
	}
	public void setBankUniteFlag(String aBankUniteFlag)
	{
		BankUniteFlag = aBankUniteFlag;
	}
	public String getBankComcode()
	{
		return BankComcode;
	}
	public void setBankComcode(String aBankComcode)
	{
		BankComcode = aBankComcode;
	}
	public String getSeqNo()
	{
		return SeqNo;
	}
	public void setSeqNo(String aSeqNo)
	{
		SeqNo = aSeqNo;
	}
	public String getPayAccNo()
	{
		return PayAccNo;
	}
	public void setPayAccNo(String aPayAccNo)
	{
		PayAccNo = aPayAccNo;
	}
	public String getProvincialBanch()
	{
		return ProvincialBanch;
	}
	public void setProvincialBanch(String aProvincialBanch)
	{
		ProvincialBanch = aProvincialBanch;
	}
	public String getDelOperator()
	{
		return DelOperator;
	}
	public void setDelOperator(String aDelOperator)
	{
		DelOperator = aDelOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		if( MakeTime != null )
			return fDate.getString(MakeTime);
		else
			return null;
	}
	public void setMakeTime(Date aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		if (aMakeTime != null && !aMakeTime.equals("") )
		{
			MakeTime = fDate.getDate( aMakeTime );
		}
		else
			MakeTime = null;
	}

	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		if( ModifyTime != null )
			return fDate.getString(ModifyTime);
		else
			return null;
	}
	public void setModifyTime(Date aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		if (aModifyTime != null && !aModifyTime.equals("") )
		{
			ModifyTime = fDate.getDate( aModifyTime );
		}
		else
			ModifyTime = null;
	}


	/**
	* 使用另外一个 LBBankSchema 对象给 Schema 赋值
	* @param: aLBBankSchema LBBankSchema
	**/
	public void setSchema(LBBankSchema aLBBankSchema)
	{
		this.DelSerialno = aLBBankSchema.getDelSerialno();
		this.BankCode = aLBBankSchema.getBankCode();
		this.BankName = aLBBankSchema.getBankName();
		this.Operator = aLBBankSchema.getOperator();
		this.ComCode = aLBBankSchema.getComCode();
		this.AgentCode = aLBBankSchema.getAgentCode();
		this.BankSelfCode = aLBBankSchema.getBankSelfCode();
		this.AccName = aLBBankSchema.getAccName();
		this.AccNo = aLBBankSchema.getAccNo();
		this.AgentPaySendF = aLBBankSchema.getAgentPaySendF();
		this.AgentPaySendPath = aLBBankSchema.getAgentPaySendPath();
		this.AgentPayReceiveF = aLBBankSchema.getAgentPayReceiveF();
		this.AgentPaySavePath = aLBBankSchema.getAgentPaySavePath();
		this.AgentPaySuccFlag = aLBBankSchema.getAgentPaySuccFlag();
		this.AgentPayFailFlag = aLBBankSchema.getAgentPayFailFlag();
		this.AgentGetSendF = aLBBankSchema.getAgentGetSendF();
		this.AgentGetSendPath = aLBBankSchema.getAgentGetSendPath();
		this.AgentGetReceiveF = aLBBankSchema.getAgentGetReceiveF();
		this.AgentGetSavePath = aLBBankSchema.getAgentGetSavePath();
		this.AgentGetSuccFlag = aLBBankSchema.getAgentGetSuccFlag();
		this.AgentGetFailFlag = aLBBankSchema.getAgentGetFailFlag();
		this.ChkSendF = aLBBankSchema.getChkSendF();
		this.ChkSendPath = aLBBankSchema.getChkSendPath();
		this.ChkReceiveF = aLBBankSchema.getChkReceiveF();
		this.ChkReceivePath = aLBBankSchema.getChkReceivePath();
		this.ChkSuccFlag = aLBBankSchema.getChkSuccFlag();
		this.ChkFailFlag = aLBBankSchema.getChkFailFlag();
		this.BankBackF = aLBBankSchema.getBankBackF();
		this.BankBackPath = aLBBankSchema.getBankBackPath();
		this.AgentPayRFType = aLBBankSchema.getAgentPayRFType();
		this.AgentGetRFType = aLBBankSchema.getAgentGetRFType();
		this.TBFlag = aLBBankSchema.getTBFlag();
		this.BQFlag = aLBBankSchema.getBQFlag();
		this.LPFlag = aLBBankSchema.getLPFlag();
		this.CanSendFlag = aLBBankSchema.getCanSendFlag();
		this.BankUniteFlag = aLBBankSchema.getBankUniteFlag();
		this.BankComcode = aLBBankSchema.getBankComcode();
		this.SeqNo = aLBBankSchema.getSeqNo();
		this.PayAccNo = aLBBankSchema.getPayAccNo();
		this.ProvincialBanch = aLBBankSchema.getProvincialBanch();
		this.DelOperator = aLBBankSchema.getDelOperator();
		this.MakeDate = fDate.getDate( aLBBankSchema.getMakeDate());
		this.MakeTime = fDate.getDate( aLBBankSchema.getMakeTime());
		this.ModifyDate = fDate.getDate( aLBBankSchema.getModifyDate());
		this.ModifyTime = fDate.getDate( aLBBankSchema.getModifyTime());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("DelSerialno") == null )
				this.DelSerialno = null;
			else
				this.DelSerialno = rs.getString("DelSerialno").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("BankName") == null )
				this.BankName = null;
			else
				this.BankName = rs.getString("BankName").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			if( rs.getString("ComCode") == null )
				this.ComCode = null;
			else
				this.ComCode = rs.getString("ComCode").trim();

			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("BankSelfCode") == null )
				this.BankSelfCode = null;
			else
				this.BankSelfCode = rs.getString("BankSelfCode").trim();

			if( rs.getString("AccName") == null )
				this.AccName = null;
			else
				this.AccName = rs.getString("AccName").trim();

			if( rs.getString("AccNo") == null )
				this.AccNo = null;
			else
				this.AccNo = rs.getString("AccNo").trim();

			if( rs.getString("AgentPaySendF") == null )
				this.AgentPaySendF = null;
			else
				this.AgentPaySendF = rs.getString("AgentPaySendF").trim();

			if( rs.getString("AgentPaySendPath") == null )
				this.AgentPaySendPath = null;
			else
				this.AgentPaySendPath = rs.getString("AgentPaySendPath").trim();

			if( rs.getString("AgentPayReceiveF") == null )
				this.AgentPayReceiveF = null;
			else
				this.AgentPayReceiveF = rs.getString("AgentPayReceiveF").trim();

			if( rs.getString("AgentPaySavePath") == null )
				this.AgentPaySavePath = null;
			else
				this.AgentPaySavePath = rs.getString("AgentPaySavePath").trim();

			if( rs.getString("AgentPaySuccFlag") == null )
				this.AgentPaySuccFlag = null;
			else
				this.AgentPaySuccFlag = rs.getString("AgentPaySuccFlag").trim();

			if( rs.getString("AgentPayFailFlag") == null )
				this.AgentPayFailFlag = null;
			else
				this.AgentPayFailFlag = rs.getString("AgentPayFailFlag").trim();

			if( rs.getString("AgentGetSendF") == null )
				this.AgentGetSendF = null;
			else
				this.AgentGetSendF = rs.getString("AgentGetSendF").trim();

			if( rs.getString("AgentGetSendPath") == null )
				this.AgentGetSendPath = null;
			else
				this.AgentGetSendPath = rs.getString("AgentGetSendPath").trim();

			if( rs.getString("AgentGetReceiveF") == null )
				this.AgentGetReceiveF = null;
			else
				this.AgentGetReceiveF = rs.getString("AgentGetReceiveF").trim();

			if( rs.getString("AgentGetSavePath") == null )
				this.AgentGetSavePath = null;
			else
				this.AgentGetSavePath = rs.getString("AgentGetSavePath").trim();

			if( rs.getString("AgentGetSuccFlag") == null )
				this.AgentGetSuccFlag = null;
			else
				this.AgentGetSuccFlag = rs.getString("AgentGetSuccFlag").trim();

			if( rs.getString("AgentGetFailFlag") == null )
				this.AgentGetFailFlag = null;
			else
				this.AgentGetFailFlag = rs.getString("AgentGetFailFlag").trim();

			if( rs.getString("ChkSendF") == null )
				this.ChkSendF = null;
			else
				this.ChkSendF = rs.getString("ChkSendF").trim();

			if( rs.getString("ChkSendPath") == null )
				this.ChkSendPath = null;
			else
				this.ChkSendPath = rs.getString("ChkSendPath").trim();

			if( rs.getString("ChkReceiveF") == null )
				this.ChkReceiveF = null;
			else
				this.ChkReceiveF = rs.getString("ChkReceiveF").trim();

			if( rs.getString("ChkReceivePath") == null )
				this.ChkReceivePath = null;
			else
				this.ChkReceivePath = rs.getString("ChkReceivePath").trim();

			if( rs.getString("ChkSuccFlag") == null )
				this.ChkSuccFlag = null;
			else
				this.ChkSuccFlag = rs.getString("ChkSuccFlag").trim();

			if( rs.getString("ChkFailFlag") == null )
				this.ChkFailFlag = null;
			else
				this.ChkFailFlag = rs.getString("ChkFailFlag").trim();

			if( rs.getString("BankBackF") == null )
				this.BankBackF = null;
			else
				this.BankBackF = rs.getString("BankBackF").trim();

			if( rs.getString("BankBackPath") == null )
				this.BankBackPath = null;
			else
				this.BankBackPath = rs.getString("BankBackPath").trim();

			if( rs.getString("AgentPayRFType") == null )
				this.AgentPayRFType = null;
			else
				this.AgentPayRFType = rs.getString("AgentPayRFType").trim();

			if( rs.getString("AgentGetRFType") == null )
				this.AgentGetRFType = null;
			else
				this.AgentGetRFType = rs.getString("AgentGetRFType").trim();

			if( rs.getString("TBFlag") == null )
				this.TBFlag = null;
			else
				this.TBFlag = rs.getString("TBFlag").trim();

			if( rs.getString("BQFlag") == null )
				this.BQFlag = null;
			else
				this.BQFlag = rs.getString("BQFlag").trim();

			if( rs.getString("LPFlag") == null )
				this.LPFlag = null;
			else
				this.LPFlag = rs.getString("LPFlag").trim();

			if( rs.getString("CanSendFlag") == null )
				this.CanSendFlag = null;
			else
				this.CanSendFlag = rs.getString("CanSendFlag").trim();

			if( rs.getString("BankUniteFlag") == null )
				this.BankUniteFlag = null;
			else
				this.BankUniteFlag = rs.getString("BankUniteFlag").trim();

			if( rs.getString("BankComcode") == null )
				this.BankComcode = null;
			else
				this.BankComcode = rs.getString("BankComcode").trim();

			if( rs.getString("SeqNo") == null )
				this.SeqNo = null;
			else
				this.SeqNo = rs.getString("SeqNo").trim();

			if( rs.getString("PayAccNo") == null )
				this.PayAccNo = null;
			else
				this.PayAccNo = rs.getString("PayAccNo").trim();

			if( rs.getString("ProvincialBanch") == null )
				this.ProvincialBanch = null;
			else
				this.ProvincialBanch = rs.getString("ProvincialBanch").trim();

			if( rs.getString("DelOperator") == null )
				this.DelOperator = null;
			else
				this.DelOperator = rs.getString("DelOperator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			this.MakeTime = rs.getDate("MakeTime");
			this.ModifyDate = rs.getDate("ModifyDate");
			this.ModifyTime = rs.getDate("ModifyTime");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LBBank表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LBBankSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LBBankSchema getSchema()
	{
		LBBankSchema aLBBankSchema = new LBBankSchema();
		aLBBankSchema.setSchema(this);
		return aLBBankSchema;
	}

	public LBBankDB getDB()
	{
		LBBankDB aDBOper = new LBBankDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBBank描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(DelSerialno)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ComCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankSelfCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentPaySendF)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentPaySendPath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentPayReceiveF)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentPaySavePath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentPaySuccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentPayFailFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGetSendF)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGetSendPath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGetReceiveF)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGetSavePath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGetSuccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGetFailFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChkSendF)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChkSendPath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChkReceiveF)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChkReceivePath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChkSuccFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ChkFailFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankBackF)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankBackPath)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentPayRFType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AgentGetRFType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TBFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BQFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LPFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(CanSendFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankUniteFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankComcode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SeqNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PayAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProvincialBanch)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DelOperator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeTime ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyTime )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBBank>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			DelSerialno = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BankName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			ComCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			BankSelfCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			AccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			AgentPaySendF = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			AgentPaySendPath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			AgentPayReceiveF = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			AgentPaySavePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			AgentPaySuccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			AgentPayFailFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			AgentGetSendF = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			AgentGetSendPath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			AgentGetReceiveF = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			AgentGetSavePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			AgentGetSuccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
			AgentGetFailFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
			ChkSendF = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22, SysConst.PACKAGESPILTER );
			ChkSendPath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 23, SysConst.PACKAGESPILTER );
			ChkReceiveF = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24, SysConst.PACKAGESPILTER );
			ChkReceivePath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25, SysConst.PACKAGESPILTER );
			ChkSuccFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 26, SysConst.PACKAGESPILTER );
			ChkFailFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27, SysConst.PACKAGESPILTER );
			BankBackF = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 28, SysConst.PACKAGESPILTER );
			BankBackPath = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29, SysConst.PACKAGESPILTER );
			AgentPayRFType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 30, SysConst.PACKAGESPILTER );
			AgentGetRFType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 31, SysConst.PACKAGESPILTER );
			TBFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 32, SysConst.PACKAGESPILTER );
			BQFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 33, SysConst.PACKAGESPILTER );
			LPFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 34, SysConst.PACKAGESPILTER );
			CanSendFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 35, SysConst.PACKAGESPILTER );
			BankUniteFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 36, SysConst.PACKAGESPILTER );
			BankComcode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 37, SysConst.PACKAGESPILTER );
			SeqNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 38, SysConst.PACKAGESPILTER );
			PayAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 39, SysConst.PACKAGESPILTER );
			ProvincialBanch = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 40, SysConst.PACKAGESPILTER );
			DelOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 41, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 42,SysConst.PACKAGESPILTER));
			MakeTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 43,SysConst.PACKAGESPILTER));
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 44,SysConst.PACKAGESPILTER));
			ModifyTime = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 45,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LBBankSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("DelSerialno"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DelSerialno));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("BankName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankName));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("ComCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ComCode));
		}
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("BankSelfCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankSelfCode));
		}
		if (FCode.equals("AccName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
		}
		if (FCode.equals("AccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccNo));
		}
		if (FCode.equals("AgentPaySendF"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPaySendF));
		}
		if (FCode.equals("AgentPaySendPath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPaySendPath));
		}
		if (FCode.equals("AgentPayReceiveF"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPayReceiveF));
		}
		if (FCode.equals("AgentPaySavePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPaySavePath));
		}
		if (FCode.equals("AgentPaySuccFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPaySuccFlag));
		}
		if (FCode.equals("AgentPayFailFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPayFailFlag));
		}
		if (FCode.equals("AgentGetSendF"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGetSendF));
		}
		if (FCode.equals("AgentGetSendPath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGetSendPath));
		}
		if (FCode.equals("AgentGetReceiveF"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGetReceiveF));
		}
		if (FCode.equals("AgentGetSavePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGetSavePath));
		}
		if (FCode.equals("AgentGetSuccFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGetSuccFlag));
		}
		if (FCode.equals("AgentGetFailFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGetFailFlag));
		}
		if (FCode.equals("ChkSendF"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChkSendF));
		}
		if (FCode.equals("ChkSendPath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChkSendPath));
		}
		if (FCode.equals("ChkReceiveF"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChkReceiveF));
		}
		if (FCode.equals("ChkReceivePath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChkReceivePath));
		}
		if (FCode.equals("ChkSuccFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChkSuccFlag));
		}
		if (FCode.equals("ChkFailFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ChkFailFlag));
		}
		if (FCode.equals("BankBackF"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankBackF));
		}
		if (FCode.equals("BankBackPath"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankBackPath));
		}
		if (FCode.equals("AgentPayRFType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentPayRFType));
		}
		if (FCode.equals("AgentGetRFType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentGetRFType));
		}
		if (FCode.equals("TBFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TBFlag));
		}
		if (FCode.equals("BQFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BQFlag));
		}
		if (FCode.equals("LPFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LPFlag));
		}
		if (FCode.equals("CanSendFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CanSendFlag));
		}
		if (FCode.equals("BankUniteFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankUniteFlag));
		}
		if (FCode.equals("BankComcode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankComcode));
		}
		if (FCode.equals("SeqNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SeqNo));
		}
		if (FCode.equals("PayAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PayAccNo));
		}
		if (FCode.equals("ProvincialBanch"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProvincialBanch));
		}
		if (FCode.equals("DelOperator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DelOperator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeTime()));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyTime()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(DelSerialno);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BankName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(ComCode);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(BankSelfCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(AccName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(AccNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(AgentPaySendF);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(AgentPaySendPath);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(AgentPayReceiveF);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(AgentPaySavePath);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(AgentPaySuccFlag);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(AgentPayFailFlag);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(AgentGetSendF);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(AgentGetSendPath);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(AgentGetReceiveF);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(AgentGetSavePath);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(AgentGetSuccFlag);
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(AgentGetFailFlag);
				break;
			case 21:
				strFieldValue = StrTool.GBKToUnicode(ChkSendF);
				break;
			case 22:
				strFieldValue = StrTool.GBKToUnicode(ChkSendPath);
				break;
			case 23:
				strFieldValue = StrTool.GBKToUnicode(ChkReceiveF);
				break;
			case 24:
				strFieldValue = StrTool.GBKToUnicode(ChkReceivePath);
				break;
			case 25:
				strFieldValue = StrTool.GBKToUnicode(ChkSuccFlag);
				break;
			case 26:
				strFieldValue = StrTool.GBKToUnicode(ChkFailFlag);
				break;
			case 27:
				strFieldValue = StrTool.GBKToUnicode(BankBackF);
				break;
			case 28:
				strFieldValue = StrTool.GBKToUnicode(BankBackPath);
				break;
			case 29:
				strFieldValue = StrTool.GBKToUnicode(AgentPayRFType);
				break;
			case 30:
				strFieldValue = StrTool.GBKToUnicode(AgentGetRFType);
				break;
			case 31:
				strFieldValue = StrTool.GBKToUnicode(TBFlag);
				break;
			case 32:
				strFieldValue = StrTool.GBKToUnicode(BQFlag);
				break;
			case 33:
				strFieldValue = StrTool.GBKToUnicode(LPFlag);
				break;
			case 34:
				strFieldValue = StrTool.GBKToUnicode(CanSendFlag);
				break;
			case 35:
				strFieldValue = StrTool.GBKToUnicode(BankUniteFlag);
				break;
			case 36:
				strFieldValue = StrTool.GBKToUnicode(BankComcode);
				break;
			case 37:
				strFieldValue = StrTool.GBKToUnicode(SeqNo);
				break;
			case 38:
				strFieldValue = StrTool.GBKToUnicode(PayAccNo);
				break;
			case 39:
				strFieldValue = StrTool.GBKToUnicode(ProvincialBanch);
				break;
			case 40:
				strFieldValue = StrTool.GBKToUnicode(DelOperator);
				break;
			case 41:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 42:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeTime()));
				break;
			case 43:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 44:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyTime()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("DelSerialno"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DelSerialno = FValue.trim();
			}
			else
				DelSerialno = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("BankName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankName = FValue.trim();
			}
			else
				BankName = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("ComCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ComCode = FValue.trim();
			}
			else
				ComCode = null;
		}
		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("BankSelfCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankSelfCode = FValue.trim();
			}
			else
				BankSelfCode = null;
		}
		if (FCode.equalsIgnoreCase("AccName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccName = FValue.trim();
			}
			else
				AccName = null;
		}
		if (FCode.equalsIgnoreCase("AccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccNo = FValue.trim();
			}
			else
				AccNo = null;
		}
		if (FCode.equalsIgnoreCase("AgentPaySendF"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentPaySendF = FValue.trim();
			}
			else
				AgentPaySendF = null;
		}
		if (FCode.equalsIgnoreCase("AgentPaySendPath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentPaySendPath = FValue.trim();
			}
			else
				AgentPaySendPath = null;
		}
		if (FCode.equalsIgnoreCase("AgentPayReceiveF"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentPayReceiveF = FValue.trim();
			}
			else
				AgentPayReceiveF = null;
		}
		if (FCode.equalsIgnoreCase("AgentPaySavePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentPaySavePath = FValue.trim();
			}
			else
				AgentPaySavePath = null;
		}
		if (FCode.equalsIgnoreCase("AgentPaySuccFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentPaySuccFlag = FValue.trim();
			}
			else
				AgentPaySuccFlag = null;
		}
		if (FCode.equalsIgnoreCase("AgentPayFailFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentPayFailFlag = FValue.trim();
			}
			else
				AgentPayFailFlag = null;
		}
		if (FCode.equalsIgnoreCase("AgentGetSendF"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGetSendF = FValue.trim();
			}
			else
				AgentGetSendF = null;
		}
		if (FCode.equalsIgnoreCase("AgentGetSendPath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGetSendPath = FValue.trim();
			}
			else
				AgentGetSendPath = null;
		}
		if (FCode.equalsIgnoreCase("AgentGetReceiveF"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGetReceiveF = FValue.trim();
			}
			else
				AgentGetReceiveF = null;
		}
		if (FCode.equalsIgnoreCase("AgentGetSavePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGetSavePath = FValue.trim();
			}
			else
				AgentGetSavePath = null;
		}
		if (FCode.equalsIgnoreCase("AgentGetSuccFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGetSuccFlag = FValue.trim();
			}
			else
				AgentGetSuccFlag = null;
		}
		if (FCode.equalsIgnoreCase("AgentGetFailFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGetFailFlag = FValue.trim();
			}
			else
				AgentGetFailFlag = null;
		}
		if (FCode.equalsIgnoreCase("ChkSendF"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChkSendF = FValue.trim();
			}
			else
				ChkSendF = null;
		}
		if (FCode.equalsIgnoreCase("ChkSendPath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChkSendPath = FValue.trim();
			}
			else
				ChkSendPath = null;
		}
		if (FCode.equalsIgnoreCase("ChkReceiveF"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChkReceiveF = FValue.trim();
			}
			else
				ChkReceiveF = null;
		}
		if (FCode.equalsIgnoreCase("ChkReceivePath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChkReceivePath = FValue.trim();
			}
			else
				ChkReceivePath = null;
		}
		if (FCode.equalsIgnoreCase("ChkSuccFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChkSuccFlag = FValue.trim();
			}
			else
				ChkSuccFlag = null;
		}
		if (FCode.equalsIgnoreCase("ChkFailFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ChkFailFlag = FValue.trim();
			}
			else
				ChkFailFlag = null;
		}
		if (FCode.equalsIgnoreCase("BankBackF"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankBackF = FValue.trim();
			}
			else
				BankBackF = null;
		}
		if (FCode.equalsIgnoreCase("BankBackPath"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankBackPath = FValue.trim();
			}
			else
				BankBackPath = null;
		}
		if (FCode.equalsIgnoreCase("AgentPayRFType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentPayRFType = FValue.trim();
			}
			else
				AgentPayRFType = null;
		}
		if (FCode.equalsIgnoreCase("AgentGetRFType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentGetRFType = FValue.trim();
			}
			else
				AgentGetRFType = null;
		}
		if (FCode.equalsIgnoreCase("TBFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TBFlag = FValue.trim();
			}
			else
				TBFlag = null;
		}
		if (FCode.equalsIgnoreCase("BQFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BQFlag = FValue.trim();
			}
			else
				BQFlag = null;
		}
		if (FCode.equalsIgnoreCase("LPFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LPFlag = FValue.trim();
			}
			else
				LPFlag = null;
		}
		if (FCode.equalsIgnoreCase("CanSendFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				CanSendFlag = FValue.trim();
			}
			else
				CanSendFlag = null;
		}
		if (FCode.equalsIgnoreCase("BankUniteFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankUniteFlag = FValue.trim();
			}
			else
				BankUniteFlag = null;
		}
		if (FCode.equalsIgnoreCase("BankComcode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankComcode = FValue.trim();
			}
			else
				BankComcode = null;
		}
		if (FCode.equalsIgnoreCase("SeqNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SeqNo = FValue.trim();
			}
			else
				SeqNo = null;
		}
		if (FCode.equalsIgnoreCase("PayAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PayAccNo = FValue.trim();
			}
			else
				PayAccNo = null;
		}
		if (FCode.equalsIgnoreCase("ProvincialBanch"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProvincialBanch = FValue.trim();
			}
			else
				ProvincialBanch = null;
		}
		if (FCode.equalsIgnoreCase("DelOperator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DelOperator = FValue.trim();
			}
			else
				DelOperator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeTime = fDate.getDate( FValue );
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyTime = fDate.getDate( FValue );
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LBBankSchema other = (LBBankSchema)otherObject;
		return
			(DelSerialno == null ? other.getDelSerialno() == null : DelSerialno.equals(other.getDelSerialno()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (BankName == null ? other.getBankName() == null : BankName.equals(other.getBankName()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (ComCode == null ? other.getComCode() == null : ComCode.equals(other.getComCode()))
			&& (AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (BankSelfCode == null ? other.getBankSelfCode() == null : BankSelfCode.equals(other.getBankSelfCode()))
			&& (AccName == null ? other.getAccName() == null : AccName.equals(other.getAccName()))
			&& (AccNo == null ? other.getAccNo() == null : AccNo.equals(other.getAccNo()))
			&& (AgentPaySendF == null ? other.getAgentPaySendF() == null : AgentPaySendF.equals(other.getAgentPaySendF()))
			&& (AgentPaySendPath == null ? other.getAgentPaySendPath() == null : AgentPaySendPath.equals(other.getAgentPaySendPath()))
			&& (AgentPayReceiveF == null ? other.getAgentPayReceiveF() == null : AgentPayReceiveF.equals(other.getAgentPayReceiveF()))
			&& (AgentPaySavePath == null ? other.getAgentPaySavePath() == null : AgentPaySavePath.equals(other.getAgentPaySavePath()))
			&& (AgentPaySuccFlag == null ? other.getAgentPaySuccFlag() == null : AgentPaySuccFlag.equals(other.getAgentPaySuccFlag()))
			&& (AgentPayFailFlag == null ? other.getAgentPayFailFlag() == null : AgentPayFailFlag.equals(other.getAgentPayFailFlag()))
			&& (AgentGetSendF == null ? other.getAgentGetSendF() == null : AgentGetSendF.equals(other.getAgentGetSendF()))
			&& (AgentGetSendPath == null ? other.getAgentGetSendPath() == null : AgentGetSendPath.equals(other.getAgentGetSendPath()))
			&& (AgentGetReceiveF == null ? other.getAgentGetReceiveF() == null : AgentGetReceiveF.equals(other.getAgentGetReceiveF()))
			&& (AgentGetSavePath == null ? other.getAgentGetSavePath() == null : AgentGetSavePath.equals(other.getAgentGetSavePath()))
			&& (AgentGetSuccFlag == null ? other.getAgentGetSuccFlag() == null : AgentGetSuccFlag.equals(other.getAgentGetSuccFlag()))
			&& (AgentGetFailFlag == null ? other.getAgentGetFailFlag() == null : AgentGetFailFlag.equals(other.getAgentGetFailFlag()))
			&& (ChkSendF == null ? other.getChkSendF() == null : ChkSendF.equals(other.getChkSendF()))
			&& (ChkSendPath == null ? other.getChkSendPath() == null : ChkSendPath.equals(other.getChkSendPath()))
			&& (ChkReceiveF == null ? other.getChkReceiveF() == null : ChkReceiveF.equals(other.getChkReceiveF()))
			&& (ChkReceivePath == null ? other.getChkReceivePath() == null : ChkReceivePath.equals(other.getChkReceivePath()))
			&& (ChkSuccFlag == null ? other.getChkSuccFlag() == null : ChkSuccFlag.equals(other.getChkSuccFlag()))
			&& (ChkFailFlag == null ? other.getChkFailFlag() == null : ChkFailFlag.equals(other.getChkFailFlag()))
			&& (BankBackF == null ? other.getBankBackF() == null : BankBackF.equals(other.getBankBackF()))
			&& (BankBackPath == null ? other.getBankBackPath() == null : BankBackPath.equals(other.getBankBackPath()))
			&& (AgentPayRFType == null ? other.getAgentPayRFType() == null : AgentPayRFType.equals(other.getAgentPayRFType()))
			&& (AgentGetRFType == null ? other.getAgentGetRFType() == null : AgentGetRFType.equals(other.getAgentGetRFType()))
			&& (TBFlag == null ? other.getTBFlag() == null : TBFlag.equals(other.getTBFlag()))
			&& (BQFlag == null ? other.getBQFlag() == null : BQFlag.equals(other.getBQFlag()))
			&& (LPFlag == null ? other.getLPFlag() == null : LPFlag.equals(other.getLPFlag()))
			&& (CanSendFlag == null ? other.getCanSendFlag() == null : CanSendFlag.equals(other.getCanSendFlag()))
			&& (BankUniteFlag == null ? other.getBankUniteFlag() == null : BankUniteFlag.equals(other.getBankUniteFlag()))
			&& (BankComcode == null ? other.getBankComcode() == null : BankComcode.equals(other.getBankComcode()))
			&& (SeqNo == null ? other.getSeqNo() == null : SeqNo.equals(other.getSeqNo()))
			&& (PayAccNo == null ? other.getPayAccNo() == null : PayAccNo.equals(other.getPayAccNo()))
			&& (ProvincialBanch == null ? other.getProvincialBanch() == null : ProvincialBanch.equals(other.getProvincialBanch()))
			&& (DelOperator == null ? other.getDelOperator() == null : DelOperator.equals(other.getDelOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : fDate.getString(MakeTime).equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : fDate.getString(ModifyTime).equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("DelSerialno") ) {
			return 0;
		}
		if( strFieldName.equals("BankCode") ) {
			return 1;
		}
		if( strFieldName.equals("BankName") ) {
			return 2;
		}
		if( strFieldName.equals("Operator") ) {
			return 3;
		}
		if( strFieldName.equals("ComCode") ) {
			return 4;
		}
		if( strFieldName.equals("AgentCode") ) {
			return 5;
		}
		if( strFieldName.equals("BankSelfCode") ) {
			return 6;
		}
		if( strFieldName.equals("AccName") ) {
			return 7;
		}
		if( strFieldName.equals("AccNo") ) {
			return 8;
		}
		if( strFieldName.equals("AgentPaySendF") ) {
			return 9;
		}
		if( strFieldName.equals("AgentPaySendPath") ) {
			return 10;
		}
		if( strFieldName.equals("AgentPayReceiveF") ) {
			return 11;
		}
		if( strFieldName.equals("AgentPaySavePath") ) {
			return 12;
		}
		if( strFieldName.equals("AgentPaySuccFlag") ) {
			return 13;
		}
		if( strFieldName.equals("AgentPayFailFlag") ) {
			return 14;
		}
		if( strFieldName.equals("AgentGetSendF") ) {
			return 15;
		}
		if( strFieldName.equals("AgentGetSendPath") ) {
			return 16;
		}
		if( strFieldName.equals("AgentGetReceiveF") ) {
			return 17;
		}
		if( strFieldName.equals("AgentGetSavePath") ) {
			return 18;
		}
		if( strFieldName.equals("AgentGetSuccFlag") ) {
			return 19;
		}
		if( strFieldName.equals("AgentGetFailFlag") ) {
			return 20;
		}
		if( strFieldName.equals("ChkSendF") ) {
			return 21;
		}
		if( strFieldName.equals("ChkSendPath") ) {
			return 22;
		}
		if( strFieldName.equals("ChkReceiveF") ) {
			return 23;
		}
		if( strFieldName.equals("ChkReceivePath") ) {
			return 24;
		}
		if( strFieldName.equals("ChkSuccFlag") ) {
			return 25;
		}
		if( strFieldName.equals("ChkFailFlag") ) {
			return 26;
		}
		if( strFieldName.equals("BankBackF") ) {
			return 27;
		}
		if( strFieldName.equals("BankBackPath") ) {
			return 28;
		}
		if( strFieldName.equals("AgentPayRFType") ) {
			return 29;
		}
		if( strFieldName.equals("AgentGetRFType") ) {
			return 30;
		}
		if( strFieldName.equals("TBFlag") ) {
			return 31;
		}
		if( strFieldName.equals("BQFlag") ) {
			return 32;
		}
		if( strFieldName.equals("LPFlag") ) {
			return 33;
		}
		if( strFieldName.equals("CanSendFlag") ) {
			return 34;
		}
		if( strFieldName.equals("BankUniteFlag") ) {
			return 35;
		}
		if( strFieldName.equals("BankComcode") ) {
			return 36;
		}
		if( strFieldName.equals("SeqNo") ) {
			return 37;
		}
		if( strFieldName.equals("PayAccNo") ) {
			return 38;
		}
		if( strFieldName.equals("ProvincialBanch") ) {
			return 39;
		}
		if( strFieldName.equals("DelOperator") ) {
			return 40;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 41;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 42;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 43;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 44;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "DelSerialno";
				break;
			case 1:
				strFieldName = "BankCode";
				break;
			case 2:
				strFieldName = "BankName";
				break;
			case 3:
				strFieldName = "Operator";
				break;
			case 4:
				strFieldName = "ComCode";
				break;
			case 5:
				strFieldName = "AgentCode";
				break;
			case 6:
				strFieldName = "BankSelfCode";
				break;
			case 7:
				strFieldName = "AccName";
				break;
			case 8:
				strFieldName = "AccNo";
				break;
			case 9:
				strFieldName = "AgentPaySendF";
				break;
			case 10:
				strFieldName = "AgentPaySendPath";
				break;
			case 11:
				strFieldName = "AgentPayReceiveF";
				break;
			case 12:
				strFieldName = "AgentPaySavePath";
				break;
			case 13:
				strFieldName = "AgentPaySuccFlag";
				break;
			case 14:
				strFieldName = "AgentPayFailFlag";
				break;
			case 15:
				strFieldName = "AgentGetSendF";
				break;
			case 16:
				strFieldName = "AgentGetSendPath";
				break;
			case 17:
				strFieldName = "AgentGetReceiveF";
				break;
			case 18:
				strFieldName = "AgentGetSavePath";
				break;
			case 19:
				strFieldName = "AgentGetSuccFlag";
				break;
			case 20:
				strFieldName = "AgentGetFailFlag";
				break;
			case 21:
				strFieldName = "ChkSendF";
				break;
			case 22:
				strFieldName = "ChkSendPath";
				break;
			case 23:
				strFieldName = "ChkReceiveF";
				break;
			case 24:
				strFieldName = "ChkReceivePath";
				break;
			case 25:
				strFieldName = "ChkSuccFlag";
				break;
			case 26:
				strFieldName = "ChkFailFlag";
				break;
			case 27:
				strFieldName = "BankBackF";
				break;
			case 28:
				strFieldName = "BankBackPath";
				break;
			case 29:
				strFieldName = "AgentPayRFType";
				break;
			case 30:
				strFieldName = "AgentGetRFType";
				break;
			case 31:
				strFieldName = "TBFlag";
				break;
			case 32:
				strFieldName = "BQFlag";
				break;
			case 33:
				strFieldName = "LPFlag";
				break;
			case 34:
				strFieldName = "CanSendFlag";
				break;
			case 35:
				strFieldName = "BankUniteFlag";
				break;
			case 36:
				strFieldName = "BankComcode";
				break;
			case 37:
				strFieldName = "SeqNo";
				break;
			case 38:
				strFieldName = "PayAccNo";
				break;
			case 39:
				strFieldName = "ProvincialBanch";
				break;
			case 40:
				strFieldName = "DelOperator";
				break;
			case 41:
				strFieldName = "MakeDate";
				break;
			case 42:
				strFieldName = "MakeTime";
				break;
			case 43:
				strFieldName = "ModifyDate";
				break;
			case 44:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("DelSerialno") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ComCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankSelfCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentPaySendF") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentPaySendPath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentPayReceiveF") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentPaySavePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentPaySuccFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentPayFailFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGetSendF") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGetSendPath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGetReceiveF") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGetSavePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGetSuccFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGetFailFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChkSendF") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChkSendPath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChkReceiveF") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChkReceivePath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChkSuccFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ChkFailFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankBackF") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankBackPath") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentPayRFType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AgentGetRFType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("TBFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BQFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LPFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("CanSendFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankUniteFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankComcode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SeqNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PayAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProvincialBanch") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DelOperator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 21:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 22:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 23:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 24:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 25:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 26:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 27:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 28:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 29:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 30:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 31:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 32:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 33:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 34:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 35:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 36:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 37:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 38:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 39:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 40:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 41:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 42:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 43:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 44:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
