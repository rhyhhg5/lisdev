/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LZOrderCardDB;

/*
 * <p>ClassName: LZOrderCardSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 单证管理
 * @CreateDate：2006-07-21
 */
public class LZOrderCardSchema implements Schema, Cloneable {
    // @Field
    /** 批次号 */
    private String SerialNo;
    /** 单证类型 */
    private String CertifyCode;
    /** 预订数量 */
    private int PreAmn;
    /** 实际征订数量 */
    private int ActuAmn;
    /** 关联印刷批次号 */
    private String RelaPrint;
    /** 订单金额 */
    private double OrderMoney;
    /** 订单制定人 */
    private String DescPerson;
    /** 制定机构 */
    private String DescCom;
    /** 操作人 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 修改日期 */
    private Date ModifyDate;
    /** 修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 13; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LZOrderCardSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "SerialNo";
        pk[1] = "CertifyCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LZOrderCardSchema cloned = (LZOrderCardSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String aSerialNo) {
        SerialNo = aSerialNo;
    }

    public String getCertifyCode() {
        return CertifyCode;
    }

    public void setCertifyCode(String aCertifyCode) {
        CertifyCode = aCertifyCode;
    }

    public int getPreAmn() {
        return PreAmn;
    }

    public void setPreAmn(int aPreAmn) {
        PreAmn = aPreAmn;
    }

    public void setPreAmn(String aPreAmn) {
        if (aPreAmn != null && !aPreAmn.equals("")) {
            Integer tInteger = new Integer(aPreAmn);
            int i = tInteger.intValue();
            PreAmn = i;
        }
    }

    public int getActuAmn() {
        return ActuAmn;
    }

    public void setActuAmn(int aActuAmn) {
        ActuAmn = aActuAmn;
    }

    public void setActuAmn(String aActuAmn) {
        if (aActuAmn != null && !aActuAmn.equals("")) {
            Integer tInteger = new Integer(aActuAmn);
            int i = tInteger.intValue();
            ActuAmn = i;
        }
    }

    public String getRelaPrint() {
        return RelaPrint;
    }

    public void setRelaPrint(String aRelaPrint) {
        RelaPrint = aRelaPrint;
    }

    public double getOrderMoney() {
        return OrderMoney;
    }

    public void setOrderMoney(double aOrderMoney) {
        OrderMoney = Arith.round(aOrderMoney, 2);
    }

    public void setOrderMoney(String aOrderMoney) {
        if (aOrderMoney != null && !aOrderMoney.equals("")) {
            Double tDouble = new Double(aOrderMoney);
            double d = tDouble.doubleValue();
            OrderMoney = Arith.round(d, 2);
        }
    }

    public String getDescPerson() {
        return DescPerson;
    }

    public void setDescPerson(String aDescPerson) {
        DescPerson = aDescPerson;
    }

    public String getDescCom() {
        return DescCom;
    }

    public void setDescCom(String aDescCom) {
        DescCom = aDescCom;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LZOrderCardSchema 对象给 Schema 赋值
     * @param: aLZOrderCardSchema LZOrderCardSchema
     **/
    public void setSchema(LZOrderCardSchema aLZOrderCardSchema) {
        this.SerialNo = aLZOrderCardSchema.getSerialNo();
        this.CertifyCode = aLZOrderCardSchema.getCertifyCode();
        this.PreAmn = aLZOrderCardSchema.getPreAmn();
        this.ActuAmn = aLZOrderCardSchema.getActuAmn();
        this.RelaPrint = aLZOrderCardSchema.getRelaPrint();
        this.OrderMoney = aLZOrderCardSchema.getOrderMoney();
        this.DescPerson = aLZOrderCardSchema.getDescPerson();
        this.DescCom = aLZOrderCardSchema.getDescCom();
        this.Operator = aLZOrderCardSchema.getOperator();
        this.MakeDate = fDate.getDate(aLZOrderCardSchema.getMakeDate());
        this.MakeTime = aLZOrderCardSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLZOrderCardSchema.getModifyDate());
        this.ModifyTime = aLZOrderCardSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SerialNo") == null) {
                this.SerialNo = null;
            } else {
                this.SerialNo = rs.getString("SerialNo").trim();
            }

            if (rs.getString("CertifyCode") == null) {
                this.CertifyCode = null;
            } else {
                this.CertifyCode = rs.getString("CertifyCode").trim();
            }

            this.PreAmn = rs.getInt("PreAmn");
            this.ActuAmn = rs.getInt("ActuAmn");
            if (rs.getString("RelaPrint") == null) {
                this.RelaPrint = null;
            } else {
                this.RelaPrint = rs.getString("RelaPrint").trim();
            }

            this.OrderMoney = rs.getDouble("OrderMoney");
            if (rs.getString("DescPerson") == null) {
                this.DescPerson = null;
            } else {
                this.DescPerson = rs.getString("DescPerson").trim();
            }

            if (rs.getString("DescCom") == null) {
                this.DescCom = null;
            } else {
                this.DescCom = rs.getString("DescCom").trim();
            }

            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LZOrderCard表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZOrderCardSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LZOrderCardSchema getSchema() {
        LZOrderCardSchema aLZOrderCardSchema = new LZOrderCardSchema();
        aLZOrderCardSchema.setSchema(this);
        return aLZOrderCardSchema;
    }

    public LZOrderCardDB getDB() {
        LZOrderCardDB aDBOper = new LZOrderCardDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZOrderCard描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SerialNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(CertifyCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PreAmn));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(ActuAmn));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(RelaPrint));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(OrderMoney));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DescPerson));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DescCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLZOrderCard>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SerialNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            CertifyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            PreAmn = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 3, SysConst.PACKAGESPILTER))).intValue();
            ActuAmn = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 4, SysConst.PACKAGESPILTER))).intValue();
            RelaPrint = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            OrderMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).doubleValue();
            DescPerson = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            DescCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                     SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 10, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LZOrderCardSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SerialNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SerialNo));
        }
        if (FCode.equals("CertifyCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(CertifyCode));
        }
        if (FCode.equals("PreAmn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PreAmn));
        }
        if (FCode.equals("ActuAmn")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ActuAmn));
        }
        if (FCode.equals("RelaPrint")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(RelaPrint));
        }
        if (FCode.equals("OrderMoney")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OrderMoney));
        }
        if (FCode.equals("DescPerson")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DescPerson));
        }
        if (FCode.equals("DescCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DescCom));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SerialNo);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(CertifyCode);
            break;
        case 2:
            strFieldValue = String.valueOf(PreAmn);
            break;
        case 3:
            strFieldValue = String.valueOf(ActuAmn);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(RelaPrint);
            break;
        case 5:
            strFieldValue = String.valueOf(OrderMoney);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(DescPerson);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(DescCom);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SerialNo")) {
            if (FValue != null && !FValue.equals("")) {
                SerialNo = FValue.trim();
            } else {
                SerialNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("CertifyCode")) {
            if (FValue != null && !FValue.equals("")) {
                CertifyCode = FValue.trim();
            } else {
                CertifyCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("PreAmn")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PreAmn = i;
            }
        }
        if (FCode.equalsIgnoreCase("ActuAmn")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                ActuAmn = i;
            }
        }
        if (FCode.equalsIgnoreCase("RelaPrint")) {
            if (FValue != null && !FValue.equals("")) {
                RelaPrint = FValue.trim();
            } else {
                RelaPrint = null;
            }
        }
        if (FCode.equalsIgnoreCase("OrderMoney")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                OrderMoney = d;
            }
        }
        if (FCode.equalsIgnoreCase("DescPerson")) {
            if (FValue != null && !FValue.equals("")) {
                DescPerson = FValue.trim();
            } else {
                DescPerson = null;
            }
        }
        if (FCode.equalsIgnoreCase("DescCom")) {
            if (FValue != null && !FValue.equals("")) {
                DescCom = FValue.trim();
            } else {
                DescCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LZOrderCardSchema other = (LZOrderCardSchema) otherObject;
        return
                SerialNo.equals(other.getSerialNo())
                && CertifyCode.equals(other.getCertifyCode())
                && PreAmn == other.getPreAmn()
                && ActuAmn == other.getActuAmn()
                && RelaPrint.equals(other.getRelaPrint())
                && OrderMoney == other.getOrderMoney()
                && DescPerson.equals(other.getDescPerson())
                && DescCom.equals(other.getDescCom())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return 0;
        }
        if (strFieldName.equals("CertifyCode")) {
            return 1;
        }
        if (strFieldName.equals("PreAmn")) {
            return 2;
        }
        if (strFieldName.equals("ActuAmn")) {
            return 3;
        }
        if (strFieldName.equals("RelaPrint")) {
            return 4;
        }
        if (strFieldName.equals("OrderMoney")) {
            return 5;
        }
        if (strFieldName.equals("DescPerson")) {
            return 6;
        }
        if (strFieldName.equals("DescCom")) {
            return 7;
        }
        if (strFieldName.equals("Operator")) {
            return 8;
        }
        if (strFieldName.equals("MakeDate")) {
            return 9;
        }
        if (strFieldName.equals("MakeTime")) {
            return 10;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 11;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 12;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SerialNo";
            break;
        case 1:
            strFieldName = "CertifyCode";
            break;
        case 2:
            strFieldName = "PreAmn";
            break;
        case 3:
            strFieldName = "ActuAmn";
            break;
        case 4:
            strFieldName = "RelaPrint";
            break;
        case 5:
            strFieldName = "OrderMoney";
            break;
        case 6:
            strFieldName = "DescPerson";
            break;
        case 7:
            strFieldName = "DescCom";
            break;
        case 8:
            strFieldName = "Operator";
            break;
        case 9:
            strFieldName = "MakeDate";
            break;
        case 10:
            strFieldName = "MakeTime";
            break;
        case 11:
            strFieldName = "ModifyDate";
            break;
        case 12:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SerialNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CertifyCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PreAmn")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ActuAmn")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("RelaPrint")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OrderMoney")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("DescPerson")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DescCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_INT;
            break;
        case 3:
            nFieldType = Schema.TYPE_INT;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
