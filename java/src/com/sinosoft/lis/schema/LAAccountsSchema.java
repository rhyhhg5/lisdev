/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAAccountsDB;

/*
 * <p>ClassName: LAAccountsSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险存折表
 * @CreateDate：2016-06-14
 */
public class LAAccountsSchema implements Schema, Cloneable
{
	// @Field
	/** 业务员编码 */
	private String AgentCode;
	/** 帐号 */
	private String Account;
	/** 状态 */
	private String State;
	/** 开户银行 */
	private String Bank;
	/** 开户时间 */
	private Date OpenDate;
	/** 销户日期 */
	private Date DestoryDate;
	/** 帐号名称 */
	private String AccountName;
	/** 操作人员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 上一次修改日期 */
	private Date ModifyDate;
	/** 上一次修改时间 */
	private String ModifyTime;
	/** 开户行行号 */
	private String BankCode;
	/** 备用1 */
	private String T1;
	/** 备用2 */
	private String T2;
	/** 备用3 */
	private String T3;
	/** 备用4 */
	private String T4;
	/** 备用5 */
	private Date T5;
	/** 备用6 */
	private Date T6;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LAAccountsSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[2];
		pk[0] = "AgentCode";
		pk[1] = "Account";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LAAccountsSchema cloned = (LAAccountsSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getAgentCode()
	{
		return AgentCode;
	}
	public void setAgentCode(String aAgentCode)
	{
		AgentCode = aAgentCode;
	}
	public String getAccount()
	{
		return Account;
	}
	public void setAccount(String aAccount)
	{
		Account = aAccount;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getBank()
	{
		return Bank;
	}
	public void setBank(String aBank)
	{
		Bank = aBank;
	}
	public String getOpenDate()
	{
		if( OpenDate != null )
			return fDate.getString(OpenDate);
		else
			return null;
	}
	public void setOpenDate(Date aOpenDate)
	{
		OpenDate = aOpenDate;
	}
	public void setOpenDate(String aOpenDate)
	{
		if (aOpenDate != null && !aOpenDate.equals("") )
		{
			OpenDate = fDate.getDate( aOpenDate );
		}
		else
			OpenDate = null;
	}

	public String getDestoryDate()
	{
		if( DestoryDate != null )
			return fDate.getString(DestoryDate);
		else
			return null;
	}
	public void setDestoryDate(Date aDestoryDate)
	{
		DestoryDate = aDestoryDate;
	}
	public void setDestoryDate(String aDestoryDate)
	{
		if (aDestoryDate != null && !aDestoryDate.equals("") )
		{
			DestoryDate = fDate.getDate( aDestoryDate );
		}
		else
			DestoryDate = null;
	}

	public String getAccountName()
	{
		return AccountName;
	}
	public void setAccountName(String aAccountName)
	{
		AccountName = aAccountName;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getT1()
	{
		return T1;
	}
	public void setT1(String aT1)
	{
		T1 = aT1;
	}
	public String getT2()
	{
		return T2;
	}
	public void setT2(String aT2)
	{
		T2 = aT2;
	}
	public String getT3()
	{
		return T3;
	}
	public void setT3(String aT3)
	{
		T3 = aT3;
	}
	public String getT4()
	{
		return T4;
	}
	public void setT4(String aT4)
	{
		T4 = aT4;
	}
	public String getT5()
	{
		if( T5 != null )
			return fDate.getString(T5);
		else
			return null;
	}
	public void setT5(Date aT5)
	{
		T5 = aT5;
	}
	public void setT5(String aT5)
	{
		if (aT5 != null && !aT5.equals("") )
		{
			T5 = fDate.getDate( aT5 );
		}
		else
			T5 = null;
	}

	public String getT6()
	{
		if( T6 != null )
			return fDate.getString(T6);
		else
			return null;
	}
	public void setT6(Date aT6)
	{
		T6 = aT6;
	}
	public void setT6(String aT6)
	{
		if (aT6 != null && !aT6.equals("") )
		{
			T6 = fDate.getDate( aT6 );
		}
		else
			T6 = null;
	}


	/**
	* 使用另外一个 LAAccountsSchema 对象给 Schema 赋值
	* @param: aLAAccountsSchema LAAccountsSchema
	**/
	public void setSchema(LAAccountsSchema aLAAccountsSchema)
	{
		this.AgentCode = aLAAccountsSchema.getAgentCode();
		this.Account = aLAAccountsSchema.getAccount();
		this.State = aLAAccountsSchema.getState();
		this.Bank = aLAAccountsSchema.getBank();
		this.OpenDate = fDate.getDate( aLAAccountsSchema.getOpenDate());
		this.DestoryDate = fDate.getDate( aLAAccountsSchema.getDestoryDate());
		this.AccountName = aLAAccountsSchema.getAccountName();
		this.Operator = aLAAccountsSchema.getOperator();
		this.MakeDate = fDate.getDate( aLAAccountsSchema.getMakeDate());
		this.MakeTime = aLAAccountsSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLAAccountsSchema.getModifyDate());
		this.ModifyTime = aLAAccountsSchema.getModifyTime();
		this.BankCode = aLAAccountsSchema.getBankCode();
		this.T1 = aLAAccountsSchema.getT1();
		this.T2 = aLAAccountsSchema.getT2();
		this.T3 = aLAAccountsSchema.getT3();
		this.T4 = aLAAccountsSchema.getT4();
		this.T5 = fDate.getDate( aLAAccountsSchema.getT5());
		this.T6 = fDate.getDate( aLAAccountsSchema.getT6());
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("AgentCode") == null )
				this.AgentCode = null;
			else
				this.AgentCode = rs.getString("AgentCode").trim();

			if( rs.getString("Account") == null )
				this.Account = null;
			else
				this.Account = rs.getString("Account").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Bank") == null )
				this.Bank = null;
			else
				this.Bank = rs.getString("Bank").trim();

			this.OpenDate = rs.getDate("OpenDate");
			this.DestoryDate = rs.getDate("DestoryDate");
			if( rs.getString("AccountName") == null )
				this.AccountName = null;
			else
				this.AccountName = rs.getString("AccountName").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("T1") == null )
				this.T1 = null;
			else
				this.T1 = rs.getString("T1").trim();

			if( rs.getString("T2") == null )
				this.T2 = null;
			else
				this.T2 = rs.getString("T2").trim();

			if( rs.getString("T3") == null )
				this.T3 = null;
			else
				this.T3 = rs.getString("T3").trim();

			if( rs.getString("T4") == null )
				this.T4 = null;
			else
				this.T4 = rs.getString("T4").trim();

			this.T5 = rs.getDate("T5");
			this.T6 = rs.getDate("T6");
		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LAAccounts表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAccountsSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LAAccountsSchema getSchema()
	{
		LAAccountsSchema aLAAccountsSchema = new LAAccountsSchema();
		aLAAccountsSchema.setSchema(this);
		return aLAAccountsSchema;
	}

	public LAAccountsDB getDB()
	{
		LAAccountsDB aDBOper = new LAAccountsDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAccounts描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(AgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Account)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Bank)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( OpenDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( DestoryDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(AccountName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(T1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(T2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(T3)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(T4)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( T5 ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( T6 )));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLAAccounts>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Account = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Bank = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			OpenDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			DestoryDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			AccountName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			T1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			T2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			T3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			T4 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			T5 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			T6 = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,SysConst.PACKAGESPILTER));
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAccountsSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("AgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
		}
		if (FCode.equals("Account"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Account));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Bank"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Bank));
		}
		if (FCode.equals("OpenDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getOpenDate()));
		}
		if (FCode.equals("DestoryDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getDestoryDate()));
		}
		if (FCode.equals("AccountName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccountName));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("T1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T1));
		}
		if (FCode.equals("T2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T2));
		}
		if (FCode.equals("T3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T3));
		}
		if (FCode.equals("T4"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(T4));
		}
		if (FCode.equals("T5"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getT5()));
		}
		if (FCode.equals("T6"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getT6()));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(AgentCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Account);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Bank);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getOpenDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getDestoryDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(AccountName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(T1);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(T2);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(T3);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(T4);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getT5()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getT6()));
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("AgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AgentCode = FValue.trim();
			}
			else
				AgentCode = null;
		}
		if (FCode.equalsIgnoreCase("Account"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Account = FValue.trim();
			}
			else
				Account = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Bank"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Bank = FValue.trim();
			}
			else
				Bank = null;
		}
		if (FCode.equalsIgnoreCase("OpenDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				OpenDate = fDate.getDate( FValue );
			}
			else
				OpenDate = null;
		}
		if (FCode.equalsIgnoreCase("DestoryDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				DestoryDate = fDate.getDate( FValue );
			}
			else
				DestoryDate = null;
		}
		if (FCode.equalsIgnoreCase("AccountName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				AccountName = FValue.trim();
			}
			else
				AccountName = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("T1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				T1 = FValue.trim();
			}
			else
				T1 = null;
		}
		if (FCode.equalsIgnoreCase("T2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				T2 = FValue.trim();
			}
			else
				T2 = null;
		}
		if (FCode.equalsIgnoreCase("T3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				T3 = FValue.trim();
			}
			else
				T3 = null;
		}
		if (FCode.equalsIgnoreCase("T4"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				T4 = FValue.trim();
			}
			else
				T4 = null;
		}
		if (FCode.equalsIgnoreCase("T5"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				T5 = fDate.getDate( FValue );
			}
			else
				T5 = null;
		}
		if (FCode.equalsIgnoreCase("T6"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				T6 = fDate.getDate( FValue );
			}
			else
				T6 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LAAccountsSchema other = (LAAccountsSchema)otherObject;
		return
			(AgentCode == null ? other.getAgentCode() == null : AgentCode.equals(other.getAgentCode()))
			&& (Account == null ? other.getAccount() == null : Account.equals(other.getAccount()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Bank == null ? other.getBank() == null : Bank.equals(other.getBank()))
			&& (OpenDate == null ? other.getOpenDate() == null : fDate.getString(OpenDate).equals(other.getOpenDate()))
			&& (DestoryDate == null ? other.getDestoryDate() == null : fDate.getString(DestoryDate).equals(other.getDestoryDate()))
			&& (AccountName == null ? other.getAccountName() == null : AccountName.equals(other.getAccountName()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (T1 == null ? other.getT1() == null : T1.equals(other.getT1()))
			&& (T2 == null ? other.getT2() == null : T2.equals(other.getT2()))
			&& (T3 == null ? other.getT3() == null : T3.equals(other.getT3()))
			&& (T4 == null ? other.getT4() == null : T4.equals(other.getT4()))
			&& (T5 == null ? other.getT5() == null : fDate.getString(T5).equals(other.getT5()))
			&& (T6 == null ? other.getT6() == null : fDate.getString(T6).equals(other.getT6()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("AgentCode") ) {
			return 0;
		}
		if( strFieldName.equals("Account") ) {
			return 1;
		}
		if( strFieldName.equals("State") ) {
			return 2;
		}
		if( strFieldName.equals("Bank") ) {
			return 3;
		}
		if( strFieldName.equals("OpenDate") ) {
			return 4;
		}
		if( strFieldName.equals("DestoryDate") ) {
			return 5;
		}
		if( strFieldName.equals("AccountName") ) {
			return 6;
		}
		if( strFieldName.equals("Operator") ) {
			return 7;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 8;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 9;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 10;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 11;
		}
		if( strFieldName.equals("BankCode") ) {
			return 12;
		}
		if( strFieldName.equals("T1") ) {
			return 13;
		}
		if( strFieldName.equals("T2") ) {
			return 14;
		}
		if( strFieldName.equals("T3") ) {
			return 15;
		}
		if( strFieldName.equals("T4") ) {
			return 16;
		}
		if( strFieldName.equals("T5") ) {
			return 17;
		}
		if( strFieldName.equals("T6") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "AgentCode";
				break;
			case 1:
				strFieldName = "Account";
				break;
			case 2:
				strFieldName = "State";
				break;
			case 3:
				strFieldName = "Bank";
				break;
			case 4:
				strFieldName = "OpenDate";
				break;
			case 5:
				strFieldName = "DestoryDate";
				break;
			case 6:
				strFieldName = "AccountName";
				break;
			case 7:
				strFieldName = "Operator";
				break;
			case 8:
				strFieldName = "MakeDate";
				break;
			case 9:
				strFieldName = "MakeTime";
				break;
			case 10:
				strFieldName = "ModifyDate";
				break;
			case 11:
				strFieldName = "ModifyTime";
				break;
			case 12:
				strFieldName = "BankCode";
				break;
			case 13:
				strFieldName = "T1";
				break;
			case 14:
				strFieldName = "T2";
				break;
			case 15:
				strFieldName = "T3";
				break;
			case 16:
				strFieldName = "T4";
				break;
			case 17:
				strFieldName = "T5";
				break;
			case 18:
				strFieldName = "T6";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("AgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Account") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Bank") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("OpenDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DestoryDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("AccountName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("T1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("T2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("T3") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("T4") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("T5") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("T6") ) {
			return Schema.TYPE_DATE;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_DATE;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
