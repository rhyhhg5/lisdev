/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LMInsuAccRateDB;

/*
 * <p>ClassName: LMInsuAccRateSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 结算利率表
 * @CreateDate：2007-12-14
 */
public class LMInsuAccRateSchema implements Schema, Cloneable
{
	// @Field
	/** 险种编码 */
	private String RiskCode;
	/** 保险帐户号码 */
	private String InsuAccNo;
	/** 结算起始日 */
	private Date StartBalaDate;
	/** 应结算日期 */
	private Date BalaDate;
	/** 利率应公布日期 */
	private Date SRateDate;
	/** 利率实际公布日期 */
	private Date ARateDate;
	/** 领取方式 */
	private String DrawType;
	/** 利率类型 */
	private String RateType;
	/** 利率间隔 */
	private int RateIntv;
	/** 利率间隔单位 */
	private String RateIntvUnit;
	/** 利率 */
	private double Rate;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 16;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LMInsuAccRateSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[6];
		pk[0] = "RiskCode";
		pk[1] = "InsuAccNo";
		pk[2] = "BalaDate";
		pk[3] = "RateType";
		pk[4] = "RateIntv";
		pk[5] = "RateIntvUnit";

		PK = pk;
	}

            /**
             * Schema克隆
             * @return Object
             * @throws CloneNotSupportedException
             */
            public Object clone()
                    throws CloneNotSupportedException
            {
                LMInsuAccRateSchema cloned = (LMInsuAccRateSchema)super.clone();
                cloned.fDate = (FDate) fDate.clone();
                cloned.mErrors = (CErrors) mErrors.clone();
                return cloned;
            }

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
            RiskCode = aRiskCode;
	}
	public String getInsuAccNo()
	{
		return InsuAccNo;
	}
	public void setInsuAccNo(String aInsuAccNo)
	{
            InsuAccNo = aInsuAccNo;
	}
	public String getStartBalaDate()
	{
		if( StartBalaDate != null )
			return fDate.getString(StartBalaDate);
		else
			return null;
	}
	public void setStartBalaDate(Date aStartBalaDate)
	{
            StartBalaDate = aStartBalaDate;
	}
	public void setStartBalaDate(String aStartBalaDate)
	{
		if (aStartBalaDate != null && !aStartBalaDate.equals("") )
		{
			StartBalaDate = fDate.getDate( aStartBalaDate );
		}
		else
			StartBalaDate = null;
	}

	public String getBalaDate()
	{
		if( BalaDate != null )
			return fDate.getString(BalaDate);
		else
			return null;
	}
	public void setBalaDate(Date aBalaDate)
	{
            BalaDate = aBalaDate;
	}
	public void setBalaDate(String aBalaDate)
	{
		if (aBalaDate != null && !aBalaDate.equals("") )
		{
			BalaDate = fDate.getDate( aBalaDate );
		}
		else
			BalaDate = null;
	}

	public String getSRateDate()
	{
		if( SRateDate != null )
			return fDate.getString(SRateDate);
		else
			return null;
	}
	public void setSRateDate(Date aSRateDate)
	{
            SRateDate = aSRateDate;
	}
	public void setSRateDate(String aSRateDate)
	{
		if (aSRateDate != null && !aSRateDate.equals("") )
		{
			SRateDate = fDate.getDate( aSRateDate );
		}
		else
			SRateDate = null;
	}

	public String getARateDate()
	{
		if( ARateDate != null )
			return fDate.getString(ARateDate);
		else
			return null;
	}
	public void setARateDate(Date aARateDate)
	{
            ARateDate = aARateDate;
	}
	public void setARateDate(String aARateDate)
	{
		if (aARateDate != null && !aARateDate.equals("") )
		{
			ARateDate = fDate.getDate( aARateDate );
		}
		else
			ARateDate = null;
	}

	public String getDrawType()
	{
		return DrawType;
	}
	public void setDrawType(String aDrawType)
	{
            DrawType = aDrawType;
	}
	public String getRateType()
	{
		return RateType;
	}
	public void setRateType(String aRateType)
	{
            RateType = aRateType;
	}
	public int getRateIntv()
	{
		return RateIntv;
	}
	public void setRateIntv(int aRateIntv)
	{
            RateIntv = aRateIntv;
	}
	public void setRateIntv(String aRateIntv)
	{
		if (aRateIntv != null && !aRateIntv.equals(""))
		{
			Integer tInteger = new Integer(aRateIntv);
			int i = tInteger.intValue();
			RateIntv = i;
		}
	}

	public String getRateIntvUnit()
	{
		return RateIntvUnit;
	}
	public void setRateIntvUnit(String aRateIntvUnit)
	{
            RateIntvUnit = aRateIntvUnit;
	}
	public double getRate()
	{
		return Rate;
	}
	public void setRate(double aRate)
	{
            Rate = Arith.round(aRate,6);
	}
	public void setRate(String aRate)
	{
		if (aRate != null && !aRate.equals(""))
		{
			Double tDouble = new Double(aRate);
			double d = tDouble.doubleValue();
                Rate = Arith.round(d,6);
		}
	}

	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
            Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
            MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
            MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
            ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
            ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LMInsuAccRateSchema 对象给 Schema 赋值
	* @param: aLMInsuAccRateSchema LMInsuAccRateSchema
	**/
	public void setSchema(LMInsuAccRateSchema aLMInsuAccRateSchema)
	{
		this.RiskCode = aLMInsuAccRateSchema.getRiskCode();
		this.InsuAccNo = aLMInsuAccRateSchema.getInsuAccNo();
		this.StartBalaDate = fDate.getDate( aLMInsuAccRateSchema.getStartBalaDate());
		this.BalaDate = fDate.getDate( aLMInsuAccRateSchema.getBalaDate());
		this.SRateDate = fDate.getDate( aLMInsuAccRateSchema.getSRateDate());
		this.ARateDate = fDate.getDate( aLMInsuAccRateSchema.getARateDate());
		this.DrawType = aLMInsuAccRateSchema.getDrawType();
		this.RateType = aLMInsuAccRateSchema.getRateType();
		this.RateIntv = aLMInsuAccRateSchema.getRateIntv();
		this.RateIntvUnit = aLMInsuAccRateSchema.getRateIntvUnit();
		this.Rate = aLMInsuAccRateSchema.getRate();
		this.Operator = aLMInsuAccRateSchema.getOperator();
		this.MakeDate = fDate.getDate( aLMInsuAccRateSchema.getMakeDate());
		this.MakeTime = aLMInsuAccRateSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLMInsuAccRateSchema.getModifyDate());
		this.ModifyTime = aLMInsuAccRateSchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("InsuAccNo") == null )
				this.InsuAccNo = null;
			else
				this.InsuAccNo = rs.getString("InsuAccNo").trim();

			this.StartBalaDate = rs.getDate("StartBalaDate");
			this.BalaDate = rs.getDate("BalaDate");
			this.SRateDate = rs.getDate("SRateDate");
			this.ARateDate = rs.getDate("ARateDate");
			if( rs.getString("DrawType") == null )
				this.DrawType = null;
			else
				this.DrawType = rs.getString("DrawType").trim();

			if( rs.getString("RateType") == null )
				this.RateType = null;
			else
				this.RateType = rs.getString("RateType").trim();

			this.RateIntv = rs.getInt("RateIntv");
			if( rs.getString("RateIntvUnit") == null )
				this.RateIntvUnit = null;
			else
				this.RateIntvUnit = rs.getString("RateIntvUnit").trim();

			this.Rate = rs.getDouble("Rate");
			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LMInsuAccRate表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMInsuAccRateSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LMInsuAccRateSchema getSchema()
	{
		LMInsuAccRateSchema aLMInsuAccRateSchema = new LMInsuAccRateSchema();
		aLMInsuAccRateSchema.setSchema(this);
		return aLMInsuAccRateSchema;
	}

	public LMInsuAccRateDB getDB()
	{
		LMInsuAccRateDB aDBOper = new LMInsuAccRateDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMInsuAccRate描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(InsuAccNo)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( StartBalaDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( BalaDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( SRateDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ARateDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(DrawType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RateType)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(RateIntv));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(RateIntvUnit)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append( ChgData.chgData(Rate));strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMInsuAccRate>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			StartBalaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,SysConst.PACKAGESPILTER));
			BalaDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,SysConst.PACKAGESPILTER));
			SRateDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,SysConst.PACKAGESPILTER));
			ARateDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,SysConst.PACKAGESPILTER));
			DrawType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			RateType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			RateIntv= new Integer(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).intValue();
			RateIntvUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,11,SysConst.PACKAGESPILTER))).doubleValue();
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LMInsuAccRateSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("InsuAccNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(InsuAccNo));
		}
		if (FCode.equals("StartBalaDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getStartBalaDate()));
		}
		if (FCode.equals("BalaDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getBalaDate()));
		}
		if (FCode.equals("SRateDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getSRateDate()));
		}
		if (FCode.equals("ARateDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getARateDate()));
		}
		if (FCode.equals("DrawType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DrawType));
		}
		if (FCode.equals("RateType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RateType));
		}
		if (FCode.equals("RateIntv"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RateIntv));
		}
		if (FCode.equals("RateIntvUnit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RateIntvUnit));
		}
		if (FCode.equals("Rate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Rate));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getStartBalaDate()));
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getBalaDate()));
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getSRateDate()));
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getARateDate()));
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(DrawType);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(RateType);
				break;
			case 8:
				strFieldValue = String.valueOf(RateIntv);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(RateIntvUnit);
				break;
			case 10:
				strFieldValue = String.valueOf(Rate);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("InsuAccNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				InsuAccNo = FValue.trim();
			}
			else
				InsuAccNo = null;
		}
		if (FCode.equalsIgnoreCase("StartBalaDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				StartBalaDate = fDate.getDate( FValue );
			}
			else
				StartBalaDate = null;
		}
		if (FCode.equalsIgnoreCase("BalaDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				BalaDate = fDate.getDate( FValue );
			}
			else
				BalaDate = null;
		}
		if (FCode.equalsIgnoreCase("SRateDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				SRateDate = fDate.getDate( FValue );
			}
			else
				SRateDate = null;
		}
		if (FCode.equalsIgnoreCase("ARateDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ARateDate = fDate.getDate( FValue );
			}
			else
				ARateDate = null;
		}
		if (FCode.equalsIgnoreCase("DrawType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DrawType = FValue.trim();
			}
			else
				DrawType = null;
		}
		if (FCode.equalsIgnoreCase("RateType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RateType = FValue.trim();
			}
			else
				RateType = null;
		}
		if (FCode.equalsIgnoreCase("RateIntv"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Integer tInteger = new Integer( FValue );
				int i = tInteger.intValue();
				RateIntv = i;
			}
		}
		if (FCode.equalsIgnoreCase("RateIntvUnit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RateIntvUnit = FValue.trim();
			}
			else
				RateIntvUnit = null;
		}
		if (FCode.equalsIgnoreCase("Rate"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				Rate = d;
			}
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LMInsuAccRateSchema other = (LMInsuAccRateSchema)otherObject;
		return
			RiskCode.equals(other.getRiskCode())
			&& InsuAccNo.equals(other.getInsuAccNo())
			&& fDate.getString(StartBalaDate).equals(other.getStartBalaDate())
			&& fDate.getString(BalaDate).equals(other.getBalaDate())
			&& fDate.getString(SRateDate).equals(other.getSRateDate())
			&& fDate.getString(ARateDate).equals(other.getARateDate())
			&& DrawType.equals(other.getDrawType())
			&& RateType.equals(other.getRateType())
			&& RateIntv == other.getRateIntv()
			&& RateIntvUnit.equals(other.getRateIntvUnit())
			&& Rate == other.getRate()
			&& Operator.equals(other.getOperator())
			&& fDate.getString(MakeDate).equals(other.getMakeDate())
			&& MakeTime.equals(other.getMakeTime())
			&& fDate.getString(ModifyDate).equals(other.getModifyDate())
			&& ModifyTime.equals(other.getModifyTime());
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("RiskCode") ) {
			return 0;
		}
		if( strFieldName.equals("InsuAccNo") ) {
			return 1;
		}
		if( strFieldName.equals("StartBalaDate") ) {
			return 2;
		}
		if( strFieldName.equals("BalaDate") ) {
			return 3;
		}
		if( strFieldName.equals("SRateDate") ) {
			return 4;
		}
		if( strFieldName.equals("ARateDate") ) {
			return 5;
		}
		if( strFieldName.equals("DrawType") ) {
			return 6;
		}
		if( strFieldName.equals("RateType") ) {
			return 7;
		}
		if( strFieldName.equals("RateIntv") ) {
			return 8;
		}
		if( strFieldName.equals("RateIntvUnit") ) {
			return 9;
		}
		if( strFieldName.equals("Rate") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "RiskCode";
				break;
			case 1:
				strFieldName = "InsuAccNo";
				break;
			case 2:
				strFieldName = "StartBalaDate";
				break;
			case 3:
				strFieldName = "BalaDate";
				break;
			case 4:
				strFieldName = "SRateDate";
				break;
			case 5:
				strFieldName = "ARateDate";
				break;
			case 6:
				strFieldName = "DrawType";
				break;
			case 7:
				strFieldName = "RateType";
				break;
			case 8:
				strFieldName = "RateIntv";
				break;
			case 9:
				strFieldName = "RateIntvUnit";
				break;
			case 10:
				strFieldName = "Rate";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("InsuAccNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StartBalaDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("BalaDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("SRateDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ARateDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("DrawType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RateType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RateIntv") ) {
			return Schema.TYPE_INT;
		}
		if( strFieldName.equals("RateIntvUnit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Rate") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 3:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 4:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_INT;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
