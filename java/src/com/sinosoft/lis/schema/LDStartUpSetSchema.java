/*
 * <p>ClassName: LDStartUpSetSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 自动批处理
 * @CreateDate：2004-12-09
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LDStartUpSetDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LDStartUpSetSchema implements Schema
{
    // @Field
    /** 序号 */
    private String SeiralNo;
    /** 启用标志 */
    private String RunFlag;
    /** 执行class */
    private String ClassName;
    /** 执行函数 */
    private String FunctionName;
    /** 函数类型 */
    private String FunctionType;
    /** 执行日期 */
    private Date RunDate;
    /** 执行时间 */
    private String RunTime;
    /** 循环间隔 */
    private String Interval;
    /** 间隔单位 */
    private String IntervalType;
    /** 执行状态 */
    private String RunState;
    /** 操作员 */
    private String Operator;
    /** 录入时间 */
    private String MakeTime;
    /** 录入日期 */
    private Date MakeDate;
    /** 修改时间 */
    private String ModifyTime;
    /** 修改日期 */
    private Date ModifyDate;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDStartUpSetSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SeiralNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getSeiralNo()
    {
        if (SeiralNo != null && !SeiralNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            SeiralNo = StrTool.unicodeToGBK(SeiralNo);
        }
        return SeiralNo;
    }

    public void setSeiralNo(String aSeiralNo)
    {
        SeiralNo = aSeiralNo;
    }

    public String getRunFlag()
    {
        if (RunFlag != null && !RunFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            RunFlag = StrTool.unicodeToGBK(RunFlag);
        }
        return RunFlag;
    }

    public void setRunFlag(String aRunFlag)
    {
        RunFlag = aRunFlag;
    }

    public String getClassName()
    {
        if (ClassName != null && !ClassName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ClassName = StrTool.unicodeToGBK(ClassName);
        }
        return ClassName;
    }

    public void setClassName(String aClassName)
    {
        ClassName = aClassName;
    }

    public String getFunctionName()
    {
        if (FunctionName != null && !FunctionName.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FunctionName = StrTool.unicodeToGBK(FunctionName);
        }
        return FunctionName;
    }

    public void setFunctionName(String aFunctionName)
    {
        FunctionName = aFunctionName;
    }

    public String getFunctionType()
    {
        if (FunctionType != null && !FunctionType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FunctionType = StrTool.unicodeToGBK(FunctionType);
        }
        return FunctionType;
    }

    public void setFunctionType(String aFunctionType)
    {
        FunctionType = aFunctionType;
    }

    public String getRunDate()
    {
        if (RunDate != null)
        {
            return fDate.getString(RunDate);
        }
        else
        {
            return null;
        }
    }

    public void setRunDate(Date aRunDate)
    {
        RunDate = aRunDate;
    }

    public void setRunDate(String aRunDate)
    {
        if (aRunDate != null && !aRunDate.equals(""))
        {
            RunDate = fDate.getDate(aRunDate);
        }
        else
        {
            RunDate = null;
        }
    }

    public String getRunTime()
    {
        if (RunTime != null && !RunTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            RunTime = StrTool.unicodeToGBK(RunTime);
        }
        return RunTime;
    }

    public void setRunTime(String aRunTime)
    {
        RunTime = aRunTime;
    }

    public String getInterval()
    {
        if (Interval != null && !Interval.equals("") && SysConst.CHANGECHARSET == true)
        {
            Interval = StrTool.unicodeToGBK(Interval);
        }
        return Interval;
    }

    public void setInterval(String aInterval)
    {
        Interval = aInterval;
    }

    public String getIntervalType()
    {
        if (IntervalType != null && !IntervalType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            IntervalType = StrTool.unicodeToGBK(IntervalType);
        }
        return IntervalType;
    }

    public void setIntervalType(String aIntervalType)
    {
        IntervalType = aIntervalType;
    }

    public String getRunState()
    {
        if (RunState != null && !RunState.equals("") && SysConst.CHANGECHARSET == true)
        {
            RunState = StrTool.unicodeToGBK(RunState);
        }
        return RunState;
    }

    public void setRunState(String aRunState)
    {
        RunState = aRunState;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }


    /**
     * 使用另外一个 LDStartUpSetSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LDStartUpSetSchema aLDStartUpSetSchema)
    {
        this.SeiralNo = aLDStartUpSetSchema.getSeiralNo();
        this.RunFlag = aLDStartUpSetSchema.getRunFlag();
        this.ClassName = aLDStartUpSetSchema.getClassName();
        this.FunctionName = aLDStartUpSetSchema.getFunctionName();
        this.FunctionType = aLDStartUpSetSchema.getFunctionType();
        this.RunDate = fDate.getDate(aLDStartUpSetSchema.getRunDate());
        this.RunTime = aLDStartUpSetSchema.getRunTime();
        this.Interval = aLDStartUpSetSchema.getInterval();
        this.IntervalType = aLDStartUpSetSchema.getIntervalType();
        this.RunState = aLDStartUpSetSchema.getRunState();
        this.Operator = aLDStartUpSetSchema.getOperator();
        this.MakeTime = aLDStartUpSetSchema.getMakeTime();
        this.MakeDate = fDate.getDate(aLDStartUpSetSchema.getMakeDate());
        this.ModifyTime = aLDStartUpSetSchema.getModifyTime();
        this.ModifyDate = fDate.getDate(aLDStartUpSetSchema.getModifyDate());
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SeiralNo") == null)
            {
                this.SeiralNo = null;
            }
            else
            {
                this.SeiralNo = rs.getString("SeiralNo").trim();
            }

            if (rs.getString("RunFlag") == null)
            {
                this.RunFlag = null;
            }
            else
            {
                this.RunFlag = rs.getString("RunFlag").trim();
            }

            if (rs.getString("ClassName") == null)
            {
                this.ClassName = null;
            }
            else
            {
                this.ClassName = rs.getString("ClassName").trim();
            }

            if (rs.getString("FunctionName") == null)
            {
                this.FunctionName = null;
            }
            else
            {
                this.FunctionName = rs.getString("FunctionName").trim();
            }

            if (rs.getString("FunctionType") == null)
            {
                this.FunctionType = null;
            }
            else
            {
                this.FunctionType = rs.getString("FunctionType").trim();
            }

            this.RunDate = rs.getDate("RunDate");
            if (rs.getString("RunTime") == null)
            {
                this.RunTime = null;
            }
            else
            {
                this.RunTime = rs.getString("RunTime").trim();
            }

            if (rs.getString("Interval") == null)
            {
                this.Interval = null;
            }
            else
            {
                this.Interval = rs.getString("Interval").trim();
            }

            if (rs.getString("IntervalType") == null)
            {
                this.IntervalType = null;
            }
            else
            {
                this.IntervalType = rs.getString("IntervalType").trim();
            }

            if (rs.getString("RunState") == null)
            {
                this.RunState = null;
            }
            else
            {
                this.RunState = rs.getString("RunState").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDStartUpSetSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LDStartUpSetSchema getSchema()
    {
        LDStartUpSetSchema aLDStartUpSetSchema = new LDStartUpSetSchema();
        aLDStartUpSetSchema.setSchema(this);
        return aLDStartUpSetSchema;
    }

    public LDStartUpSetDB getDB()
    {
        LDStartUpSetDB aDBOper = new LDStartUpSetDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDStartUpSet描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(SeiralNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RunFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ClassName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FunctionName)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FunctionType)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(RunDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RunTime)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Interval)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(IntervalType)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RunState)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate)));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDStartUpSet>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            SeiralNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RunFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                     SysConst.PACKAGESPILTER);
            ClassName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                       SysConst.PACKAGESPILTER);
            FunctionName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                          SysConst.PACKAGESPILTER);
            FunctionType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                          SysConst.PACKAGESPILTER);
            RunDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 6, SysConst.PACKAGESPILTER));
            RunTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            Interval = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                      SysConst.PACKAGESPILTER);
            IntervalType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                          SysConst.PACKAGESPILTER);
            RunState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                      SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 13, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                        SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDStartUpSetSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("SeiralNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(SeiralNo));
        }
        if (FCode.equals("RunFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RunFlag));
        }
        if (FCode.equals("ClassName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ClassName));
        }
        if (FCode.equals("FunctionName"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FunctionName));
        }
        if (FCode.equals("FunctionType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FunctionType));
        }
        if (FCode.equals("RunDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getRunDate()));
        }
        if (FCode.equals("RunTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RunTime));
        }
        if (FCode.equals("Interval"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Interval));
        }
        if (FCode.equals("IntervalType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(IntervalType));
        }
        if (FCode.equals("RunState"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RunState));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(SeiralNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RunFlag);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(ClassName);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(FunctionName);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(FunctionType);
                break;
            case 5:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getRunDate()));
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(RunTime);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(Interval);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(IntervalType);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(RunState);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("SeiralNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                SeiralNo = FValue.trim();
            }
            else
            {
                SeiralNo = null;
            }
        }
        if (FCode.equals("RunFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RunFlag = FValue.trim();
            }
            else
            {
                RunFlag = null;
            }
        }
        if (FCode.equals("ClassName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ClassName = FValue.trim();
            }
            else
            {
                ClassName = null;
            }
        }
        if (FCode.equals("FunctionName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FunctionName = FValue.trim();
            }
            else
            {
                FunctionName = null;
            }
        }
        if (FCode.equals("FunctionType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FunctionType = FValue.trim();
            }
            else
            {
                FunctionType = null;
            }
        }
        if (FCode.equals("RunDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RunDate = fDate.getDate(FValue);
            }
            else
            {
                RunDate = null;
            }
        }
        if (FCode.equals("RunTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RunTime = FValue.trim();
            }
            else
            {
                RunTime = null;
            }
        }
        if (FCode.equals("Interval"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Interval = FValue.trim();
            }
            else
            {
                Interval = null;
            }
        }
        if (FCode.equals("IntervalType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                IntervalType = FValue.trim();
            }
            else
            {
                IntervalType = null;
            }
        }
        if (FCode.equals("RunState"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RunState = FValue.trim();
            }
            else
            {
                RunState = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDStartUpSetSchema other = (LDStartUpSetSchema) otherObject;
        return
                SeiralNo.equals(other.getSeiralNo())
                && RunFlag.equals(other.getRunFlag())
                && ClassName.equals(other.getClassName())
                && FunctionName.equals(other.getFunctionName())
                && FunctionType.equals(other.getFunctionType())
                && fDate.getString(RunDate).equals(other.getRunDate())
                && RunTime.equals(other.getRunTime())
                && Interval.equals(other.getInterval())
                && IntervalType.equals(other.getIntervalType())
                && RunState.equals(other.getRunState())
                && Operator.equals(other.getOperator())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && ModifyTime.equals(other.getModifyTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("SeiralNo"))
        {
            return 0;
        }
        if (strFieldName.equals("RunFlag"))
        {
            return 1;
        }
        if (strFieldName.equals("ClassName"))
        {
            return 2;
        }
        if (strFieldName.equals("FunctionName"))
        {
            return 3;
        }
        if (strFieldName.equals("FunctionType"))
        {
            return 4;
        }
        if (strFieldName.equals("RunDate"))
        {
            return 5;
        }
        if (strFieldName.equals("RunTime"))
        {
            return 6;
        }
        if (strFieldName.equals("Interval"))
        {
            return 7;
        }
        if (strFieldName.equals("IntervalType"))
        {
            return 8;
        }
        if (strFieldName.equals("RunState"))
        {
            return 9;
        }
        if (strFieldName.equals("Operator"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "SeiralNo";
                break;
            case 1:
                strFieldName = "RunFlag";
                break;
            case 2:
                strFieldName = "ClassName";
                break;
            case 3:
                strFieldName = "FunctionName";
                break;
            case 4:
                strFieldName = "FunctionType";
                break;
            case 5:
                strFieldName = "RunDate";
                break;
            case 6:
                strFieldName = "RunTime";
                break;
            case 7:
                strFieldName = "Interval";
                break;
            case 8:
                strFieldName = "IntervalType";
                break;
            case 9:
                strFieldName = "RunState";
                break;
            case 10:
                strFieldName = "Operator";
                break;
            case 11:
                strFieldName = "MakeTime";
                break;
            case 12:
                strFieldName = "MakeDate";
                break;
            case 13:
                strFieldName = "ModifyTime";
                break;
            case 14:
                strFieldName = "ModifyDate";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("SeiralNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RunFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ClassName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FunctionName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("FunctionType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RunDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("RunTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Interval"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("IntervalType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RunState"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 12:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 13:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 14:
                nFieldType = Schema.TYPE_DATE;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
