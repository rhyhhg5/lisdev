/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.FIVoucherBnFeeDB;

/*
 * <p>ClassName: FIVoucherBnFeeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 财务接口最新版
 * @CreateDate：2011-12-13
 */
public class FIVoucherBnFeeSchema implements Schema, Cloneable
{
	// @Field
	/** 版本编号 */
	private String VersionNo;
	/** 凭证类型编号 */
	private String VoucherID;
	/** 业务交易编号 */
	private String BusinessID;
	/** 费用名称 */
	private String FeeName;
	/** 费用编码 */
	private String FeeID;
	/** 借方科目类型编码 */
	private String D_FinItemID;
	/** 借方科目类型名称 */
	private String D_FinItemName;
	/** 贷方科目类型编码 */
	private String C_FinItemID;
	/** 贷方科目类型名称 */
	private String C_FinItemName;
	/** 费用类型描述 */
	private String Remark;
	/** 状态 */
	private String State;

	public static final int FIELDNUM = 11;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public FIVoucherBnFeeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[4];
		pk[0] = "VersionNo";
		pk[1] = "VoucherID";
		pk[2] = "BusinessID";
		pk[3] = "FeeID";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		FIVoucherBnFeeSchema cloned = (FIVoucherBnFeeSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getVersionNo()
	{
		return VersionNo;
	}
	public void setVersionNo(String aVersionNo)
	{
		VersionNo = aVersionNo;
	}
	public String getVoucherID()
	{
		return VoucherID;
	}
	public void setVoucherID(String aVoucherID)
	{
		VoucherID = aVoucherID;
	}
	public String getBusinessID()
	{
		return BusinessID;
	}
	public void setBusinessID(String aBusinessID)
	{
		BusinessID = aBusinessID;
	}
	public String getFeeName()
	{
		return FeeName;
	}
	public void setFeeName(String aFeeName)
	{
		FeeName = aFeeName;
	}
	public String getFeeID()
	{
		return FeeID;
	}
	public void setFeeID(String aFeeID)
	{
		FeeID = aFeeID;
	}
	public String getD_FinItemID()
	{
		return D_FinItemID;
	}
	public void setD_FinItemID(String aD_FinItemID)
	{
		D_FinItemID = aD_FinItemID;
	}
	public String getD_FinItemName()
	{
		return D_FinItemName;
	}
	public void setD_FinItemName(String aD_FinItemName)
	{
		D_FinItemName = aD_FinItemName;
	}
	public String getC_FinItemID()
	{
		return C_FinItemID;
	}
	public void setC_FinItemID(String aC_FinItemID)
	{
		C_FinItemID = aC_FinItemID;
	}
	public String getC_FinItemName()
	{
		return C_FinItemName;
	}
	public void setC_FinItemName(String aC_FinItemName)
	{
		C_FinItemName = aC_FinItemName;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}

	/**
	* 使用另外一个 FIVoucherBnFeeSchema 对象给 Schema 赋值
	* @param: aFIVoucherBnFeeSchema FIVoucherBnFeeSchema
	**/
	public void setSchema(FIVoucherBnFeeSchema aFIVoucherBnFeeSchema)
	{
		this.VersionNo = aFIVoucherBnFeeSchema.getVersionNo();
		this.VoucherID = aFIVoucherBnFeeSchema.getVoucherID();
		this.BusinessID = aFIVoucherBnFeeSchema.getBusinessID();
		this.FeeName = aFIVoucherBnFeeSchema.getFeeName();
		this.FeeID = aFIVoucherBnFeeSchema.getFeeID();
		this.D_FinItemID = aFIVoucherBnFeeSchema.getD_FinItemID();
		this.D_FinItemName = aFIVoucherBnFeeSchema.getD_FinItemName();
		this.C_FinItemID = aFIVoucherBnFeeSchema.getC_FinItemID();
		this.C_FinItemName = aFIVoucherBnFeeSchema.getC_FinItemName();
		this.Remark = aFIVoucherBnFeeSchema.getRemark();
		this.State = aFIVoucherBnFeeSchema.getState();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("VersionNo") == null )
				this.VersionNo = null;
			else
				this.VersionNo = rs.getString("VersionNo").trim();

			if( rs.getString("VoucherID") == null )
				this.VoucherID = null;
			else
				this.VoucherID = rs.getString("VoucherID").trim();

			if( rs.getString("BusinessID") == null )
				this.BusinessID = null;
			else
				this.BusinessID = rs.getString("BusinessID").trim();

			if( rs.getString("FeeName") == null )
				this.FeeName = null;
			else
				this.FeeName = rs.getString("FeeName").trim();

			if( rs.getString("FeeID") == null )
				this.FeeID = null;
			else
				this.FeeID = rs.getString("FeeID").trim();

			if( rs.getString("D_FinItemID") == null )
				this.D_FinItemID = null;
			else
				this.D_FinItemID = rs.getString("D_FinItemID").trim();

			if( rs.getString("D_FinItemName") == null )
				this.D_FinItemName = null;
			else
				this.D_FinItemName = rs.getString("D_FinItemName").trim();

			if( rs.getString("C_FinItemID") == null )
				this.C_FinItemID = null;
			else
				this.C_FinItemID = rs.getString("C_FinItemID").trim();

			if( rs.getString("C_FinItemName") == null )
				this.C_FinItemName = null;
			else
				this.C_FinItemName = rs.getString("C_FinItemName").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的FIVoucherBnFee表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherBnFeeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public FIVoucherBnFeeSchema getSchema()
	{
		FIVoucherBnFeeSchema aFIVoucherBnFeeSchema = new FIVoucherBnFeeSchema();
		aFIVoucherBnFeeSchema.setSchema(this);
		return aFIVoucherBnFeeSchema;
	}

	public FIVoucherBnFeeDB getDB()
	{
		FIVoucherBnFeeDB aDBOper = new FIVoucherBnFeeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVoucherBnFee描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(VersionNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VoucherID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BusinessID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(FeeID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(D_FinItemID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(D_FinItemName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(C_FinItemID)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(C_FinItemName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpFIVoucherBnFee>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			VersionNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			VoucherID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BusinessID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			FeeName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			FeeID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			D_FinItemID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			D_FinItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			C_FinItemID = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			C_FinItemName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FIVoucherBnFeeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("VersionNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VersionNo));
		}
		if (FCode.equals("VoucherID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VoucherID));
		}
		if (FCode.equals("BusinessID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BusinessID));
		}
		if (FCode.equals("FeeName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeName));
		}
		if (FCode.equals("FeeID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(FeeID));
		}
		if (FCode.equals("D_FinItemID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(D_FinItemID));
		}
		if (FCode.equals("D_FinItemName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(D_FinItemName));
		}
		if (FCode.equals("C_FinItemID"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C_FinItemID));
		}
		if (FCode.equals("C_FinItemName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(C_FinItemName));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(VersionNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(VoucherID);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BusinessID);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(FeeName);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(FeeID);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(D_FinItemID);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(D_FinItemName);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(C_FinItemID);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(C_FinItemName);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("VersionNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VersionNo = FValue.trim();
			}
			else
				VersionNo = null;
		}
		if (FCode.equalsIgnoreCase("VoucherID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VoucherID = FValue.trim();
			}
			else
				VoucherID = null;
		}
		if (FCode.equalsIgnoreCase("BusinessID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BusinessID = FValue.trim();
			}
			else
				BusinessID = null;
		}
		if (FCode.equalsIgnoreCase("FeeName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeName = FValue.trim();
			}
			else
				FeeName = null;
		}
		if (FCode.equalsIgnoreCase("FeeID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				FeeID = FValue.trim();
			}
			else
				FeeID = null;
		}
		if (FCode.equalsIgnoreCase("D_FinItemID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				D_FinItemID = FValue.trim();
			}
			else
				D_FinItemID = null;
		}
		if (FCode.equalsIgnoreCase("D_FinItemName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				D_FinItemName = FValue.trim();
			}
			else
				D_FinItemName = null;
		}
		if (FCode.equalsIgnoreCase("C_FinItemID"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				C_FinItemID = FValue.trim();
			}
			else
				C_FinItemID = null;
		}
		if (FCode.equalsIgnoreCase("C_FinItemName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				C_FinItemName = FValue.trim();
			}
			else
				C_FinItemName = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		FIVoucherBnFeeSchema other = (FIVoucherBnFeeSchema)otherObject;
		return
			(VersionNo == null ? other.getVersionNo() == null : VersionNo.equals(other.getVersionNo()))
			&& (VoucherID == null ? other.getVoucherID() == null : VoucherID.equals(other.getVoucherID()))
			&& (BusinessID == null ? other.getBusinessID() == null : BusinessID.equals(other.getBusinessID()))
			&& (FeeName == null ? other.getFeeName() == null : FeeName.equals(other.getFeeName()))
			&& (FeeID == null ? other.getFeeID() == null : FeeID.equals(other.getFeeID()))
			&& (D_FinItemID == null ? other.getD_FinItemID() == null : D_FinItemID.equals(other.getD_FinItemID()))
			&& (D_FinItemName == null ? other.getD_FinItemName() == null : D_FinItemName.equals(other.getD_FinItemName()))
			&& (C_FinItemID == null ? other.getC_FinItemID() == null : C_FinItemID.equals(other.getC_FinItemID()))
			&& (C_FinItemName == null ? other.getC_FinItemName() == null : C_FinItemName.equals(other.getC_FinItemName()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return 0;
		}
		if( strFieldName.equals("VoucherID") ) {
			return 1;
		}
		if( strFieldName.equals("BusinessID") ) {
			return 2;
		}
		if( strFieldName.equals("FeeName") ) {
			return 3;
		}
		if( strFieldName.equals("FeeID") ) {
			return 4;
		}
		if( strFieldName.equals("D_FinItemID") ) {
			return 5;
		}
		if( strFieldName.equals("D_FinItemName") ) {
			return 6;
		}
		if( strFieldName.equals("C_FinItemID") ) {
			return 7;
		}
		if( strFieldName.equals("C_FinItemName") ) {
			return 8;
		}
		if( strFieldName.equals("Remark") ) {
			return 9;
		}
		if( strFieldName.equals("State") ) {
			return 10;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "VersionNo";
				break;
			case 1:
				strFieldName = "VoucherID";
				break;
			case 2:
				strFieldName = "BusinessID";
				break;
			case 3:
				strFieldName = "FeeName";
				break;
			case 4:
				strFieldName = "FeeID";
				break;
			case 5:
				strFieldName = "D_FinItemID";
				break;
			case 6:
				strFieldName = "D_FinItemName";
				break;
			case 7:
				strFieldName = "C_FinItemID";
				break;
			case 8:
				strFieldName = "C_FinItemName";
				break;
			case 9:
				strFieldName = "Remark";
				break;
			case 10:
				strFieldName = "State";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("VersionNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VoucherID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BusinessID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("FeeID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("D_FinItemID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("D_FinItemName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("C_FinItemID") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("C_FinItemName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
