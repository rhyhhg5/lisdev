/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.StatisticsErrDB;

/*
 * <p>ClassName: StatisticsErrSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 数据校验
 * @CreateDate：2006-07-04
 */
public class StatisticsErrSchema implements Schema, Cloneable {
    // @Field
    /** 模块名 */
    private String ModuleName;
    /** 单号类型 */
    private String NoType;
    /** 号码 */
    private String MajorNo;
    /** 错误信息 */
    private String ErrMess;
    /** 建立日期 */
    private Date MakeDate;
    /** 建立时间 */
    private String MakeTime;

    public static final int FIELDNUM = 6; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public StatisticsErrSchema() {
        mErrors = new CErrors();

        String[] pk = new String[3];
        pk[0] = "ModuleName";
        pk[1] = "NoType";
        pk[2] = "MajorNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        StatisticsErrSchema cloned = (StatisticsErrSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public void setModuleName(String aModuleName) {
        ModuleName = aModuleName;
    }

    public String getNoType() {
        return NoType;
    }

    public void setNoType(String aNoType) {
        NoType = aNoType;
    }

    public String getMajorNo() {
        return MajorNo;
    }

    public void setMajorNo(String aMajorNo) {
        MajorNo = aMajorNo;
    }

    public String getErrMess() {
        return ErrMess;
    }

    public void setErrMess(String aErrMess) {
        ErrMess = aErrMess;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    /**
     * 使用另外一个 StatisticsErrSchema 对象给 Schema 赋值
     * @param: aStatisticsErrSchema StatisticsErrSchema
     **/
    public void setSchema(StatisticsErrSchema aStatisticsErrSchema) {
        this.ModuleName = aStatisticsErrSchema.getModuleName();
        this.NoType = aStatisticsErrSchema.getNoType();
        this.MajorNo = aStatisticsErrSchema.getMajorNo();
        this.ErrMess = aStatisticsErrSchema.getErrMess();
        this.MakeDate = fDate.getDate(aStatisticsErrSchema.getMakeDate());
        this.MakeTime = aStatisticsErrSchema.getMakeTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("ModuleName") == null) {
                this.ModuleName = null;
            } else {
                this.ModuleName = rs.getString("ModuleName").trim();
            }

            if (rs.getString("NoType") == null) {
                this.NoType = null;
            } else {
                this.NoType = rs.getString("NoType").trim();
            }

            if (rs.getString("MajorNo") == null) {
                this.MajorNo = null;
            } else {
                this.MajorNo = rs.getString("MajorNo").trim();
            }

            if (rs.getString("ErrMess") == null) {
                this.ErrMess = null;
            } else {
                this.ErrMess = rs.getString("ErrMess").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的StatisticsErr表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "StatisticsErrSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public StatisticsErrSchema getSchema() {
        StatisticsErrSchema aStatisticsErrSchema = new StatisticsErrSchema();
        aStatisticsErrSchema.setSchema(this);
        return aStatisticsErrSchema;
    }

    public StatisticsErrDB getDB() {
        StatisticsErrDB aDBOper = new StatisticsErrDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStatisticsErr描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(ModuleName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(NoType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MajorNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ErrMess));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpStatisticsErr>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ModuleName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                        SysConst.PACKAGESPILTER);
            NoType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            MajorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            ErrMess = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                      SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "StatisticsErrSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ModuleName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModuleName));
        }
        if (FCode.equals("NoType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(NoType));
        }
        if (FCode.equals("MajorNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MajorNo));
        }
        if (FCode.equals("ErrMess")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ErrMess));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(ModuleName);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(NoType);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(MajorNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ErrMess);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ModuleName")) {
            if (FValue != null && !FValue.equals("")) {
                ModuleName = FValue.trim();
            } else {
                ModuleName = null;
            }
        }
        if (FCode.equalsIgnoreCase("NoType")) {
            if (FValue != null && !FValue.equals("")) {
                NoType = FValue.trim();
            } else {
                NoType = null;
            }
        }
        if (FCode.equalsIgnoreCase("MajorNo")) {
            if (FValue != null && !FValue.equals("")) {
                MajorNo = FValue.trim();
            } else {
                MajorNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ErrMess")) {
            if (FValue != null && !FValue.equals("")) {
                ErrMess = FValue.trim();
            } else {
                ErrMess = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        StatisticsErrSchema other = (StatisticsErrSchema) otherObject;
        return
                ModuleName.equals(other.getModuleName())
                && NoType.equals(other.getNoType())
                && MajorNo.equals(other.getMajorNo())
                && ErrMess.equals(other.getErrMess())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ModuleName")) {
            return 0;
        }
        if (strFieldName.equals("NoType")) {
            return 1;
        }
        if (strFieldName.equals("MajorNo")) {
            return 2;
        }
        if (strFieldName.equals("ErrMess")) {
            return 3;
        }
        if (strFieldName.equals("MakeDate")) {
            return 4;
        }
        if (strFieldName.equals("MakeTime")) {
            return 5;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ModuleName";
            break;
        case 1:
            strFieldName = "NoType";
            break;
        case 2:
            strFieldName = "MajorNo";
            break;
        case 3:
            strFieldName = "ErrMess";
            break;
        case 4:
            strFieldName = "MakeDate";
            break;
        case 5:
            strFieldName = "MakeTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ModuleName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("NoType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MajorNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ErrMess")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
