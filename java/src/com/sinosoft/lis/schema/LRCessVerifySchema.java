/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LRCessVerifyDB;

/*
 * <p>ClassName: LRCessVerifySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2011-08-08
 */
public class LRCessVerifySchema implements Schema, Cloneable
{
	// @Field
	/** 团体合同号/险种保单号 */
	private String GrpContNo;
	/** 团体险种号 */
	private String GrpPolNo;
	/** 险种代码 */
	private String RiskCode;
	/** 再保合同标记 */
	private String ReContState;
	/** 财务产生的费用 */
	private double AccMoney;
	/** 共保保费 */
	private double GBPrem;
	/** 再保金额 */
	private double RePrem;
	/** 分保保费 */
	private double CessPrem;
	/** 财务再保差异 */
	private double DiffPrem;
	/** 校验类型 */
	private String State;
	/** 是否已校验标志 */
	private String VFlag;
	/** 未摊回原因 */
	private String Result;
	/** 保单承保日期 */
	private Date GetDate;
	/** 再保合同最早生效日期 */
	private Date ReContCVD;
	/** 备用字段1 */
	private String Standby1;
	/** 备用字段2 */
	private String Standby2;
	/** 操作人 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;

	public static final int FIELDNUM = 21;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LRCessVerifySchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[5];
		pk[0] = "GrpContNo";
		pk[1] = "GrpPolNo";
		pk[2] = "RiskCode";
		pk[3] = "ReContState";
		pk[4] = "State";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LRCessVerifySchema cloned = (LRCessVerifySchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getGrpContNo()
	{
		return GrpContNo;
	}
	public void setGrpContNo(String aGrpContNo)
	{
		GrpContNo = aGrpContNo;
	}
	public String getGrpPolNo()
	{
		return GrpPolNo;
	}
	public void setGrpPolNo(String aGrpPolNo)
	{
		GrpPolNo = aGrpPolNo;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getReContState()
	{
		return ReContState;
	}
	public void setReContState(String aReContState)
	{
		ReContState = aReContState;
	}
	public double getAccMoney()
	{
		return AccMoney;
	}
	public void setAccMoney(double aAccMoney)
	{
		AccMoney = Arith.round(aAccMoney,2);
	}
	public void setAccMoney(String aAccMoney)
	{
		if (aAccMoney != null && !aAccMoney.equals(""))
		{
			Double tDouble = new Double(aAccMoney);
			double d = tDouble.doubleValue();
                AccMoney = Arith.round(d,2);
		}
	}

	public double getGBPrem()
	{
		return GBPrem;
	}
	public void setGBPrem(double aGBPrem)
	{
		GBPrem = Arith.round(aGBPrem,2);
	}
	public void setGBPrem(String aGBPrem)
	{
		if (aGBPrem != null && !aGBPrem.equals(""))
		{
			Double tDouble = new Double(aGBPrem);
			double d = tDouble.doubleValue();
                GBPrem = Arith.round(d,2);
		}
	}

	public double getRePrem()
	{
		return RePrem;
	}
	public void setRePrem(double aRePrem)
	{
		RePrem = Arith.round(aRePrem,2);
	}
	public void setRePrem(String aRePrem)
	{
		if (aRePrem != null && !aRePrem.equals(""))
		{
			Double tDouble = new Double(aRePrem);
			double d = tDouble.doubleValue();
                RePrem = Arith.round(d,2);
		}
	}

	public double getCessPrem()
	{
		return CessPrem;
	}
	public void setCessPrem(double aCessPrem)
	{
		CessPrem = Arith.round(aCessPrem,2);
	}
	public void setCessPrem(String aCessPrem)
	{
		if (aCessPrem != null && !aCessPrem.equals(""))
		{
			Double tDouble = new Double(aCessPrem);
			double d = tDouble.doubleValue();
                CessPrem = Arith.round(d,2);
		}
	}

	public double getDiffPrem()
	{
		return DiffPrem;
	}
	public void setDiffPrem(double aDiffPrem)
	{
		DiffPrem = Arith.round(aDiffPrem,2);
	}
	public void setDiffPrem(String aDiffPrem)
	{
		if (aDiffPrem != null && !aDiffPrem.equals(""))
		{
			Double tDouble = new Double(aDiffPrem);
			double d = tDouble.doubleValue();
                DiffPrem = Arith.round(d,2);
		}
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getVFlag()
	{
		return VFlag;
	}
	public void setVFlag(String aVFlag)
	{
		VFlag = aVFlag;
	}
	public String getResult()
	{
		return Result;
	}
	public void setResult(String aResult)
	{
		Result = aResult;
	}
	public String getGetDate()
	{
		if( GetDate != null )
			return fDate.getString(GetDate);
		else
			return null;
	}
	public void setGetDate(Date aGetDate)
	{
		GetDate = aGetDate;
	}
	public void setGetDate(String aGetDate)
	{
		if (aGetDate != null && !aGetDate.equals("") )
		{
			GetDate = fDate.getDate( aGetDate );
		}
		else
			GetDate = null;
	}

	public String getReContCVD()
	{
		if( ReContCVD != null )
			return fDate.getString(ReContCVD);
		else
			return null;
	}
	public void setReContCVD(Date aReContCVD)
	{
		ReContCVD = aReContCVD;
	}
	public void setReContCVD(String aReContCVD)
	{
		if (aReContCVD != null && !aReContCVD.equals("") )
		{
			ReContCVD = fDate.getDate( aReContCVD );
		}
		else
			ReContCVD = null;
	}

	public String getStandby1()
	{
		return Standby1;
	}
	public void setStandby1(String aStandby1)
	{
		Standby1 = aStandby1;
	}
	public String getStandby2()
	{
		return Standby2;
	}
	public void setStandby2(String aStandby2)
	{
		Standby2 = aStandby2;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}

	/**
	* 使用另外一个 LRCessVerifySchema 对象给 Schema 赋值
	* @param: aLRCessVerifySchema LRCessVerifySchema
	**/
	public void setSchema(LRCessVerifySchema aLRCessVerifySchema)
	{
		this.GrpContNo = aLRCessVerifySchema.getGrpContNo();
		this.GrpPolNo = aLRCessVerifySchema.getGrpPolNo();
		this.RiskCode = aLRCessVerifySchema.getRiskCode();
		this.ReContState = aLRCessVerifySchema.getReContState();
		this.AccMoney = aLRCessVerifySchema.getAccMoney();
		this.GBPrem = aLRCessVerifySchema.getGBPrem();
		this.RePrem = aLRCessVerifySchema.getRePrem();
		this.CessPrem = aLRCessVerifySchema.getCessPrem();
		this.DiffPrem = aLRCessVerifySchema.getDiffPrem();
		this.State = aLRCessVerifySchema.getState();
		this.VFlag = aLRCessVerifySchema.getVFlag();
		this.Result = aLRCessVerifySchema.getResult();
		this.GetDate = fDate.getDate( aLRCessVerifySchema.getGetDate());
		this.ReContCVD = fDate.getDate( aLRCessVerifySchema.getReContCVD());
		this.Standby1 = aLRCessVerifySchema.getStandby1();
		this.Standby2 = aLRCessVerifySchema.getStandby2();
		this.Operator = aLRCessVerifySchema.getOperator();
		this.MakeDate = fDate.getDate( aLRCessVerifySchema.getMakeDate());
		this.MakeTime = aLRCessVerifySchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLRCessVerifySchema.getModifyDate());
		this.ModifyTime = aLRCessVerifySchema.getModifyTime();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("GrpContNo") == null )
				this.GrpContNo = null;
			else
				this.GrpContNo = rs.getString("GrpContNo").trim();

			if( rs.getString("GrpPolNo") == null )
				this.GrpPolNo = null;
			else
				this.GrpPolNo = rs.getString("GrpPolNo").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("ReContState") == null )
				this.ReContState = null;
			else
				this.ReContState = rs.getString("ReContState").trim();

			this.AccMoney = rs.getDouble("AccMoney");
			this.GBPrem = rs.getDouble("GBPrem");
			this.RePrem = rs.getDouble("RePrem");
			this.CessPrem = rs.getDouble("CessPrem");
			this.DiffPrem = rs.getDouble("DiffPrem");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("VFlag") == null )
				this.VFlag = null;
			else
				this.VFlag = rs.getString("VFlag").trim();

			if( rs.getString("Result") == null )
				this.Result = null;
			else
				this.Result = rs.getString("Result").trim();

			this.GetDate = rs.getDate("GetDate");
			this.ReContCVD = rs.getDate("ReContCVD");
			if( rs.getString("Standby1") == null )
				this.Standby1 = null;
			else
				this.Standby1 = rs.getString("Standby1").trim();

			if( rs.getString("Standby2") == null )
				this.Standby2 = null;
			else
				this.Standby2 = rs.getString("Standby2").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LRCessVerify表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRCessVerifySchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LRCessVerifySchema getSchema()
	{
		LRCessVerifySchema aLRCessVerifySchema = new LRCessVerifySchema();
		aLRCessVerifySchema.setSchema(this);
		return aLRCessVerifySchema;
	}

	public LRCessVerifyDB getDB()
	{
		LRCessVerifyDB aDBOper = new LRCessVerifyDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRCessVerify描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(GrpContNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpPolNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ReContState)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(AccMoney));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(GBPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(RePrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(CessPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(DiffPrem));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(VFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Result)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( GetDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ReContCVD ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standby1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Standby2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLRCessVerify>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			GrpContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			GrpPolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			ReContState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			AccMoney = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,5,SysConst.PACKAGESPILTER))).doubleValue();
			GBPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			RePrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			CessPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,8,SysConst.PACKAGESPILTER))).doubleValue();
			DiffPrem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,9,SysConst.PACKAGESPILTER))).doubleValue();
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			VFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Result = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			GetDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			ReContCVD = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			Standby1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			Standby2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LRCessVerifySchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("GrpContNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpContNo));
		}
		if (FCode.equals("GrpPolNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpPolNo));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("ReContState"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ReContState));
		}
		if (FCode.equals("AccMoney"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(AccMoney));
		}
		if (FCode.equals("GBPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GBPrem));
		}
		if (FCode.equals("RePrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RePrem));
		}
		if (FCode.equals("CessPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(CessPrem));
		}
		if (FCode.equals("DiffPrem"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DiffPrem));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("VFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(VFlag));
		}
		if (FCode.equals("Result"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Result));
		}
		if (FCode.equals("GetDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getGetDate()));
		}
		if (FCode.equals("ReContCVD"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getReContCVD()));
		}
		if (FCode.equals("Standby1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standby1));
		}
		if (FCode.equals("Standby2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Standby2));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(GrpContNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(GrpPolNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(ReContState);
				break;
			case 4:
				strFieldValue = String.valueOf(AccMoney);
				break;
			case 5:
				strFieldValue = String.valueOf(GBPrem);
				break;
			case 6:
				strFieldValue = String.valueOf(RePrem);
				break;
			case 7:
				strFieldValue = String.valueOf(CessPrem);
				break;
			case 8:
				strFieldValue = String.valueOf(DiffPrem);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(VFlag);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Result);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getGetDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getReContCVD()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(Standby1);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(Standby2);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 20:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("GrpContNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpContNo = FValue.trim();
			}
			else
				GrpContNo = null;
		}
		if (FCode.equalsIgnoreCase("GrpPolNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpPolNo = FValue.trim();
			}
			else
				GrpPolNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("ReContState"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ReContState = FValue.trim();
			}
			else
				ReContState = null;
		}
		if (FCode.equalsIgnoreCase("AccMoney"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				AccMoney = d;
			}
		}
		if (FCode.equalsIgnoreCase("GBPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				GBPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("RePrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				RePrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("CessPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				CessPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("DiffPrem"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				DiffPrem = d;
			}
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("VFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				VFlag = FValue.trim();
			}
			else
				VFlag = null;
		}
		if (FCode.equalsIgnoreCase("Result"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Result = FValue.trim();
			}
			else
				Result = null;
		}
		if (FCode.equalsIgnoreCase("GetDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				GetDate = fDate.getDate( FValue );
			}
			else
				GetDate = null;
		}
		if (FCode.equalsIgnoreCase("ReContCVD"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ReContCVD = fDate.getDate( FValue );
			}
			else
				ReContCVD = null;
		}
		if (FCode.equalsIgnoreCase("Standby1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standby1 = FValue.trim();
			}
			else
				Standby1 = null;
		}
		if (FCode.equalsIgnoreCase("Standby2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Standby2 = FValue.trim();
			}
			else
				Standby2 = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LRCessVerifySchema other = (LRCessVerifySchema)otherObject;
		return
			(GrpContNo == null ? other.getGrpContNo() == null : GrpContNo.equals(other.getGrpContNo()))
			&& (GrpPolNo == null ? other.getGrpPolNo() == null : GrpPolNo.equals(other.getGrpPolNo()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (ReContState == null ? other.getReContState() == null : ReContState.equals(other.getReContState()))
			&& AccMoney == other.getAccMoney()
			&& GBPrem == other.getGBPrem()
			&& RePrem == other.getRePrem()
			&& CessPrem == other.getCessPrem()
			&& DiffPrem == other.getDiffPrem()
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (VFlag == null ? other.getVFlag() == null : VFlag.equals(other.getVFlag()))
			&& (Result == null ? other.getResult() == null : Result.equals(other.getResult()))
			&& (GetDate == null ? other.getGetDate() == null : fDate.getString(GetDate).equals(other.getGetDate()))
			&& (ReContCVD == null ? other.getReContCVD() == null : fDate.getString(ReContCVD).equals(other.getReContCVD()))
			&& (Standby1 == null ? other.getStandby1() == null : Standby1.equals(other.getStandby1()))
			&& (Standby2 == null ? other.getStandby2() == null : Standby2.equals(other.getStandby2()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return 0;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return 1;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 2;
		}
		if( strFieldName.equals("ReContState") ) {
			return 3;
		}
		if( strFieldName.equals("AccMoney") ) {
			return 4;
		}
		if( strFieldName.equals("GBPrem") ) {
			return 5;
		}
		if( strFieldName.equals("RePrem") ) {
			return 6;
		}
		if( strFieldName.equals("CessPrem") ) {
			return 7;
		}
		if( strFieldName.equals("DiffPrem") ) {
			return 8;
		}
		if( strFieldName.equals("State") ) {
			return 9;
		}
		if( strFieldName.equals("VFlag") ) {
			return 10;
		}
		if( strFieldName.equals("Result") ) {
			return 11;
		}
		if( strFieldName.equals("GetDate") ) {
			return 12;
		}
		if( strFieldName.equals("ReContCVD") ) {
			return 13;
		}
		if( strFieldName.equals("Standby1") ) {
			return 14;
		}
		if( strFieldName.equals("Standby2") ) {
			return 15;
		}
		if( strFieldName.equals("Operator") ) {
			return 16;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 17;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 18;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 19;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 20;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "GrpContNo";
				break;
			case 1:
				strFieldName = "GrpPolNo";
				break;
			case 2:
				strFieldName = "RiskCode";
				break;
			case 3:
				strFieldName = "ReContState";
				break;
			case 4:
				strFieldName = "AccMoney";
				break;
			case 5:
				strFieldName = "GBPrem";
				break;
			case 6:
				strFieldName = "RePrem";
				break;
			case 7:
				strFieldName = "CessPrem";
				break;
			case 8:
				strFieldName = "DiffPrem";
				break;
			case 9:
				strFieldName = "State";
				break;
			case 10:
				strFieldName = "VFlag";
				break;
			case 11:
				strFieldName = "Result";
				break;
			case 12:
				strFieldName = "GetDate";
				break;
			case 13:
				strFieldName = "ReContCVD";
				break;
			case 14:
				strFieldName = "Standby1";
				break;
			case 15:
				strFieldName = "Standby2";
				break;
			case 16:
				strFieldName = "Operator";
				break;
			case 17:
				strFieldName = "MakeDate";
				break;
			case 18:
				strFieldName = "MakeTime";
				break;
			case 19:
				strFieldName = "ModifyDate";
				break;
			case 20:
				strFieldName = "ModifyTime";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("GrpContNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpPolNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ReContState") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("AccMoney") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("GBPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("RePrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("CessPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("DiffPrem") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("VFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Result") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GetDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ReContCVD") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("Standby1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Standby2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 8:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 20:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
