/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LCContReceiveDB;

/*
 * <p>ClassName: LCContReceiveSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2006-01-19
 */
public class LCContReceiveSchema implements Schema, Cloneable {
    // @Field
    /** 接收序号 */
    private double ReceiveID;
    /** 合同号码 */
    private String ContNo;
    /** 印刷号码 */
    private String PrtNo;
    /** 总单类型 */
    private String ContType;
    /** 保单生效日期 */
    private Date CValiDate;
    /** 投保人客户号码 */
    private String AppntNo;
    /** 投保人名称 */
    private String AppntName;
    /** 保费 */
    private double Prem;
    /** 代理人编码 */
    private String AgentCode;
    /** 代理人姓名 */
    private String AgentName;
    /** 签单日期 */
    private Date SignDate;
    /** 保单管理机构 */
    private String ManageCom;
    /** 保单打印管理机构 */
    private String PrintManageCom;
    /** 保单打印操作人员 */
    private String PrintOperator;
    /** 保单打印日期 */
    private Date PrintDate;
    /** 保单打印时间 */
    private String PrintTime;
    /** 打印次数 */
    private int PrintCount;
    /** 保单接收管理机构 */
    private String ReceiveManageCom;
    /** 保单接收操作人员 */
    private String ReceiveOperator;
    /** 保单接收日期 */
    private Date ReceiveDate;
    /** 保单接收时间 */
    private String ReceiveTime;
    /** 接收状态 */
    private String ReceiveState;
    /** 退回原因代码 */
    private String BackReasonCode;
    /** 退回原因 */
    private String BackReason;
    /** 处理状态 */
    private String DealState;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 29; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LCContReceiveSchema() {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "ReceiveID";
        pk[1] = "ContNo";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LCContReceiveSchema cloned = (LCContReceiveSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public double getReceiveID() {
        return ReceiveID;
    }

    public void setReceiveID(double aReceiveID) {
        ReceiveID = Arith.round(aReceiveID, 0);
    }

    public void setReceiveID(String aReceiveID) {
        if (aReceiveID != null && !aReceiveID.equals("")) {
            Double tDouble = new Double(aReceiveID);
            double d = tDouble.doubleValue();
            ReceiveID = Arith.round(d, 0);
        }
    }

    public String getContNo() {
        return ContNo;
    }

    public void setContNo(String aContNo) {
        ContNo = aContNo;
    }

    public String getPrtNo() {
        return PrtNo;
    }

    public void setPrtNo(String aPrtNo) {
        PrtNo = aPrtNo;
    }

    public String getContType() {
        return ContType;
    }

    public void setContType(String aContType) {
        ContType = aContType;
    }

    public String getCValiDate() {
        if (CValiDate != null) {
            return fDate.getString(CValiDate);
        } else {
            return null;
        }
    }

    public void setCValiDate(Date aCValiDate) {
        CValiDate = aCValiDate;
    }

    public void setCValiDate(String aCValiDate) {
        if (aCValiDate != null && !aCValiDate.equals("")) {
            CValiDate = fDate.getDate(aCValiDate);
        } else {
            CValiDate = null;
        }
    }

    public String getAppntNo() {
        return AppntNo;
    }

    public void setAppntNo(String aAppntNo) {
        AppntNo = aAppntNo;
    }

    public String getAppntName() {
        return AppntName;
    }

    public void setAppntName(String aAppntName) {
        AppntName = aAppntName;
    }

    public double getPrem() {
        return Prem;
    }

    public void setPrem(double aPrem) {
        Prem = Arith.round(aPrem, 2);
    }

    public void setPrem(String aPrem) {
        if (aPrem != null && !aPrem.equals("")) {
            Double tDouble = new Double(aPrem);
            double d = tDouble.doubleValue();
            Prem = Arith.round(d, 2);
        }
    }

    public String getAgentCode() {
        return AgentCode;
    }

    public void setAgentCode(String aAgentCode) {
        AgentCode = aAgentCode;
    }

    public String getAgentName() {
        return AgentName;
    }

    public void setAgentName(String aAgentName) {
        AgentName = aAgentName;
    }

    public String getSignDate() {
        if (SignDate != null) {
            return fDate.getString(SignDate);
        } else {
            return null;
        }
    }

    public void setSignDate(Date aSignDate) {
        SignDate = aSignDate;
    }

    public void setSignDate(String aSignDate) {
        if (aSignDate != null && !aSignDate.equals("")) {
            SignDate = fDate.getDate(aSignDate);
        } else {
            SignDate = null;
        }
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getPrintManageCom() {
        return PrintManageCom;
    }

    public void setPrintManageCom(String aPrintManageCom) {
        PrintManageCom = aPrintManageCom;
    }

    public String getPrintOperator() {
        return PrintOperator;
    }

    public void setPrintOperator(String aPrintOperator) {
        PrintOperator = aPrintOperator;
    }

    public String getPrintDate() {
        if (PrintDate != null) {
            return fDate.getString(PrintDate);
        } else {
            return null;
        }
    }

    public void setPrintDate(Date aPrintDate) {
        PrintDate = aPrintDate;
    }

    public void setPrintDate(String aPrintDate) {
        if (aPrintDate != null && !aPrintDate.equals("")) {
            PrintDate = fDate.getDate(aPrintDate);
        } else {
            PrintDate = null;
        }
    }

    public String getPrintTime() {
        return PrintTime;
    }

    public void setPrintTime(String aPrintTime) {
        PrintTime = aPrintTime;
    }

    public int getPrintCount() {
        return PrintCount;
    }

    public void setPrintCount(int aPrintCount) {
        PrintCount = aPrintCount;
    }

    public void setPrintCount(String aPrintCount) {
        if (aPrintCount != null && !aPrintCount.equals("")) {
            Integer tInteger = new Integer(aPrintCount);
            int i = tInteger.intValue();
            PrintCount = i;
        }
    }

    public String getReceiveManageCom() {
        return ReceiveManageCom;
    }

    public void setReceiveManageCom(String aReceiveManageCom) {
        ReceiveManageCom = aReceiveManageCom;
    }

    public String getReceiveOperator() {
        return ReceiveOperator;
    }

    public void setReceiveOperator(String aReceiveOperator) {
        ReceiveOperator = aReceiveOperator;
    }

    public String getReceiveDate() {
        if (ReceiveDate != null) {
            return fDate.getString(ReceiveDate);
        } else {
            return null;
        }
    }

    public void setReceiveDate(Date aReceiveDate) {
        ReceiveDate = aReceiveDate;
    }

    public void setReceiveDate(String aReceiveDate) {
        if (aReceiveDate != null && !aReceiveDate.equals("")) {
            ReceiveDate = fDate.getDate(aReceiveDate);
        } else {
            ReceiveDate = null;
        }
    }

    public String getReceiveTime() {
        return ReceiveTime;
    }

    public void setReceiveTime(String aReceiveTime) {
        ReceiveTime = aReceiveTime;
    }

    public String getReceiveState() {
        return ReceiveState;
    }

    public void setReceiveState(String aReceiveState) {
        ReceiveState = aReceiveState;
    }

    public String getBackReasonCode() {
        return BackReasonCode;
    }

    public void setBackReasonCode(String aBackReasonCode) {
        BackReasonCode = aBackReasonCode;
    }

    public String getBackReason() {
        return BackReason;
    }

    public void setBackReason(String aBackReason) {
        BackReason = aBackReason;
    }

    public String getDealState() {
        return DealState;
    }

    public void setDealState(String aDealState) {
        DealState = aDealState;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LCContReceiveSchema 对象给 Schema 赋值
     * @param: aLCContReceiveSchema LCContReceiveSchema
     **/
    public void setSchema(LCContReceiveSchema aLCContReceiveSchema) {
        this.ReceiveID = aLCContReceiveSchema.getReceiveID();
        this.ContNo = aLCContReceiveSchema.getContNo();
        this.PrtNo = aLCContReceiveSchema.getPrtNo();
        this.ContType = aLCContReceiveSchema.getContType();
        this.CValiDate = fDate.getDate(aLCContReceiveSchema.getCValiDate());
        this.AppntNo = aLCContReceiveSchema.getAppntNo();
        this.AppntName = aLCContReceiveSchema.getAppntName();
        this.Prem = aLCContReceiveSchema.getPrem();
        this.AgentCode = aLCContReceiveSchema.getAgentCode();
        this.AgentName = aLCContReceiveSchema.getAgentName();
        this.SignDate = fDate.getDate(aLCContReceiveSchema.getSignDate());
        this.ManageCom = aLCContReceiveSchema.getManageCom();
        this.PrintManageCom = aLCContReceiveSchema.getPrintManageCom();
        this.PrintOperator = aLCContReceiveSchema.getPrintOperator();
        this.PrintDate = fDate.getDate(aLCContReceiveSchema.getPrintDate());
        this.PrintTime = aLCContReceiveSchema.getPrintTime();
        this.PrintCount = aLCContReceiveSchema.getPrintCount();
        this.ReceiveManageCom = aLCContReceiveSchema.getReceiveManageCom();
        this.ReceiveOperator = aLCContReceiveSchema.getReceiveOperator();
        this.ReceiveDate = fDate.getDate(aLCContReceiveSchema.getReceiveDate());
        this.ReceiveTime = aLCContReceiveSchema.getReceiveTime();
        this.ReceiveState = aLCContReceiveSchema.getReceiveState();
        this.BackReasonCode = aLCContReceiveSchema.getBackReasonCode();
        this.BackReason = aLCContReceiveSchema.getBackReason();
        this.DealState = aLCContReceiveSchema.getDealState();
        this.MakeDate = fDate.getDate(aLCContReceiveSchema.getMakeDate());
        this.MakeTime = aLCContReceiveSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLCContReceiveSchema.getModifyDate());
        this.ModifyTime = aLCContReceiveSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            this.ReceiveID = rs.getDouble("ReceiveID");
            if (rs.getString("ContNo") == null) {
                this.ContNo = null;
            } else {
                this.ContNo = rs.getString("ContNo").trim();
            }

            if (rs.getString("PrtNo") == null) {
                this.PrtNo = null;
            } else {
                this.PrtNo = rs.getString("PrtNo").trim();
            }

            if (rs.getString("ContType") == null) {
                this.ContType = null;
            } else {
                this.ContType = rs.getString("ContType").trim();
            }

            this.CValiDate = rs.getDate("CValiDate");
            if (rs.getString("AppntNo") == null) {
                this.AppntNo = null;
            } else {
                this.AppntNo = rs.getString("AppntNo").trim();
            }

            if (rs.getString("AppntName") == null) {
                this.AppntName = null;
            } else {
                this.AppntName = rs.getString("AppntName").trim();
            }

            this.Prem = rs.getDouble("Prem");
            if (rs.getString("AgentCode") == null) {
                this.AgentCode = null;
            } else {
                this.AgentCode = rs.getString("AgentCode").trim();
            }

            if (rs.getString("AgentName") == null) {
                this.AgentName = null;
            } else {
                this.AgentName = rs.getString("AgentName").trim();
            }

            this.SignDate = rs.getDate("SignDate");
            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("PrintManageCom") == null) {
                this.PrintManageCom = null;
            } else {
                this.PrintManageCom = rs.getString("PrintManageCom").trim();
            }

            if (rs.getString("PrintOperator") == null) {
                this.PrintOperator = null;
            } else {
                this.PrintOperator = rs.getString("PrintOperator").trim();
            }

            this.PrintDate = rs.getDate("PrintDate");
            if (rs.getString("PrintTime") == null) {
                this.PrintTime = null;
            } else {
                this.PrintTime = rs.getString("PrintTime").trim();
            }

            this.PrintCount = rs.getInt("PrintCount");
            if (rs.getString("ReceiveManageCom") == null) {
                this.ReceiveManageCom = null;
            } else {
                this.ReceiveManageCom = rs.getString("ReceiveManageCom").trim();
            }

            if (rs.getString("ReceiveOperator") == null) {
                this.ReceiveOperator = null;
            } else {
                this.ReceiveOperator = rs.getString("ReceiveOperator").trim();
            }

            this.ReceiveDate = rs.getDate("ReceiveDate");
            if (rs.getString("ReceiveTime") == null) {
                this.ReceiveTime = null;
            } else {
                this.ReceiveTime = rs.getString("ReceiveTime").trim();
            }

            if (rs.getString("ReceiveState") == null) {
                this.ReceiveState = null;
            } else {
                this.ReceiveState = rs.getString("ReceiveState").trim();
            }

            if (rs.getString("BackReasonCode") == null) {
                this.BackReasonCode = null;
            } else {
                this.BackReasonCode = rs.getString("BackReasonCode").trim();
            }

            if (rs.getString("BackReason") == null) {
                this.BackReason = null;
            } else {
                this.BackReason = rs.getString("BackReason").trim();
            }

            if (rs.getString("DealState") == null) {
                this.DealState = null;
            } else {
                this.DealState = rs.getString("DealState").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LCContReceive表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContReceiveSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LCContReceiveSchema getSchema() {
        LCContReceiveSchema aLCContReceiveSchema = new LCContReceiveSchema();
        aLCContReceiveSchema.setSchema(this);
        return aLCContReceiveSchema;
    }

    public LCContReceiveDB getDB() {
        LCContReceiveDB aDBOper = new LCContReceiveDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContReceive描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(ChgData.chgData(ReceiveID));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrtNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ContType));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(CValiDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntNo));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AppntName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(Prem));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AgentName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(SignDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrintManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrintOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(PrintDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(PrintTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(ChgData.chgData(PrintCount));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveOperator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ReceiveDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ReceiveState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BackReasonCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(BackReason));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(DealState));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLCContReceive>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            ReceiveID = new Double(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 1, SysConst.PACKAGESPILTER))).doubleValue();
            ContNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                    SysConst.PACKAGESPILTER);
            PrtNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                   SysConst.PACKAGESPILTER);
            ContType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                      SysConst.PACKAGESPILTER);
            CValiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 5, SysConst.PACKAGESPILTER));
            AppntNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                     SysConst.PACKAGESPILTER);
            AppntName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                       SysConst.PACKAGESPILTER);
            Prem = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    8, SysConst.PACKAGESPILTER))).doubleValue();
            AgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                       SysConst.PACKAGESPILTER);
            AgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                       SysConst.PACKAGESPILTER);
            SignDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 11, SysConst.PACKAGESPILTER));
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                       SysConst.PACKAGESPILTER);
            PrintManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            13, SysConst.PACKAGESPILTER);
            PrintOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,
                                           SysConst.PACKAGESPILTER);
            PrintDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 15, SysConst.PACKAGESPILTER));
            PrintTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16,
                                       SysConst.PACKAGESPILTER);
            PrintCount = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 17, SysConst.PACKAGESPILTER))).intValue();
            ReceiveManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              18, SysConst.PACKAGESPILTER);
            ReceiveOperator = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                             19, SysConst.PACKAGESPILTER);
            ReceiveDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 20, SysConst.PACKAGESPILTER));
            ReceiveTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 21,
                                         SysConst.PACKAGESPILTER);
            ReceiveState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 22,
                                          SysConst.PACKAGESPILTER);
            BackReasonCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                            23, SysConst.PACKAGESPILTER);
            BackReason = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 24,
                                        SysConst.PACKAGESPILTER);
            DealState = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 25,
                                       SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 26, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 27,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 28, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 29,
                                        SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCContReceiveSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("ReceiveID")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveID));
        }
        if (FCode.equals("ContNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContNo));
        }
        if (FCode.equals("PrtNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrtNo));
        }
        if (FCode.equals("ContType")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ContType));
        }
        if (FCode.equals("CValiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getCValiDate()));
        }
        if (FCode.equals("AppntNo")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntNo));
        }
        if (FCode.equals("AppntName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AppntName));
        }
        if (FCode.equals("Prem")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Prem));
        }
        if (FCode.equals("AgentCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentCode));
        }
        if (FCode.equals("AgentName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AgentName));
        }
        if (FCode.equals("SignDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getSignDate()));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("PrintManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintManageCom));
        }
        if (FCode.equals("PrintOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintOperator));
        }
        if (FCode.equals("PrintDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getPrintDate()));
        }
        if (FCode.equals("PrintTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintTime));
        }
        if (FCode.equals("PrintCount")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(PrintCount));
        }
        if (FCode.equals("ReceiveManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveManageCom));
        }
        if (FCode.equals("ReceiveOperator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveOperator));
        }
        if (FCode.equals("ReceiveDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getReceiveDate()));
        }
        if (FCode.equals("ReceiveTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveTime));
        }
        if (FCode.equals("ReceiveState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ReceiveState));
        }
        if (FCode.equals("BackReasonCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackReasonCode));
        }
        if (FCode.equals("BackReason")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(BackReason));
        }
        if (FCode.equals("DealState")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(DealState));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = String.valueOf(ReceiveID);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(ContNo);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(PrtNo);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(ContType);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getCValiDate()));
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(AppntNo);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(AppntName);
            break;
        case 7:
            strFieldValue = String.valueOf(Prem);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(AgentCode);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(AgentName);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getSignDate()));
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(PrintManageCom);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(PrintOperator);
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getPrintDate()));
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(PrintTime);
            break;
        case 16:
            strFieldValue = String.valueOf(PrintCount);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(ReceiveManageCom);
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(ReceiveOperator);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getReceiveDate()));
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(ReceiveTime);
            break;
        case 21:
            strFieldValue = StrTool.GBKToUnicode(ReceiveState);
            break;
        case 22:
            strFieldValue = StrTool.GBKToUnicode(BackReasonCode);
            break;
        case 23:
            strFieldValue = StrTool.GBKToUnicode(BackReason);
            break;
        case 24:
            strFieldValue = StrTool.GBKToUnicode(DealState);
            break;
        case 25:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 26:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 27:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 28:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("ReceiveID")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                ReceiveID = d;
            }
        }
        if (FCode.equalsIgnoreCase("ContNo")) {
            if (FValue != null && !FValue.equals("")) {
                ContNo = FValue.trim();
            } else {
                ContNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrtNo")) {
            if (FValue != null && !FValue.equals("")) {
                PrtNo = FValue.trim();
            } else {
                PrtNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("ContType")) {
            if (FValue != null && !FValue.equals("")) {
                ContType = FValue.trim();
            } else {
                ContType = null;
            }
        }
        if (FCode.equalsIgnoreCase("CValiDate")) {
            if (FValue != null && !FValue.equals("")) {
                CValiDate = fDate.getDate(FValue);
            } else {
                CValiDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppntNo")) {
            if (FValue != null && !FValue.equals("")) {
                AppntNo = FValue.trim();
            } else {
                AppntNo = null;
            }
        }
        if (FCode.equalsIgnoreCase("AppntName")) {
            if (FValue != null && !FValue.equals("")) {
                AppntName = FValue.trim();
            } else {
                AppntName = null;
            }
        }
        if (FCode.equalsIgnoreCase("Prem")) {
            if (FValue != null && !FValue.equals("")) {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Prem = d;
            }
        }
        if (FCode.equalsIgnoreCase("AgentCode")) {
            if (FValue != null && !FValue.equals("")) {
                AgentCode = FValue.trim();
            } else {
                AgentCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AgentName")) {
            if (FValue != null && !FValue.equals("")) {
                AgentName = FValue.trim();
            } else {
                AgentName = null;
            }
        }
        if (FCode.equalsIgnoreCase("SignDate")) {
            if (FValue != null && !FValue.equals("")) {
                SignDate = fDate.getDate(FValue);
            } else {
                SignDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrintManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                PrintManageCom = FValue.trim();
            } else {
                PrintManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrintOperator")) {
            if (FValue != null && !FValue.equals("")) {
                PrintOperator = FValue.trim();
            } else {
                PrintOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrintDate")) {
            if (FValue != null && !FValue.equals("")) {
                PrintDate = fDate.getDate(FValue);
            } else {
                PrintDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrintTime")) {
            if (FValue != null && !FValue.equals("")) {
                PrintTime = FValue.trim();
            } else {
                PrintTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("PrintCount")) {
            if (FValue != null && !FValue.equals("")) {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                PrintCount = i;
            }
        }
        if (FCode.equalsIgnoreCase("ReceiveManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ReceiveManageCom = FValue.trim();
            } else {
                ReceiveManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReceiveOperator")) {
            if (FValue != null && !FValue.equals("")) {
                ReceiveOperator = FValue.trim();
            } else {
                ReceiveOperator = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReceiveDate")) {
            if (FValue != null && !FValue.equals("")) {
                ReceiveDate = fDate.getDate(FValue);
            } else {
                ReceiveDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReceiveTime")) {
            if (FValue != null && !FValue.equals("")) {
                ReceiveTime = FValue.trim();
            } else {
                ReceiveTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ReceiveState")) {
            if (FValue != null && !FValue.equals("")) {
                ReceiveState = FValue.trim();
            } else {
                ReceiveState = null;
            }
        }
        if (FCode.equalsIgnoreCase("BackReasonCode")) {
            if (FValue != null && !FValue.equals("")) {
                BackReasonCode = FValue.trim();
            } else {
                BackReasonCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("BackReason")) {
            if (FValue != null && !FValue.equals("")) {
                BackReason = FValue.trim();
            } else {
                BackReason = null;
            }
        }
        if (FCode.equalsIgnoreCase("DealState")) {
            if (FValue != null && !FValue.equals("")) {
                DealState = FValue.trim();
            } else {
                DealState = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LCContReceiveSchema other = (LCContReceiveSchema) otherObject;
        return
                ReceiveID == other.getReceiveID()
                && ContNo.equals(other.getContNo())
                && PrtNo.equals(other.getPrtNo())
                && ContType.equals(other.getContType())
                && fDate.getString(CValiDate).equals(other.getCValiDate())
                && AppntNo.equals(other.getAppntNo())
                && AppntName.equals(other.getAppntName())
                && Prem == other.getPrem()
                && AgentCode.equals(other.getAgentCode())
                && AgentName.equals(other.getAgentName())
                && fDate.getString(SignDate).equals(other.getSignDate())
                && ManageCom.equals(other.getManageCom())
                && PrintManageCom.equals(other.getPrintManageCom())
                && PrintOperator.equals(other.getPrintOperator())
                && fDate.getString(PrintDate).equals(other.getPrintDate())
                && PrintTime.equals(other.getPrintTime())
                && PrintCount == other.getPrintCount()
                && ReceiveManageCom.equals(other.getReceiveManageCom())
                && ReceiveOperator.equals(other.getReceiveOperator())
                && fDate.getString(ReceiveDate).equals(other.getReceiveDate())
                && ReceiveTime.equals(other.getReceiveTime())
                && ReceiveState.equals(other.getReceiveState())
                && BackReasonCode.equals(other.getBackReasonCode())
                && BackReason.equals(other.getBackReason())
                && DealState.equals(other.getDealState())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("ReceiveID")) {
            return 0;
        }
        if (strFieldName.equals("ContNo")) {
            return 1;
        }
        if (strFieldName.equals("PrtNo")) {
            return 2;
        }
        if (strFieldName.equals("ContType")) {
            return 3;
        }
        if (strFieldName.equals("CValiDate")) {
            return 4;
        }
        if (strFieldName.equals("AppntNo")) {
            return 5;
        }
        if (strFieldName.equals("AppntName")) {
            return 6;
        }
        if (strFieldName.equals("Prem")) {
            return 7;
        }
        if (strFieldName.equals("AgentCode")) {
            return 8;
        }
        if (strFieldName.equals("AgentName")) {
            return 9;
        }
        if (strFieldName.equals("SignDate")) {
            return 10;
        }
        if (strFieldName.equals("ManageCom")) {
            return 11;
        }
        if (strFieldName.equals("PrintManageCom")) {
            return 12;
        }
        if (strFieldName.equals("PrintOperator")) {
            return 13;
        }
        if (strFieldName.equals("PrintDate")) {
            return 14;
        }
        if (strFieldName.equals("PrintTime")) {
            return 15;
        }
        if (strFieldName.equals("PrintCount")) {
            return 16;
        }
        if (strFieldName.equals("ReceiveManageCom")) {
            return 17;
        }
        if (strFieldName.equals("ReceiveOperator")) {
            return 18;
        }
        if (strFieldName.equals("ReceiveDate")) {
            return 19;
        }
        if (strFieldName.equals("ReceiveTime")) {
            return 20;
        }
        if (strFieldName.equals("ReceiveState")) {
            return 21;
        }
        if (strFieldName.equals("BackReasonCode")) {
            return 22;
        }
        if (strFieldName.equals("BackReason")) {
            return 23;
        }
        if (strFieldName.equals("DealState")) {
            return 24;
        }
        if (strFieldName.equals("MakeDate")) {
            return 25;
        }
        if (strFieldName.equals("MakeTime")) {
            return 26;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 27;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 28;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "ReceiveID";
            break;
        case 1:
            strFieldName = "ContNo";
            break;
        case 2:
            strFieldName = "PrtNo";
            break;
        case 3:
            strFieldName = "ContType";
            break;
        case 4:
            strFieldName = "CValiDate";
            break;
        case 5:
            strFieldName = "AppntNo";
            break;
        case 6:
            strFieldName = "AppntName";
            break;
        case 7:
            strFieldName = "Prem";
            break;
        case 8:
            strFieldName = "AgentCode";
            break;
        case 9:
            strFieldName = "AgentName";
            break;
        case 10:
            strFieldName = "SignDate";
            break;
        case 11:
            strFieldName = "ManageCom";
            break;
        case 12:
            strFieldName = "PrintManageCom";
            break;
        case 13:
            strFieldName = "PrintOperator";
            break;
        case 14:
            strFieldName = "PrintDate";
            break;
        case 15:
            strFieldName = "PrintTime";
            break;
        case 16:
            strFieldName = "PrintCount";
            break;
        case 17:
            strFieldName = "ReceiveManageCom";
            break;
        case 18:
            strFieldName = "ReceiveOperator";
            break;
        case 19:
            strFieldName = "ReceiveDate";
            break;
        case 20:
            strFieldName = "ReceiveTime";
            break;
        case 21:
            strFieldName = "ReceiveState";
            break;
        case 22:
            strFieldName = "BackReasonCode";
            break;
        case 23:
            strFieldName = "BackReason";
            break;
        case 24:
            strFieldName = "DealState";
            break;
        case 25:
            strFieldName = "MakeDate";
            break;
        case 26:
            strFieldName = "MakeTime";
            break;
        case 27:
            strFieldName = "ModifyDate";
            break;
        case 28:
            strFieldName = "ModifyTime";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("ReceiveID")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("ContNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrtNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ContType")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CValiDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("AppntNo")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppntName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Prem")) {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("AgentCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AgentName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SignDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrintManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrintOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrintDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("PrintTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PrintCount")) {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("ReceiveManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReceiveOperator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReceiveDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ReceiveTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ReceiveState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BackReasonCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("BackReason")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DealState")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_DOUBLE;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 14:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 15:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 16:
            nFieldType = Schema.TYPE_INT;
            break;
        case 17:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 21:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 22:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 23:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 24:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 25:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 26:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 27:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 28:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
