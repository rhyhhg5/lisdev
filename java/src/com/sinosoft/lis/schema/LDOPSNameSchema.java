/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LDOPSNameDB;
import com.sinosoft.utility.*;

/*
 * <p>ClassName: LDOPSNameSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 新增--5.23
 * @CreateDate：2005-05-23
 */
public class LDOPSNameSchema implements Schema, Cloneable
{
    // @Field
    /** 手术俗称 */
    private String OPSCommonName;
    /** 手术icd代码 */
    private String ICDOPSCode;
    /** 维护标记 */
    private String MainteinFlag;

    public static final int FIELDNUM = 3; // 数据库表的字段个数

    private static String[] PK; // 主键

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDOPSNameSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "OPSCommonName";
        pk[1] = "ICDOPSCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException
    {
        LDOPSNameSchema cloned = (LDOPSNameSchema)super.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getOPSCommonName()
    {
        if (SysConst.CHANGECHARSET && OPSCommonName != null &&
            !OPSCommonName.equals(""))
        {
            OPSCommonName = StrTool.unicodeToGBK(OPSCommonName);
        }
        return OPSCommonName;
    }

    public void setOPSCommonName(String aOPSCommonName)
    {
        OPSCommonName = aOPSCommonName;
    }

    public String getICDOPSCode()
    {
        if (SysConst.CHANGECHARSET && ICDOPSCode != null &&
            !ICDOPSCode.equals(""))
        {
            ICDOPSCode = StrTool.unicodeToGBK(ICDOPSCode);
        }
        return ICDOPSCode;
    }

    public void setICDOPSCode(String aICDOPSCode)
    {
        ICDOPSCode = aICDOPSCode;
    }

    public String getMainteinFlag()
    {
        if (SysConst.CHANGECHARSET && MainteinFlag != null &&
            !MainteinFlag.equals(""))
        {
            MainteinFlag = StrTool.unicodeToGBK(MainteinFlag);
        }
        return MainteinFlag;
    }

    public void setMainteinFlag(String aMainteinFlag)
    {
        MainteinFlag = aMainteinFlag;
    }

    /**
     * 使用另外一个 LDOPSNameSchema 对象给 Schema 赋值
     * @param: aLDOPSNameSchema LDOPSNameSchema
     **/
    public void setSchema(LDOPSNameSchema aLDOPSNameSchema)
    {
        this.OPSCommonName = aLDOPSNameSchema.getOPSCommonName();
        this.ICDOPSCode = aLDOPSNameSchema.getICDOPSCode();
        this.MainteinFlag = aLDOPSNameSchema.getMainteinFlag();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("OPSCommonName") == null)
            {
                this.OPSCommonName = null;
            }
            else
            {
                this.OPSCommonName = rs.getString("OPSCommonName").trim();
            }

            if (rs.getString("ICDOPSCode") == null)
            {
                this.ICDOPSCode = null;
            }
            else
            {
                this.ICDOPSCode = rs.getString("ICDOPSCode").trim();
            }

            if (rs.getString("MainteinFlag") == null)
            {
                this.MainteinFlag = null;
            }
            else
            {
                this.MainteinFlag = rs.getString("MainteinFlag").trim();
            }

        }
        catch (SQLException sqle)
        {
            System.out.println("数据库中的LDOPSName表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDOPSNameSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDOPSNameSchema getSchema()
    {
        LDOPSNameSchema aLDOPSNameSchema = new LDOPSNameSchema();
        aLDOPSNameSchema.setSchema(this);
        return aLDOPSNameSchema;
    }

    public LDOPSNameDB getDB()
    {
        LDOPSNameDB aDBOper = new LDOPSNameDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDOPSName描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode()
    {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(OPSCommonName)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(ICDOPSCode)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(StrTool.unicodeToGBK(MainteinFlag)));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDOPSName>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            OPSCommonName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                           SysConst.PACKAGESPILTER);
            ICDOPSCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                        SysConst.PACKAGESPILTER);
            MainteinFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                          SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDOPSNameSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("OPSCommonName"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(OPSCommonName));
        }
        if (FCode.equals("ICDOPSCode"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ICDOPSCode));
        }
        if (FCode.equals("MainteinFlag"))
        {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MainteinFlag));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(OPSCommonName);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(ICDOPSCode);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(MainteinFlag);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("OPSCommonName"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OPSCommonName = FValue.trim();
            }
            else
            {
                OPSCommonName = null;
            }
        }
        if (FCode.equals("ICDOPSCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ICDOPSCode = FValue.trim();
            }
            else
            {
                ICDOPSCode = null;
            }
        }
        if (FCode.equals("MainteinFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MainteinFlag = FValue.trim();
            }
            else
            {
                MainteinFlag = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LDOPSNameSchema other = (LDOPSNameSchema) otherObject;
        return
                OPSCommonName.equals(other.getOPSCommonName())
                && ICDOPSCode.equals(other.getICDOPSCode())
                && MainteinFlag.equals(other.getMainteinFlag());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("OPSCommonName"))
        {
            return 0;
        }
        if (strFieldName.equals("ICDOPSCode"))
        {
            return 1;
        }
        if (strFieldName.equals("MainteinFlag"))
        {
            return 2;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "OPSCommonName";
                break;
            case 1:
                strFieldName = "ICDOPSCode";
                break;
            case 2:
                strFieldName = "MainteinFlag";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("OPSCommonName"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ICDOPSCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MainteinFlag"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
