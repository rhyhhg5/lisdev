/*
 * <p>ClassName: LBPremToAccSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2005-01-11
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.sinosoft.lis.db.LBPremToAccDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LBPremToAccSchema implements Schema
{
    // @Field
    /** 批单号 */
    private String EdorNo;
    /** 保单险种号码 */
    private String PolNo;
    /** 责任编码 */
    private String DutyCode;
    /** 交费计划编码 */
    private String PayPlanCode;
    /** 保险帐户号码 */
    private String InsuAccNo;
    /** 保费项到该帐户比例 */
    private double Rate;
    /** 是否需要产生新账户 */
    private String NewFlag;
    /** 转入账户时的算法编码(现金) */
    private String CalCodeMoney;
    /** 转入账户时的算法编码(股份) */
    private String CalCodeUnit;
    /** 账户计算标志 */
    private String CalFlag;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;

    public static final int FIELDNUM = 15; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LBPremToAccSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "PolNo";
        pk[1] = "DutyCode";
        pk[2] = "PayPlanCode";
        pk[3] = "InsuAccNo";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getEdorNo()
    {
        if (EdorNo != null && !EdorNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorNo = StrTool.unicodeToGBK(EdorNo);
        }
        return EdorNo;
    }

    public void setEdorNo(String aEdorNo)
    {
        EdorNo = aEdorNo;
    }

    public String getPolNo()
    {
        if (PolNo != null && !PolNo.equals("") && SysConst.CHANGECHARSET == true)
        {
            PolNo = StrTool.unicodeToGBK(PolNo);
        }
        return PolNo;
    }

    public void setPolNo(String aPolNo)
    {
        PolNo = aPolNo;
    }

    public String getDutyCode()
    {
        if (DutyCode != null && !DutyCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            DutyCode = StrTool.unicodeToGBK(DutyCode);
        }
        return DutyCode;
    }

    public void setDutyCode(String aDutyCode)
    {
        DutyCode = aDutyCode;
    }

    public String getPayPlanCode()
    {
        if (PayPlanCode != null && !PayPlanCode.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            PayPlanCode = StrTool.unicodeToGBK(PayPlanCode);
        }
        return PayPlanCode;
    }

    public void setPayPlanCode(String aPayPlanCode)
    {
        PayPlanCode = aPayPlanCode;
    }

    public String getInsuAccNo()
    {
        if (InsuAccNo != null && !InsuAccNo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            InsuAccNo = StrTool.unicodeToGBK(InsuAccNo);
        }
        return InsuAccNo;
    }

    public void setInsuAccNo(String aInsuAccNo)
    {
        InsuAccNo = aInsuAccNo;
    }

    public double getRate()
    {
        return Rate;
    }

    public void setRate(double aRate)
    {
        Rate = aRate;
    }

    public void setRate(String aRate)
    {
        if (aRate != null && !aRate.equals(""))
        {
            Double tDouble = new Double(aRate);
            double d = tDouble.doubleValue();
            Rate = d;
        }
    }

    public String getNewFlag()
    {
        if (NewFlag != null && !NewFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            NewFlag = StrTool.unicodeToGBK(NewFlag);
        }
        return NewFlag;
    }

    public void setNewFlag(String aNewFlag)
    {
        NewFlag = aNewFlag;
    }

    public String getCalCodeMoney()
    {
        if (CalCodeMoney != null && !CalCodeMoney.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalCodeMoney = StrTool.unicodeToGBK(CalCodeMoney);
        }
        return CalCodeMoney;
    }

    public void setCalCodeMoney(String aCalCodeMoney)
    {
        CalCodeMoney = aCalCodeMoney;
    }

    public String getCalCodeUnit()
    {
        if (CalCodeUnit != null && !CalCodeUnit.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            CalCodeUnit = StrTool.unicodeToGBK(CalCodeUnit);
        }
        return CalCodeUnit;
    }

    public void setCalCodeUnit(String aCalCodeUnit)
    {
        CalCodeUnit = aCalCodeUnit;
    }

    public String getCalFlag()
    {
        if (CalFlag != null && !CalFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalFlag = StrTool.unicodeToGBK(CalFlag);
        }
        return CalFlag;
    }

    public void setCalFlag(String aCalFlag)
    {
        CalFlag = aCalFlag;
    }

    public String getOperator()
    {
        if (Operator != null && !Operator.equals("") && SysConst.CHANGECHARSET == true)
        {
            Operator = StrTool.unicodeToGBK(Operator);
        }
        return Operator;
    }

    public void setOperator(String aOperator)
    {
        Operator = aOperator;
    }

    public String getMakeDate()
    {
        if (MakeDate != null)
        {
            return fDate.getString(MakeDate);
        }
        else
        {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate)
    {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate)
    {
        if (aMakeDate != null && !aMakeDate.equals(""))
        {
            MakeDate = fDate.getDate(aMakeDate);
        }
        else
        {
            MakeDate = null;
        }
    }

    public String getMakeTime()
    {
        if (MakeTime != null && !MakeTime.equals("") && SysConst.CHANGECHARSET == true)
        {
            MakeTime = StrTool.unicodeToGBK(MakeTime);
        }
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime)
    {
        MakeTime = aMakeTime;
    }

    public String getModifyDate()
    {
        if (ModifyDate != null)
        {
            return fDate.getString(ModifyDate);
        }
        else
        {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate)
    {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate)
    {
        if (aModifyDate != null && !aModifyDate.equals(""))
        {
            ModifyDate = fDate.getDate(aModifyDate);
        }
        else
        {
            ModifyDate = null;
        }
    }

    public String getModifyTime()
    {
        if (ModifyTime != null && !ModifyTime.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            ModifyTime = StrTool.unicodeToGBK(ModifyTime);
        }
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime)
    {
        ModifyTime = aModifyTime;
    }

    /**
     * 使用另外一个 LBPremToAccSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LBPremToAccSchema aLBPremToAccSchema)
    {
        this.EdorNo = aLBPremToAccSchema.getEdorNo();
        this.PolNo = aLBPremToAccSchema.getPolNo();
        this.DutyCode = aLBPremToAccSchema.getDutyCode();
        this.PayPlanCode = aLBPremToAccSchema.getPayPlanCode();
        this.InsuAccNo = aLBPremToAccSchema.getInsuAccNo();
        this.Rate = aLBPremToAccSchema.getRate();
        this.NewFlag = aLBPremToAccSchema.getNewFlag();
        this.CalCodeMoney = aLBPremToAccSchema.getCalCodeMoney();
        this.CalCodeUnit = aLBPremToAccSchema.getCalCodeUnit();
        this.CalFlag = aLBPremToAccSchema.getCalFlag();
        this.Operator = aLBPremToAccSchema.getOperator();
        this.MakeDate = fDate.getDate(aLBPremToAccSchema.getMakeDate());
        this.MakeTime = aLBPremToAccSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLBPremToAccSchema.getModifyDate());
        this.ModifyTime = aLBPremToAccSchema.getModifyTime();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("EdorNo") == null)
            {
                this.EdorNo = null;
            }
            else
            {
                this.EdorNo = rs.getString("EdorNo").trim();
            }

            if (rs.getString("PolNo") == null)
            {
                this.PolNo = null;
            }
            else
            {
                this.PolNo = rs.getString("PolNo").trim();
            }

            if (rs.getString("DutyCode") == null)
            {
                this.DutyCode = null;
            }
            else
            {
                this.DutyCode = rs.getString("DutyCode").trim();
            }

            if (rs.getString("PayPlanCode") == null)
            {
                this.PayPlanCode = null;
            }
            else
            {
                this.PayPlanCode = rs.getString("PayPlanCode").trim();
            }

            if (rs.getString("InsuAccNo") == null)
            {
                this.InsuAccNo = null;
            }
            else
            {
                this.InsuAccNo = rs.getString("InsuAccNo").trim();
            }

            this.Rate = rs.getDouble("Rate");
            if (rs.getString("NewFlag") == null)
            {
                this.NewFlag = null;
            }
            else
            {
                this.NewFlag = rs.getString("NewFlag").trim();
            }

            if (rs.getString("CalCodeMoney") == null)
            {
                this.CalCodeMoney = null;
            }
            else
            {
                this.CalCodeMoney = rs.getString("CalCodeMoney").trim();
            }

            if (rs.getString("CalCodeUnit") == null)
            {
                this.CalCodeUnit = null;
            }
            else
            {
                this.CalCodeUnit = rs.getString("CalCodeUnit").trim();
            }

            if (rs.getString("CalFlag") == null)
            {
                this.CalFlag = null;
            }
            else
            {
                this.CalFlag = rs.getString("CalFlag").trim();
            }

            if (rs.getString("Operator") == null)
            {
                this.Operator = null;
            }
            else
            {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null)
            {
                this.MakeTime = null;
            }
            else
            {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null)
            {
                this.ModifyTime = null;
            }
            else
            {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBPremToAccSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LBPremToAccSchema getSchema()
    {
        LBPremToAccSchema aLBPremToAccSchema = new LBPremToAccSchema();
        aLBPremToAccSchema.setSchema(this);
        return aLBPremToAccSchema;
    }

    public LBPremToAccDB getDB()
    {
        LBPremToAccDB aDBOper = new LBPremToAccDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBPremToAcc描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(EdorNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PolNo)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(DutyCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(PayPlanCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(InsuAccNo)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(Rate) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(NewFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCodeMoney)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCodeUnit)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(Operator)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(MakeDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(MakeTime)) +
                    SysConst.PACKAGESPILTER
                    +
                    StrTool.cTrim(StrTool.unicodeToGBK(fDate.getString(
                            ModifyDate))) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(ModifyTime));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLBPremToAcc>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            EdorNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                    SysConst.PACKAGESPILTER);
            PolNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                   SysConst.PACKAGESPILTER);
            DutyCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            PayPlanCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                         SysConst.PACKAGESPILTER);
            InsuAccNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                       SysConst.PACKAGESPILTER);
            Rate = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,
                    6, SysConst.PACKAGESPILTER))).doubleValue();
            NewFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                     SysConst.PACKAGESPILTER);
            CalCodeMoney = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                          SysConst.PACKAGESPILTER);
            CalCodeUnit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                         SysConst.PACKAGESPILTER);
            CalFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 12, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                        SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LBPremToAccSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("EdorNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorNo));
        }
        if (FCode.equals("PolNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PolNo));
        }
        if (FCode.equals("DutyCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(DutyCode));
        }
        if (FCode.equals("PayPlanCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(PayPlanCode));
        }
        if (FCode.equals("InsuAccNo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(InsuAccNo));
        }
        if (FCode.equals("Rate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Rate));
        }
        if (FCode.equals("NewFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(NewFlag));
        }
        if (FCode.equals("CalCodeMoney"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCodeMoney));
        }
        if (FCode.equals("CalCodeUnit"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCodeUnit));
        }
        if (FCode.equals("CalFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalFlag));
        }
        if (FCode.equals("Operator"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(Operator));
        }
        if (FCode.equals("MakeDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(this.
                    getModifyDate()));
        }
        if (FCode.equals("ModifyTime"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(ModifyTime));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(EdorNo);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(PolNo);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(DutyCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(PayPlanCode);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(InsuAccNo);
                break;
            case 5:
                strFieldValue = String.valueOf(Rate);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(NewFlag);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(CalCodeMoney);
                break;
            case 8:
                strFieldValue = StrTool.GBKToUnicode(CalCodeUnit);
                break;
            case 9:
                strFieldValue = StrTool.GBKToUnicode(CalFlag);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(Operator);
                break;
            case 11:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getMakeDate()));
                break;
            case 12:
                strFieldValue = StrTool.GBKToUnicode(MakeTime);
                break;
            case 13:
                strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                        getModifyDate()));
                break;
            case 14:
                strFieldValue = StrTool.GBKToUnicode(ModifyTime);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("EdorNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorNo = FValue.trim();
            }
            else
            {
                EdorNo = null;
            }
        }
        if (FCode.equals("PolNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PolNo = FValue.trim();
            }
            else
            {
                PolNo = null;
            }
        }
        if (FCode.equals("DutyCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                DutyCode = FValue.trim();
            }
            else
            {
                DutyCode = null;
            }
        }
        if (FCode.equals("PayPlanCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                PayPlanCode = FValue.trim();
            }
            else
            {
                PayPlanCode = null;
            }
        }
        if (FCode.equals("InsuAccNo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                InsuAccNo = FValue.trim();
            }
            else
            {
                InsuAccNo = null;
            }
        }
        if (FCode.equals("Rate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Double tDouble = new Double(FValue);
                double d = tDouble.doubleValue();
                Rate = d;
            }
        }
        if (FCode.equals("NewFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                NewFlag = FValue.trim();
            }
            else
            {
                NewFlag = null;
            }
        }
        if (FCode.equals("CalCodeMoney"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCodeMoney = FValue.trim();
            }
            else
            {
                CalCodeMoney = null;
            }
        }
        if (FCode.equals("CalCodeUnit"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCodeUnit = FValue.trim();
            }
            else
            {
                CalCodeUnit = null;
            }
        }
        if (FCode.equals("CalFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalFlag = FValue.trim();
            }
            else
            {
                CalFlag = null;
            }
        }
        if (FCode.equals("Operator"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Operator = FValue.trim();
            }
            else
            {
                Operator = null;
            }
        }
        if (FCode.equals("MakeDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeDate = fDate.getDate(FValue);
            }
            else
            {
                MakeDate = null;
            }
        }
        if (FCode.equals("MakeTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                MakeTime = FValue.trim();
            }
            else
            {
                MakeTime = null;
            }
        }
        if (FCode.equals("ModifyDate"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyDate = fDate.getDate(FValue);
            }
            else
            {
                ModifyDate = null;
            }
        }
        if (FCode.equals("ModifyTime"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                ModifyTime = FValue.trim();
            }
            else
            {
                ModifyTime = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LBPremToAccSchema other = (LBPremToAccSchema) otherObject;
        return
                EdorNo.equals(other.getEdorNo())
                && PolNo.equals(other.getPolNo())
                && DutyCode.equals(other.getDutyCode())
                && PayPlanCode.equals(other.getPayPlanCode())
                && InsuAccNo.equals(other.getInsuAccNo())
                && Rate == other.getRate()
                && NewFlag.equals(other.getNewFlag())
                && CalCodeMoney.equals(other.getCalCodeMoney())
                && CalCodeUnit.equals(other.getCalCodeUnit())
                && CalFlag.equals(other.getCalFlag())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return 0;
        }
        if (strFieldName.equals("PolNo"))
        {
            return 1;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return 2;
        }
        if (strFieldName.equals("PayPlanCode"))
        {
            return 3;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return 4;
        }
        if (strFieldName.equals("Rate"))
        {
            return 5;
        }
        if (strFieldName.equals("NewFlag"))
        {
            return 6;
        }
        if (strFieldName.equals("CalCodeMoney"))
        {
            return 7;
        }
        if (strFieldName.equals("CalCodeUnit"))
        {
            return 8;
        }
        if (strFieldName.equals("CalFlag"))
        {
            return 9;
        }
        if (strFieldName.equals("Operator"))
        {
            return 10;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return 11;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return 12;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return 13;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return 14;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "EdorNo";
                break;
            case 1:
                strFieldName = "PolNo";
                break;
            case 2:
                strFieldName = "DutyCode";
                break;
            case 3:
                strFieldName = "PayPlanCode";
                break;
            case 4:
                strFieldName = "InsuAccNo";
                break;
            case 5:
                strFieldName = "Rate";
                break;
            case 6:
                strFieldName = "NewFlag";
                break;
            case 7:
                strFieldName = "CalCodeMoney";
                break;
            case 8:
                strFieldName = "CalCodeUnit";
                break;
            case 9:
                strFieldName = "CalFlag";
                break;
            case 10:
                strFieldName = "Operator";
                break;
            case 11:
                strFieldName = "MakeDate";
                break;
            case 12:
                strFieldName = "MakeTime";
                break;
            case 13:
                strFieldName = "ModifyDate";
                break;
            case 14:
                strFieldName = "ModifyTime";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("EdorNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PolNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("DutyCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("PayPlanCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("InsuAccNo"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Rate"))
        {
            return Schema.TYPE_DOUBLE;
        }
        if (strFieldName.equals("NewFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCodeMoney"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCodeUnit"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Operator"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate"))
        {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_DOUBLE;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 9:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 11:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 12:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 13:
                nFieldType = Schema.TYPE_DATE;
                break;
            case 14:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
