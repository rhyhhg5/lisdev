/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LGProjectPurviewDB;

/*
 * <p>ClassName: LGProjectPurviewSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PHYSICAL_DATA_MODEL_1
 * @CreateDate：2014-11-15
 */
public class LGProjectPurviewSchema implements Schema, Cloneable
{
	// @Field
	/** 项目权限编号 */
	private String ProjectPurviewNo;
	/** 项目名称编码 */
	private String ProjectNo;
	/** 项目类型 */
	private String ProjectType;
	/** 处理机构类型 */
	private String DealOrganization;
	/** 岗位编号 */
	private String PostNo;
	/** 金额权限 */
	private double MoneyPurview;
	/** 超额权限 */
	private double ExcessPurview;
	/** 期限权限 */
	private String TimePurview;
	/** 需互核标志 */
	private String NeedOtherAudit;
	/** 备注 */
	private String Remark;
	/** 操作员代码 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 确认权 */
	private String ConfirmFlag;
	/** 成员编码 */
	private String MemberNo;
	/** 险种代码 */
	private String RiskCode;
	/** 渠道 */
	private String SaleChnl;
	/** 状态 */
	private String StateFlag;

	public static final int FIELDNUM = 20;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LGProjectPurviewSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "ProjectPurviewNo";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LGProjectPurviewSchema cloned = (LGProjectPurviewSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getProjectPurviewNo()
	{
		return ProjectPurviewNo;
	}
	public void setProjectPurviewNo(String aProjectPurviewNo)
	{
		ProjectPurviewNo = aProjectPurviewNo;
	}
	public String getProjectNo()
	{
		return ProjectNo;
	}
	public void setProjectNo(String aProjectNo)
	{
		ProjectNo = aProjectNo;
	}
	public String getProjectType()
	{
		return ProjectType;
	}
	public void setProjectType(String aProjectType)
	{
		ProjectType = aProjectType;
	}
	public String getDealOrganization()
	{
		return DealOrganization;
	}
	public void setDealOrganization(String aDealOrganization)
	{
		DealOrganization = aDealOrganization;
	}
	public String getPostNo()
	{
		return PostNo;
	}
	public void setPostNo(String aPostNo)
	{
		PostNo = aPostNo;
	}
	public double getMoneyPurview()
	{
		return MoneyPurview;
	}
	public void setMoneyPurview(double aMoneyPurview)
	{
		MoneyPurview = Arith.round(aMoneyPurview,2);
	}
	public void setMoneyPurview(String aMoneyPurview)
	{
		if (aMoneyPurview != null && !aMoneyPurview.equals(""))
		{
			Double tDouble = new Double(aMoneyPurview);
			double d = tDouble.doubleValue();
                MoneyPurview = Arith.round(d,2);
		}
	}

	public double getExcessPurview()
	{
		return ExcessPurview;
	}
	public void setExcessPurview(double aExcessPurview)
	{
		ExcessPurview = Arith.round(aExcessPurview,2);
	}
	public void setExcessPurview(String aExcessPurview)
	{
		if (aExcessPurview != null && !aExcessPurview.equals(""))
		{
			Double tDouble = new Double(aExcessPurview);
			double d = tDouble.doubleValue();
                ExcessPurview = Arith.round(d,2);
		}
	}

	public String getTimePurview()
	{
		return TimePurview;
	}
	public void setTimePurview(String aTimePurview)
	{
		TimePurview = aTimePurview;
	}
	public String getNeedOtherAudit()
	{
		return NeedOtherAudit;
	}
	public void setNeedOtherAudit(String aNeedOtherAudit)
	{
		NeedOtherAudit = aNeedOtherAudit;
	}
	public String getRemark()
	{
		return Remark;
	}
	public void setRemark(String aRemark)
	{
		Remark = aRemark;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getConfirmFlag()
	{
		return ConfirmFlag;
	}
	public void setConfirmFlag(String aConfirmFlag)
	{
		ConfirmFlag = aConfirmFlag;
	}
	public String getMemberNo()
	{
		return MemberNo;
	}
	public void setMemberNo(String aMemberNo)
	{
		MemberNo = aMemberNo;
	}
	public String getRiskCode()
	{
		return RiskCode;
	}
	public void setRiskCode(String aRiskCode)
	{
		RiskCode = aRiskCode;
	}
	public String getSaleChnl()
	{
		return SaleChnl;
	}
	public void setSaleChnl(String aSaleChnl)
	{
		SaleChnl = aSaleChnl;
	}
	public String getStateFlag()
	{
		return StateFlag;
	}
	public void setStateFlag(String aStateFlag)
	{
		StateFlag = aStateFlag;
	}

	/**
	* 使用另外一个 LGProjectPurviewSchema 对象给 Schema 赋值
	* @param: aLGProjectPurviewSchema LGProjectPurviewSchema
	**/
	public void setSchema(LGProjectPurviewSchema aLGProjectPurviewSchema)
	{
		this.ProjectPurviewNo = aLGProjectPurviewSchema.getProjectPurviewNo();
		this.ProjectNo = aLGProjectPurviewSchema.getProjectNo();
		this.ProjectType = aLGProjectPurviewSchema.getProjectType();
		this.DealOrganization = aLGProjectPurviewSchema.getDealOrganization();
		this.PostNo = aLGProjectPurviewSchema.getPostNo();
		this.MoneyPurview = aLGProjectPurviewSchema.getMoneyPurview();
		this.ExcessPurview = aLGProjectPurviewSchema.getExcessPurview();
		this.TimePurview = aLGProjectPurviewSchema.getTimePurview();
		this.NeedOtherAudit = aLGProjectPurviewSchema.getNeedOtherAudit();
		this.Remark = aLGProjectPurviewSchema.getRemark();
		this.Operator = aLGProjectPurviewSchema.getOperator();
		this.MakeDate = fDate.getDate( aLGProjectPurviewSchema.getMakeDate());
		this.MakeTime = aLGProjectPurviewSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLGProjectPurviewSchema.getModifyDate());
		this.ModifyTime = aLGProjectPurviewSchema.getModifyTime();
		this.ConfirmFlag = aLGProjectPurviewSchema.getConfirmFlag();
		this.MemberNo = aLGProjectPurviewSchema.getMemberNo();
		this.RiskCode = aLGProjectPurviewSchema.getRiskCode();
		this.SaleChnl = aLGProjectPurviewSchema.getSaleChnl();
		this.StateFlag = aLGProjectPurviewSchema.getStateFlag();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("ProjectPurviewNo") == null )
				this.ProjectPurviewNo = null;
			else
				this.ProjectPurviewNo = rs.getString("ProjectPurviewNo").trim();

			if( rs.getString("ProjectNo") == null )
				this.ProjectNo = null;
			else
				this.ProjectNo = rs.getString("ProjectNo").trim();

			if( rs.getString("ProjectType") == null )
				this.ProjectType = null;
			else
				this.ProjectType = rs.getString("ProjectType").trim();

			if( rs.getString("DealOrganization") == null )
				this.DealOrganization = null;
			else
				this.DealOrganization = rs.getString("DealOrganization").trim();

			if( rs.getString("PostNo") == null )
				this.PostNo = null;
			else
				this.PostNo = rs.getString("PostNo").trim();

			this.MoneyPurview = rs.getDouble("MoneyPurview");
			this.ExcessPurview = rs.getDouble("ExcessPurview");
			if( rs.getString("TimePurview") == null )
				this.TimePurview = null;
			else
				this.TimePurview = rs.getString("TimePurview").trim();

			if( rs.getString("NeedOtherAudit") == null )
				this.NeedOtherAudit = null;
			else
				this.NeedOtherAudit = rs.getString("NeedOtherAudit").trim();

			if( rs.getString("Remark") == null )
				this.Remark = null;
			else
				this.Remark = rs.getString("Remark").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("ConfirmFlag") == null )
				this.ConfirmFlag = null;
			else
				this.ConfirmFlag = rs.getString("ConfirmFlag").trim();

			if( rs.getString("MemberNo") == null )
				this.MemberNo = null;
			else
				this.MemberNo = rs.getString("MemberNo").trim();

			if( rs.getString("RiskCode") == null )
				this.RiskCode = null;
			else
				this.RiskCode = rs.getString("RiskCode").trim();

			if( rs.getString("SaleChnl") == null )
				this.SaleChnl = null;
			else
				this.SaleChnl = rs.getString("SaleChnl").trim();

			if( rs.getString("StateFlag") == null )
				this.StateFlag = null;
			else
				this.StateFlag = rs.getString("StateFlag").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LGProjectPurview表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LGProjectPurviewSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LGProjectPurviewSchema getSchema()
	{
		LGProjectPurviewSchema aLGProjectPurviewSchema = new LGProjectPurviewSchema();
		aLGProjectPurviewSchema.setSchema(this);
		return aLGProjectPurviewSchema;
	}

	public LGProjectPurviewDB getDB()
	{
		LGProjectPurviewDB aDBOper = new LGProjectPurviewDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGProjectPurview描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(ProjectPurviewNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ProjectType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(DealOrganization)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(PostNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(MoneyPurview));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append( ChgData.chgData(ExcessPurview));strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(TimePurview)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(NeedOtherAudit)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Remark)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ConfirmFlag)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MemberNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(RiskCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StateFlag));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLGProjectPurview>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			ProjectPurviewNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ProjectNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			ProjectType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			DealOrganization = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			PostNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			MoneyPurview = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,6,SysConst.PACKAGESPILTER))).doubleValue();
			ExcessPurview = new Double(ChgData.chgNumericStr(StrTool.getStr(strMessage,7,SysConst.PACKAGESPILTER))).doubleValue();
			TimePurview = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			NeedOtherAudit = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Remark = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15, SysConst.PACKAGESPILTER );
			ConfirmFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			MemberNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
			StateFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LGProjectPurviewSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("ProjectPurviewNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectPurviewNo));
		}
		if (FCode.equals("ProjectNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectNo));
		}
		if (FCode.equals("ProjectType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ProjectType));
		}
		if (FCode.equals("DealOrganization"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(DealOrganization));
		}
		if (FCode.equals("PostNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(PostNo));
		}
		if (FCode.equals("MoneyPurview"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MoneyPurview));
		}
		if (FCode.equals("ExcessPurview"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ExcessPurview));
		}
		if (FCode.equals("TimePurview"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(TimePurview));
		}
		if (FCode.equals("NeedOtherAudit"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(NeedOtherAudit));
		}
		if (FCode.equals("Remark"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Remark));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("ConfirmFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ConfirmFlag));
		}
		if (FCode.equals("MemberNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MemberNo));
		}
		if (FCode.equals("RiskCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(RiskCode));
		}
		if (FCode.equals("SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(SaleChnl));
		}
		if (FCode.equals("StateFlag"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StateFlag));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(ProjectPurviewNo);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ProjectNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(ProjectType);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(DealOrganization);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(PostNo);
				break;
			case 5:
				strFieldValue = String.valueOf(MoneyPurview);
				break;
			case 6:
				strFieldValue = String.valueOf(ExcessPurview);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(TimePurview);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(NeedOtherAudit);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(Remark);
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ConfirmFlag);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(MemberNo);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(RiskCode);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(SaleChnl);
				break;
			case 19:
				strFieldValue = StrTool.GBKToUnicode(StateFlag);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("ProjectPurviewNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectPurviewNo = FValue.trim();
			}
			else
				ProjectPurviewNo = null;
		}
		if (FCode.equalsIgnoreCase("ProjectNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectNo = FValue.trim();
			}
			else
				ProjectNo = null;
		}
		if (FCode.equalsIgnoreCase("ProjectType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ProjectType = FValue.trim();
			}
			else
				ProjectType = null;
		}
		if (FCode.equalsIgnoreCase("DealOrganization"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				DealOrganization = FValue.trim();
			}
			else
				DealOrganization = null;
		}
		if (FCode.equalsIgnoreCase("PostNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				PostNo = FValue.trim();
			}
			else
				PostNo = null;
		}
		if (FCode.equalsIgnoreCase("MoneyPurview"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				MoneyPurview = d;
			}
		}
		if (FCode.equalsIgnoreCase("ExcessPurview"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Double tDouble = new Double( FValue );
				double d = tDouble.doubleValue();
				ExcessPurview = d;
			}
		}
		if (FCode.equalsIgnoreCase("TimePurview"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				TimePurview = FValue.trim();
			}
			else
				TimePurview = null;
		}
		if (FCode.equalsIgnoreCase("NeedOtherAudit"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				NeedOtherAudit = FValue.trim();
			}
			else
				NeedOtherAudit = null;
		}
		if (FCode.equalsIgnoreCase("Remark"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Remark = FValue.trim();
			}
			else
				Remark = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("ConfirmFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ConfirmFlag = FValue.trim();
			}
			else
				ConfirmFlag = null;
		}
		if (FCode.equalsIgnoreCase("MemberNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MemberNo = FValue.trim();
			}
			else
				MemberNo = null;
		}
		if (FCode.equalsIgnoreCase("RiskCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				RiskCode = FValue.trim();
			}
			else
				RiskCode = null;
		}
		if (FCode.equalsIgnoreCase("SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				SaleChnl = FValue.trim();
			}
			else
				SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("StateFlag"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StateFlag = FValue.trim();
			}
			else
				StateFlag = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LGProjectPurviewSchema other = (LGProjectPurviewSchema)otherObject;
		return
			(ProjectPurviewNo == null ? other.getProjectPurviewNo() == null : ProjectPurviewNo.equals(other.getProjectPurviewNo()))
			&& (ProjectNo == null ? other.getProjectNo() == null : ProjectNo.equals(other.getProjectNo()))
			&& (ProjectType == null ? other.getProjectType() == null : ProjectType.equals(other.getProjectType()))
			&& (DealOrganization == null ? other.getDealOrganization() == null : DealOrganization.equals(other.getDealOrganization()))
			&& (PostNo == null ? other.getPostNo() == null : PostNo.equals(other.getPostNo()))
			&& MoneyPurview == other.getMoneyPurview()
			&& ExcessPurview == other.getExcessPurview()
			&& (TimePurview == null ? other.getTimePurview() == null : TimePurview.equals(other.getTimePurview()))
			&& (NeedOtherAudit == null ? other.getNeedOtherAudit() == null : NeedOtherAudit.equals(other.getNeedOtherAudit()))
			&& (Remark == null ? other.getRemark() == null : Remark.equals(other.getRemark()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (ConfirmFlag == null ? other.getConfirmFlag() == null : ConfirmFlag.equals(other.getConfirmFlag()))
			&& (MemberNo == null ? other.getMemberNo() == null : MemberNo.equals(other.getMemberNo()))
			&& (RiskCode == null ? other.getRiskCode() == null : RiskCode.equals(other.getRiskCode()))
			&& (SaleChnl == null ? other.getSaleChnl() == null : SaleChnl.equals(other.getSaleChnl()))
			&& (StateFlag == null ? other.getStateFlag() == null : StateFlag.equals(other.getStateFlag()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("ProjectPurviewNo") ) {
			return 0;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return 1;
		}
		if( strFieldName.equals("ProjectType") ) {
			return 2;
		}
		if( strFieldName.equals("DealOrganization") ) {
			return 3;
		}
		if( strFieldName.equals("PostNo") ) {
			return 4;
		}
		if( strFieldName.equals("MoneyPurview") ) {
			return 5;
		}
		if( strFieldName.equals("ExcessPurview") ) {
			return 6;
		}
		if( strFieldName.equals("TimePurview") ) {
			return 7;
		}
		if( strFieldName.equals("NeedOtherAudit") ) {
			return 8;
		}
		if( strFieldName.equals("Remark") ) {
			return 9;
		}
		if( strFieldName.equals("Operator") ) {
			return 10;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 11;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 12;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 14;
		}
		if( strFieldName.equals("ConfirmFlag") ) {
			return 15;
		}
		if( strFieldName.equals("MemberNo") ) {
			return 16;
		}
		if( strFieldName.equals("RiskCode") ) {
			return 17;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return 18;
		}
		if( strFieldName.equals("StateFlag") ) {
			return 19;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "ProjectPurviewNo";
				break;
			case 1:
				strFieldName = "ProjectNo";
				break;
			case 2:
				strFieldName = "ProjectType";
				break;
			case 3:
				strFieldName = "DealOrganization";
				break;
			case 4:
				strFieldName = "PostNo";
				break;
			case 5:
				strFieldName = "MoneyPurview";
				break;
			case 6:
				strFieldName = "ExcessPurview";
				break;
			case 7:
				strFieldName = "TimePurview";
				break;
			case 8:
				strFieldName = "NeedOtherAudit";
				break;
			case 9:
				strFieldName = "Remark";
				break;
			case 10:
				strFieldName = "Operator";
				break;
			case 11:
				strFieldName = "MakeDate";
				break;
			case 12:
				strFieldName = "MakeTime";
				break;
			case 13:
				strFieldName = "ModifyDate";
				break;
			case 14:
				strFieldName = "ModifyTime";
				break;
			case 15:
				strFieldName = "ConfirmFlag";
				break;
			case 16:
				strFieldName = "MemberNo";
				break;
			case 17:
				strFieldName = "RiskCode";
				break;
			case 18:
				strFieldName = "SaleChnl";
				break;
			case 19:
				strFieldName = "StateFlag";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("ProjectPurviewNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ProjectType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("DealOrganization") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("PostNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MoneyPurview") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("ExcessPurview") ) {
			return Schema.TYPE_DOUBLE;
		}
		if( strFieldName.equals("TimePurview") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("NeedOtherAudit") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Remark") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ConfirmFlag") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MemberNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("RiskCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StateFlag") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 6:
				nFieldType = Schema.TYPE_DOUBLE;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 12:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 13:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 14:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 19:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
