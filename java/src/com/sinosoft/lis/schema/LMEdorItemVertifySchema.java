/*
 * <p>ClassName: LMEdorItemVertifySchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMEdorItemVertifyDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMEdorItemVertifySchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 保全项目编码 */
    private String EdorCode;
    /** 保全申请对象 */
    private String AppObj;
    /** 算法编码 */
    private String CalCode;
    /** 校验顺序 */
    private int VertifyOrder;
    /** 校验信息 */
    private String VertifyInfo;

    public static final int FIELDNUM = 7; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMEdorItemVertifySchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[4];
        pk[0] = "RiskCode";
        pk[1] = "EdorCode";
        pk[2] = "AppObj";
        pk[3] = "CalCode";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getEdorCode()
    {
        if (EdorCode != null && !EdorCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            EdorCode = StrTool.unicodeToGBK(EdorCode);
        }
        return EdorCode;
    }

    public void setEdorCode(String aEdorCode)
    {
        EdorCode = aEdorCode;
    }

    public String getAppObj()
    {
        if (AppObj != null && !AppObj.equals("") && SysConst.CHANGECHARSET == true)
        {
            AppObj = StrTool.unicodeToGBK(AppObj);
        }
        return AppObj;
    }

    public void setAppObj(String aAppObj)
    {
        AppObj = aAppObj;
    }

    public String getCalCode()
    {
        if (CalCode != null && !CalCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            CalCode = StrTool.unicodeToGBK(CalCode);
        }
        return CalCode;
    }

    public void setCalCode(String aCalCode)
    {
        CalCode = aCalCode;
    }

    public int getVertifyOrder()
    {
        return VertifyOrder;
    }

    public void setVertifyOrder(int aVertifyOrder)
    {
        VertifyOrder = aVertifyOrder;
    }

    public void setVertifyOrder(String aVertifyOrder)
    {
        if (aVertifyOrder != null && !aVertifyOrder.equals(""))
        {
            Integer tInteger = new Integer(aVertifyOrder);
            int i = tInteger.intValue();
            VertifyOrder = i;
        }
    }

    public String getVertifyInfo()
    {
        if (VertifyInfo != null && !VertifyInfo.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            VertifyInfo = StrTool.unicodeToGBK(VertifyInfo);
        }
        return VertifyInfo;
    }

    public void setVertifyInfo(String aVertifyInfo)
    {
        VertifyInfo = aVertifyInfo;
    }

    /**
     * 使用另外一个 LMEdorItemVertifySchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMEdorItemVertifySchema aLMEdorItemVertifySchema)
    {
        this.RiskCode = aLMEdorItemVertifySchema.getRiskCode();
        this.RiskVersion = aLMEdorItemVertifySchema.getRiskVersion();
        this.EdorCode = aLMEdorItemVertifySchema.getEdorCode();
        this.AppObj = aLMEdorItemVertifySchema.getAppObj();
        this.CalCode = aLMEdorItemVertifySchema.getCalCode();
        this.VertifyOrder = aLMEdorItemVertifySchema.getVertifyOrder();
        this.VertifyInfo = aLMEdorItemVertifySchema.getVertifyInfo();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("EdorCode") == null)
            {
                this.EdorCode = null;
            }
            else
            {
                this.EdorCode = rs.getString("EdorCode").trim();
            }

            if (rs.getString("AppObj") == null)
            {
                this.AppObj = null;
            }
            else
            {
                this.AppObj = rs.getString("AppObj").trim();
            }

            if (rs.getString("CalCode") == null)
            {
                this.CalCode = null;
            }
            else
            {
                this.CalCode = rs.getString("CalCode").trim();
            }

            this.VertifyOrder = rs.getInt("VertifyOrder");
            if (rs.getString("VertifyInfo") == null)
            {
                this.VertifyInfo = null;
            }
            else
            {
                this.VertifyInfo = rs.getString("VertifyInfo").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorItemVertifySchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMEdorItemVertifySchema getSchema()
    {
        LMEdorItemVertifySchema aLMEdorItemVertifySchema = new
                LMEdorItemVertifySchema();
        aLMEdorItemVertifySchema.setSchema(this);
        return aLMEdorItemVertifySchema;
    }

    public LMEdorItemVertifyDB getDB()
    {
        LMEdorItemVertifyDB aDBOper = new LMEdorItemVertifyDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorItemVertify描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(EdorCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(AppObj)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CalCode)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(VertifyOrder) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(VertifyInfo));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorItemVertify>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            EdorCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            AppObj = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                    SysConst.PACKAGESPILTER);
            CalCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            VertifyOrder = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).intValue();
            VertifyInfo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorItemVertifySchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("EdorCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(EdorCode));
        }
        if (FCode.equals("AppObj"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(AppObj));
        }
        if (FCode.equals("CalCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CalCode));
        }
        if (FCode.equals("VertifyOrder"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(VertifyOrder));
        }
        if (FCode.equals("VertifyInfo"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(VertifyInfo));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(EdorCode);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(AppObj);
                break;
            case 4:
                strFieldValue = StrTool.GBKToUnicode(CalCode);
                break;
            case 5:
                strFieldValue = String.valueOf(VertifyOrder);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(VertifyInfo);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("EdorCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                EdorCode = FValue.trim();
            }
            else
            {
                EdorCode = null;
            }
        }
        if (FCode.equals("AppObj"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                AppObj = FValue.trim();
            }
            else
            {
                AppObj = null;
            }
        }
        if (FCode.equals("CalCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CalCode = FValue.trim();
            }
            else
            {
                CalCode = null;
            }
        }
        if (FCode.equals("VertifyOrder"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                VertifyOrder = i;
            }
        }
        if (FCode.equals("VertifyInfo"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                VertifyInfo = FValue.trim();
            }
            else
            {
                VertifyInfo = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMEdorItemVertifySchema other = (LMEdorItemVertifySchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && EdorCode.equals(other.getEdorCode())
                && AppObj.equals(other.getAppObj())
                && CalCode.equals(other.getCalCode())
                && VertifyOrder == other.getVertifyOrder()
                && VertifyInfo.equals(other.getVertifyInfo());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 1;
        }
        if (strFieldName.equals("EdorCode"))
        {
            return 2;
        }
        if (strFieldName.equals("AppObj"))
        {
            return 3;
        }
        if (strFieldName.equals("CalCode"))
        {
            return 4;
        }
        if (strFieldName.equals("VertifyOrder"))
        {
            return 5;
        }
        if (strFieldName.equals("VertifyInfo"))
        {
            return 6;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVersion";
                break;
            case 2:
                strFieldName = "EdorCode";
                break;
            case 3:
                strFieldName = "AppObj";
                break;
            case 4:
                strFieldName = "CalCode";
                break;
            case 5:
                strFieldName = "VertifyOrder";
                break;
            case 6:
                strFieldName = "VertifyInfo";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("EdorCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AppObj"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CalCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("VertifyOrder"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("VertifyInfo"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
