/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.CIITC_BANK_CONFIGDB;

/*
 * <p>ClassName: CIITC_BANK_CONFIGSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2017-10-19
 */
public class CIITC_BANK_CONFIGSchema implements Schema, Cloneable
{
	// @Field
	/** Bankcode */
	private String BankCode;
	/** Ftp_lp */
	private String Ftp_lp;
	/** Ftp_username */
	private String Ftp_UserName;
	/** Ftp_password */
	private String Ftp_PassWord;
	/** Ftp_port */
	private String Ftp_Port;
	/** Ftp_remotedir */
	private String Ftp_RemoteDir;
	/** Localdir */
	private String LocalDir;

	public static final int FIELDNUM = 7;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public CIITC_BANK_CONFIGSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[1];
		pk[0] = "BankCode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		CIITC_BANK_CONFIGSchema cloned = (CIITC_BANK_CONFIGSchema) super.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBankCode()
	{
		return BankCode;
	}
	public void setBankCode(String aBankCode)
	{
		BankCode = aBankCode;
	}
	public String getFtp_lp()
	{
		return Ftp_lp;
	}
	public void setFtp_lp(String aFtp_lp)
	{
		Ftp_lp = aFtp_lp;
	}
	public String getFtp_UserName()
	{
		return Ftp_UserName;
	}
	public void setFtp_UserName(String aFtp_UserName)
	{
		Ftp_UserName = aFtp_UserName;
	}
	public String getFtp_PassWord()
	{
		return Ftp_PassWord;
	}
	public void setFtp_PassWord(String aFtp_PassWord)
	{
		Ftp_PassWord = aFtp_PassWord;
	}
	public String getFtp_Port()
	{
		return Ftp_Port;
	}
	public void setFtp_Port(String aFtp_Port)
	{
		Ftp_Port = aFtp_Port;
	}
	public String getFtp_RemoteDir()
	{
		return Ftp_RemoteDir;
	}
	public void setFtp_RemoteDir(String aFtp_RemoteDir)
	{
		Ftp_RemoteDir = aFtp_RemoteDir;
	}
	public String getLocalDir()
	{
		return LocalDir;
	}
	public void setLocalDir(String aLocalDir)
	{
		LocalDir = aLocalDir;
	}

	/**
	* 使用另外一个 CIITC_BANK_CONFIGSchema 对象给 Schema 赋值
	* @param: aCIITC_BANK_CONFIGSchema CIITC_BANK_CONFIGSchema
	**/
	public void setSchema(CIITC_BANK_CONFIGSchema aCIITC_BANK_CONFIGSchema)
	{
		this.BankCode = aCIITC_BANK_CONFIGSchema.getBankCode();
		this.Ftp_lp = aCIITC_BANK_CONFIGSchema.getFtp_lp();
		this.Ftp_UserName = aCIITC_BANK_CONFIGSchema.getFtp_UserName();
		this.Ftp_PassWord = aCIITC_BANK_CONFIGSchema.getFtp_PassWord();
		this.Ftp_Port = aCIITC_BANK_CONFIGSchema.getFtp_Port();
		this.Ftp_RemoteDir = aCIITC_BANK_CONFIGSchema.getFtp_RemoteDir();
		this.LocalDir = aCIITC_BANK_CONFIGSchema.getLocalDir();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BankCode") == null )
				this.BankCode = null;
			else
				this.BankCode = rs.getString("BankCode").trim();

			if( rs.getString("Ftp_lp") == null )
				this.Ftp_lp = null;
			else
				this.Ftp_lp = rs.getString("Ftp_lp").trim();

			if( rs.getString("Ftp_UserName") == null )
				this.Ftp_UserName = null;
			else
				this.Ftp_UserName = rs.getString("Ftp_UserName").trim();

			if( rs.getString("Ftp_PassWord") == null )
				this.Ftp_PassWord = null;
			else
				this.Ftp_PassWord = rs.getString("Ftp_PassWord").trim();

			if( rs.getString("Ftp_Port") == null )
				this.Ftp_Port = null;
			else
				this.Ftp_Port = rs.getString("Ftp_Port").trim();

			if( rs.getString("Ftp_RemoteDir") == null )
				this.Ftp_RemoteDir = null;
			else
				this.Ftp_RemoteDir = rs.getString("Ftp_RemoteDir").trim();

			if( rs.getString("LocalDir") == null )
				this.LocalDir = null;
			else
				this.LocalDir = rs.getString("LocalDir").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的CIITC_BANK_CONFIG表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CIITC_BANK_CONFIGSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public CIITC_BANK_CONFIGSchema getSchema()
	{
		CIITC_BANK_CONFIGSchema aCIITC_BANK_CONFIGSchema = new CIITC_BANK_CONFIGSchema();
		aCIITC_BANK_CONFIGSchema.setSchema(this);
		return aCIITC_BANK_CONFIGSchema;
	}

	public CIITC_BANK_CONFIGDB getDB()
	{
		CIITC_BANK_CONFIGDB aDBOper = new CIITC_BANK_CONFIGDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCIITC_BANK_CONFIG描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BankCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Ftp_lp)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Ftp_UserName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Ftp_PassWord)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Ftp_Port)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Ftp_RemoteDir)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(LocalDir));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpCIITC_BANK_CONFIG>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			Ftp_lp = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			Ftp_UserName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Ftp_PassWord = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Ftp_Port = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			Ftp_RemoteDir = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			LocalDir = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "CIITC_BANK_CONFIGSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BankCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankCode));
		}
		if (FCode.equals("Ftp_lp"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Ftp_lp));
		}
		if (FCode.equals("Ftp_UserName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Ftp_UserName));
		}
		if (FCode.equals("Ftp_PassWord"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Ftp_PassWord));
		}
		if (FCode.equals("Ftp_Port"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Ftp_Port));
		}
		if (FCode.equals("Ftp_RemoteDir"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Ftp_RemoteDir));
		}
		if (FCode.equals("LocalDir"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(LocalDir));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BankCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(Ftp_lp);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(Ftp_UserName);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Ftp_PassWord);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Ftp_Port);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(Ftp_RemoteDir);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(LocalDir);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BankCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankCode = FValue.trim();
			}
			else
				BankCode = null;
		}
		if (FCode.equalsIgnoreCase("Ftp_lp"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Ftp_lp = FValue.trim();
			}
			else
				Ftp_lp = null;
		}
		if (FCode.equalsIgnoreCase("Ftp_UserName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Ftp_UserName = FValue.trim();
			}
			else
				Ftp_UserName = null;
		}
		if (FCode.equalsIgnoreCase("Ftp_PassWord"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Ftp_PassWord = FValue.trim();
			}
			else
				Ftp_PassWord = null;
		}
		if (FCode.equalsIgnoreCase("Ftp_Port"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Ftp_Port = FValue.trim();
			}
			else
				Ftp_Port = null;
		}
		if (FCode.equalsIgnoreCase("Ftp_RemoteDir"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Ftp_RemoteDir = FValue.trim();
			}
			else
				Ftp_RemoteDir = null;
		}
		if (FCode.equalsIgnoreCase("LocalDir"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				LocalDir = FValue.trim();
			}
			else
				LocalDir = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		CIITC_BANK_CONFIGSchema other = (CIITC_BANK_CONFIGSchema)otherObject;
		return
			(BankCode == null ? other.getBankCode() == null : BankCode.equals(other.getBankCode()))
			&& (Ftp_lp == null ? other.getFtp_lp() == null : Ftp_lp.equals(other.getFtp_lp()))
			&& (Ftp_UserName == null ? other.getFtp_UserName() == null : Ftp_UserName.equals(other.getFtp_UserName()))
			&& (Ftp_PassWord == null ? other.getFtp_PassWord() == null : Ftp_PassWord.equals(other.getFtp_PassWord()))
			&& (Ftp_Port == null ? other.getFtp_Port() == null : Ftp_Port.equals(other.getFtp_Port()))
			&& (Ftp_RemoteDir == null ? other.getFtp_RemoteDir() == null : Ftp_RemoteDir.equals(other.getFtp_RemoteDir()))
			&& (LocalDir == null ? other.getLocalDir() == null : LocalDir.equals(other.getLocalDir()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BankCode") ) {
			return 0;
		}
		if( strFieldName.equals("Ftp_lp") ) {
			return 1;
		}
		if( strFieldName.equals("Ftp_UserName") ) {
			return 2;
		}
		if( strFieldName.equals("Ftp_PassWord") ) {
			return 3;
		}
		if( strFieldName.equals("Ftp_Port") ) {
			return 4;
		}
		if( strFieldName.equals("Ftp_RemoteDir") ) {
			return 5;
		}
		if( strFieldName.equals("LocalDir") ) {
			return 6;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BankCode";
				break;
			case 1:
				strFieldName = "Ftp_lp";
				break;
			case 2:
				strFieldName = "Ftp_UserName";
				break;
			case 3:
				strFieldName = "Ftp_PassWord";
				break;
			case 4:
				strFieldName = "Ftp_Port";
				break;
			case 5:
				strFieldName = "Ftp_RemoteDir";
				break;
			case 6:
				strFieldName = "LocalDir";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BankCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Ftp_lp") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Ftp_UserName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Ftp_PassWord") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Ftp_Port") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Ftp_RemoteDir") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("LocalDir") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
