/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LDSocialInsOrganDB;

/*
 * <p>ClassName: LDSocialInsOrganSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 健康管理表修改_2005-12-06
 * @CreateDate：2005-12-06
 */
public class LDSocialInsOrganSchema implements Schema, Cloneable {
    // @Field
    /** 社会医疗保险机构代码 */
    private String SIOrganCode;
    /** 社会医疗保险机构名称 */
    private String SIOrganName;
    /** 所在地区代码 */
    private String AreaCode;
    /** 地址 */
    private String Address;
    /** 邮编 */
    private String ZipCode;
    /** 联系电话 */
    private String Phone;
    /** 网址 */
    private String WebAddress;
    /** 传真 */
    private String Fax;
    /** 开户银行 */
    private String bankCode;
    /** 户名 */
    private String AccName;
    /** 银行帐号 */
    private String bankAccNO;
    /** 负责人 */
    private String SatrapName;
    /** 联系人 */
    private String Linkman;
    /** 最近修改日期 */
    private Date LastModiDate;
    /** 操作员 */
    private String Operator;
    /** 入机日期 */
    private Date MakeDate;
    /** 入机时间 */
    private String MakeTime;
    /** 最后一次修改日期 */
    private Date ModifyDate;
    /** 最后一次修改时间 */
    private String ModifyTime;
    /** 管理机构 */
    private String ManageCom;
    /** 上级机构代码 */
    private String SuperSIOrganCode;

    public static final int FIELDNUM = 21; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LDSocialInsOrganSchema() {
        mErrors = new CErrors();

        String[] pk = new String[1];
        pk[0] = "SIOrganCode";

        PK = pk;
    }

    /**
     * Schema克隆
     * @return Object
     * @throws CloneNotSupportedException
     */
    public Object clone() throws CloneNotSupportedException {
        LDSocialInsOrganSchema cloned = (LDSocialInsOrganSchema)super.clone();
        cloned.fDate = (FDate) fDate.clone();
        cloned.mErrors = (CErrors) mErrors.clone();
        return cloned;
    }

    // @Method
    public String[] getPK() {
        return PK;
    }

    public String getSIOrganCode() {
        return SIOrganCode;
    }

    public void setSIOrganCode(String aSIOrganCode) {
        SIOrganCode = aSIOrganCode;
    }

    public String getSIOrganName() {
        return SIOrganName;
    }

    public void setSIOrganName(String aSIOrganName) {
        SIOrganName = aSIOrganName;
    }

    public String getAreaCode() {
        return AreaCode;
    }

    public void setAreaCode(String aAreaCode) {
        AreaCode = aAreaCode;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String aAddress) {
        Address = aAddress;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String aZipCode) {
        ZipCode = aZipCode;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String aPhone) {
        Phone = aPhone;
    }

    public String getWebAddress() {
        return WebAddress;
    }

    public void setWebAddress(String aWebAddress) {
        WebAddress = aWebAddress;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String aFax) {
        Fax = aFax;
    }

    public String getbankCode() {
        return bankCode;
    }

    public void setbankCode(String abankCode) {
        bankCode = abankCode;
    }

    public String getAccName() {
        return AccName;
    }

    public void setAccName(String aAccName) {
        AccName = aAccName;
    }

    public String getbankAccNO() {
        return bankAccNO;
    }

    public void setbankAccNO(String abankAccNO) {
        bankAccNO = abankAccNO;
    }

    public String getSatrapName() {
        return SatrapName;
    }

    public void setSatrapName(String aSatrapName) {
        SatrapName = aSatrapName;
    }

    public String getLinkman() {
        return Linkman;
    }

    public void setLinkman(String aLinkman) {
        Linkman = aLinkman;
    }

    public String getLastModiDate() {
        if (LastModiDate != null) {
            return fDate.getString(LastModiDate);
        } else {
            return null;
        }
    }

    public void setLastModiDate(Date aLastModiDate) {
        LastModiDate = aLastModiDate;
    }

    public void setLastModiDate(String aLastModiDate) {
        if (aLastModiDate != null && !aLastModiDate.equals("")) {
            LastModiDate = fDate.getDate(aLastModiDate);
        } else {
            LastModiDate = null;
        }
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String aOperator) {
        Operator = aOperator;
    }

    public String getMakeDate() {
        if (MakeDate != null) {
            return fDate.getString(MakeDate);
        } else {
            return null;
        }
    }

    public void setMakeDate(Date aMakeDate) {
        MakeDate = aMakeDate;
    }

    public void setMakeDate(String aMakeDate) {
        if (aMakeDate != null && !aMakeDate.equals("")) {
            MakeDate = fDate.getDate(aMakeDate);
        } else {
            MakeDate = null;
        }
    }

    public String getMakeTime() {
        return MakeTime;
    }

    public void setMakeTime(String aMakeTime) {
        MakeTime = aMakeTime;
    }

    public String getModifyDate() {
        if (ModifyDate != null) {
            return fDate.getString(ModifyDate);
        } else {
            return null;
        }
    }

    public void setModifyDate(Date aModifyDate) {
        ModifyDate = aModifyDate;
    }

    public void setModifyDate(String aModifyDate) {
        if (aModifyDate != null && !aModifyDate.equals("")) {
            ModifyDate = fDate.getDate(aModifyDate);
        } else {
            ModifyDate = null;
        }
    }

    public String getModifyTime() {
        return ModifyTime;
    }

    public void setModifyTime(String aModifyTime) {
        ModifyTime = aModifyTime;
    }

    public String getManageCom() {
        return ManageCom;
    }

    public void setManageCom(String aManageCom) {
        ManageCom = aManageCom;
    }

    public String getSuperSIOrganCode() {
        return SuperSIOrganCode;
    }

    public void setSuperSIOrganCode(String aSuperSIOrganCode) {
        SuperSIOrganCode = aSuperSIOrganCode;
    }

    /**
     * 使用另外一个 LDSocialInsOrganSchema 对象给 Schema 赋值
     * @param: aLDSocialInsOrganSchema LDSocialInsOrganSchema
     **/
    public void setSchema(LDSocialInsOrganSchema aLDSocialInsOrganSchema) {
        this.SIOrganCode = aLDSocialInsOrganSchema.getSIOrganCode();
        this.SIOrganName = aLDSocialInsOrganSchema.getSIOrganName();
        this.AreaCode = aLDSocialInsOrganSchema.getAreaCode();
        this.Address = aLDSocialInsOrganSchema.getAddress();
        this.ZipCode = aLDSocialInsOrganSchema.getZipCode();
        this.Phone = aLDSocialInsOrganSchema.getPhone();
        this.WebAddress = aLDSocialInsOrganSchema.getWebAddress();
        this.Fax = aLDSocialInsOrganSchema.getFax();
        this.bankCode = aLDSocialInsOrganSchema.getbankCode();
        this.AccName = aLDSocialInsOrganSchema.getAccName();
        this.bankAccNO = aLDSocialInsOrganSchema.getbankAccNO();
        this.SatrapName = aLDSocialInsOrganSchema.getSatrapName();
        this.Linkman = aLDSocialInsOrganSchema.getLinkman();
        this.LastModiDate = fDate.getDate(aLDSocialInsOrganSchema.
                                          getLastModiDate());
        this.Operator = aLDSocialInsOrganSchema.getOperator();
        this.MakeDate = fDate.getDate(aLDSocialInsOrganSchema.getMakeDate());
        this.MakeTime = aLDSocialInsOrganSchema.getMakeTime();
        this.ModifyDate = fDate.getDate(aLDSocialInsOrganSchema.getModifyDate());
        this.ModifyTime = aLDSocialInsOrganSchema.getModifyTime();
        this.ManageCom = aLDSocialInsOrganSchema.getManageCom();
        this.SuperSIOrganCode = aLDSocialInsOrganSchema.getSuperSIOrganCode();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: rs ResultSet
     * @param: i int
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i) {
        try {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("SIOrganCode") == null) {
                this.SIOrganCode = null;
            } else {
                this.SIOrganCode = rs.getString("SIOrganCode").trim();
            }

            if (rs.getString("SIOrganName") == null) {
                this.SIOrganName = null;
            } else {
                this.SIOrganName = rs.getString("SIOrganName").trim();
            }

            if (rs.getString("AreaCode") == null) {
                this.AreaCode = null;
            } else {
                this.AreaCode = rs.getString("AreaCode").trim();
            }

            if (rs.getString("Address") == null) {
                this.Address = null;
            } else {
                this.Address = rs.getString("Address").trim();
            }

            if (rs.getString("ZipCode") == null) {
                this.ZipCode = null;
            } else {
                this.ZipCode = rs.getString("ZipCode").trim();
            }

            if (rs.getString("Phone") == null) {
                this.Phone = null;
            } else {
                this.Phone = rs.getString("Phone").trim();
            }

            if (rs.getString("WebAddress") == null) {
                this.WebAddress = null;
            } else {
                this.WebAddress = rs.getString("WebAddress").trim();
            }

            if (rs.getString("Fax") == null) {
                this.Fax = null;
            } else {
                this.Fax = rs.getString("Fax").trim();
            }

            if (rs.getString("bankCode") == null) {
                this.bankCode = null;
            } else {
                this.bankCode = rs.getString("bankCode").trim();
            }

            if (rs.getString("AccName") == null) {
                this.AccName = null;
            } else {
                this.AccName = rs.getString("AccName").trim();
            }

            if (rs.getString("bankAccNO") == null) {
                this.bankAccNO = null;
            } else {
                this.bankAccNO = rs.getString("bankAccNO").trim();
            }

            if (rs.getString("SatrapName") == null) {
                this.SatrapName = null;
            } else {
                this.SatrapName = rs.getString("SatrapName").trim();
            }

            if (rs.getString("Linkman") == null) {
                this.Linkman = null;
            } else {
                this.Linkman = rs.getString("Linkman").trim();
            }

            this.LastModiDate = rs.getDate("LastModiDate");
            if (rs.getString("Operator") == null) {
                this.Operator = null;
            } else {
                this.Operator = rs.getString("Operator").trim();
            }

            this.MakeDate = rs.getDate("MakeDate");
            if (rs.getString("MakeTime") == null) {
                this.MakeTime = null;
            } else {
                this.MakeTime = rs.getString("MakeTime").trim();
            }

            this.ModifyDate = rs.getDate("ModifyDate");
            if (rs.getString("ModifyTime") == null) {
                this.ModifyTime = null;
            } else {
                this.ModifyTime = rs.getString("ModifyTime").trim();
            }

            if (rs.getString("ManageCom") == null) {
                this.ManageCom = null;
            } else {
                this.ManageCom = rs.getString("ManageCom").trim();
            }

            if (rs.getString("SuperSIOrganCode") == null) {
                this.SuperSIOrganCode = null;
            } else {
                this.SuperSIOrganCode = rs.getString("SuperSIOrganCode").trim();
            }

        } catch (SQLException sqle) {
            System.out.println("数据库中的LDSocialInsOrgan表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSocialInsOrganSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public LDSocialInsOrganSchema getSchema() {
        LDSocialInsOrganSchema aLDSocialInsOrganSchema = new
                LDSocialInsOrganSchema();
        aLDSocialInsOrganSchema.setSchema(this);
        return aLDSocialInsOrganSchema;
    }

    public LDSocialInsOrganDB getDB() {
        LDSocialInsOrganDB aDBOper = new LDSocialInsOrganDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDSocialInsOrgan描述/A>表字段
     * @return: String 返回打包后字符串
     **/
    public String encode() {
        StringBuffer strReturn = new StringBuffer(256);
        strReturn.append(StrTool.cTrim(SIOrganCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SIOrganName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AreaCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Address));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ZipCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Phone));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(WebAddress));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Fax));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(bankCode));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(AccName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(bankAccNO));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SatrapName));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Linkman));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(LastModiDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(Operator));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(MakeDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(MakeTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(fDate.getString(ModifyDate)));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ModifyTime));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(ManageCom));
        strReturn.append(SysConst.PACKAGESPILTER);
        strReturn.append(StrTool.cTrim(SuperSIOrganCode));
        return strReturn.toString();
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLDSocialInsOrgan>历史记账凭证主表信息</A>表字段
     * @param: strMessage String 包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage) {
        try {
            SIOrganCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                         SysConst.PACKAGESPILTER);
            SIOrganName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            AreaCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                      SysConst.PACKAGESPILTER);
            Address = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            ZipCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5,
                                     SysConst.PACKAGESPILTER);
            Phone = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6,
                                   SysConst.PACKAGESPILTER);
            WebAddress = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            Fax = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                 SysConst.PACKAGESPILTER);
            bankCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9,
                                      SysConst.PACKAGESPILTER);
            AccName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,
                                     SysConst.PACKAGESPILTER);
            bankAccNO = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                       SysConst.PACKAGESPILTER);
            SatrapName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12,
                                        SysConst.PACKAGESPILTER);
            Linkman = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,
                                     SysConst.PACKAGESPILTER);
            LastModiDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 14, SysConst.PACKAGESPILTER));
            Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,
                                      SysConst.PACKAGESPILTER);
            MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 16, SysConst.PACKAGESPILTER));
            MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17,
                                      SysConst.PACKAGESPILTER);
            ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(
                    strMessage), 18, SysConst.PACKAGESPILTER));
            ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19,
                                        SysConst.PACKAGESPILTER);
            ManageCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 20,
                                       SysConst.PACKAGESPILTER);
            SuperSIOrganCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage),
                                              21, SysConst.PACKAGESPILTER);
        } catch (NumberFormatException ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDSocialInsOrganSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode String 希望取得的字段名
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(String FCode) {
        String strReturn = "";
        if (FCode.equals("SIOrganCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SIOrganCode));
        }
        if (FCode.equals("SIOrganName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SIOrganName));
        }
        if (FCode.equals("AreaCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AreaCode));
        }
        if (FCode.equals("Address")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Address));
        }
        if (FCode.equals("ZipCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ZipCode));
        }
        if (FCode.equals("Phone")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Phone));
        }
        if (FCode.equals("WebAddress")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(WebAddress));
        }
        if (FCode.equals("Fax")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Fax));
        }
        if (FCode.equals("bankCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bankCode));
        }
        if (FCode.equals("AccName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(AccName));
        }
        if (FCode.equals("bankAccNO")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(bankAccNO));
        }
        if (FCode.equals("SatrapName")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SatrapName));
        }
        if (FCode.equals("Linkman")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Linkman));
        }
        if (FCode.equals("LastModiDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.
                    getLastModiDate()));
        }
        if (FCode.equals("Operator")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
        }
        if (FCode.equals("MakeDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getMakeDate()));
        }
        if (FCode.equals("MakeTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
        }
        if (FCode.equals("ModifyDate")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(this.getModifyDate()));
        }
        if (FCode.equals("ModifyTime")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
        }
        if (FCode.equals("ManageCom")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(ManageCom));
        }
        if (FCode.equals("SuperSIOrganCode")) {
            strReturn = StrTool.GBKToUnicode(String.valueOf(SuperSIOrganCode));
        }
        if (strReturn.equals("")) {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex int 指定的字段索引值
     * @return: String
     * 如果没有对应的字段，返回""
     * 如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex) {
        String strFieldValue = "";
        switch (nFieldIndex) {
        case 0:
            strFieldValue = StrTool.GBKToUnicode(SIOrganCode);
            break;
        case 1:
            strFieldValue = StrTool.GBKToUnicode(SIOrganName);
            break;
        case 2:
            strFieldValue = StrTool.GBKToUnicode(AreaCode);
            break;
        case 3:
            strFieldValue = StrTool.GBKToUnicode(Address);
            break;
        case 4:
            strFieldValue = StrTool.GBKToUnicode(ZipCode);
            break;
        case 5:
            strFieldValue = StrTool.GBKToUnicode(Phone);
            break;
        case 6:
            strFieldValue = StrTool.GBKToUnicode(WebAddress);
            break;
        case 7:
            strFieldValue = StrTool.GBKToUnicode(Fax);
            break;
        case 8:
            strFieldValue = StrTool.GBKToUnicode(bankCode);
            break;
        case 9:
            strFieldValue = StrTool.GBKToUnicode(AccName);
            break;
        case 10:
            strFieldValue = StrTool.GBKToUnicode(bankAccNO);
            break;
        case 11:
            strFieldValue = StrTool.GBKToUnicode(SatrapName);
            break;
        case 12:
            strFieldValue = StrTool.GBKToUnicode(Linkman);
            break;
        case 13:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getLastModiDate()));
            break;
        case 14:
            strFieldValue = StrTool.GBKToUnicode(Operator);
            break;
        case 15:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getMakeDate()));
            break;
        case 16:
            strFieldValue = StrTool.GBKToUnicode(MakeTime);
            break;
        case 17:
            strFieldValue = StrTool.GBKToUnicode(String.valueOf(this.
                    getModifyDate()));
            break;
        case 18:
            strFieldValue = StrTool.GBKToUnicode(ModifyTime);
            break;
        case 19:
            strFieldValue = StrTool.GBKToUnicode(ManageCom);
            break;
        case 20:
            strFieldValue = StrTool.GBKToUnicode(SuperSIOrganCode);
            break;
        default:
            strFieldValue = "";
        }
        ;
        if (strFieldValue.equals("")) {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode String 需要赋值的对象
     * @param: FValue String 要赋的值
     * @return: boolean
     **/
    public boolean setV(String FCode, String FValue) {
        if (StrTool.cTrim(FCode).equals("")) {
            return false;
        }

        if (FCode.equalsIgnoreCase("SIOrganCode")) {
            if (FValue != null && !FValue.equals("")) {
                SIOrganCode = FValue.trim();
            } else {
                SIOrganCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("SIOrganName")) {
            if (FValue != null && !FValue.equals("")) {
                SIOrganName = FValue.trim();
            } else {
                SIOrganName = null;
            }
        }
        if (FCode.equalsIgnoreCase("AreaCode")) {
            if (FValue != null && !FValue.equals("")) {
                AreaCode = FValue.trim();
            } else {
                AreaCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Address")) {
            if (FValue != null && !FValue.equals("")) {
                Address = FValue.trim();
            } else {
                Address = null;
            }
        }
        if (FCode.equalsIgnoreCase("ZipCode")) {
            if (FValue != null && !FValue.equals("")) {
                ZipCode = FValue.trim();
            } else {
                ZipCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("Phone")) {
            if (FValue != null && !FValue.equals("")) {
                Phone = FValue.trim();
            } else {
                Phone = null;
            }
        }
        if (FCode.equalsIgnoreCase("WebAddress")) {
            if (FValue != null && !FValue.equals("")) {
                WebAddress = FValue.trim();
            } else {
                WebAddress = null;
            }
        }
        if (FCode.equalsIgnoreCase("Fax")) {
            if (FValue != null && !FValue.equals("")) {
                Fax = FValue.trim();
            } else {
                Fax = null;
            }
        }
        if (FCode.equalsIgnoreCase("bankCode")) {
            if (FValue != null && !FValue.equals("")) {
                bankCode = FValue.trim();
            } else {
                bankCode = null;
            }
        }
        if (FCode.equalsIgnoreCase("AccName")) {
            if (FValue != null && !FValue.equals("")) {
                AccName = FValue.trim();
            } else {
                AccName = null;
            }
        }
        if (FCode.equalsIgnoreCase("bankAccNO")) {
            if (FValue != null && !FValue.equals("")) {
                bankAccNO = FValue.trim();
            } else {
                bankAccNO = null;
            }
        }
        if (FCode.equalsIgnoreCase("SatrapName")) {
            if (FValue != null && !FValue.equals("")) {
                SatrapName = FValue.trim();
            } else {
                SatrapName = null;
            }
        }
        if (FCode.equalsIgnoreCase("Linkman")) {
            if (FValue != null && !FValue.equals("")) {
                Linkman = FValue.trim();
            } else {
                Linkman = null;
            }
        }
        if (FCode.equalsIgnoreCase("LastModiDate")) {
            if (FValue != null && !FValue.equals("")) {
                LastModiDate = fDate.getDate(FValue);
            } else {
                LastModiDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("Operator")) {
            if (FValue != null && !FValue.equals("")) {
                Operator = FValue.trim();
            } else {
                Operator = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeDate")) {
            if (FValue != null && !FValue.equals("")) {
                MakeDate = fDate.getDate(FValue);
            } else {
                MakeDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("MakeTime")) {
            if (FValue != null && !FValue.equals("")) {
                MakeTime = FValue.trim();
            } else {
                MakeTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyDate")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyDate = fDate.getDate(FValue);
            } else {
                ModifyDate = null;
            }
        }
        if (FCode.equalsIgnoreCase("ModifyTime")) {
            if (FValue != null && !FValue.equals("")) {
                ModifyTime = FValue.trim();
            } else {
                ModifyTime = null;
            }
        }
        if (FCode.equalsIgnoreCase("ManageCom")) {
            if (FValue != null && !FValue.equals("")) {
                ManageCom = FValue.trim();
            } else {
                ManageCom = null;
            }
        }
        if (FCode.equalsIgnoreCase("SuperSIOrganCode")) {
            if (FValue != null && !FValue.equals("")) {
                SuperSIOrganCode = FValue.trim();
            } else {
                SuperSIOrganCode = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        LDSocialInsOrganSchema other = (LDSocialInsOrganSchema) otherObject;
        return
                SIOrganCode.equals(other.getSIOrganCode())
                && SIOrganName.equals(other.getSIOrganName())
                && AreaCode.equals(other.getAreaCode())
                && Address.equals(other.getAddress())
                && ZipCode.equals(other.getZipCode())
                && Phone.equals(other.getPhone())
                && WebAddress.equals(other.getWebAddress())
                && Fax.equals(other.getFax())
                && bankCode.equals(other.getbankCode())
                && AccName.equals(other.getAccName())
                && bankAccNO.equals(other.getbankAccNO())
                && SatrapName.equals(other.getSatrapName())
                && Linkman.equals(other.getLinkman())
                && fDate.getString(LastModiDate).equals(other.getLastModiDate())
                && Operator.equals(other.getOperator())
                && fDate.getString(MakeDate).equals(other.getMakeDate())
                && MakeTime.equals(other.getMakeTime())
                && fDate.getString(ModifyDate).equals(other.getModifyDate())
                && ModifyTime.equals(other.getModifyTime())
                && ManageCom.equals(other.getManageCom())
                && SuperSIOrganCode.equals(other.getSuperSIOrganCode());
    }

    /**
     * 取得Schema拥有字段的数量
     * @return: int
     **/
    public int getFieldCount() {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldIndex(String strFieldName) {
        if (strFieldName.equals("SIOrganCode")) {
            return 0;
        }
        if (strFieldName.equals("SIOrganName")) {
            return 1;
        }
        if (strFieldName.equals("AreaCode")) {
            return 2;
        }
        if (strFieldName.equals("Address")) {
            return 3;
        }
        if (strFieldName.equals("ZipCode")) {
            return 4;
        }
        if (strFieldName.equals("Phone")) {
            return 5;
        }
        if (strFieldName.equals("WebAddress")) {
            return 6;
        }
        if (strFieldName.equals("Fax")) {
            return 7;
        }
        if (strFieldName.equals("bankCode")) {
            return 8;
        }
        if (strFieldName.equals("AccName")) {
            return 9;
        }
        if (strFieldName.equals("bankAccNO")) {
            return 10;
        }
        if (strFieldName.equals("SatrapName")) {
            return 11;
        }
        if (strFieldName.equals("Linkman")) {
            return 12;
        }
        if (strFieldName.equals("LastModiDate")) {
            return 13;
        }
        if (strFieldName.equals("Operator")) {
            return 14;
        }
        if (strFieldName.equals("MakeDate")) {
            return 15;
        }
        if (strFieldName.equals("MakeTime")) {
            return 16;
        }
        if (strFieldName.equals("ModifyDate")) {
            return 17;
        }
        if (strFieldName.equals("ModifyTime")) {
            return 18;
        }
        if (strFieldName.equals("ManageCom")) {
            return 19;
        }
        if (strFieldName.equals("SuperSIOrganCode")) {
            return 20;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     * @param: nFieldIndex int
     * @return: String
     **/
    public String getFieldName(int nFieldIndex) {
        String strFieldName = "";
        switch (nFieldIndex) {
        case 0:
            strFieldName = "SIOrganCode";
            break;
        case 1:
            strFieldName = "SIOrganName";
            break;
        case 2:
            strFieldName = "AreaCode";
            break;
        case 3:
            strFieldName = "Address";
            break;
        case 4:
            strFieldName = "ZipCode";
            break;
        case 5:
            strFieldName = "Phone";
            break;
        case 6:
            strFieldName = "WebAddress";
            break;
        case 7:
            strFieldName = "Fax";
            break;
        case 8:
            strFieldName = "bankCode";
            break;
        case 9:
            strFieldName = "AccName";
            break;
        case 10:
            strFieldName = "bankAccNO";
            break;
        case 11:
            strFieldName = "SatrapName";
            break;
        case 12:
            strFieldName = "Linkman";
            break;
        case 13:
            strFieldName = "LastModiDate";
            break;
        case 14:
            strFieldName = "Operator";
            break;
        case 15:
            strFieldName = "MakeDate";
            break;
        case 16:
            strFieldName = "MakeTime";
            break;
        case 17:
            strFieldName = "ModifyDate";
            break;
        case 18:
            strFieldName = "ModifyTime";
            break;
        case 19:
            strFieldName = "ManageCom";
            break;
        case 20:
            strFieldName = "SuperSIOrganCode";
            break;
        default:
            strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: strFieldName String
     * @return: int
     **/
    public int getFieldType(String strFieldName) {
        if (strFieldName.equals("SIOrganCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SIOrganName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AreaCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Address")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ZipCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Phone")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("WebAddress")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Fax")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bankCode")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("AccName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("bankAccNO")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SatrapName")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("Linkman")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("LastModiDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("Operator")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("MakeDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("MakeTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ModifyDate")) {
            return Schema.TYPE_DATE;
        }
        if (strFieldName.equals("ModifyTime")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("ManageCom")) {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("SuperSIOrganCode")) {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     * @param: nFieldIndex int
     * @return: int
     **/
    public int getFieldType(int nFieldIndex) {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex) {
        case 0:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 1:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 2:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 3:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 4:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 5:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 6:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 7:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 8:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 9:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 10:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 11:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 12:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 13:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 14:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 15:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 16:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 17:
            nFieldType = Schema.TYPE_DATE;
            break;
        case 18:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 19:
            nFieldType = Schema.TYPE_STRING;
            break;
        case 20:
            nFieldType = Schema.TYPE_STRING;
            break;
        default:
            nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
