/*
 * <p>ClassName: LMEdorZTSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2004-11-08
 */
package com.sinosoft.lis.schema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sinosoft.lis.db.LMEdorZTDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ChgData;
import com.sinosoft.utility.Schema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;

public class LMEdorZTSchema implements Schema
{
    // @Field
    /** 险种编码 */
    private String RiskCode;
    /** 险种版本 */
    private String RiskVersion;
    /** 期交是否允许通融退保 */
    private String CycFlag;
    /** 期交退保控制范围类型 */
    private String CycType;
    /** 期交退保控制范围,起点 */
    private int CycStart;
    /** 期交退保控制范围,终点 */
    private int CycEnd;
    /** 趸交是否允许通融退保 */
    private String OnePayFlag;
    /** 趸交退保控制范围类型 */
    private String OnePayType;
    /** 趸交退保控制范围起点 */
    private int OnePayStart;
    /** 趸交退保控制范围终点 */
    private int OnePayEnd;
    /** 补/退费财务类型 */
    private String FeeFinaType;

    public static final int FIELDNUM = 11; // 数据库表的字段个数

    private static String[] PK; // 主键

    private FDate fDate = new FDate(); // 处理日期

    public CErrors mErrors; // 错误信息

    // @Constructor
    public LMEdorZTSchema()
    {
        mErrors = new CErrors();

        String[] pk = new String[2];
        pk[0] = "RiskCode";
        pk[1] = "RiskVersion";

        PK = pk;
    }

    // @Method
    public String[] getPK()
    {
        return PK;
    }

    public String getRiskCode()
    {
        if (RiskCode != null && !RiskCode.equals("") && SysConst.CHANGECHARSET == true)
        {
            RiskCode = StrTool.unicodeToGBK(RiskCode);
        }
        return RiskCode;
    }

    public void setRiskCode(String aRiskCode)
    {
        RiskCode = aRiskCode;
    }

    public String getRiskVersion()
    {
        if (RiskVersion != null && !RiskVersion.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            RiskVersion = StrTool.unicodeToGBK(RiskVersion);
        }
        return RiskVersion;
    }

    public void setRiskVersion(String aRiskVersion)
    {
        RiskVersion = aRiskVersion;
    }

    public String getCycFlag()
    {
        if (CycFlag != null && !CycFlag.equals("") && SysConst.CHANGECHARSET == true)
        {
            CycFlag = StrTool.unicodeToGBK(CycFlag);
        }
        return CycFlag;
    }

    public void setCycFlag(String aCycFlag)
    {
        CycFlag = aCycFlag;
    }

    public String getCycType()
    {
        if (CycType != null && !CycType.equals("") && SysConst.CHANGECHARSET == true)
        {
            CycType = StrTool.unicodeToGBK(CycType);
        }
        return CycType;
    }

    public void setCycType(String aCycType)
    {
        CycType = aCycType;
    }

    public int getCycStart()
    {
        return CycStart;
    }

    public void setCycStart(int aCycStart)
    {
        CycStart = aCycStart;
    }

    public void setCycStart(String aCycStart)
    {
        if (aCycStart != null && !aCycStart.equals(""))
        {
            Integer tInteger = new Integer(aCycStart);
            int i = tInteger.intValue();
            CycStart = i;
        }
    }

    public int getCycEnd()
    {
        return CycEnd;
    }

    public void setCycEnd(int aCycEnd)
    {
        CycEnd = aCycEnd;
    }

    public void setCycEnd(String aCycEnd)
    {
        if (aCycEnd != null && !aCycEnd.equals(""))
        {
            Integer tInteger = new Integer(aCycEnd);
            int i = tInteger.intValue();
            CycEnd = i;
        }
    }

    public String getOnePayFlag()
    {
        if (OnePayFlag != null && !OnePayFlag.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OnePayFlag = StrTool.unicodeToGBK(OnePayFlag);
        }
        return OnePayFlag;
    }

    public void setOnePayFlag(String aOnePayFlag)
    {
        OnePayFlag = aOnePayFlag;
    }

    public String getOnePayType()
    {
        if (OnePayType != null && !OnePayType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            OnePayType = StrTool.unicodeToGBK(OnePayType);
        }
        return OnePayType;
    }

    public void setOnePayType(String aOnePayType)
    {
        OnePayType = aOnePayType;
    }

    public int getOnePayStart()
    {
        return OnePayStart;
    }

    public void setOnePayStart(int aOnePayStart)
    {
        OnePayStart = aOnePayStart;
    }

    public void setOnePayStart(String aOnePayStart)
    {
        if (aOnePayStart != null && !aOnePayStart.equals(""))
        {
            Integer tInteger = new Integer(aOnePayStart);
            int i = tInteger.intValue();
            OnePayStart = i;
        }
    }

    public int getOnePayEnd()
    {
        return OnePayEnd;
    }

    public void setOnePayEnd(int aOnePayEnd)
    {
        OnePayEnd = aOnePayEnd;
    }

    public void setOnePayEnd(String aOnePayEnd)
    {
        if (aOnePayEnd != null && !aOnePayEnd.equals(""))
        {
            Integer tInteger = new Integer(aOnePayEnd);
            int i = tInteger.intValue();
            OnePayEnd = i;
        }
    }

    public String getFeeFinaType()
    {
        if (FeeFinaType != null && !FeeFinaType.equals("") &&
            SysConst.CHANGECHARSET == true)
        {
            FeeFinaType = StrTool.unicodeToGBK(FeeFinaType);
        }
        return FeeFinaType;
    }

    public void setFeeFinaType(String aFeeFinaType)
    {
        FeeFinaType = aFeeFinaType;
    }

    /**
     * 使用另外一个 LMEdorZTSchema 对象给 Schema 赋值
     * @param: Schema 对象
     * @return: 无
     **/
    public void setSchema(LMEdorZTSchema aLMEdorZTSchema)
    {
        this.RiskCode = aLMEdorZTSchema.getRiskCode();
        this.RiskVersion = aLMEdorZTSchema.getRiskVersion();
        this.CycFlag = aLMEdorZTSchema.getCycFlag();
        this.CycType = aLMEdorZTSchema.getCycType();
        this.CycStart = aLMEdorZTSchema.getCycStart();
        this.CycEnd = aLMEdorZTSchema.getCycEnd();
        this.OnePayFlag = aLMEdorZTSchema.getOnePayFlag();
        this.OnePayType = aLMEdorZTSchema.getOnePayType();
        this.OnePayStart = aLMEdorZTSchema.getOnePayStart();
        this.OnePayEnd = aLMEdorZTSchema.getOnePayEnd();
        this.FeeFinaType = aLMEdorZTSchema.getFeeFinaType();
    }

    /**
     * 使用 ResultSet 中的第 i 行给 Schema 赋值
     * @param: ResultSet 对象; i 行数
     * @return: boolean
     **/
    public boolean setSchema(ResultSet rs, int i)
    {
        try
        {
            //rs.absolute(i);		// 非滚动游标
            if (rs.getString("RiskCode") == null)
            {
                this.RiskCode = null;
            }
            else
            {
                this.RiskCode = rs.getString("RiskCode").trim();
            }

            if (rs.getString("RiskVersion") == null)
            {
                this.RiskVersion = null;
            }
            else
            {
                this.RiskVersion = rs.getString("RiskVersion").trim();
            }

            if (rs.getString("CycFlag") == null)
            {
                this.CycFlag = null;
            }
            else
            {
                this.CycFlag = rs.getString("CycFlag").trim();
            }

            if (rs.getString("CycType") == null)
            {
                this.CycType = null;
            }
            else
            {
                this.CycType = rs.getString("CycType").trim();
            }

            this.CycStart = rs.getInt("CycStart");
            this.CycEnd = rs.getInt("CycEnd");
            if (rs.getString("OnePayFlag") == null)
            {
                this.OnePayFlag = null;
            }
            else
            {
                this.OnePayFlag = rs.getString("OnePayFlag").trim();
            }

            if (rs.getString("OnePayType") == null)
            {
                this.OnePayType = null;
            }
            else
            {
                this.OnePayType = rs.getString("OnePayType").trim();
            }

            this.OnePayStart = rs.getInt("OnePayStart");
            this.OnePayEnd = rs.getInt("OnePayEnd");
            if (rs.getString("FeeFinaType") == null)
            {
                this.FeeFinaType = null;
            }
            else
            {
                this.FeeFinaType = rs.getString("FeeFinaType").trim();
            }

        }
        catch (SQLException sqle)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorZTSchema";
            tError.functionName = "setSchema";
            tError.errorMessage = sqle.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    public LMEdorZTSchema getSchema()
    {
        LMEdorZTSchema aLMEdorZTSchema = new LMEdorZTSchema();
        aLMEdorZTSchema.setSchema(this);
        return aLMEdorZTSchema;
    }

    public LMEdorZTDB getDB()
    {
        LMEdorZTDB aDBOper = new LMEdorZTDB();
        aDBOper.setSchema(this);
        return aDBOper;
    }


    /**
     * 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorZT描述/A>表字段
     * @param: 无
     * @return: 返回打包后字符串
     **/
    public String encode()
    {
        String strReturn = "";
        strReturn = StrTool.cTrim(StrTool.unicodeToGBK(RiskCode)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(RiskVersion)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CycFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(CycType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(CycStart) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(CycEnd) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OnePayFlag)) +
                    SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(OnePayType)) +
                    SysConst.PACKAGESPILTER
                    + ChgData.chgData(OnePayStart) + SysConst.PACKAGESPILTER
                    + ChgData.chgData(OnePayEnd) + SysConst.PACKAGESPILTER
                    + StrTool.cTrim(StrTool.unicodeToGBK(FeeFinaType));
        return strReturn;
    }

    /**
     * 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLMEdorZT>历史记账凭证主表信息</A>表字段
     * @param: strMessage：包含一条纪录数据的字符串
     * @return: boolean
     **/
    public boolean decode(String strMessage)
    {
        try
        {
            RiskCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1,
                                      SysConst.PACKAGESPILTER);
            RiskVersion = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2,
                                         SysConst.PACKAGESPILTER);
            CycFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3,
                                     SysConst.PACKAGESPILTER);
            CycType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4,
                                     SysConst.PACKAGESPILTER);
            CycStart = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 5, SysConst.PACKAGESPILTER))).intValue();
            CycEnd = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 6, SysConst.PACKAGESPILTER))).intValue();
            OnePayFlag = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7,
                                        SysConst.PACKAGESPILTER);
            OnePayType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8,
                                        SysConst.PACKAGESPILTER);
            OnePayStart = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 9, SysConst.PACKAGESPILTER))).intValue();
            OnePayEnd = new Integer(ChgData.chgNumericStr(StrTool.getStr(
                    strMessage, 10, SysConst.PACKAGESPILTER))).intValue();
            FeeFinaType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11,
                                         SysConst.PACKAGESPILTER);
        }
        catch (NumberFormatException ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LMEdorZTSchema";
            tError.functionName = "decode";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 取得对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public String getV(String FCode)
    {
        String strReturn = "";
        if (FCode.equals("RiskCode"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskCode));
        }
        if (FCode.equals("RiskVersion"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(RiskVersion));
        }
        if (FCode.equals("CycFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CycFlag));
        }
        if (FCode.equals("CycType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CycType));
        }
        if (FCode.equals("CycStart"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CycStart));
        }
        if (FCode.equals("CycEnd"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(CycEnd));
        }
        if (FCode.equals("OnePayFlag"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OnePayFlag));
        }
        if (FCode.equals("OnePayType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OnePayType));
        }
        if (FCode.equals("OnePayStart"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OnePayStart));
        }
        if (FCode.equals("OnePayEnd"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(OnePayEnd));
        }
        if (FCode.equals("FeeFinaType"))
        {
            strReturn = StrTool.GBKToUnicode(strReturn.valueOf(FeeFinaType));
        }
        if (strReturn.equals(""))
        {
            strReturn = "null";
        }

        return strReturn;
    }


    /**
     * 取得Schema中指定索引值所对应的字段值
     * @param: nFieldIndex 指定的字段索引值
     * @return: 字段值。
     *          如果没有对应的字段，返回""
     *          如果字段值为空，返回"null"
     **/
    public String getV(int nFieldIndex)
    {
        String strFieldValue = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldValue = StrTool.GBKToUnicode(RiskCode);
                break;
            case 1:
                strFieldValue = StrTool.GBKToUnicode(RiskVersion);
                break;
            case 2:
                strFieldValue = StrTool.GBKToUnicode(CycFlag);
                break;
            case 3:
                strFieldValue = StrTool.GBKToUnicode(CycType);
                break;
            case 4:
                strFieldValue = String.valueOf(CycStart);
                break;
            case 5:
                strFieldValue = String.valueOf(CycEnd);
                break;
            case 6:
                strFieldValue = StrTool.GBKToUnicode(OnePayFlag);
                break;
            case 7:
                strFieldValue = StrTool.GBKToUnicode(OnePayType);
                break;
            case 8:
                strFieldValue = String.valueOf(OnePayStart);
                break;
            case 9:
                strFieldValue = String.valueOf(OnePayEnd);
                break;
            case 10:
                strFieldValue = StrTool.GBKToUnicode(FeeFinaType);
                break;
            default:
                strFieldValue = "";
        }
        ;
        if (strFieldValue.equals(""))
        {
            strFieldValue = "null";
        }
        return strFieldValue;
    }

    /**
     * 设置对应传入参数的String形式的字段值
     * @param: FCode 希望取得的字段名
     * @return: FValue String形式的字段值
     *			FValue = ""		没有匹配的字段
     *			FValue = "null"	字段值为null
     **/
    public boolean setV(String FCode, String FValue)
    {
        if (StrTool.cTrim(FCode).equals(""))
        {
            return false;
        }

        if (FCode.equals("RiskCode"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskCode = FValue.trim();
            }
            else
            {
                RiskCode = null;
            }
        }
        if (FCode.equals("RiskVersion"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                RiskVersion = FValue.trim();
            }
            else
            {
                RiskVersion = null;
            }
        }
        if (FCode.equals("CycFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CycFlag = FValue.trim();
            }
            else
            {
                CycFlag = null;
            }
        }
        if (FCode.equals("CycType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                CycType = FValue.trim();
            }
            else
            {
                CycType = null;
            }
        }
        if (FCode.equals("CycStart"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CycStart = i;
            }
        }
        if (FCode.equals("CycEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                CycEnd = i;
            }
        }
        if (FCode.equals("OnePayFlag"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OnePayFlag = FValue.trim();
            }
            else
            {
                OnePayFlag = null;
            }
        }
        if (FCode.equals("OnePayType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                OnePayType = FValue.trim();
            }
            else
            {
                OnePayType = null;
            }
        }
        if (FCode.equals("OnePayStart"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                OnePayStart = i;
            }
        }
        if (FCode.equals("OnePayEnd"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                Integer tInteger = new Integer(FValue);
                int i = tInteger.intValue();
                OnePayEnd = i;
            }
        }
        if (FCode.equals("FeeFinaType"))
        {
            if (FValue != null && !FValue.equals(""))
            {
                FeeFinaType = FValue.trim();
            }
            else
            {
                FeeFinaType = null;
            }
        }
        return true;
    }

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
        {
            return true;
        }
        if (otherObject == null)
        {
            return false;
        }
        if (getClass() != otherObject.getClass())
        {
            return false;
        }
        LMEdorZTSchema other = (LMEdorZTSchema) otherObject;
        return
                RiskCode.equals(other.getRiskCode())
                && RiskVersion.equals(other.getRiskVersion())
                && CycFlag.equals(other.getCycFlag())
                && CycType.equals(other.getCycType())
                && CycStart == other.getCycStart()
                && CycEnd == other.getCycEnd()
                && OnePayFlag.equals(other.getOnePayFlag())
                && OnePayType.equals(other.getOnePayType())
                && OnePayStart == other.getOnePayStart()
                && OnePayEnd == other.getOnePayEnd()
                && FeeFinaType.equals(other.getFeeFinaType());
    }

    /**
     * 取得Schema拥有字段的数量
     **/
    public int getFieldCount()
    {
        return FIELDNUM;
    }

    /**
     * 取得Schema中指定字段名所对应的索引值
     * 如果没有对应的字段，返回-1
     **/
    public int getFieldIndex(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return 0;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return 1;
        }
        if (strFieldName.equals("CycFlag"))
        {
            return 2;
        }
        if (strFieldName.equals("CycType"))
        {
            return 3;
        }
        if (strFieldName.equals("CycStart"))
        {
            return 4;
        }
        if (strFieldName.equals("CycEnd"))
        {
            return 5;
        }
        if (strFieldName.equals("OnePayFlag"))
        {
            return 6;
        }
        if (strFieldName.equals("OnePayType"))
        {
            return 7;
        }
        if (strFieldName.equals("OnePayStart"))
        {
            return 8;
        }
        if (strFieldName.equals("OnePayEnd"))
        {
            return 9;
        }
        if (strFieldName.equals("FeeFinaType"))
        {
            return 10;
        }
        return -1;
    }

    /**
     * 取得Schema中指定索引值所对应的字段名
     * 如果没有对应的字段，返回""
     **/
    public String getFieldName(int nFieldIndex)
    {
        String strFieldName = "";
        switch (nFieldIndex)
        {
            case 0:
                strFieldName = "RiskCode";
                break;
            case 1:
                strFieldName = "RiskVersion";
                break;
            case 2:
                strFieldName = "CycFlag";
                break;
            case 3:
                strFieldName = "CycType";
                break;
            case 4:
                strFieldName = "CycStart";
                break;
            case 5:
                strFieldName = "CycEnd";
                break;
            case 6:
                strFieldName = "OnePayFlag";
                break;
            case 7:
                strFieldName = "OnePayType";
                break;
            case 8:
                strFieldName = "OnePayStart";
                break;
            case 9:
                strFieldName = "OnePayEnd";
                break;
            case 10:
                strFieldName = "FeeFinaType";
                break;
            default:
                strFieldName = "";
        }
        ;
        return strFieldName;
    }

    /**
     * 取得Schema中指定字段名所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(String strFieldName)
    {
        if (strFieldName.equals("RiskCode"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("RiskVersion"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CycFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CycType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("CycStart"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("CycEnd"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("OnePayFlag"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OnePayType"))
        {
            return Schema.TYPE_STRING;
        }
        if (strFieldName.equals("OnePayStart"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("OnePayEnd"))
        {
            return Schema.TYPE_INT;
        }
        if (strFieldName.equals("FeeFinaType"))
        {
            return Schema.TYPE_STRING;
        }
        return Schema.TYPE_NOFOUND;
    }

    /**
     * 取得Schema中指定索引值所对应的字段类型
     * 如果没有对应的字段，返回Schema.TYPE_NOFOUND
     **/
    public int getFieldType(int nFieldIndex)
    {
        int nFieldType = Schema.TYPE_NOFOUND;
        switch (nFieldIndex)
        {
            case 0:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 1:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 2:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 3:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 4:
                nFieldType = Schema.TYPE_INT;
                break;
            case 5:
                nFieldType = Schema.TYPE_INT;
                break;
            case 6:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 7:
                nFieldType = Schema.TYPE_STRING;
                break;
            case 8:
                nFieldType = Schema.TYPE_INT;
                break;
            case 9:
                nFieldType = Schema.TYPE_INT;
                break;
            case 10:
                nFieldType = Schema.TYPE_STRING;
                break;
            default:
                nFieldType = Schema.TYPE_NOFOUND;
        }
        ;
        return nFieldType;
    }
}
