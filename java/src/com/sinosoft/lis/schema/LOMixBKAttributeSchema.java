/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.schema;

import java.sql.*;
import java.io.*;
import java.util.Date;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LOMixBKAttributeDB;

/*
 * <p>ClassName: LOMixBKAttributeSchema </p>
 * <p>Description: DB层 Schema 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: PhysicalDataModel_1
 * @CreateDate：2010-04-23
 */
public class LOMixBKAttributeSchema implements Schema, Cloneable
{
	// @Field
	/** 银行编码 */
	private String BandCode;
	/** 地区编码 */
	private String ZoneNo;
	/** 银行网点 */
	private String BankNode;
	/** 交叉销售业务类型 */
	private String Crs_BussType;
	/** 交叉销售销售渠道 */
	private String Crs_SaleChnl;
	/** 交叉销售代理机构 */
	private String GrpAgentCom;
	/** 交叉销售代理人编码 */
	private String GrpAgentCode;
	/** 交叉销售代理人姓名 */
	private String GrpAgentName;
	/** 交叉销售代理人身份证 */
	private String GrpAgentIDNo;
	/** 失效日期 */
	private Date Invalidate;
	/** 状态 */
	private String State;
	/** 操作员 */
	private String Operator;
	/** 入机日期 */
	private Date MakeDate;
	/** 入机时间 */
	private String MakeTime;
	/** 最后一次修改日期 */
	private Date ModifyDate;
	/** 最后一次修改时间 */
	private String ModifyTime;
	/** 备用字段1 */
	private String StandByFlag1;
	/** 备用字段2 */
	private String StandByFlag2;
	/** 备用字段3 */
	private String StandByFlag3;

	public static final int FIELDNUM = 19;	// 数据库表的字段个数

	private static String[] PK;				// 主键

	private FDate fDate = new FDate();		// 处理日期

	public CErrors mErrors;			// 错误信息

	// @Constructor
	public LOMixBKAttributeSchema()
	{
		mErrors = new CErrors();

		String[] pk = new String[3];
		pk[0] = "BandCode";
		pk[1] = "ZoneNo";
		pk[2] = "BankNode";

		PK = pk;
	}

	/**
	* Schema克隆
	* @return Object
	* @throws CloneNotSupportedException
	*/
	public Object clone()
					throws CloneNotSupportedException
   {
		LOMixBKAttributeSchema cloned = (LOMixBKAttributeSchema) super.clone();
		cloned.fDate = (FDate) fDate.clone();
		cloned.mErrors = (CErrors) mErrors.clone();
		return cloned;
	}

	// @Method
	public String[] getPK()
	{
		return PK;
	}

	public String getBandCode()
	{
		return BandCode;
	}
	public void setBandCode(String aBandCode)
	{
		BandCode = aBandCode;
	}
	public String getZoneNo()
	{
		return ZoneNo;
	}
	public void setZoneNo(String aZoneNo)
	{
		ZoneNo = aZoneNo;
	}
	public String getBankNode()
	{
		return BankNode;
	}
	public void setBankNode(String aBankNode)
	{
		BankNode = aBankNode;
	}
	public String getCrs_BussType()
	{
		return Crs_BussType;
	}
	public void setCrs_BussType(String aCrs_BussType)
	{
		Crs_BussType = aCrs_BussType;
	}
	public String getCrs_SaleChnl()
	{
		return Crs_SaleChnl;
	}
	public void setCrs_SaleChnl(String aCrs_SaleChnl)
	{
		Crs_SaleChnl = aCrs_SaleChnl;
	}
	public String getGrpAgentCom()
	{
		return GrpAgentCom;
	}
	public void setGrpAgentCom(String aGrpAgentCom)
	{
		GrpAgentCom = aGrpAgentCom;
	}
	public String getGrpAgentCode()
	{
		return GrpAgentCode;
	}
	public void setGrpAgentCode(String aGrpAgentCode)
	{
		GrpAgentCode = aGrpAgentCode;
	}
	public String getGrpAgentName()
	{
		return GrpAgentName;
	}
	public void setGrpAgentName(String aGrpAgentName)
	{
		GrpAgentName = aGrpAgentName;
	}
	public String getGrpAgentIDNo()
	{
		return GrpAgentIDNo;
	}
	public void setGrpAgentIDNo(String aGrpAgentIDNo)
	{
		GrpAgentIDNo = aGrpAgentIDNo;
	}
	public String getInvalidate()
	{
		if( Invalidate != null )
			return fDate.getString(Invalidate);
		else
			return null;
	}
	public void setInvalidate(Date aInvalidate)
	{
		Invalidate = aInvalidate;
	}
	public void setInvalidate(String aInvalidate)
	{
		if (aInvalidate != null && !aInvalidate.equals("") )
		{
			Invalidate = fDate.getDate( aInvalidate );
		}
		else
			Invalidate = null;
	}

	public String getState()
	{
		return State;
	}
	public void setState(String aState)
	{
		State = aState;
	}
	public String getOperator()
	{
		return Operator;
	}
	public void setOperator(String aOperator)
	{
		Operator = aOperator;
	}
	public String getMakeDate()
	{
		if( MakeDate != null )
			return fDate.getString(MakeDate);
		else
			return null;
	}
	public void setMakeDate(Date aMakeDate)
	{
		MakeDate = aMakeDate;
	}
	public void setMakeDate(String aMakeDate)
	{
		if (aMakeDate != null && !aMakeDate.equals("") )
		{
			MakeDate = fDate.getDate( aMakeDate );
		}
		else
			MakeDate = null;
	}

	public String getMakeTime()
	{
		return MakeTime;
	}
	public void setMakeTime(String aMakeTime)
	{
		MakeTime = aMakeTime;
	}
	public String getModifyDate()
	{
		if( ModifyDate != null )
			return fDate.getString(ModifyDate);
		else
			return null;
	}
	public void setModifyDate(Date aModifyDate)
	{
		ModifyDate = aModifyDate;
	}
	public void setModifyDate(String aModifyDate)
	{
		if (aModifyDate != null && !aModifyDate.equals("") )
		{
			ModifyDate = fDate.getDate( aModifyDate );
		}
		else
			ModifyDate = null;
	}

	public String getModifyTime()
	{
		return ModifyTime;
	}
	public void setModifyTime(String aModifyTime)
	{
		ModifyTime = aModifyTime;
	}
	public String getStandByFlag1()
	{
		return StandByFlag1;
	}
	public void setStandByFlag1(String aStandByFlag1)
	{
		StandByFlag1 = aStandByFlag1;
	}
	public String getStandByFlag2()
	{
		return StandByFlag2;
	}
	public void setStandByFlag2(String aStandByFlag2)
	{
		StandByFlag2 = aStandByFlag2;
	}
	public String getStandByFlag3()
	{
		return StandByFlag3;
	}
	public void setStandByFlag3(String aStandByFlag3)
	{
		StandByFlag3 = aStandByFlag3;
	}

	/**
	* 使用另外一个 LOMixBKAttributeSchema 对象给 Schema 赋值
	* @param: aLOMixBKAttributeSchema LOMixBKAttributeSchema
	**/
	public void setSchema(LOMixBKAttributeSchema aLOMixBKAttributeSchema)
	{
		this.BandCode = aLOMixBKAttributeSchema.getBandCode();
		this.ZoneNo = aLOMixBKAttributeSchema.getZoneNo();
		this.BankNode = aLOMixBKAttributeSchema.getBankNode();
		this.Crs_BussType = aLOMixBKAttributeSchema.getCrs_BussType();
		this.Crs_SaleChnl = aLOMixBKAttributeSchema.getCrs_SaleChnl();
		this.GrpAgentCom = aLOMixBKAttributeSchema.getGrpAgentCom();
		this.GrpAgentCode = aLOMixBKAttributeSchema.getGrpAgentCode();
		this.GrpAgentName = aLOMixBKAttributeSchema.getGrpAgentName();
		this.GrpAgentIDNo = aLOMixBKAttributeSchema.getGrpAgentIDNo();
		this.Invalidate = fDate.getDate( aLOMixBKAttributeSchema.getInvalidate());
		this.State = aLOMixBKAttributeSchema.getState();
		this.Operator = aLOMixBKAttributeSchema.getOperator();
		this.MakeDate = fDate.getDate( aLOMixBKAttributeSchema.getMakeDate());
		this.MakeTime = aLOMixBKAttributeSchema.getMakeTime();
		this.ModifyDate = fDate.getDate( aLOMixBKAttributeSchema.getModifyDate());
		this.ModifyTime = aLOMixBKAttributeSchema.getModifyTime();
		this.StandByFlag1 = aLOMixBKAttributeSchema.getStandByFlag1();
		this.StandByFlag2 = aLOMixBKAttributeSchema.getStandByFlag2();
		this.StandByFlag3 = aLOMixBKAttributeSchema.getStandByFlag3();
	}

	/**
	* 使用 ResultSet 中的第 i 行给 Schema 赋值
	* @param: rs ResultSet
	* @param: i int
	* @return: boolean
	**/
	public boolean setSchema(ResultSet rs,int i)
	{
		try
		{
			//rs.absolute(i);		// 非滚动游标
			if( rs.getString("BandCode") == null )
				this.BandCode = null;
			else
				this.BandCode = rs.getString("BandCode").trim();

			if( rs.getString("ZoneNo") == null )
				this.ZoneNo = null;
			else
				this.ZoneNo = rs.getString("ZoneNo").trim();

			if( rs.getString("BankNode") == null )
				this.BankNode = null;
			else
				this.BankNode = rs.getString("BankNode").trim();

			if( rs.getString("Crs_BussType") == null )
				this.Crs_BussType = null;
			else
				this.Crs_BussType = rs.getString("Crs_BussType").trim();

			if( rs.getString("Crs_SaleChnl") == null )
				this.Crs_SaleChnl = null;
			else
				this.Crs_SaleChnl = rs.getString("Crs_SaleChnl").trim();

			if( rs.getString("GrpAgentCom") == null )
				this.GrpAgentCom = null;
			else
				this.GrpAgentCom = rs.getString("GrpAgentCom").trim();

			if( rs.getString("GrpAgentCode") == null )
				this.GrpAgentCode = null;
			else
				this.GrpAgentCode = rs.getString("GrpAgentCode").trim();

			if( rs.getString("GrpAgentName") == null )
				this.GrpAgentName = null;
			else
				this.GrpAgentName = rs.getString("GrpAgentName").trim();

			if( rs.getString("GrpAgentIDNo") == null )
				this.GrpAgentIDNo = null;
			else
				this.GrpAgentIDNo = rs.getString("GrpAgentIDNo").trim();

			this.Invalidate = rs.getDate("Invalidate");
			if( rs.getString("State") == null )
				this.State = null;
			else
				this.State = rs.getString("State").trim();

			if( rs.getString("Operator") == null )
				this.Operator = null;
			else
				this.Operator = rs.getString("Operator").trim();

			this.MakeDate = rs.getDate("MakeDate");
			if( rs.getString("MakeTime") == null )
				this.MakeTime = null;
			else
				this.MakeTime = rs.getString("MakeTime").trim();

			this.ModifyDate = rs.getDate("ModifyDate");
			if( rs.getString("ModifyTime") == null )
				this.ModifyTime = null;
			else
				this.ModifyTime = rs.getString("ModifyTime").trim();

			if( rs.getString("StandByFlag1") == null )
				this.StandByFlag1 = null;
			else
				this.StandByFlag1 = rs.getString("StandByFlag1").trim();

			if( rs.getString("StandByFlag2") == null )
				this.StandByFlag2 = null;
			else
				this.StandByFlag2 = rs.getString("StandByFlag2").trim();

			if( rs.getString("StandByFlag3") == null )
				this.StandByFlag3 = null;
			else
				this.StandByFlag3 = rs.getString("StandByFlag3").trim();

		}
		catch(SQLException sqle)
		{
			System.out.println("数据库中的LOMixBKAttribute表字段个数和Schema中的字段个数不一致，或者执行db.executeQuery查询时没有使用select * from tables");
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMixBKAttributeSchema";
			tError.functionName = "setSchema";
			tError.errorMessage = sqle.toString();
			this.mErrors .addOneError(tError);
			return false;
		}
		return true;
	}

	public LOMixBKAttributeSchema getSchema()
	{
		LOMixBKAttributeSchema aLOMixBKAttributeSchema = new LOMixBKAttributeSchema();
		aLOMixBKAttributeSchema.setSchema(this);
		return aLOMixBKAttributeSchema;
	}

	public LOMixBKAttributeDB getDB()
	{
		LOMixBKAttributeDB aDBOper = new LOMixBKAttributeDB();
		aDBOper.setSchema(this);
		return aDBOper;
	}


	/**
	* 数据打包，按 XML 格式打包，顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMixBKAttribute描述/A>表字段
	* @return: String 返回打包后字符串
	**/
	public String encode()
	{
		StringBuffer strReturn = new StringBuffer(256);
		strReturn.append(StrTool.cTrim(BandCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ZoneNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(BankNode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_BussType)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Crs_SaleChnl)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCom)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentCode)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentName)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(GrpAgentIDNo)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( Invalidate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(State)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(Operator)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( MakeDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(MakeTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(fDate.getString( ModifyDate ))); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(ModifyTime)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByFlag1)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByFlag2)); strReturn.append(SysConst.PACKAGESPILTER);
		strReturn.append(StrTool.cTrim(StandByFlag3));
		return strReturn.toString();
	}

	/**
	* 数据解包，解包顺序参见<A href ={@docRoot}/dataStructure/tb.html#PrpLOMixBKAttribute>历史记账凭证主表信息</A>表字段
	* @param: strMessage String 包含一条纪录数据的字符串
	* @return: boolean
	**/
	public boolean decode(String strMessage)
	{
		try
		{
			BandCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 1, SysConst.PACKAGESPILTER );
			ZoneNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 2, SysConst.PACKAGESPILTER );
			BankNode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 3, SysConst.PACKAGESPILTER );
			Crs_BussType = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 4, SysConst.PACKAGESPILTER );
			Crs_SaleChnl = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 5, SysConst.PACKAGESPILTER );
			GrpAgentCom = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 6, SysConst.PACKAGESPILTER );
			GrpAgentCode = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 7, SysConst.PACKAGESPILTER );
			GrpAgentName = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 8, SysConst.PACKAGESPILTER );
			GrpAgentIDNo = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 9, SysConst.PACKAGESPILTER );
			Invalidate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 10,SysConst.PACKAGESPILTER));
			State = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 11, SysConst.PACKAGESPILTER );
			Operator = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 12, SysConst.PACKAGESPILTER );
			MakeDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 13,SysConst.PACKAGESPILTER));
			MakeTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 14, SysConst.PACKAGESPILTER );
			ModifyDate = fDate.getDate(StrTool.getStr(StrTool.GBKToUnicode(strMessage), 15,SysConst.PACKAGESPILTER));
			ModifyTime = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 16, SysConst.PACKAGESPILTER );
			StandByFlag1 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 17, SysConst.PACKAGESPILTER );
			StandByFlag2 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 18, SysConst.PACKAGESPILTER );
			StandByFlag3 = StrTool.getStr(StrTool.GBKToUnicode(strMessage), 19, SysConst.PACKAGESPILTER );
		}
		catch(NumberFormatException ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LOMixBKAttributeSchema";
			tError.functionName = "decode";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);

			return false;
		}
		return true;
	}

	/**
	* 取得对应传入参数的String形式的字段值
	* @param: FCode String 希望取得的字段名
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(String FCode)
	{
		String strReturn = "";
		if (FCode.equals("BandCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BandCode));
		}
		if (FCode.equals("ZoneNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ZoneNo));
		}
		if (FCode.equals("BankNode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(BankNode));
		}
		if (FCode.equals("Crs_BussType"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_BussType));
		}
		if (FCode.equals("Crs_SaleChnl"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Crs_SaleChnl));
		}
		if (FCode.equals("GrpAgentCom"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCom));
		}
		if (FCode.equals("GrpAgentCode"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentCode));
		}
		if (FCode.equals("GrpAgentName"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentName));
		}
		if (FCode.equals("GrpAgentIDNo"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(GrpAgentIDNo));
		}
		if (FCode.equals("Invalidate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getInvalidate()));
		}
		if (FCode.equals("State"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(State));
		}
		if (FCode.equals("Operator"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(Operator));
		}
		if (FCode.equals("MakeDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
		}
		if (FCode.equals("MakeTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(MakeTime));
		}
		if (FCode.equals("ModifyDate"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
		}
		if (FCode.equals("ModifyTime"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(ModifyTime));
		}
		if (FCode.equals("StandByFlag1"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag1));
		}
		if (FCode.equals("StandByFlag2"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag2));
		}
		if (FCode.equals("StandByFlag3"))
		{
			strReturn = StrTool.GBKToUnicode(String.valueOf(StandByFlag3));
		}
		if (strReturn.equals(""))
		{
			strReturn = "null";
		}

		return strReturn;
	}


	/**
	* 取得Schema中指定索引值所对应的字段值
	* @param: nFieldIndex int 指定的字段索引值
	* @return: String
	* 如果没有对应的字段，返回""
	* 如果字段值为空，返回"null"
	**/
	public String getV(int nFieldIndex)
	{
		String strFieldValue = "";
		switch(nFieldIndex) {
			case 0:
				strFieldValue = StrTool.GBKToUnicode(BandCode);
				break;
			case 1:
				strFieldValue = StrTool.GBKToUnicode(ZoneNo);
				break;
			case 2:
				strFieldValue = StrTool.GBKToUnicode(BankNode);
				break;
			case 3:
				strFieldValue = StrTool.GBKToUnicode(Crs_BussType);
				break;
			case 4:
				strFieldValue = StrTool.GBKToUnicode(Crs_SaleChnl);
				break;
			case 5:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCom);
				break;
			case 6:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentCode);
				break;
			case 7:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentName);
				break;
			case 8:
				strFieldValue = StrTool.GBKToUnicode(GrpAgentIDNo);
				break;
			case 9:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getInvalidate()));
				break;
			case 10:
				strFieldValue = StrTool.GBKToUnicode(State);
				break;
			case 11:
				strFieldValue = StrTool.GBKToUnicode(Operator);
				break;
			case 12:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getMakeDate()));
				break;
			case 13:
				strFieldValue = StrTool.GBKToUnicode(MakeTime);
				break;
			case 14:
				strFieldValue = StrTool.GBKToUnicode(String.valueOf( this.getModifyDate()));
				break;
			case 15:
				strFieldValue = StrTool.GBKToUnicode(ModifyTime);
				break;
			case 16:
				strFieldValue = StrTool.GBKToUnicode(StandByFlag1);
				break;
			case 17:
				strFieldValue = StrTool.GBKToUnicode(StandByFlag2);
				break;
			case 18:
				strFieldValue = StrTool.GBKToUnicode(StandByFlag3);
				break;
			default:
				strFieldValue = "";
		};
		if( strFieldValue.equals("") ) {
			strFieldValue = "null";
		}
		return strFieldValue;
	}

	/**
	* 设置对应传入参数的String形式的字段值
	* @param: FCode String 需要赋值的对象
	* @param: FValue String 要赋的值
	* @return: boolean
	**/
	public boolean setV(String FCode ,String FValue)
	{
		if( StrTool.cTrim( FCode ).equals( "" ))
			return false;

		if (FCode.equalsIgnoreCase("BandCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BandCode = FValue.trim();
			}
			else
				BandCode = null;
		}
		if (FCode.equalsIgnoreCase("ZoneNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ZoneNo = FValue.trim();
			}
			else
				ZoneNo = null;
		}
		if (FCode.equalsIgnoreCase("BankNode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				BankNode = FValue.trim();
			}
			else
				BankNode = null;
		}
		if (FCode.equalsIgnoreCase("Crs_BussType"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_BussType = FValue.trim();
			}
			else
				Crs_BussType = null;
		}
		if (FCode.equalsIgnoreCase("Crs_SaleChnl"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Crs_SaleChnl = FValue.trim();
			}
			else
				Crs_SaleChnl = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCom"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCom = FValue.trim();
			}
			else
				GrpAgentCom = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentCode"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentCode = FValue.trim();
			}
			else
				GrpAgentCode = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentName"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentName = FValue.trim();
			}
			else
				GrpAgentName = null;
		}
		if (FCode.equalsIgnoreCase("GrpAgentIDNo"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				GrpAgentIDNo = FValue.trim();
			}
			else
				GrpAgentIDNo = null;
		}
		if (FCode.equalsIgnoreCase("Invalidate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				Invalidate = fDate.getDate( FValue );
			}
			else
				Invalidate = null;
		}
		if (FCode.equalsIgnoreCase("State"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				State = FValue.trim();
			}
			else
				State = null;
		}
		if (FCode.equalsIgnoreCase("Operator"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				Operator = FValue.trim();
			}
			else
				Operator = null;
		}
		if (FCode.equalsIgnoreCase("MakeDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				MakeDate = fDate.getDate( FValue );
			}
			else
				MakeDate = null;
		}
		if (FCode.equalsIgnoreCase("MakeTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				MakeTime = FValue.trim();
			}
			else
				MakeTime = null;
		}
		if (FCode.equalsIgnoreCase("ModifyDate"))
		{
			if( FValue != null && !FValue.equals("") )
			{
				ModifyDate = fDate.getDate( FValue );
			}
			else
				ModifyDate = null;
		}
		if (FCode.equalsIgnoreCase("ModifyTime"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				ModifyTime = FValue.trim();
			}
			else
				ModifyTime = null;
		}
		if (FCode.equalsIgnoreCase("StandByFlag1"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByFlag1 = FValue.trim();
			}
			else
				StandByFlag1 = null;
		}
		if (FCode.equalsIgnoreCase("StandByFlag2"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByFlag2 = FValue.trim();
			}
			else
				StandByFlag2 = null;
		}
		if (FCode.equalsIgnoreCase("StandByFlag3"))
		{
			if( FValue != null && !FValue.equals(""))
			{
				StandByFlag3 = FValue.trim();
			}
			else
				StandByFlag3 = null;
		}
		return true;
	}

	public boolean equals(Object otherObject)
	{
		if (this == otherObject) return true;
		if (otherObject == null) return false;
		if (getClass() != otherObject.getClass()) return false;
		LOMixBKAttributeSchema other = (LOMixBKAttributeSchema)otherObject;
		return
			(BandCode == null ? other.getBandCode() == null : BandCode.equals(other.getBandCode()))
			&& (ZoneNo == null ? other.getZoneNo() == null : ZoneNo.equals(other.getZoneNo()))
			&& (BankNode == null ? other.getBankNode() == null : BankNode.equals(other.getBankNode()))
			&& (Crs_BussType == null ? other.getCrs_BussType() == null : Crs_BussType.equals(other.getCrs_BussType()))
			&& (Crs_SaleChnl == null ? other.getCrs_SaleChnl() == null : Crs_SaleChnl.equals(other.getCrs_SaleChnl()))
			&& (GrpAgentCom == null ? other.getGrpAgentCom() == null : GrpAgentCom.equals(other.getGrpAgentCom()))
			&& (GrpAgentCode == null ? other.getGrpAgentCode() == null : GrpAgentCode.equals(other.getGrpAgentCode()))
			&& (GrpAgentName == null ? other.getGrpAgentName() == null : GrpAgentName.equals(other.getGrpAgentName()))
			&& (GrpAgentIDNo == null ? other.getGrpAgentIDNo() == null : GrpAgentIDNo.equals(other.getGrpAgentIDNo()))
			&& (Invalidate == null ? other.getInvalidate() == null : fDate.getString(Invalidate).equals(other.getInvalidate()))
			&& (State == null ? other.getState() == null : State.equals(other.getState()))
			&& (Operator == null ? other.getOperator() == null : Operator.equals(other.getOperator()))
			&& (MakeDate == null ? other.getMakeDate() == null : fDate.getString(MakeDate).equals(other.getMakeDate()))
			&& (MakeTime == null ? other.getMakeTime() == null : MakeTime.equals(other.getMakeTime()))
			&& (ModifyDate == null ? other.getModifyDate() == null : fDate.getString(ModifyDate).equals(other.getModifyDate()))
			&& (ModifyTime == null ? other.getModifyTime() == null : ModifyTime.equals(other.getModifyTime()))
			&& (StandByFlag1 == null ? other.getStandByFlag1() == null : StandByFlag1.equals(other.getStandByFlag1()))
			&& (StandByFlag2 == null ? other.getStandByFlag2() == null : StandByFlag2.equals(other.getStandByFlag2()))
			&& (StandByFlag3 == null ? other.getStandByFlag3() == null : StandByFlag3.equals(other.getStandByFlag3()));
	}

	/**
	* 取得Schema拥有字段的数量
       * @return: int
	**/
	public int getFieldCount()
	{
 		return FIELDNUM;
	}

	/**
	* 取得Schema中指定字段名所对应的索引值
	* 如果没有对应的字段，返回-1
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldIndex(String strFieldName)
	{
		if( strFieldName.equals("BandCode") ) {
			return 0;
		}
		if( strFieldName.equals("ZoneNo") ) {
			return 1;
		}
		if( strFieldName.equals("BankNode") ) {
			return 2;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return 3;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return 4;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return 5;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return 6;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return 7;
		}
		if( strFieldName.equals("GrpAgentIDNo") ) {
			return 8;
		}
		if( strFieldName.equals("Invalidate") ) {
			return 9;
		}
		if( strFieldName.equals("State") ) {
			return 10;
		}
		if( strFieldName.equals("Operator") ) {
			return 11;
		}
		if( strFieldName.equals("MakeDate") ) {
			return 12;
		}
		if( strFieldName.equals("MakeTime") ) {
			return 13;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return 14;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return 15;
		}
		if( strFieldName.equals("StandByFlag1") ) {
			return 16;
		}
		if( strFieldName.equals("StandByFlag2") ) {
			return 17;
		}
		if( strFieldName.equals("StandByFlag3") ) {
			return 18;
		}
		return -1;
	}

	/**
	* 取得Schema中指定索引值所对应的字段名
	* 如果没有对应的字段，返回""
       * @param: nFieldIndex int
       * @return: String
	**/
	public String getFieldName(int nFieldIndex)
	{
		String strFieldName = "";
		switch(nFieldIndex) {
			case 0:
				strFieldName = "BandCode";
				break;
			case 1:
				strFieldName = "ZoneNo";
				break;
			case 2:
				strFieldName = "BankNode";
				break;
			case 3:
				strFieldName = "Crs_BussType";
				break;
			case 4:
				strFieldName = "Crs_SaleChnl";
				break;
			case 5:
				strFieldName = "GrpAgentCom";
				break;
			case 6:
				strFieldName = "GrpAgentCode";
				break;
			case 7:
				strFieldName = "GrpAgentName";
				break;
			case 8:
				strFieldName = "GrpAgentIDNo";
				break;
			case 9:
				strFieldName = "Invalidate";
				break;
			case 10:
				strFieldName = "State";
				break;
			case 11:
				strFieldName = "Operator";
				break;
			case 12:
				strFieldName = "MakeDate";
				break;
			case 13:
				strFieldName = "MakeTime";
				break;
			case 14:
				strFieldName = "ModifyDate";
				break;
			case 15:
				strFieldName = "ModifyTime";
				break;
			case 16:
				strFieldName = "StandByFlag1";
				break;
			case 17:
				strFieldName = "StandByFlag2";
				break;
			case 18:
				strFieldName = "StandByFlag3";
				break;
			default:
				strFieldName = "";
		};
		return strFieldName;
	}

	/**
	* 取得Schema中指定字段名所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: strFieldName String
       * @return: int
	**/
	public int getFieldType(String strFieldName)
	{
		if( strFieldName.equals("BandCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ZoneNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("BankNode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_BussType") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Crs_SaleChnl") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCom") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentCode") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentName") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("GrpAgentIDNo") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Invalidate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("State") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("Operator") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("MakeDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("MakeTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("ModifyDate") ) {
			return Schema.TYPE_DATE;
		}
		if( strFieldName.equals("ModifyTime") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByFlag1") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByFlag2") ) {
			return Schema.TYPE_STRING;
		}
		if( strFieldName.equals("StandByFlag3") ) {
			return Schema.TYPE_STRING;
		}
		return Schema.TYPE_NOFOUND;
	}

	/**
	* 取得Schema中指定索引值所对应的字段类型
	* 如果没有对应的字段，返回Schema.TYPE_NOFOUND
       * @param: nFieldIndex int
       * @return: int
	**/
	public int getFieldType(int nFieldIndex)
	{
		int nFieldType = Schema.TYPE_NOFOUND;
		switch(nFieldIndex) {
			case 0:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 1:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 2:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 3:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 4:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 5:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 6:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 7:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 8:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 9:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 10:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 11:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 12:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 13:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 14:
				nFieldType = Schema.TYPE_DATE;
				break;
			case 15:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 16:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 17:
				nFieldType = Schema.TYPE_STRING;
				break;
			case 18:
				nFieldType = Schema.TYPE_STRING;
				break;
			default:
				nFieldType = Schema.TYPE_NOFOUND;
		};
		return nFieldType;
	}
}
